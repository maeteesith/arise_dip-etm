﻿using System.Runtime.InteropServices;

namespace Rotativa.AspNetCore
{
    public class WkhtmltopdfDriver : WkhtmlDriver
    {
        private const string wkhtmlExe = "wkhtmltopdf.exe";
        private const string wkhtmlDll = "wkhtmltopdf";

        /// <summary>
        /// Converts given HTML string to PDF.
        /// </summary>
        /// <param name="wkhtmltopdfPath">Path to wkthmltopdf.</param>
        /// <param name="switches">Switches that will be passed to wkhtmltopdf binary.</param>
        /// <param name="html">String containing HTML code that should be converted to PDF.</param>
        /// <returns>PDF as byte array.</returns>
        public static byte[] ConvertHtml(string wkhtmltopdfPath, string switches, string html)
        {
            bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
            bool isOSX = RuntimeInformation.IsOSPlatform(OSPlatform.OSX);
            if (isWindows)
            {
                return Convert(wkhtmltopdfPath+ "/windows", switches, html, wkhtmlExe);
            }
            else if (isOSX)
            {
                return Convert(wkhtmltopdfPath+ "/macos", switches, html, wkhtmlDll);
            }
            else
            {
                return Convert(wkhtmltopdfPath+"/linux", switches, html, wkhtmlDll);
            }
        }

        /// <summary>
        /// Converts given URL to PDF.
        /// </summary>
        /// <param name="wkhtmltopdfPath">Path to wkthmltopdf.</param>
        /// <param name="switches">Switches that will be passed to wkhtmltopdf binary.</param>
        /// <returns>PDF as byte array.</returns>
        public static byte[] Convert(string wkhtmltopdfPath, string switches)
        {
            return Convert(wkhtmltopdfPath, switches, null, wkhtmlExe);
        }
    }
}
