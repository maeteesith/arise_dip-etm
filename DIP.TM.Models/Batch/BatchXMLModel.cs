﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System;
using System.Xml.Serialization;

namespace DIP.TM.Models.BatchXML
{
    public class BatchXMLModel
    {

    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    [XmlRoot(ElementName="NAME")]
	public class NAME {
		[XmlElement(ElementName="NAMEL")]
		public List<string> NAMEL { get; set; }
	}

	[XmlRoot(ElementName="ADDRESS")]
	public class ADDRESS {
		[XmlElement(ElementName="ADDRL")]
		public List<string> ADDRL { get; set; }
		[XmlElement(ElementName="COUNTRY")]
		public string COUNTRY { get; set; }
	}

	[XmlRoot(ElementName="ENTADDR")]
	public class ENTADDR {
		[XmlElement(ElementName="ADDRL")]
		public List<string> ADDRL { get; set; }
		[XmlElement(ElementName="COUNTRY")]
		public string COUNTRY { get; set; }
	}

	[XmlRoot(ElementName="LEGNATU")]
	public class LEGNATU {
		[XmlElement(ElementName="LEGNATT")]
		public string LEGNATT { get; set; }
		[XmlElement(ElementName="PLAINCO")]
		public string PLAINCO { get; set; }
	}

	[XmlRoot(ElementName="HOLGR")]
	public class HOLGR {
		[XmlElement(ElementName="NAME")]
		public NAME NAME { get; set; }
		[XmlElement(ElementName="ADDRESS")]
		public ADDRESS ADDRESS { get; set; }
		[XmlElement(ElementName="ENTEST")]
		public string ENTEST { get; set; }
		[XmlElement(ElementName="ENTADDR")]
		public ENTADDR ENTADDR { get; set; }
		[XmlElement(ElementName="LEGNATU")]
		public LEGNATU LEGNATU { get; set; }
		[XmlElement(ElementName="CORRIND")]
		public string CORRIND { get; set; }
		[XmlAttribute(AttributeName="CLID")]
		public string CLID { get; set; }
		[XmlAttribute(AttributeName="NOTLANG")]
		public string NOTLANG { get; set; }
		[XmlElement(ElementName="ENTNATL")]
		public string ENTNATL { get; set; }
		[XmlElement(ElementName="ENTDOM")]
		public string ENTDOM { get; set; }
		[XmlElement(ElementName="NATLTY")]
		public string NATLTY { get; set; }
	}

	[XmlRoot(ElementName="REPGR")]
	public class REPGR {
		[XmlElement(ElementName="NAME")]
		public NAME NAME { get; set; }
		[XmlElement(ElementName="ADDRESS")]
		public ADDRESS ADDRESS { get; set; }
		[XmlAttribute(AttributeName="CLID")]
		public string CLID { get; set; }
	}

	[XmlRoot(ElementName="IMAGE")]
	public class IMAGE {
		[XmlAttribute(AttributeName="NAME")]
		public string NAME { get; set; }
		[XmlAttribute(AttributeName="TEXT")]
		public string TEXT { get; set; }
		[XmlAttribute(AttributeName="COLOUR")]
		public string COLOUR { get; set; }
		[XmlAttribute(AttributeName="TYPE")]
		public string TYPE { get; set; }
	}

	[XmlRoot(ElementName="GSGR")]
	public class GSGR {
		[XmlElement(ElementName="GSTERMEN")]
		public string GSTERMEN { get; set; }
		[XmlAttribute(AttributeName="NICCLAI")]
		public string NICCLAI { get; set; }
	}

	[XmlRoot(ElementName="BASICGS")]
	public class BASICGS {
		[XmlElement(ElementName="GSGR")]
		public List<GSGR> GSGR { get; set; }
		[XmlAttribute(AttributeName="NICEVER")]
		public string NICEVER { get; set; }
	}

	[XmlRoot(ElementName="BASAPPGR")]
	public class BASAPPGR {
		[XmlElement(ElementName="BASAPPD")]
		public string BASAPPD { get; set; }
		[XmlElement(ElementName="BASAPPN")]
		public string BASAPPN { get; set; }
	}

	[XmlRoot(ElementName="BASGR")]
	public class BASGR {
		[XmlElement(ElementName="BASAPPGR")]
		public BASAPPGR BASAPPGR { get; set; }
		[XmlElement(ElementName="BASREGGR")]
		public BASREGGR BASREGGR { get; set; }
	}

	[XmlRoot(ElementName="PRIGR")]
	public class PRIGR {
		[XmlElement(ElementName="PRICP")]
		public string PRICP { get; set; }
		[XmlElement(ElementName="PRIAPPD")]
		public string PRIAPPD { get; set; }
		[XmlElement(ElementName="PRIAPPN")]
		public string PRIAPPN { get; set; }
		[XmlElement(ElementName="PRIGS")]
		public List<PRIGS> PRIGS { get; set; }
	}

	[XmlRoot(ElementName="DESPG")]
	public class DESPG {
		[XmlElement(ElementName="DCPCD")]
		public List<string> DCPCD { get; set; }
	}

	[XmlRoot(ElementName="BIRTH")]
	public class BIRTH {
		[XmlElement(ElementName="HOLGR")]
		public List<HOLGR> HOLGR { get; set; }
		[XmlElement(ElementName="REPGR")]
		public REPGR REPGR { get; set; }
		[XmlElement(ElementName="IMAGE")]
		public IMAGE IMAGE { get; set; }
		[XmlElement(ElementName="STDMIND")]
		public string STDMIND { get; set; }
		[XmlElement(ElementName="MARDUR")]
		public string MARDUR { get; set; }
		[XmlElement(ElementName="VRBLNOT")]
		public string VRBLNOT { get; set; }
		[XmlElement(ElementName="BASICGS")]
		public BASICGS BASICGS { get; set; }
		[XmlElement(ElementName="BASGR")]
		public List<BASGR> BASGR { get; set; }
		[XmlElement(ElementName="PRIGR")]
		public List<PRIGR> PRIGR { get; set; }
		[XmlElement(ElementName="DESPG")]
		public DESPG DESPG { get; set; }
		[XmlAttribute(AttributeName="TRANTYP")]
		public string TRANTYP { get; set; }
		[XmlAttribute(AttributeName="INTREGN")]
		public string INTREGN { get; set; }
		[XmlAttribute(AttributeName="OOCD")]
		public string OOCD { get; set; }
		[XmlAttribute(AttributeName="ORIGLAN")]
		public string ORIGLAN { get; set; }
		[XmlAttribute(AttributeName="EXPDATE")]
		public string EXPDATE { get; set; }
		[XmlAttribute(AttributeName="DOCID")]
		public string DOCID { get; set; }
		[XmlAttribute(AttributeName="REGRDAT")]
		public string REGRDAT { get; set; }
		[XmlAttribute(AttributeName="NOTDATE")]
		public string NOTDATE { get; set; }
		[XmlAttribute(AttributeName="REGEDAT")]
		public string REGEDAT { get; set; }
		[XmlAttribute(AttributeName="OFFREF")]
		public string OFFREF { get; set; }
		[XmlAttribute(AttributeName="INTREGD")]
		public string INTREGD { get; set; }
		[XmlAttribute(AttributeName="DESUNDER")]
		public string DESUNDER { get; set; }
		[XmlElement(ElementName="VIENNAGR")]
		public VIENNAGR VIENNAGR { get; set; }
		[XmlElement(ElementName="DESPG2")]
		public DESPG2 DESPG2 { get; set; }
		[XmlElement(ElementName="MARCOLI")]
		public string MARCOLI { get; set; }
		[XmlElement(ElementName="COLCLAGR")]
		public COLCLAGR COLCLAGR { get; set; }
		[XmlElement(ElementName="SIGVRBL")]
		public string SIGVRBL { get; set; }
		[XmlElement(ElementName="LIMGR")]
		public LIMGR LIMGR { get; set; }
		[XmlElement(ElementName="MARTRAN")]
		public string MARTRAN { get; set; }
		[XmlElement(ElementName="MARTRGR")]
		public MARTRGR MARTRGR { get; set; }
		[XmlElement(ElementName="CORRGR")]
		public CORRGR CORRGR { get; set; }
		[XmlElement(ElementName="VOLDESGR")]
		public VOLDESGR VOLDESGR { get; set; }
		[XmlElement(ElementName="MARDESGR")]
		public MARDESGR MARDESGR { get; set; }
		[XmlElement(ElementName="DISCLAIMGR")]
		public DISCLAIMGR DISCLAIMGR { get; set; }
		[XmlAttribute(AttributeName="RENDATE")]
		public string RENDATE { get; set; }
		[XmlElement(ElementName="CBOP")]
		public CBOP CBOP { get; set; }
		[XmlElement(ElementName="LIO")]
		public LIO LIO { get; set; }
		[XmlElement(ElementName="RFOT")]
		public RFOT RFOT { get; set; }
	}

	[XmlRoot(ElementName="INCORRECT")]
	public class INCORRECT {
		[XmlElement(ElementName="BIRTH")]
		public BIRTH BIRTH { get; set; }
	}

	[XmlRoot(ElementName="CORRECT")]
	public class CORRECT {
		[XmlElement(ElementName="BIRTH")]
		public BIRTH BIRTH { get; set; }
		[XmlAttribute(AttributeName="CHANGED-CORR")]
		public string CHANGEDCORR { get; set; }
		[XmlAttribute(AttributeName="CHANGED-REP")]
		public string CHANGEDREP { get; set; }
		[XmlAttribute(AttributeName="CHANGED-OR-GS")]
		public string CHANGEDORGS { get; set; }
		[XmlAttribute(AttributeName="CHANGED-OR")]
		public string CHANGEDOR { get; set; }
		[XmlAttribute(AttributeName="CHANGED-BASIC")]
		public string CHANGEDBASIC { get; set; }
		[XmlAttribute(AttributeName="CHANGED-DESGTN")]
		public string CHANGEDDESGTN { get; set; }
		[XmlAttribute(AttributeName="CHANGED-IMAGE")]
		public string CHANGEDIMAGE { get; set; }
		[XmlAttribute(AttributeName="CHANGED-GS")]
		public string CHANGEDGS { get; set; }
		[XmlAttribute(AttributeName="CHANGED-HOLDER")]
		public string CHANGEDHOLDER { get; set; }
		[XmlAttribute(AttributeName="CHANGED-IR")]
		public string CHANGEDIR { get; set; }
		[XmlAttribute(AttributeName="CHANGED-PRTY")]
		public string CHANGEDPRTY { get; set; }
		[XmlAttribute(AttributeName="CHANGED-IRTEXT")]
		public string CHANGEDIRTEXT { get; set; }
		[XmlAttribute(AttributeName="CHANGED-TYPE")]
		public string CHANGEDTYPE { get; set; }
	}

	[XmlRoot(ElementName="CORRECTION")]
	public class CORRECTION {
		[XmlElement(ElementName="TEXTEN")]
		public string TEXTEN { get; set; }
		[XmlElement(ElementName="GAZNUM")]
		public string GAZNUM { get; set; }
		[XmlElement(ElementName="INCORRECT")]
		public INCORRECT INCORRECT { get; set; }
		[XmlElement(ElementName="CORRECT")]
		public CORRECT CORRECT { get; set; }
		[XmlAttribute(AttributeName="DOCID")]
		public string DOCID { get; set; }
		[XmlAttribute(AttributeName="REGEDAT")]
		public string REGEDAT { get; set; }
		[XmlAttribute(AttributeName="OFFREF")]
		public string OFFREF { get; set; }
	}

	[XmlRoot(ElementName="VIENNAGR")]
	public class VIENNAGR {
		[XmlElement(ElementName="VIECLAI")]
		public List<string> VIECLAI { get; set; }
		[XmlElement(ElementName="VIECLA3")]
		public List<string> VIECLA3 { get; set; }
		[XmlAttribute(AttributeName="VIENVER")]
		public string VIENVER { get; set; }
	}

	[XmlRoot(ElementName="DESPG2")]
	public class DESPG2 {
		[XmlElement(ElementName="DCPCD")]
		public List<string> DCPCD { get; set; }
	}

	[XmlRoot(ElementName="COLCLAGR")]
	public class COLCLAGR {
		[XmlElement(ElementName="COLCLAEN")]
		public string COLCLAEN { get; set; }
		[XmlElement(ElementName="COLPAREN")]
		public string COLPAREN { get; set; }
	}

	[XmlRoot(ElementName="BASREGGR")]
	public class BASREGGR {
		[XmlElement(ElementName="BASREGD")]
		public string BASREGD { get; set; }
		[XmlElement(ElementName="BASREGN")]
		public string BASREGN { get; set; }
	}

	[XmlRoot(ElementName="LIMTO")]
	public class LIMTO {
		[XmlElement(ElementName="GSTERMEN")]
		public string GSTERMEN { get; set; }
		[XmlAttribute(AttributeName="NICCLAI")]
		public string NICCLAI { get; set; }
	}

	[XmlRoot(ElementName="LIMGR")]
	public class LIMGR {
		[XmlElement(ElementName="DCPCD")]
		public string DCPCD { get; set; }
		[XmlElement(ElementName="LIMTO")]
		public List<LIMTO> LIMTO { get; set; }
		[XmlElement(ElementName="GSHEADEN")]
		public string GSHEADEN { get; set; }
		[XmlElement(ElementName="GSFOOTEN")]
		public string GSFOOTEN { get; set; }
	}

	[XmlRoot(ElementName="MARTRGR")]
	public class MARTRGR {
		[XmlElement(ElementName="MARTREN")]
		public string MARTREN { get; set; }
	}

	[XmlRoot(ElementName="CORRGR")]
	public class CORRGR {
		[XmlElement(ElementName="NAME")]
		public NAME NAME { get; set; }
		[XmlElement(ElementName="ADDRESS")]
		public ADDRESS ADDRESS { get; set; }
		[XmlAttribute(AttributeName="CLID")]
		public string CLID { get; set; }
	}

	[XmlRoot(ElementName="VOLDESGR")]
	public class VOLDESGR {
		[XmlElement(ElementName="VOLDESEN")]
		public string VOLDESEN { get; set; }
	}

	[XmlRoot(ElementName="PRIGS")]
	public class PRIGS {
		[XmlAttribute(AttributeName="NICCLAI")]
		public string NICCLAI { get; set; }
		[XmlElement(ElementName="GSTERMEN")]
		public string GSTERMEN { get; set; }
	}

	[XmlRoot(ElementName="MARDESGR")]
	public class MARDESGR {
		[XmlElement(ElementName="MARDESEN")]
		public string MARDESEN { get; set; }
	}

	[XmlRoot(ElementName="DISCLAIMGR")]
	public class DISCLAIMGR {
		[XmlElement(ElementName="DISCLAIMEREN")]
		public string DISCLAIMEREN { get; set; }
	}

	[XmlRoot(ElementName="CREATED")]
	public class CREATED {
		[XmlElement(ElementName="HOLGR")]
		public HOLGR HOLGR { get; set; }
		[XmlElement(ElementName="REPGR")]
		public REPGR REPGR { get; set; }
		[XmlElement(ElementName="IMAGE")]
		public IMAGE IMAGE { get; set; }
		[XmlElement(ElementName="MARTRAN")]
		public string MARTRAN { get; set; }
		[XmlElement(ElementName="VIENNAGR")]
		public VIENNAGR VIENNAGR { get; set; }
		[XmlElement(ElementName="VOLDESGR")]
		public VOLDESGR VOLDESGR { get; set; }
		[XmlElement(ElementName="MARTRGR")]
		public MARTRGR MARTRGR { get; set; }
		[XmlElement(ElementName="BASICGS")]
		public BASICGS BASICGS { get; set; }
		[XmlElement(ElementName="BASGR")]
		public BASGR BASGR { get; set; }
		[XmlElement(ElementName="DESPG")]
		public DESPG DESPG { get; set; }
		[XmlAttribute(AttributeName="TRANTYP")]
		public string TRANTYP { get; set; }
		[XmlAttribute(AttributeName="INTREGN")]
		public string INTREGN { get; set; }
		[XmlAttribute(AttributeName="OOCD")]
		public string OOCD { get; set; }
		[XmlAttribute(AttributeName="ORIGLAN")]
		public string ORIGLAN { get; set; }
		[XmlAttribute(AttributeName="EXPDATE")]
		public string EXPDATE { get; set; }
		[XmlAttribute(AttributeName="OFFREF")]
		public string OFFREF { get; set; }
		[XmlAttribute(AttributeName="DOCID")]
		public string DOCID { get; set; }
		[XmlAttribute(AttributeName="NOTDATE")]
		public string NOTDATE { get; set; }
		[XmlAttribute(AttributeName="REGEDAT")]
		public string REGEDAT { get; set; }
		[XmlElement(ElementName="MARCOLI")]
		public string MARCOLI { get; set; }
		[XmlElement(ElementName="COLCLAGR")]
		public COLCLAGR COLCLAGR { get; set; }
		[XmlElement(ElementName="PRIGR")]
		public PRIGR PRIGR { get; set; }
	}

	[XmlRoot(ElementName="DEATH")]
	public class DEATH {
		[XmlAttribute(AttributeName="TRANTYP")]
		public string TRANTYP { get; set; }
		[XmlAttribute(AttributeName="DOCID")]
		public string DOCID { get; set; }
		[XmlAttribute(AttributeName="INTREGN")]
		public string INTREGN { get; set; }
		[XmlAttribute(AttributeName="REGRDAT")]
		public string REGRDAT { get; set; }
		[XmlAttribute(AttributeName="EXPDATE")]
		public string EXPDATE { get; set; }
	}

	[XmlRoot(ElementName="RESTRICT")]
	public class RESTRICT {
		[XmlElement(ElementName="GSHEADEN")]
		public string GSHEADEN { get; set; }
		[XmlElement(ElementName="GSFOOTEN")]
		public string GSFOOTEN { get; set; }
		[XmlAttribute(AttributeName="TRANTYP")]
		public string TRANTYP { get; set; }
		[XmlAttribute(AttributeName="ORIGLAN")]
		public string ORIGLAN { get; set; }
		[XmlAttribute(AttributeName="DOCID")]
		public string DOCID { get; set; }
		[XmlAttribute(AttributeName="INTREGN")]
		public string INTREGN { get; set; }
		[XmlAttribute(AttributeName="NOTDATE")]
		public string NOTDATE { get; set; }
		[XmlAttribute(AttributeName="REGRDAT")]
		public string REGRDAT { get; set; }
		[XmlAttribute(AttributeName="REGEDAT")]
		public string REGEDAT { get; set; }
		[XmlElement(ElementName="LIMTO")]
		public List<LIMTO> LIMTO { get; set; }
	}

	[XmlRoot(ElementName="CBOP")]
	public class CBOP {
		[XmlElement(ElementName="GSHEADEN")]
		public string GSHEADEN { get; set; }
		[XmlElement(ElementName="GSFOOTEN")]
		public string GSFOOTEN { get; set; }
		[XmlAttribute(AttributeName="GAZNO")]
		public string GAZNO { get; set; }
		[XmlAttribute(AttributeName="ORIGLAN")]
		public string ORIGLAN { get; set; }
	}

	[XmlRoot(ElementName="LIO")]
	public class LIO {
		[XmlElement(ElementName="CPCD")]
		public string CPCD { get; set; }
		[XmlElement(ElementName="GSHEADEN")]
		public string GSHEADEN { get; set; }
		[XmlAttribute(AttributeName="GAZNO")]
		public string GAZNO { get; set; }
		[XmlAttribute(AttributeName="ORIGLAN")]
		public string ORIGLAN { get; set; }
	}

	[XmlRoot(ElementName="PROCESSED")]
	public class PROCESSED {
		[XmlElement(ElementName="INTREGN")]
		public string INTREGN { get; set; }
		[XmlAttribute(AttributeName="TRANTYP")]
		public string TRANTYP { get; set; }
		[XmlAttribute(AttributeName="OFFREF")]
		public string OFFREF { get; set; }
		[XmlAttribute(AttributeName="DOCID")]
		public string DOCID { get; set; }
		[XmlAttribute(AttributeName="REGRDAT")]
		public string REGRDAT { get; set; }
		[XmlAttribute(AttributeName="REGEDAT")]
		public string REGEDAT { get; set; }
	}

	[XmlRoot(ElementName="NEWNAME")]
	public class NEWNAME {
		[XmlElement(ElementName="HOLGR")]
		public HOLGR HOLGR { get; set; }
		[XmlElement(ElementName="REPGR")]
		public REPGR REPGR { get; set; }
		[XmlAttribute(AttributeName="TRANTYP")]
		public string TRANTYP { get; set; }
		[XmlAttribute(AttributeName="INTREGN")]
		public string INTREGN { get; set; }
		[XmlAttribute(AttributeName="DOCID")]
		public string DOCID { get; set; }
		[XmlAttribute(AttributeName="NOTDATE")]
		public string NOTDATE { get; set; }
		[XmlAttribute(AttributeName="REGEDAT")]
		public string REGEDAT { get; set; }
		[XmlElement(ElementName="CORRGR")]
		public CORRGR CORRGR { get; set; }
		[XmlElement(ElementName="PHOLGR")]
		public PHOLGR PHOLGR { get; set; }
	}

	[XmlRoot(ElementName="PHOLGR")]
	public class PHOLGR {
		[XmlElement(ElementName="NAME")]
		public NAME NAME { get; set; }
		[XmlElement(ElementName="ADDRESS")]
		public ADDRESS ADDRESS { get; set; }
		[XmlAttribute(AttributeName="CLID")]
		public string CLID { get; set; }
	}

	[XmlRoot(ElementName="PROLONG")]
	public class PROLONG {
		[XmlAttribute(AttributeName="TRANTYP")]
		public string TRANTYP { get; set; }
		[XmlAttribute(AttributeName="INTREGN")]
		public string INTREGN { get; set; }
		[XmlAttribute(AttributeName="EXPDATE")]
		public string EXPDATE { get; set; }
		[XmlAttribute(AttributeName="DOCID")]
		public string DOCID { get; set; }
		[XmlAttribute(AttributeName="REGRDAT")]
		public string REGRDAT { get; set; }
		[XmlAttribute(AttributeName="DESUNDER")]
		public string DESUNDER { get; set; }
		[XmlAttribute(AttributeName="RENDATE")]
		public string RENDATE { get; set; }
	}

	[XmlRoot(ElementName="RFOT")]
	public class RFOT {
		[XmlAttribute(AttributeName="CPCD")]
		public string CPCD { get; set; }
	}

	[XmlRoot(ElementName="ENOTIF")]
	public class ENOTIF {
		[XmlElement(ElementName="CORRECTION")]
		public List<CORRECTION> CORRECTION { get; set; }
		[XmlElement(ElementName="BIRTH")]
		public List<BIRTH> BIRTH { get; set; }
		[XmlElement(ElementName="CREATED")]
		public List<CREATED> CREATED { get; set; }
		[XmlElement(ElementName="DEATH")]
		public List<DEATH> DEATH { get; set; }
		[XmlElement(ElementName="RESTRICT")]
		public List<RESTRICT> RESTRICT { get; set; }
		[XmlElement(ElementName="PROCESSED")]
		public List<PROCESSED> PROCESSED { get; set; }
		[XmlElement(ElementName="NEWNAME")]
		public List<NEWNAME> NEWNAME { get; set; }
		[XmlElement(ElementName="PROLONG")]
		public List<PROLONG> PROLONG { get; set; }
		[XmlAttribute(AttributeName="BIRTHCOUNT")]
		public string BIRTHCOUNT { get; set; }
		[XmlAttribute(AttributeName="DEATHCOUNT")]
		public string DEATHCOUNT { get; set; }
		[XmlAttribute(AttributeName="NEWNAMECOUNT")]
		public string NEWNAMECOUNT { get; set; }
		[XmlAttribute(AttributeName="RESTRICTCOUNT")]
		public string RESTRICTCOUNT { get; set; }
		[XmlAttribute(AttributeName="NEWBASECOUNT")]
		public string NEWBASECOUNT { get; set; }
		[XmlAttribute(AttributeName="PROLONGCOUNT")]
		public string PROLONGCOUNT { get; set; }
		[XmlAttribute(AttributeName="CORRECTIONCOUNT")]
		public string CORRECTIONCOUNT { get; set; }
		[XmlAttribute(AttributeName="PROCESSEDCOUNT")]
		public string PROCESSEDCOUNT { get; set; }
		[XmlAttribute(AttributeName="CREATEDCOUNT")]
		public string CREATEDCOUNT { get; set; }
		[XmlAttribute(AttributeName="LICENCE-BIRTHCOUNT")]
		public string LICENCEBIRTHCOUNT { get; set; }
		[XmlAttribute(AttributeName="LICENCE-NEWNAMECOUNT")]
		public string LICENCENEWNAMECOUNT { get; set; }
		[XmlAttribute(AttributeName="PAIDCOUNT")]
		public string PAIDCOUNT { get; set; }
		[XmlAttribute(AttributeName="CPCD")]
		public string CPCD { get; set; }
		[XmlAttribute(AttributeName="NOTLANG")]
		public string NOTLANG { get; set; }
		[XmlAttribute(AttributeName="GAZNO")]
		public string GAZNO { get; set; }
		[XmlAttribute(AttributeName="WEEKNO")]
		public string WEEKNO { get; set; }
		[XmlAttribute(AttributeName="PUBDATE")]
		public string PUBDATE { get; set; }
		[XmlAttribute(AttributeName="NOTDATE")]
		public string NOTDATE { get; set; }
	}
}
