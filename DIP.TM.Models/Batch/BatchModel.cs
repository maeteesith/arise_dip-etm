﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DIP.TM.Models.Batch
{
    public class BatchModel
    {

    }

    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Xml
    {
        public string version { get; set; }
        public string encoding { get; set; }

    }

    public class Name
    {
        public object namel { get; set; }

    }

    public class Address
    {
        public string[] addrl { get; set; }
        public string country { get; set; }

    }

    public class Entaddr
    {
        public string addrl { get; set; }
        public string country { get; set; }

    }

    public class Legnatu
    {
        public string legnatt { get; set; }
        public string plainco { get; set; }

    }

    public class Holgr
    {
        public string clid { get; set; }
        public string notlang { get; set; }
        public Name name { get; set; }
        public Address address { get; set; }
        public string entest { get; set; }
        public Entaddr entaddr { get; set; }
        public Legnatu legnatu { get; set; }
        public object corrind { get; set; }
        public string entnatl { get; set; }

    }

    public class Name2
    {
        public string namel { get; set; }

    }

    public class Address2
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Repgr
    {
        public string clid { get; set; }
        public Name2 name { get; set; }
        public Address2 address { get; set; }

    }

    public class Image
    {
        public string name { get; set; }
        public string text { get; set; }
        public string colour { get; set; }
        public string type { get; set; }

    }

    public class Basicgs
    {
        public string nicever { get; set; }
        public object gsgr { get; set; }

    }

    public class Basappgr
    {
        public string basappd { get; set; }
        public string basappn { get; set; }

    }

    public class Basreggr
    {
        public string basregd { get; set; }
        public string basregn { get; set; }

    }

    public class Basgr
    {
        public Basappgr basappgr { get; set; }
        public Basreggr basreggr { get; set; }

    }

    public class Prigr
    {
        public string pricp { get; set; }
        public string priappd { get; set; }
        public string priappn { get; set; }

    }

    public class Despg
    {
        public List<string> dcpcd { get; set; }

    }

    public class Viennagr
    {
        public string vienver { get; set; }
        public List<string> vieclai { get; set; }
        public List<string> viecla3 { get; set; }

    }

    public class Despg2
    {
        public List<string> dcpcd { get; set; }

    }

    public class Colclagr
    {
        public string colclaen { get; set; }

    }

    public class Birth
    {
        public string trantyp { get; set; }
        public string intregn { get; set; }
        public string oocd { get; set; }
        public string origlan { get; set; }
        public string expdate { get; set; }
        public string docid { get; set; }
        public string regrdat { get; set; }
        public string notdate { get; set; }
        public string regedat { get; set; }
        public string offref { get; set; }
        public string intregd { get; set; }
        public string desunder { get; set; }
        public Holgr holgr { get; set; }
        public Repgr repgr { get; set; }
        public Image image { get; set; }
        public object stdmind { get; set; }
        public string mardur { get; set; }
        public object vrblnot { get; set; }
        public Basicgs basicgs { get; set; }
        public Basgr basgr { get; set; }
        public Prigr prigr { get; set; }
        public Despg despg { get; set; }
        public Viennagr viennagr { get; set; }
        public Despg2 despg2 { get; set; }
        public object marcoli { get; set; }
        public Colclagr colclagr { get; set; }
        public string rendate { get; set; }

    }

    public class Incorrect
    {
        public Birth birth { get; set; }

    }

    public class Name3
    {
        public object namel { get; set; }

    }

    public class Address3
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Entaddr2
    {
        public string addrl { get; set; }
        public string country { get; set; }

    }

    public class Legnatu2
    {
        public string legnatt { get; set; }
        public string plainco { get; set; }

    }

    public class Holgr2
    {
        public string clid { get; set; }
        public string notlang { get; set; }
        public Name3 name { get; set; }
        public Address3 address { get; set; }
        public string entest { get; set; }
        public Entaddr2 entaddr { get; set; }
        public Legnatu2 legnatu { get; set; }
        public object corrind { get; set; }
        public string entnatl { get; set; }

    }

    public class Name4
    {
        public string namel { get; set; }

    }

    public class Address4
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Repgr2
    {
        public string clid { get; set; }
        public Name4 name { get; set; }
        public Address4 address { get; set; }

    }

    public class Image2
    {
        public string name { get; set; }
        public string text { get; set; }
        public string colour { get; set; }
        public string type { get; set; }

    }

    public class Basicgs2
    {
        public string nicever { get; set; }
        public object gsgr { get; set; }

    }

    public class Basappgr2
    {
        public string basappd { get; set; }
        public string basappn { get; set; }

    }

    public class Basreggr2
    {
        public string basregd { get; set; }
        public string basregn { get; set; }

    }

    public class Basgr2
    {
        public Basappgr2 basappgr { get; set; }
        public Basreggr2 basreggr { get; set; }

    }

    public class Prigr2
    {
        public string pricp { get; set; }
        public string priappd { get; set; }
        public string priappn { get; set; }

    }

    public class Despg3
    {
        public List<string> dcpcd { get; set; }

    }

    public class Viennagr2
    {
        public string vienver { get; set; }
        public List<string> vieclai { get; set; }
        public List<string> viecla3 { get; set; }

    }

    public class Despg22
    {
        public List<string> dcpcd { get; set; }

    }

    public class Colclagr2
    {
        public string colclaen { get; set; }

    }

    public class Birth2
    {
        public string trantyp { get; set; }
        public string intregn { get; set; }
        public string oocd { get; set; }
        public string origlan { get; set; }
        public string expdate { get; set; }
        public string docid { get; set; }
        public string regrdat { get; set; }
        public string notdate { get; set; }
        public string regedat { get; set; }
        public string offref { get; set; }
        public string intregd { get; set; }
        public string desunder { get; set; }
        public Holgr2 holgr { get; set; }
        public Repgr2 repgr { get; set; }
        public Image2 image { get; set; }
        public object stdmind { get; set; }
        public string mardur { get; set; }
        public object vrblnot { get; set; }
        public Basicgs2 basicgs { get; set; }
        public Basgr2 basgr { get; set; }
        public Prigr2 prigr { get; set; }
        public Despg3 despg { get; set; }
        public Viennagr2 viennagr { get; set; }
        public Despg22 despg2 { get; set; }
        public object marcoli { get; set; }
        public Colclagr2 colclagr { get; set; }
        public string rendate { get; set; }

    }

    public class Correct
    {
        public string changedcorr { get; set; }
        public string changedrep { get; set; }
        public string changedorgs { get; set; }
        public string changedor { get; set; }
        public string changedbasic { get; set; }
        public string changeddesgtn { get; set; }
        public string changedimage { get; set; }
        public string changedgs { get; set; }
        public string changedholder { get; set; }
        public string changedir { get; set; }
        public string changedprty { get; set; }
        public string changedirtext { get; set; }
        public string changedtype { get; set; }
        public Birth2 birth { get; set; }

    }

    public class Correction
    {
        public string docid { get; set; }
        public string regedat { get; set; }
        public string texten { get; set; }
        public string gaznum { get; set; }
        public Incorrect incorrect { get; set; }
        public Correct correct { get; set; }
        public string offref { get; set; }

    }

    public class Name5
    {
        public object namel { get; set; }

    }

    public class Address5
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Legnatu3
    {
        public string legnatt { get; set; }
        public string plainco { get; set; }

    }

    public class Entaddr3
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Holgr3
    {
        public string clid { get; set; }
        public string notlang { get; set; }
        public Name5 name { get; set; }
        public Address5 address { get; set; }
        public string entest { get; set; }
        public Legnatu3 legnatu { get; set; }
        public object corrind { get; set; }
        public string entnatl { get; set; }
        public Entaddr3 entaddr { get; set; }
        public string entdom { get; set; }
        public string natlty { get; set; }

    }

    public class Name6
    {
        public object namel { get; set; }

    }

    public class Address6
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Repgr3
    {
        public string clid { get; set; }
        public Name6 name { get; set; }
        public Address6 address { get; set; }

    }

    public class Image3
    {
        public string name { get; set; }
        public string text { get; set; }
        public string colour { get; set; }
        public string type { get; set; }

    }

    public class Basicgs3
    {
        public string nicever { get; set; }
        public object gsgr { get; set; }

    }

    public class Despg4
    {
        public List<string> dcpcd { get; set; }

    }

    public class Despg23
    {
        public object dcpcd { get; set; }

    }

    public class Viennagr3
    {
        public string vienver { get; set; }
        public object vieclai { get; set; }
        public object viecla3 { get; set; }

    }

    public class Limto
    {
        public string nicclai { get; set; }
        public string gstermen { get; set; }

    }

    public class Limgr
    {
        public string dcpcd { get; set; }
        public Limto limto { get; set; }
        public string gsheaden { get; set; }

    }

    public class Martrgr
    {
        public string martren { get; set; }

    }

    public class Name7
    {
        public object namel { get; set; }

    }

    public class Address7
    {
        public object addrl { get; set; }
        public string country { get; set; }

    }

    public class Corrgr
    {
        public string clid { get; set; }
        public Name7 name { get; set; }
        public Address7 address { get; set; }

    }

    public class Voldesgr
    {
        public string voldesen { get; set; }

    }

    public class Mardesgr
    {
        public string mardesen { get; set; }

    }

    public class Colclagr3
    {
        public string colclaen { get; set; }
        public string colparen { get; set; }

    }

    public class Birth3
    {
        public string trantyp { get; set; }
        public string intregn { get; set; }
        public string oocd { get; set; }
        public string origlan { get; set; }
        public string expdate { get; set; }
        public string docid { get; set; }
        public string regrdat { get; set; }
        public string notdate { get; set; }
        public string regedat { get; set; }
        public string offref { get; set; }
        public string intregd { get; set; }
        public string desunder { get; set; }
        public Holgr3 holgr { get; set; }
        public Repgr3 repgr { get; set; }
        public Image3 image { get; set; }
        public string sigvrbl { get; set; }
        public string mardur { get; set; }
        public Basicgs3 basicgs { get; set; }
        public object basgr { get; set; }
        public object prigr { get; set; }
        public Despg4 despg { get; set; }
        public Despg23 despg2 { get; set; }
        public object stdmind { get; set; }
        public object vrblnot { get; set; }
        public Viennagr3 viennagr { get; set; }
        public object marcoli { get; set; }
        public Limgr limgr { get; set; }
        public string martran { get; set; }
        public Martrgr martrgr { get; set; }
        public Corrgr corrgr { get; set; }
        public Voldesgr voldesgr { get; set; }
        public Mardesgr mardesgr { get; set; }
        public Colclagr3 colclagr { get; set; }

    }

    public class Name8
    {
        public string namel { get; set; }

    }

    public class Address8
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Legnatu4
    {
        public string legnatt { get; set; }
        public string plainco { get; set; }

    }

    public class Holgr4
    {
        public string clid { get; set; }
        public string notlang { get; set; }
        public Name8 name { get; set; }
        public Address8 address { get; set; }
        public string entnatl { get; set; }
        public Legnatu4 legnatu { get; set; }
        public object corrind { get; set; }

    }

    public class Name9
    {
        public List<string> namel { get; set; }

    }

    public class Address9
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Repgr4
    {
        public string clid { get; set; }
        public Name9 name { get; set; }
        public Address9 address { get; set; }

    }

    public class Image4
    {
        public string name { get; set; }
        public string text { get; set; }
        public string colour { get; set; }
        public string type { get; set; }

    }

    public class Viennagr4
    {
        public string vienver { get; set; }
        public List<string> vieclai { get; set; }
        public List<string> viecla3 { get; set; }

    }

    public class Voldesgr2
    {
        public string voldesen { get; set; }

    }

    public class Martrgr2
    {
        public string martren { get; set; }

    }

    public class Basicgs4
    {
        public string nicever { get; set; }
        public object gsgr { get; set; }

    }

    public class Basappgr3
    {
        public string basappd { get; set; }
        public string basappn { get; set; }

    }

    public class Basgr3
    {
        public Basappgr3 basappgr { get; set; }

    }

    public class Despg5
    {
        public List<string> dcpcd { get; set; }

    }

    public class Colclagr4
    {
        public string colclaen { get; set; }
        public string colparen { get; set; }

    }

    public class Prigr3
    {
        public string pricp { get; set; }
        public string priappd { get; set; }
        public string priappn { get; set; }

    }

    public class Created
    {
        public string trantyp { get; set; }
        public string intregn { get; set; }
        public string oocd { get; set; }
        public string origlan { get; set; }
        public string expdate { get; set; }
        public string offref { get; set; }
        public string docid { get; set; }
        public string notdate { get; set; }
        public string regedat { get; set; }
        public Holgr4 holgr { get; set; }
        public Repgr4 repgr { get; set; }
        public Image4 image { get; set; }
        public string martran { get; set; }
        public Viennagr4 viennagr { get; set; }
        public Voldesgr2 voldesgr { get; set; }
        public Martrgr2 martrgr { get; set; }
        public Basicgs4 basicgs { get; set; }
        public Basgr3 basgr { get; set; }
        public Despg5 despg { get; set; }
        public object marcoli { get; set; }
        public Colclagr4 colclagr { get; set; }
        public Prigr3 prigr { get; set; }

    }

    public class Death
    {
        public string trantyp { get; set; }
        public string docid { get; set; }
        public string intregn { get; set; }
        public string regrdat { get; set; }
        public string expdate { get; set; }

    }

    public class Restrict
    {
        public string trantyp { get; set; }
        public string origlan { get; set; }
        public string docid { get; set; }
        public string intregn { get; set; }
        public string notdate { get; set; }
        public string regrdat { get; set; }
        public string regedat { get; set; }
        public string gsheaden { get; set; }
        public string gsfooten { get; set; }
        public object limto { get; set; }

    }

    public class Processed
    {
        public string trantyp { get; set; }
        public string offref { get; set; }
        public string docid { get; set; }
        public string regrdat { get; set; }
        public string regedat { get; set; }
        public string intregn { get; set; }

    }

    public class Name10
    {
        public object namel { get; set; }

    }

    public class Address10
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Legnatu5
    {
        public string legnatt { get; set; }
        public string plainco { get; set; }

    }

    public class Entaddr4
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Holgr5
    {
        public string clid { get; set; }
        public string notlang { get; set; }
        public Name10 name { get; set; }
        public Address10 address { get; set; }
        public string entnatl { get; set; }
        public Legnatu5 legnatu { get; set; }
        public object corrind { get; set; }
        public string entest { get; set; }
        public Entaddr4 entaddr { get; set; }
        public string entdom { get; set; }

    }

    public class Name11
    {
        public object namel { get; set; }

    }

    public class Address11
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Repgr5
    {
        public string clid { get; set; }
        public Name11 name { get; set; }
        public Address11 address { get; set; }

    }

    public class Name12
    {
        public List<string> namel { get; set; }

    }

    public class Address12
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Corrgr2
    {
        public string clid { get; set; }
        public Name12 name { get; set; }
        public Address12 address { get; set; }

    }

    public class Name13
    {
        public object namel { get; set; }

    }

    public class Address13
    {
        public List<string> addrl { get; set; }
        public string country { get; set; }

    }

    public class Pholgr
    {
        public string clid { get; set; }
        public Name13 name { get; set; }
        public Address13 address { get; set; }

    }

    public class Newname
    {
        public string trantyp { get; set; }
        public string intregn { get; set; }
        public string docid { get; set; }
        public string notdate { get; set; }
        public string regedat { get; set; }
        public Holgr5 holgr { get; set; }
        public Repgr5 repgr { get; set; }
        public Corrgr2 corrgr { get; set; }
        public Pholgr pholgr { get; set; }

    }

    public class Prolong
    {
        public string trantyp { get; set; }
        public string intregn { get; set; }
        public string expdate { get; set; }
        public string docid { get; set; }
        public string regrdat { get; set; }
        public string desunder { get; set; }
        public string rendate { get; set; }

    }

    public class Enotif
    {
        public string birthcount { get; set; }
        public string deathcount { get; set; }
        public string newnamecount { get; set; }
        public string restrictcount { get; set; }
        public string newbasecount { get; set; }
        public string prolongcount { get; set; }
        public string correctioncount { get; set; }
        public string processedcount { get; set; }
        public string createdcount { get; set; }
        public string licencebirthcount { get; set; }
        public string licencenewnamecount { get; set; }
        public string paidcount { get; set; }
        public string cpcd { get; set; }
        public string notlang { get; set; }
        public string gazno { get; set; }
        public string weekno { get; set; }
        public string pubdate { get; set; }
        public string notdate { get; set; }
        public List<Correction> correction { get; set; }
        public List<Birth3> birth { get; set; }
        public List<Created> created { get; set; }
        public List<Death> death { get; set; }
        public List<Restrict> restrict { get; set; }
        public List<Processed> processed { get; set; }
        public List<Newname> newname { get; set; }
        public List<Prolong> prolong { get; set; }

    }

    public class Root
    {
        public Xml xml { get; set; }
        public Enotif enotif { get; set; }

    }
}
