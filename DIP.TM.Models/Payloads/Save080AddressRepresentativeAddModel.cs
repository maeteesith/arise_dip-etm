﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save080AddressRepresentativeAddModel : SaveAddressAddModel  {
        public string save080_representative_type_code { get; set; }
    }
}
