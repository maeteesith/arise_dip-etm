﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010RequestGroupAddModel : BaseModel {
        public long? save_id { get; set; }
        public string request_number { get; set; }
        public string save010_registration_group_status_code { get; set; }
        public DateTime? make_date { get; set; }
        public DateTime? allow_date { get; set; }
    }
}
