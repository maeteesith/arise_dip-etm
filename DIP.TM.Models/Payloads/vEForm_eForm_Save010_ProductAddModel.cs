﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vEForm_eForm_Save010_ProductAddModel : BaseModel {
        public long? request01_item_id { get; set; }
        public long eform_id { get; set; }
        public string request_item_sub_type_1_code { get; set; }
        public int? product_count { get; set; }
        public int? total_price { get; set; }
    }
}