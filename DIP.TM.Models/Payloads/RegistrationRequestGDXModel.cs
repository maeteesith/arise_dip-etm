using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class RegistrationRequestGDXModel : BaseModel {

        public string auto_fill_token { get; set; }
        public string citizen_idcard { get; set; }
        public string officer_idcard { get; set; }
        public string people_type { get; set; }
      
    }
}
