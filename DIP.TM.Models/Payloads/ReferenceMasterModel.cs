﻿using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class ReferenceMasterModel : BaseModel {
        public string topic { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string value_1 { get; set; }
        public string value_2 { get; set; }
        public string value_3 { get; set; }

        public bool is_exactly { set; get; }
    }
}
