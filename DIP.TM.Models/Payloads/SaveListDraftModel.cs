﻿using DIP.TM.Utils;
using System;

namespace DIP.TM.Models.Payloads {
    public class SaveListDraftModel {
        public string request_number { get; set; }
    }
    
}