﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010CheckingSimilarWordTranslateAddModel : SaveModel {
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string word_translate_search { get; set; }
        public string word_translate_dictionary_code { get; set; }
        public string word_translate_dictionary_other { get; set; }
        public string word_translate_sound { get; set; }
        public string word_translate_translate { get; set; }
        public string checking_word_translate_status_code { get; set; }
    }
}
