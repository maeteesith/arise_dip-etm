﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads.Appeal
{
    public class Appeal_Role02SaveCaseSummarySubcommitteeProposalAddModel : BaseModel
    {
        public long? save_case_summary_id { get; set; }
        public string set_time { get; set; }
        public string other { get; set; }
        public string commentary { get; set; }
        public string commentary_other { get; set; }
        public string reason { get; set; }
    }
}
