﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads.Appeal
{
    public class Appeal_Role02SaveCaseSummarySubcommitteeResulotionAddModel : BaseModel
    {
        public long? save_case_summary_id { get; set; }
        public string is_vote { get; set; }
        public long? score_vote { get; set; }
        public string reason_vote { get; set; }
    }
}
