﻿using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class ReceiptItemAddModel : BaseModel {
        public long receipt_id { get; set; }
        public long? request_id { get; set; }
        public long? post_round_id { get; set; }
        public string request_type_code { get; set; }
        public string name { get; set; }
        public string request_number { get; set; }
        public string receipt_number { get; set; }
        public string book_index { get; set; }
        public string page_index { get; set; }
        public string source_code { get; set; }
        public decimal? total_price { get; set; }
    }
}
