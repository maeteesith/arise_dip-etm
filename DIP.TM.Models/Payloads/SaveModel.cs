﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class SaveModel : BaseModel {
        public long request_id { get; set; }
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public DateTime? make_date { get; set; }
        public string request_index { get; set; }
        public string irn_number { get; set; }
        public decimal? total_price { get; set; }
        public bool? is_save_past { get; set; }
        public string request_source_code { get; set; }
        public string cancel_reason { get; set; }
        public string save_status_code { get; set; }
        public string department_send_code { get; set; }
        public DateTime? department_send_date { get; set; }
        public string consider_similar_document_status_code { get; set; }
        public DateTime? consider_similar_document_date { get; set; }
        public string consider_similar_document_remark { get; set; }
        public string consider_similar_document_item_status_list { get; set; }

        public string eform_number { get; set; }
        public string rule_number { get; set; }
    }

}