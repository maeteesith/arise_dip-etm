﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save030AddModel : SaveModel {
        public string register_number { get; set; }
        public string index_1 { get; set; }
        public string index_2 { get; set; }
        public string save030_appeal_type_code { get; set; }
        public string save030_appeal_maker_type_code { get; set; }
        public string save030_appeal_reason_code { get; set; }
        public string section_index { get; set; }
        public string remark { get; set; }
        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public SaveAddressAddModel contact_address { get; set; }
    }
}
