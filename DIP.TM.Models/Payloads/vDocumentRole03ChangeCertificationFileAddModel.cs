﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentRole03ChangeCertificationFileAddModel : BaseModel {
        public string document_classification_by_name { get; set; }
        public long document_role03_item_id { get; set; }
        public long? file_id { get; set; }
        public string file_name { get; set; }
        public decimal? file_size { get; set; }
        public string remark { get; set; }
    }
}