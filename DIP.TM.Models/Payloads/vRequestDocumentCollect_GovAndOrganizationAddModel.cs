﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads
{
    public class vRequestDocumentCollect_GovAndOrganizationAddModel : BaseModel
    {
        public long? rd_id { get; set; }
        public string requester_name { get; set; }
        public long save_id { get; set; }
        public string request_number { get; set; }
        public string reference_number { get; set; }
        public DateTime? request_date { get; set; }
        public decimal? total_price { get; set; }
        public string request_document_collect_type_code { get; set; }
        public string request_document_collect_type_name { get; set; }
        public string request_document_collect_type_file_extension { get; set; }
        public long? file_id { get; set; }
        public string physical_path { get; set; }
        public string request_document_collect_status_code { get; set; }
        public string request_document_collect_status_name { get; set; }
        public string cancel_reason { get; set; }
        public string updated_by_name { get; set; }
    }
}