﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save020AddModel : SaveModel {
        public DateTime? public_date { get; set; }
        public string book_number { get; set; }
        public string page_index { get; set; }
        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public SaveAddressAddModel contact_address { get; set; }
    }
}
