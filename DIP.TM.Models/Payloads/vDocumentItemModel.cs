﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentItemModel : vSave010Model {
        public string document_classification_by_name { get; set; }
        public string request_type_value_3 { get; set; }
        public string document_type_code { get; set; }
        public string document_type_name { get; set; }
    }
}