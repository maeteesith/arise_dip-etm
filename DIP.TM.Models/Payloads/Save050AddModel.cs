﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save050AddModel : SaveModel {
        public string register_number { get; set; }
        public string contract_number { get; set; }
        public DateTime? contract_date { get; set; }
        public bool? is_allow_contract { get; set; }
        public bool? is_extend_contract { get; set; }
        public string request_item_type_code { get; set; }
        public string save050_receiver_people_type_code { get; set; }
        public string save050_people_type_code { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<Save050AddressRepresentativeAddModel> representative_list { get; set; }
        public SaveAddressAddModel contact_address { get; set; }
        public List<SaveAddressAddModel> receiver_people_list { get; set; }
        public List<SaveAddressAddModel> receiver_representative_list { get; set; }
        public List<SaveProductAddModel> product_list { get; set; }
    }
}
