﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class ProductAddModel {
        public string item_sub_type_1 { get; set; }
        public string item_sub_type_2 { get; set; }
        public List<string> product_list { get; set; }
        public string product_list_text { get; set; }

    }
}
