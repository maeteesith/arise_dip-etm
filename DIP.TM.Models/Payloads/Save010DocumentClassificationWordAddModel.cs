﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010DocumentClassificationWordAddModel : BaseModel {
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string word_mark { get; set; }
        public string word_first_code { get; set; }
        public string word_sound_last_code { get; set; }
        public string word_sound_last_other_code { get; set; }
        public string word_sound { get; set; }
        public string word_syllable_sound { get; set; }
    }
}
