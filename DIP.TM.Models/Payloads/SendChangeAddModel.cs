﻿using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class SendChangeAddModel : BaseModel {
        public long? save_id { get; set; }
        public long? object_id { get; set; }

        public string department_code { get; set; }
        public string reason { get; set; }
    }
}
