﻿using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class ReceiptAddModel : BaseModel {
        public string reference_number { get; set; }
        public DateTime? receipt_date { get; set; }
        public DateTime? receive_date { get; set; }
        public DateTime? request_date { get; set; }
        public bool? is_paid_past { get; set; }
        public bool? is_manual { get; set; }
        public string receiver_name { get; set; }
        public string requester_name { get; set; }
        public string payer_name { get; set; }
        public string status_code { get; set; }


        public List<ReceiptItemAddModel> item_list { get; set; }
        public List<ReceiptPaidAddModel> paid_list { get; set; }

        //Add
        public string book_index { get; set; }
        public string page_index { get; set; }
    }
}
