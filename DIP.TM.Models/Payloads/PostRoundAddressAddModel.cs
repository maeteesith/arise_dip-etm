﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class PostRoundAddressAddModel : BaseModel {
        public long? post_round_id { get; set; }
        public string address_information { get; set; }
        public bool is_check { get; set; }
    }
}
