﻿using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class RequestCheckAddModel : BaseModel {
        public string reference_number { get; set; }
        public string requester_name { get; set; }
        public long? registrar_by { get; set; }
        public string id_card { get; set; }
        public string telephone { get; set; }
        public string registrar_by_name { get; set; }
        public string maker_name { get; set; }
        public decimal? total_price { get; set; }
        public bool? is_wave_fee { get; set; }
        public bool? is_appointment { get; set; }
        public bool? is_commercial_province { get; set; }
        public string voucher_number { get; set; }
        public int? voucher_remain_second { get; set; }
        public DateTime? voucher_log_in_date { get; set; }
        public bool? is_voucher_log_in { get; set; }
        public string request_item_sub_type1_code { get; set; }
        public string request_item_sub_type1_similar_list { get; set; }
        public string document_classification_image_type_code { get; set; }
        public string request_sound_type_code { get; set; }
        public string request_sound_type_name { get; set; }
        public string status_code { get; set; }

        public List<RequestCheckItemAddModel> request_check_item_list { get; set; }
        public List<RequestCheckRequestNumberAddModel> request_check_request_number_list{ get; set; }
    }
}
