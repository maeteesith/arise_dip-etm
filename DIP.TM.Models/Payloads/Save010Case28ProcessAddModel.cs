﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010Case28ProcessAddModel : BaseModel {
        public long? save_id { get; set; }
        public bool? is_considering_similar_kor10_1_1 { get; set; }
        public string considering_similar_kor10_1_1_description { get; set; }
        public bool? is_considering_similar_kor10_1_2 { get; set; }
        public string considering_similar_kor10_1_2_description { get; set; }
        public bool? is_considering_similar_kor10_1_4 { get; set; }
        public string considering_similar_kor10_1_4_description { get; set; }
        public bool? is_considering_similar_kor10_2_1 { get; set; }
        public bool? is_considering_similar_kor10_2_2 { get; set; }
        public bool? is_considering_similar_kor10_2_3 { get; set; }
        public bool? is_considering_similar_kor10_2_4 { get; set; }
        public bool? is_considering_similar_kor10_2_5 { get; set; }
        public bool? is_considering_similar_kor10_2_6 { get; set; }
        public bool? is_considering_similar_kor10_3 { get; set; }
        public string considering_similar_kor10_3_description { get; set; }

        public List<Save010Case28AddModel> case28_list { set; get; }
    }
}
