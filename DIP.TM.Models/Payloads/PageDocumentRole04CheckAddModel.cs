﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class PageDocumentRole04CheckAddModel : BaseModel {
        //public Save010ProcessView view { set; get; }
        //public List<vDocumentRole02View> instruction_rule_list { get; set; }
        //public List<vSave010CheckingSaveDocumentView> document_list { get; set; }
        //public List<vReceiptItemView> receipt_item_list { get; set; }
        //public List<vSave010HistoryView> history_list { get; set; }
        //public List<vRequestDocumentCollectView> document_scan_list { get; set; }
        public List<vDocumentRole04CheckAddModel> document_role04_check_list { get; set; }
    }
}
