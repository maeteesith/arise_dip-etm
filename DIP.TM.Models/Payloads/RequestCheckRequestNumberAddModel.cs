﻿using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class RequestCheckRequestNumberAddModel : BaseModel {
        public long? request_check_id { get; set; }
        public string request_number { get; set; }
        public bool? is_2_2 { get; set; }
        public int? number_2_3 { get; set; }
        public bool? is_2_5 { get; set; }
        public bool? is_2_6 { get; set; }
        public bool? is_2_7 { get; set; }
    }
}
