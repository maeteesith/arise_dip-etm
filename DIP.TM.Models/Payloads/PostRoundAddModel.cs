﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class PostRoundAddModel : BaseModel {
        public string post_round_instruction_rule_code { get; set; }
        public string post_round_type_code { get; set; }
        public long? object_id { get; set; }
        public int? round_index { get; set; }
        public long? book_by { get; set; }
        public string book_number { get; set; }
        public DateTime? book_start_date { get; set; }
        public DateTime? book_end_date { get; set; }
        public int? book_expired_day { get; set; }
        public string post_round_document_post_status_code { get; set; }
        public DateTime? post_round_document_post_date { get; set; }
        public string document_role02_status_code { get; set; }
        public DateTime? document_role02_date { get; set; }
        public long? document_role02_spliter_by { get; set; }
        public long? document_role02_receiver_by { get; set; }
        public string document_role02_receive_status_code { get; set; }
        public DateTime? document_role02_receive_date { get; set; }
        public string post_number { get; set; }
        public string document_role02_print_document_status_code { get; set; }
        public DateTime? document_role02_print_document_date { get; set; }
        public string document_role02_print_cover_status_code { get; set; }
        public DateTime? document_role02_print_cover_date { get; set; }
        public DateTime? document_role02_post_number_date { get; set; }
        public string document_role02_print_list_status_code { get; set; }
        public DateTime? document_role02_print_list_date { get; set; }
        public string post_round_action_post_status_code { get; set; }
        public string post_round_action_post_type_code { get; set; }
        public DateTime? book_acknowledge_date { get; set; }
        public string reference_number { get; set; }
        public DateTime? book_payment_date { get; set; }
        public string post_round_remark { get; set; }
    }
}
