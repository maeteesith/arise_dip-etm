﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class PostRoundActionImageAddModel : BaseModel {
        public long post_round_id { get; set; }
        public long file_id { get; set; }
    }
}
