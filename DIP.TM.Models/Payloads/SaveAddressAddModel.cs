﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class SaveAddressAddModel : BaseModel {
        public long? save_id { get; set; }
        public string descrition { get; set; }
        public string address_type_code { get; set; }
        public string nationality_code { get; set; }
        public string career_code { get; set; }
        public string card_type_code { get; set; }
        public string card_number { get; set; }
        public string receiver_type_code { get; set; }
        public string name { get; set; }
        public string legal_name { get; set; }
        public string house_number { get; set; }
        public string village_number { get; set; }
        public string alley { get; set; }
        public string street { get; set; }
        public string address_sub_district_code { get; set; }
        public string address_district_code { get; set; }
        public string address_province_code { get; set; }
        public string postal_code { get; set; }
        public string address_country_code { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string sex_code { get; set; }
        public string address_representative_condition_type_code { get; set; }
        public string other_text { get; set; }
        public string country_text { get; set; }
        public bool? is_contact_person { get; set; }

        //for representative_list
        public string representative_type_code { get; set; }

        //for contact_address_list
        public bool? is_specific { get; set; }

        //Save050ReceiverAddressPeople
        public string receiver_people_type_code { get; set; }


        //for eform_save060
        public bool? is_contract_check { get; set; }
        public bool? is_detail_check { get; set; }
        public long? change_ref_id { get; set; }

        //PagingModel paging { set; get; }


        // Save060AddressPeople
        public long? people_id { get; set; }
    }
}
