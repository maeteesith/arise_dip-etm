﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vSaveSendModel : BaseModel {
        public DateTime? request_date { get; set; }
        public DateTime? make_date { get; set; }
        public string request_number { get; set; }
        public DateTime? department_send_date { get; set; }
        public string request_type_code { get; set; }
        public string request_type_name { get; set; }
        public string save_status_code { get; set; }
        public string save_status_name { get; set; }
        public string request_source_code { get; set; }
        public string request_source_name { get; set; }
        public string department_send_code { get; set; }
        public string department_send_name { get; set; }
        public string evidence_address { get; set; }
    }


}