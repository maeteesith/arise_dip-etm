﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save140AddModel : SaveModel {
        public string register_number { get; set; }
        public string save140_sue_type_code { get; set; }
        public string black_index_1 { get; set; }
        public string black_index_2 { get; set; }
        public string red_index_1 { get; set; }
        public string red_index_2 { get; set; }
        public DateTime? sue_date { get; set; }
        public string description { get; set; }
        public string save140_sue_report_type_code { get; set; }
    }
}
