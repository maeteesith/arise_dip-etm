﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save060AddressRepresentativeAddModel: SaveAddressAddModel  {
        public string save060_representative_type_code { get; set; }
    }
}
