﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentRole02GroupAddModel : BaseModel {
        public long? save_id { get; set; }
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public string post_number { get; set; }
        public string book_number { get; set; }
        public int? rule_count { get; set; }
        public DateTime? instruction_send_date { get; set; }
        public DateTime? post_round_document_post_date { get; set; }
        public DateTime? document_role02_date { get; set; }
        public DateTime? document_role02_receive_date { get; set; }
        public DateTime? document_role02_print_document_date { get; set; }
        public DateTime? document_role02_print_cover_date { get; set; }
        public DateTime? document_role02_post_number_date { get; set; }
        public DateTime? document_role02_print_list_date { get; set; }
        public string post_round_type_code { get; set; }
        public string post_round_type_name { get; set; }
        public long? document_role02_receiver_by { get; set; }
        public string document_role02_receiver_by_name { get; set; }
        public string created_by_name { get; set; }
        public string department_code { get; set; }
        public string department_name { get; set; }
        public string document_role02_status_code { get; set; }
        public string document_role02_status_name { get; set; }
        public string document_role02_receive_status_code { get; set; }
        public string document_role02_receive_status_name { get; set; }
        public string document_role02_print_document_status_code { get; set; }
        public string document_role02_print_document_status_name { get; set; }
        public string document_role02_print_cover_status_code { get; set; }
        public string document_role02_print_cover_status_name { get; set; }
        public string document_role02_print_list_status_code { get; set; }
        public string document_role02_print_list_status_name { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
        public string name { get; set; }
        public string house_number { get; set; }
        public string province_name { get; set; }
        public string postal_name { get; set; }
        public bool? is_add_more_address { get; set; }
        public string document_role02_remark { get; set; }
        public string document_role02_receive_remark { get; set; }

        //document-role02-check
        public List<PostRoundAddressAddModel> post_round_address_list { get; set; }
    }
}