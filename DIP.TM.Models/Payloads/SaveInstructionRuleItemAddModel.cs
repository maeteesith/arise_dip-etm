﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class SaveInstructionRuleItemAddModel : BaseModel
    {
        public long? intruction_rule_id { get; set; }
        public string instruction_rule_item_code { get; set; }
        public string instruction_rule_description { get; set; }
        public string value_01 { get; set; }
        public string value_02 { get; set; }
        public string value_03 { get; set; }
        public string value_04 { get; set; }
        public string value_05 { get; set; }
    }
}
