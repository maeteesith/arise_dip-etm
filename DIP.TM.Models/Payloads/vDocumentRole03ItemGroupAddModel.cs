﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentRole03ItemGroupAddModel : BaseModel {
        public long save_id { get; set; }
        public long? save_01_index { get; set; }
        public string request_number { get; set; }
        public DateTime? consider_similar_document_date { get; set; }
        public DateTime? document_role03_split_date { get; set; }
        public DateTime? document_role03_receive_date { get; set; }
        public DateTime? document_role03_receive_return_date { get; set; }
        public string document_role03_receiver_by_name { get; set; }
        public string document_role03_receive_return_remark { get; set; }
        public string request_item_sub_type_1_code_list_text { get; set; }
        public string document_role03_status_code { get; set; }
        public string document_role03_status_name { get; set; }
        public int? document_role03_receive_status_index { get; set; }
        public string department_send_code { get; set; }
        public string department_send_name { get; set; }
        public string request_type_list_name { get; set; }

        public string document_role03_receive_status_code { get; set; }
        public string document_role03_receive_status_name { get; set; }
    }
}