﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vRecordRegistrationNumberAddModel : BaseModel {

        public long save_id { get; set; }
        public string request_number { get; set; }
        public DateTime? public_role05_date { get; set; }
        public long? public_role05_by { get; set; }
        public string public_role05_status_code { get; set; }
        public string registration_number { get; set; }
        public DateTime? book_payment_date { get; set; }
        public string book_index { get; set; }
        public string public_page_index { get; set; }
        public DateTime? public_start_date { get; set; }


        //
        public DateTime? floor3_proposer_date { get; set; }
        public long floor3_registrar_by { get; set; }

    }
}