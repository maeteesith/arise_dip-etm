﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vRecordRegistrationNumberAllowAddModel : BaseModel {
        public string request_number { get; set; }
        public long? floor3_proposer_by { get; set; }
        public string floor3_proposer_by_name { get; set; }
        public DateTime? floor3_proposer_date { get; set; }
        public long? floor3_registrar_by { get; set; }
        public DateTime? floor3_registrar_date { get; set; }
        public string floor3_registrar_description { get; set; }
        public string request_item_type_code { get; set; }
        public string request_item_type_name { get; set; }
        public string floor3_registrar_status_code { get; set; }
        public string floor3_registrar_status_name { get; set; }
    }
}