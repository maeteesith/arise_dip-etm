﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save080AddModel : SaveModel {
        public string register_number { get; set; }
        public string save080_revoke_type_code { get; set; }
        public string remark { get; set; }
        public string index_1 { get; set; }
        public string index_2 { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<Save080AddressRepresentativeAddModel> representative_list { get; set; }
        public SaveAddressAddModel contact_address { get; set; }
        public List<SaveAddressAddModel> joiner_list { get; set; }
        public List<Save080AddressRepresentativeAddModel> joiner_representative_list { get; set; }
    }
}
