﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save050AddressRepresentativeAddModel : SaveAddressAddModel  {
        public string save050_representative_type_code { get; set; }
    }
}
