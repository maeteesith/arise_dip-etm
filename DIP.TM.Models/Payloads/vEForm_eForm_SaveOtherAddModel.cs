﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vEForm_eForm_SaveOtherAddModel : BaseModel {
        public long? request_other_process_id { get; set; }
        public long save_id { get; set; }
        public long? request_other_item_sub_id { get; set; }
        public long? request_other_request_01_item_id { get; set; }
        public long eform_id { get; set; }
        public string eform_number { get; set; }
        public string telephone { get; set; }
        public string request_number { get; set; }
        public string registration_number { get; set; }
        public string reference_number { get; set; }
        public string request_index { get; set; }
        public DateTime? trademark_expired_start_date { get; set; }
        public DateTime? trademark_expired_date { get; set; }
        public DateTime? trademark_expired_end_date { get; set; }
        public decimal? save_total_price { get; set; }
        public int item_count { get; set; }
        public int item_sub_type_1_count { get; set; }
        public int product_count { get; set; }
        public string request_type_code { get; set; }
        public string request_type_name { get; set; }
        public string total_price { get; set; }
        public int fine { get; set; }
        public bool? is_evidence_floor7 { get; set; }
        public bool? is_sue { get; set; }
        public string name { get; set; }

        //
        public string remark { get; set; }
    }
}