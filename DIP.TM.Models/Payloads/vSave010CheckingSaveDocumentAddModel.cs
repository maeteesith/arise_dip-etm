﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vSave010CheckingSaveDocumentAddModel : BaseModel {
        public string request_number { get; set; }
        public string request_type_code { get; set; }
        public string request_type_name { get; set; }
        public DateTime? make_date { get; set; }
        public string consider_similar_document_status_code { get; set; }
        public string consider_similar_document_status_name { get; set; }
        public DateTime? consider_similar_document_date { get; set; }
        public string consider_similar_document_remark { get; set; }
        public string consider_similar_document_item_status_list { get; set; }
        public string consider_similar_document_file_id { get; set; }
    }
}