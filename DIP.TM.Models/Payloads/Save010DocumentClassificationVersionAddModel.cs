﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010DocumentClassificationVersionAddModel : BaseModel {
        public long save_id { get; set; }
        public long save_other_id { get; set; }
        public long? trademark_2d_file_id { get; set; }
        public string trademark_2d_file_name { get; set; }
        public string trademark_2d_physical_path { get; set; }
        public decimal? trademark_2d_file_size { get; set; }
        public string version_index { get; set; }
        public string save010_document_classification_language_code { get; set; }
        public string document_classification_version_status_code { get; set; }
        public DateTime? check_date { get; set; }
        public bool is_master { get; set; }
    }
}
