﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vSave010CheckingInstructionDocumentAddModel : BaseModel {
        public long? save_id { get; set; }
        public long? post_round_id { get; set; }
        public string request_number { get; set; }
        public string trademark_status_code { get; set; }
        public string instruction_rule_code { get; set; }
        public string instruction_rule_name { get; set; }
        public string considering_instruction_rule_status_code { get; set; }
        public string considering_instruction_rule_status_name { get; set; }
        public DateTime? change_status_date { get; set; }
        public string change_reason { get; set; }
        public string book_number { get; set; }
        public DateTime? book_start_date { get; set; }
        public string save010_checking_instruction_document_remark { get; set; }
    }
}