﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class PageDocumentRole03ChangeVersionAddModel : BaseModel {
        public Save010AddModel view { set; get; }

        public List<ReferenceMasterModel> allow_list { get; set; }
        ////public List<vDocumentRole02View> instruction_rule_list { get; set; }
        ////public List<vSave010CheckingSaveDocumentView> document_list { get; set; }
        ////public List<vReceiptItemView> receipt_item_list { get; set; }
        ////public List<vSave010HistoryView> history_list { get; set; }
        ////public List<vRequestDocumentCollectView> document_scan_list { get; set; }
        ////public List<vDocumentRole04CheckView> document_role04_check_list { get; set; }

        public List<vDocumentRole03ChangeCertificationFileAddModel> certification_file_list { get; set; }
        public List<vDocumentRole03ChangeRequestGroupAddModel> request_group_list { get; set; }

        public vDocumentRole03ItemAddModel document_role03_item { get; set; }
    }
}
