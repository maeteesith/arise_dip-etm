﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class vDocumentRole04ReleaseAddModel : BaseModel {
        public long? save_id { get; set; }
        public string request_number { get; set; }
        public DateTime? book_end_date { get; set; }
        public string name { get; set; }
        public string contact_address_information { get; set; }
        public string document_role04_release_type_code { get; set; }
        public string document_role04_release_type_name { get; set; }
        public string document_role04_release_check_list { get; set; }
        public string document_role04_release_status_code { get; set; }
        public string document_role04_release_status_name { get; set; }
        public string document_role05_release_status_code { get; set; }
        public string document_role05_release_status_name { get; set; }
        public string instruction_rule_code { get; set; }
        public string instruction_rule_name { get; set; }
        public string instruction_rule_description { get; set; }
        public DateTime? document_role04_release_send_date { get; set; }
        public DateTime? document_role05_release_send_date { get; set; }
        public string value_01 { get; set; }
        public string value_02 { get; set; }
        public string value_03 { get; set; }
        public string value_04 { get; set; }
        public string value_05 { get; set; }
        public long? document_role05_by { get; set; }
        public DateTime? document_role05_date { get; set; }
        public string document_role05_status_code { get; set; }
        public string document_role05_status_name { get; set; }
        public long? document_role05_release_by { get; set; }
        public string document_role05_release_by_name { get; set; }
    }
}
