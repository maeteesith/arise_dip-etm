﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentRole04CreateFileAddModel : BaseModel {
        public long document_role04_create_id { get; set; }
        public string remark { get; set; }
        public DateTime file_created_date { get; set; }
        public long? file_created_by { get; set; }
        public string file_created_by_name { get; set; }
        public long? file_id { get; set; }
        public string file_name { get; set; }
        public decimal? file_size { get; set; }
    }
}