﻿using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class ReceiptPaidAddModel : BaseModel {
        public long? receipt_id { get; set; }
        public string paid_channel_type_code { get; set; }
        public string paid_reference_number_1 { get; set; }
        public string paid_reference_number_2 { get; set; }
        public decimal? total_price { get; set; }
    }
}
