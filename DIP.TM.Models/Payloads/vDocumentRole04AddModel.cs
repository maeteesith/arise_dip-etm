﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentRole04AddModel : vSave010Model {
        public long? save_id { get; set; }
        public string request_number { get; set; }
        public DateTime? book_end_date { get; set; }
        public string document_role04_status_code { get; set; }
        public string document_role04_status_name { get; set; }
        public string document_role04_from_department_name { get; set; }
        public string document_role04_type_code { get; set; }
        public string document_role04_type_name { get; set; }
        public DateTime? document_role04_receive_date { get; set; }
        public long? document_role04_receiver_by { get; set; }
        public string document_role04_receiver_by_name { get; set; }
        public string document_role04_receive_remark { get; set; }
        public string document_role04_receive_status_code { get; set; }
        public string document_role04_receive_status_name { get; set; }
        public DateTime? document_role04_receive_send_date { get; set; }
        public string document_role05_status_code { get; set; }
        public string document_role05_status_name { get; set; }
        public DateTime? document_role05_receive_date { get; set; }
        public long? document_role05_receiver_by { get; set; }
        public string document_role05_receiver_by_name { get; set; }
        public string document_role05_receive_status_code { get; set; }
        public string document_role05_receive_status_name { get; set; }
        public DateTime? document_role05_receive_send_date { get; set; }
    }
}