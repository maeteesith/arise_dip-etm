﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class PageDocumentRole04ReleaseRequestAddModel : BaseModel {
        public List<vDocumentRole04ReleaseRequestAddModel> document_role04_release_request_list { get; set; }
    }
}
