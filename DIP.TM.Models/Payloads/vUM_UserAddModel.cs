﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vUM_UserAddModel : SaveModel {
        public string name { get; set; }
        public long? department_id { get; set; }
        public string department_name { get; set; }
    }

}