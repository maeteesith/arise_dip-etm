﻿using DIP.TM.Models.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010CertificationFileAddModel : BaseModel {
        public long save_id { get; set; }
        public long? file_id { get; set; }
        public string remark { get; set; }
    }
}
