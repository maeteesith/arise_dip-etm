﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class PublicRoundAddModel : BaseModel
    {
        public string book_index { get; set; }
        public DateTime? public_start_date { get; set; }
        public DateTime? public_end_date { get; set; }
        public string public_round_status_code { get; set; }

        public List<PublicRoundItemAddModel> public_round_item { get; set; }
        public List<Save010AddModel> save010_list { get; set; }
    }
}
