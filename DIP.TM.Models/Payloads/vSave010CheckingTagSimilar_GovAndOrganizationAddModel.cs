﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class vSave010CheckingTagSimilar_GovAndOrganizationAddModel : vSave010Model
    {
        public long save_id { get; set; }
        public long save_tag_id { get; set; }
        public bool is_same { get; set; }
        public bool is_like_approve { get; set; }
        public bool is_same_approve { get; set; }
        public bool? method_1 { get; set; }
        public bool? method_2 { get; set; }
        public bool? method_3 { get; set; }
        public bool? method_4 { get; set; }
        public bool? method_5 { get; set; }
        public bool? method_6 { get; set; }
        public bool? method_7 { get; set; }
        public bool is_owner_same { get; set; }
        public int? case10_count { get; set; }
        public bool is_created_instruction_rule { get; set; }
        public string instruction_rule_code { get; set; }
        public string instruction_rule_value_1 { get; set; }
    }
}
