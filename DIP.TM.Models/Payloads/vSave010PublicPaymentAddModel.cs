﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vSave010PublicPaymentAddModel : BaseModel {
        public long? save_id { get; set; }
        public long? public_round_id { get; set; }
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public DateTime? public_start_date { get; set; }
        public DateTime? public_end_date { get; set; }
        public string book_index { get; set; }
        public string public_page_index { get; set; }
        public string public_line_index { get; set; }
        public string public_page_index_line_index { get; set; }
        public DateTime? public_role02_check_date { get; set; }
        public string intruction_rule41_date { get; set; }
        public DateTime? public_role02_consider_payment_date { get; set; }
        public DateTime? public_role05_consider_payment_date { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
        public decimal? total_price { get; set; }
        public long? public_role02_consider_payment_by { get; set; }
        public string public_role02_consider_payment_by_name { get; set; }
        public long? public_role05_consider_payment_by { get; set; }
        public string public_role05_consider_payment_by_name { get; set; }
        public string public_role02_consider_payment_status_code { get; set; }
        public string public_role02_consider_payment_status_name { get; set; }
        public string public_role05_consider_payment_status_code { get; set; }
        public string public_role05_consider_payment_status_name { get; set; }
        public string public_role02_document_payment_status_code { get; set; }
        public string public_role02_document_payment_status_name { get; set; }
        public string post_round_document_post_status_1_code { get; set; }
        public string post_round_document_post_status_1_name { get; set; }
        public string post_round_document_post_status_2_code { get; set; }
        public string post_round_document_post_status_2_name { get; set; }
        public string post_round_document_post_status_code { get; set; }
        public long? post_round_id_1 { get; set; }
        public string post_number_1 { get; set; }
        public string book_number_1 { get; set; }
        public DateTime? book_start_1_date { get; set; }
        public DateTime? book_end_1_date { get; set; }
        public int? book_expired_day_1 { get; set; }
        public DateTime? book_acknowledge_1_date { get; set; }
        public DateTime? book_payment_1_date { get; set; }
        public string reference_number_1 { get; set; }
        public long? post_round_id_2 { get; set; }
        public string post_number_2 { get; set; }
        public string book_number_2 { get; set; }
        public DateTime? book_start_2_date { get; set; }
        public DateTime? book_end_2_date { get; set; }
        public int? book_expired_day_2 { get; set; }
        public DateTime? book_acknowledge_2_date { get; set; }
        public DateTime? book_payment_2_date { get; set; }
        public string reference_number_2 { get; set; }
        public string save010_public_payment_remark { get; set; }
        public string post_round_instruction_rule_code { get; set; }
        public string post_round_instruction_rule_name { get; set; }
        public string name { get; set; }
        public string house_number { get; set; }
        public string book_remark_1 { get; set; }
        public string book_remark_2 { get; set; }
        public string book_remark { get; set; }
        public string value_01 { get; set; }
        public string value_02 { get; set; }
        public string value_03 { get; set; }
        public string value_04 { get; set; }
        public string value_05 { get; set; }

        public string reference_number { get; set; }
        public DateTime? book_start_date { get; set; }
        
    }
}