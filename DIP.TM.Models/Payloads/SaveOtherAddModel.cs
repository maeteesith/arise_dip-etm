﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class SaveOtherAddModel : SaveModel {
        public string request_type_code { get; set; }
        public string evidence_address { get; set; }
        public long? department_id { get; set; }
        public DateTime? send_date { get; set; }

        // For considering-similar-document
    }
}
