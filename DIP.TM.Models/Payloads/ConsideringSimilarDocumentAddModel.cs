﻿using DIP.TM.Models.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class ConsideringSimilarDocumentAddModel : BaseModel {
        public string request_number { get; set; }
        public List<vSave010CheckingInstructionDocumentAddModel> instruction_rule_list { get; set; }
        public List<vSave010CheckingSaveDocumentAddModel> save_list { get; set; }
    }
}
