﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010InstructionRuleAddModel : BaseModel {
        public long? save_id { get; set; }
        public DateTime? instruction_date { get; set; }
        public string instruction_rule_code { get; set; }
        public string rule_remark { get; set; }
        public string rule_description { get; set; }
        public string considering_instruction_status_code { get; set; }
        public string considering_book_status_code { get; set; }
        public DateTime? instruction_send_date { get; set; }
        public string instruction_number { get; set; }
        public string considering_instruction_rule_status_code { get; set; }
        public DateTime? change_status_date { get; set; }
        public string change_reason { get; set; }
        public string value_1 { get; set; }
        public string value_2 { get; set; }
        public string value_3 { get; set; }
        public string value_4 { get; set; }
        public string value_5 { get; set; }
        public string document_status_code { get; set; }
        public long? document_spliter_by { get; set; }
        public long? document_receiver_by { get; set; }
        public string document_receive_status_code { get; set; }
        public DateTime? document_receive_date { get; set; }
        public DateTime? document_send_date { get; set; }

        public List<Save010InstructionRuleItemAddModel> item_list { get; set; }
        public List<Save010InstructionRule9ItemSubType1AddModel> role9_item_sub_type1_list { get; set; }
        public List<Save010InstructionRuleRequestNumberAddModel> request_number_save_list { get; set; }
    }
}
