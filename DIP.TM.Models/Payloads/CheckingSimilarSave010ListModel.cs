﻿using DIP.TM.Models.Pagers;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class CheckingSimilarSave010ListModel : BaseModel {
        public long request01_item_id { get; set; }

        public bool is_have_image { set; get; }
        public bool is_have_sound { set; get; }

        public string search_1_request_number_start { set; get; }
        public string search_1_request_number_end { set; get; }
        public string search_1_people_name { set; get; }
        public string search_1_request_item_sub_type_1_description_text { set; get; }
        public string search_1_word_mark { set; get; }

        public string search_1_request_item_sub_type_1_code { set; get; }
        public string search_1_word_first_code { set; get; }
        public string search_1_word_sound_last_code { set; get; }
        public string search_1_word_sound_last_other_code { set; get; }

        public string search_1_1_word_samecondition_code { set; get; }
        public string search_1_1_word_mark { set; get; }
        public string search_1_2_word_samecondition_code { set; get; }
        public string search_1_2_word_syllable_sound { set; get; }

        public string search_1_document_classification_image_code { set; get; }
        public string search_1_document_classification_image_condition { set; get; }
        public string search_1_document_classification_sound_code { set; get; }
        public string search_1_document_classification_sound_condition { set; get; }

        public string display_type { get; set; }

        public PageRequest paginate { set; get; }
    }
}