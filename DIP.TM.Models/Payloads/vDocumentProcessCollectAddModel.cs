﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentProcessCollectAddModel : BaseModel {
        public long save_id { get; set; }
        public string request_number { get; set; }
        public DateTime? make_date { get; set; }
        public string sender_by_name { get; set; }
        public string department_name { get; set; }
        public string remark { get; set; }
        public long? file_id { get; set; }
        public string file_trademark_2d { get; set; }
        public string document_classification_collect_status_code { get; set; }
        public string document_classification_collect_status_name { get; set; }
        public long? maker_by { get; set; }
        public string maker_by_name { get; set; }
        public string name { get; set; }
    }
}