﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vFloor3DocumentAddModel : BaseModel {
        public string request_number { get; set; }
        public string request_item_type_code { get; set; }
        public string request_item_type_name { get; set; }
        public string name { get; set; }
        public string registration_number { get; set; }
        public string book_index { get; set; }
        public string public_page_index { get; set; }
    }
}