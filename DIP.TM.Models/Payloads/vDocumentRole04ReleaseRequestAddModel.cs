﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentRole04ReleaseRequestAddModel : BaseModel {
        public long save010_checking_tag_similar_id { get; set; }
        public long? post_round_id { get; set; }
        public string book_number { get; set; }
        public DateTime? book_start_date { get; set; }
        public DateTime? book_acknowledge_date { get; set; }
        public DateTime? book_end_date { get; set; }
        public string document_role04_receive_remark { get; set; }
        public string post_round_instruction_rule_code { get; set; }
        public string post_round_instruction_rule_name { get; set; }
        public string post_round_instruction_rule_description { get; set; }
        public string post_round_type_code { get; set; }
        public string post_round_type_name { get; set; }
        public string document_role04_release_request_type_code { get; set; }
        public string document_role04_release_request_type_name { get; set; }
        public string document_role04_release_request_send_type_code { get; set; }
        public string document_role04_release_request_send_type_name { get; set; }
        public DateTime? document_role04_receive_date { get; set; }
        public DateTime? document_role04_receive_send_date { get; set; }
        public DateTime? document_role05_receive_date { get; set; }
        public DateTime? document_role05_receive_send_date { get; set; }
        public long? document_role04_receiver_by { get; set; }
        public string document_role04_receiver_by_name { get; set; }
        public long? document_role05_receiver_by { get; set; }
        public string document_role05_receiver_by_name { get; set; }
        public string document_role04_status_code { get; set; }
        public string document_role04_status_name { get; set; }
        public int? document_role04_receive_status_index { get; set; }
        public string document_role04_receive_status_code { get; set; }
        public string document_role04_receive_status_name { get; set; }
        public string document_role05_status_code { get; set; }
        public string document_role05_status_name { get; set; }
        public int? document_role05_receive_status_index { get; set; }
        public string document_role05_receive_status_code { get; set; }
        public string document_role05_receive_status_name { get; set; }
        public long save_id { get; set; }
        public string save_request_number { get; set; }
        public string name { get; set; }
        public int? save010_case28_count { get; set; }
        public long? considering_receiver_by { get; set; }
        public string considering_receiver_by_name { get; set; }
        public string trademark_status_code { get; set; }
        public long save_tag_id { get; set; }
        public string save_tag_request_number { get; set; }
        public string considering_instruction_rule_status_code { get; set; }
        public string considering_instruction_rule_status_name { get; set; }
    }
}