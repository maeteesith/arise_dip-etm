﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    public class PagingModel  {
        public string sort_name { get; set; } = "id";
        public bool is_desc { get; set; } = true;
        public int page_index { get; set; } = 1;
        public int page_total { get; set; }
        public int item_per_page { get; set; } = 50;
        public int item_start { get; set; }
        public int item_end { get; set; }
        public int item_total { get; set; }
    }
}
