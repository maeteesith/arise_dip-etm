﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save060AddModel : SaveModel {
        public string register_number { get; set; }
        public string save060_representative_change_code { get; set; }
        public string save060_period_request_code { get; set; }
        public string save060_send_department_code { get; set; }
        public string modify_description { get; set; }
        public bool is_oppose_modify_description { get; set; }
        public string representative_change_code { get; set; }
        public string certification_mark_rule { get; set; }

        public List<Save060AllowContactAddModel> allow_people_list { get; set; }
        public SaveAddressAddModel contact_address { get; set; }
        public SaveAddressAddModel contact_address_06 { get; set; }
        public List<SaveAddressAddModel> joiner_list { get; set; }
        public List<SaveAddressAddModel> people_list { get; set; }
        public List<Save060AddressRepresentativeAddModel> representative_list { get; set; }
        public List<SaveProductAddModel> allow_product_list { get; set; }
        public List<SaveProductAddModel> product_list { get; set; }
    }
}
