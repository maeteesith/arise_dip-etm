﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save210AddModel : EFormModel
    {
        public DateTime? request_date { get; set; }
        public DateTime? registration_date { get; set; }
        public DateTime? case_28_date { get; set; }
        public string save210_search_type_code { get; set; }
        public string inter_registration_number { get; set; }
        public string request_number_list { get; set; }
        public string save210_informer_type_code { get; set; }
        public string save210_representative_condition_type_code { get; set; }
        public string save210_contact_type_code { get; set; }
        public string remark_7 { get; set; }
        public bool? is_8_1 { get; set; }
        public bool? is_8_2 { get; set; }
        public bool? is_8_3 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }


        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }
        public List<SaveProductAddModel> product_list { get; set; }
    }
}
