﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save150EvidenceAddModel : BaseModel
    {
        public long? save_id { get; set; }
        public string evidence_type { get; set; }
        public string evidence_subject { get; set; }
        public int? number { get; set; }
        public string note { get; set; }
    }
}
