﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save010PublicPaymentAddModel : BaseModel
    {
        public long? save_id { get; set; }
        public decimal? total_price { get; set; }
        public string public_role02_consider_payment_status_code { get; set; }
        public DateTime? public_role02_consider_payment_date { get; set; }
        public long? public_role02_consider_payment_by { get; set; }
        public string public_role05_consider_payment_status_code { get; set; }
        public DateTime public_role05_consider_payment_date { get; set; }
        public long? public_role05_consider_payment_by { get; set; }
        public string public_role02_document_payment_status_code { get; set; }
    }
}
