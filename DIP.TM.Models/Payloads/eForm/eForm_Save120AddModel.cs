﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save120AddModel : EFormModel {
        //step2
        public string save120_submit_type_code { get; set; }
        public string rule_number { get; set; }
        //step4
        public string save120_representative_condition_type_code { get; set; }
        //step5
        public string remark_5 { get; set; }
        //step6
        public string remark_6 { get; set; }
        //step7
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
    }
}
