﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save080AddModel : EFormModel {
        public string save080_request_item_type_code { get; set; }
        public string save080_revoke_type_code { get; set; }
        public string save080_informer_type_code { get; set; }
        public string save080_contact_type_code { get; set; }
        public string save080_receiver_contact_type_code { get; set; }
        public DateTime? contract_request_date { get; set; }
        public DateTime? contract_end_date { get; set; }
        public bool? is_9_1 { get; set; }
        public bool? is_9_2 { get; set; }
        public bool? is_9_3 { get; set; }
        public bool? is_9_4 { get; set; }
        public string contract_ref_number { get; set; }
        public string save080_search_type_code { get; set; }
        public string save080_submit_type_code { get; set; }
        public string rule_number { get; set; }
        public string save080_representative_condition_type_code { get; set; }
        public string save080_receiver_representative_condition_type_code { get; set; }
        public string evidence_name { get; set; }
        public string evidence_telephone { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string sign_inform_receiver_person_list { get; set; }
        public string sign_inform_receiver_representative_list { get; set; }
        public string remark_7 { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }
        public List<SaveAddressAddModel> receiver_people_list { get; set; }
        public List<SaveAddressAddModel> receiver_representative_list { get; set; }
        public List<SaveAddressAddModel> receiver_contact_address_list { get; set; }
        public List<eForm_Save080EvidenceAddModel> evidence_list { get; set; }
    }
}
