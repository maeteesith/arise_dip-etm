﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save030AppealOrderAddModel : BaseModel {
        public long? save_id { get; set; }
        public string considering_similar_instruction_rule_code { get; set; }
        public string description { get; set; }
    }
}
