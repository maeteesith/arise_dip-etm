﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save010Kor10AddModel : EFormModel
    {
        public long? save_id { get; set; }

        //step11.1.1
        public string save010_kor10_representative_condition_type_code { get; set; }
        //step11.1.2
        public bool? is_11_1_2_1 { get; set; }
        public bool? is_11_1_2_2 { get; set; }

        //step11.1.4
        public bool? is_11_1_1 { get; set; }
        public bool? is_11_1_2 { get; set; }
        public bool? is_11_1_3 { get; set; }
        public bool? is_11_1_4 { get; set; }
        public bool? is_11_1_5 { get; set; }
        public bool? is_11_1_6 { get; set; }
        public bool? is_11_1_7 { get; set; }
        public bool? is_11_1_8 { get; set; }
        public bool? is_11_1_9 { get; set; }
        public string remark_11_1_9 { get; set; }

        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<eForm_Save010Kor10ProductAddModel> product_list { get; set; }
        public List<eForm_Save010Kor10EventAddModel> event_list { get; set; }
        public List<eForm_Save010Kor19AddModel> kor19_list { get; set; }
    }
}
