﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save060CheckingSimilarWordTranslateAddModel : BaseModel
    {
        public long? save_id { get; set; }
        public long? change_ref_id { get; set; }
        public int? save_index { get; set; }
        public string word_translate_search { get; set; }
        public string word_translate_dictionary { get; set; }
        public string word_translate_sound { get; set; }
        public string word_translate_translate { get; set; }
        public string checking_word_translate_status_code_ { get; set; }
        public string translation_language_code { get; set; }
        public string word_translate { get; set; }
        public string word_translate_sound_2 { get; set; }
        public string word_translate_sound_3 { get; set; }
    }
}
