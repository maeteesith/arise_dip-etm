﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save140FinalJudgmentAddModel : BaseModel
    {
        public long? save_id { get; set; }
        public string court_name { get; set; }
        public string black_number_1 { get; set; }
        public string black_number_2 { get; set; }
        public string red_number_1 { get; set; }
        public string red_number_2 { get; set; }
    }
}
