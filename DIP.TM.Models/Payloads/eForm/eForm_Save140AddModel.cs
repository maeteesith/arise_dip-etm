﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save140AddModel : EFormModel
    {
        //step2
        public string save140_informer_type_code { get; set; }
        //step3
        public string save140_inform_type_code { get; set; }
        //step4
        public string save140_representative_condition_type_code { get; set; }
        //step5
        public string save140_submit_type_code { get; set; }
        public string save140_department_code { get; set; }
        public string rule_number { get; set; }
        //step6
        public string remark_6_3 { get; set; }
        //step7
        public bool? is_7_1 { get; set; }
        public bool? is_7_2 { get; set; }
        public bool? is_7_3 { get; set; }
        //step8
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string sign_inform_others_list { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }
        public List<eForm_Save140SueAddModel> sue_list { get; set; }
        public List<eForm_Save140JudgmentAddModel> judgment_list { get; set; }
        public List<eForm_Save140FinalJudgmentAddModel> final_judgment_list { get; set; }
    }
}
