﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save200AddModel : EFormModel
    {
        //step3
        public string save200_recipient_code { get; set; }
        public bool? is_instruction { get; set; }
        public string request_name { get; set; }

        //step4
        public string remark_5 { get; set; }
        //step5
        public string remark_6 { get; set; }
        //step6
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<eForm_Save200RequestRefAddModel> request_ref_list { get; set; }
    }
}
