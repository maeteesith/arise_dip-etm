﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save010Kor10EventAddModel : BaseModel
    {
        public long? save_id { get; set; }
        public string product { get; set; }
        public DateTime? event_date { get; set; }
        public string event_place { get; set; }
        public string event_organizer { get; set; }
    }
}
