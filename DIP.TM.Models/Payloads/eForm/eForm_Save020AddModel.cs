﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save020AddModel : EFormModel
    {
        //step 3
        public string save020_representative_condition_type_code { get; set; }
        //step 4
        public string save020_contact_type_code { get; set; }
        //step 5
        public long? evidence_file_id { get; set; }
        //step 6
        public bool? is_6_1 { get; set; }
        public bool? is_6_2 { get; set; }
        public bool? is_6_3 { get; set; }
        public bool? is_6_4 { get; set; }
        public bool? is_6_5 { get; set; }
        public bool? is_6_6 { get; set; }
        //step 8
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }
    }
}
