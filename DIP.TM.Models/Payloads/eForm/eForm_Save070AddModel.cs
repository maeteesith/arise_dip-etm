﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save070AddModel : EFormModel {
        public string save070_submit_type_code { get; set; }
        public string save070_request_item_type_code { get; set; }
        public string save070_representative_condition_type_code { get; set; }
        public string save070_contact_type_code { get; set; }
        public string save070_extend_type_code { get; set; }
        public double? fee { get; set; }
        public double? fine { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }

        public List<SaveProductAddModel> product_list { get; set; }
    }
}
