﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save200RequestRefAddModel : BaseModel
    {
        public long? save_id { get; set; }
        public string rule_number { get; set; }
        public string save200_request_type_code { get; set; }
        public long? request_ref_id { get; set; }
    }
}
