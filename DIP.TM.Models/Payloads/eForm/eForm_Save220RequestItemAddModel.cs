﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save220RequestItemAddModel : EFormModel
    {
        public long? save_id { get; set; }
        public long? request_id { get; set; }
        public string inter_registration_number { get; set; }
        public DateTime? inter_registration_cancel_date { get; set; }
        public DateTime? inter_registration_date { get; set; }
        public DateTime? protection_date { get; set; }
        public DateTime? case_28_date { get; set; }
        public string save220_informer_type_code { get; set; }
        public string save220_representative_condition_type_code { get; set; }
        public string save220_contact_type_code { get; set; }
        public string save220_img_type_type_code { get; set; }
        public long? img_file_2d_id { get; set; }
        public long? img_file_3d_id_1 { get; set; }
        public long? img_file_3d_id_2 { get; set; }
        public long? img_file_3d_id_3 { get; set; }
        public long? img_file_3d_id_4 { get; set; }
        public long? img_file_3d_id_5 { get; set; }
        public long? img_file_3d_id_6 { get; set; }
        public int? img_w { get; set; }
        public int? img_h { get; set; }
        public string remark_5_1_3 { get; set; }
        public bool? is_sound_mark { get; set; }
        public bool? is_sound_mark_human { get; set; }
        public bool? is_sound_mark_animal { get; set; }
        public bool? is_sound_mark_sound { get; set; }
        public bool? is_sound_mark_other { get; set; }
        public string remark_5_2_2 { get; set; }
        public long? sound_file_id { get; set; }
        public long? sound_jpg_file_id { get; set; }
        public bool? is_color_protect { get; set; }
        public string remark_8 { get; set; }
        public bool? is_model_protect { get; set; }
        public string remark_9 { get; set; }
        public bool? is_before_request { get; set; }
        public bool? is_14_1 { get; set; }
        public bool? is_14_2 { get; set; }
        public bool? is_14_3 { get; set; }
        public bool? is_14_4 { get; set; }
        public bool? is_14_5 { get; set; }
        public bool? is_14_6 { get; set; }
        public bool? is_14_7 { get; set; }
        public bool? is_14_8 { get; set; }
        public bool? is_14_9 { get; set; }
        public bool? is_14_10 { get; set; }
        public bool? is_14_11 { get; set; }
        public decimal? transfer_fee { get; set; }
        public decimal? product_fee { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }


        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }
        public List<eForm_Save220CheckingSimilarWordTranslateAddModel> checking_similar_translate_list { get; set; }
        public List<SaveProductAddModel> product_list { get; set; }
        public List<eForm_Save220Kor10AddModel> kor10_list { get; set; }
    }
}
