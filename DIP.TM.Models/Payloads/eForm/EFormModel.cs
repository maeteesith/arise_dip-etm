﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class EFormModel : BaseModel {
        public DateTime? make_date { get; set; }
        public string eform_number { get; set; }
        public string email { get; set; }
        public string email_uuid { get; set; }
        public string telephone { get; set; }
        public string request_number { get; set; }
        public string registration_number { get; set; }
        public string payer_name { get; set; }
        public decimal? total_price { get; set; }
        public string wizard { get; set; }
    }
}