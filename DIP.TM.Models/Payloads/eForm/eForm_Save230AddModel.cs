﻿
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save230AddModel : EFormModel
    {
        public string save230_document_type_code { get; set; }
        public string document_other_text { get; set; }
        public string save230_informer_type_code { get; set; }
        public string save230_representative_condition_type_code { get; set; }
        public bool? is_4_1 { get; set; }
        public bool? is_4_2 { get; set; }
        public bool? is_4_3 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
    }
}
