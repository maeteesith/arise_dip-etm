﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save050AddModel : EFormModel {
        public string save050_allow_type_code { get; set; }
        public string save050_people_type_code { get; set; }
        public string save050_representative_condition_type_code { get; set; }
        public string save050_receiver_representative_condition_type_code { get; set; }
        public string save050_receiver_people_type_code { get; set; }
        public string save050_contact_type_code { get; set; }
        public string save050_transfer_form_code { get; set; }
        public DateTime? contract_date { get; set; }
        public DateTime? contract_start_date { get; set; }
        public bool? is_time_limit { get; set; }
        public DateTime? contract_end_date { get; set; }
        public string contract_duration { get; set; }
        public string remark_8_2 { get; set; }
        public string remark_8_3 { get; set; }
        public bool? is_people_rights { get; set; }
        public bool? is_people_authorize { get; set; }
        public bool? is_receiver_transfer { get; set; }
        public bool? is_receiver_authorize { get; set; }
        public string save050_extend_type_code { get; set; }
        public bool? is_10_1 { get; set; }
        public bool? is_10_2 { get; set; }
        public bool? is_10_3 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string sign_inform_receiver_person_list { get; set; }
        public string sign_inform_receiver_representative_list { get; set; }
        public string save050_search_type_code { get; set; }
        public string contract_ref_number { get; set; }
        public string save050_submit_type_code { get; set; }
        public string rule_number { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> people_change_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }
        public List<SaveAddressAddModel> receiver_people_list { get; set; }
        public List<SaveAddressAddModel> receiver_representative_list { get; set; }
        public List<SaveProductAddModel> product_list { get; set; }
    }
}
