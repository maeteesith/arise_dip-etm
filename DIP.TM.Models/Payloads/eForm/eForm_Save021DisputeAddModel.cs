﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save021DisputeAddModel : BaseModel
    {
        public long save_id { get; set; }
        public long challenge_id { get; set; }
        public string challenge_number { get; set; }

        public Save020AddModel challenge { get; set; }
    }
}
