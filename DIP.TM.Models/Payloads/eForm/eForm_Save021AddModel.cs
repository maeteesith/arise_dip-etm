﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save021AddModel : EFormModel {
        public string save021_representative_condition_type_code { get; set; }
        public string save021_contact_type_code { get; set; }
        public string save021_remark_4 { get; set; }
        public string kor_19_inform_person_type_code { get; set; }
        public string kor_19_remark { get; set; }
        public bool? is_6_1 { get; set; }
        public bool? is_6_2 { get; set; }
        public bool? is_6_3 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public long? argue_file_id { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }
        public List<eForm_Save021DisputeAddModel> dispute_list { get; set; }
    }
}
