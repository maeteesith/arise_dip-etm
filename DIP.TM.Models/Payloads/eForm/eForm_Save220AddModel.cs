﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save220AddModel : EFormModel
    {
        public string save220_search_type_code { get; set; }
        public string input_number_list { get; set; }
        public string input_number_check_list { get; set; }


        public List<eForm_Save220RequestItemAddModel> request_item_list { get; set; }
    }
}
