﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save010Kor19AddModel : EFormModel
    {
        //step11.2.2
        public long? save_id { get; set; }
        public string kor_number { get; set; }
        public int? postpone_day { get; set; }
        public string remark { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
    }
}
