﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save220Kor19AddModel : EFormModel
    {
        public long? save_id { get; set; }
        public string save220_kor19_representative_condition_type_code { get; set; }
        public string kor_number { get; set; }
        public int? postpone_day { get; set; }
        public string remark { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
    }
}
