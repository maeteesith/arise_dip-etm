﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class eForm_Save190AddModel : EFormModel
    {
        //step2
        public string save190_search_type_code { get; set; }
        public string contract_ref_number { get; set; }
        public string challenger_ref_number { get; set; }
        public string save190_informer_type_code { get; set; }
        //step3
        public string save190_representative_condition_type_code { get; set; }
        //step4
        public string save190_request_type_code { get; set; }
        public int? postpone_day { get; set; }
        //step5
        public string remark_5 { get; set; }
        //step6
        public string remark_6 { get; set; }
        //step6
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string sign_inform_others_list { get; set; }
        

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<SaveAddressAddModel> representative_list { get; set; }
        public List<SaveAddressAddModel> contact_address_list { get; set; }
    }
}
