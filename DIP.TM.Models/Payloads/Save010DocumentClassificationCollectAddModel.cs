﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010DocumentClassificationCollectAddModel : BaseModel {
        public long save_id { get; set; }
        public long file_id { get; set; }
        public string sender_by_name { get; set; }
        public string department_name { get; set; }
        public string remark { get; set; }
        public string document_classification_collect_status_code { get; set; }
        public long? maker_by { get; set; }

        // Load from Form
        public string request_number { get; set; }
    }
}
