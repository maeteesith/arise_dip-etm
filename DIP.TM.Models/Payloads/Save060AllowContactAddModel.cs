﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save060AllowContactAddModel : SaveAddressAddModel  {
        public string save060_allow_contact_code { get; set; }
    }
}
