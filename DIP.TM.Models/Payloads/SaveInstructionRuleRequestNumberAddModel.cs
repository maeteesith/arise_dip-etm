﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    /// <summary>
    /// 
    /// </summary>
    public class SaveInstructionRuleRequestNumberAddModel : BaseModel
    {
        public long? instruction_rule_id { get; set; }
        public string request_number { get; set; }
    }
}
