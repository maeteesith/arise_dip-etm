﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vCheckingSimilarResultDuplicate_SaveAddModel : BaseModel {
        public long save_id { get; set; }
        public long save_tag_id { get; set; }
        public string save_request_number { get; set; }
        public string request_number { get; set; }
        public string file_trademark_2d { get; set; }
        public string name { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
        public string word_mark_list_text { get; set; }
        public long? sound_file_id { get; set; }
        public string sound_file_physical_path { get; set; }
        public string trademark_status_code { get; set; }
        public bool? method_1 { get; set; }
        public bool? method_2 { get; set; }
        public bool? method_3 { get; set; }
        public bool? method_4 { get; set; }
        public bool? method_5 { get; set; }
        public bool? method_6 { get; set; }
        public bool? method_7 { get; set; }
    }
}