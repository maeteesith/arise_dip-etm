﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class CheckingSimilarResultDuplicateAddModel : BaseModel {
        public long save_id { get; set; }

        public List<vCheckingSimilarResultDuplicate_SaveAddModel> save_word_list { set; get; }
        public List<vCheckingSimilarResultDuplicate_SaveAddModel> save_image_list { set; get; }

        public List<vCheckingSimilarResultDuplicate_WordAddModel> word_list { set; get; }
    }
}