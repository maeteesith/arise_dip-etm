﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentProcessChangeAddModel : BaseModel {
        public long save_id { get; set; }
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public DateTime? check_date { get; set; }
        public string document_classification_version_status_code { get; set; }
        public string document_classification_version_status_name { get; set; }
        public string name { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
    }
}