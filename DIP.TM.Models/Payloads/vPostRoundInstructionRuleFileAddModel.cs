﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vPostRoundInstructionRuleFileAddModel : BaseModel {
        public long post_round_instruction_rule_id { get; set; }
        public long post_round_instruction_rule_file_id { get; set; }
        public string remark { get; set; }
        public string post_round_instruction_rule_file_type_code { get; set; }
        public string post_round_instruction_rule_file_type_name { get; set; }
        public DateTime file_created_date { get; set; }
        public long? file_created_by { get; set; }
        public string file_created_by_name { get; set; }
        public long? file_id { get; set; }
        public string file_name { get; set; }
        public decimal? file_size { get; set; }
    }
}