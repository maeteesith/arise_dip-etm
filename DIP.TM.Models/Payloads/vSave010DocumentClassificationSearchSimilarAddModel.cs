﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vSave010DocumentClassificationSearchSimilarAddModel : BaseModel {
        public string request_number { get; set; }
        public string file_trademark_2d { get; set; }
        public string people_name_list_text { get; set; }
        public string word_mark_list_text { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
        public string document_classification_image_code_list_text { get; set; }
    }
}