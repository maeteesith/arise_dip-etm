﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save040AddModel : SaveModel {
        public string register_number { get; set; }
        public string save040_transfer_type_code { get; set; }
        public string save040_transfer_form_code { get; set; }

        public List<SaveAddressAddModel> people_list { get; set; }
        public List<Save040AddressRepresentativeAddModel> representative_list { get; set; }
        public SaveAddressAddModel contact_address { get; set; }
        public List<SaveProductAddModel> product_list { get; set; }
    }
}
