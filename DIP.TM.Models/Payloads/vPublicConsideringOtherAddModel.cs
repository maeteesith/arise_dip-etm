﻿using DIP.TM.Datas;
using System;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class vPublicConsideringOtherAddModel : BaseModel {
        public long save_id { get; set; }
        public string request_number { get; set; }
        public DateTime? public_start_date { get; set; }
        public DateTime? public_end_date { get; set; }
        public string book_index { get; set; }
        public string post_round_type_code { get; set; }
        public string post_round_type_name { get; set; }
        public DateTime? document_role04_receive_date { get; set; }
        public DateTime? document_role05_receive_send_date { get; set; }
        public string document_role05_receiver_by_name { get; set; }
        public string document_role04_receive_status_code { get; set; }
        public string document_role04_receive_status_name { get; set; }
        public string value_01 { get; set; }
        public string value_02 { get; set; }
        public string value_03 { get; set; }
        public string value_04 { get; set; }
        public string value_05 { get; set; }
        public string public_considering_other_remark { get; set; }
    }
}
