﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010InstructionRule9ItemSubType1AddModel : BaseModel {
        public long? save_id { get; set; }
        public long? save010_product_id { get; set; }
        public long? suggestion_item_id { get; set; }
        public long? item_id { get; set; }
        public string item_code { get; set; }
        public string item_name { get; set; }
    }
}
