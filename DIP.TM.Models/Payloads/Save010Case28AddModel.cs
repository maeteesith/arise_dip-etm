﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010Case28AddModel : BaseModel {
        public long? save_id { get; set; }
        public string request_item_sub_type_1_code { get; set; }
        public string address_country_code { get; set; }
        public DateTime? case_28_date { get; set; }
        public bool? is_allow_role_28_1 { get; set; }
        public bool? is_allow_role_28_2 { get; set; }
        public string allow_role_28_description { get; set; }
    }
}
