﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class DocumentItemListPageModel : BaseModel {
        public long? document_people_by_receive { get; set; }
        public List<vDocumentItemModel> document_list { get; set; }
    }
}