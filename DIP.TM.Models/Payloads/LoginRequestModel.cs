﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads
{
    public class LoginRequestModel
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
