﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save070AddModel : SaveModel {
        public bool is_split_request { get; set; }
        public string register_number { get; set; }
        public double? fine { get; set; }
        public string split_request_number { get; set; }
        public SaveAddressAddModel contact_address { get; set; }
        public List<SaveProductAddModel> product_list { get; set; }
    }
}
