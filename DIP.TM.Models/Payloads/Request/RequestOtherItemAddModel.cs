﻿using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Views {
    public class RequestOtherItemAddModel : BaseModel {
        public long request_other_process_id { get; set; }
        public int? item_index { get; set; }
        public string request_type_code { get; set; }
        public int? item_count { get; set; }
        public int? item_sub_type_1_count { get; set; }
        public int? product_count { get; set; }
        public double? total_price { get; set; }
        public string book_index { get; set; }
        public string page_index { get; set; }
        public double? fine { get; set; }
        public bool? is_evidence_floor7 { get; set; }
        public bool? is_sue { get; set; }
        public string is_sue_text { get; set; }
        public List<RequestOtherItemSubAddModel> RequestOtherItemSub { get; set; }

        // For List
        public DateTime? request_date { get; set; }
        public string reference_number { get; set; }
        public string request_number { get; set; }
        public string receipt_status_code { get; set; }
    }
}
