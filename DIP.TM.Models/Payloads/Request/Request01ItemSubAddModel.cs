﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class Request01ItemSubAddModel : BaseModel {
        public long request01_item_id { get; set; }
        public int? item_sub_index { get; set; }
        public string item_sub_type_1_code { get; set; }
        public int? product_count { get; set; }
        public decimal? total_price { get; set; }
    }
}
