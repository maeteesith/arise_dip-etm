﻿using DIP.TM.Utils;
using System;

namespace DIP.TM.Models.Payloads {
    public class RequestAgencyModel : BaseModel {
        public string name { get; set; }
    }
}