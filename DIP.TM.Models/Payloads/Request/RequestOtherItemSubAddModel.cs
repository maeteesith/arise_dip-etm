﻿using System.Collections.Generic;

namespace DIP.TM.Models.Views {
    public class RequestOtherItemSubAddModel : BaseModel {
        public long request_other_item_id { get; set; }
        public long? eform_id { get; set; }
        public int? item_index { get; set; }
        public string request_number { get; set; }
        public string item_sub_type_1_code { get; set; }
        public int? product_count { get; set; }
        public double? total_price { get; set; }
        public double? fine { get; set; }
        public string request_index { get; set; }
    }
}
