﻿using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class RequestOtherRequest01ItemAddModel : BaseModel {
        public long request_other_process_id { get; set; }
        public string request_number { get; set; }
        public double? total_price { get; set; }
        public string reference_number { get; set; }
        public string name { get; set; }
        public string facilitation_act_status_code { get; set; }
        public string remark { get; set; }
        public string book_index { get; set; }
        public string page_index { get; set; }
        public string status_code { get; set; }
    }
}
