﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vEForm_eForm_Save010AddModel : BaseModel {
        public int request01_id { get; set; }
        public long eform_id { get; set; }
        public string eform_number { get; set; }
        public string telephone { get; set; }
        public string request_number { get; set; }
        public string item_type_code { get; set; }
        public string item_type_name { get; set; }
        public string income_type { get; set; }
        public string income_description { get; set; }
        public string size_over_income_type { get; set; }
        public string size_over_income_description { get; set; }
        public int? size_over_cm { get; set; }
        public int? size_over_price { get; set; }
        public bool? is_otop { get; set; }
        public int? request_01_item_sub_count { get; set; }
        public int? request_01_product_count { get; set; }
        public int? total_price { get; set; }
        public long? sound_file_id { get; set; }
        public string sound_file_physical_path { get; set; }
        public decimal? sound_file_size { get; set; }
        public string sound_type_code { get; set; }
        public string sound_description { get; set; }
    }
}