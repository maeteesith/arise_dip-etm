﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentRole04ReleaseRequestGroupAddModel : BaseModel {
        public string document_role04_release_request_type_code { get; set; }
        public string document_role04_release_request_type_name { get; set; }
        public string name { get; set; }
        public string request_number { get; set; }
        public string considering_receiver_by_name { get; set; }
        public DateTime? document_role04_receive_send_date { get; set; }
        public DateTime? document_role05_receive_send_date { get; set; }
        public string document_role04_receiver_by_name { get; set; }
        public string document_role05_receiver_by_name { get; set; }
        public string document_role04_status_code { get; set; }
        public string document_role04_status_name { get; set; }
        public int? document_role04_receive_status_index { get; set; }
        public string document_role05_status_code { get; set; }
        public string document_role05_status_name { get; set; }
        public int? document_role05_receive_status_index { get; set; }

        public string document_role04_receive_status_code { get; set; }
        public string document_role04_receive_status_name { get; set; }
        public string document_role05_receive_status_code { get; set; }
        public string document_role05_receive_status_name { get; set; }
    }
}