﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save010DocumentClassificationSoundAddModel : BaseModel {
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string document_classification_sound_code { get; set; }
    }
}
