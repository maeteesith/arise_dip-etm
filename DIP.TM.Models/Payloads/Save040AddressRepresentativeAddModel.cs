﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Payloads {
    /// <summary>
    /// 
    /// </summary>
    public class Save040AddressRepresentativeAddModel : SaveAddressAddModel  {
        public string save040_representative_type_code { get; set; }
    }
}
