﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vDocumentRole04CheckAddModel : BaseModel {
        public long? post_round_instruction_rule_id { get; set; }
        public long? instruction_rule_id { get; set; }
        public long? save_id { get; set; }
        public string instruction_rule_code { get; set; }
        public string instruction_rule_name { get; set; }
        public string instruction_rule_description { get; set; }
        public string new_instruction_rule_code { get; set; }
        public string new_instruction_rule_name { get; set; }
        public string new_instruction_rule_description { get; set; }
        public string document_role04_check_remark { get; set; }
        public DateTime? make_date { get; set; }
        public DateTime? book_start_date { get; set; }
        public DateTime? book_end_date { get; set; }
        public string document_role04_receive_check_list { get; set; }
        public string post_round_action_post_type_code { get; set; }
        public string post_round_action_post_type_name { get; set; }
        public DateTime? book_acknowledge_date { get; set; }
        public string document_role04_send_type_code { get; set; }
        public string document_role04_send_type_name { get; set; }
        public string document_role04_receive_status_code { get; set; }
        public string document_role04_receive_status_name { get; set; }
        public DateTime? document_role04_check_date { get; set; }
        public long? document_role05_receiver_by { get; set; }
        public string document_role05_receiver_by_name { get; set; }
        public string document_role05_receive_status_code { get; set; }
        public string document_role05_receive_status_name { get; set; }
    }
}