﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Payloads {
    public class vCheckingSimilarResultDuplicate_WordAddModel : BaseModel {
        public long? save_id { get; set; }
        public string request_number { get; set; }
        public string word_translate_search { get; set; }
        public string word_translate_sound { get; set; }
        public string word_translate_translate { get; set; }
        public string word_translate_dictionary_code { get; set; }
        public string word_translate_dictionary_name { get; set; }
        public string word_translate_dictionary_other { get; set; }
        public string checking_word_translate_status_code { get; set; }
        public string checking_word_translate_status_name { get; set; }
    }
}