﻿using DIP.TM.Models.Payloads;
using System;

namespace DIP.TM.Models {
    /// <summary>
    /// 
    /// </summary>
     public class BaseModel {
        public long[] ids { get; set; }
        public string cancel_reason { get; set; }

        public long? id { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public string created_by_name { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
        public string updated_by_name { get; set; }

        //public PagingModel paging { get; set; }

        /// <summary>
        ///  For Navigate Add/Update Graph
        /// </summary>
        public bool is_check { get; set; }
        public bool IsNew { get; set; }

    }

    public class BaseModelPayload<T> where T : class
    {
        public BaseModelPayload()
        {
            filter = default(T);
        }
        public T filter { get; set; }
    }

    public class BasePageModelPayload<T> where T : class
    {
        public BasePageModelPayload()
        {
            paging = new PagingModel();
            filter = default(T);
        }
        public PagingModel paging { get; set; }
        public T filter { get; set; }
    }
}
