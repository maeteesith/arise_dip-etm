﻿using System.ComponentModel.DataAnnotations;

namespace DIP.TM.Models.Email
{
    public class EmailModel
    {
        [Required]
        public string to { get; set; }
        public string from { get; set; }
        [Required]
        public string subject { get; set; }
        [Required]
        public string body { get; set; }

        public bool is_html { get; set; } = false;
    }
}
