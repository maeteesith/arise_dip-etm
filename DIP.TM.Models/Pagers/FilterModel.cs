﻿using System.Collections.Generic;

namespace DIP.TM.Models.Pagers
{
    public class FilterModel
    {
        public List<SearchByModel> search_by { get; set; } = new List<SearchByModel>();
        public string order_by { get; set; }
        public bool is_order_reverse { get; set; }
        public SearchTextModel search_text { get; set; }

        public int query_mode { get; set; }
        public List<string> filter_queries { get; set; } = new List<string>();

        public FilterModel()
        {
            search_by = new List<SearchByModel>();
            order_by = "id";
            is_order_reverse = false;
            filter_queries = new List<string>();
        }
    }

    public class SearchByModel
    {
        public Operation operation { get; set; }
        public string key { get; set; }
        public string value { get; set; }
        private List<string> _values;
        public List<string> values 
        {
            get { return string.IsNullOrWhiteSpace(value) ? _values : new List<string>() { value }; }
            set { _values = value; }
        }
    }

    public class SearchTextModel
    {
        public List<string> keys { get; set; }
        public string input_text { get; set; }
        public Operation operation { get; set; }

    }

    public enum Operation
    {
        Equals = 0,
        GreaterThan = 1,
        LessThan = 2,
        GreaterThanOrEqual = 3,
        LessThanOrEqual = 4,
        Contains = 5,
        StartsWith = 6,
        EndsWith = 7,
        StringEquals = 8,
        NotEquals = 9
    }
}
