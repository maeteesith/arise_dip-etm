﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Pagers
{
    public class PageRequest : FilterModel
    {
        public PageRequest()
        {
            page_index = 1;
            item_per_page = 50;
        }
        public int page_index { get; set; }
        public int item_per_page { get; set; }
        public int item_total { get; set; }
    }
}
