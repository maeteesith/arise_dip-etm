﻿namespace DIP.TM.Models.Configs
{
    public class AppConfigModel
    {
        public Connectionstrings ConnectionStrings { get; set; }
        public Email Email { get; set; }
        public string AllowedHosts { get; set; }
        public string BaseUrl { get; set; }
        public string SsoUrl { get; set; }
        public string SsoAppId { get; set; }
        public string SsoAppSecret { get; set; }
        public string Version { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string SigningKey { get; set; }
        public string SecretKey { get; set; }
        public Healthchecksui HealthChecksUI { get; set; }
        public string LineEndPoint { get; set; }
        public string Environment { get; set; }
        public string LineToken { get; set; }
        public int GuestUserId { get; set; }
        public Contentpath ContentPath { get; set; }
        public bool EnableMasterCache { get; set; }
        public int MasterCacheDuration { get; set; }
        public bool EnableAuditLog { get; set; }
    }



    public class Connectionstrings
    {
        public Defaultconnection DefaultConnection { get; set; }
    }

    public class Defaultconnection
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string DbName { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
        public int Timeout { get; set; }
    }

    public class Email
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public string Protocol { get; set; }
        public StartTls StartTls { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string From { get; set; }
        public string Name { get; set; }
        public bool IsAws { get; set; }
        public string Bcc { get; set; }
    }

    public class StartTls
    {
        public bool Enable { get; set; }
        public bool Required { get; set; }
    }
    public class Healthchecksui
    {
        public Healthcheck[] HealthChecks { get; set; }
        public Webhook[] Webhooks { get; set; }
        public int EvaluationTimeInSeconds { get; set; }
        public int MinimumSecondsBetweenFailureNotifications { get; set; }
    }

    public class Healthcheck
    {
        public string Name { get; set; }
        public string Uri { get; set; }
    }

    public class Webhook
    {
        public string Name { get; set; }
        public string Uri { get; set; }
        public string Payload { get; set; }
        public string RestoredPayload { get; set; }
    }

    public class Contentpath
    {
        public string BasePath { get; set; }
        public User User { get; set; }
        public Guest Guest { get; set; }
    }

    public class User
    {
        public string PhysicalPath { get; set; }
    }

    public class Guest
    {
        public string PhysicalPath { get; set; }
    }

}
