﻿using Newtonsoft.Json;
using System;

namespace DIP.TM.Models.ExternalModels
{
    public class AuthSsoResponseModel
    {
        [JsonProperty("info")]
        public AuthSsoInfoModel info { get; set; }
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public int? expires_in { get; set; }
        [JsonIgnore]
        public string bearer_token { get; set; }
    }

    public class AuthSsoInfoModel
    {
        public string username { get; set; }
        public string[] authorities { get; set; }
        public string name { get; set; }
        public string pid { get; set; }
        public object passport { get; set; }
        public object cid { get; set; }
        public string email { get; set; }
        [JsonProperty("address")]
        public AuthSsoAddressModel address { get; set; }
        public string cert_sn { get; set; }
        public string user_type { get; set; }
        public string user_type_desc { get; set; }

        [JsonProperty("user_id")]
        public int? user_sso_id { get; set; }
        public string title_name { get; set; }
        public string title_name_desc { get; set; }
        public string first_name { get; set; }
        public object middle_name { get; set; }
        public string last_name { get; set; }
        public object passport_country_id { get; set; }
        public object passport_country_name { get; set; }
        public object tax_id_number { get; set; }
        public DateTime? card_issued_date { get; set; }
        public object card_expire_date { get; set; }
        public int? nation_id { get; set; }
        public string nation_name { get; set; }
        public DateTime? birth_date { get; set; }
        [JsonProperty("contact_address")]
        public AuthSsoContactAddressModel contact_address { get; set; }
    }

    public class AuthSsoAddressModel
    {
        public object building { get; set; }
        public object floor { get; set; }
        public object village { get; set; }
        public object lane { get; set; }
        public object street { get; set; }
        public string postcode { get; set; }
        public object telephone { get; set; }
        public string mobile { get; set; }
        public object fax { get; set; }
        public string house_number { get; set; }
        public object room_number { get; set; }
        public object village_number { get; set; }
        public int? province_id { get; set; }
        public string province_name { get; set; }
        public int? district_id { get; set; }
        public string district_name { get; set; }
        public int? sub_district_id { get; set; }
        public string sub_district_name { get; set; }
        public object telephone_to { get; set; }
    }

    public class AuthSsoContactAddressModel
    {
        // TODO
        public string building { get; set; }
        public string floor { get; set; }
        public string village { get; set; }
        public string lane { get; set; }
        public string street { get; set; }
        public string postcode { get; set; }
        public string telephone { get; set; }
        public string mobile { get; set; }
        public object fax { get; set; }
        public string house_number { get; set; }
        public string room_number { get; set; }
        public string village_number { get; set; }
        public int? province_id { get; set; }
        public string province_name { get; set; }
        public int? district_id { get; set; }
        public string district_name { get; set; }
        public int? sub_district_id { get; set; }
        public string sub_district_name { get; set; }
        public object telephone_to { get; set; }
    }

}
