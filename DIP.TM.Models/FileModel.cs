﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models
{
    public class FileModel
    {
        public int file_id { get; set; }
        public string file_name { get; set; }
        public string file_url { get; set; }
    }
}
