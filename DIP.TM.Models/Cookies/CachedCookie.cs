﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Cookies
{
    public class CachedCookie
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public CookieOptions Options { get; set; }

        public bool IsDeleted { get; set; }
    }
}
