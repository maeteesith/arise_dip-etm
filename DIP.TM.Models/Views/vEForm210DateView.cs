﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class vEForm210DateView : BaseView
    {
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public DateTime? case_28_date { get; set; }
    }
}
