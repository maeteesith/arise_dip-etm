﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010DocumentClassificationWordView : BaseView {
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string word_mark { get; set; }
        public string word_first_code { get; set; }
        public string word_sound_last_code { get; set; }
        public string word_sound_last_other_code { get; set; }
        public string word_sound { get; set; }
        public string word_syllable_sound { get; set; }

        public string word_first_name { get { return word_first_codeNavigation == null ? word_first_code : word_first_codeNavigation.name; } }
        public string word_sound_last_name { get { return word_sound_last_codeNavigation == null ? word_sound_last_code : word_sound_last_codeNavigation.name; } }
        public string word_sound_last_other_name { get { return word_sound_last_other_codeNavigation == null ? word_sound_last_other_code : word_sound_last_other_codeNavigation.name; } }
        public ReferenceMasterView word_first_codeNavigation { get; set; }
        public ReferenceMasterView word_sound_last_codeNavigation { get; set; }
        public ReferenceMasterView word_sound_last_other_codeNavigation { get; set; }
    }
}
