﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class OppositionRequestView : BaseView
    {
        public string request_number { get; set; }
        public string owner_name { get; set; }
        public DateTime request_date { get; set; }
    }
}
