﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class PolicyView
    {
        public string name { get; set; }
        public List<WebPageView> web_page_list { get; set; }
        public PermissionCodeConstant permission { get; set; }
    }
}
