﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vCheckingSimilarRequestItemSubTypeReportView : BaseView {
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public string file_trademark_2d { get; set; }
        public string name { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
        public string request_item_sub_type_group_code_text { get; set; }
    }
}
