﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save040ProcessView : BaseSaveView {
        public string register_number { get; set; }
        public string save040_transfer_type_code { get; set; }
        public string save040_transfer_form_code { get; set; }

        public List<SaveAddressView> people_list { get; set; }
        public string people_name { get { return people_list != null && people_list.Count() > 0 ? string.Join(", ", people_list.Select(r => r.name)) : ""; } }
        public List<Save040AddressRepresentativeView> representative_list { get; set; }
        public SaveAddressView contact_address { get; set; }
        public List<SaveProductView> product_list { get; set; }
    }
}
