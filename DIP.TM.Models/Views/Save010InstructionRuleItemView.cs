﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010InstructionRuleItemView : BaseView {
        public long? intruction_rule_id { get; set; }
        public string instruction_rule_item_code { get; set; }
        public string instruction_rule_description { get; set; }
        public string value_01 { get; set; }
        public string value_02 { get; set; }
        public string value_03 { get; set; }
        public string value_04 { get; set; }
        public string value_05 { get; set; }

        public ReferenceMasterView instruction_rule_item_codeNavigation { get; set; }
        public string instruction_rule_item_name { get { return instruction_rule_item_codeNavigation != null ? instruction_rule_item_codeNavigation.name : ""; } }
    }
}
