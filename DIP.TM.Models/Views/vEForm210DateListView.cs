﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class vEForm210DateListView : BaseView
    {
        public bool is_search_success { get; set; } = false;
        public string alert_msg { get; set; } = string.Empty;

        public List<vEForm210DateView> eform_210_date_list { get; set; }
    }
}
