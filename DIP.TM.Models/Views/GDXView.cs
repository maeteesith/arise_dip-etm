﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class GDXView {
        public string id_card { get; set; }
        public string thai_name { get; set; }
        public string eng_name { get; set; }
        public string birth_date { get;set;}
        public string issue_date { get; set; }
        public string expire_date { get; set; }
        public string address { get; set; }
        public string return_status {get; set;}

    }
}
