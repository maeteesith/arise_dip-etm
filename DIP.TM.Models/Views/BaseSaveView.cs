﻿using DIP.TM.Models.Payloads;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    abstract public class BaseSaveView : SaveModel {

        public string request_date_text { get { return request_date.HasValue ? request_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string make_date_text { get { return make_date.HasValue ? make_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string save_status_name { get { return save_status_codeNavigation != null ? save_status_codeNavigation.name : ""; } }
        public ReferenceMasterView save_status_codeNavigation { get; set; }


        public string sound_file_physical_path { get; set; }
        public string trademark_status_code { get; set; }
        public string registration_number { get; set; }
        public DateTime? public_start_date { get; set; }
        public string book_index { get; set; }
        public SaveAddressView contact_address_01 { get; set; }
        public List<SaveAddressView> joiner_01_list { get; set; }
        public List<SaveAddressView> people_01_list { get; set; }
        public string people_01_name { get { return people_01_list != null && people_01_list.Count() > 0 ? string.Join(", ", people_01_list.Select(r => r.name)) : ""; } }
        public List<SaveAddressView> representative_01_list { get; set; }
        public List<SaveProductView> product_01_list { get; set; }
        public string request_item_sub_type_1_code_list_text { get { return product_01_list != null ? string.Join(", ", product_01_list.Select(r => r.request_item_sub_type_1_code)) : ""; } }

        public bool is_search_success { get; set; } = false;
        public string alert_msg { get; set; } = string.Empty;
        public string registration_load_number { get; set; }
        public string load_receiver_people_type_code { get; set; }
        public string inter_registration_load_number { get; set; }
        public DateTime? inter_registration_load_date { get; set; }
        public DateTime? inter_registration_cancel_load_date { get; set; }
        public DateTime? protection_load_date { get; set; }
        public DateTime? case_28_load_date { get; set; }

        public PublicRoundView public_round { get; set; }
        public Save050ProcessView save050 { get; set; }
        public List<SaveAddressView> people_load_list { get; set; }
        public List<SaveAddressView> representative_load_list { get; set; }
        public List<SaveProductView> product_load_list { get; set; }
    }
}

