﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save060ProcessView : BaseSaveView {
        public string register_number { get; set; }
        public string save060_representative_change_code { get; set; }
        public string save060_period_request_code { get; set; }
        public string save060_send_department_code { get; set; }
        public string modify_description { get; set; }
        public bool is_oppose_modify_description { get; set; }
        public string representative_change_code { get; set; }
        public string certification_mark_rule { get; set; }
        
        public List<Save060AllowContactView> allow_people_list { get; set; }
        public SaveAddressView contact_address { get; set; }
        public SaveAddressView contact_address_06 { get; set; }
        public List<SaveAddressView> joiner_list { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<Save060AddressRepresentativeView> representative_list { get; set; }
        public List<SaveProductView> allow_product_list { get; set; }
        public List<SaveProductView> product_list { get; set; }
    }
}
