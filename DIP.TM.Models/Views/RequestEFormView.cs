﻿using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Models.Views {
    public class RequestEFormView : BaseView {
        public DateTime? request_date { get; set; }
        public string requester_name { get; set; }
        public string telephone { get; set; }
        public bool? is_via_post { get; set; }
        public bool? is_wave_fee { get; set; }
        public bool? is_commercial_affairs_province { get; set; }
        public string commercial_affairs_province_code { get; set; }
        public int? item_count { get; set; }
        public decimal? total_price { get; set; }
        public string source_code { get; set; }

        public List<Request01ItemView> request01_item_list { set; get; }
    }
}
