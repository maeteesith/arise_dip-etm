﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class UM_PageMenuView : BaseView {
        public string name { get; set; }
        public List<UM_PageMenuSubView> UM_PageMenuSub { get; set; }
    }
}
