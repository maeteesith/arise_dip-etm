﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vLocationView : BaseView {
        public int index { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string postal_code { get; set; }
        public string postal_name { get; set; }
        public string district_code { get; set; }
        public string district_name { get; set; }
        public string province_code { get; set; }
        public string province_name { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
    }
}
