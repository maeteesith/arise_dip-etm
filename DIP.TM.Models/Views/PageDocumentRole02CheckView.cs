﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class PageDocumentRole02CheckView : BaseView {
        public Save010ProcessView view { set; get; }
        public List<vDocumentRole02View> instruction_rule_list { get; set; }
        public List<PostRoundAddressView> post_round_address_list { get; set; }
    }
}
