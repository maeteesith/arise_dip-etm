﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save030ProcessView : BaseSaveView {
        public string register_number { get; set; }
        public string index_1 { get; set; }
        public string index_2 { get; set; }
        public string save030_appeal_type_code { get; set; }
        public string save030_appeal_maker_type_code { get; set; }
        public string save030_appeal_reason_code { get; set; }
        public string section_index { get; set; }
        public string remark { get; set; }

        public ReferenceMasterView save030_appeal_maker_type_codeNavigation { get; set; }
        public ReferenceMasterView save030_appeal_type_code1 { get; set; }
        public ReferenceMasterView save030_appeal_type_codeNavigation { get; set; }

        public List<SaveAddressView> people_list { get; set; }
        public string people_name { get { return people_list != null && people_list.Count() > 0 ? string.Join(", ", people_list.Select(r => r.name)) : ""; } }
        public List<SaveAddressView> representative_list { get; set; }
        public SaveAddressView contact_address { get; set; }
    }
}
