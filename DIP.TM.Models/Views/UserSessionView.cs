﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class UserSessionView : BaseView {
        public string username { get; set; }
        public string name { get; set; }
        public string name_in_department { get; set; }
        public string email { get; set; }
        public DateTime? enable_start_date { get; set; }
        public DateTime? enable_end_date { get; set; }
        public bool is_enabled { get; set; }

        //public int? id { get; set; }
        //public UserView user { get; set; }
        public List<UserRoleView> user_role_list { get; set; }
        public string token { get; set; }
        public DateTime session_expired_datetime { get; set; }
    }
}
