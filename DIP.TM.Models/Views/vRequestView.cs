﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class vRequestView : BaseView
    {
        public DateTime? make_date { get; set; }
        public string name { get; set; }
    }
}
