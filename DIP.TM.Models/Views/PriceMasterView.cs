﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class PriceMasterView : BaseView
    {
        public long id { get; set; }
        public int price_type_id { get; set; }
        public int price_channel_id { get; set; }
        public int min_qty { get; set; }
        public int? max_qty { get; set; }
        public bool is_total_price { get; set; }
        public decimal price { get; set; }
    }
}
