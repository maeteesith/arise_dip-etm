﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010CheckingSimilarWordTranslateView : BaseView {
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string word_translate_search { get; set; }
        public string word_translate_dictionary_code { get; set; }
        public string word_translate_dictionary_other { get; set; }
        public string word_translate_sound { get; set; }
        public string word_translate_translate { get; set; }
        public string checking_word_translate_status_code { get; set; }

        public ReferenceMasterView checking_word_translate_status_codeNavigation { get; set; }
        public string checking_word_translate_status_name { get { return checking_word_translate_status_codeNavigation != null ? checking_word_translate_status_codeNavigation.name : ""; } }
    }
}
