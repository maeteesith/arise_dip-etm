﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010DocumentClassificationSearchMethodView : BaseView {
        public long? save_id { get; set; }
        public string search_method { get; set; }
        public string keyword_1 { get; set; }
        public string keyword_2 { get; set; }
    }
}
