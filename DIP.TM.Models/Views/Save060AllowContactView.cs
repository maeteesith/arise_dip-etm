﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save060AllowContactView : SaveAddressView {
        public string save060_allow_contact_code { get; set; }
    }
}
