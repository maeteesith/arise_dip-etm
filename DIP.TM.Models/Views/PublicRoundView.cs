﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class PublicRoundView : BaseView
    {
        public string book_index { get; set; }
        public DateTime? public_start_date { get; set; }
        public DateTime? public_end_date { get; set; }
        public string public_round_status_code { get; set; }

        public List<PublicRoundItemView> public_round_item { get; set; }
        public List<Save010ProcessView> save010_list { get; set; }
        public List<Save020ProcessView> save020_list { get; set; }
    }
}
