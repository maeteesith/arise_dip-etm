﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class TM2DView: FileView
    {
        public decimal fee { get; set; }
        public string description_1 { get; set; }
        public string description_2 { get; set; }
        public string description_3 { get; set; }
    }
}
