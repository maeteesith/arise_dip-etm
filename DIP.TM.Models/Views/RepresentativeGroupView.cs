﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class RepresentativeGroupView : BaseView
    {
        public string name { get; set; }
    }
}
