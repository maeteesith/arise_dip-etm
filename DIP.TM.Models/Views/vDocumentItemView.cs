﻿using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vDocumentItemView : vSave010View {
        public string document_classification_by_name { get; set; }
        public string request_type_value_3 { get; set; }
        public string document_type_code { get; set; }
        public string document_type_name { get; set; }
    }

}
