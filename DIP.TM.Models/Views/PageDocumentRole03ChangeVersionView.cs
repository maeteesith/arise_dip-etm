﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class PageDocumentRole03ChangeVersionView : BaseView {
        public Save010ProcessView view { set; get; }

        public List<ReferenceMasterView> allow_list { get; set; }
        //public List<vDocumentRole02View> instruction_rule_list { get; set; }
        //public List<vSave010CheckingSaveDocumentView> document_list { get; set; }
        //public List<vReceiptItemView> receipt_item_list { get; set; }
        public List<vSave010HistoryView> history_list { get; set; }
        //public List<vRequestDocumentCollectView> document_scan_list { get; set; }
        //public List<vDocumentRole04CheckView> document_role04_check_list { get; set; }

        public List<vDocumentRole03ChangeCertificationFileView> certification_file_list { get; set; }
        public List<vDocumentRole03ChangeRequestGroupView> request_group_list { get; set; }

        public vDocumentRole03ItemView document_role03_item { get; set; }
    }
}
