﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save060AddressRepresentativeView : SaveAddressView {
        public string save060_representative_type_code { get; set; }
        public ReferenceMasterView save060_representative_type_codeNavigation { get; set; }
    }
}
