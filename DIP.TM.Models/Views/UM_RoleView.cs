﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class UM_RoleView : BaseView {
        public string name { get; set; }
        public string description { get; set; }
    }
}
