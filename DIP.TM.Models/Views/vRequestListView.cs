﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class vRequestListView : BaseView
    {
        public List<vRequestView> request_load_list { get; set; }
    }
}
