﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class SaveInstructionRuleView : BaseView
    {
        public long? save_id { get; set; }
        public string request_type_code { get; set; }
        public string book_number { get; set; }
        public string topic { get; set; }
        public string recipient { get; set; }
        public DateTime? instruction_date { get; set; }
        public string instruction_rule_code { get; set; }
        public string rule_remark { get; set; }
        public string rule_description { get; set; }
        public string considering_instruction_status_code { get; set; }
        public string considering_book_status_code { get; set; }
        public DateTime? instruction_send_date { get; set; }
        public string instruction_number { get; set; }
        public string considering_instruction_rule_status_code { get; set; }
        public string document_role02_check_status_code { get; set; }
        public DateTime? document_role02_check_date { get; set; }
        public DateTime? change_status_date { get; set; }
        public string change_reason { get; set; }
        public string value_1 { get; set; }
        public string value_2 { get; set; }
        public string value_3 { get; set; }
        public string value_4 { get; set; }
        public string value_5 { get; set; }
        public long? post_round_id { get; set; }

        public string instruction_send_date_text { get { return instruction_send_date.HasValue ? instruction_send_date.Value.ToString("dd/MM/yyyy") : ""; } }

        public ReferenceMasterView considering_book_status_codeNavigation { get; set; }
        public string considering_book_status_name { get { return considering_book_status_codeNavigation != null ? considering_book_status_codeNavigation.name : ""; } }
        public ReferenceMasterView considering_instruction_rule_status_codeNavigation { get; set; }
        public string considering_instruction_rule_status_name { get { return considering_instruction_rule_status_codeNavigation != null ? considering_instruction_rule_status_codeNavigation.name : ""; } }
        public ReferenceMasterView considering_instruction_status_codeNavigation { get; set; }
        public string considering_instruction_status_name { get { return considering_instruction_status_codeNavigation != null ? considering_instruction_status_codeNavigation.name : ""; } }
        public ReferenceMasterView instruction_rule_codeNavigation { get; set; }
        public string instruction_rule_name { get { return instruction_rule_codeNavigation != null ? instruction_rule_codeNavigation.name : ""; } }
        public List<SaveInstructionRuleItemView> item_list { get; set; }

        public List<SaveInstructionRuleRequestNumberView> request_number_save_list { get; set; }
    }
}
