﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vOnlinePublicRoundMonthView : BaseView {
        public int? public_year { get; set; }
        public int? public_month { get; set; }
        public DateTime? public_start_date { get; set; }
        public int? item_count { get; set; }
    }
}
