﻿using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vSave010View : BaseSaveView {
        public long? sound_file_id { get; set; }
        public bool? is_convert_irn { get; set; }
        public string irn_reference_number { get; set; }
        public string request_item_type_code { get; set; }
        public string commercial_affairs_province_code { get; set; }
        public string save_otop_type_code { get; set; }
        public string evidence_address { get; set; }
        public string otop_reference_number { get; set; }
        public string request_mark_feature_code_list { get; set; }
        public DateTime? receive_date { get; set; }
        public string address_country_code { get; set; }
        public long? contact_address_id { get; set; }
        public string trademark_role { get; set; }
        public string sound_description { get; set; }
        public string sound_mark_list { get; set; }
        public DateTime? saved_process_date { get; set; }
        public string document_status_code { get; set; }
        public string document_status_name { get; set; }
        public long? document_spliter_by { get; set; }
        public long? document_classification_by { get; set; }
        public DateTime? document_classification_date { get; set; }
        public string document_classification_remark { get; set; }
        public string document_classification_status_code { get; set; }
        public string save010_document_classification_language_code { get; set; }
        public string checking_type_code { get; set; }
        public string checking_status_code { get; set; }
        public long? checking_spliter_by { get; set; }
        public long? checking_receiver_by { get; set; }
        public DateTime? checking_receive_date { get; set; }
        public string checking_receive_status_code { get; set; }
        public DateTime? checking_send_date { get; set; }
        public string checking_remark { get; set; }
        public long? considering_spliter_by { get; set; }
        public long? considering_receiver_by { get; set; }
        public string considering_receive_status_code { get; set; }
        public DateTime? considering_receive_date { get; set; }
        public string considering_receive_remark { get; set; }
        public DateTime? considering_send_date { get; set; }
        public string considering_reason_send_back { get; set; }
        public string considering_remark { get; set; }
        public string considering_similar_instruction_type_code { get; set; }
        public string considering_similar_evidence_27_code { get; set; }
        public string considering_similar_instruction_description { get; set; }
        public string public_type_code { get; set; }
        public string public_status_code { get; set; }
        public long? public_spliter_by { get; set; }
        public long? public_receiver_by { get; set; }
        public string public_receive_status_code { get; set; }
        public DateTime? public_receive_date { get; set; }
        public DateTime? public_send_date { get; set; }
        public string public_receive_do_status_code { get; set; }
        public long? public_round_id { get; set; }
        public string public_page_index { get; set; }
        public string public_line_index { get; set; }
        public string public_role02_check_status_code { get; set; }
        public long? public_role02_check_by { get; set; }
        public DateTime? public_role02_check_date { get; set; }
        public DateTime? public_role04_payment_date { get; set; }
        public string public_role04_status_code { get; set; }
        public DateTime? public_role04_date { get; set; }
        public long? public_role04_spliter_by { get; set; }
        public long? public_role04_receiver_by { get; set; }
        public string public_role04_receive_status_code { get; set; }
        public DateTime? public_role04_receive_date { get; set; }
        public long? public_role05_by { get; set; }
        public string public_role05_status_code { get; set; }
        public DateTime? public_role05_date { get; set; }
        public long? floor3_proposer_by { get; set; }
        public DateTime? floor3_proposer_date { get; set; }
        public long? floor3_registrar_by { get; set; }
        public string floor3_registrar_status_code { get; set; }
        public DateTime? floor3_registrar_date { get; set; }
        public string floor3_registrar_description { get; set; }
        public DateTime? trademark_expired_start_date { get; set; }
        public DateTime? trademark_expired_date { get; set; }
        public DateTime? trademark_expired_end_date { get; set; }
        public string public_role04_document_status_code { get; set; }
        public DateTime? public_role04_document_date { get; set; }
        public string public_role02_document_status_code { get; set; }
        public DateTime? public_role02_document_date { get; set; }


        /// <summary>
        /// ////////////
        /// </summary>
        public string request_source_name { get; set; }
        public DateTime? last_extend_date { get; set; }
        public string request_item_type_name { get; set; }
        public string request_item_sub_type_1_code { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
        public string request_item_sub_type_1_description { get; set; }
        public string name { get; set; }
        public string people_name { get; set; }
        public string people_information { get; set; }
        public string representative_name { get; set; }
        public string joiner_name { get; set; }
        public string file_save01 { get; set; }
        public string file_trademark_2d { get; set; }
        public string document_classification_status_name { get; set; }
        public string checking_type_name { get; set; }
        public string checking_status_name { get; set; }
        public string checking_spliter_by_name { get; set; }
        public string checking_receiver_by_name { get; set; }
        public string checking_receive_status_name { get; set; }
        public string considering_spliter_by_name { get; set; }
        public string considering_receiver_by_name { get; set; }
        public string considering_receive_status_name { get; set; }
        public int considering_similar_round_index { get; set; }
        public string considering_similar_instruction_type_name { get; set; }
        public string considering_similar_evidence_27_name { get; set; }
        public string department_send_name { get; set; }

        public string saved_process_date_text { get { return saved_process_date.HasValue ? saved_process_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string document_classification_date_text { get { return document_classification_date.HasValue ? document_classification_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string checking_receive_date_text { get { return checking_receive_date.HasValue ? checking_receive_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string checking_send_date_text { get { return checking_send_date.HasValue ? checking_send_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string considering_receive_date_text { get { return considering_receive_date.HasValue ? considering_receive_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string considering_send_date_text { get { return considering_send_date.HasValue ? considering_send_date.Value.ToString("yyyy-MM-dd") : ""; } }

        public Save010ProcessView full_view { get; set; }
        public List<vDocumentRole02View> document_role_02_list { set; get; }
    }

}
