﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class RepresentativeView : BaseView
    {
        public string name { get; set; }
        public RepresentativeGroupView representative_group { get; set; }
    }
}
