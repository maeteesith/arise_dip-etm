﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class ConsideringSimilarDocumentView : BaseView {
        public string request_number { get; set; }
        public List<vSave010CheckingInstructionDocumentView> instruction_rule_list { get; set; }
        public List<vSave010CheckingSaveDocumentView> save_list { get; set; }
    }
}
