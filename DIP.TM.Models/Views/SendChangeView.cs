﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class SendChangeView : BaseView {
        public string department_code { get; set; }
        public string reason { get; set; }
    }
}
