﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class ListView
    {
        public List<BaseView> object_list { get; set; }
        public PagingView paging { get; set; }
    }
}
