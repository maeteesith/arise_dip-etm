﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class PageDocumentRole04ReleaseRequestView : BaseView {
        public Save010ProcessView view { set; get; }
        //public List<vDocumentRole02View> instruction_rule_list { get; set; }
        public List<vDocumentRole04ReleaseRequestView> document_role04_release_request_list { get; set; }
    }
}
