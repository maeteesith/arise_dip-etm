﻿using DIP.TM.Models.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views.Appeal
{
    public class Appeal_Role02SaveCaseSummarySubcommitteeResulotionView : BaseView
    {
        public long? save_case_summary_id { get; set; }
        public string is_vote { get; set; }
        public long? score_vote { get; set; }
        public string reason_vote { get; set; }
    }
}
