﻿using DIP.TM.Models.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views.Appeal
{
   public class Appeal_Role02SaveCaseSummaryEvidenceView : BaseView
    {
        public long? save_case_summary_id { get; set; }
        public string evidence_subject { get; set; }
        public DateTime? return_date { get; set; }
        public string remark { get; set; }
    }
}
