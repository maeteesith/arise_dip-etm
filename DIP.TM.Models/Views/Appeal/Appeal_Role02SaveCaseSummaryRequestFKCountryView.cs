﻿using DIP.TM.Models.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views.Appeal
{
    public class Appeal_Role02SaveCaseSummaryRequestFKCountryView : BaseView
    {
        public long? save_case_summary_id { get; set; }
        public long? file_id { get; set; }
        public string request_number { get; set; }
        public string owner_name { get; set; }
        public DateTime? request_date { get; set; }
        public string genera { get; set; }
        public string item { get; set; }
    }
}
