﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views.Appeal
{
    public class vCaseSummaryView : BaseView
    {
        public long? request_id { get; set; }
        public string reuest_number { get; set; }
        public DateTime? request_date { get; set; }
        public string no_number { get; set; }
        public string requst_type_code { get; set; }
        public string request_type_name { get; set; }
        public string appeal_reason_code { get; set; }
        public string reason_name { get; set; }
        public string process_action_code { get; set; }
        public string process_action_name { get; set; }
        public string step_action_code { get; set; }
        public string step_action_name { get; set; }
        public string status_action_code { get; set; }
        public string status_action_name { get; set; }
        public DateTime? document_more { get; set; }
        public string decition_registrar { get; set; }
        public string summary_remark { get; set; }
        public string wait_committee { get; set; }
        public string meeting_update { get; set; }
        public bool? is_considering { get; set; }
    }
}
