﻿using DIP.TM.Models.Payloads.Appeal;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views.Appeal
{
   public class Appeal_Role02SaveCaseSummaryView : BaseView
    {
        public long? request_id { get; set; }
        public string reuest_number { get; set; }
        public DateTime? request_date { get; set; }
        public string requst_type_code { get; set; }
        public string appeal_reason_code { get; set; }
        public string process_action_code { get; set; }
        public string step_action_code { get; set; }
        public string status_action_code { get; set; }
        public DateTime? document_more { get; set; }
        public string decition_registrar { get; set; }
        public string summary_remark { get; set; }
        public string wait_committee { get; set; }
        public string meeting_update { get; set; }
        public bool? is_considering { get; set; }
        public List<Appeal_Role02SaveCaseSummaryEvidenceView> item_evidence { get; set; }
        public List<Appeal_Role02SaveCaseSummaryMarkReferentView> item_mark_referent { get; set; }
        public List<Appeal_Role02SaveCaseSummaryRequestFKCountryView> item_fk_country { get; set; }
        public List<Appeal_Role02SaveCaseSummaryRequestSimilarView> item_similar { get; set; }
        public List<Appeal_Role02SaveCaseSummarySectionView> item_section { get; set; }
        public List<Appeal_Role02SaveCaseSummarySubcommitteeProposalView> item_subcommittee_proposal { get; set; }
        public List<Appeal_Role02SaveCaseSummarySubcommitteeResulotionView> item_subcommittee_resulotion { get; set; }
    }
}
