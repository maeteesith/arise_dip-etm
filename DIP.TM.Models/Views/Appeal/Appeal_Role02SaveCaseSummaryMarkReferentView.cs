﻿using DIP.TM.Models.Views;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views.Appeal
{
    public class Appeal_Role02SaveCaseSummaryMarkReferentView : BaseView
    {
        public long? save_case_summary_id { get; set; }
        public bool? is_referent { get; set; }
        public string request_number { get; set; }
        public string other { get; set; }
    }
}
