﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class RS_AddressView : BaseView {
        public string object_type_code { get; set; }
        public long? object_id { get; set; }
        public string descrition { get; set; }
        public string address_type_code { get; set; }
        public string nationality_code { get; set; }
        public string career_code { get; set; }
        public string card_type_code { get; set; }
        public string card_number { get; set; }
        public string receiver_type_code { get; set; }
        public string name { get; set; }
        public string house_number { get; set; }
        public string village_number { get; set; }
        public string alley { get; set; }
        public string street { get; set; }
        public string address_sub_district_code { get; set; }
        public string address_district_code { get; set; }
        public string address_province_code { get; set; }
        public string postal_code { get; set; }
        public string address_country_code { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string sex_code { get; set; }
    }
}
