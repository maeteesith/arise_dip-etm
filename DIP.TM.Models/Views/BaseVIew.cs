﻿using System;
using System.Reflection;

namespace DIP.TM.Models.Views {
    public class BaseView {
        public long id { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public string created_date_text { get { return created_date.ToString("yyyy-MM-dd"); } }
        public string created_date_time_text { get { return created_date.ToString("yyyy-MM-dd HH:mm:ss"); } }
        public long created_by { get; set; }
        public string created_by_name { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
        public string updated_by_name { get; set; }
    }
}
