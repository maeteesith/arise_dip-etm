﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class ItemCalculatorView
    {
        public int qty { get; set; }
        public decimal total { get; set; }
    }
}
