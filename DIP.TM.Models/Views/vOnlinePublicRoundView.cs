﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vOnlinePublicRoundView : BaseView {
        public int? public_year { get; set; }
        public int? public_month { get; set; }
    }
}
