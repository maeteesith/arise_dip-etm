﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vSave010RequestGroupView : BaseView {
        public long? save_id { get; set; }
        public string request_number { get; set; }
        public string registration_number { get; set; }
        public string trademark_status_code { get; set; }
        public string name { get; set; }
        public string save010_registration_group_status_code { get; set; }
        public DateTime? make_date { get; set; }
        public DateTime? allow_date { get; set; }
    }
}
