﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class SaveProductView : BaseView {
        public long? save_id { get; set; }
        public long? change_ref_id { get; set; }
        public string request_item_sub_type_1_code { get; set; }
        public string request_item_sub_type_2_code { get; set; }
        public string request_item_sub_status_code { get; set; }
        public string cancel_reason { get; set; }
        public long? cancel_by { get; set; }
        public string cancel_by_name { get; set; }
        public string description { get; set; }
    }
}
