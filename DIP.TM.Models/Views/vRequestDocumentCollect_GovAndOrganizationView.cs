﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class vRequestDocumentCollect_GovAndOrganizationView : vRequestDocumentCollectAddModel
    {
        public string url_path { get { return file_id.HasValue ? "/File/Content/" + file_id : ""; } }
    }
}
