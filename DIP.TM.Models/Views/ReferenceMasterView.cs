﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class ReferenceMasterView : BaseView {
        public string parent_code { get; set; }
        public int index { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string value_1 { get; set; }
        public string value_2 { get; set; }
        public string value_3 { get; set; }
        public string value_4 { get; set; }
        public string value_5 { get; set; }
        public bool is_show { get; set; }

        //
        public bool is_check { get; set; }
    }
}
