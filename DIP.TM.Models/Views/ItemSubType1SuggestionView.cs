﻿using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class ItemSubType1SuggestionView : SaveProductView {
        public List<ReferenceMasterView> suggestion_list { set; get; }

        public long? save010_product_id { get; set; }
        public long? suggestion_item_id { get; set; }
        public long? item_id { get; set; }
        public string item_code { get; set; }
        public string item_name { get; set; }
    }
}
