﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class UM_PageMenuSubView : BaseView {
        public long menu_id { get; set; }
        public string name { get; set; }
        public List<UM_PageView> UM_Page { get; set; }
    }
}
