﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010DocumentClassificationVersionView : Save010DocumentClassificationVersionAddModel {
        public List<Save010DocumentClassificationWordView> document_classification_word_list { get; set; }
        public List<Save010DocumentClassificationImageView> document_classification_image_list { get; set; }
        public List<Save010DocumentClassificationSoundView> document_classification_sound_list { get; set; }
    }
}
