﻿using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vUM_UserView : BaseView {
        public string name { get; set; }
        public long? department_id { get; set; }
        public string department_name { get; set; }
    }
}
