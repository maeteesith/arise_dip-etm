﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vDashboardDocumentRole01View : BaseView {
        public int? all { get; set; }
        public int? all_wait_documenting { get; set; }
        public int? all_received { get; set; }
        public int? save { get; set; }
        public int? save_wait_do { get; set; }
        public int? save_wait_save { get; set; }
        public int? save_send { get; set; }
        public int? change { get; set; }
        public int? change_wait_do { get; set; }
        public int? change_wait_send { get; set; }
        public int? change_send { get; set; }
        public int? collect { get; set; }
    }
}
