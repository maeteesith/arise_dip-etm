﻿using DIP.TM.Models.Payloads;
using System.Collections.Generic;

namespace DIP.TM.Models.Views {
    public class RequestEForm01ItemView : BaseView {
        public long request01_id { get; set; }
        public long eform_id { get; set; }
        public int? item_index { get; set; }
        public string item_type_code { get; set; }
        public string income_type { get; set; }
        public string income_description { get; set; }
        public long? sound_file_id { get; set; }
        public string sound_type_code { get; set; }
        public string sound_description { get; set; }
        public string facilitation_act_status_code { get; set; }
        public string request_number { get; set; }
        public decimal? size_over_cm { get; set; }
        public decimal? size_over_price { get; set; }
        public string size_over_income_type { get; set; }
        public string size_over_income_description { get; set; }
        public string request_evidence_list { get; set; }
        public decimal? total_price { get; set; }
        public string book_index { get; set; }
        public string page_index { get; set; }
        public bool? is_otop { get; set; }
        public string reference_number { get; set; }
        public string cancel_reason { get; set; }
        public string status_code { get; set; }

        public List<RequestEForm01ItemSubView> request01_item_sub_list { set; get; }
    }
}
