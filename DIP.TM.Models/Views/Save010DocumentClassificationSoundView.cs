﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010DocumentClassificationSoundView : BaseView {
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string document_classification_sound_code { get; set; }
        public ReferenceMasterView document_classification_sound_codeNavigation { get; set; }
        public string document_classification_sound_name { get { return document_classification_sound_codeNavigation != null ? document_classification_sound_codeNavigation.name : ""; } }
    }
}
