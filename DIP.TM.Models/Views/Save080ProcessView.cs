﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save080ProcessView : BaseSaveView {
        public string register_number { get; set; }
        public string save080_revoke_type_code { get; set; }
        public string save080_revoke_type_name { get { return save080_revoke_type_codeNavigation != null ? save080_revoke_type_codeNavigation.name : ""; } }
        public string remark { get; set; }
        public string index_1 { get; set; }
        public string index_2 { get; set; }

        public ReferenceMasterView save080_revoke_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<Save080AddressRepresentativeView> representative_list { get; set; }
        public SaveAddressView contact_address { get; set; }
        public List<SaveAddressView> joiner_list { get; set; }
        public List<Save080AddressRepresentativeView> joiner_representative_list { get; set; }
    }
}
