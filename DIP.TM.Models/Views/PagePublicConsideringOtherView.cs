﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class PagePublicOtherView : BaseView {
        public Save010ProcessView view { set; get; }
        public List<vDocumentRole02View> instruction_rule_list { get; set; }
        public List<vSave010CheckingSaveDocumentView> document_list { get; set; }
        public List<vReceiptItemView> receipt_item_list { get; set; }
        public List<vSave010HistoryView> history_list { get; set; }
        public List<vRequestDocumentCollectView> document_scan_list { get; set; }

        public vPublicOtherView public_other { get; set; }
        //public vPublicRole05Other20View document_role04_release_20 { get; set; }
    }
}
