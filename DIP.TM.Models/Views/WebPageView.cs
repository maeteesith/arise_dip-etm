﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class WebPageView
    {
        public string name { get; set; }
        public string url { get; set; }
    }
}
