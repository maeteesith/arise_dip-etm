﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Request01ReceiptChangeItemView : BaseView {
        public long? request01_receipt_change_id { get; set; }
        public string column_name { get; set; }
        public string value_1 { get; set; }
        public string value_2 { get; set; }
    }
}
