﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Request01ProcessView : BaseView {
        public DateTime? request_date { get; set; }
        public string requester_name { get; set; }
        public string telephone { get; set; }
        public bool? is_via_post { get; set; }
        public bool? is_wave_fee { get; set; }
        public bool? is_commercial_affairs_province { get; set; }
        public string commercial_affairs_province_code { get; set; }
        public int? item_count { get; set; }
        public decimal? total_price { get; set; }
        public string source_code { get; set; }
        public List<Request01ItemView> item_list { get; set; }

        //
        public string request_date_text { get { return request_date.Value.ToString("yyyy-MM-dd"); } }
        //For Report

        // TODO
        public string reference_number { get { return item_list==null ? "" : item_list.FirstOrDefault().reference_number; } }

        // TODO
        public string barcode { get { return item_list == null ? "" : BarcodeHelper.Generate(item_list.FirstOrDefault().reference_number); } }
    }
}
