﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class RequestOtherRequest01ItemView : BaseView {
        public long request_other_process_id { get; set; }
        public string request_number { get; set; }
        public double? total_price { get; set; }
        public string reference_number { get; set; }
        public string name { get; set; }
        public string facilitation_act_status_code { get; set; }
        public string remark { get; set; }
        public string book_index { get; set; }
        public string page_index { get; set; }
        public string status_code { get; set; }
        public ReferenceMasterView status_codeNavigation { get; set; }



        public long save_id { get; set; }
        public string registration_number { get; set; }
        public DateTime? trademark_expired_start_date { get; set; }
        public DateTime? trademark_expired_date { get; set; }
        public DateTime? trademark_expired_end_date { get; set; }
        public List<SaveProductView> product_list { get; set; }
        public string barcode { get { return reference_number == null ? "" : BarcodeHelper.Generate(reference_number); } }
    }
}
