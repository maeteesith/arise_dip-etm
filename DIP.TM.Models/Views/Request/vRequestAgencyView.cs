﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vRequestAgencyView : BaseView {
        public string name { get; set; }
        public bool is_wave_fee { get; set; }
        public long agency_group_id { get; set; }
        public string agency_group_name { get; set; }
    }
}
