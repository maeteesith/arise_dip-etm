﻿using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Views
{
    public class Request01ReceiptChangeView : BaseView {
        public long? request_id { get; set; }
        public string request01_receipt_change_code { get; set; }
        public string request01_receipt_change_name { get; set; }
        public DateTime? request_approve_date { get; set; }
        public long? request_approve_by { get; set; }
        public string request_approve_by_name { get; set; }
        public DateTime? receipt_approve_date { get; set; }
        public long? receipt_approve_by { get; set; }
        public string receipt_approve_by_name { get; set; }

        public Request01ProcessView before { set; get; }
        public Request01ProcessView after { set; get; }

        public List<Request01ReceiptChangeItemView> item_list { set; get; }
    }
}
