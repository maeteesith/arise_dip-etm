﻿using System;

namespace DIP.TM.Models.Views {
    public class vRequestOtherItemView : BaseView {
        public long request_other_process_id { get; set; }
        public string request_index { get; set; }
        public long? eform_id { get; set; }
        public DateTime? request_date { get; set; }
        public string request_number { get; set; }
        public string requester_name { get; set; }
        public string request_type_code { get; set; }
        public int? item_sub_type_1_count { get; set; }
        public decimal? total_price { get; set; }
        public string name { get; set; }
        public DateTime created_date { get; set; }
        public string reference_number { get; set; }
        public string status_code { get; set; }
        public string status_name { get; set; }
        public string cancel_reason { get; set; }
        public string receipt_status_code { get; set; }
        public string receipt_status_name { get; set; }
    }
}
