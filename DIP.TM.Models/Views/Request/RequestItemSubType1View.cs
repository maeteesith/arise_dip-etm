﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class RequestItemSubType1View : BaseView {
        public string code { get; set; }
        public string name { get; set; }
    }
}
