﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class RequestSoundTypeView : BaseView
    {
        public long id { get; set; }
        public string code { get; set; }
        public string name { get; set; }
    }
}
