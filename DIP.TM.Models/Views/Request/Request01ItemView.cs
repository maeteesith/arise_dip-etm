﻿using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Models.Views {
    public class Request01ItemView : BaseView {
        public long request01_id { get; set; }
        public long? eform_id { get; set; }
        public int? item_index { get; set; }
        public string item_type_code { get; set; }
        public string item_type_name { get { return item_type_codeNavigation != null ? item_type_codeNavigation.name : ""; } }
        public ReferenceMasterView item_type_codeNavigation { get; set; }
        public string income_type { get; set; }
        public string income_description { get; set; }
        public long? sound_file_id { get; set; }
        public long? sound_file_size { get { return sound_file_ != null ? sound_file_.file_size : 0; } }
        public string? sound_file_name { get { return sound_file_ != null ? sound_file_.file_name : ""; } }
        public List<Request01ItemSubView> item_sub_list { get; set; }
        public string sound_type_code { get; set; }
        public string sound_type_name { get { return sound_type_codeNavigation != null ? sound_type_codeNavigation.name : ""; } }
        public ReferenceMasterView sound_type_codeNavigation { get; set; }
        public string sound_description { get; set; }
        public string facilitation_act_status_code { get; set; }
        public string facilitation_act_status_name { get { return facilitation_act_status_codeNavigation != null ? facilitation_act_status_codeNavigation.name : ""; } }
        public ReferenceMasterView facilitation_act_status_codeNavigation { get; set; }
        public string request_number { get; set; }
        public decimal? size_over_cm { get; set; }
        public decimal? size_over_price { get; set; }
        public string size_over_income_type { get; set; }
        public string size_over_income_description { get; set; }
        public string request_evidence_list { get; set; }
        public decimal? total_price { get; set; }
        public string book_index { get; set; }
        public string page_index { get; set; }
        public bool? is_otop { get; set; }
        public string reference_number { get; set; }
        public string cancel_reason { get; set; }
        public string status_code { get; set; }

        public FileView sound_file_ { get; set; }

        public long request_01_item_sub_count { get { return item_sub_list != null ? item_sub_list.Select(r => r.item_sub_type_1_code).Distinct().Count() : 0; } }
        public long request_01_product_count { get { return item_sub_list.Count(); } }
    }
}
