﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class RequestOtherProcessView : BaseView {
        public DateTime? request_date { get; set; }
        public string request_date_text { get { return request_date.HasValue ? request_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string requester_name { get; set; }
        public string telephone { get; set; }
        public bool? is_wave_fee { get; set; }
        public bool? is_via_post { get; set; }
        public bool? is_commercial_affairs_province { get; set; }
        public string commercial_affairs_province_code { get; set; }
        public double? total_price { get; set; }
        public string source_code { get; set; }
        public List<RequestOtherRequest01ItemView> request_01_item_list { get; set; }
        public List<RequestOtherItemView> item_list { get; set; }
        public FileView receipt { set; get; }

        public List<vEForm_eForm_SaveOtherView> item_eform_list { get; set; }
    }
}
