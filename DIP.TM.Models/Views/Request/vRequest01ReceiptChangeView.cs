﻿using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Views {
    public class vRequest01ReceiptChangeView : BaseView {
        public long? request_id { get; set; }
        public string request01_receipt_change_code { get; set; }
        public DateTime? request_approve_date { get; set; }
        public long? request_approve__by { get; set; }
        public DateTime? receipt_approve_date { get; set; }
        public long? receipt_approve__by { get; set; }
    }
}
