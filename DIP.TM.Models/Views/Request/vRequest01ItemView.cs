﻿using System;
using System.Collections.Generic;

namespace DIP.TM.Models.Views {
    public class vRequest01ItemView : BaseView {
        public long request01_id { get; set; }
        public long? eform_id { get; set; }
        public long? save_id { get; set; }
        public string name { get; set; }
        public string registration_number { get; set; }
        public DateTime? trademark_expired_start_date { get; set; }
        public DateTime? trademark_expired_date { get; set; }
        public DateTime? trademark_expired_end_date { get; set; }
        public DateTime? last_extend_date { get; set; }
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public string requester_name { get; set; }
        public string income_type { get; set; }
        public decimal? total_price { get; set; }
        public int? item_sub_type_1_count { get; set; }
        public int? product_count { get; set; }
        public decimal? size_over_cm { get; set; }
        public string reference_number { get; set; }
        public string status_code { get; set; }
        public string status_name { get; set; }
        public string receipt_number { get; set; }
        public string receipt_status_code { get; set; }
        public string receipt_status_name { get; set; }
        public string cancel_reason { get; set; }


        public List<SaveProductView> product_list { get; set; }
    }
}
