﻿using System.Collections.Generic;

namespace DIP.TM.Models.Views {
    public class RequestOtherItemView : BaseView {
        public long request_other_process_id { get; set; }
        public int? item_index { get; set; }
        public string request_type_code { get; set; }
        public int? item_count { get; set; }
        public int? item_sub_type_1_count { get; set; }
        public int? product_count { get; set; }
        public double? total_price { get; set; }
        public string book_index { get; set; }
        public string page_index { get; set; }
        public double? fine { get; set; }
        public bool? is_evidence_floor7 { get; set; }
        public bool? is_sue { get; set; }
        public string is_sue_text { get; set; }
        public ReferenceMasterView request_type_codeNavigation { get; set; }

        public string request_type_name { get { return request_type_codeNavigation == null ? "" : request_type_codeNavigation.name; } }
        public List<RequestOtherItemSubView> RequestOtherItemSub { get; set; }
    }
}
