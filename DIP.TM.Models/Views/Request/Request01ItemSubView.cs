﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Request01ItemSubView : BaseView {
        public long request01_item_id { get; set; }
        public int? item_sub_index { get; set; }
        public string item_sub_type_1_code { get; set; }
        public string item_sub_type_1_name { get { return item_sub_type_1_codeNavigation != null ? item_sub_type_1_codeNavigation.name : ""; } }
        public ReferenceMasterView item_sub_type_1_codeNavigation { get; set; }
        public int? product_count { get; set; }
        public decimal? total_price { get; set; }
    }
}
