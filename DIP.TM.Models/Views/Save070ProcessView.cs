﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save070ProcessView : BaseSaveView {
        public bool is_split_request { get; set; }
        public string register_number { get; set; }
        public double? fine { get; set; }
        public string split_request_number { get; set; }
        public SaveAddressView contact_address { get; set; }
        public List<SaveProductView> product_list { get; set; }
    }
}
