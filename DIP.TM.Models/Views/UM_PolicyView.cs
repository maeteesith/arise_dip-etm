﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class UM_PolicyView : BaseView {
        public string code { get; set; }
        public string name { get; set; }
    }
}
