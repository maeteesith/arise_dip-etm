﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vOnlinePublicRoundItemView : BaseView {
        public long pr_id { get; set; }
        public string request_item_sub_type_1_code { get; set; }
        public long? line_index { get; set; }
        public string request_number { get; set; }
        public DateTime? public_start_date { get; set; }
        public DateTime? public_end_date { get; set; }
        public long? trademark_2d_file_id { get; set; }
        public long? sound_mark_file_id { get; set; }
        public string sound_mark_list { get; set; }
        public string request_item_type_code { get; set; }
        public long? save_id { get; set; }
        public string request_item_sub_status_code { get; set; }
        public long? cancel_by { get; set; }
        public string cancel_by_name { get; set; }
        public string cancel_reason { get; set; }
    }
}
