﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vPublicRequestItemView : vPublicRequestItemAddModel {
        public long? public_round_id { get; set; }
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public string book_index { get; set; }
        public DateTime? public_end_date { get; set; }
        public DateTime? public_role02_check_date { get; set; }
        public string address_country_code { get; set; }
        public string address_country_name { get; set; }
        public string public_receive_do_status_code { get; set; }
        public string public_receive_do_status_name { get; set; }
        public string public_round_status_code { get; set; }
        public string public_round_status_name { get; set; }
        public string public_role02_check_status_code { get; set; }
        public string public_role02_check_status_name { get; set; }
        public string request_item_type_code { get; set; }
        public string request_item_type_name { get; set; }
        public string sound_mark_list { get; set; }
        public string public_page_index { get; set; }
        public string public_line_index { get; set; }
        public string public_page_index_line_index { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
        public string public_role2_check_remark { get; set; }
    }
}
