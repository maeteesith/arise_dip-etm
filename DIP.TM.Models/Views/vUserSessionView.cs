﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class vUserSessionView : UserSessionView {
        public string department_code { get; set; }
        public string department_name { get; set; }

        public List<UM_RoleView> UM_Role { get; set; }
        public List<UM_PageMenuView> UM_PageMenu { get; set; }
    }
}
