﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vDashboardConsideringView : BaseView {
        public int? round_1_wait { get; set; }
        public int? round_1_done { get; set; }
        public int? round_2_wait { get; set; }
        public int? round_2_done { get; set; }
    }
}
