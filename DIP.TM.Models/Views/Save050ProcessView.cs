﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save050ProcessView : BaseSaveView {
        public string register_number { get; set; }
        public string contract_number { get; set; }
        public DateTime? contract_date { get; set; }
        public bool? is_allow_contract { get; set; }
        public bool? is_extend_contract { get; set; }
        public string request_item_type_code { get; set; }
        public string save050_receiver_people_type_code { get; set; }
        public string save050_people_type_code { get; set; }

        public List<SaveAddressView> people_list { get; set; }
        public string people_name { get { return people_list != null && people_list.Count() > 0 ? string.Join(", ", people_list.Select(r => r.name)) : ""; } }
        public List<Save050AddressRepresentativeView> representative_list { get; set; }
        public SaveAddressView contact_address { get; set; }
        public List<SaveAddressView> receiver_people_list { get; set; }
        public List<SaveAddressView> receiver_representative_list { get; set; }
        public List<SaveProductView> product_list { get; set; }
    }
}
