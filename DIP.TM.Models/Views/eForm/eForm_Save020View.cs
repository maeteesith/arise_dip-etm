﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save020View : EFormView
    {
        //step 3
        public string save020_representative_condition_type_code { get; set; }
        //step 4
        public string save020_contact_type_code { get; set; }
        //step 5
        public long? evidence_file_id { get; set; }
        //step 6
        public bool? is_6_1 { get; set; }
        public bool? is_6_2 { get; set; }
        public bool? is_6_3 { get; set; }
        public bool? is_6_4 { get; set; }
        public bool? is_6_5 { get; set; }
        public bool? is_6_6 { get; set; }
        //step 8
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public FileView evidence_file { get; set; }
        public ReferenceMasterView save020_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save020_contact_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<SaveAddressView> contact_address_list { get; set; }
    }
}
