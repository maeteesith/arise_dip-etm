﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class eForm_Save060View : EFormView {
        public string challenger_number { get; set; }
        public string rule_number { get; set; }
        public string save060_period_request_code { get; set; }
        public string save060_search_type_code { get; set; }
        public bool? is_menu_1 { get; set; }
        public bool? is_menu_2 { get; set; }
        public bool? is_menu_3 { get; set; }
        public bool? is_menu_4 { get; set; }
        public bool? is_menu_4_1 { get; set; }
        public bool? is_menu_4_2 { get; set; }
        public bool? is_menu_5 { get; set; }
        public bool? is_menu_6 { get; set; }
        public bool? is_menu_7 { get; set; }
        public bool? is_menu_8 { get; set; }
        public bool? is_menu_9 { get; set; }
        public bool? is_menu_10 { get; set; }
        public bool? is_menu_11 { get; set; }
        public bool? is_menu_12 { get; set; }
        public bool? is_menu_12_1 { get; set; }
        public bool? is_menu_12_2 { get; set; }
        public bool? is_menu_12_3 { get; set; }
        public bool? is_menu_12_4 { get; set; }
        public bool? is_menu_12_5 { get; set; }
        public bool? is_menu_12_6 { get; set; }
        public bool? is_menu_12_7 { get; set; }
        public bool? is_menu_12_8 { get; set; }
        public bool? is_menu_12_9 { get; set; }
        public bool? is_menu_12_10 { get; set; }
        public bool? is_menu_12_11 { get; set; }
        public bool? is_menu_12_12 { get; set; }
        public long? contract_detail_ref_id { get; set; }
        public string save060_change_type_code { get; set; }
        public string save060_representative_change_code { get; set; }
        public string save060_representative_condition_type_code { get; set; }
        public string save060_receiver_representative_change_code { get; set; }
        public string save060_receiver_representative_condition_type_code { get; set; }
        public string save060_contract_receiver_type_code { get; set; }
        public string save060_contract_receiver_representative_change_code { get; set; }
        public string save060_contract_receiver_representative_condition_type_code { get; set; }
        public string save060_detail_receiver_type_code { get; set; }
        public DateTime? contract_start_date { get; set; }
        public bool? is_contract { get; set; }
        public DateTime? contract_end_date { get; set; }
        public DateTime? contract_date { get; set; }
        public string remark_8_2 { get; set; }
        public bool? is_people_rights { get; set; }
        public bool? is_people_authorize { get; set; }
        public bool? is_receiver_transfer { get; set; }
        public bool? is_receiver_authorize { get; set; }
        public string save060_legacy_representative_change_code { get; set; }
        public string save060_legacy_representative_condition_type_code { get; set; }
        public string save060_img_type_type_code { get; set; }
        public long? img_file_2d_id { get; set; }
        public long? img_file_3d_id_1 { get; set; }
        public long? img_file_3d_id_2 { get; set; }
        public long? img_file_3d_id_3 { get; set; }
        public long? img_file_3d_id_4 { get; set; }
        public long? img_file_3d_id_5 { get; set; }
        public long? img_file_3d_id_6 { get; set; }
        public int? img_w { get; set; }
        public int? img_h { get; set; }
        public string remark_5_1_3 { get; set; }
        public string remark_8 { get; set; }
        public string remark_9 { get; set; }
        public bool? is_sound_mark_human { get; set; }
        public bool? is_sound_mark_animal { get; set; }
        public bool? is_sound_mark_sound { get; set; }
        public bool? is_sound_mark_other { get; set; }
        public long? sound_file_id { get; set; }
        public long? sound_jpg_file_id { get; set; }
        public string remark_5_2_2 { get; set; }
        public string save060_contact_type_code { get; set; }
        public long? rule_file_id { get; set; }
        public string people_kor4_number { get; set; }
        public string save060_transfer_representative_change_code { get; set; }
        public string save060_transfer_representative_condition_type_code { get; set; }
        public string receiver_kor4_number { get; set; }
        public string save060_transfer_receiver_representative_change_code { get; set; }
        public string save060_transfer_receiver_representative_condition_type_code { get; set; }
        public string save060_challenger_representative_change_code { get; set; }
        public string save060_challenger_representative_period_change_type_code { get; set; }
        public string save060_challenger_representative_condition_type_code { get; set; }
        public string save060_challenger_representative_period_condition_type_code { get; set; }
        public string save060_challenger_contact_type_code { get; set; }
        public bool? is_1 { get; set; }
        public bool? is_2 { get; set; }
        public bool? is_3 { get; set; }
        public bool? is_4 { get; set; }
        public bool? is_5 { get; set; }
        public bool? is_6 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public FileView img_2D_file { get; set; }
        public FileView img_3D_file_1 { get; set; }
        public FileView img_3D_file_2 { get; set; }
        public FileView img_3D_file_3 { get; set; }
        public FileView img_3D_file_4 { get; set; }
        public FileView img_3D_file_5 { get; set; }
        public FileView img_3D_file_6 { get; set; }
        public FileView sound_file { get; set; }
        public FileView sound_jpg_file { get; set; }
        public ReferenceMasterView save060_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save060_challenger_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save060_contract_receiver_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save060_legacy_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save060_receiver_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save060_transfer_receiver_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save060_transfer_representative_condition_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<SaveAddressView> receiver_people_list { get; set; }//05
        public List<SaveAddressView> receiver_representative_list { get; set; }//05
        public List<SaveAddressView> contract_receiver_people_list { get; set; }//05
        public List<SaveAddressView> contract_receiver_representative_list { get; set; }//05
        public List<Save050ProcessView> save050_list { get; set; }
        public List<SaveAddressView> legacy_people_list { get; set; }
        public List<SaveAddressView> legacy_representative_list { get; set; }
        public List<SaveAddressView> transfer_people_list { get; set; }
        public List<SaveAddressView> transfer_representative_list { get; set; }
        public List<SaveAddressView> transfer_receiver_people_list { get; set; }
        public List<SaveAddressView> transfer_receiver_representative_list { get; set; }
        public List<SaveAddressView> challenger_people_list { get; set; }
        public List<SaveAddressView> challenger_representative_list { get; set; }
        public List<SaveAddressView> contact_address_list { get; set; }
        public List<SaveAddressView> challenger_contact_address_list { get; set; }
        public List<eForm_Save060CheckingSimilarWordTranslateView> checking_similar_translate_list { get; set; }
        public List<SaveProductView> product_list { get; set; }


        //
        public List<SaveAddressView> people_change_list { get; set; }
    }
}
