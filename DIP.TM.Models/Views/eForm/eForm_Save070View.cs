﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class eForm_Save070View : EFormView {
        public string save070_submit_type_code { get; set; }
        public string save070_request_item_type_code { get; set; }
        public string save070_representative_condition_type_code { get; set; }
        public string save070_contact_type_code { get; set; }
        public string save070_extend_type_code { get; set; }
        public double? fee { get; set; }
        public double? fine { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }


        public ReferenceMasterView save070_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save070_request_item_type_codeNavigation { get; set; }
        public ReferenceMasterView save070_extend_type_codeNavigation { get; set; }
        public ReferenceMasterView save070_contact_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<SaveAddressView> contact_address_list { get; set; }

        public List<SaveProductView> product_list { get; set; }
    }
}
