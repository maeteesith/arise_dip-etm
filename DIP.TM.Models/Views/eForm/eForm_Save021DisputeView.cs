﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save021DisputeView : BaseView
    {
        public long save_id { get; set; }
        public long challenge_id { get; set; }
        public string challenge_number { get; set; }

        public Save020ProcessView challenge { get; set; }
    }
}
