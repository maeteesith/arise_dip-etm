﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save200RequestRefView : BaseView
    {
        public long? save_id { get; set; }
        public string rule_number { get; set; }
        public string save200_request_type_code { get; set; }
        public long? request_ref_id { get; set; }

        public ReferenceMasterView save200_request_type_codeNavigation { get; set; }
    }
}
