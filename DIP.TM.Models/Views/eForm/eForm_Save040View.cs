﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class eForm_Save040View : EFormView {
        //step2 not show in ui
        public string contract_ref_number { get; set; }
        public string save040_transfer_type_code { get; set; }
        //step3
        public string save040_submit_type_code { get; set; }
        public string rule_number { get; set; }
        //step4
        public string save040_representative_condition_type_code { get; set; }
        //step5
        public string save040_contact_type_code { get; set; }
        //step6
        public string save040_receiver_representative_condition_type_code { get; set; }
        //step7
        public string save040_receiver_contact_type_code { get; set; }
        //step8
        public string save040_transfer_form_code { get; set; }
        public string save040_transfer_part_code { get; set; }
        //step9
        public string save040_transfer_request_type_code { get; set; }
        public bool? is_9_1 { get; set; }
        public bool? is_9_2 { get; set; }
        public bool? is_9_3 { get; set; }
        public bool? is_9_4 { get; set; }
        public bool? is_9_5 { get; set; }
        public bool? is_9_6 { get; set; }
        public bool? is_9_7 { get; set; }
        public bool? is_9_8 { get; set; }
        public bool? is_9_9 { get; set; }
        public bool? is_9_10 { get; set; }
        //step11
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string sign_inform_receiver_person_list { get; set; }
        public string sign_inform_receiver_representative_list { get; set; }


        public ReferenceMasterView save040_receiver_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save040_representative_condition_type_codeNavigation { get; set; }
        public ReferenceMasterView save040_submit_type_codeNavigation { get; set; }
        public ReferenceMasterView save040_transfer_form_codeNavigation { get; set; }
        public ReferenceMasterView save040_transfer_type_codeNavigation { get; set; }
        public ReferenceMasterView save040_transfer_part_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<SaveAddressView> contact_address_list { get; set; }
        public List<SaveAddressView> receiver_people_list { get; set; }
        public List<SaveAddressView> receiver_representative_list { get; set; }
        public List<SaveAddressView> receiver_contact_address_list { get; set; }
        public List<SaveProductView> product_list { get; set; }
    }
}
