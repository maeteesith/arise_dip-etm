﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save010Kor10EventView : BaseView
    {
        public long? save_id { get; set; }
        public string product { get; set; }
        public DateTime? event_date { get; set; }
        public string event_place { get; set; }
        public string event_organizer { get; set; }
    }
}
