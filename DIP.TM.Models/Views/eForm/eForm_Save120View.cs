﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class eForm_Save120View : EFormView {
        //step2
        public string save120_submit_type_code { get; set; }
        public string rule_number { get; set; }
        //step4
        public string save120_representative_condition_type_code { get; set; }
        //step5
        public string remark_5 { get; set; }
        //step6
        public string remark_6 { get; set; }
        //step7
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public ReferenceMasterView save120_representative_condition_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
    }
}
