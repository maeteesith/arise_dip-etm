﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class EFormView : EFormModel {
        public bool is_search_success { get; set; } = false;
        public string alert_msg { get; set; } = string.Empty;

        public string load_representative_condition_type_code { get; set; }
        public string load_contact_type_code { get; set; }
        public string request_date_text { get; set; }
        public string registration_load_number { get; set; }
        public string load_receiver_people_type_code { get; set; }
        public string inter_registration_load_number { get; set; }
        public DateTime? inter_registration_load_date { get; set; }
        public DateTime? inter_registration_cancel_load_date { get; set; }
        public DateTime? protection_load_date { get; set; }
        public DateTime? case_28_load_date { get; set; }


        public PublicRoundView public_round { get; set; }
        public Save010ProcessView save010 { get; set; }
        public Save050ProcessView save050 { get; set; }
        public List<SaveAddressView> people_load_list { get; set; }
        public List<SaveAddressView> representative_load_list { get; set; }
        public List<SaveAddressView> contact_address_load_list { get; set; }
        public List<SaveProductView> product_load_list { get; set; }
    }
}
