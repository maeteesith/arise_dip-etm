﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save200View : EFormView
    {
        //step3
        public string save200_recipient_code { get; set; }
        public bool? is_instruction { get; set; }
        public string request_name { get; set; }

        //step4
        public string remark_5 { get; set; }
        //step5
        public string remark_6 { get; set; }
        //step6
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }


        public ReferenceMasterView save200_recipient_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<eForm_Save200RequestRefView> request_ref_list { get; set; }
    }
}
