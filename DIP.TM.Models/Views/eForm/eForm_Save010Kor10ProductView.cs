﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save010Kor10ProductView : BaseView
    {
        public long? save_id { get; set; }
        public string request_number { get; set; }
        public DateTime? request_date { get; set; }
        public string request_country { get; set; }
        public string request_nationality { get; set; }
        public string request_domicile { get; set; }
        public string product_class { get; set; }
        public string product { get; set; }
        public string status { get; set; }
    }
}
