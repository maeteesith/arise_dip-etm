﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save150View : EFormView
    {
        //step2
        public string save150_search_type_code { get; set; }
        public string contract_ref_number { get; set; }
        public string save150_informer_type_code { get; set; }
        //step3
        public string save150_mark_type_code { get; set; }
        //step4
        public string save150_representative_condition_type_code { get; set; }
        //step5
        public string remark_4 { get; set; }
        public bool? is_kor_19 { get; set; }
        //step6
        public string evidence_name { get; set; }
        public string evidence_telephone { get; set; }
        //step7
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string sign_inform_others_list { get; set; }


        public ReferenceMasterView save150_representative_condition_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<eForm_Save150EvidenceView> evidence_list { get; set; }
    }
}
