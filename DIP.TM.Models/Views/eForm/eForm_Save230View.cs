﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save230View : EFormView
    {
        public string save230_document_type_code { get; set; }
        public string document_other_text { get; set; }
        public string save230_informer_type_code { get; set; }
        public string save230_representative_condition_type_code { get; set; }
        public bool? is_4_1 { get; set; }
        public bool? is_4_2 { get; set; }
        public bool? is_4_3 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public ReferenceMasterView save230_document_type_codeNavigation { get; set; }
        public ReferenceMasterView save230_informer_type_codeNavigation { get; set; }
        public ReferenceMasterView save230_representative_condition_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
    }
}
