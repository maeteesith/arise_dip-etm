﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save220Kor10View : EFormView
    {
        public long? save_id { get; set; }
        public string save220_kor10_representative_condition_type_code { get; set; }
        public bool? is_11_1_2_1 { get; set; }
        public bool? is_11_1_2_2 { get; set; }
        public bool? is_11_1_1 { get; set; }
        public bool? is_11_1_2 { get; set; }
        public bool? is_11_1_3 { get; set; }
        public bool? is_11_1_4 { get; set; }
        public bool? is_11_1_5 { get; set; }
        public bool? is_11_1_6 { get; set; }
        public bool? is_11_1_7 { get; set; }
        public bool? is_11_1_8 { get; set; }
        public bool? is_11_1_9 { get; set; }
        public string remark_11_1_9 { get; set; }

        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }

        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<eForm_Save220Kor10ProductView> product_list { get; set; }
        public List<eForm_Save220Kor10EventView> event_list { get; set; }
        public List<eForm_Save220Kor19View> kor19_list { get; set; }
    }
}
