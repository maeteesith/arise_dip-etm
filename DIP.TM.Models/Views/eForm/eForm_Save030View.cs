﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class eForm_Save030View : EFormView {

        public string save030_search_type_code { get; set; }
        public string contract_ref_number { get; set; }
        public string challenger_ref_number { get; set; }
        public string save030_informer_type_code { get; set; }
        public string save030_representative_condition_type_code { get; set; }
        public string save030_contact_type_code { get; set; }
        public string appeal_order { get; set; }
        public long? evidence_file_id { get; set; }
        public bool? is_8_1 { get; set; }
        public bool? is_8_2 { get; set; }
        public bool? is_8_3 { get; set; }
        public bool? is_8_4 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }


        public ReferenceMasterView save030_representative_condition_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<SaveAddressView> contact_address_list { get; set; }
        public List<eForm_Save030EvidenceView> evidence_list { get; set; }
        public List<eForm_Save030AppealOrderView> eform_appeal_order_list { get; set; }
    }
}
