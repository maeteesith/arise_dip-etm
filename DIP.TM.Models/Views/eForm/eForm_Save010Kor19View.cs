﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save010Kor19View : EFormView
    {
        public long? save_id { get; set; }
        public string kor_number { get; set; }
        public int? postpone_day { get; set; }
        public string remark { get; set; }

        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
    }
}
