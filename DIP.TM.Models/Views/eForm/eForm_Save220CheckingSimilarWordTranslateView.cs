﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save220CheckingSimilarWordTranslateView : BaseView
    {
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string word_translate_search { get; set; }
        public string word_translate_dictionary { get; set; }
        public string word_translate_sound { get; set; }
        public string word_translate_translate { get; set; }
        public string checking_word_translate_status_code_ { get; set; }
        public string translation_language_code { get; set; }
        public string word_translate { get; set; }

        public ReferenceMasterView translation_language_codeNavigation { get; set; }
        public ReferenceMasterView checking_word_translate_status_codeNavigation { get; set; }
    }
}
