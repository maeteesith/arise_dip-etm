﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save080EvidenceView : BaseView
    {
        public long? save_id { get; set; }
        public string evidence_type { get; set; }
        public string evidence_subject { get; set; }
        public int? number { get; set; }
        public string note { get; set; }
    }
}
