﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save010View : EFormView
    {
        //step2
        public string save010_mark_type_type_code { get; set; }
        //step3
        public string save010_representative_condition_type_code { get; set; }
        //step4
        public string save010_contact_type_code { get; set; }
        //step5.1
        public string save010_img_type_type_code { get; set; }
        public long? img_file_2d_id { get; set; }
        public long? img_file_3d_id_1 { get; set; }
        public long? img_file_3d_id_2 { get; set; }
        public long? img_file_3d_id_3 { get; set; }
        public long? img_file_3d_id_4 { get; set; }
        public long? img_file_3d_id_5 { get; set; }
        public long? img_file_3d_id_6 { get; set; }
        public string img_file_3d_path { get; set; }
        public string img_file_3d_type { get; set; }
        public int? img_w { get; set; }
        public int? img_h { get; set; }
        public string remark_5_1_3 { get; set; }
        //step5.2
        public bool? is_sound_mark { get; set; }
        //public string save010_sound_mark_type_type_code { get; set; }
        public bool? is_sound_mark_human { get; set; }
        public bool? is_sound_mark_animal { get; set; }
        public bool? is_sound_mark_sound { get; set; }
        public bool? is_sound_mark_other { get; set; }
        public string remark_5_2_2 { get; set; }
        public long? sound_file_id { get; set; }
        public long? sound_jpg_file_id { get; set; }
        //step8
        public bool? is_color_protect { get; set; }
        public string remark_8 { get; set; }
        //step9
        public bool? is_model_protect { get; set; }
        public string remark_9 { get; set; }
        //step10
        public bool? is_before_request { get; set; }
        //step11
        public string save010_assert_type_code { get; set; }
        //step12
        public string save010_otop_type_code { get; set; }
        public string otop_number { get; set; }
        //step13
        public bool? is_14_1 { get; set; }
        public bool? is_14_2 { get; set; }
        public bool? is_14_3 { get; set; }
        public bool? is_14_4 { get; set; }
        public bool? is_14_5 { get; set; }
        public bool? is_14_6 { get; set; }
        public bool? is_14_7 { get; set; }
        public bool? is_14_8 { get; set; }
        public bool? is_14_9 { get; set; }
        public bool? is_14_10 { get; set; }
        //step14
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }


        public string img_path { get; set; }//xx
        public string translation_language_code { get; set; } //xx
        public string save010_requestor_type_code { get; set; }//xx
        public string product_ctg_path { get; set; }//xx
        public long? product_ctg_file_id { get; set; }//xx
        public long? guarantee_rule_file_id { get; set; }//xx


        public FileView img_2D_file { get; set; }
        public FileView img_3D_file_1 { get; set; }
        public FileView img_3D_file_2 { get; set; }
        public FileView img_3D_file_3 { get; set; }
        public FileView img_3D_file_4 { get; set; }
        public FileView img_3D_file_5 { get; set; }
        public FileView img_3D_file_6 { get; set; }
        public FileView sound_file { get; set; }
        public FileView sound_jpg_file { get; set; }
        public ReferenceMasterView save010_representative_condition_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<SaveAddressView> contact_address_list { get; set; }
        public List<SaveAddressView> joiner_list { get; set; }
        public List<SaveAddressView> representative_kor_18_list { get; set; }
        public List<SaveProductView> product_list { get; set; }
        public List<eForm_Save010PublicPaymentView> public_payment_list { get; set; }
        public List<eForm_Save010Kor10View> kor10_list { get; set; }
        public List<eForm_Save010CheckingSimilarWordTranslateView> checking_similar_translate_list { get; set; }
    }
}
