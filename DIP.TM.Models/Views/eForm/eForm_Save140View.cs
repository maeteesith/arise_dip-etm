﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save140View : EFormView
    {
        //step2
        public string save140_informer_type_code { get; set; }
        //step3
        public string save140_inform_type_code { get; set; }
        //step4
        public string save140_representative_condition_type_code { get; set; }
        //step5
        public string save140_submit_type_code { get; set; }
        public string save140_department_code { get; set; }
        public string rule_number { get; set; }
        //step6
        public string remark_6_3 { get; set; }
        //step7
        public bool? is_7_1 { get; set; }
        public bool? is_7_2 { get; set; }
        public bool? is_7_3 { get; set; }
        //step8
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string sign_inform_others_list { get; set; }

        public ReferenceMasterView save140_representative_condition_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<SaveAddressView> contact_address_list { get; set; }
        public List<eForm_Save140SueView> sue_list { get; set; }
        public List<eForm_Save140JudgmentView> judgment_list { get; set; }
        public List<eForm_Save140FinalJudgmentView> final_judgment_list { get; set; }
    }
}
