﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class eForm_Save021View : EFormView {
        public string save021_representative_condition_type_code { get; set; }
        public string save021_contact_type_code { get; set; }
        public string save021_remark_4 { get; set; }
        public string kor_19_inform_person_type_code { get; set; }
        public string kor_19_remark { get; set; }
        public bool? is_6_1 { get; set; }
        public bool? is_6_2 { get; set; }
        public bool? is_6_3 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public long? argue_file_id { get; set; }

        public ReferenceMasterView save021_representative_condition_type_codeNavigation { get; set; }
        public List<SaveAddressView> people_list { get; set; }
        public List<SaveAddressView> representative_list { get; set; }
        public List<SaveAddressView> contact_address_list { get; set; }
        public List<eForm_Save021DisputeView> dispute_list { get; set; }
    }
}
