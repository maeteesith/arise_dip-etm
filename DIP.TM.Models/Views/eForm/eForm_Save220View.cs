﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save220View : EFormView
    {
        public string save220_search_type_code { get; set; }
        public string input_number_list { get; set; }
        public string input_number_check_list { get; set; }


        public List<eForm_Save220RequestItemView> request_item_list { get; set; }
    }
}
