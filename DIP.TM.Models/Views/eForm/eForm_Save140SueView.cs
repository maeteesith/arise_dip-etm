﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class eForm_Save140SueView : BaseView
    {
        public long? save_id { get; set; }
        public string court_name { get; set; }
        public string black_number_1 { get; set; }
        public string black_number_2 { get; set; }
        public DateTime? sue_date { get; set; }
    }
}
