﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vSaveSendView : vSaveSendModel {
        public DateTime? request_date { get; set; }
        public DateTime? make_date { get; set; }
        public string request_number { get; set; }
        public DateTime? department_send_date { get; set; }
        public long? file_id { get; set; }
        public string request_type_code { get; set; }
        public string request_type_name { get; set; }
        public string save_status_code { get; set; }
        public string save_status_name { get; set; }
        public string request_source_code { get; set; }
        public string request_source_name { get; set; }
        public string department_send_code { get; set; }
        public string department_send_name { get; set; }
        public string evidence_address { get; set; }
    }
}
