﻿using DIP.TM.Models.Payloads;

namespace DIP.TM.Models.Views {
    public class RequestCheckItemView : BaseView {
        public long? request_check_id { get; set; }
        public string request_check_item_code { get; set; }
        public bool? is_select { get; set; }
        public string value_other { get; set; }
        public int? value_quantity { get; set; }
        public decimal? value_total_price { get; set; }
        public bool? is_check_3 { get; set; }
    }
}
