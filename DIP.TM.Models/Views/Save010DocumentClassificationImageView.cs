﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010DocumentClassificationImageView : BaseView {
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string document_classification_image_code { get; set; }
        public ReferenceMasterView document_classification_image_codeNavigation { get; set; }
        public string document_classification_image_name { get { return document_classification_image_codeNavigation != null ? document_classification_image_codeNavigation.name : document_classification_image_code; } }
    }
}
