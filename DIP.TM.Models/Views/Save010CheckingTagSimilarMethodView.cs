﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010CheckingTagSimilarMethodView : BaseView {
        public long save_id { get; set; }
        public bool? method_1 { get; set; }
        public bool? method_2 { get; set; }
        public bool? method_3 { get; set; }
        public bool? method_4 { get; set; }
        public bool? method_5 { get; set; }
        public bool? method_6 { get; set; }
        public bool? method_7 { get; set; }
    }
}
