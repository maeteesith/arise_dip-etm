﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class PagingView
    {
        public string search_key { get; set; }
        public int page_index { get; set; }
        public int page_total { get; set; }
        public int item_per_page { get; set; }
        public int item_start { get; set; }
        public int item_end { get; set; }
        public int item_total { get; set; }
    }
}
