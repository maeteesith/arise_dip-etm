﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save020ProcessView : BaseSaveView {
        public DateTime? public_date { get; set; }
        public string book_number { get; set; }
        public string page_index { get; set; }
        public bool is_active { get; set; }
        public bool is_remove { get; set; }

        public List<SaveAddressView> people_list { get; set; }
        public string people_name { get { return people_list != null && people_list.Count() > 0 ? string.Join(", ", people_list.Select(r => r.name)) : ""; } }
        public List<SaveAddressView> representative_list { get; set; }
        public SaveAddressView contact_address { get; set; }
        public PostRoundView post_round { get; set; }
    }
}