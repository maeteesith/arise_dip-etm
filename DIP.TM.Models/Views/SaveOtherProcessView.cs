﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class SaveOtherProcessView : BaseSaveView {
        public string request_type_code { get; set; }
        public string request_type_name { get { return request_type_codeNavigation != null ? request_type_codeNavigation.name : ""; } }
        public ReferenceMasterView request_type_codeNavigation { get; set; }
        public string evidence_address { get; set; }
        public long? department_id { get; set; }
        public DateTime? send_date { get; set; }
    }
}
