﻿using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class BaseResponseView<T> {
        public string version { get { return "0.0.1"; } }
        public int event_log_id { get; set; }
        public T data { get; set; }
        public bool is_error { get; set; } = false;
        public int error_code { get; set; }
        public string error_message { get; set; } = string.Empty;
    }


    public class BaseResponsePageView<T> {
        public string version { get { return "0.0.1"; } }
        public int event_log_id { get; set; }
        public BaseResponsePageDataModel<T> data { get; set; }
        public bool is_error { get; set; } = false;
        public int error_code { get; set; }
        public string error_message { get; set; } = string.Empty;
    }

    public class BaseResponsePageDataModel<T> {
        public T list { get; set; }
        public PageRequest paging { get; set; }
    }
}
