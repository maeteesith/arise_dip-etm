﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class StatisticView : BaseView {
        public string code { get; set; }
        public string value { get; set; }
    }
}
