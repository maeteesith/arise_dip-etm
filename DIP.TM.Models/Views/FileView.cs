﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class FileView : BaseView
    {
        public string file_name { get; set; }
        public string file_path { get; set; }
        public string file_url { get; set; }
        public string physical_path { get; set; }
        public string display_path { get; set; }
        public long file_size { get; set; }
        public string file_size_text { get; set; }
    }
}
