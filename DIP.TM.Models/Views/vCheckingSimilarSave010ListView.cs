﻿using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class vCheckingSimilarSave010ListView : BaseView {
        public string request_number { get; set; }
        public string trademark_status_code { get; set; }
        public string request_item_sub_type_1_code_text { get; set; }
        public string request_item_sub_type_1_description_text { get; set; }
        public string display_type { get; set; }
        public string word_mark_text { get; set; }
        public string word_first_code_text { get; set; }
        public string word_sound_last_code_text { get; set; }
        public string word_sound_last_other_code_text { get; set; }
        public string name_text { get; set; }
        public string document_classification_image_code_text { get; set; }
        public string document_classification_sound_code_text { get; set; }
        public string document_classification_sound_name_text { get; set; }
        public string file_trademark_2d { get; set; }
        public long? sound_file_id { get; set; }
    }
}
