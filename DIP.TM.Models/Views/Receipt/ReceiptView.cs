﻿using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class ReceiptView : BaseView {
        public string reference_number { get; set; }
        public DateTime? receipt_date { get; set; }
        public DateTime? receive_date { get; set; }
        public DateTime? request_date { get; set; }
        public bool? is_paid_past { get; set; }
        public bool? is_manual { get; set; }
        public string receiver_name { get; set; }
        public string requester_name { get; set; }
        public string payer_name { get; set; }
        public string status_code { get; set; }

        public List<ReceiptItemView> item_list { get; set; }
        public List<ReceiptPaidView> paid_list { get; set; }

        //Add
        public string book_index { get; set; }
        public string page_index { get; set; }
        public string request_date_text { get { return request_date.HasValue ? request_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string barcode { get { return reference_number == null ? "" : BarcodeHelper.Generate(reference_number); } }
    }
}
