﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class ReceiptPaidView : BaseView {
        public long? receipt_id { get; set; }
        public string paid_channel_type_code { get; set; }
        public string paid_channel_type_name { get { return paid_channel_type_codeNavigation != null ? paid_channel_type_codeNavigation.name : ""; } }
        public string paid_reference_number_1 { get; set; }
        public string paid_reference_number_2 { get; set; }
        public decimal? total_price { get; set; }

        public ReferenceMasterView paid_channel_type_codeNavigation { get; set; }
    }
}
