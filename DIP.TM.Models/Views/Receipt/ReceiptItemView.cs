﻿using DIP.TM.Models.Payloads;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class ReceiptItemView : ReceiptItemAddModel {
        public string request_type_name { get { return request_type_codeNavigation != null ? request_type_codeNavigation.name : ""; } }
        public ReferenceMasterView request_type_codeNavigation { get; set; }
    }
}
