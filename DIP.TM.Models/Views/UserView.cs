﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class UserView
    {
        public string username { get; set; }
        public string name { get; set; }
        public string name_in_department { get; set; }
        public DepartmentView department { get; set; }
        public string email { get; set; }
        public DateTime? enable_start_date { get; set; }
        public DateTime? enable_end_date { get; set; }
        public bool is_enabled { get; set; }
    }
}
