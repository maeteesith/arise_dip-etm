﻿using DIP.TM.Models.ExternalModels;

namespace DIP.TM.Models.Views
{
    public class UMSsoUserModel
    {
        public long id { get; set; }
        public long user_sso_id { get; set; }
        public long? user_id { get; set; }
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public int expires_in { get; set; }
        public AuthSsoResponseModel data { get; set; }
    }
}
