﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save080AddressRepresentativeView : SaveAddressView {
        public string save080_representative_type_code { get; set; }
        public ReferenceMasterView save080_representative_type_codeNavigation { get; set; }
    }
}
