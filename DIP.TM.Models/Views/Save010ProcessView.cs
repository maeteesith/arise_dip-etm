﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save010ProcessView : BaseSaveView {
        public long? sound_file_id { get; set; }

        public bool? is_convert_irn { get; set; }
        public string irn_reference_number { get; set; }
        public string request_item_type_code { get; set; }
        public string commercial_affairs_province_code { get; set; }
        public string save_otop_type_code { get; set; }
        public string evidence_address { get; set; }
        public string otop_reference_number { get; set; }
        public string request_mark_feature_code_list { get; set; }
        public DateTime? receive_date { get; set; }
        public string address_country_code { get; set; }
        public long? contact_address_id { get; set; }
        public string trademark_role { get; set; }
        public string sound_description { get; set; }
        public string sound_mark_list { get; set; }
        public DateTime? saved_process_date { get; set; }
        public string document_status_code { get; set; }
        public long? document_spliter_by { get; set; }
        public long? document_classification_by { get; set; }
        public DateTime? document_classification_date { get; set; }
        public string document_classification_remark { get; set; }
        public string document_classification_status_code { get; set; }
        public string save010_document_classification_language_code { get; set; }
        public string checking_type_code { get; set; }
        public string checking_status_code { get; set; }
        public long? checking_spliter_by { get; set; }
        public long? checking_receiver_by { get; set; }
        public DateTime? checking_receive_date { get; set; }
        public string checking_receive_status_code { get; set; }
        public DateTime? checking_send_date { get; set; }
        public string checking_remark { get; set; }
        public long? considering_spliter_by { get; set; }
        public long? considering_receiver_by { get; set; }
        public string considering_receive_status_code { get; set; }
        public DateTime? considering_receive_date { get; set; }
        public string considering_receive_remark { get; set; }
        public DateTime? considering_send_date { get; set; }
        public string considering_reason_send_back { get; set; }
        public string considering_remark { get; set; }

        public string considering_similar_instruction_type_code { get; set; }
        public string considering_similar_evidence_27_code { get; set; }
        public string considering_similar_instruction_description { get; set; }



        public string public_status_code { get; set; }
        public long? public_spliter_by { get; set; }
        public long? public_receiver_by { get; set; }
        public string public_receive_status_code { get; set; }
        public DateTime? public_receive_date { get; set; }
        public DateTime? public_send_date { get; set; }
        public string public_receive_do_status_code { get; set; }
        public long? public_round_id { get; set; }
        public string public_page_index { get; set; }
        public string public_line_index { get; set; }
        public string public_role02_check_status_code { get; set; }
        public long? public_role02_check_by { get; set; }
        public DateTime? public_role02_check_date { get; set; }
        public DateTime? public_role04_payment_date { get; set; }
        public string public_role04_status_code { get; set; }
        public DateTime? public_role04_date { get; set; }
        public long? public_role04_spliter_by { get; set; }
        public long? public_role04_receiver_by { get; set; }
        public string public_role04_receive_status_code { get; set; }
        public DateTime? public_role04_receive_date { get; set; }
        public long? public_role05_by { get; set; }
        public string public_role05_status_code { get; set; }
        public DateTime? public_role05_date { get; set; }
        public long? floor3_proposer_by { get; set; }
        public DateTime? floor3_proposer_date { get; set; }
        public long? floor3_registrar_by { get; set; }
        public string floor3_registrar_status_code { get; set; }
        public DateTime? floor3_registrar_date { get; set; }
        public string floor3_registrar_description { get; set; }
        public DateTime? trademark_expired_start_date { get; set; }
        public DateTime? trademark_expired_date { get; set; }
        public DateTime? trademark_expired_end_date { get; set; }
        public string public_role04_document_status_code { get; set; }
        public DateTime? public_role04_document_date { get; set; }
        public string public_role02_document_status_code { get; set; }
        public DateTime? public_role02_document_date { get; set; }
        public string registration_number { get; set; }


        public string trademark_status_code { get; set; }


        public int? img_w { get; set; }
        public int? img_h { get; set; }
        public int? sound_jpg_file_id { get; set; }
        public string remark_8 { get; set; }
        public string remark_9 { get; set; }
        public string remark_5_1_3 { get; set; }


        public string saved_process_date_text { get { return saved_process_date.HasValue ? saved_process_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string document_classification_date_text { get { return document_classification_date.HasValue ? document_classification_date.Value.ToString("yyyy-MM-dd") : ""; } }
        public string document_classification_status_name { get { return document_classification_status_codeNavigation != null ? document_classification_status_codeNavigation.name : ""; } }
        public ReferenceMasterView document_classification_status_codeNavigation { get; set; }

        public ReferenceMasterView considering_similar_evidence_27_codeNavigation { get; set; }
        public string considering_similar_evidence_27_name { get { return considering_similar_evidence_27_codeNavigation != null ? considering_similar_evidence_27_codeNavigation.name : ""; } }
        public ReferenceMasterView considering_similar_instruction_type_codeNavigation { get; set; }
        public string considering_similar_instruction_type_name { get { return considering_similar_instruction_type_codeNavigation != null ? considering_similar_instruction_type_codeNavigation.name : ""; } }


        public string request_item_type_name { get { return request_item_type_codeNavigation != null ? request_item_type_codeNavigation.name : ""; } }
        public ReferenceMasterView request_item_type_codeNavigation { get; set; }

        public List<SaveAddressView> people_list { get; set; }
        public string people_name { get { return people_list != null && people_list.Count() > 0 ? people_list[0].name : ""; } }
        public string people_name_all { get { return people_list != null && people_list.Count() > 0 ? string.Join(", ", people_list.Select(r => r.name)) : ""; } }
        public string address_information { get { return people_list != null && people_list.Count() > 0 ? people_list[0].address_information : ""; } }
        public string contact_address_information {
            get {
                if (contact_address != null && contact_address.address_type_code == "OTHERS") {
                    return contact_address.address_information;
                } else if (contact_address != null && contact_address.address_type_code == "OWNER") {
                    if (people_list != null && people_list.Count() > 0)
                        return people_list[0].address_information;
                } else if (contact_address != null && contact_address.address_type_code == "REPRESENTATIVE") {
                    if (representative_list != null && representative_list.Count() > 0)
                        return representative_list[0].address_information;
                }
                return "Not found";
            }
        }
        public List<SaveAddressView> representative_list { get; set; }
        public string representative_name { get { return representative_list != null && representative_list.Count() > 0 ? representative_list[0].name : ""; } }
        public string representative_name_all { get { return representative_list != null && representative_list.Count() > 0 ? string.Join(", ", representative_list.Select(r => r.name)) : ""; } }
        public string representative_address_information { get { return representative_list != null && representative_list.Count() > 0 ? representative_list[0].address_information : ""; } }
        public SaveAddressView contact_address { get; set; }
        public List<SaveAddressView> joiner_list { get; set; }
        public List<SaveProductView> product_list { get; set; }
        public List<Save010Case28View> case28_list { get; set; }

        public string request_item_sub_type_1_code_first_text { get { return product_list != null && product_list.Count() > 0 ? product_list[0].request_item_sub_type_1_code : ""; } }
        public string request_item_sub_type_1_code_text { get { return product_list != null ? string.Join(", ", product_list.Select(r => r.request_item_sub_type_1_code)) : ""; } }
        public string request_item_sub_type_1_description_first_text { get { return product_list != null && product_list.Count() > 0 ? product_list[0].description : ""; } }
        public string request_item_sub_type_1_description_text { get { return product_list != null ? string.Join(", ", product_list.Select(r => r.description)) : ""; } }
        public List<Save010CheckingSimilarWordTranslateView> checking_similar_wordsoundtranslate_list { get; set; }
        public List<Save010DocumentClassificationWordView> document_classification_word_list { get; set; }
        public string word_mark_text { get { return document_classification_word_list != null ? string.Join(", ", document_classification_word_list.Select(r => r.word_mark)) : ""; } }
        public string word_sound_text { get { return document_classification_word_list != null ? string.Join(", ", document_classification_word_list.Select(r => r.word_sound)) : ""; } }
        public List<Save010DocumentClassificationImageView> document_classification_image_list { get; set; }
        public List<Save010DocumentClassificationSoundView> document_classification_sound_list { get; set; }
        public string sound_type_text { get { return document_classification_sound_list != null ? string.Join(", ", document_classification_sound_list.Select(r => r.document_classification_sound_name)) : ""; } }
        public List<Save010DocumentClassificationSearchMethodView> document_classification_search_method { get; set; }
        public List<Save010InstructionRuleView> instruction_rule_list { get; set; }

        public string file_save01 { get; set; }
        public string trademark_2d_physical_path { get; set; }
        public vPublicRegisterView public_register { get; set; }
        public string considering_receiver_by_name { get; set; }
        public DateTime? last_extend_date { get; set; }

        // 
        public long? classification_send_next_save_id { get; set; }

        //public-role01-check-do
        public List<vSave010RequestGroupView> request_group_list { set; get; }
        //public-role02-consider-payment/edit
        public vSave010PublicPaymentView save010_public_payment { set; get; }


        //public List<Save020ProcessView> save020_list { get; set; }
    }
}
