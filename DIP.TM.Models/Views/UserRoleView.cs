﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class UserRoleView
    {
        public string name { get; set; }
        public List<PolicyView> policy_list { get; set; }
    }
}
