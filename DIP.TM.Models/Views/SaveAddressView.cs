﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class SaveAddressView : BaseView {
        public long? save_id { get; set; }
        public string descrition { get; set; }
        public string address_type_code { get; set; }
        public string nationality_code { get; set; }
        public string career_code { get; set; }
        public string card_type_code { get; set; }
        public string card_type_name { get { return card_type_codeNavigation != null ? card_type_codeNavigation.name : ""; } }
        public string card_number { get; set; }
        public string receiver_type_code { get; set; }
        public string name { get; set; }
        public string legal_name { get; set; }
        public string house_number { get; set; }
        public string village_number { get; set; }
        public string alley { get; set; }
        public string street { get; set; }
        public string address_sub_district_code { get; set; }
        public string address_district_code { get; set; }
        public string address_province_code { get; set; }
        public string postal_code { get; set; }
        public string address_country_code { get; set; }
        public string telephone { get; set; }
        public string fax { get; set; }
        public string email { get; set; }
        public string sex_code { get; set; }
        public string address_representative_condition_type_code { get; set; }
        public string other_text { get; set; }
        public string country_text { get; set; }
        public bool? is_contact_person { get; set; }


        // for eform_save060
        public bool? is_contract_check { get; set; }
        public bool? is_detail_check { get; set; }
        public long? change_ref_id { get; set; }


        //for representative_list
        public string representative_type_code { get; set; }
        // for contact_address_list
        public bool? is_specific { get; set; }


        // Save010AddressRepresentative
        public ReferenceMasterView representative_type_codeNavigation { get; set; }

        //Save050ReceiverAddressPeople
        public string receiver_people_type_code { get; set; }

        // Save060AddressPeople
        public long? people_id { get; set; }
        public ReferenceMasterView career_codeNavigation { get; set; }
        public ReferenceMasterView nationality_codeNavigation { get; set; }
        public ReferenceMasterView card_type_codeNavigation { get; set; }
        public ReferenceMasterView address_country_codeNavigation { get; set; }
        public ReferenceMasterView address_district_codeNavigation { get; set; }
        public ReferenceMasterView address_province_codeNavigation { get; set; }
        public ReferenceMasterView address_sub_district_codeNavigation { get; set; }

        public string nationality_name { get; set; }
        public string career_name { get; set; }
        public string address_information {
            get {
                return
                    (!string.IsNullOrEmpty(house_number) ? house_number + " " : "") +

                    (address_country_code != "TH" ? "" :
                    (address_sub_district_codeNavigation != null ? "ตำบล" + address_sub_district_codeNavigation.name + " " : "") +
                    (address_district_codeNavigation != null ? "อำเภอ" + address_district_codeNavigation.name + " " : "") +
                    (address_province_codeNavigation != null ? "จังหวัด" + address_province_codeNavigation.name + " " : "") +
                    (!string.IsNullOrEmpty(postal_code) ? postal_code + " " : "") +
                    "")
                ;
            }
        }

        public string address_details_1
        {
            get
            {
                return
                    (address_country_code != "TH") ? house_number :

                    (!string.IsNullOrEmpty(house_number) ? house_number + " " : "") +
                    (!string.IsNullOrEmpty(village_number) ? "หมู่ที่ " + village_number + " " : "") +
                    (!string.IsNullOrEmpty(alley) ? "ซอย/ตรอก " + alley + " " : "") +
                    (!string.IsNullOrEmpty(street) ? "ถนน " + street + " " : "")
                    ;
            }
        }

        public string address_details_2
        {
            get
            {
                return
                    (address_country_code != "TH" ? "" :
                    (address_sub_district_codeNavigation != null ? "แขวง/ตำบล " + address_sub_district_codeNavigation.name + " " : "") +
                    (address_district_codeNavigation != null ? "เขต/อำเภอ " + address_district_codeNavigation.name + " " : "") +
                    (address_province_codeNavigation != null ? "จังหวัด " + address_province_codeNavigation.name + " " : "") +
                    (!string.IsNullOrEmpty(postal_code) ? "รหัสไปรษณีย์ " + postal_code + " " : "") +
                    (address_country_codeNavigation != null ? "ประเทศ" + address_country_codeNavigation.name + " " : "") +
                    "")
                    ;
            }
        }
    }
}
