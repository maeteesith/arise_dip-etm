﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class Save040AddressRepresentativeView : SaveAddressView {
        public string save040_representative_type_code { get; set; }
        public ReferenceMasterView save040_representative_type_codeNavigation { get; set; }
    }
}
