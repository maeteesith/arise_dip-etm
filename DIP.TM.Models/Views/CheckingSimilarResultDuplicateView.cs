﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views {
    public class CheckingSimilarResultDuplicateView : BaseView {
        public List<vCheckingSimilarResultDuplicate_SaveView> save_word_list { set; get; }
        public List<vCheckingSimilarResultDuplicate_SaveView> save_image_list { set; get; }
        public List<vCheckingSimilarResultDuplicate_SaveView> save_sound_list { set; get; }

        public List<vCheckingSimilarResultDuplicate_WordView> word_list { set; get; }
    }
}
