﻿using DIP.TM.Models.Payloads;
using DIP.TM.Utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class RequestCheckRequestNumberView : RequestCheckRequestNumberAddModel {
        public long? save_id { get; set; }
        public string trademark_status_code { get; set; }
    }
}
