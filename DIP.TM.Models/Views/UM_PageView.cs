﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Views {
    public class UM_PageView : BaseView {
        public string code { get; set; }
        public long menu_sub_id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public string page_link { get; set; }
        public bool is_show_menu { get; set; }
        public List<UM_PolicyView> UM_Policy { get; set; }
    }
}
