﻿using DIP.TM.Datas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Models.Views
{
    public class SaveInstructionRuleRequestNumberView : vSave010View
    {
        public long? instruction_rule_id { get; set; }
    }
}
