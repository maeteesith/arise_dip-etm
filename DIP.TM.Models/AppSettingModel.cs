﻿using Newtonsoft.Json;

namespace DIP.TM.Models
{
    public class AppSettingModel
    {
        [JsonProperty("ConnectionStrings")]
        public AppConnectionstrings ConnectionStrings { get; set; }
        [JsonProperty("Logging")]
        public AppLogging Logging { get; set; }
        public string AllowedHosts { get; set; }
        public string BaseUrl { get; set; }
        public string SsoUrl { get; set; }
        public string SsoAppId { get; set; }
        public string SsoAppSecret { get; set; }
        public string Version { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public string SigningKey { get; set; }
        public string SecretKey { get; set; }
        [JsonProperty("HealthChecksUI")]
        public AppHealthchecksui HealthChecksUI { get; set; }
    }

    public class AppConnectionstrings
    {
        [JsonProperty("DefaultConnection")]
        public AppDefaultconnection DefaultConnection { get; set; }
    }

    public class AppDefaultconnection
    {
        public string Host { get; set; }
        public string Port { get; set; }
        public string DbName { get; set; }
        public string User { get; set; }
        public string Password { get; set; }
    }

    public class AppLogging
    {
        [JsonProperty("LogLevel")]
        public AppLoglevel LogLevel { get; set; }
    }

    public class AppLoglevel
    {
        public string Default { get; set; }
    }

    public class AppHealthchecksui
    {
        [JsonProperty("HealthChecks")]
        public AppHealthcheck[] HealthChecks { get; set; }
        [JsonProperty("Webhooks")]
        public AppWebhook[] Webhooks { get; set; }
        public int EvaluationTimeInSeconds { get; set; }
        public int MinimumSecondsBetweenFailureNotifications { get; set; }
    }

    public class AppHealthcheck
    {
        public string Name { get; set; }
        public string Uri { get; set; }
    }

    public class AppWebhook
    {
        public string Name { get; set; }
        public string Uri { get; set; }
        public string Payload { get; set; }
        public string RestoredPayload { get; set; }
    }

}
