﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Models.Documents
{
    public class ReceiptPdfHeaderModel
    {
        // TODO REMOVE
        public string request_number { get; set; } = "654656545645";
        public string receipt_number { get; set; } = "TM056544";

        public string year { get; set; } = "2561";
        public string date { get; set; } = "10 กันยายน 2561";

        public string receive_name { get; set; } = "บริษัท กฎหมายและบัญชี ซี แอนด์ พี จำกัด";
    }
}
