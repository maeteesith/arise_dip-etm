﻿using DIP.TM.Datas;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Uows.DataAccess.Uow;

namespace DIP.TM.Services.InitDbs
{
    public class BootService
    {
        private readonly IUowProvider uowProvider;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="uowProvider"></param>
        public BootService(IUowProvider uowProvider)
        {
            this.uowProvider = uowProvider;
        }

        public void BootDatabase()
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<UM_User>();
                var users = repo.Get(1);
            }
        }
    }
}
