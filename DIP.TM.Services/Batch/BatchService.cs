﻿using DIP.TM.Models.Configs;
using DIP.TM.Models.Email;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Interfaces;
using DIP.TM.Utils.Logging;
using Microsoft.Extensions.Configuration;
using System;
using System.Net;
using System.Net.Mail;
using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;
using DIP.TM.Uows.DataAccess;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.IO.Compression;
using System.Xml;
using System.Xml.Linq;
using Newtonsoft.Json;
using DIP.TM.Models.Batch;
using System.Xml.Serialization;
using DIP.TM.Models.BatchXML;
using System.Globalization;

namespace DIP.TM.Services.Batch
{
    public class BatchService
    {
        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public BatchService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }

        public string Tesst()
        {
            return "topland";
        }

        private long? ConvertLong(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return null;
            }
            else
            {
                long res;
                var check = Int64.TryParse(val, out res);
                if (check)
                {
                    return res;
                }
                else
                {
                    return null;
                }
            }
        }

        private int? ConvertInt(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return null;
            }
            else
            {
                int res;
                var check = Int32.TryParse(val, out res);
                if (check)
                {
                    return res;
                }
                else
                {
                    return null;
                }
            }
        }

        private DateTime? ConvertDateTime(string val)
        {
            if (string.IsNullOrEmpty(val))
            {
                return null;
            }
            else
            {
                DateTime res;
                var check = DateTime.TryParse(DateTime.ParseExact(val, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"), out res);
                if (check)
                {
                    return res;
                }
                else
                {
                    return null;
                }
            }
        }

        public string ListFiles()
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftpird.wipo.int/wipo/madrid/notif/th");
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                //request.Credentials = new NetworkCredential("username", "password");
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                var nn = names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                nn = nn.OrderByDescending(x => x).ToList();
                List<string> res = new List<string>();

                nn.ForEach(x =>
                {
                    if (x.IndexOf(".zip") > 0)
                    {
                        res.Add(x);
                    }
                });

                return res.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ListImages()
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftpird.wipo.int/wipo/madrid/notif/th/images");
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                //request.Credentials = new NetworkCredential("username", "password");
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                var nn = names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                nn = nn.OrderByDescending(x => x).ToList();
                List<string> res = new List<string>();

                nn.ForEach(x =>
                {
                    if (x.IndexOf(".zip") > 0)
                    {
                        res.Add(x);
                    }
                });

                return res.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ListIrregs()
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftpird.wipo.int/wipo/madrid/notif/th/irregs");
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                //request.Credentials = new NetworkCredential("username", "password");
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                var nn = names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                nn = nn.OrderByDescending(x => x).ToList();
                List<string> res = new List<string>();

                nn.ForEach(x =>
                {
                    if (x.IndexOf(".zip") > 0)
                    {
                        res.Add(x);
                    }
                });

                return res.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string ListPDF()
        {
            try
            {
                FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://ftpird.wipo.int/wipo/madrid/notif/th/PDF");
                request.Method = WebRequestMethods.Ftp.ListDirectory;

                //request.Credentials = new NetworkCredential("username", "password");
                FtpWebResponse response = (FtpWebResponse)request.GetResponse();
                Stream responseStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(responseStream);
                string names = reader.ReadToEnd();

                reader.Close();
                response.Close();

                var nn = names.Split(new string[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).ToList();
                nn = nn.OrderByDescending(x => x).ToList();
                List<string> res = new List<string>();

                nn.ForEach(x =>
                {
                    if (x.IndexOf(".zip") > 0)
                    {
                        res.Add(x);
                    }
                });

                return res.FirstOrDefault();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void DownloadFile(string path, string fname)
        {
            FtpWebRequest request =
                (FtpWebRequest)WebRequest.Create("ftp://ftpird.wipo.int/wipo/madrid/notif/th/" + path + fname);
            //request.Credentials = new NetworkCredential("username", "password");
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            using (Stream ftpStream = request.GetResponse().GetResponseStream())
            using (Stream fileStream = System.IO.File.Create(@"\\192.168.30.15\Project\DIP\ENOTIF\" + fname))
            {
                ftpStream.CopyTo(fileStream);
            }

        }

        public void ExtractFiles(string type, string fname)
        {
            string zipPath = @"\\192.168.30.15\Project\DIP\ENOTIF\" + fname;

            if (type == "xml")
            {
                string extractPath = @"\\192.168.30.15\Project\DIP\ENOTIF\xml\";
                ZipFile.ExtractToDirectory(zipPath, extractPath);
                System.IO.File.Delete(@"\\192.168.30.15\Project\DIP\ENOTIF\" + fname);
            }
            else if (type == "irregs")
            {
                string extractPath = @"\\192.168.30.15\Project\DIP\ENOTIF\irregs\";
                ZipFile.ExtractToDirectory(zipPath, extractPath);
                System.IO.File.Delete(@"\\192.168.30.15\Project\DIP\ENOTIF\" + fname);
            }
            else if (type == "PDF")
            {
                string extractPath = @"\\192.168.30.15\Project\DIP\ENOTIF\PDF\";
                ZipFile.ExtractToDirectory(zipPath, extractPath);
                System.IO.File.Delete(@"\\192.168.30.15\Project\DIP\ENOTIF\" + fname);
            }
            else if (type == "images")
            {
                string extractPath = @"\\192.168.30.15\Project\DIP\ENOTIF\images\";
                ZipFile.ExtractToDirectory(zipPath, extractPath);
                System.IO.File.Delete(@"\\192.168.30.15\Project\DIP\ENOTIF\" + fname);
            }

        }

        public Root Writefile()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(@"C:\Users\User\Desktop\N202025.xml");
            //doc.Load(@"\\192.168.30.15\Project\DIP\Batch\extract\x202025\N202025.xml");
            string json = JsonConvert.SerializeObject(doc, Newtonsoft.Json.Formatting.Indented);
            var js = json.Replace("@", "").Replace("-", "_");
            var Obj = JsonConvert.DeserializeObject<Root>(js);
            return Obj;
        }

        public ENOTIF Mapdata()
        {
            ENOTIF MyObj = DeserializeXMLFileToObject<ENOTIF>(@"C:\Users\User\Desktop\N202025.xml");
            return MyObj;
        }

        public static T DeserializeXMLFileToObject<T>(string XmlFilename)
        {
            T returnObject = default(T);
            if (string.IsNullOrEmpty(XmlFilename)) return default(T);

            try
            {
                StreamReader xmlStream = new StreamReader(XmlFilename);
                XmlSerializer serializer = new XmlSerializer(typeof(T));
                returnObject = (T)serializer.Deserialize(xmlStream);
            }
            catch (Exception ex)
            {
                throw;
            }
            return returnObject;
        }

        public List<Madrid_Enotif> GetPriceRequestMaster()
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var masterRepo = uow.GetRepository<Madrid_Enotif>();
                var results = masterRepo.GetAll().ToList();
                // Madrid_Enotif nn = new Madrid_Enotif();
                // nn.id = 5;
                // nn.is_deleted = false;
                // nn.cpcd = "top";

                // masterRepo.Add(nn);

                return results;
            }
        }

        public void TestAdd()
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var masterRepo = uow.GetRepository<Madrid_Enotif>();
                //var results = masterRepo.GetAll().ToList();
                Madrid_Enotif nn = new Madrid_Enotif();
                // nn.is_deleted = false;
                // nn.CPCD = "top";
                // nn.created_date = DateTime.Now;
                // nn.updated_date = DateTime.Now;

                // masterRepo.Add(nn);

                uow.SaveChanges();


                // return results;
            }
        }

        public void AddMadrid_EnotifCount(ENOTIF data)
        {
            try
            {
                using (var uow = uowProvider.CreateUnitOfWork())
                {
                    //var masterRepo = uow.GetRepository<Madrid_EnotifCount>();
                    //Madrid_EnotifCount mdEno = new Madrid_EnotifCount();
                    //mdEno.BIRTHCOUNT = data.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(data.BIRTHCOUNT);
                    //mdEno.DEATHCOUNT = data.DEATHCOUNT == null ? (int?)null : Convert.ToInt32(data.DEATHCOUNT);
                    //mdEno.NEWNAMECOUNT = data.NEWNAMECOUNT == null ? (int?)null : Convert.ToInt32(data.NEWNAMECOUNT);
                    //mdEno.RESTRICTCOUNT = data.RESTRICTCOUNT == null ? (int?)null : Convert.ToInt32(data.RESTRICTCOUNT);
                    //mdEno.NEWBASECOUNT = data.NEWBASECOUNT == null ? (int?)null : Convert.ToInt32(data.NEWBASECOUNT);
                    //mdEno.PROLONGCOUNT = data.PROLONGCOUNT == null ? (int?)null : Convert.ToInt32(data.PROLONGCOUNT);
                    //mdEno.CORRECTIONCOUNT = data.CORRECTIONCOUNT == null ? (int?)null : Convert.ToInt32(data.CORRECTIONCOUNT);
                    //mdEno.PROCESSEDCOUNT = data.PROCESSEDCOUNT == null ? (int?)null : Convert.ToInt32(data.PROCESSEDCOUNT);
                    //mdEno.CREATEDCOUNT = data.CREATEDCOUNT == null ? (int?)null : Convert.ToInt32(data.CREATEDCOUNT);
                    //mdEno.LICENCE_BIRTHCOUNT = data.LICENCEBIRTHCOUNT == null ? (int?)null : Convert.ToInt32(data.LICENCEBIRTHCOUNT);
                    //mdEno.LICENCE_NEWNAMECOUNT = data.LICENCEBIRTHCOUNT == null ? (int?)null : Convert.ToInt32(data.LICENCEBIRTHCOUNT);
                    //mdEno.PAIDCOUNT = data.PAIDCOUNT == null ? (int?)null : Convert.ToInt32(data.PAIDCOUNT);
                    //mdEno.CPCD = data.CPCD;
                    //mdEno.NOTLANG = data.NOTLANG == null ? (int?)null : Convert.ToInt32(data.NOTLANG);
                    //mdEno.GAZNO = data.GAZNO;
                    //mdEno.WEEKNO = data.WEEKNO == null ? (int?)null : Convert.ToInt32(data.WEEKNO);
                    //mdEno.PUBDATE = data.PUBDATE == null ? (DateTime?)null : Convert.ToDateTime(DateTime.ParseExact(data.PUBDATE, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));
                    //mdEno.NOTDATE = data.NOTDATE == null ? (DateTime?)null : Convert.ToDateTime(DateTime.ParseExact(data.NOTDATE, "yyyyMMdd", CultureInfo.InvariantCulture).ToString("yyyy-MM-dd"));

                    //masterRepo.Add(mdEno);
                    uow.SaveChanges();
                    // return results;
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public void AddMadrid_Enotif(ENOTIF data)
        {
            try
            {
                using (var uow = uowProvider.CreateUnitOfWork())
                {
                    var masterRepo = uow.GetRepository<Madrid_Enotif>();

                    foreach(var dat in data.CORRECTION)
                    {
                        Madrid_Enotif mdEno = new Madrid_Enotif();
                        //mdEno.TRANTYP = dat.CORRECT.BIRTH.TRANTYP;
                        //mdEno.INTREGN = dat.CORRECT.BIRTH.INTREGN == null ? (long?)null : Convert.ToInt64(dat.CORRECT.BIRTH.INTREGN);
                        //mdEno.DOCID = dat.DOCID == null ? (long?)null : Convert.ToInt64(dat.DOCID);
                        // mdEno.WEEKNO = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.CPCD = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.NOTLANG = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.GAZNO = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.PUBDATE = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.NOTDATE = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.OOCD = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.ORIGLAN = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.EXPDATE = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.REGRDAT = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.REGEDAT = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.OFFREF = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.INTREGD = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.DESUNDER = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.RENDATE = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.TEXTEN = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.GAZNUM = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.SIGVRBL = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.STDMIND = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.MARDUR = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.VRBLNOT = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.MARCOLI = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                        // mdEno.ReqType = dat.BIRTHCOUNT == null ? (int?)null : Convert.ToInt32(dat.BIRTHCOUNT);
                         masterRepo.Add(mdEno);
                    }

                    uow.SaveChanges();
                    // return results;
                }
            }
            catch(Exception ex)
            {
                throw;
            }
        }
    }
}
