﻿using AutoMapper;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Extensions;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Uows.DataAccess.Entities;
using DIP.TM.Utils.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Web;

namespace DIP.TM.Services.Saves {

    public class BaseService<D, V> where D : EntityBase {

        protected long? userId;

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public BaseService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;

            userId = HttpContext.Current?.User?.UserId();
        }
        [LogAopInterceptor]
        public virtual BaseResponsePageView<List<V>> List(PageRequest payload, string[] column_filter_list = null) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                if (column_filter_list != null) {
                    foreach (var column_filter in column_filter_list) {
                        payload.filter_queries.Add("!string.IsNullOrEmpty(" + column_filter + ")");
                    }
                }
                var repo = uow.GetRepository<D>();
                var itemList = repo.Filters(x => x.is_deleted == false);
                var filterList = itemList.FilterDynamic(payload);
                payload.item_total = filterList.Count();
                if ((payload.page_index - 1) * payload.item_per_page > payload.item_total) {
                    payload.page_index = 1;
                }
                var resultPage = filterList.Page(payload.page_index, payload.item_per_page);

                try {
                    var list = mapper.Map<List<V>>(resultPage.ToList());
                    return new BaseResponsePageView<List<V>>() {
                        data = new BaseResponsePageDataModel<List<V>>() {
                            list = list,
                            paging = payload
                        }
                    };
                } catch (Exception _e) {
                    System.Diagnostics.Debug.WriteLine(_e.Message);
                    throw _e;
                }
            }
        }

        [LogAopInterceptor]
        public virtual V Get(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var entity = repo.Get(id);
                return mapper.Map<V>(entity);
            }
        }

        public long GetUserId() {
            return userId.Value;
        }
    }
}
