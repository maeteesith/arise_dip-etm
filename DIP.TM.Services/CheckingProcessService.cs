﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Auths;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Helps;
using DIP.TM.Services.Interfaces;
using DIP.TM.Services.ReferenceMaster;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Utils.Extensions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Web;

namespace DIP.TM.Services.Checking {
    public class CheckingProcessService : ICheckingProcessService {
        protected long? userId;

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public CheckingProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;

            userId = HttpContext.Current?.User?.UserId();
        }

        public BaseResponsePageView<List<vSave010View>> CheckingItemList(PageRequest payload) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
            payload.search_by.Add(new SearchByModel() {
                key = "document_classification_status_code",
                value = DocumentClassificationStatusCodeConstant.SEND.ToString(),
            });
            return service.ListPage(payload); ;
        }

        public BaseResponsePageView<List<vUM_UserView>> CheckingItemCheckerList(PageRequest payload) {
            var service = new UserService(configuration, uowProvider, mapper);
            //payload.filter.department_id = 1;
            return service.UserViewList(payload);
        }

        //public List<vUM_UserView> CheckingItemCheckerList(BasePageModelPayload<vUM_UserAddModel> payload)
        //{
        //    var service = new UserService(configuration, uowProvider, mapper);
        //    payload.filter.department_id = 1;
        //    return service.List(payload);
        //}


        public virtual List<Save010ProcessView> CheckingItemAutoSplit() {
            var view_list = new List<Save010ProcessView>();
            //int item_min = 10;
            //int item_max = 20;

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vUM_User>();
                var user_list = repo.Query(r => r.department_id == 2 || 1 == 1);

                //TODO Split into staff by min max
                foreach (var user in user_list) {
                    var item_repo = uow.GetRepository<Save010>();
                    var item_list = item_repo.Query(r =>
                        r.checking_status_code == CheckingStatusCodeConstant.WAIT_CHECKING.ToString() ||
                        r.checking_status_code == CheckingStatusCodeConstant.WAIT_CONSIDERING.ToString()
                    );

                    foreach (var item in item_list) {
                        if (item.checking_status_code == CheckingStatusCodeConstant.WAIT_CHECKING.ToString()) {
                            item.checking_spliter_by = userId;
                            item.checking_receiver_by = user.id;
                            item.checking_type_code = CheckingTypeCodeConstant.CHECKING.ToString();
                            item.checking_status_code = CheckingStatusCodeConstant.RECEIVED_CHECKING.ToString();
                            item.checking_receive_status_code = CheckingReceivingStatusCodeConstant.WAIT_DONE.ToString();

                            item_repo.Update(item);
                        } else if (item.checking_status_code == CheckingStatusCodeConstant.WAIT_CONSIDERING.ToString()
                            // For Test
                            //|| checking.considering_receive_status_code != ConsideringReceivingStatusCodeConstant.SEND.ToString()
                            ) {
                            item.considering_spliter_by = userId;
                            item.considering_receiver_by = user.id;
                            item.considering_receive_date = DateTime.Now;
                            item.checking_type_code = CheckingTypeCodeConstant.CONSIDERING.ToString();
                            item.checking_status_code = CheckingStatusCodeConstant.RECEIVED_CONSIDERING.ToString();
                            item.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT.ToString();

                            item_repo.Update(item);
                        } else {
                        }

                        view_list.Add(mapper.Map<Save010ProcessView>(item));
                    }
                }
                uow.SaveChanges();
            }

            return view_list;
        }

        public List<vSave010View> CheckingItemReceive(vSave010Model payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();

                foreach (vSave010Model checking in payload.checking_list) {
                    if (checking.is_check) {
                        if (checking.checking_status_code == null || checking.checking_status_code == CheckingStatusCodeConstant.WAIT_CHECKING.ToString()) {
                            checking.checking_spliter_by = userId;
                            checking.checking_receiver_by = payload.checking_receiver_by;
                            checking.checking_type_code = CheckingTypeCodeConstant.CHECKING.ToString();
                            checking.checking_status_code = CheckingStatusCodeConstant.RECEIVED_CHECKING.ToString();
                            checking.checking_receive_status_code = CheckingReceivingStatusCodeConstant.WAIT_DONE.ToString();

                            var service = new SaveProcessService<vSave010Model, Save010, vSave010View>(configuration, uowProvider, mapper);
                            service.Save(checking);
                        } else if (checking.checking_status_code == CheckingStatusCodeConstant.WAIT_CONSIDERING.ToString()
                            // For Test
                            //|| checking.considering_receive_status_code != ConsideringReceivingStatusCodeConstant.SEND.ToString()
                            ) {
                            checking.considering_spliter_by = userId;
                            checking.considering_receiver_by = payload.checking_receiver_by;
                            checking.considering_receive_date = DateTime.Now;
                            checking.checking_type_code = CheckingTypeCodeConstant.CONSIDERING.ToString();
                            checking.checking_status_code = CheckingStatusCodeConstant.RECEIVED_CONSIDERING.ToString();
                            checking.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT.ToString();

                            var service = new SaveProcessService<vSave010Model, Save010, vSave010View>(configuration, uowProvider, mapper);
                            service.Save(checking);
                        } else {
                        }
                    }
                }

                return mapper.Map<List<vSave010View>>(payload.checking_list);
            }
        }

        public vSave010View CheckingSimilarWordSoundTranslateSave(Save010AddModel payload) {
            var service = new SaveProcessService<Save010AddModel, Save010, vSave010View>(configuration, uowProvider, mapper);
            var view = service.Save(payload);
            return service.Save010Load(payload.id.Value, true);
        }

        public List<StatisticView> CheckingSimilarStatisticList() {

            var list = CheckingSimilarList(new PageRequest() {
                filter_queries = new List<string>() {
                     "(checking_receive_status_code == \"" + CheckingReceivingStatusCodeConstant.DONE.ToString() + "\" && checking_send_date > DateTime.Now.Date.AddDays(-DateTime.Now.Day + 1)) || " +
                     "(checking_receive_status_code != \"" + CheckingReceivingStatusCodeConstant.DONE.ToString() + "\")"
                 }, item_per_page = int.MaxValue,
                //filter_queries = new List<string>() {
                //     "checking_receive_status_code == \"" + CheckingReceivingStatusCodeConstant.DONE.ToString() + "\""
                // },
                //search_by = new List<SearchByModel>() {
                //    new SearchByModel() {
                //         key = "checking_receive_status_code",
                //         value = CheckingReceivingStatusCodeConstant.DONE.ToString(),
                //    }
                //}
            });


            var view_list = new List<StatisticView>();

            foreach (var checking_receive_status_code in list.data.list.Select(r => r.checking_receive_status_code).Distinct()) {
                view_list.Add(new StatisticView() {
                    code = checking_receive_status_code,
                    value = list.data.list.Where(r => r.checking_receive_status_code == checking_receive_status_code).Count().ToString(),
                });
            }

            return view_list;
        }

        public BaseResponsePageView<List<vSave010View>> CheckingSimilarList(PageRequest payload) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
            // TODO Check payload from client
            //payload.checking_receiver_by = userId;
            //if (ReferenceEquals(payload.search_by, null)) {
            //    payload.search_by = new List<SearchByModel>();
            //}

            payload.filter_queries.Add("request_id < 10000");
            payload.filter_queries.Add("!string.IsNullOrEmpty(checking_receive_status_code)");
            //payload.search_by.Add(new SearchByModel() {
            //    key = "checking_receive_status_code",
            //    values = new List<string>() {
            //        CheckingReceivingStatusCodeConstant.WAIT_CHANGE.ToString(),
            //        CheckingReceivingStatusCodeConstant.WAIT_DONE.ToString(),
            //        CheckingReceivingStatusCodeConstant.DONE.ToString(),
            //    }
            //});

            // TODO
            //if (payload.search_by.Where(x => x.key.ToLower() == "checking_receiver_by").Count() == 0) {
            //    payload.search_by.Add(new SearchByModel() { key = "checking_receiver_by", value = userId.ToString() });
            //}

            return service.ListPage(payload);
        }

        public BaseResponsePageView<List<vSave010View>> ConsideringSimilarListPage(PageRequest payload) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
            // TODO Check payload from client
            //payload.checking_receiver_by = userId;
            if (ReferenceEquals(payload.search_by, null)) {
                payload.search_by = new List<SearchByModel>();
            }

            payload.filter_queries.Add("!string.IsNullOrEmpty(checking_receive_status_code)");
            //payload.search_by.Add(new SearchByModel() {
            //    key = "checking_receive_status_code",
            //    values = new List<string>() {
            //        CheckingReceivingStatusCodeConstant.WAIT_CHANGE.ToString(),
            //        CheckingReceivingStatusCodeConstant.WAIT_DONE.ToString(),
            //        CheckingReceivingStatusCodeConstant.DONE.ToString(),
            //    }
            //});

            // TODO
            //if (payload.search_by.Where(x => x.key.ToLower() == "checking_receiver_by").Count() == 0) {
            //    payload.search_by.Add(new SearchByModel() { key = "checking_receiver_by", value = userId.ToString() });
            //}

            return service.ListPage(payload);
        }

        public vSave010View CheckingSimilarLoad(long id) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
            var view = service.Get(id);

            var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
            view.full_view = service_full_view.Get(id);

            return view;
        }

        //public object PublicRole01PrepareList(PageRequest payload) {
        //    throw new NotImplementedException();
        //}

        public bool CheckingSimilarTagAdd(object[] payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010CheckingTagSimilar>();
                long save_id = Convert.ToInt64(payload[0].ToString());
                long save_tag_id = Convert.ToInt64(payload[1].ToString());
                var entity = repo.Query(r => r.save_id == save_id && r.save_tag_id == save_tag_id).FirstOrDefault();

                bool is_same = Convert.ToBoolean(payload[2].ToString());

                var methods = JsonConvert.DeserializeObject<string[]>(Convert.ToString(payload[3]));

                if (entity != null) {
                    if (entity.is_same != is_same) {
                        var edit = mapper.Map<vSave010CheckingTagSimilarAddModel>(entity);
                        entity.is_same = is_same;


                        using (var _uow = uowProvider.CreateUnitOfWork()) {
                            var _repo = _uow.GetRepository<Save010CheckingTagSimilar>();
                            var _edit = mapper.Map<vSave010CheckingTagSimilarAddModel>(entity);
                            _edit.method_1 = methods.Where(r => r == "search_1_word_first_code" || r == "search_1_word_sound_last_code").Count() > 0;
                            _edit.method_2 = methods.Where(r => r == "search_1_1_word_mark").Count() > 0;
                            _edit.method_3 = methods.Where(r => r == "search_1_word_sound_last_other_code").Count() > 0;
                            _edit.method_4 = methods.Where(r => r == "search_1_2_word_syllable_sound").Count() > 0;
                            _edit.method_5 = methods.Where(r => r == "search_1_document_classification_image_code").Count() > 0;
                            _edit.method_6 = methods.Where(r => r == "search_1_document_classification_sound_code").Count() > 0;
                            _edit.method_7 = methods.Where(r => r == "search_smell_type").Count() > 0;
                            _edit.is_same = is_same;
                            _repo.Update(mapper.Map<Save010CheckingTagSimilar>(_edit));
                            _uow.SaveChanges();
                        }
                    }
                } else {
                    var edit = new vSave010CheckingTagSimilarAddModel() {
                        save_id = save_id,
                        save_tag_id = save_tag_id,
                        method_1 = methods.Where(r => r == "search_1_word_first_code" || r == "search_1_word_sound_last_code").Count() > 0,
                        method_2 = methods.Where(r => r == "search_1_1_word_mark" || r == "search_1_word_mark").Count() > 0,
                        method_3 = methods.Where(r => r == "search_1_word_sound_last_other_code").Count() > 0,
                        method_4 = methods.Where(r => r == "search_1_2_word_syllable_sound").Count() > 0,
                        method_5 = methods.Where(r => r == "search_image_type").Count() > 0,
                        method_6 = methods.Where(r => r == "search_sound_type").Count() > 0,
                        method_7 = methods.Where(r => r == "search_smell_type").Count() > 0,
                        is_same = is_same,
                    };
                    using (var _uow = uowProvider.CreateUnitOfWork()) {
                        var _repo = _uow.GetRepository<Save010CheckingTagSimilar>();
                        var _edit = mapper.Map<Save010CheckingTagSimilar>(edit);
                        _repo.Add(_edit);
                        _uow.SaveChanges();
                    }

                }

                return true;
            }
        }

        public bool CheckingSimilarTagRemove(List<vSave010CheckingTagSimilarAddModel> payloads) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010CheckingTagSimilar>();
                foreach (var payload in payloads) {
                    if (payload.is_check) {
                        var entity = repo.Query(r => r.save_id == payload.save_id && r.save_tag_id == payload.save_tag_id).FirstOrDefault();

                        var edit = mapper.Map<vSave010CheckingTagSimilarAddModel>(entity);
                        entity.is_same = false;

                        using (var _uow = uowProvider.CreateUnitOfWork()) {
                            var _repo = _uow.GetRepository<Save010CheckingTagSimilar>();
                            var _edit = mapper.Map<vSave010CheckingTagSimilarAddModel>(entity);
                            _repo.Update(mapper.Map<Save010CheckingTagSimilar>(_edit));
                            _uow.SaveChanges();
                        }
                    }
                }
                return true;
            }
        }
        public List<vSave010CheckingTagSimilarView> CheckingSimilarTagList(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vSave010CheckingTagSimilar>();
                var entity_list = repo.Query(r => r.save_id == id && r.is_same).ToList();

                var view_list = mapper.Map<List<vSave010CheckingTagSimilarView>>(entity_list);
                foreach (vSave010CheckingTagSimilarView view in view_list) {
                    var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                    view.full_view = service_full_view.Get(view.id.Value);
                }

                return view_list;
            }
        }

        public bool CheckingSimilarResultSend(vSave010Model payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                //var entity = repo.Get(payload.id.Value);

                var payload_new = mapper.Map<Save010AddModel>(payload);
                payload_new.checking_receive_status_code = CheckingReceivingStatusCodeConstant.DONE.ToString();
                payload_new.checking_type_code = CheckingTypeCodeConstant.CONSIDERING.ToString();
                payload_new.checking_status_code = CheckingStatusCodeConstant.WAIT_CONSIDERING.ToString();

                //if (payload.considering_receive_status_code == ConsideringReceivingStatusCodeConstant.WAIT_CHANGE.ToString()) {
                //    payload_new.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_CHANGE_SEND.ToString();
                //} else {
                //}

                using (var _uow = uowProvider.CreateUnitOfWork()) {
                    var _repo = _uow.GetRepository<Save010>();
                    _repo.Update(mapper.Map<Save010>(payload_new));
                    _uow.SaveChanges();

                    return true;
                }
            }
        }

        //public List<vSave010View> ConsideringSimilarList(PageRequest payload) {
        //    var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
        //    if (ReferenceEquals(payload.search_by, null)) {
        //        payload.search_by = new List<SearchByModel>();
        //    }


        //    payload.search_by.Add(new SearchByModel() {
        //        key = "considering_receive_status_code",
        //        values = new List<string>() {
        //            ConsideringReceivingStatusCodeConstant.WAIT_CHANGE.ToString(),
        //            ConsideringReceivingStatusCodeConstant.WAIT_DONE.ToString(),
        //            ConsideringReceivingStatusCodeConstant.DONE.ToString(),
        //        }
        //    });

        //    // TODO
        //    //if (payload.search_by.Where(x => x.key.ToLower() == "considering_receiver_by").Count() == 0) {
        //    //    payload.search_by.Add(new SearchByModel() { key = "considering_receiver_by", value = userId.ToString() });
        //    //}

        //    return service.ListSave010(payload);
        //}

        public vSave010View ConsideringSimilarLoad(long id) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
            var view = service.Get(id);

            var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
            view.full_view = service_full_view.Get(view.id.Value);

            return view;
        }

        public List<vSave010CheckingTagSimilarView> ConsideringSimilarSave(List<vSave010CheckingTagSimilarAddModel> payloads) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                //var save_repo = uow.GetRepository<vSave010>();
                var public_round_repo = uow.GetRepository<PublicRound>();

                var repo = uow.GetRepository<Save010CheckingTagSimilar>();
                var instruction_rule_repo = uow.GetRepository<Save010InstructionRule>();

                foreach (vSave010CheckingTagSimilarAddModel payload in payloads) {
                    var entity = repo.Query(r => r.save_id == payload.save_id && r.save_tag_id == payload.save_tag_id).FirstOrDefault();
                    if (entity.is_like_approve != payload.is_like_approve ||
                        entity.is_same_approve != payload.is_same_approve) {

                        entity.is_like_approve = payload.is_like_approve;
                        entity.is_same_approve = payload.is_same_approve;

                        //var model = mapper.Map<vSave010CheckingTagSimilarAddModel>(entity);

                        //var _repo = uow.GetRepository<Save010CheckingTagSimilar>();

                        if (!entity.is_created_instruction_rule) {
                            if (entity.is_like_approve || entity.is_same_approve) {
                                entity.is_created_instruction_rule = true;
                                if (payload.trademark_status_code == TrademarkStatusCodeConstant.P.ToString()) {
                                    entity.instruction_rule_code = entity.is_like_approve ? ConsideringInstructionRuleCodeConstant.ROLE_20_1.ToString() : ConsideringInstructionRuleCodeConstant.ROLE_20_2.ToString();

                                    if (payload.public_round_id.HasValue && payload.public_round_id.Value > 0) {
                                        var public_round_entity = public_round_repo.Get(payload.public_round_id.Value);
                                        if (public_round_entity.public_start_date <= DateTime.Now && DateTime.Now <= public_round_entity.public_end_date) {
                                            instruction_rule_repo.Add(new Save010InstructionRule() {
                                                save_id = payload.save_id,
                                                instruction_date = DateTime.Now,
                                                instruction_rule_code = ConsideringInstructionRuleCodeConstant.ROLE_29.ToString(),
                                                considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                                                considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                                                considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                                            });
                                        }
                                    }
                                } else if (payload.trademark_status_code == TrademarkStatusCodeConstant.R.ToString()) {
                                    entity.instruction_rule_code = entity.is_like_approve ? ConsideringInstructionRuleCodeConstant.ROLE_13_1.ToString() : ConsideringInstructionRuleCodeConstant.ROLE_13_2.ToString();
                                } else {
                                    return null;
                                }
                                entity.instruction_rule_value_1 = payload.instruction_rule_value_1;

                                instruction_rule_repo.Add(new Save010InstructionRule() {
                                    save_id = payload.save_id,
                                    instruction_date = DateTime.Now,
                                    instruction_rule_code = entity.instruction_rule_code,
                                    value_1 = entity.instruction_rule_value_1,
                                    considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                                    considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                                    considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                                });
                            }
                        }

                        repo.Update(entity);
                    }
                }

                uow.SaveChanges();

                return mapper.Map<List<vSave010CheckingTagSimilarView>>(payloads);
            }
        }


        public List<ItemSubType1SuggestionView> ItemSubType1SuggestionLoad(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010Product>();
                var entity_list = repo.Query(r => r.save_id == id);
                entity_list = entity_list.OrderBy(r => r.request_item_sub_type_2_code).OrderBy(r => r.request_item_sub_type_1_code).ToList();
                var view_list = mapper.Map<List<ItemSubType1SuggestionView>>(entity_list);

                var item_sub_type1_repo = uow.GetRepository<Save010InstructionRule9ItemSubType1>();
                var item_sub_type1_entity_list = item_sub_type1_repo.Query(r => r.save_id == id);

                var request_item_sub_type1_repo = uow.GetRepository<RM_RequestItemSubType1>();
                //var service = new RequestItemSubType1Service(configuration, uowProvider, mapper);
                foreach (var view in view_list) {
                    var request_item_sub_type1_entity = request_item_sub_type1_repo.Query(r => r.name == view.description).FirstOrDefault();

                    if (request_item_sub_type1_entity == null) {
                        //view.suggestion_list = service.CheckTag(new ReferenceMasterModel() {
                        //    name = view.description,
                        //});
                    } else {
                        view.suggestion_list = new List<ReferenceMasterView>() {
                                new ReferenceMasterView() {
                                    id = request_item_sub_type1_entity.id,
                                    code = request_item_sub_type1_entity.code,
                                    name = request_item_sub_type1_entity.name,
                                }
                            };
                    }

                    var item_sub_type1_entity = item_sub_type1_entity_list.Where(r => r.save010_product_id == view.id && !r.is_deleted).FirstOrDefault();
                    if (item_sub_type1_entity != null) {
                        view.save010_product_id = item_sub_type1_entity.save010_product_id;
                        view.suggestion_item_id = item_sub_type1_entity.suggestion_item_id;
                        view.item_id = item_sub_type1_entity.item_id;
                        view.item_code = item_sub_type1_entity.item_code;
                        view.item_name = item_sub_type1_entity.item_name;
                    }
                }

                return view_list;
            }
        }

        bool CheckingSimilarSearchMethodAdd(CheckingSimilarSave010ListModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010CheckingTagSimilarMethod>();
                var save = repo.Query(r => r.save_id == payload.request01_item_id).FirstOrDefault();
                if (save == null) save = new Save010CheckingTagSimilarMethod() {
                    save_id = payload.request01_item_id
                };

                if (!string.IsNullOrEmpty(payload.search_1_word_first_code) || !string.IsNullOrEmpty(payload.search_1_word_first_code)) {
                    save.method_1 = true;
                }
                if (!string.IsNullOrEmpty(payload.search_1_word_sound_last_code)) {
                    save.method_1 = true;
                }
                if (!string.IsNullOrEmpty(payload.search_1_word_mark) || !string.IsNullOrEmpty(payload.search_1_1_word_mark)) {
                    save.method_2 = true;
                }
                if (!string.IsNullOrEmpty(payload.search_1_word_sound_last_other_code)) {
                    save.method_3 = true;
                }
                if (!string.IsNullOrEmpty(payload.search_1_2_word_syllable_sound)) {
                    save.method_4 = true;
                }
                if (!string.IsNullOrEmpty(payload.search_1_document_classification_image_code)) {
                    save.method_5 = true;
                }
                if (!string.IsNullOrEmpty(payload.search_1_document_classification_sound_code)) {
                    save.method_6 = true;
                }
                //if (!string.IsNullOrEmpty(payload.search_smell_type) || !string.IsNullOrEmpty(payload.search_smell_type)) {
                //    save.method_7 = true;
                //}

                repo.Update(save);

                uow.SaveChanges();
                return true;
            }
        }

        public BaseResponsePageView<List<vCheckingSimilarSave010ListView>> CheckingSimilarSave010List(CheckingSimilarSave010ListModel payload) {
            CheckingSimilarSearchMethodAdd(payload);

            using (var uow = uowProvider.CreateUnitOfWork()) {
                //var repo = uow.GetRepository<Save010>();
                //var vrepo = uow.GetRepository<vSave010>();
                //var tag_repo = uow.GetRepository<Save010CheckingTagSimilar>();

                //var search_1_request_item_sub_type_1_code_list = string.Join(",", string.IsNullOrEmpty(payload.search_1_request_item_sub_type_1_code) ? new string[] { } : payload.search_1_request_item_sub_type_1_code.Split(new string[] { " ", "," }, StringSplitOptions.RemoveEmptyEntries));
                //if (search_1_request_item_sub_type_1_code_list != "") search_1_request_item_sub_type_1_code_list = "," + search_1_request_item_sub_type_1_code_list + ",";

                //if (!string.IsNullOrEmpty(payload.search_1_1_word_mark)) payload.search_1_1_word_mark = payload.search_1_1_word_mark.Replace("_", "#");

                //var search_1_request_item_sub_type_1_description_text = payload.search_1_request_item_sub_type_1_description_text;

                //var search_1_document_classification_image_code_count = 0;
                //var search_1_document_classification_image_code_text = "";
                //if (!string.IsNullOrEmpty(payload.search_1_document_classification_image_code) && !string.IsNullOrEmpty(payload.search_1_document_classification_image_condition)) {
                //    var search_1_document_classification_image_code_list = payload.search_1_document_classification_image_code.Split(new string[] { " ", "|" }, StringSplitOptions.RemoveEmptyEntries);
                //    if (search_1_document_classification_image_code_list.Length > 0) {
                //        search_1_document_classification_image_code_count = payload.search_1_document_classification_image_condition == "AND" ? search_1_document_classification_image_code_list.Length : 1;
                //        search_1_document_classification_image_code_text = "," + string.Join(",", search_1_document_classification_image_code_list) + ",";
                //    }
                //}

                //var search_1_document_classification_sound_code_count = 0;
                //var search_1_document_classification_sound_code_text = "";
                //if (!string.IsNullOrEmpty(payload.search_1_document_classification_sound_code) && !string.IsNullOrEmpty(payload.search_1_document_classification_sound_condition)) {
                //    var search_1_document_classification_sound_code_list = payload.search_1_document_classification_sound_code.Split(new string[] { " ", "|" }, StringSplitOptions.RemoveEmptyEntries);
                //    if (search_1_document_classification_sound_code_list.Length > 0) {
                //        search_1_document_classification_sound_code_count = payload.search_1_document_classification_sound_condition == "AND" ? search_1_document_classification_sound_code_list.Length : 1;
                //        search_1_document_classification_sound_code_text = "," + string.Join(",", search_1_document_classification_sound_code_list) + ",";
                //    }
                //}

                //var entity_list = repo.Query(r =>
                // (r.document_classification_status_code == DocumentClassificationStatusCodeConstant.SEND.ToString()) &&

                // (string.IsNullOrEmpty(payload.search_1_request_number_start) || string.Compare(r.request_number, payload.search_1_request_number_start) >= 0) &&
                // (string.IsNullOrEmpty(payload.search_1_request_number_end) || string.Compare(r.request_number, payload.search_1_request_number_end) <= 0) &&
                // (string.IsNullOrEmpty(payload.search_1_word_mark) || r.Save010DocumentClassificationWord.Where(r => r.word_mark == payload.search_1_word_mark.Replace("_", "#")).Count() > 0) &&
                // (string.IsNullOrEmpty(payload.search_1_people_name) || r.Save010AddressPeople.Where(r => r.name.Replace("_", "").Replace(".", "").Replace(",", "").Replace(" ", "").IndexOf(payload.search_1_people_name.Replace("_", "").Replace(".", "").Replace(",", "").Replace(" ", "")) >= 0).Count() > 0) &&

                // (string.IsNullOrEmpty(search_1_request_item_sub_type_1_code_list) || r.Save010Product.Where(r => search_1_request_item_sub_type_1_code_list.IndexOf("," + r.request_item_sub_type_1_code + ",") >= 0).Count() > 0) &&

                // (string.IsNullOrEmpty(search_1_request_item_sub_type_1_description_text) || r.Save010Product.Where(s => s.description.IndexOf(search_1_request_item_sub_type_1_description_text) >= 0).Count() > 0) &&

                // (string.IsNullOrEmpty(payload.search_1_word_first_code) || r.Save010DocumentClassificationWord.Where(r => r.word_first_code == payload.search_1_word_first_code).Count() > 0) &&
                // (string.IsNullOrEmpty(payload.search_1_word_sound_last_code) || r.Save010DocumentClassificationWord.Where(r => r.word_sound_last_code == payload.search_1_word_sound_last_code).Count() > 0) &&
                // (string.IsNullOrEmpty(payload.search_1_word_sound_last_other_code) || r.Save010DocumentClassificationWord.Where(r => r.word_sound_last_other_code == payload.search_1_word_sound_last_other_code).Count() > 0) &&

                // (string.IsNullOrEmpty(payload.search_1_1_word_samecondition_code) || string.IsNullOrEmpty(payload.search_1_1_word_mark) || (
                //    (payload.search_1_1_word_samecondition_code == "WORD_SAME" &&
                //     r.Save010DocumentClassificationWord.Where(r => r.word_mark == payload.search_1_1_word_mark).Count() > 0) ||
                //    (payload.search_1_1_word_samecondition_code == "LIKE" &&
                //     r.Save010DocumentClassificationWord.Where(r => r.word_mark.Contains(payload.search_1_1_word_mark)).Count() > 0) ||
                //    (payload.search_1_1_word_samecondition_code == "LIKE_FRONT" &&
                //     r.Save010DocumentClassificationWord.Where(r => r.word_mark.StartsWith(payload.search_1_1_word_mark)).Count() > 0) ||
                //    (payload.search_1_1_word_samecondition_code == "LIKE_BACK" &&
                //     r.Save010DocumentClassificationWord.Where(r => r.word_mark.EndsWith(payload.search_1_1_word_mark)).Count() > 0) ||
                //     false
                // )) &&

                // (string.IsNullOrEmpty(payload.search_1_2_word_samecondition_code) || string.IsNullOrEmpty(payload.search_1_2_word_syllable_sound) || (
                //    (payload.search_1_2_word_samecondition_code == "WORD_SAME" &&
                //     r.Save010DocumentClassificationWord.Where(r => r.word_syllable_sound == payload.search_1_2_word_syllable_sound).Count() > 0) ||
                //    (payload.search_1_2_word_samecondition_code == "LIKE" &&
                //     r.Save010DocumentClassificationWord.Where(r => (" " + r.word_syllable_sound + " ").Contains(" " + payload.search_1_2_word_syllable_sound + " ")).Count() > 0) ||
                //    (payload.search_1_2_word_samecondition_code == "LIKE_FRONT" &&
                //     r.Save010DocumentClassificationWord.Where(r => (" " + r.word_syllable_sound + " ").StartsWith(" " + payload.search_1_2_word_syllable_sound + " ")).Count() > 0) ||
                //    (payload.search_1_2_word_samecondition_code == "LIKE_BACK" &&
                //     r.Save010DocumentClassificationWord.Where(r => r.word_syllable_sound.EndsWith(payload.search_1_2_word_syllable_sound)).Count() > 0) ||
                //     false
                // )) &&

                // (string.IsNullOrEmpty(search_1_document_classification_image_code_text) || r.Save010DocumentClassificationImage.Where(r => search_1_document_classification_image_code_text.Contains("," + r.document_classification_image_code + ",")).Count() >= search_1_document_classification_image_code_count) &&

                // (string.IsNullOrEmpty(search_1_document_classification_sound_code_text) || r.Save010DocumentClassificationSound.Where(r => search_1_document_classification_sound_code_text.Contains("," + r.document_classification_sound_code + ",")).Count() >= search_1_document_classification_sound_code_count) &&
                // 1 == 1
                //);

                //var tag_list = tag_repo.Query(r => r.save_id == payload.request01_item_id && r.is_same.Value).ToList();

                //var view_list = mapper.Map<List<vSave010View>>(entity_list);
                //foreach (vSave010View view in view_list) {
                //    var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                //    view.full_view = service_full_view.Get(view.id.Value);
                //    view.is_check = tag_list.Where(r => r.save_tag_id == view.id).Count() > 0;

                //    if (view.sound_file_id.HasValue && view.sound_file_id > 0) {
                //        var file_repo = uow.GetRepository<File>();
                //        var file = file_repo.Get(view.sound_file_id.Value);
                //        view.sound_file_physical_path = file.physical_path;
                //    }

                //    var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                //    view.file_trademark_2d = rdc_service.GetTrademark2DPhysicalPath(view.id.Value);
                //}

                //var repo = uow.GetRepository<vCheckingSimilarSave010List>();


                //var a = new vCheckingSimilarSave010List();
                //a.sound_file_id.HasValue

                var paging = payload.paginate;
                paging.search_by = new List<SearchByModel>();
                paging.filter_queries = new List<string>();

                if (payload.is_have_image) {
                    paging.filter_queries.Add("!string.IsNullOrEmpty(file_trademark_2d)");
                }
                if (payload.is_have_sound) {
                    paging.filter_queries.Add("sound_file_id.HasValue");
                }

                if (payload.display_type != string.Empty)
                {
                    paging.search_by.Add(new SearchByModel()
                    {
                        key = "display_type",
                        value = payload.display_type,
                        operation = Operation.Equals,
                    });
                }

                if (!string.IsNullOrEmpty(payload.search_1_request_number_start)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "request_number",
                        value = payload.search_1_request_number_start,
                        operation = Operation.GreaterThanOrEqual,
                    });
                }
                if (!string.IsNullOrEmpty(payload.search_1_request_number_end)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "request_number",
                        value = payload.search_1_request_number_end,
                        operation = Operation.LessThanOrEqual,
                    });
                }
                if (!string.IsNullOrEmpty(payload.search_1_people_name)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "name_text",
                        value = payload.search_1_people_name,
                        operation = Operation.Contains,
                    });
                }
                if (!string.IsNullOrEmpty(payload.search_1_request_item_sub_type_1_description_text)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "request_item_sub_type_1_description_text",
                        value = payload.search_1_request_item_sub_type_1_description_text,
                        operation = Operation.Contains,
                    });
                }
                if (!string.IsNullOrEmpty(payload.search_1_word_mark)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "word_mark_text",
                        value = "|" + payload.search_1_word_mark + "|",
                        operation = Operation.Contains,
                    });
                }

                if (!string.IsNullOrEmpty(payload.search_1_request_item_sub_type_1_code)) {
                    var request_item_sub_type_1_code = string.Join(" || ", payload.search_1_request_item_sub_type_1_code.Split(new string[] { " ", "|" }, StringSplitOptions.RemoveEmptyEntries).
                           Select(r => "request_item_sub_type_1_code_text.Contains(\"|" + r + "|\")"));
                    paging.filter_queries.Add(request_item_sub_type_1_code);
                }
                if (!string.IsNullOrEmpty(payload.search_1_word_first_code)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "word_first_code_text",
                        value = "|" + payload.search_1_word_first_code + "|",
                        operation = Operation.Contains,
                    });
                }
                if (!string.IsNullOrEmpty(payload.search_1_word_sound_last_code)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "word_sound_last_code_text",
                        value = "|" + payload.search_1_word_sound_last_code + "|",
                        operation = Operation.Contains,
                    });
                }
                if (!string.IsNullOrEmpty(payload.search_1_word_sound_last_other_code)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "word_sound_last_other_code_text",
                        value = "|" + payload.search_1_word_sound_last_other_code + "|",
                        operation = Operation.Contains,
                    });
                }

                if (!string.IsNullOrEmpty(payload.search_1_1_word_samecondition_code) && !string.IsNullOrEmpty(payload.search_1_1_word_mark)) {
                    paging.search_by.Add(new SearchByModel() {
                        key = "word_mark_text",
                        value =
                            (payload.search_1_1_word_samecondition_code == "WORD_SAME" || payload.search_1_1_word_samecondition_code == "LIKE_FRONT" ? " | " : "") +
                            payload.search_1_1_word_mark +
                            (payload.search_1_1_word_samecondition_code == "WORD_SAME" || payload.search_1_1_word_samecondition_code == "LIKE_BACK" ? " | " : ""),
                        operation = Operation.Contains,
                    });
                }
                if (!string.IsNullOrEmpty(payload.search_1_2_word_samecondition_code) && !string.IsNullOrEmpty(payload.search_1_2_word_syllable_sound)) {
                    //paging.search_by.Add(new SearchByModel() {
                    //    key = "word_mark_text",
                    //    value =
                    //        (payload.search_1_2_word_samecondition_code == "WORD_SAME" || payload.search_1_2_word_samecondition_code == "LIKE_FRONT" ? " | " : "") +
                    //        payload.search_1_2_word_syllable_sound +
                    //        (payload.search_1_2_word_samecondition_code == "WORD_SAME" || payload.search_1_2_word_samecondition_code == "LIKE_BACK" ? " | " : ""),
                    //    operation = Operation.Contains,
                    //});
                }

                if (!string.IsNullOrEmpty(payload.search_1_document_classification_image_condition) && !string.IsNullOrEmpty(payload.search_1_document_classification_image_code)) {
                    var document_classification_image_code = string.Join(" " + (payload.search_1_document_classification_image_condition == "AND" ? "&&" : "||") + " ", payload.search_1_document_classification_image_code.Split(new string[] { " ", "|" }, StringSplitOptions.RemoveEmptyEntries).
                           Select(r => "document_classification_image_code_text.Contains(\"|" + r + "|\")"));
                    paging.filter_queries.Add(document_classification_image_code);
                }
                if (!string.IsNullOrEmpty(payload.search_1_document_classification_sound_condition) && !string.IsNullOrEmpty(payload.search_1_document_classification_sound_code)) {
                    var document_classification_sound_code = string.Join(" " + (payload.search_1_document_classification_sound_condition == "AND" ? "&&" : "||") + " ", payload.search_1_document_classification_sound_code.Split(new string[] { " ", "|" }, StringSplitOptions.RemoveEmptyEntries).
                           Select(r => "document_classification_sound_code_text.Contains(\"|" + r + "|\")"));
                    paging.filter_queries.Add(document_classification_sound_code);
                }

                var repo = uow.GetRepository<vCheckingSimilarSave010>();
                var itemList = repo.Filters(x => x.is_deleted == false);
                var filterList = itemList.FilterDynamic(payload.paginate);
                payload.paginate.item_total = filterList.Count();
                var resultPage = filterList.Page(payload.paginate.page_index, payload.paginate.item_per_page);

                return new BaseResponsePageView<List<vCheckingSimilarSave010ListView>>() {
                    data = new BaseResponsePageDataModel<List<vCheckingSimilarSave010ListView>>() {
                        list = mapper.Map<List<vCheckingSimilarSave010ListView>>(resultPage),
                        paging = payload.paginate
                    }
                };

                //return view_list;
            }
        }

        public BaseResponsePageView<List<vSave010CheckingInstructionDocumentView>> ConsideringSimilarInstructionList(PageRequest payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vSave010CheckingInstructionDocument>();
                var itemList = repo.Filters(x => x.is_deleted == false);
                var filterList = itemList.FilterDynamic(payload);
                payload.item_total = filterList.Count();
                var resultPage = filterList.Page(payload.page_index, payload.item_per_page);

                return new BaseResponsePageView<List<vSave010CheckingInstructionDocumentView>>() {
                    data = new BaseResponsePageDataModel<List<vSave010CheckingInstructionDocumentView>>() {
                        list = mapper.Map<List<vSave010CheckingInstructionDocumentView>>(resultPage),
                        paging = payload
                    }
                };
            }
        }

        public List<vSave010CheckingTagSimilarView> CheckingSimilarResultOwnerSame(List<vSave010CheckingTagSimilarAddModel> payloads) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010CheckingTagSimilar>();

                foreach (vSave010CheckingTagSimilarAddModel payload in payloads) {
                    if (payload.is_check) {
                        var entity = repo.Query(r => r.save_id == payload.save_id && r.save_tag_id == payload.save_tag_id).FirstOrDefault();
                        entity.is_owner_same = !payload.is_owner_same;

                        var model = mapper.Map<vSave010CheckingTagSimilarAddModel>(entity);

                        payload.is_owner_same = entity.is_owner_same;
                        model.is_owner_same = entity.is_owner_same;

                        using (var _uow = uowProvider.CreateUnitOfWork()) {
                            var _repo = _uow.GetRepository<Save010CheckingTagSimilar>();
                            _repo.Update(mapper.Map<Save010CheckingTagSimilar>(model));
                            _uow.SaveChanges();
                        }
                    }
                }

                return mapper.Map<List<vSave010CheckingTagSimilarView>>(payloads);
            }
        }

        public vSave010View ConsideringSimilarInstructionLoad(long id) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
            var view = service.Get(id);

            var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
            view.full_view = service_full_view.Get(view.id.Value);

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010InstructionRuleRequestNumber>();
                foreach (var instruction_rule in view.full_view.instruction_rule_list) {
                    var instruction_rule_request_number_list = repo.QueryActive(r => r.instruction_rule_id == instruction_rule.id);
                    instruction_rule.request_number_save_list = new List<Save010InstructionRuleRequestNumberView>();
                    foreach (var instruction_rule_request_number in instruction_rule_request_number_list) {
                        var save010 = service.List(new vSave010Model() { request_number = instruction_rule_request_number.request_number }).FirstOrDefault();
                        var instruction_rule_request_number_view = mapper.Map<Save010InstructionRuleRequestNumberView>(save010);
                        instruction_rule_request_number_view.id = instruction_rule_request_number.id;
                        instruction_rule_request_number_view.instruction_rule_id = instruction_rule.id;
                        instruction_rule.request_number_save_list.Add(instruction_rule_request_number_view);
                    }
                }
            }

            view.full_view.instruction_rule_list = view.full_view.instruction_rule_list.OrderByDescending(r => r.id).ToList();
            return view;
        }

        public vSave010View ConsideringSimilarInstructionSave(vSave010Model payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var entity = mapper.Map<Save010>(payload);

                var instruction_rule_repo = uow.GetRepository<Save010InstructionRule>();

                if (payload.considering_similar_instruction_type_code == ConsideringSimilarInstructionTypeCodeConstant.YES.ToString()) {
                    if (entity.Save010InstructionRule.Where(r => r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.SPECIFIC.ToString() &&
                        r.considering_instruction_status_code != ConsideringInstructionStatusCodeConstant.DELETE.ToString()).Count() == 0) {
                        var instruction_rule_entity = new Save010InstructionRule() {
                            save_id = payload.id,
                            instruction_date = DateTime.Now,
                            instruction_rule_code = ConsideringInstructionRuleCodeConstant.SPECIFIC.ToString(),
                            considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                            considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                            considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                        };
                        instruction_rule_repo.Add(instruction_rule_entity);
                    }
                } else if (payload.considering_similar_instruction_type_code == ConsideringSimilarInstructionTypeCodeConstant.NO.ToString()) {
                    if (entity.Save010InstructionRule.Where(r => r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.NOT_SPECIFIC.ToString() &&
                        r.considering_instruction_status_code != ConsideringInstructionStatusCodeConstant.DELETE.ToString()).Count() == 0) {
                        var instruction_rule_entity = new Save010InstructionRule() {
                            save_id = payload.id,
                            instruction_date = DateTime.Now,
                            instruction_rule_code = ConsideringInstructionRuleCodeConstant.NOT_SPECIFIC.ToString(),
                            considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                            considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                            considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                        };
                        instruction_rule_repo.Add(instruction_rule_entity);
                    }
                }

                repo.Update(entity);

                uow.SaveChanges();
            }

            return ConsideringSimilarInstructionLoad(payload.id.Value);
        }

        public vSave010View ConsideringSimilarInstructionRuleSave(Save010InstructionRuleAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010InstructionRule>();

                payload.considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString();
                payload.considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString();
                payload.considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString();

                if (payload.instruction_rule_code == ConsideringInstructionRuleCodeConstant.ROLE_7.ToString()) {
                    if (repo.Query(r => r.save_id == payload.save_id && r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.NOT_SPECIFIC.ToString() && r.considering_instruction_status_code != ConsideringInstructionStatusCodeConstant.DELETE.ToString()).Count() == 0) {
                        var instruction_rule_entity = new Save010InstructionRule() {
                            save_id = payload.save_id,
                            instruction_date = DateTime.Now,
                            instruction_rule_code = ConsideringInstructionRuleCodeConstant.NOT_SPECIFIC.ToString(),
                            considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                            considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                            considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                        };
                        repo.Add(instruction_rule_entity);
                    }
                } else if (payload.instruction_rule_code == ConsideringInstructionRuleCodeConstant.ROLE_9.ToString()) {
                    var item_role_9_repo = uow.GetRepository<Save010InstructionRule9ItemSubType1>();
                    var item_role_9_entity_list = item_role_9_repo.Query(r => r.save_id == payload.save_id);
                    foreach (var item_role_9_entity in item_role_9_entity_list) {
                        item_role_9_entity.is_deleted = true;
                    }

                    foreach (var role9_item_sub_type1 in payload.role9_item_sub_type1_list) {
                        var item_role_9_entity = item_role_9_entity_list.Where(r => r.save010_product_id == role9_item_sub_type1.id).FirstOrDefault();
                        if (item_role_9_entity == null) {
                            item_role_9_entity = new Save010InstructionRule9ItemSubType1() {
                                save_id = payload.save_id,
                                save010_product_id = role9_item_sub_type1.id,
                            };
                        }

                        item_role_9_entity.suggestion_item_id = role9_item_sub_type1.suggestion_item_id;
                        item_role_9_entity.item_id = role9_item_sub_type1.item_id;
                        item_role_9_entity.item_code = role9_item_sub_type1.item_code;
                        item_role_9_entity.item_name = role9_item_sub_type1.item_name;
                        item_role_9_entity.is_deleted = false;

                        item_role_9_repo.Update(item_role_9_entity);
                    }
                }

                var model = mapper.Map<Save010InstructionRule>(payload);
                var entity = repo.Update(model);
                uow.SaveChanges();

                var instruction_rule_request_repo = uow.GetRepository<Save010InstructionRuleRequestNumber>();
                if (payload.request_number_save_list != null) {
                    foreach (var request_number_save in payload.request_number_save_list) {
                        instruction_rule_request_repo.Update(new Save010InstructionRuleRequestNumber() {
                            id = request_number_save.id.HasValue ? request_number_save.id.Value : 0,
                            instruction_rule_id = model.id,
                            request_number = request_number_save.request_number,
                            is_deleted = request_number_save.is_deleted,
                        });
                    }
                }
                uow.SaveChanges();
            }

            return ConsideringSimilarInstructionLoad(payload.save_id.Value);
        }

        public vSave010View ConsideringSimilarInstructionDelete(Save010InstructionRuleAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_id = 0l;
                var repo = uow.GetRepository<Save010InstructionRule>();
                if (payload.ids != null) {
                    foreach (long id in payload.ids) {
                        var entity = repo.Get(id);
                        save_id = entity.save_id.Value;
                        entity.considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DELETE.ToString();
                        entity.rule_remark = payload.cancel_reason;
                        repo.Update(entity);
                    }
                }
                uow.SaveChanges();

                return Save010Get(save_id);
            }
        }

        public vSave010View ConsideringSimilarInstructionPublicAdd(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010InstructionRule>();
                var entity_list = repo.Query(r => r.save_id == id && r.considering_instruction_status_code != ConsideringInstructionStatusCodeConstant.DELETE.ToString());

                if (repo.Query(r => r.save_id == id &&
                    r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.PUBLIC.ToString() &&
                    r.considering_instruction_status_code != ConsideringInstructionStatusCodeConstant.DELETE.ToString()
                ).Count() == 0) {
                    if (entity_list.Where(r =>
                    r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.SPECIFIC.ToString() ||
                    r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.NOT_SPECIFIC.ToString()).Count() == 0
                    ) {
                        var instruction_rule_entity = new Save010InstructionRule() {
                            save_id = id,
                            instruction_date = DateTime.Now,
                            instruction_rule_code = ConsideringInstructionRuleCodeConstant.SPECIFIC.ToString(),
                            considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                            considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                            considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                        };
                        repo.Add(instruction_rule_entity);
                        //uow.SaveChanges();
                        //return Save010Get(id);
                    }

                    var model = new Save010InstructionRuleAddModel() {
                        save_id = id,
                        instruction_date = DateTime.Now,

                        instruction_rule_code = ConsideringInstructionRuleCodeConstant.PUBLIC.ToString(),
                        considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                        considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                        considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                    };
                    repo.Add(mapper.Map<Save010InstructionRule>(model));

                    uow.SaveChanges();
                }
            }

            return Save010Get(id);
        }

        public vSave010View ConsideringSimilarInstructionPublicDelete(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                //var save_repo = uow.GetRepository<Save010>();
                //var save = save_repo.Get(id);

                var repo = uow.GetRepository<Save010InstructionRule>();
                var entity = repo.Query(r => r.save_id == id &&
                    r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.PUBLIC.ToString() &&
                    r.considering_instruction_status_code != ConsideringInstructionStatusCodeConstant.DELETE.ToString()
                ).FirstOrDefault();

                if (entity != null) {
                    entity.considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DELETE.ToString();
                    repo.Update(entity);

                    //if (!string.IsNullOrEmpty(save.public_receive_status_code)) {
                    //    save.public_receive_status_code = PublicReceiveStatusCodeConstant.WAIT_CHANGE.ToString();
                    //}
                    //save_repo.Update(save);

                    uow.SaveChanges();
                }
            }

            return Save010Get(id);
        }

        public vSave010View ConsideringSimilarInstructionSend(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var document_role_02_repo = uow.GetRepository<vDocumentRole02>();
                var post_round_instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();
                var repo = uow.GetRepository<Save010InstructionRule>();
                var post_round_repo = uow.GetRepository<PostRound>();

                //
                var document_role4_release_repo = uow.GetRepository<vDocumentRole04ReleaseRequest>();
                var save_tag_repo = uow.GetRepository<Save010CheckingTagSimilar>();
                var save_tag_ir_repo = uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();
                //

                var public_service = new PublicProcessService(configuration, uowProvider, mapper);

                var entity_list = repo.Query(r => r.save_id == id && r.considering_instruction_status_code != ConsideringInstructionStatusCodeConstant.DELETE.ToString()).ToList();

                var entity_draft_list = entity_list.Where(r => r.considering_instruction_status_code == ConsideringInstructionStatusCodeConstant.DRAFT.ToString()).ToList();
                if (entity_draft_list.Count() > 0) {
                    if (entity_draft_list.Where(r => r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.ROLE_7.ToString()).Count() > 0) {
                        if (entity_list.Where(r => r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.NOT_SPECIFIC.ToString()).Count() == 0) {
                            var instruction_rule_entity = new Save010InstructionRule() {
                                save_id = id,
                                instruction_date = DateTime.Now,
                                instruction_rule_code = ConsideringInstructionRuleCodeConstant.NOT_SPECIFIC.ToString(),
                                considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                                considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                                considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                            };
                            repo.Add(instruction_rule_entity);
                            uow.SaveChanges();
                            return Save010Get(id);
                        }
                    } else {
                        if (entity_list.Where(r => r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.SPECIFIC.ToString()).Count() == 0) {
                            var instruction_rule_entity = new Save010InstructionRule() {
                                save_id = id,
                                instruction_date = DateTime.Now,
                                instruction_rule_code = ConsideringInstructionRuleCodeConstant.SPECIFIC.ToString(),
                                considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                                considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT.ToString(),
                                considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
                            };
                            repo.Add(instruction_rule_entity);
                            uow.SaveChanges();
                            return Save010Get(id);
                        }
                    }

                    int book_expired_day = 60;
                    var post_round_entity = new PostRound() {
                        //post_round_instruction_rule_code = entity.instruction_rule_code,
                        post_round_type_code = PostRoundTypeCodeConstant.SEND_MORE.ToString(),
                        //object_id = entity.save_id,
                        round_index = 1,
                        book_by = userId,

                        // Do it at document-role02-print-document
                        //book_number = HelpService.GetBookNumber(uowProvider),
                        //book_start_date = DateTime.Now,
                        //book_end_date = DateTime.Now.AddDays(book_expired_day),
                        //book_expired_day = book_expired_day,

                        //No neccessory to considering
                        post_round_document_post_status_code = PostRoundDocumentPostStatusCodeConstant.DONE.ToString(),
                        post_round_document_post_date = DateTime.Now,

                        document_role02_status_code = DocumentRole02StatusCodeConstant.WAIT.ToString(),
                    };

                    using (var _uow = uowProvider.CreateUnitOfWork()) {
                        var _post_round_repo = _uow.GetRepository<PostRound>();
                        _post_round_repo.Add(post_round_entity);
                        _uow.SaveChanges();
                    }

                    foreach (var entity in entity_draft_list) {
                        entity.considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.SEND.ToString();
                        entity.instruction_send_date = DateTime.Now;

                        //Send to document-role02-item/list
                        if (entity.instruction_rule_code.StartsWith("CASE_") ||
                            entity.instruction_rule_code.StartsWith("ROLE_")) {


                            using (var _uow = uowProvider.CreateUnitOfWork()) {
                                var _post_round_instruction_rule_repo = _uow.GetRepository<PostRoundInstructionRule>();
                                _post_round_instruction_rule_repo.Add(new PostRoundInstructionRule() {
                                    post_round_id = post_round_entity.id,
                                    save010_instruction_rule_id = entity.id,
                                    document_role02_check_status_code = DocumentRole02CheckStatusCodeConstant.DRAFT.ToString(),
                                });
                                _uow.SaveChanges();
                            }

                            //entity.post_round_id = post_round_entity.id;
                            //entity.document_role02_check_status_code = DocumentRole02CheckStatusCodeConstant.DRAFT.ToString();
                        }


                        repo.Update(entity);
                    }

                    var save_repo = uow.GetRepository<Save010>();
                    var save_entity = save_repo.Get(id);
                    if (save_entity.considering_receive_status_code == ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString()) {
                        save_entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.SEND_FIX.ToString();
                    } else if (save_entity.considering_receive_status_code == ConsideringReceivingStatusCodeConstant.DRAFT_CHANGE.ToString()) {
                        save_entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.SEND_CHANGE.ToString();
                    } else {
                        save_entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.SEND.ToString();
                    }
                    save_entity.considering_receive_date = DateTime.Now;
                    save_repo.Update(save_entity);
                    uow.SaveChanges();

                    if (entity_draft_list.Where(r => r.instruction_rule_code == ConsideringInstructionRuleCodeConstant.PUBLIC.ToString()).Count() > 0) {
                        if (save_entity.public_status_code == null) {
                            public_service.ForcePublic(id, PublicTypeCodeConstant.PUBLIC, PublicSourceCodeConstant.CHECKING);
                        } else if (save_entity.public_receive_status_code == PublicReceiveStatusCodeConstant.WAIT_CHANGE.ToString()) {
                            save_entity = save_repo.Get(id);
                            save_entity.public_receive_status_code = PublicReceiveStatusCodeConstant.DRAFT_CHANGE.ToString();
                            save_repo.Update(save_entity);
                            uow.SaveChanges();
                        }
                    }
                }

                // Check what didnot finish in Document Role 4 Release Request
                using (var _uow = uowProvider.CreateUnitOfWork()) {
                    var _repo = _uow.GetRepository<vDocumentRole04ReleaseRequest>();
                    var _entity_list = _repo.Query(r => r.save_id == id &&
                        r.document_role04_release_request_send_type_code == DocumentRole04ReleaseRequestSendTypeCodeConstant.CHECKING.ToString() &&
                        r.document_role05_receive_status_code == DocumentRole05ReceiveStatusCodeConstant.WAIT_CHANGE.ToString());

                    var _sir_reop = _uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();
                    foreach (var _entity in _entity_list) {
                        var _sir = _sir_reop.Get(_entity.id);
                        _sir.document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.DRAFT_CHANGE.ToString();
                        _sir_reop.Update(_sir);
                    }
                    _uow.SaveChanges();
                }

                // Document Role 2 send back to change
                var entity_draft_fix_list = entity_list.Where(r => r.considering_instruction_status_code == ConsideringInstructionStatusCodeConstant.DRAFT_FIX.ToString()).ToList();
                if (entity_draft_fix_list.Count() > 0) {
                    foreach (var entity in entity_draft_fix_list) {
                        entity.considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.SEND.ToString();
                        repo.Update(entity);

                        var document_role_02 = document_role_02_repo.Query(r => r.save_id == entity.save_id && r.instruction_rule_id == entity.id).LastOrDefault();
                        var post_round_instruction_rule = post_round_instruction_rule_repo.Get(document_role_02.post_round_instruction_rule_id.Value);
                        post_round_instruction_rule.document_role02_check_status_code = DocumentRole02CheckStatusCodeConstant.DRAFT_CHANGE.ToString();
                        post_round_instruction_rule_repo.Update(post_round_instruction_rule);

                        using (var _uow = uowProvider.CreateUnitOfWork()) {
                            var _post_round_repo = _uow.GetRepository<PostRound>();
                            var _post_round = _post_round_repo.Get(post_round_instruction_rule.id);
                            _post_round.document_role02_receive_status_code = DocumentRole02ReceiveStatusCodeConstant.DRAFT_CHANGE.ToString();
                            _post_round_repo.Update(_post_round);
                            _uow.SaveChanges();
                        }
                    }

                    var save_repo = uow.GetRepository<Save010>();
                    var save_entity = save_repo.Get(id);
                    save_entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.SEND_FIX.ToString();
                    save_repo.Update(save_entity);
                }
                //

                uow.SaveChanges();
            }

            return Save010Get(id);
        }

        public ConsideringSimilarDocumentView ConsideringSimilarDocumentLoad(long id) {
            var view = new ConsideringSimilarDocumentView();
            view.id = id;

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                view.request_number = repo.Get(id).request_number;

                var instruction_rule_repo = uow.GetRepository<vSave010CheckingInstructionDocument>();
                view.instruction_rule_list = mapper.Map<List<vSave010CheckingInstructionDocumentView>>(instruction_rule_repo.Query(r =>
                r.save_id == id &&
                (
                r.instruction_rule_code.StartsWith("ROLE") ||
                r.instruction_rule_code.StartsWith("CASE"))
                ));

                //TODO AJ Get from another source
                //if (view.instruction_rule_list.Count() > 0) {
                //    view.request_number = view.instruction_rule_list.FirstOrDefault().request_number;
                //}
                var save_repo = uow.GetRepository<vSave010CheckingSaveDocument>();
                view.save_list = mapper.Map<List<vSave010CheckingSaveDocumentView>>(save_repo.Query(r => r.request_number == view.request_number));
            }

            return view;
        }

        public Save010Case28ProcessView ConsideringSimilarKor10Load(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010Case28Process>();
                var entity = repo.Query(r => r.save_id == id).FirstOrDefault();
                if (entity == null) {

                    entity = new Save010Case28Process() {
                        save_id = id,
                    };
                }

                var view = mapper.Map<Save010Case28ProcessView>(entity);

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(id);
                view.request_number = save.request_number;


                var case28 = uow.GetRepository<Save010Case28>();
                var case28_list = case28.Query(r => r.save_id == id);
                view.case28_list = mapper.Map<List<Save010Case28View>>(case28_list);

                return view;
            }
        }

        public Save010Case28ProcessView ConsideringSimilarKor10Save(Save010Case28ProcessAddModel payload) {
            var view = new CheckingSimilarResultDuplicateView();
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010Case28Process>();
                var entity = repo.Update(mapper.Map<Save010Case28Process>(payload));

                var case28_repo = uow.GetRepository<Save010Case28>();
                foreach (var case28 in payload.case28_list) {
                    case28_repo.Update(mapper.Map<Save010Case28>(case28));
                }

                uow.SaveChanges();
                return ConsideringSimilarKor10Load(entity.save_id.Value);
            }
        }

        public Save010ProcessView CheckingSimilarWordSoundTranslateBackSend(Save010AddModel payload) {
            var view = new CheckingSimilarResultDuplicateView();
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var entity = repo.Get(payload.id.Value);

                entity.checking_receive_status_code = CheckingReceivingStatusCodeConstant.WAIT_CHANGE.ToString();
                entity.checking_remark = payload.checking_remark;

                entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.WAIT_CHANGE.ToString();

                repo.Update(entity);

                //var case28_repo = uow.GetRepository<Save010Case28>();
                //foreach (var case28 in payload.case28_list) {
                //    case28_repo.Update(mapper.Map<Save010Case28>(case28));
                //}

                uow.SaveChanges();
                return mapper.Map<Save010ProcessView>(entity);
            }
        }

        vSave010View Save010Get(long id) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
            var view = service.Get(id);

            var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
            view.full_view = service_full_view.Get(id);

            return view;
        }

        public ConsideringSimilarDocumentView ConsideringSimilarDocumentSave(ConsideringSimilarDocumentAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var vSave010_repo = uow.GetRepository<vSave010>();
                var save010_repo = uow.GetRepository<Save010>();
                var document_service = new DocumentProcessService(configuration, uowProvider, mapper);
                foreach (var item in payload.save_list) {
                    if (!item.consider_similar_document_date.HasValue) {
                        item.consider_similar_document_date = DateTime.Now;
                    }
                    if (item.request_type_code == "40") {
                        var save_repo = uow.GetRepository<Save040>();
                        var entity = save_repo.Query(r => r.id == item.id).FirstOrDefault();

                        if (entity.consider_similar_document_status_code != item.consider_similar_document_status_code) {
                            entity.consider_similar_document_status_code = item.consider_similar_document_status_code;

                            if (entity.request_source_code == RequestSourceCodeConstant.EFORM.ToString()) {
                                if (item.consider_similar_document_status_code == ConsideringSimilarDocumentStatusCodeConstant.ALLOW.ToString()) {
                                    var save010 = save010_repo.Get(payload.id.Value);
                                    var save010_model = mapper.Map<Save010AddModel>(save010);

                                    foreach (var people in save010_model.people_list) {
                                        people.is_deleted = true;
                                    }
                                    foreach (var representative in save010_model.representative_list) {
                                        representative.is_deleted = true;
                                    }

                                    var save_model = mapper.Map<Save040AddModel>(entity);
                                    foreach (var people in save_model.people_list) {
                                        people.id = null;
                                        people.save_id = null;

                                        save010_model.people_list.Add(people);
                                    }
                                    foreach (var representative in save_model.representative_list) {
                                        representative.id = null;
                                        representative.save_id = null;

                                        save010_model.representative_list.Add(representative);
                                    }

                                    using (var _uow = uowProvider.CreateUnitOfWork()) {
                                        var _save010_repo = _uow.GetRepository<Save010>();
                                        _save010_repo.Update(mapper.Map<Save010>(save010_model));
                                        _uow.SaveChanges();
                                    }
                                }
                            }

                        }
                        if (entity.consider_similar_document_date != item.consider_similar_document_date) {
                            entity.consider_similar_document_date = item.consider_similar_document_date;
                        }
                        if (entity.consider_similar_document_remark != item.consider_similar_document_remark) {
                            entity.consider_similar_document_remark = item.consider_similar_document_remark;
                        }
                        if (entity.consider_similar_document_item_status_list != item.consider_similar_document_item_status_list) {
                            entity.consider_similar_document_item_status_list = item.consider_similar_document_item_status_list;
                        }
                        save_repo.Update(entity);
                    } else if (item.request_type_code == "60") {
                        var save_repo = uow.GetRepository<Save060>();
                        var entity = save_repo.Query(r => r.id == item.id).FirstOrDefault();
                        if (entity.consider_similar_document_status_code != item.consider_similar_document_status_code) {
                            entity.consider_similar_document_status_code = item.consider_similar_document_status_code;
                        }
                        if (entity.consider_similar_document_date != item.consider_similar_document_date) {
                            entity.consider_similar_document_date = item.consider_similar_document_date;
                        }
                        if (entity.consider_similar_document_remark != item.consider_similar_document_remark) {
                            entity.consider_similar_document_remark = item.consider_similar_document_remark;
                        }
                        if (entity.consider_similar_document_item_status_list != item.consider_similar_document_item_status_list) {
                            entity.consider_similar_document_item_status_list = item.consider_similar_document_item_status_list;

                            if (entity.consider_similar_document_item_status_list.IndexOf("\"TRADEMARK\":\"ALLOW\"") >= 0 ||
                                entity.consider_similar_document_item_status_list.IndexOf("\"SOUND_TRANSLATE\":\"ALLOW\"") >= 0) {
                                var save010 = vSave010_repo.Query(r => r.request_number == entity.request_number).FirstOrDefault();
                                document_service.ClassificationVersionAdd(save010.id, entity.id, true);
                            } else if (entity.consider_similar_document_item_status_list.IndexOf("\"TRADEMARK\":\"NOT_ALLOW\"") >= 0 &&
                                entity.consider_similar_document_item_status_list.IndexOf("\"SOUND_TRANSLATE\":\"NOT_ALLOW\"") >= 0) {
                                var save010 = vSave010_repo.Query(r => r.request_number == entity.request_number).FirstOrDefault();
                                document_service.ClassificationVersionAdd(save010.id, entity.id, false);
                            }
                        }
                        save_repo.Update(entity);
                    } else if (item.request_type_code == "120") {
                        var save_repo = uow.GetRepository<SaveOther>();
                        var entity = save_repo.Query(r => r.id == item.id).FirstOrDefault();
                        if (entity.consider_similar_document_status_code != item.consider_similar_document_status_code) {
                            entity.consider_similar_document_status_code = item.consider_similar_document_status_code;

                            if (entity.request_source_code == RequestSourceCodeConstant.EFORM.ToString()) {
                                if (item.consider_similar_document_status_code == ConsideringSimilarDocumentStatusCodeConstant.ALLOW.ToString()) {
                                    //If eform & allow what to do
                                }
                            }
                        }
                        if (entity.consider_similar_document_date != item.consider_similar_document_date) {
                            entity.consider_similar_document_date = item.consider_similar_document_date;
                        }
                        if (entity.consider_similar_document_remark != item.consider_similar_document_remark) {
                            entity.consider_similar_document_remark = item.consider_similar_document_remark;
                        }
                        if (entity.consider_similar_document_item_status_list != item.consider_similar_document_item_status_list) {
                            entity.consider_similar_document_item_status_list = item.consider_similar_document_item_status_list;
                        }
                        save_repo.Update(entity);
                    }
                }

                var repo = uow.GetRepository<Save010InstructionRule>();
                foreach (var item in payload.instruction_rule_list) {
                    var entity = repo.Get(item.id.Value);
                    if (entity.considering_instruction_rule_status_code != item.considering_instruction_rule_status_code) {
                        entity.considering_instruction_rule_status_code = item.considering_instruction_rule_status_code;
                    }
                    if (entity.change_status_date != item.change_status_date) {
                        entity.change_status_date = item.change_status_date;
                    }
                    if (entity.change_reason != item.change_reason) {
                        entity.change_reason = item.change_reason;
                    }
                    repo.Update(entity);
                }

                uow.SaveChanges();
            }
            return ConsideringSimilarDocumentLoad(payload.id.Value);
        }

        public Save010CheckingTagSimilarMethodView CheckingTagSimilarMethod(CheckingSimilarSave010ListModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010CheckingTagSimilarMethod>();
                var entity = repo.Query(r => r.save_id == payload.request01_item_id).FirstOrDefault();
                return mapper.Map<Save010CheckingTagSimilarMethodView>(entity);
            }
        }

        public string CheckingSimilarGetRequestItemSubTypeGroup(string item_sub_type_list) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RM_RequestItemSubTypeGroup>();
                item_sub_type_list = " " + item_sub_type_list + " ";
                var item_sub_type_group_code = string.Join(" ", string.Join(" ", repo.Query(r => item_sub_type_list.IndexOf(" " + r.code + " ") >= 0).Select(r => r.value_1)).Split(" ").Distinct().OrderBy(r => Convert.ToInt64(r)));
                return item_sub_type_group_code;
            }
        }

        public CheckingSimilarResultDuplicateView CheckingSimilarResultDuplicateList(string request_number) {
            var view = new CheckingSimilarResultDuplicateView();
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<vCheckingSimilarResultDuplicate_Save>();
                var save_list = save_repo.Query(r => r.save_request_number == request_number).ToList();
                view.save_word_list = mapper.Map<List<vCheckingSimilarResultDuplicate_SaveView>>(save_list.Where(r => !r.method_5.Value && !r.method_5.Value));
                view.save_image_list = mapper.Map<List<vCheckingSimilarResultDuplicate_SaveView>>(save_list.Where(r => r.method_5.Value));
                view.save_sound_list = mapper.Map<List<vCheckingSimilarResultDuplicate_SaveView>>(save_list.Where(r => r.method_6.Value));

                var word_repo = uow.GetRepository<vCheckingSimilarResultDuplicate_Word>();
                var word_list = word_repo.Query(r => r.request_number == request_number).ToList();
                view.word_list = mapper.Map<List<vCheckingSimilarResultDuplicate_WordView>>(word_list);

                return view;
            }
        }


        public CheckingSimilarResultDuplicateView CheckingSimilarResultDuplicateAdd(CheckingSimilarResultDuplicateAddModel payload) {
            var view = new CheckingSimilarResultDuplicateView();
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var tag_repo = uow.GetRepository<Save010CheckingTagSimilar>();

                var save_list = new List<vCheckingSimilarResultDuplicate_SaveAddModel>();
                save_list.AddRange(payload.save_word_list);
                save_list.AddRange(payload.save_image_list);
                foreach (var save in save_list) {
                    var tag = tag_repo.Query(r => r.save_id == payload.save_id && r.save_tag_id == save.save_tag_id).FirstOrDefault();
                    if (tag == null) {
                        tag = new Save010CheckingTagSimilar() {
                            save_id = payload.save_id,
                            save_tag_id = save.save_tag_id,
                        };
                    }
                    tag.is_same = true;
                    tag.is_same_approve = false;
                    tag.method_1 = save.method_1;
                    tag.method_2 = save.method_2;
                    tag.method_3 = save.method_3;
                    tag.method_4 = save.method_4;
                    tag.method_5 = save.method_5;
                    tag.method_6 = save.method_6;
                    tag.method_7 = save.method_7;
                    tag.is_owner_same = false;
                    tag.is_deleted = false;
                    tag_repo.Update(tag);
                }

                var translate_repo = uow.GetRepository<Save010CheckingSimilarWordTranslate>();
                foreach (var word in payload.word_list) {
                    var translate = new Save010CheckingSimilarWordTranslate() {
                        save_id = payload.save_id,
                        word_translate_search = word.word_translate_search,
                        word_translate_dictionary_code = word.word_translate_dictionary_code,
                        word_translate_dictionary_other = word.word_translate_dictionary_other,
                        word_translate_sound = word.word_translate_sound,
                        word_translate_translate = word.word_translate_translate,
                        checking_word_translate_status_code = word.checking_word_translate_status_code,
                    };
                    translate_repo.Update(translate);
                }

                uow.SaveChanges();
                return view;
            }
        }

    }
}