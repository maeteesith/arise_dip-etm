﻿using DIP.TM.Models.Configs;
using DIP.TM.Models.Email;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Interfaces;
using DIP.TM.Utils.Logging;
using MailKit.Net.Smtp;
using MailKit.Security;
using Microsoft.Extensions.Configuration;
using MimeKit;
using System;

namespace DIP.TM.Services.Email
{
    public class BaseEmailService : IBaseEmailService
    {
        private readonly IConfiguration configuration;
        private readonly AppConfigModel configModel;
        public BaseEmailService(IConfiguration configuration)
        {
            this.configuration = configuration;
            this.configModel = configuration.ToConfig();
        }

        public bool Send(EmailModel emailModel)
        {
            try
            {
                var client = new SmtpClient();
                client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                client.Connect(this.configModel.Email.Host, this.configModel.Email.Port, SecureSocketOptions.StartTls);
                client.Authenticate(this.configModel.Email.Username, this.configModel.Email.Password);
                var msg = new MimeMessage();
                msg.From.Add(new MailboxAddress(this.configModel.Email.Name, this.configModel.Email.From));
                msg.To.Add(new MailboxAddress("Customer", emailModel.to));
                if (this.configModel.Email.Bcc != string.Empty)
                    msg.Bcc.Add(new MailboxAddress("admin team", this.configModel.Email.Bcc));
                msg.Subject = emailModel.subject;
                msg.Body = new TextPart("html")
                {
                    Text = emailModel.body
                };

                client.Send(msg);
                client.Disconnect(true);
                return true;
            }
            catch (Exception ex)
            {
                LineNotifyHelper.SendWait(configuration, ex.ToString());
                return false;
            }
        }

        /*public virtual bool SendWithAwsSes(EmailModel email)
        {
            if (this.configModel.Email.IsAws == false)
            {
                using (MailMessage mm = new MailMessage(configuration["Email:Username"], email.to))
                {
                    mm.Subject = email.subject;
                    mm.Body = email.body;

                    mm.IsBodyHtml = email.is_html;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = configuration["Email:Host"];
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(configuration["Email:Username"], configuration["Email:Password"]);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = int.Parse(configuration["Email:Port"]);
                    smtp.Send(mm);
                }
                return true;
            }
            else
            {
                String FROM = configuration["Email:From"];
                String FROMNAME = configuration["Email:Name"];
                String TO = email.to;
                String SMTP_USERNAME = configuration["Email:Username"];
                String SMTP_PASSWORD = configuration["Email:Password"];
                String CONFIGSET = "ConfigSet";
                String HOST = configuration["Email:Host"];
                int PORT = int.Parse(configuration["Email:Port"]);
                String SUBJECT = email.subject;
                String BODY = email.body;
                MailMessage message = new MailMessage();
                message.IsBodyHtml = true;
                message.From = new MailAddress(FROM, FROMNAME);
                message.To.Add(new MailAddress(TO));
                message.Subject = SUBJECT;
                message.Body = BODY;
                message.Headers.Add("X-SES-CONFIGURATION-SET", CONFIGSET);

                using (var client = new System.Net.Mail.SmtpClient(HOST, PORT))
                {
                    // Pass SMTP credentials
                    client.Credentials =
                        new NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

                    // Enable SSL encryption
                    client.EnableSsl = true;

                    // Try to send the message. Show status in console.
                    try
                    {
                        client.Send(message);
                        return true;
                    }
                    catch (Exception ex)
                    {
                        LineNotifyHelper.SendWait(configuration, ex.ToString());
                        return false;
                    }
                }
            }*/
    }
}