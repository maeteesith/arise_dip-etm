﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Services.ReferenceMasters {
    public class LocationService {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public LocationService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }
        [LogAopInterceptor]
        public virtual List<vLocationView> List(BasePageModelPayload<RS_AddressAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                int _take = payload.paging == null || payload.paging.item_per_page == 0 ? 5 : payload.paging.item_per_page;

                var repo = uow.GetRepository<vLocation>();
                var object_list = repo.Query(
                        s =>
                        (s.name +
                        s.postal_name +
                        s.district_name +
                        s.province_name).IndexOf(payload.filter.name) >= 0
                    ).Take(_take);
                return mapper.Map<List<vLocationView>>(object_list);
            }
        }
    }
}
