﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Extensions;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace DIP.TM.Services.Saves {
    public class SaveProductService<P, D, V> where P : SaveProductAddModel where D : SaveProductEntityBase where V : SaveProductView {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public SaveProductService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }


        [LogAopInterceptor]
        public virtual List<V> Save(List<P> payload_list, long save_id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var entity_list = repo.Query(r => r.save_id == save_id).ToList();
                var product_list = mapper.Map<List<P>>(entity_list);

                foreach (var product in product_list) {
                    if (!product.is_deleted) product.is_deleted = true;
                }

                foreach (var payload in payload_list) {
                    foreach (var description in payload.description.Split(new string[] { "  ", "\n" }, StringSplitOptions.RemoveEmptyEntries)) {
                        var product = product_list.Where(r =>
                        r.request_item_sub_type_1_code == payload.request_item_sub_type_1_code &&
                        r.description == description &&
                        r.save_id == save_id).FirstOrDefault();

                        if (product != null) product.is_deleted = false;
                        else {
                            product_list.Add(mapper.Map<P>(new SaveProductView() {
                                save_id = save_id,
                                request_item_sub_type_1_code = payload.request_item_sub_type_1_code,
                                description = description,
                            }));
                        }
                    }
                }

                entity_list.Clear();
                using (var uow_update = uowProvider.CreateUnitOfWork()) {
                    var repo_update = uow_update.GetRepository<D>();

                    foreach (var product in product_list) {
                        var entity = mapper.Map<D>(product);
                        repo_update.Update(entity);

                        entity_list.Add(entity);
                        uow_update.SaveChanges();
                    }

                    return mapper.Map<List<V>>(entity_list);
                }
            }
        }
    }
}
