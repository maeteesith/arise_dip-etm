﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Extensions;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace DIP.TM.Services.Saves {
    public class SaveProcessService<P, D, V> where P : SaveModel where D : SaveEntityBase where V : BaseSaveView {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public SaveProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }
        [LogAopInterceptor]
        public virtual V Add(SaveModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var entity = mapper.Map<D>(payload);
                repo.Add(entity);
                uow.SaveChanges();
                return mapper.Map<V>(entity);
            }
        }
        [LogAopInterceptor]
        public virtual V Add(ReceiptItem item) {
            var save_list = List(mapper.Map<P>(new SaveModel() {
                request_id = item.request_id.Value,
                request_number = item.request_number,
            }));

            if (save_list.Count() == 0) {
                var save = new SaveModel() {
                    request_id = item.request_id.Value,
                    //request_index = item.book_index,
                    request_number = item.request_number,
                    request_date = item.receipt_.request_date.Value,
                    make_date = DateTime.Now,
                    total_price = item.total_price,
                    request_source_code = item.source_code,
                    save_status_code = SaveStatusCodeConstant.DRAFT.ToString(),
                };

                using (var uow = uowProvider.CreateUnitOfWork()) {
                    var repo = uow.GetRepository<RequestOtherItemSub>();
                    var request_other_item_sub = repo.Query(r => r.request_other_item_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();
                    if (request_other_item_sub != null) {
                        save.request_index = request_other_item_sub.request_index;
                    }
                };

                return Add(save);
            }

            return mapper.Map<V>(save_list[0]);
        }

        [LogAopInterceptor]
        public virtual V Get(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var entity = repo.Get(id);

                V view = mapper.Map<V>(entity);
                GetSave01Detail(view);
                return view;
            }
        }

        [LogAopInterceptor]
        public virtual V Save(dynamic _payload) {
            return Save(JsonConvert.DeserializeObject<P>(Convert.ToString(_payload)));
        }
        [LogAopInterceptor]
        public virtual V Save(P payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                D data = mapper.Map<D>(payload);
                repo.Update(data);
                uow.SaveChanges();

                V view = mapper.Map<V>(data);
                return Get(view.id.Value);
            }
        }


        [LogAopInterceptor]
        public virtual V Send(dynamic _payload) {
            return Send(JsonConvert.DeserializeObject<P>(Convert.ToString(_payload)));
        }
        [LogAopInterceptor]
        public virtual V Send(P payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();

                payload.save_status_code = SaveStatusCodeConstant.SENT.ToString();
                //TODO Not All Save sent to consider
                payload.consider_similar_document_status_code = ConsideringSimilarDocumentStatusCodeConstant.WAIT_CONSIDER.ToString();
                payload.department_send_date = DateTime.Now;

                D data = mapper.Map<D>(payload);
                repo.Update(data);
                uow.SaveChanges();

                V view = mapper.Map<V>(data);
                return Get(view.id.Value);
            }
        }

        [LogAopInterceptor]
        public virtual List<V> Send(List<vSaveSendModel> payload_list) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();

                foreach (var payload in payload_list.Where(r => r.is_check)) {
                    var entity = repo.Get(payload.id.Value);

                    var model = mapper.Map<P>(entity);

                    model.save_status_code = SaveStatusCodeConstant.SENT.ToString();
                    //TODO Not All Save sent to consider
                    model.consider_similar_document_status_code = ConsideringSimilarDocumentStatusCodeConstant.WAIT_CONSIDER.ToString();
                    model.department_send_code = payload.department_send_code;
                    model.department_send_date = DateTime.Now;

                    //D data = mapper.Map<D>(payload);
                    Save(model);
                }

                uow.SaveChanges();

                return mapper.Map<List<V>>(null);
            }
        }


        [LogAopInterceptor]
        public virtual V RequestBack(dynamic _payload) {
            return RequestBack(JsonConvert.DeserializeObject<P>(Convert.ToString(_payload)));
        }
        [LogAopInterceptor]
        public virtual V RequestBack(P payload) {

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();

                payload.save_status_code = SaveStatusCodeConstant.DRAFT.ToString();
                //TODO Not All Save sent to consider
                payload.consider_similar_document_status_code = null;

                D data = mapper.Map<D>(payload);
                repo.Update(data);
                uow.SaveChanges();

                V view = mapper.Map<V>(data);
                return Get(view.id.Value);
            }
        }


        [LogAopInterceptor]
        public virtual V eFormFromRequestNumberLoad(string request_number, string eform_code) {
            D entity;
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                entity = repo.Query(r => r.request_number == request_number).FirstOrDefault();

                V view = mapper.Map<V>(entity);
                GetSave01Detail(view);

                view = LoadEForm(view, view.request_number, eform_code);

                return view;
            }
        }

        V LoadEForm(V view, string request_number, string eform_code) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var isEntityNull = (view == null) ? true : false;
                //var request_number = (!isEntityNull) ? view.request_number : "";

                if (view == null) {
                    view = mapper.Map<V>(new Save010());
                }

                if (request_number == "") {
                    view.is_search_success = false;
                    view.alert_msg = "ไม่มีเลขที่คำขอนี้ในระบบฐานข้อมูล";

                    return view;
                }

                view.is_search_success = true;
                var s010_view = mapper.Map<Save010ProcessView>(view);

                var dtn = DateTime.Now;

                if (s010_view != null) {
                    if ((s010_view.trademark_expired_date.HasValue) ? DateTime.Compare(dtn, s010_view.trademark_expired_date.Value) > 0 : true) {
                        view.is_search_success = false;
                        view.alert_msg = "ไม่สามารถยื่นได้ เนื่องจากเลขที่คำขอหมดอายุ";
                    }

                    if (eform_code == "020" || eform_code == "021") {
                        var public_repo = uow.GetRepository<PublicRound>();
                        //var public_entity = public_repo.Get((s010_entity.public_round_id.HasValue) ? s010_entity.public_round_id.Value : 0);
                        var public_entity = public_repo.Query(r => r.id == (s010_view.public_round_id.HasValue ? s010_view.public_round_id.Value : 0)).FirstOrDefault();
                        var public_view = mapper.Map<PublicRoundView>(public_entity);

                        if (public_entity != null) {
                            var save010_list = public_view.save010_list.Where(r => r.request_number == request_number).ToList();

                            public_view.save010_list = save010_list;
                            public_view.public_round_item = public_view.public_round_item.Where(r => r.save_id == s010_view.id).ToList();

                            if (eform_code == "020") {
                                if (public_view.public_start_date.HasValue) {
                                    if (DateTime.Compare(dtn, public_view.public_start_date.Value.AddDays(60)) > 0) {
                                        view.is_search_success = false;
                                        view.alert_msg = "ไม่สามารถคัดค้านเลขที่คำขอนี้ได้ เนื่องจากไม่อยู่ในระยะเวลาที่กำหนด";
                                    }
                                }
                            }

                            if (eform_code == "021") {
                                var s020_repo = uow.GetRepository<Save020>();
                                var s020_list = s020_repo.Query(r => r.request_number == request_number && r.public_round_id == public_entity.id).ToList();
                                var s020_view_list = mapper.Map<List<Save020ProcessView>>(s020_list);

                                var dispute_repo = uow.GetRepository<eForm_Save021Dispute>();
                                var dispute_list = dispute_repo.Query(r => r.save_id == ((view.id.HasValue) ? view.id.Value : 0) && !r.is_deleted).ToList();
                                var dispute_view_list = mapper.Map<List<eForm_Save021DisputeView>>(dispute_list);

                                if (s020_list != null && s020_list.Count() > 0) {
                                    s020_view_list.ForEach(item => {
                                        var post_repo = uow.GetRepository<PostRound>();
                                        var post_entity_list = post_repo.Query(r => r.object_id == item.id && r.post_round_instruction_rule_code == "RULE_6_1").ToList();
                                        var post_entity = post_entity_list.FirstOrDefault(w => w.created_date == post_entity_list.Max(m => m.created_date));
                                        var post_view = mapper.Map<PostRoundView>(post_entity);

                                        item.post_round = post_view;

                                        var dispute_item = dispute_view_list.Where(r => r.challenge_id == item.id).FirstOrDefault();
                                        item.is_active = (dispute_item != null) ? true : false;
                                        if (item.is_active) {
                                            dispute_view_list.Remove(dispute_item);
                                        }

                                        item.is_remove = false;
                                        if (post_entity != null) {
                                            if (post_view.book_acknowledge_date.HasValue) {
                                                if (DateTime.Compare(dtn, post_view.book_acknowledge_date.Value.AddDays(60)) > 0) {
                                                    item.is_remove = true;
                                                }

                                            }
                                        } else {
                                            item.is_remove = true;
                                        }
                                    });

                                    public_view.save020_list = s020_view_list.Where(w => !w.is_remove).ToList();
                                }

                                if (isEntityNull) {
                                    //waiting for tell netcube before remove this if
                                    var view_021 = mapper.Map<eForm_Save021View>(view);
                                    view_021.people_list = s010_view.people_list; //change to people_load_list
                                    view_021.representative_list = s010_view.representative_list; //change to representative_load_list
                                    view = mapper.Map<V>(view_021);
                                }
                            }

                            view.public_round = public_view;
                        } else {
                            view.is_search_success = false;
                            view.alert_msg = "ไม่สามารถยื่นได้ กรุณาติดต่อเจ้าหน้าที่";
                        }
                    } else if (eform_code == "030") {
                        var post_repo = uow.GetRepository<PostRound>();
                        var post_entity_list = post_repo.Query(r => r.object_id == s010_view.id/* && r.post_round_instruction_rule_code == "RULE_?"*/).ToList(); //waiting confirm for rule_code
                        var post_entity = post_entity_list.FirstOrDefault(w => w.created_date == post_entity_list.Max(m => m.created_date));
                        var post_view = mapper.Map<PostRoundView>(post_entity);

                        if (post_entity != null) {
                            if (post_view.book_acknowledge_date.HasValue) {
                                if (DateTime.Compare(dtn, post_view.book_acknowledge_date.Value.AddDays(60)) > 0) {
                                    view.is_search_success = false;
                                    view.alert_msg = "ไม่สามารถยื่นได้ เนื่องจากเกินระยะเวลาที่กำหนด";
                                }

                            }
                        } else {
                            view.is_search_success = false;
                            view.alert_msg = "ไม่สามารถยื่นได้ กรุณาติดต่อเจ้าหน้าที่";
                        }
                    } else if (eform_code == "070") {
                        var expDate = s010_view.trademark_expired_date;

                        var s070_repo = uow.GetRepository<Save070>();
                        var s070_entity = s070_repo.Query(r => r.request_number == request_number && r.make_date >= dtn.AddDays(-60)/* && r.status == "รอดำเนินการ"*/).FirstOrDefault(); //waiting db to filter with status รอดำเนินการ ในงวด3

                        if (expDate != null && expDate.HasValue) {
                            if (DateTime.Compare(dtn, expDate.Value.AddDays(-90)) < 0 || DateTime.Compare(dtn, expDate.Value.AddDays(180)) > 0) {
                                view.is_search_success = false;
                                view.alert_msg = "ไม่สามารถต่ออายุได้ เนื่องจากคำขอนี้ไม่ได้อยู่ในช่วงต่ออายุ กรุณาติดต่อเจ้าหน้าที่";
                            } else if (s070_entity != null) {
                                view.is_search_success = false;
                                view.alert_msg = "มีการยื่นต่ออายุของเลขที่คำขอนี้แล้ว ไม่สามารถยื่นซ้ำได้";
                            } else if (s070_entity == null) {
                                if (DateTime.Compare(dtn, expDate.Value) > 0 && DateTime.Compare(dtn, expDate.Value.AddDays(180)) <= 0) {
                                    view.is_search_success = true;
                                    view.alert_msg = "มีค่าปรับในการต่ออายุ";
                                }

                            }
                        }
                    }

                    //view.save010 = s010_view;
                    view.registration_load_number = s010_view.registration_number;
                    view.product_load_list = s010_view.product_list;
                    if (typeof(V) != typeof(eForm_Save020View)) {
                        view.people_load_list = s010_view.people_list;
                        view.representative_load_list = s010_view.representative_list;
                    }
                    if (typeof(V) != typeof(eForm_Save220View)) {
                        //mock for eform 220 and will be removed in 3rd round
                        view.inter_registration_load_number = "300300003";
                        view.inter_registration_load_date = DateTime.Now;
                        view.inter_registration_cancel_load_date = DateTime.Now;
                        view.protection_load_date = DateTime.Now;
                        view.case_28_load_date = DateTime.Now;
                    }
                } else {
                    view.is_search_success = false;
                    view.alert_msg = "ไม่พบเลขที่คำขอนี้ในระบบ";
                }

                return view;
            }
        }


        [LogAopInterceptor]
        public virtual V Send(Save010AddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                payload.save_status_code = SaveStatusCodeConstant.SENT.ToString();
                payload.saved_process_date = DateTime.Now;
                payload.document_status_code = DocumentStatusCodeConstant.WAIT_DOCUMENTING.ToString();
                //payload.document_classification_status_code = DocumentClassificationStatusCodeConstant.WAIT_SPLIT_JOB.ToString();
                D data = mapper.Map<D>(payload);
                repo.Update(data);
                uow.SaveChanges();

                V view = mapper.Map<V>(data);
                return Get(view.id.Value);
            }
        }

        [LogAopInterceptor]
        public virtual V Delete(dynamic payload) {
            return Delete(JsonConvert.DeserializeObject<P>(Convert.ToString(payload)));
        }

        [LogAopInterceptor]
        public virtual V Delete(P payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                if (payload.id.HasValue) {
                    return Delete(payload.id.Value, payload.cancel_reason);
                }
                if (payload.ids != null) {
                    foreach (long id in payload.ids) {
                        Delete(id, payload.cancel_reason);
                    }
                }
                return mapper.Map<V>(null);
            }
        }

        [LogAopInterceptor]
        public virtual V Delete(long id, string cancel_reason = null) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var entity = repo.Get(id);

                var new_payload = mapper.Map<P>(entity);
                new_payload.save_status_code = SaveStatusCodeConstant.DELETE.ToString();
                new_payload.cancel_reason = cancel_reason;

                //entity.save_status_code = SaveStatusCodeConstant.DELETE.ToString();
                //entity.cancel_reason = cancel_reason;

                //repo.Update(entity);
                //uow.SaveChanges();

                //return mapper.Map<V>(entity);

                return Save(new_payload);
            }
        }

        [LogAopInterceptor]
        internal virtual V LoadFromRequest(long request_id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var request01 = repo.Query(r => r.request_id == request_id).FirstOrDefault();
                return mapper.Map<V>(request01);
            }
        }
        [LogAopInterceptor]
        public virtual List<V> List(dynamic _payload) {
            return List(JsonConvert.DeserializeObject<P>(Convert.ToString(_payload)));
        }

        [LogAopInterceptor]
        public virtual List<V> List(P payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                List<D> data_list = repo.Query(r =>
                (payload.request_id == 0 || r.request_id == payload.request_id) &&
                (string.IsNullOrEmpty(payload.request_number) || r.request_number == payload.request_number) &&
                (string.IsNullOrEmpty(payload.request_source_code) || payload.request_source_code == RequestSourceCodeConstant.ALL.ToString() || r.request_source_code == payload.request_source_code) &&
                (!payload.make_date.HasValue || r.make_date.Value.Date == payload.make_date.Value.Date) &&
                (string.IsNullOrEmpty(payload.request_index) || r.request_index == payload.request_index) &&
                (string.IsNullOrEmpty(payload.save_status_code) || payload.save_status_code == SaveStatusCodeConstant.ALL.ToString() || r.save_status_code == payload.save_status_code &&
                !r.is_deleted)
                , r => r.OrderByDescending(s => s.id)).ToList();

                List<V> view_list = mapper.Map<List<V>>(data_list);

                foreach (V view in view_list) {
                    GetSave01Detail(view);
                }

                return view_list;
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponsePageView<List<V>> ListPage(PageRequest payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var itemList = repo.Filters(x => x.is_deleted == false);
                var filterList = itemList.FilterDynamic(payload);
                payload.item_total = filterList.Count();
                if ((payload.page_index - 1) * payload.item_per_page > payload.item_total) {
                    payload.page_index = 1;
                }
                var resultPage = filterList.Page(payload.page_index, payload.item_per_page).ToList();

                var view_list = mapper.Map<List<V>>(resultPage);
                foreach (V view in view_list) {
                    GetSave01Detail(view);
                }

                return new BaseResponsePageView<List<V>>() {
                    data = new BaseResponsePageDataModel<List<V>>() {
                        list = view_list,
                        paging = payload
                    }
                };
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<List<V>> GetResponseListView(List<V> view_list) {
            return new BaseResponseView<List<V>>() { data = view_list };
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> GetResponseView(V view) {
            return new BaseResponseView<V>() { data = view };
        }


        [LogAopInterceptor]
        public virtual List<V> ListSave010(PageRequest payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vSave010>();
                //List<vSave010> data_list = repo.Query(r =>
                //(string.IsNullOrEmpty(payload.document_status_code) || payload.document_status_code == "ALL" || r.document_status_code == payload.document_status_code) &&
                //(string.IsNullOrEmpty(payload.save_status_code) || payload.save_status_code == "ALL" || r.save_status_code == payload.save_status_code) &&
                //(string.IsNullOrEmpty(payload.document_classification_status_code) || payload.document_classification_status_code == "ALL" || r.document_classification_status_code == payload.document_classification_status_code) &&
                //(string.IsNullOrEmpty(payload.checking_type_code) || payload.checking_type_code == "ALL" || r.checking_type_code == payload.checking_type_code) &&
                //(string.IsNullOrEmpty(payload.checking_status_code) || payload.checking_status_code == "ALL" || r.checking_status_code == payload.checking_status_code) &&
                //(!payload.checking_receiver_by.HasValue || payload.checking_receiver_by == r.checking_receiver_by) &&
                //(!payload.considering_receiver_by.HasValue || payload.considering_receiver_by == r.considering_receiver_by) &&
                //1 == 1
                //, r => r.OrderByDescending(s => s.updated_by)).ToList();
                var db_list = repo.Filters(x => x.is_deleted == false);
                var data_list = db_list.FilterDynamic(payload);
                payload.item_total = data_list.Count();
                if ((payload.page_index - 1) * payload.item_per_page > payload.item_total) {
                    payload.page_index = 1;
                }
                var resultPage = data_list.Page(payload.page_index, payload.item_per_page);

                var view_list = mapper.Map<List<V>>(resultPage);
                foreach (V view in view_list) {
                    GetSave01Detail(view);
                }
                return view_list;
            }
        }

        [LogAopInterceptor]
        public virtual vSave010View Save010Load(long id, bool is_full = false) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
            var view = service.Get(id);

            if (is_full) {
                var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                view.full_view = service_full_view.Get(id);
            }

            return view;
        }
        [LogAopInterceptor]
        public virtual List<V> ListNotSent(BasePageModelPayload<SaveListDraftModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var data_list = repo.Query(r =>
                    r.request_number == payload.filter.request_number &&
                    r.save_status_code == SaveStatusCodeConstant.DRAFT.ToString(),
                    s => s.OrderBy("created_date")
                );

                List<V> view_list = mapper.Map<List<V>>(data_list);

                foreach (V view in view_list) {
                    GetSave01Detail(view);
                }

                return view_list;
            }
        }

        void GetSave01Detail(V view) {
            try {
                using (var uow = uowProvider.CreateUnitOfWork()) {
                    var save_repo = uow.GetRepository<Save010>();
                    Save010 save = save_repo.Query(r => r.request_number == view.request_number).FirstOrDefault();

                    if (typeof(V) == typeof(Save010ProcessView)) {
                        if (save.sound_file_id.HasValue && save.sound_file_id > 0) {
                            var file_repo = uow.GetRepository<File>();
                            var file = file_repo.Get(save.sound_file_id.Value);
                            view.sound_file_physical_path = file.physical_path;
                        }

                        //if (save.public_round_id.HasValue && save.public_round_id > 0) {
                        //    var pr_repo = uow.GetRepository<PublicRound>();
                        //    var pr = pr_repo.Get(save.public_round_id.Value);
                        //    view.public_start_date = pr.public_start_date;
                        //}
                    }

                    if (typeof(V) == typeof(Save020ProcessView)) {
                        if (save.public_round_id.HasValue && save.public_round_id > 0) {
                            var pr_repo = uow.GetRepository<PublicRound>();
                            var pr = pr_repo.Get(save.public_round_id.Value);
                            view.public_start_date = pr.public_start_date;
                            view.book_index = pr.book_index;
                        }
                    }

                    view.trademark_status_code = save.trademark_status_code;
                    view.registration_number = save.registration_number;
                    view.people_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressPeople);
                    view.representative_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressRepresentative);
                    view.joiner_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressJoiner);
                    view.product_01_list = mapper.Map<List<SaveProductView>>(save.Save010Product);
                    view.contact_address_01 = mapper.Map<SaveAddressView>(save.Save010AddressContact.FirstOrDefault());
                }
            } catch (Exception _ex) {

            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<vRequestListView> LoadRequestList(string request_number) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<D>();
                var list_entity = repo.Query(r => r.request_number == request_number).ToList();
                var list_view = mapper.Map<List<V>>(list_entity);
                if (list_entity == null) {
                    return new BaseResponseView<vRequestListView>() { data = null };
                }

                vRequestListView view = new vRequestListView();
                List<vRequestView> vlist = new List<vRequestView>();

                list_view.ForEach(item => {
                    vRequestView vitem = new vRequestView();

                    vitem.id = item.id.HasValue ? item.id.Value : 0;
                    vitem.make_date = item.make_date;

                    if (typeof(V) == typeof(Save010ProcessView)) {
                        var request_item_01_repo = uow.GetRepository<Request01Item>();
                        var request_item_01_entity = request_item_01_repo.Query(r => r.id == item.request_id).FirstOrDefault();

                        if (request_item_01_entity != null) {
                            var request_01_repo = uow.GetRepository<Request01Process>();
                            var request_01_entity = request_01_repo.Query(r => r.id == request_item_01_entity.request01_id).FirstOrDefault();

                            if (request_01_entity != null) {
                                vitem.name = request_01_entity.requester_name;
                            }
                        }
                    } else {
                        var request_item_other_repo = uow.GetRepository<RequestOtherItem>();
                        var request_item_other_entity = request_item_other_repo.Query(r => r.id == item.request_id).FirstOrDefault();

                        if (request_item_other_entity != null) {
                            var request_other_repo = uow.GetRepository<RequestOtherProcess>();
                            var request_other_entity = request_other_repo.Query(r => r.id == request_item_other_entity.request_other_process_id).FirstOrDefault();

                            if (request_other_entity != null) {
                                vitem.name = request_other_entity.requester_name;
                            }
                        }
                    }

                    vlist.Add(vitem);
                });
                view.request_load_list = vlist;

                return new BaseResponseView<vRequestListView>() { data = view };
            }
        }
    }
}
