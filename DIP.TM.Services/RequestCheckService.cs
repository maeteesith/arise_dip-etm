﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Helps;
using DIP.TM.Services.Receipts;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;

namespace DIP.TM.Services.RequestChecks {
    public class RequestCheckService : BaseSaveView {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestCheckService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }
        [LogAopInterceptor]
        public virtual RequestCheckView RequestCheckSave(RequestCheckAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestCheck>();

                //if (string.IsNullOrEmpty(payload.voucher_number)) {
                payload.voucher_number = HelpService.GetRequestCheckVoucherNumber(uowProvider);
                //}
                //if (string.IsNullOrEmpty(payload.status_code)) {
                //    //payload.status_code = ReceiptStatusCodeConstant.UNPAID.ToString();
                //}

                var entity = mapper.Map<RequestCheck>(payload);
                repo.Update(entity);
                uow.SaveChanges();

                return RequestCheckLoad(entity.id);
            }
        }
        [LogAopInterceptor]
        public virtual RequestCheckView RequestCheckPrint(RequestCheckAddModel payload) {
            //if (string.IsNullOrEmpty(payload.reference_number)) {
            payload.reference_number = HelpService.GetReferenceNumber(uowProvider);
            //}
            var view = RequestCheckSave(payload);
            var entity = mapper.Map<RequestCheck>(view);

            var service = new ReceiptService(configuration, uowProvider, mapper);
            var receipt = service.Add(entity);

            return view;
        }
        [LogAopInterceptor]
        public virtual List<RequestCheckView> RequestCheckList(RequestCheckAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestCheck>();
                var entity = repo.QueryActive(r => r.reference_number == payload.reference_number);
                var view = mapper.Map<List<RequestCheckView>>(entity);
                return view;
            }
        }

        [LogAopInterceptor]
        public virtual RequestCheckView RequestCheckLoad(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestCheck>();
                var entity = repo.GetActive(id);
                var view = mapper.Map<RequestCheckView>(entity);

                var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
                foreach (var item in view.request_check_request_number_list) {
                    var save_view = service.List(new vSave010Model() {
                        request_number = item.request_number,
                    }).FirstOrDefault();

                    item.save_id = save_view.id;
                    item.trademark_status_code = save_view.trademark_status_code;
                }

                return view;
            }
        }

        [LogAopInterceptor]
        public virtual RequestCheckView RequestCheckVoucherNumberLoad(string voucher_number) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestCheck>();
                var entity = repo.Query(r => r.voucher_number == voucher_number).FirstOrDefault();
                return mapper.Map<RequestCheckView>(entity);
            }
        }
    }
}
