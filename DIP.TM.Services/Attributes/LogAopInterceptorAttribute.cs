﻿using AspectCore.DynamicProxy;
using DIP.TM.Models.Logging;
using DIP.TM.Services.Logging;
using DIP.TM.Utils.Extensions;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Services.Attributes
{
    public class LogAopInterceptorAttribute : AbstractInterceptorAttribute
    {
        private LibLogService _logger;
        public async override Task Invoke(AspectContext context, AspectDelegate next)
        {
            
            var methodName = context.GetMethodName();
            Type returnType = context.GetReturnType();
            var result = await RunAndGetReturn(context, next);
            if (!ReferenceEquals(_logger, null))
            {
                _logger.Log(new AuditLogModel()
                {
                    Method = methodName,
                    ResponseBody = JsonConvert.SerializeObject(result),
                    StatusCode = 1
                }); ;
            }
            else
            {
                _logger = context.GetLibLogService();
                if (!ReferenceEquals(_logger, null))
                {
                     _logger.Log(new AuditLogModel()
                    {
                        Method = methodName,
                        ResponseBody = result.ToJsonIgnoreNull(),
                        StatusCode = 1
                    }); ;
                }
            }
            return;
        }

        private async Task<object> RunAndGetReturn(AspectContext context, AspectDelegate next)
        {
            await context.Invoke(next);
            return context.IsAsync()
            ? await context.UnwrapAsyncReturnValue()
            : context.ReturnValue;
        }
    }

    public static class AopExtension
    {
        public static LibLogService GetLibLogService(this AspectContext context)
        {
            var logger = context.ServiceProvider.GetService(typeof(LibLogService));
            if (!ReferenceEquals(logger, null))
            {
                var _logger = logger as LibLogService;
                if (!ReferenceEquals(_logger, null))
                {
                    return _logger;
                }
            }
            return null;
        }
        public static Type GetReturnType(this AspectContext context)
        {
            return context.IsAsync()
                ? context.ServiceMethod.ReturnType.GetGenericArguments().First()
        : context.ServiceMethod.ReturnType;
        }

        public static string GetMethodName(this AspectContext context)
        {
            return $"{context.ServiceMethod.DeclaringType?.FullName}.{context.ServiceMethod?.Name}";
        }
    } 

}
