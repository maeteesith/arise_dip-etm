﻿using System;

namespace DIP.TM.Services.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class PropertyDTOMapAttribute : Attribute
    {
        public string SearchByLinqPath { get; set; }
        public string OrderByLinqPath { get; set; }

        public PropertyDTOMapAttribute(string searchByLinqPath, string orderByLinqPath = "")
        {
            SearchByLinqPath = searchByLinqPath;
            OrderByLinqPath = string.IsNullOrEmpty(orderByLinqPath) ? searchByLinqPath : orderByLinqPath;
        }
    }

    public enum PropertyLinqType
    {
        SearchBy = 0,
        OrderBy = 1
    };
}
