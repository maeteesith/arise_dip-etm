﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;
using DIP.TM.Services.Interfaces;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;

namespace DIP.TM.Services.Medias
{
    public class FileService: IFileService
    {
        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public FileService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        public FileView Add(FileView payload)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<File>();
                var file = new File();
                file = mapper.Map<File>(payload);
                repo.Add(file);
                uow.SaveChanges();
                return mapper.Map<FileView>(file);
            }
        }

        public FileView Get(long fileId)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<File>();
                var file = repo.Get(fileId);
                return mapper.Map<FileView>(file);
            }
        }
    }
}
