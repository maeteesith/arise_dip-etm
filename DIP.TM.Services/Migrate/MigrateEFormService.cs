﻿using AutoMapper;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;
using System.Linq;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System;
using DIP.TM.Utils;

namespace DIP.TM.Services.Migrate {
    public class MigrateEFormService<EForm, M, E, V> where EForm : eFormEntityBase where M : SaveModel where E : SaveEntityBase where V : BaseSaveView {
        private readonly IUowProvider _uowProvider;
        private readonly IConfiguration _configuration;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public MigrateEFormService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this._uowProvider = uowProvider;
            this._configuration = configuration;
            this._mapper = mapper;
        }

        [LogAopInterceptor]
        public V DownloadFromEForm(string eform_number, SaveModel model = null) {
            using (var uow = _uowProvider.CreateUnitOfWork()) {
                var eform_repo = uow.GetRepository<EForm>();
                var eform = eform_repo.Query(r => r.eform_number == eform_number).FirstOrDefault();
                if (eform == null)
                    return default;

                var entity = _mapper.Map<E>(eform);
                var save = _mapper.Map<M>(entity);
                save.id = null;

                if (model != null) {
                    save.request_number = model.request_number;
                    save.request_id = model.request_id;
                    save.request_date = DateTime.Now;
                    save.make_date = DateTime.Now;
                    save.request_index = model.request_index;
                    save.request_source_code = RequestSourceCodeConstant.EFORM.ToString();
                    save.save_status_code = SaveStatusCodeConstant.DRAFT.ToString();
                }

                save = MapManual(eform, save);

                var save_repo = uow.GetRepository<E>();
                var e = _mapper.Map<E>(save);
                save_repo.Add(e);
                uow.SaveChanges();

                return _mapper.Map<V>(e);
            }
        }

        public M MapManual(EForm eform, M save) {
            if (typeof(EForm) == typeof(eForm_Save010)) {
                var ef010 = _mapper.Map<eForm_Save010>(eform);
                var s010 = _mapper.Map<Save010AddModel>(save);

                s010.request_date = DateTime.Now;
                s010.trademark_expired_date = s010.request_date.Value.AddYears(10);
                s010.trademark_expired_start_date = s010.trademark_expired_date.Value.AddMonths(-3);
                s010.trademark_expired_end_date = s010.trademark_expired_date.Value.AddMonths(6);

                s010.product_list = new List<SaveProductAddModel>();
                foreach (var product in ef010.eForm_Save010Product) {
                    s010.product_list.Add(new SaveProductAddModel() {
                        request_item_sub_type_1_code = product.request_item_sub_type_1_code,
                        description = product.description,
                    });
                }

                var kor10 = (ef010.eForm_Save010Kor10 != null) ? ef010.eForm_Save010Kor10.Last() : new eForm_Save010Kor10();
                var case28_list = new List<Save010Case28AddModel>();
                var kor10_product_list = (kor10.eForm_Save010Kor10Product != null) ? kor10.eForm_Save010Kor10Product.ToList() : new List<eForm_Save010Kor10Product>();
                if (kor10_product_list != null) {
                    kor10_product_list.ForEach(ff => {
                        var case28 = new Save010Case28AddModel();

                        case28.request_item_sub_type_1_code = ff.product_class;
                        case28.case_28_date = ff.request_date;
                        case28.address_country_code = ff.request_country;

                        case28_list.Add(case28);
                    });
                }

                s010.case28_list = case28_list;
                s010.request_item_type_code = ef010.save010_mark_type_type_code;
                s010.save_otop_type_code = ef010.save010_otop_type_code;
                s010.otop_reference_number = ef010.otop_number;

                string sound_mark_txt = "";
                if (ef010.is_sound_mark_human.HasValue) {
                    if (ef010.is_sound_mark_human.Value) {
                        sound_mark_txt += " HUMAN";
                    }
                }
                if (ef010.is_sound_mark_animal.HasValue) {
                    if (ef010.is_sound_mark_animal.Value) {
                        sound_mark_txt += " ANIMAL";
                    }
                }
                if (ef010.is_sound_mark_sound.HasValue) {
                    if (ef010.is_sound_mark_sound.Value) {
                        sound_mark_txt += " NATURE";
                    }
                }
                if (ef010.is_sound_mark_other.HasValue) {
                    if (ef010.is_sound_mark_other.Value) {
                        sound_mark_txt += " OTHERS";
                    }
                }
                if (sound_mark_txt != "") {
                    s010.sound_mark_list = sound_mark_txt.Substring(1);
                }

                save = _mapper.Map<M>(s010);
            } else if (typeof(EForm) == typeof(eForm_Save050)) {
                var ef050 = _mapper.Map<eForm_Save050>(eform);
                var s050 = _mapper.Map<Save050AddModel>(save);

                s050.is_allow_contract = (ef050.save050_allow_type_code == "START") ? true : false;
                s050.is_extend_contract = (ef050.save050_allow_type_code == "EXTEND") ? true : false;

                save = _mapper.Map<M>(s050);
            } else if (typeof(EForm) == typeof(eForm_Save120)) {
                var so = _mapper.Map<SaveOtherAddModel>(save);

                so.request_type_code = "120";
                so.save_status_code = SaveStatusCodeConstant.SENT.ToString();
                so.department_send_code = "CHECKING";
                so.department_send_date = DateTime.Today;
                so.consider_similar_document_status_code = ConsideringSimilarDocumentStatusCodeConstant.WAIT_CONSIDER.ToString();

                save = _mapper.Map<M>(so);
            }


            return PortSaveAddress(eform, save);
        }

        public M PortSaveAddress(EForm eform, M save) {
            using (var uow = _uowProvider.CreateUnitOfWork()) {

                if (typeof(EForm) == typeof(eForm_Save010)) {
                    var s010 = _mapper.Map<Save010>(save);
                    var ef010 = _mapper.Map<eForm_Save010>(eform);

                    if (s010.Save010AddressRepresentative != null && s010.Save010AddressRepresentative.Count() > 0) {
                        foreach (var item in s010.Save010AddressRepresentative) {
                            item.address_representative_condition_type_code = ef010.save010_representative_condition_type_code;
                        }
                    }

                    save = _mapper.Map<M>(s010);
                } else if (typeof(EForm) == typeof(eForm_Save020)) {
                    var s020 = _mapper.Map<Save020>(save);
                    var ef020 = _mapper.Map<eForm_Save020>(eform);

                    if (s020.Save020AddressRepresentative != null && s020.Save020AddressRepresentative.Count() > 0) {
                        foreach (var item in s020.Save020AddressRepresentative) {
                            item.address_representative_condition_type_code = ef020.save020_representative_condition_type_code;
                        }
                    }

                    save = _mapper.Map<M>(s020);
                } else if (typeof(EForm) == typeof(eForm_Save030)) {
                    var s030 = _mapper.Map<Save030>(save);
                    var ef030 = _mapper.Map<eForm_Save030>(eform);

                    if (s030.Save030AddressRepresentative != null && s030.Save030AddressRepresentative.Count() > 0) {
                        foreach (var item in s030.Save030AddressRepresentative) {
                            item.address_representative_condition_type_code = ef030.save030_representative_condition_type_code;
                        }
                    }

                    save = _mapper.Map<M>(s030);
                } else if (typeof(EForm) == typeof(eForm_Save040)) {
                    var s040 = _mapper.Map<Save040>(save);
                    var ef040 = _mapper.Map<eForm_Save040>(eform);

                    if (s040.Save040AddressRepresentative != null && s040.Save040AddressRepresentative.Count() > 0) {
                        foreach (var item in s040.Save040AddressRepresentative) {
                            item.address_representative_condition_type_code = ef040.save040_representative_condition_type_code;
                        }
                    }

                    save = _mapper.Map<M>(s040);
                } else if (typeof(EForm) == typeof(eForm_Save050)) {
                    var s050 = _mapper.Map<Save050>(save);
                    var ef050 = _mapper.Map<eForm_Save050>(eform);

                    if (s050.Save050AddressRepresentative != null && s050.Save050AddressRepresentative.Count() > 0) {
                        foreach (var item in s050.Save050AddressRepresentative) {
                            item.address_representative_condition_type_code = ef050.save050_representative_condition_type_code;
                        }
                    }

                    if (s050.Save050ReceiverAddressPeople != null && s050.Save050ReceiverAddressPeople.Count() > 0) {
                        foreach (var item in s050.Save050ReceiverAddressPeople) {
                            item.receiver_people_type_code = ef050.save050_receiver_people_type_code;
                        }
                    }

                    if (s050.Save050ReceiverAddressRepresentative != null && s050.Save050ReceiverAddressRepresentative.Count() > 0) {
                        foreach (var item in s050.Save050ReceiverAddressRepresentative) {
                            item.address_representative_condition_type_code = ef050.save050_receiver_representative_condition_type_code;
                        }
                    }

                    save = _mapper.Map<M>(s050);
                } else if (typeof(EForm) == typeof(eForm_Save060)) {
                    var s060 = _mapper.Map<Save060>(save);
                    var ef060 = _mapper.Map<eForm_Save060>(eform);

                    if (s060.Save060AddressRepresentative != null && s060.Save060AddressRepresentative.Count() > 0) {
                        foreach (var item in s060.Save060AddressRepresentative) {
                            item.address_representative_condition_type_code = ef060.save060_representative_condition_type_code;
                        }
                    }

                    save = _mapper.Map<M>(s060);
                } else if (typeof(EForm) == typeof(eForm_Save080)) {
                    var s080 = _mapper.Map<Save080>(save);
                    var ef080 = _mapper.Map<eForm_Save080>(eform);

                    if (s080.Save080AddressRepresentative != null && s080.Save080AddressRepresentative.Count() > 0) {
                        foreach (var item in s080.Save080AddressRepresentative) {
                            item.address_representative_condition_type_code = ef080.save080_representative_condition_type_code;
                        }
                    }

                    save = _mapper.Map<M>(s080);
                }

            }

            return save;
        }
    }
}
