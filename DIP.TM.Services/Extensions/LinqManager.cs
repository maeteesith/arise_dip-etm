﻿using DIP.TM.Models.Pagers;
using DIP.TM.Services.Attributes;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using Z.EntityFramework.Plus;
using Z.Expressions;

namespace DIP.TM.Services.Extensions
{

    public static class LinqManager
    {
        public static MethodInfo ContainsMethod = typeof(string).GetMethod("Contains", new[] { typeof(string) });
        public static MethodInfo EqualsMethod = typeof(string).GetMethod("Equals", new[] { typeof(string) });
        private static readonly MethodInfo StartsWithMethod = typeof(string).GetMethod("StartsWith", new Type[] { typeof(string) });
        private static readonly MethodInfo EndsWithMethod = typeof(string).GetMethod("EndsWith", new Type[] { typeof(string) });
        private static readonly MethodInfo ToStringMethod = typeof(object).GetMethod("ToString");

        public static MethodInfo GetOperatorMethod(Operation operation)
        {
            switch (operation)
            {
                case Operation.Contains:
                    return ContainsMethod;
                case Operation.StartsWith:
                    return StartsWithMethod;
                case Operation.EndsWith:
                    return EndsWithMethod;
                case Operation.StringEquals:
                    return EqualsMethod; ;
                default:
                    throw new ArgumentException("Invalid Operation");
            }
        }

        private static Expression FindAndBuildExpression(IEnumerable<PropertyInfo> dtoProperties, string propertyKey,
            SearchTextModel searchTextModel, Expression t)
        {
            var foundProperty =
                dtoProperties.FirstOrDefault(
                    property =>
                        property.PropertyType == typeof(string) &&
                        String.Equals(property.Name, propertyKey, StringComparison.CurrentCultureIgnoreCase));
            if (ReferenceEquals(null, foundProperty)) return null;
            var stringValue = Expression.Call(Expression.Property(t, foundProperty.Name), ToStringMethod);
            var nextExpression = Expression.Call(stringValue, GetOperatorMethod(searchTextModel.operation),
                Expression.Constant(searchTextModel.input_text));

            return nextExpression;
        }

        public static IQueryable<T> SearchText<T>(this IQueryable<T> source, SearchTextModel searchTextModel, Type modelType)
        {
            if (searchTextModel == null || searchTextModel.keys == null || string.IsNullOrEmpty(searchTextModel.input_text))
            {
                return source;
            }

            var t = Expression.Parameter(typeof(T));
            Expression body = null;

            var dtoProperties = typeof(T).GetProperties();
            foreach (var propertyKey in searchTextModel.keys)
            {
                var propertyExpression = FindAndBuildExpression(dtoProperties, propertyKey, searchTextModel, t);
                if (!ReferenceEquals(null, propertyExpression))
                {
                    body = !ReferenceEquals(null, body) ? Expression.Or(body, propertyExpression) : propertyExpression;
                    continue;
                }

                Type paramType;
                var linqProperty = GetLinqString(propertyKey, modelType, PropertyLinqType.SearchBy, out paramType);
                if (linqProperty == propertyKey)
                    continue;

                propertyExpression = FindAndBuildExpression(dtoProperties, linqProperty, searchTextModel, t);
                if (!ReferenceEquals(null, propertyExpression))
                {
                    body = !ReferenceEquals(null, body) ? Expression.Or(body, propertyExpression) : propertyExpression;
                    continue;
                }

                var expression = DynamicExpressionParser.ParseLambda(new[] { t }, null,
                    linqProperty);
                var linqExpression = Expression.Call(expression.Body, GetOperatorMethod(searchTextModel.operation),
                    Expression.Constant(searchTextModel.input_text));

                if (!ReferenceEquals(null, body))
                    body = Expression.Or(body, linqExpression);
                else
                    body = linqExpression;
            }

            return !ReferenceEquals(null, body) ? source.Where(Expression.Lambda<Func<T, bool>>(body, t)) : source;
        }

        public static string GetSearchStringOperation(Operation operation)
        {
            switch (operation){
                case Operation.Contains:
                    return "Contains";
                case Operation.StartsWith:
                    return "StartsWith";
                case Operation.EndsWith:
                    return "EndsWith";
            }
            return "";
        }

        public static IQueryable<T> SearchBy<T>(this IQueryable<T> source, SearchByModel searchByModel, Type modelType)
        {
            if (searchByModel == null || string.IsNullOrEmpty(searchByModel.key) || searchByModel.values == null ||
                searchByModel.values.Count == 0)
                return source;
            if (searchByModel.operation != Operation.Contains && searchByModel.operation != Operation.StringEquals &&
                searchByModel.operation != Operation.StartsWith && searchByModel.operation != Operation.EndsWith)
                return source.FilterDynamic(searchByModel, modelType);

            var t = Expression.Parameter(typeof(T));
            var objectProperty =
                typeof(T).GetProperties()
                    .FirstOrDefault(
                        property => property.PropertyType == typeof(string) && searchByModel.key.Equals(property.Name, StringComparison.CurrentCultureIgnoreCase));
            if (!ReferenceEquals(null, objectProperty))
            {
                var body = CheckValues(searchByModel, objectProperty, t);
                var valueSearch = searchByModel.values.FirstOrDefault();
                var sOperation = GetSearchStringOperation(searchByModel.operation);
                //return !ReferenceEquals(body, null) ? source.Where(Expression.Lambda<Func<T, bool>>(body, t)) : source;
                return !ReferenceEquals(body, null) ? source.Where(objectProperty.Name+"."+ sOperation + "(@0)", valueSearch) : source;
            }
            Type paramType;
            var linqProperty = GetLinqString(searchByModel.key, modelType, PropertyLinqType.SearchBy, out paramType);
            if (linqProperty == searchByModel.key)
                return source;

            objectProperty =
                typeof(T).GetProperties()
                    .FirstOrDefault(
                        property => property.PropertyType == typeof(string) && linqProperty.Equals(property.Name, StringComparison.CurrentCultureIgnoreCase));
            if (!ReferenceEquals(null, objectProperty))
            {
                var body = CheckValues(searchByModel, objectProperty, t);
                return !ReferenceEquals(body, null) ? source.Where(Expression.Lambda<Func<T, bool>>(body, t)) : source;
            }

            var expression = DynamicExpressionParser.ParseLambda(new[] { t }, null, linqProperty);
            Expression linqBody = null;
            foreach (var searchText in searchByModel.values.Where(p => !string.IsNullOrEmpty(p)))
            {
                var linqExpression = Expression.Call(expression.Body, GetOperatorMethod(searchByModel.operation), Expression.Constant(searchText));

                if (!ReferenceEquals(null, linqBody))
                    linqBody = Expression.Or(linqBody, linqExpression);
                else
                    linqBody = linqExpression;
            }

            return !ReferenceEquals(linqBody, null) ? source.Where(Expression.Lambda<Func<T, bool>>(linqBody, t)) : source;
        }

        public static Expression CheckValues(SearchByModel searchByModel, PropertyInfo objectProperty, Expression t)
        {
            Expression body = null;
            foreach (var searchText in searchByModel.values.Where(p => !string.IsNullOrEmpty(p)))
            {
                var stringValue = Expression.Call(Expression.Property(t, objectProperty.Name), ToStringMethod);
                var nextExpression = Expression.Call(stringValue, GetOperatorMethod(searchByModel.operation),
                    Expression.Constant(searchText));

                if (ReferenceEquals(body, null))
                    body = nextExpression;
                else
                    body = Expression.Or(body, nextExpression);
            }
            return body;
        }

        public static IQueryable<T> SearchBy<T>(this IQueryable<T> source, List<SearchByModel> searchByModelList,
            Type modelType)
        {
            if (searchByModelList != null && searchByModelList.Count > 0)
                source = searchByModelList.Aggregate(source, (current, @where) => current.SearchBy(@where, modelType));
            return source;
        }

        public static IQueryable<T> FilterByOperation<T>(this IQueryable<T> oModelList, SearchByModel dipSearchModel, string ptOperation, Type modelType)
        {
            var myType = typeof(T);
            var oPropInfo = typeof(T)
                    .GetProperties()
                    .Where(x => x.Name == dipSearchModel.key)
                    .FirstOrDefault();
                    //.FirstOrDefault(x => x.IsGenericMethod);
            //var oPropInfo = myType.GetProperty(dipSearchModel.key);

            if (oPropInfo != null)
                return DoFilter(oModelList, oPropInfo.PropertyType, dipSearchModel, ptOperation);

            Type paramType;
            var linqProperty = GetLinqString(dipSearchModel.key, modelType, PropertyLinqType.SearchBy, out paramType);
            if (linqProperty == dipSearchModel.key)
                throw new Exception("Property not found!");

            var t = Expression.Parameter(typeof(T));

            var propertyExpression = myType.GetProperty(linqProperty);
            if (!ReferenceEquals(null, propertyExpression))
                return DoFilter(oModelList, propertyExpression.PropertyType, dipSearchModel, ptOperation);

            return DoFilter(oModelList, paramType, dipSearchModel, ptOperation, linqProperty);
            //var tOperation = string.Format("{0} @", ptOperation);
            //var tExpression = string.Empty;
            //var i = 0;
            //var oParams = new List<object>();
            //foreach (string tValue in dipSearchModel.Values)
            //{
            //    if (!string.IsNullOrEmpty(tValue))
            //    {
            //        if (!string.IsNullOrEmpty(tExpression))
            //        {
            //            tExpression += " || ";
            //        }
            //        tExpression += linqProperty + tOperation + i;
            //        oParams.Add(tValue);
            //        i++;
            //    }
            //}
            //return string.IsNullOrEmpty(tExpression) ? oModelList : oModelList.Where(tExpression, oParams.ToArray());
        }

        private static IQueryable<T> DoFilter<T>(this IQueryable<T> oModelList, Type oPropInfoType, SearchByModel dipSearchModel, string ptOperation, string expression = null)
        {
            var tExpression = string.Empty;
            var keyExpression = string.IsNullOrEmpty(expression) ? dipSearchModel.key : expression;
            var oParams = new List<object>();
            var tOperation = string.Format("{0} @", ptOperation);
            if (oPropInfoType == typeof(string))
            {
                int i = 0;
                foreach (string tValue in dipSearchModel.values)
                {
                    if (!string.IsNullOrEmpty(tValue))
                    {
                        if (!string.IsNullOrEmpty(tExpression))
                        {
                            tExpression += " || ";
                        }
                        tExpression += keyExpression + tOperation + i;
                        oParams.Add(tValue);
                        i++;
                    }
                }

            }
            else if (oPropInfoType == typeof(int) || oPropInfoType == typeof(int?))
            {
                int i = 0;
                foreach (string tValue in dipSearchModel.values)
                {

                    if (!string.IsNullOrEmpty(tValue))
                    {
                        int value = 0;
                        if (int.TryParse(tValue, out value))
                        {
                            if (!string.IsNullOrEmpty(tExpression))
                            {
                                tExpression += " || ";
                            }
                            tExpression += keyExpression + tOperation + i;
                            oParams.Add(value);
                            i++;
                        }
                    }
                }
            }
            else if (oPropInfoType == typeof(long) || oPropInfoType == typeof(long?))
            {
                int i = 0;
                foreach (string tValue in dipSearchModel.values)
                {
                    if (!string.IsNullOrEmpty(tValue))
                    {
                        long value = 0;
                        if (long.TryParse(tValue, out value))
                        {
                            if (!string.IsNullOrEmpty(tExpression))
                            {
                                tExpression += " || ";
                            }
                            tExpression += keyExpression + tOperation + i;
                            oParams.Add(value);
                            i++;
                        }
                    }
                }
            }
            else if (oPropInfoType == typeof(bool) || oPropInfoType == typeof(bool?))
            {
                int i = 0;
                foreach (string tValue in dipSearchModel.values)
                {
                    if (!string.IsNullOrEmpty(tValue))
                    {
                        bool value = false;
                        if (bool.TryParse(tValue, out value))
                        {
                            if (!string.IsNullOrEmpty(tExpression))
                            {
                                tExpression += " || ";
                            }
                            tExpression += keyExpression + tOperation + i;
                            oParams.Add(value);
                            i++;
                        }
                    }

                }
            }
            else if (oPropInfoType == typeof(decimal) || oPropInfoType == typeof(decimal?))
            {
                int i = 0;
                foreach (string tValue in dipSearchModel.values)
                {
                    if (!string.IsNullOrEmpty(tValue))
                    {
                        decimal value = 0;
                        if (decimal.TryParse(tValue, out value))
                        {
                            if (!string.IsNullOrEmpty(tExpression))
                            {
                                tExpression += " || ";
                            }
                            tExpression += keyExpression + tOperation + i;
                            oParams.Add(value);
                            i++;
                        }
                    }
                }
            }
            else if (oPropInfoType == typeof(double) || oPropInfoType == typeof(double?))
            {
                int i = 0;
                foreach (string tValue in dipSearchModel.values)
                {
                    if (!string.IsNullOrEmpty(tValue))
                    {
                        decimal value = 0;
                        if (decimal.TryParse(tValue, out value))
                        {
                            if (!string.IsNullOrEmpty(tExpression))
                            {
                                tExpression += " || ";
                            }
                            tExpression += keyExpression + tOperation + i;
                            oParams.Add(value);
                            i++;
                        }
                    }

                }
            }
            else if (oPropInfoType == typeof(DateTime?))
            {
                int i = 0;
                foreach (string tValue in dipSearchModel.values)
                {
                    if (!string.IsNullOrEmpty(tValue))
                    {
                        DateTime value = DateTime.MinValue;
                        if (DateTime.TryParse(tValue, out value))
                        {
                            if (!string.IsNullOrEmpty(tExpression))
                            {
                                tExpression += " || ";
                            }
                            tExpression += keyExpression + ".value.date" + tOperation + i;
                            oParams.Add(value.ToString("yyyy-MM-dd"));
                            i++;
                            //return oModelList.Where(keyExpression+".value.date == @0", value.ToString("yyyy-MM-dd"));
                        }
                    }
                }
            }

            else if (oPropInfoType == typeof(DateTime))
            {
                int i = 0;
                foreach (string tValue in dipSearchModel.values)
                {
                    if (!string.IsNullOrEmpty(tValue))
                    {
                        DateTime value = DateTime.MinValue;
                        if (DateTime.TryParse(tValue, out value))
                        {
                            if (!string.IsNullOrEmpty(tExpression))
                            {
                                tExpression += " || ";
                            }
                            tExpression += keyExpression+".date" + tOperation + i;
                            oParams.Add(value.ToString("yyyy-MM-dd"));
                            i++;
                            //return oModelList.Where(keyExpression + ".date == @0", value.ToString("yyyy-MM-dd"));
                        }
                    }
                }
            }
            return string.IsNullOrEmpty(tExpression) ? oModelList : oModelList.Where(tExpression, oParams.ToArray());
        }

        public static Expression<Func<TElement, bool>> IsSameDate<TElement>(Expression<Func<TElement, DateTime>> valueSelector, DateTime value)
        {
            ParameterExpression p = valueSelector.Parameters.Single();

            var antes = Expression.GreaterThanOrEqual(valueSelector.Body, Expression.Constant(value.Date, typeof(DateTime)));

            var despues = Expression.LessThan(valueSelector.Body, Expression.Constant(value.AddDays(1).Date, typeof(DateTime)));

            Expression body = Expression.And(antes, despues);

            return Expression.Lambda<Func<TElement, bool>>(body, p);
        }


        public static IQueryable<T> FilterDynamic<T>(this IQueryable<T> source, SearchByModel dipSearchModel, Type modelType)
        {
            if (string.IsNullOrEmpty(dipSearchModel.key) || (dipSearchModel.values == null || dipSearchModel.values.Count == 0))
            {
                return source;
            }

            switch (dipSearchModel.operation)
            {
                case Operation.Equals:
                    return FilterByOperation<T>(source, dipSearchModel, "=", modelType);
                case Operation.GreaterThan:
                    return FilterByOperation<T>(source, dipSearchModel, ">", modelType);
                case Operation.LessThan:
                    return FilterByOperation<T>(source, dipSearchModel, "<", modelType);
                case Operation.GreaterThanOrEqual:
                    return FilterByOperation<T>(source, dipSearchModel, ">=", modelType);
                case Operation.LessThanOrEqual:
                    return FilterByOperation<T>(source, dipSearchModel, "<=", modelType);
                case Operation.NotEquals:
                    return FilterByOperation<T>(source, dipSearchModel, "!=", modelType);
                default:
                    break;
            }
            return source;
        }

        public static IQueryable<T> FilterDynamic<T>(this IQueryable<T> source, FilterModel dipFilter, params string[] expectedFilters)
        {
            return FilterDynamic(source, dipFilter, typeof(T), expectedFilters);
        }

        public static IQueryable<T> FilterDynamic<T>(this IQueryable<T> source, FilterModel dipFilter, Type modelType, params string[] expectedFilters)
        {
            CheckExpectedFilters(dipFilter, expectedFilters);
            if (dipFilter != null)
            {
                source = source.FilterByParams(dipFilter.filter_queries);
                source = source.SearchBy(dipFilter.search_by, modelType);
                source = source.SearchText(dipFilter.search_text, modelType);
            }

            if (!string.IsNullOrWhiteSpace(dipFilter.order_by))
            {
                Type paramType;
                var orderByString = GetLinqString(dipFilter.order_by, modelType, PropertyLinqType.OrderBy, out paramType);
                source = source.OrderBy(string.Format("{0} {1}", orderByString, dipFilter.is_order_reverse ? "desc" : "asc"));
            }
            return source;
        }

        private static string GetLinqString(string orderByParam, Type modelType, PropertyLinqType linqType, out Type parameterType)
        {
            parameterType = null;
            if (ReferenceEquals(modelType, null))
                return orderByParam;

            var property = modelType.GetProperty(orderByParam, BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
            if (ReferenceEquals(property, null))
                throw new Exception("Property " + orderByParam + " does not exist on model " + modelType.Name);
            parameterType = property.PropertyType;
            var customAttr =
                property.GetCustomAttributes(typeof(PropertyDTOMapAttribute))
                    .Cast<PropertyDTOMapAttribute>()
                    .FirstOrDefault();
            if (ReferenceEquals(customAttr, null)) return orderByParam;

            return linqType == PropertyLinqType.SearchBy ? customAttr.SearchByLinqPath : customAttr.OrderByLinqPath;
        }

        public static void CheckExpectedFilters(FilterModel dipFilter, string[] expectedFilters)
        {
            if (expectedFilters == null || !expectedFilters.Any())
                return;
            if (dipFilter == null || dipFilter.filter_queries == null)
                throw new Exception("This method has expected filters but no FilterQuery strings were passed!");
            foreach (var expectedFilter in expectedFilters.Where(expectedFilter => !dipFilter.filter_queries.Any(p => p.ToLower().RemoveWhitespace().Contains(expectedFilter.ToLower().RemoveWhitespace()))))
            {
                throw new Exception("This method expected the filter " + expectedFilter + " but this filter was not found in request");
            }
        }

        private static string RemoveWhitespace(this string input)
        {
            return new string(input.ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .ToArray());
        }

        public static IQueryable<T> FilterByParams<T>(this IQueryable<T> source, List<string> paramList)
        {
            return paramList == null ? source : paramList.Aggregate(source, (current, param) => current.Where(param));
        }
    }
}
