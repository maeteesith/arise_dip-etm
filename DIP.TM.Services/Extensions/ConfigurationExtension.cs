﻿using DIP.TM.Models.Configs;
using Microsoft.Extensions.Configuration;

namespace DIP.TM.Services.Extensions
{
    public static class ConfigurationExtension
    {
        public static AppConfigModel  ToConfig(this IConfiguration config)
        {
            return config.Get<AppConfigModel>();
        }
    }
}
