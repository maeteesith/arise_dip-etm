﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Auths;
using DIP.TM.Services.Helps;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Utils.Extensions;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace DIP.TM.Services.Saves {
    public class DocumentProcessService {

        protected long? userId;

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public DocumentProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;

            userId = HttpContext.Current?.User?.UserId();
        }

        //public List<vDocumentItemView> DocumentItemList(PageRequest payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<vDocumentItem>();
        //        List<vDocumentItem> data_list = repo.Query(r =>
        //        (string.IsNullOrEmpty(payload.document_status_code) || payload.document_status_code == "ALL" || r.document_status_code == payload.document_status_code) &&
        //        (string.IsNullOrEmpty(payload.document_classification_status_code) || payload.document_classification_status_code == "ALL" || r.document_classification_status_code == payload.document_classification_status_code) &&
        //        (!payload.document_classification_by.HasValue || payload.document_classification_by == r.document_classification_by) &&
        //        1 == 1
        //        , r => r.OrderByDescending(s => s.updated_by)).ToList();

        //        return mapper.Map<List<vDocumentItemView>>(data_list);
        //    }
        //}
        [LogAopInterceptor]
        public virtual List<vDocumentItemView> DocumentItemReceive(DocumentItemListPageModel payload) {
            foreach (vDocumentItemModel document in payload.document_list) {
                if (document.is_check) {
                    if (document.document_status_code == null || document.document_status_code == DocumentStatusCodeConstant.WAIT_DOCUMENTING.ToString()) {
                        using (var uow = uowProvider.CreateUnitOfWork()) {
                            var repo = uow.GetRepository<Save010>();
                            var model = mapper.Map<Save010AddModel>(repo.Get(document.id.Value));
                            uow.SaveChanges();

                            model.document_spliter_by = 3;
                            model.document_classification_by = payload.document_people_by_receive;
                            model.document_status_code = DocumentStatusCodeConstant.RECEIVED_DOCUMENTING.ToString();
                            model.document_classification_status_code = DocumentClassificationStatusCodeConstant.WAIT_DO.ToString();
                            model.document_classification_date = DateTime.Now;
                            var entity = mapper.Map<Save010>(model);

                            using (var _uow = uowProvider.CreateUnitOfWork()) {
                                repo = _uow.GetRepository<Save010>();
                                repo.Update(entity);
                                _uow.SaveChanges();
                            }
                        }
                    }
                }
            }

            return mapper.Map<List<vDocumentItemView>>(payload.document_list);
        }

        [LogAopInterceptor]
        public virtual List<vUM_UserView> DocumentItemDocumentPeopleList(BasePageModelPayload<vUM_UserAddModel> payload) {
            var service = new UserService(configuration, uowProvider, mapper);
            //payload.filter.department_id = 1;
            return service.List(payload); ;
        }

        [LogAopInterceptor]
        public virtual Save010ProcessView ClassificationSave(Save010AddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();

                if (payload.is_master) {
                    var save_version_repo = uow.GetRepository<Save010DocumentClassificationVersion>();
                    var save_version_entity = save_version_repo.Query(r => r.save_id == payload.id && r.is_master).FirstOrDefault();

                    save_version_entity.save010_document_classification_language_code = payload.save010_document_classification_language_code;
                    save_version_entity.document_classification_version_status_code = DocumentClassificationVersionStatusCodeConstant.WAIT_SEND.ToString();

                    if (payload.trademark_2d_file_id.HasValue) {
                        save_version_entity.trademark_2d_file_id = payload.trademark_2d_file_id;
                        save_version_entity.trademark_2d_file_name = payload.trademark_2d_file_name;
                        save_version_entity.trademark_2d_file_size = payload.trademark_2d_file_size;
                        save_version_entity.trademark_2d_physical_path = payload.trademark_2d_physical_path;
                    }

                    int _index = 1;
                    save_version_entity.Save010DocumentClassificationVersionWord.ToList().ForEach(delegate (Save010DocumentClassificationVersionWord word) {
                        word.is_deleted = true;
                    });
                    payload.document_classification_word_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationWordAddModel word) {
                        word.id = null;
                        word.save_id = null;
                        word.save_index = _index++;
                        save_version_entity.Save010DocumentClassificationVersionWord.Add(mapper.Map<Save010DocumentClassificationVersionWord>(word));
                    });

                    _index = 1;
                    save_version_entity.Save010DocumentClassificationVersionImage.ToList().ForEach(delegate (Save010DocumentClassificationVersionImage image) {
                        image.is_deleted = true;
                    });
                    payload.document_classification_image_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationImageAddModel image) {
                        image.id = null;
                        image.save_id = null;
                        image.save_index = _index++;
                        save_version_entity.Save010DocumentClassificationVersionImage.Add(mapper.Map<Save010DocumentClassificationVersionImage>(image));
                    });

                    _index = 1;
                    save_version_entity.Save010DocumentClassificationVersionSound.ToList().ForEach(delegate (Save010DocumentClassificationVersionSound sound) {
                        sound.is_deleted = true;
                    });
                    payload.document_classification_sound_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationSoundAddModel sound) {
                        sound.id = null;
                        sound.save_id = null;
                        sound.save_index = _index++;
                        save_version_entity.Save010DocumentClassificationVersionSound.Add(mapper.Map<Save010DocumentClassificationVersionSound>(sound));
                    });

                    save_version_repo.Update(save_version_entity);
                    uow.SaveChanges();
                } else {
                    payload.document_classification_status_code = DocumentClassificationStatusCodeConstant.WAIT_SEND.ToString();

                    var entity = mapper.Map<Save010>(payload);

                    repo.Update(entity);
                    uow.SaveChanges();
                }

                var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                var view = service.Get(payload.id.Value);
                if (view != null && view.id.HasValue) {
                    view.document_classification_word_list = view.document_classification_word_list.OrderBy(r => r.save_index).ToList();
                    view.document_classification_image_list = view.document_classification_image_list.OrderBy(r => r.save_index).ToList();
                    view.document_classification_sound_list = view.document_classification_sound_list.OrderBy(r => r.save_index).ToList();
                }
                return view;
            }
        }

        [LogAopInterceptor]
        public virtual Save010ProcessView ClassificationSend(Save010AddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var rdc_repo = uow.GetRepository<RequestDocumentCollect>();

                if (payload.is_master) {
                    var save_version_repo = uow.GetRepository<Save010DocumentClassificationVersion>();
                    var save_version_entity = save_version_repo.Query(r => r.save_id == payload.id && r.is_master).FirstOrDefault();

                    save_version_entity.save010_document_classification_language_code = payload.save010_document_classification_language_code;
                    save_version_entity.document_classification_version_status_code = DocumentClassificationVersionStatusCodeConstant.SEND.ToString();
                    save_version_entity.check_date = DateTime.Now;

                    if (payload.trademark_2d_file_id.HasValue) {
                        save_version_entity.trademark_2d_file_id = payload.trademark_2d_file_id;
                        save_version_entity.trademark_2d_file_name = payload.trademark_2d_file_name;
                        save_version_entity.trademark_2d_file_size = payload.trademark_2d_file_size;
                        save_version_entity.trademark_2d_physical_path = payload.trademark_2d_physical_path;
                    }

                    int _index = 1;
                    save_version_entity.Save010DocumentClassificationVersionWord.ToList().ForEach(delegate (Save010DocumentClassificationVersionWord word) {
                        word.is_deleted = true;
                    });
                    payload.document_classification_word_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationWordAddModel word) {
                        word.id = null;
                        word.save_id = null;
                        word.save_index = _index++;
                        save_version_entity.Save010DocumentClassificationVersionWord.Add(mapper.Map<Save010DocumentClassificationVersionWord>(word));
                    });

                    _index = 1;
                    save_version_entity.Save010DocumentClassificationVersionImage.ToList().ForEach(delegate (Save010DocumentClassificationVersionImage image) {
                        image.is_deleted = true;
                    });
                    payload.document_classification_image_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationImageAddModel image) {
                        image.id = null;
                        image.save_id = null;
                        image.save_index = _index++;
                        save_version_entity.Save010DocumentClassificationVersionImage.Add(mapper.Map<Save010DocumentClassificationVersionImage>(image));
                    });

                    _index = 1;
                    save_version_entity.Save010DocumentClassificationVersionSound.ToList().ForEach(delegate (Save010DocumentClassificationVersionSound sound) {
                        sound.is_deleted = true;
                    });
                    payload.document_classification_sound_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationSoundAddModel sound) {
                        sound.id = null;
                        sound.save_id = null;
                        sound.save_index = _index++;
                        save_version_entity.Save010DocumentClassificationVersionSound.Add(mapper.Map<Save010DocumentClassificationVersionSound>(sound));
                    });

                    save_version_repo.Update(save_version_entity);

                    ///////////////////

                    var save_entity = repo.Query(r => r.id == payload.id).FirstOrDefault();

                    save_entity.save010_document_classification_language_code = payload.save010_document_classification_language_code;
                    save_entity.document_classification_status_code = DocumentClassificationStatusCodeConstant.SEND.ToString();

                    if (payload.trademark_2d_file_id.HasValue) {
                        var rdc = rdc_repo.Query(r => r.save_id == save_entity.id &&
                            r.request_document_collect_type_code == RequestDocumentCollectTypeCodeConstant.TRADEMARK_2D.ToString() &&
                            r.request_document_collect_status_code == RequestDocumentCollectStatusCodeConstant.ADD.ToString()
                        ).FirstOrDefault();

                        long? receipt_item_id = null;
                        if (rdc != null) {
                            rdc.request_document_collect_status_code = RequestDocumentCollectStatusCodeConstant.DELETED.ToString();
                            receipt_item_id = rdc.receipt_item_id;
                            rdc_repo.Update(rdc);
                        }

                        rdc = new RequestDocumentCollect() {
                            save_id = save_entity.id,
                            receipt_item_id = receipt_item_id,
                            request_document_collect_type_code = RequestDocumentCollectTypeCodeConstant.TRADEMARK_2D.ToString(),
                            file_id = payload.trademark_2d_file_id,
                            request_document_collect_status_code = RequestDocumentCollectStatusCodeConstant.ADD.ToString(),
                        };
                        rdc_repo.Update(rdc);
                    }

                    _index = 1;
                    save_entity.Save010DocumentClassificationWord.ToList().ForEach(delegate (Save010DocumentClassificationWord word) {
                        word.is_deleted = true;
                    });
                    payload.document_classification_word_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationWordAddModel word) {
                        word.id = null;
                        word.save_id = null;
                        word.save_index = _index++;
                        save_entity.Save010DocumentClassificationWord.Add(mapper.Map<Save010DocumentClassificationWord>(word));
                    });

                    _index = 1;
                    save_entity.Save010DocumentClassificationImage.ToList().ForEach(delegate (Save010DocumentClassificationImage image) {
                        image.is_deleted = true;
                    });
                    payload.document_classification_image_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationImageAddModel image) {
                        image.id = null;
                        image.save_id = null;
                        image.save_index = _index++;
                        save_entity.Save010DocumentClassificationImage.Add(mapper.Map<Save010DocumentClassificationImage>(image));
                    });

                    _index = 1;
                    save_entity.Save010DocumentClassificationSound.ToList().ForEach(delegate (Save010DocumentClassificationSound sound) {
                        sound.is_deleted = true;
                    });
                    payload.document_classification_sound_list.Where(r => !r.is_deleted).ToList().ForEach(delegate (Save010DocumentClassificationSoundAddModel sound) {
                        sound.id = null;
                        sound.save_id = null;
                        sound.save_index = _index++;
                        save_entity.Save010DocumentClassificationSound.Add(mapper.Map<Save010DocumentClassificationSound>(sound));
                    });

                    repo.Update(save_entity);

                    uow.SaveChanges();

                    var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                    var view = service.Get(payload.id.Value);
                    if (view != null && view.id.HasValue) {
                        view.document_classification_word_list = view.document_classification_word_list.OrderBy(r => r.save_index).ToList();
                        view.document_classification_image_list = view.document_classification_image_list.OrderBy(r => r.save_index).ToList();
                        view.document_classification_sound_list = view.document_classification_sound_list.OrderBy(r => r.save_index).ToList();
                    }
                    var save_next = save_version_repo.Query(r => r.is_master &&
                        (r.document_classification_version_status_code == DocumentClassificationVersionStatusCodeConstant.WAIT_DO.ToString() ||
                        r.document_classification_version_status_code == DocumentClassificationVersionStatusCodeConstant.WAIT_SEND.ToString())
                    ).FirstOrDefault();
                    if (save_next != null) view.classification_send_next_save_id = save_next.save_id;
                    return view;
                } else {
                    payload.document_classification_status_code = DocumentClassificationStatusCodeConstant.SEND.ToString();
                    payload.checking_type_code = CheckingTypeCodeConstant.CHECKING.ToString();
                    payload.checking_status_code = CheckingStatusCodeConstant.WAIT_CHECKING.ToString();

                    var entity = mapper.Map<Save010>(payload);

                    repo.Update(entity);
                    uow.SaveChanges();


                    var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                    var view = service.Get(payload.id.Value);
                    if (view != null && view.id.HasValue) {
                        view.document_classification_word_list = view.document_classification_word_list.OrderBy(r => r.save_index).ToList();
                        view.document_classification_image_list = view.document_classification_image_list.OrderBy(r => r.save_index).ToList();
                        view.document_classification_sound_list = view.document_classification_sound_list.OrderBy(r => r.save_index).ToList();
                    }
                    var save_next = repo.Query(r =>
                         r.document_classification_status_code == DocumentClassificationStatusCodeConstant.WAIT_DO.ToString() ||
                         r.document_classification_status_code == DocumentClassificationStatusCodeConstant.WAIT_SEND.ToString()
                    ).FirstOrDefault();
                    if (save_next != null) view.classification_send_next_save_id = save_next.id;
                    return view;
                }
            }
        }


        [LogAopInterceptor]
        public virtual vDocumentProcessCollectView DocumentProcessCollectHistoryImageSend(vDocumentProcessCollectAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010DocumentClassificationCollect>();
                var entity = repo.Get(payload.id.Value);
                entity.file_id = payload.file_id.Value;
                entity.document_classification_collect_status_code = DocumentClassificationCollectStatusCodeConstant.DONE.ToString();
                repo.Update(entity);

                var file_repo = uow.GetRepository<RequestDocumentCollect>();
                var file_add_list = file_repo.Query(r => r.save_id == payload.save_id && r.request_document_collect_type_code == RequestDocumentCollectTypeCodeConstant.TRADEMARK_2D.ToString() && r.request_document_collect_status_code == RequestDocumentCollectStatusCodeConstant.ADD.ToString());
                long? receipt_item_id = 0;
                foreach (var file_add in file_add_list) {
                    receipt_item_id = file_add.receipt_item_id;
                    file_add.request_document_collect_status_code = RequestDocumentCollectStatusCodeConstant.DELETED.ToString();
                    file_repo.Update(file_add);
                }

                file_repo.Add(new RequestDocumentCollect() {
                    save_id = payload.save_id,
                    receipt_item_id = receipt_item_id,
                    request_document_collect_type_code = RequestDocumentCollectTypeCodeConstant.TRADEMARK_2D.ToString(),
                    file_id = payload.file_id.Value,
                    request_document_collect_status_code = RequestDocumentCollectStatusCodeConstant.ADD.ToString(),
                });

                uow.SaveChanges();
                return mapper.Map<vDocumentProcessCollectView>(payload);
            }
        }


        [LogAopInterceptor]
        public virtual Save010ProcessView ClassificationVersionAdd(long save_id, long save_other_id, bool is_added) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var save_entity = repo.Get(save_id);
                var save_version_repo = uow.GetRepository<Save010DocumentClassificationVersion>();

                if (is_added) {
                    var save_version_count = save_version_repo.Query(r => r.save_id == save_id && r.save_other_id == save_other_id).Count();
                    if (save_version_count == 0) {
                        var save_version_entity = save_version_repo.Query(r => r.save_id == save_id && r.is_master).FirstOrDefault();

                        if (save_version_entity != null) {
                            save_version_entity.is_master = false;
                            save_version_entity.check_date = DateTime.Now;

                            save_version_repo.Update(save_version_entity);
                        } else {
                            save_version_entity = new Save010DocumentClassificationVersion() {
                                save_id = save_id,
                                version_index = "1",
                                save010_document_classification_language_code = save_entity.save010_document_classification_language_code,
                                document_classification_version_status_code = DocumentClassificationVersionStatusCodeConstant.SEND.ToString(),
                                check_date = DateTime.Now,
                            };
                            foreach (var word in save_entity.Save010DocumentClassificationWord.Where(r => !r.is_deleted)) {
                                save_version_entity.Save010DocumentClassificationVersionWord.Add(new Save010DocumentClassificationVersionWord {
                                    save_index = word.save_index,
                                    word_mark = word.word_mark,
                                    word_first_code = word.word_first_code,
                                    word_sound_last_code = word.word_sound_last_code,
                                    word_sound_last_other_code = word.word_sound_last_other_code,
                                    word_sound = word.word_sound,
                                    word_syllable_sound = word.word_syllable_sound,
                                });
                            }
                            foreach (var image in save_entity.Save010DocumentClassificationImage.Where(r => !r.is_deleted)) {
                                save_version_entity.Save010DocumentClassificationVersionImage.Add(new Save010DocumentClassificationVersionImage {
                                    save_index = image.save_index,
                                    document_classification_image_code = image.document_classification_image_code,
                                });
                            }
                            foreach (var sound in save_entity.Save010DocumentClassificationSound.Where(r => !r.is_deleted)) {
                                save_version_entity.Save010DocumentClassificationVersionSound.Add(new Save010DocumentClassificationVersionSound {
                                    save_index = sound.save_index,
                                    document_classification_sound_code = sound.document_classification_sound_code,
                                });
                            }

                            save_version_repo.Update(save_version_entity);
                        }

                        save_version_entity = new Save010DocumentClassificationVersion() {
                            save_id = save_id,
                            save_other_id = save_other_id,
                            version_index = (Convert.ToInt64(save_version_entity.version_index) + 1).ToString(),
                            save010_document_classification_language_code = save_entity.save010_document_classification_language_code,
                            document_classification_version_status_code = DocumentClassificationVersionStatusCodeConstant.WAIT_DO.ToString(),
                            check_date = DateTime.Now,
                            is_master = true,
                        };
                        foreach (var word in save_entity.Save010DocumentClassificationWord.Where(r => !r.is_deleted)) {
                            save_version_entity.Save010DocumentClassificationVersionWord.Add(new Save010DocumentClassificationVersionWord {
                                save_index = word.save_index,
                                word_mark = word.word_mark,
                                word_first_code = word.word_first_code,
                                word_sound_last_code = word.word_sound_last_code,
                                word_sound_last_other_code = word.word_sound_last_other_code,
                                word_sound = word.word_sound,
                                word_syllable_sound = word.word_syllable_sound,
                            });
                        }
                        foreach (var image in save_entity.Save010DocumentClassificationImage.Where(r => !r.is_deleted)) {
                            save_version_entity.Save010DocumentClassificationVersionImage.Add(new Save010DocumentClassificationVersionImage {
                                save_index = image.save_index,
                                document_classification_image_code = image.document_classification_image_code,
                            });
                        }
                        foreach (var sound in save_entity.Save010DocumentClassificationSound.Where(r => !r.is_deleted)) {
                            save_version_entity.Save010DocumentClassificationVersionSound.Add(new Save010DocumentClassificationVersionSound {
                                save_index = sound.save_index,
                                document_classification_sound_code = sound.document_classification_sound_code,
                            });
                        }
                        save_version_repo.Update(save_version_entity);
                        uow.SaveChanges();
                    }
                } else {
                    var save_version_entity = save_version_repo.Query(r => r.save_id == save_id && r.save_other_id == save_other_id).FirstOrDefault();
                    if (save_version_entity != null) {
                        save_version_entity.document_classification_version_status_code = DocumentClassificationVersionStatusCodeConstant.DELETE.ToString();
                        save_version_repo.Update(save_version_entity);
                        uow.SaveChanges();
                    }

                }

                var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                var view = service.Get(save_id);
                if (view != null && view.id.HasValue) {
                    view.document_classification_word_list = view.document_classification_word_list.OrderBy(r => r.save_index).ToList();
                    view.document_classification_image_list = view.document_classification_image_list.OrderBy(r => r.save_index).ToList();
                    view.document_classification_sound_list = view.document_classification_sound_list.OrderBy(r => r.save_index).ToList();
                }
                return view;
            }
        }

        [LogAopInterceptor]
        public virtual List<ReferenceMasterView> ClassificationListImageType(BasePageModelPayload<ReferenceMasterModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RM_Save010DocumentClassificationImageType>();
                return mapper.Map<List<ReferenceMasterView>>(repo.Query(r =>
                        payload.filter.is_exactly ? r.code == payload.filter.code : (
                            string.IsNullOrEmpty(payload.filter.code) ||
                            r.code.IndexOf(payload.filter.code) >= 0 ||
                            r.name.IndexOf(payload.filter.code) >= 0
                        ), null, s => s.Take(payload.paging.item_per_page).OrderBy(r => r.index)
                    ));
            }
        }

        [LogAopInterceptor]
        public virtual List<ReferenceMasterView> ClassificationListSoundType(BasePageModelPayload<ReferenceMasterModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RM_Save010DocumentClassificationSoundType>();
                return mapper.Map<List<ReferenceMasterView>>(repo.Query(r =>
                        string.IsNullOrEmpty(payload.filter.code) ||
                         r.code.IndexOf(payload.filter.code) >= 0 ||
                         r.name.IndexOf(payload.filter.code) >= 0
                         , null, s => s.Take(payload.paging.item_per_page)
                     ));
            }
        }

        [LogAopInterceptor]
        public Save010DocumentClassificationCollectView CollectRequestAdd(Save010DocumentClassificationCollectAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();
                var save_entity = save_repo.Query(r => r.request_number == payload.request_number).FirstOrDefault();
                if (save_entity != null) {
                    var repo = uow.GetRepository<Save010DocumentClassificationCollect>();
                    var entity = new Save010DocumentClassificationCollect() {
                        save_id = save_entity.id,
                        sender_by_name = payload.sender_by_name,
                        department_name = payload.department_name,
                        remark = payload.remark,
                        document_classification_collect_status_code = DocumentClassificationCollectStatusCodeConstant.WAIT.ToString(),
                    };
                    repo.Update(entity);
                    uow.SaveChanges();
                    return mapper.Map<Save010DocumentClassificationCollectView>(entity);
                } else {
                    return null;
                }
            }
        }

        [LogAopInterceptor]
        public PageDocumentRole02CheckView DocumentRole02CheckLoad(long payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view = new PageDocumentRole02CheckView();

                var repo = uow.GetRepository<vDocumentRole02Group>();
                var entity = repo.Query(r => r.id == payload).FirstOrDefault();


                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(entity.save_id.Value);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(entity.save_id.Value);

                var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                var result = view_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "id",
                              value = payload.ToString(),
                          }
                    },
                    filter_queries = new List<string>() {
                        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                    }
                });
                view.instruction_rule_list = result.data.list;

                var address_service = new BaseService<PostRoundAddress, PostRoundAddressView>(configuration, uowProvider, mapper);
                var address = address_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "post_round_id",
                              value = payload.ToString(),
                          }
                     }
                });
                view.post_round_address_list = address.data.list;

                return view;
            }
        }


        [LogAopInterceptor]
        public PageDocumentRole04ReleaseRequestItemView DocumentRole04ReleaseRequestItemLoad(long payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view = new PageDocumentRole04ReleaseRequestItemView();

                //var repo = uow.GetRepository<vDocumentRole04>();
                //var entity = repo.Query(r => r.id == payload).FirstOrDefault();


                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(payload);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(payload);

                var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                var result = view_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                    },
                    filter_queries = new List<string>() {
                        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                    }
                });
                view.instruction_rule_list = result.data.list;

                var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                var document = document_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          }
                     }
                });
                view.document_list = document.data.list;

                var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                var receipt_item = receipt_item_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                     }
                });
                view.receipt_item_list = receipt_item.data.list;

                var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                var history = history_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                     }
                });
                view.history_list = history.data.list;

                var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                var scan = scan_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          },
                          new SearchByModel() {
                              key = "file_id",
                              value = "0",
                              operation= Operation.GreaterThan,
                          }
                     }
                });
                view.document_scan_list = scan.data.list;

                return view;
            }
        }


        [LogAopInterceptor]
        public Save010DocumentClassificationCollectView CollectRequestDelete(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010DocumentClassificationCollect>();
                var entity = repo.Get(id);
                entity.document_classification_collect_status_code = DocumentClassificationCollectStatusCodeConstant.DELETE.ToString();
                repo.Update(entity);
                uow.SaveChanges();

                return mapper.Map<Save010DocumentClassificationCollectView>(entity);
            }
        }

        [LogAopInterceptor]
        public Save010DocumentClassificationCollectView CollectHistoryAdd(Save010DocumentClassificationCollectAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010DocumentClassificationCollect>();

                payload.file_id = payload.file_id;
                payload.document_classification_collect_status_code = DocumentClassificationCollectStatusCodeConstant.DONE.ToString();
                payload.maker_by = userId;

                var entity = mapper.Map<Save010DocumentClassificationCollect>(payload);
                repo.Update(entity);
                uow.SaveChanges();
                return mapper.Map<Save010DocumentClassificationCollectView>(entity);
            }
        }

        [LogAopInterceptor]
        public Save010DocumentClassificationCollectView DocumentProcessCollectHistoryDelete(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010DocumentClassificationCollect>();
                var entity = repo.Get(id);
                entity.document_classification_collect_status_code = DocumentClassificationCollectStatusCodeConstant.DELETE.ToString();
                repo.Update(entity);
                uow.SaveChanges();

                return mapper.Map<Save010DocumentClassificationCollectView>(entity);
            }
        }

        [LogAopInterceptor]
        public virtual List<vDocumentRole02View> DocumentRole02ItemListAutoSplit() {
            var view_list = new List<vDocumentRole02View>();
            int item_min = 10;
            int item_max = 20;

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vUM_User>();
                var user_list = repo.Query(r => r.department_id == 2 || 1 == 1);

                //TODO Split into staff by min max
                foreach (var user in user_list) {
                    var item_repo = uow.GetRepository<PostRound>();
                    var item_count = item_repo.Query(r => r.document_role02_receiver_by == user.id).Count();

                    if (item_count < item_min) {
                        //    using (var _uow = uowProvider.CreateUnitOfWork()) {
                        //var save_repo = uow.GetRepository<Save010InstructionRule>();
                        var pr_list = item_repo.Query(r => r.document_role02_status_code == DocumentRole02StatusCodeConstant.WAIT.ToString());

                        foreach (var pr in pr_list) {
                            pr.document_role02_status_code = DocumentRole02StatusCodeConstant.DONE.ToString();
                            pr.document_role02_date = DateTime.Now;

                            pr.document_role02_spliter_by = userId;

                            pr.document_role02_receiver_by = user.id;
                            pr.document_role02_receive_status_code = DocumentRole02ReceiveStatusCodeConstant.DRAFT.ToString();

                            item_repo.Update(pr);
                        }
                    }
                }
                uow.SaveChanges();
            }

            return view_list;
        }



        [LogAopInterceptor]
        public virtual List<vDocumentRole03ItemView> DocumentRole03ItemListAutoSplit() {
            var view_list = new List<vDocumentRole03ItemView>();
            //int item_min = 10;
            //int item_max = 30;

            using (var uow = uowProvider.CreateUnitOfWork()) {
                //var repo = uow.GetRepository<vUM_User>();
                //var user_list = repo.Query(r => r.department_id == 3 || 1 == 1);

                ////TODO Split into staff by min max
                var v_repo = uow.GetRepository<vDocumentRole03Item>();
                var item_repo = uow.GetRepository<DocumentRole03Item>();
                //foreach (var user in user_list) {
                //var item_count = item_repo.Query(r => !r.save_01_index.HasValue).Count();

                //    if (item_count < item_min) {
                //        //    using (var _uow = uowProvider.CreateUnitOfWork()) {
                //var save_repo = uow.GetRepository<Save010InstructionRule>();
                var v_list = v_repo.Query(r => r.document_role03_status_code == DocumentRole03StatusCodeConstant.WAIT.ToString());

                foreach (var v in v_list) {
                    var save_01_index = v_repo
                        .Query(r => r.save_id == v.save_id && r.document_role03_receive_status_code.StartsWith(DocumentRole03ReceiveStatusCodeConstant.DRAFT.ToString()))
                        .Max(r => r.save_01_index);

                    if (!save_01_index.HasValue) {
                        save_01_index = v_repo.Query(r => r.save_id == v.save_id)
                                              .Max(r => r.save_01_index);

                        if (!save_01_index.HasValue) {
                            save_01_index = 1;
                        }
                    }

                    var item = new DocumentRole03Item() {
                        save_01_id = v.save_id,
                        save_01_index = save_01_index,
                        save_id = v.id,
                        request_type_code = v.request_type_code,
                        document_role03_status_code = DocumentRole03StatusCodeConstant.DONE.ToString(),
                        document_role03_split_date = DateTime.Now,
                        document_role03_spliter_by = userId,


                        // TODO
                        document_role03_receiver_by = userId,

                        document_role03_receive_status_code = DocumentRole03ReceiveStatusCodeConstant.DRAFT.ToString(),
                    };
                    //            pr.document_role03_status_code = DocumentRole03StatusCodeConstant.DONE.ToString();
                    //            pr.document_role03_date = DateTime.Now;

                    //            pr.document_role03_spliter_by = userId;

                    //            pr.document_role03_receiver_by = user.id;
                    //            pr.document_role03_receive_status_code = DocumentRole03ReceiveStatusCodeConstant.DRAFT.ToString();

                    item_repo.Update(item);
                }
                //}
                uow.SaveChanges();
            }

            return view_list;
        }


        [LogAopInterceptor]
        public virtual List<SendChangeView> DocumentRole02CheckSendChange(List<SendChangeAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();
                var repo = uow.GetRepository<Save010InstructionRule>();
                var post_round_repo = uow.GetRepository<PostRound>();
                var post_round_instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload) {
                    var post_round = post_round_repo.Get(model.id.Value);
                    post_round.document_role02_receive_status_code = DocumentRole02ReceiveStatusCodeConstant.WAIT_CHANGE.ToString();
                    post_round_repo.Update(post_round);

                    var entity = repo.Get(model.object_id.Value);
                    entity.considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.DRAFT_FIX.ToString();
                    repo.Update(entity);

                    var post_round_instruction_rule = post_round_instruction_rule_repo.Query(r => r.post_round_id == post_round.id && r.save010_instruction_rule_id == model.object_id.Value).LastOrDefault();
                    post_round_instruction_rule.document_role02_check_status_code = DocumentRole02CheckStatusCodeConstant.WAIT_CHANGE.ToString();
                    post_round_instruction_rule_repo.Update(post_round_instruction_rule);

                    var save = save_repo.Get(entity.save_id.Value);
                    save.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();
                    save.considering_receive_remark = model.reason;
                    save_repo.Update(save);
                }

                uow.SaveChanges();
                return mapper.Map<List<SendChangeView>>(payload);
            }
        }



        [LogAopInterceptor]
        public virtual List<vDocumentRole02GroupView> DocumentRole02CheckSend(List<vDocumentRole02GroupAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var post_round_repo = uow.GetRepository<PostRound>();
                var post_round_instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();
                var address_repo = uow.GetRepository<PostRoundAddress>();

                foreach (var model in payload) {
                    var post_round = post_round_repo.Get(model.id.Value);
                    post_round.document_role02_receive_status_code = DocumentRole02ReceiveStatusCodeConstant.SEND.ToString();
                    if (!post_round.document_role02_print_document_date.HasValue) {
                        post_round.document_role02_print_document_date = DateTime.Now;
                    }
                    if (model.document_role02_print_document_date.HasValue) {
                        post_round.document_role02_print_document_date = model.document_role02_print_document_date;
                    }
                    post_round.document_role02_print_document_status_code = DocumentRole02PrintDocumentStatusCodeConstant.DRAFT.ToString();
                    post_round_repo.Update(post_round);

                    var post_round_instruction_rule_list = post_round_instruction_rule_repo.Query(r => r.post_round_id == model.id.Value);
                    foreach (var post_round_instruction_rule in post_round_instruction_rule_list) {
                        post_round_instruction_rule.document_role02_check_status_code = DocumentRole02CheckStatusCodeConstant.SEND.ToString();
                        post_round_instruction_rule.document_role02_check_date = DateTime.Now;
                        post_round_instruction_rule_repo.Update(post_round_instruction_rule);
                    }

                    foreach (var post_round_address in model.post_round_address_list.Where(r => r.id.HasValue || !r.is_deleted)) {
                        post_round_address.post_round_id = model.id.Value;
                        var address = mapper.Map<PostRoundAddress>(post_round_address);
                        address_repo.Update(address);
                    }
                }


                uow.SaveChanges();
                return mapper.Map<List<vDocumentRole02GroupView>>(payload);
            }
        }

        [LogAopInterceptor]
        public virtual Save010InstructionRuleView DocumentRole02CheckIntructionRuleSave(vDocumentRole02AddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010InstructionRule>();
                var entity = repo.Get(payload.instruction_rule_id.Value);

                entity.value_1 = payload.value_1;

                repo.Update(entity);
                uow.SaveChanges();
                return mapper.Map<Save010InstructionRuleView>(entity);
            }
        }


        [LogAopInterceptor]
        public virtual vDocumentRole02View DocumentRole02CheckInstructionRuleSend(vDocumentRole02AddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                //var repo = uow.GetRepository<Save010InstructionRule>();
                //var entity = repo.Get(payload.instruction_rule_id.Value);

                //entity.document_role02_check_status_code = DocumentRole02CheckStatusCodeConstant.SEND.ToString();
                //repo.Update(entity);
                //uow.SaveChanges();

                //var document_role_repo = uow.GetRepository<vDocumentRole02>();
                //if (document_role_repo.Query(r => r.save_id == payload.save_id && r.document_role02_check_status_code == DocumentRole02CheckStatusCodeConstant.DRAFT.ToString()).Count() == 0) {
                //    var post_repo = uow.GetRepository<PostRound>();
                //    var post_entity = post_repo.Get(payload.id.Value);

                //    post_entity.document_role02_receive_status_code = DocumentRole02ReceiveStatusCodeConstant.SEND.ToString();
                //    post_entity.document_role02_receive_date = DateTime.Now;

                //    //post_entity.document_role02_print_document_status_code = DocumentRole02PrintDocumentStatusCodeConstant.DRAFT.ToString();

                //    post_repo.Update(post_entity);
                //}
                //uow.SaveChanges();

                return mapper.Map<vDocumentRole02View>(payload);
            }
        }

        [LogAopInterceptor]
        public PageDocumentRole04ReleaseRequestView DocumentRole04ReleaseRequestLoad(long payload, string type) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view = new PageDocumentRole04ReleaseRequestView();

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(payload);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(payload);

                var repo = uow.GetRepository<vDocumentRole04ReleaseRequest>();
                view.document_role04_release_request_list = mapper.Map<List<vDocumentRole04ReleaseRequestView>>(
                    repo.Query(r => r.save_tag_id == payload && r.document_role04_release_request_type_code == type
                ).ToList());

                return view;
            }
        }


        [LogAopInterceptor]
        public PageDocumentRole04ReleaseView DocumentRole04ReleaseLoad(long payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var create = uow.GetRepository<vDocumentRole04Release>();
                var view = new PageDocumentRole04ReleaseView();

                view.document_role04_release = mapper.Map<vDocumentRole04ReleaseView>(create.Query(r => r.save_id == payload).FirstOrDefault());

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(payload);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(payload);

                var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                var result = view_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                    },
                    filter_queries = new List<string>() {
                        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                    }
                });
                view.instruction_rule_list = result.data.list;

                var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                var document = document_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          }
                     }
                });
                view.document_list = document.data.list;

                var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                var receipt_item = receipt_item_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                     }
                });
                view.receipt_item_list = receipt_item.data.list;

                var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                var history = history_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                     }
                });
                view.history_list = history.data.list;

                var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                var scan = scan_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          },
                          new SearchByModel() {
                              key = "file_id",
                              value = "0",
                              operation= Operation.GreaterThan,
                          }
                     }
                });
                view.document_scan_list = scan.data.list;

                return view;
            }
        }



        [LogAopInterceptor]
        public PageDocumentRole03ChangeView DocumentRole03ChangeLoad(long save_id, long save_01_index) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var create = uow.GetRepository<vDocumentRole03Item>();
                var rdc_repo = uow.GetRepository<vRequestDocumentCollect>();
                var view = new PageDocumentRole03ChangeView();

                view.document_role03_item_list = mapper.Map<List<vDocumentRole03ItemView>>(
                    create.Query(r => r.save_id == save_id)
                );

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(save_id);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(save_id);


                var rdc = rdc_repo.Query(r => r.save_id == view.view.id &&
                       r.request_document_collect_type_code == RequestDocumentCollectTypeCodeConstant.CERTIFICATION_MARK.ToString() &&
                       r.request_document_collect_status_code == RequestDocumentCollectStatusCodeConstant.ADD.ToString()
                  ).FirstOrDefault();

                if (rdc != null) {
                    view.certification_mark_file_physical_path = rdc.physical_path;
                }

                //var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                //view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(payload);

                //var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                //var result = view_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "save_id",
                //              value = payload.ToString(),
                //          }
                //     }, filter_queries = new List<string>() {
                //"instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                //    }
                //});
                //view.instruction_rule_list = result.data.list;

                //var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                //var document = document_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "request_number",
                //              value = view.view.request_number,
                //          }
                //     }
                //});
                //view.document_list = document.data.list;

                //var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                //var receipt_item = receipt_item_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "save_id",
                //              value = payload.ToString(),
                //          }
                //     }
                //});
                //view.receipt_item_list = receipt_item.data.list;

                //var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                //var history = history_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "save_id",
                //              value = payload.ToString(),
                //          }
                //     }
                //});
                //view.history_list = history.data.list;

                //var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                //var scan = scan_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "request_number",
                //              value = view.view.request_number,
                //          },
                //          new SearchByModel() {
                //              key = "file_id",
                //              value = "0",
                //              operation= Operation.GreaterThan,
                //          }
                //     }
                //});
                //view.document_scan_list = scan.data.list;

                return view;
            }
        }

        public List<vSave010CertificationFileView> CertificationFileAdd(List<Save010CertificationFileAddModel> payload) {
            var view_list = new List<vSave010CertificationFileView>();

            foreach (var model in payload) {
                using (var uow = uowProvider.CreateUnitOfWork()) {
                    var repo = uow.GetRepository<Save010CertificationFile>();
                    var entity = repo.Update(mapper.Map<Save010CertificationFile>(model));
                    uow.SaveChanges();

                    var view_repo = uow.GetRepository<vSave010CertificationFile>();
                    var view = view_repo.Get(entity.id);
                    view_list.Add(mapper.Map<vSave010CertificationFileView>(view));
                }
            }

            return view_list;
        }

        [LogAopInterceptor]
        public PageDocumentRole04ReleaseView DocumentRole04Release20Load(long payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var create = uow.GetRepository<vDocumentRole04Release20>();
                var view = new PageDocumentRole04ReleaseView();

                view.document_role04_release_20 = mapper.Map<vDocumentRole04Release20View>(create.Query(r => r.save_id == payload).FirstOrDefault());

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(payload);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(payload);

                var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                var result = view_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                     },
                    filter_queries = new List<string>() {
                        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                    }
                });
                view.instruction_rule_list = result.data.list;

                var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                var document = document_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          }
                     }
                });
                view.document_list = document.data.list;

                var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                var receipt_item = receipt_item_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                     }
                });
                view.receipt_item_list = receipt_item.data.list;

                var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                var history = history_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = payload.ToString(),
                          }
                     }
                });
                view.history_list = history.data.list;

                var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                var scan = scan_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          },
                          new SearchByModel() {
                              key = "file_id",
                              value = "0",
                              operation= Operation.GreaterThan,
                          }
                     }
                });
                view.document_scan_list = scan.data.list;

                return view;
            }
        }

        [LogAopInterceptor]
        public List<vDocumentRole03ItemView> DocumentRole03ChangeVersionList(long save_id, string request_type_code) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vDocumentRole03Item>();
                var view_list = repo.Query(r => r.save_id == save_id && r.request_type_code == request_type_code);
                return mapper.Map<List<vDocumentRole03ItemView>>(view_list);
            }
        }

        [LogAopInterceptor]
        public virtual List<vDocumentRole02GroupView> DocumentRole02PrintDocumentSend(List<vDocumentRole02GroupAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<PostRound>();

                int book_expired_day = 60;
                var book_start_date = DateTime.Now;

                foreach (var model in payload) {
                    if (model.is_check) {
                        var entity = repo.Get(model.id.Value);

                        entity.document_role02_print_document_status_code = DocumentRole02PrintDocumentStatusCodeConstant.SEND.ToString();

                        if (!entity.document_role02_print_document_date.HasValue) {
                            entity.document_role02_print_document_date = DateTime.Now;
                        }

                        entity.document_role02_print_cover_status_code = DocumentRole02PrintCoverStatusCodeConstant.WAIT_COVER.ToString();

                        book_start_date = entity.document_role02_print_document_date.Value;
                        if (string.IsNullOrEmpty(entity.book_number)) {
                            entity.book_number = HelpService.GetBookNumber(uowProvider);
                            entity.book_start_date = book_start_date;
                            entity.book_end_date = book_start_date.AddDays(book_expired_day);
                            entity.book_expired_day = book_expired_day;
                        }

                        repo.Update(entity);
                    }
                }
                uow.SaveChanges();

                return mapper.Map<List<vDocumentRole02GroupView>>(payload);
            }
        }
        [LogAopInterceptor]
        public virtual List<vDocumentRole02GroupView> DocumentRole02PrintCoverSend(List<vDocumentRole02GroupAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<PostRound>();

                foreach (var model in payload) {
                    if (model.is_check) {
                        var entity = repo.Get(model.id.Value);

                        entity.document_role02_print_cover_status_code = DocumentRole02PrintCoverStatusCodeConstant.DONE.ToString();
                        entity.document_role02_print_cover_date = DateTime.Now;

                        entity.document_role02_print_list_status_code = DocumentRole02PrintListStatusCodeConstant.WAIT_POST_NUMBER.ToString();

                        repo.Update(entity);
                    }
                }
                uow.SaveChanges();

                return mapper.Map<List<vDocumentRole02GroupView>>(payload);
            }
        }
        [LogAopInterceptor]
        public virtual vDocumentRole02GroupView DocumentRole02PrintPostNumberSend(vDocumentRole02GroupAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<PostRound>();
                var entity = repo.Get(payload.id.Value);

                entity.post_number = payload.post_number;
                entity.document_role02_print_list_status_code = DocumentRole02PrintListStatusCodeConstant.WAIT_PRINT.ToString();

                repo.Update(entity);
                uow.SaveChanges();

                var service = new BaseService<vDocumentRole02Group, vDocumentRole02GroupView>(configuration, uowProvider, mapper);
                return service.Get(entity.id);
            }
        }
        [LogAopInterceptor]
        public virtual List<vDocumentRole02GroupView> DocumentRole02PrintListSend(List<vDocumentRole02GroupAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<PostRound>();

                foreach (var model in payload) {
                    if (model.is_check) {
                        var entity = repo.Get(model.id.Value);

                        entity.document_role02_print_list_status_code = DocumentRole02PrintListStatusCodeConstant.DONE.ToString();
                        entity.document_role02_print_list_date = DateTime.Now;

                        entity.post_round_action_post_status_code = PostRoundActionPostStatusCodeConstant.WAIT.ToString();

                        repo.Update(entity);
                    }
                }
                uow.SaveChanges();

                return mapper.Map<List<vDocumentRole02GroupView>>(payload);
            }
        }


        [LogAopInterceptor]
        public virtual List<vDocumentRole04View> DocumentRole04ItemAutoSplit() {
            var view_list = new List<vDocumentRole04View>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vDocumentRole04>();
                var model_list = repo.Query(r => r.document_role04_status_code == DocumentRole04StatusCodeConstant.WAIT.ToString());

                var user_repo = uow.GetRepository<vUM_User>();
                var user_list = user_repo.Query(r => r.department_id == 2 || 1 == 1);

                //TODO Split into staff by min max
                var user = user_list.FirstOrDefault();

                foreach (var model in model_list) {
                    var item_repo = uow.GetRepository<PostRound>();
                    var item = item_repo.Get(model.id);

                    item.document_role04_status_code = DocumentRole04StatusCodeConstant.DONE.ToString();
                    item.document_role04_receiver_by = user.id;
                    item.document_role04_receive_date = DateTime.Now;
                    item.document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.DRAFT.ToString();

                    view_list.Add(mapper.Map<vDocumentRole04View>(model));
                }
                uow.SaveChanges();
            }

            return view_list;
        }


        [LogAopInterceptor]
        public virtual List<vDocumentRole04View> DocumentRole05ItemAutoSplit() {
            var view_list = new List<vDocumentRole04View>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vDocumentRole04>();
                var model_list = repo.Query(r => r.document_role05_status_code == DocumentRole05StatusCodeConstant.WAIT.ToString());

                var user_repo = uow.GetRepository<vUM_User>();
                var user_list = user_repo.Query(r => r.department_id == 2 || 1 == 1);

                //TODO Split into staff by min max
                var user = user_list.FirstOrDefault();

                foreach (var model in model_list) {
                    var item_repo = uow.GetRepository<PostRound>();
                    var item = item_repo.Get(model.id);

                    item.document_role05_status_code = DocumentRole05StatusCodeConstant.DONE.ToString();
                    item.document_role05_receiver_by = user.id;
                    item.document_role05_receive_date = DateTime.Now;
                    item.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.DRAFT.ToString();

                    view_list.Add(mapper.Map<vDocumentRole04View>(model));
                }
                uow.SaveChanges();
            }

            return view_list;
        }


        [LogAopInterceptor]
        public PageDocumentRole04CheckView DocumentRole04CheckLoad(long payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view = new PageDocumentRole04CheckView();

                var repo = uow.GetRepository<vDocumentRole04>();
                var entity = repo.Query(r => r.id == payload).FirstOrDefault();


                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(entity.save_id);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(entity.save_id);

                var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                var result = view_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = entity.save_id.ToString(),
                          }
                     },
                    filter_queries = new List<string>() {
                        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                    }
                });
                view.instruction_rule_list = result.data.list;

                var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                var document = document_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          }
                     }
                });
                view.document_list = document.data.list;

                var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                var receipt_item = receipt_item_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = view.view.id.ToString(),
                          }
                     }
                });
                view.receipt_item_list = receipt_item.data.list;

                var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                var history = history_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = view.view.id.ToString(),
                          }
                     }
                });
                view.history_list = history.data.list;

                var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                var scan = scan_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          },
                          new SearchByModel() {
                              key = "file_id",
                              value = "0",
                              operation= Operation.GreaterThan,
                          }
                     }
                });
                view.document_scan_list = scan.data.list;

                var document_role04_check_service = new BaseService<vDocumentRole04Check, vDocumentRole04CheckView>(configuration, uowProvider, mapper);
                var document_role04_check = document_role04_check_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                        new SearchByModel() {
                            key = "id",
                            value = payload.ToString(),
                        }
                    }
                });
                view.document_role04_check_list = document_role04_check.data.list;

                return view;
            }
        }


        [LogAopInterceptor]
        public PageDocumentRole03ChangeVersionView DocumentRole03ChangeVersionLoad(long save_id, long id, string request_type_code) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view = new PageDocumentRole03ChangeVersionView();

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(save_id);
                view.view = mapper.Map<Save010ProcessView>(save);

                var repo = uow.GetRepository<vDocumentRole03Item>();
                var entity = repo.Query(r => r.save_id == save_id && r.id == id && r.request_type_code == request_type_code).FirstOrDefault();
                view.document_role03_item = mapper.Map<vDocumentRole03ItemView>(entity);

                {
                    var view_service = new BaseService<vSave010CertificationFile, vDocumentRole03ChangeCertificationFileView>(configuration, uowProvider, mapper);
                    var result = view_service.List(new PageRequest() {
                        search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "id",
                              value = save_id.ToString(),
                          }
                     }
                    });
                    view.certification_file_list = result.data.list;
                }
                {
                    var view_service = new BaseService<vSave010RequestGroup, vDocumentRole03ChangeRequestGroupView>(configuration, uowProvider, mapper);
                    var result = view_service.List(new PageRequest() {
                        search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "id",
                              value = save_id.ToString(),
                          }
                     }
                    });
                    view.request_group_list = result.data.list;
                }
                {
                    var view_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                    var result = view_service.List(new PageRequest() {
                        search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = save_id.ToString(),
                          }
                     }
                    });
                    view.history_list = result.data.list;
                }


                //var result = service.List(payload);
                //return Json(result);

                var document_role03_item_repo = uow.GetRepository<DocumentRole03Item>();
                var document_role03_item = document_role03_item_repo.Query(r => r.save_id == id && r.save_01_id == save_id && r.request_type_code == request_type_code).FirstOrDefault();
                if (document_role03_item != null) {
                    {
                        var people_repo = uow.GetRepository<DocumentRole03ChangeAddressPeople>();
                        var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                        if (people_list.Count() > 0) {
                            view.view.people_list = mapper.Map<List<SaveAddressView>>(people_list);
                        }
                    }
                    {
                        var people_repo = uow.GetRepository<DocumentRole03ChangeAddressRepresentative>();
                        var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                        if (people_list.Count() > 0) {
                            view.view.representative_list = mapper.Map<List<SaveAddressView>>(people_list);
                        }
                    }
                    {
                        var people_repo = uow.GetRepository<DocumentRole03ChangeAddressContact>();
                        var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                        if (people_list.Count() > 0) {
                            view.view.contact_address = mapper.Map<List<SaveAddressView>>(people_list).FirstOrDefault();
                        }
                    }
                    {
                        var people_repo = uow.GetRepository<DocumentRole03ChangeProduct>();
                        var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                        if (people_list.Count() > 0) {
                            view.view.product_list = mapper.Map<List<SaveProductView>>(people_list);
                        }
                    }
                    {
                        var people_repo = uow.GetRepository<DocumentRole03ChangeAddressJoiner>();
                        var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                        if (people_list.Count() > 0) {
                            view.view.joiner_list = mapper.Map<List<SaveAddressView>>(people_list);
                        }
                    }
                    view.view.request_mark_feature_code_list = document_role03_item.request_mark_feature_code_list;
                    view.view.sound_description = document_role03_item.sound_description;
                    {
                        var people_repo = uow.GetRepository<vDocumentRole03ChangeCertificationFile>();
                        var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                        if (people_list.Count() > 0) {
                            view.certification_file_list = mapper.Map<List<vDocumentRole03ChangeCertificationFileView>>(people_list);
                        }
                    }
                    {
                        var people_repo = uow.GetRepository<vDocumentRole03ChangeRequestGroup>();
                        var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                        if (people_list.Count() > 0) {
                            view.request_group_list = mapper.Map<List<vDocumentRole03ChangeRequestGroupView>>(people_list);
                        }
                    }
                }

                if (request_type_code == RequestTypeConstant.SAVE040.ToString().Replace("SAVE0", "")) {
                    var allow_repo = uow.GetRepository<RM_Save010ConsideringSimilarDocument04TypeCode>();
                    view.allow_list = mapper.Map<List<ReferenceMasterView>>(allow_repo.Query(r => !r.is_deleted));
                    foreach (var allow in view.allow_list) {
                        allow.value_1 = view.document_role03_item.consider_similar_document_remark;
                    }
                } else if (request_type_code == RequestTypeConstant.SAVE060.ToString().Replace("SAVE0", "")) {
                    var allow_repo = uow.GetRepository<RM_Save010ConsideringSimilarDocument06TypeCode>();
                    view.allow_list = mapper.Map<List<ReferenceMasterView>>(allow_repo.Query(r => !r.is_deleted));
                    view.allow_list = view.allow_list.OrderBy(r => r.index).Where(r => r.code != "TRADEMARK" && r.code != "SOUND_TRANSLATE").ToList();
                    view.allow_list = view.allow_list.Where(r =>
                        view.document_role03_item.consider_similar_document_item_status_list.IndexOf("\"" + r.code + "\":\"ALLOW\"") >= 0
                    ).ToList();
                    foreach (var allow in view.allow_list) {
                        allow.value_1 = view.document_role03_item.consider_similar_document_remark;
                    }
                } else if (request_type_code == RequestTypeConstant.SAVE120.ToString().Replace("SAVE", "")) {
                    var allow_repo = uow.GetRepository<RM_Save010ConsideringSimilarDocument12TypeCode>();
                    view.allow_list = mapper.Map<List<ReferenceMasterView>>(allow_repo.Query(r => !r.is_deleted));
                    foreach (var allow in view.allow_list) {
                        allow.value_1 = view.document_role03_item.consider_similar_document_remark;
                    }
                }

                if (!string.IsNullOrEmpty(entity.document_role03_receive_allow_list)) {
                    foreach (var allow_check in entity.document_role03_receive_allow_list.Split('|').ToList()) {
                        var allow = view.allow_list.Where(r => r.code == allow_check).FirstOrDefault();
                        if (allow != null) allow.is_check = true;
                    }
                }

                //var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                //view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(entity.save_id);

                //var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                //var result = view_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "id",
                //              value = payload.ToString(),
                //          }
                //     },
                //filter_queries = new List<string>() {
                //        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                //    }
                //});
                //view.instruction_rule_list = result.data.list;

                //var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                //var document = document_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "request_number",
                //              value = view.view.request_number,
                //          }
                //     }
                //});
                //view.document_list = document.data.list;

                //var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                //var receipt_item = receipt_item_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "save_id",
                //              value = view.view.id.ToString(),
                //          }
                //     }
                //});
                //view.receipt_item_list = receipt_item.data.list;

                //var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                //var history = history_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "save_id",
                //              value = view.view.id.ToString(),
                //          }
                //     }
                //});
                //view.history_list = history.data.list;

                //var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                //var scan = scan_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //          new SearchByModel() {
                //              key = "request_number",
                //              value = view.view.request_number,
                //          },
                //          new SearchByModel() {
                //              key = "file_id",
                //              value = "0",
                //              operation= Operation.GreaterThan,
                //          }
                //     }
                //});
                //view.document_scan_list = scan.data.list;

                //var document_role04_check_service = new BaseService<vDocumentRole04Check, vDocumentRole04CheckView>(configuration, uowProvider, mapper);
                //var document_role04_check = document_role04_check_service.List(new PageRequest() {
                //    search_by = new List<SearchByModel>() {
                //        new SearchByModel() {
                //            key = "id",
                //            value = payload.ToString(),
                //        }
                //    }
                //});
                //view.document_role04_check_list = document_role04_check.data.list;

                return view;
            }
        }

        [LogAopInterceptor]
        public virtual PageDocumentRole03ChangeVersionView DocumentRole03ChangeSave(PageDocumentRole03ChangeVersionAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var result = new PageDocumentRole03ChangeVersionView();

                var document_role03_item_repo = uow.GetRepository<DocumentRole03Item>();
                var document_role03_item = document_role03_item_repo.Query(r =>
                     r.save_id == payload.document_role03_item.id &&
                     r.save_01_id == payload.document_role03_item.save_id &&
                     r.request_type_code == payload.document_role03_item.request_type_code
                ).FirstOrDefault();

                if (document_role03_item == null) {
                    document_role03_item = new DocumentRole03Item() {
                        save_id = payload.document_role03_item.id.Value,
                        save_01_id = payload.document_role03_item.save_id,
                        save_01_index = payload.document_role03_item.save_01_index,
                        request_type_code = payload.document_role03_item.request_type_code,
                        document_role03_status_code = DocumentRole03StatusCodeConstant.DONE.ToString(),
                        document_role03_split_date = DateTime.Now,
                        document_role03_spliter_by = userId,
                        document_role03_receiver_by = payload.document_role03_item.document_role03_receiver_by,
                    };
                }

                document_role03_item.request_mark_feature_code_list = payload.view.request_mark_feature_code_list;
                document_role03_item.sound_description = payload.view.sound_description;
                document_role03_item.document_role03_receive_status_code = DocumentRole03ReceiveStatusCodeConstant.SAVE.ToString();
                document_role03_item.document_role03_receive_allow_list = string.Join("|", payload.allow_list.Where(r => r.is_check).Select(r => r.code));

                document_role03_item.value_01 = payload.document_role03_item.value_01;
                document_role03_item.value_02 = payload.document_role03_item.value_02;
                document_role03_item.value_03 = payload.document_role03_item.value_03;
                document_role03_item.value_04 = payload.document_role03_item.value_04;
                document_role03_item.value_05 = payload.document_role03_item.value_05;

                document_role03_item_repo.Update(document_role03_item);
                uow.SaveChanges();

                //////////

                {
                    var people_repo = uow.GetRepository<DocumentRole03ChangeAddressPeople>();
                    var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.view.people_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<DocumentRole03ChangeAddressPeople>(model);
                        entity.document_role03_item_id = document_role03_item.id;
                        people_repo.Add(entity);
                    }
                }
                {
                    var people_repo = uow.GetRepository<DocumentRole03ChangeAddressRepresentative>();
                    var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.view.representative_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<DocumentRole03ChangeAddressRepresentative>(model);
                        entity.document_role03_item_id = document_role03_item.id;
                        people_repo.Add(entity);
                    }
                }
                {
                    var people_repo = uow.GetRepository<DocumentRole03ChangeAddressContact>();
                    var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    payload.view.contact_address.id = null;
                    var entity = mapper.Map<DocumentRole03ChangeAddressContact>(payload.view.contact_address);
                    entity.document_role03_item_id = document_role03_item.id;
                    people_repo.Add(entity);
                }
                {
                    var people_repo = uow.GetRepository<DocumentRole03ChangeProduct>();
                    var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.view.product_list.Where(r => !r.is_deleted)) {
                        foreach (var description in model.description.Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries)) {
                            people_repo.Add(new DocumentRole03ChangeProduct() {
                                document_role03_item_id = document_role03_item.id,
                                request_item_sub_type_1_code = model.request_item_sub_type_1_code,
                                request_item_sub_type_2_code = model.request_item_sub_type_2_code,
                                description = description,
                            });
                        }
                    }
                }
                {
                    var people_repo = uow.GetRepository<DocumentRole03ChangeAddressJoiner>();
                    var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.view.joiner_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<DocumentRole03ChangeAddressJoiner>(model);
                        entity.document_role03_item_id = document_role03_item.id;
                        people_repo.Add(entity);
                    }
                }
                {
                    var people_repo = uow.GetRepository<DocumentRole03ChangeCertificationFile>();
                    var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.certification_file_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<DocumentRole03ChangeCertificationFile>(model);
                        entity.document_role03_item_id = document_role03_item.id;
                        people_repo.Add(entity);
                    }
                }
                {
                    var people_repo = uow.GetRepository<DocumentRole03ChangeRequestGroup>();
                    var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.request_group_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<DocumentRole03ChangeRequestGroup>(model);
                        entity.document_role03_item_id = document_role03_item.id;
                        people_repo.Add(entity);
                    }
                }

                uow.SaveChanges();

                return result;
            }
        }

        //void DocumentRole03ChangeItemDelete<T>(IUnitOfWork uow, long document_role03_item_id, List<BaseModel> object_list) where T : DocumentRole03ChangeItemEntityBase {
        //    var people_repo = uow.GetRepository<T>();
        //    var people_list = people_repo.Query(r => r.document_role03_item_id == document_role03_item_id && !r.is_deleted);
        //    foreach (var people in people_list) {
        //        people.is_deleted = true;
        //        people_repo.Update(people);
        //    }
        //    foreach (var model in object_list) {
        //        model.id = null;
        //        var entity = mapper.Map<T>(model);
        //        entity.document_role03_item_id = document_role03_item_id;
        //        people_repo.Add(entity);
        //    }
        //}

        [LogAopInterceptor]
        public virtual PageDocumentRole03ChangeVersionView DocumentRole03ChangeSend(PageDocumentRole03ChangeVersionAddModel payload) {
            var result = DocumentRole03ChangeSave(payload);

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var document_role03_item_repo = uow.GetRepository<DocumentRole03Item>();
                var document_role03_item = document_role03_item_repo.Query(r => r.save_id == payload.document_role03_item.id && r.save_01_id == payload.document_role03_item.save_id && r.request_type_code == payload.document_role03_item.request_type_code).FirstOrDefault();

                document_role03_item.document_role03_receive_status_code = DocumentRole03ReceiveStatusCodeConstant.SEND.ToString();

                //document_role03_item.value_01 = payload.document_role03_item.value_01;
                //document_role03_item.value_02 = payload.document_role03_item.value_02;
                //document_role03_item.value_03 = payload.document_role03_item.value_03;
                //document_role03_item.value_04 = payload.document_role03_item.value_04;
                //document_role03_item.value_05 = payload.document_role03_item.value_05;

                document_role03_item_repo.Update(document_role03_item);
                uow.SaveChanges();

                //////////

                {
                    var people_repo = uow.GetRepository<Save010AddressPeople>();
                    var people_list = people_repo.Query(r => r.save_id == payload.view.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.view.people_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<Save010AddressPeople>(model);
                        entity.save_id = payload.view.id.Value;
                        people_repo.Add(entity);
                    }
                }
                {
                    var people_repo = uow.GetRepository<Save010AddressRepresentative>();
                    var people_list = people_repo.Query(r => r.save_id == payload.view.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.view.representative_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<Save010AddressRepresentative>(model);
                        entity.save_id = payload.view.id.Value;
                        people_repo.Add(entity);
                    }
                }
                {
                    var people_repo = uow.GetRepository<Save010AddressContact>();
                    var people_list = people_repo.Query(r => r.save_id == payload.view.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    payload.view.contact_address.id = null;
                    var entity = mapper.Map<Save010AddressContact>(payload.view.contact_address);
                    entity.save_id = payload.view.id.Value;
                    people_repo.Add(entity);
                }
                {
                    var people_repo = uow.GetRepository<Save010Product>();
                    var people_list = people_repo.Query(r => r.save_id == payload.view.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.view.product_list.Where(r => !r.is_deleted)) {
                        foreach (var description in model.description.Split(new string[] { "  " }, StringSplitOptions.RemoveEmptyEntries)) {
                            people_repo.Add(new Save010Product() {
                                save_id = payload.view.id.Value,
                                request_item_sub_type_1_code = model.request_item_sub_type_1_code,
                                request_item_sub_type_2_code = model.request_item_sub_type_2_code,
                                description = description,
                            });
                        }
                    }
                }
                {

                    var people_repo = uow.GetRepository<Save010>();
                    var people = people_repo.Get(payload.view.id.Value);
                    people.request_mark_feature_code_list = payload.view.request_mark_feature_code_list;
                    people.sound_description = payload.view.sound_description;
                }
                {
                    var people_repo = uow.GetRepository<Save010AddressJoiner>();
                    var people_list = people_repo.Query(r => r.save_id == payload.view.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.view.joiner_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<Save010AddressJoiner>(model);
                        entity.save_id = payload.view.id.Value;
                        people_repo.Add(entity);
                    }
                }
                {
                    var people_repo = uow.GetRepository<Save010CertificationFile>();
                    var people_list = people_repo.Query(r => r.save_id == payload.view.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.certification_file_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<Save010CertificationFile>(model);
                        entity.save_id = payload.view.id.Value;
                        people_repo.Add(entity);
                    }
                }
                {
                    var people_repo = uow.GetRepository<Save010RequestGroup>();
                    var people_list = people_repo.Query(r => r.save_id == payload.view.id && !r.is_deleted);
                    foreach (var people in people_list) {
                        people.is_deleted = true;
                        people_repo.Update(people);
                    }
                    foreach (var model in payload.request_group_list.Where(r => !r.is_deleted)) {
                        model.id = null;
                        var entity = mapper.Map<Save010RequestGroup>(model);
                        entity.save_id = payload.view.id.Value;
                        people_repo.Add(entity);
                    }
                }

                //var product_list = payload.product_list;
                //payload.product_list = null;
                //var entity = repo.Update(mapper.Map<Save010>(payload));

                //var request_group_repo = uow.GetRepository<Save010RequestGroup>();
                //foreach (var request_group in payload.request_group_list) {
                //    request_group_repo.Update(mapper.Map<Save010RequestGroup>(request_group));
                //}

                uow.SaveChanges();

                ////var view = mapper.Map<Save010ProcessView>(entity);

                ////var product_service = new SaveProductService<SaveProductAddModel, Save010Product, SaveProductView>(configuration, uowProvider, mapper);
                ////view.product_list = product_service.Save(product_list, payload.id.Value);


                return result;
            }
        }

        [LogAopInterceptor]
        public virtual List<vDocumentRole03ItemGroupView> DocumentRole03ChangeSendAll(List<vDocumentRole03ItemGroupAddModel> payload) {
            //using (var uow = uowProvider.CreateUnitOfWork()) {
            //var repo = uow.GetRepository<Save010>();

            foreach (var model in payload) {
                var document = DocumentRole03ChangeLoad(model.save_id, model.save_01_index.Value);
                foreach (var item in document.document_role03_item_list.Where(r => r.document_role03_receive_status_code == DocumentRole03ReceiveStatusCodeConstant.SAVE.ToString())) {
                    var version_list = DocumentRole03ChangeVersionList(model.save_id, item.request_type_code);
                    foreach (var version in version_list.Where(r => r.document_role03_receive_status_code == DocumentRole03ReceiveStatusCodeConstant.SAVE.ToString())) {
                        //var document = DocumentRole03ChangeLoad(model.save_id, model.save_01_index.Value);
                        //var version_list = DocumentRole03ChangeVersionList(model.save_id, item.request_type_code);
                        var version_item = DocumentRole03ChangeVersionLoad(model.save_id, version.id.Value, version.request_type_code);

                        version_item.view.checking_similar_wordsoundtranslate_list = null;

                        var version_model = new PageDocumentRole03ChangeVersionAddModel() {
                            view = mapper.Map<Save010AddModel>(version_item.view),
                            allow_list = mapper.Map<List<ReferenceMasterModel>>(version_item.allow_list),
                            certification_file_list = mapper.Map<List<vDocumentRole03ChangeCertificationFileAddModel>>(version_item.certification_file_list),
                            request_group_list = mapper.Map<List<vDocumentRole03ChangeRequestGroupAddModel>>(version_item.request_group_list),
                            document_role03_item = mapper.Map<vDocumentRole03ItemAddModel>(version_item.document_role03_item),
                        };
                        var version_view = DocumentRole03ChangeSend(version_model);
                    }
                }
            }

            return mapper.Map<List<vDocumentRole03ItemGroupView>>(payload);
            //}
        }

        [LogAopInterceptor]
        public List<vDocumentRole04ReleaseRequestView> DocumentRole04ReleaseRequestSend(List<vDocumentRole04ReleaseRequestAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();

                foreach (var document_role05_release_request in payload) {
                    foreach (var document_role04_release_request in payload) {
                        var entity = repo.Query(r => r.id == document_role04_release_request.id).FirstOrDefault();

                        if (entity.document_role04_receive_status_code.StartsWith("DRAFT")) {
                            entity.document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.SEND_WAIT.ToString();
                            entity.document_role04_receiver_by = userId;
                            entity.document_role04_receive_send_date = DateTime.Now;
                            entity.document_role04_receive_remark = document_role04_release_request.document_role04_receive_remark;

                            entity.document_role04_release_request_send_type_code = document_role04_release_request.document_role04_release_request_send_type_code;

                            entity.document_role05_receive_date = DateTime.Now;
                            entity.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.DRAFT.ToString();

                            repo.Update(entity);
                        }
                    }
                }

                uow.SaveChanges();
                return mapper.Map<List<vDocumentRole04ReleaseRequestView>>(payload);
            }
        }


        [LogAopInterceptor]
        public PageDocumentRole04ReleaseRequestView DocumentRole05ReleaseRequestSend(List<vDocumentRole04ReleaseRequestAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();
                var repo = uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();

                foreach (var document_role05_release_request in payload) {
                    var entity = repo.Query(r => r.id == document_role05_release_request.id).FirstOrDefault();

                    //if (entity.document_role05_receive_status_code.StartsWith("DRAFT")) {
                    if (entity.document_role04_release_request_send_type_code == DocumentRole04ReleaseRequestSendTypeCodeConstant.DOCUMENT.ToString()) {
                        entity.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.SEND.ToString();
                    } else {
                        entity.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.WAIT_CHANGE.ToString();
                    }
                    entity.document_role05_receiver_by = userId;
                    entity.document_role05_receive_send_date = DateTime.Now;
                    entity.document_role04_receive_remark = document_role05_release_request.document_role04_receive_remark;

                    entity.document_role04_release_request_send_type_code = document_role05_release_request.document_role04_release_request_send_type_code;

                    repo.Update(entity);

                    if (entity.document_role04_release_request_send_type_code == DocumentRole04ReleaseRequestSendTypeCodeConstant.DOCUMENT.ToString()) {
                        ForceIntructionRuleAdd(document_role05_release_request.save_id, document_role05_release_request.post_round_instruction_rule_code, PostRoundTypeCodeConstant.SEND_MORE, 1, "", "", "", "", "", false, null);
                    } else if (entity.document_role04_release_request_send_type_code == DocumentRole04ReleaseRequestSendTypeCodeConstant.CHECKING.ToString()) {
                        var save = save_repo.Get(document_role05_release_request.save_id);
                        save.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();
                        save.considering_receive_remark = document_role05_release_request.document_role04_receive_remark;
                        save_repo.Update(save);
                    }
                    //}
                }

                uow.SaveChanges();
                return DocumentRole04ReleaseRequestLoad(payload[0].save_tag_id, payload[0].document_role04_release_request_type_code);
            }
        }

        [LogAopInterceptor]
        public PageDocumentRole04ReleaseRequestView DocumentRole05ReleaseRequestSendChange(List<vDocumentRole04ReleaseRequestAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();

                foreach (var document_role05_release_request in payload) {
                    var entity = repo.Query(r => r.id == document_role05_release_request.id).FirstOrDefault();
                    entity.document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.DRAFT_FIX.ToString();

                    entity.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.WAIT_CHANGE.ToString();
                    entity.document_role05_receiver_by = userId;
                    entity.document_role05_receive_send_date = DateTime.Now;
                    entity.document_role04_receive_remark = document_role05_release_request.document_role04_receive_remark;

                    repo.Update(entity);
                }

                uow.SaveChanges();
                return DocumentRole04ReleaseRequestLoad(payload[0].save_tag_id, payload[0].document_role04_release_request_type_code);
            }
        }


        [LogAopInterceptor]
        public virtual List<PageDocumentRole04CheckView> DocumentRole04CheckSend(List<PageDocumentRole04CheckAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view_list = new List<PageDocumentRole04CheckView>();
                var post_round_repo = uow.GetRepository<PostRound>();
                var post_round_instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload) {
                    var post_round = post_round_repo.Get(model.document_role04_check_list[0].id.Value);
                    post_round.document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.SEND_WAIT.ToString();
                    post_round.document_role04_receive_check_list = model.document_role04_check_list[0].document_role04_receive_check_list;
                    post_round.document_role04_receive_send_date = DateTime.Now;

                    if (string.IsNullOrEmpty(post_round.document_role05_receive_status_code)) {
                        post_round.document_role05_status_code = DocumentRole05StatusCodeConstant.WAIT.ToString();
                    } else {
                        post_round.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.DRAFT_CHANGE.ToString();
                    }
                    post_round_repo.Update(post_round);

                    foreach (var instruction_rule in model.document_role04_check_list) {
                        var post_round_instruction_rule = post_round_instruction_rule_repo.Get(instruction_rule.post_round_instruction_rule_id.Value);

                        post_round_instruction_rule.document_role04_send_type_code = instruction_rule.document_role04_send_type_code;
                        post_round_instruction_rule.document_role04_check_date = DateTime.Now;
                        post_round_instruction_rule.document_role04_check_remark = instruction_rule.document_role04_check_remark;
                        post_round_instruction_rule.new_instruction_rule_code = instruction_rule.new_instruction_rule_code;
                        post_round_instruction_rule_repo.Update(post_round_instruction_rule);
                    }

                    view_list.Add(DocumentRole04CheckLoad(payload[0].document_role04_check_list[0].id.Value));
                }
                uow.SaveChanges();

                return view_list;
            }
        }


        [LogAopInterceptor]
        public virtual List<PageDocumentRole04CheckView> DocumentRole05CheckSend(List<PageDocumentRole04CheckAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view_list = new List<PageDocumentRole04CheckView>();
                var save_repo = uow.GetRepository<Save010>();
                var post_round_repo = uow.GetRepository<PostRound>();
                var post_round_instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload) {
                    var post_round = post_round_repo.Get(model.document_role04_check_list[0].id.Value);
                    post_round.document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.SEND_DONE.ToString();
                    post_round.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.SEND.ToString();
                    post_round.document_role04_receive_check_list = model.document_role04_check_list[0].document_role04_receive_check_list;
                    post_round.document_role05_receive_send_date = DateTime.Now;
                    post_round_repo.Update(post_round);

                    foreach (var instruction_rule in model.document_role04_check_list) {
                        var post_round_instruction_rule = post_round_instruction_rule_repo.Get(instruction_rule.post_round_instruction_rule_id.Value);
                        post_round_instruction_rule.document_role04_send_type_code = instruction_rule.document_role04_send_type_code;
                        //post_round_instruction_rule.document_role04_check_date = DateTime.Now;
                        post_round_instruction_rule.document_role04_check_remark = instruction_rule.document_role04_check_remark;
                        post_round_instruction_rule.new_instruction_rule_code = instruction_rule.new_instruction_rule_code;
                        post_round_instruction_rule_repo.Update(post_round_instruction_rule);
                    }

                    var document_role04_send_type_code_list = model.document_role04_check_list.Select(r => r.document_role04_send_type_code).Distinct();
                    foreach (var document_role04_send_type_code in document_role04_send_type_code_list) {
                        if (document_role04_send_type_code.StartsWith("ROUND_")) {
                            //int book_expired_day = 60;
                            //var book_start_date = DateTime.Now;

                            var post_round_new = new PostRound() {
                                post_round_instruction_rule_code = post_round.post_round_instruction_rule_code,
                                post_round_type_code = post_round.post_round_type_code,
                                object_id = post_round.object_id,
                                round_index = document_role04_send_type_code == DocumentRole04SendTypeCodeConstant.ROUND_1.ToString() ? 1 : post_round.round_index + 1,
                                book_by = post_round.book_by,
                                //book_number = HelpService.GetBookNumber(uowProvider),
                                //book_start_date = book_start_date,
                                //book_end_date = book_start_date.AddDays(book_expired_day),
                                //book_expired_day = book_expired_day,


                                ////No neccessory to considering
                                //post_round_document_post_status_code = PostRoundDocumentPostStatusCodeConstant.WAIT.ToString(),
                                //post_round_document_post_status_code = PostRoundDocumentPostStatusCodeConstant.DONE.ToString(),
                                //post_round_document_post_date = DateTime.Now,

                                //reference_number = HelpService.GetReferenceNumber(uowProvider),
                                document_role02_status_code = DocumentRole02StatusCodeConstant.WAIT.ToString(),

                                ////K.May change flow (deisgn "2 เจ้าหน้าที่ออกหนังสือ R7" => "2 เจ้าหน้าที่ออกหนังสือ R9")
                                //document_role02_print_document_date = book_start_date,
                                //document_role02_print_cover_status_code = DocumentRole02PrintCoverStatusCodeConstant.WAIT_COVER.ToString(),
                            };

                            using (var _uow = uowProvider.CreateUnitOfWork()) {
                                var _post_round_repo = _uow.GetRepository<PostRound>();
                                _post_round_repo.Add(post_round_new);
                                _uow.SaveChanges();
                            }

                            foreach (var document_role04_check in model.document_role04_check_list.Where(r => r.document_role04_send_type_code == document_role04_send_type_code).ToList()) {
                                //Send to document-role02-item/list
                                if (document_role04_check.instruction_rule_code.StartsWith("CASE_") ||
                                    document_role04_check.instruction_rule_code.StartsWith("ROLE_")) {

                                    using (var _uow = uowProvider.CreateUnitOfWork()) {
                                        var _post_round_instruction_rule_repo = _uow.GetRepository<PostRoundInstructionRule>();
                                        _post_round_instruction_rule_repo.Add(new PostRoundInstructionRule() {
                                            post_round_id = post_round_new.id,
                                            save010_instruction_rule_id = document_role04_check.instruction_rule_id.Value,
                                            document_role02_check_status_code = DocumentRole02CheckStatusCodeConstant.DRAFT.ToString(),
                                        });
                                        _uow.SaveChanges();
                                    }
                                }
                            }
                        } else if (document_role04_send_type_code == DocumentRole04SendTypeCodeConstant.CHECKING.ToString()) {
                            var save = save_repo.Get(model.document_role04_check_list[0].save_id.Value);
                            save.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();
                            save.considering_receive_remark = string.Join(", ", model.document_role04_check_list.Where(r => r.document_role04_send_type_code == document_role04_send_type_code).Select(r => r.document_role04_check_remark));
                            save_repo.Update(save);
                        } else if (document_role04_send_type_code == DocumentRole04SendTypeCodeConstant.PUBLIC.ToString()) {
                            var save = save_repo.Get(model.document_role04_check_list[0].save_id.Value);
                            save.public_source_code = PublicSourceCodeConstant.DOCUMENT.ToString();
                            save.public_type_code = PublicTypeCodeConstant.PUBLIC.ToString();
                            save.public_status_code = PublicStatusCodeConstant.WAIT.ToString();
                            save_repo.Update(save);
                        }
                    }

                    //TODO
                    //post_round.document_role05_status_code = DocumentRole05StatusCodeConstant.WAIT.ToString();

                    view_list.Add(DocumentRole04CheckLoad(payload[0].document_role04_check_list[0].id.Value));
                }
                uow.SaveChanges();

                return view_list;
            }
        }


        [LogAopInterceptor]
        public virtual List<vDocumentRole04CreateView> DocumentRole04CreateSend(List<vDocumentRole04CreateAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view_list = new List<vDocumentRole04CreateView>();
                var repo = uow.GetRepository<DocumentRole04Create>();
                //var post_round_instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload) {
                    var entity = repo.Get(model.id.Value);

                    entity.document_role04_create_type_code = model.document_role04_create_type_code;
                    entity.document_role04_create_instruction_rule_code = model.instruction_rule_code;
                    entity.document_role04_create_check_list = model.document_role04_create_check_list;
                    entity.value_01 = model.value_01;
                    entity.value_02 = model.value_02;
                    entity.value_03 = model.value_03;
                    entity.value_04 = model.value_04;
                    entity.value_05 = model.value_05;

                    entity.document_role04_create_send_date = DateTime.Now;
                    entity.document_role04_create_status_code = DocumentRole04CreateStatusCodeConstant.SEND_WAIT.ToString();

                    entity.document_role05_create_status_code = DocumentRole05CreateStatusCodeConstant.DRAFT.ToString();

                    repo.Update(entity);
                }
                uow.SaveChanges();

                return view_list;
            }
        }

        [LogAopInterceptor]
        public virtual List<vDocumentRole04CreateView> DocumentRole05CreateSend(List<vDocumentRole04CreateAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();
                var view_list = new List<vDocumentRole04CreateView>();
                var repo = uow.GetRepository<DocumentRole04Create>();
                foreach (var model in payload) {
                    var entity = repo.Get(model.id.Value);

                    entity.document_role04_create_type_code = model.document_role04_create_type_code;
                    entity.document_role04_create_instruction_rule_code = model.instruction_rule_code;
                    entity.document_role04_create_check_list = model.document_role04_create_check_list;
                    entity.value_01 = model.value_01;
                    entity.value_02 = model.value_02;
                    entity.value_03 = model.value_03;
                    entity.value_04 = model.value_04;
                    entity.value_05 = model.value_05;

                    entity.document_role04_create_status_code = DocumentRole04CreateStatusCodeConstant.SEND_DONE.ToString();
                    entity.document_role05_create_by = userId;
                    entity.document_role05_create_send_date = DateTime.Now;
                    entity.document_role05_create_status_code = DocumentRole05CreateStatusCodeConstant.SEND.ToString();

                    repo.Update(entity);

                    if (entity.document_role04_create_type_code == DocumentRole04CreateTypeCodeConstant.CHECKING.ToString()) {
                        var save = save_repo.Get(model.save_id);
                        save.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT.ToString();
                        save.considering_receive_remark = model.value_01;
                        save_repo.Update(save);
                    } else if (entity.document_role04_create_type_code == DocumentRole04CreateTypeCodeConstant.PUBLIC.ToString()) {
                        var save = save_repo.Get(model.save_id);
                        save.public_source_code = PublicSourceCodeConstant.DOCUMENT.ToString();
                        save.public_type_code = PublicTypeCodeConstant.PUBLIC.ToString();
                        save.public_status_code = PublicStatusCodeConstant.WAIT.ToString();
                        save_repo.Update(save);
                    } else if (entity.document_role04_create_type_code == DocumentRole04CreateTypeCodeConstant.RELEASE.ToString()) {
                        ForceStatusD(model.save_id, "", "", "", "", "", "");
                        //} else if (entity.document_role04_create_type_code == DocumentRole04CreateTypeCodeConstant.PUBLIC_41.ToString()) {
                        //    var save = save_repo.Get(model.save_id);
                        //    save.public_source_code = PublicSourceCodeConstant.DOCUMENT.ToString();
                        //    save.public_type_code = PublicTypeCodeConstant.PUBLIC_CASE41.ToString();
                        //    save.public_status_code = PublicStatusCodeConstant.WAIT.ToString();
                        //    save_repo.Update(save);
                    } else if (entity.document_role04_create_type_code == DocumentRole04CreateTypeCodeConstant.RECOVER.ToString()) {
                        var save = save_repo.Get(model.save_id);
                        save.trademark_status_code = TrademarkStatusCodeConstant.P.ToString();
                        save_repo.Update(save);
                    }

                    //if (entity.document_role04_create_type_code != DocumentRole04CreateTypeCodeConstant.RECOVER.ToString()) {
                    //    ForceIntructionRuleAdd(entity.save_id, entity.document_role04_create_instruction_rule_code, PostRoundTypeCodeConstant.SEND_MORE, 1, entity.value_01, entity.value_02, entity.value_03, entity.value_04, entity.value_05, false, null);
                    //}
                }
                uow.SaveChanges();

                return view_list;
            }
        }

        public PostRound ForceIntructionRuleAdd(long save_id, string instruction_rule_code, PostRoundTypeCodeConstant type, int round_index, string value_01, string value_02, string value_03, string value_04, string value_05, bool is_generate_reference, long? save010_instruction_rule_id, bool is_generate_book_start_date = true) {
            var instruction_rule_entity = new Save010InstructionRule() {
                save_id = save_id,
                instruction_date = DateTime.Now,
                instruction_rule_code = instruction_rule_code,
                considering_book_status_code = ConsideringBookStatusCodeConstant.NORMAL.ToString(),
                considering_instruction_status_code = ConsideringInstructionStatusCodeConstant.SEND.ToString(),
                instruction_send_date = DateTime.Now,
                considering_instruction_rule_status_code = ConsideringInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString(),
            };

            if (!save010_instruction_rule_id.HasValue) {
                using (var _uow = uowProvider.CreateUnitOfWork()) {
                    var instruction_rule_repo = _uow.GetRepository<Save010InstructionRule>();
                    instruction_rule_repo.Add(instruction_rule_entity);
                    _uow.SaveChanges();

                    save010_instruction_rule_id = instruction_rule_entity.id;
                }
            }

            int book_expired_day = 60;
            var post_round_entity = new PostRound() {
                post_round_instruction_rule_code = instruction_rule_code,
                post_round_type_code = type.ToString(),
                object_id = save_id,
                round_index = round_index,
                book_by = userId,

                reference_number = is_generate_reference ? HelpService.GetReferenceNumber(uowProvider) : "",

                //No neccessory to considering
                post_round_document_post_status_code = PostRoundDocumentPostStatusCodeConstant.DONE.ToString(),
                post_round_document_post_date = DateTime.Now,

                document_role02_status_code = DocumentRole02StatusCodeConstant.WAIT.ToString(),
            };

            if (is_generate_book_start_date) {
                post_round_entity.book_number = HelpService.GetBookNumber(uowProvider);
                post_round_entity.book_start_date = DateTime.Now;
                post_round_entity.book_end_date = DateTime.Now.AddDays(book_expired_day);
                post_round_entity.book_expired_day = book_expired_day;
            }

            using (var _uow = uowProvider.CreateUnitOfWork()) {
                var _post_round_repo = _uow.GetRepository<PostRound>();
                _post_round_repo.Add(post_round_entity);
                _uow.SaveChanges();
            }

            using (var _uow = uowProvider.CreateUnitOfWork()) {
                var _post_round_instruction_rule_repo = _uow.GetRepository<PostRoundInstructionRule>();
                _post_round_instruction_rule_repo.Add(new PostRoundInstructionRule() {
                    post_round_id = post_round_entity.id,
                    save010_instruction_rule_id = save010_instruction_rule_id.Value,
                    document_role02_check_status_code = DocumentRole02CheckStatusCodeConstant.DRAFT.ToString(),
                    value_01 = value_01,
                    value_02 = value_02,
                    value_03 = value_03,
                    value_04 = value_04,
                    value_05 = value_05,
                });
                _uow.SaveChanges();
            }

            return post_round_entity;
        }

        [LogAopInterceptor]
        public virtual List<vDocumentRole04ReleaseView> DocumentRole04ReleaseSend(List<vDocumentRole04ReleaseAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view_list = new List<vDocumentRole04ReleaseView>();
                var repo = uow.GetRepository<DocumentRole04Release>();

                foreach (var model in payload) {
                    var entity = repo.Get(model.id.Value);

                    if (entity == null) {
                        entity = new DocumentRole04Release() {
                            save_id = model.save_id.Value,
                        };
                    }

                    entity.document_role04_release_type_code = model.document_role04_release_type_code;
                    entity.document_role04_release_instruction_rule_code = model.instruction_rule_code;
                    entity.document_role04_release_check_list = model.document_role04_release_check_list;
                    entity.value_01 = model.value_01;
                    entity.value_02 = model.value_02;
                    entity.value_03 = model.value_03;
                    entity.value_04 = model.value_04;
                    entity.value_05 = model.value_05;

                    entity.document_role04_release_send_date = DateTime.Now;
                    entity.document_role04_release_status_code = DocumentRole04ReleaseStatusCodeConstant.SEND_WAIT.ToString();

                    entity.document_role05_release_status_code = DocumentRole05ReleaseStatusCodeConstant.DRAFT.ToString();

                    repo.Update(entity);
                }
                uow.SaveChanges();

                return view_list;
            }
        }

        [LogAopInterceptor]
        public virtual List<vDocumentRole04ReleaseView> DocumentRole05ReleaseSend(List<vDocumentRole04ReleaseAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();
                var new_instruction_rule_repo = uow.GetRepository<vDocumentRole04ReleaseNewInstructionRule>();
                var view_list = new List<vDocumentRole04ReleaseView>();
                var repo = uow.GetRepository<DocumentRole04Release>();

                var save_tag_repo = uow.GetRepository<Save010CheckingTagSimilar>();
                var save_tag_ir_repo = uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();

                foreach (var model in payload) {
                    var entity = repo.Get(model.id.Value);

                    if (entity == null) {
                        entity = new DocumentRole04Release() {
                            save_id = model.save_id.Value,
                        };
                    }

                    entity.document_role04_release_type_code = model.document_role04_release_type_code;
                    entity.document_role04_release_instruction_rule_code = model.instruction_rule_code;
                    entity.document_role04_release_check_list = model.document_role04_release_check_list;
                    entity.value_01 = model.value_01;
                    entity.value_02 = model.value_02;
                    entity.value_03 = model.value_03;
                    entity.value_04 = model.value_04;
                    entity.value_05 = model.value_05;

                    entity.document_role04_release_status_code = DocumentRole04ReleaseStatusCodeConstant.SEND_DONE.ToString();
                    entity.document_role05_release_by = userId;
                    entity.document_role05_release_send_date = DateTime.Now;
                    entity.document_role05_release_status_code = DocumentRole05ReleaseStatusCodeConstant.SEND.ToString();

                    repo.Update(entity);

                    if (entity.document_role04_release_type_code == DocumentRole04ReleaseTypeCodeConstant.RELEASE.ToString()) {
                        ForceStatusD(model.save_id.Value, entity.document_role04_release_instruction_rule_code, model.value_01, model.value_02, model.value_03, model.value_04, model.value_05);
                    } else if (entity.document_role04_release_type_code == DocumentRole04ReleaseTypeCodeConstant.CHECKING.ToString()) {
                        var save = save_repo.Get(model.save_id.Value);
                        save.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT.ToString();
                        save.considering_receive_remark = model.value_03;
                        save_repo.Update(save);
                    } else if (entity.document_role04_release_type_code == DocumentRole04ReleaseTypeCodeConstant.NEW.ToString()) {
                        var new_instruction_rule = new_instruction_rule_repo.Query(r => r.save_id == model.save_id.Value).OrderByDescending(r => r.post_round_id).FirstOrDefault();
                        ForceIntructionRuleAdd(new_instruction_rule.save_id, new_instruction_rule.instruction_rule_code, (PostRoundTypeCodeConstant)Enum.Parse(typeof(PostRoundTypeCodeConstant), new_instruction_rule.post_round_type_code), new_instruction_rule.round_index.Value, new_instruction_rule.value_01, new_instruction_rule.value_02, new_instruction_rule.value_03, new_instruction_rule.value_04, new_instruction_rule.value_05, !string.IsNullOrEmpty(new_instruction_rule.reference_number), new_instruction_rule.save010_instruction_rule_id, false);
                    } else if (entity.document_role04_release_type_code == DocumentRole04ReleaseTypeCodeConstant.WAIT.ToString()) {
                        //TODO
                    }
                }
                uow.SaveChanges();


                return view_list;
            }
        }


        internal void ForceStatusD(long save_id, string instruction_code, string value_01, string value_02, string value_03, string value_04, string value_05) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var public_service = new PublicProcessService(configuration, uowProvider, mapper);

                var save_repo = uow.GetRepository<Save010>();

                var save_tag_repo = uow.GetRepository<Save010CheckingTagSimilar>();
                var save_tag_ir_repo = uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();

                var save = save_repo.Get(save_id);
                save.trademark_status_code = TrademarkStatusCodeConstant.D.ToString();
                save_repo.Update(save);

                //Create Torkor to D
                if (!string.IsNullOrEmpty(instruction_code)) {
                    ForceIntructionRuleAdd(save_id, instruction_code, PostRoundTypeCodeConstant.SEND_MORE, 1, value_01, value_02, value_03, value_04, value_05, false, null);
                }

                //Create Torkor to under D
                {
                    var save_tag_list = save_tag_repo.Query(r =>
                                                    r.save_tag_id == save_id &&
                                                    r.save_tag_id != r.save_id &&
                                                    r.is_owner_same == false &&
                                                    r.is_same.Value &&
                                                    (r.is_same_approve || r.is_like_approve)).ToList();
                    // Check other request number is similar this request number -> If true goto document
                    if (save_tag_list.Count() > 0) {
                        foreach (var save_tag in save_tag_list) {
                            var save_tag_other_list = save_tag_repo.Query(r =>
                                                            r.save_id == save_tag.save_id &&
                                                            r.save_tag_id != r.save_id &&
                                                            r.is_owner_same == false &&
                                                            r.is_same.Value &&
                                                            //
                                                            r.save_tag_id != save_id &&
                                                            //
                                                            (r.is_same_approve || r.is_like_approve)).ToList();
                            if (save_tag_other_list.Count() == 0) {
                                public_service.ForcePublic(save_tag.save_id, PublicTypeCodeConstant.PUBLIC, PublicSourceCodeConstant.DOCUMENT, true);
                            }
                        }
                    }
                }

                uow.SaveChanges();
            }
        }



        [LogAopInterceptor]
        public virtual List<vDocumentRole04Release20View> DocumentRole04Release20Send(List<vDocumentRole04Release20AddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var view_list = new List<vDocumentRole04Release20View>();
                var repo = uow.GetRepository<DocumentRole04Release20>();

                foreach (var model in payload) {
                    var entity = repo.Get(model.id.Value);

                    if (entity == null) {
                        entity = new DocumentRole04Release20() {
                            save_id = model.save_id,
                        };
                    }

                    entity.document_role04_release_20_type_code = model.document_role04_release_20_type_code;
                    entity.document_role04_release_20_instruction_rule_code = model.instruction_rule_code;
                    entity.document_role04_release_20_check_list = model.document_role04_release_20_check_list;
                    entity.value_01 = model.value_01;
                    entity.value_02 = model.value_02;
                    entity.value_03 = model.value_03;
                    entity.value_04 = model.value_04;
                    entity.value_05 = model.value_05;

                    entity.document_role04_release_20_send_date = DateTime.Now;
                    entity.document_role04_release_20_status_code = DocumentRole04ReleaseStatusCodeConstant.SEND_WAIT.ToString();

                    entity.document_role05_release_20_status_code = DocumentRole05ReleaseStatusCodeConstant.DRAFT.ToString();

                    repo.Update(entity);
                }
                uow.SaveChanges();

                return view_list;
            }
        }

        [LogAopInterceptor]
        public virtual List<vDocumentRole04Release20View> DocumentRole05Release20Send(List<vDocumentRole04Release20AddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();
                var view_list = new List<vDocumentRole04Release20View>();
                var repo = uow.GetRepository<DocumentRole04Release20>();
                foreach (var model in payload) {
                    var entity = repo.Get(model.id.Value);

                    if (entity == null) {
                        entity = new DocumentRole04Release20() {
                            save_id = model.save_id,
                        };
                    }

                    entity.document_role04_release_20_type_code = model.document_role04_release_20_type_code;
                    entity.document_role04_release_20_instruction_rule_code = model.instruction_rule_code;
                    entity.document_role04_release_20_check_list = model.document_role04_release_20_check_list;
                    entity.value_01 = model.value_01;
                    entity.value_02 = model.value_02;
                    entity.value_03 = model.value_03;
                    entity.value_04 = model.value_04;
                    entity.value_05 = model.value_05;

                    entity.document_role04_release_20_status_code = DocumentRole04ReleaseStatusCodeConstant.SEND_DONE.ToString();
                    entity.document_role05_release_20_by = userId;
                    entity.document_role05_release_20_send_date = DateTime.Now;
                    entity.document_role05_release_20_status_code = DocumentRole05ReleaseStatusCodeConstant.SEND.ToString();

                    repo.Update(entity);

                    //if (entity.document_role04_release_20_type_code)
                    ForceStatusD(model.save_id, entity.document_role04_release_20_instruction_rule_code, model.value_01, model.value_02, model.value_03, model.value_04, model.value_05);
                }
                uow.SaveChanges();

                return view_list;
            }
        }






        [LogAopInterceptor]
        public virtual List<SendChangeView> DocumentRole05CheckSendChange(List<SendChangeAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var post_round_repo = uow.GetRepository<PostRound>();
                var post_round_instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload) {
                    var post_round = post_round_repo.Get(model.object_id.Value);
                    post_round.document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.DRAFT_FIX.ToString();
                    post_round.document_role04_receive_remark = model.reason;
                    post_round.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.WAIT_CHANGE.ToString();
                    post_round_repo.Update(post_round);
                }
                uow.SaveChanges();

                return mapper.Map<List<SendChangeView>>(payload);
            }
        }

        [LogAopInterceptor]
        public virtual vPostRoundInstructionRuleFileView DocumentRole04CheckInstructionRuleFileAdd(vPostRoundInstructionRuleFileAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<PostRoundInstructionRuleFile>();
                var entity = new PostRoundInstructionRuleFile() {
                    post_round_instruction_rule_id = payload.post_round_instruction_rule_id,
                    file_id = payload.file_id,
                    post_round_instruction_rule_file_type_code = payload.post_round_instruction_rule_file_type_code,
                    remark = payload.remark,
                };
                repo.Add(entity);
                uow.SaveChanges();

                var view_repo = uow.GetRepository<vPostRoundInstructionRuleFile>();
                return mapper.Map<vPostRoundInstructionRuleFileView>(view_repo.Query(r => r.post_round_instruction_rule_file_id == entity.id).FirstOrDefault());
            }
        }

        [LogAopInterceptor]
        public virtual List<long> DocumentRole04CheckInstructionRuleFileDelete(List<long> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<PostRoundInstructionRuleFile>();
                foreach (var id in payload) {
                    var entity = repo.Get(id);
                    entity.is_deleted = true;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return payload;
            }
        }


        [LogAopInterceptor]
        public virtual vDocumentRole04CreateView DocumentRole04CreateAdd(vDocumentRole04CreateAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<DocumentRole04Create>();
                var entity = new DocumentRole04Create() {
                    document_role04_create_type_code = payload.document_role04_create_type_code,
                    save_id = payload.save_id,
                    value_01 = payload.value_01,
                    document_role04_create_status_code = DocumentRole04CreateStatusCodeConstant.DRAFT.ToString(),
                };
                repo.Add(entity);
                uow.SaveChanges();

                var view_repo = uow.GetRepository<vDocumentRole04Create>();
                var view = view_repo.Get(entity.id);
                return mapper.Map<vDocumentRole04CreateView>(view);
            }
        }

        [LogAopInterceptor]
        public virtual vDocumentRole04CreateFileView DocumentRole04CreateFileAdd(vDocumentRole04CreateFileAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<DocumentRole04CreateFile>();
                var entity = new DocumentRole04CreateFile() {
                    document_role04_create_id = payload.document_role04_create_id,
                    file_id = payload.file_id,
                    //document_role04_create_file_type_code = payload.document_role04_create_file_type_code,
                    remark = payload.remark,
                };
                repo.Add(entity);
                uow.SaveChanges();

                var view_repo = uow.GetRepository<vDocumentRole04CreateFile>();
                var view = view_repo.Get(entity.id);
                return mapper.Map<vDocumentRole04CreateFileView>(view);
            }
        }

        [LogAopInterceptor]
        public virtual List<long> DocumentRole04CreateFileDelete(List<long> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<DocumentRole04CreateFile>();
                foreach (var id in payload) {
                    var entity = repo.Get(id);
                    entity.is_deleted = true;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return payload;
            }
        }


        [LogAopInterceptor]
        public PageDocumentRole04CreateView DocumentRole04CreateLoad(long payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var create = uow.GetRepository<vDocumentRole04Create>();
                var view = new PageDocumentRole04CreateView();

                view.document_role04_create = mapper.Map<vDocumentRole04CreateView>(create.Get(payload));

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(view.document_role04_create.save_id);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(view.document_role04_create.save_id);

                var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                var result = view_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = view.document_role04_create.save_id.ToString(),
                          }
                    },
                    filter_queries = new List<string>() {
                        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                    }
                });
                view.instruction_rule_list = result.data.list;

                var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                var document = document_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          }
                     }
                });
                view.document_list = document.data.list;

                var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                var receipt_item = receipt_item_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = view.document_role04_create.save_id.ToString(),
                          }
                     }
                });
                view.receipt_item_list = receipt_item.data.list;

                var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                var history = history_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = view.document_role04_create.save_id.ToString(),
                          }
                     }
                });
                view.history_list = history.data.list;

                var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                var scan = scan_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          },
                          new SearchByModel() {
                              key = "file_id",
                              value = "0",
                              operation= Operation.GreaterThan,
                          }
                     }
                });
                view.document_scan_list = scan.data.list;


                return view;
            }
        }

    }
}
