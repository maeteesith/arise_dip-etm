﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Auths;
using DIP.TM.Services.Extensions;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Uows.DataAccess.Entities;
using DIP.TM.Utils;
using DIP.TM.Utils.Extensions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Web;

namespace DIP.TM.Services.Helps {

    public class HelpService {

        protected long? userId;

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public HelpService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;

            userId = HttpContext.Current?.User?.UserId();
        }

        public static string GetReferenceNumber(IUowProvider uowProvider) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<SequenceNumber>();
                string year = DateTime.Now.ToThaiYearString().Substring(2);
                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.REQUEST_REFERENCE_NUMBER.ToString() && r.code == year).FirstOrDefault();

                if (entity == null) {
                    entity = new SequenceNumber() {
                        topic = SequenceNumberConstant.REQUEST_REFERENCE_NUMBER.ToString(),
                        code = year,
                        number = 1,
                    };
                    repo.Add(entity);
                } else {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return year + entity.number.Value.ToString("000000");
            }
        }

        public static string GetRequestCheckVoucherNumber(IUowProvider uowProvider) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<SequenceNumber>();
                string year = DateTime.Now.ToThaiYearString().Substring(2);
                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.REQUEST_CHECK_VOUCHER_NUMBER.ToString() && r.code == year).FirstOrDefault();

                if (entity == null) {
                    entity = new SequenceNumber() {
                        topic = SequenceNumberConstant.REQUEST_CHECK_VOUCHER_NUMBER.ToString(),
                        code = year,
                        number = 1,
                    };
                    repo.Add(entity);
                } else {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return year + entity.number.Value.ToString("0000");
            }
        }

        public static string GetRegistrationNumber(IUowProvider uowProvider, string item_type_code) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<SequenceNumber>();
                string year = DateTime.Now.Year.ToString().Substring(2);
                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.REGISTRATION_NUMBER.ToString()).FirstOrDefault();

                if (entity == null) {
                    entity = new SequenceNumber() {
                        topic = SequenceNumberConstant.REGISTRATION_NUMBER.ToString(),
                        code = year,
                        number = 1,
                    };
                    repo.Add(entity);
                } else {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return year +
                    (item_type_code == "TRADE_MARK_AND_SERVICE_MARK" ? "01" :
                    (item_type_code == "CERTIFICATION_MARK" ? "02" : "03")) +
                    entity.number.Value.ToString("00000");
            }
        }

        public static string GetRequestNumber(IUowProvider uowProvider, string item_type_code) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<SequenceNumber>();
                string year = DateTime.Now.Year.ToString().Substring(2);
                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.REQUEST_01_ITEM_NUMBER.ToString() && r.code == year).FirstOrDefault();

                if (entity == null) {
                    entity = new SequenceNumber() {
                        topic = SequenceNumberConstant.REQUEST_01_ITEM_NUMBER.ToString(),
                        code = year,
                        number = 1,
                    };
                    repo.Add(entity);
                } else {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return year +
                    (item_type_code == "TRADE_MARK_AND_SERVICE_MARK" ? "01" :
                    (item_type_code == "CERTIFICATION_MARK" ? "02" : "03")) +
                    entity.number.Value.ToString("00000");
            }
        }

        public static string GetRequestIndex(IUowProvider uowProvider, string request_type_code) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<SequenceNumber>();
                string code = DateTime.Now.ToThaiYearString() + "_" + request_type_code;
                string year = DateTime.Now.ToThaiYearString();
                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.REQUEST_INDEX.ToString() && r.code == code).FirstOrDefault();

                if (entity == null) {
                    entity = new SequenceNumber() {
                        topic = SequenceNumberConstant.REQUEST_INDEX.ToString(),
                        code = code,
                        number = 1,
                    };
                    repo.Add(entity);
                } else {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return entity.number.Value.ToString("000") + "/" + year;
            }
        }

        public static string GetBookNumber(IUowProvider uowProvider) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<SequenceNumber>();
                string year = (DateTime.Now.Year + 543).ToString();
                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.PUBLIC_BOOK_NUMBER.ToString()).FirstOrDefault();

                if (entity == null) {
                    entity = new SequenceNumber() {
                        topic = SequenceNumberConstant.PUBLIC_BOOK_NUMBER.ToString(),
                        code = year,
                        number = 1,
                    };
                    repo.Add(entity);
                } else {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();

                return entity.number.Value.ToString() + " / " + year;
            }
        }
    }
}
