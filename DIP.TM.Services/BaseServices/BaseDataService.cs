﻿using AutoMapper;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DIP.TM.Services.BaseServices
{
    public  class BaseDataService<TC, T>
    {

        protected readonly IUowProvider uowProvider;
        protected readonly IConfiguration configuration;
        protected readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public BaseDataService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TC FetchViewModel(long id)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var commonRepo = uow.GetRepository<T>();
                var dto = commonRepo.Get(id);
                return mapper.Map<TC>(dto);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual T FetchViewData(long id)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var commonRepo = uow.GetRepository<T>();
                var dto = commonRepo.Get(id);
                return dto;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public virtual TC FetchViewModel(Expression<Func<T, bool>> filter)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var commonRepo = uow.GetRepository<T>();
                var data = commonRepo.Query(filter).FirstOrDefault();
                if (ReferenceEquals(data, null))
                    return default(TC);
                
             return mapper.Map<TC>(data);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filter"></param>
        /// <returns></returns>
        public virtual T FetchViewData(Expression<Func<T, bool>> filter)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var commonRepo = uow.GetRepository<T>();
                var data = commonRepo.Query(filter).FirstOrDefault();
                if (ReferenceEquals(data, null))
                    return default(T);

                return data;
            }
        }

        public async virtual Task<T> CreateLogAsync(T dto)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var commonRepo = uow.GetRepository<T>();
                commonRepo.Add(dto);
                await uow.SaveChangesAsync();
                return dto;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public virtual TC Create(T dto)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var commonRepo = uow.GetRepository<T>();
                commonRepo.Add(dto);
                uow.SaveChanges();
                return mapper.Map<TC>(dto);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public virtual T CreateData(T dto)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var commonRepo = uow.GetRepository<T>();
                commonRepo.Add(dto);
                uow.SaveChanges();
                return dto;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public virtual T UpdateData(T dto)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var commonRepo = uow.GetRepository<T>();
                commonRepo.Update(dto);
                uow.SaveChanges();
                return dto;
            }
        }
    }
}
