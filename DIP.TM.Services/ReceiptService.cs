﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Helps;
using DIP.TM.Services.Medias;
using DIP.TM.Services.Migrate;
using DIP.TM.Services.Requests;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Services.Receipts {
    public class ReceiptService {
        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public ReceiptService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }
        [LogAopInterceptor]
        internal virtual Receipt Add(Request01Process input, string reference_number) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();
                var entity = new Receipt() {
                    reference_number = reference_number,
                    receipt_date = input.request_date,
                    receive_date = input.request_date,
                    requester_name = input.requester_name,
                    payer_name = input.requester_name,
                    request_date = input.request_date,
                    status_code = ReceiptStatusCodeConstant.UNPAID.ToString(),
                };

                var item_list = input.Request01Item.Where(r => r.reference_number == reference_number).ToList();
                foreach (Request01Item item in item_list) {
                    entity.ReceiptItem.Add(new ReceiptItem() {
                        request_id = item.id,
                        request_type_code = RequestTypeConstant.SAVE010.ToString().Replace("SAVE", ""),
                        request_number = item.request_number,
                        source_code = input.source_code,
                        total_price = item.total_price,
                    });
                    if (item.size_over_price > 0) {
                        entity.ReceiptItem.Add(new ReceiptItem() {
                            request_id = item.id,
                            request_type_code = RequestTypeConstant.SAVE210.ToString().Replace("SAVE", ""),
                            request_number = item.request_number,
                            source_code = input.source_code,
                            total_price = item.size_over_price,
                        });
                    }
                }

                repo.Add(entity);
                uow.SaveChanges();

                return entity;
            }
        }
        [LogAopInterceptor]
        internal virtual Receipt Add(RequestCheck input) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();
                var entity = new Receipt() {
                    reference_number = input.reference_number,
                    receipt_date = DateTime.Now,
                    receive_date = DateTime.Now,
                    requester_name = input.requester_name,
                    payer_name = input.requester_name,
                    request_date = DateTime.Now,
                    status_code = ReceiptStatusCodeConstant.UNPAID.ToString(),
                };

                foreach (var item in input.RequestCheckItem.Where(r => r.value_quantity.HasValue && r.value_quantity.Value > 0)) {
                    if (item.request_check_item_code == "320") {
                        foreach (var request in input.RequestCheckRequestNumber.Where(r => r.is_2_2.HasValue && r.is_2_2.Value)) {
                            entity.ReceiptItem.Add(new ReceiptItem() {
                                request_id = item.id,
                                request_type_code = item.request_check_item_code,
                                request_number = request.request_number,
                                source_code = RequestSourceCodeConstant.CENTRAL.ToString(),
                                total_price = input.is_wave_fee.Value ? 0 : item.value_total_price / item.value_quantity,
                            });
                        }
                    } else if (item.request_check_item_code == "330") {
                        foreach (var request in input.RequestCheckRequestNumber.Where(r => r.number_2_3.HasValue && r.number_2_3.Value > 0)) {
                            entity.ReceiptItem.Add(new ReceiptItem() {
                                request_id = item.id,
                                request_type_code = item.request_check_item_code,
                                request_number = request.request_number,
                                source_code = RequestSourceCodeConstant.CENTRAL.ToString(),
                                total_price = input.is_wave_fee.Value ? 0 : item.value_total_price / item.value_quantity * request.number_2_3,
                            });
                        }
                    } else if (item.request_check_item_code == "350") {
                        foreach (var request in input.RequestCheckRequestNumber.Where(r => r.is_2_5.HasValue && r.is_2_5.Value)) {
                            entity.ReceiptItem.Add(new ReceiptItem() {
                                request_id = item.id,
                                request_type_code = item.request_check_item_code,
                                request_number = request.request_number,
                                source_code = RequestSourceCodeConstant.CENTRAL.ToString(),
                                total_price = input.is_wave_fee.Value ? 0 : item.value_total_price / item.value_quantity,
                            });
                        }
                    } else if (item.request_check_item_code == "360") {
                        foreach (var request in input.RequestCheckRequestNumber.Where(r => r.is_2_6.HasValue && r.is_2_6.Value)) {
                            entity.ReceiptItem.Add(new ReceiptItem() {
                                request_id = item.id,
                                request_type_code = item.request_check_item_code,
                                request_number = request.request_number,
                                source_code = RequestSourceCodeConstant.CENTRAL.ToString(),
                                total_price = input.is_wave_fee.Value ? 0 : item.value_total_price / item.value_quantity,
                            });
                        }
                    } else if (item.request_check_item_code == "380") {
                        foreach (var request in input.RequestCheckRequestNumber.Where(r => r.is_2_7.HasValue && r.is_2_7.Value)) {
                            entity.ReceiptItem.Add(new ReceiptItem() {
                                request_id = item.id,
                                request_type_code = item.request_check_item_code,
                                request_number = request.request_number,
                                source_code = RequestSourceCodeConstant.CENTRAL.ToString(),
                                total_price = input.is_wave_fee.Value ? 0 : item.value_total_price / item.value_quantity,
                            });
                        }
                    } else {
                        entity.ReceiptItem.Add(new ReceiptItem() {
                            request_id = item.id,
                            request_type_code = item.request_check_item_code,
                            //request_number = item.
                            source_code = RequestSourceCodeConstant.CENTRAL.ToString(),
                            total_price = input.is_wave_fee.Value ? 0 : item.value_total_price,
                        });
                    }

                }

                repo.Add(entity);
                uow.SaveChanges();

                if (input.is_wave_fee.Value) {
                    Paid(entity.id);
                }

                return entity;
            }
        }
        [LogAopInterceptor]
        internal virtual List<Receipt> Add(RequestOtherProcess input) {
            List<Receipt> receipt_list = new List<Receipt>();
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();
                foreach (RequestOtherRequest01Item request in input.RequestOtherRequest01Item) {
                    var entity = new Receipt() {
                        reference_number = request.reference_number,
                        requester_name = input.requester_name,
                        receipt_date = input.request_date,
                        receive_date = input.request_date,
                        payer_name = input.requester_name,
                        request_date = input.request_date,
                        status_code = ReceiptStatusCodeConstant.UNPAID.ToString(),
                    };

                    foreach (var item in input.RequestOtherItem) {
                        if (item.RequestOtherItemSub.Where(r => r.request_number == request.request_number).Count() > 0) {
                            entity.ReceiptItem.Add(new ReceiptItem() {
                                request_id = item.id,
                                request_type_code = item.request_type_code.Replace("SAVE0", "").Replace("SAVE", ""),
                                request_number = request.request_number,
                                source_code = input.source_code,
                                total_price = item.RequestOtherItemSub.Where(r => r.request_number == request.request_number).Sum(r => r.total_price),
                                book_index = string.IsNullOrEmpty(request.book_index) ? item.book_index : request.book_index,
                                page_index = string.IsNullOrEmpty(request.page_index) ? item.page_index : request.page_index,
                            });
                        }
                    }

                    repo.Add(entity);
                    receipt_list.Add(entity);
                }
                uow.SaveChanges();
            }
            return receipt_list;
        }

        [LogAopInterceptor]
        internal virtual Receipt Add(RequestEFormView input, string reference_number) {
            List<Receipt> receipt_list = new List<Receipt>();
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();
                var entity = new Receipt() {
                    reference_number = reference_number,
                    requester_name = input.requester_name,
                    receipt_date = input.request_date,
                    receive_date = input.request_date,
                    payer_name = input.requester_name,
                    request_date = input.request_date,
                    status_code = ReceiptStatusCodeConstant.UNPAID.ToString(),
                };

                foreach (var item in input.request01_item_list) {
                    entity.ReceiptItem.Add(new ReceiptItem() {
                        request_id = item.id,
                        request_type_code = RequestTypeConstant.SAVE010.ToString().Replace("SAVE", ""),
                        request_number = item.request_number,
                        source_code = input.source_code,
                        total_price = item.total_price,
                    });
                    if (item.size_over_price > 0) {
                        entity.ReceiptItem.Add(new ReceiptItem() {
                            request_id = item.id,
                            request_type_code = RequestTypeConstant.SAVE210.ToString().Replace("SAVE", ""),
                            request_number = item.request_number,
                            source_code = input.source_code,
                            total_price = item.size_over_price,
                        });
                    }
                }

                repo.Add(entity);
                uow.SaveChanges();
                return entity;
            }
        }

        [LogAopInterceptor]
        internal virtual Receipt Add(PostRound input, string name, decimal _price) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<vSave010>();
                var repo = uow.GetRepository<Receipt>();

                var receipt_list = repo.Query(r => r.reference_number == input.reference_number);
                if (receipt_list.Count() > 0) return receipt_list.FirstOrDefault();

                var entity = new Receipt() {
                    reference_number = input.reference_number,
                    receipt_date = DateTime.Now,
                    receive_date = DateTime.Now,
                    requester_name = name,
                    payer_name = name,
                    request_date = DateTime.Now,
                    status_code = ReceiptStatusCodeConstant.UNPAID.ToString(),
                };

                var save = save_repo.Get(input.object_id.Value);

                entity.ReceiptItem.Add(new ReceiptItem() {
                    name = name,
                    //request_id = item.id,
                    post_round_id = input.id,
                    request_type_code = RequestTypeConstant.POST_ROUND_290.ToString().Replace("POST_ROUND_", ""),
                    request_number = save.request_number,
                    source_code = RequestSourceCodeConstant.CENTRAL.ToString(),
                    total_price = _price,
                });

                repo.Add(entity);
                uow.SaveChanges();

                return entity;
            }
        }
        [LogAopInterceptor]
        public virtual ReceiptView Save(ReceiptAddModel payload, bool is_change_page_book_index = true) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();

                if (string.IsNullOrEmpty(payload.book_index)) payload.book_index = "1";
                if (string.IsNullOrEmpty(payload.page_index)) payload.page_index = "1";

                if (is_change_page_book_index && (!payload.is_manual.HasValue || !payload.is_manual.Value)) {
                    foreach (ReceiptItemAddModel item in payload.item_list) {
                        item.book_index = payload.book_index;
                        item.page_index = payload.page_index;

                        payload.page_index = (Convert.ToInt32(payload.page_index) + 1).ToString();
                        if (Convert.ToInt32(payload.page_index) >= 100) {
                            payload.page_index = "1";
                            payload.book_index = (Convert.ToInt32(payload.book_index) + 1).ToString();
                        }

                        item.page_index = Convert.ToInt32(item.page_index).ToString("000");
                    }
                }

                var entity = mapper.Map<Receipt>(payload);
                repo.Update(entity);
                uow.SaveChanges();

                var view = Paid(entity.id);
                view.book_index = payload.book_index;
                view.page_index = payload.page_index;
                return view;
            }
        }
        [LogAopInterceptor]
        public virtual ReceiptView Load(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();
                var entity = repo.Get(id);
                return mapper.Map<ReceiptView>(entity);
            }
        }
        [LogAopInterceptor]
        public virtual ReceiptView Load(ReceiptAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();
                var entity = repo.Query(r =>
                (string.IsNullOrEmpty(payload.reference_number) || r.reference_number == payload.reference_number) &&
                (string.IsNullOrEmpty(payload.status_code) || r.status_code == payload.status_code)
                ).FirstOrDefault();
                return mapper.Map<ReceiptView>(entity);
            }
        }
        [LogAopInterceptor]
        public virtual ReceiptView Paid(long id, bool is_only_free = false) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();
                var entity = repo.Get(id);

                var receipt_item_list = entity.ReceiptItem;
                if (is_only_free) {
                    receipt_item_list = receipt_item_list.Where(r => r.total_price == 0).ToList();

                    if (entity.ReceiptItem.Where(r => r.total_price > 0).Count() == 0) {
                        entity.status_code = ReceiptStatusCodeConstant.PAID.ToString();
                    }
                } else {
                    entity.status_code = ReceiptStatusCodeConstant.PAID.ToString();
                }

                foreach (ReceiptItem item in receipt_item_list) {
                    if (string.IsNullOrEmpty(item.receipt_number)) {
                        item.receipt_number = GetReceiptNumber();
                    }

                    if (item.request_type_code == RequestTypeConstant.SAVE010.ToString().Replace("SAVE", "")) {

                        var save_service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                        var save = save_service.LoadFromRequest(item.request_id.Value);

                        if (save == null) {
                            if (item.source_code == RequestSourceCodeConstant.EFORM.ToString()) {
                                var migration = new MigrateEFormService<eForm_Save010, Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                                var request_item_repo = uow.GetRepository<Request01Item>();
                                var eform_repo = uow.GetRepository<eForm_Save010>();

                                var request_item = request_item_repo.Get(item.request_id.Value);
                                var eform = eform_repo.Get(request_item.eform_id.Value);
                                var view = migration.DownloadFromEForm(eform.eform_number, new SaveModel() {
                                    request_id = item.request_id.Value,
                                    request_number = item.request_number,
                                    request_index = null,
                                });

                                if (eform.img_file_2d_id.HasValue) {
                                    using (var _uow = uowProvider.CreateUnitOfWork()) {
                                        var file_guest_repo = _uow.GetRepository<FileGuest>();
                                        var file_repo = _uow.GetRepository<File>();
                                        var rdc_repo = _uow.GetRepository<RequestDocumentCollect>();

                                        var file_guest = file_guest_repo.Get(eform.img_file_2d_id.Value);
                                        var file = new File() {
                                            file_name = file_guest.file_name,
                                            physical_path = file_guest.physical_path,
                                            display_path = file_guest.display_path,
                                            file_size = file_guest.file_size,
                                        };
                                        file_repo.Add(file);
                                        _uow.SaveChanges();

                                        rdc_repo.Add(new RequestDocumentCollect() {
                                            save_id = view.id,
                                            receipt_item_id = item.id,
                                            request_document_collect_type_code = RequestDocumentCollectTypeCodeConstant.TRADEMARK_2D.ToString(),
                                            file_id = file.id,
                                            request_document_collect_status_code = RequestDocumentCollectStatusCodeConstant.ADD.ToString(),
                                        });
                                        _uow.SaveChanges();
                                    }
                                }

                                continue;
                            } else {
                                var request_service = new RequestProcessService(configuration, uowProvider, mapper);
                                var request_item = request_service.Request01ItemLoad(item.request_id.Value);
                                var request = request_service.Request01Load(request_item.request01_id);

                                save = save_service.Add(new Save010AddModel() {
                                    request_id = item.request_id.Value,
                                    request_number = item.request_number,
                                    total_price = item.total_price,
                                    request_source_code = item.source_code,
                                    commercial_affairs_province_code = request.commercial_affairs_province_code,
                                    save_status_code = SaveStatusCodeConstant.DRAFT.ToString(),

                                    request_date = entity.request_date.Value,
                                    trademark_expired_date = entity.request_date.Value.AddYears(10),
                                    trademark_expired_start_date = entity.request_date.Value.AddYears(10).AddMonths(-3),
                                    trademark_expired_end_date = entity.request_date.Value.AddYears(10).AddMonths(6),

                                    request_item_type_code = request_item.item_type_code,
                                    sound_description = request_item.sound_description,

                                    sound_file_id = request_item.sound_file_id,
                                });
                            }
                        }
                    } else if (item.request_type_code == RequestTypeConstant.SAVE020.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var save_service = new SaveProcessService<Save020AddModel, Save020, Save020ProcessView>(configuration, uowProvider, mapper);
                        var save = save_service.LoadFromRequest(item.request_id.Value);

                        if (save == null) {
                            if (item.source_code == RequestSourceCodeConstant.EFORM.ToString()) {
                                var migration = new MigrateEFormService<eForm_Save020, Save020AddModel, Save020, Save020ProcessView>(configuration, uowProvider, mapper);
                                var request_item_repo = uow.GetRepository<RequestOtherItemSub>();
                                var eform_repo = uow.GetRepository<eForm_Save020>();

                                var request_item = request_item_repo.Query(r => r.request_other_item_id == item.request_id.Value).FirstOrDefault();
                                var eform = eform_repo.Get(request_item.eform_id.Value);
                                var view = migration.DownloadFromEForm(eform.eform_number, new SaveModel() {
                                    request_id = item.request_id.Value,
                                    request_number = item.request_number,
                                    request_index = null,
                                });

                                continue;
                            } else {
                                var service = new SaveProcessService<Save020AddModel, Save020, Save020ProcessView>(configuration, uowProvider, mapper);
                                service.Add(item);
                            }
                        }
                    } else if (item.request_type_code == RequestTypeConstant.SAVE021.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var service = new SaveProcessService<Save021AddModel, Save021, Save021ProcessView>(configuration, uowProvider, mapper);
                        service.Add(item);
                    } else if (item.request_type_code == RequestTypeConstant.SAVE030.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var service = new SaveProcessService<Save030AddModel, Save030, Save030ProcessView>(configuration, uowProvider, mapper);
                        service.Add(item);
                    } else if (item.request_type_code == RequestTypeConstant.SAVE040.ToString().Replace("SAVE0", "").Replace("SAVE", "") ||
                        item.request_type_code == RequestTypeConstant.SAVE041.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var save_service = new SaveProcessService<Save040AddModel, Save040, Save040ProcessView>(configuration, uowProvider, mapper);
                        var save = save_service.LoadFromRequest(item.request_id.Value);

                        if (save == null) {
                            if (item.source_code == RequestSourceCodeConstant.EFORM.ToString()) {
                                var migration = new MigrateEFormService<eForm_Save040, Save040AddModel, Save040, Save040ProcessView>(configuration, uowProvider, mapper);
                                var request_item_repo = uow.GetRepository<RequestOtherItemSub>();
                                var eform_repo = uow.GetRepository<eForm_Save040>();

                                var request_item = request_item_repo.Query(r => r.request_other_item_id == item.request_id.Value).FirstOrDefault();
                                var eform = eform_repo.Get(request_item.eform_id.Value);
                                var view = migration.DownloadFromEForm(eform.eform_number, new SaveModel() {
                                    request_id = item.request_id.Value,
                                    request_number = item.request_number,
                                    request_index = null,
                                });

                                continue;
                            } else {
                                var service = new SaveProcessService<Save040AddModel, Save040, Save040ProcessView>(configuration, uowProvider, mapper);
                                service.Add(item);
                            }
                        }
                    } else if (item.request_type_code == RequestTypeConstant.SAVE050.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var service = new SaveProcessService<Save050AddModel, Save050, Save050ProcessView>(configuration, uowProvider, mapper);
                        service.Add(item);
                    } else if (item.request_type_code == RequestTypeConstant.SAVE060.ToString().Replace("SAVE0", "").Replace("SAVE", "") ||
                        item.request_type_code == RequestTypeConstant.SAVE061.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var service = new SaveProcessService<Save060AddModel, Save060, Save060ProcessView>(configuration, uowProvider, mapper);
                        service.Add(item);

                        var view = service.Add(item);
                        if (item.request_type_code == RequestTypeConstant.SAVE060.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            view.save060_period_request_code = "TYPE1";
                        } else if (item.request_type_code == RequestTypeConstant.SAVE061.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            view.save060_period_request_code = "TYPE2";
                        }
                        service.Save(mapper.Map<Save060AddModel>(view));

                    } else if (item.request_type_code == RequestTypeConstant.SAVE070.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var service = new SaveProcessService<Save070AddModel, Save070, Save070ProcessView>(configuration, uowProvider, mapper);
                        service.Add(item);
                    } else if (item.request_type_code == RequestTypeConstant.SAVE080.ToString().Replace("SAVE0", "").Replace("SAVE", "") ||
                        item.request_type_code == RequestTypeConstant.SAVE081.ToString().Replace("SAVE0", "").Replace("SAVE", "") ||
                        item.request_type_code == RequestTypeConstant.SAVE082.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var service = new SaveProcessService<Save080AddModel, Save080, Save080ProcessView>(configuration, uowProvider, mapper);

                        var view = service.Add(item);
                        if (item.request_type_code == RequestTypeConstant.SAVE080.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            view.save080_revoke_type_code = "TYPE2";
                        } else if (item.request_type_code == RequestTypeConstant.SAVE081.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            view.save080_revoke_type_code = "TYPE3";
                        } else if (item.request_type_code == RequestTypeConstant.SAVE082.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            view.save080_revoke_type_code = "TYPE1";
                        }
                        var model = mapper.Map<Save080AddModel>(view);
                        service.Save(model);

                    } else if (item.request_type_code == RequestTypeConstant.SAVE140.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                        var service = new SaveProcessService<Save140AddModel, Save140, Save140ProcessView>(configuration, uowProvider, mapper);
                        service.Add(item);
                    } else if (item.request_type_code == RequestTypeConstant.POST_ROUND_290.ToString().Replace("POST_ROUND_", "")) {
                        var post_round_repo = uow.GetRepository<PostRound>();
                        var post_round = post_round_repo.Get(item.post_round_id.Value);

                        var save_repo = uow.GetRepository<Save010>();
                        var save = save_repo.Get(post_round.object_id.Value);

                        var payment_repo = uow.GetRepository<Save010PublicPayment>();

                        save.public_role04_payment_date = DateTime.Now;
                        if (string.IsNullOrEmpty(save.public_role04_receive_status_code)) {
                            save.public_role04_status_code = PublicRole04StatusCodeConstant.WAIT.ToString();

                            var payment = payment_repo.Query(r => r.save_id == save.id).FirstOrDefault();
                            if (payment != null) {
                                payment.public_role02_document_payment_status_code = PublicRole02DocumentPaymentStatusCodeConstant.DONE.ToString();
                                payment_repo.Update(payment);
                            }
                        } else {
                            save.public_role04_receive_status_code = PublicRole04ReceiveStatusCodeConstant.NOT_YET.ToString();
                        }

                        save_repo.Update(save);
                    } else if (item.request_type_code == RequestTypeConstant.REQUEST_CHECK_310.ToString().Replace("REQUEST_CHECK_", "")) {

                    } else {
                        var service = new SaveProcessService<SaveOtherAddModel, SaveOther, SaveOtherProcessView>(configuration, uowProvider, mapper);
                        var is_added = false;
                        if (item.request_type_code == RequestTypeConstant.SAVE120.ToString().Replace("SAVE", "")) {
                            var save = service.LoadFromRequest(item.request_id.Value);

                            if (save == null) {
                                if (item.source_code == RequestSourceCodeConstant.EFORM.ToString()) {
                                    var migration = new MigrateEFormService<eForm_Save120, SaveOtherAddModel, SaveOther, SaveOtherProcessView>(configuration, uowProvider, mapper);
                                    var request_item_repo = uow.GetRepository<RequestOtherItemSub>();
                                    var eform_repo = uow.GetRepository<eForm_Save120>();

                                    var request_item = request_item_repo.Query(r => r.request_other_item_id == item.request_id.Value).FirstOrDefault();
                                    //request_item.request_type_code = item.request_type_code;
                                    var eform = eform_repo.Get(request_item.eform_id.Value);
                                    var view = migration.DownloadFromEForm(eform.eform_number, new SaveModel() {
                                        request_id = item.request_id.Value,
                                        request_number = item.request_number,
                                        request_index = null,
                                    });

                                    is_added = true;
                                }
                            }
                        }

                        if (!is_added) {
                            var save_other = service.Add(item);
                            save_other.request_type_code = item.request_type_code;
                            service.Save(mapper.Map<SaveOtherAddModel>(save_other));
                        }
                    }
                }

                repo.Update(entity);
                uow.SaveChanges();
                return mapper.Map<ReceiptView>(entity);
            }
        }
        [LogAopInterceptor]
        public virtual FileView Print(ReceiptAddModel payload) {
            FileService file = new FileService(configuration, uowProvider, mapper);
            return file.Add(new FileView() {
                file_name = DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt",
                physical_path = "Contents/Media/" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".txt",
                file_size = (long)Convert.ToDouble(DateTime.Now.ToString("yyyyMMddHHmmss")),
            });
        }

        [LogAopInterceptor]
        public virtual ReceiptView Delete(BaseModel payload) {
            if (payload.id.HasValue) {
                Delete(payload.id.Value);
            }

            if (payload.ids != null) {
                foreach (long id in payload.ids) {
                    Delete(id, payload.cancel_reason);
                }
            }
            return mapper.Map<ReceiptView>(null);
        }

        [LogAopInterceptor]
        public virtual ReceiptView Delete(long id, string cancel_reason = null) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Receipt>();
                var entity = repo.Get(id);

                if (entity.status_code == ReceiptStatusCodeConstant.PAID.ToString()) {
                    foreach (var item in entity.ReceiptItem.Where(r => r.total_price > 0)) {
                        item.receipt_number = null;

                        if (item.request_type_code == RequestTypeConstant.SAVE010.ToString().Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save010>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE020.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save020>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save020AddModel, Save020, Save020ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE021.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save021>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save021AddModel, Save021, Save021ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE030.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save030>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save030AddModel, Save030, Save030ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE040.ToString().Replace("SAVE0", "").Replace("SAVE", "") ||
                            item.request_type_code == RequestTypeConstant.SAVE041.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save040>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save040AddModel, Save040, Save040ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE050.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save050>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save050AddModel, Save050, Save050ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE060.ToString().Replace("SAVE0", "").Replace("SAVE", "") ||
                            item.request_type_code == RequestTypeConstant.SAVE061.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save060>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save060AddModel, Save060, Save060ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE070.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save070>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save070AddModel, Save070, Save070ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE080.ToString().Replace("SAVE0", "").Replace("SAVE", "") ||
                            item.request_type_code == RequestTypeConstant.SAVE081.ToString().Replace("SAVE0", "").Replace("SAVE", "") ||
                            item.request_type_code == RequestTypeConstant.SAVE082.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save080>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save080AddModel, Save080, Save080ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.SAVE140.ToString().Replace("SAVE0", "").Replace("SAVE", "")) {
                            var save_repo = uow.GetRepository<Save140>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<Save140AddModel, Save140, Save140ProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        } else if (item.request_type_code == RequestTypeConstant.REQUEST_CHECK_310.ToString().Replace("REQUEST_CHECK_", "")) {

                        } else {
                            var save_repo = uow.GetRepository<SaveOther>();
                            var save = save_repo.Query(r => r.request_id == item.request_id && r.request_number == item.request_number).FirstOrDefault();

                            if (save != null) {
                                var save_service = new SaveProcessService<SaveOtherAddModel, SaveOther, SaveOtherProcessView>(configuration, uowProvider, mapper);
                                save_service.Delete(save.id, cancel_reason);
                            }
                        }
                    }
                }

                entity.status_code = ReceiptStatusCodeConstant.DELETED.ToString();

                repo.Update(entity);

                uow.SaveChanges();

                return mapper.Map<ReceiptView>(null);
            }
        }

        string GetReceiptNumber() {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<SequenceNumber>();
                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.RECEIPT_NUMBER.ToString()).FirstOrDefault();

                if (entity == null) {
                    entity = new SequenceNumber() {
                        topic = SequenceNumberConstant.RECEIPT_NUMBER.ToString(),
                        number = 1,
                    };
                    repo.Add(entity);
                } else {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return "TM" + entity.number.Value.ToString("000000");
            }
        }
        [LogAopInterceptor]
        public virtual List<vReceiptItemView> ItemList(ReceiptAddModel payload) {

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vReceiptItem>();
                List<vReceiptItem> item_list = repo.Query(r => r.reference_number == payload.reference_number,
                    r => r.OrderBy(s => s.reference_number)).ToList();
                var view_list = mapper.Map<List<vReceiptItemView>>(item_list);

                if (view_list.Count() > 0) {
                    var paid_repo = uow.GetRepository<ReceiptPaid>();
                    var paid_list = paid_repo.QueryActive(r => r.receipt_id == view_list[0].id);

                    while (paid_list.Count() > 0) {
                        paid_list = paid_list.Where(r => r.total_price > 0).ToList();
                        var paid = paid_list.FirstOrDefault();
                        if (paid == null) break;

                        var view = view_list.Where(r => r.paid_list == null || r.total_price > r.paid_list.Sum(s => s.total_price)).FirstOrDefault();
                        if (view == null) break;
                        if (view.paid_list == null) view.paid_list = new List<ReceiptPaidView>();

                        var remain = view.total_price - view.paid_list.Sum(s => s.total_price);
                        var fill = Math.Min(remain.Value, paid.total_price.Value);

                        paid.total_price -= fill;

                        var receipt_paid_view = mapper.Map<ReceiptPaidView>(paid);
                        receipt_paid_view.total_price = fill;

                        view.paid_list.Add(receipt_paid_view);
                    }
                    //foreach (vReceiptItemView paid in view_list) {

                    //paid.paid_list = mapper.Map<List<ReceiptPaidView>>(paid_list);
                    //}
                }

                return view_list;
            }
        }
    }
}
