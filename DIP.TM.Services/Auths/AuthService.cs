﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Configs;
using DIP.TM.Models.ExternalModels;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.BaseServices;
using DIP.TM.Services.Extensions;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils.Logging;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net;

namespace DIP.TM.Services.Auths
{
    public class AuthService : BaseDataService<UMSsoUserModel, UM_SSO_User>
    {
        private AppConfigModel config;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public AuthService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
            : base(configuration, uowProvider, mapper)
        {
            config = configuration.ToConfig();
        }

        [LogAopInterceptor]
        public virtual AuthSsoResponseModel VerifyAuthCode(string auth_code)
        {

            // verify sso
            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var client = new RestClient(config.SsoUrl);
            client.RemoteCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            var request = new RestRequest(Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(new { app_id = config.SsoAppId, app_secret = config.SsoAppSecret, auth_code });
            IRestResponse response = client.Execute(request);
            //if (stopWatch.Elapsed.TotalSeconds >= 5)
            //{
            LineNotifyHelper.SendWait(configuration, "Verify sso : " + stopWatch.Elapsed.TotalSeconds.ToString() + " seconds");
            //}


            stopWatch.Stop();

            if (response.ResponseStatus == ResponseStatus.Completed)
            {
                switch (response.StatusCode)
                {
                    case HttpStatusCode.OK:
                        var authSSo = JsonConvert.DeserializeObject<AuthSsoResponseModel>(response.Content);
                        var result = AddOrUpdate(authSSo);
                        var bearerToken = new UserService(this.configuration, this.uowProvider, this.mapper).GenerateToken(result.user_id ?? 0, DateTime.Now.AddHours(2));
                        authSSo.bearer_token = bearerToken;
                        return authSSo;
                    default:
                        return null;
                }

            }
            else
            {
                LineNotifyHelper.SendWait(configuration, "Verify sso error : " + auth_code + "  >>" + response.ErrorMessage);
                // TODO Handle exception/Error message;
                return null;
            }
        }

        public UM_SSO_User AddOrUpdate(AuthSsoResponseModel request)
        {
            var dtoMap = mapper.Map<UM_SSO_User>(request);
            var dto = this.FetchViewData(x => x.user_sso_id == request.info.user_sso_id);
            if (ReferenceEquals(dto, null))
            {
                var user = mapper.Map<UM_User>(request);
                user.UM_SSO_User.Add(dtoMap);
                var userWithSSO = new BaseDataService<UM_User, UM_User>(this.configuration, this.uowProvider, this.mapper).CreateData(user);
                return userWithSSO.UM_SSO_User.FirstOrDefault();
            }
            else
            {
                return base.UpdateData(dto);
            }
        }
    }
}
