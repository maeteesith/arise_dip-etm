﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models;
using DIP.TM.Models.Configs;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.BaseServices;
using DIP.TM.Services.Extensions;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Security.Claims;
using System.Text;
using System.Web;

namespace DIP.TM.Services.Auths {
    /// <summary>
    /// 
    /// </summary>
    public class UserService : BaseDataService<UserSessionView, UM_User> {
        private AppConfigModel config;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public UserService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) 
            : base(configuration, uowProvider, mapper)
        {
            config = configuration.ToConfig();
        }

        [LogAopInterceptor]
        public virtual List<UserRoleView> FetchUserRoles(long userId) {
            var user = base.FetchViewModel(userId);
            return user.user_role_list;
        }


        [LogAopInterceptor]
        public virtual List<vUM_UserView> List(BasePageModelPayload<vUM_UserAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vUM_User>();
                var list = repo.Query(r =>
                (!payload.filter.department_id.HasValue || payload.filter.department_id == 0 || payload.filter.department_id == r.department_id) &&
                (string.IsNullOrEmpty(payload.filter.name) || r.name.IndexOf(payload.filter.name) >= 0),
                r => r.OrderBy(s => s.name));
                return mapper.Map<List<vUM_UserView>>(list);
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponsePageView<List<vUM_UserView>> UserViewList(PageRequest payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vUM_User>();
                var list = repo.Filters(x => x.is_deleted == false);
                var filterList = list.FilterDynamic(payload);
                payload.item_total = filterList.Count();
                var resultPage = filterList.Page(payload.page_index, payload.item_per_page);
                return new BaseResponsePageView<List<vUM_UserView>>() {
                    data = new BaseResponsePageDataModel<List<vUM_UserView>>() {
                        list = mapper.Map<List<vUM_UserView>>(resultPage),
                        paging = payload
                    }
                };
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="payload"></param>
        /// <returns></returns>
        [LogAopInterceptor]
        public virtual UserSessionView Login(LoginRequestModel payload) {
            var user = new UM_User() { id = 1, email = "demo@demo.conm", name = "demo", username = "demo" };

            if (user == null)
                return null;

            var expiredDate = DateTime.UtcNow.AddMonths(12);


            var token = GenerateToken(user.id, expiredDate);
            return new UserSessionView() { token = token, session_expired_datetime = expiredDate };
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <param name="expiredDate"></param>
        /// <returns></returns>
        [LogAopInterceptor]
        public virtual string GenerateToken(long userId, DateTime expiredDate) {
            var SecretKey = config.SecretKey;
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(SecretKey);
            var tokenDescriptor = new SecurityTokenDescriptor {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(JwtRegisteredClaimNames.NameId, userId.ToString())
                }),
                Expires = expiredDate,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [LogAopInterceptor]
        public virtual vUserSessionView GetUserInfo() {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vUserSession>();
                // TODO Validate user id
                var entity = repo.Query(r => r.id == HttpContext.Current.User.UserId()).ToList();
                // TODO validate info
                vUserSessionView view = entity.Any() ? mapper.Map<vUserSessionView>(entity[0]) : new vUserSessionView();

                view.UM_Role = new List<UM_RoleView>();
                view.UM_Role.AddRange(entity.Where(r => r.role_id.HasValue).GroupBy(r => new { r.role_id, r.role_name }).Select(r => new UM_RoleView() { id = r.Key.role_id.Value, name = r.Key.role_name }));

                view.UM_PageMenu = new List<UM_PageMenuView>();
                view.UM_PageMenu.AddRange(entity.Where(r => r.page_menu_id.HasValue).GroupBy(r => new { r.page_menu_id, r.page_menu_name }).Select(r => new UM_PageMenuView() { id = r.Key.page_menu_id.Value, name = r.Key.page_menu_name }));

                foreach (UM_PageMenuView page_menu in view.UM_PageMenu) {
                    page_menu.UM_PageMenuSub = new List<UM_PageMenuSubView>();
                    page_menu.UM_PageMenuSub.AddRange(entity.Where(r => r.page_menu_id == page_menu.id).GroupBy(r => new { r.page_menu_sub_id, r.page_menu_sub_name }).Select(r => new UM_PageMenuSubView() { id = r.Key.page_menu_sub_id.Value, name = r.Key.page_menu_sub_name }));

                    foreach (UM_PageMenuSubView page_menu_sub in page_menu.UM_PageMenuSub) {
                        page_menu_sub.UM_Page = new List<UM_PageView>();
                        page_menu_sub.UM_Page.AddRange(entity.Where(r => r.page_menu_sub_id == page_menu_sub.id).GroupBy(r => new { r.page_code, r.page_name, r.page_link, r.is_show_menu }).Select(r => new UM_PageView() { code = r.Key.page_code, name = r.Key.page_name, page_link = r.Key.page_link, is_show_menu = r.Key.is_show_menu.Value }));

                        foreach (UM_PageView page in page_menu_sub.UM_Page) {
                            page.UM_Policy = new List<UM_PolicyView>();
                            page.UM_Policy.AddRange(entity.Where(r => r.page_code == page.code).GroupBy(r => new { r.policy_code, r.policy_name }).Select(r => new UM_PolicyView() { code = r.Key.policy_code, name = r.Key.policy_name }));
                        }
                    }
                }

                return view;
            }
        }
    }
}
