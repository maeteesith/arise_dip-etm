using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using DIP.TM.Models.Payloads;
using DIP.TM.Services.Interfaces;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Auths;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Helps;
using DIP.TM.Utils.Extensions;
using Newtonsoft.Json;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Web;

namespace DIP.TM.Services.ReferenceMaster {
    /// <summary>
    /// 
    /// </summary>
    public class RequestItemSubType1Service : IRequestItemSubType1Service
    {
        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestItemSubType1Service(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }

        public List<ReferenceMasterView> CheckTag(ReferenceMasterModel payload) {
            //public List<string> CheckTag(ReferenceMasterModel payload) {
            var view_list = new List<ReferenceMasterView>();
            //var view_list = new List<string>();
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var masterRepo = uow.GetRepository<RM_RequestItemSubType1>();

                DbContext context = uow.GetContext<RM_RequestItemSubType1>();
                //context.Database.
                //var results = masterRepo.GetAllActive(x => x.OrderBy(y => y.id));
                //return mapper.Map<List<RM_RequestItemSubType1>>(results);

                using (var command = context.Database.GetDbConnection().CreateCommand()) {
                    command.CommandText = "SELECT top 5 t.id, t.code, t.name, dbo.ufn_levenshtein(name,'@name') value_1 FROM RM_RequestItemSubType1 t order by dbo.ufn_levenshtein(name,'@name') ";
                    //command.CommandText = "SELECT t.id, t.code, t.name, dbo.ufn_levenshtein(name,'@name') FROM RM_RequestItemSubType1 t where dbo.ufn_levenshtein(name,'@name') < 15";
                    var parameter = command.CreateParameter();
                    parameter.ParameterName = "@name";
                    parameter.Value = payload.name;
                    command.Parameters.Add(parameter);
                    context.Database.OpenConnection();
                    using (var result = command.ExecuteReader()) {
                        while (result.Read()) {
                            view_list.Add(new ReferenceMasterView() {
                                id = result.GetInt64(0),
                                code = result.GetString(1),
                                name = result.GetString(2),
                                value_1 = result.GetInt32(3).ToString(),
                            });
                            //view_list.Add(result.GetInt64(0).ToString() + " " + result.GetString(2) + " " + result.GetInt32(3).ToString());
                        }
                    }
                }
            }
            return view_list;
        }

        [LogAopInterceptor]
        public virtual BaseResponsePageView<List<vProductCatagoryView>> ProductCatagory01ItemList(PageRequest payload)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<eForm_Save010Product>();
                var itemList = repo.Filters(x => x.is_deleted == false);
                var filterList = itemList.FilterDynamic(payload);
                payload.item_total = filterList.Count();
                var resultPage = filterList.Page(payload.page_index, payload.item_per_page);

                return new BaseResponsePageView<List<vProductCatagoryView>>()
                {
                    data = new BaseResponsePageDataModel<List<vProductCatagoryView>>()
                    {
                        list = mapper.Map<List<vProductCatagoryView>>(resultPage),
                        paging = payload
                    }
                };
            }
        }
    }
}
