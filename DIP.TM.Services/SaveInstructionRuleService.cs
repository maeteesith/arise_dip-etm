﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace DIP.TM.Services.SaveInstructionRules
{
    public class SaveInstructionRuleService
    {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public SaveInstructionRuleService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<SaveInstructionRuleView> LoadInstructionRuleByInstructionNumber(string instruction_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<SaveInstructionRule>();
                var entity = repo.Query(r => r.instruction_number == instruction_number).FirstOrDefault();
                var view = mapper.Map<SaveInstructionRuleView>(entity);

                return new BaseResponseView<SaveInstructionRuleView>() { data = view };
            }
        }
    }
}
