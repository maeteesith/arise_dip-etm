﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Services.RequestAgencys
{
    public class RequestAgencyService {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestAgencyService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }
        [LogAopInterceptor]
        public virtual List<vRequestAgencyView> ListView(RequestAgencyModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vRequestAgency>();
                var entity_list = repo.Query(r =>
                     r.name.IndexOf(payload.name) >= 0, r => r.OrderBy(r => r.name), r => r.Take(5)
                 ).ToList();
                return mapper.Map<List<vRequestAgencyView>>(entity_list);
            }
        }
    }
}
