﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models.Email;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Email;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Utils.Logging;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Color = System.Drawing.Color;

namespace DIP.TM.Services.eForms
{
    public class EFormProcessService<P, D, V> where P : EFormModel where D : eFormEntityBase where V : EFormView
    {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public EFormProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }

        //public V Add(SaveModel payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<D>();
        //        var entity = mapper.Map<D>(payload);
        //        repo.Add(entity);
        //        uow.SaveChanges();
        //        return mapper.Map<V>(entity);
        //    }
        //}

        //public V Add(ReceiptItem item) {
        //    var save_list = List(mapper.Map<P>(new SaveModel() {
        //        request_id = item.request_id,
        //        request_number = item.request_number,
        //    }));

        //    if (save_list.Count() == 0) {
        //        var save = Add(new SaveModel() {
        //            request_id = item.request_id,
        //            request_number = item.request_number,
        //            request_date = item.receipt_.request_date.Value,
        //            total_price = item.total_price,
        //            request_source_code = item.source_code,
        //            save_status_code = SaveStatusCodeConstant.DRAFT.ToString(),
        //        });
        //        return save;
        //    }

        //    return mapper.Map<V>(save_list[0]);
        //}

        //public V Get(long id) {
        //    D entity;
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<D>();
        //        entity = repo.Get(id);

        //        V view = mapper.Map<V>(entity);
        //        GetSave01Detail(view);
        //        return view;
        //    }
        //}


        [LogAopInterceptor]
        public virtual BaseResponseView<V> Save(object payload_json)
        {
            bool isNewEForm = false;
            P payload = JsonConvert.DeserializeObject<P>(Convert.ToString(payload_json));
            if (payload.id == null || payload.id == 0)
            {
                isNewEForm = true;
                payload.eform_number = GetEFormNumber();
            }

            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var isNewEmail = CheckNewEmail(payload);
                if (isNewEForm)
                {
                    payload.email_uuid = Guid.NewGuid().ToString();
                }

                var repo = uow.GetRepository<D>();
                D data = mapper.Map<D>(payload);
                data = repo.Update(data);
                uow.SaveChanges();
                V view = mapper.Map<V>(data);


                if (!string.IsNullOrEmpty(view.email) && (isNewEForm || isNewEmail))
                {
                    SendEmail(view);
                }


                return new BaseResponseView<V>() { data = view };
            }
        }

        string GetEFormNumber()
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<SequenceNumber>();
                string prefix = DateTime.Now.Year.ToString().Substring(2)
                    + DateTime.Now.Month.ToString("00")
                    //+ DateTime.Now.Date.ToString("00")
                    ;

                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.EFORM_NUMBER.ToString() && r.code == prefix).FirstOrDefault();

                if (entity == null)
                {
                    entity = new SequenceNumber()
                    {
                        topic = SequenceNumberConstant.EFORM_NUMBER.ToString(),
                        code = prefix,
                        number = 1,
                    };
                    repo.Add(entity);
                }
                else
                {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();
                return prefix + entity.number.Value.ToString("000000");
            }
        }

        [LogAopInterceptor]
        public virtual bool CheckNewEmail(P payload)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var result = true;
                var repo = uow.GetRepository<D>();
                var entity = repo.Query(r => r.id == payload.id).FirstOrDefault();

                if (entity != null)
                {
                    P data = mapper.Map<P>(entity);
                    result = (payload.email != data.email) ? true : false;
                }

                return result;
            }
        }
        [LogAopInterceptor]
        public virtual void SendEmail(V view)
        {
            //view.email_uuid = Guid.NewGuid().ToString();
            //var link = configuration["BaseUrl"] + "eform-save-01-process/edit/2/" + view.email_uuid;
            string eform_code = "01";

            if (typeof(V) == typeof(eForm_Save010View))
            {
                eform_code = "01";
            }
            else if (typeof(V) == typeof(eForm_Save020View))
            {
                eform_code = "02";
            }
            else if (typeof(V) == typeof(eForm_Save021View))
            {
                eform_code = "021";
            }
            else if (typeof(V) == typeof(eForm_Save030View))
            {
                eform_code = "030";
            }
            else if (typeof(V) == typeof(eForm_Save040View))
            {
                eform_code = "040";
            }
            else if (typeof(V) == typeof(eForm_Save050View))
            {
                eform_code = "050";
            }
            else if (typeof(V) == typeof(eForm_Save060View))
            {
                eform_code = "060";
            }
            else if (typeof(V) == typeof(eForm_Save070View))
            {
                eform_code = "070";
            }
            else if (typeof(V) == typeof(eForm_Save080View))
            {
                eform_code = "080";
            }
            else if (typeof(V) == typeof(eForm_Save120View))
            {
                eform_code = "120";
            }
            else if (typeof(V) == typeof(eForm_Save140View))
            {
                eform_code = "140";
            }
            else if (typeof(V) == typeof(eForm_Save190View))
            {
                eform_code = "190";
            }
            else if (typeof(V) == typeof(eForm_Save200View))
            {
                eform_code = "200";
            }
            else if (typeof(V) == typeof(eForm_Save210View))
            {
                eform_code = "210";
            }
            else if (typeof(V) == typeof(eForm_Save220View))
            {
                eform_code = "220";
            }
            else if (typeof(V) == typeof(eForm_Save230View))
            {
                eform_code = "230";
            }

            //waiting send link with id + uuid encryption

            var link = configuration["BaseUrl"] + "eform-save-" + eform_code + "-process/edit/" + view.id;
            var strEmailBody = new StringBuilder();

            strEmailBody.AppendLine("<h2>");
            strEmailBody.AppendLine(string.Format("<a href=\"{0}\">View</a>", link));
            strEmailBody.AppendLine("</h2>");

            strEmailBody.AppendLine("<h3>");
            strEmailBody.AppendLine("EForm Number: " + view.eform_number + " ");
            strEmailBody.AppendLine("</h3>");

            string updated_date = (view.updated_date.HasValue) ? view.updated_date.Value.ToString("dd/MM/yyyy") : "";
            strEmailBody.AppendLine("<h3>");
            strEmailBody.AppendLine("Last modified date: " + updated_date + " ");
            strEmailBody.AppendLine("</h3>");

            LineNotifyHelper.SendWait(configuration, string.Format("send mail : {0} link : {1}", view.email, link));

            var result = new BaseEmailService(configuration).Send(new EmailModel
            {
                subject = "eform-save-" + eform_code,
                to = view.email,
                body = strEmailBody.ToString(),
                is_html = true,
            });
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> Load(object payload_json)
        {
            P payload = JsonConvert.DeserializeObject<P>(Convert.ToString(payload_json));
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<D>();
                var entity = repo.Query(r => r.eform_number == payload.eform_number).FirstOrDefault();

                V view = mapper.Map<V>(entity);
                //GetSave01Detail(view);
                return new BaseResponseView<V>() { data = view };
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> Load(string eform_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<D>();
                var entity = repo.Query(r => r.eform_number == eform_number).FirstOrDefault();

                V view = mapper.Map<V>(entity);
                if (view == null)
                {
                    if (typeof(V) == typeof(eForm_Save010View))
                    {
                        view = mapper.Map<V>(new eForm_Save010View());
                    }
                    else if (typeof(V) == typeof(eForm_Save020View))
                    {
                        view = mapper.Map<V>(new eForm_Save020View());
                    }
                    else if (typeof(V) == typeof(eForm_Save021View))
                    {
                        view = mapper.Map<V>(new eForm_Save021View());
                    }
                    else if (typeof(V) == typeof(eForm_Save030View))
                    {
                        view = mapper.Map<V>(new eForm_Save030View());
                    }
                    else if (typeof(V) == typeof(eForm_Save040View))
                    {
                        view = mapper.Map<V>(new eForm_Save040View());
                    }
                    else if (typeof(V) == typeof(eForm_Save050View))
                    {
                        view = mapper.Map<V>(new eForm_Save050View());
                    }
                    else if (typeof(V) == typeof(eForm_Save060View))
                    {
                        view = mapper.Map<V>(new eForm_Save060View());
                    }
                    else if (typeof(V) == typeof(eForm_Save070View))
                    {
                        view = mapper.Map<V>(new eForm_Save070View());
                    }
                    else if (typeof(V) == typeof(eForm_Save080View))
                    {
                        view = mapper.Map<V>(new eForm_Save080View());
                    }
                    else if (typeof(V) == typeof(eForm_Save120View))
                    {
                        view = mapper.Map<V>(new eForm_Save120View());
                    }
                    else if (typeof(V) == typeof(eForm_Save140View))
                    {
                        view = mapper.Map<V>(new eForm_Save140View());
                    }
                    else if (typeof(V) == typeof(eForm_Save150View))
                    {
                        view = mapper.Map<V>(new eForm_Save150View());
                    }
                    else if (typeof(V) == typeof(eForm_Save190View))
                    {
                        view = mapper.Map<V>(new eForm_Save190View());
                    }
                    else if (typeof(V) == typeof(eForm_Save200View))
                    {
                        view = mapper.Map<V>(new eForm_Save200View());
                    }

                    view.is_search_success = false;
                    view.alert_msg = "null";
                }

                //GetSave01Detail(view);
                return new BaseResponseView<V>() { data = view };
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> LoadEFormByContractNumber(string contract_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<D>();
                var entity = repo.Query(r => false).FirstOrDefault();
                var view = mapper.Map<V>(entity);

                if (view == null)
                {
                    if (typeof(V) == typeof(eForm_Save030View))
                    {
                        view = mapper.Map<V>(new eForm_Save030());
                    }
                    else if (typeof(V) == typeof(eForm_Save050View))
                    {
                        view = mapper.Map<V>(new eForm_Save050());
                    }
                    else if (typeof(V) == typeof(eForm_Save080View))
                    {
                        view = mapper.Map<V>(new eForm_Save080());
                    }
                    else if (typeof(V) == typeof(eForm_Save150View))
                    {
                        view = mapper.Map<V>(new eForm_Save150());
                    }
                    else if (typeof(V) == typeof(eForm_Save190View))
                    {
                        view = mapper.Map<V>(new eForm_Save190());
                    }
                    else
                    {
                        return new BaseResponseView<V>() { data = null, is_error = true };
                    }
                }

                view.is_search_success = true;

                var s050_repo = uow.GetRepository<Save050>();
                var s050_entity = s050_repo.Query(r => r.contract_number == contract_number).FirstOrDefault();
                var s050_view = mapper.Map<Save050ProcessView>(s050_entity);

                if (s050_entity != null)
                {
                    s050_view.save050_receiver_people_type_code = (s050_view.receiver_people_list != null && s050_view.receiver_people_list.Count() > 0) ? s050_view.receiver_people_list[0].receiver_people_type_code : "RECEIVER";
                    s050_view.save050_people_type_code = (s050_view.save050_receiver_people_type_code == "RECEIVER_PERIOD") ? "RECEIVER" : "OWNER";
                    //s050_view.save050_people_type_code = (s050_view.people_list != null && s050_view.people_list.Count() > 0) ? s050_view.people_list[0].receiver_people_type_code : "OWNER"; //waiting confirm how to save type

                    if (s050_view.request_number != null)
                    {
                        if (typeof(V) == typeof(eForm_Save030View) || typeof(V) == typeof(eForm_Save040View) || typeof(V) == typeof(eForm_Save050View) || typeof(V) == typeof(eForm_Save080View) || typeof(V) == typeof(eForm_Save150View) || typeof(V) == typeof(eForm_Save190View)) //waitting confirm for 040
                        {
                            var s010_repo = uow.GetRepository<Save010>();
                            var s010_entity = s010_repo.Query(r => r.request_number == s050_view.request_number).FirstOrDefault();
                            var s010_view = mapper.Map<Save010ProcessView>(s010_entity);
                            var dtn = DateTime.Now;

                            if (typeof(V) == typeof(eForm_Save030View))
                            {
                                var post_repo = uow.GetRepository<PostRound>();
                                var post_entity_list = post_repo.Query(r => r.object_id == s010_view.id/* && r.post_round_instruction_rule_code == "RULE_?"*/).ToList(); //waiting confirm for rule_code
                                var post_entity = post_entity_list.FirstOrDefault(w => w.created_date == post_entity_list.Max(m => m.created_date));
                                var post_view = mapper.Map<PostRoundView>(post_entity);

                                if (post_entity != null)
                                {
                                    if (post_view.book_acknowledge_date.HasValue)
                                    {
                                        if (DateTime.Compare(dtn, post_view.book_acknowledge_date.Value.AddDays(60)) > 0)
                                        {
                                            view.is_search_success = false;
                                            view.alert_msg = "ไม่สามารถยื่นได้ เนื่องจากเกินระยะเวลาที่กำหนด";

                                            return new BaseResponseView<V>() { data = view };
                                        }

                                    }
                                }
                                else
                                {
                                    view.is_search_success = false;
                                    view.alert_msg = "ไม่สามารถยื่นได้ กรุณาติดต่อเจ้าหน้าที่";

                                    return new BaseResponseView<V>() { data = view };
                                }
                            }
                            else if ((s010_view.trademark_expired_date.HasValue) ? DateTime.Compare(dtn, s010_view.trademark_expired_date.Value) > 0 : true)
                            {
                                view.is_search_success = false;
                                view.alert_msg = "ไม่สามารถยื่นได้ เนื่องจากคำขอหมดอายุ";

                                return new BaseResponseView<V>() { data = view };
                            }

                            view.save010 = s010_view;
                            view.registration_load_number = s010_view.registration_number;
                            view.product_load_list = s010_view.product_list;
                            view.request_date_text = (s010_view.request_date.HasValue) ? s010_view.request_date.Value.ToString("dd/MM/yyyy") : "";

                            view.save050 = s050_view;
                            view.load_receiver_people_type_code = s050_view.save050_receiver_people_type_code;
                            view.people_load_list = s050_view.receiver_people_list; //Where(r => r.receiver_people_type_code == s050_view.save_receiver_people_type_code).ToList();
                            view.representative_load_list = s050_view.receiver_representative_list;
                        }
                    }
                }
                else
                {
                    view.is_search_success = false;
                    view.alert_msg = "ไม่พบเลขที่สัญญานี้ในระบบ กรุณาติดต่อเจ้าหน้าที่";
                }

                return new BaseResponseView<V>() { data = view };
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> LoadEFormByChallengerNumber(string challenger_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<D>();
                var entity = repo.Query(r => false).FirstOrDefault();
                var view = mapper.Map<V>(entity);

                if (view == null)
                {
                    if (typeof(V) == typeof(eForm_Save030View))
                    {
                        view = mapper.Map<V>(new eForm_Save030());
                    }
                    else if (typeof(V) == typeof(eForm_Save060View))
                    {
                        view = mapper.Map<V>(new eForm_Save060());
                    }
                    else if (typeof(V) == typeof(eForm_Save190View))
                    {
                        view = mapper.Map<V>(new eForm_Save190());
                    }
                    else
                    {
                        return new BaseResponseView<V>() { data = null, is_error = true };
                    }
                }
                view.is_search_success = true;

                var s020_repo = uow.GetRepository<Save020>();
                var s020_entity = s020_repo.Query(r => r.request_index == challenger_number).FirstOrDefault();
                var s020_view = mapper.Map<Save020ProcessView>(s020_entity);
                var dtn = DateTime.Now;

                if (s020_entity != null)
                {
                    if (s020_view.request_number != null)
                    {

                        var s010_repo = uow.GetRepository<Save010>();
                        var s010_entity = s010_repo.Query(r => r.request_number == s020_view.request_number).FirstOrDefault();
                        var s010_view = mapper.Map<Save010ProcessView>(s010_entity);

                        if (typeof(V) == typeof(eForm_Save030View))
                        {
                            var post_repo = uow.GetRepository<PostRound>();
                            var post_entity_list = post_repo.Query(r => r.object_id == s020_view.id/* && r.post_round_instruction_rule_code == "RULE_?"*/).ToList(); //waiting confirm for rule_code
                            var post_entity = post_entity_list.FirstOrDefault(w => w.created_date == post_entity_list.Max(m => m.created_date));
                            var post_view = mapper.Map<PostRoundView>(post_entity);

                            if (post_entity != null)
                            {
                                if (post_view.book_acknowledge_date.HasValue)
                                {
                                    if (DateTime.Compare(dtn, post_view.book_acknowledge_date.Value.AddDays(60)) > 0)
                                    {
                                        view.is_search_success = false;
                                        view.alert_msg = "ไม่สามารถยื่นได้ เนื่องจากเกินระยะเวลาที่กำหนด";

                                        return new BaseResponseView<V>() { data = view };
                                    }

                                }
                            }
                            else
                            {
                                view.is_search_success = false;
                                view.alert_msg = "ไม่สามารถยื่นได้ กรุณาติดต่อเจ้าหน้าที่";

                                return new BaseResponseView<V>() { data = view };
                            }
                        }
                        else if (typeof(V) == typeof(eForm_Save190View) && (s010_view.trademark_expired_date.HasValue) ? DateTime.Compare(dtn, s010_view.trademark_expired_date.Value) > 0 : true)
                        {
                            view.is_search_success = false;
                            view.alert_msg = "ไม่สามารถยื่นได้ เนื่องจากคำขอหมดอายุ";

                            return new BaseResponseView<V>() { data = view };
                        }

                        view.save010 = s010_view;
                        view.registration_load_number = s010_view.registration_number;
                        view.product_load_list = s010_view.product_list;
                        view.request_date_text = (s010_view.request_date.HasValue) ? s010_view.request_date.Value.ToString("dd/MM/yyyy") : "";

                        view.people_load_list = s020_view.people_list;
                        view.representative_load_list = s020_view.representative_list;
                    }
                }
                else
                {
                    view.is_search_success = false;
                    view.alert_msg = "ไม่พบเลขที่ผู้คัดค้านนี้ในระบบ กรุณาติดต่อเจ้าหน้าที่";
                }

                return new BaseResponseView<V>() { data = view };
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> LoadEFormByRequestNumber(string request_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                //var repo = uow.GetRepository<D>();
                //var entity = repo.Query(r => r.request_number == request_number).FirstOrDefault();
                //var view = mapper.Map<V>(entity);
                //if (entity == null)
                //{
                //    view.is_search_success = false;
                //    view.alert_msg = "ไม่พบเลขที่คำขอนี้ในระบบ";

                //    return new BaseResponseView<V>() { data = view };
                //}

                return new BaseResponseView<V>() { data = LoadEForm(null, request_number) };
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> LoadEFormByID(int id)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<D>();
                var entity = repo.Query(r => r.id == id).FirstOrDefault();
                var view = mapper.Map<V>(entity);
                string rqn = (entity != null) ? view.request_number : "";

                view = LoadEForm(view, rqn);

                return new BaseResponseView<V>() { data = view };
            }
        }

        V LoadEForm(V view, string request_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var isEntityNull = (view == null) ? true : false;
                //var request_number = (!isEntityNull) ? view.request_number : "";

                if (view == null)
                {
                    if (typeof(V) == typeof(eForm_Save020View))
                    {
                        view = mapper.Map<V>(new eForm_Save020());
                    }
                    else if (typeof(V) == typeof(eForm_Save021View))
                    {
                        view = mapper.Map<V>(new eForm_Save021());
                    }
                    else if (typeof(V) == typeof(eForm_Save030View))
                    {
                        view = mapper.Map<V>(new eForm_Save030());
                    }
                    else if (typeof(V) == typeof(eForm_Save040View))
                    {
                        view = mapper.Map<V>(new eForm_Save040());
                    }
                    else if (typeof(V) == typeof(eForm_Save050View))
                    {
                        view = mapper.Map<V>(new eForm_Save050());
                    }
                    else if (typeof(V) == typeof(eForm_Save060View))
                    {
                        view = mapper.Map<V>(new eForm_Save060());
                    }
                    else if (typeof(V) == typeof(eForm_Save070View))
                    {
                        view = mapper.Map<V>(new eForm_Save070());
                    }
                    else if (typeof(V) == typeof(eForm_Save080View))
                    {
                        view = mapper.Map<V>(new eForm_Save080());
                    }
                    else if (typeof(V) == typeof(eForm_Save120View))
                    {
                        view = mapper.Map<V>(new eForm_Save120());
                    }
                    else if (typeof(V) == typeof(eForm_Save140View))
                    {
                        view = mapper.Map<V>(new eForm_Save140());
                    }
                    else if (typeof(V) == typeof(eForm_Save150View))
                    {
                        view = mapper.Map<V>(new eForm_Save150());
                    }
                    else if (typeof(V) == typeof(eForm_Save190View))
                    {
                        view = mapper.Map<V>(new eForm_Save190());
                    }
                    else if (typeof(V) == typeof(eForm_Save200View))
                    {
                        view = mapper.Map<V>(new eForm_Save200());
                    }
                    else if (typeof(V) == typeof(eForm_Save210View))
                    {
                        view = mapper.Map<V>(new eForm_Save210());
                    }
                    else if (typeof(V) == typeof(eForm_Save220View))
                    {
                        view = mapper.Map<V>(new eForm_Save220());
                    }
                    else if (typeof(V) == typeof(eForm_Save230View))
                    {
                        view = mapper.Map<V>(new eForm_Save230());
                    }
                    else
                    {
                        return null;
                    }

                    if (request_number == "")
                    {
                        view.is_search_success = false;
                        view.alert_msg = "ไม่มี EForm นี้ในระบบฐานข้อมูล";

                        return view;
                    }
                }

                view.is_search_success = true;

                var s010_repo = uow.GetRepository<Save010>();
                var s010_entity = s010_repo.Query(r => r.request_number == request_number).FirstOrDefault();
                var s010_view = mapper.Map<Save010ProcessView>(s010_entity);
                //GetSave01Detail(s010_view);
                var dtn = DateTime.Now;

                if (s010_entity != null)
                {
                    if ((s010_view.trademark_expired_date.HasValue) ? DateTime.Compare(dtn, s010_view.trademark_expired_date.Value) > 0 : true)
                    {
                        view.is_search_success = false;
                        view.alert_msg = "ไม่สามารถยื่นได้ เนื่องจากเลขที่คำขอหมดอายุ";

                        return view;
                    }

                    if (typeof(V) == typeof(eForm_Save020View) || typeof(V) == typeof(eForm_Save021View))
                    {
                        var public_repo = uow.GetRepository<PublicRound>();
                        var public_entity = public_repo.Query(r => r.id == (s010_entity.public_round_id.HasValue ? s010_entity.public_round_id.Value : 0)).FirstOrDefault();
                        var public_view = mapper.Map<PublicRoundView>(public_entity);

                        if (public_entity != null)
                        {
                            var save010_list = public_view.save010_list.Where(r => r.request_number == request_number).ToList();

                            public_view.save010_list = save010_list;
                            public_view.public_round_item = public_view.public_round_item.Where(r => r.save_id == s010_entity.id).ToList();

                            if (typeof(V) == typeof(eForm_Save020View))
                            {
                                if (public_view.public_start_date.HasValue)
                                {
                                    if (DateTime.Compare(dtn, public_view.public_start_date.Value.AddDays(60)) > 0)
                                    {
                                        view.is_search_success = false;
                                        view.alert_msg = "ไม่สามารถคัดค้านเลขที่คำขอนี้ได้ เนื่องจากไม่อยู่ในระยะเวลาที่กำหนด";

                                        return view;
                                    }
                                }
                            }

                            if (typeof(V) == typeof(eForm_Save021View))
                            {
                                var s020_repo = uow.GetRepository<Save020>();
                                var s020_list = s020_repo.Query(r => r.request_number == request_number && r.public_round_id == public_entity.id).ToList();
                                var s020_view_list = mapper.Map<List<Save020ProcessView>>(s020_list);

                                var dispute_repo = uow.GetRepository<eForm_Save021Dispute>();
                                var dispute_list = dispute_repo.Query(r => r.save_id == ((view.id.HasValue) ? view.id.Value : 0) && !r.is_deleted).ToList();
                                var dispute_view_list = mapper.Map<List<eForm_Save021DisputeView>>(dispute_list);

                                if (s020_list != null && s020_list.Count() > 0)
                                {
                                    s020_view_list.ForEach(item =>
                                    {
                                        var post_repo = uow.GetRepository<PostRound>();
                                        var post_entity_list = post_repo.Query(r => r.object_id == item.id && r.post_round_instruction_rule_code == "RULE_6_1").ToList();
                                        var post_entity = post_entity_list.FirstOrDefault(w => w.created_date == post_entity_list.Max(m => m.created_date));
                                        var post_view = mapper.Map<PostRoundView>(post_entity);

                                        item.post_round = post_view;

                                        var dispute_item = dispute_view_list.Where(r => r.challenge_id == item.id).FirstOrDefault();
                                        item.is_active = (dispute_item != null) ? true : false;
                                        if (item.is_active)
                                        {
                                            dispute_view_list.Remove(dispute_item);
                                        }

                                        item.is_remove = false;
                                        if (post_entity != null)
                                        {
                                            if (post_view.book_acknowledge_date.HasValue)
                                            {
                                                if (DateTime.Compare(dtn, post_view.book_acknowledge_date.Value.AddDays(60)) > 0)
                                                {
                                                    item.is_remove = true;
                                                }

                                            }
                                        }
                                        else
                                        {
                                            item.is_remove = true;
                                        }
                                    });

                                    public_view.save020_list = s020_view_list.Where(w => !w.is_remove).ToList();
                                }

                                if (isEntityNull)
                                {
                                    //waiting for tell netcube before remove this if
                                    var view_021 = mapper.Map<eForm_Save021View>(view);
                                    view_021.people_list = s010_view.people_list; //change to people_load_list
                                    view_021.representative_list = s010_view.representative_list; //change to representative_load_list
                                    view = mapper.Map<V>(view_021);
                                }
                            }

                            view.public_round = public_view;
                        }
                        else
                        {
                            view.is_search_success = false;
                            view.alert_msg = "ไม่สามารถยื่นได้ กรุณาติดต่อเจ้าหน้าที่";

                            return view;
                        }
                    }
                    else if (typeof(V) == typeof(eForm_Save030View))
                    {
                        var post_repo = uow.GetRepository<PostRound>();
                        var post_entity_list = post_repo.Query(r => r.object_id == s010_view.id/* && r.post_round_instruction_rule_code == "RULE_?"*/).ToList(); //waiting confirm for rule_code
                        var post_entity = post_entity_list.FirstOrDefault(w => w.created_date == post_entity_list.Max(m => m.created_date));
                        var post_view = mapper.Map<PostRoundView>(post_entity);

                        if (post_entity != null)
                        {
                            if (post_view.book_acknowledge_date.HasValue)
                            {
                                if (DateTime.Compare(dtn, post_view.book_acknowledge_date.Value.AddDays(60)) > 0)
                                {
                                    view.is_search_success = false;
                                    view.alert_msg = "คำขอนี้ไม่สามารถยื่นอุทธรณืได้";

                                    return view;
                                }

                            }
                        }
                        else
                        {
                            view.is_search_success = false;
                            view.alert_msg = "คำขอนี้ไม่สามารถยื่นอุทธรณืได้";

                            return view;
                        }
                    }
                    else if (typeof(V) == typeof(eForm_Save070View))
                    {
                        var expDate = s010_view.trademark_expired_date;

                        var s070_repo = uow.GetRepository<Save070>();
                        var s070_entity = s070_repo.Query(r => r.request_number == request_number && r.make_date >= dtn.AddDays(-60)/* && r.status == "รอดำเนินการ"*/).FirstOrDefault(); //waiting db to filter with status รอดำเนินการ ในงวด3

                        if (expDate != null && expDate.HasValue)
                        {
                            if (DateTime.Compare(dtn, expDate.Value.AddDays(-90)) < 0 || DateTime.Compare(dtn, expDate.Value.AddDays(180)) > 0)
                            {
                                view.is_search_success = false;
                                view.alert_msg = "ไม่สามารถต่ออายุได้ เนื่องจากคำขอนี้ไม่ได้อยู่ในช่วงต่ออายุ กรุณาติดต่อเจ้าหน้าที่";

                                return view;
                            }
                            else if (s070_entity != null)
                            {
                                view.is_search_success = false;
                                view.alert_msg = "มีการยื่นต่ออายุของเลขที่คำขอนี้แล้ว ไม่สามารถยื่นซ้ำได้";

                                return view;
                            }
                            else if (s070_entity == null)
                            {
                                if (DateTime.Compare(dtn, expDate.Value) > 0 && DateTime.Compare(dtn, expDate.Value.AddDays(180)) <= 0)
                                {
                                    view.is_search_success = true;
                                    view.alert_msg = "มีค่าปรับในการต่ออายุ";
                                }

                            }
                        }
                    }

                    view.save010 = s010_view;
                    view.registration_load_number = s010_view.registration_number;
                    view.request_date_text = (s010_view.request_date.HasValue) ? s010_view.request_date.Value.ToString("dd/MM/yyyy") : "";
                    view.product_load_list = s010_view.product_list;

                    if (typeof(V) != typeof(eForm_Save020View))
                    {
                        view.people_load_list = s010_view.people_list;
                        view.representative_load_list = s010_view.representative_list;
                        
                        if (s010_view.contact_address != null)
                        {
                            view.contact_address_load_list = new List<SaveAddressView>();
                            view.contact_address_load_list.Add(s010_view.contact_address);
                        }

                        view.load_contact_type_code = "REPRESENTATIVE"; //mock, waiting field confirm

                        

                        if (s010_view.representative_list != null && s010_view.representative_list.Count() > 0)
                        {
                            view.load_representative_condition_type_code = s010_view.representative_list[0].address_representative_condition_type_code;
                        }
                    }
                    if (typeof(V) != typeof(eForm_Save220View))
                    {
                        //mock for eform 220 and will be removed in 3rd round
                        view.inter_registration_load_number = "300300003";
                        view.inter_registration_load_date = DateTime.Now;
                        view.inter_registration_cancel_load_date = DateTime.Now;
                        view.protection_load_date = DateTime.Now;
                        view.case_28_load_date = DateTime.Now;
                    }
                }
                else
                {
                    view.is_search_success = false;
                    view.alert_msg = "ไม่พบเลขที่คำขอนี้ในระบบ";
                }

                return view;
            }
        }

        [LogAopInterceptor]
        public virtual string GetProductCategory(string productName)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<RM_RequestItemSubType1>();
                var entity = repo.Query(r => r.name == productName).FirstOrDefault();
                var view = mapper.Map<ReferenceMasterView>(entity);

                string result = (entity != null) ? view.code : "";

                return result;
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> EForm060SearchDataRuleNumber(string rule_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<SaveInstructionRule>();
                var entity = repo.Query(r => r.instruction_number == rule_number).FirstOrDefault();
                var view = mapper.Map<SaveInstructionRuleView>(entity);

                if (view != null)
                {
                    string request_number = "";
                    string request_type = (!string.IsNullOrEmpty(view.request_type_code)) ? view.request_type_code : "";

                    if (request_type != "")
                    {
                        if (request_type == "010")
                        {
                            var s010_repo = uow.GetRepository<Save010>();
                            var s010_entity = s010_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s010_view = mapper.Map<Save010ProcessView>(s010_entity);

                            request_number = (s010_view != null) ? s010_view.request_number : "";
                        }
                        else if (request_type == "20")
                        {
                            var s020_repo = uow.GetRepository<Save020>();
                            var s020_entity = s020_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s020_view = mapper.Map<Save020ProcessView>(s020_entity);

                            request_number = (s020_view != null) ? s020_view.request_number : "";
                        }
                        else if (request_type == "21")
                        {
                            var s021_repo = uow.GetRepository<Save021>();
                            var s021_entity = s021_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s021_view = mapper.Map<Save021ProcessView>(s021_entity);

                            request_number = (s021_view != null) ? s021_view.request_number : "";
                        }
                        else if (request_type == "30")
                        {
                            var s030_repo = uow.GetRepository<Save030>();
                            var s030_entity = s030_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s030_view = mapper.Map<Save030ProcessView>(s030_entity);

                            request_number = (s030_view != null) ? s030_view.request_number : "";
                        }
                        else if (request_type == "40")
                        {
                            var s040_repo = uow.GetRepository<Save040>();
                            var s040_entity = s040_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s040_view = mapper.Map<Save040ProcessView>(s040_entity);

                            request_number = (s040_view != null) ? s040_view.request_number : "";
                        }
                        else if (request_type == "50")
                        {
                            var s050_repo = uow.GetRepository<Save050>();
                            var s050_entity = s050_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s050_view = mapper.Map<Save050ProcessView>(s050_entity);

                            request_number = (s050_view != null) ? s050_view.request_number : "";
                        }
                        else if (request_type == "60")
                        {
                            var s060_repo = uow.GetRepository<Save060>();
                            var s060_entity = s060_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s060_view = mapper.Map<Save060ProcessView>(s060_entity);

                            request_number = (s060_view != null) ? s060_view.request_number : "";
                        }
                        else if (request_type == "70")
                        {
                            var s070_repo = uow.GetRepository<Save070>();
                            var s070_entity = s070_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s070_view = mapper.Map<Save070ProcessView>(s070_entity);

                            request_number = (s070_view != null) ? s070_view.request_number : "";
                        }
                        else if (request_type == "80")
                        {
                            var s080_repo = uow.GetRepository<Save080>();
                            var s080_entity = s080_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s080_view = mapper.Map<Save080ProcessView>(s080_entity);

                            request_number = (s080_view != null) ? s080_view.request_number : "";
                        }
                        else if (request_type == "140")
                        {
                            var s140_repo = uow.GetRepository<Save140>();
                            var s140_entity = s140_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var s140_view = mapper.Map<Save140ProcessView>(s140_entity);

                            request_number = (s140_view != null) ? s140_view.request_number : "";
                        }
                        else
                        {
                            var sOther_repo = uow.GetRepository<SaveOther>();
                            var sOther_entity = sOther_repo.Query(r => r.id == view.save_id).LastOrDefault();
                            var sOther_view = mapper.Map<SaveOtherProcessView>(sOther_entity);

                            request_number = (sOther_view != null) ? sOther_view.request_number : "";
                        }

                        if (request_number != "")
                        {
                            var result = EForm060SearchDataRequestNumber(request_number);

                            return result;
                        }
                    }
                }

                return null;
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> EForm060SearchDataChallengerNumber(string challenger_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var s020_repo = uow.GetRepository<Save020>();
                var s020_entity = s020_repo.Query(r => r.request_index == challenger_number).FirstOrDefault();
                var s020_view = mapper.Map<Save020ProcessView>(s020_entity);
                string request_number = (s020_view != null) ? s020_view.request_number : "";

                var result = EForm060SearchDataRequestNumber(request_number);

                return result;
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<V> EForm060SearchDataRequestNumber(string request_number)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var ef060view = new eForm_Save060View();
                var view = mapper.Map<V>(ef060view);

                var s010_repo = uow.GetRepository<Save010>();
                var s010_entity = s010_repo.Query(r => r.request_number == request_number).LastOrDefault();
                var s010_view = mapper.Map<Save010ProcessView>(s010_entity);

                var s020_repo = uow.GetRepository<Save020>();
                var s020_entity_list = s020_repo.Query(r => r.request_number == request_number).ToList();
                var s020_view_list = mapper.Map<List<Save020ProcessView>>(s020_entity_list);

                var s040_repo = uow.GetRepository<Save040>();
                var s040_entity_list = s040_repo.Query(r => r.request_number == request_number).ToList();
                var s040_view_list = mapper.Map<List<Save040ProcessView>>(s040_entity_list);

                var s050_repo = uow.GetRepository<Save050>();
                var s050_entity_list = s050_repo.Query(r => r.request_number == request_number).ToList();
                var s050_view_list = mapper.Map<List<Save050ProcessView>>(s050_entity_list);

                var rdc_repo = uow.GetRepository<RequestDocumentCollect>();
                var file_repo = uow.GetRepository<DIP.TM.Datas.File>();

                if (s010_view != null)
                {
                    ef060view.people_list = new List<SaveAddressView>();
                    ef060view.representative_list = new List<SaveAddressView>();
                    ef060view.transfer_people_list = new List<SaveAddressView>();
                    ef060view.transfer_representative_list = new List<SaveAddressView>();
                    ef060view.contact_address_list = new List<SaveAddressView>();
                    ef060view.checking_similar_translate_list = new List<eForm_Save060CheckingSimilarWordTranslateView>();
                    ef060view.product_list = new List<SaveProductView>();

                    ef060view.request_number = s010_view.request_number;
                    ef060view.registration_number = s010_view.registration_number;

                    bool is3D = false;
                    if (!string.IsNullOrEmpty(s010_view.request_mark_feature_code_list))
                    {
                        int indx = s010_view.request_mark_feature_code_list.IndexOf("TRADEMARK_3D");

                        if (indx != -1)
                        {
                            is3D = true;
                        }
                    }

                    ef060view.save060_img_type_type_code = (is3D) ? "3D" : "2D";


                    var rdc_2d_entity = rdc_repo.Query(
                        r => r.save_id == s010_view.id &&
                        r.request_document_collect_type_code == "TRADEMARK_2D" &&
                        r.request_document_collect_status_code == "ADD")
                        .LastOrDefault();
                    var rdc_2d_view = mapper.Map<vRequestDocumentCollectView>(rdc_2d_entity);
                    if (rdc_2d_view != null)
                    {
                        ef060view.img_file_2d_id = rdc_2d_view.file_id;
                    }


                    var rdc_3d_1_entity = rdc_repo.Query(
                        r => r.save_id == s010_view.id &&
                        r.request_document_collect_type_code == "TRADEMARK_3D_1" &&
                        r.request_document_collect_status_code == "ADD")
                        .LastOrDefault();
                    var rdc_3d_1_view = mapper.Map<vRequestDocumentCollectView>(rdc_3d_1_entity);
                    if (rdc_3d_1_view != null)
                    {
                        ef060view.img_file_3d_id_1 = rdc_3d_1_view.file_id;
                    }


                    var rdc_3d_2_entity = rdc_repo.Query(
                        r => r.save_id == s010_view.id &&
                        r.request_document_collect_type_code == "TRADEMARK_3D_2" &&
                        r.request_document_collect_status_code == "ADD")
                        .LastOrDefault();
                    var rdc_3d_2_view = mapper.Map<vRequestDocumentCollectView>(rdc_3d_2_entity);
                    if (rdc_3d_2_view != null)
                    {
                        ef060view.img_file_3d_id_2 = rdc_3d_2_view.file_id;
                    }


                    var rdc_3d_3_entity = rdc_repo.Query(
                        r => r.save_id == s010_view.id &&
                        r.request_document_collect_type_code == "TRADEMARK_3D_3" &&
                        r.request_document_collect_status_code == "ADD")
                        .LastOrDefault();
                    var rdc_3d_3_view = mapper.Map<vRequestDocumentCollectView>(rdc_3d_3_entity);
                    if (rdc_3d_3_view != null)
                    {
                        ef060view.img_file_3d_id_3 = rdc_3d_3_view.file_id;
                    }

                    var rdc_3d_4_entity = rdc_repo.Query(
                        r => r.save_id == s010_view.id &&
                        r.request_document_collect_type_code == "TRADEMARK_3D_4" &&
                        r.request_document_collect_status_code == "ADD")
                        .LastOrDefault();
                    var rdc_3d_4_view = mapper.Map<vRequestDocumentCollectView>(rdc_3d_4_entity);
                    if (rdc_3d_4_view != null)
                    {
                        ef060view.img_file_3d_id_4 = rdc_3d_4_view.file_id;
                    }

                    var rdc_3d_5_entity = rdc_repo.Query(
                        r => r.save_id == s010_view.id &&
                        r.request_document_collect_type_code == "TRADEMARK_3D_5" &&
                        r.request_document_collect_status_code == "ADD")
                        .LastOrDefault();
                    var rdc_3d_5_view = mapper.Map<vRequestDocumentCollectView>(rdc_3d_5_entity);
                    if (rdc_3d_5_view != null)
                    {
                        ef060view.img_file_3d_id_5 = rdc_3d_5_view.file_id;
                    }

                    var rdc_3d_6_entity = rdc_repo.Query(
                        r => r.save_id == s010_view.id &&
                        r.request_document_collect_type_code == "TRADEMARK_3D_6" &&
                        r.request_document_collect_status_code == "ADD")
                        .LastOrDefault();
                    var rdc_3d_6_view = mapper.Map<vRequestDocumentCollectView>(rdc_3d_6_entity);
                    if (rdc_3d_6_view != null)
                    {
                        ef060view.img_file_3d_id_6 = rdc_3d_6_view.file_id;
                    }

                    ef060view.is_sound_mark_human = false;
                    ef060view.is_sound_mark_animal = false;
                    ef060view.is_sound_mark_sound = false;
                    ef060view.is_sound_mark_other = false;

                    if (!string.IsNullOrEmpty(s010_view.sound_mark_list))
                    {
                        int indx_human = s010_view.sound_mark_list.IndexOf("HUMAN");
                        int indx_animal = s010_view.sound_mark_list.IndexOf("ANIMAL");
                        int indx_sound = s010_view.sound_mark_list.IndexOf("NATURE");
                        int indx_other = s010_view.sound_mark_list.IndexOf("OTHERS");

                        if (indx_human != -1)
                        {
                            ef060view.is_sound_mark_human = true;
                        }
                        if (indx_animal != -1)
                        {
                            ef060view.is_sound_mark_animal = true;
                        }
                        if (indx_sound != -1)
                        {
                            ef060view.is_sound_mark_sound = true;
                        }
                        if (indx_other != -1)
                        {
                            ef060view.is_sound_mark_other = true;
                        }
                    }

                    ef060view.img_w = s010_view.img_w;
                    ef060view.img_h = s010_view.img_h;
                    ef060view.remark_5_1_3 = s010_view.remark_8;
                    ef060view.remark_8 = s010_view.remark_8;
                    ef060view.remark_9 = s010_view.remark_9;
                    ef060view.sound_jpg_file_id = s010_view.sound_jpg_file_id;

                    var ef060_sound_img_entity = file_repo.Query(r => r.id == ef060view.sound_jpg_file_id).FirstOrDefault();
                    ef060view.sound_jpg_file = mapper.Map<FileView>(ef060_sound_img_entity);

                    var ef060_img_2d_entity = file_repo.Query(r => r.id == ef060view.img_file_2d_id).FirstOrDefault();
                    ef060view.img_2D_file = mapper.Map<FileView>(ef060_img_2d_entity);

                    var ef060_img_3d_entity_1 = file_repo.Query(r => r.id == ef060view.img_file_3d_id_1).FirstOrDefault();
                    ef060view.img_3D_file_1 = mapper.Map<FileView>(ef060_img_3d_entity_1);

                    var ef060_img_3d_entity_2 = file_repo.Query(r => r.id == ef060view.img_file_3d_id_2).FirstOrDefault();
                    ef060view.img_3D_file_2 = mapper.Map<FileView>(ef060_img_3d_entity_2);

                    var ef060_img_3d_entity_3 = file_repo.Query(r => r.id == ef060view.img_file_3d_id_3).FirstOrDefault();
                    ef060view.img_3D_file_3 = mapper.Map<FileView>(ef060_img_3d_entity_3);

                    var ef060_img_3d_entity_4 = file_repo.Query(r => r.id == ef060view.img_file_3d_id_4).FirstOrDefault();
                    ef060view.img_3D_file_4 = mapper.Map<FileView>(ef060_img_3d_entity_4);

                    var ef060_img_3d_entity_5 = file_repo.Query(r => r.id == ef060view.img_file_3d_id_5).FirstOrDefault();
                    ef060view.img_3D_file_5 = mapper.Map<FileView>(ef060_img_3d_entity_5);

                    var ef060_img_3d_entity_6 = file_repo.Query(r => r.id == ef060view.img_file_3d_id_6).FirstOrDefault();
                    ef060view.img_3D_file_6 = mapper.Map<FileView>(ef060_img_3d_entity_6);

                    if (s010_view.people_list != null && s010_view.people_list.Count() > 0)
                    {
                        s010_view.people_list.ForEach(f =>
                        {
                            var item = f;
                            item.change_ref_id = f.id;
                            item.id = 0;
                            item.save_id = 0;

                            ef060view.people_list.Add(item);
                            ef060view.transfer_people_list.Add(item);
                        });
                    }

                    if (s010_view.representative_list != null && s010_view.representative_list.Count() > 0)
                    {
                        s010_view.representative_list.ForEach(f =>
                        {
                            var item = f;
                            item.change_ref_id = f.id;
                            item.id = 0;
                            item.save_id = 0;

                            ef060view.representative_list.Add(item);
                            ef060view.transfer_representative_list.Add(item);
                        });
                    }

                    if (s010_view.checking_similar_wordsoundtranslate_list != null && s010_view.checking_similar_wordsoundtranslate_list.Count() > 0)
                    {
                        s010_view.checking_similar_wordsoundtranslate_list.ForEach(f =>
                        {
                            var item = mapper.Map<eForm_Save060CheckingSimilarWordTranslateView>(f);
                            item.change_ref_id = item.id;
                            item.id = 0;
                            item.save_id = 0;

                            ef060view.checking_similar_translate_list.Add(item);
                        });
                    }

                    if (s010_view.product_list != null && s010_view.product_list.Count() > 0)
                    {
                        s010_view.product_list.ForEach(f =>
                        {
                            var item = f;
                            item.change_ref_id = f.id;
                            item.id = 0;
                            item.save_id = 0;

                            ef060view.product_list.Add(item);
                        });
                    }

                    if (s010_view.contact_address != null)
                    {
                        var item = s010_view.contact_address;
                        item.change_ref_id = s010_view.contact_address.id;
                        item.id = 0;
                        item.save_id = 0;

                        ef060view.contact_address_list.Add(item);
                    }



                    if (s020_view_list != null && s020_view_list.Count() > 0)
                    {
                        ef060view.challenger_people_list = new List<SaveAddressView>();
                        ef060view.challenger_representative_list = new List<SaveAddressView>();
                        ef060view.challenger_contact_address_list = new List<SaveAddressView>();

                        s020_view_list.ForEach(f =>
                        {
                            if (f.people_list != null && f.people_list.Count() > 0)
                            {
                                f.people_list.ForEach(ff =>
                                {
                                    var item = ff;
                                    item.change_ref_id = ff.id;
                                    item.id = 0;
                                    item.save_id = 0;

                                    ef060view.challenger_people_list.Add(item);
                                });
                            }

                            if (f.representative_list != null && f.representative_list.Count() > 0)
                            {
                                f.representative_list.ForEach(ff =>
                                {
                                    var item = ff;
                                    item.change_ref_id = ff.id;
                                    item.id = 0;
                                    item.save_id = 0;

                                    ef060view.challenger_representative_list.Add(item);
                                });
                            }

                            if (f.contact_address != null)
                            {
                                var item = f.contact_address;
                                item.change_ref_id = f.id;
                                item.id = 0;
                                item.save_id = 0;

                                ef060view.challenger_contact_address_list.Add(f.contact_address);
                            }
                        });
                    }

                    if (s040_view_list != null && s040_view_list.Count() > 0)
                    {
                        ef060view.legacy_people_list = new List<SaveAddressView>();
                        ef060view.legacy_representative_list = new List<SaveAddressView>();
                        ef060view.transfer_receiver_people_list = new List<SaveAddressView>();
                        ef060view.transfer_receiver_representative_list = new List<SaveAddressView>();

                        s040_view_list.ForEach(f =>
                        {
                            if (f.people_list != null && f.people_list.Count() > 0)
                            {
                                f.people_list.ForEach(ff =>
                                {
                                    var item = ff;
                                    item.change_ref_id = ff.id;
                                    item.id = 0;
                                    item.save_id = 0;

                                    ef060view.transfer_receiver_people_list.Add(item);
                                    ef060view.legacy_people_list.Add(item);
                                });
                            }

                            if (f.representative_list != null && f.representative_list.Count() > 0)
                            {
                                f.representative_list.ForEach(ff =>
                                {
                                    var item = ff;
                                    item.change_ref_id = ff.id;
                                    item.id = 0;
                                    item.save_id = 0;

                                    ef060view.transfer_receiver_representative_list.Add(item);
                                    ef060view.legacy_representative_list.Add(item);
                                });
                            }
                        });
                    }

                    if (s050_view_list != null && s050_view_list.Count() > 0)
                    {
                        ef060view.receiver_people_list = new List<SaveAddressView>();
                        ef060view.receiver_representative_list = new List<SaveAddressView>();
                        ef060view.contract_receiver_people_list = new List<SaveAddressView>();
                        ef060view.contract_receiver_representative_list = new List<SaveAddressView>();

                        s050_view_list.ForEach(f =>
                        {
                            if (f.receiver_people_list != null && f.receiver_people_list.Count() > 0)
                            {
                                f.receiver_people_list.ForEach(ff =>
                                {
                                    ff.change_ref_id = ff.id;
                                    ff.id = 0;
                                    ff.save_id = 0;
                                    //var item = ff;
                                    //item.change_ref_id = ff.id;
                                    //item.id = 0;
                                    //item.save_id = 0;

                                    //ef060view.receiver_people_list.Add(item);
                                    //ef060view.contract_receiver_people_list.Add(item);
                                });
                            }

                            if (f.receiver_representative_list != null && f.receiver_representative_list.Count() > 0)
                            {
                                f.receiver_representative_list.ForEach(ff =>
                                {
                                    ff.change_ref_id = ff.id;
                                    ff.id = 0;
                                    ff.save_id = 0;

                                    //var item = ff;
                                    //item.change_ref_id = ff.id;
                                    //item.id = 0;
                                    //item.save_id = 0;

                                    //ef060view.receiver_representative_list.Add(item);
                                    //ef060view.contract_receiver_representative_list.Add(item);
                                });
                            }
                        });

                        ef060view.save050_list = s050_view_list;
                    }
                }
                else
                {
                    view.is_search_success = false;
                    view.alert_msg = "ไม่มีเลขที่คำขอนี้ในระบบ";
                }

                view = mapper.Map<V>(ef060view);

                return new BaseResponseView<V>() { data = view };
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponseView<vEForm210DateListView> DateListByRequestNumber(string request_number_list)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                string[] resultRequestNumber = request_number_list.Split(',');

                vEForm210DateListView view = new vEForm210DateListView();
                List<vEForm210DateView> result = new List<vEForm210DateView>();

                if (resultRequestNumber.Count() > 0)
                {
                    foreach (var request_number in resultRequestNumber)
                    {
                        if (request_number != "" && request_number != ",")
                        {
                            vEForm210DateView item = new vEForm210DateView();

                            var s010_repo = uow.GetRepository<Save010>();
                            var s010_entity = s010_repo.Query(r => r.request_number == request_number).FirstOrDefault();
                            var s010_view = mapper.Map<Save010ProcessView>(s010_entity);

                            if (s010_view != null)
                            {
                                var case28_repo = uow.GetRepository<Save010Case28>();
                                var case28_entity = case28_repo.Query(r => r.save_id == s010_view.id).FirstOrDefault();
                                var case28_view = mapper.Map<Save010Case28View>(case28_entity);

                                item.request_number = s010_view.request_number;
                                item.request_date = s010_view.request_date;
                                if (case28_view != null)
                                {
                                    item.case_28_date = case28_view.case_28_date;
                                }
                            }

                            result.Add(item);
                        }
                    }
                }

                view.eform_210_date_list = result;
                view.alert_msg = (view.eform_210_date_list.Count() % 2 == 0) ? "tets message" : ""; //mock
                view.is_search_success = (view.eform_210_date_list.Count() % 2 == 0) ? false : true; //mock

                return new BaseResponseView<vEForm210DateListView>() { data = view };
            }
        }

        //void GetSave01Detail(BaseSaveView s010_view)
        //{
        //    try
        //    {
        //        using (var uow = uowProvider.CreateUnitOfWork())
        //        {
        //            var save_repo = uow.GetRepository<Save010>();
        //            Save010 save = save_repo.Query(r => r.request_number == s010_view.request_number).FirstOrDefault();

        //            if (typeof(V) == typeof(Save010ProcessView))
        //            {
        //                if (save.sound_file_id.HasValue && save.sound_file_id > 0)
        //                {
        //                    var file_repo = uow.GetRepository<File>();
        //                    var file = file_repo.Get(save.sound_file_id.Value);
        //                    s010_view.sound_file_physical_path = file.physical_path;
        //                }

        //                //if (save.public_round_id.HasValue && save.public_round_id > 0) {
        //                //    var pr_repo = uow.GetRepository<PublicRound>();
        //                //    var pr = pr_repo.Get(save.public_round_id.Value);
        //                //    s010_view.public_start_date = pr.public_start_date;
        //                //}
        //            }

        //            if (typeof(V) == typeof(Save020ProcessView))
        //            {
        //                if (save.public_round_id.HasValue && save.public_round_id > 0)
        //                {
        //                    var pr_repo = uow.GetRepository<PublicRound>();
        //                    var pr = pr_repo.Get(save.public_round_id.Value);
        //                    s010_view.public_start_date = pr.public_start_date;
        //                    s010_view.book_index = pr.book_index;
        //                }
        //            }

        //            s010_view.trademark_status_code = save.trademark_status_code;
        //            s010_view.registration_number = save.registration_number;
        //            s010_view.people_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressPeople);
        //            s010_view.representative_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressRepresentative);
        //            s010_view.joiner_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressJoiner);
        //            s010_view.product_01_list = mapper.Map<List<SaveProductView>>(save.Save010Product);
        //            s010_view.contact_address_01 = mapper.Map<SaveAddressView>(save.Save010AddressContact.FirstOrDefault());
        //        }
        //    }
        //    catch (Exception _ex)
        //    {

        //    }
        //}

        //public BaseResponseView<List<V>> GetResponseView(List<V> view_list) {
        //    return new BaseResponseView<List<V>>() { data = view_list };
        //}

        //public V Send(P payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<D>();
        //        payload.save_status_code = SaveStatusCodeConstant.SENT.ToString();
        //        D data = mapper.Map<D>(payload);
        //        repo.Update(data);
        //        uow.SaveChanges();

        //        V view = mapper.Map<V>(data);
        //        GetSave01Detail(view);
        //        return view;
        //    }
        //}

        //public V Send(Save010AddModel payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<D>();
        //        payload.save_status_code = SaveStatusCodeConstant.SENT.ToString();
        //        payload.saved_process_date = DateTime.Now;
        //        payload.document_status_code = DocumentStatusCodeConstant.WAIT_DOCUMENTING.ToString();
        //        //payload.document_classification_status_code = DocumentClassificationStatusCodeConstant.WAIT_SPLIT_JOB.ToString();
        //        D data = mapper.Map<D>(payload);
        //        repo.Update(data);
        //        uow.SaveChanges();

        //        V view = mapper.Map<V>(data);
        //        GetSave01Detail(view);

        //        return view;
        //    }
        //}

        //public V Delete(dynamic payload) {
        //    return Delete(JsonConvert.DeserializeObject<P>(Convert.ToString(payload)));
        //}
        //public V Delete(P payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<D>();
        //        if (payload.ids != null) {
        //            foreach (long id in payload.ids) {
        //                var entity = repo.Get(id);
        //                var new_payload = mapper.Map<P>(entity);
        //                new_payload.save_status_code = SaveStatusCodeConstant.DELETE.ToString();
        //                new_payload.cancel_reason = payload.cancel_reason;
        //                Save(new_payload);
        //            }
        //        }
        //        return mapper.Map<V>(null);
        //    }
        //}

        //internal V LoadFromRequest(long request_id) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<D>();
        //        var request01 = repo.Query(r => r.request_id == request_id).FirstOrDefault();
        //        return mapper.Map<V>(request01);
        //    }
        //}

        //public List<V> List(dynamic _payload) {
        //    return List(JsonConvert.DeserializeObject<P>(Convert.ToString(_payload)));
        //}

        //public List<V> List(P payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<D>();
        //        List<D> data_list = repo.Query(r =>
        //        (payload.request_id == 0 || r.request_id == payload.request_id) &&
        //        (string.IsNullOrEmpty(payload.request_number) || r.request_number == payload.request_number) &&
        //        (string.IsNullOrEmpty(payload.request_source_code) || payload.request_source_code == RequestSourceCodeConstant.ALL.ToString() || r.request_source_code == payload.request_source_code) &&
        //        (!payload.make_date.HasValue || r.make_date.Value.Date == payload.make_date.Value.Date) &&
        //        (string.IsNullOrEmpty(payload.request_index) || r.request_index == payload.request_index) &&
        //        (string.IsNullOrEmpty(payload.save_status_code) || payload.save_status_code == SaveStatusCodeConstant.ALL.ToString() || r.save_status_code == payload.save_status_code)
        //        , r => r.OrderByDescending(s => s.id)).ToList();

        //        List<V> view_list = mapper.Map<List<V>>(data_list);

        //        foreach (V view in view_list) {
        //            GetSave01Detail(view);
        //        }

        //        return view_list;
        //    }
        //}

        //public List<V> ListSave010(vSave010Model payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<vSave010>();
        //        List<vSave010> data_list = repo.Query(r =>
        //        (string.IsNullOrEmpty(payload.document_status_code) || payload.document_status_code == "ALL" || r.document_status_code == payload.document_status_code) &&
        //        (string.IsNullOrEmpty(payload.save_status_code) || payload.save_status_code == "ALL" || r.save_status_code == payload.save_status_code) &&
        //        (string.IsNullOrEmpty(payload.document_classification_status_code) || payload.document_classification_status_code == "ALL" || r.document_classification_status_code == payload.document_classification_status_code) &&
        //        (string.IsNullOrEmpty(payload.checking_type_code) || payload.checking_type_code == "ALL" || r.checking_type_code == payload.checking_type_code) &&
        //        (string.IsNullOrEmpty(payload.checking_status_code) || payload.checking_status_code == "ALL" || r.checking_status_code == payload.checking_status_code) &&
        //        (!payload.checking_receiver_by.HasValue || payload.checking_receiver_by == r.checking_receiver_by) &&
        //        (!payload.considering_receiver_by.HasValue || payload.considering_receiver_by == r.considering_receiver_by) &&
        //        1 == 1
        //        , r => r.OrderByDescending(s => s.updated_by)).ToList();

        //        var view_list = mapper.Map<List<V>>(data_list);
        //        foreach (V view in view_list) {
        //            GetSave01Detail(view);
        //        }
        //        return view_list;
        //    }
        //}

        //public vSave010View Save010Load(long id, bool is_full = false) {
        //    var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(configuration, uowProvider, mapper);
        //    var view = service.Get(id);

        //    if (is_full) {
        //        var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(configuration, uowProvider, mapper);
        //        view.full_view = service_full_view.Get(id);
        //    }

        //    return view;
        //}

        //public List<V> ListNotSent(BasePageModelPayload<SaveListDraftModel> payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<D>();
        //        var data_list = repo.Query(r =>
        //            r.request_number == payload.filter.request_number &&
        //            r.save_status_code != SaveStatusCodeConstant.SENT.ToString(),
        //            s => s.OrderBy(o => o.created_date)
        //        );

        //        List<V> view_list = mapper.Map<List<V>>(data_list);

        //        foreach (V view in view_list) {
        //            GetSave01Detail(view);
        //        }

        //        return view_list;
        //    }
        //}

        //void GetSave01Detail(V view) {
        //    try {
        //        using (var uow = uowProvider.CreateUnitOfWork()) {
        //            var save_repo = uow.GetRepository<Save010>();
        //            Save010 save = save_repo.Query(r => r.request_number == view.request_number).FirstOrDefault();
        //            view.people_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressPeople);
        //            view.representative_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressRepresentative);
        //            view.joiner_01_list = mapper.Map<List<SaveAddressView>>(save.Save010AddressJoiner);
        //            view.product_01_list = mapper.Map<List<SaveProductView>>(save.Save010Product);
        //            view.contact_address_01 = mapper.Map<SaveAddressView>(save.Save010AddressContact.FirstOrDefault());
        //        }
        //    } catch (Exception _ex) {

        //    }
        //}


    }
}
