﻿using AutoMapper;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using DIP.TM.Services.Attributes;
using DIP.TM.Datas;
using System.Linq;

using Amazon.SimpleEmail;
using DIP.TM.Models.Payloads.Appeal;
using DIP.TM.Utils;
using System.Threading.Tasks;
using DIP.TM.Models.Views.Appeal;
using Org.BouncyCastle.Ocsp;

namespace DIP.TM.Services
{
    public class AppealProcessService
    {
        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;

        public AppealProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            this.uowProvider = uowProvider;
            this.configuration = configuration;
            this.mapper = mapper;
        }

        [LogAopInterceptor]
        public virtual Appeal_Role02SaveCaseSummaryView StartRequestCaseSummary(int request_id, string requst_type_code)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repoSave03 = uow.GetRepository<Save030>();
                var repoSaveCaseSummary = uow.GetRepository<Appeal_Role02SaveCaseSummary>();

                var save030 = repoSave03.Query(x => x.request_id == request_id).FirstOrDefault();
                var entity = repoSaveCaseSummary.Query(x => x.request_id == save030.request_id).FirstOrDefault();

                if (entity == null)
                {
                    // Insert 
                    #region
                    var save = new Appeal_Role02SaveCaseSummary();
                    save.request_id = save030.request_id;
                    save.request_date = save030.request_date;
                    save.reuest_number = save030.request_number;
                    save.requst_type_code = requst_type_code;
                    save.document_more = null;
                    save.process_action_code = AppealCaseSummaryProcessCodeConstant.CONSIDER.ToString();
                    save.step_action_code = AppealCaseSummaryStepCodeConstant.CREATE_CASESUMMARY.ToString();
                    save.status_action_code = AppealCaseSummaryStatusCodeConstant.WAIT_CREATE_CASESUMMARY.ToString();
                    save.appeal_reason_code = save030.save030_appeal_reason_code;
                    #endregion
                    repoSaveCaseSummary.Update(save);
                }
                else
                {
                    // Update
                    #region
                    entity.id = entity.id;
                    entity.request_id = save030.request_id;
                    entity.request_date = save030.request_date;
                    entity.reuest_number = save030.request_number;
                    entity.requst_type_code = requst_type_code;
                    entity.document_more = null;
                    entity.process_action_code = AppealCaseSummaryProcessCodeConstant.CONSIDER.ToString();
                    entity.step_action_code = AppealCaseSummaryStepCodeConstant.CREATE_CASESUMMARY.ToString();
                    entity.status_action_code = AppealCaseSummaryStatusCodeConstant.WAIT_CREATE_CASESUMMARY.ToString();
                    entity.appeal_reason_code = save030.save030_appeal_reason_code;
                    #endregion
                    repoSaveCaseSummary.Update(entity);
                }

                uow.SaveChanges();

                return mapper.Map<Appeal_Role02SaveCaseSummaryView>(entity);

            }
        }

        [LogAopInterceptor]
        public virtual Appeal_Role02SaveCaseSummaryView GetRequestCaseSummary(int request_id)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Appeal_Role02SaveCaseSummary>();
                var object_list = repo.Query(x => x.request_id == request_id).FirstOrDefault();
                return mapper.Map<Appeal_Role02SaveCaseSummaryView>(object_list);
            }
        }

        [LogAopInterceptor]
        public virtual Appeal_Role02SaveCaseSummaryView SaveDraftRequestCaseSummary(Appeal_Role02SaveCaseSummaryAddModel payload)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Appeal_Role02SaveCaseSummary>();

                //logic
                payload.status_action_code = AppealCaseSummaryStatusCodeConstant.SAVE_DRAFT.ToString();

                var entity = mapper.Map<Appeal_Role02SaveCaseSummary>(payload);
                repo.Update(entity);
                uow.SaveChanges();
                return mapper.Map<Appeal_Role02SaveCaseSummaryView>(entity);
            }
        }

        [LogAopInterceptor]
        public virtual Appeal_Role02SaveCaseSummaryView SaveRequestCaseSummarySend(Appeal_Role02SaveCaseSummaryAddModel payload)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Appeal_Role02SaveCaseSummary>();

                //logic
                payload.status_action_code = AppealCaseSummaryStatusCodeConstant.DONE.ToString();

                var entity = mapper.Map<Appeal_Role02SaveCaseSummary>(payload);
                repo.Update(entity);
                uow.SaveChanges();
                return mapper.Map<Appeal_Role02SaveCaseSummaryView>(entity);
            }
        }

        [LogAopInterceptor]
        public virtual List<vCaseSummaryView> GetRequestCaseSummaryList()
        {
            List<vAppeal_CaseSummary> list = new List<vAppeal_CaseSummary>();
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<vAppeal_CaseSummary>();
                var object_list = repo.GetAll().ToList();
                return mapper.Map<List<vCaseSummaryView>>(object_list);
            }
        }
    }
}
