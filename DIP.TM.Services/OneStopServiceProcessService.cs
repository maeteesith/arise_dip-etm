﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Auths;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Helps;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Utils.Extensions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Web;

namespace DIP.TM.Services.OneStopServices {

    public class OneStopServiceProcessService {

        protected long? userId;

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public OneStopServiceProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;

            userId = HttpContext.Current?.User?.UserId();
        }

        [LogAopInterceptor]
        public virtual List<vRecordRegistrationNumberView> RecordRegistrationNumberSave(List<vRecordRegistrationNumberAddModel> payload) {
            var view_list = new List<vRecordRegistrationNumberView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();

                foreach (var model in payload.Where(r => r.is_check
                //&& string.IsNullOrEmpty(r.registration_number)
                )) {
                    var entity = repo.Get(model.save_id);

                    entity.registration_number = HelpService.GetRegistrationNumber(uowProvider, entity.request_item_type_code);

                    entity.floor3_proposer_by = userId;
                    entity.floor3_proposer_date = model.floor3_proposer_date;
                    entity.floor3_registrar_status_code = Floor3RegistrarStatusCodeConstant.WAIT.ToString();
                    entity.floor3_registrar_by = model.floor3_registrar_by;

                    repo.Update(entity);
                    ////view_list.Add(mapper.Map<vRecordRegistrationNumberView>(entity));
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vRecordRegistrationNumberAllowView> RecordRegistrationNumberAllowDone(List<vRecordRegistrationNumberAllowAddModel> payload) {
            var view_list = new List<vRecordRegistrationNumberAllowView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = repo.Get(model.id.Value);

                    //entity.registration_number = HelpService.GetRegistrationNumber(uowProvider);

                    entity.floor3_registrar_by = userId;
                    entity.floor3_registrar_date = model.floor3_registrar_date;
                    entity.floor3_registrar_status_code = Floor3RegistrarStatusCodeConstant.DONE.ToString();

                    repo.Update(entity);
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vRecordRegistrationNumberAllowView> RecordRegistrationNumberAllowCancel(List<vRecordRegistrationNumberAllowAddModel> payload) {
            var view_list = new List<vRecordRegistrationNumberAllowView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = repo.Get(model.id.Value);

                    //entity.registration_number = HelpService.GetRegistrationNumber(uowProvider);

                    entity.floor3_registrar_by = userId;
                    entity.floor3_registrar_date = model.floor3_registrar_date;
                    entity.floor3_registrar_status_code = Floor3RegistrarStatusCodeConstant.CANCEL.ToString();
                    entity.floor3_registrar_description = model.floor3_registrar_description;

                    repo.Update(entity);
                }

                uow.SaveChanges();
            }

            return view_list;
        }
    }
}
