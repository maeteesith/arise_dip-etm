﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Configs;
using DIP.TM.Models.Logging;
using DIP.TM.Services.BaseServices;
using DIP.TM.Services.Extensions;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils.Extensions;
using DIP.TM.Utils.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Web;

namespace DIP.TM.Services.Logging
{
    public class LibLogService : BaseDataService<AuditLogModel, EventLog>
    {
        private readonly long? userId;
        private string clientIp;
        private AppConfigModel configModel;
        public LibLogService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) : base(configuration, uowProvider, mapper)
        {
            userId = HttpContext.Current?.User?.UserId();
            clientIp = HttpContext.Current.Request.HttpContext.Connection.RemoteIpAddress?.ToString();
            configModel = configuration.ToConfig();
        }

        public void Log(AuditLogModel log)
        {
            if (configModel.EnableAuditLog == false)
                return;
            try
            {
                var logDb = mapper.Map<EventLog>(log);
                logDb.from_ip_address = clientIp == "::1" ? "localhost" : clientIp;
                Task.Run(() =>
                {
                    base.Create(logDb);
                });
                return;
            }
            catch (System.Exception ex)
            {
                LineNotifyHelper.SendWait(configuration, ex.Message);
                return;
            }
        }
    }
}
