﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Logging;
using DIP.TM.Services.BaseServices;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils.Extensions;
using DIP.TM.Utils.Logging;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using System.Web;

namespace DIP.TM.Services.Logging
{
    public class ApiLogService: BaseDataService<AuditLogModel, AuditLog>
    {
        private readonly long? userId;
        public ApiLogService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper):base(configuration, uowProvider, mapper)
        {
            userId = HttpContext.Current?.User?.UserId();
        }

        public void Log(AuditLogModel log)
        {
            try
            {
                var logDb = mapper.Map<AuditLog>(log);
                Task.Run(()=> {
                    base.Create(logDb);
                });
                return;
            }
            catch (System.Exception ex)
            {
                LineNotifyHelper.SendWait(configuration, ex.Message);
                return;
            }
        }
    }
}
