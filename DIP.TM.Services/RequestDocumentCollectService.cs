﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace DIP.TM.Services.Saves {
    public class RequestDocumentCollectService {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestDocumentCollectService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }
        [LogAopInterceptor]
        public virtual vRequestDocumentCollectView RequestDocumentCollectItemSave(vRequestDocumentCollectAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestDocumentCollect>();
                var entity = repo.Update(new RequestDocumentCollect() {
                    id = payload.rd_id.HasValue ? payload.rd_id.Value : 0,
                    save_id = payload.save_id,
                    receipt_item_id = payload.id,
                    request_document_collect_type_code = payload.request_document_collect_type_code,
                    file_id = payload.file_id,
                    request_document_collect_status_code = RequestDocumentCollectStatusCodeConstant.ADD.ToString(),
                });

                uow.SaveChanges();

                return mapper.Map<vRequestDocumentCollectView>(payload);
            }
        }
        [LogAopInterceptor]
        public virtual vRequestDocumentCollectView RequestDocumentCollectItemRemove(vRequestDocumentCollectAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestDocumentCollect>();
                var entity = repo.Get(payload.rd_id.Value);
                entity.request_document_collect_status_code = RequestDocumentCollectStatusCodeConstant.DELETED.ToString();
                entity.cancel_reason = payload.cancel_reason;
                repo.Update(entity);
                uow.SaveChanges();
                return mapper.Map<vRequestDocumentCollectView>(payload);
            }
        }
        [LogAopInterceptor]
        public virtual string GetTrademark2DPhysicalPath(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vRequestDocumentCollect>();

                var entity = repo.Query(r => r.save_id == id && r.request_document_collect_type_code == RequestDocumentCollectTypeCodeConstant.TRADEMARK_2D.ToString()).FirstOrDefault();

                return entity != null ? "/File/Content/" + entity.file_id : "";
            }
        }
    }
}
