using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Services.Interfaces;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace DIP.TM.Services.Inquiry
{
    /// <summary>
    /// 
    /// </summary>
    public class RequestItemSubTypeService: IRequestItemSubTypeService
    {
        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestItemSubTypeService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }

        public List<String> GetRequestItemSubType(string inputs)
        {
            HashSet<string> re = new HashSet<string>();
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var masterRepo = uow.GetRepository<RM_RequestItemSubType1>();
               //DbContext context = uow.GetContext<RM_RequestItemSubType1>();
                //context.Database.
                
                // Get all data from request item subtype 1 table
                var results = masterRepo.GetAllActive(x => x.OrderBy(y => y.id));
                //return mapper.Map<List<RM_RequestItemSubType1>>(results);
                var reqItmSubTypeList = mapper.Map<List<RM_RequestItemSubType1>>(results);

                //Split input data by space
                //var inputList = inputs.Split(" ").ToList();
                //foreach (var input in inputList)
                //{
                    foreach ( RM_RequestItemSubType1 rist in reqItmSubTypeList)
                    {

                        var wordList = Regex.Replace(rist.name,@"\.\.\.$", "").Split(" ").ToList();
                        foreach (var item in wordList)
                        {
                            //Console.WriteLine( Regex.Replace(item,@"\.\.\.$", ""));
                            if (!String.IsNullOrEmpty(item) && inputs.Contains(item))
                            {
                               // Console.WriteLine("Input :"+inputs+" item :"+item);
                               // Console.WriteLine("Yeaah found :"+rist.name);
                                re.Add(rist.name);
                            }


                        }
                    }                    
                //}


                // using (var command = context.Database.GetDbConnection().CreateCommand())
                // {
                //     command.CommandText = "SELECT TOP (5) t.name FROM RM_RequestItemSubType1 t where dbo.ufn_levenshtein(name,'@name') < 15";
                //     var parameter = command.CreateParameter();
                //     parameter.ParameterName = "@name";
                //     parameter.Value = input;
                //     command.Parameters.Add(parameter);
                //     context.Database.OpenConnection();
                //     using (var result = command.ExecuteReader())
                //     {
                //         while(result.Read())
                //         {
                //             re.Add(result.GetValue(0).ToString());
                //         }
                //     }
                // }
            }
            return re.ToList();
        }

    }
}
