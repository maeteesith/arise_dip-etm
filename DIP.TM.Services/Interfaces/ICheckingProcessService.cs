﻿using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using System.Collections.Generic;

namespace DIP.TM.Services.Interfaces {
    public interface ICheckingProcessService {
        [LogAopInterceptor]
        BaseResponsePageView<List<vSave010View>> CheckingItemList(PageRequest payload);
        [LogAopInterceptor]
        BaseResponsePageView<List<vUM_UserView>> CheckingItemCheckerList(PageRequest payload);
        [LogAopInterceptor]
        List<vSave010View> CheckingItemReceive(vSave010Model payload);
        [LogAopInterceptor]
        vSave010View CheckingSimilarWordSoundTranslateSave(Save010AddModel payload);
        [LogAopInterceptor]
        BaseResponsePageView<List<vSave010View>> CheckingSimilarList(PageRequest payload);
        [LogAopInterceptor]
        BaseResponsePageView<List<vSave010View>> ConsideringSimilarListPage(PageRequest payload);

        [LogAopInterceptor]
        vSave010View CheckingSimilarLoad(long id);

        [LogAopInterceptor]
        string CheckingSimilarGetRequestItemSubTypeGroup(string item_sub_type_list);


        [LogAopInterceptor]
        bool CheckingSimilarTagAdd(object[] payload);
        [LogAopInterceptor]
        bool CheckingSimilarTagRemove(List<vSave010CheckingTagSimilarAddModel> payloads);
        [LogAopInterceptor]
        List<vSave010CheckingTagSimilarView> CheckingSimilarTagList(long id);
        [LogAopInterceptor]
        bool CheckingSimilarResultSend(vSave010Model payload);
        //[LogAopInterceptor]
        //List<vSave010View> ConsideringSimilarList(PageRequest payload);
        [LogAopInterceptor]
        vSave010View ConsideringSimilarLoad(long id);
        [LogAopInterceptor]
        List<vSave010CheckingTagSimilarView> ConsideringSimilarSave(List<vSave010CheckingTagSimilarAddModel> payloads);
        [LogAopInterceptor]
        List<ItemSubType1SuggestionView> ItemSubType1SuggestionLoad(long id);

        [LogAopInterceptor]
        BaseResponsePageView<List<vCheckingSimilarSave010ListView>> CheckingSimilarSave010List(CheckingSimilarSave010ListModel payload);

        [LogAopInterceptor]
        List<StatisticView> CheckingSimilarStatisticList();

        [LogAopInterceptor]
        BaseResponsePageView<List<vSave010CheckingInstructionDocumentView>> ConsideringSimilarInstructionList(PageRequest payload);
        [LogAopInterceptor]
        List<vSave010CheckingTagSimilarView> CheckingSimilarResultOwnerSame(List<vSave010CheckingTagSimilarAddModel> payloads);
        [LogAopInterceptor]
        vSave010View ConsideringSimilarInstructionLoad(long id);
        [LogAopInterceptor]
        vSave010View ConsideringSimilarInstructionRuleSave(Save010InstructionRuleAddModel payload);
        [LogAopInterceptor]
        vSave010View ConsideringSimilarInstructionDelete(Save010InstructionRuleAddModel payload);
        [LogAopInterceptor]
        vSave010View ConsideringSimilarInstructionPublicAdd(long id);
        [LogAopInterceptor]
        vSave010View ConsideringSimilarInstructionPublicDelete(long id);
        [LogAopInterceptor]
        vSave010View ConsideringSimilarInstructionSave(vSave010Model payload);
        [LogAopInterceptor]
        vSave010View ConsideringSimilarInstructionSend(long id);

        [LogAopInterceptor]
        ConsideringSimilarDocumentView ConsideringSimilarDocumentLoad(long id);

        [LogAopInterceptor]
        Save010Case28ProcessView ConsideringSimilarKor10Load(long id);

        [LogAopInterceptor]
        Save010Case28ProcessView ConsideringSimilarKor10Save(Save010Case28ProcessAddModel payload);

        [LogAopInterceptor]
        ConsideringSimilarDocumentView ConsideringSimilarDocumentSave(ConsideringSimilarDocumentAddModel payload);

        [LogAopInterceptor]
        Save010CheckingTagSimilarMethodView CheckingTagSimilarMethod(CheckingSimilarSave010ListModel payload);

        [LogAopInterceptor]
        CheckingSimilarResultDuplicateView CheckingSimilarResultDuplicateList(string request_number);

        [LogAopInterceptor]
        CheckingSimilarResultDuplicateView CheckingSimilarResultDuplicateAdd(CheckingSimilarResultDuplicateAddModel payload);

        [LogAopInterceptor]
        Save010ProcessView CheckingSimilarWordSoundTranslateBackSend(Save010AddModel payload);

        [LogAopInterceptor]
        List<Save010ProcessView> CheckingItemAutoSplit();
    }
}
