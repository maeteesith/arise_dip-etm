﻿using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using System.Collections.Generic;

namespace DIP.TM.Services.Interfaces
{
    public interface IRequestItemSubType1Service
    {
        [LogAopInterceptor]
        List<ReferenceMasterView> CheckTag(ReferenceMasterModel payload);
    }
}
