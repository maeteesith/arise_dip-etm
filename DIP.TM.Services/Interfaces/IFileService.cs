﻿using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;

namespace DIP.TM.Services.Interfaces
{
    public interface IFileService
    {
        [LogAopInterceptor]
        FileView Add(FileView payload);
        [LogAopInterceptor]
        FileView Get(long fileId);
    }
}
