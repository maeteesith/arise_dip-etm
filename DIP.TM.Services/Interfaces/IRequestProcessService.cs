﻿using DIP.TM.Models.Pagers;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using System.Collections.Generic;

namespace DIP.TM.Services.Interfaces
{
    public interface IRequestProcessService<T,U>
    {
        [LogAopInterceptor]
        Request01ProcessView Request01Add(Request01ProcessAddModel payload);
        [LogAopInterceptor]
        Request01ProcessView Request01Split(Request01ProcessAddModel payload);
        [LogAopInterceptor]
        Request01ProcessView Request01Save(Request01ProcessAddModel payload);
        [LogAopInterceptor]
        Request01ProcessView Request01LoadFromReferenceNumber(string reference_number);
        [LogAopInterceptor]
        Request01ProcessView Request01Load(long id);
        [LogAopInterceptor]
        Request01ItemView Request01ItemDelete(Request01ItemAddModel payload);
        [LogAopInterceptor]
        Request01ItemView Request01ItemLoad(long id);
        [LogAopInterceptor]
        BaseResponsePageView<List<vRequest01ItemView>> Request01ItemList(PageRequest payload);
        [LogAopInterceptor]
        ItemCalculatorView CalculateItemprice(int qty);
        [LogAopInterceptor]
        List<Request01ItemView> Request01List(ref PageRequest payload);
        [LogAopInterceptor]
        RequestOtherProcessView RequestOtherAdd(RequestOtherProcessAddModel payload);
        [LogAopInterceptor]
        RequestOtherProcessView RequestOtherSave(RequestOtherProcessAddModel payload);
        [LogAopInterceptor]
        RequestOtherProcessView RequestOtherLoad(long id);
        [LogAopInterceptor]
        RequestOtherItemSubView RequestOtherItemSubDelete(RequestOtherItemSubAddModel payload);
        [LogAopInterceptor]
        RequestOtherProcessView RequestOtherPrint(long id);
        [LogAopInterceptor]
        RequestOtherItemView RequestOtherItemLoad(long request_id, string request_number);
        [LogAopInterceptor]
        List<vRequestOtherItemView> RequestOtherItemSubList(RequestOtherItemAddModel payload);
    }
}
