﻿using DIP.TM.Models;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DIP.TM.Services.Interfaces
{
    public interface IReferenceMasterService
    {
        [LogAopInterceptor]
        Task<List<ReferenceMasterView>> List(BasePageModelPayload<ReferenceMasterModel> payload);
    }
}
