﻿using DIP.TM.Services.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Services.Interfaces
{
    public interface IRequestItemSubTypeService
    {
        [LogAopInterceptor]
        List<String> GetRequestItemSubType(string inputs);

    }
}
