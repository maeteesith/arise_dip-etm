﻿using DIP.TM.Models.Email;
using DIP.TM.Services.Attributes;

namespace DIP.TM.Services.Interfaces
{
    public interface IBaseEmailService
    {
        [LogAopInterceptor]
        bool Send(EmailModel email);
    }
}
