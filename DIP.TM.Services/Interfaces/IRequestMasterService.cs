﻿using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Services.Interfaces
{
    public interface IRequestMasterService
    {
        [LogAopInterceptor]
        List<Request01ListEvidenceTypeView> GetRequest01ListEvidenceType();
        [LogAopInterceptor]
        List<PriceMasterView> GetPriceRequestMaster();
        [LogAopInterceptor]
        List<RequestSoundTypeView> GetRequestSoundMaster();
    }
}
