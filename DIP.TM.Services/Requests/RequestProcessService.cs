﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.BaseServices;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Helps;
using DIP.TM.Services.Receipts;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Uows.DataAccess.Paging;
using DIP.TM.Utils;
using DIP.TM.Utils.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Web;

namespace DIP.TM.Services.Requests {
    /// <summary>
    /// 
    /// </summary>
    public class RequestProcessService : BaseDataService<Request01ProcessView, Request01Process> {

        protected IDataPager<Request01Item> pager;
        protected long? userId;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
            : base(configuration, uowProvider, mapper) {
            var user = HttpContext.Current.User;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        /// <param name="pager"></param>
        public RequestProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, IDataPager<Request01Item> pager)
           : base(configuration, uowProvider, mapper) {
            userId = HttpContext.Current?.User?.UserId();
            this.pager = pager;
        }
        [LogAopInterceptor]
        public virtual Request01ProcessView Request01Add(Request01ProcessAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Request01Process>();
                var entity = mapper.Map<Request01Process>(payload);
                entity = MapRequest01(entity);
                repo.Add(entity);
                uow.SaveChanges();
                //uow.OnCompleteTransaction(() => new ActivityLogService<Request01Process>(uowProvider).Add(entity));
                return base.FetchViewModel(entity.id);
            }
        }
        [LogAopInterceptor]
        public virtual Request01ProcessView Request01Split(Request01ProcessAddModel payload) {
            List<Request01ItemAddModel> payload_item_check_list = payload.item_list.Where(r => r.is_check).ToList();

            if (payload_item_check_list.Count() > 0 && payload.item_list.Count() - payload_item_check_list.Count() > 0) {
                //foreach (var item in payload.item_list) {
                //    item.reference_number = "";
                //}

                var view = Request01Save(payload);
                using (var uow = uowProvider.CreateUnitOfWork()) {
                    //var repo = uow.GetRepository<Request01Process>();
                    //var entity = repo.Get(view.id);
                    //entity.id = 0;
                    //entity.item_count = payload_item_check_list.Count();
                    //repo.Add(entity);
                    //uow.SaveChanges();

                    //entity = repo.Get(view.id);
                    //entity.item_count = payload.item_list.Count() - payload_item_check_list.Count();
                    //repo.Update(entity);
                    //uow.SaveChanges();

                    var repo = uow.GetRepository<Request01Process>();
                    var entity = repo.Get(view.id);
                    entity.item_count = payload.item_list.Count() - payload_item_check_list.Count();
                    repo.Update(entity);
                    uow.SaveChanges();

                    entity.id = 0;
                    entity.item_count = payload_item_check_list.Count();
                    repo.Add(entity);
                    uow.SaveChanges();

                    using (var _uow = uowProvider.CreateUnitOfWork()) {
                        var item_repo = _uow.GetRepository<Request01Item>();
                        foreach (var payload_item_check in payload.item_list) {
                            if (payload_item_check.is_check)
                                payload_item_check.request01_id = entity.id;
                            payload_item_check.reference_number = "";
                            item_repo.Update(mapper.Map<Request01Item>(payload_item_check));
                        }
                        _uow.SaveChanges();
                    }

                    return base.FetchViewModel(entity.id);
                }
            }

            return base.FetchViewModel(payload.id.Value);
        }
        [LogAopInterceptor]
        public virtual Request01ProcessView Request01Save(Request01ProcessAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Request01Process>();

                payload.item_count = payload.item_list.Count();

                if (payload.commercial_affairs_province_code != "FLOOR3") {
                    payload.source_code = RequestSourceCodeConstant.COMMERCIAL_AFFAIRS_PROVINCE.ToString();
                } else {
                    payload.source_code = RequestSourceCodeConstant.CENTRAL.ToString();
                }

                // Receipt only is_check
                if (false) {
                    string reference_number = "";
                    List<Request01ItemAddModel> payload_item_check_list = payload.item_list.Where(r => r.is_check).ToList();
                    if (payload_item_check_list.Count() > 0) {
                        reference_number = HelpService.GetReferenceNumber(uowProvider);
                        payload_item_check_list.ForEach(r => { r.reference_number = reference_number; r.status_code = RequestStatusCodeConstant.RECEIPT.ToString(); });
                    }
                    Request01Process entity = mapper.Map<Request01Process>(payload);
                    foreach (Request01Item item in entity.Request01Item) {
                        if (string.IsNullOrEmpty(item.request_number)) {
                            item.request_number = HelpService.GetRequestNumber(uowProvider, item.item_type_code);
                            item.status_code = RequestStatusCodeConstant.SAVE.ToString();
                        }
                    }

                    repo.Update(entity);

                    //uow.OnCompleteTransaction(() => new ActivityLogService<Request01Process>(uowProvider).Add(entity));

                    uow.SaveChanges();

                    if (payload_item_check_list.Count() > 0) {
                        ReceiptService receipt_service = new ReceiptService(configuration, uowProvider, mapper);
                        Receipt receipt = receipt_service.Add(entity, reference_number);
                    }
                    return base.FetchViewModel(entity.id);
                } else {
                    // Receipt all
                    string reference_number = "";
                    foreach (var item in payload.item_list) {
                        if (string.IsNullOrEmpty(item.request_number)) {
                            item.request_number = HelpService.GetRequestNumber(uowProvider, item.item_type_code);
                            item.status_code = RequestStatusCodeConstant.SAVE.ToString();
                        }
                    }

                    List<Request01ItemAddModel> payload_item_check_list = payload.item_list.Where(r => string.IsNullOrEmpty(r.reference_number)).ToList();
                    if (payload_item_check_list.Count() > 0) {
                        reference_number = HelpService.GetReferenceNumber(uowProvider);
                        payload.item_list.ForEach(r => { r.reference_number = reference_number; r.status_code = RequestStatusCodeConstant.RECEIPT.ToString(); });
                    }

                    Request01Process entity = mapper.Map<Request01Process>(payload);

                    repo.Update(entity);

                    //uow.OnCompleteTransaction(() => new ActivityLogService<Request01Process>(uowProvider).Add(entity));

                    uow.SaveChanges();

                    if (payload_item_check_list.Count() > 0) {
                        ReceiptService receipt_service = new ReceiptService(configuration, uowProvider, mapper);
                        Receipt receipt = receipt_service.Add(entity, reference_number);

                        if (payload.commercial_affairs_province_code != "FLOOR3") {
                            var receipt_model = mapper.Map<ReceiptAddModel>(receipt);

                            foreach (var item in receipt_model.item_list) {
                                item.book_index = payload.item_list.Where(r => r.request_number == item.request_number).FirstOrDefault().book_index;
                                item.page_index = payload.item_list.Where(r => r.request_number == item.request_number).FirstOrDefault().page_index;
                            }

                            receipt_service.Save(receipt_model, false);
                        }
                    }

                    return base.FetchViewModel(entity.id);
                }
            }
        }

        [LogAopInterceptor]
        public virtual Request01ProcessView Request01LoadFromReferenceNumber(string reference_number) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var item_repo = uow.GetRepository<Request01Item>();
                var item_entity = item_repo.Query(r => r.reference_number == reference_number).FirstOrDefault();

                var request_repo = uow.GetRepository<Request01Process>();
                var request_entity = request_repo.Get(item_entity.request01_id);
                var view = mapper.Map<Request01ProcessView>(request_entity);

                var user_repo = uow.GetRepository<UM_User>();
                var user_entity = user_repo.Get(item_entity.created_by);
                view.created_by_name = user_entity.name;

                view.item_list = view.item_list.Where(r => r.reference_number == reference_number).ToList();
                return view;
            }

        }

        [LogAopInterceptor]
        public virtual Request01ProcessView Request01Load(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var request01Repo = uow.GetRepository<Request01Process>();
                var request01 = request01Repo.GetActive(id);
                return mapper.Map<Request01ProcessView>(request01);
            }
        }


        private Request01Process MapRequest01(Request01Process entity) {
            for (int i = 1; i <= entity.item_count; i++) {
                var item_sub = new Request01ItemSub() {
                    item_sub_index = 1,
                    product_count = 1,
                    total_price = CalculateItemprice(1).total
                };

                var item = new Request01Item() {
                    item_index = i,
                    total_price = entity.is_wave_fee == false ? item_sub.total_price : 0,
                    item_type_code = "TRADE_MARK_AND_SERVICE_MARK",
                    income_type = "010",
                    income_description = "ค่าขอจดทะเบียนเครื่องหมายการค้า/บริการ/รับรอง/เครื่องหมายร่วมใช้",
                    sound_type_code = "HUMAN",
                    facilitation_act_status_code = "DOCUMENT_COMPLETED",
                    is_otop = false,
                    status_code = RequestStatusCodeConstant.DRAFT.ToString(),
                };

                item.Request01ItemSub.Add(item_sub);
                entity.Request01Item.Add(item);
            }
            entity.total_price = entity.Request01Item.Sum(r => r.total_price);
            entity.source_code = RequestSourceCodeConstant.CENTRAL.ToString();

            return entity;
        }

        [LogAopInterceptor]
        public virtual Request01ItemView Request01ItemDelete(Request01ItemAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Request01Item>();
                if (payload.ids != null) {
                    foreach (long id in payload.ids) {
                        var entity = repo.Get(id);
                        entity.status_code = RequestStatusCodeConstant.DELETE.ToString();
                        entity.cancel_reason = payload.cancel_reason;
                        repo.Update(entity);
                    }
                }
                uow.SaveChanges();
                return mapper.Map<Request01ItemView>(null);
            }
        }

        [LogAopInterceptor]
        public virtual Request01ItemView Request01ItemLoad(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Request01Item>();
                var item = repo.Get(id);
                return mapper.Map<Request01ItemView>(item);
            }
        }

        [LogAopInterceptor]
        public virtual BaseResponsePageView<List<vRequest01ItemView>> Request01ItemList(PageRequest payload) {

            using (var uow = uowProvider.CreateUnitOfWork()) {



                //var repo = uow.GetRepository<vRequest01Item>();
                //List<vRequest01Item> item_list = repo.Query(r =>
                //    (string.IsNullOrEmpty(payload.created_by_name) || r.created_by_name.IndexOf(payload.created_by_name) >= 0) &&
                //    (!payload.request_date.HasValue || r.request_date.Value.Date == payload.request_date.Value.Date) &&
                //    (string.IsNullOrEmpty(payload.reference_number) || r.reference_number == payload.reference_number) &&
                //    (string.IsNullOrEmpty(payload.request_number) || r.request_number == payload.request_number) &&
                //    (
                //    string.IsNullOrEmpty(payload.receipt_status_code) ||
                //    payload.receipt_status_code == ReceiptStatusCodeConstant.ALL.ToString() ||
                //    (payload.receipt_status_code == ReceiptStatusCodeConstant.UNPAID.ToString() && r.receipt_status_code == null) ||
                //    r.receipt_status_code == payload.receipt_status_code
                //    )
                //, r => r.OrderByDescending(s => s.id)).ToList();

                var repo = uow.GetRepository<vRequest01Item>();
                var itemList = repo.Filters(x => x.is_deleted == false);
                var filterList = itemList.FilterDynamic(payload);
                payload.item_total = filterList.Count();
                var resultPage = filterList.Page(payload.page_index, payload.item_per_page);
                var view_list = mapper.Map<List<vRequest01ItemView>>(resultPage);

                foreach (var view in view_list) {
                    var product_repo = uow.GetRepository<Save010Product>();
                    var product = product_repo.Query(r => r.save_id == view.save_id);
                    view.product_list = mapper.Map<List<SaveProductView>>(product);
                }

                return new BaseResponsePageView<List<vRequest01ItemView>>() {
                    data = new BaseResponsePageDataModel<List<vRequest01ItemView>>() {
                        list = view_list,
                        paging = payload
                    }
                };
            }

        }
        [LogAopInterceptor]
        public virtual ItemCalculatorView CalculateItemprice(int qty) {
            if (qty <= 5) {
                return new ItemCalculatorView() { qty = qty, total = qty * 1000 };
            } else {
                return new ItemCalculatorView() { qty = qty, total = 9000 };
            }
        }

        //string GetReferenceNumber() {

        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<SequenceNumber>();
        //        string year = (DateTime.Now.Year + 543).ToString().Substring(2);
        //        SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.REQUEST_REFERENCE_NUMBER.ToString() && r.code == year).FirstOrDefault();

        //        if (entity == null) {
        //            entity = new SequenceNumber() {
        //                topic = SequenceNumberConstant.REQUEST_REFERENCE_NUMBER.ToString(),
        //                code = year,
        //                number = 1,
        //            };
        //            repo.Add(entity);
        //        } else {
        //            entity.number++;
        //            repo.Update(entity);
        //        }
        //        uow.SaveChanges();
        //        return year + entity.number.Value.ToString("000000");
        //    }
        //}
        [LogAopInterceptor]
        public virtual List<Request01ItemView> Request01List(ref PageRequest payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Request01Item>();
                var testQuery = repo.Filters(x => x.id > 0 && x.is_deleted == false);
                var testFilter = testQuery.FilterDynamic(payload);
                payload.item_total = testFilter.Count();
                var resultPage = testFilter.Page(payload.page_index, payload.item_per_page);
            }

            var data = pager.Get(payload.page_index, payload.item_per_page);
            payload.item_total = (int)data.TotalEntityCount;
            return mapper.Map<List<Request01ItemView>>(data.Data);
        }

        [LogAopInterceptor]
        public virtual RequestOtherProcessView RequestOtherAdd(RequestOtherProcessAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestOtherProcess>();
                foreach (RequestOtherRequest01ItemAddModel item in payload.request_01_item_list) {
                    item.status_code = RequestStatusCodeConstant.DRAFT.ToString();
                }
                foreach (RequestOtherItemAddModel item in payload.item_list) {
                    item.request_type_code = item.request_type_code.Replace("SAVE0", "").Replace("SAVE", "");
                }
                var entity = mapper.Map<RequestOtherProcess>(payload);
                repo.Add(entity);
                uow.SaveChanges();
                return mapper.Map<RequestOtherProcessView>(entity);
            }
        }

        [LogAopInterceptor]
        public virtual RequestOtherProcessView RequestOtherSave(RequestOtherProcessAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                using (var request_other_item_uow = uowProvider.CreateUnitOfWork()) {
                    var repo = uow.GetRepository<RequestOtherProcess>();
                    var request_other_item_repo = request_other_item_uow.GetRepository<vRequestOtherItem>();
                    var item_sub_index = new Dictionary<string, int>();

                    if (payload.source_code == "EFORM") {
                    } else if (payload.commercial_affairs_province_code != "FLOOR3") {
                        payload.source_code = RequestSourceCodeConstant.COMMERCIAL_AFFAIRS_PROVINCE.ToString();
                    } else {
                        payload.source_code = RequestSourceCodeConstant.CENTRAL.ToString();
                    }

                    payload.request_01_item_list = payload.request_01_item_list.Where(r => !string.IsNullOrEmpty(r.request_number)).ToList();

                    var request_number_list = payload.request_01_item_list.Select(r => r.request_number).ToList();
                    foreach (var item in payload.item_list.Where(r => r.request_type_code == "20" || r.request_type_code == "21")) {
                        item.RequestOtherItemSub = item.RequestOtherItemSub.Where(r => request_number_list.IndexOf(r.request_number) >= 0).ToList();

                        var item_sub_no_index_list = item.RequestOtherItemSub.Where(r => string.IsNullOrEmpty(r.request_index)).ToList();
                        foreach (var item_sub in item_sub_no_index_list) {
                            //    var found = item.RequestOtherItemSub.Where(r => r.request_number == item_sub.request_number && !string.IsNullOrEmpty(r.request_index)).FirstOrDefault();
                            //    if (found != null) {
                            item_sub.request_index = HelpService.GetRequestIndex(uowProvider, item.request_type_code);
                            //    } else {
                            //        if (!item_sub_index.ContainsKey(item_sub.request_number + "_" + item.request_type_code)) {
                            //            var request_index_list = request_other_item_repo.Query(r => r.request_number == item_sub.request_number && r.request_type_code == item.request_type_code);
                            //            var request_index_max = request_index_list.Count() > 0 ? request_index_list.Max(r => Convert.ToInt32(r.request_index)) : 0;
                            //            item_sub_index.Add(item_sub.request_number + "_" + item.request_type_code, request_index_max);
                            //        }

                            //        item_sub.request_index = (++item_sub_index[item_sub.request_number + "_" + item.request_type_code]).ToString();
                            //    }
                        }
                    }

                    //foreach (RequestOtherRequest01ItemAddModel request01item in payload.request_01_item_list) {
                    //    request01item.status_code = RequestStatusCodeConstant.SAVE.ToString();

                    //    //foreach (RequestOtherItemAddModel item in payload.item_list) {
                    //    //    if (item.RequestOtherItemSub == null) item.RequestOtherItemSub = new List<RequestOtherItemSubAddModel>();
                    //    //    if (item.RequestOtherItemSub.Where(r => r.request_number == request01item.request_number).Count() == 0) {
                    //    //        item.RequestOtherItemSub.Add(new RequestOtherItemSubAddModel() {
                    //    //            request_number = request01item.request_number,
                    //    //            request_index = "1",
                    //    //            total_price = item.total_price,
                    //    //        });
                    //    //    }
                    //    //}


                    var entity = mapper.Map<RequestOtherProcess>(payload);
                    //if (!payload.id.HasValue) repo.Add(entity);
                    //else repo.Update(entity);
                    repo.Update(entity);
                    uow.SaveChanges();

                    RequestOtherPrint(entity.id);


                    return RequestOtherLoad(entity.id);
                }
            }
        }

        [LogAopInterceptor]
        public virtual RequestOtherProcessView RequestOtherLoad(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestOtherProcess>();
                var entity = repo.Get(id);
                var view = mapper.Map<RequestOtherProcessView>(entity);

                var user_repo = uow.GetRepository<vUM_User>();
                var user = user_repo.Get(view.updated_by.HasValue && view.updated_by.Value > 0 ? view.updated_by.Value : view.created_by);
                if (user != null) {
                    view.created_by_name = user.name;
                }
                //view.created_by_name

                var save_repo = uow.GetRepository<vSave010>();
                foreach (var request_01_item in view.request_01_item_list) {
                    var save = save_repo.Query(r => r.request_number == request_01_item.request_number).FirstOrDefault();
                    if (save != null) {
                        request_01_item.save_id = save.id;
                        request_01_item.name = save.name;
                        request_01_item.registration_number = save.registration_number;
                        request_01_item.trademark_expired_start_date = save.trademark_expired_start_date;
                        request_01_item.trademark_expired_date = save.trademark_expired_date;
                        request_01_item.trademark_expired_end_date = save.trademark_expired_end_date;

                        var product_repo = uow.GetRepository<Save010Product>();
                        var product = product_repo.Query(r => r.save_id == save.id);
                        request_01_item.product_list = mapper.Map<List<SaveProductView>>(product);
                    }
                }

                view.item_eform_list = new List<vEForm_eForm_SaveOtherView>();
                foreach (var item in view.item_list) {
                    foreach (var item_sub_form in item.RequestOtherItemSub) {
                        var save = save_repo.Query(r => r.request_number == item_sub_form.request_number).FirstOrDefault();

                        var item_view = mapper.Map<vEForm_eForm_SaveOtherView>(item);
                        item_view.request_other_item_sub_id = item_sub_form.id;
                        item_view.request_other_request_01_item_id = view.request_01_item_list.Where(r => r.request_number == save.request_number).FirstOrDefault().id;
                        item_view.eform_id = item_sub_form.eform_id.HasValue ? item_sub_form.eform_id.Value : 0;
                        item_view.registration_number = save.registration_number;
                        item_view.reference_number = view.request_01_item_list.Where(r => r.request_number == save.request_number).FirstOrDefault().reference_number; ;
                        item_view.request_index = item_sub_form.request_index;
                        item_view.name = save.name;
                        item_view.request_number = save.request_number;
                        item_view.request_type_name = item.request_type_name;
                        item_view.save_total_price = save.total_price;
                        item_view.trademark_expired_start_date = save.trademark_expired_start_date;
                        item_view.trademark_expired_date = save.trademark_expired_date;
                        item_view.trademark_expired_end_date = save.trademark_expired_end_date;

                        item_view.remark = "";
                        if (save.trademark_expired_date.HasValue) {
                            item_view.remark += "วันสิ้นอายุ: " + save.trademark_expired_date.Value.ToString("dd/MM/yyyy");
                        }

                        view.item_eform_list.Add(item_view);
                    }
                }
                return view;
            }
        }

        [LogAopInterceptor]
        public virtual RequestOtherItemSubView RequestOtherItemSubDelete(RequestOtherItemSubAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestOtherItemSub>();
                if (payload.ids != null) {
                    foreach (long id in payload.ids) {
                        var entity = repo.Get(id);
                        entity.status_code = RequestStatusCodeConstant.DELETE.ToString();
                        entity.cancel_reason = payload.cancel_reason;
                        repo.Update(entity);
                    }
                }
                uow.SaveChanges();
                return mapper.Map<RequestOtherItemSubView>(null);
            }
        }
        [LogAopInterceptor]
        public virtual RequestOtherProcessView RequestOtherPrint(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<RequestOtherProcess>();
                RequestOtherProcess entity = repo.Get(id);

                foreach (RequestOtherRequest01Item request in entity.RequestOtherRequest01Item) {
                    request.reference_number = HelpService.GetReferenceNumber(uowProvider);
                    request.status_code = RequestStatusCodeConstant.RECEIPT.ToString();
                }
                //foreach (RequestOtherItem item in entity.RequestOtherItem) {
                //    if (item == null) {

                //    }
                //}

                //foreach (RequestOtherRequest01Item item in entity.RequestOtherRequest01Item) {
                ReceiptService receipt_service = new ReceiptService(configuration, uowProvider, mapper);
                List<Receipt> receipt_list = receipt_service.Add(entity);
                //}

                foreach (var receipt in receipt_list) {
                    if (entity.commercial_affairs_province_code != "FLOOR3" && entity.commercial_affairs_province_code != "EFORM") {
                        //var receipt_model = mapper.Map<ReceiptAddModel>(receipt);

                        //foreach (var item in receipt.ReceiptItem) {
                        //    item.book_index = payload.item_list.Where(r => r.request_number == item.request_number).FirstOrDefault().book_index;
                        //    item.page_index = payload.item_list.Where(r => r.request_number == item.request_number).FirstOrDefault().page_index;
                        //}

                        receipt_service.Save(mapper.Map<ReceiptAddModel>(receipt), false);
                    } else {
                        receipt_service.Paid(receipt.id, true);
                    }
                }

                repo.Update(entity);
                uow.SaveChanges();
                return mapper.Map<RequestOtherProcessView>(entity); ;
            }
        }

        //[LogAopInterceptor]
        //public virtual RequestEFormView RequestEformLoad(long id) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<Request01Process>();
        //        //var request01_item_repo = uow.GetRepository<vEForm_Request01Item>();
        //        //var request01_item_sub_repo = uow.GetRepository<RequestEForm01ItemSub>();

        //        var entity = repo.Get(id);
        //        var view = mapper.Map<RequestEFormView>(entity);
        //        //view.request01_item_list = mapper.Map<List<Request01ItemView>>(entity.Request01Item);
        //        //var request01_item_list = request01_item_repo.Query(r => r.request01_id == entity.id).ToList();
        //        //view.request01_item_list = mapper.Map<List<vEForm_Request01ItemView>>(request01_item_list);
        //        //foreach (var request01_item in entity.Request01Item) {
        //        //    view.request01_item_list. = mapper.Map<List<Request01ItemSubView>>(request01_item.Request01ItemSub);
        //        //}

        //        return view;
        //    }
        //}

        [LogAopInterceptor]
        public virtual Request01ProcessView RequestEformSend(Request01ProcessAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var receipt_service = new ReceiptService(configuration, uowProvider, mapper);
                var repo = uow.GetRepository<Request01Process>();
                //var request01_item_repo = uow.GetRepository<Request01Item>();
                var request01_item_sub_repo = uow.GetRepository<Request01ItemSub>();

                var is_no_reference_number = payload.item_list.Where(r => string.IsNullOrEmpty(r.reference_number)).Count() > 0;
                var reference_number = is_no_reference_number ? HelpService.GetReferenceNumber(uowProvider) : payload.item_list.FirstOrDefault().reference_number;

                foreach (var item in payload.item_list) {
                    if (is_no_reference_number) {
                        item.reference_number = reference_number;
                    }
                    if (string.IsNullOrEmpty(item.request_number)) {
                        item.request_number = HelpService.GetRequestNumber(uowProvider, item.item_type_code);
                    }
                    item.status_code = RequestStatusCodeConstant.SAVE.ToString();

                    if (!string.IsNullOrEmpty(item.cancel_reason)) {
                        item.status_code = RequestStatusCodeConstant.DELETE.ToString();
                    }
                }
                payload.item_count = payload.item_list.Count();
                payload.total_price = payload.item_list.Where(r => r.status_code != RequestStatusCodeConstant.DELETE.ToString()).Sum(r => r.total_price + (r.size_over_price.HasValue ? r.size_over_price : 0));
                payload.source_code = RequestSourceCodeConstant.EFORM.ToString();

                var entity = mapper.Map<Request01Process>(payload);
                repo.Update(entity);
                uow.SaveChanges();

                var eform_save010_product_repo = uow.GetRepository<vEForm_eForm_Save010_Product>();

                ////var view = mapper.Map<RequestEFormView>(entity);
                ////view.request01_item_list = new List<RequestEForm01ItemView>();

                foreach (var request01_item in entity.Request01Item) {
                    var eform_save010_product_list = eform_save010_product_repo.Query(r => r.eform_id == request01_item.eform_id).ToList();

                    if (request01_item_sub_repo.Query(r => r.request01_item_id == request01_item.id).Count() == 0) {
                        foreach (var eform_save010_product in eform_save010_product_list) {
                            request01_item.Request01ItemSub.Add(new Request01ItemSub() {
                                request01_item_id = request01_item.id,
                                item_sub_type_1_code = eform_save010_product.request_item_sub_type_1_code,
                                product_count = eform_save010_product.product_count,
                                total_price = eform_save010_product.total_price,
                            });
                        }
                    }
                }
                uow.SaveChanges();

                var view = Request01Load(entity.id);
                if (is_no_reference_number) {
                    receipt_service.Add(entity, reference_number);
                }
                return view;
            }
        }

        [LogAopInterceptor]
        public virtual RequestOtherProcessView RequestOtherEformSend(RequestOtherProcessAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var receipt_service = new ReceiptService(configuration, uowProvider, mapper);
                //var repo = uow.GetRepository<RequestOtherProcess>();
                ////var request01_item_repo = uow.GetRepository<Request01Item>();
                //var request01_item_sub_repo = uow.GetRepository<Request01ItemSub>();

                var is_no_reference_number = payload.item_eform_list.Where(r => string.IsNullOrEmpty(r.reference_number)).Count() > 0;
                //var reference_number = is_no_reference_number ? HelpService.GetReferenceNumber(uowProvider) : payload.item_list.FirstOrDefault().reference_number;

                payload.item_list = new List<RequestOtherItemAddModel>();
                foreach (var request_type_code in payload.item_eform_list.Select(r => r.request_type_code).Distinct()) {
                    var item_eform = payload.item_eform_list.Where(r => r.request_type_code == request_type_code).FirstOrDefault();
                    var item = mapper.Map<RequestOtherItemAddModel>(item_eform);
                    item.RequestOtherItemSub = new List<RequestOtherItemSubAddModel>();
                    payload.item_list.Add(item);
                }

                payload.request_01_item_list = new List<RequestOtherRequest01ItemAddModel>();
                foreach (var request_number in payload.item_eform_list.Select(r => r.request_number).Distinct()) {
                    var item_eform = payload.item_eform_list.Where(r => r.request_number == request_number).FirstOrDefault();
                    var item = mapper.Map<RequestOtherRequest01ItemAddModel>(item_eform);
                    item.id = item_eform.request_other_request_01_item_id;
                    //item.RequestOtherItemSub = new List<RequestOtherItemSubAddModel>();
                    payload.request_01_item_list.Add(item);
                }

                if (is_no_reference_number) {
                    foreach (var item in payload.request_01_item_list) {
                        item.reference_number = HelpService.GetReferenceNumber(uowProvider);
                    }
                }


                foreach (var item_eform in payload.item_eform_list) {
                    var item = payload.item_list.Where(r => r.request_type_code == item_eform.request_type_code).FirstOrDefault();
                    var item_sub = mapper.Map<RequestOtherItemSubAddModel>(item_eform);
                    item_sub.id = item_eform.request_other_item_sub_id;
                    item.RequestOtherItemSub.Add(item_sub);
                }
                //payload.item_count = payload.item_list.Count();
                payload.total_price = payload.item_list.Where(r => !r.is_deleted).Sum(r => r.total_price + (r.fine.HasValue ? r.fine : 0));
                payload.source_code = RequestSourceCodeConstant.EFORM.ToString();
                payload.commercial_affairs_province_code = "EFORM";

                return RequestOtherSave(payload);
                //var entity = mapper.Map<RequestOtherProcess>(payload);
                //repo.Update(entity);
                //uow.SaveChanges();

                ////var eform_save010_product_repo = uow.GetRepository<vEForm_eForm_Save010_Product>();

                ////////var view = mapper.Map<RequestEFormView>(entity);
                ////////view.request01_item_list = new List<RequestEForm01ItemView>();

                ////foreach (var request01_item in entity.Request01Item) {
                ////    var eform_save010_product_list = eform_save010_product_repo.Query(r => r.eform_id == request01_item.eform_id).ToList();

                ////    if (request01_item_sub_repo.Query(r => r.request01_item_id == request01_item.id).Count() == 0) {
                ////        foreach (var eform_save010_product in eform_save010_product_list) {
                ////            request01_item.Request01ItemSub.Add(new Request01ItemSub() {
                ////                request01_item_id = request01_item.id,
                ////                item_sub_type_1_code = eform_save010_product.request_item_sub_type_1_code,
                ////                product_count = eform_save010_product.product_count,
                ////                total_price = eform_save010_product.total_price,
                ////            });
                ////        }
                ////    }
                ////}
                ////uow.SaveChanges();

                //var view = RequestOtherLoad(entity.id);
                ////if (is_no_reference_number) {
                ////    receipt_service.Add(entity, reference_number);
                ////}
                //return view;
            }
        }

        [LogAopInterceptor]
        public virtual RequestOtherItemView RequestOtherItemLoad(long request_id, string request_number) {
            throw new NotImplementedException();
        }

        //[LogAopInterceptor]
        //public virtual List<vRequestOtherItemView> RequestOtherItemSubList(RequestOtherItemAddModel payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<vRequestOtherItem>();
        //        List<vRequestOtherItem> item_list = repo.Query(r =>
        //            (string.IsNullOrEmpty(payload.created_by_name) || r.created_by_name.IndexOf(payload.created_by_name) >= 0) &&
        //            (!payload.request_date.HasValue || r.request_date.Value.Date == payload.request_date.Value.Date) &&
        //            (string.IsNullOrEmpty(payload.reference_number) || r.reference_number == payload.reference_number) &&
        //            (string.IsNullOrEmpty(payload.request_number) || r.request_number == payload.request_number) &&
        //            (string.IsNullOrEmpty(payload.receipt_status_code) || payload.receipt_status_code == ReceiptStatusCodeConstant.ALL.ToString() || r.receipt_status_code == payload.receipt_status_code)
        //        ).OrderByDescending(r => r.created_date).Take(10).ToList();
        //        return mapper.Map<List<vRequestOtherItemView>>(item_list);
        //    }
        //}
    }
}
