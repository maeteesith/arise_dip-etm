﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;
using DIP.TM.Services.Interfaces;
using DIP.TM.Uows.DataAccess;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Services.Requests
{
    /// <summary>
    /// 
    /// </summary>
    public class RequestMasterService: IRequestMasterService
    {
        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestMasterService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
        }

        public List<Request01ListEvidenceTypeView> GetRequest01ListEvidenceType()
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var masterRepo = uow.GetRepository<RM_RequestEvidenceType>();
                var results = masterRepo.GetAllActive(x => x.OrderBy(y => y.id));
                return mapper.Map<List<Request01ListEvidenceTypeView>>(results);
            }
        }

        public List<PriceMasterView> GetPriceRequestMaster()
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var masterRepo = uow.GetRepository<PriceMaster>();
                var results = masterRepo.Query(x => x.price_type_id == 1).ToList();
                return mapper.Map<List<PriceMasterView>>(results);
            }
        }

        public List<RequestSoundTypeView> GetRequestSoundMaster()
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var masterRepo = uow.GetRepository<RM_RequestSoundType>();
                var results = masterRepo.GetAll();
                return mapper.Map<List<RequestSoundTypeView>>(results);
            }
        }
    }
}
