﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Utils.Extensions;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIP.TM.Services.Requests
{
    public class RequestReceiptChangeService
    {
        protected long? userId;
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        public RequestReceiptChangeService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            userId = HttpContext.Current.User.UserId();
        }

        [LogAopInterceptor]
        public virtual Request01ReceiptChangeView Request01ReceiptChangeAdd(Request01ProcessAddModel payload)
        {
            using (var uow = _uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Request01Process>();
                var request01Process = repo.Query(i => i.id == payload.id).FirstOrDefault();
                if (request01Process == null)
                    return null;

                var request01ChangeViewRepo = uow.GetRepository<vRequest01ReceiptChange>();
                var existRequest01Change = request01ChangeViewRepo.Query(i => !i.is_deleted
                && i.request01_receipt_change_code != Request01ReceiptChangeCodeType.APPROVED.ToString()
                && i.request01_receipt_change_code != Request01ReceiptChangeCodeType.RECEIPT_APPROVE_REJECT.ToString()
                && i.request01_receipt_change_code != Request01ReceiptChangeCodeType.REQUEST_APPROVE_REJECT.ToString()
                && i.request_id == request01Process.id).FirstOrDefault();

                if (existRequest01Change != null && (existRequest01Change.request01_receipt_change_code == Request01ReceiptChangeCodeType.RECEIPT_APPROVE_WAIT.ToString() || existRequest01Change.request01_receipt_change_code == Request01ReceiptChangeCodeType.REQUEST_APPROVE_WAIT.ToString()))
                    return _mapper.Map<Request01ReceiptChangeView>(existRequest01Change);

                payload.item_list.ForEach(item =>
                {
                    var itemCompare = request01Process.Request01Item.FirstOrDefault(f => f.request_number == item.request_number);
                    if (itemCompare != null && itemCompare.id != item.id)
                    {
                        item.id = itemCompare.id;
                        foreach (var itemSub in item.item_sub_list)
                        {
                            var itemSubCompare = itemCompare.Request01ItemSub.FirstOrDefault(f => f.item_sub_type_1_code == itemSub.item_sub_type_1_code);
                            if (itemSubCompare != null && itemSub.id != itemSubCompare.id)
                                itemSub.id = itemSubCompare.id;
                        }
                    }
                });

                foreach (var item in request01Process.Request01Item)
                {
                    var itemCompare = payload.item_list.FirstOrDefault(f => f.request_number == item.request_number);
                    if (itemCompare == null)
                    {
                        var updateItem = _mapper.Map<Request01ItemAddModel>(item);
                        updateItem.is_deleted = true;
                        payload.item_list.Add(updateItem);
                    }

                    foreach (var itemSub in item.Request01ItemSub)
                    {
                        var itemSubCompare = itemCompare.item_sub_list.FirstOrDefault(f => f.item_sub_type_1_code == itemSub.item_sub_type_1_code);
                        if (itemSubCompare == null)
                        {
                            var updateItemSub = _mapper.Map<Request01ItemSubAddModel>(itemSub);
                            updateItemSub.is_deleted = true;
                            payload.item_list.FirstOrDefault(f => f.request_number == item.request_number).item_sub_list.Add(updateItemSub);
                        }
                    }
                }

                var request01ProcessChange = _mapper.Map<Request01ProcessChange>(payload);
                var request01ChangeRepo = uow.GetRepository<Request01ReceiptChange>();
                var newRequestChange = new Request01ReceiptChange
                {
                    created_by = userId ?? 0,
                    created_date = DateTime.Now,
                    is_deleted = false,
                    request_id = request01Process.id,
                    Request01ProcessChange = new List<Request01ProcessChange>() { request01ProcessChange },
                    request01_receipt_change_code = Request01ReceiptChangeCodeType.REQUEST_APPROVE_WAIT.ToString(),
                };

                request01ChangeRepo.Update(newRequestChange);
                uow.SaveChanges();

                var newRequestChangeView = _mapper.Map<Request01ReceiptChangeView>(newRequestChange);
                newRequestChangeView.before = _mapper.Map<Request01ProcessView>(request01Process);
                newRequestChangeView.after = _mapper.Map<Request01ProcessView>(request01ProcessChange);
                return newRequestChangeView;
            }
        }

        public Request01ReceiptChangeView Request01ReceiptChangeReceiptReject(int id)
        {
            using (var uow = _uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Request01ReceiptChange>();
                var request01Change = repo.Query(i => !i.is_deleted
                && i.request01_receipt_change_code == Request01ReceiptChangeCodeType.RECEIPT_APPROVE_WAIT.ToString()
                && i.request_id == id).FirstOrDefault();
                if (request01Change == null)
                    return null;

                if (request01Change.request_id is long request01Id)
                {
                    request01Change.request01_receipt_change_code = Request01ReceiptChangeCodeType.RECEIPT_APPROVE_REJECT.ToString();
                    repo.Update(request01Change);
                    uow.SaveChanges();

                    var request01Process = uow.GetRepository<Request01Process>().Get(request01Id);
                    var request01ChangeView = _mapper.Map<Request01ReceiptChangeView>(request01Change);
                    request01ChangeView.before = _mapper.Map<Request01ProcessView>(request01Process);
                    request01ChangeView.after = _mapper.Map<Request01ProcessView>(request01Change.Request01ProcessChange.FirstOrDefault());
                    return request01ChangeView;
                }
                else
                {
                    throw new NullReferenceException($"Column Name {nameof(request01Change.request_id)} is null");
                }
            }
        }

        public Request01ReceiptChangeView Request01ReceiptChangeRequestReject(int id)
        {
            using (var uow = _uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Request01ReceiptChange>();
                var request01Change = repo.Query(i => !i.is_deleted
                && i.request01_receipt_change_code == Request01ReceiptChangeCodeType.REQUEST_APPROVE_WAIT.ToString()
                && i.request_id == id).FirstOrDefault();
                if (request01Change == null)
                    return null;

                if (request01Change.request_id is long request01Id)
                {
                    request01Change.request01_receipt_change_code = Request01ReceiptChangeCodeType.REQUEST_APPROVE_REJECT.ToString();
                    repo.Update(request01Change);
                    uow.SaveChanges();

                    var request01Process = uow.GetRepository<Request01Process>().Get(request01Id);
                    var request01ChangeView = _mapper.Map<Request01ReceiptChangeView>(request01Change);
                    request01ChangeView.before = _mapper.Map<Request01ProcessView>(request01Process);
                    request01ChangeView.after = _mapper.Map<Request01ProcessView>(request01Change.Request01ProcessChange.FirstOrDefault());
                    return request01ChangeView;
                }
                else
                {
                    throw new NullReferenceException($"Column Name {nameof(request01Change.request_id)} is null");
                }
            }
        }

        public Request01ReceiptChangeView Request01ReceiptChangeLoad(long id)
        {
            using (var uow = _uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Request01Process>();
                var request01Process = repo.Query(i => i.id == id).FirstOrDefault();
                if (request01Process == null)
                    return null;

                var request01ChangeRepo = uow.GetRepository<Request01ReceiptChange>();
                var request01Change = request01ChangeRepo.Query(i => !i.is_deleted
                && i.request01_receipt_change_code != Request01ReceiptChangeCodeType.APPROVED.ToString()
                && i.request01_receipt_change_code != Request01ReceiptChangeCodeType.RECEIPT_APPROVE_REJECT.ToString()
                && i.request01_receipt_change_code != Request01ReceiptChangeCodeType.REQUEST_APPROVE_REJECT.ToString()
                && i.request_id == request01Process.id).FirstOrDefault();
                if (request01Change == null)
                    return null;

                var request01ChangeView = _mapper.Map<Request01ReceiptChangeView>(request01Change);
                request01ChangeView.before = _mapper.Map<Request01ProcessView>(request01Process);
                request01ChangeView.after = _mapper.Map<Request01ProcessView>(request01Change.Request01ProcessChange.FirstOrDefault());
                return request01ChangeView;
            }
        }

        [LogAopInterceptor]
        public virtual Request01ReceiptChangeView Request01ReceiptChangeRequestApprove(long id)
        {
            using (var uow = _uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Request01ReceiptChange>();
                var request01Change = repo.Query(i => !i.is_deleted
                && i.request01_receipt_change_code == Request01ReceiptChangeCodeType.REQUEST_APPROVE_WAIT.ToString()
                && i.request_id == id).FirstOrDefault();
                if (request01Change == null)
                    return null;

                if (request01Change.request_id is long request01Id)
                {
                    request01Change.request01_receipt_change_code = Request01ReceiptChangeCodeType.RECEIPT_APPROVE_WAIT.ToString();
                    request01Change.request_approve_by = userId;
                    request01Change.request_approve_date = DateTime.Now;
                    repo.Update(request01Change);
                    uow.SaveChanges();

                    var request01Process = uow.GetRepository<Request01Process>().Get(request01Id);
                    var request01ChangeView = _mapper.Map<Request01ReceiptChangeView>(request01Change);
                    request01ChangeView.before = _mapper.Map<Request01ProcessView>(request01Process);
                    request01ChangeView.after = _mapper.Map<Request01ProcessView>(request01Change.Request01ProcessChange.FirstOrDefault());
                    return request01ChangeView;
                }
                else
                {
                    throw new NullReferenceException($"Column Name {nameof(request01Change.request_id)} is null");
                }
            }
        }

        [LogAopInterceptor]
        public virtual Request01ReceiptChangeView Request01ReceiptChangeReceiptApprove(long id)
        {
            using (var uow = _uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<Request01ReceiptChange>();
                var request01Change = repo.Query(i => !i.is_deleted
                && i.request01_receipt_change_code == Request01ReceiptChangeCodeType.REQUEST_APPROVE_WAIT.ToString()
                && i.request_id == id).FirstOrDefault();
                if (request01Change == null)
                    return null;

                if (request01Change.request_id is long request01Id)
                {
                    var request01ChangeView = _mapper.Map<Request01ReceiptChangeView>(request01Change);
                    var request01ProcessRepo = uow.GetRepository<Request01Process>();
                    var request01Process = request01ProcessRepo.Get(request01Id);
                    request01ChangeView.before = _mapper.Map<Request01ProcessView>(request01Process);

                    request01Process = _mapper.Map<Request01Process>(request01Change.Request01ProcessChange.FirstOrDefault());
                    request01Process.updated_by = userId;
                    request01Process.updated_date = DateTime.Now;
                    request01ProcessRepo.Update(request01Process);

                    request01Change.request01_receipt_change_code = Request01ReceiptChangeCodeType.APPROVED.ToString();
                    request01Change.receipt_approve_by = userId;
                    request01Change.receipt_approve_date = DateTime.Now;
                    repo.Update(request01Change);

                    uow.SaveChanges();

                    request01ChangeView.after = _mapper.Map<Request01ProcessView>(request01Process);
                    return request01ChangeView;
                }
                else
                {
                    throw new NullReferenceException($"Column Name {nameof(request01Change.request_id)} is null");
                }
            }
        }
    }
}
