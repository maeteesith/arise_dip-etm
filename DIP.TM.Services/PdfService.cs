using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models.Email;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Email;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DocumentFormat.OpenXml.Office.CustomUI;
using DocumentFormat.OpenXml.Spreadsheet;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Color = System.Drawing.Color;

namespace DIP.TM.Services.Pdf
{
    public class PdfService<P, D, V> where P : EFormModel where D : eFormEntityBase where V : EFormView
    {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        private readonly IHostingEnvironment env;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public PdfService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, IHostingEnvironment env)
        {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;
            this.env = env;
        }

        [LogAopInterceptor]
        public virtual V ViewPDF(object payload_json)
        {
            V payload = JsonConvert.DeserializeObject<V>(Convert.ToString(payload_json));
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                V view = GetAddressNavigation(payload);

                if (view == null)
                {
                    if (typeof(V) == typeof(eForm_Save010View))
                    {
                        view = mapper.Map<V>(new eForm_Save010View());
                    }
                    else if (typeof(V) == typeof(eForm_Save020View))
                    {
                        view = mapper.Map<V>(new eForm_Save020View());
                    }
                    else if (typeof(V) == typeof(eForm_Save021View))
                    {
                        view = mapper.Map<V>(new eForm_Save021View());
                    }
                    else if (typeof(V) == typeof(eForm_Save030View))
                    {
                        view = mapper.Map<V>(new eForm_Save030View());
                    }
                    else if (typeof(V) == typeof(eForm_Save040View))
                    {
                        view = mapper.Map<V>(new eForm_Save040View());
                    }
                    else if (typeof(V) == typeof(eForm_Save050View))
                    {
                        view = mapper.Map<V>(new eForm_Save050View());
                    }
                    else if (typeof(V) == typeof(eForm_Save060View))
                    {
                        view = mapper.Map<V>(new eForm_Save060View());
                    }
                    else if (typeof(V) == typeof(eForm_Save070View))
                    {
                        view = mapper.Map<V>(new eForm_Save070View());
                    }
                    else if (typeof(V) == typeof(eForm_Save080View))
                    {
                        view = mapper.Map<V>(new eForm_Save080View());
                    }
                    else if (typeof(V) == typeof(eForm_Save120View))
                    {
                        view = mapper.Map<V>(new eForm_Save120View());
                    }
                    else if (typeof(V) == typeof(eForm_Save140View))
                    {
                        view = mapper.Map<V>(new eForm_Save140View());
                    }
                    else if (typeof(V) == typeof(eForm_Save150View))
                    {
                        view = mapper.Map<V>(new eForm_Save150View());
                    }
                    else if (typeof(V) == typeof(eForm_Save190View))
                    {
                        view = mapper.Map<V>(new eForm_Save190View());
                    }
                    else if (typeof(V) == typeof(eForm_Save200View))
                    {
                        view = mapper.Map<V>(new eForm_Save200View());
                    }

                    view.is_search_success = false;
                    view.alert_msg = "null";
                }

                return view;
            }
        }

        public V GetAddressNavigation(V view)
        {
            using (var uow = uowProvider.CreateUnitOfWork())
            {
                var card_repo = uow.GetRepository<RM_AddressEFormCardType>();
                var career_repo = uow.GetRepository<RM_AddressCareer>();
                var nation_repo = uow.GetRepository<RM_AddressNationality>();
                var country_repo = uow.GetRepository<RM_AddressCountry>();
                var district_repo = uow.GetRepository<RM_AddressDistrict>();
                var subdistrict_repo = uow.GetRepository<RM_AddressSubDistrict>();
                var province_repo = uow.GetRepository<RM_AddressProvince>();
                var representative_condition_repo = uow.GetRepository<RM_AddressRepresentativeConditionTypeCode>();
                var translate_repo = uow.GetRepository<RM_TranslationLanguage>();
                var save190_request_repo = uow.GetRepository<RM_Save190RequestTypeCode>();
                var file_guest_repo = uow.GetRepository<FileGuest>();


                var s010_repo = uow.GetRepository<Save010>();
                var s010_entity = s010_repo.Query(r => r.request_number == view.request_number).FirstOrDefault();
                var s010_view = mapper.Map<Save010ProcessView>(s010_entity);
                long public_round_id = 0;

                if (s010_entity != null)
                {
                    public_round_id = (s010_entity.public_round_id.HasValue) ? s010_entity.public_round_id.Value : 0;
                }

                var public_repo = uow.GetRepository<PublicRound>();
                var public_entity = public_repo.Query(r => r.id == public_round_id).FirstOrDefault();
                var public_view = mapper.Map<PublicRoundView>(public_entity);

                if (public_entity != null)
                {
                    public_view.public_round_item = public_view.public_round_item.Where(r => r.save_id == s010_entity.id).ToList();

                    view.public_round = public_view;
                }

                if (typeof(V) == typeof(eForm_Save010View))
                {
                    var v010 = mapper.Map<eForm_Save010View>(view);

                    var s010_sound_img_entity = file_guest_repo.Query(r => r.id == v010.sound_jpg_file_id).FirstOrDefault();
                    v010.sound_jpg_file = mapper.Map<FileView>(s010_sound_img_entity);

                    var s010_img_2d_entity = file_guest_repo.Query(r => r.id == v010.img_file_2d_id).FirstOrDefault();
                    v010.img_2D_file = mapper.Map<FileView>(s010_img_2d_entity);

                    var s010_img_3d_entity_1 = file_guest_repo.Query(r => r.id == v010.img_file_3d_id_1).FirstOrDefault();
                    v010.img_3D_file_1 = mapper.Map<FileView>(s010_img_3d_entity_1);

                    var s010_img_3d_entity_2 = file_guest_repo.Query(r => r.id == v010.img_file_3d_id_2).FirstOrDefault();
                    v010.img_3D_file_2 = mapper.Map<FileView>(s010_img_3d_entity_2);

                    var s010_img_3d_entity_3 = file_guest_repo.Query(r => r.id == v010.img_file_3d_id_3).FirstOrDefault();
                    v010.img_3D_file_3 = mapper.Map<FileView>(s010_img_3d_entity_3);

                    var s010_img_3d_entity_4 = file_guest_repo.Query(r => r.id == v010.img_file_3d_id_4).FirstOrDefault();
                    v010.img_3D_file_4 = mapper.Map<FileView>(s010_img_3d_entity_4);

                    var s010_img_3d_entity_5 = file_guest_repo.Query(r => r.id == v010.img_file_3d_id_5).FirstOrDefault();
                    v010.img_3D_file_5 = mapper.Map<FileView>(s010_img_3d_entity_5);

                    var s010_img_3d_entity_6 = file_guest_repo.Query(r => r.id == v010.img_file_3d_id_6).FirstOrDefault();
                    v010.img_3D_file_6 = mapper.Map<FileView>(s010_img_3d_entity_6);

                    string img_3d_path_1 = (v010.img_3D_file_1 != null) ? v010.img_3D_file_1.physical_path : "";
                    string img_3d_path_2 = (v010.img_3D_file_2 != null) ? v010.img_3D_file_2.physical_path : "";
                    string img_3d_path_3 = (v010.img_3D_file_3 != null) ? v010.img_3D_file_3.physical_path : "";
                    string img_3d_path_4 = (v010.img_3D_file_4 != null) ? v010.img_3D_file_4.physical_path : "";
                    string img_3d_path_5 = (v010.img_3D_file_5 != null) ? v010.img_3D_file_5.physical_path : "";
                    string img_3d_path_6 = (v010.img_3D_file_6 != null) ? v010.img_3D_file_6.physical_path : "";

                    string contentRootPath = env.ContentRootPath;
                    string webRootPath = env.WebRootPath;
                    if (img_3d_path_1 != "")
                    {
                        img_3d_path_1 = Path.Combine(webRootPath, img_3d_path_1);
                    }
                    if (img_3d_path_2 != "")
                    {
                        img_3d_path_2 = Path.Combine(webRootPath, img_3d_path_2);
                    }
                    if (img_3d_path_3 != "")
                    {
                        img_3d_path_3 = Path.Combine(webRootPath, img_3d_path_3);
                    }
                    if (img_3d_path_4 != "")
                    {
                        img_3d_path_4 = Path.Combine(webRootPath, img_3d_path_4);
                    }
                    if (img_3d_path_5 != "")
                    {
                        img_3d_path_5 = Path.Combine(webRootPath, img_3d_path_5);
                    }
                    if (img_3d_path_6 != "")
                    {
                        img_3d_path_6 = Path.Combine(webRootPath, img_3d_path_6);
                    }

                    v010.img_file_3d_path = Merge3DImage(img_3d_path_1, img_3d_path_2, img_3d_path_3, img_3d_path_4, img_3d_path_5, img_3d_path_6);
                    v010.img_file_3d_type = "jpeg";

                    var s010_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v010.save010_representative_condition_type_code).FirstOrDefault();
                    v010.save010_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s010_representative_condtion_entity);

                    if (v010.checking_similar_translate_list != null && v010.checking_similar_translate_list.Count() > 0)
                    {
                        foreach (var item in v010.checking_similar_translate_list)
                        {
                            var translate_entity = translate_repo.Query(r => r.code == item.translation_language_code).FirstOrDefault();
                            item.translation_language_codeNavigation = mapper.Map<ReferenceMasterView>(translate_entity);
                        }
                    }

                    if (v010.people_list != null && v010.people_list.Count() > 0)
                    {
                        foreach (var item in v010.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v010.representative_list != null && v010.representative_list.Count() > 0)
                    {
                        foreach (var item in v010.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v010.contact_address_list != null && v010.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v010.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v010.joiner_list != null && v010.joiner_list.Count() > 0)
                    {
                        foreach (var item in v010.joiner_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v010.representative_kor_18_list != null && v010.representative_kor_18_list.Count() > 0)
                    {
                        foreach (var item in v010.representative_kor_18_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }


                    if (v010.kor10_list != null && v010.kor10_list.Count() > 0)
                    {
                        foreach (var kor10 in v010.kor10_list)
                        {
                            if (kor10.people_list != null && kor10.people_list.Count() > 0)
                            {
                                foreach (var item in kor10.people_list)
                                {
                                    var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                                    item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                                    var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                                    item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                                    var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                                    item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                                    var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                                    item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                                    var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                                    item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                                    var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                                    item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                                    var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                                    item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                                }
                            }

                            if (kor10.representative_list != null && kor10.representative_list.Count() > 0)
                            {
                                foreach (var item in kor10.representative_list)
                                {
                                    var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                                    item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                                    var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                                    item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                                    var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                                    item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                                    var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                                    item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                                    var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                                    item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                                    var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                                    item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                                    var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                                    item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                                }
                            }

                            if (kor10.kor19_list != null && kor10.kor19_list.Count() > 0)
                            {
                                foreach (var kor19 in kor10.kor19_list)
                                {
                                    if (kor19.people_list != null && kor19.people_list.Count() > 0)
                                    {
                                        foreach (var item in kor19.people_list)
                                        {
                                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                                        }
                                    }

                                    if (kor19.representative_list != null && kor19.representative_list.Count() > 0)
                                    {
                                        foreach (var item in kor19.representative_list)
                                        {
                                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                                        }
                                    }
                                }
                            }
                        }
                    }

                    view = mapper.Map<V>(v010);
                }
                else if (typeof(V) == typeof(eForm_Save020View))
                {
                    var v020 = mapper.Map<eForm_Save020View>(view);

                    var s020_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v020.save020_representative_condition_type_code).FirstOrDefault();
                    v020.save020_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s020_representative_condtion_entity);

                    if (v020.people_list != null && v020.people_list.Count() > 0)
                    {
                        foreach (var item in v020.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v020.representative_list != null && v020.representative_list.Count() > 0)
                    {
                        foreach (var item in v020.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v020.contact_address_list != null && v020.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v020.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v020);
                }
                else if (typeof(V) == typeof(eForm_Save021View))
                {
                    var v021 = mapper.Map<eForm_Save021View>(view);

                    var s021_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v021.save021_representative_condition_type_code).FirstOrDefault();
                    v021.save021_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s021_representative_condtion_entity);

                    if (v021.people_list != null && v021.people_list.Count() > 0)
                    {
                        foreach (var item in v021.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v021.representative_list != null && v021.representative_list.Count() > 0)
                    {
                        foreach (var item in v021.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v021.contact_address_list != null && v021.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v021.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v021);
                }
                else if (typeof(V) == typeof(eForm_Save030View))
                {
                    var v030 = mapper.Map<eForm_Save030View>(view);

                    var s030_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v030.save030_representative_condition_type_code).FirstOrDefault();
                    v030.save030_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s030_representative_condtion_entity);

                    if (v030.people_list != null && v030.people_list.Count() > 0)
                    {
                        foreach (var item in v030.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v030.representative_list != null && v030.representative_list.Count() > 0)
                    {
                        foreach (var item in v030.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v030.contact_address_list != null && v030.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v030.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v030);
                }
                else if (typeof(V) == typeof(eForm_Save040View))
                {
                    var v040 = mapper.Map<eForm_Save040View>(view);

                    var s040_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v040.save040_representative_condition_type_code).FirstOrDefault();
                    v040.save040_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s040_representative_condtion_entity);

                    if (v040.people_list != null && v040.people_list.Count() > 0)
                    {
                        foreach (var item in v040.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v040.representative_list != null && v040.representative_list.Count() > 0)
                    {
                        foreach (var item in v040.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v040.contact_address_list != null && v040.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v040.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v040.receiver_people_list != null && v040.receiver_people_list.Count() > 0)
                    {
                        foreach (var item in v040.receiver_people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v040.receiver_representative_list != null && v040.receiver_representative_list.Count() > 0)
                    {
                        foreach (var item in v040.receiver_representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v040.receiver_contact_address_list != null && v040.receiver_contact_address_list.Count() > 0)
                    {
                        foreach (var item in v040.receiver_contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v040);
                }
                else if (typeof(V) == typeof(eForm_Save050View))
                {
                    var v050 = mapper.Map<eForm_Save050View>(view);

                    var s050_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v050.save050_representative_condition_type_code).FirstOrDefault();
                    v050.save050_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s050_representative_condtion_entity);

                    if (v050.people_list != null && v050.people_list.Count() > 0)
                    {
                        foreach (var item in v050.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v050.representative_list != null && v050.representative_list.Count() > 0)
                    {
                        foreach (var item in v050.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v050.contact_address_list != null && v050.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v050.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v050.receiver_people_list != null && v050.receiver_people_list.Count() > 0)
                    {
                        foreach (var item in v050.receiver_people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v050.receiver_representative_list != null && v050.receiver_representative_list.Count() > 0)
                    {
                        foreach (var item in v050.receiver_representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v050);
                }
                else if (typeof(V) == typeof(eForm_Save060View))
                {
                    var v060 = mapper.Map<eForm_Save060View>(view);

                    var s060_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v060.save060_representative_condition_type_code).FirstOrDefault();
                    v060.save060_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s060_representative_condtion_entity);

                    if (v060.people_list != null && v060.people_list.Count() > 0)
                    {
                        foreach (var item in v060.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v060.representative_list != null && v060.representative_list.Count() > 0)
                    {
                        foreach (var item in v060.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v060.contact_address_list != null && v060.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v060.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v060);
                }
                else if (typeof(V) == typeof(eForm_Save070View))
                {
                    var v070 = mapper.Map<eForm_Save070View>(view);

                    var s070_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v070.save070_representative_condition_type_code).FirstOrDefault();
                    v070.save070_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s070_representative_condtion_entity);

                    if (v070.people_list != null && v070.people_list.Count() > 0)
                    {
                        foreach (var item in v070.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v070.representative_list != null && v070.representative_list.Count() > 0)
                    {
                        foreach (var item in v070.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v070.contact_address_list != null && v070.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v070.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v070);
                }
                else if (typeof(V) == typeof(eForm_Save080View))
                {
                    var v080 = mapper.Map<eForm_Save080View>(view);

                    var s080_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v080.save080_representative_condition_type_code).FirstOrDefault();
                    v080.save080_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s080_representative_condtion_entity);

                    if (v080.people_list != null && v080.people_list.Count() > 0)
                    {
                        foreach (var item in v080.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v080.representative_list != null && v080.representative_list.Count() > 0)
                    {
                        foreach (var item in v080.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v080.contact_address_list != null && v080.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v080.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v080.receiver_people_list != null && v080.receiver_people_list.Count() > 0)
                    {
                        foreach (var item in v080.receiver_people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v080.receiver_representative_list != null && v080.receiver_representative_list.Count() > 0)
                    {
                        foreach (var item in v080.receiver_representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v080.receiver_contact_address_list != null && v080.receiver_contact_address_list.Count() > 0)
                    {
                        foreach (var item in v080.receiver_contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v080);
                }
                else if (typeof(V) == typeof(eForm_Save120View))
                {
                    var v120 = mapper.Map<eForm_Save120View>(view);

                    var s120_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v120.save120_representative_condition_type_code).FirstOrDefault();
                    v120.save120_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s120_representative_condtion_entity);

                    if (v120.people_list != null && v120.people_list.Count() > 0)
                    {
                        foreach (var item in v120.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v120.representative_list != null && v120.representative_list.Count() > 0)
                    {
                        foreach (var item in v120.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v120);
                }
                else if (typeof(V) == typeof(eForm_Save140View))
                {
                    var v140 = mapper.Map<eForm_Save140View>(view);

                    var s140_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v140.save140_representative_condition_type_code).FirstOrDefault();
                    v140.save140_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s140_representative_condtion_entity);

                    if (v140.people_list != null && v140.people_list.Count() > 0)
                    {
                        foreach (var item in v140.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v140.representative_list != null && v140.representative_list.Count() > 0)
                    {
                        foreach (var item in v140.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v140.contact_address_list != null && v140.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v140.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v140);
                }
                else if (typeof(V) == typeof(eForm_Save150View))
                {
                    var v150 = mapper.Map<eForm_Save150View>(view);

                    var s150_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v150.save150_representative_condition_type_code).FirstOrDefault();
                    v150.save150_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s150_representative_condtion_entity);

                    if (v150.people_list != null && v150.people_list.Count() > 0)
                    {
                        foreach (var item in v150.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v150.representative_list != null && v150.representative_list.Count() > 0)
                    {
                        foreach (var item in v150.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v150);
                }
                else if (typeof(V) == typeof(eForm_Save190View))
                {
                    var v190 = mapper.Map<eForm_Save190View>(view);

                    var s190_representative_condtion_entity = representative_condition_repo.Query(r => r.code == v190.save190_representative_condition_type_code).FirstOrDefault();
                    v190.save190_representative_condition_type_codeNavigation = mapper.Map<ReferenceMasterView>(s190_representative_condtion_entity);

                    var request_entity = save190_request_repo.Query(r => r.code == v190.save190_request_type_code).FirstOrDefault();
                    v190.save190_request_type_codeNavigation = mapper.Map<ReferenceMasterView>(request_entity);

                    if (v190.people_list != null && v190.people_list.Count() > 0)
                    {
                        foreach (var item in v190.people_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v190.representative_list != null && v190.representative_list.Count() > 0)
                    {
                        foreach (var item in v190.representative_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    if (v190.contact_address_list != null && v190.contact_address_list.Count() > 0)
                    {
                        foreach (var item in v190.contact_address_list)
                        {
                            var card_entity = card_repo.Query(r => r.code == item.card_type_code).FirstOrDefault();
                            item.card_type_codeNavigation = mapper.Map<ReferenceMasterView>(card_entity);

                            var career_entity = career_repo.Query(r => r.code == item.career_code).FirstOrDefault();
                            item.career_codeNavigation = mapper.Map<ReferenceMasterView>(career_entity);

                            var nation_entity = nation_repo.Query(r => r.code == item.nationality_code).FirstOrDefault();
                            item.nationality_codeNavigation = mapper.Map<ReferenceMasterView>(nation_entity);

                            var country_entity = country_repo.Query(r => r.code == item.address_country_code).FirstOrDefault();
                            item.address_country_codeNavigation = mapper.Map<ReferenceMasterView>(country_entity);

                            var district_entity = district_repo.Query(r => r.code == item.address_district_code).FirstOrDefault();
                            item.address_district_codeNavigation = mapper.Map<ReferenceMasterView>(district_entity);

                            var subdistrict_entity = subdistrict_repo.Query(r => r.code == item.address_sub_district_code).FirstOrDefault();
                            item.address_sub_district_codeNavigation = mapper.Map<ReferenceMasterView>(subdistrict_entity);

                            var province_entity = province_repo.Query(r => r.code == item.address_province_code).FirstOrDefault();
                            item.address_province_codeNavigation = mapper.Map<ReferenceMasterView>(province_entity);
                        }
                    }

                    view = mapper.Map<V>(v190);
                }
            }

            return view;
        }

        public string Merge3DImage(string img_path1,
            string img_path2,
            string img_path3,
            string img_path4,
            string img_path5,
            string img_path6)
        {
            string base64String = "";
            int MaxWidthMerge = 1000;
            int MaxHeightMerge = 1000;
            int frameWidth = 50;
            int frameHeight = 50;
            int MaxWidthSub = (MaxWidthMerge - (2 * frameWidth)) / 3;
            int MaxHeightSub = (MaxHeightMerge - (2 * frameHeight)) / 3;

            Image img1 = (!string.IsNullOrEmpty(img_path1)) ? Image.FromFile(img_path1) : null;
            Image img2 = (!string.IsNullOrEmpty(img_path2)) ? Image.FromFile(img_path2) : null;
            Image img3 = (!string.IsNullOrEmpty(img_path3)) ? Image.FromFile(img_path3) : null;
            Image img4 = (!string.IsNullOrEmpty(img_path4)) ? Image.FromFile(img_path4) : null;
            Image img5 = (!string.IsNullOrEmpty(img_path5)) ? Image.FromFile(img_path5) : null;
            Image img6 = (!string.IsNullOrEmpty(img_path6)) ? Image.FromFile(img_path6) : null;

            if (img1 != null)
            {
                img1 = FixedSize(img1, MaxWidthSub, MaxHeightSub);
            }
            if (img2 != null)
            {
                img2 = FixedSize(img2, MaxWidthSub, MaxHeightSub);
            }
            if (img3 != null)
            {
                img3 = FixedSize(img3, MaxWidthSub, MaxHeightSub);
            }
            if (img4 != null)
            {
                img4 = FixedSize(img4, MaxWidthSub, MaxHeightSub);
            }
            if (img5 != null)
            {
                img5 = FixedSize(img5, MaxWidthSub, MaxHeightSub);
            }
            if (img6 != null)
            {
                img6 = FixedSize(img6, MaxWidthSub, MaxHeightSub);
            }

            Image mergeImage = new Bitmap(MaxWidthMerge, MaxHeightMerge, PixelFormat.Format24bppRgb);

            using (Graphics grpMergeImage = Graphics.FromImage(mergeImage))
            {
                grpMergeImage.FillRectangle(Brushes.White, 0, 0, MaxWidthMerge, MaxHeightMerge);

                if (img1 != null)
                {
                    grpMergeImage.DrawImage(img1, new Point(
                        frameWidth, 
                        frameHeight));
                }
                if (img2 != null)
                {
                    grpMergeImage.DrawImage(img2, new Point(
                        MaxWidthMerge - MaxWidthSub - frameWidth, 
                        frameHeight));
                }
                if (img3 != null)
                {
                    grpMergeImage.DrawImage(img3, new Point(
                        frameWidth, 
                        MaxHeightMerge - (2 * MaxHeightSub) - frameHeight));
                }
                if (img4 != null)
                {
                    grpMergeImage.DrawImage(img4, new Point(
                        MaxWidthMerge - MaxWidthSub - frameWidth, 
                        MaxHeightMerge - (2 * MaxHeightSub) - frameHeight));
                }
                if (img5 != null)
                {
                    grpMergeImage.DrawImage(img5, new Point(
                        frameWidth, 
                        MaxHeightMerge - MaxHeightSub - frameHeight));
                }
                if (img6 != null)
                {
                    grpMergeImage.DrawImage(img6, new Point(
                        MaxWidthMerge - MaxWidthSub - frameWidth, 
                        MaxHeightMerge - MaxHeightSub - frameHeight));
                }

                using (MemoryStream m = new MemoryStream())
                {
                    mergeImage.Save(m, ImageFormat.Jpeg);
                }
            }

            if (mergeImage != null)
            {
                using (Image resultImage = mergeImage)
                {
                    using (MemoryStream m = new MemoryStream())
                    {
                        resultImage.Save(m, ImageFormat.Jpeg);
                        byte[] imageBytes = m.ToArray();

                        // Convert byte[] to Base64 String
                        base64String = Convert.ToBase64String(imageBytes);
                    }
                }
            }

            return base64String;
        }

        static Image FixedSize(Image imgPhoto, int Width, int Height)
        {
            int sourceWidth = imgPhoto.Width;
            int sourceHeight = imgPhoto.Height;
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)Width / (float)sourceWidth);
            nPercentH = ((float)Height / (float)sourceHeight);
            if (nPercentH < nPercentW)
            {
                nPercent = nPercentH;
                destX = System.Convert.ToInt16((Width -
                    (sourceWidth * nPercent)) / 2);
            }
            else
            {
                nPercent = nPercentW;
                destY = System.Convert.ToInt16((Height -
                    (sourceHeight * nPercent)) / 2);
            }

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap bmPhoto = new Bitmap(Width, Height, PixelFormat.Format24bppRgb);
            bmPhoto.SetResolution(imgPhoto.HorizontalResolution, imgPhoto.VerticalResolution);

            Graphics grPhoto = Graphics.FromImage(bmPhoto);
            grPhoto.Clear(Color.White);
            grPhoto.InterpolationMode = InterpolationMode.HighQualityBicubic;

            grPhoto.DrawImage(imgPhoto,
                new Rectangle(destX, destY, destWidth, destHeight),
                new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight),
                GraphicsUnit.Pixel);

            grPhoto.Dispose();
            return bmPhoto;
        }
    }
}
