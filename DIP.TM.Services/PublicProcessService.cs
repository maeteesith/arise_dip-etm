﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Auths;
using DIP.TM.Services.Checking;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Helps;
using DIP.TM.Services.Receipts;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Utils.Extensions;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic.Core;
using System.Linq.Expressions;
using System.Web;

namespace DIP.TM.Services.Saves {

    public class PublicProcessService {

        protected long? userId;

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public PublicProcessService(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            this.configuration = configuration;
            this.uowProvider = uowProvider;
            this.mapper = mapper;

            userId = HttpContext.Current?.User?.UserId();
        }
        //[LogAopInterceptor]
        //public virtual BaseResponsePageView<List<vPublicItemView>> PublicRole01ItemList(PageRequest payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<vPublicItem>();
        //        var itemList = repo.Filters(x => x.is_deleted == false);
        //        var filterList = itemList.FilterDynamic(payload);
        //        payload.item_total = filterList.Count();
        //        var resultPage = filterList.Page(payload.page_index, payload.item_per_page);

        //        return new BaseResponsePageView<List<vPublicItemView>>() {
        //            data = new BaseResponsePageDataModel<List<vPublicItemView>>() {
        //                list = mapper.Map<List<vPublicItemView>>(resultPage),
        //                paging = payload
        //            }
        //        };
        //    }
        //}
        //[LogAopInterceptor]
        //public virtual BaseResponsePageView<List<vPublicItemView>> PublicRole01CheckList(PageRequest payload) {
        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<vPublicItem>();
        //        var itemList = repo.Filters(
        //            x => x.is_deleted == false &&
        //            //TODO
        //            //x.public_receiver_by == userId &&
        //            1 == 1
        //        );
        //        var filterList = itemList.FilterDynamic(payload);
        //        payload.item_total = filterList.Count();
        //        var resultPage = filterList.Page(payload.page_index, payload.item_per_page);

        //        return new BaseResponsePageView<List<vPublicItemView>>() {
        //            data = new BaseResponsePageDataModel<List<vPublicItemView>>() {
        //                list = mapper.Map<List<vPublicItemView>>(resultPage),
        //                paging = payload
        //            }
        //        };
        //    }
        //}

        public List<PostRoundActionImageView> PostRoundActionImageSave(List<PostRoundActionImageAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<PostRoundActionImage>();
                foreach (var model in payload) {
                    if (model.is_deleted && !model.id.HasValue) continue;

                    repo.Update(mapper.Map<PostRoundActionImage>(model));
                }
                uow.SaveChanges();
            }
            return mapper.Map<List<PostRoundActionImageView>>(payload);
        }

        [LogAopInterceptor]
        public virtual List<vPublicItemView> PublicRole01CheckDoAuto(List<vPublicItemAddModel> payload) {
            var view_list = new List<vPublicItemView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                //var payment_repo = uow.GetRepository<Save010PublicPayment>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    PublicRole01CheckSend(model.id);
                    //    var entity = payment_repo.Get(model.id.Value);
                    //    entity.public_role02_consider_payment_status_code = PublicRole02ConsiderPaymentStatusCodeConstant.WAIT.ToString();
                    //    entity.public_role02_consider_payment_date = model.public_role02_consider_payment_date.HasValue ? model.public_role02_consider_payment_date : DateTime.Now;
                    //    entity.public_role02_consider_payment_by = userId;

                    //    entity.public_role05_consider_payment_status_code = PublicRole05ConsiderPaymentStatusCodeConstant.WAIT.ToString();
                    //    payment_repo.Update(entity);
                }

                //uow.SaveChanges();
            }

            return view_list;
        }


        [LogAopInterceptor]
        public virtual List<vPublicItemView> PublicRole01ItemAutoSplit() {
            var view_list = new List<vPublicItemView>();
            int item_min = 10;
            int item_max = 20;

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vUM_User>();
                var user_list = repo.Query(r => r.department_id == 2 || 1 == 1);

                //TODO Split into staff by min max
                foreach (var user in user_list) {
                    var item_repo = uow.GetRepository<vPublicItem>();
                    var item_count = item_repo.Query(r => r.public_receiver_by == user.id).Count();

                    if (item_count < item_min) {
                        using (var _uow = uowProvider.CreateUnitOfWork()) {
                            var save_repo = _uow.GetRepository<Save010>();
                            var save_list = save_repo.Query(r => r.public_status_code == PublicStatusCodeConstant.WAIT.ToString());

                            foreach (var save in save_list) {
                                save.public_status_code = PublicStatusCodeConstant.RECEIVED.ToString();
                                save.public_spliter_by = 1;
                                save.public_receiver_by = user.id;
                                save.public_receive_date = DateTime.Now;

                                if (save.public_type_code == PublicTypeCodeConstant.PUBLIC.ToString()) {
                                    save.public_receive_status_code = PublicReceiveStatusCodeConstant.DRAFT.ToString();
                                } else if (save.public_type_code == PublicTypeCodeConstant.PUBLIC_CASE41.ToString()) {
                                    var pr41_repo = uow.GetRepository<PublicRoundCase41>();
                                    var pr41 = pr41_repo.Query(r => r.save_id == save.id).FirstOrDefault();

                                    if (pr41 != null) {
                                        pr41.public_role01_case41_status_code = PublicRole01Case41StatusCodeConstant.DRAFT.ToString();

                                        pr41_repo.Update(pr41);
                                    }
                                }

                                save_repo.Update(save);
                            }
                            _uow.SaveChanges();
                        }
                    }
                }
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual BaseResponsePageView<List<vReceiptItemView>> ReceiptList(PageRequest payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vReceiptItem>();
                var itemList = repo.Filters(x => x.is_deleted == false);
                var filterList = itemList.FilterDynamic(payload);
                payload.item_total = filterList.Count();
                var resultPage = filterList.Page(payload.page_index, payload.item_per_page);

                return new BaseResponsePageView<List<vReceiptItemView>>() {
                    data = new BaseResponsePageDataModel<List<vReceiptItemView>>() {
                        list = mapper.Map<List<vReceiptItemView>>(resultPage),
                        paging = payload
                    }
                };
            }
        }


        [LogAopInterceptor]
        public virtual SendChangeView PublicRole01CheckSendChange(SendChangeAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var entity = repo.Get(payload.save_id.Value);
                entity.public_receive_status_code = PublicReceiveStatusCodeConstant.WAIT_CHANGE.ToString();
                entity.considering_receive_remark = payload.reason;
                entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();
                repo.Update(entity);
                uow.SaveChanges();

                var service = new CheckingProcessService(configuration, uowProvider, mapper);
                service.ConsideringSimilarInstructionPublicDelete(payload.save_id.Value);

                return mapper.Map<SendChangeView>(payload);
            }
        }


        [LogAopInterceptor]
        public virtual List<SendChangeView> PublicRole02ConsiderPaymentSendChange(List<SendChangeAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var payment_repo = uow.GetRepository<Save010PublicPayment>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = repo.Get(model.save_id.Value);

                    entity.considering_receive_remark = model.reason;
                    entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();

                    repo.Update(entity);

                    // For multiply round -> Get current one
                    var payment = payment_repo.Query(r => r.save_id == model.save_id.Value).OrderBy(r => r.id).LastOrDefault();
                    payment.public_role02_consider_payment_status_code = PublicRole02ConsiderPaymentStatusCodeConstant.WAIT_CHANGE.ToString();
                    payment_repo.Update(payment);

                    //var service = new CheckingProcessService(configuration, uowProvider, mapper);
                    //service.ConsideringSimilarInstructionPublicDelete(model.save_id.Value);
                }

                uow.SaveChanges();

                return mapper.Map<List<SendChangeView>>(payload);
            }
        }




        [LogAopInterceptor]
        public virtual List<SendChangeView> PublicRole02DocumentPaymentSendChange(List<SendChangeAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var payment_repo = uow.GetRepository<Save010PublicPayment>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = repo.Get(model.save_id.Value);

                    entity.considering_receive_remark = model.reason;
                    entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();

                    repo.Update(entity);

                    // For multiply round -> Get current one
                    var payment = payment_repo.Query(r => r.save_id == model.save_id.Value).OrderBy(r => r.id).LastOrDefault();
                    payment.public_role02_document_payment_status_code = PublicRole02DocumentPaymentStatusCodeConstant.WAIT_CHANGE.ToString();
                    payment_repo.Update(payment);

                    //var service = new CheckingProcessService(configuration, uowProvider, mapper);
                    //service.ConsideringSimilarInstructionPublicDelete(model.save_id.Value);
                }

                uow.SaveChanges();

                return mapper.Map<List<SendChangeView>>(payload);
            }
        }


        [LogAopInterceptor]
        public virtual List<SendChangeView> PublicRole05ConsiderPaymentSendChange(List<SendChangeAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var payment_repo = uow.GetRepository<Save010PublicPayment>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = repo.Get(model.save_id.Value);

                    entity.considering_receive_remark = model.reason;
                    entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();

                    repo.Update(entity);

                    // For multiply round -> Get current one
                    var payment = payment_repo.Query(r => r.save_id == model.save_id.Value).OrderBy(r => r.id).LastOrDefault();
                    payment.public_role05_consider_payment_status_code = PublicRole05ConsiderPaymentStatusCodeConstant.WAIT_CHANGE.ToString();
                    payment_repo.Update(payment);

                    //var service = new CheckingProcessService(configuration, uowProvider, mapper);
                    //service.ConsideringSimilarInstructionPublicDelete(model.save_id.Value);
                }

                uow.SaveChanges();

                return mapper.Map<List<SendChangeView>>(payload);
            }
        }


        [LogAopInterceptor]
        public virtual List<SendChangeView> PublicRole04CheckSendChange(List<SendChangeAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var payment_repo = uow.GetRepository<Save010PublicPayment>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = repo.Get(model.id.Value);

                    entity.considering_receive_remark = model.reason;
                    entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();

                    entity.public_role04_receive_status_code = PublicRole04ReceiveStatusCodeConstant.WAIT_CHANGE.ToString();

                    repo.Update(entity);

                    //var service = new CheckingProcessService(configuration, uowProvider, mapper);
                    //service.ConsideringSimilarInstructionPublicDelete(model.save_id.Value);
                }

                uow.SaveChanges();

                return mapper.Map<List<SendChangeView>>(payload);
            }
        }

        [LogAopInterceptor]
        public virtual SendChangeView PublicRole04DocumentSendChange(SendChangeAddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                var entity = repo.Get(payload.save_id.Value);

                //TODO How to send back to last department
                if (payload.department_code == "PUBLIC") {
                }

                entity.public_role04_receive_status_code = PublicRole04ReceiveStatusCodeConstant.WAIT_CHANGE.ToString();

                repo.Update(entity);
                uow.SaveChanges();

                return mapper.Map<SendChangeView>(payload);
            }
        }

        [LogAopInterceptor]
        public virtual List<SendChangeView> PublicRole05DocumentSendChange(List<SendChangeAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();

                foreach (var model in payload) {
                    var entity = repo.Get(model.id.Value);

                    entity.considering_receive_remark = model.reason;
                    entity.considering_receive_status_code = ConsideringReceivingStatusCodeConstant.DRAFT_FIX.ToString();

                    entity.public_role05_status_code = PublicRole05StatusCodeConstant.WAIT_CHANGE.ToString();

                    repo.Update(entity);
                }
                uow.SaveChanges();

                return mapper.Map<List<SendChangeView>>(payload);
            }
        }

        [LogAopInterceptor]
        public virtual Save010ProcessView PublicRole01CheckSend(long id) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();

                var entity = repo.Get(id);
                if (entity.public_receive_status_code == PublicReceiveStatusCodeConstant.DRAFT_FIX.ToString()) {
                    entity.public_receive_status_code = PublicReceiveStatusCodeConstant.SEND_FIX.ToString();
                } else {
                    entity.public_receive_status_code = PublicReceiveStatusCodeConstant.SEND.ToString();
                }
                entity.public_send_date = DateTime.Now;
                entity.public_receive_do_status_code = PublicReceiveDoStatusCodeConstant.WAIT_PUBLIC.ToString();
                repo.Update(entity);

                uow.SaveChanges();

                return mapper.Map<Save010ProcessView>(entity);
            }
        }

        public List<vSave010CertificationFileView> CertificationFileAdd(List<Save010CertificationFileAddModel> payload) {
            var view_list = new List<vSave010CertificationFileView>();

            foreach (var model in payload) {
                using (var uow = uowProvider.CreateUnitOfWork()) {
                    var repo = uow.GetRepository<Save010CertificationFile>();
                    var entity = repo.Update(mapper.Map<Save010CertificationFile>(model));
                    uow.SaveChanges();

                    var view_repo = uow.GetRepository<vSave010CertificationFile>();
                    var view = view_repo.Get(entity.id);
                    view_list.Add(mapper.Map<vSave010CertificationFileView>(view));
                }
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual Save010ProcessView PublicRole01CheckSave(Save010AddModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();

                var product_list = payload.product_list;
                payload.product_list = null;
                var entity = repo.Update(mapper.Map<Save010>(payload));

                var request_group_repo = uow.GetRepository<Save010RequestGroup>();
                foreach (var request_group in payload.request_group_list) {
                    request_group_repo.Update(mapper.Map<Save010RequestGroup>(request_group));
                }

                uow.SaveChanges();

                var view = mapper.Map<Save010ProcessView>(entity);

                var product_service = new SaveProductService<SaveProductAddModel, Save010Product, SaveProductView>(configuration, uowProvider, mapper);
                view.product_list = product_service.Save(product_list, payload.id.Value);


                return view;
            }
        }


        [LogAopInterceptor]
        public virtual BaseView PublicRole01PrepareDelete(BaseModel payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<Save010>();
                if (payload.ids != null) {
                    foreach (long id in payload.ids) {
                        var entity = repo.Get(id);
                        entity.public_receive_do_status_code = PublicReceiveDoStatusCodeConstant.SEND_CHANGE.ToString();
                        entity.considering_receive_remark = payload.cancel_reason;

                        entity.public_receive_status_code = PublicReceiveStatusCodeConstant.WAIT_CHANGE.ToString();
                        repo.Update(entity);
                    }
                }
                uow.SaveChanges();
                return mapper.Map<BaseView>(null);
            }
        }


        [LogAopInterceptor]
        public virtual vPublicRole01DoingView PublicRoundAdd() {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var public_round_repo = uow.GetRepository<PublicRound>();
                var save_repo = uow.GetRepository<Save010>();
                //var product_repo = uow.GetRepository<Save010Product>();
                var public_round_item_repo = uow.GetRepository<PublicRoundItem>();
                var request_document_collect_repo = uow.GetRepository<RequestDocumentCollect>();

                var save_list = save_repo.Query(r =>
                    r.public_receive_do_status_code == PublicReceiveDoStatusCodeConstant.WAIT_PUBLIC.ToString()
                //|| 1 == 1
                );

                if (save_list.Count() > 0) {
                    DateTime now = DateTime.Now;
                    now = now.AddDays(1 - (int)DateTime.Now.DayOfWeek);

                    var public_round = new PublicRound() {
                        book_index = GetBookIndex(),
                        public_start_date = now,
                        public_end_date = now.AddDays(60),
                        public_round_status_code = PublicRoundStatusCodeConstant.DOING_PUBLIC.ToString(),
                    };
                    public_round_repo.Add(public_round);
                    uow.SaveChanges();

                    var request_item_sub_type_1_count_list = new Dictionary<string, int>();
                    foreach (var save in save_list) {
                        save.public_round_id = public_round.id;
                        save.public_receive_do_status_code = PublicReceiveDoStatusCodeConstant.DOING_PUBLIC.ToString();
                        save_repo.Update(save);

                        foreach (var request_item_sub_type_1_code in save.Save010Product.Select(r => r.request_item_sub_type_1_code).Distinct()) {
                            if (string.IsNullOrEmpty(request_item_sub_type_1_code)) continue;

                            if (!request_item_sub_type_1_count_list.ContainsKey(request_item_sub_type_1_code))
                                request_item_sub_type_1_count_list.Add(request_item_sub_type_1_code, 1);
                            else
                                request_item_sub_type_1_count_list[request_item_sub_type_1_code]++;

                            //product.request_item_sub_status_code = RequestItemSubStatusCodeConstant.DOING_PUBLIC.ToString();
                            //product_repo.Update(product);

                            var request_document_collect = request_document_collect_repo.Query(r => r.save_id == save.id && r.request_document_collect_type_code == RequestDocumentCollectTypeCodeConstant.TRADEMARK_2D.ToString()).FirstOrDefault();
                            var trademark_2d_file_id = request_document_collect == null ? null : request_document_collect.file_id;
                            var public_round_item = new PublicRoundItem() {
                                public_round_id = public_round.id,
                                line_index = request_item_sub_type_1_count_list[request_item_sub_type_1_code],
                                request_number = save.request_number,
                                request_item_type_code = save.request_item_type_code,
                                sound_mark_list = save.sound_mark_list,
                                //sound_mark_file_id = save.save010
                                trademark_2d_file_id = trademark_2d_file_id,
                                save_id = save.id,
                                request_item_sub_type_1_code = request_item_sub_type_1_code,
                                request_item_sub_status_code = RequestItemSubStatusCodeConstant.DOING_PUBLIC.ToString(),
                            };
                            public_round_item_repo.Add(public_round_item);
                        }
                    }

                    uow.SaveChanges();
                }
                //}

                return null;
            }
        }

        [LogAopInterceptor]
        public virtual List<vPublicRequestItemView> PublicRoundEnd() {
            var view_list = new List<vPublicRequestItemView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vPublicRequestItem>();
                var entity_list = repo.Query(r =>
                    r.public_round_status_code == PublicRoundStatusCodeConstant.DOING_PUBLIC.ToString()
                //r.public_end_date < DateTime.Now && 
                //&& string.IsNullOrEmpty(r.public_role02_check_status_code )
                );

                var save_repo = uow.GetRepository<Save010>();
                var payment_repo = uow.GetRepository<Save010PublicPayment>();
                foreach (var entity in entity_list) {
                    var save_entity = save_repo.Get(entity.id.Value);
                    save_entity.public_receive_do_status_code = PublicReceiveDoStatusCodeConstant.DONE.ToString();

                    var payment = payment_repo.Query(r => r.save_id == entity.id.Value).FirstOrDefault();
                    if (payment == null) {
                        payment = new Save010PublicPayment() {
                            save_id = entity.id.Value,
                            total_price = 600,
                            public_role02_consider_payment_status_code = PublicRole02ConsiderPaymentStatusCodeConstant.NOT_YET.ToString(),
                        };
                    } else {
                        if (payment.public_role02_consider_payment_status_code == PublicRole02ConsiderPaymentStatusCodeConstant.WAIT_CHANGE.ToString()) {
                            payment.public_role02_consider_payment_status_code = PublicRole02ConsiderPaymentStatusCodeConstant.DRAFT_CHANGE.ToString();
                        } else {
                            payment.public_role02_consider_payment_status_code = PublicRole02ConsiderPaymentStatusCodeConstant.NOT_YET.ToString();
                        }
                    }
                    payment_repo.Update(payment);

                    save_repo.Update(save_entity);
                }

                var public_round_repo = uow.GetRepository<PublicRound>();
                foreach (var public_round_id in entity_list.Select(r => r.public_round_id).Distinct()) {
                    var public_round_entity = public_round_repo.Get(public_round_id.Value);
                    public_round_entity.public_round_status_code = PublicRoundStatusCodeConstant.DONE.ToString();
                    public_round_repo.Update(public_round_entity);
                }
                uow.SaveChanges();
            }

            return view_list;
        }

        //[LogAopInterceptor]
        //public virtual List<vPublicRequestItemView> PublicRole02CheckAutoDo(List<vPublicRequestItemAddModel> payload) {
        //    var view_list = new List<vPublicRequestItemView>();

        //    using (var uow = uowProvider.CreateUnitOfWork()) {
        //        var repo = uow.GetRepository<Save010>();
        //        var payment_repo = uow.GetRepository<Save010PublicPayment>();

        //        foreach (var model in payload.Where(r => r.is_check)) {
        //            var entity = repo.Get(model.id.Value);
        //            entity.public_role02_check_by = userId;
        //            entity.public_role02_check_date = DateTime.Now;
        //            entity.public_role02_check_status_code = PublicRole02CheckStatusCodeConstant.DONE.ToString();
        //            repo.Update(entity);

        //            payment_repo.Add(new Save010PublicPayment() {
        //                save_id = model.id,
        //                total_price = 600,
        //                public_role02_consider_payment_status_code = PublicRole02ConsiderPaymentStatusCodeConstant.NOT_YET.ToString(),
        //            });
        //        }

        //        uow.SaveChanges();
        //    }

        //    return view_list;
        //}

        [LogAopInterceptor]
        public virtual List<vSave010PublicPaymentView> PublicRole02ConsiderPaymentSend(List<vSave010PublicPaymentAddModel> payload) {
            var view_list = new List<vSave010PublicPaymentView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var payment_repo = uow.GetRepository<Save010PublicPayment>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = payment_repo.Query(r => r.save_id == model.save_id.Value).OrderBy(r => r.id).LastOrDefault();
                    entity.public_role02_consider_payment_status_code = PublicRole02ConsiderPaymentStatusCodeConstant.WAIT.ToString();
                    entity.public_role02_consider_payment_date = model.public_role02_consider_payment_date.HasValue ? model.public_role02_consider_payment_date : DateTime.Now;
                    entity.public_role02_consider_payment_by = userId;

                    entity.value_01 = model.value_01;
                    entity.value_02 = model.value_02;
                    entity.value_03 = model.value_03;
                    entity.value_04 = model.value_04;
                    entity.value_05 = model.value_05;

                    entity.public_role05_consider_payment_status_code = PublicRole05ConsiderPaymentStatusCodeConstant.WAIT.ToString();
                    payment_repo.Update(entity);
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vSave010PublicPaymentView> PublicRole05ConsiderPaymentSend(List<vSave010PublicPaymentAddModel> payload) {
            var view_list = new List<vSave010PublicPaymentView>();
            var document_service = new DocumentProcessService(configuration, uowProvider, mapper);

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var post_round_repo = uow.GetRepository<PostRound>();
                var payment_repo = uow.GetRepository<Save010PublicPayment>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = payment_repo.Query(r => r.save_id == model.save_id.Value).OrderBy(r => r.id).LastOrDefault();
                    entity.public_role02_consider_payment_status_code = PublicRole02ConsiderPaymentStatusCodeConstant.DONE.ToString();

                    entity.public_role05_consider_payment_status_code = PublicRole05ConsiderPaymentStatusCodeConstant.DONE.ToString();
                    entity.public_role05_consider_payment_date = model.public_role05_consider_payment_date.HasValue ? model.public_role05_consider_payment_date.Value : DateTime.Now;
                    entity.public_role05_consider_payment_by = userId;

                    entity.value_01 = model.value_01;
                    entity.value_02 = model.value_02;
                    entity.value_03 = model.value_03;
                    entity.value_04 = model.value_04;
                    entity.value_05 = model.value_05;

                    entity.public_role02_document_payment_status_code = PublicRole02DocumentPaymentStatusCodeConstant.WAIT_1.ToString();

                    payment_repo.Update(entity);

                    if (post_round_repo.Query(r => r.object_id == entity.save_id.Value && r.post_round_instruction_rule_code == PostRoundInstructionRuleCodeConstant.RULE_5_3.ToString()).Count() == 0) {
                        var post_round = document_service.ForceIntructionRuleAdd(entity.save_id.Value, PostRoundInstructionRuleCodeConstant.RULE_5_3.ToString(), PostRoundTypeCodeConstant.INFORM_PAYMENT, 1, "", "", "", "", "", true, null);
                        post_round.document_role02_receiver_by = userId;
                        post_round_repo.Update(post_round);
                    }
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vSave010PublicPaymentView> PublicRole02DocumentPaymentSend(List<vSave010PublicPaymentAddModel> payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var payment_repo = uow.GetRepository<Save010PublicPayment>();
                var post_round_repo = uow.GetRepository<PostRound>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = payment_repo.Get(model.id.Value);

                    model.book_start_date = model.book_start_date.HasValue ? model.book_start_date : DateTime.Now;
                    if (entity.public_role02_document_payment_status_code == PublicRole02DocumentPaymentStatusCodeConstant.WAIT_1.ToString()) {

                        var post_round = post_round_repo.Query(r => r.object_id == entity.save_id.Value && r.post_round_instruction_rule_code == PostRoundInstructionRuleCodeConstant.RULE_5_3.ToString()).FirstOrDefault();
                        post_round.document_role02_receive_date = DateTime.Now;
                        post_round.document_role02_print_document_status_code = DocumentRole02PrintDocumentStatusCodeConstant.DRAFT.ToString();
                        //post_round.document_role02_print_cover_status_code = DocumentRole02PrintCoverStatusCodeConstant.WAIT_COVER.ToString();
                        //post_round.document_role02_print_document_date = model.book_start_date;
                        post_round_repo.Update(post_round);

                        entity.public_role02_document_payment_status_code = PublicRole02DocumentPaymentStatusCodeConstant.WAIT_PAYMENT_1.ToString();

                        var receipt_service = new ReceiptService(configuration, uowProvider, mapper);
                        receipt_service.Add(post_round, model.name, entity.total_price.Value);
                    }

                    payment_repo.Update(entity);
                }

                uow.SaveChanges();
            }

            return mapper.Map<List<vSave010PublicPaymentView>>(payload.Where(r => r.is_check));
        }
        [LogAopInterceptor]
        public virtual List<vPostRoundView> PublicRole02DocumentPostSend(List<vPostRoundAddModel> payload) {
            var view_list = new List<vPostRoundView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var payment_repo = uow.GetRepository<PostRound>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = payment_repo.Get(model.id.Value);

                    entity.post_number = model.post_number;
                    entity.post_round_document_post_status_code = PostRoundDocumentPostStatusCodeConstant.DONE.ToString();
                    //No Send to document role 2
                    //entity.document_role02_status_code = DocumentRole02StatusCodeConstant.WAIT.ToString();

                    entity.post_round_document_post_date = DateTime.Now;
                    entity.post_round_action_post_status_code = PostRoundActionPostStatusCodeConstant.WAIT.ToString();

                    payment_repo.Update(entity);
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vPostRoundView> PublicRole02ActionPostSend(List<vPostRoundAddModel> payload) {
            var view_list = new List<vPostRoundView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var payment_repo = uow.GetRepository<PostRound>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var entity = payment_repo.Get(model.id.Value);

                    entity.post_round_action_post_type_code = model.post_round_action_post_type_code;
                    entity.book_acknowledge_date = model.book_acknowledge_date;
                    entity.post_round_remark = model.post_round_remark;
                    entity.post_round_action_post_status_code = PostRoundActionPostStatusCodeConstant.DONE.ToString();
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vPublicRegisterView> PublicRole04ItemAutoSplit() {
            var view_list = new List<vPublicRegisterView>();
            int item_min = 10;
            int item_max = 20;

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<vUM_User>();
                var user_list = repo.Query(r => r.department_id == 2 || 1 == 1);

                //TODO Split into staff by min max
                foreach (var user in user_list) {
                    //var item_count = item_repo.Query(r => r.public_receiver_by == user.id).Count();

                    //if (item_count < item_min) {
                    var save_repo = uow.GetRepository<Save010>();
                    //    using (var _uow = uowProvider.CreateUnitOfWork()) {
                    //        var save_repo = _uow.GetRepository<Save010>();
                    //        var save_list = save_repo.Query(r => r.public_status_code == PublicStatusCodeConstant.WAIT.ToString());

                    var save_list = save_repo.Query(r =>
                        !string.IsNullOrEmpty(r.public_role04_status_code)
                    //&& string.IsNullOrEmpty(r.public_role04_receive_status_code)
                    );
                    foreach (var save in save_list) {
                        save.public_role04_status_code = PublicRole04StatusCodeConstant.DONE.ToString();
                        save.public_role04_date = DateTime.Now;

                        save.public_role04_spliter_by = userId;
                        save.public_role04_receiver_by = user.id;
                        if (string.IsNullOrEmpty(save.public_role04_receive_status_code)) {
                            save.public_role04_receive_status_code = PublicRole04ReceiveStatusCodeConstant.NOT_YET.ToString();
                        }

                        save_repo.Update(save);
                    }
                }
                //}
                //}

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vPublicRegisterView> PublicRole04CheckSend(List<vPublicRegisterAddModel> payload) {
            var view_list = new List<vPublicRegisterView>();
            var document_service = new DocumentProcessService(configuration, uowProvider, mapper);

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();
                var post_round_repo = uow.GetRepository<PostRound>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var save = save_repo.Get(model.id.Value);

                    if (string.IsNullOrEmpty(model.public_instruction_rule_type_code)) {
                        save.public_role04_receive_status_code = PublicRole04ReceiveStatusCodeConstant.WAIT.ToString();
                        save.public_role04_receive_date = DateTime.Now;

                        if (save.public_role05_status_code == PublicRole05StatusCodeConstant.WAIT_CHANGE.ToString()) {
                            save.public_role05_status_code = PublicRole05StatusCodeConstant.DRAFT_CHANGE.ToString();
                        } else {
                            save.public_role05_status_code = PublicRole05StatusCodeConstant.DRAFT.ToString();
                        }

                    } else if (model.public_instruction_rule_type_code == PublicRole04CheckInstructionRuleTypeCodeConstant.RULE_9_11.ToString()) {
                        save.public_role04_receive_status_code = PublicRole04ReceiveStatusCodeConstant.WAIT_PAID_MORE.ToString();
                        save.public_role04_receive_date = DateTime.Now;

                        var post_round = document_service.ForceIntructionRuleAdd(save.id, PostRoundInstructionRuleCodeConstant.RULE_5_8_FEE.ToString(), PostRoundTypeCodeConstant.PUBLIC_OTHER_FEE, 1, model.public_total_price.HasValue ? model.public_total_price.Value.ToString() : "", model.public_remark_1, model.public_remark_2, "", "", true, null);
                        post_round.document_role04_receiver_by = userId;
                        post_round.document_role04_receive_date = DateTime.Now;
                        post_round.document_role04_receive_send_date = DateTime.Now;
                        post_round.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.DRAFT.ToString();
                        post_round_repo.Update(post_round);
                    }
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        string GetBookIndex() {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<SequenceNumber>();
                SequenceNumber entity = repo.Query(r => r.topic == SequenceNumberConstant.PUBLIC_BOOK_INDEX.ToString()).FirstOrDefault();

                if (entity == null) {
                    entity = new SequenceNumber() {
                        topic = SequenceNumberConstant.PUBLIC_BOOK_INDEX.ToString(),
                        number = 1,
                    };
                    repo.Add(entity);
                } else {
                    entity.number++;
                    repo.Update(entity);
                }
                uow.SaveChanges();

                return entity.number.Value.ToString();
            }
        }
        [LogAopInterceptor]
        public virtual List<vPublicRegisterView> PublicRole05DocumentSend(List<vPublicRegisterAddModel> payload) {
            var view_list = new List<vPublicRegisterView>();
            var document_service = new DocumentProcessService(configuration, uowProvider, mapper);

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();
                var post_round_repo = uow.GetRepository<PostRound>();

                var save_tag_repo = uow.GetRepository<Save010CheckingTagSimilar>();
                var save_tag_ir_repo = uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var save = save_repo.Get(model.id.Value);

                    //TODO Confirm
                    save.trademark_status_code = TrademarkStatusCodeConstant.R.ToString();

                    save.public_role04_receive_status_code = PublicRole04ReceiveStatusCodeConstant.DONE.ToString();

                    save.public_role05_by = userId;
                    if (save.public_role05_status_code == PublicRole05StatusCodeConstant.DRAFT_FIX.ToString()) {
                        save.public_role05_status_code = PublicRole05StatusCodeConstant.SEND_FIX.ToString();
                    } else {
                        save.public_role05_status_code = PublicRole05StatusCodeConstant.SEND.ToString();
                    }

                    save.public_role05_date = DateTime.Now;
                    save.trademark_expired_date = save.request_date.Value.AddYears(10).AddDays(-1);
                    save.trademark_expired_end_date = save.request_date.Value.AddYears(10).AddDays(-1);
                    save.trademark_expired_start_date = save.trademark_expired_end_date.Value.AddMonths(-6);
                    save.registration_number = HelpService.GetRegistrationNumber(uowProvider, save.request_item_type_code);
                    save.public_role04_document_status_code = PublicRole04DocumentStatusCodeConstant.WAIT.ToString();
                    save.public_role02_document_status_code = PublicRole02DocumentStatusCodeConstant.WAIT.ToString();
                    save_repo.Update(save);


                    if (post_round_repo.Query(r => r.object_id == model.id.Value && r.post_round_instruction_rule_code == PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString()).Count() == 0) {
                        var post_round = document_service.ForceIntructionRuleAdd(model.id.Value, PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString(), PostRoundTypeCodeConstant.REGISTERED, 1, "", "", "", "", "", false, null);


                        //var post_round = post_repo.Query(r =>
                        //     r.post_round_instruction_rule_code == PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString() &&
                        //     r.post_round_type_code == PostRoundTypeCodeConstant.REGISTERED.ToString() &&
                        //     r.object_id == model.id &&
                        //     r.round_index == 1
                        // ).FirstOrDefault();
                        //if (post_round == null) {
                        //    post_repo.Add(new PostRound() {
                        //        post_round_instruction_rule_code = PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString(),
                        //        post_round_type_code = PostRoundTypeCodeConstant.REGISTERED.ToString(),
                        //        object_id = model.id,
                        //        round_index = 1,
                        //    });

                        // Check other request number is similar this request number
                        var save_tag_list = save_tag_repo.Query(r =>
                            r.save_tag_id == save.id &&
                            r.save_tag_id != r.save_id &&
                            r.is_owner_same == false &&
                            r.is_same.Value &&
                            (r.is_same_approve || r.is_like_approve)).ToList();
                        // Check other request number is similar this request number -> If true goto document
                        if (save_tag_list.Count() > 0) {
                            foreach (var save_tag in save_tag_list) {
                                var save_tag_ir = new Save010CheckingTagSimilarInstructionRule() {
                                    save010_checking_tag_similar_id = save_tag.id,
                                    post_round_instruction_rule_code = save_tag.is_same_approve ? PostRoundInstructionRuleCodeConstant.ROLE_13_1.ToString() : PostRoundInstructionRuleCodeConstant.ROLE_13_2.ToString(),
                                    post_round_type_code = PostRoundTypeCodeConstant.SEND_MORE.ToString(),
                                    //
                                    document_role04_receive_date = DateTime.Now,
                                    document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.DRAFT.ToString(),
                                };

                                save_tag_ir_repo.Add(save_tag_ir);
                            }
                        }
                        //}
                    }
                }

                uow.SaveChanges();
            }

            return view_list;
        }
        [LogAopInterceptor]
        public virtual List<vPublicRegisterView> PublicRole04DocumentSend(List<vPublicRegisterAddModel> payload) {
            var view_list = new List<vPublicRegisterView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var save = save_repo.Get(model.id.Value);

                    save.public_role04_document_status_code = PublicRole04DocumentStatusCodeConstant.DONE.ToString();
                    save.public_role04_document_date = DateTime.Now;
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vPublicRegisterView> PublicRole02DocumentSend(List<vPublicRegisterAddModel> payload) {
            var view_list = new List<vPublicRegisterView>();
            int book_expired_day = 60;

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var repo = uow.GetRepository<PostRound>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var post_round = repo.Query(r =>
                    r.object_id == model.id.Value &&
                    r.post_round_instruction_rule_code == PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString() &&
                    r.post_round_type_code == PostRoundTypeCodeConstant.REGISTERED.ToString()).FirstOrDefault();

                    post_round.book_by = userId;
                    post_round.book_number = HelpService.GetBookNumber(uowProvider);
                    post_round.book_start_date = DateTime.Now;
                    post_round.book_end_date = DateTime.Now.AddDays(book_expired_day);
                    post_round.book_expired_day = book_expired_day;

                    //post_round.document_role02_print_cover_status_code = DocumentRole02PrintCoverStatusCodeConstant.WAIT_COVER.ToString();
                    post_round.document_role02_receive_date = model.public_role02_document_date.HasValue ? model.public_role02_document_date : DateTime.Now;
                    post_round.document_role02_print_document_status_code = DocumentRole02PrintDocumentStatusCodeConstant.DRAFT.ToString();
                    //post_round.document_role02_print_document_date = model.public_role02_document_date.HasValue ? model.public_role02_document_date : DateTime.Now;

                    var save_repo = uow.GetRepository<Save010>();
                    var save_entity = save_repo.Get(model.id.Value);
                    save_entity.public_role02_document_status_code = PublicRole02DocumentStatusCodeConstant.DONE.ToString();
                    save_repo.Update(save_entity);

                    post_round.post_round_action_post_status_code = PostRoundActionPostStatusCodeConstant.WAIT.ToString();

                    repo.Update(post_round);
                    //var entity = payment_repo.Get(model.id.Value);

                    //if (entity.public_role02_document_payment_status_code != PublicRole02DocumentPaymentStatusCodeConstant.WAIT_2.ToString()) {
                    //    var post_round_repo = uow.GetRepository<PostRound>();
                    //    post_round_repo.Add(new PostRound() {
                    //        post_round_instruction_rule_code = PostRoundInstructionRuleCodeConstant.RULE_5_3.ToString(),
                    //        post_round_type_code = PostRoundTypeCodeConstant.INFORM_PAYMENT.ToString(),
                    //        object_id = entity.save_id,
                    //        round_index = 1,
                    //        book_by = userId,
                    //        book_number = GetBookNumber(),
                    //        book_start_date = DateTime.Now,


                }

                uow.SaveChanges();
            }

            return view_list;
        }

        internal void ForcePublic(long save_id, PublicTypeCodeConstant type, PublicSourceCodeConstant source, bool is_force_public = false) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var save_repo = uow.GetRepository<Save010>();

                var save_tag_repo = uow.GetRepository<Save010CheckingTagSimilar>();
                var save_tag_ir_repo = uow.GetRepository<Save010CheckingTagSimilarInstructionRule>();

                var save_tag_list = save_tag_repo.Query(r =>
                     r.save_tag_id == save_id &&
                     r.save_tag_id != r.save_id &&
                     r.is_owner_same == false &&
                     r.is_same.Value &&
                     (r.is_same_approve || r.is_like_approve)).ToList();
                // Check other request number is similar this request number -> If true goto document
                if (save_tag_list.Count() > 0) {
                    foreach (var save_tag in save_tag_list) {
                        var save_tag_ir = new Save010CheckingTagSimilarInstructionRule() {
                            save010_checking_tag_similar_id = save_tag.id,
                            post_round_instruction_rule_code = save_tag.is_same_approve ? PostRoundInstructionRuleCodeConstant.ROLE_20_1.ToString() : PostRoundInstructionRuleCodeConstant.ROLE_20_2.ToString(),
                            post_round_type_code = PostRoundTypeCodeConstant.SEND_MORE.ToString(),
                            //
                            document_role04_receive_date = DateTime.Now,
                            document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.DRAFT.ToString(),
                        };

                        save_tag_ir_repo.Add(save_tag_ir);
                    }
                }

                if (save_tag_list.Count() == 0 || is_force_public) {
                    var save = save_repo.Get(save_id);
                    save.public_source_code = source.ToString();
                    save.public_type_code = type.ToString();
                    save.public_status_code = PublicStatusCodeConstant.WAIT.ToString();
                    save_repo.Update(save);
                }

                uow.SaveChanges();
            }
        }


        [LogAopInterceptor]
        public PagePublicOtherView PublicOtherLoad(long payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var create = uow.GetRepository<vPublicOther>();
                var view = new PagePublicOtherView();

                view.public_other = mapper.Map<vPublicOtherView>(create.Get(payload));

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(view.public_other.save_id);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(save.id);

                var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                var result = view_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = save.id.ToString(),
                          }
                    }, filter_queries = new List<string>() {
                        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                    }
                });
                view.instruction_rule_list = result.data.list;

                var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                var document = document_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          }
                     }
                });
                view.document_list = document.data.list;

                var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                var receipt_item = receipt_item_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = save.id.ToString(),
                          }
                     }
                });
                view.receipt_item_list = receipt_item.data.list;

                var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                var history = history_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = save.id.ToString(),
                          }
                     }
                });
                view.history_list = history.data.list;

                var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                var scan = scan_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          },
                          new SearchByModel() {
                              key = "file_id",
                              value = "0",
                              operation= Operation.GreaterThan,
                          }
                     }
                });
                view.document_scan_list = scan.data.list;

                return view;
            }
        }


        [LogAopInterceptor]
        public PagePublicConsideringOtherView PublicConsideringOtherLoad(long payload) {
            using (var uow = uowProvider.CreateUnitOfWork()) {
                var create = uow.GetRepository<vPublicConsideringOther>();
                var view = new PagePublicConsideringOtherView();

                view.public_considering_other = mapper.Map<vPublicConsideringOtherView>(create.Get(payload));

                var save_repo = uow.GetRepository<Save010>();
                var save = save_repo.Get(view.public_considering_other.save_id);
                view.view = mapper.Map<Save010ProcessView>(save);

                var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
                view.view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(save.id);

                var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
                var result = view_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = view.public_considering_other.save_id.ToString(),
                          }
                    }, filter_queries = new List<string>() {
                        "instruction_rule_code.StartsWith(\"CASE_\") || instruction_rule_code.StartsWith(\"ROLE_\")"
                    }
                });
                view.instruction_rule_list = result.data.list;

                var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
                var document = document_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          }
                     }
                });
                view.document_list = document.data.list;

                var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
                var receipt_item = receipt_item_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = view.public_considering_other.save_id.ToString(),
                          }
                     }
                });
                view.receipt_item_list = receipt_item.data.list;

                var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
                var history = history_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "save_id",
                              value = view.public_considering_other.save_id.ToString(),
                          }
                     }
                });
                view.history_list = history.data.list;

                var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
                var scan = scan_service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                          new SearchByModel() {
                              key = "request_number",
                              value = view.view.request_number,
                          },
                          new SearchByModel() {
                              key = "file_id",
                              value = "0",
                              operation= Operation.GreaterThan,
                          }
                     }
                });
                view.document_scan_list = scan.data.list;

                return view;
            }
        }


        [LogAopInterceptor]
        public virtual List<vPublicOtherView> PublicRole05OtherSend(List<vPublicOtherAddModel> payload) {
            var view_list = new List<vPublicOtherView>();
            //int book_expired_day = 60;

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var post_round_repo = uow.GetRepository<PostRound>();
                var instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var post_round = post_round_repo.Get(model.id.Value);
                    post_round.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.SEND.ToString();
                    post_round.document_role05_receive_send_date = DateTime.Now;

                    post_round.document_role02_receive_status_code = DocumentRole02ReceiveStatusCodeConstant.DRAFT.ToString();
                    post_round_repo.Update(post_round);

                    var instruction_rule = instruction_rule_repo.Get(model.post_round_instruction_rule_id);
                    instruction_rule.value_01 = model.value_01;
                    instruction_rule.value_02 = model.value_02;
                    instruction_rule.value_03 = model.value_03;
                    instruction_rule.value_04 = model.value_04;
                    instruction_rule.value_05 = model.value_05;
                    instruction_rule_repo.Update(instruction_rule);
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vPublicOtherView> PublicRole02OtherSend(List<vPublicOtherAddModel> payload) {
            var view_list = new List<vPublicOtherView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var post_round_repo = uow.GetRepository<PostRound>();
                var instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var post_round = post_round_repo.Get(model.post_round_id);
                    post_round.document_role02_receiver_by = userId;
                    post_round.document_role02_receive_date = DateTime.Now;
                    post_round.document_role02_receive_status_code = DocumentRole02ReceiveStatusCodeConstant.SEND.ToString();
                    post_round.document_role02_print_document_status_code = DocumentRole02PrintDocumentStatusCodeConstant.DRAFT.ToString();
                    post_round_repo.Update(post_round);

                    if (post_round.post_round_instruction_rule_code == PostRoundInstructionRuleCodeConstant.RULE_5_8_FEE.ToString()) {
                        var receipt_service = new ReceiptService(configuration, uowProvider, mapper);
                        var receipt = receipt_service.Add(post_round, model.name, Convert.ToDecimal(model.value_01));
                    }
                }

                uow.SaveChanges();
            }

            return view_list;
        }


        [LogAopInterceptor]
        public virtual List<vPublicConsideringOtherView> PublicRole02ConsideringOtherSend(List<vPublicConsideringOtherAddModel> payload) {
            var view_list = new List<vPublicConsideringOtherView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var document_service = new DocumentProcessService(configuration, uowProvider, mapper);
                var post_round_repo = uow.GetRepository<PostRound>();
                var instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    if (model.post_round_type_code == PostRoundTypeCodeConstant.PUBLIC_OTHER_CANCEL.ToString()) {
                        var post_round = post_round_repo.Query(r => r.object_id == model.save_id && r.post_round_instruction_rule_code == PostRoundInstructionRuleCodeConstant.RULE_9_11_CANCEL_PUBLIC.ToString()).FirstOrDefault();
                        if (post_round == null) {
                            post_round = document_service.ForceIntructionRuleAdd(model.save_id, PostRoundInstructionRuleCodeConstant.RULE_9_11_CANCEL_PUBLIC.ToString(), PostRoundTypeCodeConstant.PUBLIC_OTHER_CANCEL, 1, model.value_01, model.value_02, model.value_03, model.value_04, model.value_05, false, null);
                        } else {
                            var instruction_rule = instruction_rule_repo.Query(r => r.post_round_id == post_round.id).FirstOrDefault();
                            instruction_rule.value_01 = model.value_01;
                            instruction_rule.value_02 = model.value_02;
                            instruction_rule.value_03 = model.value_03;
                            instruction_rule.value_04 = model.value_04;
                            instruction_rule.value_05 = model.value_05;
                            instruction_rule_repo.Update(instruction_rule);
                        }

                        post_round.document_role04_receive_status_code = DocumentRole04ReceiveStatusCodeConstant.SEND_WAIT.ToString();
                        post_round.document_role04_receive_date = DateTime.Now;
                        post_round.document_role04_receive_send_date = DateTime.Now;
                        post_round.document_role04_receiver_by = userId;

                        post_round.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.DRAFT.ToString();
                        post_round_repo.Update(post_round);
                    }
                }

                uow.SaveChanges();
            }

            return view_list;
        }

        [LogAopInterceptor]
        public virtual List<vPublicOtherView> PublicRole02OtherSendChange(List<vPublicOtherAddModel> payload) {
            var view_list = new List<vPublicOtherView>();

            using (var uow = uowProvider.CreateUnitOfWork()) {
                var post_round_repo = uow.GetRepository<PostRound>();
                var instruction_rule_repo = uow.GetRepository<PostRoundInstructionRule>();

                foreach (var model in payload.Where(r => r.is_check)) {
                    var post_round = post_round_repo.Get(model.post_round_id);
                    post_round.document_role05_receive_status_code = DocumentRole05ReceiveStatusCodeConstant.DRAFT_FIX.ToString();

                    post_round.document_role02_receiver_by = userId;
                    post_round.document_role02_receive_date = DateTime.Now;
                    post_round.document_role02_receive_status_code = DocumentRole02ReceiveStatusCodeConstant.WAIT_CHANGE.ToString();
                    post_round_repo.Update(post_round);
                }

                uow.SaveChanges();
            }

            return view_list;
        }
    }
}
