﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class Request01Process
    {
        public Request01Process()
        {
            Request01Item = new HashSet<Request01Item>();
        }

        public long id { get; set; }
        public DateTime? request_date { get; set; }
        public string requester_name { get; set; }
        public string telephone { get; set; }
        public bool? is_via_post { get; set; }
        public bool? is_wave_fee { get; set; }
        public bool? is_commercial_affairs_province { get; set; }
        public string commercial_affairs_province_code { get; set; }
        public int? item_count { get; set; }
        public decimal? total_price { get; set; }
        public string source_code { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual ICollection<Request01Item> Request01Item { get; set; }
    }
}