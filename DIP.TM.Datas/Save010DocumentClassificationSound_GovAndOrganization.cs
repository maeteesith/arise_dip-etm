﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class Save010DocumentClassificationSound_GovAndOrganization
    {
        public long id { get; set; }
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string document_classification_sound_code { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual RM_Save010DocumentClassificationSoundType document_classification_sound_codeNavigation { get; set; }
        public virtual Save010_GovAndOrganization save_ { get; set; }
    }
}