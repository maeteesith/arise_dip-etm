﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class PublicRoundItem
    {
        public long id { get; set; }
        public long? public_round_id { get; set; }
        public long? line_index { get; set; }
        public string request_number { get; set; }
        public string request_item_type_code { get; set; }
        public string sound_mark_list { get; set; }
        public long? sound_mark_file_id { get; set; }
        public long? trademark_2d_file_id { get; set; }
        public string remark { get; set; }
        public long? save_id { get; set; }
        public string request_item_sub_type_1_code { get; set; }
        public string request_item_sub_status_code { get; set; }
        public string cancel_reason { get; set; }
        public long? cancel_by { get; set; }
        public string cancel_by_name { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual PublicRound public_round_ { get; set; }
    }
}