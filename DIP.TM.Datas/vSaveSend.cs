﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class vSaveSend
    {
        public long id { get; set; }
        public DateTime? request_date { get; set; }
        public DateTime? make_date { get; set; }
        public string request_number { get; set; }
        public DateTime? department_send_date { get; set; }
        public long? file_id { get; set; }
        public string request_type_code { get; set; }
        public string request_type_name { get; set; }
        public string save_status_code { get; set; }
        public string save_status_name { get; set; }
        public string request_source_code { get; set; }
        public string request_source_name { get; set; }
        public string department_send_code { get; set; }
        public string department_send_name { get; set; }
        public string evidence_address { get; set; }
        public bool is_deleted { get; set; }
        public long created_by { get; set; }
        public DateTime created_date { get; set; }
        public long? updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }
}