﻿using DIP.TM.Datas.Attributes;
using DIP.TM.Uows.DataAccess.Entities;

namespace DIP.TM.Datas.EntityBases
{

    [IgnoreWhereSoftDeleteAttributes]
    public class SaveProductEntityBase : EntityBase {
        public long? save_id { get; set; }
        public string request_item_sub_type_1_code { get; set; }
        public string description { get; set; }
    }
}
