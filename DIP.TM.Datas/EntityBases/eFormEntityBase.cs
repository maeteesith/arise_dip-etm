﻿using DIP.TM.Datas.Attributes;
using DIP.TM.Uows.DataAccess.Entities;
using System;

namespace DIP.TM.Datas.EntityBases
{
    [IgnoreWhereSoftDeleteAttributes]
    public class eFormEntityBase : EntityBase {
        public DateTime? make_date { get; set; }
        public string eform_number { get; set; }
        public string email { get; set; }
        public string email_uuid { get; set; }
        public string telephone { get; set; }
        public string request_number { get; set; }
    }
}
