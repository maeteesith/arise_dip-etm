﻿using DIP.TM.Datas.Attributes;
using DIP.TM.Uows.DataAccess.Entities;
using System;

namespace DIP.TM.Datas.EntityBases {
    [IgnoreWhereSoftDeleteAttributes]
    public class MadridEntityBase : EntityBase {
        public long id { get; set; }
        public bool is_deleted { get; set; }
    }
}
