﻿using DIP.TM.Datas.Attributes;
using DIP.TM.Uows.DataAccess.Entities;
using System;

namespace DIP.TM.Datas.EntityBases {
    [IgnoreWhereSoftDeleteAttributes]
    public class DocumentRole03ChangeItemEntityBase : EntityBase {
        public long? document_role03_item_id { get; set; }
        public bool is_deleted { get; set; }
    }
}
