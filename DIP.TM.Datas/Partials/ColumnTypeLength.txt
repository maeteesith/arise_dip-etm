﻿select 'ALTER TABLE [' + t.TABLE_NAME + '] ALTER COLUMN [' + column_name + '] nvarchar(1000);', * 
from INFORMATION_SCHEMA.COLUMNS  t
where 
1=1
--and column_name like 'cancel_reason%' 
--and t.CHARACTER_MAXIMUM_LENGTH is null

and t.CHARACTER_MAXIMUM_LENGTH is not null
and t.CHARACTER_MAXIMUM_LENGTH != 1000
and t.CHARACTER_MAXIMUM_LENGTH != 500
and t.CHARACTER_MAXIMUM_LENGTH > 0

and t.COLUMN_NAME not like '%code'
and t.TABLE_NAME not like 'v%'
order by t.COLUMN_NAME, t.TABLE_NAME;

