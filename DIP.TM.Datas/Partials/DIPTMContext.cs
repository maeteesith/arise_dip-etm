﻿using DIP.TM.Datas.Attributes;
using DIP.TM.Datas.EntityBases;
using DIP.TM.Datas.Extensions;
using DIP.TM.Uows.DataAccess.Context;
using DIP.TM.Uows.DataAccess.Entities;
using DIP.TM.Utils.Extensions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace DIP.TM.Datas
{
    public partial class DIPTMContext : EntityContextBase<DIPTMContext> {
        private readonly long? userId;
        private List<Type> _typeBaseClasses = new List<Type>()
        {
            typeof(EntityBase),
            typeof(SaveEntityBase),
            typeof(eFormEntityBase),
            typeof(SaveProductEntityBase)
        };

        public DIPTMContext(DbContextOptions<DIPTMContext> options) : base(options) {
           userId = HttpContext.Current?.User?.UserId();
        }

        private readonly MethodInfo SetGlobalQueryMethod = typeof(DIPTMContext).GetMethods(BindingFlags.Public | BindingFlags.Instance)
                                                        .Single(t => t.IsGenericMethod && t.Name == "SetGlobalQuery");
        private IList<Type> _entityTypeCache;
        private IEnumerable<Assembly> GetReferencingAssemblies()
        {
            var assemblies = new List<Assembly>();
            var dependencies = DependencyContext.Default.RuntimeLibraries;

            foreach (var library in dependencies)
            {
                try
                {
                    var assembly = Assembly.Load(new AssemblyName(library.Name));
                    assemblies.Add(assembly);
                }
                catch (FileNotFoundException ex)
                {
                    // TODO
                    //throw ex;
                }
            }
            var dllDatas= assemblies.Where(x=>x.FullName.Contains("DIP.TM.Datas")).ToList();

            return dllDatas;
        }

        private IList<Type> GetEntityTypes()
        {
            if (_entityTypeCache != null)
            {
                return _entityTypeCache.ToList();
            }

            var assemblies = GetReferencingAssemblies();
            _entityTypeCache = (from a in assemblies
                                from t in a.DefinedTypes
                                where _typeBaseClasses.Contains(t.BaseType)// t.BaseType == typeof(EntityBase) || t.BaseType == typeof(SaveEntityBase)
                                select t.AsType()).ToList();



            return _entityTypeCache;
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            foreach (var type in GetEntityTypes())
            {
                var att = type.GetCustomAttribute(typeof(IgnoreWhereSoftDeleteAttributes), false);
                if (ReferenceEquals(att, null)) {
                    var method = SetGlobalQueryMethod.MakeGenericMethod(type);
                    method.Invoke(this, new object[] { modelBuilder });
                }
            }

            base.OnModelCreating(modelBuilder);
        }

        public void SetGlobalQuery<T>(ModelBuilder builder) where T : EntityBase
        {
            try
            {

                builder.Entity<T>().HasQueryFilter(e => !e.is_deleted);
            }
            catch (Exception ex)
            {
                // TODO Handle exception
                throw ex;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges() {
            var addedEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Added).ToList();

            addedEntities.ForEach(e => {

                e.Property("created_date").CurrentValue = DateTime.Now;
                e.Property("updated_date").CurrentValue = DateTime.Now;
                e.Property("is_deleted").CurrentValue = false;
                e.Property("created_by").CurrentValue = this.userId.HasValue ? this.userId : 0;
                e.Property("updated_by").CurrentValue = this.userId;
            });

            var editedEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified).ToList();

            editedEntities.ForEach(e => {
                e.Property("updated_date").CurrentValue = DateTime.Now;
                e.Property("updated_by").CurrentValue = this.userId;
            });


            addedEntities.ForEach(e => {
                if (e.Property("id").CurrentValue.ToPrimaryKey() > 0) {
                    e.State = EntityState.Modified;
                    e.Property("updated_date").CurrentValue = DateTime.Now;
                    e.Property("updated_by").CurrentValue = this.userId;
                } else {
                    e.State = EntityState.Added;
                    e.Property("is_deleted").CurrentValue = false;
                    e.Property("created_date").CurrentValue = DateTime.Now;
                    e.Property("updated_date").CurrentValue = DateTime.Now;
                    e.Property("created_by").CurrentValue = this.userId.HasValue ? this.userId : 0;
                    e.Property("updated_by").CurrentValue = this.userId;
                }
            });

            var recordsToValidate = ChangeTracker.Entries();
            foreach (var recordToValidate in recordsToValidate)
            {
                var entity = recordToValidate.Entity;
                var validationContext = new ValidationContext(entity);
                var results = new List<ValidationResult>();
                if (!Validator.TryValidateObject(entity, validationContext, results, true))
                {
                    var messages = results.Select(r => r.ErrorMessage).ToList().Aggregate((message, nextMessage) => message + ", " + nextMessage);
                    throw new ApplicationException($"Unable to save changes for {entity.GetType().FullName} due to error(s): {messages}");
                }
            }


            return base.SaveChanges();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="acceptAllChangesOnSuccess"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default(CancellationToken)) {
            var addedEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Added).ToList();

            addedEntities.ForEach(e => {
                e.Property("created_date").CurrentValue = DateTime.Now;
                e.Property("updated_date").CurrentValue = DateTime.Now;
                e.Property("is_deleted").CurrentValue = false;
                e.Property("created_by").CurrentValue = this.userId;
            });

            var editedEntities = ChangeTracker.Entries().Where(e => e.State == EntityState.Modified).ToList();

            editedEntities.ForEach(e => {
                e.Property("updated_date").CurrentValue = DateTime.Now;
                e.Property("updated_by").CurrentValue = this.userId;
            });

            addedEntities.ForEach(e => {
                if (e.Property("id").CurrentValue.ToPrimaryKey() > 0) {
                    e.State = EntityState.Modified;
                    e.Property("updated_by").CurrentValue = this.userId;
                } else {
                    e.State = EntityState.Added;
                    e.Property("is_deleted").CurrentValue = false;
                    e.Property("created_date").CurrentValue = DateTime.Now;
                    e.Property("updated_date").CurrentValue = DateTime.Now;
                    e.Property("created_by").CurrentValue = this.userId;
                    e.Property("updated_by").CurrentValue = this.userId;
                }
            });


            var recordsToValidate = ChangeTracker.Entries();
            foreach (var recordToValidate in recordsToValidate)
            {
                var entity = recordToValidate.Entity;
                var validationContext = new ValidationContext(entity);
                var results = new List<ValidationResult>();
                if (!Validator.TryValidateObject(entity, validationContext, results, true))
                {
                    var messages = results.Select(r => r.ErrorMessage).ToList().Aggregate((message, nextMessage) => message + ", " + nextMessage);
                    throw new ApplicationException($"Unable to save changes for {entity.GetType().FullName} due to error(s): {messages}");
                }
            }

            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        //public static async Task<int> SaveChangesWithValidationAsync(this DbContext context)
        //{
        //    var recordsToValidate = context.ChangeTracker.Entries();
        //    foreach (var recordToValidate in recordsToValidate)
        //    {
        //        var entity = recordToValidate.Entity;
        //        var validationContext = new ValidationContext(entity);
        //        var results = new List<ValidationResult>();
        //        if (!Validator.TryValidateObject(entity, validationContext, results, true))
        //        {
        //            var messages = results.Select(r => r.ErrorMessage).ToList().Aggregate((message, nextMessage) => message + ", " + nextMessage);
        //            throw new ApplicationException($"Unable to save changes for {entity.GetType().FullName} due to error(s): {messages}");
        //        }
        //    }
        //    return await context.SaveChangesAsync();
        //}

        //public static int SaveChangesWithValidation(this DbContext context)
        //{
        //    var recordsToValidate = context.ChangeTracker.Entries();
        //    foreach (var recordToValidate in recordsToValidate)
        //    {
        //        var entity = recordToValidate.Entity;
        //        var validationContext = new ValidationContext(entity);
        //        var results = new List<ValidationResult>();
        //        if (!Validator.TryValidateObject(entity, validationContext, results, true))
        //        {
        //            var messages = results.Select(r => r.ErrorMessage).ToList().Aggregate((message, nextMessage) => message + ", " + nextMessage);
        //            throw new ApplicationException($"Unable to save changes for {entity.GetType().FullName} due to error(s): {messages}");
        //        }
        //    }
        //    return context.SaveChanges();
        //}
    }
}
