﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class dSave010DocumentClassificationSearchMethod
    {
        public long id { get; set; }
        public long? save_id { get; set; }
        public string search_method { get; set; }
        public string keyword_1 { get; set; }
        public string keyword_2 { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual Save010 save_ { get; set; }
    }
}