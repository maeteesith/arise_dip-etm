﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class eForm_Save040
    {
        public eForm_Save040()
        {
            eForm_Save040AddressContact = new HashSet<eForm_Save040AddressContact>();
            eForm_Save040AddressPeople = new HashSet<eForm_Save040AddressPeople>();
            eForm_Save040AddressRepresentative = new HashSet<eForm_Save040AddressRepresentative>();
            eForm_Save040Product = new HashSet<eForm_Save040Product>();
            eForm_Save040ReceiverAddressContact = new HashSet<eForm_Save040ReceiverAddressContact>();
            eForm_Save040ReceiverAddressPeople = new HashSet<eForm_Save040ReceiverAddressPeople>();
            eForm_Save040ReceiverAddressRepresentative = new HashSet<eForm_Save040ReceiverAddressRepresentative>();
        }

        public long id { get; set; }
        public DateTime? make_date { get; set; }
        public string eform_number { get; set; }
        public string email { get; set; }
        public string email_uuid { get; set; }
        public string telephone { get; set; }
        public string request_number { get; set; }
        public string registration_number { get; set; }
        public string payer_name { get; set; }
        public decimal? total_price { get; set; }
        public string save040_transfer_type_code { get; set; }
        public string save040_contact_type_code { get; set; }
        public string save040_receiver_contact_type_code { get; set; }
        public string save040_transfer_form_code { get; set; }
        public string save040_transfer_part_code { get; set; }
        public bool? is_9_1 { get; set; }
        public bool? is_9_2 { get; set; }
        public bool? is_9_3 { get; set; }
        public bool? is_9_4 { get; set; }
        public bool? is_9_5 { get; set; }
        public bool? is_9_6 { get; set; }
        public bool? is_9_7 { get; set; }
        public bool? is_9_8 { get; set; }
        public bool? is_9_9 { get; set; }
        public bool? is_9_10 { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string rule_number { get; set; }
        public string save040_submit_type_code { get; set; }
        public string save040_representative_condition_type_code { get; set; }
        public string save040_receiver_representative_condition_type_code { get; set; }
        public string save040_transfer_request_type_code { get; set; }
        public string contract_ref_number { get; set; }
        public string sign_inform_receiver_person_list { get; set; }
        public string sign_inform_receiver_representative_list { get; set; }
        public string wizard { get; set; }

        public virtual RM_AddressRepresentativeConditionTypeCode save040_receiver_representative_condition_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save040_representative_condition_type_codeNavigation { get; set; }
        public virtual RM_Save040SubmitType save040_submit_type_codeNavigation { get; set; }
        public virtual RM_Save040TransferForm save040_transfer_form_codeNavigation { get; set; }
        public virtual RM_Save040TransferPart save040_transfer_part_codeNavigation { get; set; }
        public virtual RM_Save040TransferType save040_transfer_type_codeNavigation { get; set; }
        public virtual ICollection<eForm_Save040AddressContact> eForm_Save040AddressContact { get; set; }
        public virtual ICollection<eForm_Save040AddressPeople> eForm_Save040AddressPeople { get; set; }
        public virtual ICollection<eForm_Save040AddressRepresentative> eForm_Save040AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save040Product> eForm_Save040Product { get; set; }
        public virtual ICollection<eForm_Save040ReceiverAddressContact> eForm_Save040ReceiverAddressContact { get; set; }
        public virtual ICollection<eForm_Save040ReceiverAddressPeople> eForm_Save040ReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save040ReceiverAddressRepresentative> eForm_Save040ReceiverAddressRepresentative { get; set; }
    }
}