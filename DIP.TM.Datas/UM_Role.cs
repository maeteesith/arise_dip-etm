﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class UM_Role
    {
        public UM_Role()
        {
            UM_RolePagePolicy = new HashSet<UM_RolePagePolicy>();
            UM_UserRole = new HashSet<UM_UserRole>();
        }

        public long id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual ICollection<UM_RolePagePolicy> UM_RolePagePolicy { get; set; }
        public virtual ICollection<UM_UserRole> UM_UserRole { get; set; }
    }
}