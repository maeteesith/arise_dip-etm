﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class UM_SSO_User
    {
        public long id { get; set; }
        public long user_sso_id { get; set; }
        public long? user_id { get; set; }
        public string access_token { get; set; }
        public string refresh_token { get; set; }
        public long expires_in { get; set; }
        public string data_json { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long? created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual UM_User user_ { get; set; }
    }
}