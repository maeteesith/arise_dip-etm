﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class Save010InstructionRuleRequestNumber
    {
        public long id { get; set; }
        public long? instruction_rule_id { get; set; }
        public string request_number { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
    }
}