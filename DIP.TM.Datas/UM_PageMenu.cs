﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class UM_PageMenu
    {
        public UM_PageMenu()
        {
            UM_PageMenuSub = new HashSet<UM_PageMenuSub>();
        }

        public long id { get; set; }
        public string name { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual ICollection<UM_PageMenuSub> UM_PageMenuSub { get; set; }
    }
}