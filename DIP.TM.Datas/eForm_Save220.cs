﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class eForm_Save220
    {
        public eForm_Save220()
        {
            eForm_Save220AddressContact = new HashSet<eForm_Save220AddressContact>();
            eForm_Save220AddressPeople = new HashSet<eForm_Save220AddressPeople>();
            eForm_Save220AddressRepresentative = new HashSet<eForm_Save220AddressRepresentative>();
            eForm_Save220CheckingSimilarWordTranslate = new HashSet<eForm_Save220CheckingSimilarWordTranslate>();
            eForm_Save220Kor10 = new HashSet<eForm_Save220Kor10>();
            eForm_Save220Product = new HashSet<eForm_Save220Product>();
        }

        public long id { get; set; }
        public DateTime? make_date { get; set; }
        public string eform_number { get; set; }
        public string email { get; set; }
        public string email_uuid { get; set; }
        public string telephone { get; set; }
        public string request_number { get; set; }
        public string registration_number { get; set; }
        public string payer_name { get; set; }
        public decimal? total_price { get; set; }
        public string inter_registration_number { get; set; }
        public DateTime? inter_registration_cancel_date { get; set; }
        public DateTime? inter_registration_date { get; set; }
        public DateTime? protection_date { get; set; }
        public DateTime? case_28_date { get; set; }
        public string save220_informer_type_code { get; set; }
        public string save220_representative_condition_type_code { get; set; }
        public string save220_contact_type_code { get; set; }
        public string save220_img_type_type_code { get; set; }
        public long? img_file_2d_id { get; set; }
        public long? img_file_3d_id_1 { get; set; }
        public long? img_file_3d_id_2 { get; set; }
        public long? img_file_3d_id_3 { get; set; }
        public long? img_file_3d_id_4 { get; set; }
        public long? img_file_3d_id_5 { get; set; }
        public long? img_file_3d_id_6 { get; set; }
        public int? img_w { get; set; }
        public int? img_h { get; set; }
        public string remark_5_1_3 { get; set; }
        public bool? is_sound_mark { get; set; }
        public bool? is_sound_mark_human { get; set; }
        public bool? is_sound_mark_animal { get; set; }
        public bool? is_sound_mark_sound { get; set; }
        public bool? is_sound_mark_other { get; set; }
        public string remark_5_2_2 { get; set; }
        public long? sound_file_id { get; set; }
        public long? sound_jpg_file_id { get; set; }
        public bool? is_color_protect { get; set; }
        public string remark_8 { get; set; }
        public bool? is_model_protect { get; set; }
        public string remark_9 { get; set; }
        public bool? is_before_request { get; set; }
        public bool? is_14_1 { get; set; }
        public bool? is_14_2 { get; set; }
        public bool? is_14_3 { get; set; }
        public bool? is_14_4 { get; set; }
        public bool? is_14_5 { get; set; }
        public bool? is_14_6 { get; set; }
        public bool? is_14_7 { get; set; }
        public bool? is_14_8 { get; set; }
        public bool? is_14_9 { get; set; }
        public bool? is_14_10 { get; set; }
        public bool? is_14_11 { get; set; }
        public decimal? transfer_fee { get; set; }
        public decimal? product_fee { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public string wizard { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual RM_RequestImageMarkType save220_img_type_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save220_representative_condition_type_codeNavigation { get; set; }
        public virtual File sound_file_ { get; set; }
        public virtual File sound_jpg_file_ { get; set; }
        public virtual ICollection<eForm_Save220AddressContact> eForm_Save220AddressContact { get; set; }
        public virtual ICollection<eForm_Save220AddressPeople> eForm_Save220AddressPeople { get; set; }
        public virtual ICollection<eForm_Save220AddressRepresentative> eForm_Save220AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save220CheckingSimilarWordTranslate> eForm_Save220CheckingSimilarWordTranslate { get; set; }
        public virtual ICollection<eForm_Save220Kor10> eForm_Save220Kor10 { get; set; }
        public virtual ICollection<eForm_Save220Product> eForm_Save220Product { get; set; }
    }
}