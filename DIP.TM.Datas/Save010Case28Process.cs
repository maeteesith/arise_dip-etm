﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class Save010Case28Process
    {
        public long id { get; set; }
        public long? save_id { get; set; }
        public bool? is_considering_similar_kor10_1_1 { get; set; }
        public string considering_similar_kor10_1_1_description { get; set; }
        public bool? is_considering_similar_kor10_1_2 { get; set; }
        public string considering_similar_kor10_1_2_description { get; set; }
        public bool? is_considering_similar_kor10_1_4 { get; set; }
        public string considering_similar_kor10_1_4_description { get; set; }
        public bool? is_considering_similar_kor10_2_1 { get; set; }
        public bool? is_considering_similar_kor10_2_2 { get; set; }
        public bool? is_considering_similar_kor10_2_3 { get; set; }
        public bool? is_considering_similar_kor10_2_4 { get; set; }
        public bool? is_considering_similar_kor10_2_5 { get; set; }
        public bool? is_considering_similar_kor10_2_6 { get; set; }
        public bool? is_considering_similar_kor10_3 { get; set; }
        public string considering_similar_kor10_3_description { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
    }
}