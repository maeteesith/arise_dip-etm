﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class Save010CheckingSimilarWordTranslate
    {
        public long id { get; set; }
        public long? save_id { get; set; }
        public int? save_index { get; set; }
        public string word_translate_search { get; set; }
        public string word_translate_dictionary_code { get; set; }
        public string word_translate_dictionary_other { get; set; }
        public string word_translate_sound { get; set; }
        public string word_translate_translate { get; set; }
        public string checking_word_translate_status_code { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
        public long? word_translate_file_id { get; set; }

        public virtual RM_CheckingWordTranslateStatusCode checking_word_translate_status_codeNavigation { get; set; }
        public virtual Save010 save_ { get; set; }
    }
}