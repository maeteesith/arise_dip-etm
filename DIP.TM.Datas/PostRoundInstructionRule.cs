﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class PostRoundInstructionRule
    {
        public long id { get; set; }
        public long post_round_id { get; set; }
        public long save010_instruction_rule_id { get; set; }
        public string document_role02_check_status_code { get; set; }
        public DateTime? document_role02_check_date { get; set; }
        public string document_role04_send_type_code { get; set; }
        public DateTime? document_role04_check_date { get; set; }
        public string document_role04_check_remark { get; set; }
        public string new_instruction_rule_code { get; set; }
        public string value_01 { get; set; }
        public string value_02 { get; set; }
        public string value_03 { get; set; }
        public string value_04 { get; set; }
        public string value_05 { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
    }
}