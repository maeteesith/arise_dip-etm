﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class RM_Save030AppealReason
    {
        public RM_Save030AppealReason()
        {
            Save030 = new HashSet<Save030>();
        }

        public long id { get; set; }
        public int index { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual ICollection<Save030> Save030 { get; set; }
    }
}