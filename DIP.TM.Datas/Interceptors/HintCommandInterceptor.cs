﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using System.Data.Common;

namespace DIP.TM.Datas.Interceptors
{
    public class HintCommandInterceptor : DbCommandInterceptor
    {
        public override InterceptionResult<DbDataReader> ReaderExecuting(DbCommand command, CommandEventData eventData, InterceptionResult<DbDataReader> result)
        {
            // TODO Format SQL Speed up
            return base.ReaderExecuting(command, eventData, result);
        }
    }
}
