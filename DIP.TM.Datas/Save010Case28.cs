﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class Save010Case28
    {
        public long id { get; set; }
        public long? save_id { get; set; }
        public string request_item_sub_type_1_code { get; set; }
        public string address_country_code { get; set; }
        public DateTime? case_28_date { get; set; }
        public bool? is_allow_role_28_1 { get; set; }
        public bool? is_allow_role_28_2 { get; set; }
        public string allow_role_28_description { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual Save010 save_ { get; set; }
    }
}