﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class vDocumentRole04CreateFile
    {
        public long id { get; set; }
        public long document_role04_create_id { get; set; }
        public string remark { get; set; }
        public DateTime file_created_date { get; set; }
        public long? file_created_by { get; set; }
        public string file_created_by_name { get; set; }
        public long? file_id { get; set; }
        public string file_name { get; set; }
        public decimal? file_size { get; set; }
        public bool is_deleted { get; set; }
        public long created_by { get; set; }
        public DateTime created_date { get; set; }
        public long? updated_by { get; set; }
        public DateTime? updated_date { get; set; }
    }
}