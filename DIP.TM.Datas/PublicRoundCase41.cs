﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class PublicRoundCase41
    {
        public long id { get; set; }
        public long? save_id { get; set; }
        public long? maker_by { get; set; }
        public DateTime? make_date { get; set; }
        public string department_code { get; set; }
        public string public_case41_remark { get; set; }
        public string public_case41_additional_remark { get; set; }
        public string public_role01_case41_status_code { get; set; }
        public DateTime? public_role01_case41_date { get; set; }
        public long? public_role05_case41_by { get; set; }
        public string public_role05_case41_status_code { get; set; }
        public DateTime? public_role05_case41_date { get; set; }
        public string public_round_case41_status_code { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
    }
}