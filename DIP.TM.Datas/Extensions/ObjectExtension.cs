﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Datas.Extensions
{
    public static class ObjectExtension
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public static long ToPrimaryKey(this Object o)
        {
            return Convert.ToInt64(o);
        }

    }
}
