﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class RM_AddressCountry
    {
        public RM_AddressCountry()
        {
            DocumentRole03ChangeAddressContact = new HashSet<DocumentRole03ChangeAddressContact>();
            DocumentRole03ChangeAddressJoiner = new HashSet<DocumentRole03ChangeAddressJoiner>();
            DocumentRole03ChangeAddressPeople = new HashSet<DocumentRole03ChangeAddressPeople>();
            DocumentRole03ChangeAddressRepresentative = new HashSet<DocumentRole03ChangeAddressRepresentative>();
            RM_AddressProvince = new HashSet<RM_AddressProvince>();
            RS_Address = new HashSet<RS_Address>();
            Save010AddressContact = new HashSet<Save010AddressContact>();
            Save010AddressJoiner = new HashSet<Save010AddressJoiner>();
            Save010AddressPeople = new HashSet<Save010AddressPeople>();
            Save010AddressPeople_GovAndOrganization = new HashSet<Save010AddressPeople_GovAndOrganization>();
            Save010AddressPeople_InterPharmacy = new HashSet<Save010AddressPeople_InterPharmacy>();
            Save010AddressRepresentative = new HashSet<Save010AddressRepresentative>();
            Save020AddressContact = new HashSet<Save020AddressContact>();
            Save020AddressPeople = new HashSet<Save020AddressPeople>();
            Save020AddressRepresentative = new HashSet<Save020AddressRepresentative>();
            Save030AddressContact = new HashSet<Save030AddressContact>();
            Save030AddressPeople = new HashSet<Save030AddressPeople>();
            Save030AddressRepresentative = new HashSet<Save030AddressRepresentative>();
            Save040AddressContact = new HashSet<Save040AddressContact>();
            Save040AddressPeople = new HashSet<Save040AddressPeople>();
            Save040AddressRepresentative = new HashSet<Save040AddressRepresentative>();
            Save050AddressContact = new HashSet<Save050AddressContact>();
            Save050AddressPeople = new HashSet<Save050AddressPeople>();
            Save050AddressRepresentative = new HashSet<Save050AddressRepresentative>();
            Save050ReceiverAddressPeople = new HashSet<Save050ReceiverAddressPeople>();
            Save050ReceiverAddressRepresentative = new HashSet<Save050ReceiverAddressRepresentative>();
            Save060AddressAllowPeople = new HashSet<Save060AddressAllowPeople>();
            Save060AddressContact = new HashSet<Save060AddressContact>();
            Save060AddressContact06 = new HashSet<Save060AddressContact06>();
            Save060AddressJoiner = new HashSet<Save060AddressJoiner>();
            Save060AddressPeople = new HashSet<Save060AddressPeople>();
            Save060AddressRepresentative = new HashSet<Save060AddressRepresentative>();
            Save070AddressContact = new HashSet<Save070AddressContact>();
            Save080AddressContact = new HashSet<Save080AddressContact>();
            Save080AddressJoiner = new HashSet<Save080AddressJoiner>();
            Save080AddressJoinerRepresentative = new HashSet<Save080AddressJoinerRepresentative>();
            Save080AddressPeople = new HashSet<Save080AddressPeople>();
            Save080AddressRepresentative = new HashSet<Save080AddressRepresentative>();
            eForm_Save010AddressContact = new HashSet<eForm_Save010AddressContact>();
            eForm_Save010AddressJoiner = new HashSet<eForm_Save010AddressJoiner>();
            eForm_Save010AddressPeople = new HashSet<eForm_Save010AddressPeople>();
            eForm_Save010AddressRepresentative = new HashSet<eForm_Save010AddressRepresentative>();
            eForm_Save010AddressRepresentativeKor18 = new HashSet<eForm_Save010AddressRepresentativeKor18>();
            eForm_Save010Kor10AddressPeople = new HashSet<eForm_Save010Kor10AddressPeople>();
            eForm_Save010Kor10AddressRepresentative = new HashSet<eForm_Save010Kor10AddressRepresentative>();
            eForm_Save010Kor19AddressPeople = new HashSet<eForm_Save010Kor19AddressPeople>();
            eForm_Save010Kor19AddressRepresentative = new HashSet<eForm_Save010Kor19AddressRepresentative>();
            eForm_Save020AddressContact = new HashSet<eForm_Save020AddressContact>();
            eForm_Save020AddressPeople = new HashSet<eForm_Save020AddressPeople>();
            eForm_Save020AddressRepresentative = new HashSet<eForm_Save020AddressRepresentative>();
            eForm_Save021AddressContact = new HashSet<eForm_Save021AddressContact>();
            eForm_Save021AddressPeople = new HashSet<eForm_Save021AddressPeople>();
            eForm_Save021AddressRepresentative = new HashSet<eForm_Save021AddressRepresentative>();
            eForm_Save030AddressContact = new HashSet<eForm_Save030AddressContact>();
            eForm_Save030AddressPeople = new HashSet<eForm_Save030AddressPeople>();
            eForm_Save030AddressRepresentative = new HashSet<eForm_Save030AddressRepresentative>();
            eForm_Save040AddressContact = new HashSet<eForm_Save040AddressContact>();
            eForm_Save040AddressPeople = new HashSet<eForm_Save040AddressPeople>();
            eForm_Save040AddressRepresentative = new HashSet<eForm_Save040AddressRepresentative>();
            eForm_Save040ReceiverAddressContact = new HashSet<eForm_Save040ReceiverAddressContact>();
            eForm_Save040ReceiverAddressPeople = new HashSet<eForm_Save040ReceiverAddressPeople>();
            eForm_Save040ReceiverAddressRepresentative = new HashSet<eForm_Save040ReceiverAddressRepresentative>();
            eForm_Save050AddressContact = new HashSet<eForm_Save050AddressContact>();
            eForm_Save050AddressPeople = new HashSet<eForm_Save050AddressPeople>();
            eForm_Save050AddressPeopleChange = new HashSet<eForm_Save050AddressPeopleChange>();
            eForm_Save050AddressRepresentative = new HashSet<eForm_Save050AddressRepresentative>();
            eForm_Save050ReceiverAddressPeople = new HashSet<eForm_Save050ReceiverAddressPeople>();
            eForm_Save050ReceiverAddressRepresentative = new HashSet<eForm_Save050ReceiverAddressRepresentative>();
            eForm_Save060AddressContact = new HashSet<eForm_Save060AddressContact>();
            eForm_Save060AddressPeople = new HashSet<eForm_Save060AddressPeople>();
            eForm_Save060AddressPeopleChange = new HashSet<eForm_Save060AddressPeopleChange>();
            eForm_Save060AddressRepresentative = new HashSet<eForm_Save060AddressRepresentative>();
            eForm_Save060ChallengerAddressContact = new HashSet<eForm_Save060ChallengerAddressContact>();
            eForm_Save060ChallengerAddressPeople = new HashSet<eForm_Save060ChallengerAddressPeople>();
            eForm_Save060ChallengerAddressRepresentative = new HashSet<eForm_Save060ChallengerAddressRepresentative>();
            eForm_Save060ContractReceiverAddressPeople = new HashSet<eForm_Save060ContractReceiverAddressPeople>();
            eForm_Save060ContractReceiverAddressRepresentative = new HashSet<eForm_Save060ContractReceiverAddressRepresentative>();
            eForm_Save060LegacyAddressPeople = new HashSet<eForm_Save060LegacyAddressPeople>();
            eForm_Save060LegacyAddressRepresentative = new HashSet<eForm_Save060LegacyAddressRepresentative>();
            eForm_Save060ReceiverAddressPeople = new HashSet<eForm_Save060ReceiverAddressPeople>();
            eForm_Save060ReceiverAddressRepresentative = new HashSet<eForm_Save060ReceiverAddressRepresentative>();
            eForm_Save060TransferAddressPeople = new HashSet<eForm_Save060TransferAddressPeople>();
            eForm_Save060TransferAddressRepresentative = new HashSet<eForm_Save060TransferAddressRepresentative>();
            eForm_Save060TransferReceiverAddressPeople = new HashSet<eForm_Save060TransferReceiverAddressPeople>();
            eForm_Save060TransferReceiverAddressRepresentative = new HashSet<eForm_Save060TransferReceiverAddressRepresentative>();
            eForm_Save070AddressContact = new HashSet<eForm_Save070AddressContact>();
            eForm_Save070AddressPeople = new HashSet<eForm_Save070AddressPeople>();
            eForm_Save070AddressRepresentative = new HashSet<eForm_Save070AddressRepresentative>();
            eForm_Save080AddressContact = new HashSet<eForm_Save080AddressContact>();
            eForm_Save080AddressPeople = new HashSet<eForm_Save080AddressPeople>();
            eForm_Save080AddressRepresentative = new HashSet<eForm_Save080AddressRepresentative>();
            eForm_Save080ReceiverAddressContact = new HashSet<eForm_Save080ReceiverAddressContact>();
            eForm_Save080ReceiverAddressPeople = new HashSet<eForm_Save080ReceiverAddressPeople>();
            eForm_Save080ReceiverAddressRepresentative = new HashSet<eForm_Save080ReceiverAddressRepresentative>();
            eForm_Save120AddressPeople = new HashSet<eForm_Save120AddressPeople>();
            eForm_Save120AddressRepresentative = new HashSet<eForm_Save120AddressRepresentative>();
            eForm_Save140AddressContact = new HashSet<eForm_Save140AddressContact>();
            eForm_Save140AddressPeople = new HashSet<eForm_Save140AddressPeople>();
            eForm_Save140AddressRepresentative = new HashSet<eForm_Save140AddressRepresentative>();
            eForm_Save150AddressPeople = new HashSet<eForm_Save150AddressPeople>();
            eForm_Save150AddressRepresentative = new HashSet<eForm_Save150AddressRepresentative>();
            eForm_Save190AddressContact = new HashSet<eForm_Save190AddressContact>();
            eForm_Save190AddressPeople = new HashSet<eForm_Save190AddressPeople>();
            eForm_Save190AddressRepresentative = new HashSet<eForm_Save190AddressRepresentative>();
            eForm_Save210AddressContact = new HashSet<eForm_Save210AddressContact>();
            eForm_Save210AddressPeople = new HashSet<eForm_Save210AddressPeople>();
            eForm_Save210AddressRepresentative = new HashSet<eForm_Save210AddressRepresentative>();
            eForm_Save220AddressContact = new HashSet<eForm_Save220AddressContact>();
            eForm_Save220AddressPeople = new HashSet<eForm_Save220AddressPeople>();
            eForm_Save220AddressRepresentative = new HashSet<eForm_Save220AddressRepresentative>();
            eForm_Save220Kor10AddressPeople = new HashSet<eForm_Save220Kor10AddressPeople>();
            eForm_Save220Kor10AddressRepresentative = new HashSet<eForm_Save220Kor10AddressRepresentative>();
            eForm_Save220Kor19AddressPeople = new HashSet<eForm_Save220Kor19AddressPeople>();
            eForm_Save220Kor19AddressRepresentative = new HashSet<eForm_Save220Kor19AddressRepresentative>();
            eForm_Save230AddressPeople = new HashSet<eForm_Save230AddressPeople>();
            eForm_Save230AddressRepresentative = new HashSet<eForm_Save230AddressRepresentative>();
        }

        public long id { get; set; }
        public int index { get; set; }
        public string code { get; set; }
        public string name { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }

        public virtual ICollection<DocumentRole03ChangeAddressContact> DocumentRole03ChangeAddressContact { get; set; }
        public virtual ICollection<DocumentRole03ChangeAddressJoiner> DocumentRole03ChangeAddressJoiner { get; set; }
        public virtual ICollection<DocumentRole03ChangeAddressPeople> DocumentRole03ChangeAddressPeople { get; set; }
        public virtual ICollection<DocumentRole03ChangeAddressRepresentative> DocumentRole03ChangeAddressRepresentative { get; set; }
        public virtual ICollection<RM_AddressProvince> RM_AddressProvince { get; set; }
        public virtual ICollection<RS_Address> RS_Address { get; set; }
        public virtual ICollection<Save010AddressContact> Save010AddressContact { get; set; }
        public virtual ICollection<Save010AddressJoiner> Save010AddressJoiner { get; set; }
        public virtual ICollection<Save010AddressPeople> Save010AddressPeople { get; set; }
        public virtual ICollection<Save010AddressPeople_GovAndOrganization> Save010AddressPeople_GovAndOrganization { get; set; }
        public virtual ICollection<Save010AddressPeople_InterPharmacy> Save010AddressPeople_InterPharmacy { get; set; }
        public virtual ICollection<Save010AddressRepresentative> Save010AddressRepresentative { get; set; }
        public virtual ICollection<Save020AddressContact> Save020AddressContact { get; set; }
        public virtual ICollection<Save020AddressPeople> Save020AddressPeople { get; set; }
        public virtual ICollection<Save020AddressRepresentative> Save020AddressRepresentative { get; set; }
        public virtual ICollection<Save030AddressContact> Save030AddressContact { get; set; }
        public virtual ICollection<Save030AddressPeople> Save030AddressPeople { get; set; }
        public virtual ICollection<Save030AddressRepresentative> Save030AddressRepresentative { get; set; }
        public virtual ICollection<Save040AddressContact> Save040AddressContact { get; set; }
        public virtual ICollection<Save040AddressPeople> Save040AddressPeople { get; set; }
        public virtual ICollection<Save040AddressRepresentative> Save040AddressRepresentative { get; set; }
        public virtual ICollection<Save050AddressContact> Save050AddressContact { get; set; }
        public virtual ICollection<Save050AddressPeople> Save050AddressPeople { get; set; }
        public virtual ICollection<Save050AddressRepresentative> Save050AddressRepresentative { get; set; }
        public virtual ICollection<Save050ReceiverAddressPeople> Save050ReceiverAddressPeople { get; set; }
        public virtual ICollection<Save050ReceiverAddressRepresentative> Save050ReceiverAddressRepresentative { get; set; }
        public virtual ICollection<Save060AddressAllowPeople> Save060AddressAllowPeople { get; set; }
        public virtual ICollection<Save060AddressContact> Save060AddressContact { get; set; }
        public virtual ICollection<Save060AddressContact06> Save060AddressContact06 { get; set; }
        public virtual ICollection<Save060AddressJoiner> Save060AddressJoiner { get; set; }
        public virtual ICollection<Save060AddressPeople> Save060AddressPeople { get; set; }
        public virtual ICollection<Save060AddressRepresentative> Save060AddressRepresentative { get; set; }
        public virtual ICollection<Save070AddressContact> Save070AddressContact { get; set; }
        public virtual ICollection<Save080AddressContact> Save080AddressContact { get; set; }
        public virtual ICollection<Save080AddressJoiner> Save080AddressJoiner { get; set; }
        public virtual ICollection<Save080AddressJoinerRepresentative> Save080AddressJoinerRepresentative { get; set; }
        public virtual ICollection<Save080AddressPeople> Save080AddressPeople { get; set; }
        public virtual ICollection<Save080AddressRepresentative> Save080AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save010AddressContact> eForm_Save010AddressContact { get; set; }
        public virtual ICollection<eForm_Save010AddressJoiner> eForm_Save010AddressJoiner { get; set; }
        public virtual ICollection<eForm_Save010AddressPeople> eForm_Save010AddressPeople { get; set; }
        public virtual ICollection<eForm_Save010AddressRepresentative> eForm_Save010AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save010AddressRepresentativeKor18> eForm_Save010AddressRepresentativeKor18 { get; set; }
        public virtual ICollection<eForm_Save010Kor10AddressPeople> eForm_Save010Kor10AddressPeople { get; set; }
        public virtual ICollection<eForm_Save010Kor10AddressRepresentative> eForm_Save010Kor10AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save010Kor19AddressPeople> eForm_Save010Kor19AddressPeople { get; set; }
        public virtual ICollection<eForm_Save010Kor19AddressRepresentative> eForm_Save010Kor19AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save020AddressContact> eForm_Save020AddressContact { get; set; }
        public virtual ICollection<eForm_Save020AddressPeople> eForm_Save020AddressPeople { get; set; }
        public virtual ICollection<eForm_Save020AddressRepresentative> eForm_Save020AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save021AddressContact> eForm_Save021AddressContact { get; set; }
        public virtual ICollection<eForm_Save021AddressPeople> eForm_Save021AddressPeople { get; set; }
        public virtual ICollection<eForm_Save021AddressRepresentative> eForm_Save021AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save030AddressContact> eForm_Save030AddressContact { get; set; }
        public virtual ICollection<eForm_Save030AddressPeople> eForm_Save030AddressPeople { get; set; }
        public virtual ICollection<eForm_Save030AddressRepresentative> eForm_Save030AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save040AddressContact> eForm_Save040AddressContact { get; set; }
        public virtual ICollection<eForm_Save040AddressPeople> eForm_Save040AddressPeople { get; set; }
        public virtual ICollection<eForm_Save040AddressRepresentative> eForm_Save040AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save040ReceiverAddressContact> eForm_Save040ReceiverAddressContact { get; set; }
        public virtual ICollection<eForm_Save040ReceiverAddressPeople> eForm_Save040ReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save040ReceiverAddressRepresentative> eForm_Save040ReceiverAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save050AddressContact> eForm_Save050AddressContact { get; set; }
        public virtual ICollection<eForm_Save050AddressPeople> eForm_Save050AddressPeople { get; set; }
        public virtual ICollection<eForm_Save050AddressPeopleChange> eForm_Save050AddressPeopleChange { get; set; }
        public virtual ICollection<eForm_Save050AddressRepresentative> eForm_Save050AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save050ReceiverAddressPeople> eForm_Save050ReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save050ReceiverAddressRepresentative> eForm_Save050ReceiverAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060AddressContact> eForm_Save060AddressContact { get; set; }
        public virtual ICollection<eForm_Save060AddressPeople> eForm_Save060AddressPeople { get; set; }
        public virtual ICollection<eForm_Save060AddressPeopleChange> eForm_Save060AddressPeopleChange { get; set; }
        public virtual ICollection<eForm_Save060AddressRepresentative> eForm_Save060AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060ChallengerAddressContact> eForm_Save060ChallengerAddressContact { get; set; }
        public virtual ICollection<eForm_Save060ChallengerAddressPeople> eForm_Save060ChallengerAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060ChallengerAddressRepresentative> eForm_Save060ChallengerAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060ContractReceiverAddressPeople> eForm_Save060ContractReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060ContractReceiverAddressRepresentative> eForm_Save060ContractReceiverAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060LegacyAddressPeople> eForm_Save060LegacyAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060LegacyAddressRepresentative> eForm_Save060LegacyAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060ReceiverAddressPeople> eForm_Save060ReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060ReceiverAddressRepresentative> eForm_Save060ReceiverAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060TransferAddressPeople> eForm_Save060TransferAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060TransferAddressRepresentative> eForm_Save060TransferAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060TransferReceiverAddressPeople> eForm_Save060TransferReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060TransferReceiverAddressRepresentative> eForm_Save060TransferReceiverAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save070AddressContact> eForm_Save070AddressContact { get; set; }
        public virtual ICollection<eForm_Save070AddressPeople> eForm_Save070AddressPeople { get; set; }
        public virtual ICollection<eForm_Save070AddressRepresentative> eForm_Save070AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save080AddressContact> eForm_Save080AddressContact { get; set; }
        public virtual ICollection<eForm_Save080AddressPeople> eForm_Save080AddressPeople { get; set; }
        public virtual ICollection<eForm_Save080AddressRepresentative> eForm_Save080AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save080ReceiverAddressContact> eForm_Save080ReceiverAddressContact { get; set; }
        public virtual ICollection<eForm_Save080ReceiverAddressPeople> eForm_Save080ReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save080ReceiverAddressRepresentative> eForm_Save080ReceiverAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save120AddressPeople> eForm_Save120AddressPeople { get; set; }
        public virtual ICollection<eForm_Save120AddressRepresentative> eForm_Save120AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save140AddressContact> eForm_Save140AddressContact { get; set; }
        public virtual ICollection<eForm_Save140AddressPeople> eForm_Save140AddressPeople { get; set; }
        public virtual ICollection<eForm_Save140AddressRepresentative> eForm_Save140AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save150AddressPeople> eForm_Save150AddressPeople { get; set; }
        public virtual ICollection<eForm_Save150AddressRepresentative> eForm_Save150AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save190AddressContact> eForm_Save190AddressContact { get; set; }
        public virtual ICollection<eForm_Save190AddressPeople> eForm_Save190AddressPeople { get; set; }
        public virtual ICollection<eForm_Save190AddressRepresentative> eForm_Save190AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save210AddressContact> eForm_Save210AddressContact { get; set; }
        public virtual ICollection<eForm_Save210AddressPeople> eForm_Save210AddressPeople { get; set; }
        public virtual ICollection<eForm_Save210AddressRepresentative> eForm_Save210AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save220AddressContact> eForm_Save220AddressContact { get; set; }
        public virtual ICollection<eForm_Save220AddressPeople> eForm_Save220AddressPeople { get; set; }
        public virtual ICollection<eForm_Save220AddressRepresentative> eForm_Save220AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save220Kor10AddressPeople> eForm_Save220Kor10AddressPeople { get; set; }
        public virtual ICollection<eForm_Save220Kor10AddressRepresentative> eForm_Save220Kor10AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save220Kor19AddressPeople> eForm_Save220Kor19AddressPeople { get; set; }
        public virtual ICollection<eForm_Save220Kor19AddressRepresentative> eForm_Save220Kor19AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save230AddressPeople> eForm_Save230AddressPeople { get; set; }
        public virtual ICollection<eForm_Save230AddressRepresentative> eForm_Save230AddressRepresentative { get; set; }
    }
}