﻿// <auto-generated> This file has been auto generated by EF Core Power Tools. </auto-generated>
using System;
using System.Collections.Generic;

namespace DIP.TM.Datas
{
    public partial class eForm_Save060
    {
        public eForm_Save060()
        {
            eForm_Save060AddressContact = new HashSet<eForm_Save060AddressContact>();
            eForm_Save060AddressPeople = new HashSet<eForm_Save060AddressPeople>();
            eForm_Save060AddressRepresentative = new HashSet<eForm_Save060AddressRepresentative>();
            eForm_Save060ChallengerAddressContact = new HashSet<eForm_Save060ChallengerAddressContact>();
            eForm_Save060ChallengerAddressPeople = new HashSet<eForm_Save060ChallengerAddressPeople>();
            eForm_Save060ChallengerAddressRepresentative = new HashSet<eForm_Save060ChallengerAddressRepresentative>();
            eForm_Save060CheckingSimilarWordTranslate = new HashSet<eForm_Save060CheckingSimilarWordTranslate>();
            eForm_Save060ContractReceiverAddressPeople = new HashSet<eForm_Save060ContractReceiverAddressPeople>();
            eForm_Save060ContractReceiverAddressRepresentative = new HashSet<eForm_Save060ContractReceiverAddressRepresentative>();
            eForm_Save060LegacyAddressPeople = new HashSet<eForm_Save060LegacyAddressPeople>();
            eForm_Save060LegacyAddressRepresentative = new HashSet<eForm_Save060LegacyAddressRepresentative>();
            eForm_Save060Product = new HashSet<eForm_Save060Product>();
            eForm_Save060ReceiverAddressPeople = new HashSet<eForm_Save060ReceiverAddressPeople>();
            eForm_Save060ReceiverAddressRepresentative = new HashSet<eForm_Save060ReceiverAddressRepresentative>();
            eForm_Save060TransferAddressPeople = new HashSet<eForm_Save060TransferAddressPeople>();
            eForm_Save060TransferAddressRepresentative = new HashSet<eForm_Save060TransferAddressRepresentative>();
            eForm_Save060TransferReceiverAddressPeople = new HashSet<eForm_Save060TransferReceiverAddressPeople>();
            eForm_Save060TransferReceiverAddressRepresentative = new HashSet<eForm_Save060TransferReceiverAddressRepresentative>();
        }

        public long id { get; set; }
        public DateTime? make_date { get; set; }
        public string eform_number { get; set; }
        public string email { get; set; }
        public string email_uuid { get; set; }
        public string telephone { get; set; }
        public string request_number { get; set; }
        public string registration_number { get; set; }
        public string payer_name { get; set; }
        public decimal? total_price { get; set; }
        public string wizard { get; set; }
        public string challenger_number { get; set; }
        public string rule_number { get; set; }
        public string save060_period_request_code { get; set; }
        public string save060_search_type_code { get; set; }
        public bool? is_menu_1 { get; set; }
        public bool? is_menu_2 { get; set; }
        public bool? is_menu_3 { get; set; }
        public bool? is_menu_4 { get; set; }
        public bool? is_menu_4_1 { get; set; }
        public bool? is_menu_4_2 { get; set; }
        public bool? is_menu_5 { get; set; }
        public bool? is_menu_6 { get; set; }
        public bool? is_menu_7 { get; set; }
        public bool? is_menu_8 { get; set; }
        public bool? is_menu_9 { get; set; }
        public bool? is_menu_10 { get; set; }
        public bool? is_menu_11 { get; set; }
        public bool? is_menu_12 { get; set; }
        public bool? is_menu_12_1 { get; set; }
        public bool? is_menu_12_2 { get; set; }
        public bool? is_menu_12_3 { get; set; }
        public bool? is_menu_12_4 { get; set; }
        public bool? is_menu_12_5 { get; set; }
        public bool? is_menu_12_6 { get; set; }
        public bool? is_menu_12_7 { get; set; }
        public bool? is_menu_12_8 { get; set; }
        public bool? is_menu_12_9 { get; set; }
        public bool? is_menu_12_10 { get; set; }
        public bool? is_menu_12_11 { get; set; }
        public bool? is_menu_12_12 { get; set; }
        public string save060_change_type_code { get; set; }
        public string save060_representative_change_code { get; set; }
        public string save060_representative_condition_type_code { get; set; }
        public string save060_receiver_representative_change_code { get; set; }
        public string save060_receiver_representative_condition_type_code { get; set; }
        public string save060_contract_receiver_type_code { get; set; }
        public string save060_contract_receiver_representative_change_code { get; set; }
        public string save060_contract_receiver_representative_condition_type_code { get; set; }
        public string save060_detail_receiver_type_code { get; set; }
        public DateTime? contract_start_date { get; set; }
        public bool? is_contract { get; set; }
        public DateTime? contract_end_date { get; set; }
        public DateTime? contract_date { get; set; }
        public string remark_8_2 { get; set; }
        public bool? is_people_rights { get; set; }
        public bool? is_people_authorize { get; set; }
        public bool? is_receiver_transfer { get; set; }
        public bool? is_receiver_authorize { get; set; }
        public string save060_legacy_representative_change_code { get; set; }
        public string save060_legacy_representative_condition_type_code { get; set; }
        public string save060_img_type_type_code { get; set; }
        public long? img_file_2d_id { get; set; }
        public long? img_file_3d_id_1 { get; set; }
        public long? img_file_3d_id_2 { get; set; }
        public long? img_file_3d_id_3 { get; set; }
        public long? img_file_3d_id_4 { get; set; }
        public long? img_file_3d_id_5 { get; set; }
        public long? img_file_3d_id_6 { get; set; }
        public int? img_w { get; set; }
        public int? img_h { get; set; }
        public string remark_5_1_3 { get; set; }
        public bool? is_sound_mark_human { get; set; }
        public bool? is_sound_mark_animal { get; set; }
        public bool? is_sound_mark_sound { get; set; }
        public bool? is_sound_mark_other { get; set; }
        public string remark_5_2_2 { get; set; }
        public string save060_contact_type_code { get; set; }
        public long? rule_file_id { get; set; }
        public string people_kor4_number { get; set; }
        public string save060_transfer_representative_change_code { get; set; }
        public string save060_transfer_representative_condition_type_code { get; set; }
        public string receiver_kor4_number { get; set; }
        public string save060_transfer_receiver_representative_change_code { get; set; }
        public string save060_transfer_receiver_representative_condition_type_code { get; set; }
        public string save060_challenger_representative_change_code { get; set; }
        public string save060_challenger_representative_period_change_type_code { get; set; }
        public string save060_challenger_representative_condition_type_code { get; set; }
        public string save060_challenger_representative_period_condition_type_code { get; set; }
        public string save060_challenger_contact_type_code { get; set; }
        public bool? is_1 { get; set; }
        public bool? is_2 { get; set; }
        public bool? is_3 { get; set; }
        public bool? is_4 { get; set; }
        public bool? is_5 { get; set; }
        public bool? is_6 { get; set; }
        public string sign_inform_person_list { get; set; }
        public string sign_inform_representative_list { get; set; }
        public bool is_deleted { get; set; }
        public DateTime created_date { get; set; }
        public long created_by { get; set; }
        public DateTime? updated_date { get; set; }
        public long? updated_by { get; set; }
        public long? sound_file_id { get; set; }
        public long? sound_jpg_file_id { get; set; }
        public long? contract_detail_ref_id { get; set; }
        public string remark_8 { get; set; }
        public string remark_9 { get; set; }

        public virtual FileGuest img_file_2d_ { get; set; }
        public virtual FileGuest img_file_3d_id_1Navigation { get; set; }
        public virtual FileGuest img_file_3d_id_2Navigation { get; set; }
        public virtual FileGuest img_file_3d_id_3Navigation { get; set; }
        public virtual FileGuest img_file_3d_id_4Navigation { get; set; }
        public virtual FileGuest img_file_3d_id_5Navigation { get; set; }
        public virtual FileGuest img_file_3d_id_6Navigation { get; set; }
        public virtual RM_Save060ChallengerAddressTypeCode save060_challenger_contact_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save060_challenger_representative_condition_type_codeNavigation { get; set; }
        public virtual RM_Save060AddressTypeCode save060_contact_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save060_contract_receiver_representative_condition_type_codeNavigation { get; set; }
        public virtual RM_Save060ReceiverTypeCode save060_contract_receiver_type_codeNavigation { get; set; }
        public virtual RM_Save060ReceiverTypeCode save060_detail_receiver_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save060_legacy_representative_condition_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save060_receiver_representative_condition_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save060_representative_condition_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save060_transfer_receiver_representative_condition_type_codeNavigation { get; set; }
        public virtual RM_AddressRepresentativeConditionTypeCode save060_transfer_representative_condition_type_codeNavigation { get; set; }
        public virtual FileGuest sound_file_ { get; set; }
        public virtual FileGuest sound_jpg_file_ { get; set; }
        public virtual ICollection<eForm_Save060AddressContact> eForm_Save060AddressContact { get; set; }
        public virtual ICollection<eForm_Save060AddressPeople> eForm_Save060AddressPeople { get; set; }
        public virtual ICollection<eForm_Save060AddressRepresentative> eForm_Save060AddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060ChallengerAddressContact> eForm_Save060ChallengerAddressContact { get; set; }
        public virtual ICollection<eForm_Save060ChallengerAddressPeople> eForm_Save060ChallengerAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060ChallengerAddressRepresentative> eForm_Save060ChallengerAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060CheckingSimilarWordTranslate> eForm_Save060CheckingSimilarWordTranslate { get; set; }
        public virtual ICollection<eForm_Save060ContractReceiverAddressPeople> eForm_Save060ContractReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060ContractReceiverAddressRepresentative> eForm_Save060ContractReceiverAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060LegacyAddressPeople> eForm_Save060LegacyAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060LegacyAddressRepresentative> eForm_Save060LegacyAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060Product> eForm_Save060Product { get; set; }
        public virtual ICollection<eForm_Save060ReceiverAddressPeople> eForm_Save060ReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060ReceiverAddressRepresentative> eForm_Save060ReceiverAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060TransferAddressPeople> eForm_Save060TransferAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060TransferAddressRepresentative> eForm_Save060TransferAddressRepresentative { get; set; }
        public virtual ICollection<eForm_Save060TransferReceiverAddressPeople> eForm_Save060TransferReceiverAddressPeople { get; set; }
        public virtual ICollection<eForm_Save060TransferReceiverAddressRepresentative> eForm_Save060TransferReceiverAddressRepresentative { get; set; }
    }
}