﻿using DIP.TM.Utils.Extensions;
using Xunit;

namespace DIP.TM.Utils.Test.ExtensionTests
{
    public class DoubleUnitTest
    {
        [Fact]
        public void TestOneDigitNumber()
        {
            double request = 1.0;
            var result = request.ToThaiBahtText();
            var actualValue = "หนึ่งบาทถ้วน";
            Assert.Equal(actualValue, result);
        }


        [Fact]
        public void TestTwoDigitNumber()
        {
            double request = 21.0;
            var result = request.ToThaiBahtText();
            var actualValue = "ยี่สิบเอ็ดบาทถ้วน";
            Assert.Equal(actualValue, result);
        }

        [Fact]
        public void TestThreeDigitNumber()
        {
            double request = 221.0;
            var result = request.ToThaiBahtText();
            var actualValue = "สองร้อยยี่สิบเอ็ดบาทถ้วน";
            Assert.Equal(actualValue, result);
        }

        [Fact]
        public void TestFourDigitNumber()
        {
            double request = 2221.0;
            var result = request.ToThaiBahtText();
            var actualValue = "สองพันสองร้อยยี่สิบเอ็ดบาทถ้วน";
            Assert.Equal(actualValue, result);
        }

        [Fact]
        public void TestFiveDigitNumber()
        {
            double request = 32221.0;
            var result = request.ToThaiBahtText();
            var actualValue = "สามหมื่นสองพันสองร้อยยี่สิบเอ็ดบาทถ้วน";
            Assert.Equal(actualValue, result);
        }
    }
}
