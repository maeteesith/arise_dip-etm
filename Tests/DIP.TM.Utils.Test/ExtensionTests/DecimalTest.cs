﻿using DIP.TM.Utils.Extensions;
using Xunit;

namespace DIP.TM.Utils.Test.ExtensionTests
{
    public class DecimalTest
    {

        [Fact]
        public void TestOneDigitNumber()
        {
            decimal request = 1.0M;
            var result = request.ToThaiBahtText();
            var actualValue = "หนึ่งบาทถ้วน";
            Assert.Equal(actualValue, result);
        }


        [Fact]
        public void TestTwoDigitNumber()
        {
            decimal request = 21.0M;
            var result = request.ToThaiBahtText();
            var actualValue = "ยี่สิบเอ็ดบาทถ้วน";
            Assert.Equal(actualValue, result);
        }

        [Fact]
        public void TestThreeDigitNumber()
        {
            decimal request = 221.0M;
            var result = request.ToThaiBahtText();
            var actualValue = "สองร้อยยี่สิบเอ็ดบาทถ้วน";
            Assert.Equal(actualValue, result);
        }

        [Fact]
        public void TestFourDigitNumber()
        {
            decimal request = 2221.0M;
            var result = request.ToThaiBahtText();
            var actualValue = "สองพันสองร้อยยี่สิบเอ็ดบาทถ้วน";
            Assert.Equal(actualValue, result);
        }

        [Fact]
        public void TestFiveDigitNumber()
        {
            decimal request = 32221.0M;
            var result = request.ToThaiBahtText();
            var actualValue = "สามหมื่นสองพันสองร้อยยี่สิบเอ็ดบาทถ้วน";
            Assert.Equal(actualValue, result);
        }
    }
}
