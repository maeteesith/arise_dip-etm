﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;
using DIP.TM.Web.Profiles;
using Xunit;

namespace DIP.TM.Web.Test.AutoMapper
{

    public class UserProfileTest
    {
        public UserProfileTest()
        {

        }
        [Fact]
        public void TestMapDbToView()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new UserProfile());
            });
            var mapper = mockMapper.CreateMapper();

            var dbModel = new vUserSession()
            {
                id=1,
                email="mbk@hall.com"
            };
            var viewModel = mapper.Map<vUserSessionView>(dbModel);
            Assert.NotNull(viewModel);
            Assert.Equal(1, viewModel.id);
            Assert.Equal("mbk@hall.com", viewModel.email);
        }
    }
}
