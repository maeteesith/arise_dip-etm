﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;
using DIP.TM.Web.Profiles;
using System.Collections.Generic;
using Xunit;

namespace DIP.TM.Web.Test.AutoMapper
{
    public class ReceiptProfileTest
    {
        [Fact]
        public void TestGetOnlyActiveDbMapper()
        {
            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new ReceiptProfile());
            });

            var mapper = mockMapper.CreateMapper();
            var source = new Receipt() { 
                id=1,
                ReceiptItem=new List<ReceiptItem>() {
                    new ReceiptItem(){id=1,is_deleted=false},
                    new ReceiptItem(){id=2,is_deleted=true}
                }
            };
            var viewModel = mapper.Map<ReceiptView>(source);
            Assert.NotNull(viewModel);
            var countItem = viewModel.item_list.Count;
            Assert.Equal(2, countItem);
        }
    }
}


//Receipt