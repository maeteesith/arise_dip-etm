﻿using DIP.TM.Datas;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Moq;
using Xunit;

namespace DIP.TM.Web.Test.Controllers
{
    public class WebMockTest
    {
        [Fact]
        public void MockConfiguration()
        {
            var mockConfig = new Mock<IConfiguration>();
            mockConfig.Setup((config) => config["SecretKey"])
                      .Returns(() => "DIMTM");

            var secrteKey = mockConfig.Object["SecretKey"];

            Assert.NotNull(secrteKey);
            Assert.NotNull(mockConfig.Object);
            Assert.Equal("DIMTM",secrteKey);
        }

        [Fact]
        public void MockDatabaseContext()
        {
            var options = new DbContextOptionsBuilder<DIPTMContext>()
                .UseInMemoryDatabase(databaseName: "Add_writes_to_database")
                .Options;
            var context = new DIPTMContext(options);
            // in memory context
        }

    }
}
