﻿using Microsoft.Extensions.Configuration;
using Moq;

namespace DIP.TM.Web.Test.MockObjects
{
    public static class WebConfiguration
    {

        public static Mock<IConfiguration> MockConfiguration()
        {
            var mockConfig = new Mock<IConfiguration>();
            mockConfig.Setup((config) => config["SecretKey"])
                      .Returns(() => "DIMTM");


            // TODO all config

            return mockConfig;
        }
    }
}
