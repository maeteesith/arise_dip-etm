﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Uows.DataAccess.Context;
using DIP.TM.Uows.DataAccess.Repositories;
using DIP.TM.Uows.DataAccess.Uow;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using Xunit;

namespace DIP.TM.Services.Test
{
    public class DocumentProcessServiceTest
    {
        private DIPTMContext _context;

        /// <summary>
        /// init db memory
        /// </summary>
        public DocumentProcessServiceTest()
        {
            var options = new DbContextOptionsBuilder<DIPTMContext>()
               .UseInMemoryDatabase(databaseName: "Documents")
               .Options;
            _context = new DIPTMContext(options);
        }

        private void AddMemoryData()
        {
            //_context.Save010InstructionRule.Add(new Save010InstructionRule() { id=1 });
            //_context.SaveChanges();
            //_context.PostRound.Add(new PostRound() { id = 1 });
            //_context.SaveChanges();
        }


        [Fact]
        public void DocumentRole02CheckInstructionRuleSend_Nominal()
        {
            /*var logger = new Mock<ILogger<Uows.DataAccess.DataAccess>>();
            var sp = new Mock<IServiceProvider>();

            AddMemoryData();

            sp.Setup((o) => o.GetService(typeof(IEntityContext))).Returns(_context);
            sp.Setup((o) => o.GetService(typeof(IRepository<Save010InstructionRule>))).Returns(new GenericEntityRepository<Save010InstructionRule>(logger.Object));
            sp.Setup((o) => o.GetService(typeof(IRepository<vDocumentRole02>))).Returns(new GenericEntityRepository<vDocumentRole02>(logger.Object));
            sp.Setup((o) => o.GetService(typeof(IRepository<PostRound>))).Returns(new GenericEntityRepository<PostRound>(logger.Object));

            var provider = new UowProvider(logger.Object, sp.Object);
            var mockUow = new Mock<IUowProvider>();
            mockUow.Setup(uow => uow.CreateUnitOfWork(true, false))
                   .Returns(provider.CreateUnitOfWork());

            var mockConfig = new Mock<IConfiguration>();

            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MockUM_UserToUseViewTest());
            });
            var mapper = mockMapper.CreateMapper();


            var service = new DocumentProcessService(mockConfig.Object, mockUow.Object, mapper);
            var request = new vDocumentRole02AddModel() { instruction_rule_id=1 };
            var result = service.DocumentRole02CheckInstructionRuleSend(request);*/

        }
    }
}
