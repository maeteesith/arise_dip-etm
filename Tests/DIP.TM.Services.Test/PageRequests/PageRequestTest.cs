﻿using DIP.TM.Models.Pagers;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace DIP.TM.Services.Test.PageRequests
{
    public class PageRequestTest
    {

        [Fact]
        public void MapValueToValues()
        {
            var req = new PageRequest() { 
                filter_queries=null,
                is_order_reverse=false,
                item_per_page=10,
                item_total=0,
                order_by="id",
                page_index=1,
                search_by=new List<SearchByModel>() { 
                    new SearchByModel(){operation= Operation.Equals,key="id",value="1000"}
                }
            };
            var actualCount = req.search_by[0].values.Count;
            Assert.Equal(1, actualCount);
        }


        [Fact]
        public void NoMapValueToValues()
        {
            var req = new PageRequest()
            {
                filter_queries = null,
                is_order_reverse = false,
                item_per_page = 10,
                item_total = 0,
                order_by = "id",
                page_index = 1,
                search_by = new List<SearchByModel>() {
                    new SearchByModel(){operation= Operation.Equals,key="id",values= new List<String>(){"1000" } }
                }
            };
            var actualCount = req.search_by[0].values.Count;
            var actualValue = req.search_by[0].value;
            Assert.Equal(1, actualCount);
            Assert.Null(actualValue);
        }
    }
}
