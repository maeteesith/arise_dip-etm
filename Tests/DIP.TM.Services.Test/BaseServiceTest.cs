using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Views;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Uows.DataAccess.Context;
using DIP.TM.Uows.DataAccess.Repositories;
using DIP.TM.Uows.DataAccess.Uow;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Xunit;


namespace DIP.TM.Services.Test
{
    public class BaseServiceTest
    {
        [Fact]
        public void ListTest()
        {
           
            var logger = new Mock<ILogger<Uows.DataAccess.DataAccess>>();
            var sp = new Mock<IServiceProvider>();
            var dbContext = new DIPTMContext(new DbContextOptions<DIPTMContext>());
            var repoUserRes = new Mock<GenericEntityRepository<UM_User>>(logger.Object);


            var users = new List<UM_User>()
                       {
                           new UM_User(){id=1,name="user1",is_deleted=false},
                           new UM_User(){id=2,name="user2",is_deleted=false},
                           new UM_User(){id=3,name="user3",is_deleted=false},
                           new UM_User(){id=4,name="user4",is_deleted=false},
                       }.AsQueryable();

            repoUserRes.Setup(uow => uow.Filters(It.IsAny<Expression<Func<UM_User, bool>>>()))
                    .Returns(
                        (Expression<Func<UM_User, bool>> condition) =>
                        users.Where(condition)
                    );



            sp.Setup((o) => o.GetService(typeof(IEntityContext))).Returns(dbContext);
            sp.Setup((o) => o.GetService(typeof(IRepository<UM_User>))).Returns(repoUserRes.Object);

            

            var provider = new UowProvider(logger.Object, sp.Object);

            var mockUow = new Mock<IUowProvider>();
            mockUow.Setup(uow => uow.CreateUnitOfWork(true, false))
                   .Returns(provider.CreateUnitOfWork());


            var mockConfig = new Mock<IConfiguration>();


            var mockMapper = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new MockUM_UserToUseViewTest());
            });
            var mapper = mockMapper.CreateMapper();


            var service = new BaseService<UM_User, UserView>(mockConfig.Object, mockUow.Object, mapper);

            var request = new PageRequest()
            {
                item_per_page=2,
                page_index=1
            };


            var results = service.List(request);

            Assert.NotNull(results);
            Assert.NotNull(results.data);
            Assert.NotNull(results.data.list);
            Assert.NotNull(results.data.paging);
            Assert.Equal(2, results.data.list.Count);
            Assert.Equal("user1", results.data.list[0].name);
            Assert.Equal(4, results.data.paging.item_total);
        }
    }
    public class MockUM_UserToUseViewTest : Profile
    {
        public MockUM_UserToUseViewTest()
        {
            CreateMap<UM_User, UserView>();
        }
    }

}
