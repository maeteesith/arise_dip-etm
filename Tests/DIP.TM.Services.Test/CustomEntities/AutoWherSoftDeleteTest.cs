﻿using DIP.TM.Models.Pagers;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Test.FilterPaginations;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DIP.TM.Services.Test.CustomEntities
{
    public class AutoWherSoftDeleteTest
    {
        private IQueryable<TestModel> Mocks()
        {
            var lst = new List<TestModel>()
            {
                new TestModel(){id=1,is_active=true,str="aaaaaaaaaaaa",created_date=DateTime.Now, updated_date=DateTime.Now,is_deleted=true},
                new TestModel(){id=2,is_active=false,str="bbbdbb",created_date=DateTime.Now, updated_date=DateTime.Now.AddDays(1),is_deleted=true},
                new TestModel(){id=3,is_active=true,str="ccacc",created_date=DateTime.Now.AddDays(1), updated_date=DateTime.Now,is_deleted=true},
                new TestModel(){id=4,is_active=false,str="ddd",created_date=DateTime.Now, updated_date=DateTime.Now.AddDays(-1),is_deleted=false},
                new TestModel(){id=5,is_active=true,str="eee",created_date=DateTime.Now.AddDays(-1), updated_date=DateTime.Now,is_deleted=true}
            };
            return lst.AsQueryable<TestModel>();
        }

        //[Fact]
        //public void DynamicWhereInteger()
        //{
        //    var datas = Mocks();
        //    var payload = new PageRequest()
        //    {
        //        is_order_reverse = false,
        //        order_by = "id",
        //        page_index = 1,
        //        item_per_page = 50,
        //        search_by = new List<SearchByModel>()
        //        {
        //            new SearchByModel(){key="id",value="1",operation=Operation.Equals}
        //        }
        //    };
        //    var results = datas.FilterDynamic(payload);
        //    var actualCount = results.Count();
        //    Assert.Equal(0, actualCount);
        //}
    }
}
