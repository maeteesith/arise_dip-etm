﻿using DIP.TM.Models.Pagers;
using DIP.TM.Services.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace DIP.TM.Services.Test.FilterPaginations
{

    public class TestModel
    {
        public int id { get; set; }
        public string str { get; set; }
        public bool is_active { get; set; }
        public DateTime created_date { get; set; }
        public DateTime? updated_date { get; set; }
        public bool is_deleted { get; set; }
    }

    public class FilterDynamicTest
    {

        private IQueryable<TestModel> Mocks()
        {
            var lst = new List<TestModel>()
            {
                new TestModel(){id=1,is_active=true,str="aaaaaaaaaaaa",created_date=DateTime.Now, updated_date=DateTime.Now},
                new TestModel(){id=2,is_active=false,str="bbbdbb",created_date=DateTime.Now, updated_date=DateTime.Now.AddDays(1)},
                new TestModel(){id=3,is_active=true,str="ccacc",created_date=DateTime.Now.AddDays(1), updated_date=DateTime.Now},
                new TestModel(){id=4,is_active=false,str="ddd",created_date=DateTime.Now, updated_date=DateTime.Now.AddDays(-1)},
                new TestModel(){id=5,is_active=true,str="eee",created_date=DateTime.Now.AddDays(-1), updated_date=DateTime.Now}
            };
            return lst.AsQueryable<TestModel>();
        }

        [Fact]
        public void DynamicWhereInteger()
        {
            var datas = Mocks();
            var payload = new PageRequest() { 
                is_order_reverse=false,
                order_by="id",
                page_index=1,
                item_per_page=50,
                search_by=new List<SearchByModel>()
                {
                    new SearchByModel(){key="id",value="1",operation=Operation.Equals}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(1, actualCount);
        }


        [Fact]
        public void DynamicWhereIntegers()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                search_by = new List<SearchByModel>()
                {
                    new SearchByModel(){key="id",values=new List<string>(){"1","2" },operation=Operation.Equals}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(2, actualCount);
        }

        [Fact]
        public void DynamicWhereBooleans()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                search_by = new List<SearchByModel>()
                {
                    new SearchByModel(){key="is_active",values=new List<string>(){ "true" },operation=Operation.Equals}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(3, actualCount);
        }


        [Fact]
        public void DynamicWhereIgnoreNull()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                search_by = new List<SearchByModel>()
                {
                    new SearchByModel(){key="is_active",values=null,operation=Operation.Equals},
                    new SearchByModel(){key="id",values=null,operation=Operation.Equals}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(5, actualCount);
        }

        /*[Fact]
        public void DynamicWhereDateTime()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                search_by = new List<SearchByModel>()
                {
                    new SearchByModel(){key="created_date",value=DateTime.Now.ToString("yyyy-MM-dd"),operation=Operation.Equals}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(3, actualCount);
        }*/

        /*[Fact]
        public void DynamicWhereNullableDateTime()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                search_by = new List<SearchByModel>()
                {
                    new SearchByModel(){key="updated_date",value=DateTime.Now.ToString("yyyy-MM-dd"),operation=Operation.Equals}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(3, actualCount);
        }*/

        [Fact]
        public void DynamicWhereContain()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                search_by = new List<SearchByModel>()
                {
                    new SearchByModel(){key="str",value="a",operation=Operation.Contains}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(2, actualCount);
        }

        [Fact]
        public void DynamicWhereMultipleValues()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                search_by = new List<SearchByModel>()
                {
                    new SearchByModel(){key="id",values=new List<string>(){ "1","2","3" },operation=Operation.Equals}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(3, actualCount);
        }

        // TODO
        /*[Fact]
        public void DynamicWhereBetweenDate()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                search_by = new List<SearchByModel>()
                {
                    new SearchByModel(){key="created_date",value=DateTime.Now.ToString("yyyy-MM-dd"),operation=Operation.GreaterThanOrEqual},
                    new SearchByModel(){key="created_date",value=DateTime.Now.AddDays(1).ToString("yyyy-MM-dd"),operation=Operation.LessThanOrEqual}
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(4, actualCount);
        }*/

        [Fact]
        public void DynamicFilterQuery()
        {
            var datas = Mocks();
            var payload = new PageRequest()
            {
                is_order_reverse = false,
                order_by = "id",
                page_index = 1,
                item_per_page = 50,
                filter_queries=new List<string>()
                {
                    "str.Contains(\"a\") OR id=5"
                }
            };
            var results = datas.FilterDynamic(payload);
            var actualCount = results.Count();
            Assert.Equal(3, actualCount);
        }
    }

}
