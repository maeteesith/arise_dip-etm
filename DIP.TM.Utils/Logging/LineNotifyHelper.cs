﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

namespace DIP.TM.Utils.Logging {
    public static class LineNotifyHelper {
        private static void PostLine(string environtment, string payload, HttpClient client) {
            if (!string.IsNullOrEmpty(payload)) {
                var dev_name = Environment.MachineName + ":" + Environment.UserName;

                if (dev_name == "DESKTOP-2LNCHCD:natee") dev_name = "Sa";

                payload = "[" + dev_name + "] " + environtment + " > " + payload;
            }

            try {
                var pairs = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>("message", payload)
                };
                var content = new FormUrlEncodedContent(pairs);
                var response = client.PostAsync("", content).Result;
            } catch (Exception) {
            }
        }

        public static void SendWait(IConfiguration Configuration, string message) {
            var client = new HttpClient {
                BaseAddress = new Uri(Configuration["LineEndPoint"])
            };
            client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", Configuration["LineToken"]);

            PostLine(Configuration["Environment"], message, client);
        }
    }
}

