﻿using System.IO;

namespace DIP.TM.Utils
{
    public static class FileHelper
    {
        public static void CreateIfMissing(string path)
        {
            bool folderExists = Directory.Exists(path);
            if (!folderExists)
                Directory.CreateDirectory(path);
        }
    }
}
