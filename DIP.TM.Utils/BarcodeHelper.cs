﻿using NetBarcode;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Utils {
    public static class BarcodeHelper {
        public static string Generate(string value) {
            if (string.IsNullOrEmpty(value)) return "";

            var barcode = new Barcode(value);
            return barcode.GetBase64Image();
        }
    }
}
