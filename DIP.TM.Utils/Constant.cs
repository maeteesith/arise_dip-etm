﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Utils {
    public enum SequenceNumberConstant {
        REQUEST_INDEX,
        REQUEST_01_ITEM_NUMBER,
        REQUEST_REFERENCE_NUMBER,
        RECEIPT_NUMBER,
        PUBLIC_BOOK_INDEX,
        PUBLIC_BOOK_NUMBER,
        REGISTRATION_NUMBER,
        EFORM_NUMBER,
        REQUEST_CHECK_VOUCHER_NUMBER,
    }

    public enum RequestDocumentCollectTypeCodeConstant {
        TRADEMARK_2D, SAVE01, CERTIFICATION_MARK,
    }

    public enum RequestDocumentCollectStatusCodeConstant {
        ADD, DELETED,
    }


    public enum RequestStatusCodeConstant {
        DRAFT, SAVE, RECEIPT, DELETE
    }

    public enum ReceiptStatusCodeConstant {
        UNPAID, PAID, DELETED, ALL
    }

    public enum TrademarkStatusCodeConstant {
        D, P, R,
    }

    public enum SaveStatusCodeConstant {
        DRAFT, SENT, DELETE,
        ALL,
    }

    public enum ConsideringSimilarDocumentStatusCodeConstant {
        WAIT_CONSIDER, ALLOW
    }

    public enum DocumentClassificationCollectStatusCodeConstant {
        WAIT, DONE, DELETE,
    }

    public enum CheckingTypeCodeConstant {
        CHECKING, CONSIDERING,
    }

    public enum CheckingStatusCodeConstant {
        WAIT_CHECKING, RECEIVED_CHECKING, WAIT_CONSIDERING, RECEIVED_CONSIDERING,
    }

    public enum CheckingReceivingStatusCodeConstant {
        WAIT_DONE, WAIT_CHANGE, DONE, CHANGE,
    }

    public enum ConsideringReceivingStatusCodeConstant {
        //WAIT_DONE, DONE,
        //SEND_CHANGE, WAIT_CHANGE, DONE_CHANGE,


        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE, SEND_CHANGE,
        // Was sent back
        DRAFT_FIX, SEND_FIX,
    }

    public enum ConsideringSimilarInstructionTypeCodeConstant {
        YES, NO,
    }

    public enum ConsideringInstructionRuleCodeConstant {
        CASE_10, CASE_7,
        PUBLIC, SPECIFIC, NOT_SPECIFIC,
        ROLE_10, ROLE_11, ROLE_17, ROLE_27, ROLE_29, ROLE_7, ROLE_8, ROLE_9,
        ROLE_13_1, ROLE_13_2,
        ROLE_20_1, ROLE_20_2,
    }

    public enum ConsideringInstructionStatusCodeConstant {
        DRAFT, SEND, DELETE,
        DRAFT_FIX,
    }

    public enum Save010InstructionRuleDocumentStatusCodeConstant {
        WAIT, RECEIVED
    }

    public enum Save010InstructionRuleDocumentReceiveStatusCodeConstant {
        WAIT_DO, WAIT_CHANE, DONE
    }

    public enum ConsideringInstructionRuleStatusCodeConstant {
        WAIT_ACTION,
    }

    public enum ConsideringBookStatusCodeConstant {
        NORMAL
    }

    public enum DocumentStatusCodeConstant {
        WAIT_DOCUMENTING, RECEIVED_DOCUMENTING
    }

    public enum PublicSourceCodeConstant {
        CHECKING, DOCUMENT,
    }

    public enum PublicTypeCodeConstant {
        PUBLIC, PUBLIC_CASE41,
    }

    public enum PublicStatusCodeConstant {
        WAIT, RECEIVED,
    }

    public enum PublicReceiveStatusCodeConstant {
        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Was sent back
        DRAFT_FIX, SEND_FIX,
    }

    public enum PublicReceiveDoStatusCodeConstant {
        WAIT_PUBLIC, DOING_PUBLIC, DONE,
        // ส่งแก้ไข
        SEND_CHANGE,
    }

    public enum RequestItemSubStatusCodeConstant {
        DOING_PUBLIC, CANCEL_PUBLIC,
    }

    public enum PublicRoundStatusCodeConstant {
        WAIT_PUBLIC, DOING_PUBLIC, DONE,
    }

    public enum PublicRole01Case41StatusCodeConstant {
        DRAFT, WAIT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Was sent back
        DRAFT_FIX,
    }

    public enum PublicRole02CheckStatusCodeConstant {
        WAIT, AGAIN, DONE,
    }

    public enum PublicRole02ConsiderPaymentStatusCodeConstant {
        NOT_YET, WAIT, DONE,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
    }

    public enum PublicRole05ConsiderPaymentStatusCodeConstant {
        WAIT, DONE,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
    }

    public enum DocumentRole05CheckStatusCodeConstant {
        WAIT, DONE,
    }


    public enum PublicRole02DocumentPaymentStatusCodeConstant {
        WAIT_1, WAIT_2, WAIT_PAYMENT_1, WAIT_PAYMENT_2, DONE,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
    }

    public enum PostRoundInstructionRuleCodeConstant {
        RULE_5_3, RULE_9_1,
        ROLE_13_1, ROLE_13_2,
        ROLE_20_1, ROLE_20_2,
        RULE_9_10,

        RULE_9_11_CANCEL_PUBLIC,
        RULE_5_8_EXTEND, RULE_5_8_FEE,
    }
    public enum PostRoundTypeCodeConstant {
        INFORM_PAYMENT, REGISTERED,

        PUBLIC_OTHER_CANCEL, PUBLIC_OTHER_EVIDENCE,
        PUBLIC_OTHER_EXTEND, PUBLIC_OTHER_FEE,

        SEND_MORE,
    }

    public enum PublicRole04CheckInstructionRuleTypeCodeConstant {
        RULE_9_11,
    }


    public enum ConsideringSimilarInstructionRuleStatusCodeConstant {
        WAIT_ACTION
    }


    public enum PostRoundDocumentPostStatusCodeConstant {
        WAIT, DONE,
    }

    public enum PostRoundActionPostStatusCodeConstant {
        WAIT, DONE,
    }

    public enum PostRoundActionPostTypeCodeConstant {
        BACK, NOT_COMPLETE, OK,
    }

    public enum PublicRole04StatusCodeConstant {
        WAIT, DONE,
    }

    public enum PublicRole04ReceiveStatusCodeConstant {
        NOT_YET, WAIT, DONE,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Paid More
        WAIT_PAID_MORE,
    }

    public enum PublicRole05StatusCodeConstant {
        //WAIT, DONE,
        //SEND_CHANGE,

        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Was sent back
        DRAFT_FIX, SEND_FIX,
    }

    public enum Floor3RegistrarStatusCodeConstant {
        WAIT, DONE, CANCEL,
    }

    public enum PublicRole04DocumentStatusCodeConstant {
        WAIT,
        DONE,
        SEND_CHANGE,
    }

    public enum PublicRole02DocumentStatusCodeConstant {
        WAIT, DONE,
    }

    public enum DocumentRole02StatusCodeConstant {
        WAIT, DONE,
    }

    public enum DocumentRole02CheckStatusCodeConstant {
        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
    }

    public enum DocumentRole02ReceiveStatusCodeConstant {
        //WAIT_DO, WAIT_CHANGE, DONE,
        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
    }

    public enum DocumentRole03StatusCodeConstant {
        WAIT, DONE,
    }

    public enum DocumentRole03ReceiveStatusCodeConstant {
        //WAIT_DO, WAIT_CHANGE, DONE,
        DRAFT, SAVE, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
    }

    public enum DocumentRole02PrintDocumentStatusCodeConstant {
        DRAFT, SEND,
        //WAIT, DONE,
    }

    public enum DocumentRole02PrintCoverStatusCodeConstant {
        WAIT_COVER,
        //WAIT_POST_NUMBER,
        DONE,
    }

    public enum DocumentRole02PrintListStatusCodeConstant {
        WAIT_POST_NUMBER,
        WAIT_PRINT,
        DONE,
    }



    public enum DocumentRole04StatusCodeConstant {
        WAIT, DONE,
    }


    public enum DocumentRole04CreateStatusCodeConstant {
        DRAFT, SEND_WAIT, SEND_DONE,
        // Send back 
        WAIT_CHANGE,
        // Was sent back
        DRAFT_FIX,
    }

    public enum DocumentRole04CreatTypeCodeConstant {
        CHECKING, PUBLIC, RECOVER,
    }

    public enum DocumentRole05CreateStatusCodeConstant {
        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Was sent back
        DRAFT_FIX,
    }



    public enum DocumentRole04ReleaseStatusCodeConstant {
        DRAFT, SEND_WAIT, SEND_DONE,
        // Send back 
        WAIT_CHANGE,
        // Was sent back
        DRAFT_FIX,
    }
    public enum DocumentRole04Release20StatusCodeConstant {
        DRAFT, SEND_WAIT, SEND_DONE,
        // Send back 
        WAIT_CHANGE,
        // Was sent back
        DRAFT_FIX,
    }
    public enum DocumentRole05ReleaseStatusCodeConstant {
        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Was sent back
        DRAFT_FIX,
    }
    public enum DocumentRole05Release20StatusCodeConstant {
        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Was sent back
        DRAFT_FIX,
    }





    public enum DocumentRole04CreateTypeCodeConstant {
        CHECKING, PUBLIC, RECOVER, RELEASE,
    }

    public enum DocumentRole04ReceiveStatusCodeConstant {
        DRAFT, SEND_WAIT, SEND_DONE,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Was sent back
        DRAFT_FIX, SEND_FIX,
    }

    public enum DocumentRole04SendTypeCodeConstant {
        CHECKING, PUBLIC, ROUND_1, ROUND_2, SOME,
    }

    public enum DocumentRole05StatusCodeConstant {
        WAIT, DONE,
    }

    public enum DocumentRole05ReceiveStatusCodeConstant {
        DRAFT, SEND,
        // Send back 
        WAIT_CHANGE, DRAFT_CHANGE,
        // Was sent back
        DRAFT_FIX, SEND_FIX,
    }

    public enum DocumentRole04ReleaseRequestSendTypeCodeConstant {
        DOCUMENT, CHECKING,
    }

    public enum DocumentRole04ReleaseTypeCodeConstant {
        RELEASE, CHECKING, NEW, WAIT,
    }

    public enum DocumentClassificationStatusCodeConstant {
        WAIT_DO, WAIT_SEND, SEND, WAIT_CHANGE,
    }

    public enum DocumentClassificationVersionStatusCodeConstant {
        WAIT_DO, WAIT_SEND, SEND,
        DELETE,
    }

    public enum RequestTypeConstant {
        SAVE010, SAVE020, SAVE021, SAVE030, SAVE040, SAVE050, SAVE060, SAVE061, SAVE070,
        SAVE080, SAVE120, SAVE140, SAVE210,

        SAVE041,
        SAVE081, SAVE082,

        POST_ROUND_290,

        REQUEST_CHECK_310,
    }


    public enum RequestSourceCodeConstant {
        CENTRAL, INTERNET, POST, MADRID, COMMERCIAL_AFFAIRS_PROVINCE, EFORM,
        ALL
    }

    public enum FacilitationActStatusCodeConstant {
        DOCUMENT_COMPLETED,
    }

    public enum PermissionCodeConstant {

    }

    public enum MigrateEFormType {
        eFrom010,
        eFrom020,
        eFrom021,
        eFrom030,
        eFrom040,
        eFrom050,
        eFrom060,
        eFrom070,
        eFrom080,
        eFrom120,
        eFrom140,
        eFrom150,
        eFrom190,
        eFrom200,
    }

    public enum Request01ReceiptChangeCodeType {
        APPROVED,
        RECEIPT_APPROVE_REJECT,
        RECEIPT_APPROVE_WAIT,
        REQUEST_APPROVE_REJECT,
        REQUEST_APPROVE_WAIT
    }

    //Appeal Group
    public enum AppealCaseSummaryProcessCodeConstant { 
        CONSIDER,
        CONSIDER_2,
        CREATE_DOCUMENT,
        CREATE_DOCUMENT_2,
        WAIT_CONSIDER_2   
    }
    public enum AppealCaseSummaryStepCodeConstant{
        CREATE_CASESUMMARY,
        CRAETE_CASESUMMER_2,
        WAIT_CREATE_CASESUMMARY,
        WAIT_MEETING,
        SEND_CASESUMMARY,
        SEND_CASESUMMARY_2
    }
    public enum AppealCaseSummaryStatusCodeConstant{
        WAIT_CREATE_CASESUMMARY,
        SAVE_DRAFT,
        WAIT_CHANGDATA,
        DONE,
        WAIT_CONSIDER,
        SEND_FIX,
        WAIT_MEETING,
        WAIT_DOCUMENT_MORE,
        WAIT_PROCESS
    }
}
