﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Utils
{
    public static class ConvertHelper
    {
        public static T  To<T>(this OkObjectResult data)
        {
            return (T)data.Value;
        }
    }
}
