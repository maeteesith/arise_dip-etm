﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Text;
using System.Security.Claims;

namespace DIP.TM.Utils.Extensions
{
    public static class UserExtensions
    {
        public static long? UserId(this ClaimsPrincipal user)
        {
            try
            {
                if (user == null)
                    return null;

                if (!user.Identity.IsAuthenticated)
                    return null;

                var claims = ((ClaimsIdentity)user.Identity).Claims;
                var userId = claims.Where(x => x.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier").FirstOrDefault();
                return (ReferenceEquals(userId, null)) ? 0 : int.Parse(userId.Value);
            }
            catch (Exception ex)
            {
                return null;
            }

        }
    }
}
