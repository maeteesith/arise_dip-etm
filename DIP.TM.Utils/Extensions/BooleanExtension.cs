﻿using System;

namespace DIP.TM.Utils.Extensions
{
    public static class BooleanExtension
    {
        public static bool ToBoolean(this Object obj)
        {
            if (ReferenceEquals(obj, null))
                return false;
            return Boolean.Parse(obj.ToString());

        }
    }
}
