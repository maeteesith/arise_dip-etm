﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace DIP.TM.Utils.Extensions
{
    public static class ObjectExtension
    {

        public static long ToLong(this object data)
        {
            if (ReferenceEquals(data, null))
                return 0;
            else
            {
                var str = data.ToString();
                if (string.IsNullOrWhiteSpace(str))
                    return 0;
                else
                    return long.Parse(str);

            }
        }

        public static string ToJsonIgnoreNull(this object obj)
        {
            return JsonConvert.SerializeObject(obj,
                            Formatting.None,
                            new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore
                            });

        }

    }
}
