﻿using System;
using System.Globalization;

namespace DIP.TM.Utils.Extensions {
    public static class DateTimeExtension {
        public static string ToString(this DateTime date) {
            return date.ToString("dd/MM/yyyy");
        }

        public static string ToThaiDateString(this DateTime date) {
            var _cultureTHInfo = new CultureInfo("th-TH");
            var dateThai = Convert.ToDateTime(date, _cultureTHInfo);
            return dateThai.ToString("dd MMMM yyyy", _cultureTHInfo);
        }

        public static string ToThaiShortDateString(this DateTime date) {
            var _cultureTHInfo = new CultureInfo("th-TH");
            var dateThai = Convert.ToDateTime(date, _cultureTHInfo);
            return dateThai.ToString("dd/MM/yyyy", _cultureTHInfo);
        }

        public static string ToThaiMonthString(this DateTime date) {
            var _cultureTHInfo = new CultureInfo("th-TH");
            var dateThai = Convert.ToDateTime(date, _cultureTHInfo);
            return dateThai.ToString("MMMM", _cultureTHInfo);
        }

        public static string ToThaiYearString(this DateTime date) {
            var _cultureTHInfo = new CultureInfo("th-TH");
            var dateThai = Convert.ToDateTime(date, _cultureTHInfo);
            return dateThai.ToString("yyyy", _cultureTHInfo);
        }

        public static string ToThaiYearMoneyString(this DateTime date) {
            var _cultureTHInfo = new CultureInfo("th-TH");
            var dateThai = Convert.ToDateTime(date.AddMonths(2), _cultureTHInfo);
            return dateThai.ToString("yyyy", _cultureTHInfo);
        }
    }
}
