﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DIP.TM.Utils.Extensions
{
    public static class ArrayExtention
    {
        public static string ToDelimitedString<T>(this IEnumerable<T> lst, string separator = ", ")
        {
            return lst.ToDelimitedString(p => p, separator);
        }

        public static string ToDelimitedString<S, T>(this IEnumerable<S> lst, Func<S, T> selector,
                                                     string separator = ", ")
        {
            return string.Join(separator, lst.Select(selector));
        }
    }
}
