// import { NgModule } from '@angular/core'
// import { RouterModule, Routes } from '@angular/router'
// import {
//   RegistrationRequestComponent,
//   Request01ItemListComponent,
//   RepresentativeManagementComponent,
//   Save01ProcessComponent,
//   DailyFeeIncomeRecordsComponent,
//   DashboardComponent,
//   RegistrationOtherRequestComponent,
//   Save020ProcessComponent,
//   Save020ProcessListComponent,
//   Save021ProcessComponent,
//   Save021ProcessListComponent,
//   Save01ProcessListComponent,
//   Save01ProcessOwnerFormComponent,
//   Save01ProcessAgentFormComponent,
//   Save01ProcessUserFormComponent,
//   Save03ProcessComponent,
//   Save03ProcessListComponent,
//   Save04ProcessComponent,
//   Save04ProcessListComponent,
//   Save05ProcessComponent,
//   Save05ProcessListComponent,
//   Save06ProcessComponent,
//   Save06ProcessListComponent,
//   RequestOtherItemListComponent,
//   TestLoginComponent,
//   ReceiptDailyComponent,
//   Save07ProcessComponent,
//   Save07ProcessListComponent,
//   Save08ProcessComponent,
//   Save08ProcessListComponent,
//   Save140ProcessComponent,
//   RequestSearchComponent,
//   RequestDocumentCollectComponent,
//   AllTaskComponent,
//   DocumentProcessClassificationComponent,
//   DocumentProcessClassificationDoComponent,
//   Save140ProcessListComponent,
//   CheckingSimilarImageComponent,
//   CheckingItemListComponent,
//   CheckingSimilarListComponent,
//   CheckingSimilarComponent,
//   CheckingSimilarWordComponent,
//   CheckingSimilarResultComponent,
//   ConsideringSimilarListComponent,
//   ConsideringSimilarComponent,
//   PublicDashboardRole01Component,
//   eFormSave01ProcessComponent,
//   eFormSave02ProcessComponent,
//   eFormHomeComponent,
//   EFormBrandTypeComponent,
//   EFormOwnerAndAgentComponent,
//   DocumentItemListComponent,
//   NotFoundComponent,
//   ConsideringSimilarInstructionComponent,
//   ConsideringSimilarDocumentComponent,
//   PublicItemListComponent,
//   PublicRole01ItemListComponent,
//   PublicRole01CheckListComponent,
//   PublicRole01CheckDoComponent,
//   PublicRole01PrepareListComponent,
//   PublicRole01DoingListComponent,
//   PublicRole02CheckListComponent,
//   PublicRole02ConsiderPaymentListComponent,
//   PublicRole05ConsiderPaymentListComponent,
//   PublicRole02DocumentPaymentListComponent,
//   PublicRole02DocumentPostListComponent,
//   PublicRole02ActionPostListComponent,
//   PublicRole04itemListComponent,
//   PublicRole04CheckListComponent,
//   PublicRole05DocumentListComponent,
//   PublicRole04DocumentListComponent,
//   PublicRole02DocumentListComponent,
//   DocumentRole02ItemListComponent,
//   eFormSave140ProcessComponent,
//   eFormSave150ProcessComponent,
//   eFormSave190ProcessComponent,
//   eFormSave200ProcessComponent,
//   eFormSave030ProcessComponent,
//   eFormSave040ProcessComponent,
//   eFormSave050ProcessComponent,
//   eFormSave060ProcessComponent,
//   eFormSave070ProcessComponent,
//   eFormSave080ProcessComponent,
//   eFormSave021ProcessComponent,
//   eFormSave120ProcessComponent,
//   DocumentRole02CheckListComponent,
//   DocumentRole02CheckComponent,
//   DocumentRole02PrintDocumentListComponent,
//   DocumentRole02PrintCoverListComponent,
//   DocumentRole02PrintListListComponent,
//   RequestCheckComponent,
//   RequestSearchPeopleComponent,
//   Floor3DocumentComponent
// } from './pages'
// import {
//   ROUTE_PATH
// } from './helpers'

// const routes: Routes = [
//   { path: `${ROUTE_PATH.DASHBOARD.ROUTE}`, component: DashboardComponent },

//   { path: `${ROUTE_PATH.REGISTRATION_REQUEST.ROUTE}`, component: RegistrationRequestComponent },
//   { path: `${ROUTE_PATH.REGISTRATION_REQUEST_ADD.ROUTE}`, component: RegistrationRequestComponent },
//   { path: `${ROUTE_PATH.REGISTRATION_REQUEST_EDIT.ROUTE}/:id`, component: RegistrationRequestComponent },

//   { path: `${ROUTE_PATH.REPRESENTATIVE_MANAGEMENT.ROUTE}`, component: RepresentativeManagementComponent },
//   { path: `${ROUTE_PATH.DAILY_FEE_INCOME_RECORDS.ROUTE}`, component: DailyFeeIncomeRecordsComponent },

//   { path: `${ROUTE_PATH.REPRESENTATIVE_MANAGEMENT.ROUTE}`, component: RepresentativeManagementComponent },

//   { path: `${ROUTE_PATH.REGISTRATION_OTHER_REQUEST.ROUTE}`, component: RegistrationOtherRequestComponent },
//   { path: `${ROUTE_PATH.REGISTRATION_OTHER_REQUEST_ADD.ROUTE}`, component: RegistrationOtherRequestComponent },
//   { path: `${ROUTE_PATH.REGISTRATION_OTHER_REQUEST_EDIT.ROUTE}/:id`, component: RegistrationOtherRequestComponent },

//   { path: `${ROUTE_PATH.REQUEST_01_ITEM_LIST.ROUTE}`, component: Request01ItemListComponent },

//   { path: `${ROUTE_PATH.REQUEST_OTHER_ITEM_LIST.ROUTE}`, component: RequestOtherItemListComponent },

//   { path: `${ROUTE_PATH.TEST_LOGIN.ROUTE}`, component: TestLoginComponent },

//   { path: `${ROUTE_PATH.RECEIPT_DAILY.ROUTE}`, component: ReceiptDailyComponent },
//   { path: `${ROUTE_PATH.RECEIPT_DAILY_EDIT.ROUTE}/:id`, component: ReceiptDailyComponent },

//   { path: `${ROUTE_PATH.SAVE_01_PROCESS_LIST.ROUTE}`, component: Save01ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_01_PROCESS.ROUTE}`, component: Save01ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_01_PROCESS_ADD.ROUTE}`, component: Save01ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_01_PROCESS_EDIT.ROUTE}/:id`, component: Save01ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_020_PROCESS.ROUTE}`, component: Save020ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_020_PROCESS_ADD.ROUTE}`, component: Save020ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_020_PROCESS_EDIT.ROUTE}/:id`, component: Save020ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_020_PROCESS_LIST.ROUTE}`, component: Save020ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_021_PROCESS.ROUTE}`, component: Save021ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_021_PROCESS_ADD.ROUTE}`, component: Save021ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_021_PROCESS_EDIT.ROUTE}/:id`, component: Save021ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_021_PROCESS_LIST.ROUTE}`, component: Save021ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_03_PROCESS.ROUTE}`, component: Save03ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_03_PROCESS_LIST.ROUTE}`, component: Save03ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_04_PROCESS.ROUTE}`, component: Save04ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_04_PROCESS_LIST.ROUTE}`, component: Save04ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_05_PROCESS.ROUTE}`, component: Save05ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_05_PROCESS_LIST.ROUTE}`, component: Save05ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_06_PROCESS.ROUTE}`, component: Save06ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_06_PROCESS_LIST.ROUTE}`, component: Save06ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_07_PROCESS.ROUTE}`, component: Save07ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_07_PROCESS_LIST.ROUTE}`, component: Save07ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_08_PROCESS.ROUTE}`, component: Save08ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_08_PROCESS_LIST.ROUTE}`, component: Save08ProcessListComponent },

//   { path: `${ROUTE_PATH.SAVE_140_PROCESS.ROUTE}`, component: Save140ProcessComponent },

//   { path: `${ROUTE_PATH.REQUEST_SEARCH.ROUTE}`, component: RequestSearchComponent },
//   { path: `${ROUTE_PATH.REQUEST_SEARCH_LOAD.ROUTE}/:id`, component: RequestSearchComponent },

//   { path: `${ROUTE_PATH.REQUEST_DOCUMENT_COLLECT.ROUTE}`, component: RequestDocumentCollectComponent },

//   { path: `${ROUTE_PATH.ALL_TASK.ROUTE}`, component: AllTaskComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_PROCESS_CLASSIFICATION.ROUTE}`, component: DocumentProcessClassificationComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_PROCESS_CLASSIFICATION_DO.ROUTE}`, component: DocumentProcessClassificationDoComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_PROCESS_CLASSIFICATION_DO_EDIT.ROUTE}/:id`, component: DocumentProcessClassificationDoComponent },

//   { path: `${ROUTE_PATH.SAVE_04_PROCESS.ROUTE}`, component: Save04ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_04_PROCESS_ADD.ROUTE}`, component: Save04ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_04_PROCESS_EDIT.ROUTE}/:id`, component: Save04ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_05_PROCESS.ROUTE}`, component: Save05ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_05_PROCESS_ADD.ROUTE}`, component: Save05ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_05_PROCESS_EDIT.ROUTE}/:id`, component: Save05ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_07_PROCESS.ROUTE}`, component: Save07ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_07_PROCESS_ADD.ROUTE}`, component: Save07ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_07_PROCESS_EDIT.ROUTE}/:id`, component: Save07ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_08_PROCESS.ROUTE}`, component: Save08ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_08_PROCESS_ADD.ROUTE}`, component: Save08ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_08_PROCESS_EDIT.ROUTE}/:id`, component: Save08ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_140_PROCESS.ROUTE}`, component: Save140ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_140_PROCESS_ADD.ROUTE}`, component: Save140ProcessComponent },
//   { path: `${ROUTE_PATH.SAVE_140_PROCESS_EDIT.ROUTE}/:id`, component: Save140ProcessComponent },

//   { path: `${ROUTE_PATH.SAVE_140_PROCESS_LIST.ROUTE}`, component: Save140ProcessListComponent },

//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_IMAGE.ROUTE}`, component: CheckingSimilarImageComponent },

//   { path: `${ROUTE_PATH.CHECKING_ITEM_LIST.ROUTE}`, component: CheckingItemListComponent },

//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_LIST.ROUTE}`, component: CheckingSimilarListComponent },

//   { path: `${ROUTE_PATH.CHECKING_SIMILAR.ROUTE}`, component: CheckingSimilarComponent },
//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_ADD.ROUTE}`, component: CheckingSimilarComponent },
//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_EDIT.ROUTE}/:id`, component: CheckingSimilarComponent },

//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_WORD.ROUTE}`, component: CheckingSimilarWordComponent },
//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_WORD_ADD.ROUTE}`, component: CheckingSimilarWordComponent },
//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_WORD_EDIT.ROUTE}/:id`, component: CheckingSimilarWordComponent },

//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_RESULT.ROUTE}`, component: CheckingSimilarResultComponent },
//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_RESULT_ADD.ROUTE}`, component: CheckingSimilarResultComponent },
//   { path: `${ROUTE_PATH.CHECKING_SIMILAR_RESULT_EDIT.ROUTE}/:id`, component: CheckingSimilarResultComponent },

//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_LIST.ROUTE}`, component: ConsideringSimilarListComponent },

//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR.ROUTE}`, component: ConsideringSimilarComponent },
//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_ADD.ROUTE}`, component: ConsideringSimilarComponent },
//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_EDIT.ROUTE}/:id`, component: ConsideringSimilarComponent },

//   { path: `${ROUTE_PATH.PUBLIC_DASHBOARD_ROLE_01.ROUTE}`, component: PublicDashboardRole01Component },

//   { path: `${ROUTE_PATH.EFORM_SAVE_01_PROCESS.ROUTE}`, component: eFormSave01ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_02_PROCESS.ROUTE}`, component: eFormSave02ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_HOME.ROUTE}`, component: eFormHomeComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_01_PROCESS_ADD.ROUTE}`, component: eFormSave01ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_01_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave01ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_BRAND_TYPE.ROUTE}`, component: EFormBrandTypeComponent },
//   { path: `${ROUTE_PATH.EFORM_OWNER_AND_AGENT.ROUTE}`, component: EFormOwnerAndAgentComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_ITEM_LIST.ROUTE}`, component: DocumentItemListComponent },

//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_INSTRUCTION.ROUTE}`, component: ConsideringSimilarInstructionComponent },
//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_INSTRUCTION_ADD.ROUTE}`, component: ConsideringSimilarInstructionComponent },
//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_INSTRUCTION_EDIT.ROUTE}/:id`, component: ConsideringSimilarInstructionComponent },

//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_DOCUMENT.ROUTE}`, component: ConsideringSimilarDocumentComponent },
//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_DOCUMENT_ADD.ROUTE}`, component: ConsideringSimilarDocumentComponent },
//   { path: `${ROUTE_PATH.CONSIDERING_SIMILAR_DOCUMENT_EDIT.ROUTE}/:id`, component: ConsideringSimilarDocumentComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE01_ITEM_LIST.ROUTE}`, component: PublicRole01ItemListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE01_CHECK_LIST.ROUTE}`, component: PublicRole01CheckListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE01_CHECK_DO.ROUTE}`, component: PublicRole01CheckDoComponent },
//   { path: `${ROUTE_PATH.PUBLIC_ROLE01_CHECK_DO_ADD.ROUTE}`, component: PublicRole01CheckDoComponent },
//   { path: `${ROUTE_PATH.PUBLIC_ROLE01_CHECK_DO_EDIT.ROUTE}/:id`, component: PublicRole01CheckDoComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE01_PREPARE_LIST.ROUTE}`, component: PublicRole01PrepareListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE01_DOING_LIST.ROUTE}`, component: PublicRole01DoingListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE02_CHECK_LIST.ROUTE}`, component: PublicRole02CheckListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE02_CONSIDER_PAYMENT_LIST.ROUTE}`, component: PublicRole02ConsiderPaymentListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE05_CONSIDER_PAYMENT_LIST.ROUTE}`, component: PublicRole05ConsiderPaymentListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE02_DOCUMENT_PAYMENT_LIST.ROUTE}`, component: PublicRole02DocumentPaymentListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE02_DOCUMENT_POST_LIST.ROUTE}`, component: PublicRole02DocumentPostListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE02_ACTION_POST_LIST.ROUTE}`, component: PublicRole02ActionPostListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE04_ITEM_LIST.ROUTE}`, component: PublicRole04itemListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE04_CHECK_LIST.ROUTE}`, component: PublicRole04CheckListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE05_DOCUMENT_LIST.ROUTE}`, component: PublicRole05DocumentListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE04_DOCUMENT_LIST.ROUTE}`, component: PublicRole04DocumentListComponent },

//   { path: `${ROUTE_PATH.PUBLIC_ROLE02_DOCUMENT_LIST.ROUTE}`, component: PublicRole02DocumentListComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_ROLE02_ITEM_LIST.ROUTE}`, component: DocumentRole02ItemListComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_140_PROCESS.ROUTE}`, component: eFormSave140ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_140_PROCESS_ADD.ROUTE}`, component: eFormSave140ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_140_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave140ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_150_PROCESS.ROUTE}`, component: eFormSave150ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_150_PROCESS_ADD.ROUTE}`, component: eFormSave150ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_150_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave150ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_190_PROCESS.ROUTE}`, component: eFormSave190ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_190_PROCESS_ADD.ROUTE}`, component: eFormSave190ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_190_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave190ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_200_PROCESS.ROUTE}`, component: eFormSave200ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_200_PROCESS_ADD.ROUTE}`, component: eFormSave200ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_200_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave200ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_030_PROCESS.ROUTE}`, component: eFormSave030ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_030_PROCESS_ADD.ROUTE}`, component: eFormSave030ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_030_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave030ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_040_PROCESS.ROUTE}`, component: eFormSave040ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_040_PROCESS_ADD.ROUTE}`, component: eFormSave040ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_040_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave040ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_050_PROCESS.ROUTE}`, component: eFormSave050ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_050_PROCESS_ADD.ROUTE}`, component: eFormSave050ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_050_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave050ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_060_PROCESS.ROUTE}`, component: eFormSave060ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_060_PROCESS_ADD.ROUTE}`, component: eFormSave060ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_060_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave060ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_070_PROCESS.ROUTE}`, component: eFormSave070ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_070_PROCESS_ADD.ROUTE}`, component: eFormSave070ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_070_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave070ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_080_PROCESS.ROUTE}`, component: eFormSave080ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_080_PROCESS_ADD.ROUTE}`, component: eFormSave080ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_080_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave080ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_021_PROCESS.ROUTE}`, component: eFormSave021ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_021_PROCESS_ADD.ROUTE}`, component: eFormSave021ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_021_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave021ProcessComponent },

//   { path: `${ROUTE_PATH.EFORM_SAVE_120_PROCESS.ROUTE}`, component: eFormSave120ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_120_PROCESS_ADD.ROUTE}`, component: eFormSave120ProcessComponent },
//   { path: `${ROUTE_PATH.EFORM_SAVE_120_PROCESS_EDIT.ROUTE}/:id`, component: eFormSave120ProcessComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_ROLE02_CHECK_LIST.ROUTE}`, component: DocumentRole02CheckListComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_ROLE02_CHECK.ROUTE}`, component: DocumentRole02CheckComponent },
//   { path: `${ROUTE_PATH.DOCUMENT_ROLE02_CHECK_ADD.ROUTE}`, component: DocumentRole02CheckComponent },
//   { path: `${ROUTE_PATH.DOCUMENT_ROLE02_CHECK_EDIT.ROUTE}/:id`, component: DocumentRole02CheckComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_ROLE02_PRINT_DOCUMENT_LIST.ROUTE}`, component: DocumentRole02PrintDocumentListComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_ROLE02_PRINT_COVER_LIST.ROUTE}`, component: DocumentRole02PrintCoverListComponent },

//   { path: `${ROUTE_PATH.DOCUMENT_ROLE02_PRINT_LIST_LIST.ROUTE}`, component: DocumentRole02PrintListListComponent },

//   { path: `${ROUTE_PATH.REQUEST_CHECK.ROUTE}`, component: RequestCheckComponent },
//   { path: `${ROUTE_PATH.REQUEST_CHECK_ADD.ROUTE}`, component: RequestCheckComponent },
//   { path: `${ROUTE_PATH.REQUEST_CHECK_EDIT.ROUTE}/:id`, component: RequestCheckComponent },

//   { path: `${ROUTE_PATH.REQUEST_SEARCH_PEOPLE.ROUTE}`, component: RequestSearchPeopleComponent },

//   { path: `${ROUTE_PATH.FLOOR3_DOCUMENT.ROUTE}`, component: Floor3DocumentComponent },

//   { path: '', redirectTo: `${ROUTE_PATH.DASHBOARD.LINK}`, pathMatch: 'full' }, // Default page
//   { path: '**', component: NotFoundComponent } // Page not found
// ]

// @NgModule({
//   imports: [RouterModule.forRoot(routes)],
//   exports: [RouterModule]
// })
// export class AppRoutingModule { }
