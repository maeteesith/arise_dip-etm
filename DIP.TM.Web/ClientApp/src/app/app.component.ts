import { Component } from "@angular/core";
import { GlobalService } from "./global.service";
import { Router, NavigationStart, NavigationEnd } from '@angular/router'
import { Auth } from "./auth";
import { UserService } from "./services/user.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastrService } from "ngx-toastr";
import { LOCAL_STORAGE, LOGIN_URL, setLocalStorage } from "./helpers";

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"]
})
export class AppComponent {
    //TODO >>> Declarations <<<
    public token: any;
    public isLogin: any;
    public isEForm: any;
    public isVoucherNumber: any;

    constructor(
        private global: GlobalService,
        private router: Router,
        private auth: Auth,
        private userService: UserService,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService
    ) {
        //TODO >>> Detect global change <<<
        this.global.watchUserInfo().subscribe((data: any) => {
            this.isLogin = data ? true : false;
        });
        this.global.watchLoading().subscribe((isShow: boolean) => {
            isShow ? this.spinner.show() : this.spinner.hide();
        });
        this.global.watchToast().subscribe((data: any) => {
            this.toastr[data.action](data.message, data.header);
        });
        //TODO >>> Detect route change <<<
        this.router.events.subscribe((event: any) => {
            if (event instanceof NavigationStart) {
                this.global.setLoading(true);
            }
            if (event instanceof NavigationEnd) {
                this.global.setLoading(false);
                this.isEForm = event.url.includes('eform') && !event.url.startsWith("/request")
                this.isVoucherNumber = event.url.includes('request-search?request_check_voucher_number') ||
                    event.url.includes('request-search-people')
            }
        })
    }

    ngOnInit() {
        //TODO >>> Init value <<<
        this.isLogin = this.auth.isLogin();
        //console.log(location.pathname)
        this.isEForm = location.pathname.includes('eform') && !location.pathname.startsWith("/request");
        this.isVoucherNumber = location.href.includes('request-search?request_check_voucher_number') ||
            location.href.includes('request-search-people')

        //TODO >>> Check token <<<
        if (!this.isEForm && !this.isVoucherNumber) {
            if (this.auth.getToken()) {
                console.log('You has token.')
                if (!this.isLogin) {
                    this.callGetUserInfo({});
                }
            } else {
                console.log('You not has token.')
                this.redirect(LOGIN_URL)
            }
        }
    }

    //! <<< Call API >>>
    callGetUserInfo(params: any): void {
        // Open loading
        this.global.setLoading(true);
        this.userService.GetUserInfo(params).subscribe((data: any) => {
            if (data) {
                this.isLogin = true
                this.global.setUserInfo(data);
                console.log(data)
                setLocalStorage(LOCAL_STORAGE.USER_INFO, data);
            } else {
                this.redirect(LOGIN_URL);
            }
            // Close loading
            this.global.setLoading(false);
        });
    }

    //! <<< Other >>>
    redirect(url: string): void {
        location.href = url;
    }
}
