export * from './requestProcess.service';
import { RequestProcessService } from './requestProcess.service';
export * from './upload.service';
import { UploadService } from './upload.service';
export * from './user.service';
import { UserService } from './user.service';
export const APIS = [RequestProcessService, UploadService, UserService];
