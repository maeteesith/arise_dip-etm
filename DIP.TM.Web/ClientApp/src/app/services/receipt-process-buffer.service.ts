import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class ReceiptProcessService {
  private apiPath = '/ReceiptProcess'
  constructor(private apiService: ApiService) { }
  Load(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Load`, params)
  }
  Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Reset`, params)
  }
  List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/List`, params)
  }
  LoadUnpaid(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadUnpaid/${param}`, params)
  }
  ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ItemAdd`, params)
  }
  ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ItemDelete`, params)
  }
  PaidAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/PaidAdd`, params)
  }
  PaidDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/PaidDelete`, params)
  }
  Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save`, params)
  }
  ReceiptDailyList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ReceiptDailyList`, params)
  }

  ReceiptDailyReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ReceiptDailyReset`, params)
  }

  ReceiptDailyAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ReceiptDailyAdd`, params)
  }

  ReceiptDailyDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ReceiptDailyDelete`, params)
  }

ReceiptDailyLoad(param: any, params: any = null): Observable < any > {
  return this.apiService.post(`${this.apiPath}/ReceiptDailyLoad/${param}`, params)
}


}
