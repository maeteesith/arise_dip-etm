import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class eFormSaveProcessService {
  private apiPath = '/EFormSaveProcess'
  constructor(private apiService: ApiService) { }

  SearchLocation(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave01ListModalLocation`, params)
  }

  eFormFromRequestNumberLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormFromRequestNumberLoad/${param}`, params)
  }

  eForm070FromRequestNumberLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eForm070FromRequestNumberLoad/${param}`, params)
  }

  eForm120FromRequestNumberLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eForm120FromRequestNumberLoad/${param}`, params)
  }

  eFormSave120Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm120ByID/${param}`, params) //[edit]comment_tag: eFormSave120Load to LoadEForm120ByID
  }
  eFormSave030Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm030ByID/${param}`, params) //[edit]comment_tag: eFormSave030Load to LoadEForm030ByID
  }
  eFormSave030LoadConsideringSimilarDocument(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave030LoadConsideringSimilarDocument/${param}`, params)
  }
  eFormSave040Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm040ByID/${param}`, params) //[edit]comment_tag: eFormSave040Load to LoadEForm040ByID
  }
  eFormSave050Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm050ByID/${param}`, params) //[edit]comment_tag: eFormSave050Load to LoadEForm050ByID
  }
  eFormSave060Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave060Load/${param}`, params)
  }
  eFormSave070Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm070ByID/${param}`, params) //[edit]comment_tag: eFormSave070Load to LoadEForm070ByID
  }
  eFormSave080Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm080ByID/${param}`, params) //[edit]comment_tag: eFormSave080Load to LoadEForm080ByID
  }
  eFormSave021Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm021ByID/${param}`, params) //[edit]comment_tag: eFormSave021Load to LoadEForm021ByID
  }

  eFormSave120Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave120Save`, params)
  }
  eFormSave010Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave010Save`, params)
  }
  eFormSave030Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave030Save`, params)
  }

  eFormSave040Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave040Save`, params)
  }
  eFormSave050Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave050Save`, params)
  }
  eFormSave060Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave060Save`, params)
  }
  eFormSave070Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave070Save`, params)
  }
  eFormSave080Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave080Save`, params)
  }
  eFormSave021Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave021Save`, params)
  }

}
