import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'

@Injectable({
  providedIn: 'root'
})
export class ReceiptProcessService {
  private apiPath = '/ReceiptProcess'

  constructor(private apiService: ApiService) { }

  Load(id: any, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Load/${id}`, params)
  }

  LoadUnpaid(reference_number: any, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadUnpaid/${reference_number}`, params)
  }

}
