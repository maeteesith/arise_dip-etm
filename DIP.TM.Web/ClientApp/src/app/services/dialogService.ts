import { MatDialog } from "@angular/material";
import { of } from "rxjs";
import { ConfirmationDialogComponent } from '../shared/dialog/confirmation/confirmation.dialog.component'
import { switchMap } from "rxjs/operators";
import { Injectable } from "@angular/core";
import { EditorDialogComponent } from "../shared/dialog/editor/editor.dialog.component";
@Injectable({
    providedIn: 'root'
  })
export class DialogService {

    constructor(private _dialog: MatDialog) {

    }

    public showDecisionConfirm(title: string,titlecolor: string, description: string, button: any[]) {
		if (!this._dialog) return of(false);
		
		const dialogRef = this._dialog.open(ConfirmationDialogComponent, {
			hasBackdrop: true,
			panelClass: 'kt-mat-dialog-container__wrapper',
			height: '223px',
			disableClose: true,
			width: '481px',
			data: {
                title: title,
                titlecolor: titlecolor,
				description: description,
                iconUrl: "",
                buttons: button
				// buttons: [
                //     { id: "CANCEL",butclass:"dip-icon-close", border:"solid 1px #C5C7C9", color: "#C5C7C9", name: "ยกเลิก" },
				// 	{ id: "YES", name: action, color: "#ffffff", backgroundColor: "#0C84C8" }
					
				// ]
			}
		});

		return dialogRef.afterClosed().pipe(
			switchMap(result => {
				
				return of(result);
			})
		);
    }
    
    public showEditorConfirm(header: string,headerIcon: string, detailHtml: string, button: any[]) {
		if (!this._dialog) return of(false);
		
		const dialogRef = this._dialog.open(EditorDialogComponent, {
			hasBackdrop: true,
			panelClass: 'kt-mat-dialog-container__wrapper',
			height: '427px',
			disableClose: true,
			width: '674px',
			data: {
                headerIcon: headerIcon,
                header: header,
				detailHtml: detailHtml,
                buttons: button
				// buttons: [
                //     { id: "CANCEL",butclass:"dip-icon-close", border:"solid 1px #C5C7C9", color: "#C5C7C9", name: "ยกเลิก" },
				// 	{ id: "YES", name: action, color: "#ffffff", backgroundColor: "#0C84C8" }
					
				// ]
			}
		});

		return dialogRef.afterClosed().pipe(
			switchMap(result => {
				
				return of(result);
			})
		);
	}
}