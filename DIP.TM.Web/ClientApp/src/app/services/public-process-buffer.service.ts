import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
    providedIn: 'root'
})
export class PublicProcessService {
    private apiPath = '/PublicProcess'
    constructor(private apiService: ApiService) { }
    PublicRole01Load(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01Load`, params)
    }
    PublicItemPublicReceiverList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicItemPublicReceiverList`, params)
    }
    PublicItemList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicItemList`, params)
    }
    PublicItemReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicItemReset`, params)
    }
    PublicItemAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicItemAdd`, params)
    }
    PublicItemDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicItemDelete`, params)
    }
    PublicItemLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicItemLoad/${param}`, params)
    }
    PublicRole01ItemPublicReceiverList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01ItemPublicReceiverList`, params)
    }
    PublicRole01PreparePublicRoundAdd(params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRoundAdd`, params)
    }
    PublicRole01ItemList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01ItemList`, params)
    }
    PublicRole01ItemAutoSplit(params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01ItemAutoSplit`, params)
    }
    PublicInstructionList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicInstructionList`, params)
    }
    CertificationFileAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/CertificationFileAdd`, params)
    }
    CertificationFileList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/CertificationFileList`, params)
    }
    HistoryList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/HistoryList`, params)
    }
    RequestDocumentCollectList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/RequestDocumentCollectList`, params)
    }
    ReceiptList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/ReceiptList`, params)
    }
    PublicRole01ItemReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01ItemReset`, params)
    }
    PublicRole01ItemPublicItemAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01ItemPublicItemAdd`, params)
    }
    PublicRole01ItemPublicItemDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01ItemPublicItemDelete`, params)
    }
    PublicRole01ItemLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01ItemLoad/${param}`, params)
    }
    PublicRole01CheckList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckList`, params)
    }
    PublicRole01CheckReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckReset`, params)
    }
    PublicRole01CheckDoAuto(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckDoAuto`, params)
    }
    PublicRole01CheckAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckAdd`, params)
    }
    PublicRole01CheckDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckDelete`, params)
    }
    PublicRole01CheckLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckLoad/${param}`, params)
    }
    PublicRole01Case41CheckList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01Case41CheckList`, params)
    }
    PostRoundActionImageList(param: any, params: any = null) {
        return this.apiService.post(`${this.apiPath}/PostRoundActionImageList/${param}`, params)
    }
    PostRoundActionImageSave(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PostRoundActionImageSave`, params)
    }
    PublicRole01CheckSendChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckSendChange`, params)
    }
    PublicRole02ConsiderPaymentSendChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsiderPaymentSendChange`, params)
    }
    PublicRole02DocumentPaymentSendChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPaymentSendChange`, params)
    }
    PublicRole05ConsiderPaymentSendChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05ConsiderPaymentSendChange`, params)
    }
    PublicRole04DocumentSendChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04DocumentSendChange`, params)
    }
    PublicRole05DocumentSendChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05DocumentSendChange`, params)
    }
    PublicRole04CheckSendChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04CheckSendChange`, params)
    }
    PublicRole01CheckSave(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckSave`, params)
    }
    PublicRole01CheckSend(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckSend/${param}`, params)
    }
    PublicRole01CheckDocumentScanListAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckDocumentScanListAdd`, params)
    }
    PublicRole01CheckDocumentScanListDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckDocumentScanListDelete`, params)
    }
    PublicRole01CheckInstructionListAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckInstructionListAdd`, params)
    }
    PublicRole01CheckInstructionListDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckInstructionListDelete`, params)
    }
    PublicRole01CheckReceiptListAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckReceiptListAdd`, params)
    }
    PublicRole01CheckReceiptListDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01CheckReceiptListDelete`, params)
    }
    PublicRole01PrepareList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01PrepareList`, params)
    }
    PublicRole01PrepareReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01PrepareReset`, params)
    }
    PublicRole01PrepareAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01PrepareAdd`, params)
    }
    PublicRole01PrepareDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01PrepareDelete`, params)
    }
    PublicRole01PrepareLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01PrepareLoad/${param}`, params)
    }
    PublicRole01DoingList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01DoingList`, params)
    }
    PublicRole01DoingReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01DoingReset`, params)
    }
    PublicRole01DoingAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01DoingAdd`, params)
    }
    PublicRole01DoingDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01DoingDelete`, params)
    }
    PublicRole01DoingLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01DoingLoad/${param}`, params)
    }
    PublicRoundAdd(params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRoundAdd`, params)
    }
    PublicRoundEnd(params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRoundEnd`, params)
    }
    PublicRole02CheckList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02CheckList`, params)
    }
    PublicRole02CheckReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02CheckReset`, params)
    }
    PublicRole02CheckAutoDo(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02CheckAutoDo`, params)
    }
    PublicRole02CheckAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02CheckAdd`, params)
    }
    PublicRole02CheckDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02CheckDelete`, params)
    }
    PublicRole02CheckLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02CheckLoad/${param}`, params)
    }
    PublicRole02ConsiderPaymentList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsiderPaymentList`, params)
    }
    PublicRole02ConsiderPaymentReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsiderPaymentReset`, params)
    }
    PublicRole02ConsiderPaymentAutoDo(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsiderPaymentAutoDo`, params)
    }
    PublicRole02ConsiderPaymentAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsiderPaymentAdd`, params)
    }
    PublicRole02ConsiderPaymentDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsiderPaymentDelete`, params)
    }
    PublicRole02ConsiderPaymentLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsiderPaymentLoad/${param}`, params)
    }
    PublicRole02ConsiderPaymentSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsiderPaymentSend`, params)
    }
    PublicRole05ConsiderPaymentList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05ConsiderPaymentList`, params)
    }
    PublicRole05ConsiderPaymentReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05ConsiderPaymentReset`, params)
    }
    PublicRole05ConsiderPaymentSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05ConsiderPaymentSend`, params)
    }
    PublicRole05ConsiderPaymentAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05ConsiderPaymentAdd`, params)
    }
    PublicRole05ConsiderPaymentDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05ConsiderPaymentDelete`, params)
    }
    PublicRole02DocumentPaymentList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPaymentList`, params)
    }
    PublicRole02DocumentPaymentReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPaymentReset`, params)
    }
    PublicRole02DocumentPaymentSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPaymentSend`, params)
    }
    PublicRole02DocumentPaymentAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPaymentAdd`, params)
    }
    PublicRole02DocumentPaymentDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPaymentDelete`, params)
    }
    PublicRole02DocumentPaymentLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPaymentLoad/${param}`, params)
    }
    PublicRole02DocumentPostList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPostList`, params)
    }
    PublicRole02DocumentPostReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPostReset`, params)
    }
    PublicRole02DocumentPostSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPostSend`, params)
    }
    PublicRole02DocumentPostAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPostAdd`, params)
    }
    PublicRole02DocumentPostDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPostDelete`, params)
    }
    PublicRole02DocumentPostLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentPostLoad/${param}`, params)
    }
    PublicRole02ActionPostList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ActionPostList`, params)
    }
    PublicRole02ActionPostReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ActionPostReset`, params)
    }
    PublicRole02ActionPostSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ActionPostSend`, params)
    }
    PublicRole02ActionPostPublicRole02DocumentPostAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ActionPostPublicRole02DocumentPostAdd`, params)
    }
    PublicRole02ActionPostPublicRole02DocumentPostDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ActionPostPublicRole02DocumentPostDelete`, params)
    }
    PublicRole02ActionPostLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ActionPostLoad/${param}`, params)
    }
    PublicRole04itemList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04itemList`, params)
    }
    PublicRole04ItemAutoSplit(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04ItemAutoSplit`, params)
    }
    PublicRole04itemReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04itemReset`, params)
    }
    PublicRole04itemAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04itemAdd`, params)
    }
    PublicRole04itemDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04itemDelete`, params)
    }
    PublicRole04itemLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04itemLoad/${param}`, params)
    }
    PublicRole04CheckList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04CheckList`, params)
    }
    PublicRole04CheckReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04CheckReset`, params)
    }
    PublicRole04CheckAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04CheckAdd`, params)
    }
    PublicRole04CheckDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04CheckDelete`, params)
    }
    SaveList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/SaveList`, params)
    }
    PublicRole04CheckLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04CheckLoad/${param}`, params)
    }
    PublicRole04CheckSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04CheckSend`, params)
    }
    PublicRole05DocumentList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05DocumentList`, params)
    }
    PublicRole05DocumentReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05DocumentReset`, params)
    }
    PublicRole05DocumentSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05DocumentSend`, params)
    }
    PublicRole05DocumentAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05DocumentAdd`, params)
    }
    PublicRole05DocumentDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05DocumentDelete`, params)
    }
    PublicRole05DocumentLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05DocumentLoad/${param}`, params)
    }
    PublicRole04DocumentList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04DocumentList`, params)
    }
    PublicRole04DocumentReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04DocumentReset`, params)
    }
    PublicRole04DocumentSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04DocumentSend`, params)
    }
    PublicRole04DocumentAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04DocumentAdd`, params)
    }
    PublicRole04DocumentDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04DocumentDelete`, params)
    }
    PublicRole04DocumentLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole04DocumentLoad/${param}`, params)
    }
    PublicRole02DocumentList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentList`, params)
    }
    PublicRole02DocumentReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentReset`, params)
    }
    PublicRole02DocumentSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentSend`, params)
    }
    PublicRole02DocumentAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentAdd`, params)
    }
    PublicRole02DocumentDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentDelete`, params)
    }
    PublicRole02DocumentLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02DocumentLoad/${param}`, params)
    }
    PublicRole01Case41CheckLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole01Case41CheckLoad/${param}`, params)
    }
    PublicOtherLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicOtherLoad/${param}`, params)
    }
    PublicConsideringOtherLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicConsideringOtherLoad/${param}`, params)
    }
    PublicRole05OtherSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole05OtherSend`, params)
    }
    PublicRole02OtherSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02OtherSend`, params)
    }
    PublicRole02ConsideringOtherSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsideringOtherSend`, params)
    }
    PublicRole02OtherSendChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02OtherSendChange`, params)
    }
    List(view_name: any, params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/List/${view_name}`, params)
    }
    PublicRole02OtherLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02OtherLoad/${param}`, params)
    }
    PublicRole02ConsideringOtherLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole02ConsideringOtherLoad/${param}`, params)
    }
    PublicRole03ReleaseLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/PublicRole03ReleaseLoad/${param}`, params)
    }
PublicRole02PrintDocumentLoad(param: any, params: any = null): Observable < any > {
  return this.apiService.post(`${this.apiPath}/PublicRole02PrintDocumentLoad/${param}`, params)
}


}
