import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class RequestCheckService {
  private apiPath = '/RequestCheck'
  constructor(private apiService: ApiService) { }
  RequestCheckRequestCheckLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckRequestCheckLoad`, params)
  }
  RequestCheckList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckList`, params)
  }
  RequestCheckSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckSave`, params)
  }
  RequestCheckReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckReset`, params)
  }
  RequestCheckPrintKor09(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckPrintKor09`, params)
  }
  RequestCheckPrint(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckPrint`, params)
  }
  RequestCheckPrintDocumentCopy(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckPrintDocumentCopy`, params)
  }
  RequestCheckPrintGuarantee(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckPrintGuarantee `, params)
  }
  RequestCheckPrintImportantDocuments(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckPrintImportantDocuments`, params)
  }
  RequestCheckPrintRegistration(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckPrintRegistration`, params)
  }
  RequestCheckPrintRequest(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckPrintRequest`, params)
  }
  RequestCheckAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckAdd`, params)
  }
  RequestCheckDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckDelete`, params)
  }
  RequestCheckLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckLoad/${param}`, params)
  }
  RequestCheckRequestNumberAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckRequestNumberAdd`, params)
  }
  RequestCheckRegistrarList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckRegistrarList`, params)
  }
  RequestItemSubTypeGroupList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestItemSubTypeGroupList`, params)
  }
  DocumentClassificationImageTypeCodeList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentClassificationImageTypeCodeList`, params)
  }
  RequestSoundTypeCodeList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestSoundTypeCodeList`, params)
  }
} 
