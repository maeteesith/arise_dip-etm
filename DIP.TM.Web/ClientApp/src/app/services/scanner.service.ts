import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
import { CONSTANTS } from '../helpers'
import { GlobalService } from '../global.service'
import { catchError, map } from 'rxjs/operators'
@Injectable({
    providedIn: 'root'
})
export class ScannerService {
    protected basePath = "http://127.0.0.1:8081";
    constructor(
        private global: GlobalService,
        private apiService: ApiService, protected httpClient: HttpClient, ) {

    }
    scanAndWait(params: any): Observable<any> {
        let url = `${this.basePath}`
        url += "?scan_type=" + params;
        return this.httpClient.get<any>(url, params).pipe(
            map((res: any) => {
                if (res.is_error) {
                    // Open toast error
                    let toast = CONSTANTS.TOAST.ERROR
                    toast.message = res.error_message
                    this.global.setToast(toast)
                    return false
                } else {
                    return res
                }
            })
        )
    }
}
