import { Injectable } from '@angular/core'
import { Observable, forkJoin } from 'rxjs'
import { ReferenceMasterService } from './reference-master.service'
import { RequestProcessService } from './request-process.service'

@Injectable({
  providedIn: 'root'
})
export class ForkJoinService {
  constructor(
    private ReferenceMasterService: ReferenceMasterService,
    private requestProcessService: RequestProcessService
  ) { }

  initEForm01Page(): Observable<any> {
    return forkJoin({
      requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      saveOTOPTypeCodeList: this.ReferenceMasterService.SaveOTOPTypeCodeList({}),
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestImageMarkTypeCodeList: this.ReferenceMasterService.RequestImageMarkTypeCodeList({}),
      translationLanguageCodeList: this.ReferenceMasterService.TranslationLanguageCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      assertTypeCodeList: this.ReferenceMasterService.AssertTypeCodeList({}),
      priceMasterList: this.requestProcessService.Request01PriceMaster({})
    })
  }

  initEForm02Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      requestTypeList: this.ReferenceMasterService.RequestTypeList({})
    })
  }

  initEForm021Page(): Observable<any> {
    return forkJoin({
      //itemTypeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
    })
  }

  initEForm04Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      save040RepresentativeTypeCodeList: this.ReferenceMasterService.Save040RepresentativeTypeCodeList({}),
      save040TransferTypeCodeList: this.ReferenceMasterService.Save040TransferTypeCodeList({}),
      save040TransferFormCodeList: this.ReferenceMasterService.Save040TransferFormCodeList({}),
      save040TransferPartCodeList: this.ReferenceMasterService.Save040TransferPartCodeList({}),
      save040TransferRequestTypeCodeList: this.ReferenceMasterService.Save040TransferRequestTypeCodeList({}),
      save040SubmitTypeCodeList: this.ReferenceMasterService.Save040SubmitTypeCodeList({}),
      save040DonorAddressTypeCodeList: this.ReferenceMasterService.Save040DonorAddressTypeCodeList({}),
      save040ReceiverAddressTypeCodeList: this.ReferenceMasterService.Save040ReceiverAddressTypeCodeList({}),
      requestTypeList: this.ReferenceMasterService.RequestTypeList({})
    })
  }

  initEForm120Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      submitTypeCodeList: this.ReferenceMasterService.Save120SubmitTypeCodeList({}),
    })
  }

  initEForm140Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      informerTypeCodeList: this.ReferenceMasterService.Save140InformerTypeCode({}),
      informTypeCodeList: this.ReferenceMasterService.Save140InformTypeCode({}),
      submitTypeCodeList: this.ReferenceMasterService.Save140SubmitTypeCode({}),
      depertmentTypeCodeList: this.ReferenceMasterService.Save140DepertmentTypeCode({})
    })
  }

  initEForm150Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      searchTypeCodeList: this.ReferenceMasterService.Save150SearchTypeCode({}),
      informerPeopleTypeCodeList: this.ReferenceMasterService.Save150InformerPeopleTypeCode({}),
      informerReceiverTypeCodeList: this.ReferenceMasterService.Save150InformerReceiverTypeCode({}),
      informerPeriodTypeCodeList: this.ReferenceMasterService.Save150InformerPeriodTypeCode({}),
    })
  }

  initEForm190Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      searchTypeCodeList: this.ReferenceMasterService.Save190SearchTypeCode({}),
      informerPeopleTypeCodeList: this.ReferenceMasterService.Save190InformerPeopleTypeCode({}),
      informerReceiverTypeCodeList: this.ReferenceMasterService.Save190InformerReceiverTypeCode({}),
      informerPeriodTypeCodeList: this.ReferenceMasterService.Save190InformerPeriodTypeCode({}),
      informerChallengerTypeCodeList: this.ReferenceMasterService.Save190InformerChallengerTypeCode({}),
      requestTypeCodeList: this.ReferenceMasterService.Save190RequestTypeCode({}),
    })
  }

  initEForm200Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      subjectTypeCodeList: this.ReferenceMasterService.Save200SubjectTypeCode({}),
      requestTypeCodeList: this.ReferenceMasterService.Save200RequestTypeCode({}),
      receipientTypeCodeList: this.ReferenceMasterService.Save200ReceipientTypeCode({}),
    })
  }

  initEForm210Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      searchTypeCodeList: this.ReferenceMasterService.Save210SearchTypeCode({}),
    })
  }

  initEForm220Page(): Observable<any> {
    return forkJoin({
      requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestImageMarkTypeCodeList: this.ReferenceMasterService.RequestImageMarkTypeCodeList({}),
      translationLanguageCodeList: this.ReferenceMasterService.TranslationLanguageCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      assertTypeCodeList: this.ReferenceMasterService.AssertTypeCodeList({}),
      priceMasterList: this.requestProcessService.Request01PriceMaster({}),
      searchTypeCodeList: this.ReferenceMasterService.Save210SearchTypeCode({}),
    })
  }

  initEForm230Page(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      documentTypeCodeList: this.ReferenceMasterService.Save230DocumentTypeCode({}),
    })
  }

}
