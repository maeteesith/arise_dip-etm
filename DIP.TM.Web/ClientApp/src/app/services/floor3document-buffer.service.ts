import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class Floor3DocumentService {
  private apiPath = '/OneStopServiceProcess'
  constructor(private apiService: ApiService) { }

  Floor3DocumentLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Floor3DocumentLoad/${param}`, params)
  }

  Floor3RegistrarList(params: any = {}): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Floor3RegistrarList`, params)
  }


  RequestExtendDoneList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestExtendDoneList`, params)
  }
  PrintSelect(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/PrintSelect`, params)
  }
  PrintAll(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/PrintAll`, params)
  }
  PrintBySave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/PrintBySave`, params)
  }
  RequestNumberListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestNumberListAdd`, params)
  }
  RequestNumberListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestNumberListDelete`, params)
  }
  Floor3DocumentRequestNumberAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Floor3DocumentRequestNumberAdd`, params)
  }
}
