import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class eFormSaveProcessService {
  private apiPath = '/EFormSaveProcess'
  constructor(private apiService: ApiService) { }

  SearchLocation(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave01ListModalLocation`, params)
  }

  ProductCatagory01ItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ProductCatagory01ItemList`, params)
  }
  eFormSave010Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave010Save`, params)
  }
  eFormSave010Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm010ByID/${id}`, params)
  }

  eFormSave020Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave020Save`, params)
  }
  eFormSave020Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm020ByID/${id}`, params)
  }
  eFormSave020Search(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm020ByRequestNumber/${id}`, params)
  }

  eFormSave021Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave021Save`, params)
  }
  eFormSave021Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm021ByID/${id}`, params)
  }
  eFormSave021Search(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm021ByRequestNumber/${id}`, params)
  }

  eFormSave040Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave040Save`, params)
  }
  eFormSave040Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm040ByID/${id}`, params)
  }
  eFormSave040Search(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm040ByRequestNumber/${id}`, params)
  }

  eFormSave120Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave120Save`, params)
  }
  eFormSave120Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm120ByID/${id}`, params)
  }
  eFormSave120Search(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm120ByRequestNumber/${id}`, params)
  }

  eFormSave140Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave140Save`, params)
  }
  eFormSave140Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm140ByID/${id}`, params)
  }
  eFormSave140Search(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm140ByRequestNumber/${id}`, params)
  }

  eFormSave150Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave150Save`, params)
  }
  eFormSave150Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm150ByID/${id}`, params)
  }
  eFormSave150SearchRequestNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm150ByRequestNumber/${id}`, params)
  }
  eFormSave150SearchContractNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm150ByContractNumber/${id}`, params)
  }

  eFormSave190Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave190Save`, params)
  }
  eFormSave190Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm190ByID/${id}`, params)
  }
  eFormSave190SearchRequestNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm190ByRequestNumber/${id}`, params)
  }
  eFormSave190SearchContractNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm190ByContractNumber/${id}`, params)
  }
  eFormSave190SearchChallengerNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm190ByChallengerNumber/${id}`, params)
  }

  eFormSave200Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave200Save`, params)
  }
  eFormSave200Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm200ByID/${id}`, params)
  }
  eFormSave200SearchRequestNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm200ByRequestNumber/${id}`, params)
  }
  eFormSave200SearchInstructionRule(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadInstructionRuleByInstructionNumber/${id}`, params)
  }
  eFormSave200LoadRequestList(code: any, id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Load${code}RequestList/${id}`, params)
  }

  eFormSave210Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave210Save`, params)
  }
  eFormSave210Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm210ByID/${id}`, params)
  }
  eFormSave210SearchRequestNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm210ByRequestNumber/${id}`, params)
  }
  eFormSave210SearchInterRegistrationNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm210ByInterRegistrationNumber/${id}`, params)
  }
  eFormSave210MultipleSearch(id: number, params: any): Observable<any> {
    return this.apiService.get(`${this.apiPath}/DateList210ByRequestNumber/${id}`, params)
  }

  eFormSave220Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave220Save`, params)
  }
  eFormSave220Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm220ByID/${id}`, params)
  }
  eFormSave220SearchRequestNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm220ByRequestNumber/${id}`, params)
  }
  eFormSave220SearchInterRegistrationNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm220ByInterRegistrationNumber/${id}`, params)
  }
  eFormSave220MultipleSearch(id: number, params: any): Observable<any> {
    return this.apiService.get(`${this.apiPath}/DateList220ByRequestNumber/${id}`, params)
  }
  
  eFormSave230Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave230Save`, params)
  }
  eFormSave230Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm230ByID/${id}`, params)
  }
  eFormSave230SearchRequestNumber(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm230ByRequestNumber/${id}`, params)
  }

  eFormSave030Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm030ByID/${param}`, params)
  }

  eFormSave030Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave030Save`, params)
  }

  eFormSave030LoadConsideringSimilarDocument(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave030LoadConsideringSimilarDocument/${param}`, params)
  }

  eFormSave030SearchRequestNumber(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm030ByRequestNumber/${param}`, params)
  }

  eFormSave030SearchChallengerNumber(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm030ByChallengerNumber/${param}`, params)
  }

  eFormSave030SearcContractNumber(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm030ByContractNumber/${param}`, params)
  }

  eFormSave050Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm050ByID/${param}`, params) //[edit]comment_tag: eFormSave050Load to LoadEForm050ByID
  }

  eFormSave050Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave050Save`, params)
  }

  eFormSave050SearchRequestNumber(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm050ByRequestNumber/${param}`, params)
  }

  eFormSave050SearcContractNumber(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm050ByContractNumber/${param}`, params)
  }

  eFormSave080Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm080ByID/${param}`, params) //[edit]comment_tag: eFormSave080Load to LoadEForm080ByID
  }

  eFormSave080Save(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/eFormSave080Save`, params)
  }

  eFormSave080SearchRequestNumber(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm080ByRequestNumber/${param}`, params)
  }

  eFormSave080SearcContractNumber(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/LoadEForm080ByContractNumber/${param}`, params)
  }
}
