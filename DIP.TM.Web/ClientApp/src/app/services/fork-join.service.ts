import { Injectable } from '@angular/core'
import { Observable, forkJoin } from 'rxjs'
import { ReferenceMasterService } from './reference-master.service'
import { RequestProcessService } from './request-process.service'
import { RequestAgencyService } from './request-agency.service'

@Injectable({
  providedIn: 'root'
})
export class ForkJoinService {
  constructor(
    private ReferenceMasterService: ReferenceMasterService,
    private requestProcessService: RequestProcessService,
    private RequestAgencyService: RequestAgencyService,
  ) { }

  initRegistrationRequestPage(): Observable<any> {
    return forkJoin({
      itemTypeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      soundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      facilitationActStatusList: this.ReferenceMasterService.FacilitationActStatusCodeList({}),
      priceMasterList: this.requestProcessService.Request01PriceMaster({}),
      commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({})
    })
  }

  initDailyFeeIncomeRecordsPage(): Observable<any> {
    return forkJoin({
      receiptChannelTypeList: this.ReferenceMasterService.ReceiptChannelTypeList({})
    })
  }

  initSave01ProcessPage(): Observable<any> {
    return forkJoin({
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }

  initIncludeRole01ChangeListPage(): Observable<any> {
    // TODO
    return forkJoin({
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }

  initIncludeRole01ChangePage(): Observable<any> {
    // TODO
    return forkJoin({
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }

  initConsiderLicenseRequestPage(): Observable<any> {
    // TODO
    return forkJoin({
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }

  initRequestKor06ChangePage(): Observable<any> {
    // TODO
    return forkJoin({
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }

  initAppealKORCommercialProvincePage(): Observable<any> {
    return forkJoin({
      commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({}),
      requestTypeList: this.ReferenceMasterService.RequestTypeList({}),
    })
  }

  initAppealRegisterRevokeKOR08Page(): Observable<any> {
    return forkJoin({
      requestRevokeCodeList: this.ReferenceMasterService.Save080RevokeTypeCodeList({}),
    })
  }

  initAppealKOROtherFloor3Page(): Observable<any> {
    return forkJoin({
      requestTypeList: this.ReferenceMasterService.RequestTypeList({}),
      agencyList: this.RequestAgencyService.ListView({}),
    })
  }

  initAppealBookPage(): Observable<any> {
    return forkJoin({
      appealReasonList: this.ReferenceMasterService.Save030AppealReasonCodeList({})
    })
  }

  initAppealReason(): Observable<any> {
    return forkJoin({
      appealBoardSolutionList: this.ReferenceMasterService.AppealBoardSolutionList({})
    })
  }

  initAppealRegisterConsiderBoardPage(): Observable<any> {
    return forkJoin({
      appealBoardSolutionList: this.ReferenceMasterService.AppealBoardSolutionList({})
    })
  }

  initAppealCaseSummaryPage(): Observable<any> {
    return forkJoin({
      appealSectionList: this.ReferenceMasterService.AppealSectionList({}),
      appealReasonList: this.ReferenceMasterService.Save030AppealReasonCodeList({})
    })
  }
}
