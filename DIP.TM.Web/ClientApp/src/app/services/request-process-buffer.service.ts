import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class RequestProcessService {
  private apiPath = '/RequestProcess'
  constructor(private apiService: ApiService) { }
  Request01ItemLoad(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01ItemLoad/${id}`, params)
  }
  RequestOtherPrint(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherPrint/${id}`, params)
  }
  RequestOtherLoad(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherLoad/${id}`, params)
  }
  Request01ItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01ItemList`, params)
  }
  RequestOtherItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemList`, params)
  }
  RequestOtherItemReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemReset`, params)
  }
  RequestOtherItemRequest01ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemRequest01ItemAdd`, params)
  }
  RequestOtherItemRequest01ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemRequest01ItemDelete`, params)
  }
  Request01ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01ItemAdd`, params)
  }
  Request01ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01ItemDelete`, params)
  }
  RequestOtherMakerAutoFill(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherMakerAutoFill`, params)
  }
  RequestOtherSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherSave`, params)
  }
  RequestOtherReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherReset`, params)
  }
  RequestOtherRequestOtherProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherProcessAdd`, params)
  }
  RequestOtherRequestOtherProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherProcessDelete`, params)
  }
  RequestOtherRequestOtherRequestTypeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherRequestTypeAdd`, params)
  }
  RequestOtherRequestOtherRequestTypeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherRequestTypeDelete`, params)
  }
  RequestOtherRequestOtherRequest01ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherRequest01ItemAdd`, params)
  }
  RequestOtherRequestOtherRequest01ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherRequest01ItemDelete`, params)
  }
  RequestOtherRequestOtherItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherItemAdd`, params)
  }
  RequestOtherRequestOtherItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherItemDelete`, params)
  }
  RequestOtherRequest01ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequest01ItemAdd`, params)
  }
  RequestOtherRequest01ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequest01ItemDelete`, params)
  }
  RequestOtherItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemAdd`, params)
  }
  RequestOtherItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemDelete`, params)
  }
  RequestOtherItemLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemLoad`, params)
  }
  RequestOtherItemRequestOtherItemSubAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemRequestOtherItemSubAdd`, params)
  }
  RequestOtherItemRequestOtherItemSubDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemRequestOtherItemSubDelete`, params)
  }
  RequestOtherItemSubList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemSubList`, params)
  }
  RequestOtherItemSubReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemSubReset`, params)
  }
  RequestOtherItemSubAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemSubAdd`, params)
  }
  RequestOtherItemSubDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherItemSubDelete`, params)
  }
RequestOtherItemSubLoad(param: any, params: any = null): Observable < any > {
  return this.apiService.post(`${this.apiPath}/RequestOtherItemSubLoad/${param}`, params)
}
  RequestPrintCoverPrint(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestPrintCoverPrint`, params)
  }

  RequestPrintCoverReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestPrintCoverReset`, params)
  }

RequestPrintCoverLoad(param: any, params: any = null): Observable < any > {
  return this.apiService.post(`${this.apiPath}/RequestPrintCoverLoad/${param}`, params)
}


}
