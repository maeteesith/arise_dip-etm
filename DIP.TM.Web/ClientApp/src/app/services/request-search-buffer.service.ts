import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class RequestSearchService {
  private apiPath = '/RequestSearch'
  constructor(private apiService: ApiService) { }
  RequestSearchLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestSearchLoad/${param}`, params)
  }
  Owner(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Owner`, params)
  }
  Representative(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Representative`, params)
  }
  Card(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Card`, params)
  }
  RequestGroup(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestGroup`, params)
  }
  AllowContact(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/AllowContact`, params)
  }
  TrademarkWarranty(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/TrademarkWarranty`, params)
  }
  TrademarkJoin(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/TrademarkJoin`, params)
  }
  Trademark07(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Trademark07`, params)
  }
  StatusLog(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/StatusLog`, params)
  }
  Fee(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Fee`, params)
  }
  FileAddress(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/FileAddress`, params)
  }
  BookSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/BookSend`, params)
  }
  Rule(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Rule`, params)
  }
  RegisterPermit(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RegisterPermit`, params)
  }
  RequestInstead(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestInstead`, params)
  }
  ProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ProductAdd`, params)
  }
  ProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ProductDelete`, params)
  }
  Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Load/${param}`, params)
  }
  OwnerChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/OwnerChange`, params)
  }
  RepresentativeChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RepresentativeChange`, params)
  }
  CardChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CardChange`, params)
  }
  ProductChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ProductChange`, params)
  }
  TrademarkWarrantyChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/TrademarkWarrantyChange`, params)
  }
  TrademarkJoinChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/TrademarkJoinChange`, params)
  }
  TransferLogChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/TransferLogChange`, params)
  }
  SplitRequestLogChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/SplitRequestLogChange`, params)
  }
  Start(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Start`, params)
  }

  Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Reset`, params)
  }


}
