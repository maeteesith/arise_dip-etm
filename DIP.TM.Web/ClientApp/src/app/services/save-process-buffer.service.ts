import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
    providedIn: 'root'
})
export class SaveProcessService {
    private apiPath = '/SaveProcess'
    constructor(private apiService: ApiService) { }
    Save01Load(id: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Load/${id}`, params)
    }
    Save020Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020Load/${param}`, params)
    }
    Save020LocationList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020LocationList/1`, params)
    }
    Save020Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020Save`, params)
    }
    Save020Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020Reset`, params)
    }
    Save020FileScanView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020FileScanView`, params)
    }
    Save020Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020Send`, params)
    }
    Save020People01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020People01Add`, params)
    }
    Save020People01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020People01Delete`, params)
    }
    Save020ProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ProductAdd`, params)
    }
    Save020ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ProductDelete`, params)
    }
    Save020PeopleAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020PeopleAdd`, params)
    }
    Save020PeopleDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020PeopleDelete`, params)
    }
    Save020RepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020RepresentativeAdd`, params)
    }
    Save020RepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020RepresentativeDelete`, params)
    }
    Save020OppositionRequestAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020OppositionRequestAdd`, params)
    }
    Save020OppositionRequestDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020OppositionRequestDelete`, params)
    }
    Save01List(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01List`, params)
    }
    Save01ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ListPage`, params)
    }
    Save01Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Reset`, params)
    }
    Save01SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01SaveProcessAdd`, params)
    }
    Save01SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01SaveProcessDelete`, params)
    }
    Save03Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03Load/${param}`, params)
    }
    Save03MadridLoad(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03MadridLoad`, params)
    }
    Save03LocationList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03LocationList/1`, params)
    }
    Save03Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03Delete`, params)
    }
    Save03Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03Save`, params)
    }
    Save03Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03Reset`, params)
    }
    Save03FileScanView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03FileScanView`, params)
    }
    Save03Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03Send`, params)
    }
    Save03CopyRequest01(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03CopyRequest01`, params)
    }
    Save03CopyRequest020(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03CopyRequest020`, params)
    }
    Save03People01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03People01Add`, params)
    }
    Save03People01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03People01Delete`, params)
    }
    Save03ProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ProductAdd`, params)
    }
    Save03ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ProductDelete`, params)
    }
    Save03PeopleAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03PeopleAdd`, params)
    }
    Save03PeopleDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03PeopleDelete`, params)
    }
    Save03RepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03RepresentativeAdd`, params)
    }
    Save03RepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03RepresentativeDelete`, params)
    }
    Save04Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Load/${param}`, params)
    }
    Save04MadridLoad(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04MadridLoad`, params)
    }
    Save04LocationList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04LocationList/1`, params)
    }
    Save04Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Delete`, params)
    }
    Save04Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Save`, params)
    }
    Save04Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Reset`, params)
    }
    Save04FileScanView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04FileScanView`, params)
    }
    Save04Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Send`, params)
    }
    Save04CopyProduct(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04CopyProduct`, params)
    }
    Save04People01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04People01Add`, params)
    }
    Save04People01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04People01Delete`, params)
    }
    Save04ProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ProductAdd`, params)
    }
    Save04ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ProductDelete`, params)
    }
    Save04PeopleAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04PeopleAdd`, params)
    }
    Save04PeopleDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04PeopleDelete`, params)
    }
    Save04RepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04RepresentativeAdd`, params)
    }
    Save04RepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04RepresentativeDelete`, params)
    }
    Save04Product01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Product01Add`, params)
    }
    Save04Product01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Product01Delete`, params)
    }
    Save020ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ListPage`, params)
    }
    Save020SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020SaveProcessAdd`, params)
    }
    Save020SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020SaveProcessDelete`, params)
    }
    Save021MardidLoad(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021MardidLoad`, params)
    }
    Save021Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021Save`, params)
    }
    Save021Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021Reset`, params)
    }
    Save021FileScanView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021FileScanView`, params)
    }
    Save021Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021Send`, params)
    }
    Save021People01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021People01Add`, params)
    }
    Save021People01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021People01Delete`, params)
    }
    Save021ProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021ProductAdd`, params)
    }
    Save021ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021ProductDelete`, params)
    }
    Save021ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021ListPage`, params)
    }
    Save021SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021SaveProcessAdd`, params)
    }
    Save021SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021SaveProcessDelete`, params)
    }
    Save03ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ListPage`, params)
    }
    Save03SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03SaveProcessAdd`, params)
    }
    Save03SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03SaveProcessDelete`, params)
    }
    Save04ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ListPage`, params)
    }
    Save04SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04SaveProcessAdd`, params)
    }
    Save04SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04SaveProcessDelete`, params)
    }
    Save05ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05ListPage`, params)
    }
    Save05Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05Reset`, params)
    }
    Save05SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05SaveProcessAdd`, params)
    }
    Save05SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05SaveProcessDelete`, params)
    }
    Save05Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05Save`, params)
    }
    Save05MadridLoad(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05MadridLoad`, params)
    }
    Save05Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05Send`, params)
    }
    Save05ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05ProductDelete`, params)
    }
    Save06Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Load/${param}`, params)
    }
    Save06MadridLoad(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06MadridLoad`, params)
    }
    Save06LocationList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06LocationList/1`, params)
    }
    Save06Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Delete`, params)
    }
    Save06Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Save`, params)
    }
    Save06Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Reset`, params)
    }
    Save06FileScanView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06FileScanView`, params)
    }
    Save06Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Send`, params)
    }
    Save06WorkGroupChange(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06WorkGroupChange`, params)
    }
    Save06CopyRequest01(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06CopyRequest01`, params)
    }
    Save06CopyRequest020(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06CopyRequest020`, params)
    }
    Save06OldPeopleAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06OldPeopleAdd`, params)
    }
    Save06OldPeopleDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06OldPeopleDelete`, params)
    }
    Save06OldRepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06OldRepresentativeAdd`, params)
    }
    Save06OldRepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06OldRepresentativeDelete`, params)
    }
    Save06OwnProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06OwnProductAdd`, params)
    }
    Save06OwnProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06OwnProductDelete`, params)
    }
    Save06PeopleAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06PeopleAdd`, params)
    }
    Save06PeopleDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06PeopleDelete`, params)
    }
    Save06RepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06RepresentativeAdd`, params)
    }
    Save06RepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06RepresentativeDelete`, params)
    }
    Save06AddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06AddressAdd`, params)
    }
    Save06AddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06AddressDelete`, params)
    }
    Save06ProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ProductAdd`, params)
    }
    Save06ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ProductDelete`, params)
    }
    Save06AllowContractPersonAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06AllowContractPersonAdd`, params)
    }
    Save06AllowContractPersonDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06AllowContractPersonDelete`, params)
    }
    Save06JoinerAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06JoinerAdd`, params)
    }
    Save06JoinerDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06JoinerDelete`, params)
    }
    Save06ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ListPage`, params)
    }
    Save06SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06SaveProcessAdd`, params)
    }
    Save06SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06SaveProcessDelete`, params)
    }
    Save01Request01ItemAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Request01ItemAdd`, params)
    }
    Save01Request01ItemDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Request01ItemDelete`, params)
    }
    Save01Save01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Save01Add`, params)
    }
    Save01Save01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Save01Delete`, params)
    }
    Save020ListDraft(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ListDraft`, params)
    }
    Save021ListDraft(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021ListDraft/1`, params)
    }
    Save05ListDraft(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05ListDraft`, params)
    }
    Save04ListDraft(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ListDraft`, params)
    }
    Save03ListDraft(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ListDraft`, params)
    }
    Save06ContactAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ContactAddressAdd`, params)
    }
    Save06ContactAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ContactAddressDelete`, params)
    }
    Save06ContactLocationList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ContactLocationList/1`, params)
    }
    Save01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Add`, params)
    }
    Save01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Delete`, params)
    }
    Save01ListDraft(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ListDraft`, params)
    }
    Save01LoadOld(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01LoadOld`, params)
    }
    Save01Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Madrid`, params)
    }
    Save01DocumentView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01DocumentView`, params)
    }
    Save01PeopleAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01PeopleAdd`, params)
    }
    Save01PeopleDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01PeopleDelete`, params)
    }
    Save01RepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01RepresentativeAdd`, params)
    }
    Save01RepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01RepresentativeDelete`, params)
    }
    Save01ProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ProductAdd`, params)
    }
    Save01ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ProductDelete`, params)
    }
    Save01JoinerAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01JoinerAdd`, params)
    }
    Save01JoinerDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01JoinerDelete`, params)
    }
    Save01SoundMarkAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01SoundMarkAdd`, params)
    }
    Save01SoundMarkDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01SoundMarkDelete`, params)
    }
    Save01CopyOld(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01CopyOld`, params)
    }
    Save01Test(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Test/${param}`, params)
    }
    Save01Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Save`, params)
    }
    Save01Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01Send`, params)
    }
    Save01RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01RequestBack`, params)
    }
    Save01FindDraft(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01FindDraft`, params)
    }
    Save01ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ListNotSent`, params)
    }
    Save01ListCommercialAffairsProvince(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ListCommercialAffairsProvince`, params)
    }
    Save01ListLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ListLocation`, params)
    }
    Save01ContactLocationList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ContactLocationList`, params)
    }
    Save01ContactAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ContactAddressAdd`, params)
    }
    Save01ContactAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ContactAddressDelete`, params)
    }
    Save01ContactAddressTTAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ContactAddressTTAdd`, params)
    }
    Save01ContactAddressTTDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ContactAddressTTDelete`, params)
    }
    Save01ListModalLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ListModalLocation`, params)
    }
    Save020ListModalLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ListModalLocation`, params)
    }
    Save03ListModalLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ListModalLocation`, params)
    }
    Save04ListModalLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ListModalLocation`, params)
    }
    Save06ListModalLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ListModalLocation`, params)
    }
    Save08ListModalLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08ListModalLocation`, params)
    }
    ListItemSubType1Code(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/ListItemSubType1Code`, params)
    }
    ListItemSubType2Code(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/ListItemSubType2Code`, params)
    }
    Save01ModalAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ModalAddressAdd`, params)
    }
    Save01ModalAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save01ModalAddressDelete`, params)
    }
    Save020ListLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ListLocation`, params)
    }
    Save020ContactAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ContactAddressAdd`, params)
    }
    Save020ContactAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ContactAddressDelete`, params)
    }
    Save020Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020Delete`, params)
    }
    Save020DocumentView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020DocumentView`, params)
    }
    Save020Product01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020Product01Add`, params)
    }
    Save020Product01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020Product01Delete`, params)
    }
    Save020ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020ListNotSent`, params)
    }
    Save021ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021ListNotSent`, params)
    }
    Save021Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021Delete`, params)
    }
    Save021DocumentView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021DocumentView`, params)
    }
    Save021Product01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021Product01Add`, params)
    }
    Save021Product01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021Product01Delete`, params)
    }
    Save021Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021Madrid`, params)
    }
    Save03ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ListNotSent`, params)
    }
    Save03Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03Madrid`, params)
    }
    Save03ListLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ListLocation`, params)
    }
    Save03DocumentView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03DocumentView`, params)
    }
    Save03ContactAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ContactAddressAdd`, params)
    }
    Save03ContactAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03ContactAddressDelete`, params)
    }
    Save04Save04ProcessListNotSentLoad(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Save04ProcessListNotSentLoad`, params)
    }
    Save04ListLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ListLocation`, params)
    }
    Save04ContactAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ContactAddressAdd`, params)
    }
    Save04ContactAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ContactAddressDelete`, params)
    }
    Save04Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Madrid`, params)
    }
    Save05Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05Madrid`, params)
    }
    Save06Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Madrid`, params)
    }
    Save06People01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06People01Add`, params)
    }
    Save06People01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06People01Delete`, params)
    }
    Save06Representative01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Representative01Add`, params)
    }
    Save06Representative01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Representative01Delete`, params)
    }
    Save06Product01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Product01Add`, params)
    }
    Save06Product01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06Product01Delete`, params)
    }
    Save07ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07ListNotSent`, params)
    }
    Save07ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07ListPage`, params)
    }
    Save08ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08ListPage`, params)
    }
    Save07Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Madrid`, params)
    }
    Save07Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Delete`, params)
    }
    Save07Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Save`, params)
    }
    Save07Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Reset`, params)
    }
    Save07DocumentView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07DocumentView`, params)
    }
    Save07Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Send`, params)
    }
    Save07Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Load/${param}`, params)
    }
    Save07SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07SaveProcessAdd`, params)
    }
    Save07SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07SaveProcessDelete`, params)
    }
    Save08ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08ListNotSent`, params)
    }
    Save08Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08Madrid`, params)
    }
    Save08Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08Reset`, params)
    }
    Save08Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08Send`, params)
    }
    Save08Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08Load/${param}`, params)
    }
    Save08SaveProcessAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08SaveProcessAdd`, params)
    }
    Save08SaveProcessDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08SaveProcessDelete`, params)
    }
    Save140ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14ListNotSent`, params)
    }
    Save140Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Delete`, params)
    }
    Save140Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Save`, params)
    }
    Save140Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Reset`, params)
    }
    Save140Send(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Send`, params)
    }
    Save140Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Load/${param}`, params)
    }
    Save021Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021Load/${param}`, params)
    }
    Save04ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04ListNotSent`, params)
    }
    Save04Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04Add`, params)
    }
    Save05ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05ListNotSent`, params)
    }
    Save05Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05Load/${param}`, params)
    }
    Save05Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05Add`, params)
    }
    Save05Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05Delete`, params)
    }
    Save06ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ListNotSent`, params)
    }
    Save06ListLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ListLocation`, params)
    }
    Save06TrademarkWarrantyAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06TrademarkWarrantyAdd`, params)
    }
    Save06TrademarkWarrantyDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06TrademarkWarrantyDelete`, params)
    }
    Save06AllowContactAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06AllowContactAdd`, params)
    }
    Save06AllowContactDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06AllowContactDelete`, params)
    }
    Save06ContactAddress06Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ContactAddress06Add`, params)
    }
    Save06ContactAddress06Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06ContactAddress06Delete`, params)
    }
    Save07ListLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07ListLocation`, params)
    }
    Save07People01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07People01Add`, params)
    }
    Save07People01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07People01Delete`, params)
    }
    Save07Product01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Product01Add`, params)
    }
    Save07Product01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Product01Delete`, params)
    }
    Save07ProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07ProductAdd`, params)
    }
    Save07ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07ProductDelete`, params)
    }
    Save07ContactAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07ContactAddressAdd`, params)
    }
    Save07ContactAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07ContactAddressDelete`, params)
    }
    Save07CopyProductt01(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07CopyProductt01`, params)
    }
    Save07Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07Add`, params)
    }
    Save08ListLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08ListLocation`, params)
    }
    Save08Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08Delete`, params)
    }
    Save08Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08Save`, params)
    }
    Save08Copy01(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08Copy01`, params)
    }
    Save08PeopleAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08PeopleAdd`, params)
    }
    Save08PeopleDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08PeopleDelete`, params)
    }
    Save08RepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08RepresentativeAdd`, params)
    }
    Save08RepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08RepresentativeDelete`, params)
    }
    Save08ContactAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08ContactAddressAdd`, params)
    }
    Save08ContactAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08ContactAddressDelete`, params)
    }
    Save08JoinerAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08JoinerAdd`, params)
    }
    Save08JoinerDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08JoinerDelete`, params)
    }
    Save08JoinerRepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08JoinerRepresentativeAdd`, params)
    }
    Save08JoinerRepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08JoinerRepresentativeDelete`, params)
    }
    Save08RequestIndexNew(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08RequestIndexNew`, params)
    }
    Save08RequestIndexSave(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08RequestIndexSave`, params)
    }
    Save08Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08Add`, params)
    }
    Save140People01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save140People01Add`, params)
    }
    Save140People01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save140People01Delete`, params)
    }
    Save140Product01Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save140Product01Add`, params)
    }
    Save140Product01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save140Product01Delete`, params)
    }
    Save14ListPage(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14ListPage`, params)
    }
    Save14Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Reset`, params)
    }
    Save14Add(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Add`, params)
    }
    Save14Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Delete`, params)
    }
    Save14Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14Load/${param}`, params)
    }
    eFormSave01ListNotSent(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01ListNotSent`, params)
    }
    eFormSave01CopyOld(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01CopyOld`, params)
    }
    eFormSave01ListCommercialAffairsProvince(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01ListCommercialAffairsProvince`, params)
    }
    eFormSave01Madrid(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01Madrid`, params)
    }
    eFormSave01ListLocation(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01ListLocation`, params)
    }
    eFormSave01Delete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01Delete`, params)
    }
    eFormSave01DocumentView(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01DocumentView`, params)
    }
    eFormSave01Save(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01Save`, params)
    }
    eFormSave01Reset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01Reset`, params)
    }
    eFormSave01PeopleAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01PeopleAdd`, params)
    }
    eFormSave01PeopleDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01PeopleDelete`, params)
    }
    eFormSave01RepresentativeAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01RepresentativeAdd`, params)
    }
    eFormSave01RepresentativeDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01RepresentativeDelete`, params)
    }
    eFormSave01ContactAddressAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01ContactAddressAdd`, params)
    }
    eFormSave01ContactAddressDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01ContactAddressDelete`, params)
    }
    eFormSave01ProductAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01ProductAdd`, params)
    }
    eFormSave01ProductDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01ProductDelete`, params)
    }
    eFormSave01JoinerAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01JoinerAdd`, params)
    }
    eFormSave01JoinerDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01JoinerDelete`, params)
    }
    eFormSave01SoundMarkAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01SoundMarkAdd`, params)
    }
    eFormSave01SoundMarkDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01SoundMarkDelete`, params)
    }
    eFormSave01Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave01Load/${param}`, params)
    }
    eFormSave140Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave140Load/${param}`, params)
    }
    eFormSave150Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave150Load/${param}`, params)
    }
    eFormSave190Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave190Load/${param}`, params)
    }
    eFormSave200Load(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/eFormSave200Load/${param}`, params)
    }
    Save020RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save020RequestBack`, params)
    }
    Save021RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save021RequestBack`, params)
    }
    Save030RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save03RequestBack`, params)
    }
    Save040RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save04RequestBack`, params)
    }
    Save050RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save05RequestBack`, params)
    }
    Save060RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save06RequestBack`, params)
    }
    Save070RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save07RequestBack`, params)
    }
    Save080RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save08RequestBack`, params)
    }
    Save140RequestBack(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Save14RequestBack`, params)
    }
    SaveSendList(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/SaveSendList`, params)
    }
    SaveSendReset(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/SaveSendReset`, params)
    }
    SaveOtherSend(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/SaveOtherSend`, params)
    }
    SaveSendAdd(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/SaveSendAdd`, params)
    }
    SaveSendDelete(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/SaveSendDelete`, params)
    }
    SaveSendLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/SaveSendLoad/${param}`, params)
    }

}
