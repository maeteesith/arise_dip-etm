import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class DocumentProcessService {
  private apiPath = '/DocumentProcess'
  constructor(private apiService: ApiService) { }
  ClassificationPrint(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationPrint`, params)
  }
  ClassificationSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationSend`, params)
  }
  ClassificationAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationAdd`, params)
  }
  ClassificationDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationDelete`, params)
  }
  ClassificationLoad(id: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationLoad/${id}`, params)
  }
  ClassificationVersionList(id: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationVersionList/${id}`, params)
  }
  ClassificationLoadFromRequestNumber(id: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationLoadFromRequestNumber/${id}`, params)
  }
  ClassificationList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationList`, params)
  }
  ClassificationReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationReset`, params)
  }
  ClassificationListWordFirst(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationListWordFirst`, params)
  }
  ClassificationListWordSoundLast(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationListWordSoundLast`, params)
  }
  ClassificationListWordSoundLastOther(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationListWordSoundLastOther`, params)
  }
  ClassificationViewScanFile(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationViewScanFile`, params)
  }
  ClassificationDownload(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationDownload`, params)
  }
  ClassificationTranslateTool(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationTranslateTool`, params)
  }
  ClassificationSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationSave`, params)
  }
  ClassificationPreview(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationPreview`, params)
  }
  ClassificationSearchRequest(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationSearchRequest`, params)
  }
  ClassificationSearchSameWord(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationSearchSameWord`, params)
  }
  DocumentItemCheckerList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentItemCheckerList`, params)
  }
  DocumentItemDocumentPeopleList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentItemDocumentPeopleList`, params)
  }
  DocumentItemReceive(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentItemReceive`, params)
  }
  DocumentItemReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentItemReset`, params)
  }
  DocumentItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentItemList`, params)
  }
  DocumentItemDocumentAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentItemDocumentAdd`, params)
  }
  DocumentItemDocumentDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentItemDocumentDelete`, params)
  }
  DocumentItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentItemLoad/${param}`, params)
  }
  ClassificationListImageType(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationListImageType`, params)
  }
  ClassificationListSoundType(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationListSoundType`, params)
  }
  DocumentRole02ItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02ItemList`, params)
  }
  DocumentRole02ItemReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02ItemReset`, params)
  }
  DocumentRole02ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02ItemAdd`, params)
  }
  DocumentRole02ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02ItemDelete`, params)
  }
  DocumentRole02ItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02ItemLoad/${param}`, params)
  }
  DocumentRole02CheckList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckList`, params)
  }
  DocumentRole02ItemListAutoSplit(param: any = null, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02ItemListAutoSplit`, params)
  }
  DocumentRole03ItemListAutoSplit(param: any = null, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ItemListAutoSplit`, params)
  }
  DocumentRole04ItemAutoSplit(param: any = null, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemAutoSplit`, params)
  }
  DocumentRole05ItemAutoSplit(param: any = null, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ItemAutoSplit`, params)
  }
  DocumentRole02CheckReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckReset`, params)
  }
  DocumentRole02CheckAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckAdd`, params)
  }
  DocumentRole02CheckDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckDelete`, params)
  }
  DocumentRole02CheckLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckLoad/${param}`, params)
  }
  Save010Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save010Load/${param}`, params)
  }
  DocumentRole02CheckSendChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckSendChange`, params)
  }
  DocumentRole02CheckIntructionRuleSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckIntructionRuleSave`, params)
  }
  DocumentRole02CheckSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckSend`, params)
  }
  List(view_name: any, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/List/${view_name}`, params)
  }
  DocumentRole02CheckCheckingInstructionListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckCheckingInstructionListAdd`, params)
  }
  DocumentRole02CheckCheckingInstructionListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckCheckingInstructionListDelete`, params)
  }
  DocumentRole02CheckAddressListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckAddressListAdd`, params)
  }
  DocumentRole02CheckAddressListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckAddressListDelete`, params)
  }
  DocumentRole02CheckCheckingInstructionDocumentListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckCheckingInstructionDocumentListAdd`, params)
  }
  DocumentRole02CheckCheckingInstructionDocumentListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckCheckingInstructionDocumentListDelete`, params)
  }
  DocumentRole02CheckInstructionRuleSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02CheckInstructionRuleSend`, params)
  }
  DocumentRole02PrintDocumentList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintDocumentList`, params)
  }
  DocumentRole02PrintDocumentSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintDocumentSend`, params)
  }
  DocumentRole02PrintDocumentItemSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintDocumentItemSend`, params)
  }
  DocumentRole02PrintDocumentReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintDocumentReset`, params)
  }
  DocumentRole02PrintDocumentAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintDocumentAdd`, params)
  }
  DocumentRole02PrintDocumentDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintDocumentDelete`, params)
  }
  DocumentRole02PrintDocumentLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintDocumentLoad/${param}`, params)
  }
  DocumentRole02PrintDocumentItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintDocumentItemLoad/${param}`, params)
  }
  DocumentRole02PrintCoverList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintCoverList`, params)
  }
  DocumentRole02PrintCoverSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintCoverSend`, params)
  }
  DocumentRole02PrintPostNumberSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintPostNumberSend`, params)
  }
  DocumentRole02PrintCoverReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintCoverReset`, params)
  }
  DocumentRole02PrintCoverAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintCoverAdd`, params)
  }
  DocumentRole02PrintCoverDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintCoverDelete`, params)
  }
  DocumentRole02PrintCoverLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintCoverLoad/${param}`, params)
  }
  DocumentRole02PrintListList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintListList`, params)
  }
  DocumentRole02PrintListReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintListReset`, params)
  }
  DocumentRole02PrintListSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintListSend`, params)
  }
  DocumentRole02PrintListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintListAdd`, params)
  }
  DocumentRole02PrintListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintListDelete`, params)
  }
  DocumentRole02PrintListLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole02PrintListLoad/${param}`, params)
  }
  DocumentRole03ChangeCheckingInstructionListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeCheckingInstructionListAdd`, params)
  }
  DocumentRole03ChangeCheckingInstructionListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeCheckingInstructionListDelete`, params)
  }
  DocumentRole03ChangeAddressListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeAddressListAdd`, params)
  }
  DocumentRole03ChangeAddressListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeAddressListDelete`, params)
  }
  DocumentRole03ChangeCheckingInstructionDocumentListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeCheckingInstructionDocumentListAdd`, params)
  }
  DocumentRole03ChangeCheckingInstructionDocumentListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeCheckingInstructionDocumentListDelete`, params)
  }
  DocumentRole03ChangeLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeLoad/${param}`, params)
  }
  DocumentRole04DocumentInstructionCheckingInstructionListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionCheckingInstructionListAdd`, params)
  }
  DocumentRole04DocumentInstructionCheckingInstructionListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionCheckingInstructionListDelete`, params)
  }
  DocumentRole04DocumentInstructionAddressListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionAddressListAdd`, params)
  }
  DocumentRole04DocumentInstructionAddressListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionAddressListDelete`, params)
  }
  DocumentRole04DocumentInstructionCheckingInstructionDocumentListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionCheckingInstructionDocumentListAdd`, params)
  }
  DocumentRole04DocumentInstructionCheckingInstructionDocumentListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionCheckingInstructionDocumentListDelete`, params)
  }
  DocumentRole04DocumentInstructionLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionLoad/${param}`, params)
  }
  DocumentRole04AppealCheckingInstructionListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04AppealCheckingInstructionListAdd`, params)
  }
  DocumentRole04AppealCheckingInstructionListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04AppealCheckingInstructionListDelete`, params)
  }
  DocumentRole04AppealAddressListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04AppealAddressListAdd`, params)
  }
  DocumentRole04AppealAddressListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04AppealAddressListDelete`, params)
  }
  DocumentRole04AppealCheckingInstructionDocumentListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04AppealCheckingInstructionDocumentListAdd`, params)
  }
  DocumentRole04AppealCheckingInstructionDocumentListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04AppealCheckingInstructionDocumentListDelete`, params)
  }
  DocumentRole04AppealLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04AppealLoad/${param}`, params)
  }
  DocumentRole04ReleaseRequestCheckingInstructionListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestCheckingInstructionListAdd`, params)
  }
  DocumentRole04ReleaseRequestCheckingInstructionListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestCheckingInstructionListDelete`, params)
  }
  DocumentRole04ReleaseRequestAddressListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestAddressListAdd`, params)
  }
  DocumentRole04ReleaseRequestAddressListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestAddressListDelete`, params)
  }
  DocumentRole04ReleaseRequestCheckingInstructionDocumentListAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestCheckingInstructionDocumentListAdd`, params)
  }
  DocumentRole04ReleaseRequestCheckingInstructionDocumentListDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestCheckingInstructionDocumentListDelete`, params)
  }
  DocumentRole04ReleaseRequestLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestLoad/${param}`, params)
  }
  DocumentRole04ReleaseRequestItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestItemLoad/${param}`, params)
  }
  SearchSimilarList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/SearchSimilarList`, params)
  }
  DocumentRole05ItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ItemList`, params)
  }
  DocumentRole05ItemReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ItemReset`, params)
  }
  DocumentRole05ItemSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ItemSend`, params)
  }
  DocumentRole05ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ItemAdd`, params)
  }
  DocumentRole05ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ItemDelete`, params)
  }
  DocumentRole05ItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ItemLoad/${param}`, params)
  }
  DashboardPublicRole01List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole01List`, params)
  }
  DashboardPublicRole01Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole01Reset`, params)
  }
  DashboardPublicRole01Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole01Send`, params)
  }
  DashboardPublicRole01Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole01Add`, params)
  }
  DashboardPublicRole01Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole01Delete`, params)
  }
  DashboardPublicRole01Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole01Load/${param}`, params)
  }
  DocumentRole05ChangeList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ChangeList`, params)
  }
  DocumentRole05ChangeReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ChangeReset`, params)
  }
  DocumentRole05ChangeSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ChangeSend`, params)
  }
  DocumentRole05ChangeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ChangeAdd`, params)
  }
  DocumentRole05ChangeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ChangeDelete`, params)
  }
  DocumentRole05ChangeLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ChangeLoad/${param}`, params)
  }
  DocumentRole05ConsiderDocumentList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ConsiderDocumentList`, params)
  }
  DocumentRole05ConsiderDocumentReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ConsiderDocumentReset`, params)
  }
  DocumentRole05ConsiderDocumentSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ConsiderDocumentSend`, params)
  }
  DocumentRole05ConsiderDocumentAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ConsiderDocumentAdd`, params)
  }
  DocumentRole05ConsiderDocumentDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ConsiderDocumentDelete`, params)
  }
  DocumentRole05ConsiderDocumentLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ConsiderDocumentLoad/${param}`, params)
  }
  DashboardPublicRole04List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole04List`, params)
  }
  DashboardPublicRole04Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole04Reset`, params)
  }
  DashboardPublicRole04Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole04Send`, params)
  }
  DashboardPublicRole04Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole04Add`, params)
  }
  DashboardPublicRole04Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole04Delete`, params)
  }
  DashboardPublicRole04Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardPublicRole04Load/${param}`, params)
  }
  DocumentRole04ItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemList`, params)
  }
  DocumentRole04ItemReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemReset`, params)
  }
  DocumentRole04ItemSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemSend`, params)
  }
  DocumentRole04ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemAdd`, params)
  }
  DocumentRole04ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemDelete`, params)
  }
  DocumentRole04ItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemLoad/${param}`, params)
  }
  DocumentRole04ChangeList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ChangeList`, params)
  }
  DocumentRole04ChangeReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ChangeReset`, params)
  }
  DocumentRole04ChangeSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ChangeSend`, params)
  }
  DocumentRole04ChangeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ChangeAdd`, params)
  }
  DocumentRole04ChangeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ChangeDelete`, params)
  }
  DocumentRole04ChangeLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ChangeLoad/${param}`, params)
  }
  DocumentRole04ReleaseRequestList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestList`, params)
  }
  DocumentRole04ReleaseRequestReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestReset`, params)
  }
  DocumentRole04ReleaseRequestSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestSend`, params)
  }
  DocumentRole05ReleaseRequestSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ReleaseRequestSend`, params)
  }
  DocumentRole05ReleaseRequestSendChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ReleaseRequestSendChange`, params)
  }
  DocumentRole04ReleaseRequestDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestDelete`, params)
  }
  DocumentRole04DocumentInstructionList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionList`, params)
  }
  DocumentRole04DocumentInstructionReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionReset`, params)
  }
  DocumentRole04DocumentInstructionSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionSend`, params)
  }
  DocumentRole04DocumentInstructionDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04DocumentInstructionDelete`, params)
  }
  DocumentProcessChangeList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessChangeList`, params)
  }
  DocumentProcessChangeReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessChangeReset`, params)
  }
  DocumentProcessChangeClassificationAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessChangeClassificationAdd`, params)
  }
  DocumentProcessChangeClassificationDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessChangeClassificationDelete`, params)
  }
  DocumentProcessChangeLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessChangeLoad/${param}`, params)
  }
  DocumentProcessCollectList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessCollectList`, params)
  }
  DocumentProcessCollectReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessCollectReset`, params)
  }
  CollectRequestAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CollectRequestAdd`, params)
  }
  CollectRequestDelete(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CollectRequestDelete/${param}`, params)
  }
  CollectHistoryAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CollectHistoryAdd`, params)
  }
  DocumentProcessCollectHistoryDelete(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessCollectHistoryDelete/${param}`, params)
  }
  ClassificationRequestList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationRequestList`, params)
  }
  ClassificationHistoryList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationHistoryList`, params)
  }
  DocumentProcessCollectLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessCollectLoad/${param}`, params)
  }
  DocumentProcessCollectHistoryImageSend(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentProcessCollectHistoryImageSend`, params)
  }
  DashboardDocumentRole01List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardDocumentRole01List`, params)
  }
  DashboardDocumentRole01Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardDocumentRole01Reset`, params)
  }
  DashboardDocumentRole01Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardDocumentRole01Send`, params)
  }
  DashboardDocumentRole01DashboardPublicRole01Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardDocumentRole01DashboardPublicRole01Add`, params)
  }
  DashboardDocumentRole01DashboardPublicRole01Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardDocumentRole01DashboardPublicRole01Delete`, params)
  }
  DashboardDocumentRole01Load(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DashboardDocumentRole01Load`, params)
  }
  DocumentRole04ItemDocumentRole02ItemAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemDocumentRole02ItemAdd`, params)
  }
  DocumentRole04ItemDocumentRole02ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ItemDocumentRole02ItemDelete`, params)
  }
  DocumentRole04CheckLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CheckLoad/${param}`, params)
  }
  DocumentRole04CheckSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CheckSend`, params)
  }
  DocumentRole04CheckInstructionRuleFileAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CheckInstructionRuleFileAdd`, params)
  }
  DocumentRole04CheckInstructionRuleFileDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CheckInstructionRuleFileDelete`, params)
  }
  DocumentRole05CheckSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05CheckSend`, params)
  }
  DocumentRole05CheckSendChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05CheckSendChange`, params)
  }
  DocumentRole05ReleaseRequestLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseRequestLoad/${param}`, params)
  }
  DocumentRole04CreateLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CreateLoad/${param}`, params)
  }
  DocumentRole04CreateAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CreateAdd`, params)
  }
  DocumentRole04CreateFileAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CreateFileAdd`, params)
  }
  DocumentRole04CreateFileDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CreateFileDelete`, params)
  }
  DocumentRole04CreateSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04CreateSend`, params)
  }
  DocumentRole05CreateSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05CreateSend`, params)
  }
  DocumentRole05CreateLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05CreateLoad/${param}`, params)
  }
  DocumentRole04ReleaseLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseLoad/${param}`, params)
  }
  DocumentRole04Release20Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04Release20Load/${param}`, params)
  }
  DocumentRole04ReleaseSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04ReleaseSend`, params)
  }
  DocumentRole05ReleaseSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05ReleaseSend`, params)
  }
  DocumentRole04Release20Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole04Release20Send`, params)
  }
  DocumentRole05Release20Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05Release20Send`, params)
  }
  DocumentRole03ItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ItemLoad/${param}`, params)
  }
  SaveList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/SaveList`, params)
  }
  //PublicRole01CheckSend(param: any, params: any = null): Observable<any> {
  //  return this.apiService.post(`${this.apiPath}/PublicRole01CheckSend/${param}`, params)
  //}
  CertificationFileAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CertificationFileAdd`, params)
  }
  DocumentRole03ChangeSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeSave`, params)
  }
  DocumentRole03ChangeSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeSend`, params)
  }
  DocumentRole03ChangeSendAll(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeSendAll`, params)
  }
  DocumentRole03ChangeVersionList(save_id: any, request_type_code: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeVersionList/${save_id}/${request_type_code}`, params)
  }
  DocumentRole03ChangeVersionLoad(save_id: any, id: any, request_type_code: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole03ChangeVersionLoad/${save_id}/${id}/${request_type_code}`, params)
  }
  DocumentRole05OtherLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentRole05OtherLoad/${param}`, params)
  }
DocumentRole02ActionPostLoad(param: any, params: any = null): Observable < any > {
  return this.apiService.post(`${this.apiPath}/DocumentRole02ActionPostLoad/${param}`, params)
}


}
