import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'

@Injectable({
  providedIn: 'root'
})
export class RequestItemSubTypeService {
  private apiPath = '/RequestItemSubType1'

  constructor(private apiService: ApiService) { }

  ProductCatagory01ItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ProductCatagory01ItemList`, params)
  }

}
