import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable, of } from 'rxjs'
import { catchError, map } from 'rxjs/operators'
import { BASE_API, BASE_PATH_API } from '../config/config'
import { GlobalService } from '../global.service'
import { Auth } from '../auth'
import { CONSTANTS, LOGIN_URL, displayDate } from '../helpers'

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private global: GlobalService,
    private auth: Auth,
    private http: HttpClient
  ) { }

  get(path: string, httpParams: any): Observable<any> {
    const url = `${BASE_API}${BASE_PATH_API}${path}`
    console.log(`%cparams[GET] ${url}`, 'color: cyan', httpParams)
    const httpOptions = {
      headers: { 'Authorization': this.auth.getAuthorization() },
      params: httpParams
    }
    return this.http.get<any>(url, httpOptions).pipe(
      map(res => { return this.validateResponse(res, url, 'GET') }),
      catchError(this.handleError<any>(url))
    )
  }

  post(path: string, data: any): Observable<any> {
    const url = `${BASE_API}${BASE_PATH_API}${path}`
    console.log(`%cparams[POST] ${url}`, 'color: cyan', data)
    const httpOptions = {
      headers: { 'Authorization': this.auth.getAuthorization() },
    }
    return this.http.post<any>(url, data, httpOptions).pipe(
      map(res => { return this.validateResponse(res, url, 'POST') }),
      catchError(this.handleError<any>(url))
    )
  }

  put(path: string, data: any): Observable<any> {
    const url = `${BASE_API}${BASE_PATH_API}${path}`
    console.log(`%cparams[PUT] ${url}`, 'color: cyan', data)
    const httpOptions = {
      headers: { 'Authorization': this.auth.getAuthorization() },
    }
    return this.http.put<any>(url, data, httpOptions).pipe(
      map(res => { return this.validateResponse(res, url, 'PUT') }),
      catchError(this.handleError<any>(url))
    )
  }

  delete(path: string, httpParams: any): Observable<any> {
    const url = `${BASE_API}${BASE_PATH_API}${path}`
    console.log(`%cparams[DELETE] ${url}`, 'color: cyan', httpParams)
    const httpOptions = {
      headers: { 'Authorization': this.auth.getAuthorization() },
      params: httpParams
    }
    return this.http.delete<any>(url, httpOptions).pipe(
      map(res => { return this.validateResponse(res, url, 'DELETE') }),
      catchError(this.handleError<any>(url))
    )
  }

  pdf(path: string, data: any): Observable<any> {
    const url = path
    console.log(`%cparams[POST] ${url}`, 'color: lime', data)
    const httpOptions = {
      headers: { 'Authorization': this.auth.getAuthorization() },
    }
    return this.http.post<any>(url, data, httpOptions).pipe(
      catchError(this.handleError<any>(url))
    )
  }

  // Validate response
  validateResponse(res: any, url: string, type: string): any {
    console.log(`%ccall[${type}] ${url}`, 'color: orange', res)
    if (res.is_error) {
      // Open toast error
      let toast = CONSTANTS.TOAST.ERROR
      toast.message = res.error_message
      this.global.setToast(toast)
      return false
    } else {
      return this.helpData(res.data)
    }
  }

  helpData(object: any): any {
    if (object)
      Object.keys(object).forEach((item: any) => {
        if (object[item])
          if (typeof object[item] == "string") {
            if (item.endsWith("date"))
              object[item + "_text"] = displayDate(object[item])
            else if (item.endsWith("_code"))
              if (object[item + "Navigation"] && object[item + "Navigation"].name)
                object[item.replace("_code", "_name")] = object[item + "Navigation"].name
          } else if (typeof object[item] == "number" || typeof object[item] == "boolean") {
          } else if (typeof object[item] == "object") {
            if (item == "product_01_list") {
              object["request_item_sub_type_1_code_text"] = object[item].map((r: any) => { return r.request_item_sub_type_1_code })
              //console.log(object["request_item_sub_type_1_code_text"])
              object["request_item_sub_type_1_code_text"] = object["request_item_sub_type_1_code_text"].filter(function (value, index, self) {
                return self.indexOf(value) === index;
              })
              console.log(object["request_item_sub_type_1_code_text"])
              object["request_item_sub_type_1_code_text"] = object["request_item_sub_type_1_code_text"].join(", ")
              console.log(object["request_item_sub_type_1_code_text"])
              //console.log(object["request_item_sub_type_1_code_text"])
            } else {
              this.helpData(object[item])
            }
          } else {
            console.log(typeof object[item], item)
          }
      })

    return object;
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // Open toast error
      let toast = CONSTANTS.TOAST.ERROR
      toast.message = error.statusText
      this.global.setToast(toast)

      // Close loading
      this.global.setLoading(false);

      // TODO: send the error to remote logging infrastructure
      console.error(error) // log to console instead
      if (error.status === 401) {
        // this.auth.logout()
        location.href = LOGIN_URL
      }

      // TODO: better job of transforming error for user consumption
      // this.log(`${operation} failed: ${error.message}`)

      // Let the app keep running by returning an empty result.
      return of(result as T)
    };
  }

}
