import { Injectable } from '@angular/core'
import { Observable, forkJoin } from 'rxjs'
import { ApiService } from './apiService'

@Injectable({
  providedIn: 'root'
})
export class AppealService {
  private apiPath = '/AppealProcess'
  constructor(
    private apiService: ApiService
    ) { }

  SaveRequestCaseSummarySend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/SaveRequestCaseSummarySend`, params)
  }

  SaveDraftRequestCaseSummary(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/SaveDraftRequestCaseSummary`, params)
  }

  GetRequestCaseSummaryList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/GetRequestCaseSummaryList`, params)
  }

  GetDataRequestCaseSummary(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/GetRequestCaseSummary/${param}`, params)
  }

}
