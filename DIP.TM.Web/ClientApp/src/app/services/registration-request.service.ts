import { Injectable } from '@angular/core'

import { HttpClient } from '@angular/common/http'

import { catchError, map } from 'rxjs/operators'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
    providedIn: 'root'
})

export class RegistrationRequestService {
    private apiPath = "/GDX"
    private clientPath = "http://192.168.1.113:3000";

    constructor(private apiService: ApiService, private http: HttpClient) {

    }
    AutoFillFindID(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Load`, params)
    }
    DeleteIDCardReader(params: any, error: Function): Observable<any> {
        let url = `${this.clientPath}/delete`
        return this.http.post<any>(url, params).pipe(
            map(res => { return this.apiService.validateResponse(res, url, 'POST') }),
            catchError(error())
        )
    }

    GetIDCard(params: any): Observable<any> {
        let url = `${this.clientPath}`
        return this.http.post<any>(url, params).pipe(
            map(res => { return this.apiService.validateResponse(res, url, 'POST') })
        )
    }
}
