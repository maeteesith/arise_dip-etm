import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class UploadService {
  private apiPath = '/upload'

  constructor(private apiService: ApiService) { }

  upload(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}`, params)
  }
  guestUpload(params: any): Observable<any> {
    return this.apiService.post('/guestupload', params)
  }

}
