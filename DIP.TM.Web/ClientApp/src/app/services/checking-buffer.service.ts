import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class CheckingService {
  private apiPath = '/Checking'
  constructor(private apiService: ApiService) { }
  CheckingItemCheckingItemLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckingItemLoad`, params)
  }
  CheckingItemCheckerList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckerList`, params)
  }
  CheckingItemReceive(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemReceive`, params)
  }
  CheckingItemReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemReset`, params)
  }
  CheckingItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemList`, params)
  }
  CheckingItemCheckingAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckingAdd`, params)
  }
  CheckingItemCheckingDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckingDelete`, params)
  }
  CheckingItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemLoad/${param}`, params)
  }
  CheckingSimilarList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarList`, params)
  }

  CheckingSimilarReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarReset`, params)
  }

  CheckingSimilarAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarAdd`, params)
  }

  CheckingSimilarDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarDelete`, params)
  }

CheckingSimilarLoad(param: any, params: any = null): Observable < any > {
  return this.apiService.post(`${this.apiPath}/CheckingSimilarLoad/${param}`, params)
}


}
