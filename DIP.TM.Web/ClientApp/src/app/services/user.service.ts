import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class UserService {
  private apiPath = '/User'

  constructor(private apiService: ApiService) { }

  Login(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}`, params)
  }

  GetUserInfo(params: any): Observable<any> {
    return this.apiService.get(`${this.apiPath}/GetUserInfo`, params)
  }

}
