import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class OneStopServiceProcessService {
  private apiPath = '/OneStopServiceProcess'
  constructor(private apiService: ApiService) { }
  RecordRegistrationNumberCheckRegisterPayment(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberCheckRegisterPayment`, params)
  }
  List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/List`, params)
  }
  RecordRegistrationNumberSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberSave`, params)
  }
  RecordRegistrationNumberPrintDocument(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberPrintDocument`, params)
  }
  RecordRegistrationNumberSendRegistrarConsider(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberSendRegistrarConsider`, params)
  }
  RecordRegistrationNumberAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberAdd`, params)
  }
  RecordRegistrationNumberDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberDelete`, params)
  }
  RecordRegistrationNumberLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberLoad/${param}`, params)
  }
  Floor3RegistrarList(params: any = {}): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Floor3RegistrarList`, params)
  }
  RecordRegistrationNumberAllowList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberAllowList`, params)
  }
  RecordRegistrationNumberAllowReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberAllowReset`, params)
  }
  RecordRegistrationNumberAllowDone(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberAllowDone`, params)
  }
  RecordRegistrationNumberAllowCancel(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberAllowCancel`, params)
  }
  RecordRegistrationNumberAllowAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberAllowAdd`, params)
  }
  RecordRegistrationNumberAllowDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberAllowDelete`, params)
  }
  RecordRegistrationNumberAllowLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RecordRegistrationNumberAllowLoad/${param}`, params)
  }
}
