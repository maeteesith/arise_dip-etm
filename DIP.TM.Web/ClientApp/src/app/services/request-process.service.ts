import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class RequestProcessService {
  private apiPath = '/RequestProcess'
  constructor(private apiService: ApiService) { }
  Request01Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01Add`, params)
  }
  Request01Split(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01Split`, params)
  }
  Request01Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01Save`, params)
  }
  Request01ListItemType(params: any): Observable<any> {
    return this.apiService.get(`${this.apiPath}/Request01ListItemType`, params)
  }
  Request01ListEvidenceType(params: any): Observable<any> {
    return this.apiService.get(`${this.apiPath}/Request01ListEvidenceType`, params)
  }
  Request01ListFacilitationActStatus(params: any): Observable<any> {
    return this.apiService.get(`${this.apiPath}/Request01ListFacilitationActStatus`, params)
  } 
  Request01PriceMaster(params: any): Observable<any> {
    return this.apiService.get(`${this.apiPath}/Request01PriceMaster`, params)
  }
  Request01Load(id: number, params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01Load/${id}`, params)
  }
  Request01List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01List`, params)
  }
  Request01ItemDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Request01ItemDelete`, params)
  }
  MakerAutoFill(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/MakerAutoFill`, params)
  }
  RequestOtherSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherSave`, params)
  }
  Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Reset`, params)
  }
  RequestOtherRequestAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestAdd`, params)
  }
  RequestOtherRequestTypeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestTypeAdd`, params)
  }
  RequestOtherRequestDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestDelete`, params)
  }
  RequestOtherRequestTypeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestTypeDelete`, params)
  }
  RequestOtherRequestOtherProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherProcessAdd`, params)
  }
  RequestOtherRequestOtherProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherProcessDelete`, params)
  }
  RequestOtherRequestOtherRequestTypeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherRequestTypeAdd`, params)
  }
  RequestOtherRequestOtherRequestTypeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherRequestOtherRequestTypeDelete`, params)
  }
  RequestOtherMakerAutoFill(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestOtherMakerAutoFill`, params)
  }

}
