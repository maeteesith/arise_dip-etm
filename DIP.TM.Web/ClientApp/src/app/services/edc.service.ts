import { Injectable } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
import { catchError, map } from 'rxjs/operators'
@Injectable({
    providedIn: 'root'
})
export class EdcService {
    protected basePath = "http://127.0.0.1:8080";
    constructor(private apiService: ApiService, protected httpClient: HttpClient, ) {

    }
    payAndWait(params: any, channel_type: any): Observable<any> {

        let url = `${this.basePath}`
        url += "?method=start";
        url += "&channel_type=" + channel_type;
        url += "&amt=" + params;
        return this.httpClient.get<any>(url, params).pipe(
            map(res => { return res })
        )

    }

    checkPaidResult(params: any): Observable<any> {

        let url = `${this.basePath}`
        url += "?method=recieve";
        return this.httpClient.post<any>(url, params).pipe(
            map(res => { return res })
        )

    }

}
