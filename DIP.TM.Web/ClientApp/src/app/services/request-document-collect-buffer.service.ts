import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
import { HttpClient } from '@angular/common/http'
import { catchError, map } from 'rxjs/operators'
import 'rxjs/add/operator/catch';

@Injectable({
  providedIn: 'root'
})
export class RequestDocumentCollectService {
  private apiPath = '/RequestDocumentCollect'
  private clientPath = "http://127.0.0.1:3001";

  constructor(private apiService: ApiService, private http: HttpClient) { }

  //Test(params: any = null): Observable<any> {
  //  let url = `http://localhost:5000/api/EFormProcess/eForm030Save`

  //  params = {
  //    remark: "Test",
  //  }

  //  return this.http.post<any>(url, params).pipe(
  //    map(res => { return this.apiService.validateResponse(res, url, 'POST') })
  //  )
  //}

  List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/List`, params)
  }

  Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Reset`, params)
  }

  DocumentAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentAdd`, params)
  }

  DocumentDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/DocumentDelete`, params)
  }

  Load(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Load`, params)
  }

  RequestDocumentCollectFileScan(params: any = null): Observable<any> {
    let url = `${this.clientPath}/scan=` + params
    return this.http.post<any>(url, params).pipe(
      map(res => { return this.apiService.validateResponse(res, url, 'POST') })
    )
  }

  RequestDocumentCollectItemSave(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestDocumentCollectItemSave`, params)
  }

  RequestDocumentCollectItemRemove(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestDocumentCollectItemRemove`, params)
  }

  FileLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/FileLoad/${param}`, params)
  }
}
