import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class CheckingProcessService {
  private apiPath = '/CheckingProcess'
  constructor(private apiService: ApiService) { }
  ClassificationListWordFirst(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationListWordFirst`, params)
  }
  List(params: any, service_name: string): Observable<any> {
    return this.apiService.post(`${this.apiPath}/` + service_name + `/List`, params)
  }
  ClassificationListSoundLastOther(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ClassificationListSoundLastOther`, params)
  }
  CheckingCheckingPrint(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingCheckingPrint`, params)
  }
  CheckingReportPrint(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingReportPrint`, params)
  }
  CheckingBack(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingBack`, params)
  }

  CheckingItemAutoSplit(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemAutoSplit`, params)
  }

  CheckingAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingAdd`, params)
  }
  CheckingDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingDelete`, params)
  }
  CheckingLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingLoad/${param}`, params)
  }
  CheckingSimilarGetRequestItemSubTypeGroup(param: any, params: any = null) {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarGetRequestItemSubTypeGroup/${param}`, params)
  }
  CheckingCancel(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingCancel`, params)
  }
  CheckingSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSave`, params)
  }
  CheckingItemCheckingItemLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckingItemLoad`, params)
  }
  CheckingItemCheckerList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckerList`, params)
  }
  CheckingItemReceive(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemReceive`, params)
  }
  CheckingItemReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemReset`, params)
  }
  CheckingItemList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemList`, params)
  }
  CheckingItemCheckingAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckingAdd`, params)
  }
  CheckingItemCheckingDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckingDelete`, params)
  }
  CheckingItemLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemLoad/${param}`, params)
  }
  CheckingItemCheckerReceiveList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingItemCheckerReceiveList`, params)
  }
  CheckingSimilarList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarList`, params)
  }
  CheckingSimilarReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarReset`, params)
  }
  CheckingSimilarAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarAdd`, params)
  }
  CheckingSimilarDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarDelete`, params)
  }
  CheckingSimilarLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarLoad/${param}`, params)
  }
  CheckingSimilarSave010List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarSave010List`, params)
  }
  CheckingSimilarStatisticList(params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarStatisticList`, params)
  }
  CheckingTagSimilarMethod(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingTagSimilarMethod`, params)
  }
  CheckingSimilarSearchAsk(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarSearchAsk`, params)
  }
  CheckingSimilarSave010PDFView(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarSave010PDFView`, params)
  }
  CheckingSimilarResultLink(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultLink`, params)
  }
  CheckingSimilarWordSoundTranslateSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordSoundTranslateSave`, params)
  }
  CheckingSimilarWordSoundTranslateBackSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordSoundTranslateBackSend`, params)
  }
  CheckingSimilarSearchTransactionList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarSearchTransactionList`, params)
  }
  CheckingSimilarSearchImageList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarSearchImageList`, params)
  }
  CheckingSimilarSearchSoundList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarSearchSoundList`, params)
  }
  CheckingSimilarWordMarkAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordMarkAdd`, params)
  }
  CheckingSimilarWordMarkDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordMarkDelete`, params)
  }
  CheckingSimilarImageMarkAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarImageMarkAdd`, params)
  }
  CheckingSimilarImageMarkDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarImageMarkDelete`, params)
  }
  CheckingSimilarSoundMarkAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarSoundMarkAdd`, params)
  }
  CheckingSimilarSoundMarkDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarSoundMarkDelete`, params)
  }
  CheckingSimilarWordSoundTranslateAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordSoundTranslateAdd`, params)
  }
  CheckingSimilarWordSoundTranslateDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordSoundTranslateDelete`, params)
  }
  CheckingSimilarProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarProductAdd`, params)
  }
  CheckingSimilarProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarProductDelete`, params)
  }
  CheckingSimilarPharmacyAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarPharmacyAdd`, params)
  }
  CheckingSimilarPharmacyDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarPharmacyDelete`, params)
  }
  CheckingSimilarOrganizeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarOrganizeAdd`, params)
  }
  CheckingSimilarOrganizeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarOrganizeDelete`, params)
  }
  CheckingSimilarWordSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordSave`, params)
  }
  CheckingSimilarForTagList(params: any, save_id: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarForTagList/${save_id}`, params)
  }


  CheckingSimilarWordPrint(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordPrint`, params)
  }
  CheckingSimilarWordBack(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordBack`, params)
  }
  CheckingSimilarWordTrademarkAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordTrademarkAdd`, params)
  }
  CheckingSimilarWordTrademarkDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordTrademarkDelete`, params)
  }
  CheckingSimilarWordLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarWordLoad/${param}`, params)
  }
  CheckingSimilarImageLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarImageLoad/${param}`, params)
  }
  CheckingSimilarTagAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarTagAdd`, params)
  }
  CheckingSimilarResultOwnerSame(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultOwnerSame`, params)
  }
  CheckingSimilarTagRemove(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarTagRemove`, params)
  }
  CheckingSimilarResultDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultDelete`, params)
  }
  CheckingSimilarResultSend(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultSend`, params)
  }
  ConsideringSimilarKor10Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarKor10Save`, params)
  }
  CheckingSimilarResultBack(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultBack`, params)
  }
  CheckingSimilarResultDuplicateList(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultDuplicateList/${param}`, params)
  }
  CheckingSimilarResultDuplicateAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultDuplicateAdd`, params)
  }
  CheckingSimilarResultTrademarkAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultTrademarkAdd`, params)
  }
  CheckingSimilarResultTrademarkDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultTrademarkDelete`, params)
  }
  CheckingSimilarResultLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarResultLoad/${param}`, params)
  }
  CheckingSimilarTagList(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarTagList/${param}`, params)
  }
  //ConsideringSimilarList(params: any): Observable<any> {
  //    return this.apiService.post(`${this.apiPath}/ConsideringSimilarList`, params)
  //}
  ConsideringSimilarListPage(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarListPage`, params)
  }
  ConsideringSimilarDashboard(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDashboard`, params)
  }

  ConsideringSimilarReset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarReset`, params)
  }
  ConsideringSimilarAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarAdd`, params)
  }
  ConsideringSimilarDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDelete`, params)
  }
  ConsideringSimilarLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarLoad/${param}`, params)
  }
  ConsideringSimilarCheckingSimilar(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarCheckingSimilar`, params)
  }
  ConsideringSimilarConsideringDocument(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarConsideringDocument`, params)
  }
  ConsideringSimilarInstruction(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstruction`, params)
  }
  ConsideringSimilarSearchAsk(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarSearchAsk`, params)
  }
  ConsideringSimilarSave010PDFView(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarSave010PDFView`, params)
  }
  ConsideringSimilarResultLink(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarResultLink`, params)
  }
  ConsideringSimilarSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarSave`, params)
  }
  ItemSubType1SuggestionLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ItemSubType1SuggestionLoad/${param}`, params)
  }
  ConsideringSimilarWordSoundTranslateAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarWordSoundTranslateAdd`, params)
  }
  ConsideringSimilarWordSoundTranslateDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarWordSoundTranslateDelete`, params)
  }
  ConsideringSimilarTrademarkAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarTrademarkAdd`, params)
  }
  ConsideringSimilarTrademarkDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarTrademarkDelete`, params)
  }
  ConsideringSimilarInstructionBack(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionBack`, params)
  }
  ConsideringSimilarInstructionOwnerSame(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionOwnerSame`, params)
  }
  ConsideringSimilarInstructionAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionAdd`, params)
  }
  ConsideringSimilarInstructionLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionLoad/${param}`, params)
  }
  ItemSubType1SuggestionList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ItemSubType1SuggestionList`, params)
  }
  ConsideringSimilarInstructionSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionSave`, params)
  }
  ConsideringSimilarInstructionRuleSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionRuleSave`, params)
  }
  ConsideringSimilarInstructionDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionDelete`, params)
  }
  ConsideringSimilarInstructionPublicAdd(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionPublicAdd/${param}`, params)
  }
  ConsideringSimilarInstructionPublicDelete(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionPublicDelete/${param}`, params)
  }
  ConsideringSimilarInstructionSend(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionSend/${param}`, params)
  }
  ConsideringSimilarDocumentSave(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDocumentSave`, params)
  }
  ConsideringSimilarInstructionRequestNumberAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarInstructionRequestNumberAdd`, params)
  }
  //ConsideringSimilarDocumentSave(params: any): Observable<any> {
  //    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDocumentSave`, params)
  //}
  //ConsideringSimilarDocumentSave(params: any): Observable<any> {
  //    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDocumentSave`, params)
  //}
  ConsideringSimilarDocumentAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDocumentAdd`, params)
  }
  ConsideringSimilarDocumentDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDocumentDelete`, params)
  }
  ConsideringSimilarDocumentConsideringSimilarInstructionAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDocumentConsideringSimilarInstructionAdd`, params)
  }
  ConsideringSimilarDocumentConsideringSimilarInstructionDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDocumentConsideringSimilarInstructionDelete`, params)
  }
  ConsideringSimilarDocumentLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarDocumentLoad/${param}`, params)
  }
  CheckingSimilarCompareBack(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareBack`, params)
  }
  CheckingSimilarCompareDocumentClassificationWordAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareDocumentClassificationWordAdd`, params)
  }
  CheckingSimilarCompareDocumentClassificationWordDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareDocumentClassificationWordDelete`, params)
  }
  CheckingSimilarCompareDocumentClassificationImageAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareDocumentClassificationImageAdd`, params)
  }
  CheckingSimilarCompareDocumentClassificationImageDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareDocumentClassificationImageDelete`, params)
  }
  CheckingSimilarCompareDocumentClassificationSoundAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareDocumentClassificationSoundAdd`, params)
  }
  CheckingSimilarCompareDocumentClassificationSoundDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareDocumentClassificationSoundDelete`, params)
  }
  CheckingSimilarCompareDocumentClassificationSmellAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareDocumentClassificationSmellAdd`, params)
  }
  CheckingSimilarCompareDocumentClassificationSmellDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareDocumentClassificationSmellDelete`, params)
  }
  CheckingSimilarCompareImageSoundTabAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareImageSoundTabAdd`, params)
  }
  CheckingSimilarCompareImageSoundTabDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareImageSoundTabDelete`, params)
  }
  CheckingSimilarCompareLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/CheckingSimilarCompareLoad/${param}`, params)
  }
  ConsideringSimilarKor10ProductKor10Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarKor10ProductKor10Add`, params)
  }

  ConsideringSimilarKor10ProductKor10Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarKor10ProductKor10Delete`, params)
  }

  ConsideringSimilarKor10ProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarKor10ProductAdd`, params)
  }

  ConsideringSimilarKor10ProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarKor10ProductDelete`, params)
  }

  ConsideringSimilarKor10Load(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ConsideringSimilarKor10Load/${param}`, params)
  }


}
