import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class pdfService {
  private apiPath = '/ViewPDF'
  constructor(private apiService: ApiService) { }

  checkPdfKor11(path: any, params: any): Observable<any> {
    return this.apiService.post(`/EFormSaveProcess/checkPdfKor11/${path}`, params)
  }

  viewPdf(path: any, params: any): Observable<any> {
    return this.apiService.pdf(`${this.apiPath}/${path}`, params)
  }

}
