import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
    providedIn: 'root'
})
export class RequestProcessService {
    private apiPath = '/RequestProcess'
    constructor(private apiService: ApiService) { }
    RequestEformLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/RequestEformLoad/${param}`, params)
    }
    RequestEformSend(params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/RequestEformSend`, params)
    }
    RequestOtherEformSend(params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/RequestOtherEformSend`, params)
    }
    List(view_name: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/List/${view_name}`, params)
    }
    RequestOtherEformLoad(param: any, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/RequestOtherEformLoad/${param}`, params)
    }
    Request01Load(id: number, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/Request01Load/${id}`, params)
    }
    RequestOtherLoad(id: number, params: any = null): Observable<any> {
        return this.apiService.post(`${this.apiPath}/RequestOtherLoad/${id}`, params)
    }
}
