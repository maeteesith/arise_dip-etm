import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
    providedIn: 'root'
})
export class ReferenceMasterService {
    private apiPath = '/ReferenceMaster/List'
    constructor(private apiService: ApiService) { }
    AddressTypeCodeList(param: any): Observable<any> {
        param.topic = "ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    Save020AddressTypeCodeList(param: any): Observable<any> {
        param.topic = "SAVE020_ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    CardTypeCodeList(param: any): Observable<any> {
        param.topic = "CARD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    ReceiverTypeCodeList(param: any): Observable<any> {
        param.topic = "RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    appeal_reason_codeList(param: any): Observable<any> {
        param.topic = "APPEAL_REASON_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    RequestItemTypeCodeList(param: any): Observable<any> {
        param.topic = "REQUEST_ITEM_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    FacilitationActStatusCodeList(param: any): Observable<any> {
        param.topic = "FACILITATION_ACT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    CommercialAffairsProvinceList(param: any): Observable<any> {
        param.topic = "COMMERCIAL_AFFAIRS_PROVINCE"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(this.apiPath, payload)
    }
    RequestSourceCodeList(param: any): Observable<any> {
        param.topic = "REQUEST_SOURCE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    RequestSoundTypeCodeList(param: any): Observable<any> {
        param.topic = "REQUEST_SOUND_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    RequestTypeList(param: any): Observable<any> {
        param.topic = "REQUEST_TYPE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(this.apiPath, payload)
    }
    ReceiptChannelTypeList(param: any): Observable<any> {
        param.topic = "RECEIPT_CHANNEL_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    ReceiptStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "RECEIPT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    SaveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DepartmentList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DEPARTMENT"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    appeal_type_codeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "APPEAL_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    appeal_maker_type_codeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "APPEAL_MAKER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    sex_codeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SEX_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ItemTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ITEM_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AppealTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "APPEAL_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AppealMakerTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "APPEAL_MAKER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    SexCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SEX_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ContactAddressTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONTACT_ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ContactCardTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONTACT_CARD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ContactReceiverTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONTACT_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ContactSexCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONTACT_SEX_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestOTOPTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_OTOP_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CountryCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_COUNTRY_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AddressCardTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_CARD_TYPE_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AddressReceiverTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AddressCareerCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_CAREER_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AddressSexCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_SEX_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    SaveOTOPTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE_OTOP_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AddressCountryCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_COUNTRY_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 500
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AddressEformCardTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_EFORM_CARD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ModalAddressTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "MODAL_ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ModalAddressCardTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "MODAL_ADDRESS_CARD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ModalAddressReceiverTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "MODAL_ADDRESS_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ModalAddressSexCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "MODAL_ADDRESS_SEX_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    inputModalAddress_AddressTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "INPUTMODALADDRESS_ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    inputModalAddress_AddressCardTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "INPUTMODALADDRESS_ADDRESS_CARD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    inputModalAddress_AddressReceiverTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "INPUTMODALADDRESS_ADDRESS_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    inputModalAddress_AddressSexCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "INPUTMODALADDRESS_ADDRESS_SEX_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    inputModal_Address_AddressTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "INPUTMODAL_ADDRESS_ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    inputModal_Address_AddressCardTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "INPUTMODAL_ADDRESS_ADDRESS_CARD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    inputModal_Address_AddressReceiverTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "INPUTMODAL_ADDRESS_ADDRESS_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    inputModal_Address_AddressSexCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "INPUTMODAL_ADDRESS_ADDRESS_SEX_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AppealReasonCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "APPEAL_REASON_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestRevokeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_REVOKE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    SueTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SUE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    SueReportTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SUE_REPORT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestDocumentCollectTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_DOCUMENT_COLLECT_TYPE_CODE"
        const payload = {
            filter: {
                ...param,
            }, paging: { item_per_page: 100 }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentClassificationStatusList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_CLASSIFICATION_STATUS"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentClassificationLanguageList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_CLASSIFICATION_LANGUAGE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030AppealTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_APPEAL_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030AppealMakerTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_APPEAL_MAKER_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030AppealReasonList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_APPEAL_REASON"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030AddressTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030AppealTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_APPEAL_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030AppealMakerTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_APPEAL_MAKER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030AppealReasonCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_APPEAL_REASON_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030InformerPeopleTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_INFORMER_PEOPLE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030InformerChallengeTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_INFORMER_CHALLENGE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030InformerReceiverTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_INFORMER_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030InformerPeriodTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_INFORMER_PERIOD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save030SearchTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE030_SEARCH_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save040TransferTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE040_TRANSFER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save040TransferFormCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE040_TRANSFER_FORM_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save040TransferPartCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE040_TRANSFER_PART_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save040TransferRequestTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE040_TRANSFER_REQUEST_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save040RepresentativeTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE040_REPRESENTATIVE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save040SubmitTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE040_SUBMIT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save040ReceiverAddressTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE040_RECEIVER_ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save040DonorAddressTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE040_DONOR_ADDRESS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050RepresentativeTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_REPRESENTATIVE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050PeriodAddressTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_PERIOD_ADDRESS_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050AllowAddressTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_ALLOW_ADDRESS_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050AllowTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_ALLOW_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050TransferFormList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_TRANSFER_FORM"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050ExtendTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_EXTEND_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050PeopleTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_PEOPLE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050ReceiverPeopleTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_RECEIVER_PEOPLE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050AddressTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_ADDRESS_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050SubmitTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_SUBMIT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save050SearchTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE050_SEARCH_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save060PeriodRequestCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE060_PERIOD_REQUEST_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save060RepresentativeTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE060_REPRESENTATIVE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save060RepresentativeChangeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE060_REPRESENTATIVE_CHANGE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save060AllowContactCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE060_ALLOW_CONTACT_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save070ExtendTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE070_EXTEND_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save070SubmitTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE070_SUBMIT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080RepresentativeTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_REPRESENTATIVE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080RevokeTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_REVOKE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080RequestItemTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_REQUEST_ITEM_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080PeopleTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_PEOPLE_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080AddressTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_ADDRESS_TYPE"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080AddressPeopleTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_ADDRESS_PEOPLE_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080AddressOthersTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_ADDRESS_OTHERS_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080AddressReceiverTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_ADDRESS_RECEIVER_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080AddressPeriodTypeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_ADDRESS_PERIOD_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080InformerPeopleTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_INFORMER_PEOPLE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080InformerOthersTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_INFORMER_OTHERS_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080InformerReceiverTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_INFORMER_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080InformerPeriodTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_INFORMER_PERIOD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080SearchTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_SEARCH_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save080SubmitTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE080_SUBMIT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save120SubmitTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE120_SUBMIT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save140SueTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE140_SUE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save140SueReportTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE140_SUE_REPORT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CheckingMethodTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CHECKING_METHOD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CheckingTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CHECKING_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CheckingStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CHECKING_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CheckingReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CHECKING_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CheckingWordTranslateStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CHECKING_WORD_TRANSLATE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CheckingWordTranslateDictionaryCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CHECKING_WORD_TRANSLATE_DICTIONARY_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CheckingWordSameConditionCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CHECKING_WORD_SAME_CONDITION_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    CheckingOrganizeTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CHECKING_ORGANIZE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ConsideringReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONSIDERING_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ConsideringSimilarInstructionTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONSIDERING_SIMILAR_INSTRUCTION_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ConsideringSimilarEvidence27CodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONSIDERING_SIMILAR_EVIDENCE_27_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ConsideringSimilarInstructionRuleCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONSIDERING_SIMILAR_INSTRUCTION_RULE_CODE"
        const payload = {
            filter: param,
            paging: { item_per_page: 100 }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    ConsideringSimilarInstructionRuleItemCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "CONSIDERING_SIMILAR_INSTRUCTION_RULE_ITEM_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save010ConsideringSimilarDocument06TypeCodeList(param: any): Observable<any> {
        param.topic = "SAVE010_CONSIDERING_SIMILAR_DOCUMENT_06_TYPE_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(this.apiPath, payload)
    }
    ConsideringSimilarDocumentStatusCodeList(param: any): Observable<any> {
        param.topic = "CONSIDERING_SIMILAR_DOCUMENT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    ConsideringSimilarInstructionRuleStatusCodeList(param: any): Observable<any> {
        param.topic = "CONSIDERING_SIMILAR_INSTRUCTION_RULE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    PublicTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicSourceCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_SOURCE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole01Case41StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE01_CASE41_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole05Case41StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE05_CASE41_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRoundCase41StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROUND_CASE41_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole01SendBackDepartmentList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE01_SEND_BACK_DEPARTMENT"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole04SendBackDepartmentList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE04_SEND_BACK_DEPARTMENT"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole04CheckInstructionRuleTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE04_CHECK_INSTRUCTION_RULE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole05SendBackDepartmentList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE05_SEND_BACK_DEPARTMENT"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestItemSubType1CodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_ITEM_SUB_TYPE_1_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AddressNationalityCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_NATIONALITY_CODE"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 300
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AddressRepresentativeConditionTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "ADDRESS_REPRESENTATIVE_CONDITION_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRoundStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROUND_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestMarkFeatureCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_MARK_FEATURE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestImageMarkTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_IMAGE_MARK_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    AssertTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE010_ASSERT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    TranslationLanguageCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "TRANSLATION_LANGUAGE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicReceiveDoStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_RECEIVE_DO_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole02CheckStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE02_CHECK_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole02ConsiderPaymentStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE02_CONSIDER_PAYMENT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole05ConsiderPaymentStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE05_CONSIDER_PAYMENT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PostRoundInstructionRuleCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "POST_ROUND_INSTRUCTION_RULE_CODE"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 100,
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PostRoundInstructionRuleFileTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "POST_ROUND_INSTRUCTION_RULE_FILE_TYPE_CODE"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 100,
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole02DocumentPaymentStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE02_DOCUMENT_PAYMENT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole02DocumentPostStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE02_DOCUMENT_POST_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PostRoundTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "POST_ROUND_TYPE_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100,
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PostRoundActionPostStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "POST_ROUND_ACTION_POST_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PostRoundActionPostTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "POST_ROUND_ACTION_POST_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DepartmentGroupCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DEPARTMENT_GROUP_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole04StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE04_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole04ReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE04_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole05StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE05_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole02StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE02_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    PublicRole02DocumentStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "PUBLIC_ROLE02_DOCUMENT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole02StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE02_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole02CheckStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE02_CHECK_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole02ReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE02_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole02ChangeKor120TypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE2_CHANGE_KOR120_TYPE_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            },
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DepartmentCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DEPARTMENT_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save010InstructionRuleDocumentReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE010_INSTRUCTION_RULE_DOCUMENT_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole2CheckSendBackDepartmentCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE2_CHECK_SEND_BACK_DEPARTMENT_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole02PrintDocumentStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE02_PRINT_DOCUMENT_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole02PrintCoverStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE02_PRINT_COVER_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole02PrintListStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE02_PRINT_LIST_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestCheckItemList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_CHECK_ITEM"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save010DocumentClassificationWordFirst(param: any): Observable<any> {
        param.topic = "SAVE010_DOCUMENT_CLASSIFICATION_WORD_FIRST"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(this.apiPath, payload)
    }
    Save010RequestGroupStatusCodeList(param: any): Observable<any> {
        param.topic = "SAVE010_REQUEST_GROUP_STATUS_CODE"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(this.apiPath, payload)
    }
    Save010DocumentClassificationSoundLast(param: any): Observable<any> {
        param.topic = "SAVE010_DOCUMENT_CLASSIFICATION_SOUND_LAST"
        const payload = {
            filter: param,
            paging: {
                item_per_page: 10000
            }
        }
        return this.apiService.post(this.apiPath, payload)
    }
    Save010DocumentClassificationSoundLastOther(param: any): Observable<any> {
        param.topic = "SAVE010_DOCUMENT_CLASSIFICATION_SOUND_LAST_OTHER"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(this.apiPath, payload)
    }
    Save010DocumentClassificationImage(param: any): Observable<any> {
        param.topic = "SAVE010_DOCUMENT_CLASSIFICATION_IMAGE_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    Save010DocumentClassificationSound(param: any): Observable<any> {
        param.topic = "SAVE010_DOCUMENT_CLASSIFICATION_SOUND_TYPE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    Save010DocumentClassificationLanguageList(param: any): Observable<any> {
        param.topic = "SAVE010_DOCUMENT_CLASSIFICATION_LANGUAGE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(this.apiPath, payload)
    }
    Floor3DocumentTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "FLOOR3_DOCUMENT_TYPE_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestItemTypeSplitCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_ITEM_TYPE_SPLIT_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Floor3RegistrarStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "FLOOR3_REGISTRAR_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RequestPrintCoverTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REQUEST_PRINT_COVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    RepresentativeTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "REPRESENTATIVE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole05PrintListStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE05_PRINT_LIST_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04PrintListStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_PRINT_LIST_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04ReleaseRequestSendTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_RELEASE_REQUEST_SEND_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04CreateTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_CREATE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04ReleaseStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_RELEASE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04Release20StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_RELEASE_20_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04ReleaseTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_RELEASE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04ReleaseReasonCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_RELEASE_REASON_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100,
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04ReleaseCaseCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_RELEASE_CASE_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100,
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    } 
    DocumentRole04Release20TypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_RELEASE_20_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole05ReleaseStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE05_RELEASE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole03StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE03_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole03ReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE03_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole03RequestTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE03_REQUEST_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole03ReceiveReturnTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE03_RECEIVE_RETURN_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole05Release20StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE05_RELEASE_20_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04CreateStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_CREATE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole05CreateStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE05_CREATE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole05StatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE05_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04TypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04ReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole05ReceiveStatusCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE05_RECEIVE_STATUS_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04SendTypeCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_SEND_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04CheckListCodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_CHECK_LIST_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentRole04CheckList2CodeList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_ROLE04_CHECK_LIST_2_CODE"
        const payload = {
            filter: param, paging: {
                item_per_page: 100
            }
        }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    DocumentClassificationVersionStatusList(param: any): Observable<any> {
        param = param || {}
        param.topic = "DOCUMENT_CLASSIFICATION_VERSION_STATUS"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save140InformerTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE140_INFORMER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save140InformTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE140_INFORM_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save140SubmitTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE140_SUBMIT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save140DepertmentTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE140_DEPARTMENT_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save150SearchTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE150_SEARCH_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save230DocumentTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE230_DOCUMENT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save150InformerPeopleTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE150_INFORMER_PEOPLE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save150InformerReceiverTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE150_INFORMER_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save150InformerPeriodTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE150_INFORMER_PERIOD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save190SearchTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE190_SEARCH_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save190InformerPeopleTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE190_INFORMER_PEOPLE_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save190InformerChallengerTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE190_INFORMER_CHALLENGER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save190InformerReceiverTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE190_INFORMER_RECEIVER_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save190InformerPeriodTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE190_INFORMER_PERIOD_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save190RequestTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE190_REQUEST_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save200SubjectTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE200_SUBJECT_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save200RequestTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE200_REQUEST_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save200ReceipientTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE200_RECEIPIENT_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }
    Save210SearchTypeCode(param: any): Observable<any> {
        param = param || {}
        param.topic = "SAVE210_SEARCH_TYPE_CODE"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(`/ReferenceMaster/List`, payload)
    }


    AppealBoardSolutionList(param: any): Observable<any> {
        param.topic = "RM_APPEAL_BOARDRE_SOLUTION"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }
    AppealSectionList(param: any): Observable<any> {
        param.topic = "RM_APPEAL_SECTION"
        const payload = { filter: param, paging: {} }
        return this.apiService.post(this.apiPath, payload)
    }

}
