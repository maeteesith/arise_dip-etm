import { Injectable } from '@angular/core'
import { Observable, forkJoin } from 'rxjs'
import { ReferenceMasterService } from './reference-master.service'
import { RequestProcessService } from './request-process.service'
@Injectable({
  providedIn: 'root'
})
export class ForkJoinService {
  constructor(
    private ReferenceMasterService: ReferenceMasterService,
    private requestProcessService: RequestProcessService
  ) { }
  initTestLogin(): Observable<any> {
    return forkJoin({
    })
  }
  initRegistrationOtherRequest(): Observable<any> {
    return forkJoin({
      facilitationActStatusCodeList: this.ReferenceMasterService.FacilitationActStatusCodeList({}),
      requestTypeList: this.ReferenceMasterService.RequestTypeList({}),
      commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({})
    })
  }
  initReceiptDaily(): Observable<any> {
    return forkJoin({
      paidChannelTypeCodeList: this.ReferenceMasterService.ReceiptChannelTypeList({}),
    })
  }
  initRequest01ItemList(): Observable<any> {
    return forkJoin({
      receiptStatusCodeList: this.ReferenceMasterService.ReceiptStatusCodeList({}),
    })
  }
  initRequestOtherItemList(): Observable<any> {
    return forkJoin({
      receiptStatusCodeList: this.ReferenceMasterService.ReceiptStatusCodeList({}),
      itemTypeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }
  initSave01Process(): Observable<any> {
    return forkJoin({
      requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      requestSourceCodeList: this.ReferenceMasterService.RequestSourceCodeList({}),
      saveOTOPTypeCodeList: this.ReferenceMasterService.SaveOTOPTypeCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
    })
  }
  initSave01ProcessList(): Observable<any> {
    return forkJoin({
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
      requestSourceCodeList: this.ReferenceMasterService.RequestSourceCodeList({}),
      itemTypeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }
  initSave020Process(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initSave020ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
      itemTypeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }
  initSave021Process(): Observable<any> {
    return forkJoin({
      itemTypeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }
  initSave021ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
    })
  }
  initSave03Process(): Observable<any> {
    return forkJoin({
      save030AppealTypeCodeList: this.ReferenceMasterService.Save030AppealTypeCodeList({}),
      save030AppealMakerTypeCodeList: this.ReferenceMasterService.Save030AppealMakerTypeCodeList({}),
      save030AppealReasonCodeList: this.ReferenceMasterService.Save030AppealReasonCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initSave03ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
    })
  }
  initSave04Process(): Observable<any> {
    return forkJoin({
      save040RepresentativeTypeCodeList: this.ReferenceMasterService.Save040RepresentativeTypeCodeList({}),
      save040TransferTypeCodeList: this.ReferenceMasterService.Save040TransferTypeCodeList({}),
      save040TransferFormCodeList: this.ReferenceMasterService.Save040TransferFormCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initSave04ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
    })
  }
  initSave05Process(): Observable<any> {
    return forkJoin({
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      save050RepresentativeTypeCodeList: this.ReferenceMasterService.Save050RepresentativeTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initSave05ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
    })
  }
  initSave06Process(): Observable<any> {
    return forkJoin({
      save060RepresentativeChangeCodeList: this.ReferenceMasterService.Save060RepresentativeChangeCodeList({}),
      save060AllowPeopleCodeList: this.ReferenceMasterService.Save060AllowContactCodeList({}),
      save060RepresentativeTypeCodeList: this.ReferenceMasterService.Save060RepresentativeTypeCodeList({}),
      save060PeriodRequestCodeList: this.ReferenceMasterService.Save060PeriodRequestCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      //requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      //requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      //requestSourceCodeList: this.ReferenceMasterService.RequestSourceCodeList({}),
      //saveOTOPTypeCodeList: this.ReferenceMasterService.SaveOTOPTypeCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      //addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      //addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      //addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      //addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      //commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      //requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
    })
  }
  initSave06ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
    })
  }
  initSave07Process(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      //requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      //requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      //requestSourceCodeList: this.ReferenceMasterService.RequestSourceCodeList({}),
      //saveOTOPTypeCodeList: this.ReferenceMasterService.SaveOTOPTypeCodeList({}),
      //addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      //addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      //addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      //addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      //addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      //commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({}),
      //addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      //addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      //requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
    })
  }
  initSave07ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
    })
  }
  initSave08Process(): Observable<any> {
    return forkJoin({
      save080RevokeTypeCodeList: this.ReferenceMasterService.Save080RevokeTypeCodeList({}),
      save080RepresentativeTypeCodeList: this.ReferenceMasterService.Save080RepresentativeTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initSave08ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
    })
  }
  initSave140Process(): Observable<any> {
    return forkJoin({
      save140SueTypeCodeList: this.ReferenceMasterService.Save140SueTypeCodeList({}),
      save140SueReportTypeCodeList: this.ReferenceMasterService.Save140SueReportTypeCodeList({}),
    })
  }
  initSave140ProcessList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
    })
  }
  initRequestSearch(): Observable<any> {
    return forkJoin({
    })
  }
  initRequestDocumentCollect(): Observable<any> {
    return forkJoin({
      requestTypeCodeList: this.ReferenceMasterService.RequestTypeCodeList({}),
      requestDocumentCollectTypeCodeList: this.ReferenceMasterService.RequestDocumentCollectTypeCodeList({}),
    })
  }
  initDocumentProcessClassification(): Observable<any> {
    return forkJoin({
      documentClassificationStatusList: this.ReferenceMasterService.DocumentClassificationStatusList({}),
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }
  initDocumentProcessClassificationDo(): Observable<any> {
    return forkJoin({
      save010DocumentClassificationWordFirst: this.ReferenceMasterService.Save010DocumentClassificationWordFirst({}),
      save010DocumentClassificationSoundLast: this.ReferenceMasterService.Save010DocumentClassificationSoundLast({}),
      save010DocumentClassificationSoundLastOther: this.ReferenceMasterService.Save010DocumentClassificationSoundLastOther({}),
      checkingWordSameConditionCodeList: this.ReferenceMasterService.CheckingWordSameConditionCodeList({}),
      save010DocumentClassificationLanguageList: this.ReferenceMasterService.Save010DocumentClassificationLanguageList({}),
    })
  }
  initCheckingSimilarImage(): Observable<any> {
    return forkJoin({
      checkingMethodTypeCodeList: this.ReferenceMasterService.CheckingMethodTypeCodeList({}),
    })
  }
  initCheckingItemList(): Observable<any> {
    return forkJoin({
      checkingTypeCodeList: this.ReferenceMasterService.CheckingTypeCodeList({}),
      checkingStatusCodeList: this.ReferenceMasterService.CheckingStatusCodeList({}),
    })
  }
  initCheckingSimilarList(): Observable<any> {
    return forkJoin({
      checkingReceiveStatusCodeList: this.ReferenceMasterService.CheckingReceiveStatusCodeList({}),
    })
  }
  initCheckingSimilar(): Observable<any> {
    return forkJoin({
      checkingWordTranslateStatusCodeList: this.ReferenceMasterService.CheckingWordTranslateStatusCodeList({}),
      checkingWordTranslateDictionaryCodeList: this.ReferenceMasterService.CheckingWordTranslateDictionaryCodeList({}),
      checkingWordSameConditionCodeList: this.ReferenceMasterService.CheckingWordSameConditionCodeList({}),
      checkingOrganizeTypeCodeList: this.ReferenceMasterService.CheckingOrganizeTypeCodeList({}),
    })
  }
  initCheckingSimilarWord(): Observable<any> {
    return forkJoin({
      checkingMethodTypeCodeList: this.ReferenceMasterService.CheckingMethodTypeCodeList({}),
    })
  }
  initCheckingSimilarResult(): Observable<any> {
    return forkJoin({
    })
  }
  initConsideringSimilarList(): Observable<any> {
    return forkJoin({
      consideringReceiveStatusCodeList: this.ReferenceMasterService.ConsideringReceiveStatusCodeList({}),
    })
  }
  initConsideringSimilar(): Observable<any> {
    return forkJoin({
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      consideringSimilarInstructionRuleCodeList: this.ReferenceMasterService.ConsideringSimilarInstructionRuleCodeList({}),
      checkingWordTranslateDictionaryCodeList: this.ReferenceMasterService.CheckingWordTranslateDictionaryCodeList({}),
      checkingWordTranslateStatusCodeList: this.ReferenceMasterService.CheckingWordTranslateStatusCodeList({}),
    })
  }
  initPublicDashboardRole01(): Observable<any> {
    return forkJoin({
    })
  }
  initeFormSave01Process(): Observable<any> {
    return forkJoin({
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      requestSourceCodeList: this.ReferenceMasterService.RequestSourceCodeList({}),
      saveOTOPTypeCodeList: this.ReferenceMasterService.SaveOTOPTypeCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      //add from save
      requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
    })
  }
  initeFormSave020Process(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initeFormSave021Process(): Observable<any> {
    return forkJoin({
      //itemTypeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
    })
  }
  initeFormSave030Process(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      //save030AppealTypeCodeList: this.ReferenceMasterService.Save030AppealTypeCodeList({}),
      save030AppealMakerTypeCodeList: this.ReferenceMasterService.Save030AppealMakerTypeCodeList({}),
      //save030AppealReasonCodeList: this.ReferenceMasterService.Save030AppealReasonCodeList({}),
      //addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      save30AddressTypeCodeList: this.ReferenceMasterService.Save030AddressTypeCodeList({}),
      save030InformPeopleTypeCodeList: this.ReferenceMasterService.Save030InformerPeopleTypeCodeList({}),
      save030InformChallengeTypeCodeList: this.ReferenceMasterService.Save030InformerChallengeTypeCodeList({}),
      save030InformReceiverTypeCodeList: this.ReferenceMasterService.Save030InformerReceiverTypeCodeList({}),
      save030InformPeriodTypeCodeList: this.ReferenceMasterService.Save030InformerPeriodTypeCodeList({}),
      save030SearchTypeCode: this.ReferenceMasterService.Save030SearchTypeCodeList({}),
      addressRepresentativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      requestTypeList: this.ReferenceMasterService.RequestTypeList({})
    })
  }
  initeFormSave040Process(): Observable<any> {
    return forkJoin({
      save040RepresentativeTypeCodeList: this.ReferenceMasterService.Save040RepresentativeTypeCodeList({}),
      save040TransferTypeCodeList: this.ReferenceMasterService.Save040TransferTypeCodeList({}),
      save040TransferFormCodeList: this.ReferenceMasterService.Save040TransferFormCodeList({}),
      save040TransferPartCodeList: this.ReferenceMasterService.Save040TransferPartCodeList({}),
      save040TransferRequestTypeCodeList: this.ReferenceMasterService.Save040TransferRequestTypeCodeList({}),
      save040SubmitTypeCodeList: this.ReferenceMasterService.Save040SubmitTypeCodeList({}),
      save040DonorAddressTypeCodeList: this.ReferenceMasterService.Save040DonorAddressTypeCodeList({}),
      save040ReceiverAddressTypeCodeList: this.ReferenceMasterService.Save040ReceiverAddressTypeCodeList({}),
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initeFormSave050Process(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      save050ExtendTypeCodeList: this.ReferenceMasterService.Save050ExtendTypeCodeList({}),
      save050PeopleTypeCodeList: this.ReferenceMasterService.Save050PeopleTypeCodeList({}),
      save050RepresentativeTypeCodeList: this.ReferenceMasterService.Save050RepresentativeTypeCodeList({}),
      save050ReceiverPeopleTypeCodeList: this.ReferenceMasterService.Save050ReceiverPeopleTypeCodeList({}),
      save050AddressTypeList: this.ReferenceMasterService.Save050AddressTypeList({}),
      save050PeriodAddressTypeList: this.ReferenceMasterService.Save050PeriodAddressTypeList({}),
      save050AllowAddressTypeList: this.ReferenceMasterService.Save050AllowAddressTypeList({}),
      save050AllowTypeCodeList: this.ReferenceMasterService.Save050AllowTypeCodeList({}),
      save050TransferFormList: this.ReferenceMasterService.Save050TransferFormList({}),
      save050SearchTypeCodeList: this.ReferenceMasterService.Save050SearchTypeCodeList({}),
      save050SubmitTypeCoseList: this.ReferenceMasterService.Save050SubmitTypeCodeList({}),
      requestTypeCodeList: this.ReferenceMasterService.RequestTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      requestTypeList: this.ReferenceMasterService.RequestTypeList({})
    })
  }
  initeFormSave060Process(): Observable<any> {
    return forkJoin({
      save060RepresentativeChangeCodeList: this.ReferenceMasterService.Save060RepresentativeChangeCodeList({}),
      save060AllowPeopleCodeList: this.ReferenceMasterService.Save060AllowContactCodeList({}),
      save060RepresentativeTypeCodeList: this.ReferenceMasterService.Save060RepresentativeTypeCodeList({}),
      save060PeriodRequestCodeList: this.ReferenceMasterService.Save060PeriodRequestCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      //requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      //requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      //requestSourceCodeList: this.ReferenceMasterService.RequestSourceCodeList({}),
      //saveOTOPTypeCodeList: this.ReferenceMasterService.SaveOTOPTypeCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      //addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      //addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      //addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      //addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      //commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      //requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
    })
  }
  initeFormSave070Process(): Observable<any> {
    return forkJoin({
      save070ExtendTypeList: this.ReferenceMasterService.Save070ExtendTypeList({}),
      save070SubmitTypeCodeList: this.ReferenceMasterService.Save070SubmitTypeCodeList({}),
      addressRepresentativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      //requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
      //requestSourceCodeList: this.ReferenceMasterService.RequestSourceCodeList({}),
      //saveOTOPTypeCodeList: this.ReferenceMasterService.SaveOTOPTypeCodeList({}),
      //commercialAffairsProvinceList: this.ReferenceMasterService.CommercialAffairsProvinceList({}),
      //requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
    })
  }
  initeFormSave080Process(): Observable<any> {
    return forkJoin({
      addressEformCardTypeCodeList: this.ReferenceMasterService.AddressEformCardTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.Save020AddressTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      representativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      save080RepresentativeTypeCodeList: this.ReferenceMasterService.Save080RepresentativeTypeCodeList({}),
      save080RevokeTypeCodeList: this.ReferenceMasterService.Save080RevokeTypeCodeList({}),
      save080RequestItemTypeCodeList: this.ReferenceMasterService.Save080RequestItemTypeCodeList({}),
      save080PeopleTypeList: this.ReferenceMasterService.Save080PeopleTypeList({}),
      save080AddressTypeList: this.ReferenceMasterService.Save080AddressTypeList({}),
      save080AddressPeopleTypeList: this.ReferenceMasterService.Save080AddressPeopleTypeList({}),
      save080AddressOthersTypeList: this.ReferenceMasterService.Save080AddressOthersTypeList({}),
      save080AddressReceiverTypeList: this.ReferenceMasterService.Save080AddressReceiverTypeList({}),
      save080AddressPeriodTypeList: this.ReferenceMasterService.Save080AddressPeriodTypeList({}),
      save080InformerPeopleTypeCodeList: this.ReferenceMasterService.Save080InformerPeopleTypeCodeList({}),
      save080InformerOthersTypeCodeList: this.ReferenceMasterService.Save080InformerOthersTypeCodeList({}),
      save080InformerReceiverTypeCodeList: this.ReferenceMasterService.Save080InformerReceiverTypeCodeList({}),
      save080InformerPeriodTypeCodeList: this.ReferenceMasterService.Save080InformerPeriodTypeCodeList({}),
      save080SearchTypeCodeList: this.ReferenceMasterService.Save080SearchTypeCodeList({}),
      save080SubmitTypeCodeList: this.ReferenceMasterService.Save080SubmitTypeCodeList({}),
      //saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
      //addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      requestTypeList: this.ReferenceMasterService.RequestTypeList({})
    })
  }
  initeFormSave120Process(): Observable<any> {
    return forkJoin({
      save120SubmitTypeCodeList: this.ReferenceMasterService.Save120SubmitTypeCodeList({}),
      addressRepresentativeTypeCodeList: this.ReferenceMasterService.RepresentativeTypeCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initeFormSave140Process(): Observable<any> {
    return forkJoin({
      save140SueTypeCodeList: this.ReferenceMasterService.Save140SueTypeCodeList({}),
      save140SueReportTypeCodeList: this.ReferenceMasterService.Save140SueReportTypeCodeList({}),
    })
  }
  initeFormSave150Process(): Observable<any> {
    return forkJoin({
    })
  }
  initeFormSave190Process(): Observable<any> {
    return forkJoin({
    })
  }
  initeFormSave200Process(): Observable<any> {
    return forkJoin({
    })
  }
  initDocumentItemList(): Observable<any> {
    return forkJoin({
      documentTypeCodeList: this.ReferenceMasterService.DocumentTypeCodeList({}),
      documentStatusCodeList: this.ReferenceMasterService.DocumentStatusCodeList({}),
    })
  }
  initConsideringSimilarInstruction(): Observable<any> {
    return forkJoin({
      consideringSimilarInstructionTypeCodeList: this.ReferenceMasterService.ConsideringSimilarInstructionTypeCodeList({}),
      consideringSimilarEvidence27CodeList: this.ReferenceMasterService.ConsideringSimilarEvidence27CodeList({}),
      consideringSimilarInstructionRuleCodeList: this.ReferenceMasterService.ConsideringSimilarInstructionRuleCodeList({}),
      consideringSimilarInstructionRuleItemCodeList: this.ReferenceMasterService.ConsideringSimilarInstructionRuleItemCodeList({}),
    })
  }
  initConsideringSimilarDocument(): Observable<any> {
    return forkJoin({
      save010ConsideringSimilarDocument06TypeCodeList: this.ReferenceMasterService.Save010ConsideringSimilarDocument06TypeCodeList({}),
      consideringSimilarDocumentStatusCodeList: this.ReferenceMasterService.ConsideringSimilarDocumentStatusCodeList({}),
      consideringSimilarInstructionRuleStatusCodeList: this.ReferenceMasterService.ConsideringSimilarInstructionRuleStatusCodeList({}),
    })
  }
  initPublicRole01ItemList(): Observable<any> {
    return forkJoin({
      publicTypeCodeList: this.ReferenceMasterService.PublicTypeCodeList({}),
      publicSourceCodeList: this.ReferenceMasterService.PublicSourceCodeList({}),
      publicStatusCodeList: this.ReferenceMasterService.PublicStatusCodeList({}),
    })
  }
  initPublicRole01CheckList(): Observable<any> {
    return forkJoin({
      publicReceiveStatusCodeList: this.ReferenceMasterService.PublicReceiveStatusCodeList({}),
    })
  }
  initPublicRole01CheckDo(): Observable<any> {
    return forkJoin({
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
      save010RequestGroupStatusCodeList: this.ReferenceMasterService.Save010RequestGroupStatusCodeList({}),
      publicRole01SendBackDepartmentList: this.ReferenceMasterService.PublicRole01SendBackDepartmentList({}),
    })
  }
  initPublicRole01PrepareList(): Observable<any> {
    return forkJoin({
      publicReceiveDoStatusCodeList: this.ReferenceMasterService.PublicReceiveDoStatusCodeList({}),
      requestItemSubType1CodeList: this.ReferenceMasterService.RequestItemSubType1CodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      publicRole01SendBackDepartmentList: this.ReferenceMasterService.PublicRole01SendBackDepartmentList({}),
    })
  }
  initPublicRole01DoingList(): Observable<any> {
    return forkJoin({
      publicRoundStatusCodeList: this.ReferenceMasterService.PublicRoundStatusCodeList({}),
    })
  }
  initPublicRole02CheckList(): Observable<any> {
    return forkJoin({
      publicRole02CheckStatusCodeList: this.ReferenceMasterService.PublicRole02CheckStatusCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
    })
  }
  initPublicRole02ConsiderPaymentList(): Observable<any> {
    return forkJoin({
      publicRole02ConsiderPaymentStatusCodeList: this.ReferenceMasterService.PublicRole02ConsiderPaymentStatusCodeList({}),
    })
  }
  initPublicRole05ConsiderPaymentList(): Observable<any> {
    return forkJoin({
      publicRole05ConsiderPaymentStatusCodeList: this.ReferenceMasterService.PublicRole05ConsiderPaymentStatusCodeList({}),
    })
  }
  initPublicRole02DocumentPaymentList(): Observable<any> {
    return forkJoin({
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      publicRole02DocumentPaymentStatusCodeList: this.ReferenceMasterService.PublicRole02DocumentPaymentStatusCodeList({}),
      publicRole01SendBackDepartmentList: this.ReferenceMasterService.PublicRole01SendBackDepartmentList({}),
    })
  }
  initPublicRole02DocumentPostList(): Observable<any> {
    return forkJoin({
      publicDocumentPaymentInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      publicRole02DocumentPostStatusCodeList: this.ReferenceMasterService.PublicRole02DocumentPostStatusCodeList({}),
    })
  }
  initPublicRole02ActionPostList(): Observable<any> {
    return forkJoin({
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      postRoundTypeCodeList: this.ReferenceMasterService.PostRoundTypeCodeList({}),
      postRoundActionPostStatusCodeList: this.ReferenceMasterService.PostRoundActionPostStatusCodeList({}),
      postRoundActionPostTypeCodeList: this.ReferenceMasterService.PostRoundActionPostTypeCodeList({}),
    })
  }
  initPublicRole04itemList(): Observable<any> {
    return forkJoin({
      departmentGroupCodeList: this.ReferenceMasterService.DepartmentGroupCodeList({}),
      publicRole04StatusCodeList: this.ReferenceMasterService.PublicRole04StatusCodeList({}),
    })
  }
  initPublicRole04CheckList(): Observable<any> {
    return forkJoin({
      publicRole04SendBackDepartmentList: this.ReferenceMasterService.PublicRole04SendBackDepartmentList({}),
      publicRole04ReceiveStatusCodeList: this.ReferenceMasterService.PublicRole04ReceiveStatusCodeList({}),
    })
  }
  initPublicRole05DocumentList(): Observable<any> {
    return forkJoin({
      publicRole05SendBackDepartmentList: this.ReferenceMasterService.PublicRole05SendBackDepartmentList({}),
      publicRole05StatusCodeList: this.ReferenceMasterService.PublicRole05StatusCodeList({}),
    })
  }
  initPublicRole04DocumentList(): Observable<any> {
    return forkJoin({
      publicRole04StatusCodeList: this.ReferenceMasterService.PublicRole04StatusCodeList({}),
    })
  }
  initPublicRole02DocumentList(): Observable<any> {
    return forkJoin({
      publicRole02StatusCodeList: this.ReferenceMasterService.PublicRole02StatusCodeList({}),
      publicRole02DocumentStatusCodeList: this.ReferenceMasterService.PublicRole02DocumentStatusCodeList({}),
    })
  }
  initDocumentRole02ItemList(): Observable<any> {
    return forkJoin({
      postRoundTypeCodeList: this.ReferenceMasterService.PostRoundTypeCodeList({}),
      departmentGroupCodeList: this.ReferenceMasterService.DepartmentGroupCodeList({}),
      documentRole02StatusCodeList: this.ReferenceMasterService.DocumentRole02StatusCodeList({}),
    })
  }
  initDocumentRole02CheckList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole02CheckStatusCodeList: this.ReferenceMasterService.DocumentRole02CheckStatusCodeList({}),
      documentRole02ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole02ReceiveStatusCodeList({}),
      save010InstructionRuleDocumentReceiveStatusCodeList: this.ReferenceMasterService.Save010InstructionRuleDocumentReceiveStatusCodeList({}),
    })
  }
  initDocumentRole02Check(): Observable<any> {
    return forkJoin({
      documentRole02ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole02ReceiveStatusCodeList({}),
      documentRole2CheckSendBackDepartmentCodeList: this.ReferenceMasterService.DocumentRole2CheckSendBackDepartmentCodeList({}),
    })
  }
  initDocumentRole02PrintDocumentList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole02PrintDocumentStatusCodeList: this.ReferenceMasterService.DocumentRole02PrintDocumentStatusCodeList({}),
    })
  }
  initDocumentRole02PrintCoverList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole02PrintCoverStatusCodeList: this.ReferenceMasterService.DocumentRole02PrintCoverStatusCodeList({}),
    })
  }
  initDocumentRole02PrintListList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole02PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole02PrintListStatusCodeList({}),
    })
  }
  initRequestCheck(): Observable<any> {
    return forkJoin({
      requestCheckItemList: this.ReferenceMasterService.RequestCheckItemList({}),
    })
  }
  initRequestSearchPeople(): Observable<any> {
    return forkJoin({
    })
  }
  initFloor3Document(): Observable<any> {
    return forkJoin({
      floor3DocumentTypeCodeList: this.ReferenceMasterService.Floor3DocumentTypeCodeList({}),
      requestItemTypeSplitCodeList: this.ReferenceMasterService.RequestItemTypeSplitCodeList({}),
    })
  }
  initRecordRegistrationNumber(): Observable<any> {
    return forkJoin({
    })
  }
  initRecordRegistrationNumberAllow(): Observable<any> {
    return forkJoin({
      floor3RegistrarStatusCodeList: this.ReferenceMasterService.Floor3RegistrarStatusCodeList({}),
    })
  }
  initCheckingSimilarCompare(): Observable<any> {
    return forkJoin({
    })
  }
  initReceiptDailyList(): Observable<any> {
    return forkJoin({
      receiptStatusCodeList: this.ReferenceMasterService.ReceiptStatusCodeList({}),
    })
  }
  initSaveSendList(): Observable<any> {
    return forkJoin({
      saveStatusCodeList: this.ReferenceMasterService.SaveStatusCodeList({}),
      departmentList: this.ReferenceMasterService.DepartmentList({}),
    })
  }
  initRequestPrintCover(): Observable<any> {
    return forkJoin({
      requestPrintCoverTypeCodeList: this.ReferenceMasterService.RequestPrintCoverTypeCodeList({}),
    })
  }
  initDocumentRole03Change(): Observable<any> {
    return forkJoin({
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
      save010RequestGroupStatusCodeList: this.ReferenceMasterService.Save010RequestGroupStatusCodeList({}),
      //save010ConsideringSimilarDocument06TypeCodeList: this.ReferenceMasterService.Save010ConsideringSimilarDocument06TypeCodeList({}),
      documentRole02ChangeKor120TypeCodeList: this.ReferenceMasterService.DocumentRole02ChangeKor120TypeCodeList({}),
    })
  }
  initDocumentRole04DocumentInstruction(): Observable<any> {
    return forkJoin({
    })
  }
  initDocumentRole04Appeal(): Observable<any> {
    return forkJoin({
    })
  }
  initDocumentRole04ReleaseRequest(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      documentRole04ReleaseRequestSendTypeCodeList: this.ReferenceMasterService.DocumentRole04ReleaseRequestSendTypeCodeList({}),
    })
  }
  initDashboardPublicRole01(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole05PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole05PrintListStatusCodeList({}),
    })
  }
  initDocumentRole05ChangeList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole05PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole05PrintListStatusCodeList({}),
    })
  }
  initDocumentRole05ConsiderDocumentList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole05PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole05PrintListStatusCodeList({}),
    })
  }
  initDashboardPublicRole04(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole05PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole05PrintListStatusCodeList({}),
    })
  }
  initDocumentRole04ChangeList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole04PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole04PrintListStatusCodeList({}),
    })
  }
  initDocumentRole04ReleaseRequestList(): Observable<any> {
    return forkJoin({
      documentRole04ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole04ReceiveStatusCodeList({}),
    })
  }
  initDocumentRole04DocumentInstructionList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole04PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole04PrintListStatusCodeList({}),
    })
  }
  initDocumentProcessChangeList(): Observable<any> {
    return forkJoin({
      documentClassificationVersionStatusList: this.ReferenceMasterService.DocumentClassificationVersionStatusList({}),
      //requestItemTypeCodeList: this.ReferenceMasterService.RequestItemTypeCodeList({}),
    })
  }
  initDocumentProcessCollectList(): Observable<any> {
    return forkJoin({
    })
  }
  initDashboardDocumentRole01(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole05PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole05PrintListStatusCodeList({}),
    })
  }
  initConsideringSimilarKor10(): Observable<any> {
    return forkJoin({
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
    })
  }
  initPublicRole02DocumentPayment(): Observable<any> {
    return forkJoin({
    })
  }
  initPublicRole02ConsiderPayment(): Observable<any> {
    return forkJoin({
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
      save010RequestGroupStatusCodeList: this.ReferenceMasterService.Save010RequestGroupStatusCodeList({}),
      publicRole01SendBackDepartmentList: this.ReferenceMasterService.PublicRole01SendBackDepartmentList({}),
    })
  }
  initPublicRole05ConsiderPayment(): Observable<any> {
    return forkJoin({
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
      save010RequestGroupStatusCodeList: this.ReferenceMasterService.Save010RequestGroupStatusCodeList({}),
      publicRole01SendBackDepartmentList: this.ReferenceMasterService.PublicRole01SendBackDepartmentList({}),
    })
  }
  initPublicRole04Check(): Observable<any> {
    return forkJoin({
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
      save010RequestGroupStatusCodeList: this.ReferenceMasterService.Save010RequestGroupStatusCodeList({}),
      publicRole01SendBackDepartmentList: this.ReferenceMasterService.PublicRole01SendBackDepartmentList({}),
      publicRole04CheckInstructionRuleTypeCodeList: this.ReferenceMasterService.PublicRole04CheckInstructionRuleTypeCodeList({}),
    })
  }
  initPublicRole05Document(): Observable<any> {
    return forkJoin({
      addressRepresentativeConditionTypeCodeList: this.ReferenceMasterService.AddressRepresentativeConditionTypeCodeList({}),
      addressCountryCodeList: this.ReferenceMasterService.AddressCountryCodeList({}),
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      addressCardTypeCodeList: this.ReferenceMasterService.AddressCardTypeCodeList({}),
      addressReceiverTypeCodeList: this.ReferenceMasterService.AddressReceiverTypeCodeList({}),
      addressSexCodeList: this.ReferenceMasterService.AddressSexCodeList({}),
      addressCareerCodeList: this.ReferenceMasterService.AddressCareerCodeList({}),
      addressNationalityCodeList: this.ReferenceMasterService.AddressNationalityCodeList({}),
      requestMarkFeatureCodeList: this.ReferenceMasterService.RequestMarkFeatureCodeList({}),
      save010RequestGroupStatusCodeList: this.ReferenceMasterService.Save010RequestGroupStatusCodeList({}),
      publicRole01SendBackDepartmentList: this.ReferenceMasterService.PublicRole01SendBackDepartmentList({}),
    })
  }
  initPublicRole01Case41CheckList(): Observable<any> {
    return forkJoin({
      publicRole01Case41StatusCodeList: this.ReferenceMasterService.PublicRole01Case41StatusCodeList({}),
      //publicRole05Case41StatusCodeList: this.ReferenceMasterService.PublicRole05Case41StatusCodeList({}),
      //publicRoundCase41StatusCodeList: this.ReferenceMasterService.PublicRoundCase41StatusCodeList({}),
    })
  }
  initDocumentRole04ItemList(): Observable<any> {
    return forkJoin({
      documentRole04StatusCodeList: this.ReferenceMasterService.DocumentRole04StatusCodeList({}),
      documentRole4TypeCodeList: this.ReferenceMasterService.DocumentRole04TypeCodeList({}),
      //departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      //documentRole04PrintListStatusCodeList: this.ReferenceMasterService.DocumentRole04PrintListStatusCodeList({}),
    })
  }
  initDocumentRole05ItemList(): Observable<any> {
    return forkJoin({
      documentRole04TypeCodeList: this.ReferenceMasterService.DocumentRole04TypeCodeList({}),
      documentRole04SendTypeCodeList: this.ReferenceMasterService.DocumentRole04SendTypeCodeList({}),
      documentRole05StatusCodeList: this.ReferenceMasterService.DocumentRole05StatusCodeList({}),
    })
  }
  initDocumentRole04CheckList(): Observable<any> {
    return forkJoin({
      documentRole04ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole04ReceiveStatusCodeList({}),
    })
  }
  initDocumentRole04Check(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      documentRole04SendTypeCodeList: this.ReferenceMasterService.DocumentRole04SendTypeCodeList({}),
      documentRole04CheckListCodeList: this.ReferenceMasterService.DocumentRole04CheckListCodeList({}),
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      postRoundInstructionRuleFileTypeCodeList: this.ReferenceMasterService.PostRoundInstructionRuleFileTypeCodeList({}),
    })
  }
  initDocumentRole05CheckList(): Observable<any> {
    return forkJoin({
      documentRole05ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole05ReceiveStatusCodeList({}),
    })
  }
  initDocumentRole05Check(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      documentRole04SendTypeCodeList: this.ReferenceMasterService.DocumentRole04SendTypeCodeList({}),
      documentRole04CheckListCodeList: this.ReferenceMasterService.DocumentRole04CheckListCodeList({}),
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      postRoundInstructionRuleFileTypeCodeList: this.ReferenceMasterService.PostRoundInstructionRuleFileTypeCodeList({}),
    })
  }
  initDocumentRole05ReleaseRequestList(): Observable<any> {
    return forkJoin({
      documentRole05ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole05ReceiveStatusCodeList({}),
    })
  }
  initDocumentRole05ReleaseRequest(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      documentRole04ReleaseRequestSendTypeCodeList: this.ReferenceMasterService.DocumentRole04ReleaseRequestSendTypeCodeList({}),
    })
  }
  initDocumentRole04CreateList(): Observable<any> {
    return forkJoin({
      documentRole04CreateTypeCodeList: this.ReferenceMasterService.DocumentRole04CreateTypeCodeList({}),
      documentRole04CreateStatusCodeList: this.ReferenceMasterService.DocumentRole04CreateStatusCodeList({}),
    })
  }
  initDocumentRole04Create(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      documentRole04CreateTypeCodeList: this.ReferenceMasterService.DocumentRole04CreateTypeCodeList({}),
      documentRole04CreateListCodeList: this.ReferenceMasterService.DocumentRole04CheckListCodeList({}),
      documentRole04CreateList2CodeList: this.ReferenceMasterService.DocumentRole04CheckList2CodeList({}),
      //postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      documentRole04ReleaseReasonCodeList: this.ReferenceMasterService.DocumentRole04ReleaseReasonCodeList({}),
    })
  }
  initDocumentRole05CreateList(): Observable<any> {
    return forkJoin({
      documentRole04CreateTypeCodeList: this.ReferenceMasterService.DocumentRole04CreateTypeCodeList({}),
      documentRole05CreateStatusCodeList: this.ReferenceMasterService.DocumentRole05CreateStatusCodeList({}),
    })
  }
  initDocumentRole05Create(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      documentRole04CreateTypeCodeList: this.ReferenceMasterService.DocumentRole04CreateTypeCodeList({}),
      documentRole04CreateListCodeList: this.ReferenceMasterService.DocumentRole04CheckListCodeList({}),
      documentRole04CreateList2CodeList: this.ReferenceMasterService.DocumentRole04CheckList2CodeList({}),
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      documentRole04ReleaseReasonCodeList: this.ReferenceMasterService.DocumentRole04ReleaseReasonCodeList({}),
    })
  }
  initDocumentRole04ReleaseList(): Observable<any> {
    return forkJoin({
      documentRole04ReleaseStatusCodeList: this.ReferenceMasterService.DocumentRole04ReleaseStatusCodeList({}),
    })
  }
  initDocumentRole04Release(): Observable<any> {
    return forkJoin({
      documentRole04ReleaseTypeCodeList: this.ReferenceMasterService.DocumentRole04ReleaseTypeCodeList({}),
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      documentRole04CheckListCodeList: this.ReferenceMasterService.DocumentRole04CheckList2CodeList({}),
      documentRole04ReleaseReasonCodeList: this.ReferenceMasterService.DocumentRole04ReleaseReasonCodeList({}),
      documentRole04ReleaseCaseCodeList: this.ReferenceMasterService.DocumentRole04ReleaseCaseCodeList({}),
    })
  }
  initDocumentRole04Release20(): Observable<any> {
    return forkJoin({
      documentRole04ReleaseTypeCodeList: this.ReferenceMasterService.DocumentRole04ReleaseTypeCodeList({}),
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      documentRole04CheckListCodeList: this.ReferenceMasterService.DocumentRole04CheckList2CodeList({}),
      documentRole04ReleaseReasonCodeList: this.ReferenceMasterService.DocumentRole04ReleaseReasonCodeList({}),
      documentRole04ReleaseCaseCodeList: this.ReferenceMasterService.DocumentRole04ReleaseCaseCodeList({}),
      documentRole04Release20TypeCodeList: this.ReferenceMasterService.DocumentRole04Release20TypeCodeList({}),
    })
  }
  initDocumentRole04Release20List(): Observable<any> {
    return forkJoin({
      documentRole04Release20StatusCodeList: this.ReferenceMasterService.DocumentRole04Release20StatusCodeList({}),
    })
  }
  initDocumentRole05Release20List(): Observable<any> {
    return forkJoin({
      documentRole05Release20StatusCodeList: this.ReferenceMasterService.DocumentRole05Release20StatusCodeList({}),
    })
  }
  initDocumentRole05Release20(): Observable<any> {
    return forkJoin({
      documentRole04ReleaseTypeCodeList: this.ReferenceMasterService.DocumentRole04ReleaseTypeCodeList({}),
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      documentRole04CheckListCodeList: this.ReferenceMasterService.DocumentRole04CheckList2CodeList({}),
      documentRole04ReleaseReasonCodeList: this.ReferenceMasterService.DocumentRole04ReleaseReasonCodeList({}),
      documentRole04ReleaseCaseCodeList: this.ReferenceMasterService.DocumentRole04ReleaseCaseCodeList({}),
      documentRole04Release20TypeCodeList: this.ReferenceMasterService.DocumentRole04Release20TypeCodeList({}),
    })
  }
  initDocumentRole05Release(): Observable<any> {
    return forkJoin({
      documentRole04ReleaseTypeCodeList: this.ReferenceMasterService.DocumentRole04ReleaseTypeCodeList({}),
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      documentRole04CheckListCodeList: this.ReferenceMasterService.DocumentRole04CheckList2CodeList({}),
      documentRole04ReleaseReasonCodeList: this.ReferenceMasterService.DocumentRole04ReleaseReasonCodeList({}),
      documentRole04ReleaseCaseCodeList: this.ReferenceMasterService.DocumentRole04ReleaseCaseCodeList({}),
    })
  }
  initDocumentRole05ReleaseList(): Observable<any> {
    return forkJoin({
      documentRole05ReleaseStatusCodeList: this.ReferenceMasterService.DocumentRole05ReleaseStatusCodeList({}),
    })
  }
  initDocumentRole03ChangeList(): Observable<any> {
    return forkJoin({
      documentRole03ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole03ReceiveStatusCodeList({}),
    })
  }
  initDocumentRole03ItemList(): Observable<any> {
    return forkJoin({
      //documentRole03StatusCodeList: this.ReferenceMasterService.DocumentRole03StatusCodeList({}),
      documentRole03StatusCodeList: this.ReferenceMasterService.DocumentRole03StatusCodeList({}),
    })
  }
  initRequestEformList(): Observable<any> {
    return forkJoin({
    })
  }
  initRequestEform(): Observable<any> {
    return forkJoin({
      facilitationActStatusCodeList: this.ReferenceMasterService.FacilitationActStatusCodeList({}),
      requestSoundTypeList: this.ReferenceMasterService.RequestSoundTypeCodeList({}),
    })
  }
  initRequestOtherEform(): Observable<any> {
    return forkJoin({
    })
  }
  initRequestOtherEformList(): Observable<any> {
    return forkJoin({
    })
  }
  initPublicRole05OtherList(): Observable<any> {
    return forkJoin({
      documentRole05ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole05ReceiveStatusCodeList({}),
      postRoundTypeCodeList: this.ReferenceMasterService.PostRoundTypeCodeList({}),
    })
  }
  initPublicRole05Other(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      postRoundTypeCodeList: this.ReferenceMasterService.PostRoundTypeCodeList({}),
    })
  }
  initPublicRole02OtherList(): Observable<any> {
    return forkJoin({
      documentRole02ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole02ReceiveStatusCodeList({}),
      postRoundTypeCodeList: this.ReferenceMasterService.PostRoundTypeCodeList({}),
    })
  }
  initPublicRole02ConsideringOtherList(): Observable<any> {
    return forkJoin({
      documentRole04ReceiveStatusCodeList: this.ReferenceMasterService.DocumentRole04ReceiveStatusCodeList({}),
      postRoundTypeCodeList: this.ReferenceMasterService.PostRoundTypeCodeList({}),
    })
  }
  initPublicRole02ConsideringOther(): Observable<any> {
    return forkJoin({
      addressTypeCodeList: this.ReferenceMasterService.AddressTypeCodeList({}),
      postRoundTypeCodeList: this.ReferenceMasterService.PostRoundTypeCodeList({}),
    })
  }
  initPublicRole03ReleaseList(): Observable<any> {
    return forkJoin({
    })
  }
  initPublicRole03Release(): Observable<any> {
    return forkJoin({
    })
  }
  initPublicRole02PrintDocumentList(): Observable<any> {
    return forkJoin({
      departmentCodeList: this.ReferenceMasterService.DepartmentCodeList({}),
      documentRole02PrintDocumentStatusCodeList: this.ReferenceMasterService.DocumentRole02PrintDocumentStatusCodeList({}),
    })
  }

  initDocumentRole02ActionPostList(): Observable<any> {
    return forkJoin({
      postRoundInstructionRuleCodeList: this.ReferenceMasterService.PostRoundInstructionRuleCodeList({}),
      postRoundTypeCodeList: this.ReferenceMasterService.PostRoundTypeCodeList({}),
      postRoundActionPostStatusCodeList: this.ReferenceMasterService.PostRoundActionPostStatusCodeList({}),
      postRoundActionPostTypeCodeList: this.ReferenceMasterService.PostRoundActionPostTypeCodeList({}),
    })
  }


}
