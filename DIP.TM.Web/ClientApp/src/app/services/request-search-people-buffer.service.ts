import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class RequestSearchPeopleService {
  private apiPath = '/RequestSearchPeople'
  constructor(private apiService: ApiService) { }
  RequestCheckVoucherNumberLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestCheckVoucherNumberLoad/${param}`, params)
  }
  RequestSearchLoad(param: any, params: any = null): Observable<any> {
    return this.apiService.post(`${this.apiPath}/RequestSearchLoad/${param}`, params)
  }
}
