import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class RequestAgencyService {
  private apiPath = '/RequestAgency'
  constructor(private apiService: ApiService) { }

  ListView(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/ListView`, params)
  }
}
