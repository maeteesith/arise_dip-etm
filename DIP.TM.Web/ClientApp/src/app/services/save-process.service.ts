import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
  providedIn: 'root'
})
export class SaveProcessService {
  private apiPath = '/SaveProcess'
  constructor(private apiService: ApiService) { }
  Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Reset`, params)
  }
  Save01Load(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save01Load/1`, params)
  }
  Save021(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021`, params)
  }
  Save021Load(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021Load/1`, params)
  }
  Save021Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021Save`, params)
  }
  Save021FileScanView(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021FileScanView`, params)
  }
  Save021Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021Send`, params)
  }
  Save021List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021List`, params)
  }
  Save01List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save01List`, params)
  }
  ItemTypeList(params: any): Observable<any> {
    return this.apiService.get(`/RequestProcess/Request01ListItemType`, params)
  }
  RequestSourceList(params: any): Observable<any> {
    return this.apiService.get(`/RequestProcess/Request01ListItemType`, params)
  }
  Save021MardidLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021MardidLoad`, params)
  }
  Save021People01Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021People01Add`, params)
  }
  Save021People01Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021People01Delete`, params)
  }
  Save021ProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021ProductAdd`, params)
  }
  Save021ProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021ProductDelete`, params)
  }
  Save021SaveProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021SaveProcessAdd`, params)
  }
  Save021SaveProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021SaveProcessDelete`, params)
  }
  Save03MadridLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03MadridLoad`, params)
  }
  Save03FileScanView(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03FileScanView`, params)
  }
  Save03Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03Save`, params)
  }
  Save03Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03Send`, params)
  }
  Save03CopyRequest01(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03CopyRequest01`, params)
  }
  Save03CopyRequest020(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03CopyRequest020`, params)
  }
  Save03PeopleAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03PeopleAdd`, params)
  }
  Save03PeopleDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03PeopleDelete`, params)
  }
  Save03RepresentativeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03RepresentativeAdd`, params)
  }
  Save03RepresentativeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03RepresentativeDelete`, params)
  }
  Save03People01Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03People01Add`, params)
  }
  Save03People01Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03People01Delete`, params)
  }
  Save03ProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03ProductAdd`, params)
  }
  Save03ProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03ProductDelete`, params)
  }
  Save03Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03Delete`, params)
  }
  Save03SaveProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03SaveProcessAdd`, params)
  }
  Save03SaveProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save03SaveProcessDelete`, params)
  }
  Save04MadridLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04MadridLoad`, params)
  }
  Save04Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04Delete`, params)
  }
  Save04Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04Save`, params)
  }
  Save04FileScanView(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04FileScanView`, params)
  }
  Save04Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04Send`, params)
  }
  Save04CopyProduct(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04CopyProduct`, params)
  }
  Save04People01Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04People01Add`, params)
  }
  Save04People01Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04People01Delete`, params)
  }
  Save04PeopleAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04PeopleAdd`, params)
  }
  Save04PeopleDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04PeopleDelete`, params)
  }
  Save04RepresentativeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04RepresentativeAdd`, params)
  }
  Save04RepresentativeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04RepresentativeDelete`, params)
  }
  Save04ProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04ProductAdd`, params)
  }
  Save04ProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04ProductDelete`, params)
  }
  Save04SaveProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04SaveProcessAdd`, params)
  }
  Save04SaveProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save04SaveProcessDelete`, params)
  }
  Save020Load(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020Load`, params)
  }
  Save020LocationList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020LocationList/1`, params)
  }
  Save020Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020Save`, params)
  }
  Save020Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020Reset`, params)
  }
  Save020FileScanView(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020FileScanView`, params)
  }
  Save020Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020Send`, params)
  }
  Save020People01Add(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020People01Add`, params)
  }
  Save020People01Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020People01Delete`, params)
  }
  Save020ProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020ProductAdd`, params)
  }
  Save020ProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020ProductDelete`, params)
  }
  Save020PeopleAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020PeopleAdd`, params)
  }
  Save020PeopleDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020PeopleDelete`, params)
  }
  Save020RepresentativeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020RepresentativeAdd`, params)
  }
  Save020RepresentativeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020RepresentativeDelete`, params)
  }
  Save020OppositionRequestAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020OppositionRequestAdd`, params)
  }
  Save020OppositionRequestDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020OppositionRequestDelete`, params)
  }
  Save020List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020List`, params)
  }
  Save020SaveProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020SaveProcessAdd`, params)
  }
  Save020SaveProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save020SaveProcessDelete`, params)
  }
  Save021Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save021Reset`, params)
  }
  Save05Load(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save05Load`, params)
  }
  Save05MadridLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save05MadridLoad`, params)
  }
  Save05Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save05Save`, params)
  }
  Save05Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save05Reset`, params)
  }
  Save05Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save05Send`, params)
  }
  Save05List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save05List`, params)
  }
  Save05SaveProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save05SaveProcessAdd`, params)
  }
  Save05SaveProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save05SaveProcessDelete`, params)
  }
  Save06Load(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06Load`, params)
  }
  Save06MadridLoad(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06MadridLoad`, params)
  }
  Save06LocationList(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06LocationList/1`, params)
  }
  Save06Delete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06Delete`, params)
  }
  Save06Save(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06Save`, params)
  }
  Save06Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06Reset`, params)
  }
  Save06FileScanView(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06FileScanView`, params)
  }
  Save06Send(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06Send`, params)
  }
  Save06WorkGroupChange(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06WorkGroupChange`, params)
  }
  Save06CopyRequest01(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06CopyRequest01`, params)
  }
  Save06CopyRequest020(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06CopyRequest020`, params)
  }
  Save06OldPeopleAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06OldPeopleAdd`, params)
  }
  Save06OldPeopleDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06OldPeopleDelete`, params)
  }
  Save06OldRepresentativeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06OldRepresentativeAdd`, params)
  }
  Save06OldRepresentativeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06OldRepresentativeDelete`, params)
  }
  Save06OwnProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06OwnProductAdd`, params)
  }
  Save06OwnProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06OwnProductDelete`, params)
  }
  Save06PeopleAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06PeopleAdd`, params)
  }
  Save06PeopleDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06PeopleDelete`, params)
  }
  Save06RepresentativeAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06RepresentativeAdd`, params)
  }
  Save06RepresentativeDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06RepresentativeDelete`, params)
  }
  Save06AddressAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06AddressAdd`, params)
  }
  Save06ProductAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06ProductAdd`, params)
  }
  Save06ProductDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06ProductDelete`, params)
  }
  Save06AllowContractPersonAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06AllowContractPersonAdd`, params)
  }
  Save06AllowContractPersonDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06AllowContractPersonDelete`, params)
  }
  Save06JoinerAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06JoinerAdd`, params)
  }
  Save06JoinerDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06JoinerDelete`, params)
  }
  Save06AddressDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06AddressDelete`, params)
  }
  Save06List(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06List`, params)
  }
  Save06SaveProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06SaveProcessAdd`, params)
  }
  Save06SaveProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save06SaveProcessDelete`, params)
  }
  Save01Reset(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save01Reset`, params)
  }
  Save01SaveProcessAdd(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save01SaveProcessAdd`, params)
  }
  Save01SaveProcessDelete(params: any): Observable<any> {
    return this.apiService.post(`${this.apiPath}/Save01SaveProcessDelete`, params)
  }

}
