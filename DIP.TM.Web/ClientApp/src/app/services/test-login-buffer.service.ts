import { Injectable } from '@angular/core'
import { Observable } from 'rxjs'
import { ApiService } from './apiService'
@Injectable({
    providedIn: 'root'
})
export class TestLoginService {
    private apiPath = '/User'
    constructor(private apiService: ApiService) { }

    SSO_Token(params: any): Observable<any> {
        return this.apiService.post(`${this.apiPath}/SSO_Token`, params)
    }
}
