import { Injectable } from "@angular/core";
import { Observable, Subject } from "rxjs";

@Injectable({
  providedIn: "root"
})
export class GlobalService {
  // User Info
  private subjectUserInfo = new Subject<any>();
  private userInfo: any;
  // Loading
  private subjectLoading = new Subject<boolean>();
  private isLoading: boolean;
  // Toast
  private subjectToast = new Subject<any>();
  private toast: any;

  constructor() {}

  //? Example use
  //? Must use in component
  //? Declared in constructor (private global: GlobalService)
  // this.global.watchUserAuth().subscribe((data: any) => {
  //   console.log('detect change')
  // })

  //! <<< User Info >>>
  watchUserInfo(): Observable<any> {
    return this.subjectUserInfo.asObservable();
  }
  getUserInfo(): any {
    return this.userInfo;
  }
  setUserInfo(value: any): void {
    console.log('%cuserInfo', 'color: deeppink', value)
    this.userInfo = value;
    this.subjectUserInfo.next(value);
  }

  //! <<< Loading >>>
  watchLoading(): Observable<boolean> {
    return this.subjectLoading.asObservable();
  }
  getLoading(): boolean {
    return this.isLoading;
  }
  setLoading(value: boolean): void {
    value ? console.log('%c-------- Loading --------', 'color: lawngreen') : console.log('%c-------------------------', 'color: lawngreen')
    this.isLoading = value;
    this.subjectLoading.next(value);
  }

  //! <<< Toast >>>
  watchToast(): Observable<any> {
    return this.subjectToast.asObservable();
  }
  getToast(): any {
    return this.toast;
  }
  setToast(value: any): void {
    console.log('toast', value)
    this.toast = value;
    this.subjectToast.next(value);
  }
}
