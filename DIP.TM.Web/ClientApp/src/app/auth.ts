import { Injectable } from "@angular/core";
import { GlobalService } from "./global.service";
import {
  LOCAL_STORAGE,
  LOGOUT_URL,
  getLocalStorage,
  removeLocalStorage
} from "./helpers";

@Injectable({
  providedIn: "root"
})
export class Auth {
  constructor(private global: GlobalService) { }

  isLogin(): boolean {
    return this.getAuth() ? true : false;
  }

  getToken(): any {
    return document.cookie.replace(/(?:(?:^|.*;\s*)DIM-TM-TOKEN\s*\=\s*([^;]*).*$)|^.*$/, "$1")
  }

  getAuthorization(): any {
    return `Bearer ${this.getToken()}`
  }

  getAuth(): any {
    let authGlobal = this.global.getUserInfo();
    if (authGlobal) {
      return authGlobal;
    } else {
      let authLocal = getLocalStorage(LOCAL_STORAGE.USER_INFO);
      if (authLocal) {
        this.global.setUserInfo(getLocalStorage(LOCAL_STORAGE.USER_INFO));
        return authLocal;
      } else {
        return null;
      }
    }
  }

  logout(): any {
    this.global.setUserInfo(null)
    removeLocalStorage(LOCAL_STORAGE.USER_INFO)
    // document.cookie = 'DIM-TM-TOKEN='
    this.redirect(LOGOUT_URL)
  }

  redirect(url: string): void {
    location.href = url
  }
}
