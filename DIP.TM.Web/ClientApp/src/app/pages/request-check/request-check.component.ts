import { Component, OnInit } from '@angular/core'
import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { RequestCheckService } from '../../services/request-check-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { AutoComplete } from '../../helpers/autocomplete'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone,  displayDate
} from '../../helpers'

@Component({
  selector: "app-request-check",
  templateUrl: "./request-check.component.html",
  styleUrls: ["./request-check.component.scss"]
})
export class RequestCheckComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List RequestCheck
  public listRequestCheck: any[]
  public paginateRequestCheck: any
  public perPageRequestCheck: number[]
  // Response RequestCheck
  public responseRequestCheck: any
  public listRequestCheckEdit: any


  // List RequestCheckRequestNumber
  public listRequestCheckRequestNumber: any[]
  public paginateRequestCheckRequestNumber: any
  public perPageRequestCheckRequestNumber: number[]
  // Response RequestCheckRequestNumber
  public responseRequestCheckRequestNumber: any
  public listRequestCheckRequestNumberEdit: any

  public autocompleteListRegistrarList: any
  public is_autocomplete_RegistrarList_show: any
  public is_autocomplete_RegistrarList_load: any


  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  public autocompleteListRequestCheckList: any
  public is_autocomplete_RequestCheckList_show: any
  public is_autocomplete_RequestCheckList_load: any

  //Modal Initial
  //Modal Initial
  public popup: any

  public request_item_sub_type_group_code_search: any
  public listRequestItemSubTypeGroup: any[]
  public listRequestItemSubTypeGroupCheck: any
  public paginateRequestItemSubTypeGroup: any

  public document_classification_image_type_code_search: any
  public listDocumentClassificationImageTypeCode: any
  public listDocumentClassificationImageTypeCodeCheck: any
  public paginateDocumentClassificationImageTypeCode: any

  public request_sound_type_code_search: any
  public listRequestSoundTypeCode: any
  public listRequestSoundTypeCodeCheck: any
  public paginateRequestSoundTypeCode: any


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      reference_number: '',
      requester_name: '',
      total_price: 0,
      is_wave_fee: false,
      select_list: {},
      other_list: {},
      item_id_list: {},
      quantity_list: {},
      total_price_list: {},
      check_value_3_list: {},
      maker_name: this.auth.getAuth().name,
    }
    this.listRequestCheck = []
    this.paginateRequestCheck = CONSTANTS.PAGINATION.INIT
    this.perPageRequestCheck = CONSTANTS.PAGINATION.PER_PAGE

    this.listRequestCheckRequestNumber = []

    this.paginateRequestCheckRequestNumber = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateRequestCheckRequestNumber.id = 'paginateRequestCheckRequestNumber'
    this.perPageRequestCheckRequestNumber = CONSTANTS.PAGINATION.PER_PAGE

    this.listRequestItemSubTypeGroup = []
    this.listRequestItemSubTypeGroupCheck = {}
    this.paginateRequestItemSubTypeGroup = CONSTANTS.PAGINATION.INIT

    this.listDocumentClassificationImageTypeCode = []
    this.listDocumentClassificationImageTypeCodeCheck = {}
    this.paginateDocumentClassificationImageTypeCode = CONSTANTS.PAGINATION.INIT

    this.listRequestSoundTypeCode = []
    this.listRequestSoundTypeCodeCheck = {}
    this.paginateRequestSoundTypeCode = CONSTANTS.PAGINATION.INIT

    //Master List
    this.master = {
    }
    //Master List

    this.autocompleteListRequestCheckList = []
    this.is_autocomplete_RequestCheckList_show = false
    this.is_autocomplete_RequestCheckList_load = false

    this.autocompleteListRegistrarList = []
    this.is_autocomplete_RegistrarList_show = false
    this.is_autocomplete_RegistrarList_load = false


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.autoComplete.Initial()
    this.autoComplete.Add("/RequestCheck/RequestItemSubType1List", "รหัสจำพวก", ["code", "name", "value_1"], [0, 1, 2], 0, this.input, [
      ["request_item_sub_type1_code", "code"],
      ["request_item_sub_type1_similar_list", "value_1"],
    ])
    this.autoComplete.Add("/RequestCheck/DocumentClassificationImageTypeList", "รหัสรูป", ["code", "name"], [0, 1], 0, this.input, [
      ["document_classification_image_type_code", "code"],
    ])
    this.autoComplete.Add("/RequestCheck/RequestSoundTypeList", "รหัสเสียง", ["name"], [0], 0, this.input, [
      ["request_sound_type_code", "code"],
      ["request_sound_type_name", "name"],
    ])
    this.autoComplete.Add("/RequestCheck/RequestAgencyList", "ชื่อผู้ยื่นคำขอ", ["name"], [0], 0, this.input, [
      ["requester_name", "name"],
      ["is_wave_fee", "is_wave_fee"],
    ])

    this.popup = {
      //is_request_item_sub_type_group_code_show: true
      warning_message_show_list: []
    }

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initRequestCheck().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.input.total_price_list = {}
      }
      if (this.editID) {
        this.RequestCheckService.RequestCheckLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private auth: Auth,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private RequestCheckService: RequestCheckService,
    private autoComplete: AutoComplete,
  ) { }

  // Autocomplete
  autocompleteRequestCheckList(object: any, name: any, item_per_page: any = 5, length: any = 8): void {
    if (object[name].length >= length) {
      const params = {
        item_per_page: item_per_page,
        //order_by: 'name',
        //is_order_reverse: false,
        search_by: [{
          key: 'reference_number',
          value: object[name],
          operation: 0
        }]
      }

      this.callAutocompleteRequestCheckList(params)
    }
  }
  autocompleteBlurRequestCheckList(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_RequestCheckList_show = false
    }, 200)
  }
  callAutocompleteRequestCheckList(params: any): void {
    if (this.input.is_autocomplete_RequestCheckList_load) return
    this.input.is_autocomplete_RequestCheckList_load = true
    this.global.setLoading(true)
    let pThis = this
    this.RequestCheckService.RequestCheckList(params).subscribe((data: any) => {
      if (data && data.list) {
        if (data.list.length == 1) {
          setTimeout(function () {
            pThis.loadData(data.list[0])
            pThis.global.setLoading(false)
          }, 200)
        } else if (data.list.length == 0) {
          this.onClickReset()
        } else {
          pThis.autocompleteListRequestCheckList = data.list
          //this.input.is_autocomplete_RequestCheckList_load = false
          pThis.global.setLoading(false)
        }
      }
    })
  }
  autocompleteChooseRequestCheckList(data: any): void {
    this.input = data

    this.listRequestCheck = data.request_check_item_list || []
    this.listRequestCheckRequestNumber = data.request_check_request_number_list || []
    this.changePaginateTotal(this.listRequestCheck.length, 'paginateRequestCheck')
    this.changePaginateTotal(this.listRequestCheckRequestNumber.length, 'paginateRequestCheckRequestNumber')

    this.listRequestItemSubTypeGroupCheck = {}
    this.listDocumentClassificationImageTypeCodeCheck = {}

    this.is_autocomplete_RequestCheckList_show = false
  }


  onClickRequestCheckSave(): void {
    //if(this.validateRequestCheckSave()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callRequestCheckSave(this.saveData())
    //}
  }
  validateRequestCheckSave(): boolean {
    let result = validateService('validateRequestCheckSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestCheckSave(params: any): void {
    this.RequestCheckService.RequestCheckSave(params).subscribe((data: any) => {
      // if(isValidRequestCheckSaveResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
    this.global.setLoading(false)
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickRequestCheckPrintKor09(): void {
    //if(this.validateRequestCheckPrintKor09()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callRequestCheckPrintKor09(this.saveData())
    //}
  }
  validateRequestCheckPrintKor09(): boolean {
    let result = validateService('validateRequestCheckPrintKor09', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestCheckPrintKor09(params: any): void {
    this.RequestCheckService.RequestCheckPrint(params).subscribe((data: any) => {
      // if(isValidRequestCheckPrintKor09Response(res)) {
      if (data) {
        // Set value
        this.loadData(data)
        window.open("/pdf/RequestCheckKor09/" + data.reference_number, "_blank")
        //window.open("/pdf/ReceiptRequestCheckReference/" + data.reference_number, "_blank")
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestCheckPrint(): void {
    //if(this.validateRequestCheckPrint()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callRequestCheckPrint(this.saveData())
    //}
  }
  validateRequestCheckPrint(): boolean {
    let result = validateService('validateRequestCheckPrint', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestCheckPrint(params: any): void {
    this.RequestCheckService.RequestCheckPrint(params).subscribe((data: any) => {
      // if(isValidRequestCheckPrintResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
        window.open("/pdf/RequestCheckKor09/" + data.reference_number, "_blank")
        //window.open("/pdf/ReceiptRequestCheckReference/" + data.reference_number, "_blank")
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestCheckPrintDocumentCopy(): void {
    ////if(this.validateRequestCheckPrintDocumentCopy()) {
    //// Open loading
    //this.global.setLoading(true)
    //// Call api
    //this.callRequestCheckPrintDocumentCopy(this.saveData())
    ////}

    var item_list = this.listRequestCheckRequestNumber.filter(r => r.number_2_3 && r.number_2_3 > 0)
    if (item_list.length > 0)
      window.open("/pdf/Kor09CopyDocument/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
    else
      alert("กรุณาเลือกเลขที่คำขอที่จะพิมพ์ ที่ข้อ 2.3")
  }
  validateRequestCheckPrintDocumentCopy(): boolean {
    let result = validateService('validateRequestCheckPrintDocumentCopy', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestCheckPrintDocumentCopy(params: any): void {
    this.RequestCheckService.RequestCheckPrintDocumentCopy(params).subscribe((data: any) => {
      // if(isValidRequestCheckPrintDocumentCopyResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestCheckPrintGuarantee(): void {
    ////if(this.validateRequestCheckPrintGuarantee ()) {
    //// Open loading
    //this.global.setLoading(true)
    //// Call api
    //this.callRequestCheckPrintGuarantee(this.saveData())
    ////}

    var item_list = this.listRequestCheckRequestNumber.filter(r => r.is_2_5)
    if (item_list.length > 0)
      window.open("/pdf/KorMor002/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
    else
      alert("กรุณาเลือกเลขที่คำขอที่จะพิมพ์ ที่ข้อ 2.5")
  }
  validateRequestCheckPrintGuarantee(): boolean {
    let result = validateService('validateRequestCheckPrintGuarantee ', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestCheckPrintGuarantee(params: any): void {
    this.RequestCheckService.RequestCheckPrintGuarantee(params).subscribe((data: any) => {
      // if(isValidRequestCheckPrintGuarantee Response(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestCheckPrintImportantDocuments(): void {
    ////if(this.validateRequestCheckPrintImportantDocuments()) {
    //// Open loading
    //this.global.setLoading(true)
    //// Call api
    //this.callRequestCheckPrintImportantDocuments(this.saveData())
    ////}

    var item_list = this.listRequestCheckRequestNumber.filter(r => r.is_2_6)
    if (item_list.length > 0)
      window.open("/pdf/KorMor04/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
    else
      alert("กรุณาเลือกเลขที่คำขอที่จะพิมพ์ ที่ข้อ 2.6")
  }
  validateRequestCheckPrintImportantDocuments(): boolean {
    let result = validateService('validateRequestCheckPrintImportantDocuments', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestCheckPrintImportantDocuments(params: any): void {
    this.RequestCheckService.RequestCheckPrintImportantDocuments(params).subscribe((data: any) => {
      // if(isValidRequestCheckPrintImportantDocumentsResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestCheckPrintRegistration(): void {
    ////if(this.validateRequestCheckPrintRegistration()) {
    //// Open loading
    //this.global.setLoading(true)
    //// Call api
    //this.callRequestCheckPrintRegistration(this.saveData())
    ////}
    var item_list = this.listRequestCheckRequestNumber.filter(r => r.is_2_2)
    if (item_list.length > 0)
      window.open("/pdf/Kor09RegisterForm/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
    else
      alert("กรุณาเลือกเลขที่คำขอที่จะพิมพ์ ที่ข้อ 2.2")
  }
  validateRequestCheckPrintRegistration(): boolean {
    let result = validateService('validateRequestCheckPrintRegistration', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestCheckPrintRegistration(params: any): void {
    this.RequestCheckService.RequestCheckPrintRegistration(params).subscribe((data: any) => {
      // if(isValidRequestCheckPrintRegistrationResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestCheckPrintRequest(): void {
    //if(this.validateRequestCheckPrintRequest()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callRequestCheckPrintRequest(this.saveData())
    //}
  }
  validateRequestCheckPrintRequest(): boolean {
    let result = validateService('validateRequestCheckPrintRequest', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestCheckPrintRequest(params: any): void {
    this.RequestCheckService.RequestCheckPrintRequest(params).subscribe((data: any) => {
      // if(isValidRequestCheckPrintRequestResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestCheckAdd(): void {
    this.listRequestCheck.push({
      index: this.listRequestCheck.length + 1,
      request_number: null,
      trademark_status_code: null,
      is_2_2: false,
      number_2_3: 0,
      is_2_5: false,
      is_2_6: false,
      is_2_7: false,
      document_status_code: null,
      page_index: null,
      page_lastest: null,

    })
    this.changePaginateTotal(this.listRequestCheck.length, 'paginateRequestCheck')
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.RequestCheckService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }


  onClickRequestCheckRequestNumberKeyPress(event: any): void {
    if (event.key == "Enter") {
      this.callRequestCheckRequestNumberKeyPress()
    }
  }
  callRequestCheckRequestNumberKeyPress(): void {
    let param = {
      request_number: this.input.request_number,
      //instruction_rule_id: this.editID ? +this.editID : null,
      //request_number: row_item.request_number,
      //is_deleted: row_item.is_deleted,
    }
    this.global.setLoading(true)
    this.RequestCheckService.RequestCheckRequestNumberAdd(param).subscribe((data: any) => {
      // if(isValidRequestCheckRequestNumberKeyPressResponse(res)) {
      if (data) {
        var request_check = {}

        if (data.trademark_expired_date && data.trademark_expired_date != "") {
          var trademark_expired_date = getMoment(data.trademark_expired_date, "YYYY-MM-DDTHH:mm:ss")
          var trademark_expired_start_date = getMoment(data.trademark_expired_start_date, "YYYY-MM-DDTHH:mm:ss")
          var trademark_expired_end_date = getMoment(data.trademark_expired_end_date, "YYYY-MM-DDTHH:mm:ss")

          if (getMoment() > trademark_expired_date) {
            var is_show_expired = true

            if (data.last_extend_date && data.last_extend_date != "") {
              var last_extend_date = getMoment(data.last_extend_date, "YYYY-MM-DDTHH:mm:ss")

              if (last_extend_date > trademark_expired_start_date) {
                var msg = {
                  message: "เลขคำขอ <b>" + data.request_number + "</b> " +
                    "ทะเบียนเลขที่ <b>" + data.registration_number + "</b> " +
                    "ยังพิจารณาต่ออายุไม่เสร็จ",
                  is_warning: true,
                }
                //console.log("warnning_message")
                //row_item.warnning_message = row_item.warnning_message || ""
                //if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                //row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ถึงกำหนดการต่ออายุวันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>"
                this.popup.warning_message_show_list.push(msg)
                is_show_expired = false
              }
            }

            if (is_show_expired) {
              var msg = {
                message: "เลขคำขอ <b>" + data.request_number + "</b> " +
                  "ทะเบียนเลขที่ <b>" + data.registration_number + "</b> " +
                  "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
                  "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
                  "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
                  "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
                  "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
                is_warning: true,
              }
              //console.log("warnning_message")
              //row_item.warnning_message = row_item.warnning_message || ""
              //if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
              //row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ถึงกำหนดการต่ออายุวันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>"
              this.popup.warning_message_show_list.push(msg)
            }
            //var msg = {
            //  message: "เลขคำขอ <b>" + item.request_number + "</b> " +
            //    "ทะเบียนเลขที่ <b>" + item.registration_number + "</b> " +
            //    "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
            //    "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
            //    "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
            //    "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
            //    "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
            //  is_warning: true,
            //}
            //console.log("warnning_message")
            //row_item.warnning_message = row_item.warnning_message || ""
            //if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
            //row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ถึงกำหนดการต่ออายุวันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>"
            //pThis.popup.warning_message_show_list.push(msg)
            this.global.setLoading(false)
            return;
          }
        }

        Object.keys(data).forEach((item: any) => {
          if (item != "id") request_check[item] = data[item]
        })
        this.listRequestCheckRequestNumber.push(request_check)
        this.changePaginateTotal(this.listRequestCheckRequestNumber.length, 'paginateRequestCheckRequestNumber')
        // Set value
        //this.loadData(data)
        //this.automateTest.test(this, { Send: 1 });
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }


  onClickRequestCheckRequestNumberDelete(item: any): void {
    // if(this.validateConsideringSimilarInstructionDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    var rs = item.id ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
    if (rs && rs != "") {
      item.is_deleted = true
    }
    this.global.setLoading(false)
  }

  onChangeUpdate(): void {
    //is_wave_fee = is_wave_fee || this.input.is_wave_fee

    this.input.quantity_list["320"] = this.listRequestCheckRequestNumber.reduce(function (a, b) { return a + (b.is_2_2 ? 1 : 0) }, 0)
    this.input.quantity_list["330"] = this.listRequestCheckRequestNumber.reduce(function (a, b) { return a + b.number_2_3 }, 0)
    this.input.quantity_list["350"] = this.listRequestCheckRequestNumber.reduce(function (a, b) { return a + (b.is_2_5 ? 1 : 0) }, 0)
    this.input.quantity_list["360"] = this.listRequestCheckRequestNumber.reduce(function (a, b) { return a + (b.is_2_6 ? 1 : 0) }, 0)
    this.input.quantity_list["380"] = this.listRequestCheckRequestNumber.reduce(function (a, b) { return a + (b.is_2_7 ? 1 : 0) }, 0)

    this.input.total_price = 0
    this.master.requestCheckItemList.forEach((item: any) => {
      //if (item.code == "340")
      //  console.log(this.input.quantity_list[item.code])
      //if (this.input.quantity_list[item.code] > 40)
      //  console.log(this.input.quantity_list[item.code])

      this.input.total_price_list[item.code] =
        (item.code == "340" && this.input.quantity_list[item.code] > 40 ? 800 :
          (this.input.quantity_list[item.code] || 0) * +item.value_2)
      if (!this.input.is_wave_fee) {
        this.input.total_price += this.input.total_price_list[item.code]
      }
    })

    //console.log(this.input.total_price_list)
  }

  _onChangeUpdate(): void {
    let pThis = this
    setTimeout(function () { pThis.onChangeUpdate(); }, 100)
  }



  // Autocomplete
  autocompleteChangeRegistrarList(object: any, name: any, item_per_page: any = 5, length: any = 0): void {
    if (length == 0 || (object[name] && object[name].length >= length)) {
      object[name] = object[name] || ""

      this.is_autocomplete_RegistrarList_show = true

      const params = {
        item_per_page: item_per_page,
        order_by: 'name',
        is_order_reverse: false,
        search_by: [{
          key: 'name',
          value: object[name],
          operation: 5
        }]
      }

      this.callAutocompleteChangeRegistrarList(params)
    }
  }
  autocompleteBlurRegistrarList(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_RegistrarList_show = false
    }, 200)
  }
  callAutocompleteChangeRegistrarList(params: any): void {
    if (this.input.is_autocomplete_RegistrarList_load) return
    this.input.is_autocomplete_RegistrarList_load = true
    let pThis = this
    this.RequestCheckService.RequestCheckRegistrarList(params).subscribe((data: any) => {
      if (data && data.list) {
        if (data.list.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseRegistrarList(data.list[0])
          }, 200)
        } else {
          pThis.autocompleteListRegistrarList = data.list
        }
      }
    })
    this.input.is_autocomplete_RegistrarList_load = false
  }
  autocompleteChooseRegistrarList(data: any): void {
    this.input.registrar_by = data.id
    this.input.registrar_by_name = data.name

    this.is_autocomplete_RegistrarList_show = false
  }






  loadData(data: any): void {
    this.input = {
      ...data,
      select_list: {},
      other_list: {},
      item_id_list: {},
      quantity_list: {},
      total_price_list: {},
      check_value_3_list: {},
    }

    //this.listRequestCheck = data.request_check_item_list || []
    data.request_check_item_list.forEach((item: any) => {
      this.input.item_id_list[item.request_check_item_code] = item.id
      this.input.select_list[item.request_check_item_code] = item.is_select
      this.input.other_list[item.request_check_item_code] = item.value_other
      this.input.quantity_list[item.request_check_item_code] = item.value_quantity
      this.input.check_value_3_list[item.request_check_item_code] = item.is_check_3
    })

    this.listRequestCheckRequestNumber = data.request_check_request_number_list || []
    this.changePaginateTotal(this.listRequestCheck.length, 'paginateRequestCheck')
    this.changePaginateTotal(this.listRequestCheckRequestNumber.length, 'paginateRequestCheckRequestNumber')

    this.autoComplete.Update(this.input);
    this.onChangeUpdate()
  }

  listData(data: any): void {
    this.listRequestCheck = data.list || []
    this.listRequestCheckRequestNumber = data.list || []
    this.changePaginateTotal(this.listRequestCheck.length, 'paginateRequestCheck')
    this.changePaginateTotal(this.listRequestCheckRequestNumber.length, 'paginateRequestCheckRequestNumber')

  }

  saveData(): any {
    let params = this.input

    params.request_check_item_list = []
    Object.keys(this.input.quantity_list).forEach((item: any) => {
      params.request_check_item_list.push({
        id: this.input.item_id_list[item],
        request_check_id: this.input.id,
        request_check_item_code: item,
        is_select: this.input.select_list[item],
        value_other: this.input.other_list[item],
        value_quantity: this.input.quantity_list[item],
        value_total_price: this.input.total_price_list[item],
        is_check_3: this.input.check_value_3_list[item],
      })
    })

    params.requester_name = this.autoComplete.item_list[3].value_display

    params.request_check_request_number_list = this.listRequestCheckRequestNumber || []


    return params
  }

  /////////////////////
  onKeyDownRequestItemSubTypeGroup(event: any): void {
    if (event.key == "Enter") {
      this.onClickRequestItemSubTypeGroupList()
    }
  }
  onClickRequestItemSubTypeGroupList(): void {
    // if(this.validateRequestItemSubTypeGroupList()) {
    // Open loading
    // Call api
    var param = {
      name: this.request_item_sub_type_group_code_search
    }

    this.callRequestItemSubTypeGroupList(param)
    // }
  }
  //! <<< Call API >>>
  callRequestItemSubTypeGroupList(params: any): void {
    this.global.setLoading(true)
    this.RequestCheckService.RequestItemSubTypeGroupList(this.help.GetFilterParams(params, this.paginateRequestItemSubTypeGroup)).subscribe((data: any) => {
      // if(isValidRequestItemSubTypeGroupListResponse(res)) {
      if (data) {
        // Set value
        this.listRequestItemSubTypeGroup = data.list || []

        this.listRequestItemSubTypeGroup.forEach((item: any) => {
          if (this.listRequestItemSubTypeGroupCheck[item.id]) {
            item.is_check = true
          }
        })

        this.help.PageSet(data, this.paginateRequestItemSubTypeGroup)

        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickRequestItemSubTypeGroupShow(): void {
    this.popup.is_request_item_sub_type_group_code_show = true
    this.onClickRequestItemSubTypeGroupList()
  }
  onClickRequestItemSubTypeGroupSave(): void {
    var pThis = this

    if (this.input.is_listRequestItemSubTypeGroup_all) {
      this.input.request_item_sub_type1_code = "รหัสจำพวกทั้งหมด"
      this.input.request_item_sub_type1_similar_list = ""
    } else {
      this.input.request_item_sub_type1_code =
        Object.keys(this.listRequestItemSubTypeGroupCheck).map(function (r) {
          return pThis.listRequestItemSubTypeGroupCheck[r][0]
        }).join(", ")

      var request_item_sub_type1_similar_list = {}
      Object.keys(this.listRequestItemSubTypeGroupCheck).map(function (r) {
        return pThis.listRequestItemSubTypeGroupCheck[r][2]
      }).join(" ").split(" ").forEach((item: any) => {
        request_item_sub_type1_similar_list[item] = true
      })
      this.input.request_item_sub_type1_similar_list = Object.keys(request_item_sub_type1_similar_list).join(" ")
    }

    this.popup.is_request_item_sub_type_group_code_show = false
  }

  /////////////////////

  onKeyDownDocumentClassificationImageTypeCode(event: any): void {
    if (event.key == "Enter") {
      this.onClickDocumentClassificationImageTypeCodeList()
    }
  }
  onClickDocumentClassificationImageTypeCodeList(): void {
    // if(this.validateDocumentClassificationImageTypeCodeList()) {
    // Open loading
    // Call api
    var param = {
      name: this.document_classification_image_type_code_search
    }

    this.callDocumentClassificationImageTypeCodeList(param)
    // }
  }
  //! <<< Call API >>>
  callDocumentClassificationImageTypeCodeList(params: any): void {
    this.global.setLoading(true)
    this.RequestCheckService.DocumentClassificationImageTypeCodeList(this.help.GetFilterParams(params, this.paginateDocumentClassificationImageTypeCode)).subscribe((data: any) => {
      // if(isValidDocumentClassificationImageTypeCodeListResponse(res)) {
      if (data) {
        // Set value
        this.listDocumentClassificationImageTypeCode = data.list || []

        this.listDocumentClassificationImageTypeCode.forEach((item: any) => {
          if (this.listDocumentClassificationImageTypeCodeCheck[item.id]) {
            item.is_check = true
          }
        })

        this.help.PageSet(data, this.paginateDocumentClassificationImageTypeCode)

        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickDocumentClassificationImageTypeCodeShow(): void {
    this.popup.is_document_classification_image_type_code_show = true
    this.onClickDocumentClassificationImageTypeCodeList()
  }
  onClickDocumentClassificationImageTypeCodeSave(): void {
    var pThis = this

    if (this.input.is_listDocumentClassificationImageTypeCode_all) {
      this.input.document_classification_image_type_code = "รหัสภาพทั้งหมด"
    } else {
      this.input.document_classification_image_type_code =
        Object.keys(this.listDocumentClassificationImageTypeCodeCheck).map(function (r) {
          return pThis.listDocumentClassificationImageTypeCodeCheck[r][0]
        }).join(", ")
    }

    this.popup.is_document_classification_image_type_code_show = false
  }

  /////////////////////

  onKeyDownRequestSoundTypeCode(event: any): void {
    if (event.key == "Enter") {
      this.onClickRequestSoundTypeCodeList()
    }
  }
  onClickRequestSoundTypeCodeList(): void {
    // if(this.validateRequestSoundTypeCodeList()) {
    // Open loading
    // Call api
    var param = {
      name: this.request_sound_type_code_search
    }

    this.callRequestSoundTypeCodeList(param)
    // }
  }
  //! <<< Call API >>>
  callRequestSoundTypeCodeList(params: any): void {
    this.global.setLoading(true)
    this.RequestCheckService.RequestSoundTypeCodeList(this.help.GetFilterParams(params, this.paginateRequestSoundTypeCode)).subscribe((data: any) => {
      // if(isValidRequestSoundTypeCodeListResponse(res)) {
      if (data) {
        // Set value
        this.listRequestSoundTypeCode = data.list || []

        this.listRequestSoundTypeCode.forEach((item: any) => {
          if (this.listRequestSoundTypeCodeCheck[item.id]) {
            item.is_check = true
          }
        })

        this.help.PageSet(data, this.paginateRequestSoundTypeCode)

        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickRequestSoundTypeCodeShow(): void {
    this.popup.is_request_sound_type_code_show = true
    this.onClickRequestSoundTypeCodeList()
  }
  onClickRequestSoundTypeCodeSave(): void {
    var pThis = this

    if (this.input.is_listDocumentClassificationImageTypeCode_all) {
      this.input.request_sound_type_code = "รหัสเสียงทั้งหมด"
    } else {
      this.input.request_sound_type_code =
        Object.keys(this.listRequestSoundTypeCodeCheck).map(function (r) {
          return pThis.listRequestSoundTypeCodeCheck[r][1]
        }).join(", ")
    }

    this.popup.is_request_sound_type_code_show = false
  }


  /////////////////////

  onSelectColumnInTable(name: any, row_item: any = null, value: boolean) {
    if (name == "listRequestItemSubTypeGroup") {
      if (value)
        this.listRequestItemSubTypeGroupCheck[row_item.id] = [row_item.code, row_item.name, row_item.value_1]
      else
        delete this.listRequestItemSubTypeGroupCheck[row_item.id]
    } else if (name == "listDocumentClassificationImageTypeCode") {
      if (value)
        this.listDocumentClassificationImageTypeCodeCheck[row_item.id] = [row_item.code, row_item.name]
      else
        delete this.listDocumentClassificationImageTypeCodeCheck[row_item.id]
    } else if (name == "listRequestSoundTypeCode") {
      if (value)
        this.listRequestSoundTypeCodeCheck[row_item.id] = [row_item.code, row_item.name]
      else
        delete this.listRequestSoundTypeCodeCheck[row_item.id]
    }
  }


  onClickRequestCheckCopy(): void {
    Object.keys(this.input.quantity_list).forEach((item: any) => {
      this.input.quantity_list[item] = 0
    })

    this.input.request_item_sub_type1_code = ""
    this.input.request_item_sub_type1_similar_list = ""
    this.input.document_classification_image_type_code = ""
    this.input.request_sound_type_code = ""
    //this.listRequestCheckRequestNumber = []
    //this.changePaginateTotal(this.listRequestCheckRequestNumber.length, 'paginateRequestCheckRequestNumber')
    this.input.registrar_by_name = ""

    this.onChangeUpdate()
  }

  onClickSelectAll(object: any, attr: any, value: any): void {
    this[object].forEach((item: any) => {
      item[attr] = value
    })
    this.onChangeUpdate()
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    console.log(page)
    console.log(name)

    if (+ page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
    //let paginate = CONSTANTS.PAGINATION.INIT
    //paginate.totalItems = total
    //this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
