import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GlobalService } from "./../../global.service";
import { ForkJoinService } from "../../services/fork-join.service";
import {
  ROUTE_PATH,
  CONSTANTS,
  clone,
  displayLabelReferenceMaster,
} from "../../helpers";

@Component({
  selector: "app-request-kor06-change",
  templateUrl: "./request-kor06-change.component.html",
  styleUrls: ["./request-kor06-change.component.scss"],
})
export class RequestKor06ChangeComponent implements OnInit {
  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public list: any;
  public tab: any;
  public master: any;
  public validate: any;

  public tableList: any;
  public tableRequestDetail: any;
  public tableViewDocument: any;
  public tableInfo: any;
  public tableIssueBook: any;
  public tableAgent: any;
  public tableLicense: any;
  public tableProduct: any;
  public modal: any;

  constructor(
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService
  ) {}

  ngOnInit() {
    this.input = {
      dimension: "2D",
      audio: {
        sound: null,
        palyer: null,
        action: false,
      },
      control_code_list: [],
    };
    this.list = {};
    this.tab = {
      changeDetail: [
        { label: "เจ้าของ", isSelected: true },
        { label: "ตัวแทน", isSelected: false },
        { label: "สถานที่ติดต่อ", isSelected: false },
        { label: "รายการสินค้า", isSelected: false },
        { label: "เครื่องหมายรับรอง", isSelected: false },
        { label: "ผู้ร่วมใช้", isSelected: false },
        { label: "สัญญาอนุญาต", isSelected: false },
        { label: "สถานที่ติดต่อ (ก.06)", isSelected: false },
      ],
      registrar: [
        { label: "บันทึกเสนอนายทะเบียน", status: "done", isSelected: true },
        { label: "พิจารณาเอกสาร", status: "edit", isSelected: false },
        { label: "ออกหนังสือ", status: "edit", isSelected: false },
      ],
    };
    this.master = {};
    this.validate = {};

    this.tableList = {
      column_list: [
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.06",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting",
        },
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.07",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting",
        },
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.20",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting",
        },
      ],
    };

    this.tableRequestDetail = {
      head_column_list: [
        {
          order: "#",
          name: "ชื่อเจ้าของ",
          address: "ที่อยู่",
          tel: "โทรศัพท์",
          fax: "โทรสาร",
          name_old: "ชื่อเจ้าของเดิม",
          operation: "คำสั่ง",
        },
      ],

      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          fax: "-",
          name_old: "ฮันส์โกรส์ เอสอี",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          fax: "-",
          name_old: "ฮันส์โกรส์ เอสอี",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
          fax: "-",
          name_old: "-",
        },
      ],
    };

    this.tableViewDocument = {
      column_list: [
        {
          date: "10/10/2550",
        },
        {
          date: "10/10/2550",
        },
        {
          date: "10/10/2550",
        },
      ],
    };

    this.tableInfo = {
      column_list: [
        {
          detail: "ม.7",
        },
        {
          detail: "ม.9",
        },
        {
          detail: "ม.7",
        },
      ],
    };

    this.tableIssueBook = {
      column_list: [
        {
          date: "10/10/2550",
        },
        {
          date: "10/10/2550",
        },
        {
          date: "10/10/2550",
        },
      ],
    };

    this.tableAgent = {
      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
        },
      ],
    };

    this.tableLicense = {
      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          editor: "ชื่อผู้ได้รับอนุญาต",
          name_old: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          editor: "ที่อยู่ผู้ได้รับอนุญาต",
          name_old: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
          editor: "รายละเอียดสัญญา",
          name_old: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
          editor: "รายละเอียดสัญญา",
          name_old: "นายพงศกร อาจหาญ",
        },
      ],
    };

    this.tableProduct = {
      head_column_list: [
        {
          order: "#",
          type: "จำพวกสินค้า",
          description: "รายการสินค้า/บริการ",
        },
      ],

      column_list: [
        {
          type: "21",
        },
        {
          type: "3",
        },
      ],
    };

    this.modal = {};

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initRequestKor06ChangePage().subscribe((data: any) => {
      if (data) {
        this.master = data;

        //! Mock master data
        this.master = {
          controlCodeList: [
            { code: "1", name: "หนังสือแจ้งคำสั่ง" },
            { code: "2", name: "การปฏิบัติตามคำสั่ง" },
            { code: "3", name: "การชำระค่าธรรมเนียม" },
            { code: "4", name: "การแจ้งฟ้องคดี / คำพิพากษา" },
            { code: "5", name: "สถานที่ติดต่อ" },
            { code: "6", name: "ชื่อคู่กรณี (ถ้ามี)" },
            { code: "7", name: "การขอถอน" },
            { code: "8", name: "ใบตอบรับ / ครบกำหนด" },
            { code: "9", name: "มีอุทธรณ์ / คำวินิจฉัยฯ" },
          ],
          typeCodeList: [
            { code: "1", name: "ประเภท 1" },
            { code: "2", name: "ประเภท 2" },
          ],
          typeCodeList2: [
            { code: "1", name: "รอบที่ 1" },
            { code: "2", name: "รอบที่ 2" },
          ],
          typeCodeList3: [
            { code: "1", name: "อนุญาต" },
            { code: "2", name: "รวมเรื่อง" },
            { code: "3", name: "ไม่อนุญาต" },
          ],
          typeCodeList4: [
            { code: "1", name: "ยังไม่หลุด" },
            { code: "2", name: "ยังไม่หลุด" },
            { code: "3", name: "ยังไม่หลุด" },
          ],
          typeCodeList5: [
            { code: "1", name: "เจ้าของ" },
            { code: "2", name: "ตัวแทน" },
            { code: "3", name: "ตัวแทนช่วง" },
            { code: "4", name: "อื่นๆ" },
            { code: "5", name: "ตามก.ที่ยื่น" },
          ],
          typeCodeList6: [
            { code: "1", name: "ต.ค.5 ข้อ 8" },
            { code: "2", name: "ต.ค.9 ข้อ 10" },
            { code: "3", name: "ต.ค.9 ข้อ 2" },
            { code: "4", name: "ต.ค.9 ข้อ 3" },
          ],
          typeCodeList7: [
            { code: "1", name: "แก้ไขรายละเอียดตัวแทน" },
            { code: "2", name: "แต่งตั้งตัวแทนใหม่" },
            { code: "3", name: "แต่งตั้งตัวแทน (เพิ่ม)" },
            { code: "4", name: "ยกเลิกตัวแทนบางส่วน" },
            { code: "5", name: "ยกเลิกตัวแทนทั้งหมด" },
          ],
          typeCodeList8: [
            { code: "1", name: "มีสิทธิ" },
            { code: "2", name: "ไม่มีสิทธิ" },
          ],
          typeCodeList9: [
            { code: "1", name: "ได้" },
            { code: "2", name: "ไม่ได้" },
          ],
        };
        console.log("this.master", this.master);
      }

      // Call api get data
      this.getRequestKor06Data();
    });
  }

  //! <<< Call API >>>
  callRequestKor06Change(params: any): void {
    //! Mock data list
    this.global.setLoading(false);
    this.list.timeline = [
      { label: "10/10/2020", isSelected: false },
      { label: "15/10/2020", isSelected: false },
      { label: "20/10/2020", isSelected: false },
      { label: "25/10/2020", isSelected: false },
      { label: "29/10/2020", isSelected: false },
      { label: "30/10/2020", isSelected: false },
      { label: "31/10/2020", isSelected: true },
    ];

    // this.requestKor06ChangeService.List(params).subscribe((data: any) => {
    //   if (data) {
    //     // Set data
    //     this.input.url_2d = data.img_file_2d_id
    //       ? `/File/Content/${data.img_file_2d_id}`
    //       : null;
    //     this.input.url_3d = data.img_file_3d_id
    //       ? `/File/Content/${data.img_file_3d_id}`
    //       : null;
    //     this.input.audio.sound = data.sound_file
    //       ? `/File/Content/${data.sound_file.id}`
    //       : null;
    //   }
    //   // Close loading
    //   this.global.setLoading(false);
    // });
  }

  //! <<< Prepare Call API >>>
  getRequestKor06Data(): void {
    // Clear validate
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {};
    // Call api
    this.callRequestKor06Change(params);
  }

  //! <<< Event >>>
  onClickMadrid(): void {
    this.router.navigate([ROUTE_PATH.DASHBOARD.LINK]);
  }
  onClickContact(): void {
    this.router.navigate([ROUTE_PATH.DASHBOARD.LINK]);
  }
  onClickScan(): void {
    this.router.navigate([ROUTE_PATH.DASHBOARD.LINK]);
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: string, name: string, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: string, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
    } else {
      // Is close
      this.modal[name] = false;
    }
  }

  //! <<< Timeline >>>
  manageTimelineCallApi(): void {
    //TODO call api
  }
  onClickTimeline(item: any): void {
    this.clearSelectedTimeline();
    item.isSelected = !item.isSelected;
    this.manageTimelineCallApi();
  }
  onClickNextTimeline(): void {
    let index = this.getCurrentIndexTimeline();
    if (index < this.list.timeline.length - 1) {
      this.clearSelectedTimeline();
      this.list.timeline[index + 1].isSelected = true;
      this.manageTimelineCallApi();
    }
  }
  onClickPreviousTimeline(): void {
    let index = this.getCurrentIndexTimeline();
    if (index > 0) {
      this.clearSelectedTimeline();
      this.list.timeline[index - 1].isSelected = true;
      this.manageTimelineCallApi();
    }
  }
  getCurrentIndexTimeline(): number {
    let index = 0;
    if (this.list.timeline) {
      index = this.list.timeline.findIndex((item: any) => {
        return item.isSelected;
      });
    }
    return index;
  }
  clearSelectedTimeline(): void {
    this.list.timeline.forEach((item: any) => {
      item.isSelected = false;
    });
  }
  getSizeTimeline(): number {
    let size = 0;
    if (this.list.timeline) {
      size = this.list.timeline.length;
    }
    return size;
  }

  //! <<< Profile >>>
  togglePlayer(obj: string, name: string): void {
    this[obj][name].action = !this[obj][name].action;
    if (!this[obj][name].palyer) {
      this[obj][name].palyer = new Audio(this[obj][name].sound);
    }
    this[obj][name].action
      ? this[obj][name].palyer.play()
      : this[obj][name].palyer.pause();
  }

  //! <<< Tab >>>
  onChangeTab(obj: string, name: string, item: any): void {
    this[obj][name].forEach((item: any) => {
      item.isSelected = false;
    });
    item.isSelected = true;
  }
  getIsActiveTab(obj: string, name: string, index: number): boolean {
    if (this[obj][name]) {
      return this[obj][name][index].isSelected;
    }
    return false;
  }
  getStatusTabStyle(code: any): string {
    switch (code) {
      case "edit":
        return "status edit";
      case "done":
        return "status done";
      default:
        return "status edit";
    }
  }
}
