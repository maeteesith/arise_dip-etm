import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Help } from './../../helpers/help';
import { AutomateTest } from './../../test/automate_test';
import { GlobalService } from './../../global.service';
import {
  CONSTANTS,
  getMoment,
  clone,
  displayDateServer
} from '../../helpers'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mm09',
  templateUrl: './mm09.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./mm09.component.scss',
    "./../../../assets/theme/styles/madrid/madrid.scss"]
})
export class Mm09Component implements OnInit {

  constructor(private dialog: MatDialog,
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,) { }

  ngOnInit() {
  }

}
