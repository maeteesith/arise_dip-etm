/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Mm09Component } from './mm09.component';

describe('Mm09Component', () => {
  let component: Mm09Component;
  let fixture: ComponentFixture<Mm09Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mm09Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mm09Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
