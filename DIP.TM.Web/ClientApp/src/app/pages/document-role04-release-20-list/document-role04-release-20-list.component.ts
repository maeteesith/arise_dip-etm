import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-document-role04-release-20-list",
  templateUrl: "./document-role04-release-20-list.component.html",
  styleUrls: ["./document-role04-release-20-list.component.scss"]
})
export class DocumentRole04Release20ListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  // List DocumentRole04Release20
  public listDocumentRole04Release20: any[]
  public paginateDocumentRole04Release20: any
  public perPageDocumentRole04Release20: number[]
  // Response DocumentRole04Release20
  public responseDocumentRole04Release20: any
  public listDocumentRole04Release20Edit: any


  // List DocumentProcess
  public listDocumentProcess: any[]
  public paginateDocumentProcess: any
  public perPageDocumentProcess: number[]
  // Response DocumentProcess
  public responseDocumentProcess: any

  //label_save_combobox || label_save_radio
  public postRoundTypeCodeList: any[]
  public departmentGroupCodeList: any[]
  public documentRole04StatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public tableList: any


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //post_round_document_post_date: getMoment(),
      post_round_type_code: '',
      department_group_code: '',
      document_Role04_status_code: '',
      request_number: '',
    }
    this.listDocumentRole04Release20 = []
    this.paginateDocumentRole04Release20 = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentRole04Release20 = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentProcess = []

    //Master List
    this.master = {
      postRoundTypeCodeList: [],
      departmentGroupCodeList: [],
      documentRole04StatusCodeList: [],
    }
    //Master List


    if (!this.tableList) {
      this.tableList = {
        column_list: {
          index: "#",
          request_number: "เลขที่คำขอ",
          request_type_name: "เอกสาร",
          department_send_date: "วันที่ยื่นเอกสาร",
          name: "ชื่อผู้รับ",
          //address_information: "ที่อยู่",
          document_role04_release_20_send_date: "วันที่เสนอพิจรณา",
          document_role05_release_20_by_name: "เสนอนายทะเบียน",
          document_role05_release_20_send_date: "วันที่พิจรณา",
          value_01: "รายละเอียดเพิ่มเติม",
          document_role04_release_20_status_name: "สถานะ",
        },
        command_item: [{
          name: "item_edit"
        }],
      }
    }


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
    })

    this.forkJoinService.initDocumentRole04Release20List().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
        this.master.documentRole04Release20StatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.document_role04_release_20_status_code = ""
      }

      //this.global.setLoading(false)
      this.onClickDocumentRole04Release20List()

    })
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  onClickCommand($event) {
    console.log($event)
    if ($event) {
      if ($event.command) {
        if ($event.command == "item_edit") {
          window.open("document-role04-release-20/edit/" + $event.object_list[0].save_id)
        }
      } else {
        this.onClickDocumentRole04Release20List($event)
      }
    }
  }
  onClickDocumentRole04Release20List(paging = null): void {
    console.log(paging)
    // if(this.validateDocumentRole04Release20List()) {
    // Call api
    var param = {}

    Object.keys(this.tableList.column_list).forEach((item: any) => {
      console.log(item)
      if (this.input[item]) {
        console.log(item)
        param[item] = this.input[item]
      }

      if (item.endsWith("_name")) {
        var code = item.replace("_name", "_code")
        if (this.input[code]) {
          param[code] = this.input[code]
        }
      }

      if (item.endsWith("_date")) {
        var start_date = item.replace("_date", "_start_date")
        if (this.input[start_date]) {
          param[start_date] = this.input[start_date]
        }
        var end_date = item.replace("_date", "_end_date")
        if (this.input[end_date]) {
          param[end_date] = this.input[end_date]
        }
      }
    })

    this.callDocumentRole04Release20List(this.help.GetFilterParams(param, paging))
    // }
  }
  //! <<< Call API >>>
  callDocumentRole04Release20List(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.List("DocumentRole04Release20", params).subscribe((data: any) => {
      // if(isValidDocumentRole04Release20ListResponse(res)) {
      if (data) {
        // Set value
        //this.tableList.SetPaging(data)
        this.tableList.SetDataList(data)
        //this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickDocumentRole04Release20DocumentRole04Release20Add(): void {
    this.listDocumentRole04Release20.push({
      index: this.listDocumentRole04Release20.length + 1,
      request_number: null,
      request_date_text: null,
      request_item_sub_type_1_code_text: null,
      document_Role04_date_text: null,
      department_group_name: null,
      book_by_name: null,
      document_Role04_receive_date_text: null,
      document_Role04_receiver_by_name: null,
      document_Role04_remark: null,
      document_Role04_status_name: null,

    })
    this.changePaginateTotal(this.listDocumentRole04Release20.length, 'paginateDocumentRole04Release20')
  }

  onClickDocumentRole04Release20DocumentRole04Release20Edit(item: any): void {
    var win = window.open("/" + item.id)
  }

  //onClickDocumentRole04Release20AutoSplit(): void {
  //  this.DocumentProcessService.DocumentRole04Release20AutoSplit().subscribe((data: any) => {
  //    // if(isValidDocumentRole04Release20ListResponse(res)) {
  //    if (data) {
  //      // Set value
  //      //this.tableList.SetPaging(data)
  //      this.onClickDocumentRole04Release20List()
  //      //this.tableList.SetDataList(data)
  //      //this.listData(data)
  //      //this.automateTest.test(this, { list: data.list })
  //    }
  //    this.global.setLoading(false)
  //    // }
  //    // Close loading
  //  })
  //}

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listDocumentRole04Release20 = data.documentRole04item_list || []
    this.help.PageSet(data, this.paginateDocumentRole04Release20)

  }

  //listData(data: any): void {
  //  //this.listDocumentRole04Release20 = data.list || []
  //  //this.help.PageSet(data, this.paginateDocumentRole04Release20)
  //  this.tableList.SetDataList(data)
  //}

  saveData(): any {
    let params = this.input

    params.documentRole04item_list = this.listDocumentRole04Release20 || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalChecks / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

}
