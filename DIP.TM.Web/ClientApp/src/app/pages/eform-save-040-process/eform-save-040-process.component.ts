import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GlobalService } from "../../global.service";
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from "../../services/fork-join-eform.service";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";

import {
  CONSTANTS,
  ROUTE_PATH,
  validateService,
  clone,
  displayAddress,
  clearIdAndSaveId,
  viewPDF,
  getParamsOverwrite,
} from "../../helpers";

@Component({
  selector: "app-eform-save-040-process",
  templateUrl: "./eform-save-040-process.component.html",
  styleUrls: ["./eform-save-040-process.component.scss"],
})
export class eFormSave040ProcessComponent implements OnInit, DeactivationGuarded {
  //TODO >>> Declarations <<</'
  public editID: any;
  public eform_number: any;
  public input: any;
  public contactAddress: any;
  public receiverContactAddress: any;
  public saveInput: any;
  public search: any;
  public validate: any;
  public popup: any;
  public master: any;
  public userList: any[];
  public userList2: any[];
  public itemList1: any[];
  public validateObj: any;
  // Response
  public response: any;
  public menuList: any[];
  public currentStep: number;
  public currentID: number;
  public currentSubstep: number;
  public currentSubID: number;
  public currentSubStep: number;
  public progressPercent: number;
  public typeRadio: number;
  public typeRadio2: number;
  public typeRadio3: number;
  public typeRadio71: number;
  public typeRadio72: number;
  public stepPass: number;
  public whatTrademarkOwnerType: any;
  public listdropdownContact: any;
  public listdropdownReceiverContact: any;
  public ContactWord: any;
  public email: string;
  public telephone: string;
  public inputFileStep8: boolean;
  public calStep: number;
  public typeTransfer: any;
  public kor17List: any[];
  public save040_receiver_contact_index: number;
  public save040_contact_index: number;
  public contactChangeType: string;
  public receiverContactChangeType: string;

  public autocompleteListListModalLocation: any;
  public is_autocomplete_ListModalLocation_show: any;
  public is_autocomplete_ListModalLocation_load: any;
  public inputModalAddress: any;
  public inputModalAddressEdit: any;
  public modal: any;
  public inputAddress: any;
  public Next4: any;
  public Next6: any;
  public fileReaded: any;
  public csv: any;
  public lines: any[];
  public productList: any[];
  public productMainList: any[];
  // Autocomplete
  public autocompleteList: any;
  public isShowAutocomplete: any;

  //Table
  public currentP1: number;
  public perpageP1: number;
  public totalP1: number;

  public currentMain: number;
  public perpageMain: number;
  public totalMain: number;
  public typeS2: any;
  
  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eFormSaveProcessService: eFormSaveProcessService
  ) {}

  ngOnInit() {
    this.editID = +this.route.snapshot.paramMap.get("id");
    this.productMainList = [];
    this.inputFileStep8 = false;
    this.perpageMain = 5;
    this.whatTrademarkOwnerType = {
      person: true,
      corporation: false,
      foreign: false,
      foreignCorporation: false,
      governmentAgencies: false,
      other: false,
    };

    this.typeRadio = 1;
    this.menuList = [
      {
        id: 1,
        number: 1,
        name: "บันทึกเพื่อรับไฟล์",
        //required: true
        canEdit: false,
      },
      {
        id: 2,
        number: 2,
        name: "คำขอโอนหรือรับมรดก",
        //required: true
        canEdit: false,
      },
      {
        id: 3,
        number: 3,
        name: "ประเภทการยื่น",
        //required: true
        canEdit: false,
      },
      {
        id: 4,
        number: 4,
        name: "เจ้าของ (ผู้โอน)/ตัวแทน",
        canEdit: false,
      },
      {
        id: 5,
        number: 5,
        name: "สถานที่ติดต่อภายในประเทศไทย (ผู้โอน)",
        canEdit: false,
      },
      {
        id: 6,
        number: 6,
        name: "ผู้รับโอน/ตัวแทน",
        canEdit: false,
      },
      {
        id: 7,
        number: 7,
        name: "สถานที่ติดต่อภายในประเทศ (ผู้รับโอน)",
        canEdit: false,
      },
      {
        id: 8,
        number: 8,
        name: "รายละเอียดเครื่องหมายที่โอน",
        canEdit: false,
      },
      {
        id: 9,
        number: 9,
        name: "เอกสารหลักฐานประกอบคำขอโอนหรือรับมรดก",
        canEdit: false,
      },
      {
        id: 10,
        number: 10,
        name: "ค่าธรรมเนียม",
        canEdit: false,
      },
      {
        id: 11,
        number: 11,
        name: "เสร็จสิ้น",
        canEdit: false,
      },
    ];
    this.input = {
      indexEdit: undefined,
      point: "",
      isAllowEditRequestNumber: true,
      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      save040_representative_condition_type_code: "AND_OR",
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      contact_type_index: 0,
      listAgentMarkRepresentative1: [],
      listAgentMarkRepresentative2: [],
      address: {
        receiver_type_code: "PEOPLE",
      },
      ownerDomesticContactAddress: 0,
      agentDomesticContactAddress: 0,
      subagentDomesticContactAddress: 0,
      dimension_image: 2,
      imageDimension2: {
        file: {},
        blob: null,
      },
      imageDimension3_1: {
        file: {},
        blob: null,
      },
      imageDimension3_2: {
        file: {},
        blob: null,
      },
      imageDimension3_3: {
        file: {},
        blob: null,
      },
      imageDimension3_4: {
        file: {},
        blob: null,
      },
      sizeX: 5,
      sizeY: 5,
      isResize: false,
      audio: {
        input: "",
        validate: "",
        file: {},
        sound: null,
        palyer: null,
        action: false,
      },
      imageNote: {
        file: {},
        blob: null,
      },
      listTranslation: [],
      translationItem: {},
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0,
      },
      imageMark: {
        file: {},
        blob: null,
      },
      request_number: null,
      email: "",
      telephone: "",
      request_date_text: null,
      registration_number: null,
      save040_transfer_type_code: "TYPE1",
      people_list: [],
      representative_list: [],
      save040_contact_type_code: null,
      receiver_people_list: [],
      receiver_representative_list: [],
      save040_receiver_representative_condition_type_code: "AND_OR",
      receiver_contact_type_index: 0,
      listReceiverAgentMarkRepresentative1: [],
      listReceiverAgentMarkRepresentative2: [],
      receiver_address: {
        receiver_type_code: "PEOPLE",
      },
      save040_receiver_contact_type_code: null,
      save040_transfer_form_code: null,
      save040_transfer_part_code: null,
      is_9_1: false,
      is_9_2: false,
      is_9_3: false,
      is_9_4: false,
      is_9_5: false,
      is_9_6: false,
      is_9_7: false,
      is_9_8: false,
      is_9_9: false,
      is_9_10: false,
      payer_name: null,
      total_price: null,
      countryVal: "TH",
      personalTypeVal: "PEOPLE",
      nationalityTypeVal: null,
      agent_personal_typeVal: "PEOPLE",
      agent_countryVal: "TH",
      agent_nationalityTypeVal: null,
      CareerTypeVal: null,
      agent_CareerTypeVal: null,
      receiverMarkItem: {
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      agenReceiverMarkItem: {
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      isCheckAllOwnerSignature: false,
      isCheckAllAgentSignature: false,
    };

    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalDomesticContactAddressOpen: true,
    };

    this.input.listTranslation = [
      {
        id: 1,
        name: "yok",
        address: 1,
        tel: "คำขอโอนหรือรับมรดก",
        //required: true
      },
      {
        id: 2,
        name: "thnat",
        address: 2,
        tel: "ประเภทคำขอ",
      },
    ];

    this.popup = {
      isWarning: false,
      isConfirmSave: false,
      isSearch: false,
    };

    this.validate = {};
    this.contactAddress = {};
    this.receiverContactAddress = {};
    this.kor17List = [];

    //Master List
    this.master = {};
    this.response = {
      load: {},
    };

    // Autocomplete
    this.autocompleteList = {
      address_sub_district_name: [],
    };
    this.isShowAutocomplete = {
      address_sub_district_name: false,
    };

    this.autocompleteListListModalLocation = [];
    this.is_autocomplete_ListModalLocation_show = false;
    this.is_autocomplete_ListModalLocation_load = false;
    this.inputAddress = {};
    this.inputModalAddress = {};

    // Wizard
    this.currentStep = 1;
    this.currentID = 1;
    this.currentSubID = 1;
    this.currentSubStep = 1;
    this.progressPercent = 0;
    this.stepPass = 1;
    this.calStep = 0;

    // Other
    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initEForm04Page().subscribe((data: any) => {
      if (data) {
        this.master = data;
        data.requestTypeList.forEach((item: any) => {
          if (item.code === "40" && !this.editID) {
            this.input.total_price = item.value_2;
          }
        });
        console.log("this.master", this.master);
      }
      if (this.editID) {
        this.eFormSaveProcessService
          .eFormSave040Load(this.editID, {})
          .subscribe((data: any) => {
            if (data) {
              this.response.load = {
                eform_number: data.eform_number,
                id: data.id,
                wizard: data.wizard,
              };
              this.input.isAllowEditRequestNumber = data.request_number
                ? false
                : true;
              // step 1
              this.input.email = data.email;
              this.input.telephone = data.telephone;
              // step 2
              this.input.request_date_text = data.request_date_text;
              this.input.request_number = data.request_number;
              this.input.registration_number = data.registration_number;
              this.input.save040_transfer_type_code =
                data.save040_transfer_type_code;
              // step 3
              this.input.save040_submit_type_code =
                data.save040_submit_type_code;
              this.input.rule_number = data.rule_number;
              // step 4
              this.input.people_list = data.people_list;
              this.input.representative_list = data.representative_list;
              this.input.save040_representative_condition_type_code = data.save040_representative_condition_type_code
                ? data.save040_representative_condition_type_code
                : "AND_OR";
              // step 5
              this.input.save040_contact_type_code =
                data.save040_contact_type_code;
              this.input.address =
                data.save040_contact_type_code === "OTHERS"
                  ? data.contact_address_list[0]
                  : this.input.address;
              // step 6
              this.input.receiver_people_list = data.receiver_people_list;
              this.input.receiver_representative_list =
                data.receiver_representative_list;
              this.input.save040_receiver_representative_condition_type_code = data.save040_receiver_representative_condition_type_code
                ? data.save040_receiver_representative_condition_type_code
                : "AND_OR";
              // step 7
              this.input.save040_receiver_contact_type_code =
                data.save040_receiver_contact_type_code;
              this.input.receiver_address =
                data.save040_receiver_contact_type_code === "OTHERS"
                  ? data.receiver_contact_address_list[0]
                  : this.input.receiver_address;
              //TODO step 8
              this.input.save040_transfer_form_code =
                data.save040_transfer_form_code;
              this.input.save040_transfer_part_code =
                data.save040_transfer_part_code;
              // step 9
              this.input.save040_transfer_request_type_code =
                data.save040_transfer_request_type_code;
              this.input.is_9_1 = data.is_9_1;
              this.input.is_9_2 = data.is_9_2;
              this.input.is_9_3 = data.is_9_3;
              this.input.is_9_4 = data.is_9_4;
              this.input.is_9_5 = data.is_9_5;
              this.input.is_9_6 = data.is_9_6;
              this.input.is_9_7 = data.is_9_7;
              this.input.is_9_8 = data.is_9_8;
              this.input.is_9_9 = data.is_9_9;
              this.input.is_9_10 = data.is_9_10;
              // step 10
              this.input.payer_name = data.payer_name;
              this.input.total_price = data.total_price;
              // step 11
              this.input.isCheckAllOwnerSignature =
                data.sign_inform_person_list === "0" ? true : false;
              this.input.isCheckAllAgentSignature =
                data.sign_inform_representative_list === "0" ? true : false;
              this.manageWizard(data.wizard);
              this.setSignInform(data.sign_inform_representative_list);
            }
            // Close loading
            this.global.setLoading(false);
          });
      } else {
        this.global.setLoading(false);
      }
    });
  }

  //! <<< Call API >>>
  callSendEmail040(params: any): void {
    this.eFormSaveProcessService
      .eFormSave040Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchRequestNumber(): void {
    this.eFormSaveProcessService
      .eFormSave040Search(this.input.request_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.request_date_text = data.request_date_text;
          this.input.registration_number = data.registration_load_number;
          this.input.people_list = clearIdAndSaveId(data.people_load_list);
          this.input.representative_list = clearIdAndSaveId(
            data.representative_load_list
          );
          this.input.product_list = data.product_load_list;
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callEFormSave040Save(params: any): void {
    this.eFormSaveProcessService
      .eFormSave040Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.isDeactivation = true;
          // Open toast success
          let toast = CONSTANTS.TOAST.SUCCESS;
          toast.message = "บันทึกข้อมูลสำเร็จ";
          this.global.setToast(toast);
          // Navigate
          this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
        } else {
          // Close loading
          this.global.setLoading(false);
        }
      });
  }
  callSubDistrict(params: any, name: any): void {
    this.eFormSaveProcessService
      .SearchLocation(params)
      .subscribe((data: any) => {
        if (data) {
          this.autocompleteList[name] = data;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }

  //! <<< Prepare Call API >>>
  sendEmail(): void {
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      wizard: this.getWizard(),
      email: this.input.email,
      telephone: this.input.telephone,
    };
    // Call api
    this.callSendEmail040(params);
  }
  searchRequestNumber(): void {
    let result = validateService(
      "validateEFormSave140ProcessStep2",
      this.input
    );
    this.validate = result.validate;

    if (result.isValid) {
      // Open loading
      this.global.setLoading(true);
      // Call api
      this.callSearchRequestNumber();
    }
  }
  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }
  save(isOverwrite: boolean): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = this.getParamsSave();
    if (!isOverwrite) {
      params = getParamsOverwrite(params);
    }
    // Call api
    this.callEFormSave040Save(params);
  }
  getParamsSave(): any {
    return {
      id: this.response.load.id ? this.response.load.id : null,
      eform_number: this.response.load.eform_number
        ? this.response.load.eform_number
        : null,
      wizard: this.getWizard(),
      // step 1
      email: this.input.email,
      telephone: this.input.telephone,
      // step 2
      request_date_text: this.input.request_date_text,
      request_number: this.input.request_number,
      registration_number: this.input.registration_number,
      save040_transfer_type_code: this.input.save040_transfer_type_code,
      // step 3
      save040_submit_type_code: this.input.save040_submit_type_code,
      rule_number: this.input.rule_number,
      // step 4
      people_list: this.input.people_list,
      representative_list: this.input.representative_list,
      save040_representative_condition_type_code: this.input
        .save040_representative_condition_type_code,
      // step 5
      save040_contact_type_code: this.input.save040_contact_type_code,
      contact_address_list:
        this.input.save040_contact_type_code === "OTHERS"
          ? [this.input.address]
          : [],
      // step 6
      receiver_people_list: this.input.receiver_people_list,
      receiver_representative_list: this.input.receiver_representative_list,
      save040_receiver_representative_condition_type_code: this.input
        .save040_receiver_representative_condition_type_code,
      // step 7
      save040_receiver_contact_type_code: this.input
        .save040_receiver_contact_type_code,
      receiver_contact_address_list:
        this.input.save040_receiver_contact_type_code === "OTHERS"
          ? [this.input.receiver_address]
          : [],
      //TODO step 8
      save040_transfer_form_code: this.input.save040_transfer_form_code,
      save040_transfer_part_code: this.input.save040_transfer_part_code,
      // step 9
      save040_transfer_request_type_code: this.input
        .save040_transfer_request_type_code,
      is_9_1: this.input.is_9_1,
      is_9_2: this.input.is_9_2,
      is_9_3: this.input.is_9_3,
      is_9_4: this.input.is_9_4,
      is_9_5: this.input.is_9_5,
      is_9_6: this.input.is_9_6,
      is_9_7: this.input.is_9_7,
      is_9_8: this.input.is_9_8,
      is_9_9: this.input.is_9_9,
      is_9_10: this.input.is_9_10,
      // step 10
      payer_name: this.input.payer_name,
      total_price: this.input.total_price,
      // step 11
      sign_inform_person_list: this.input.isCheckAllOwnerSignature ? "0" : "",
      sign_inform_representative_list:
        this.input.isCheckAllAgentSignature &&
        this.input.save040_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform(),
    };
  }
  onClickViewPdfKor04(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM04", this.getParamsSave());
    }
  }
  onClickViewPdfKor17(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM17", this.getParamsSave());
    }
  }

  //! <<< Event >>>
  getWizard(): String {
    if (this.editID) {
      let wizardList = this.response.load.wizard.split("|");
      let sum = 0;
      let sumNow =
        this.currentID +
        this.currentStep +
        this.currentSubID +
        this.currentSubStep;

      wizardList.forEach((item: any) => {
        sum = +sum + +item;
      });

      if (sumNow > sum) {
        return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
      } else {
        return this.response.load.wizard;
      }
    } else {
      return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
    }
  }
  getSignInform(): String {
    let result = "";
    this.input.representative_list.forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input.representative_list.forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save040_representative_condition_type_code != "AND") {
        this.input.representative_list.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }

  //! <<< Wizard >>>
  changeStep(action: string): void {
    if (this.validateWizard()) {
      this.menuList[this.currentStep - 1].canEdit = true;

      action === "next" ? this.currentStep++ : this.currentStep--;
      this.currentID = this.menuList[this.currentStep - 1].id;
      this.currentSubstep = 1;
      if (this.stepPass == this.currentStep - 1)
        this.stepPass = this.stepPass + 1;
      console.log(
        "currentID",
        this.currentID,
        "currentStep",
        this.currentStep,
        "stepPass",
        this.stepPass
      );
      this.calcProgressPercent(this.currentStep);
    }
  }
  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    } else if (this.currentID === 2) {
      let result = validateService(
        "validateEFormSave140ProcessStep2",
        this.input
      );
      this.validate = result.validate;

      if (!this.input.people_list || this.input.people_list.length === 0) {
        result.isValid = false;
        console.warn(`request_number is invalid.`);
        this.validate.request_number = "กรุณาคลิก ค้นหา";
      } else {
        if (this.input.registration_number) {
          this.input.save040_transfer_type_code = "TYPE2";
        } else {
          this.input.save040_transfer_type_code = "TYPE1";
          this.input.save040_transfer_form_code = "TYPE1";
        }
      }

      return result.isValid;
    } else if (this.currentID === 3) {
      let result = validateService(
        "validateEFormSave04ProcessStep3",
        this.input
      );
      this.validate = result.validate;
      return result.isValid;
    } else if (this.currentID === 4) {
      if (this.input.people_list.length < 1) {
        console.warn(`step4 is invalid.`);
        this.validate.step4 = "กรุณาเพิ่มเจ้าของเครื่องหมาย";
        return false;
      } else {
        // Condition
        this.conditionWizard("clone_representative_list");
        return true;
      }
    } else if (this.currentID === 5) {
      let result = validateService(
        "validateEFormSave04ProcessStep5",
        this.input
      );
      this.validate = result.validate;

      // Condition
      if (result.isValid) {
        if (this.input.save040_contact_type_code === "OTHERS") {
          let result = validateService(
            "validateEFormSave01ProcessStep4Other",
            this.input.address
          );
          this.validate = result.validate;
          if (
            result.validate.address_district_name ||
            result.validate.address_province_name ||
            result.validate.postal_code
          ) {
            this.validate.address_sub_district_name = "กรุณากรอกและเลือก ตำบล";
          }
          return result.isValid;
        } else {
          // Set is_contact_person
          if (this.input.people_list) {
            if (this.input.save040_contact_type_code === "PEOPLE" &&
              this.input.people_list.length == 1) {
              this.input.contact_type_index = 0;
            }

            this.input.people_list.forEach((item: any, index: any) => {
              if (+this.input.contact_type_index === index &&
                this.input.save040_contact_type_code === "PEOPLE") {
                item.is_contact_person = true;
              } else {
                item.is_contact_person = false;
              }
            });
          }
          if (this.input.representative_list) {
            if (this.input.save040_contact_type_code === "REPRESENTATIVE" &&
              this.input.listAgentMarkRepresentative1.length == 1) {
              this.input.contact_type_index = 0;
            }
            else if (this.input.save040_contact_type_code === "REPRESENTATIVE_PERIOD" &&
              this.input.listAgentMarkRepresentative2.length == 1) {
              this.input.contact_type_index = 0;
            }

            this.input.representative_list.forEach((item: any, index: any) => {
              if (this.input.save040_contact_type_code !== "REPRESENTATIVE_PERIOD") {
                // ตัวแทน
                if (
                  this.input.listAgentMarkRepresentative1[
                  +this.input.contact_type_index
                  ] === index &&
                  this.input.save040_contact_type_code === "REPRESENTATIVE"
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              }
              else if (this.input.save040_contact_type_code !== "REPRESENTATIVE") {
                // ตัวแทนช่วง
                if (
                  this.input.listAgentMarkRepresentative2[
                  +this.input.contact_type_index
                  ] === index &&
                  this.input.save040_contact_type_code === "REPRESENTATIVE_PERIOD"
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              }
            });
          }
        }
      }

      return result.isValid;
    } else if (this.currentID === 6) {
      if (this.input.receiver_people_list.length < 1) {
        console.warn(`step6 is invalid.`);
        this.validate.step6 = "กรุณาเพิ่มผู้รับโอน";
        return false;
      } else {
        // Condition
        this.conditionWizard("clone_receiver_representative_list");
        return true;
      }
    } else if (this.currentID === 7) {
      let result = validateService(
        "validateEFormSave04ProcessStep7",
        this.input
      );
      this.validate = result.validate;

      // Condition
      if (result.isValid) {
        if (this.input.save040_receiver_contact_type_code === "OTHERS") {
          let result = validateService(
            "validateEFormSave01ProcessStep4Other",
            this.input.address
          );
          this.validate = result.validate;
          if (
            result.validate.address_district_name ||
            result.validate.address_province_name ||
            result.validate.postal_code
          ) {
            this.validate.address_sub_district_name = "กรุณากรอกและเลือก ตำบล";
          }
          return result.isValid;
        } else {
          // Set is_contact_person
          if (this.input.receiver_people_list) {
            if (this.input.save040_receiver_contact_type_code === "RECEIVER" &&
              this.input.receiver_people_list.length == 1) {
              this.input.receiver_contact_type_index = 0;
            }

            this.input.receiver_people_list.forEach((item: any, index: any) => {
              if (+this.input.receiver_contact_type_index === index &&
                this.input.save040_receiver_contact_type_code === "RECEIVER") {
                item.is_contact_person = true;
              } else {
                item.is_contact_person = false;
              }
            });
          } if (this.input.receiver_representative_list) {
            if (this.input.save040_receiver_contact_type_code === "REPRESENTATIVE" &&
              this.input.listReceiverAgentMarkRepresentative1.length == 1) {
              this.input.receiver_contact_type_index = 0;
            }
            else if (this.input.save040_receiver_contact_type_code === "REPRESENTATIVE_PERIOD" &&
              this.input.listReceiverAgentMarkRepresentative2.length == 1) {
              this.input.receiver_contact_type_index = 0;
            }

            this.input.receiver_representative_list.forEach(
              (item: any, index: any) => {
                if (
                  this.input.save040_receiver_contact_type_code !==
                  "REPRESENTATIVE_PERIOD"
                ) {
                  // ตัวแทน
                  if (
                    this.input.listReceiverAgentMarkRepresentative1[
                    +this.input.receiver_contact_type_index
                    ] === index &&
                    this.input.save040_receiver_contact_type_code === "REPRESENTATIVE"
                  ) {
                    item.is_contact_person = true;
                  } else {
                    item.is_contact_person = false;
                  }
                }
                else if (
                  this.input.save040_receiver_contact_type_code !== "REPRESENTATIVE"
                ) {
                  // ตัวแทนช่วง
                  if (
                    this.input.listReceiverAgentMarkRepresentative2[
                    +this.input.receiver_contact_type_index
                    ] === index &&
                    this.input.save040_receiver_contact_type_code === "REPRESENTATIVE_PERIOD"
                  ) {
                    item.is_contact_person = true;
                  } else {
                    item.is_contact_person = false;
                  }
                }
              }
            );
          }
        }
      }

      return result.isValid;
    } else if (this.currentID === 11) {
      let isValid = false;

      if (
        this.input.isCheckAllOwnerSignature ||
        this.input.isCheckAllAgentSignature
      ) {
        isValid = true;
      }

      if (this.input.representative_list.length > 0) {
        this.input.representative_list.forEach((item: any) => {
          if (item.isCheck) {
            isValid = true;
          }
        });
      }

      if (!isValid) {
        console.warn(`step11 is invalid.`);
        this.validate.step11 = "กรุณาเลือกลงลายมือชื่อ";
      } else {
        // Set can edit
        this.menuList[10].canEdit = true;
      }

      return isValid;
    } else {
      return true;
    }
  }
  conditionWizard(condition: any): void {
    if (condition === "clone_representative_list") {
      let listAgentMarkRepresentative1 = [];
      let listAgentMarkRepresentative2 = [];
      this.input.representative_list.forEach((item: any, index: any) => {
        if (item.representative_type_code == "REPRESENTATIVE") {
          listAgentMarkRepresentative1.push(index);
        } else {
          listAgentMarkRepresentative2.push(index);
        }
      });
      this.input.listAgentMarkRepresentative1 = clone(
        listAgentMarkRepresentative1
      );
      this.input.listAgentMarkRepresentative2 = clone(
        listAgentMarkRepresentative2
      );

      if (!this.input.payer_name) {
        this.input.payer_name = this.input.people_list[0].name;
      }
    }
    if (condition === "clone_receiver_representative_list") {
      let listReceiverAgentMarkRepresentative1 = [];
      let listReceiverAgentMarkRepresentative2 = [];
      this.input.receiver_representative_list.forEach(
        (item: any, index: any) => {
          if (item.representative_type_code == "REPRESENTATIVE") {
            listReceiverAgentMarkRepresentative1.push(index);
          } else {
            listReceiverAgentMarkRepresentative2.push(index);
          }
        }
      );
      this.input.listReceiverAgentMarkRepresentative1 = clone(
        listReceiverAgentMarkRepresentative1
      );
      this.input.listReceiverAgentMarkRepresentative2 = clone(
        listReceiverAgentMarkRepresentative2
      );
    }
  }
  manageWizard(wizard: any): void {
    let wizardList = wizard ? wizard.split("|") : [1, 1, 1, 1];
    this.currentID = +wizardList[0];
    this.currentStep = +wizardList[1];
    this.currentSubID = +wizardList[2];
    this.currentSubStep = +wizardList[3];
    this.calcProgressPercent(this.currentStep);

    if (this.currentID >= 4) {
      this.conditionWizard("clone_representative_list");
    }
    if (this.currentID >= 5) {
      if (this.input.save040_contact_type_code !== "OTHERS") {
        if (this.input.save040_contact_type_code === "PEOPLE") {
          this.input.people_list.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.contact_type_index = index;
            }
          });
        } else {
          this.input.representative_list.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.contact_type_index = index;
            }
          });
        }
      }
    }
    if (this.currentID >= 6) {
      this.conditionWizard("clone_receiver_representative_list");
    }
    if (this.currentID >= 7) {
      if (this.input.save040_receiver_contact_type_code !== "OTHERS") {
        if (this.input.save040_receiver_contact_type_code === "RECEIVER") {
          this.input.receiver_people_list.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.receiver_contact_type_index = index;
            }
          });
        } else {
          this.input.receiver_representative_list.forEach(
            (item: any, index: any) => {
              if (item.is_contact_person) {
                this.input.receiver_contact_type_index = index;
              }
            }
          );
        }
      }
    }

    this.menuList.forEach((item: any) => {
      if (this.currentID > item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            sub.canEdit = true;
          });
        }
      }
      if (this.currentID === item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (this.currentSubID >= sub.id) {
              sub.canEdit = true;
            }
          });
        }
      }
    });
  }

  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }

  onClickCheckBoxTable1(object: any, name: any, value: boolean): void {
    console.log("cccccccc",this.input.product_list)
  }

  onClickCheckBoxTable2(object: any, value: boolean): void {
    //if (isAll) {
    //console.log(value)
    this[object].forEach((item: any) => {
      item.is_check = value;
      //console.log(item)
    });
    //} else {
    //  object.forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } })
    //}
  }

  // UX/UI function
  onChangeTrademarkOwnerType(trademarkOwnerType: string): void {
    for (let prop in this.whatTrademarkOwnerType) {
      this.whatTrademarkOwnerType[prop] = false;
    }

    switch (trademarkOwnerType) {
      case "isPerson":
        this.whatTrademarkOwnerType.person = true;
        break;
      case "isCorporation":
        this.whatTrademarkOwnerType.corporation = true;
        break;
      case "isForeign":
        this.whatTrademarkOwnerType.foreign = true;
        break;
      case "isForeignCorporation":
        this.whatTrademarkOwnerType.foreignCorporation = true;
        break;
      case "isGovernmentAgencies":
        this.whatTrademarkOwnerType.governmentAgencies = true;
        break;
      case "isOther":
        this.whatTrademarkOwnerType.other = true;
        break;
      default:
        break;
    }
  }

  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    console.log("validate", this.validateSaveItemModal(nameItem));
    if (this.validateSaveItemModal(nameItem)) {
      console.log("indexEdit", this.input.indexEdit);
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        console.log("people_list", this[obj][nameList]);
        this.toggleModal(nameModal);
      }

      alert(
        "กรณีที่ทีการแก้ไขเปลี่ยนแปลงข้อมูลเจ้าของ/ตัวแทน ให้ยื่น ก.06 แนบมาด้วย"
      );
    }
  }

  csv2Array(fileInput: any) {
    //read file from input

    console.log(">>>>>>>>>>>>>>>>>");
    this.fileReaded = fileInput.target.files[0];

    let reader: FileReader = new FileReader();
    reader.readAsText(this.fileReaded);

    reader.onload = (e) => {
      this.csv = reader.result;
      let allTextLines = this.csv.split(/\r|\n|\r/);
      let headers = allTextLines[0].split(",");
      this.productList = [];

      for (let i = 1; i < allTextLines.length; i++) {
        // split content based on comma
        let data = allTextLines[i].split(",");
        if (data.length === headers.length) {
          let tarr = [];
          for (let j = 0; j < headers.length; j++) {
            tarr.push(data[j]);
          }
          this.productList.push({
            is_check: false,
            cls: data[0],
            product: data[1],
          });
          // log each row to see output
          console.log(tarr);
          this.inputFileStep8 = true;
        }
      }
      // all rows in the csv file
      console.log(">>>>>>>>>>>>>>>>>", this.productList);
      let element: any;
      element = document.getElementById("fileInput") as HTMLElement;
      element.value = "";
      this.perpageP1 = 5;
      this.calOnchangepage();
    };
  }

  validateSaveItemModal(nameItem: any): boolean {
    if (nameItem === "ownerMarkItem") {
      if (this.input.ownerMarkItem.address_country_code != "TH") {
        this.validateObj = {
          receiver_type_code: this.input.ownerMarkItem.receiver_type_code,
          card_number: this.input.ownerMarkItem.card_number,
          name: this.input.ownerMarkItem.name,
          sex_code: this.input.ownerMarkItem.sex_code,
          nationality_code: this.input.ownerMarkItem.nationality_code,
          career_code: this.input.ownerMarkItem.career_code,
          address_country_code: this.input.ownerMarkItem.address_country_code,
          house_number: this.input.ownerMarkItem.house_number,
          address_sub_district_name: "not validate",
          postal_code: "not validate",
          email: this.input.ownerMarkItem.email,
          telephone: this.input.ownerMarkItem.telephone,
        };
      } else {
        this.validateObj = {
          receiver_type_code: this.input.ownerMarkItem.receiver_type_code,
          card_number: this.input.ownerMarkItem.card_number,
          name: this.input.ownerMarkItem.name,
          sex_code: this.input.ownerMarkItem.sex_code,
          nationality_code: this.input.ownerMarkItem.nationality_code,
          career_code: this.input.ownerMarkItem.career_code,
          address_country_code: this.input.ownerMarkItem.address_country_code,
          house_number: this.input.ownerMarkItem.house_number,
          address_sub_district_name: this.input.ownerMarkItem
            .address_sub_district_name,
          postal_code: this.input.ownerMarkItem.postal_code,
          email: this.input.ownerMarkItem.email,
          telephone: this.input.ownerMarkItem.telephone,
        };
      }
      let result = validateService(
        "validateEFormSave04ProcessStep5OwnerMarkItem",
        this.validateObj
      );
      this.validate = result.validate;
      return result.isValid;
    }
    if (nameItem === "agentMarkItem") {
      if (this.input.agentMarkItem.address_country_code != "TH") {
        this.validateObj = {
          receiver_type_codeAgent: this.input.agentMarkItem.receiver_type_code,
          card_numberAgent: this.input.agentMarkItem.card_number,
          nameAgent: this.input.agentMarkItem.name,
          sex_codeAgent: this.input.agentMarkItem.sex_code,
          nationality_codeAgent: this.input.agentMarkItem.nationality_code,
          career_codeAgent: this.input.agentMarkItem.career_code,
          address_country_codeAgent: this.input.agentMarkItem
            .address_country_code,
          house_numberAgent: this.input.agentMarkItem.house_number,
          address_sub_district_nameAgent: "not validate",
          postal_codeAgent: "not validate",
          emailAgent: this.input.agentMarkItem.email,
          telephoneAgent: this.input.agentMarkItem.telephone,
        };
      } else {
        this.validateObj = {
          receiver_type_codeAgent: this.input.agentMarkItem.receiver_type_code,
          card_numberAgent: this.input.agentMarkItem.card_number,
          nameAgent: this.input.agentMarkItem.name,
          sex_codeAgent: this.input.agentMarkItem.sex_code,
          nationality_codeAgent: this.input.agentMarkItem.nationality_code,
          career_codeAgent: this.input.agentMarkItem.career_code,
          address_country_codeAgent: this.input.agentMarkItem
            .address_country_code,
          house_numberAgent: this.input.agentMarkItem.house_number,
          address_sub_district_nameAgent: this.input.agentMarkItem
            .address_sub_district_name,
          postal_codeAgent: this.input.agentMarkItem.postal_code,
          emailAgent: this.input.agentMarkItem.email,
          telephoneAgent: this.input.agentMarkItem.telephone,
        };
      }

      let result = validateService(
        "validateEFormSave04ProcessStep5AgentMarkItem",
        this.validateObj
      );
      this.validate = result.validate;
      return result.isValid;
    }

    if (nameItem === "receiverMarkItem") {
      if (this.input.receiverMarkItem.address_country_code != "TH") {
        this.validateObj = {
          receiver_type_codeReceiver: this.input.receiverMarkItem
            .receiver_type_code,
          card_numberReceiver: this.input.receiverMarkItem.card_number,
          nameReceiver: this.input.receiverMarkItem.name,
          sex_codeReceiver: this.input.receiverMarkItem.sex_code,
          nationality_codeReceiver: this.input.receiverMarkItem
            .nationality_code,
          career_codeReceiver: this.input.receiverMarkItem.career_code,
          address_country_codeReceiver: this.input.receiverMarkItem
            .address_country_code,
          house_numberReceiver: this.input.receiverMarkItem.house_number,
          address_sub_district_nameReceiver: "not validate",
          postal_codeReceiver: "not validate",
          emailReceiver: this.input.receiverMarkItem.email,
          telephoneReceiver: this.input.receiverMarkItem.telephone,
        };
      } else {
        this.validateObj = {
          receiver_type_codeReceiver: this.input.receiverMarkItem
            .receiver_type_code,
          card_numberReceiver: this.input.receiverMarkItem.card_number,
          nameReceiver: this.input.receiverMarkItem.name,
          sex_codeReceiver: this.input.receiverMarkItem.sex_code,
          nationality_codeReceiver: this.input.receiverMarkItem
            .nationality_code,
          career_codeReceiver: this.input.receiverMarkItem.career_code,
          address_country_codeReceiver: this.input.receiverMarkItem
            .address_country_code,
          house_numberReceiver: this.input.receiverMarkItem.house_number,
          address_sub_district_nameReceiver: this.input.receiverMarkItem
            .address_sub_district_name,
          postal_codeReceiver: this.input.receiverMarkItem.postal_code,
          emailReceiver: this.input.receiverMarkItem.email,
          telephoneReceiver: this.input.receiverMarkItem.telephone,
        };
      }

      let result = validateService(
        "validateEFormSave04ProcessStep7receiverMarkItem",
        this.validateObj
      );
      this.validate = result.validate;
      return result.isValid;
    }

    if (nameItem === "agenReceiverMarkItem") {
      if (this.input.agenReceiverMarkItem.address_country_code != "TH") {
        this.validateObj = {
          receiver_type_codeAgenReceiver: this.input.agenReceiverMarkItem
            .receiver_type_code,
          card_numberAgenReceiver: this.input.agenReceiverMarkItem.card_number,
          nameAgenReceiver: this.input.agenReceiverMarkItem.name,
          sex_codeAgenReceiver: this.input.agenReceiverMarkItem.sex_code,
          nationality_codeAgenReceiver: this.input.agenReceiverMarkItem
            .nationality_code,
          career_codeAgenReceiver: this.input.agenReceiverMarkItem.career_code,
          address_country_codeAgenReceiver: this.input.agenReceiverMarkItem
            .address_country_code,
          house_numberAgenReceiver: this.input.agenReceiverMarkItem
            .house_number,
          address_sub_district_nameAgenReceiver: "not validate",
          postal_codeAgenReceiver: "not validate",
          emailAgenReceiver: this.input.agenReceiverMarkItem.email,
          telephoneAgenReceiver: this.input.agenReceiverMarkItem.telephone,
        };
      } else {
        this.validateObj = {
          receiver_type_codeAgenReceiver: this.input.agenReceiverMarkItem
            .receiver_type_code,
          card_numberAgenReceiver: this.input.agenReceiverMarkItem.card_number,
          nameAgenReceiver: this.input.agenReceiverMarkItem.name,
          sex_codeAgenReceiver: this.input.agenReceiverMarkItem.sex_code,
          nationality_codeAgenReceiver: this.input.agenReceiverMarkItem
            .nationality_code,
          career_codeAgenReceiver: this.input.agenReceiverMarkItem.career_code,
          address_country_codeAgenReceiver: this.input.agenReceiverMarkItem
            .address_country_code,
          house_numberAgenReceiver: this.input.agenReceiverMarkItem
            .house_number,
          address_sub_district_nameAgenReceiver: this.input.agenReceiverMarkItem
            .address_sub_district_name,
          postal_codeAgenReceiver: this.input.agenReceiverMarkItem.postal_code,
          emailAgenReceiver: this.input.agenReceiverMarkItem.email,
          telephoneAgenReceiver: this.input.agenReceiverMarkItem.telephone,
        };
      }

      let result = validateService(
        "validateEFormSave04ProcessStep7agenReceiverMarkItem",
        this.validateObj
      );
      this.validate = result.validate;
      return result.isValid;
    }

    return true;
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any, item: any): void {
    if (item) {
      this[obj][item][name] = value;
    } else {
      this[obj][name] = value;
    }
  }

  contactChange(contactType: string, index: number): void {
    console.log("index", index);
    if (contactType == "OWNER") {
      this.input.people_list.forEach((data) => {
        data.is_contact_person = false;
      });

      this.input.people_list[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE") {
      this.input.representative_list.forEach((data) => {
        data.is_contact_person = false;
      });

      this.input.representative_list[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE_PERIOD") {
      this.input.representative_list.forEach((data) => {
        data.is_contact_person = false;
      });

      this.input.representative_list[index].is_contact_person = true;
    }

    console.log("representative_list", this.input.representative_list);
  }

  receiverContactChange(contactType: string, index: number): void {
    console.log("index", index);
    if (contactType == "OWNER") {
      this.input.receiver_people_list.forEach((data) => {
        data.is_contact_person = false;
      });

      this.input.receiver_people_list[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE") {
      this.input.receiver_representative_list.forEach((data) => {
        data.is_contact_person = false;
      });

      this.input.receiver_representative_list[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE_PERIOD") {
      this.input.receiver_representative_list.forEach((data) => {
        data.is_contact_person = false;
      });

      this.input.receiver_representative_list[index].is_contact_person = true;
    }

    console.log("representative_list", this.input.receiver_representative_list);
  }

  backtomenu(canEdit: any, number: any): void {
    console.log(canEdit, number);
    if (canEdit == true) {
      this.currentStep = number;
      this.currentID = this.menuList[this.currentStep - 1].id;

      for (var i = 1; i <= this.menuList.length + 1; i++) {
        if (i <= this.stepPass - 1) {
          this.menuList[i].canEdit = true;
        }
      }
    }
  }

  doSomething(): void {
    alert("testtttt");
  }

  binding6(): void {
    this.Next6 = true;
  }

  onClickAddLanguage(): void {
    this.input.listTranslation.push({
      isEdit: false,
      language_code: "EN",
      read: "",
      mean: "",
    });
  }

  contactTypeChange(): void {
    this.Next4 = true;
    switch (this.input.save040_contact_type_code) {
      case "OWNER":
        this.listdropdownContact = this.input.people_list;
        this.ContactWord = "ใช้ที่อยู่ของเจ้าของลำดับที่";
        this.contactChangeType = "OWNER";
        break;
      case "REPRESENTATIVE":
        this.listdropdownContact = this.input.representative_list;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนลำดับที่";
        this.contactChangeType = "REPRESENTATIVE";
        break;
      case "REPRESENTATIVE_PERIOD":
        this.listdropdownContact = this.input.representative_list;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนช่วงลำดับที่";
        this.contactChangeType = "REPRESENTATIVE_PERIOD";
        break;
      default:
        break;
    }
  }

  receiverContactTypeChange(): void {
    this.Next4 = true;
    switch (this.input.save040_receiver_contact_type_code) {
      case "OWNER":
        this.listdropdownReceiverContact = this.input.receiver_people_list;
        this.ContactWord = "ใช้ที่อยู่ของเจ้าของลำดับที่";
        this.receiverContactChangeType = "OWNER";
        break;
      case "REPRESENTATIVE":
        this.listdropdownReceiverContact = this.input.receiver_representative_list;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนลำดับที่";
        this.receiverContactChangeType = "REPRESENTATIVE";
        break;
      case "REPRESENTATIVE_PERIOD":
        this.listdropdownReceiverContact = this.input.receiver_representative_list;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนช่วงลำดับที่";
        this.receiverContactChangeType = "REPRESENTATIVE_PERIOD";
        break;
      default:
        break;
    }
  }

  getLanguageLabel(code: any): string {
    let label = "";
    this.master.languageCodeList.forEach((item: any) => {
      if (item.code === code) {
        label = item.name;
      }
    });
    return label;
  }

  calcProgressPercent(currentStep: number): void {
    let lastItem = this.menuList[this.menuList.length - 1];
    let progressPercent = Math.round(
      ((currentStep + 1) / lastItem.number) * 100
    );
    this.progressPercent = progressPercent > 100 ? 100 : progressPercent;
  }

  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name];
  }
  toggleBooleanInTable(item: any, name: any): void {
    item[name] = !item[name];
  }

  // Modal Location
  autocompleteChangeListModalLocation(
    object: any,
    name: any,
    item_per_page: any = 5,
    length: any = 2
  ): void {
    if (object[name].length >= length) {
      console.log("d");
      this.is_autocomplete_ListModalLocation_show = true;

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page,
        },
      };

      this.callAutocompleteChangeListModalLocation(params);
    }
  }

  AddKor17List(): void {}

  openInput(): void {
    console.log(">>>>>>>");
    let element: any;
    element = document.getElementById("fileInput") as HTMLElement;
    element.click();
  }

  autocompleteBlurListModalLocation(
    object: any,
    name: any,
    item_per_page: any = 5
  ): void {
    let pThis = this;
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false;
    }, 200);
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return;
    this.input.is_autocomplete_ListModalLocation_load = true;
    let pThis = this;
    // this.eFormSaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false;
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code;
    this.inputModalAddress.address_sub_district_name = data.name;
    this.inputModalAddress.address_district_code = data.district_code;
    this.inputModalAddress.address_district_name = data.district_name;
    this.inputModalAddress.address_province_code = data.province_code;
    this.inputModalAddress.address_province_name = data.province_name;
    this.inputModalAddress.address_country_code = data.country_code;
    this.inputModalAddress.address_country_name = data.country_name;
    this.is_autocomplete_ListModalLocation_show = false;
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item];
    });
    this.toggleModal("isModalPeopleEditOpen");
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string): void {
    this.validate[name] = null;
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Event >>>
  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    this.currentP1 = page;
    this.calOnchangepage();
  }
  onChangePerPage(value: number): void {
    this.perpageP1 = value;
    this.calOnchangepage();
  }
  calOnchangepage() {
    this.totalP1 = Math.ceil(this.productList.length / this.perpageP1);
  }

  onChangePageMain(page: any, name: string): void {
    this.currentMain = page;
    this.calOnchangepageMain();
  }
  onChangePerPageMain(value: number, name: string): void {
    this.perpageMain = value;
    this.calOnchangepageMain();
  }
  calOnchangepageMain() {
    this.totalMain = Math.ceil(
      this.input.product_list.length / this.perpageMain
    );
  }

  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT;
    paginate.totalItems = total;
    this[name] = paginate;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value);
    }

    if (object) {
      object[name] = value;
    } else {
      this.input[name] = value;
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => {
      item.is_check = value;
    });
  }

  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(name: any, value: any, obj: any): void {
    clearTimeout(this.timeout);
    if (name === "address_sub_district_name") {
      this.validate[name] = null;
      this.clearValue(name, obj);
    }

    this.timeout = setTimeout(() => {
      if (value) {
        if (name === "address_sub_district_name") {
          this.callSubDistrict(
            {
              filter: { name: value },
              paging: {
                item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page,
              },
            },
            name
          );
        }
      } else {
        this.onClickOutsideAutocomplete(name);
      }
    }, CONSTANTS.DELAY_CALL_API);
  }
  onSelectAutocomplete(name: any, item: any, obj: any): void {
    if (name === "address_sub_district_name") {
      // ตำบล
      this.input[obj].address_sub_district_code = item.code;
      this.input[obj].address_sub_district_name = item.name;
      // อำเภอ
      this.input[obj].address_district_code = item.district_code;
      this.input[obj].address_district_name = item.district_name;
      // จังหวัด
      this.input[obj].address_province_code = item.province_code;
      this.input[obj].address_province_name = item.province_name;
      // รหัสไปรษณีย์
      this.input[obj].postal_code = item.postal_code;
    }
    this.onClickOutsideAutocomplete(name);
  }
  onClickOutsideAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = false;
    this.autocompleteList[name] = [];
  }
  clearValue(name: any, obj: any): void {
    if (name === "address_sub_district_name") {
      // อำเภอ
      this.input[obj].address_district_code = "";
      this.input[obj].address_district_name = "";
      // จังหวัด
      this.input[obj].address_province_code = "";
      this.input[obj].address_province_name = "";
      // รหัสไปรษณีย์
      this.input[obj].postal_code = "";
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (
          name === "isModalOwnerFormOpen" ||
          name === "isModalReceiverFormOpen"
        ) {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (
          name === "isModalAgentFormOpen" ||
          name === "isModalAgentReceiverFormOpen"
        ) {
          this.input.agentMarkItem = clone(this.input[point][index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
    }
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
    }

    if (nameList === "people_list" || nameList === "representative_list") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }

  //! <<< Other >>>
  displayAddress(value: any): any {
    return displayAddress(value);
  }
  onKey(e: any, name: any): void {
    if (e.keyCode === 13) {
      if (name === "request_number") {
        this.searchRequestNumber();
      }
    } else {
      if (name === "request_number") {
        this.input.registration_number = "";
        this.input.people_list = [];
        this.input.representative_list = [];
        this.input.product_list = [];
      }
    }
  }
}
