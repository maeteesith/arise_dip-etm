import { Component, OnInit } from '@angular/core';
import { ForkJoinService } from '../../services/fork-join.service'

@Component({
  selector: 'app-appeal-role02-register-consider-board',
  templateUrl: './appeal-role02-register-consider-board.component.html',
  styleUrls: ['./appeal-role02-register-consider-board.component.scss']
})
export class AppealRole02RegisterConsiderBoardComponent implements OnInit {

  constructor(
    private forkJoinService: ForkJoinService,
  ) { }

  public tab_search: any
  public tab_show: any
  public appealBoardSolutionList: any []
  public tab_input: any

  ngOnInit() {
    this.tab_input = {
      similar: 'CONFIRM',
      oppose: 'CONFIRM',
      revoke: 'CONFIRM'
    }
    this.tab_search = true
    this.tab_show = false
    this.callInit()
  }

  toggle_show(){
    switch(this.tab_search){
      case true: 
        this.tab_search = false; this.tab_show = true
      break;
      case false:
        this.tab_search = true; this.tab_show = false
      break;
    }
  }

  callInit(): void{
    this.forkJoinService.initAppealReason().subscribe((data: any) => {
      if (data) {
        this.appealBoardSolutionList = data.appealBoardSolutionList.map( Item => {
          return Item
        })
      }     
    })
  }

}
