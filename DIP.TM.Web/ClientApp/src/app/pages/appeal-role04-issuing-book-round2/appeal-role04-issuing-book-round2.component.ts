import { Component, OnInit } from '@angular/core';
const dummy_data = [
  { 
    book: 'ตค.7(3)', order_date: '10/10/2560', book_r1: '10/10/2560', book_r2: '', repond: 'ตีกลับ', 
    accept_date: '', due_date: '10/10/2560', status: 'รอดำเนินการ'
  },
  { 
    book: 'ตค.9(10)', order_date: '10/10/2560', book_r1: '10/10/2560', book_r2: '10/10/2560', repond: 'ตีกลับ', 
    accept_date: '10/10/2560', due_date: '10/10/2560', status: 'เสร็จสิ้น'
  },
];

@Component({
  selector: 'app-appeal-role04-issuing-book-round2',
  templateUrl: './appeal-role04-issuing-book-round2.component.html',
  styleUrls: ['./appeal-role04-issuing-book-round2.component.scss']
})
export class AppealRole04IssuingBookRound2Component implements OnInit {

  constructor() { }

  public modal: any
  public dummyList: any[]

  ngOnInit() {
    this.modal = {
      isModalAddReason: false,
      isModalSection: false,
      isModalK08Detail: false
    }

    this.dummyList = dummy_data.map( dummyItem => {
      return dummyItem
    })
  }


  toggleModal(name: string): void {
    if (!this.modal[name]) {
      this.modal[name] = true
    } else {
      this.modal[name] = false
      switch(name){
      }
    }
  }

}
