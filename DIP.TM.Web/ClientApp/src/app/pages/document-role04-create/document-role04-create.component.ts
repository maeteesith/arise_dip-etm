import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { UploadService } from "../../services/upload.service"
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-document-role04-create",
    templateUrl: "./document-role04-create.component.html",
    styleUrls: ["./document-role04-create.component.scss"]
})
export class DocumentRole04CreateComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any

    public validate: any
    public master: any
    // Response
    public response: any

    public input_create: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    //public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    public rowEdit: any

    //label_save_combobox || label_save_radio
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial

    public data: any

    public popup: any

    public tab_menu_show_index: any
    public tab_menu_file_show_index: any

    public tableInstructionRule: any
    public tableSaveDocument: any
    public tablePeople: any
    public tableRepresentative: any
    public contactAddress: any
    public tableProduct: any
    public tableReceiptItem: any
    public tableHistory: any
    public tableDocumentScan: any

    public tableDocumentRole04Create: any

    public tableInstructionRuleFile: any

    public documentRole04Create: any

    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
        }

        this.data = {}

        this.popup = {
            instruction_rule_select: {},
            instruction_rule_file: {},
        }

        //Master List
        this.master = {
        }
        //Master List

        this.documentRole04Create = {}


        this.tab_menu_show_index = 1
        this.tab_menu_file_show_index = 1

        this.tableInstructionRule = {
            column_list: {
                index: "#",
                instruction_rule_name: "คำสั่ง",
                instruction_date: "วันที่สั่ง",
                created_by_name: "นายทะเบียน",
                considering_book_status_name: "สถานะหนังสือ",
                instruction_send_date: "วันที่ส่งคำสั่ง",
                book_number: "เลข พณ.",
                document_role02_receiver_by_name: "ผู้รับงาน",
                document_role02_receive_remark: "หมายเหตุ",
                considering_instruction_rule_status_name: "สถานะ",
            },
        }
        this.tableSaveDocument = {
            column_list: {
                index: "#",
                request_type_name: "เอกสาร",
                make_date: "วันที่รับ",
                consider_similar_document_status_name: "พิจารณา",
                consider_similar_document_date: "วันที่พิจารณา",
                //instruction_rule_name: "เอกสารสแกน",
                consider_similar_document_remark: "รายละเอียด",
                //consider_similar_document_remark: "เหตุผล",
            },
        }
        this.tablePeople = {
            column_list: {
                index: "#",
                name: "ชื่อเจ้าของ",
                address_information: "ที่อยู่",
                telephone: "โทรศัพท์",
                fax: "โทรสาร",
            },
        }
        this.tableRepresentative = {
            column_list: {
                index: "#",
                name: "ชื่อตัวแทน",
                address_information: "ที่อยู่",
                telephone: "โทรศัพท์",
                fax: "โทรสาร",
            },
            object_name: "ตัวแทน",
            //is_address_edit: true,
        }
        this.tableProduct = {
            column_list: {
                index: "#",
                request_item_sub_type_1_code: "จำพวกสินค้า",
                description: "รายการสินค้า/บริการ",
                //count: "จำนวน",
            },
        }
        this.tableReceiptItem = {
            column_list: {
                index: "#",
                request_type_name: "รายการ",
                receipt_number: "เลขที่ใบเสร็จ",
                receive_date: "วันที่รับเงิน",
                total_price: "จำนวนเงิน",
                receiver_name: "ผู้รับเงิน",
                receipt_status_name: "สถานะ",
            },
        }
        this.tableHistory = {
            column_list: {
                index: "#",
                make_date: "วันที่",
                created_by_name: "ผู้ออกคำสั่ง",
                description: "รายการ",
            },
        }
        this.tableDocumentScan = {
            column_list: {
                index: "#",
                request_document_collect_type_name: "ชื่อเอกสาร",
                updated_by_name: "เจ้าหน้าที่",
                created_date: "วันที่แนบ",
            },
            //has_link: ["'/File/Content/' + row_item.file_id"],
        }

        this.tableDocumentRole04Create = {
            column_list: {
                index: "#",
                instruction_rule_name: "หนังสือ",
                make_date: "วันที่สั่ง",
                book_start_date: "ออกหนังสือรอบ 1",
                post_round_action_post_type_name: "การโต้ตอบ",
                book_acknowledge_date: "วันที่ตอบโต้",
                book_end_date: "ครบกำหนด",
                document_role04_send_type_name: "เรื่อง",
                document_role04_receive_status_name: "สถานะ",
            },
            //command: [{
            //  name: "document_role04_print",
            //  title: "พิมพ์เอกสารที่เลือก",
            //  icon: "printer",
            //}],
            command_item: [{
                name: "document_role04_edit",
                icon: "edit-pen",
            }],
        }

        this.tableInstructionRuleFile = {
            column_list: {
                index: "#",
                file_name: "ชื่อเอกสาร",
                file_created_by_name: "เจ้าหน้าที่",
                file_created_date: "วันที่แนบ",
                remark: "รายละเอียดเพิ่มเติม",
            },
            //command: [{
            //  name: "document_role04_print",
            //  title: "พิมพ์เอกสารที่เลือก",
            //  icon: "printer",
            //}],
            can_deleted: 'instruction_file_delete',
            command_item: [{
                name: "instruction_file_download",
                icon: "file-alt",
            }],
        }

        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //


        this.contactAddress = {
            address_type_code: "OWNER"
        }
        this.modalAddress = {}

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.route.queryParams.subscribe((param_url: any) => {
        })

        this.forkJoinService.initDocumentRole04Create().subscribe((data: any) => {
            if (data) {
                this.help.Clone(data, this.master)
                this.data.document_role04_create_type_code = this.master.documentRole04CreateTypeCodeList[0].code
                //this.popup.instruction_rule_select = this.master.postRoundInstructionRuleCodeList[0]
                //this.master.postRoundInstructionRuleCodeList = this.master.postRoundInstructionRuleCodeList.filter(r => r.code.startsWith("CASE_") || r.code.startsWith("RULE_") || r.code.startsWith("ROLE_"))

                ////popup.instruction_rule_file.post_round_instruction_rule_file_type_code
                //this.popup.instruction_rule_select = this.master.postRoundInstructionRuleCodeList[0]
                //this.popup.instruction_rule_file = {
                //  post_round_instruction_rule_file_type_code: this.master.postRoundInstructionRuleFileTypeCodeList[0].code,
                //}

            }
            if (this.editID) {
                this.DocumentProcessService.DocumentRole04CreateLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }

            //this.popup.is_instruction_rule_select = true
        })
    }

    constructor(
        private auth: Auth,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService,
        private uploadService: UploadService,
    ) { }

    onClickDocumentRole04CreateSave(): void {
        this.help.Clone(this.popup.rowEdit, this.rowEdit)
        this.rowEdit.document_role04_send_type_name = this.master.documentRole04SendTypeCodeList.filter(r => r.code == this.popup.rowEdit.document_role04_send_type_code)[0].name

        this.popup.is_edit_show = false
    }

    onClickDocumentRole04CreateItemDocumentScan(): void {
        var param = {
            document_role04_create_id: this.editID.toString(),
        }
        this.global.setLoading(true)
        this.popup.is_instruction_rule_file = true
        this.DocumentProcessService.List("DocumentRole04CreateFile", this.help.GetFilterParams(param)).subscribe((data: any) => {
            // if(isValidDocumentRole04CreateSendResponse(res)) {
            if (data) {
                this.popup.instruction_rule_file.list = data.list
                this.updateTableInstructionRuleFile()

                this.global.setLoading(false)
            }
        })
    }

    updateTableInstructionRuleFile() {
        this.tableInstructionRuleFile.Set(this.popup.instruction_rule_file.list)
    }

    onClickDocumentRole04CreateInstructionRuleSave(): void {
        //this.popup.rowEdit.new_instruction_rule_code = this.popup.instruction_rule_select.code
        //this.popup.rowEdit.new_instruction_rule_name = this.popup.instruction_rule_select.name
        //this.popup.rowEdit.new_instruction_rule_description = this.popup.instruction_rule_select.description
        this.data.value_01 = this.popup.instruction_rule_select.description

        this.popup.is_instruction_rule_select = false
    }

    onClickDocumentRole04CreateSend(paging = null): void {
        console.log(paging)
        // if(this.validateDocumentRole04CreateSend()) {
        // Call api

        //this.data.
        //this.data.document_role04_create_list[0].document_role04_create_check_list = this.data.document_role04_create_list[0].document_role04_create_check_list||""
        console.log(this.documentRole04Create)
        this.data.document_role04_create_check_list = Object.keys(this.documentRole04Create).join("|")
        this.callDocumentRole04CreateSend(this.data)
        // }
    }
    //! <<< Call API >>>
    callDocumentRole04CreateSend(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.DocumentRole04CreateSend([params]).subscribe((data: any) => {
            // if(isValidDocumentRole04CreateSendResponse(res)) {
            if (data) {

                this.popup.isPopupSendOpen = true
                //close()
                //// Set value
                ////this.tableList.SetPaging(data)
                //this.tableList.SetDataList(data)
                ////this.listData(data)
                //this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }


    onClicDocumentRole04ReleaseReasonSave(): void {
        //console.log(this.tableReleaseCase.Get())
        //var relesae_case_list = this.tableReleaseCase.Get().filter(r => r.is_check)

        //this.data.value_02 = this.data.value_02 || ""
        //this.data.value_02 = this.data.value_02.trim()
        //this.data.value_02 += "\n" + relesae_case_list.map(r => r.description).join("\n")
        //this.data.value_02 = this.data.value_02.trim()

        //relesae_case_list.forEach((item: any) => {
        //    item.is_check = false
        //})

        //this.popup.is_reason_select = false

    }

    onClickUpload(event: any, obj: any): void {
        if (event) {
            let file = event.target.files[0]
            event.target.value = ""

            let reader = new FileReader()
            reader.onload = e => {
                this.popup.instruction_rule_file.blob = reader.result
                this.popup.instruction_rule_file.file_name = file.name
            }
            reader.readAsDataURL(file)
        }
    }

    onClickDocumentRole04CreateInstructionRuleFileAdd(): void {
        if (this.popup.instruction_rule_file && this.popup.instruction_rule_file.blob) {
            this.global.setLoading(true)

            //  var image_upload_list = this.ImageListEdit.filter(r => !r.file_id && !r.is_deleted)
            //  if (image_upload_list.length > 0) {
            this.callUpload(this.popup.instruction_rule_file)
        }
    }
    callUpload(file): void {
        let params = {
            file: this.dataURItoBlob(file.blob)
        }

        const formData = new FormData()
        formData.append("file", params.file, file.file_name)

        this.global.setLoading(false)
        this.uploadService.upload(formData).subscribe((data: any) => {
            if (data) {
                var param = {
                    document_role04_create_id: +this.editID,
                    file_id: data.id,
                    //post_round_instruction_rule_file_type_code: this.popup.instruction_rule_file.post_round_instruction_rule_file_type_code,
                    remark: this.popup.instruction_rule_file.remark,
                }
                this.DocumentProcessService.DocumentRole04CreateFileAdd(param).subscribe((data: any) => {
                    if (data) {
                        this.popup.instruction_rule_file.list.push(data)
                        this.updateTableInstructionRuleFile()

                        this.popup.instruction_rule_file.remark = ""
                        //  this.popup.is_upload_show = false
                    }
                    this.global.setLoading(false)
                })
            }
        })
    }
    dataURItoBlob(dataURI: any): any {
        var byteString = atob(dataURI.split(",")[1])

        var mimeString = dataURI
            .split(",")[0]
            .split(":")[1]
            .split("")[0]

        var ab = new ArrayBuffer(byteString.length)
        var ia = new Uint8Array(ab)
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i)
        }

        var bb = new Blob([ab], { type: mimeString })
        return bb
    }

    //onClickPublicRole02ActionPostImageSave(): void {
    //  this.global.setLoading(true)

    //  var image_upload_list = this.ImageListEdit.filter(r => !r.file_id && !r.is_deleted)
    //  if (image_upload_list.length > 0) {
    //    this.callUpload(image_upload_list[0])
    //  } else {
    //    this.PublicProcessService.PostRoundActionImageSave(this.ImageListEdit).subscribe((data: any) => {
    //      //  //this.global.setLoading(false)
    //      //  //this.modal["isModalImageEditOpen"] = false

    //      if (data) {
    //        this.popup.is_upload_show = false
    //        //  this.ImageListEdit = data.list
    //        //  //  item.file_id = data.id
    //        //  //  this.onClickPublicRole02ActionPostImageSave()
    //        //  //  //    this.callRequestDocumentCollectItemSave(this.rowImageEdit)
    //      }
    //      this.global.setLoading(false)
    //    })
    //  }
    //}

    onClickCommand($event): void {
        console.log($event)

        //if ($event.command.name) {
        //  if ($event.command.name == "document_role04_print") {
        //    //window.open("pdf/TTT/" + $event.command.opject_list.)
        //  }
        //} else {
        if ($event.command == "document_role04_edit") {
            //    this.popup.is_edit_show = true

            //    this.rowEdit = $event.object_list[0]

            //    this.popup.rowEdit = this.popup.rowEdit || {}
            //    this.help.Clone($event.object_list[0], this.popup.rowEdit)
            //    this.popup.rowEdit.document_role04_send_type_code = this.popup.rowEdit.document_role04_send_type_code || "ROUND_2"
            //    this.popup.rowEdit.new_instruction_rule_code = this.popup.rowEdit.new_instruction_rule_code || this.popup.rowEdit.instruction_rule_code
            //    this.popup.rowEdit.new_instruction_rule_name = this.popup.rowEdit.new_instruction_rule_name || this.popup.rowEdit.instruction_rule_name
            //    this.popup.rowEdit.new_instruction_rule_description = this.popup.rowEdit.new_instruction_rule_description || this.popup.rowEdit.instruction_rule_description
        } else if ($event.command == "instruction_file_download") {
            window.open("file/Content/" + $event.object_list[0].file_id)
        } else if ($event.command == "instruction_file_delete") {
            this.global.setLoading(true)
            var param = $event.object_list.map((item: any) => { return item.id })
            this.DocumentProcessService.DocumentRole04CreateFileDelete(param).subscribe((data: any) => {
                if (data) {

                    $event.object_list.forEach((item: any) => {
                        this.popup.instruction_rule_file.list.splice(this.popup.instruction_rule_file.list.indexOf(item), 1)
                    })

                    this.updateTableInstructionRuleFile()

                    //this.tableInstructionRuleFile.Add(data)

                    //this.popup.instruction_rule_file.remark = ""
                    //  this.popup.is_upload_show = false
                }
                this.global.setLoading(false)
            })
        }
    }


    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.data = data.document_role04_create

        this.input = data.view
        this.input_create = data.document_role04_create

        this.documentRole04Create = {}
        this.data.document_role04_create_check_list = this.data.document_role04_create_check_list || ""
        this.data.document_role04_create_check_list.split("|").forEach((item: any) => {
            if (item != "") this.documentRole04Create[item] = true
        })

        //if (!this.data.instruction_rule_code) {
        //  var instruction_rule_default = this.master.postRoundInstructionRuleCodeList[0]
        //  this.data.instruction_rule_code = instruction_rule_default.code
        //  this.data.instruction_rule_name = instruction_rule_default.name
        //  this.data.instruction_rule_description = instruction_rule_default.description
        //}

        //this.data.document_role04_create_list[0].document_role04_create_check_list = this.DocumentRole04Create.filter(r => r === true).join("|")


        this.tableInstructionRule.Set(data.instruction_rule_list)
        this.tableSaveDocument.Set(data.document_list)

        this.tablePeople.Set(data.view.people_list)
        this.tableRepresentative.Set(data.view.representative_list)

        this.help.Clone(data.view.contact_address, this.contactAddress)
        this.tableProduct.Set(data.view.product_list)
        this.tableReceiptItem.Set(data.receipt_item_list)
        this.tableHistory.Set(data.history_list)
        this.tableDocumentScan.Set(data.document_scan_list)

        //this.tableDocumentRole04Create.Set(data.document_role04_create_list)
    }

    listData(data: any): void {

    }

    saveData(): any {
        let params = this.input


        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+ page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }
    displayMoney(value: any): any {
        return displayMoney(value);
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        //if (name.indexOf("price") >= 0) {
        //  value = parseFloat(value)
        //}

        console.log(name)
        console.log(object)
        console.log(value)

        if (object) {
            if (value) object[name] = value
            else delete object[name]
        } else {
            this.input[name] = value
        }

        console.log(this.documentRole04Create)
        console.log(value)
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

}
