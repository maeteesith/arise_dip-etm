import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  clone,
  displayDateServer
} from '../../helpers'

@Component({
  selector: "app-public-role02-print-document-list",
  templateUrl: "./public-role02-print-document-list.component.html",
  styleUrls: ["./public-role02-print-document-list.component.scss"]
})
export class PublicRole02PrintDocumentListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole02PrintDocument
  public listPublicRole02PrintDocument: any[]
  public paginatePublicRole02PrintDocument: any
  public perPagePublicRole02PrintDocument: number[]
  // Response PublicRole02PrintDocument
  public responsePublicRole02PrintDocument: any
  public listPublicRole02PrintDocumentEdit: any

  public itemEdit: any

  public player: any
  public player_action: any

  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public departmentCodeList: any[]
  public publicRole02PrintDocumentStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public tablePublicRole02: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
      //isModalPreviewOpen: true
    }

    this.input = {
      id: null,
      //public_role02_receive_start_date: getMoment(),
      //public_role02_receive_end_date: getMoment(),
      department_code: '',
      rule_count: '',
      document_role02_print_document_status_code: '',
      request_number: '',
    }
    this.listPublicRole02PrintDocument = []
    this.paginatePublicRole02PrintDocument = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole02PrintDocument.id = 'paginatePublicRole02PrintDocument'
    this.perPagePublicRole02PrintDocument = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    this.itemEdit = {}

    //Master List
    this.master = {
      departmentCodeList: [],
      publicRole02PrintDocumentStatusCodeList: [],
    }
    //Master List


    this.tablePublicRole02 = {
      column_list: {
        index: "#",
        instruction_rule_name: "ประเภทหนังสือ",
        instruction_send_date: "วันที่มีคำสั่ง",
        document_role02_check_date: "วันที่ส่งมา",
        send_address_count: "จำนวนที่อยู่",
        document_role02_print_document_date: "วันที่พิมพ์",
        document_role02_print_document_status_name: "สถานะ",
      },
      button_list: [{
        name: "report",
        title: "พิมพ์ทั้งหมด",
        icon: "printer",
      }],
    }


    //
    //this.autocompleteListListModalLocation = []
    //this.is_autocomplete_ListModalLocation_show = false
    //this.is_autocomplete_ListModalLocation_load = false
    ////this.modal = { isModalPeopleEditOpen: false, }
    //this.inputAddress = {}
    //this.inputModalAddress = {}
    ////

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole02PrintDocumentList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.departmentCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.department_code = "ALL"
        this.master.documentRole02PrintDocumentStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.document_role02_print_document_status_code = "ALL"

      }
      if (this.editID) {
        this.PublicProcessService.PublicRole02PrintDocumentLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

      this.onClickPublicRole02PrintDocumentList()
    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService,
    private PublicProcessService: PublicProcessService,
  ) { }

  onClickPublicRole02PrintDocumentList(): void {
    // if(this.validatePublicRole02PrintDocumentList()) {
    // Open loading
    // Call api
    this.callPublicRole02PrintDocumentList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole02PrintDocumentList(params: any): void {
    this.global.setLoading(true)
    var param = this.help.GetFilterParams(params, this.paginatePublicRole02PrintDocument)
    this.DocumentProcessService.DocumentRole02PrintDocumentList(param).subscribe((data: any) => {
      // if(isValidPublicRole02PrintDocumentListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }

  onClickPublicRole02PrintDocumentSend(): void {
    // if(this.validatePublicRole02PrintDocumentSend()) {
    // Open loading
    // Call api
    this.callPublicRole02PrintDocumentSend(this.listPublicRole02PrintDocument)
    // }
  }
  //! <<< Call API >>>
  callPublicRole02PrintDocumentSend(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintDocumentSend(params).subscribe((data: any) => {
      // if(isValidPublicRole02PrintDocumentSendResponse(res)) {
      if (data) {
        // Set value
        //this.listData(data)
        //this.automateTest.test(this, { list: data.list })
        this.onClickPublicRole02PrintDocumentList()

        window.open("pdf/TorKor/" + params.filter(r => r.is_check).map(r => r.id).join("|"))
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }


  onClickPublicRole02PrintDocumentAdd(): void {
    this.listPublicRole02PrintDocument.push({
      index: this.listPublicRole02PrintDocument.length + 1,
      request_number: null,
      document_role02_receiver_by_name: null,
      rule_count: null,
      name: null,
      house_number: null,
      document_role02_print_document_date_text: null,
      public_role02_receive_remark: null,
      document_role02_print_document_status_name: null,

    })
    this.changePaginateTotal(this.listPublicRole02PrintDocument.length, 'paginatePublicRole02PrintDocument')
  }

  onClickPublicRole02PrintDocumentEdit(item: any): void {
    console.log(item)

    this.modal.isModalPreviewOpen = true
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintDocumentItemLoad(item.id).subscribe((data: any) => {
      if (data) {

        this.itemEdit = data
        console.log(data.document_role_02_list)
        this.tablePublicRole02.Set(data.document_role_02_list)
        console.log(this.itemEdit)
        // Set value
      }
      this.global.setLoading(false)
    })
  }


  onClickPublicRole02PrintDocumentDelete(item: any): void {
    // if(this.validatePublicRole02PrintDocumentDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listPublicRole02PrintDocument.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listPublicRole02PrintDocument.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listPublicRole02PrintDocument.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listPublicRole02PrintDocument.length; i++) {
          if (this.listPublicRole02PrintDocument[i].is_check) {
            this.listPublicRole02PrintDocument[i].cancel_reason = rs
            this.listPublicRole02PrintDocument[i].status_code = "DELETE"
            this.listPublicRole02PrintDocument[i].is_deleted = true

            if (true && this.listPublicRole02PrintDocument[i].id) ids.push(this.listPublicRole02PrintDocument[i].id)
            // else this.listPublicRole02PrintDocument.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callPublicRole02PrintDocumentDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listPublicRole02PrintDocument.length; i++) {
          this.listPublicRole02PrintDocument[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callPublicRole02PrintDocumentDelete(params: any, ids: any[]): void {
    //this.PublicProcessService.PublicRole02PrintDocumentDelete(params).subscribe((data: any) => {
    //  // if(isValidPublicRole02PrintDocumentDeleteResponse(res)) {

    //  ids.forEach((id: any) => {
    //    for (let i = 0; i < this.listPublicRole02PrintDocument.length; i++) {
    //      if (this.listPublicRole02PrintDocument[i].id == id) {
    //        this.listPublicRole02PrintDocument.splice(i--, 1);
    //      }
    //    }
    //  });

    //  for (let i = 0; i < this.listPublicRole02PrintDocument.length; i++) {
    //    this.listPublicRole02PrintDocument[i] = i + 1
    //  }

    //  this.onClickPublicRole02PrintDocumentList()
    //  // Close loading
    //  this.global.setLoading(false)
    //})
  }


  onClickCommand($event): void {
    console.log($event)

    if ($event.command == "report") {
      this.global.setLoading(true)
      var param = { id: this.itemEdit.document_role_02_list[0].id, is_check: true }
      this.DocumentProcessService.DocumentRole02PrintDocumentSend([param]).subscribe((data: any) => {
        if (data) {
          this.onClickPublicRole02PrintDocumentList()
          window.open("pdf/TorKor/" + this.itemEdit.document_role_02_list[0].id)
          //this.global.setLoading(false)
        }
      })
    }
    //}
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicRole02PrintDocument = data.documentrole02printdocument_list || []
    this.changePaginateTotal(this.listPublicRole02PrintDocument.length, 'paginatePublicRole02PrintDocument')

  }

  listData(data: any): void {
    this.listPublicRole02PrintDocument = data.list || []
    this.help.PageSet(data, this.paginatePublicRole02PrintDocument)

  }

  saveData(): any {
    // let params = this.input
    // params.documentrole02printdocument_list = this.listPublicRole02PrintDocument || []

    const params = {
      public_role02_receive_start_date: this.input.public_role02_receive_start_date,
      public_role02_receive_end_date: this.input.public_role02_receive_end_date,
      department_code: this.input.department_code,
      rule_count: this.input.rule_count,
      document_role02_print_document_status_code: this.input.public_role02_receive_end_date,
      request_number: this.input.request_number,
      //page_index: +this.paginatePublicRole02PrintDocument.currentPage,
      //item_per_page: +this.paginatePublicRole02PrintDocument.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'public_role02_receive_date',
      //  value: displayDateServer(this.input.public_role02_receive_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_role02_receive_date',
      //  value: displayDateServer(this.input.public_role02_receive_end_date),
      //  operation: 4
      //}, {
      //  key: 'department_code',
      //  value: this.input.department_code,
      //  operation: 0
      //}, {
      //  key: 'rule_count',
      //  value: this.input.rule_count,
      //  operation: 5
      //}, {
      //  key: 'document_role02_print_document_status_code',
      //  value: this.input.document_role02_print_document_status_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}]
    }

    return params
  }


  togglePlayer(item: any = null): void {
    if (!item) {
      item = this.input
    }

    if (!item.player) {
      item.player = new Audio(item.sound_file_physical_path)
      item.player_action = null
    }

    if (item.player_action) {
      item.player.pause()
    } else {
      item.player.play()
    }
    item.player_action = !item.player_action
  }


  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicRole02PrintDocument') {
      this.onClickPublicRole02PrintDocumentList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
