import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-document-process-change-list",
    templateUrl: "./document-process-change-list.component.html",
    styleUrls: ["./document-process-change-list.component.scss"]
})
export class DocumentProcessChangeListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    // List Classification
    public listClassification: any[]
    public paginateDocumentProcessChange: any
    public perPageClassification: number[]
    // Response Classification
    public responseClassification: any
    public listClassificationEdit: any


    // List DocumentProcess
    public listDocumentProcess: any[]
    public paginateDocumentProcess: any
    public perPageDocumentProcess: number[]
    // Response DocumentProcess
    public responseDocumentProcess: any

    //label_save_combobox || label_save_radio
    public documentClassificationVersionStatusList: any[]
    public requestItemTypeCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            request_number: '',
            document_classification_version_status_code: '',
            request_item_type_code: '',
            //created_start_date: getMoment(),
            //created_end_date: getMoment(),
            //check_start_date: getMoment(),
            //check_end_date: getMoment(),
        }
        this.listClassification = []
        this.paginateDocumentProcessChange = CONSTANTS.PAGINATION.INIT
        this.perPageClassification = CONSTANTS.PAGINATION.PER_PAGE

        this.listDocumentProcess = []

        //Master List
        this.master = {
            documentClassificationVersionStatusList: [],
            requestItemTypeCodeList: [],
        }
        //Master List



        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //


        this.contactAddress = {
            address_type_code: "OWNER"
        }
        this.modalAddress = {}

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.route.queryParams.subscribe((param_url: any) => {
        })

        this.forkJoinService.initDocumentProcessChangeList().subscribe((data: any) => {
            if (data) {
                this.help.Clone(data, this.master)
                this.master.documentClassificationVersionStatusList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.document_classification_version_status_code = ""
                //this.master.requestItemTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.request_item_type_code = ""

            }
            if (this.editID) {
                this.DocumentProcessService.DocumentProcessChangeLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }

        })
    }

    constructor(
        private auth: Auth,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }

    onClickDocumentProcessChangeList(): void {
        // if(this.validateDocumentProcessChangeList()) {
        // Open loading
        // Call api
        var param = {
            request_number: this.input.request_number,
            document_classification_version_status_code: this.input.document_classification_version_status_code,
            request_item_type_code: this.input.request_item_type_code,
            created_start_date: this.input.created_start_date,
            created_end_date: this.input.created_end_date,
            check_start_date: this.input.check_start_date,
            check_end_date: this.input.check_end_date,

        }

        this.callDocumentProcessChangeList(param)
        // }
    }
    //! <<< Call API >>>
    callDocumentProcessChangeList(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.DocumentProcessChangeList(this.help.GetFilterParams(params, this.paginateDocumentProcessChange)).subscribe((data: any) => {
            // if(isValidDocumentProcessChangeListResponse(res)) {
            if (data) {
                // Set value
                this.listData(data)
                this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }

    onClickDocumentProcessChangeClassificationAdd(): void {
        this.listClassification.push({
            index: this.listClassification.length + 1,
            request_date_text: null,
            created_start_text: null,
            people_name: null,
            request_item_sub_type_1_code_text: null,
            check_date_text: null,
            document_classification_version_remark: null,
            document_classification_version_status_name: null,

        })
        this.changePaginateTotal(this.listClassification.length, 'paginateDocumentProcessChange')
    }
    onClickClassificationMadridStatus(id: any): void { }
    onClickClassificationId(id: any): void {
        window.open("document-process-classification-do/edit/" + id)
    }

    // Modal Location
    autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
        if (object[name].length >= length) {
            this.is_autocomplete_ListModalLocation_show = true

            let params = {
                name: object[name],
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListModalLocation(params)
        }
    }
    autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_ListModalLocation_show = false
        }, 200)
    }
    callAutocompleteChangeListModalLocation(params: any): void {
        if (this.input.is_autocomplete_ListModalLocation_load) return
        this.input.is_autocomplete_ListModalLocation_load = true
        let pThis = this
        // this.DocumentProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
        //   if (data) {
        //     if (data.length == 1) {
        //       setTimeout(function () {
        //         pThis.autocompleteChooseListModalLocation(data[0])
        //       }, 200)
        //     } else {
        //       pThis.autocompleteListListModalLocation = data
        //     }
        //   }
        // })
        this.input.is_autocomplete_ListModalLocation_load = false
    }
    autocompleteChooseListModalLocation(data: any): void {
        this.inputModalAddress.address_sub_district_code = data.code
        this.inputModalAddress.address_sub_district_name = data.name
        this.inputModalAddress.address_district_code = data.district_code
        this.inputModalAddress.address_district_name = data.district_name
        this.inputModalAddress.address_province_code = data.province_code
        this.inputModalAddress.address_province_name = data.province_name
        this.inputModalAddress.address_country_code = data.country_code
        this.inputModalAddress.address_country_name = data.country_name
        this.is_autocomplete_ListModalLocation_show = false
    }
    onClickSave04ModalAddressSave(): void {
        Object.keys(this.inputModalAddress).forEach((item: any) => {
            this.inputModalAddressEdit[item] = this.inputModalAddress[item]
        })
        this.toggleModal("isModalPeopleEditOpen")
    }
    // Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listClassification = data.classification_list || []
        this.help.PageSet(data, this.paginateDocumentProcessChange)

    }

    listData(data: any): void {
        this.listClassification = data.list || []
        this.help.PageSet(data, this.paginateDocumentProcessChange)

    }

    saveData(): any {
        let params = this.input

        params.classification_list = this.listClassification || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }
    displayMoney(value: any): any {
        return displayMoney(value);
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

}
