import { Component, OnInit } from '@angular/core'
import { Auth } from "../../auth";
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { OneStopServiceProcessService } from '../../services/one-stop-service-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  displayDateServer,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-record-registration-number-allow",
  templateUrl: "./record-registration-number-allow.component.html",
  styleUrls: ["./record-registration-number-allow.component.scss"]
})
export class RecordRegistrationNumberAllowComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  // List RecordRegistrationNumberAllow
  public listRecordRegistrationNumberAllow: any[]
  public paginateRecordRegistrationNumberAllow: any
  public perPageRecordRegistrationNumberAllow: number[]
  // Response RecordRegistrationNumberAllow
  public responseRecordRegistrationNumberAllow: any
  public listRecordRegistrationNumberAllowEdit: any


  // List OneStopServiceProcess
  public listOneStopServiceProcess: any[]
  public paginateOneStopServiceProcess: any
  public perPageOneStopServiceProcess: number[]
  // Response OneStopServiceProcess
  public responseOneStopServiceProcess: any

  //label_save_combobox || label_save_radio
  public floor3RegistrarStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public floor3_registrar_list: any[]

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      floor3_proposer_date: getMoment(),
      floor3_registrar_date: getMoment(),
      request_number: '',
      floor3_registrar_status_code: '',
    }
    this.listRecordRegistrationNumberAllow = []
    this.paginateRecordRegistrationNumberAllow = CONSTANTS.PAGINATION.INIT
    this.perPageRecordRegistrationNumberAllow = CONSTANTS.PAGINATION.PER_PAGE

    this.listOneStopServiceProcess = []

    //Master List
    this.master = {
      floor3RegistrarStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.OneStopServiceProcessService.Floor3RegistrarList().subscribe((floor3_registrar_list: any) => {
      this.floor3_registrar_list = floor3_registrar_list.list
      console.log(this.auth.getAuth())
      this.input.floor3_registrar_by = this.auth.getAuth().id

      this.forkJoinService.initRecordRegistrationNumberAllow().subscribe((data: any) => {
        if (data) {
          this.help.Clone(data, this.master)
          this.master.floor3RegistrarStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
          this.input.floor3_registrar_status_code = "WAIT"

        }
        if (this.editID) {
          this.OneStopServiceProcessService.RecordRegistrationNumberAllowLoad(this.editID).subscribe((data: any) => {
            if (data) {
              // Manage structure
              this.loadData(data)
            }
            // Close loading
            this.global.setLoading(false)
            this.automateTest.test(this)
          })
        } else {
          this.global.setLoading(false)
          this.automateTest.test(this)
        }

      })
    })
  }

  constructor(
    private auth: Auth,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private OneStopServiceProcessService: OneStopServiceProcessService
  ) { }

  onClickRecordRegistrationNumberAllowList(): void {
    //const params = {
    //  //page_index: +this.paginatePublicItem.currentPage,
    //  //item_per_page: +this.paginatePublicItem.itemsPerPage,
    //  order_by: 'created_date',
    //  is_order_reverse: false,
    //  search_by: [{
    //    key: 'floor3_registrar_date',
    //    value: displayDateServer(this.input.floor3_registrar_start_date),
    //    operation: 3
    //  }, {
    //    key: 'floor3_registrar_date',
    //    value: displayDateServer(this.input.floor3_registrar_end_date),
    //    operation: 4
    //  }, {
    //    key: 'floor3_proposer_date',
    //    value: displayDateServer(this.input.floor3_proposer_date),
    //    operation: 0
    //  }, {
    //    key: 'floor3_registrar_status_code',
    //    value: this.input.floor3_registrar_status_code,
    //    operation: 0
    //  }, {
    //    key: 'request_number',
    //    values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
    //    operation: 5
    //  }]
    //}

    const params = {
      floor3_proposer_date: this.input.floor3_proposer_date,
      floor3_registrar_start_date: this.input.floor3_registrar_start_date,
      floor3_registrar_end_date: this.input.floor3_registrar_end_date,
      floor3_registrar_status_code: this.input.floor3_registrar_status_code,
      request_number: this.input.request_number,
      floor3_registrar_by: this.input.floor3_registrar_by.toString(),
    }

    // if(this.validateRecordRegistrationNumberAllowList()) {
    // Open loading
    // Call api
    this.callRecordRegistrationNumberAllowList(params)
    // }
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberAllowList(params: any): void {
    this.global.setLoading(true)
    this.OneStopServiceProcessService.RecordRegistrationNumberAllowList(this.help.GetFilterParams(params, this.paginateRecordRegistrationNumberAllow)).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberAllowListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickRecordRegistrationNumberAllowDone(): void {
    //if(this.validateRecordRegistrationNumberAllowDone()) {
    // Open loading
    this.global.setLoading(true)
    // Call api

    this.listRecordRegistrationNumberAllow.filter(r => r.is_check).forEach((item: any) => {
      item.floor3_registrar_date = this.input.floor3_registrar_date
    })
    this.callRecordRegistrationNumberAllowDone(this.listRecordRegistrationNumberAllow)
  }
  validateRecordRegistrationNumberAllowDone(): boolean {
    let result = validateService('validateRecordRegistrationNumberAllowDone', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberAllowDone(params: any): void {
    this.OneStopServiceProcessService.RecordRegistrationNumberAllowDone(params).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberAllowDoneResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.onClickRecordRegistrationNumberAllowList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRecordRegistrationNumberAllowCancel(): void {
    var rs = prompt("รายละเอียดการไม่พิจารณา", "กรุณาใส่เหตุผล")

    if (rs && rs != "") {
      // Open loading
      this.global.setLoading(true)
      // Call api

      this.listRecordRegistrationNumberAllow.filter(r => r.is_check).forEach((item: any) => {
        item.floor3_registrar_date = this.input.floor3_registrar_date
        item.floor3_registrar_description = rs
      })
      this.callRecordRegistrationNumberAllowCancel(this.listRecordRegistrationNumberAllow)
    }
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberAllowCancel(params: any): void {
    this.OneStopServiceProcessService.RecordRegistrationNumberAllowCancel(params).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberAllowCancelResponse(res)) {
      if (data) {
        // Set value
        this.onClickRecordRegistrationNumberAllowList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRecordRegistrationNumberAllowAdd(): void {
    this.listRecordRegistrationNumberAllow.push({
      request_number: null,
      request_item_type_name: null,
      floor3_proposer_date_text: null,
      floor3_proposer_by_name: null,
      floor3_registrar_remark: null,
      floor3_registrar_date_text: null,

    })
    this.changePaginateTotal(this.listRecordRegistrationNumberAllow.length, 'paginateRecordRegistrationNumberAllow')
  }
  //onClickRecordRegistrationNumberAllowID(id: any): void { }
  onClickRecordRegistrationNumberAllowID(row_item: any): void {
    window.open('/considering-similar-instruction/edit/' + row_item.id)
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.OneStopServiceProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listRecordRegistrationNumberAllow = data.recordregistrationnumberallow_list || []
    this.changePaginateTotal(this.listRecordRegistrationNumberAllow.length, 'paginateRecordRegistrationNumberAllow')

  }

  listData(data: any): void {
    this.listRecordRegistrationNumberAllow = data.list || []
    this.help.PageSet(data, this.paginateRecordRegistrationNumberAllow)
  }

  saveData(): any {
    let params = this.input

    params.recordregistrationnumberallow_list = this.listRecordRegistrationNumberAllow || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
