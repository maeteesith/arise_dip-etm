import { Mm04_01_2_modalComponent } from './../mm04_01-2-modal/mm04_01-2-modal.component';
import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { MatDialog } from "@angular/material";

@Component({
  selector: "app-mm04",
  templateUrl: "./mm04.component.html",

  encapsulation: ViewEncapsulation.None,
  styleUrls: [
    "./mm04.component.scss",
    "./../../../assets/theme/styles/madrid/madrid.scss",
  ],
})
export class Mm04Component implements OnInit {
  constructor(private dialog: MatDialog) {}

  ItemArray: any = [];

  Item: any;
  Countrylist1 = [
    {id:1,name:"AF Afghanistan"},
    {id:2,name:"AG Antigua and Barbuda"},
    {id:3,name:"AL Albania"},
    {id:4,name:"AM Armenia"},
    {id:5,name:"AT Austria"},
    {id:6,name:"AU Australia"},
    {id:7,name:"AZ Azerbaijan"},
    {id:8,name:"BA Bosnia and Herzegovina"},
    {id:9,name:"BG Bulgaria"},
    {id:10,name:"BH Bahrain"},
    {id:11,name:"BN Brunei Darrussalam"},
    {id:12,name:"BQ Bonaire, Sint Eustatius and Saba"},
    {id:13,name:"BR Brazil"},
    {id:14,name:"BT Bhutan"},
    {id:15,name:"BW Botswana"},
    {id:16,name:"BX Benelux"},
    {id:17,name:"BY Belarus"},
    {id:18,name:"CA Canada"},
    {id:19,name:"CH Switzerland"},
    {id:20,name:"CN China"},
    {id:21,name:"CO Colombia"},
    {id:22,name:"CU Cuba"},
    {id:23,name:"CW Curacao"},
    {id:24,name:"CY Cyprus"},
    {id:25,name:"CZ Czech Republic"},
    {id:28,name:"DE Germany"},
    {id:27,name:"DK Denmark"},
  ]

  Countrylist2 = [
    {id:28,name:"DZ Algeria"},
    {id:29,name:"EE Estonia"},
    {id:30,name:"EG Egypt"},
    {id:31,name:"EM European Union"},
    {id:32,name:"ES Spain"},
    {id:33,name:"FI Finland"},
    {id:34,name:"FR France"},
    {id:35,name:"GB United Kingdon"},
    {id:36,name:"GE Georgia"},
    {id:37,name:"GH Ghana"},
    {id:38,name:"GM Gambia"},
    {id:39,name:"GR Greece"},
    {id:40,name:"HR Croatia"},
    {id:41,name:"HU Hungary"},
    {id:42,name:"ID Indonesia"},
    {id:43,name:"IE Ireland"},
    {id:44,name:"IL Israel"},
    {id:45,name:"IN India"},
    {id:46,name:"IR Iran(Islamic Republic of)"},
    {id:47,name:"IS Iceland"},
    {id:48,name:"IT Italy"},
    {id:49,name:"JP Japan"},
    {id:50,name:"KE Kenya"},
    {id:51,name:"KG Kyrgyzstan"},
    {id:52,name:"KH Cambodia"},
    {id:53,name:"KP Democratic People's Republic of Korea"},
    {id:54,name:"KR Republic of Korea"},
  ]

  Countrylist3 = [
    {id:55,name:"KZ Kazakhstan"},
    {id:56,name:"LA Lao People's Democratic Republic"},
    {id:57,name:"LI Liechtenstein"},
    {id:58,name:"LR Liberia"},
    {id:59,name:"LS Lesotho"},
    {id:60,name:"LT Lithuania"},
    {id:61,name:"LV Latvia"},
    {id:62,name:"MA Morocco"},
    {id:63,name:"MC Monaco"},
    {id:64,name:"MD Republic of Moldova"},
    {id:65,name:"ME Montenegro"},
    {id:66,name:"MG Madagascar"},
    {id:67,name:"MK The Republic of Nort Macedonia"},
    {id:68,name:"MN Mongolia"},
    {id:69,name:"MW Malawi"},
    {id:70,name:"MX Mexico"},
    {id:71,name:"MY Malaysia"},
    {id:72,name:"MZ Mozambique"},
    {id:73,name:"NA Namibia"},
    {id:74,name:"NO Norway"},
    {id:75,name:"NZ New Zealand"},
    {id:76,name:"OA African Intellectual Property Organization (OAPI)"},
    {id:77,name:"OM Oman"},
    {id:78,name:"PH Philippines"},
    {id:79,name:"PL Poland"},
    {id:80,name:"PT Portugal"},
    {id:81,name:"RO Romania"},
  ]

  Countrylist4 = [
    {id:82,name:"RS Serbia"},
    {id:83,name:"RU Russian Federation"},
    {id:84,name:"RW Rwanda"},
    {id:85,name:"SD Sudan "},
    {id:86,name:"SE Sweden "},
    {id:87,name:"SG Singapore"},
    {id:88,name:"SI Slovenia "},
    {id:89,name:"SK Slovakia "},
    {id:90,name:"SL Sierra Leone"},
    {id:91,name:"SM San Marino"},
    {id:92,name:"ST Sao Tome and Principe "},
    {id:93,name:"SX Sint Maarten (Dutch part)"},
    {id:94,name:"SY Syrian Arab Republic"},
    {id:95,name:"SZ Eswatini "},
    {id:96,name:"TH Thailand "},
    {id:97,name:"TJ Tajikistan "},
    {id:98,name:"TM Turkmenistan "},
    {id:99,name:"TN Tunisia "},
    {id:100,name:"TR Turkey"},
    {id:101,name:"UA Ukraine"},
    {id:102,name:"US United States of America"},
    {id:103,name:"UZ  Uzbekistan "},
    {id:104,name:"VN Viet Nam"},
    {id:105,name:"WS Samoa "},
    {id:106,name:"ZM Zambia "},
    {id:107,name:"ZW Zimbabwe "}
  ]
  ngOnInit() {
    //this.onClickModal01_4()
    //this.onClickModal01_2()
  }

  onClickModal01_2() {
    //console.log(resp, "selected");
    const dialogRef = this.dialog.open(Mm04_01_2_modalComponent, {
      width: "calc(100% - 80px)",
      maxWidth: "1500px",
       height: "calc(100% - 20px)",
      data: {},
    });
    dialogRef.afterClosed().subscribe((res) => {
      // this.router.navigate(['eordering/delivery-note/create-request']);
      // if (!res) return;
      // this.router.navigate(['eordering/delivery-note/create-request']);
      // this.navToAddProduct(<ApiProductGroupDisplay>res);
    });
  }
}
