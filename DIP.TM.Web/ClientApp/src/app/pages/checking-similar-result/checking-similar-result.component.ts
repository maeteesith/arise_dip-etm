import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-checking-similar-result",
  templateUrl: "./checking-similar-result.component.html",
  styleUrls: ["./checking-similar-result.component.scss"]
})
export class CheckingSimilarResultComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any

  public popup: any

  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List Trademark
  public listTrademark: any[]
  public paginateTrademark: any
  public perPageTrademark: number[]
  // Response Trademark
  public responseTrademark: any
  public listTrademarkEdit: any

  public input_duplicate: any
  public listDuplicateWordMark: any[]
  public listDuplicateWordMarkShow: any[]
  public is_updated_listDuplicateWordMark: any

  public listDuplicateImage: any[]
  public listDuplicateImageShow: any[]
  public is_updated_listDuplicateImage: any

  public listDuplicateDictionary: any[]
  public listDuplicateDictionaryShow: any[]
  public is_updated_listDuplicateDictionary: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      name: '',
      request_item_sub_type_1_code: '',
      request_item_sub_type_description: '',
      sound_description: '',
      register_person_name: '',
    }


    this.input_duplicate = {
      menu: 1,
      //request_number: "200100005"
    }
    //this.onClickCheckingSimilarResultDuplicateList()

    this.listDuplicateWordMark = []
    this.listDuplicateWordMarkShow = []
    this.is_updated_listDuplicateWordMark = {}

    this.listDuplicateImage = []
    this.listDuplicateImageShow = []
    this.is_updated_listDuplicateImage = {}

    this.listDuplicateDictionary = []
    this.listDuplicateDictionaryShow = []
    this.is_updated_listDuplicateDictionary = {}

    //this.listDuplicateWordMark = []

    this.listTrademark = []
    this.paginateTrademark = CONSTANTS.PAGINATION.INIT
    this.perPageTrademark = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List

    this.popup = {
      //is_duplicate_show: true
    }


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    //this.forkJoinService.initCheckingSimilarResult().subscribe((data: any) => {
    //    if (data) {
    //        this.master = data

    //    }

    if (this.editID) {
      let pThis = this
      this.CheckingProcessService.CheckingSimilarLoad(this.editID).subscribe((data: any) => {
        if (data) {
          // Manage structure
          pThis.loadData(data)

          this.CheckingSimilarTagList()
        }
      })
    } else {
      this.global.setLoading(false)
      this.automateTest.test(this)
    }
    //})
  }

  constructor(
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }

  CheckingSimilarTagList(): void {
    this.global.setLoading(true)
    this.CheckingProcessService.CheckingSimilarTagList(this.editID).subscribe((data: any) => {
      if (data) {
        console.log(data)
        this.listTrademark = data
        var pThis = this
        this.listTrademark.forEach(function (item: any, index) {
          item.word_mark = item.full_view.document_classification_word_list.map(function (item) { return item.word_mark }).join(", ")

          item.full_view.people_list.forEach((people: any) => {
            if (people.name == pThis.input.name) {
              item.is_owner_same_auto = true
              return false
            }
          })

          //if (item.is_owner_same || item.is_owner_same_auto) {
          //  var trademark = pThis.listTrademark.splice(index, 1)[0]
          //  pThis.listTrademark.splice(0, 0, trademark)
          //}
        })

        var _index = 0
        this.listTrademark.forEach(function (item: any, index) {
          if (item.is_owner_same_auto) {
            var trademark = pThis.listTrademark.splice(index, 1)[0]
            pThis.listTrademark.splice(_index++, 0, trademark)
          }
        })

        _index = 0
        this.listTrademark.forEach(function (item: any, index) {
          if (item.is_owner_same) {
            var trademark = pThis.listTrademark.splice(index, 1)[0]
            trademark.considering_remark = "เจ้าของเดียวกัน"
            pThis.listTrademark.splice(_index++, 0, trademark)
          }
        })
      }
      // Close loading
      this.global.setLoading(false)
      this.automateTest.test(this)
    })
  }

  onClickCheckingSimilarResultSend(): void {
    //if(this.validateCheckingSimilarResultSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingSimilarResultSend(this.saveData())
    //}
  }
  validateCheckingSimilarResultSend(): boolean {
    let result = validateService('validateCheckingSimilarResultSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarResultSend(params: any): void {
    this.global.setLoading(true)
    this.CheckingProcessService.CheckingSimilarResultSend(params).subscribe((data: any) => {
      this.togglePopup('isPopupSaveOpen')

      //var win = window.open("./checking-similar/list", "_self")
      this.global.setLoading(false)
      this.automateTest.test(this, { OwnerSame: 1 })
    })
  }

  onClickCheckingSimilarResultBack(): void {
    var win = window.open("./checking-similar/edit/" + this.editID)
  }
  validateCheckingSimilarResultBack(): boolean {
    let result = validateService('validateCheckingSimilarResultBack', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarResultBack(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingSimilarResultBack(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarResultBackResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingSimilarResultOwnerSame(): void {
    //if(this.validateCheckingSimilarResultOwnerSame()) {
    // Open loading
    // Call api
    this.callCheckingSimilarResultOwnerSame(this.listTrademark)
    //}
  }
  validateCheckingSimilarResultOwnerSame(): boolean {
    let result = validateService('validateCheckingSimilarResultOwnerSame', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarResultOwnerSame(params: any): void {
    this.global.setLoading(true)
    this.CheckingProcessService.CheckingSimilarResultOwnerSame(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarResultOwnerSameResponse(res)) {
      if (data) {
        // Set value
        //pThis.loadData(data)
        this.CheckingSimilarTagList()
      } else {
        this.global.setLoading(false)
      }
      // }
      // Close loading
    })
  }


  onClickCheckingSimilarTagRemove(): void {
    //if(this.validateCheckingSimilarTagRemove()) {
    // Open loading
    // Call api
    this.callCheckingSimilarTagRemove(this.listTrademark)
    //}
  }
  validateCheckingSimilarTagRemove(): boolean {
    let result = validateService('validateCheckingSimilarTagRemove', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarTagRemove(params: any): void {
    this.global.setLoading(true)
    this.CheckingProcessService.CheckingSimilarTagRemove(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarTagRemoveResponse(res)) {
      if (data) {
        // Set value
        //pThis.loadData(data)
        this.CheckingSimilarTagList()
      } else {
        this.global.setLoading(false)
      }
      // }
      // Close loading
    })
  }


  onClickCheckingSimilarResultTrademarkAdd(): void {
    this.listTrademark.push({
      index: this.listTrademark.length + 1,
      request_number: null,
      trademark_status_code: null,
      request_item_sub_type_description: null,
      word_mark: null,
      name: null,
      sound_link: null,

    })
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location


  onClickCheckingSimilarResultDuplicateList(): void {
    this.callCheckingSimilarResultDuplicateList(this.input_duplicate.request_number)
  }
  callCheckingSimilarResultDuplicateList(params): void {
    //let pThis = this
    this.CheckingProcessService.CheckingSimilarResultDuplicateList(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarResultBackResponse(res)) {
      if (data) {
        // Set value
        this.listDuplicateWordMark.length = 0
        data.save_word_list.forEach((item: any) => {
          this.listDuplicateWordMark.push(item)
        })
        this.is_updated_listDuplicateWordMark.updated = true

        this.listDuplicateImage.length = 0
        data.save_image_list.forEach((item: any) => {
          this.listDuplicateImage.push(item)
        })
        this.is_updated_listDuplicateImage.updated = true

        this.listDuplicateDictionary.length = 0
        data.word_list.forEach((item: any) => {
          this.listDuplicateDictionary.push(item)
        })
        this.is_updated_listDuplicateDictionary.updated = true

        //this.listDuplicateWordMark = data.save_word_list
        //this.help
        //this.listDuplicateImage = data.save_image_list
        //this.listDuplicateDictionary = data.word_list
        //pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingSimilarResultDuplicateAdd(): void {
    var params = {
      save_id: +this.editID,
      save_word_list: this.listDuplicateWordMark.filter(r => r.is_check),
      save_image_list: this.listDuplicateImage.filter(r => r.is_check),
      word_list: this.listDuplicateDictionary.filter(r => r.is_check),
    }

    this.callCheckingSimilarResultDuplicateAdd(params)
  }
  callCheckingSimilarResultDuplicateAdd(params): void {
    this.popup.is_duplicate_show = false
    this.global.setLoading(true)

    //let pThis = this
    this.CheckingProcessService.CheckingSimilarResultDuplicateAdd(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarResultBackResponse(res)) {
      if (data) {
        // Set value
        //this.listDuplicateWordMark = data.save_word_list
        //this.listDuplicateImage = data.save_image_list
        //this.listDuplicateDictionary = data.word_list
        //pThis.loadData(data)
        this.callInit()
      }
      // }
      // Close loading
      //this.global.setLoading(false)
    })
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listTrademark = data.trademark_list || []
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')

    //this.listTrademark.forEach((item: any) => {
    //  if (item.sound_file_physical_path && item.sound_file_physical_path != '') {
    //    item.player = new Audio(item.sound_file_physical_path)
    //    item.player_action = null
    //  }
    //})
  }

  togglePlayer(item: any): void {
    if (!item.player) {
      item.player = new Audio(item.sound_file_physical_path)
      item.player_action = null
    }

    if (item.player_action) {
      item.player.pause()
    } else {
      item.player.play()
    }
    item.player_action = !item.player_action
  }


  listData(data: any): void {
    this.listTrademark = data || []
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')

  }

  saveData(): any {
    let params = this.input

    params.trademark_list = this.listTrademark || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

  togglePopup(name: string): void {
    if (!this.popup[name]) { // Is open
      this.popup[name] = true
    } else { // Is close
      this.popup[name] = false
      if (name === 'isPopupConfirmDeleteOpen') {
        this.popup.removeId = 0
      }
    }
  }

}
