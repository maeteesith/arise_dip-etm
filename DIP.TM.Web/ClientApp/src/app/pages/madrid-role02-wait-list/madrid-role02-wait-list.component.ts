import { MadridRole023111ModalComponent } from './../madrid-role02-31-1-1-modal/madrid-role02-31-1-1-modal.component';
import { MadridRole02wait231modelModalComponent } from './../madrid-role02-wait-23-1-model/madrid-role02-wait-23-1-model.component';
import { MatDialog } from '@angular/material';
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { Router } from "@angular/router";

import {
  ROUTE_PATH,
  CONSTANTS,
  validateService,
  clone,
  getItemCalculator,
  displayMoney,
  displayFormatBytes,
  PapaParseCsvToJson,
  loopDisplayDateServer,
  displayAddress,
  viewPDF,
} from "../../helpers";
import { MadridRole02RequestProtectionModalComponent } from '../madrid-role02-request-protection-modal/madrid-role02-request-protection-modal.component';
import { MadridRole02ConciderProtectionModalComponent } from '../madrid-role02-concider-protection-modal/madrid-role02-concider-protection-modal.component';

@Component({
  selector: "app-madrid-role02-wait-list",
  templateUrl: "./madrid-role02-wait-list.component.html",
  styleUrls: ["./madrid-role02-wait-list.component.scss"]
})
export class MadridRole02WaitListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    
    public modal: any
    
    listPublicItem = [{
        index:1,
        week:2,
        request_number:111,
        irn_number:222,
        register_date:333,
        wipi_noti:"123",
        expite_date:"1/1/2020",
        mask:1,
        request_type:"Testttttttttttttttt",
        translator:"Somjai",
        remark:"Test",
        public_status_name:"Test"

    },{
        index:1,
        week:2,
        request_number:111,
        irn_number:222,
        register_date:333,
        wipi_noti:"123",
        expite_date:"1/1/2020",
        mask:1,
        request_type:"Testttttttttttttttt",
        translator:"Somjai",
        remark:"Test",
        public_status_name:"Test"

    }]

    ngOnInit() {
        this.validate = {}
        this.input = {
            id: null,
            //instruction_send_start_date: getMoment(),
            //instruction_send_end_date: getMoment(),
            public_type_code: '',
            public_source_code: '',
            public_receiver_by: '',
            public_status_code: '',
            request_number: '',
          }
          this.master={
            request_type:[{code:1,name:'1'}],
            job_reciver:[{id:1,name:"Sam"},{id:2,name:"Tom"}]
        }
        //  this.onClickModal2_2()
       // this.onClickModal2_9_8()
      // this.onClickModal2_3_1()
      //this.onClickModal3_31_1();
    }

    

    constructor(
        private help: Help,
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService,
        private dialog: MatDialog,
    ) { }

    



    //! <<< ----------------- >>>
    //! <<< --- Copy Zone --- >>>
    //! <<< ----------------- >>>

    //! <<< Binding (one way) >>>
    binding(obj: any, name: any, value: any): void {
        this[obj][name] = value
    }

    //! <<< Table >>>
    onSelectColumnInTable(name: string, item: any): void {
        if (!item) {
            this.input.is_check_all = !this.input.is_check_all
            this[name].forEach((item: any) => {
                item.is_check = this.input.is_check_all
            })
        } else {
            item.is_check = !item.is_check
        }
    }
    onChangeInputInTable(item: any, name: any, value: any): void {
        item[name] = value
    }
    toggleInputInTable(item: any, name: any): void {
        item[name] = !item[name]
    }

    //! <<<< Pagination >>>
    managePaginateCallApi(name: string): void {
        if (name === 'paginateDocumentRole02Check') {
            //this.onClickDocumentRole02CheckList()
            this.input.is_check_all = false
        }
    }
    onChangePage(page: any, name: string): void {
        this[name].currentPage = page === '' ? 1 : page
        if ((+page || page === '') && page <= this.getMaxPage(name)) {
            this.managePaginateCallApi(name)
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
        this.managePaginateCallApi(name)
    }
    changePaginateTotal(total: any, name: string): void {
        this[name].totalItems = total
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

    onClickModal2_2(){
        //console.log(resp, "selected");
            const dialogRef = this.dialog.open(MadridRole02RequestProtectionModalComponent, {
                width: "calc(100% - 80px)",
                maxWidth: "1500px",
                height:"calc(100% - 20px)",
                data: {}
            });
            dialogRef.afterClosed().subscribe(res => {
                // this.router.navigate(['eordering/delivery-note/create-request']);
                // if (!res) return;
    
                // this.router.navigate(['eordering/delivery-note/create-request']);
                // this.navToAddProduct(<ApiProductGroupDisplay>res);
            });
      }
      //MR2-9-8
      onClickModal2_9_8(){
        //console.log(resp, "selected");
            const dialogRef = this.dialog.open(MadridRole02ConciderProtectionModalComponent, {
                width: "calc(100% - 80px)",
                maxWidth: "1800px",
                height:"calc(100% - 20px)",
                data: {}
            });
            dialogRef.afterClosed().subscribe(res => {
                // this.router.navigate(['eordering/delivery-note/create-request']);
                // if (!res) return;
    
                // this.router.navigate(['eordering/delivery-note/create-request']);
                // this.navToAddProduct(<ApiProductGroupDisplay>res);
            });
      }
        //MR2-3-1
      onClickModal2_3_1(){
        //console.log(resp, "selected");
            const dialogRef = this.dialog.open(MadridRole02wait231modelModalComponent, {
                width: "calc(100% - 80px)",
                maxWidth: "1000px",
                height:"calc(100% - 20px)",
                data: {}
            });
            dialogRef.afterClosed().subscribe(res => {
                // this.router.navigate(['eordering/delivery-note/create-request']);
                // if (!res) return;
    
                // this.router.navigate(['eordering/delivery-note/create-request']);
                // this.navToAddProduct(<ApiProductGroupDisplay>res);
            });
      }
      //MR3-31-1
      onClickModal3_31_1(){
        //console.log(resp, "selected");
            const dialogRef = this.dialog.open(MadridRole023111ModalComponent, {
                width: "calc(100% - 80px)",
                maxWidth: "1200px",
                height:"calc(100% - 20px)",
                data: {}
            });
            dialogRef.afterClosed().subscribe(res => {
                // this.router.navigate(['eordering/delivery-note/create-request']);
                // if (!res) return;
    
                // this.router.navigate(['eordering/delivery-note/create-request']);
                // this.navToAddProduct(<ApiProductGroupDisplay>res);
            });
      }
}
