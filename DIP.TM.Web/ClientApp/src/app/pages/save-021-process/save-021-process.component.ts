import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-save-021-process",
  templateUrl: "./save-021-process.component.html",
  styleUrls: ["./save-021-process.component.scss"]
})
export class Save021ProcessComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public modal: any

  // List People01
  public listPeople01: any[]
  public paginatePeople01: any
  public perPagePeople01: number[]
  // Response People01
  public responsePeople01: any
  public listPeople01Edit: any

  // List Product01
  public listProduct01: any[]
  public paginateProduct01: any
  public perPageProduct01: number[]
  // Response Product01
  public responseProduct01: any
  public listProduct01Edit: any


  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      make_date: getMoment(),
      remark: '',
      request_date_text: '',
      index: '',
      irn_number: '',
    }
    this.listPeople01 = []
    this.paginatePeople01 = CONSTANTS.PAGINATION.INIT
    this.perPagePeople01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct01 = []
    this.paginateProduct01 = CONSTANTS.PAGINATION.INIT
    this.perPageProduct01 = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false


    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave021Process().subscribe((data: any) => {
      if (data) {
        this.master = data

      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save021Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          pThis.global.setLoading(false)
        })
      } else {
        this.global.setLoading(false)

      }
    })
  }

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService
  ) { }

  autocompleteKeyDown(event: any) {
    if (event.key == "Enter") {
      this.autocompleteChangeListNotSent(this.input, 'request_number', 5, 1)
    }
  }


  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: { request_number: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    setTimeout(function () {
      this.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    let pthis = this
    this.SaveProcessService.Save021ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pthis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else {
          this.autocompleteListListNotSent = data
        }
      }
    })
    this.input.is_autocomplete_ListNotSent_load = false
  }
  autocompleteChooseListNotSent(data: any): void {
    this.input = data

    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []
    this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')
    this.changePaginateTotal((this.listProduct01 || []).length, 'paginateProduct01')


    this.is_autocomplete_ListNotSent_show = false
  }


  onClickSave021Madrid(): void {
    //if(this.validateSave021Madrid()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave021Madrid(this.saveData())
    //}
  }
  validateSave021Madrid(): boolean {
    let result = validateService('validateSave021Madrid', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave021Madrid(params: any): void {
    let pThis = this
    this.SaveProcessService.Save021Madrid(params).subscribe((data: any) => {
      // if(isValidSave021MadridResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave021Delete(): void {
    //if(this.validateSave021Delete()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave021Delete(this.saveData())
    //}
  }
  validateSave021Delete(): boolean {
    let result = validateService('validateSave021Delete', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave021Delete(params: any): void {
    let pThis = this
    this.SaveProcessService.Save021Delete(params).subscribe((data: any) => {
      // if(isValidSave021DeleteResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave021DocumentView(): void {
    window.open('/File/RequestDocumentCollect/21/' + this.input.request_number + "_" + this.input.request_id)
  }
  validateSave021DocumentView(): boolean {
    let result = validateService('validateSave021DocumentView', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave021DocumentView(params: any): void {
    let pThis = this
    this.SaveProcessService.Save021DocumentView(params).subscribe((data: any) => {
      // if(isValidSave021DocumentViewResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave021Save(): void {
    //if(this.validateSave021Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave021Save(this.saveData())
    //}
  }
  validateSave021Save(): boolean {
    let result = validateService('validateSave021Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave021Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save021Save(params).subscribe((data: any) => {
      // if(isValidSave021SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave021Send(): void {
    //if(this.validateSave021Send()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave021Send(this.saveData())
    //}
  }
  validateSave021Send(): boolean {
    let result = validateService('validateSave021Send', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave021Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save021Send(params).subscribe((data: any) => {
      // if(isValidSave021SendResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }

  onClickSave021RequestBack(): void {
    //if(this.validateSave021Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave021RequestBack(this.saveData())
    //}
  }
  callSave021RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save021RequestBack(params).subscribe((data: any) => {
      // if(isValidSave021SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }


  onClickSave021People01Add(): void {
    this.listPeople01.push({
      card_type_name: null,
      card_number: null,
      name: null,
      house_number: null,
      telephone: null,

    })
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
  }
  onChangeCheckboxPeople01(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
  onChangeSelectPeople01(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }

  onClickSave021Product01Add(): void {
    this.listProduct01.push({
      index: this.listProduct01.length + 1,
      request_item_sub_type_1_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
  }
  onChangeCheckboxProduct01(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
  onChangeSelectProduct01(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }



  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data
    if (data && data.length) this.changePaginateTotal(data.length, 'paginateSaveProcess')

    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []
    let index = 1
    data.people_01_list.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    data.product_01_list.map((item: any) => { item.is_check = false; item.index = index++; return item })
    this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')
    this.changePaginateTotal((this.listProduct01 || []).length, 'paginateProduct01')

  }

  saveData(): any {
    let params = this.input

    params.people_01_list = this.listPeople01 || []
    params.product_01_list = this.listProduct01 || []


    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
