import { Component, OnInit } from '@angular/core'
import { fabric } from "fabric"
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router'
import { Auth } from "../../auth";
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'
import { Location } from '@angular/common'
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { UploadService } from "../../services/upload.service"
import { AutomateTest } from '../../test/automate_test'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-document-process-classification-do",
    templateUrl: "./document-process-classification-do.component.html",
    styleUrls: ["./document-process-classification-do.component.scss"]
})
export class DocumentProcessClassificationDoComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public beforEditID: any
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public modal: any
    public tab_show_index: any

    public player: any
    public player_action: any

    public documentClassificationVersionList: any[]

    public documentClassification: any

    public documentClassificationWord: any
    public documentClassificationWordIndex: any
    public documentClassificationWordList: any[]
    public documentClassificationWordList_Deleted: any[]

    public documentClassificationImage: any
    public documentClassificationImageIndex: any
    public documentClassificationImageList: any[]
    public documentClassificationImageAutocompleteIndex: any
    public documentClassificationImageAutocompleteList: any[]
    public documentClassificationImageAutocompleteListShowData: any[]
    public documentClassificationImageAutocompleteListShowPaging: any

    public documentClassificationSound: any
    public documentClassificationSoundIndex: any
    public documentClassificationSoundList: any[]
    public documentClassificationSoundAutocompleteIndex: any
    public documentClassificationSoundAutocompleteList: any[]

    //label_save_combobox || label_save_radio
    public documentClassificationLanguageList: any[]
    //label_save_combobox || label_save_radio

    public autocompleteListListWordFirst: any
    public is_autocomplete_ListWordFirst_show: any
    public is_autocomplete_ListWordFirst_load: any
    public autocompleteListListWordSoundLast: any
    public is_autocomplete_ListWordSoundLast_show: any
    public is_autocomplete_ListWordSoundLast_load: any
    public autocompleteListListWordSoundLastOther: any
    public is_autocomplete_ListWordSoundLastOther_show: any
    public is_autocomplete_ListWordSoundLastOther_load: any
    //Modal Initial
    //Modal Initial

    public menu: string = 'menu1';
    public menutab: any

    public canvas: any
    public canvas_img: any
    public canvas_el: any
    public canvas_rectangle: any
    public canvas_is_erase: any
    public canvas_is_crop: any
    public tempLeft: number
    public tempTop: number
    public tempWidth: number
    public tempHeight: number
    public tempScale: number
    public tempScaleWidth: number
    public tempScaleHeight: number

    public popup: any
    public search_similar: any
    public listSeachSimilar: any[]
    public paginateSeachSimilar: any

    public is_show_file_scan: any
    public url: any
    public is_DocumentClassificationLanguage: boolean

    public word_first_error: any[]
    public word_sound_last_code: any[]
    public word_sound_last_other_code: any[]
    public word_syllable_sound_error: any[]

    public image_show_zoom_list: any[]

    public is_SearchSimilarList_list: boolean

    public tab_version_show_index: any

    ngOnInit() {
        this.editID = this.editID || this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }
        this.tab_show_index = 'WORD'
        //this.tab_show_index = 'IMAGE'
        //this.tab_show_index = 'SOUND'

        this.input = {
            id: null,
            name: '',
            document_classification_language: '',
            word: '',
            first_character_code: '',
            first_character_name: '',
            last_sound_name: '',
            last_sound_other_code: '',
            last_sound_other_name: '',
            word_read: '',
            all_sound: '',
            all_sound_cound: '',
        }

        this.documentClassificationVersionList = []

        this.documentClassificationWord = {}
        this.documentClassificationWordIndex = 0
        this.documentClassificationWordList = [{}]
        this.documentClassificationWordList_Deleted = [{}]

        this.documentClassificationImage = {}
        this.documentClassificationImageIndex = 0
        this.documentClassificationImageList = [{}]
        this.documentClassificationImageAutocompleteIndex = -1

        this.documentClassificationSound = {}
        this.documentClassificationSoundIndex = 0
        this.documentClassificationSoundList = [{}]
        this.documentClassificationSoundAutocompleteIndex = -1

        //let pThis = this
        //setInterval(function () {
        //    console.log(pThis.documentClassificationWord)
        //    console.log(pThis.documentClassificationWordIndex)
        //    console.log(pThis.documentClassificationWordList)
        //}, 1000)

        //Master List
        this.master = {
            documentClassificationLanguageList: [{ code: "TH_EN", name: "ไทย, อังกฤษ" }],
        }
        //Master List

        this.autocompleteListListWordFirst = []
        this.is_autocomplete_ListWordFirst_show = false
        this.is_autocomplete_ListWordFirst_load = false
        this.autocompleteListListWordSoundLast = []
        this.is_autocomplete_ListWordSoundLast_show = false
        this.is_autocomplete_ListWordSoundLast_load = false
        this.autocompleteListListWordSoundLastOther = []
        this.is_autocomplete_ListWordSoundLastOther_show = false
        this.is_autocomplete_ListWordSoundLastOther_load = false
        this.autocompleteListListWordFirst = []
        this.is_autocomplete_ListWordFirst_show = false
        this.is_autocomplete_ListWordFirst_load = false
        this.autocompleteListListWordSoundLast = []
        this.is_autocomplete_ListWordSoundLast_show = false
        this.is_autocomplete_ListWordSoundLast_load = false
        this.autocompleteListListWordSoundLastOther = []
        this.is_autocomplete_ListWordSoundLastOther_show = false
        this.is_autocomplete_ListWordSoundLastOther_load = false
        this.autocompleteListListWordFirst = []
        this.is_autocomplete_ListWordFirst_show = false
        this.is_autocomplete_ListWordFirst_load = false
        this.autocompleteListListWordSoundLast = []
        this.is_autocomplete_ListWordSoundLast_show = false
        this.is_autocomplete_ListWordSoundLast_load = false
        this.autocompleteListListWordSoundLastOther = []
        this.is_autocomplete_ListWordSoundLastOther_show = false
        this.is_autocomplete_ListWordSoundLastOther_load = false

        this.popup = {
            image_show_zoom: 100,
        }
        this.search_similar = {
            result: {}
        }

        this.word_first_error = []
        this.word_sound_last_code = []
        this.word_sound_last_other_code = []
        this.word_syllable_sound_error = []

        this.image_show_zoom_list = [10, 25, 75, 100, 125, 150, 200, 400, 800, 1600, 2400, 3200, 6400]

        this.listSeachSimilar = []
        this.paginateSeachSimilar = CONSTANTS.PAGINATION.INIT

        this.documentClassificationImageAutocompleteListShowPaging = CONSTANTS.PAGINATION.INIT

        var pThis = this
        this.autoComplete.Initial()
        this.autoComplete.Add("/DocumentProcess/ClassificationListWordFirst", "รหัสรายการ", ["code", "name"], [0, 1], 0,
            this.documentClassificationWord, [
            ["word_first_code", "code"],
            ["word_first_name", "name"],
        ], null, 100
        )

        this.autoComplete.Add("/DocumentProcess/ClassificationListWorSoundLast", "รหัสรายการ", ["name"], [0], 0,
            this.documentClassificationWord, [
            ["word_sound_last_code", "code"],
        ], null, 200
        )

        this.autoComplete.Add("/DocumentProcess/ClassificationListWorSoundLastOther", "รหัสรายการ", ["code", "name"], [0, 1], 0,
            this.documentClassificationWord, [
            ["word_sound_last_other_code", "code"],
            ["word_sound_last_other_name", "name"],
        ], null, 100
        )

        this.is_SearchSimilarList_list = true

        this.callInit()

        this.dragElement(document.getElementById("div_image_show"))
        this.dragExpandElement(document.getElementById("div_image_show"), document.getElementById("div_image_expand"))

        var pThis = this
        document.onkeydown = function (event) {
            //var evt = event || window.event

            if (event.code == "Escape") {
                pThis.popup.isPopupErrorOpen = false
            }
        }
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initDocumentProcessClassificationDo().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.search_similar.people_name_condition = "LIKE"
                this.search_similar.word_mark_condition = "LIKE"
            }

            if (this.editID) {
                this.DocumentProcessService.ClassificationLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        this.documentClassification = data
                        // Manage structure
                        this.loadData(data)

                        // Set value
                        // this.requestList = data.item_list
                        // this.response.request01Load = data
                        // this.isHasRequestList = true
                    }


                    this.DocumentProcessService.ClassificationVersionList(this.editID).subscribe((data: any) => {
                        this.global.setLoading(false)
                        this.automateTest.test(this)

                        if (data && data.list && data.list.length > 0) {
                            this.documentClassificationVersionList = data.list
                            this.loadData(this.documentClassificationVersionList.filter(r => r.is_master)[0], false)
                        }
                    })
                })

                console.log(this.input)
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }
        })
    }

    constructor(
        private autoComplete: AutoComplete,
        private uploadService: UploadService,
        private help: Help,
        private auth: Auth,
        public sanitizer: DomSanitizer,

        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }


    // Autocomplete
    autocompleteChangeListWordFirst(object: any, name: any, item_per_page: any = 5, length: any = 1): void {
        if (object[name].length >= length) {
            this.is_autocomplete_ListWordFirst_show = true

            let params = {
                filter: { first_character_code: object[name] },
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListWordFirst(params)
        }
    }
    autocompleteBlurListWordFirst(object: any, name: any, item_per_page: any = 5): void {
        setTimeout(function () {
            this.is_autocomplete_ListWordFirst_show = false
        }, 200)
    }
    callAutocompleteChangeListWordFirst(params: any): void {
        if (this.input.is_autocomplete_ListWordFirst_load) return
        this.input.is_autocomplete_ListWordFirst_load = true
        let pthis = this
        this.DocumentProcessService.ClassificationListWordFirst(params).subscribe((data: any) => {
            if (data) {
                if (data.length == 1) {
                    setTimeout(function () {
                        pthis.autocompleteChooseListWordFirst(data[0])
                    }, 200)
                } else {
                    this.autocompleteListListWordFirst = data
                }
            }
        })
        this.input.is_autocomplete_ListWordFirst_load = false
    }
    autocompleteChooseListWordFirst(data: any): void {
        this.input.first_character_code = data.first_character_code
        this.input.first_character_name = data.first_character_name

        this.is_autocomplete_ListWordFirst_show = false
    }


    // Autocomplete
    autocompleteChangeListWordSoundLast(object: any, name: any, item_per_page: any = 5, length: any = 1): void {
        if (object[name].length >= length) {
            this.is_autocomplete_ListWordSoundLast_show = true

            let params = {
                filter: { last_sound_name: object[name] },
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListWordSoundLast(params)
        }
    }
    autocompleteBlurListWordSoundLast(object: any, name: any, item_per_page: any = 5): void {
        setTimeout(function () {
            this.is_autocomplete_ListWordSoundLast_show = false
        }, 200)
    }
    callAutocompleteChangeListWordSoundLast(params: any): void {
        if (this.input.is_autocomplete_ListWordSoundLast_load) return
        this.input.is_autocomplete_ListWordSoundLast_load = true
        let pthis = this
        this.DocumentProcessService.ClassificationListWordSoundLast(params).subscribe((data: any) => {
            if (data) {
                if (data.length == 1) {
                    setTimeout(function () {
                        pthis.autocompleteChooseListWordSoundLast(data[0])
                    }, 200)
                } else {
                    this.autocompleteListListWordSoundLast = data
                }
            }
        })
        this.input.is_autocomplete_ListWordSoundLast_load = false
    }
    autocompleteChooseListWordSoundLast(data: any): void {
        this.input.last_sound_name = data.last_sound_name

        this.is_autocomplete_ListWordSoundLast_show = false
    }


    // Autocomplete
    autocompleteChangeListWordSoundLastOther(object: any, name: any, item_per_page: any = 5, length: any = 1): void {
        if (object[name].length >= length) {
            this.is_autocomplete_ListWordSoundLastOther_show = true

            let params = {
                filter: { last_sound_other_code: object[name] },
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListWordSoundLastOther(params)
        }
    }
    autocompleteBlurListWordSoundLastOther(object: any, name: any, item_per_page: any = 5): void {
        setTimeout(function () {
            this.is_autocomplete_ListWordSoundLastOther_show = false
        }, 200)
    }
    callAutocompleteChangeListWordSoundLastOther(params: any): void {
        if (this.input.is_autocomplete_ListWordSoundLastOther_load) return
        this.input.is_autocomplete_ListWordSoundLastOther_load = true
        let pthis = this
        this.DocumentProcessService.ClassificationListWordSoundLastOther(params).subscribe((data: any) => {
            if (data) {
                if (data.length == 1) {
                    setTimeout(function () {
                        pthis.autocompleteChooseListWordSoundLastOther(data[0])
                    }, 200)
                } else {
                    this.autocompleteListListWordSoundLastOther = data
                }
            }
        })
        this.input.is_autocomplete_ListWordSoundLastOther_load = false
    }
    autocompleteChooseListWordSoundLastOther(data: any): void {
        this.input.last_sound_other_code = data.last_sound_other_code

        this.is_autocomplete_ListWordSoundLastOther_show = false
    }

    onClickDocumentClassificationImageAutocomplete(object: any, index: any): void {
        var pThis = this
        setTimeout(function () {
            pThis.documentClassificationImageAutocompleteIndex = index;
            pThis.onChangeDocumentClassificationImageAutocomplete(object)
        }, 300);
    }

    onClickDocumentClassificationSoundAutocomplete(object: any, index: any): void {
        var pThis = this
        setTimeout(function () {
            pThis.documentClassificationSoundAutocompleteIndex = index;
            pThis.onChangeDocumentClassificationSoundAutocomplete(object)
        }, 300);
    }

    // Autocomplete
    onChangeDocumentClassificationImageAutocomplete(object: any): void {
        //if (object.document_classification_image_code.length > 0) {
        this.documentClassificationImageAutocompleteList = []
        let params = {
            filter: { code: object.document_classification_image_code },
            paging: { item_per_page: 10000 },
        }
        this.callDocumentClassificationImageAutocomplete(object, params)
        //}
    }
    callDocumentClassificationImageAutocomplete(object: any, params: any): void {
        let pthis = this
        this.DocumentProcessService.ClassificationListImageType(params).subscribe((data: any) => {
            if (data) {
                if (data.length == 1) {
                    setTimeout(function () {
                        pthis.onClickDocumentClassificationImageAutocompleteLoad(object, data[0])
                    }, 200)
                } else {
                    this.documentClassificationImageAutocompleteList = data
                    this.documentClassificationImageAutocompleteListShowPaging = {
                        page_index: 1,
                        item_per_page: 10,
                        item_total: data.length,
                    }
                    this.onClickDocumentClassificationImageAutocompleteList()
                }
            }
        })
    }
    onClickDocumentClassificationImageAutocompleteLoad(object: any, data: any): void {
        object.document_classification_image_code = data.code
        object.document_classification_image_name = data.name

        console.log(object)
        console.log(data)

        this.documentClassificationImageAutocompleteIndex = -1
    }

    onClickDocumentClassificationImageAutocompleteInside(): void {
        var pThis = this
        setTimeout(function () {
            clearTimeout(pThis.input.timeout)
        }, 50);
    }

    onClickDocumentClassificationImageAutocompleteOutside(item: any): void {
        var pThis = this
        this.input.timeout = setTimeout(function () {
            var _documentClassificationImageAutocompleteListShowData = pThis.documentClassificationImageAutocompleteListShowData.filter(r => r.code == item.document_classification_image_code);
            if (_documentClassificationImageAutocompleteListShowData.length > 0) {
                item.document_classification_image_code = _documentClassificationImageAutocompleteListShowData[0].code;
                item.document_classification_image_name = _documentClassificationImageAutocompleteListShowData[0].name;
                //} else {
                //item.document_classification_image_code = '';
                //item.document_classification_image_name = '';
            } else {
                let params = {
                    filter: {
                        code: item.document_classification_image_code,
                        is_exactly: true,
                    },
                    paging: { item_per_page: 10000 },
                }

                pThis.DocumentProcessService.ClassificationListImageType(params).subscribe((data: any) => {
                    if (data) {
                        if (data.length == 1) {
                            item.document_classification_image_code = data[0].code;
                            item.document_classification_image_name = data[0].name;
                            //  setTimeout(function () {
                            //    pthis.onClickDocumentClassificationImageAutocompleteLoad(object, data[0])
                            //  }, 200)
                        } else {
                            pThis.input.error = "ไม่พบรหัสรูป"
                            pThis.documentClassificationWord.word_sound_last_code = ""
                            pThis.documentClassificationWord.word_sound_last_name = ""
                            pThis.togglePopup('isPopupErrorOpen')
                        }
                    }
                })
            }

            pThis.documentClassificationImageAutocompleteIndex = -1
        }, 100);
    }

    onClickDocumentClassificationImageAutocompleteList(): void {
        this.documentClassificationImageAutocompleteListShowData = this.help.GetArrayFromPaging(this.documentClassificationImageAutocompleteList, this.documentClassificationImageAutocompleteListShowPaging)
    }


    onKeyDownDocumentClassificationImageAutocomplete(event, i): void {
        if (event.key == "Tab") {
            //var document_classification_image_code_list = document.getElementsByClassName("document_classification_image_code")
            //var index = document_classification_image_code_list.indexOf(event.target)
            //console.log(index)
            //var document_classification_image_code_list = document.getElementsByClassName("document_classification_image_code")
            var document_classification_image_code = document.getElementById("document_classification_image_code_" + (i + 1))

            console.log(document_classification_image_code)

            if (document_classification_image_code == null) {
                this.documentClassificationImageList.push({})
                setTimeout(function () {
                    document_classification_image_code = document.getElementById("document_classification_image_code_" + (i + 1))
                }, 50)
            }

            setTimeout(function () {
                document_classification_image_code.focus()
            }, 100)
            //for (var _i = 0; _i < document_classification_image_code_list.length; _i++) {
            //  if (document_classification_image_code_list[_i] === event.target) {
            //    //console.log(_i)
            //    //setTimeout(function () {
            //    //  document_classification_image_code_list[_i + 1].focus()

            //    //}, 1000)
            //  }
            //}
            console.log()
        }
    }

    // Autocomplete
    onChangeDocumentClassificationSoundAutocomplete(object: any): void {
        this.documentClassificationSoundAutocompleteList = []
        let params = {
            filter: { code: object.document_classification_sound_code },
            paging: { item_per_page: 5 },
        }
        this.callDocumentClassificationSoundAutocomplete(object, params)
    }
    callDocumentClassificationSoundAutocomplete(object: any, params: any): void {
        let pthis = this
        this.DocumentProcessService.ClassificationListSoundType(params).subscribe((data: any) => {
            if (data) {
                if (data.length == 1) {
                    setTimeout(function () {
                        pthis.onClickDocumentClassificationSoundAutocompleteLoad(object, data[0])
                    }, 200)
                } else {
                    this.documentClassificationSoundAutocompleteList = data
                }
            }
        })
    }
    onClickDocumentClassificationSoundAutocompleteLoad(object: any, data: any): void {
        object.document_classification_sound_code = data.code
        object.document_classification_sound_name = data.name

        this.documentClassificationSoundAutocompleteIndex = -1
    }


    onClickClassificationViewScanFile(): void {
        this.url = "File/RequestDocumentCollect/SAVE01/" + this.editID
        this.is_show_file_scan = !this.is_show_file_scan
        //window.open()
    }
    validateClassificationViewScanFile(): boolean {
        let result = validateService('validateClassificationViewScanFile', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callClassificationViewScanFile(params: any): void {
        this.DocumentProcessService.ClassificationViewScanFile(params).subscribe((data: any) => {
            // if(isValidClassificationViewScanFileResponse(res)) {
            if (data) {
                // Set value
                this.input = data
                this.response = data

            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickClassificationDownload(): void {
        //if(this.validateClassificationDownload()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        const params = this.input

        // Call api
        this.callClassificationDownload(params)
        //}
    }
    validateClassificationDownload(): boolean {
        let result = validateService('validateClassificationDownload', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callClassificationDownload(params: any): void {
        this.DocumentProcessService.ClassificationDownload(params).subscribe((data: any) => {
            // if(isValidClassificationDownloadResponse(res)) {
            if (data) {
                // Set value
                this.input = data
                this.response = data

            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickClassificationTranslateTool(): void {
        //if(this.validateClassificationTranslateTool()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        const params = this.input

        // Call api
        this.callClassificationTranslateTool(params)
        //}
    }
    validateClassificationTranslateTool(): boolean {
        let result = validateService('validateClassificationTranslateTool', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callClassificationTranslateTool(params: any): void {
        this.DocumentProcessService.ClassificationTranslateTool(params).subscribe((data: any) => {
            // if(isValidClassificationTranslateToolResponse(res)) {
            if (data) {
                // Set value
                this.input = data
                this.response = data

            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickClassificationSave(): void {
        if (this.word_syllable_sound_error.length > 0) {
            this.togglePopup('isPopupErrorOpen')
            return
        }
        //if(this.validateClassificationSave()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        //const params = this.input

        // Call api
        this.callClassificationSave(this.saveData())
        //}
    }
    validateClassificationSave(): boolean {
        let result = validateService('validateClassificationSave', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callClassificationSave(params: any): void {
        console.log(params.document_classification_word_list)

        this.DocumentProcessService.ClassificationSave(params).subscribe((data: any) => {
            // if(isValidClassificationSaveResponse(res)) {
            if (data) {
                // Set value
                //this.loadData(data)

                //if (data.classification_send_next_save_id) {
                //  this.beforEditID = this.editID
                //  this.editID = data.classification_send_next_save_id
                //}
                this.popup.isPopupSaveOpen = true
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickClassificationPreview(): void {
        window.open("pdf/DocumentProcessClassification/" + this.input.request_number)
    }
    validateClassificationPreview(): boolean {
        let result = validateService('validateClassificationPreview', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callClassificationPreview(params: any): void {
        this.DocumentProcessService.ClassificationPreview(params).subscribe((data: any) => {
            // if(isValidClassificationPreviewResponse(res)) {
            if (data) {
                // Set value
                this.input = data
                this.response = data

            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickSearchSimilarPreview(): void {
        window.open("pdf/DocumentProcessClassification/" + this.search_similar.result.request_number)
    }

    onClickClassificationBack(): void {
        this.editID = this.beforEditID
        this.beforEditID = null
        this.onClickReset()
    }

    onClickClassificationSend(): void {
        if (this.word_syllable_sound_error.length > 0) {
            this.togglePopup('isPopupErrorOpen')
            return
        }

        //if(this.validateClassificationSend()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        // Call api
        this.callClassificationSend(this.saveData())
        //}
    }
    validateClassificationSend(): boolean {
        let result = validateService('validateClassificationSend', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callClassificationSend(params: any): void {
        this.DocumentProcessService.ClassificationSend(params).subscribe((data: any) => {
            // if(isValidClassificationSendResponse(res)) {
            if (data) {
                // Set value
                this.loadData(data)

                this.popup.isPopupSendOpen = true
                if (data.classification_send_next_save_id) {
                    this.beforEditID = this.editID
                    this.editID = data.classification_send_next_save_id
                }
                //this.popup.isPopupSendOpenID = data.classification_send_next_save_id
            }
            // }
            // Close loading
            this.global.setLoading(false)
            this.automateTest.test(this, { Send: 1 })
        })
    }

    onClickClassificationSearchRequest(): void {
        //if(this.validateClassificationSearchRequest()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        const params = this.input

        // Call api
        this.callClassificationSearchRequest(params)
        //}
    }
    validateClassificationSearchRequest(): boolean {
        let result = validateService('validateClassificationSearchRequest', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callClassificationSearchRequest(params: any): void {
        this.DocumentProcessService.ClassificationSearchRequest(params).subscribe((data: any) => {
            // if(isValidClassificationSearchRequestResponse(res)) {
            if (data) {
                // Set value
                this.input = data
                this.response = data

            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickClassificationSearchSameWord(): void {
        //if(this.validateClassificationSearchSameWord()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        const params = this.input

        // Call api
        this.callClassificationSearchSameWord(params)
        //}
    }
    validateClassificationSearchSameWord(): boolean {
        let result = validateService('validateClassificationSearchSameWord', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callClassificationSearchSameWord(params: any): void {
        this.DocumentProcessService.ClassificationSearchSameWord(params).subscribe((data: any) => {
            // if(isValidClassificationSearchSameWordResponse(res)) {
            if (data) {
                // Set value
                this.input = data
                this.response = data

            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickDocumentClassificationWordMenuLeft(index): void {
        //console.log(this.documentClassificationWordList)
        this.documentClassificationWordList.splice(index - 1, 0, this.documentClassificationWordList.splice(index, 1)[0])
        if (index - 1 == this.documentClassificationWordIndex || index == this.documentClassificationWordIndex)
            this.documentClassificationWordIndex--
        //console.log(this.documentClassificationWordList)
    }

    onClickDocumentClassificationWordMenuDelete(index): void {
        //console.log(this.documentClassificationWordList[index].id)

        if (index > 0) {
            this.onClickDocumentClassificationWordMenu(0)

            //if (this.documentClassificationWordList[index].id)
            //  this.documentClassificationWordList[index].is_deleted = true
            //else
            this.documentClassificationWordList_Deleted.push(this.documentClassificationWordList.splice(index, 1)[0])
        } else {
            this.onClickDocumentClassificationWordMenu(1)

            //if (this.documentClassificationWordList[index].id)
            //  this.documentClassificationWordList[index].is_deleted = true
            //else
            this.documentClassificationWordList_Deleted.push(this.documentClassificationWordList.splice(index, 1)[0])

            this.documentClassificationWordIndex = 0
        }
    }

    onClickDocumentClassificationWordMenuRight(index): void {
        //console.log(this.documentClassificationWordList)
        this.documentClassificationWordList.splice(index + 1, 0, this.documentClassificationWordList.splice(index, 1)[0])
        if (index + 1 == this.documentClassificationWordIndex || index == this.documentClassificationWordIndex)
            this.documentClassificationWordIndex++
        //console.log(this.documentClassificationWordList)
    }

    onClickDocumentClassificationWordMenu(index): void {
        //console.log(index)
        //console.log(this.documentClassificationWordList)

        this.documentClassificationWordList = this.documentClassificationWordList || [{}]
        this.documentClassificationWord = this.documentClassificationWord || {}

        if (this.documentClassificationWordList.length > index) {
            if (this.documentClassificationWordIndex >= 0) {
                Object.keys(this.documentClassificationWordList[this.documentClassificationWordIndex]).forEach((item: any) => {
                    this.documentClassificationWordList[this.documentClassificationWordIndex][item] = this.documentClassificationWord[item]
                })
                Object.keys(this.documentClassificationWord).forEach((item: any) => {
                    this.documentClassificationWordList[this.documentClassificationWordIndex][item] = this.documentClassificationWord[item]
                })
            }


            Object.keys(this.documentClassificationWordList[index]).forEach((item: any) => {
                this.documentClassificationWord[item] = this.documentClassificationWordList[index][item]
            })
            Object.keys(this.documentClassificationWord).forEach((item: any) => {
                this.documentClassificationWord[item] = this.documentClassificationWordList[index][item]
            })
        }

        //this.autoComplete.Update(this.documentClassificationWord)

        this.documentClassificationWord.word_syllable_sound = this.documentClassificationWord.word_syllable_sound || ""
        this.documentClassificationWord.word_syllable_count = this.documentClassificationWord.word_syllable_sound.split(" ").length

        this.documentClassificationWordIndex = index
    }

    onClickDocumentClassificationLanguage(): void {
        var pThis = this
        pThis.input.save010_document_classification_language_code = this.input.save010_document_classification_language_code || ""

        setTimeout(function () {
            var save010_document_classification_language_code_list = pThis.input.save010_document_classification_language_code.split("")

            pThis.master.save010DocumentClassificationLanguageList.forEach((item: any) => {
                item.is_check = save010_document_classification_language_code_list.indexOf(item.code) >= 0
            })

            pThis.is_DocumentClassificationLanguage = true
        }, 200)
    }

    onSelectDocumentClassificationLanguage(item: any): void {
        item.is_check = !item.is_check

        if (item.is_check) {
            this.input.save010_document_classification_language_code += (this.input.save010_document_classification_language_code == "" ? "" : " ") + item.code
        } else {
            this.input.save010_document_classification_language_code = this.input.save010_document_classification_language_code.replace(item.code, "").trim()
            this.input.save010_document_classification_language_code = this.input.save010_document_classification_language_code.replace("  ", " ")
        }
    }


    onClickSearchSimilarList(): void {
        // if(this.validateSearchSimilarList()) {
        // Open loading
        this.global.setLoading(true)

        var param = {
            request_number: this.search_similar.request_number,
            people_name_list_text: [this.search_similar.people_name, this.search_similar.people_name_condition],
            word_mark_list_text: [this.search_similar.word_mark, this.search_similar.word_mark_condition],
        }
        // Call api
        this.callSearchSimilarList(param)
        // }
    }
    //! <<< Call API >>>
    callSearchSimilarList(params: any): void {
        let pThis = this
        this.DocumentProcessService.SearchSimilarList(this.help.GetFilterParams(params, this.paginateSeachSimilar)).subscribe((data: any) => {
            // if(isValidSearchSimilarListResponse(res)) {
            if (data) {
                // Set value
                data.list.forEach((item: any) => {
                    item.request_number_registration_number = (item.request_number || "-") + "/" + (item.registration_number || "-")
                })

                this.listSeachSimilar = data.list || []
                this.help.PageSet(data, this.paginateSeachSimilar)
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        //this.global.setLoading(false)
    }

    onClickSearchSimilarShow(row_item: any) {
        this.global.setLoading(true)

        this.DocumentProcessService.ClassificationLoad(row_item.id).subscribe((data: any) => {
            if (data) {
                // Manage structure

                this.popup.is_search_similar_result = true
                setTimeout(() => {
                    document.getElementById("search_similar_result").focus()
                }, 100)

                this.search_similar.result = data

                this.onClickSearchSimilar_WordSelect(0)



                //this.documentClassificationWordList = data.document_classification_word_list || [{}]
                //this.documentClassificationWordIndex = -1
                //this.onClickDocumentClassificationWordMenu(0)

                //if (this.input.sound_file_physical_path && this.input.sound_file_physical_path != '') {
                //  this.player = new Audio(this.input.sound_file_physical_path)
                //  this.player_action = null
                //}

                //this.documentClassificationImageList = data.document_classification_image_list || [{}]
                //while (this.documentClassificationImageList.length <= 5) {
                //  this.documentClassificationImageList.push({})
                //}

                //this.documentClassificationSoundList = data.document_classification_sound_list || [{}]
                //while (this.documentClassificationSoundList.length <= 5) {
                //  this.documentClassificationSoundList.push({})
                //}



                // Set value
                // this.requestList = data.item_list
                // this.response.request01Load = data
                // this.isHasRequestList = true
            }
            // Close loading
            this.global.setLoading(false)
            this.automateTest.test(this)
        })
    }

    onClickSearchSimilar_WordSelect(index: any) {
        this.search_similar.result.document_classification_word_select = this.search_similar.result.document_classification_word_list[index]

        this.search_similar.result.document_classification_word_select.word_syllable_sound = this.search_similar.result.document_classification_word_select.word_syllable_sound || ""
        this.search_similar.result.document_classification_word_select.word_syllable_count = this.search_similar.result.document_classification_word_select.word_syllable_sound.split(" ").length
    }

    onClickSearchSimilarCopy(is_remove_old: boolean = true) {
        this.popup.is_search_similar = false
        this.popup.is_search_similar_result = false

        this.input.save010_document_classification_language_code = this.search_similar.result.save010_document_classification_language_code

        if (is_remove_old) {
            this.documentClassificationWordList.forEach((item: any) => { item.is_deleted = true })
            this.documentClassificationImageList.forEach((item: any) => { item.is_deleted = true })
            this.documentClassificationSoundList.forEach((item: any) => { item.is_deleted = true })
        }

        this.search_similar.result.document_classification_word_list.reverse()
        this.search_similar.result.document_classification_word_list.forEach((item: any) => {
            this.documentClassificationWordList.unshift({
                word_mark: item.word_mark,
                word_first_code: item.word_first_code,
                word_first_name: item.word_first_name,
                word_sound_last_code: item.word_sound_last_code,
                word_sound_last_other_code: item.word_sound_last_other_code,
                word_sound_last_other_name: item.word_sound_last_other_name,
                word_sound: item.word_sound,
                word_syllable_sound: item.word_syllable_sound,
                word_syllable_count: item.word_syllable_count,
            })
        })

        this.search_similar.result.document_classification_image_list.reverse()
        this.search_similar.result.document_classification_image_list.forEach((item: any) => {
            this.documentClassificationImageList.unshift({
                document_classification_image_code: item.document_classification_image_code,
                document_classification_image_name: item.document_classification_image_name,
            })
        })

        this.search_similar.result.document_classification_sound_list.reverse()
        this.search_similar.result.document_classification_sound_list.forEach((item: any) => {
            this.documentClassificationSoundList.unshift({
                document_classification_sound_code: item.document_classification_sound_code,
                document_classification_sound_name: item.document_classification_sound_name,
            })
        })

        this.documentClassificationWordIndex = -1
        this.onClickDocumentClassificationWordMenu(0)

        console.log(this.documentClassificationImageList)
        while (this.documentClassificationImageList.filter(r => !r.is_deleted).length < 5) {
            this.documentClassificationImageList.push({})
        }

        while (this.documentClassificationSoundList.filter(r => !r.is_deleted).length < 5) {
            this.documentClassificationSoundList.push({})
        }
    }


    onClickSearchSimilarListCopy(): void {
        this.documentClassificationWordList.forEach((item: any) => { item.is_deleted = true })
        this.documentClassificationImageList.forEach((item: any) => { item.is_deleted = true })
        this.documentClassificationSoundList.forEach((item: any) => { item.is_deleted = true })

        var params = this.listSeachSimilar.filter(r => r.is_check).map(r => r.id);
        params.reverse()
        this.onCallSearchSimilarListCopy(params)
    }
    onCallSearchSimilarListCopy(params): void {
        if (params.length > 0) {
            this.global.setLoading(true)
            this.DocumentProcessService.ClassificationLoad(params[0]).subscribe((data: any) => {
                params.splice(0, 1)

                if (data) {
                    this.search_similar.result = data

                    this.onClickSearchSimilar_WordSelect(0)

                    this.onClickSearchSimilarCopy(false)

                    this.popup['isPopupDuplicateOpen'] = true
                    //this.togglePopup('isPopupDuplicateOpen')
                }

                this.global.setLoading(false)
                this.onCallSearchSimilarListCopy(params)
            })
        }
    }

    onPressRequestNumberDuplicate(event: any): void {
        if (event.key == "Enter") {
            this.onCallRequestNumberDuplicate();
        }
    }
    onCallRequestNumberDuplicate(): void {
        if (this.input.request_number_duplicate.length > 5) {
            this.global.setLoading(true)

            this.DocumentProcessService.ClassificationLoadFromRequestNumber(this.input.request_number_duplicate).subscribe((data: any) => {
                //onClickSearchSimilarCopy() {
                //  this.popup.is_search_similar = false
                //  this.popup.is_search_similar_result = false

                //  this.input.save010_document_classification_language_code = this.search_similar.result.save010_document_classification_language_code
                if (data) {
                    this.search_similar.result = data

                    this.onClickSearchSimilar_WordSelect(0)

                    this.onClickSearchSimilarCopy()

                    this.togglePopup('isPopupDuplicateOpen')
                }

                this.global.setLoading(false)
            })
        }
    }

    onClickDocumentClassificationWordReset(): void {
        this.documentClassificationWord.word_mark = ""
        this.documentClassificationWord.word_first_code = ""
        this.documentClassificationWord.word_first_name = ""
        this.documentClassificationWord.word_sound_last_code = ""
        this.documentClassificationWord.word_sound_last_other_code = ""
        this.documentClassificationWord.word_sound_last_other_name = ""

        this.documentClassificationWord.word_sound = ""
        this.documentClassificationWord.word_syllable_sound = ""
        this.documentClassificationWord.word_syllable_count = 0
    }

    onOpenPopupSearchSimilar(): void {
        this.popup.is_search_similar = true
        setTimeout(() => {
            document.getElementById("search_similar").focus()
        }, 100)
    }
    onPressPopup(event: any, name: any): void {
        console.log(event)

        if (event.key == "Escape") {
            this.popup[name] = false

            if (name == "is_search_similar_result") {
                setTimeout(() => {
                    document.getElementById("search_similar").focus()
                }, 100)
            }
        }
    }

    //onMouseDownImageShow(event: any): void {
    //  console.log(event)
    //}
    //onMouseMoveImageShow(event: any): void {
    //  if (this.popup.is_image_show) {
    //    //this.popup.image_show_x = 20;
    //    //console.log(event.clientX + " " + event.pageX + " " + event.offsetX + " " + (event.clientX / event.pageX * event.offsetX))
    //    //document.getElementById("div_popup_body").scrollLeft = event.clientX / event.pageX * event.offsetX;

    //    var x = event.clientX / document.getElementById("div_popup_body").clientWidth *
    //      (document.getElementById("div_popup_body").scrollWidth - document.getElementById("div_popup_body").clientWidth)
    //    var y = event.clientY / document.getElementById("div_popup_body").clientHeight *
    //      (document.getElementById("div_popup_body").scrollHeight - document.getElementById("div_popup_body").clientHeight)
    //    //var y = event.clientY / event.pageY * event.offsetY

    //    document.getElementById("div_popup_body").scrollTo(x, y);

    //    //console.log(event)
    //    //console.log(event.clientX + " " + event.pageX)
    //    //console.log(document.getElementById("div_popup_body").scrollWidth + " " + document.getElementById("div_popup_body").clientWidth)
    //    //console.log(x + " " + y)
    //    //console.log(document.getElementById("div_popup_body").scrollTop)
    //    //console.log(document.getElementById("div_popup_body"))
    //    //console.log(document.getElementById("div_popup_body").scrollLeft)
    //  }
    //}
    //onMouseUpImageShow(event: any): void {
    //  console.log(event)
    //}

    onFocusInDocumentClassificationWord_WordFirst(): void {
        this.word_first_error = []
        //this.word_first_error = []
        //this.word_sound_last_code = []
        //this.word_sound_last_other_code = []
    }
    onFocusOutDocumentClassificationWord_WordFirst(): void {
        this.documentClassificationWord.word_first_code = this.documentClassificationWord.word_first_code.toUpperCase()

        var pThis = this
        setTimeout(function () {
            pThis.word_first_error = []
            if (pThis.documentClassificationWord.word_first_code && pThis.documentClassificationWord.word_first_code != "")
                //console.log(pThis.master.save010DocumentClassificationWordFirst)
                var save010DocumentClassificationWordFirst = pThis.master.save010DocumentClassificationWordFirst.filter(r => r.code == pThis.documentClassificationWord.word_first_code)
            if (save010DocumentClassificationWordFirst.length == 0) {
                pThis.input.error = "อักษรแรก: ไม่พบ [" + pThis.documentClassificationWord.word_first_code + "]"
                pThis.documentClassificationWord.word_first_code = ""
                pThis.documentClassificationWord.word_first_name = ""
                pThis.togglePopup('isPopupErrorOpen')
            } else {
                pThis.documentClassificationWord.word_first_code = save010DocumentClassificationWordFirst[0].code
                pThis.documentClassificationWord.word_first_name = save010DocumentClassificationWordFirst[0].name
            }
        }, 100)
    }

    onFocusOutDocumentClassificationWord_WordSoundLast(): void {
        var pThis = this
        setTimeout(function () {
            if (pThis.documentClassificationWord.word_sound_last_code && pThis.documentClassificationWord.word_sound_last_code != "")
                if (pThis.master.save010DocumentClassificationSoundLast.filter(r => r.code == pThis.documentClassificationWord.word_sound_last_code).length == 0) {
                    pThis.input.error = "เสียงท้าย: ไม่พบ [" + pThis.documentClassificationWord.word_sound_last_code + "]"
                    pThis.documentClassificationWord.word_sound_last_code = ""
                    pThis.documentClassificationWord.word_sound_last_name = ""
                    pThis.togglePopup('isPopupErrorOpen')
                }
        }, 100)
    }

    onFocusOutDocumentClassificationWord_WordSoundLastOther(): void {
        var pThis = this
        setTimeout(function () {
            if (pThis.documentClassificationWord.word_sound_last_other_code && pThis.documentClassificationWord.word_sound_last_other_code != "")
                if (pThis.master.save010DocumentClassificationSoundLastOther.filter(r => r.code == pThis.documentClassificationWord.word_sound_last_other_code).length == 0) {
                    pThis.input.error = "เสียงท้ายอีน: ไม่พบ [" + pThis.documentClassificationWord.word_sound_last_other_code + "]"
                    pThis.documentClassificationWord.word_sound_last_other_code = ""
                    pThis.documentClassificationWord.word_sound_last_other_name = ""
                    pThis.togglePopup('isPopupErrorOpen')
                }
        }, 100)
    }

    onFocusInDocumentClassificationWord_WordSound(): void {
        this.word_syllable_sound_error = []
    }

    onFocusoutDocumentClassificationWord_WordSound(): void {
        var error_list = []
        this.word_syllable_sound_error = []
        //console.log(this.master.save010DocumentClassificationSoundLast)
        var word_syllable_sound = []

        this.documentClassificationWord.word_syllable_sound.split(" ").forEach((item: any) => {
            if (this.master.save010DocumentClassificationSoundLast.filter(r => r.name == item).length == 0) {
                error_list.push(item)
                this.word_syllable_sound_error.push(item)
                //} else {
                //word_syllable_sound.push(item)
            }
        })

        //this.documentClassificationWord.word_syllable_sound = word_syllable_sound.join(" ")

        this.input.error = ""

        console.log(error_list)
        if (error_list.length > 0) {
            this.input.error = "เสียงแต่ละพยางค์: ไม่พบ [" + error_list.join(", ") + "]"
            //this.togglePopup('isPopupErrorOpen')
            console.log("dd")
        }

        //if (this.documentClassificationWord.word_syllable_sound && this.documentClassificationWord.word_syllable_sound != "")
        //console.log(this.documentClassificationWord.word_syllable_sound)
        var word_syllable_sound_list = this.documentClassificationWord.word_syllable_sound.split(" ")
        if (word_syllable_sound_list[word_syllable_sound_list.length - 1] != this.documentClassificationWord.word_sound_last_code) {
            //error_list.push(word_syllable_sound_list[word_syllable_sound_list.length - 1])
            this.word_syllable_sound_error.push(word_syllable_sound_list[word_syllable_sound_list.length - 1])

            if (this.input.error != "") {
                this.input.error += "<br>"
            }

            this.input.error += "เสียงแต่ละพยางค์: เสียงพยางค์สุดท้ายไม่ตรงกับเสียงท้าย [" + word_syllable_sound_list[word_syllable_sound_list.length - 1] + "," + this.documentClassificationWord.word_sound_last_code + "]"
            //this.togglePopup('isPopupErrorOpen')
            console.log("dd")
        }


        if (this.input.error != "") {
            this.togglePopup('isPopupErrorOpen')
        }

        this.documentClassificationWord.word_syllable_count = this.documentClassificationWord.word_syllable_sound.split(" ").length
    }

    dragElement(elmnt): void {
        console.log(elmnt)

        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (document.getElementById(elmnt.id + "Header")) {
            // if present, the header is where you move the DIV from:
            document.getElementById(elmnt.id + "Header").onmousedown = dragMouseDown;
        } else {
            // otherwise, move the DIV from anywhere inside the DIV:
            elmnt.onmousedown = dragMouseDown;
        }

        function dragMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementDrag;
        }

        function elementDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            pos1 = pos3 - e.clientX;
            pos2 = pos4 - e.clientY;
            pos3 = e.clientX;
            pos4 = e.clientY;
            // set the element's new position:
            elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
        }

        function closeDragElement() {
            // stop moving when mouse button is released:
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }


    dragExpandElement(element_div: any, element_button: any): void {

        var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
        if (document.getElementById(element_button.id)) {
            //  // if present, the header is where you move the DIV from:
            document.getElementById(element_button.id).onmousedown = dragExpandMouseDown;
        } else {
            //  // otherwise, move the DIV from anywhere inside the DIV:
            element_button.onmousedown = dragExpandMouseDown;
        }

        var width = element_div.offSetWidth
        var height = element_div.offSetHeight

        console.log(width)
        console.log(element_div.offSetWidth)

        function dragExpandMouseDown(e) {
            e = e || window.event;
            e.preventDefault();
            // get the mouse cursor position at startup:
            pos3 = e.clientX;
            pos4 = e.clientY;
            document.onmouseup = closeDragExpandElement;
            // call a function whenever the cursor moves:
            document.onmousemove = elementExpandDrag;

            width = element_div.offsetWidth
            height = element_div.offsetHeight
            console.log(width)
            console.log(element_div.offsetWidth)
        }

        function elementExpandDrag(e) {
            e = e || window.event;
            e.preventDefault();
            // calculate the new cursor position:
            //pos1 = pos3 - e.clientX;
            //pos2 = pos4 - e.clientY;
            //pos3 = e.clientX;
            //pos4 = e.clientY;
            pos1 = e.clientX;
            pos2 = e.clientY;
            // set the element's new position:
            //elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
            //elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";

            element_div.style.width = Math.max(575, width - (pos3 - pos1)) + "px";
            element_div.style.height = Math.max(300, height - (pos4 - pos2)) + "px";

            //console.log(width)
            //console.log(pos3)
            //console.log(pos1)
            //console.log(element_div.style.width)
            //console.log(element_div.style.height)
        }

        function closeDragExpandElement() {
            // stop moving when mouse button is released:
            document.onmouseup = null;
            document.onmousemove = null;
        }
    }

    loadData(data: any, is_load_all: boolean = true): void {
        //if (data.is_master) {
        //    data = this.documentClassification
        //    is_load_all = true
        //}

        this.tab_version_show_index = data

        if (is_load_all) {
            this.input = data

            if (this.input.sound_file_physical_path && this.input.sound_file_physical_path != '') {
                this.player = new Audio(this.input.sound_file_physical_path)
                this.player_action = null
            }

            //this.input.is_master = true
        } else {
            this.input.save010_document_classification_language_code = data.save010_document_classification_language_code

            if (data.trademark_2d_file_id) {
                this.input.trademark_2d_file_id = data.trademark_2d_file_id
                this.input.trademark_2d_file_name = data.trademark_2d_file_name
                this.input.trademark_2d_file_size = data.trademark_2d_file_size
                this.input.trademark_2d_physical_path = data.trademark_2d_physical_path
            }

            this.input.is_master = data.is_master

            if (data.document_classification_version_status_code == "DELETE") {
                this.input.is_master = false
            }
            //this.input.is_master = true
        }

        this.documentClassificationWordList = data.document_classification_word_list || []
        if (this.documentClassificationWordList.length == 0) {
            this.documentClassificationWordList.push({})
        }
        this.documentClassificationWordIndex = -1
        this.onClickDocumentClassificationWordMenu(0)

        if (this.input.is_master || is_load_all) {
            this.documentClassificationImageList = data.document_classification_image_list || [{}]
            while (this.documentClassificationImageList.length < 5) {
                this.documentClassificationImageList.push({})
            }
        } else {
            this.documentClassificationImageList = data.document_classification_image_list || []
        }

        if (this.input.is_master || is_load_all) {
            this.documentClassificationSoundList = data.document_classification_sound_list || [{}]
            while (this.documentClassificationSoundList.length < 5) {
                this.documentClassificationSoundList.push({})
            }
        } else {
            this.documentClassificationSoundList = data.document_classification_sound_list || []
        }

        console.log(this.input.is_master)
        console.log(this.documentClassificationWordList.length > 1)
        console.log(this.documentClassificationWordList.length > 1 && (this.input.is_master == null || this.input.is_master === true))
        console.log((this.input.is_master == null || this.input.is_master === true))
    }

    listData(data: any): void {
    }

    saveData(): any {
        let params = this.input
        let save_index = 1

        this.onClickDocumentClassificationWordMenu(0)

        //console.log(this.document_classification_word_list[0].word_first_code)

        //console.log(this.documentClassificationWordList)
        this.documentClassificationWordList.forEach((item: any) => { item.save_index = save_index++ })
        params.document_classification_word_list = this.documentClassificationWordList

        //console.log(params.document_classification_word_list)
        //console.log(this.documentClassificationWordList_Deleted)
        //console.log(this.documentClassificationWordList_Deleted.filter(r => r.id))
        this.documentClassificationWordList_Deleted.filter(r => r.id).forEach((item: any) => {
            item.is_deleted = true
            params.document_classification_word_list.push(item)
        })
        //console.log(params.document_classification_word_list)

        save_index = 1
        this.documentClassificationImageList.forEach((item: any) => { item.save_index = save_index++ })
        this.documentClassificationImageList.filter(r => (!r.document_classification_image_code || r.document_classification_image_code == "") && r.id).forEach((item: any) => { item.is_deleted = true })
        params.document_classification_image_list = this.documentClassificationImageList.filter(r => (r.document_classification_image_code && r.document_classification_image_code != "") || r.id)

        save_index = 1
        this.documentClassificationSoundList.forEach((item: any) => { item.save_index = save_index++ })
        this.documentClassificationSoundList.filter(r => (!r.document_classification_sound_code || r.document_classification_sound_code == "") && r.id).forEach((item: any) => { item.is_deleted = true })
        params.document_classification_sound_list = this.documentClassificationSoundList.filter(r => (r.document_classification_sound_code && r.document_classification_sound_code != "") || r.id)

        return params
    }

    togglePlayer(): void {
        if (this.player_action) {
            this.player.pause()
        } else {
            this.player.play()
        }
        this.player_action = !this.player_action
    }

    onMouseDownImageShow(event: any): void {
        this.input.mouse_down = true
        //this.input.mouse_move = [event.layerX, event.layerY]
        ////document.getElementById("div_image_show").scroll(10, 10);
        ////document.getElementById("div_image_show").scrollTo(10, 10);
    }
    onMouseMoveImageShow(event: any): void {
        if (this.input.mouse_down) {
            document.getElementById("div_image_show_image").scroll(
                document.getElementById("div_image_show_image").scrollLeft - event.movementX,
                document.getElementById("div_image_show_image").scrollTop - event.movementY);
        }
    }
    onMouseUpImageShow(event: any): void {
        //document.getElementById("div_image_show").scroll(10, 10);
        //document.getElementById("div_image_show").scrollTo(10, 10);
        this.input.mouse_down = false
    }

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }
    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name == "word_mark" || name == "word_mark") {
            value = value.toUpperCase()
        }

        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (name == "word_syllable_sound") {
            this.documentClassificationWord.word_syllable_count = this.documentClassificationWord.word_syllable_sound.split(" ").length
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }

    onClickImageShowZoomNext(): void {
        var index = this.image_show_zoom_list.indexOf(+this.popup.image_show_zoom)
        if (index < this.image_show_zoom_list.length - 1) {
            this.popup.image_show_zoom = +this.image_show_zoom_list[index + 1]
        }
    }
    onClickImageShowZoomPrevious(): void {
        var index = this.image_show_zoom_list.indexOf(+this.popup.image_show_zoom)
        if (index > 0) {
            this.popup.image_show_zoom = +this.image_show_zoom_list[index - 1]
        }
    }

    onClickDocumentClassificationImageDeleteAll(): void {
        this.documentClassificationImageList.forEach((item: any) => {
            item.is_deleted = true
        })
        while (this.documentClassificationImageList.filter(r => !r.is_deleted).length < 5) {
            this.documentClassificationImageList.push({})
        }
    }

    onClickDocumentClassificationImageDelete(item: any): void {
        item.is_deleted = true
        var index = this.documentClassificationImageList.indexOf(item)
        this.documentClassificationImageList.unshift(this.documentClassificationImageList.splice(index, 1)[0])
    }

    onClickDocumentClassificationSoundDeleteAll(): void {
        this.documentClassificationSoundList.forEach((item: any) => {
            item.is_deleted = true
        })
        while (this.documentClassificationSoundList.filter(r => !r.is_deleted).length < 5) {
            this.documentClassificationSoundList.push({})
        }
    }

    onClickDocumentClassificationSoundDelete(item: any): void {
        item.is_deleted = true
        var index = this.documentClassificationSoundList.indexOf(item)
        this.documentClassificationSoundList.unshift(this.documentClassificationSoundList.splice(index, 1)[0])
    }

    onClickDocumentClassificationWordMarkDictionaryOpen(): void {
        window.open("https://dict.longdo.com/search/" + this.documentClassificationWord.word_mark)
    }

    onClickDocumentClassificationUpload(): void {
        this.popup.is_edit_show = true
        //this.popup.edit_item = this.input

        setTimeout(() => {
            this.ImageEdit(this.input.trademark_2d_physical_path)
        }, 100)
    }


    //! <<< Upload (Image) >>>
    onClickUploadImage(event: any, obj: string, name: any, maxSize: any, item: any): void {
        if (event) {
            let file = event.target.files[0]
            event.target.value = ""
            //this.setFileData(file, obj, name, item)

            //this[obj][name].file = file
            let reader = new FileReader()
            reader.onload = e => {
                //this[obj][name].blob = reader.result
                //item.file = file
                //this.onClickRequestDocumentCollectFileUpload(item)

                //// TODO upload file process start
                //this.rowImageEdit.file_id = 0
                //this.rowImageEdit.physical_path = this.input.imageUpload.blob
                //// TODO upload file process end

                this.ImageEdit(reader.result)
            }
            reader.readAsDataURL(file)
        }
    }

    ImageEdit(physical_path: any): void {
        console.log("canvas - ImageEdit")

        //this.global.setLoading(false)
        this.modal["isModalImageEditOpen"] = true

        //var pThis = this
        //this.canvasStage = {
        //    erase: false
        //}
        //// Clear canvas before init
        if (this.canvas) {
            let canvasContainer = document.getElementById("canvas-container")
            while (canvasContainer.firstChild) {
                canvasContainer.removeChild(canvasContainer.firstChild)
            }
            let canvasStore = document.createElement("canvas")
            canvasStore.id = "canvas"
            canvasStore.width = 800
            canvasStore.height = 800
            document.getElementById("canvas-container").appendChild(canvasStore)
        }

        this.canvas = new fabric.Canvas("canvas", {
            isDrawingMode: false,
            freeDrawingBrush: "vline",
        })
        var canvas = this.canvas
        // Set eraser size and color
        canvas.freeDrawingBrush.width = 25
        canvas.freeDrawingBrush.color = "rgba(255,255,255,1)"
        // Add image into canvas
        const img = new Image()
        img.src = physical_path

        //console.log(physical_path)

        img.onload = () => {
            this.canvas_img = new fabric.Image(img, {
                left: 0,
                top: 0
            })

            console.log("img: " + img.width + " " + img.height)
            //    this.tempScale = 1;
            //    this.tempScaleWidth = 1;
            //    this.tempScaleHeight = 1;

            if (img.width > 800 || img.height > 800) {
                //Scale to width
                if (img.width > img.height) {
                    this.canvas_img.scaleToWidth(800)
                    this.tempScale = img.width / 800;
                } else {
                    //Scale to height
                    this.canvas_img.scaleToHeight(800)
                    this.tempScale = img.height / 800;
                }

                this.canvas_img.scaleToWidth(img.width / this.tempScale);
                this.canvas_img.scaleToHeight(img.height / this.tempScale);

                canvas.add(this.canvas_img)

                this.ImageEdit(this.canvas.toDataURL("image/jpeg"))

                return
            }

            canvas.add(this.canvas_img)

            //    this.refreshSizeImage();

            var left = -this.canvas_img.width / 2;
            var top = -this.canvas_img.height / 2;
            var width = this.canvas_img.width;
            var height = this.canvas_img.height;
            this.canvas_rectangle = [left, top, width, height]
            this.tempLeft = this.canvas_rectangle[0];
            this.tempTop = this.canvas_rectangle[1];
            this.tempWidth = this.canvas_rectangle[2];
            this.tempHeight = this.canvas_rectangle[3];

            this.canvas_img.clipTo = (ctx) => {
                console.log("clipTo 1")
                ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
            }

            this.canvas_img.selectable = !this.canvas_is_crop
            //console.log(this.canvas_img.selectable)

            //    console.log("nL:" + left + ", nT:" + top + ", nW:" + width + ", nH:" + height);

            //    var allObjects = this.canvas.getObjects().slice()
            //    var group = new fabric.Group(allObjects)
            //    this.canvas.clear().renderAll()
            //    this.canvas.add(group)
            //    this.canvas.setActiveObject(group)
            //    this.canvas_img = this.canvas.getObjects()[0]
            //    this.canvas_img.clipTo = function (ctx) {
            //        ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
            //    }
            //    console.log(this.canvas_rectangle);
        }

        var mousedown_x, mousedown_y, mouseup_x, mouseup_y
        var crop = false
        ////var disabled = false
        canvas.on("mouse:down", (event) => {
            if (!this.canvas_is_crop) return

            var r = document.getElementById('canvas').getBoundingClientRect()
            mousedown_x = event.e.pageX - r.left - window.scrollX
            mousedown_y = event.e.pageY - r.top - window.scrollY
            console.log("mouse:down: " + mousedown_x + " " + mousedown_y)

            crop = true
            if (this.canvas_el) {
                canvas.remove(this.canvas_el);
                this.canvas_el = null;
            }

            //window.scroll(0, 0)
            //setTimeout(() => {
            //}, 500)
        })
        canvas.on("mouse:up", (event) => {
            if (!this.canvas_is_crop) return
            if (!crop) return
            crop = false

            var r = document.getElementById('canvas').getBoundingClientRect()
            mouseup_x = event.e.pageX - r.left - window.scrollX
            mouseup_y = event.e.pageY - r.top - window.scrollY
            console.log("mouse:up: " + mouseup_x + " " + mouseup_y)

            this.canvas_el = new fabric.Rect({
                fill: 'transparent',
                originX: 'left',
                originY: 'top',
                stroke: '#ccc',
                strokeDashArray: [2, 2],
                opacity: 1,
                width: 1,
                height: 1,
                selectable: false,
            })
            canvas.add(this.canvas_el)
            canvas.bringToFront(this.canvas_el)

            //console.log(mousex + " " + mousey)
            //console.log(event.e.pageX + " " + event.e.pageY)
            //console.log(pos)

            this.canvas_el.left = mousedown_x
            this.canvas_el.top = mousedown_y
            this.canvas_el.width = mouseup_x - mousedown_x
            this.canvas_el.height = mouseup_y - mousedown_y

            //console.log(this.canvas_img.left + " " + this.canvas_img.top)
            //console.log(this.canvas_el.left + " " + this.canvas_el.top + " " + this.canvas_el.width + " " + this.canvas_el.height)

            var left = this.canvas_el.left - this.canvas_img.width / 2 - this.canvas_img.left;
            var top = this.canvas_el.top - this.canvas_img.height / 2 - this.canvas_img.top;
            var width = this.canvas_el.width;
            var height = this.canvas_el.height;

            //var left = this.canvas_el.left - this.canvas_img.left;
            //var top = this.canvas_el.top - this.canvas_img.top;
            //var width = this.canvas_el.width;
            //var height = this.canvas_el.height;

            this.canvas_rectangle = [left, top, width, height]
            console.log("Select Area: " + this.canvas_rectangle);

            //this.canvas.renderAll();
        })
    }
    // If check ยางลบ drawingMode = true
    onClickCheckBoxErase(value: boolean): void {
        if (this.canvas_is_crop) return
        this.canvas_is_erase = value

        //var pThis = this
        //setTimeout(() => {
        if (!value) {
            var allObjects = this.canvas.getObjects().slice()
            console.log(allObjects.length)
            var group = new fabric.Group(allObjects)
            this.canvas.clear().renderAll()
            this.canvas.add(group)
            this.canvas.setActiveObject(group)
            //this.canvas_img = this.canvas.getObjects()[0]
            //this.canvas_img.clipTo = (ctx) => {
            //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
            //}

            this.canvas.isDrawingMode = false
        } else {
            this.canvas.isDrawingMode = true
        }
        //}, 100)
    }
    refreshSizeImage(): void {
        //var pThis = this;

        //var allObjects = pThis.canvas.getObjects().slice()
        //var group = new fabric.Group(allObjects)
        //pThis.canvas.clear().renderAll()
        //pThis.canvas.add(group)
        //pThis.canvas.setActiveObject(group)
        //pThis.canvas_img = pThis.canvas.getObjects()[0]

        //pThis.canvas.renderAll();
    }
    onClickImageCrop(value: boolean): void {
        console.log(value)
        if (this.canvas_is_erase) return
        this.canvas_is_crop = value
        //var this = this;
        //var left;
        //var top;
        //var width;
        //var height;

        //var tCVW = this.canvas_img.width;
        //var tCVH = this.canvas_img.height;
        //this.refreshSizeImage();
        //var nW = tCVW / this.canvas_img.width;
        //var nH = tCVH / this.canvas_img.height;
        //this.tempScaleWidth *= nW;
        //this.tempScaleHeight *= nH;
        //console.log("changed W: " + tCVW + " to " + this.canvas_img.width + " Scale: " + nW + " TotalScale:" + this.tempScaleWidth);
        //console.log("changed H: " + tCVH + " to " + this.canvas_img.height + " Scale: " + nH + " TotalScale" + this.tempScaleHeight);

        //if ((tCVW != this.canvas_img.width || tCVH != this.canvas_img.height)) {
        //    if (this.tempScaleWidth < 1) {
        //        left = this.tempLeft / this.tempScaleWidth;
        //        width = this.tempWidth / this.tempScaleWidth;
        //    }
        //    else {
        //        left = this.tempLeft;
        //        width = this.tempWidth;
        //    }
        //    if (this.tempScaleHeight < 1) {
        //        top = this.tempTop / this.tempScaleHeight;
        //        height = this.tempHeight / this.tempScaleHeight;
        //    }
        //    else {
        //        top = this.tempTop;
        //        height = this.tempHeight;
        //    }
        //}
        //else {
        //    left = this.tempLeft;
        //    top = this.tempTop;
        //    width = this.tempWidth;
        //    height = this.tempHeight;
        //}
        //console.log("nL:" + left + ", nT:" + top + ", nW:" + width + ", nH:" + height);

        //this.canvas_rectangle = [left, top, width, height]
        //console.log(this.canvas_rectangle);
        //var allObjects = this.canvas.getObjects().slice()
        //var group = new fabric.Group(allObjects)
        //this.canvas.clear().renderAll()
        //this.canvas.add(group)
        //this.canvas.setActiveObject(group)
        //this.canvas_img = this.canvas.getObjects()[0]
        //this.canvas_img.clipTo = (ctx) => {
        //    ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
        //}
        //this.tempLeft = this.canvas_rectangle[0];
        //this.tempTop = this.canvas_rectangle[1];
        //this.tempWidth = this.canvas_rectangle[2];
        //this.tempHeight = this.canvas_rectangle[3];


        //window.scroll(0, 0)

        if (this.canvas_is_crop) {
            this.ImageEdit(this.canvas.toDataURL("image/jpeg"))
        } else {
            this.onClickImageCropConfirm(false)
        }
        //this.canvas.discardActiveObject().renderAll();
        //this.canvas_img.selectable = !value
    }
    onClickImageCropConfirm(is_confirm: boolean): void {
        console.log("onClickImageCropConfirm")

        if (this.canvas_el) {
            this.canvas.remove(this.canvas_el);
            this.canvas_el = null;
        }
        this.canvas_is_crop = false
        this.canvas_img.selectable = true

        if (is_confirm) {
            //let saveData = this.canvas.toDataURL("image/jpeg")
            //let params = {
            //    file: this.dataURItoBlob(saveData)
            //}
            //console.log(saveData)
            //    if (this.tempScaleWidth > 1) {
            //        this.canvas_rectangle[0] *= this.tempScaleWidth;
            //        this.canvas_rectangle[2] *= this.tempScaleWidth;
            //    }
            //    if (this.tempScaleHeight > 1) {
            //        this.canvas_rectangle[1] *= this.tempScaleHeight;
            //        this.canvas_rectangle[3] *= this.tempScaleHeight;
            //    }
            //setTimeout(() => {
            //this.canvas_img = this.canvas.getObjects()[0]
            //this.canvas_img.clipTo = (ctx) => {
            //  //  console.log("clipTo 2")
            //  //  this.canvas_img.clipTo = null

            //  //  //console.log(this.canvas_rectangle)
            //  //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
            //  //  //ctx.save();
            //ctx.drawImage(this, 100, 100, 100, 100, 100, 100, 100, 100)
            //  //  //var allObjects = this.canvas.getObjects().slice()
            //  //  //var group = new fabric.Group(allObjects)
            //  //  //this.canvas.clear().renderAll()
            //  //  //this.canvas.add(group)
            //  //  //this.canvas.setActiveObject(group)
            //  //  //this.canvas.isDrawingMode = false

            //var imgInstance = new fabric.Image(this.canvas_img, {
            //  left: 10,
            //  top: 10,
            //  width: 10,
            //  height: 10,
            //  clipTo: function (ctx) {
            //    ctx.rect(100, 100, 100, 100);
            //  }
            //});
            //this.canvas.add(imgInstance);

            //this.ImageEdit(this.canvas.toDataURL("image/jpeg"))


            const img = new Image()
            img.src = this.canvas.toDataURL("image/jpeg")
            this.canvas.clear()
            this.canvas.renderAll();

            img.onload = () => {
                var canvas_img = new fabric.Image(img, {
                    left: -(this.canvas_rectangle[0] + this.canvas.width / 2),
                    top: -(this.canvas_rectangle[1] + this.canvas.height / 2),
                    width: this.canvas_rectangle[2] + this.canvas_rectangle[0] + this.canvas.width / 2,
                    height: this.canvas_rectangle[3] + this.canvas_rectangle[1] + this.canvas.height / 2,
                })
                this.canvas.add(canvas_img)

                const new_img = new Image()
                new_img.src = this.canvas.toDataURL("image/jpeg")
                this.canvas.clear()
                this.canvas.renderAll();

                new_img.onload = () => {
                    this.canvas_img = new fabric.Image(new_img, {
                        left: this.canvas_rectangle[0] + this.canvas.width / 2,
                        top: this.canvas_rectangle[1] + this.canvas.height / 2,
                        width: this.canvas_rectangle[2],
                        height: this.canvas_rectangle[3],
                    })
                    this.canvas.add(this.canvas_img)
                    this.canvas.setActiveObject(this.canvas_img)

                    //this.canvas_img.clipTo = (ctx) => {
                    //  console.log("clipTo 2")
                    //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
                    //}

                    this.canvas.renderAll();
                }
            }

            //}
            //console.log("d")
            //this.canvas.renderAll();
            //}, 100)
        } else {
            var left = this.tempLeft;
            var top = this.tempTop;
            var width = this.tempWidth;
            var height = this.tempHeight;
            this.canvas_rectangle = [left, top, width, height]
        }
        //if (is_confirm) {
        //    this.tempLeft = this.canvas_rectangle[0];
        //    this.tempTop = this.canvas_rectangle[1];
        //    this.tempWidth = this.canvas_rectangle[2];
        //    this.tempHeight = this.canvas_rectangle[3];
        //}
    }

    onClickDocumentProcessCollectHistoryImageSave(): void {
        let saveData = this.canvas.toDataURL("image/jpeg")
        let params = {
            file: this.dataURItoBlob(saveData)
        }
        this.callUpload(params)
    }
    dataURItoBlob(dataURI: any): any {
        var byteString = atob(dataURI.split(",")[1])

        var mimeString = dataURI
            .split(",")[0]
            .split(":")[1]
            .split("")[0]

        var ab = new ArrayBuffer(byteString.length)
        var ia = new Uint8Array(ab)
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i)
        }

        var bb = new Blob([ab], { type: mimeString })
        return bb
    }
    callUpload(params: any): void {
        console.log(params)
        const formData = new FormData()
        formData.append("file", params.file, params.file.name || "img.jpg")

        //this.modal["isModalImageEditOpen"] = false
        this.global.setLoading(true)
        this.uploadService.upload(formData).subscribe((data: any) => {
            //this.global.setLoading(false)
            //this.modal["isModalImageEditOpen"] = false

            if (data) {
                //this.rowImageEdit.file_id = data.id
                this.callDocumentProcessCollectHistoryImageSend(data)
            }
        })
    }

    callDocumentProcessCollectHistoryImageSend(data): void {
        if (data.id) {
            console.log(data)
            //this.popup.edit_item["file_id"] = data.id

            this.input.trademark_2d_file_id = data.id
            this.input.trademark_2d_file_name = data.file_name
            this.input.trademark_2d_file_size = data.file_size
            this.input.trademark_2d_physical_path = data.file_url

            //this.DocumentProcessService.DocumentProcessCollectHistoryImageSend(this.popup.edit_item).subscribe((data: any) => {
            //this.onClickClassificationHistoryList()
            this.global.setLoading(false)
            this.popup.is_edit_show = false
            //})
        }
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

    //! <<< Popup >>>
    togglePopup(name: string): void {
        if (!this.popup[name]) { // Is open
            this.popup[name] = true
        } else { // Is close
            this.popup[name] = false
            //if (name === 'isPopupSendOpen') {
            //  window.location.href = "document-process-classification-do/edit/" + this.popup.isPopupSendOpenID
            //}
        }
    }

    //Test For Active Test By su
    testactive(index) {
        this.menutab = 'menutab' + index
        console.log('menutab' + index)
    }
    documentClassificationIndexUp(list: any[], item: any) {
        var index = list.indexOf(item)
        if (index > 0)
            list.splice(index, 0, list.splice(index - 1, 1)[0])
    }
    documentClassificationIndexDown(list: any[], item: any) {
        var index = list.indexOf(item)
        if (list.length > index + 1)
            list.splice(index, 0, list.splice(index + 1, 1)[0])
    }
}
