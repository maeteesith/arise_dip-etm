import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  clone,
  displayDateServer
} from '../../helpers'

@Component({
  selector: "app-document-process-classification",
  templateUrl: "./document-process-classification.component.html",
  styleUrls: ["./document-process-classification.component.scss"]
})
export class DocumentProcessClassificationComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  // List Classification
  public listClassification: any[]
  public paginateClassification: any
  public perPageClassification: number[]
  // Response Classification
  public responseClassification: any
  public listClassificationEdit: any


  // List DocumentProcess
  public listDocumentProcess: any[]
  public paginateDocumentProcess: any
  public perPageDocumentProcess: number[]
  // Response DocumentProcess
  public responseDocumentProcess: any

  //label_save_combobox || label_save_radio
  public documentClassificationStatusList: any[]
  public requestItemTypeCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}

    this.input = {
      id: null,
      request_number: '',
      document_classification_status_code: '',
      request_item_type_code: '',
      //saved_process_date_start: getMoment(),
      //saved_process_date_start: getMoment(),
      //saved_process_date_end: getMoment(),
      //updated_date_start: getMoment(),
      //updated_date_end: getMoment(),
    }
    this.listClassification = []
    this.paginateClassification = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateClassification.id = 'paginateClassification'
    this.perPageClassification = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listDocumentProcess = []

    //Master List
    this.master = {
      documentClassificationStatusList: [],
      requestItemTypeCodeList: [],
    }
    //Master List



    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initDocumentProcessClassification().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.documentClassificationStatusList.unshift({ "code": "", "name": "ทั้งหมด" });
        this.input.document_classification_status_code = "WAIT_DO"
        this.master.requestItemTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" });

      }
      if (this.editID) {
        this.DocumentProcessService.ClassificationLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            Object.keys(data).forEach((item: any) => {
              this.input[item] = data[item]
            })

            this.listClassification = data.classification_list || []
            data.classification_list.map((item: any) => { item.is_check = false; return item })
            this.changePaginateTotal((this.listClassification || []).length, 'paginateClassification')


            // Set value
            // this.requestList = data.item_list
            // this.response.request01Load = data
            // this.isHasRequestList = true
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      }

      this.route.queryParams.subscribe((param_url: any) => {
        if (param_url.query) {
          //this.input.saved_process_date = ""
          if (param_url.query == "save_wait_do") {
            this.input.document_classification_status_code = "WAIT_DO"
          } else if (param_url.query == "save_send") {
            this.input.document_classification_status_code = "SEND"
          }
          this.onClickClassificationList()
        }
      });

      this.global.setLoading(false)
      this.automateTest.test(this)
    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  onClickClassificationList(): void {
    // if(this.validateClassificationList()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    // const params = {
    //     request_number: this.input.request_number,
    //     document_classification_status_code: this.input.document_classification_status_code,
    //     request_item_type_code: this.input.request_item_type_code,
    //     saved_process_date_start: this.input.saved_process_date_start,
    //     saved_process_date_end: this.input.saved_process_date_end,
    //     updated_date_start: this.input.updated_date_start,
    //     updated_date_end: this.input.updated_date_end,
    //     classification_list: this.listClassification || [],

    // }
    //const params = {
    //    page_index: +this.paginateClassification.currentPage,
    //    item_per_page: +this.paginateClassification.itemsPerPage,
    //    order_by: 'created_date',
    //    is_order_reverse: false,
    //    search_by: [{
    //        key: 'request_number',
    //        values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
    //        operation: 5
    //    }, {
    //        key: 'document_classification_status_code',
    //        value: this.input.document_classification_status_code,
    //        operation: 5
    //    }, {
    //        key: 'request_item_type_code',
    //        value: this.input.request_item_type_code,
    //        operation: 0
    //    }, {
    //        key: 'saved_process_date',
    //        value: displayDateServer(this.input.saved_process_date_start),
    //        operation: 3
    //    }, {
    //        key: 'saved_process_date',
    //        value: displayDateServer(this.input.saved_process_date_end),
    //        operation: 4
    //    }, {
    //        key: 'updated_date',
    //        value: displayDateServer(this.input.updated_date_start),
    //        operation: 3
    //    }, {
    //        key: 'updated_date',
    //        value: displayDateServer(this.input.updated_date_end),
    //        operation: 4
    //    }]
    //}
    // Call api
    const params = {
      request_number: this.input.request_number,
      document_classification_status_code: this.input.document_classification_status_code,
      request_item_type_code: this.input.request_item_type_code,
      saved_process_start_date: this.input.saved_process_date_start,
      saved_process_end_date: this.input.saved_process_date_end,
      updated_start_date: this.input.updated_date_start,
      updated_end_date: this.input.updated_date_end,
    }

    this.callClassificationList(params)
    // }
  }

  //! <<< Call API >>>
  callClassificationList(params: any): void {
    this.DocumentProcessService.ClassificationList(this.help.GetFilterParams(params, this.paginateClassification)).subscribe((data: any) => {
      // if(isValidClassificationListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
      }
      //if (data) {
      //  // Set value
      //  this.listDocumentProcess = data || []
      //  this.listClassification = data || []

      //  this.changePaginateTotal(data.length, 'paginateClassification')
      //}
      // }
      // Close loading
      this.global.setLoading(false)
      this.automateTest.test(this, { list: 1 })
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }
  onClickClassificationPrint(): void {
    ////if(this.validateClassificationPrint()) {
    //// Open loading
    //this.global.setLoading(true)
    //// Set param
    //const params = this.input

    //// Call api
    //this.callClassificationPrint(params)
    ////}

    window.open("http://localhost:5000/pdf/DocumentProcessClassification/" + this.listClassification.filter(r => r.is_check).map((item: any) => { return item.request_number }).join("|"))
  }
  //validateClassificationPrint(): boolean {
  //let result = validateService('validateClassificationPrint', this.input)
  //this.validate = result.validate
  //return result.isValid
  //}
  //! <<< Call API >>>
  callClassificationPrint(params: any): void {
    this.DocumentProcessService.ClassificationPrint(params).subscribe((data: any) => {
      // if(isValidClassificationPrintResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listClassification = data.classification_list || []
        this.changePaginateTotal((this.listClassification || []).length, 'paginateClassification')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickClassificationSend(): void {
    //if(this.validateClassificationSend()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callClassificationSend(params)
    //}
  }
  validateClassificationSend(): boolean {
    let result = validateService('validateClassificationSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callClassificationSend(params: any): void {
    this.DocumentProcessService.ClassificationSend(params).subscribe((data: any) => {
      // if(isValidClassificationSendResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listClassification = data.classification_list || []
        this.changePaginateTotal((this.listClassification || []).length, 'paginateClassification')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickClassificationAdd(): void {
    this.listClassification.push({
      index: this.listClassification.length + 1,
      request_date_text: null,
      saved_process_date_text: null,
      people_name: null,
      request_item_sub_type_1_code_text: null,
      document_classification_date_text: null,
      document_classification_remark: null,
      document_classification_status_name: null,

    })
    this.changePaginateTotal(this.listClassification.length, 'paginateClassification')
  }
  onChangeCheckboxClassification(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
  onChangeSelectClassification(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }
  onClickClassificationMadridStatus(id: any): void { }
  onClickClassificationId(id: any): void {
    var win = window.open("document-process-classification-do/edit/" + id)
  }



  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }



  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }


  listData(data: any): void {
    this.listClassification = data.list || []
    this.help.PageSet(data, this.paginateClassification)
  }


  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginateClassification') {
      this.onClickClassificationList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
}
