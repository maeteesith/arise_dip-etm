import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Component, Input, Output, OnInit, EventEmitter, Inject } from "@angular/core";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { CONSTANTS, validateService } from "../../helpers";


@Component({
  selector: "app-modal-madrid-role03-holder",
  templateUrl: "./modal-madrid-role03-holder.component.html",
  styleUrls: ["./modal-madrid-role03-holder.component.scss",
  './../../../assets/theme/styles/madrid/madrid.scss'],
})
export class ModalMadridRole03Holder implements OnInit {

 
  constructor( 
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalMadridRole03Holder>,
    
    @Inject(MAT_DIALOG_DATA) public data: any)  {}

  ngOnInit() {
    
    
    
  }

  closeModal() {
    
    this.dialogRef.close();
  }

 
 
  }

