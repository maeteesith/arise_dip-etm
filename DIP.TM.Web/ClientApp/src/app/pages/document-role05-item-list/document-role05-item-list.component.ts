import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-document-role05-item-list",
    templateUrl: "./document-role05-item-list.component.html",
    styleUrls: ["./document-role05-item-list.component.scss"]
})
export class DocumentRole05ItemListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    // List DocumentRole05Item
    public listDocumentRole05Item: any[]
    public paginateDocumentRole05Item: any
    public perPageDocumentRole05Item: number[]
    // Response DocumentRole05Item
    public responseDocumentRole05Item: any
    public listDocumentRole05ItemEdit: any


    // List DocumentProcess
    public listDocumentProcess: any[]
    public paginateDocumentProcess: any
    public perPageDocumentProcess: number[]
    // Response DocumentProcess
    public responseDocumentProcess: any

    //label_save_combobox || label_save_radio
    public postRoundTypeCodeList: any[]
    public departmentGroupCodeList: any[]
    public documentRole05StatusCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial

    public tableList: any


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            //post_round_document_post_date: getMoment(),
            post_round_type_code: '',
            department_group_code: '',
            document_Role05_status_code: '',
            request_number: '',
        }
        this.listDocumentRole05Item = []
        this.paginateDocumentRole05Item = CONSTANTS.PAGINATION.INIT
        this.perPageDocumentRole05Item = CONSTANTS.PAGINATION.PER_PAGE

        this.listDocumentProcess = []

        //Master List
        this.master = {
            postRoundTypeCodeList: [],
            departmentGroupCodeList: [],
            documentRole05StatusCodeList: [],
        }
        //Master List


        if (!this.tableList) {
            this.tableList = {
                column_list: {
                    index: "#",
                    request_number: "เลขที่คำขอ",
                    book_end_date: "วันที่ส่งมา",
                    document_role04_from_department_name: "กลุ่มงานที่ส่งมา",
                    document_role04_type_name: "ประเภท",
                    document_role05_receive_date: "วันที่โอนงาน",
                    document_role05_receiver_by_name: "ผู้รับงาน",
                    document_role04_receive_remark: "รายละเอียดเพิ่มเติม",
                    document_role05_status_name: "สถานะ",
                },
                //command_item: [{
                //  name: "item_edit"
                //}],
            }
        }


        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //


        this.contactAddress = {
            address_type_code: "OWNER"
        }
        this.modalAddress = {}

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.route.queryParams.subscribe((param_url: any) => {
        })

        this.forkJoinService.initDocumentRole05ItemList().subscribe((data: any) => {
            if (data) {
                this.help.Clone(data, this.master)
                //this.master.documentRole4TypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.document_role04_type_code = ""
                this.master.documentRole05StatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.document_role05_status_code = ""

                this.master.documentRole04SendTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.document_role04_send_type_code = ""

                this.master.documentRole04TypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.document_role04_type_code = ""
                //this.master.departmentGroupCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.department_group_code = ""
                //this.master.documentRole05StatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.document_Role05_status_code = ""

            }
            if (this.editID) {
                this.DocumentProcessService.DocumentRole05ItemLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }

        })
    }

    constructor(
        private auth: Auth,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }

    onClickCommand($event) {
        console.log($event)
        if ($event) {
            if ($event.command) {
                if ($event.command == "item_edit") {
                    window.open("document-role05-item/edit/" + $event.object_list[0].id)
                }
            } else {
                this.onClickDocumentRole05ItemList($event)
            }
        }
    }
    onClickDocumentRole05ItemList(paging = null): void {
        console.log(paging)
        // if(this.validateDocumentRole05ItemList()) {
        // Open loading
        // Call api
        var param = {
            document_role04_receive_send_start_date: this.input.document_role04_receive_send_start_date,
            document_role04_receive_send_end_date: this.input.document_role04_receive_send_end_date,
            document_role05_receive_start_date: this.input.document_role05_receive_start_date,
            document_role05_receive_end_date: this.input.document_role05_receive_end_date,

            request_number: this.input.request_number,

            document_role05_status_code: this.input.document_role05_status_code,
            document_role04_type_code: this.input.document_role04_type_code,
            document_role04_send_type_code: this.input.document_role04_send_type_code,
        }

        this.callDocumentRole05ItemList(this.help.GetFilterParams(param, paging))
        // }
    }
    //! <<< Call API >>>
    callDocumentRole05ItemList(params: any): void {
        params.filter_queries = ["!string.IsNullOrEmpty(document_role05_status_code)"]

        this.global.setLoading(true)
        this.DocumentProcessService.List("DocumentRole04", params).subscribe((data: any) => {
            // if(isValidDocumentRole05ItemListResponse(res)) {
            if (data) {
                // Set value
                //this.tableList.SetPaging(data)
                this.tableList.SetDataList(data)
                //this.listData(data)
                this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickDocumentRole05ItemDocumentRole05ItemAdd(): void {
        this.listDocumentRole05Item.push({
            index: this.listDocumentRole05Item.length + 1,
            request_number: null,
            request_date_text: null,
            request_item_sub_type_1_code_text: null,
            document_Role05_date_text: null,
            department_group_name: null,
            book_by_name: null,
            document_Role05_receive_date_text: null,
            document_Role05_receiver_by_name: null,
            document_Role05_remark: null,
            document_Role05_status_name: null,

        })
        this.changePaginateTotal(this.listDocumentRole05Item.length, 'paginateDocumentRole05Item')
    }

    onClickDocumentRole05ItemDocumentRole05ItemEdit(item: any): void {
        var win = window.open("/" + item.id)
    }

    onClickDocumentRole05ItemAutoSplit(): void {
        this.DocumentProcessService.DocumentRole05ItemAutoSplit().subscribe((data: any) => {
            // if(isValidDocumentRole05ItemListResponse(res)) {
            if (data) {
                // Set value
                //this.tableList.SetPaging(data)
                this.onClickDocumentRole05ItemList()
                //this.tableList.SetDataList(data)
                //this.listData(data)
                //this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listDocumentRole05Item = data.documentRole05item_list || []
        this.help.PageSet(data, this.paginateDocumentRole05Item)

    }

    //listData(data: any): void {
    //  //this.listDocumentRole05Item = data.list || []
    //  //this.help.PageSet(data, this.paginateDocumentRole05Item)
    //  this.tableList.SetDataList(data)
    //}

    saveData(): any {
        let params = this.input

        params.documentRole05item_list = this.listDocumentRole05Item || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }
    displayMoney(value: any): any {
        return displayMoney(value);
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

}
