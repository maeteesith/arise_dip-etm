import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
import { Auth } from "../../auth";
import { AppealService } from '../../services/Appeal.service'
import { ForkJoinService } from '../../services/fork-join.service'
import { AppealRequestList } from '../../helpers/appealMaster'
import {
  CONSTANTS,
  getMoment,
  clone,
  validateService,
  displayDate,
  displayMoney,
  displayString,
} from '../../helpers'
import { ROUTE_PATH } from 'src/app/helpers/constants';

@Component({
  selector: 'app-appeal-role02-case-summary-list',
  templateUrl: './appeal-role02-case-summary-list.component.html',
  styleUrls: ['./appeal-role02-case-summary-list.component.scss']
})
export class AppealRole02CaseSummaryListComponent implements OnInit {

  constructor(
    private router: Router,
    private auth: Auth,
    private appealServices: AppealService,
    private forkJoinService: ForkJoinService,
    private global: GlobalService
  ) { }

  public input: any
  public requestList: any[]
  public dataList: any[]

  public paginateItem: any
  public perPageItem: number[]
  public perPage: any[]

  public listItem: any[]

  ngOnInit() {
    this.input = {
      request_number: null,
      request_type: "03",
       start_date: getMoment(),
      end_date: getMoment(),
    }
    this.requestList = AppealRequestList.map(Item => {return Item})
    this.callInit()
    this.paginateItem = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateItem.id = 'paginateItem'
    this.perPage = [10, 50, 100]
  }

  filter(){
    const params = {
      request_number: this.input.request_number,
      request_type: this.input.request_type,
      start_date: this.input.start_date,
      end_date: this.input.end_date,
    }
    // this.appealServices.CaseSummaryFilter(params).subscribe((data: any) => {
    //   console.log(data)
    // })
  }

  callInit(): void{
    this.global.setLoading(true)
    this.appealServices.GetRequestCaseSummaryList({}).subscribe((data: any) => {
      if(data){
         this.dataList = data.map(Item => {return Item})
         this.setPagination(this.dataList, this.paginateItem.id)
        //this.changePaginateTotal((this.dataList || []).length, 'paginateItem')
        console.log(data)
      } else {
        this.dataList = []
        this.setPagination(this.dataList, this.paginateItem.id)
      }
      this.global.setLoading(false)
    })
  }

  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  setPagination(list: any[], paginate: string): void {
    switch(paginate){
      case this.paginateItem.id: this.dataList = list; break;
    }
    this.changePaginateTotal(list.length, paginate)
    this[paginate].currentPage = 1 
  }

  go(params: any): void {
    // console.log(item)
    //this.router.navigate(['edit/'+params]); 
    //this.router.navigate([ROUTE_PATH.APPEAL_ROLE02_CASE_SUMMARY_EDIT.LINK, params])
    window.open([ROUTE_PATH.APPEAL_ROLE02_CASE_SUMMARY_EDIT.LINK] + `/${params}`)
  }

}
