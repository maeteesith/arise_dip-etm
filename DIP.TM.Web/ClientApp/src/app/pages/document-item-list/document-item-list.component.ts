import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import {
    CONSTANTS,
    getMoment,
    validateService,
    clone,
    displayDateServer
} from '../../helpers'

@Component({
    selector: "app-document-item-list",
    templateUrl: "./document-item-list.component.html",
    styleUrls: ["./document-item-list.component.scss"]
})
export class DocumentItemListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    // List Document
    public listDocument: any[]
    public paginateDocument: any
    public perPageDocument: number[]
    // Response Document
    public responseDocument: any
    public listDocumentEdit: any


    // List DocumentProcess
    public listDocumentProcess: any[]
    public paginateDocumentProcess: any
    public perPageDocumentProcess: number[]
    // Response DocumentProcess
    public responseDocumentProcess: any

    //label_save_combobox || label_save_radio
    public documentTypeCodeList: any[]
    public documentStatusCodeList: any[]
    //label_save_combobox || label_save_radio

    public autocompleteListCheckerList: any
    public is_autocomplete_CheckerList_show: any
    public is_autocomplete_CheckerList_load: any
    public autocompleteListDocumentPeopleList: any
    public is_autocomplete_DocumentPeopleList_show: any
    public is_autocomplete_DocumentPeopleList_load: any
    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            saved_process_date: getMoment(),
            document_type_code: '',
            documentor_name: '',
            request_number: '',
            document_status_code: '',
            document_people_name_receive: '',
            document_classification_by_name: ''
        }
        this.listDocument = []
        this.paginateDocument = clone(CONSTANTS.PAGINATION.INIT)
        this.paginateDocument.id = 'paginateDocument'
        this.perPageDocument = clone(CONSTANTS.PAGINATION.PER_PAGE)

        this.listDocumentProcess = []

        //Master List
        this.master = {
            documentTypeCodeList: [],
            documentStatusCodeList: [],
        }
        //Master List

        this.autocompleteListCheckerList = []
        this.is_autocomplete_CheckerList_show = false
        this.is_autocomplete_CheckerList_load = false
        this.autocompleteListDocumentPeopleList = []
        this.is_autocomplete_DocumentPeopleList_show = false
        this.is_autocomplete_DocumentPeopleList_load = false


        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initDocumentItemList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.documentTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" });
                this.master.documentStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });

            }
            if (this.editID) {
                let pThis = this
                this.DocumentProcessService.DocumentItemLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        pThis.loadData(data)
                    }
                    // Close loading
                    pThis.global.setLoading(false)
                    this.automateTest.test(this)
                })
            }

            this.route.queryParams.subscribe((param_url: any) => {
                if (param_url.query) {
                    this.input.saved_process_date = ""
                    if (param_url.query == "all_wait_documenting") {
                        this.input.document_status_code = "WAIT_DOCUMENTING"
                    } else if (param_url.query == "all_received") {
                        this.input.document_status_code = "RECEIVED_DOCUMENTING"
                    }
                    this.onClickDocumentItemList()
                }
            });

            this.global.setLoading(false)
            this.automateTest.test(this)
        })
    }

    constructor(
        private help: Help,
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }


    // Autocomplete
    autocompleteChangeCheckerList(object: any, name: any, item_per_page: any = 5, length: any = 1): void {
        //if (object[name].length >= length) {
        //    this.is_autocomplete_CheckerList_show = true

        //    let params = {
        //        documentor_name: object[name],
        //        paging: {
        //            item_per_page: item_per_page
        //        },
        //    }

        //    this.callAutocompleteChangeCheckerList(params)
        //}
    }
    autocompleteBlurCheckerList(object: any, name: any, item_per_page: any = 5): void {
        //let pThis = this
        //setTimeout(function () {
        //    pThis.is_autocomplete_CheckerList_show = false
        //}, 200)
    }
    callAutocompleteChangeCheckerList(params: any): void {
        //if (this.input.is_autocomplete_CheckerList_load) return
        //this.input.is_autocomplete_CheckerList_load = true
        //let pThis = this
        //this.DocumentProcessService.DocumentItemCheckerList(params).subscribe((data: any) => {
        //    if (data) {
        //        if (data.length == 1) {
        //            setTimeout(function () {
        //                pThis.autocompleteChooseCheckerList(data[0])
        //            }, 200)
        //        } else {
        //            pThis.autocompleteListCheckerList = data
        //        }
        //    }
        //})
        //this.input.is_autocomplete_CheckerList_load = false
    }
    autocompleteChooseCheckerList(data: any): void {
        //console.log(data)

        //this.input = data

        //this.listDocument = data.document_list || []
        //this.changePaginateTotal(this.listDocument.length, 'paginateDocument')


        //this.is_autocomplete_CheckerList_show = false
    }


    // Autocomplete
    autocompleteChangeDocumentPeopleList(object: any, name: any, item_per_page: any = 5, length: any = 1): void {
        if (object[name].length >= length) {
            this.is_autocomplete_DocumentPeopleList_show = true

            let params = {
                filter: { name: object[name] },
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeDocumentPeopleList(params)
        }
    }
    autocompleteBlurDocumentPeopleList(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_DocumentPeopleList_show = false
        }, 200)
    }
    callAutocompleteChangeDocumentPeopleList(params: any): void {
        if (this.input.is_autocomplete_DocumentPeopleList_load) return
        this.input.is_autocomplete_DocumentPeopleList_load = true
        let pThis = this
        this.DocumentProcessService.DocumentItemDocumentPeopleList(params).subscribe((data: any) => {
            if (data) {
                if (data.length == 1) {
                    setTimeout(function () {
                        pThis.autocompleteChooseDocumentPeopleList(data[0])
                    }, 200)
                } else {
                    console.log(pThis.is_autocomplete_DocumentPeopleList_show)
                    console.log(pThis.autocompleteListDocumentPeopleList)
                    pThis.autocompleteListDocumentPeopleList = data
                }
            }
        })
        this.input.is_autocomplete_DocumentPeopleList_load = false
    }
    autocompleteChooseDocumentPeopleList(data: any): void {
        console.log(data)

        this.input.document_people_by_receive = data.id
        this.input.document_people_name_receive = data.name

        this.is_autocomplete_DocumentPeopleList_show = false
        this.automateTest.test(this, { receiver: 1 })
    }


    onClickDocumentItemReceive(): void {
        //if(this.validateDocumentItemReceive()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        var param = {
            ... this.input,
            document_list: this.listDocument,
        }
        this.callDocumentItemReceive(param)
        //}
    }
    validateDocumentItemReceive(): boolean {
        let result = validateService('validateDocumentItemReceive', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callDocumentItemReceive(params: any): void {
        let pThis = this
        this.DocumentProcessService.DocumentItemReceive(params).subscribe((data: any) => {
            // if(isValidDocumentItemReceiveResponse(res)) {
            if (data) {
                // Set value
                pThis.onClickDocumentItemList()
            }
            // }
            // Close loading
            this.global.setLoading(false)
            this.automateTest.test(this, { DocumentItemReceive: 1 })
        })
    }
    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }

    onClickDocumentItemList(): void {
        // if(this.validateDocumentItemList()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callDocumentItemList(this.saveData())
        // }
    }
    //! <<< Call API >>>
    callDocumentItemList(params: any): void {
        let pThis = this
        this.DocumentProcessService.DocumentItemList(this.help.GetFilterParams(params, this.paginateDocument)).subscribe((data: any) => {
            // if(isValidDocumentItemListResponse(res)) {
            if (data) {
                // Set value
                pThis.listData(data)
            }
            // }
            // Close loading
            this.global.setLoading(false)
            this.automateTest.test(this, { list: 1 })
        })
    }


    onClickDocumentItemDocumentAdd(): void {
        this.listDocument.push({
            index: this.listDocument.length + 1,
            saved_process_date_text: null,
            request_number: null,
            request_type_value_3: null,
            request_date_text: null,
            request_item_sub_type_1_code: null,
            document_type_name: null,
            document_status_name: null,
            document_people_name: null,
            document_receiver_name: null,

        })
        this.changePaginateTotal(this.listDocument.length, 'paginateDocument')
    }


    // Modal Location
    autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
        if (object[name].length >= length) {
            console.log("d")
            this.is_autocomplete_ListModalLocation_show = true

            let params = {
                name: object[name],
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListModalLocation(params)
        }
    }
    autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_ListModalLocation_show = false
        }, 200)
    }
    callAutocompleteChangeListModalLocation(params: any): void {
        if (this.input.is_autocomplete_ListModalLocation_load) return
        this.input.is_autocomplete_ListModalLocation_load = true
        let pThis = this
        // this.DocumentProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
        //   if (data) {
        //     if (data.length == 1) {
        //       setTimeout(function () {
        //         pThis.autocompleteChooseListModalLocation(data[0])
        //       }, 200)
        //     } else {
        //       pThis.autocompleteListListModalLocation = data
        //     }
        //   }
        // })
        this.input.is_autocomplete_ListModalLocation_load = false
    }
    autocompleteChooseListModalLocation(data: any): void {
        this.inputModalAddress.address_sub_district_code = data.code
        this.inputModalAddress.address_sub_district_name = data.name
        this.inputModalAddress.address_district_code = data.district_code
        this.inputModalAddress.address_district_name = data.district_name
        this.inputModalAddress.address_province_code = data.province_code
        this.inputModalAddress.address_province_name = data.province_name
        this.inputModalAddress.address_country_code = data.country_code
        this.inputModalAddress.address_country_name = data.country_name
        this.is_autocomplete_ListModalLocation_show = false
    }
    onClickSave04ModalAddressSave(): void {
        Object.keys(this.inputModalAddress).forEach((item: any) => {
            this.inputModalAddressEdit[item] = this.inputModalAddress[item]
        })
        this.toggleModal("isModalPeopleEditOpen")
    }
    // Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listDocument = data.document_list || []
        this.changePaginateTotal(this.listDocument.length, 'paginateDocument')

    }

    listData(data: any): void {
        this.listDocument = data.list || []
        this.help.PageSet(data, this.paginateDocument)
    }

    saveData(): any {
        // let params = this.input
        // params.document_list = this.listDocument || []

        const params = {
            //page_index: +this.paginateDocument.currentPage,
            //item_per_page: +this.paginateDocument.itemsPerPage,
            //order_by: 'created_date',
            //is_order_reverse: false,
            //search_by: [{
            //    key: 'saved_process_date',
            //    value: displayDateServer(this.input.saved_process_date),
            //    operation: 0
            //}, {
            //    key: 'document_type_code',
            //    value: this.input.document_type_code,
            //    operation: 0
            //}, {
            //    key: 'document_classification_by_name',
            //    value: this.input.document_classification_by_name,
            //    operation: 5
            //}, {
            //    key: 'request_number',
            //    values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
            //    operation: 5
            //}, {
            //    key: 'document_status_code',
            //    value: this.input.document_status_code,
            //    operation: 0
            //}]
            saved_process_date: this.input.saved_process_date,
            document_type_code: this.input.document_type_code,
            document_classification_by_name: this.input.document_classification_by_name,
            request_number: this.input.request_number,
            document_status_code: this.input.document_status_code,
        }

        return params
    }



    //! <<< ----------------- >>>
    //! <<< --- Copy Zone --- >>>
    //! <<< ----------------- >>>

    //! <<< Binding (one way) >>>
    binding(obj: any, name: any, value: any): void {
        this[obj][name] = value
    }

    //! <<< Table >>>
    onSelectColumnInTable(name: string, item: any): void {
        if (!item) {
            this.input.is_check_all = !this.input.is_check_all
            this[name].forEach((item: any) => {
                item.is_check = this.input.is_check_all
            })
        } else {
            item.is_check = !item.is_check
        }
    }
    onChangeInputInTable(item: any, name: any, value: any): void {
        item[name] = value
    }
    toggleInputInTable(item: any, name: any): void {
        item[name] = !item[name]
    }

    //! <<<< Pagination >>>
    managePaginateCallApi(name: string): void {
        if (name === 'paginateDocument') {
            this.onClickDocumentItemList()
            this.input.is_check_all = false
        }
    }
    onChangePage(page: any, name: string): void {
        this[name].currentPage = page === '' ? 1 : page
        if ((+page || page === '') && page <= this.getMaxPage(name)) {
            this.managePaginateCallApi(name)
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
        this.managePaginateCallApi(name)
    }
    changePaginateTotal(total: any, name: string): void {
        this[name].totalItems = total
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
