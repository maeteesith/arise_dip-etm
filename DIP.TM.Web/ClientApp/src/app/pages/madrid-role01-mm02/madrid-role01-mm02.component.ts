import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { DialogService } from '../../services/dialogService';
import { MadridRole01AlertComponent } from '../madrid-role01-alert/madrid-role01-alert.component';

@Component({
  selector: 'app-madrid-role01-mm02',
  templateUrl: './madrid-role01-mm02.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./madrid-role01-mm02.component.scss',
    './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole01Mm02Component implements OnInit {

  constructor(
    private dialog: MatDialog,
    private _dialog: DialogService,) { }

  private currentStep: number

  ngOnInit() {
    this.currentStep = 1;
  }

  onNextStep()
  {
    this.currentStep++;
  }

  onBackStep()
  {
    if (this.currentStep > 1)
      this.currentStep--;
  }

  OnClickEdit()
  {
    const dialogRef = this.dialog.open(MadridRole01AlertComponent, {
      width: "calc(40% - 80px)",
      maxWidth: "100%",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
      // this.router.navigate(['eordering/delivery-note/create-request']);
      // if (!res) return;

      // this.router.navigate(['eordering/delivery-note/create-request']);
      // this.navToAddProduct(<ApiProductGroupDisplay>res);
    });
  }
  

}
