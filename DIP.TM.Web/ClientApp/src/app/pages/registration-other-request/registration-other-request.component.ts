import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Auth } from "../../auth";
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser'
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { RequestProcessService } from '../../services/request-process-buffer.service'
import { RequestAgencyService } from '../../services/request-agency.service'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayDate,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-registration-other-request",
  templateUrl: "./registration-other-request.component.html",
  styleUrls: ["./registration-other-request.component.scss"]
})
export class RegistrationOtherRequestComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  public modal: any
  // Response
  public response: any

  // // Input Request01Item
  // public inputRequest01Item: any
  // List Request01Item
  public listRequest01Item: any[]
  public paginateRequest01Item: any
  public perPageRequest01Item: number[]
  // Response Request01Item
  public responseRequest01Item: any

  //// List Request01Item
  public requestOtherItemSubEdit: any
  public listRequestOtherItemSubEdit: any[]
  //public paginateRequestOtherItemSub: any
  //public perPageRequestOtherItemSub: number[]
  //// Response RequestOtherItemSub
  //public responseRequestOtherItemSub: any

  // // Input Item
  // public inputItem: any
  // List Item
  public listItem: any[]
  public paginateItem: any
  public perPageItem: number[]
  // Response Item
  public responseItem: any

  public timeout: any
  // Autocomplete
  public autocompleteList: any
  public isShowAutocomplete: any
  public url: string;

  public popup: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}

    this.input = {
      id: null,
      requester_name: '',
      created_by_name: this.auth.getAuth().name,
      telephone: '',
      is_via_post: false,
      is_wave_fee: false,
      request_date: getMoment(),
      commercial_affairs_province_code: "FLOOR3",
    }
    this.listRequest01Item = []
    this.listRequest01Item.push({})
    this.listRequest01Item.push({})
    this.listRequest01Item.push({})
    this.listRequest01Item.push({})
    this.listRequest01Item.push({})
    this.paginateRequest01Item = CONSTANTS.PAGINATION.INIT
    this.perPageRequest01Item = CONSTANTS.PAGINATION.PER_PAGE

    this.listItem = []
    this.listItem.push({})
    this.listItem.push({})
    this.listItem.push({})
    this.listItem.push({})
    this.listItem.push({})
    this.paginateItem = CONSTANTS.PAGINATION.INIT
    this.perPageItem = CONSTANTS.PAGINATION.PER_PAGE


    this.master = {
    }

    // Autocomplete
    this.autocompleteList = {
      requester_name: []
    }
    this.isShowAutocomplete = {
      requester_name: false
    }

    this.modal = {
      isModalEditCategoryOpen: false,
      isModalPreviewPdfOpen: false,
    }

    var pThis = this
    this.autoComplete.Initial()
    this.autoComplete.Add("/RequestProcess/RequestTypeCodeList", "รหัสรายการ", ["code", "name"], [0, 1], 0, this.input, [
      ["request_type_code", "code"]],
      function (row_item) {
        pThis.popup.warning_message_show_list = []
        //console.log(pThis.listRequest01Item)
        //console.log(row_item)
        //console.log(pThis.listRequest01Item)

        //if (!row_item.requestOtherItemSub) {
        //console.log(row_item)
        //pThis.help.Clear(row_item)
        row_item.requestOtherItemSub = []
        row_item.is_disabled = true

        pThis.listRequest01Item.forEach((item: any) => {
          console.log(item)
          if (item.request_number && item.request_number != "") {
            if (row_item.request_type_code != "70") {
              row_item.requestOtherItemSub.push({
                request_number: item.request_number,
              })
            } else {
              //console.log(item)
              var product_list = pThis.help.Distinct(item.product_list, "request_item_sub_type_1_code")
              //console.log(product_list)
              Object.keys(product_list).forEach((product: any) => {
                row_item.requestOtherItemSub.push({
                  request_number: item.request_number,
                  item_sub_type_1_code: product,
                  product_count: product_list[product],
                })
              })
              pThis.updateRequestOtherItemSub(row_item.requestOtherItemSub)
            }
          }

          if (item.request_number && item.request_number != "") {
            if (row_item.request_type_code == "70") {
              console.log(item.request_number)
              //console.log(getMoment())
              //console.log(item.trademark_expired_end_date)
              //console.log(getMoment())


              var trademark_expired_start_date = getMoment(item.trademark_expired_start_date, "YYYY-MM-DDTHH:mm:ss")
              var trademark_expired_date = getMoment(item.trademark_expired_date, "YYYY-MM-DDTHH:mm:ss")
              var trademark_expired_end_date = getMoment(item.trademark_expired_end_date, "YYYY-MM-DDTHH:mm:ss")

              if (!item.registration_number || item.registration_number == "") {
                var msg = {
                  message: "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ได้เลขทะเบียน",
                  is_warning: true,
                }
                console.log("warnning_message")
                if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                row_item.warnning_message += msg.message
                pThis.popup.warning_message_show_list.push(msg)
                row_item.is_deactivated = true
              } else if (item.trademark_expired_start_date && trademark_expired_start_date > getMoment()) {
                var msg = {
                  message: "เลขคำขอ <b>" + item.request_number + "</b> " +
                    "ทะเบียนเลขที่ <b>" + item.registration_number + "</b> " +
                    "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
                    "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
                    "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
                    "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
                    "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
                  is_warning: true,
                }
                console.log("warnning_message")
                row_item.warnning_message = row_item.warnning_message || ""
                if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ถึงกำหนดการต่ออายุวันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>"
                pThis.popup.warning_message_show_list.push(msg)
              } else if (item.trademark_expired_end_date && getMoment() > trademark_expired_end_date) {
                console.log("warnning_message")

                if (item.last_extend_date) {
                  var last_extend_date = getMoment(item.last_extend_date, "YYYY-MM-DDTHH:mm:ss")
                  if (last_extend_date > trademark_expired_start_date) {
                    msg = {
                      message: "เลขคำขอ <b>" + item.request_number + "</b> " +
                        "ยื่นต่ออายุครั้งล่าสุดเมื่อวันที่ <b>" + displayDate(last_extend_date) + "</b> ",
                      is_warning: false,
                    }
                    pThis.popup.warning_message_show_list.push(msg)
                  } else {
                    row_item.is_deactivated = true
                  }
                } else {
                  row_item.is_deactivated = true
                }

                if (row_item.is_deactivated) {
                  var msg = {
                    message: "เลขคำขอ <b>" + item.request_number + "</b> " +
                      "ทะเบียนเลขที่ <b>" + item.registration_number + "</b> " +
                      "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
                      "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
                      "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
                      "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
                      "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
                    is_warning: true,
                  }
                  row_item.warnning_message = row_item.warnning_message || ""
                  if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                  row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ไม่สามารถต่ออายุได้ เนื่องจากเกินกำหนดระยะเวลา"
                  pThis.popup.warning_message_show_list.push(msg)
                }
              }
            } else if (row_item.request_type_code == "60") {
              if (item.registration_number && item.registration_number != "") {
                var msg = {
                  message: "เลขคำขอ <b>" + item.request_number + "</b> ได้เลขทะเบียนแล้ว",
                  is_warning: true,
                }
                console.log("warnning_message")
                row_item.warnning_message = row_item.warnning_message || ""
                if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                row_item.warnning_message += msg.message
                pThis.popup.warning_message_show_list.push(msg)
              }
            } else if (row_item.request_type_code == "61") {
              if (!item.registration_number || item.registration_number == "") {
                var msg = {
                  message: "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ได้เลขทะเบียน",
                  is_warning: true,
                }
                console.log("warnning_message")
                row_item.warnning_message = row_item.warnning_message || ""
                if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                row_item.warnning_message += msg.message
                pThis.popup.warning_message_show_list.push(msg)
              }
            }
          }
        })

        //row_item.

        //console.log(row_item)

        pThis.listRequest01Item.forEach((item: any) => { item.reference_number = "" })

        pThis.updateItem()
      }
    )

    //this.listRequestOtherItemSubEdit = []
    //this.listRequestOtherItemSubEdit.push({})
    //this.listRequestOtherItemSubEdit.push({})
    //this.listRequestOtherItemSubEdit.push({})
    //this.listRequestOtherItemSubEdit.push({})
    //this.listRequestOtherItemSubEdit.push({})

    //this.listRequestOtherItemSub = []

    this.popup = {
      warning_message_show_list: [],
    }

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initRegistrationOtherRequest().subscribe((data: any) => {
      if (data) {
        this.master = data

      }
      if (this.editID) {
        this.RequestProcessService.RequestOtherLoad(this.editID, {}).subscribe((data: any) => {
          if (data) {
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)

          //For Test
          //this.onClickRequestOtherItemEdit(this.listItem[0])
          //this.onClickRequestOtherItemSubAdd()
        })
      }

      this.global.setLoading(false)
    })
  }
  loadData(data: any) {
    // Manage structure
    Object.keys(data).forEach((item: any) => {
      this.input[item] = data[item]
    })

    this.listRequest01Item = data.request_01_item_list || []
    this.listItem = data.item_list || []
    data.request_01_item_list.map((item: any) => {
      item.is_check = false
      item.is_disabled = true
      return item
    })
    data.item_list.map((item: any) => {
      item.code = item.request_type_code
      item.name = item.request_type_name
      item.is_check = false
      item.is_disabled = true

      item.requestOtherItemSub.map((item_sub: any) => {
        var request_item = this.listRequest01Item.filter(r => r.request_number == item_sub.request_number)
        if (request_item.length == 1) {
          request_item[0].request_index_list = request_item[0].request_index_list || []
          if (item_sub.request_index && request_item[0].request_index_list.indexOf(item.request_type_code + ": " + item_sub.request_index) < 0)
            request_item[0].request_index_list.push(item.request_type_code + ": " + item_sub.request_index)
        }
      })


      return item
    })
    this.changePaginateTotal((this.listRequest01Item || []).length, 'paginateRequest01Item')
    this.changePaginateTotal((this.listItem || []).length, 'paginateItem')




    //console.log(this.listItem)
    //this.autoComplete.Update(this.listItem)


    // Set value
    // this.requestList = data.item_list
    // this.response.request01Load = data
    // this.isHasRequestList = true
    this.updateItem()
  }

  constructor(
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private auth: Auth,
    private route: ActivatedRoute,
    private location: Location,
    private requestAgencyService: RequestAgencyService,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private RequestProcessService: RequestProcessService
  ) { }


  onClickRequestOtherMakerAutoFill(): void {
    if (this.validateRequestOtherMakerAutoFill()) {
      // Open loading
      this.global.setLoading(true)
      // Set param
      const params = {
        id: this.input.id,
        requester_name: this.input.requester_name,
        telephone: this.input.telephone,
        request_01_item_list: this.listRequest01Item || [],
        item_list: this.listItem || [],

      }
      // Call api
      this.callRequestOtherMakerAutoFill(params)
    }
  }
  validateRequestOtherMakerAutoFill(): boolean {
    let result = validateService('validateRequestOtherMakerAutoFill', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestOtherMakerAutoFill(params: any): void {
    this.RequestProcessService.RequestOtherMakerAutoFill(params).subscribe((data: any) => {
      // if(isValidRequestOtherMakerAutoFillResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listRequest01Item = data.request_01_item_list || []
        this.listItem = data.item_list || []
        this.changePaginateTotal((this.listRequest01Item || []).length, 'paginateRequest01Item')
        this.changePaginateTotal((this.listItem || []).length, 'paginateItem')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestOtherSave(): void {
    if (this.validateRequestOtherSave()) {
      // Open loading
      this.global.setLoading(true)
      // Set param
      for (let i = 0; i < this.listItem.length; i++) {
        if (this.listItem[i].request_type_code == "") {
          this.listItem.splice(i--)
        }
      }
      const params = {
        id: this.input.id,
        request_date: this.input.request_date,
        requester_name: this.input.requester_name,
        telephone: this.input.telephone,
        is_via_post: this.input.is_via_post,
        is_wave_fee: this.input.is_wave_fee,
        //is_commercial_affairs_province: this.input.is_commercial_affairs_province,
        commercial_affairs_province_code: this.input.commercial_affairs_province_code,
        total_price: this.input.total_price,
        request_01_item_list: this.listRequest01Item || [],
        item_list: this.listItem || [],
      }
      // Call api
      this.callRequestOtherSave(params)
    }
  }
  validateRequestOtherSave(): boolean {
    let result = validateService('validateRequestOtherSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestOtherSave(params: any): void {
    this.RequestProcessService.RequestOtherSave(params).subscribe((data: any) => {
      if (data) {
        this.loadData(data)

        if (data.total_price > 0) {
          this.url = '/pdf/ReceiptOtherReference/' + data.id
          console.log(this.url)
          this.toggleModal('isModalPreviewPdfOpen')
        } else {
          this.toggleModal('isPopupSaveOpen')
        }
      }
    })
  }

  onClickRequestOtherRequest01ItemAdd(): void {
    this.listRequest01Item.push({
      //index: this.listRequest01Item.length + 1,
      request_number: null,
      total_price: null,
      reference_number: null,
      owner_name: null,
      facilitation_act_status_code: this.master.facilitationActStatusCodeList[0].code,

    })
    this.changePaginateTotal(this.listRequest01Item.length, 'paginateRequest01Item')
    this.updateItem()
  }

  onClickRequestOtherRequest01ItemDelete(item: any): void {
    // if(this.validateRequestOtherRequest01ItemDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listRequest01Item.filter(r => r.is_check).length > 0 || item) {
      var rs = prompt("Do you want to delete", "Yes / No")
      if (rs && rs != "") {
        if (item) {
          this.listRequest01Item.filter(r => r.is_check).forEach((item: any) => {
            item.is_check = false
          });
          item.is_check = true
        }

        let ids = []

        for (let i = 0; i < this.listRequest01Item.length; i++) {
          if (this.listRequest01Item[i].is_check) {
            this.listRequest01Item[i].cancel_reason = rs
            this.listRequest01Item[i].status_code = "DELETE"

            this.listItem.forEach((item: any) => {
              console.log(item)
              if (item.requestOtherItemSub)
                item.requestOtherItemSub = item.requestOtherItemSub.filter(r => r.request_number != this.listRequest01Item[i].request_number)
            })
            this.updateItem()

            if (true && this.listRequest01Item[i].id) ids.push(this.listRequest01Item[i].id)
            else this.listRequest01Item.splice(i--, 1);

          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callRequestOtherRequest01ItemDelete(params, ids)
            return;
          }
        }

        //for (let i = 0; i < this.listRequest01Item.length; i++) {
        //  this.listRequest01Item[i].index = i + 1
        //}
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callRequestOtherRequest01ItemDelete(params: any, ids: any[]): void {
    this.RequestProcessService.RequestOtherRequest01ItemDelete(params).subscribe((data: any) => {
      // if(isValidRequestOtherRequest01ItemDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listRequest01Item.length; i++) {
          if (this.listRequest01Item[i].id == id) {
            this.listRequest01Item.splice(i--, 1);
          }
        }
      });

      //for (let i = 0; i < this.listRequest01Item.length; i++) {
      //  this.listRequest01Item[i].index = i + 1
      //}


      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickCheckBoxTable2(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_2_check_all = !this.input.is_table_2_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_2_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxRequest01Item(object: any, object_name: string, value: string): void { object[object_name] = !object[object_name] }
  onChangeSelectRequest01Item(row_item: any): void {
    this.global.setLoading(true)

    let pThis = this
    setTimeout(function () {
      //pThis.listRequest01Item = pThis.listRequest01Item || []
      //console.log(this.listRequest01Item)
      if (row_item.request_number.trim() == "" || pThis.listRequest01Item.filter(r => r.request_number == row_item.request_number).length > 1) {
        pThis.listRequest01Item.splice(pThis.listRequest01Item.indexOf(row_item), 1)
        pThis.global.setLoading(false)
        pThis.listRequest01Item.forEach((item: any) => { item.reference_number = "" })
        pThis.updateItem()
        return;
      }

      var params = pThis.help.GetFilterParams({ request_number: row_item.request_number })

      pThis.RequestProcessService.Request01ItemList(params).subscribe((data: any) => {
        if (data && data.list && data.list.length > 0) {
          Object.keys(data.list[0]).forEach((item: any) => {
            if (item == "id") return true;
            if (item == "reference_number") return true;
            row_item[item] = data.list[0][item]
          })
          if (pThis.input.requester_name == "") {
            pThis.input.requester_name = data.list[0].name
          }
          row_item.is_disabled = true
          pThis.listRequest01Item.forEach((item: any) => { item.reference_number = "" })

          pThis.listItem.forEach((item: any) => {
            console.log(item)
            if (item.request_type_code && item.request_type_code != "") {
              if (item.request_type_code != "70") {
                item.requestOtherItemSub.push({
                  request_number: data.list[0].request_number,
                })
              } else {
                var product_list = pThis.help.Distinct(data.list[0].product_list, "request_item_sub_type_1_code")
                Object.keys(product_list).forEach((product: any) => {
                  item.requestOtherItemSub.push({
                    request_number: data.list[0].request_number,
                    item_sub_type_1_code: product,
                    product_count: product_list[product],
                  })
                })
                pThis.updateRequestOtherItemSub(item.requestOtherItemSub)
              }
            }
          })






          //} else
          if (row_item.trademark_expired_date) {
            var trademark_expired_start_date = getMoment(row_item.trademark_expired_start_date, "YYYY-MM-DDTHH:mm:ss")
            var trademark_expired_date = getMoment(row_item.trademark_expired_date, "YYYY-MM-DDTHH:mm:ss")
            var trademark_expired_end_date = getMoment(row_item.trademark_expired_end_date, "YYYY-MM-DDTHH:mm:ss")

            if (getMoment() > trademark_expired_date) {
              pThis.popup.warning_message_show_list.push({
                message: "เลขคำขอ <b>" + row_item.request_number + "</b> " +
                  "ทะเบียนเลขที่ <b>" + row_item.registration_number + "</b> " +
                  "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
                  "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
                  "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
                  "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
                  "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
                is_warning: true,
              })

              if (row_item.last_extend_date) {
                var last_extend_date = getMoment(row_item.last_extend_date, "YYYY-MM-DDTHH:mm:ss")
                if (last_extend_date > trademark_expired_start_date) {
                  var msg = {
                    message: "เลขคำขอ <b>" + row_item.request_number + "</b> " +
                      "ยื่นต่ออายุครั้งล่าสุดเมื่อวันที่ <b>" + displayDate(last_extend_date) + "</b> ",
                    is_warning: false,
                  }
                  pThis.popup.warning_message_show_list.push(msg)
                }
              }
            }
          }









          pThis.updateItem()
        } else {
          row_item.request_number = ""
        }
        pThis.global.setLoading(false)
      })
    }, 200)
  }

  onClickRequest01Itemtable_link_picRequest01Item(id: any): void { }

  onClickRequestOtherItemAdd(): void {
    this.listItem.push({
      //index: this.listItem.length + 1,
      request_type_code: null,
      request_type_description: null,
      item_count: null,
      item_sub_type_1_count: null,
      product_count: null,
      total_price: null,
      fine: null,
      is_evidence_floor7: false,
      product_list_text: null,
      is_sue_text: null,

    })
    this.changePaginateTotal(this.listItem.length, 'paginateItem')
  }

  onClickRequestOtherItemDelete(item: any): void {
    // if(this.validateRequestOtherItemDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listItem.filter(r => r.is_check).length > 0 || item) {
      var rs = confirm("Do you want to delete")
      if (rs) {
        if (item) {
          this.listItem.filter(r => r.is_check).forEach((item: any) => {
            item.is_check = false
          });
          item.is_check = true
        }

        let ids = []

        for (let i = 0; i < this.listItem.length; i++) {
          if (this.listItem[i].is_check) {
            this.listItem[i].is_deleted = true
            //this.listItem[i].cancel_reason = rs
            //this.listItem[i].status_code = "DELETE"

            if (true && this.listItem[i].id) ids.push(this.listItem[i].id)
            else this.listItem.splice(i--, 1);
          }
        }

        //for (let i = 0; i < this.listItem.length; i++) {
        //  this.listItem[i].index = i + 1
        //}
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callRequestOtherItemDelete(params: any, ids: any[]): void {
    this.RequestProcessService.RequestOtherItemDelete(params).subscribe((data: any) => {
      // if(isValidRequestOtherItemDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listItem.length; i++) {
          if (this.listItem[i].id == id) {
            this.listItem.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listItem.length; i++) {
        this.listItem[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickCheckBoxTable3(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_3_check_all = !this.input.is_table_3_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_3_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxItem(object: any, object_name: string, value: string): void { object[object_name] = !object[object_name] }
  //onChangeSelectItem(object: any, object_name: string, value: string): void {
  //  //object[object_name] = value
  //  this.updateItem()
  //}
  updateItem(): void {
    this.global.setLoading(true)
    let pThis = this
    setTimeout(function () {
      let total_price = 0
      pThis.listItem.forEach((item: any) => {
        //console.log(item.requestOtherItemSub)

        let request_type_list = pThis.master.requestTypeList.filter(r => r.code == item.request_type_code);

        if (request_type_list.length == 0) {
          item.request_type_code = ""
          item.request_type_description = ""
          item.item_count = 0
          item.item_sub_type_1_count = 0
          item.product_count = 0
          item.total_price = 0
          item.fine = 0
          item.is_evidence_floor7 = false
        } else {
          //console.log(item)

          item.request_type_description = request_type_list[0].name
          //console.log(item.requestOtherItemSub)
          item.item_count = item.requestOtherItemSub ? pThis.help.CountDistinct(item.requestOtherItemSub, "request_number") : 0
          item.item_sub_type_1_count = item.request_type_code != "70" ? 0 :
            pThis.help.CountDistinct(item.requestOtherItemSub, "item_sub_type_1_code")
          item.product_count = item.request_type_code != "70" ? 0 :
            item.requestOtherItemSub.reduce(function (a, b) {
              return a + parseFloat(b.product_count || 1)
            }, 0)
          if (pThis.input.is_wave_fee) {
            item.total_price = 0
          } else {
            if (item.request_type_code != "70") {
              item.requestOtherItemSub.forEach((item_sub: any) => {
                item_sub.total_price = +request_type_list[0].value_2
              })

              item.total_price =
                item.requestOtherItemSub.reduce(function (a, b) {
                  return a + parseFloat(b.total_price || 0)
                }, 0)
            } else {
              item.total_price =
                item.requestOtherItemSub.reduce(function (a, b) {
                  return a + parseFloat(b.total_price || 0)
                }, 0)
            }
          }

          item.fine = 0
        }

        total_price += item.total_price
        total_price += item.fine
      })
      pThis.input.total_price = total_price
      pThis.input.item_list = pThis.listItem
    }, 200)
    this.global.setLoading(false)
  }

  onClickRequest01ItemRequestSearch(row_item: any): void {
    //console.log(row_item)
    window.open('/request-search/edit/' + row_item.save_id, "_blank");
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(obj: any, name: any): void {
    this.validate[name] = null
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      if (this[obj][name]) {
        if (name === 'requester_name') {
          this.callListView({
            name: this[obj][name],
            paging: {
              item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page
            }
          }, name)
        }
      } else {
        this.onClickOutsideAutocomplete(name)
      }
    }, CONSTANTS.DELAY_CALL_API)
  }
  callListView(params: any, name: any): void {
    this.requestAgencyService.ListView(params).subscribe((data: any) => {
      if (data) {
        this.autocompleteList[name] = data
        this.isShowAutocomplete[name] = true
      } else {
        this.isShowAutocomplete[name] = false
        this.autocompleteList[name] = []
      }
    })
  }
  onSelectAutocomplete(name: any, item: any): void {
    if (name === 'requester_name') {
      this.input[name] = item.name
      this.input.is_wave_fee = item.is_wave_fee
      this.updateRequestList()
    }
    this.onClickOutsideAutocomplete(name)
  }
  onClickOutsideAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = false
    this.autocompleteList[name] = []
  }

  updateRequestList(): void {
    if (this.input.is_wave_fee) {
      //this.input.total_price = 0
      //this.requestList.forEach(request => {
      //  request.total_price = 0
      //  request.size_over_price = 0
      //  request.item_sub_list.forEach((item: any) => {
      //    item.total_price = 0
      //  })
      //  request.size_over_price = 0
      //})
    } else {
      //let total = 0
      //this.requestList.forEach((request: any) => {
      //  let total_price_request = 0
      //  request.item_sub_list.forEach((item: any) => {
      //    let total = getItemCalculator(item.product_count, this.master.priceMasterList)
      //    item.total_price = total
      //    total_price_request = total_price_request + total
      //  })
      //  request.total_price = total_price_request
      //  total = total + total_price_request

      //  request.size_over_price = request.size_over_cm * 200
      //  total = total + (request.size_over_price || 0) * request.amount_product

      //})
      //this.input.total_price = total
    }
  }

  onClickRequestOtherItemEdit(row_item: any): void {
    this.toggleModal("isModalEditCategoryOpen")

    this.requestOtherItemSubEdit = row_item
    this.listRequestOtherItemSubEdit = JSON.parse(JSON.stringify(row_item.requestOtherItemSub))

    this.updateRequestOtherItemSub(this.listRequestOtherItemSubEdit)
  }

  onClickRequestOtherItemSubSave(): void {
    this.toggleModal("isModalEditCategoryOpen")

    this.listRequestOtherItemSubEdit = this.listRequestOtherItemSubEdit.filter(r => r.request_number != "" && r.product_count && r.product_count != "")
    this.requestOtherItemSubEdit.requestOtherItemSub = JSON.parse(JSON.stringify(this.listRequestOtherItemSubEdit))

    this.listRequest01Item.forEach((item: any) => { item.reference_number = "" })
    this.updateItem()
  }
  onClickRequestOtherItemSubAdd(): void {
    this.listRequestOtherItemSubEdit.push({})
    this.updateRequestOtherItemSub(this.listRequestOtherItemSubEdit)
  }
  onClickRequestOtherItemSubDelete(row_item: any): void {
    if (row_item.id) {
      row_item.is_deleted = true
    } else {
      this.listRequestOtherItemSubEdit.splice(this.listRequestOtherItemSubEdit.indexOf(row_item), 1)
    }
    this.updateRequestOtherItemSub(this.listRequestOtherItemSubEdit)
  }
  updateRequestOtherItemSub(item_list: any[]): void {
    setTimeout(function () {
      //console.log(item_list)
      item_list.forEach((item: any) => {
        item.total_price = item.product_count ? (item.product_count > 5 ? 18000 : item.product_count * 2000) : 0
        item.fine = 0
      })
    }, 200)
  }


  displayMoney(value: any): any {
    return displayMoney(value)
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }

    if (name == "is_wave_fee") {
      this.listRequest01Item.forEach((item: any) => { item.reference_number = "" })
      this.updateItem()
    }
  }
  reset(): void {
    this.ngOnInit()
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

  displayDate(date: any): any {
    return displayDate(date)
  }
}
