import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-checking-similar-list",
  templateUrl: "./checking-similar-list.component.html",
  styleUrls: ["./checking-similar-list.component.scss"]
})
export class CheckingSimilarListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any
  public responseStatistic: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List CheckingSimilar
  public listCheckingSimilar: any[]
  public paginateCheckingSimilar: any
  public perPageCheckingSimilar: number[]
  // Response CheckingSimilar
  public responseCheckingSimilar: any
  public listCheckingSimilarEdit: any


  // List CheckingProcess
  public listCheckingProcess: any[]
  public paginateCheckingProcess: any
  public perPageCheckingProcess: number[]
  // Response CheckingProcess
  public responseCheckingProcess: any

  //label_save_combobox || label_save_radio
  public checkingReceiveStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_start_date: getMoment(),
      checking_receive_status_code: '',
      request_number: '',
    }
    this.listCheckingSimilar = []
    this.paginateCheckingSimilar = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateCheckingSimilar.id = 'paginateCheckingSimilar'
    this.perPageCheckingSimilar = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listCheckingProcess = []

    //Master List
    this.master = {
      checkingReceiveStatusCodeList: [],
    }
    //Master List

    this.responseStatistic = {}


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initCheckingSimilarList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.checkingReceiveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });

      }
      if (this.editID) {
        let pThis = this
        this.CheckingProcessService.CheckingSimilarLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          pThis.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.CheckingProcessService.CheckingSimilarStatisticList().subscribe((data: any) => {

          this.responseStatistic = {
            ALL: 0
          }
          data.forEach((item: any) => {
            this.responseStatistic[item.code] = +item.value
            this.responseStatistic["ALL"] += parseInt(item.value)
          })

          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      }
    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }

  onClickALLList(): void {
    this.onClickReset()
    this.input.request_start_date = ""
    this.input.checking_receive_status_code = ""
    this.onClickCheckingSimilarList()
  }
  onClickWaitDoneList(): void {
    this.onClickReset()
    this.input.request_start_date = ""
    this.input.checking_receive_end_date = ""
    this.input.checking_receive_status_code = "WAIT_DONE"
    this.onClickCheckingSimilarList()
  }
  onClickWaitChangeList(): void {
    this.onClickReset()
    this.input.request_start_date = ""
    this.input.checking_receive_end_date = ""
    this.input.checking_receive_status_code = "WAIT_CHANGE"
    this.onClickCheckingSimilarList()
  }
  onClickDoneList(): void {
    this.onClickReset()
    this.input.checking_send_start_date = getMoment().startOf('month')

    this.input.request_start_date = ""
    this.input.checking_receive_end_date = ""
    this.input.checking_receive_status_code = "DONE"
    this.onClickCheckingSimilarList()
    this.input.checking_send_start_date = ""
  }

  onClickCheckingSimilarList(): void {
    // if(this.validateCheckingSimilarList()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingSimilarList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callCheckingSimilarList(params: any): void {
    console.log(params)
    this.CheckingProcessService.CheckingSimilarList(this.help.GetFilterParams(params, this.paginateCheckingSimilar)).subscribe((data: any) => {
      // if(isValidCheckingSimilarListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
      this.automateTest.test(this, { list: 1 })
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickCheckingSimilarAdd(): void {
    this.listCheckingSimilar.push({
      index: this.listCheckingSimilar.length + 1,
      request_number: null,
      name: null,
      request_date_text: null,
      request_item_sub_type_1_name: null,
      checking_receive_date_text: null,
      checking_receive_status_name: null,
      request_image: null,
      request_sound: null,
      checking_send_date_text: null,
      checking_remark: null,

    })
    this.changePaginateTotal(this.listCheckingSimilar.length, 'paginateCheckingSimilar')
  }

  onClickCheckingSimilarEdit(item: any): void {
    var win = window.open("checking-similar/edit/" + item.id)
  }


  onClickCheckingSimilarDelete(item: any): void {
    // if(this.validateCheckingSimilarDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listCheckingSimilar.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listCheckingSimilar.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listCheckingSimilar.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listCheckingSimilar.length; i++) {
          if (this.listCheckingSimilar[i].is_check) {
            this.listCheckingSimilar[i].cancel_reason = rs
            this.listCheckingSimilar[i].status_code = "DELETE"
            this.listCheckingSimilar[i].is_deleted = true

            if (true && this.listCheckingSimilar[i].id) ids.push(this.listCheckingSimilar[i].id)
            // else this.listCheckingSimilar.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callCheckingSimilarDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listCheckingSimilar.length; i++) {
          this.listCheckingSimilar[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callCheckingSimilarDelete(params: any, ids: any[]): void {
    this.CheckingProcessService.CheckingSimilarDelete(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listCheckingSimilar.length; i++) {
          if (this.listCheckingSimilar[i].id == id) {
            this.listCheckingSimilar.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listCheckingSimilar.length; i++) {
        this.listCheckingSimilar[i] = i + 1
      }

      this.onClickCheckingSimilarList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listCheckingSimilar = data.checkingsimilar_list || []
    this.changePaginateTotal(this.listCheckingSimilar.length, 'paginateCheckingSimilar')

  }

  listData(data: any): void {
    this.listCheckingSimilar = data.list || []
    this.help.PageSet(data, this.paginateCheckingSimilar)
    //this.listCheckingSimilar = data || []
    //this.changePaginateTotal(this.listCheckingSimilar.length, 'paginateCheckingSimilar')
  }

  saveData(): any {
    // let params = this.input
    // params.checkingsimilar_list = this.listCheckingSimilar || []

    const params = {
      checking_send_start_date: this.input.checking_send_start_date,

      request_start_date: this.input.request_start_date,
      request_end_date: this.input.request_end_date,
      request_number: this.input.request_number,
      checking_receive_status_code: this.input.checking_receive_status_code,
      //  page_index: +this.paginateCheckingSimilar.currentPage,
      //  item_per_page: +this.paginateCheckingSimilar.itemsPerPage,
      //  order_by: 'created_date',
      //  is_order_reverse: false,
      //  search_by: [{
      //    key: 'request_date',
      //    value: displayDateServer(this.input.request_date),
      //    operation: 3
      //  }, {
      //    key: 'request_number',
      //    values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //    operation: 5
      //  }, {
      //    key: 'checking_receive_status_code',
      //    value: this.input.checking_receive_status_code,
      //    operation: 0
      //  }]
    }

    //const params = {
    //  page_index: +this.paginateCheckingSimilar.currentPage,
    //  item_per_page: +this.paginateCheckingSimilar.itemsPerPage,
    //  order_by: 'created_date',
    //  is_order_reverse: false,
    //  search_by: [{
    //    key: 'request_date',
    //    value: displayDateServer(this.input.request_date),
    //    operation: 3
    //  }, {
    //    key: 'request_number',
    //    values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
    //    operation: 5
    //  }, {
    //    key: 'checking_receive_status_code',
    //    value: this.input.checking_receive_status_code,
    //    operation: 0
    //  }]
    //}

    return params
  }


  togglePlayer(item: any): void {
    if (!item.player) {
      item.player = new Audio(item.sound_file_physical_path)
      item.player_action = null
    }

    if (item.player_action) {
      item.player.pause()
    } else {
      item.player.play()
    }
    item.player_action = !item.player_action
  }


  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginateCheckingSimilar') {
      this.onClickCheckingSimilarList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }

  onClickCheckingSimilarRequestItemSubTypeReport(): void {
    window.open("/pdf/CheckingSimilarRequestItemSubTypeReport/" + this.listCheckingSimilar.filter(r => r.is_check).map(
      function (item) {
        return item.request_number
      }
    ).join("|"))
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
