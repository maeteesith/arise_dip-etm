import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-dashboard-public-role-01",
  templateUrl: "./dashboard-public-role-01.component.html",
  styleUrls: ["./dashboard-public-role-01.component.scss"]
})
export class DashboardPublicRole01Component implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any
  
  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

    // List DashboardPublicRole01
  public listDashboardPublicRole01: any[]
  public paginateDashboardPublicRole01: any
  public perPageDashboardPublicRole01: number[]
  // Response DashboardPublicRole01
  public responseDashboardPublicRole01: any
    public listDashboardPublicRole01Edit: any


  // List DocumentProcess
  public listDocumentProcess: any[]
  public paginateDocumentProcess: any
  public perPageDocumentProcess: number[]
  // Response DocumentProcess
  public responseDocumentProcess: any
  
//label_save_combobox || label_save_radio
  public departmentCodeList: any[]
  public documentRole05PrintListStatusCodeList: any[]
//label_save_combobox || label_save_radio

//Modal Initial
//Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

        this.input = {
      id: null,
      instruction_send_date: getMoment(),
      department_code: '',
      rule_count: '',
      document_role05_print_list_status_code: '',
      request_number: '',
    }
this.listDashboardPublicRole01 = []
this.paginateDashboardPublicRole01 = CONSTANTS.PAGINATION.INIT
this.perPageDashboardPublicRole01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentProcess = []

//Master List
    this.master = {
      departmentCodeList: [],
      documentRole05PrintListStatusCodeList: [],
    }
//Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

	
    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = {}
	
    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
	})
	
    this.forkJoinService.initDashboardPublicRole01().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
                        this.master.departmentCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.department_code = ""
                this.master.documentRole05PrintListStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.document_role05_print_list_status_code = ""
  
	  }
      if (this.editID) {
        this.DocumentProcessService.DashboardPublicRole01Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }
      
    })
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  onClickDashboardPublicRole01List(): void {
  // if(this.validateDashboardPublicRole01List()) {
  // Open loading
  // Call api
  var param = {
    instruction_send_date: this.input.instruction_send_date,
    department_code: this.input.department_code,
    rule_count: this.input.rule_count,
    document_role05_print_list_status_code: this.input.document_role05_print_list_status_code,
    request_number: this.input.request_number,

  }

  this.callDashboardPublicRole01List(param)
  // }
}
//! <<< Call API >>>
callDashboardPublicRole01List(params: any): void {
  this.global.setLoading(true)
  this.DocumentProcessService.DashboardPublicRole01List(this.help.GetFilterParams(params, this.paginateDashboardPublicRole01)).subscribe((data: any) => {
    // if(isValidDashboardPublicRole01ListResponse(res)) {
    if (data) {
      // Set value
      this.listData(data)
      this.automateTest.test(this, { list: data.list })
    }
    this.global.setLoading(false)
    // }
    // Close loading
  })
}

onClickReset(): void {
  this.global.setLoading(true)
  this.ngOnInit();
  this.global.setLoading(false)
}


onClickDashboardPublicRole01Send(): void {
    //if(this.validateDashboardPublicRole01Send()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callDashboardPublicRole01Send(this.saveData())
    //}
  }
  validateDashboardPublicRole01Send(): boolean {
      let result = validateService('validateDashboardPublicRole01Send', this.input)
      this.validate = result.validate
      return result.isValid
  }
  //! <<< Call API >>>
  callDashboardPublicRole01Send(params: any): void {
    this.DocumentProcessService.DashboardPublicRole01Send(params).subscribe((data: any) => {
      // if(isValidDashboardPublicRole01SendResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

onClickDashboardPublicRole01Add(): void {
  this.listDashboardPublicRole01.push({
      index: this.listDashboardPublicRole01.length + 1,
      request_number: null,
      instruction_send_date_text: null,
      document_role05_receiver_by_name: null,
      rule_count: null,
      name: null,
      house_number: null,
      document_role05_print_cover_date_text: null,
      post_number: null,
      document_role05_print_list_status_name: null,

  })
  this.changePaginateTotal(this.listDashboardPublicRole01.length, 'paginateDashboardPublicRole01')
}

onClickDashboardPublicRole01Edit(item: any): void {
  var win = window.open("/" + item.id)
}


onClickDashboardPublicRole01Delete(item: any): void {
  // if(this.validateDashboardPublicRole01Delete()) {
  // Open loading
  this.global.setLoading(true)
      // Set param
      if (this.listDashboardPublicRole01.filter(r => r.is_check).length > 0 || item) {
        if (item) {			
            this.listDashboardPublicRole01.forEach((_item: any) => { _item.is_check = _item == item })
        }

        let delete_save_item_count = 0
        if (true) {
          delete_save_item_count = this.listDashboardPublicRole01.filter(r=>r.is_check && r.id).length
        }

        var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
        if(rs && rs != "") {
          if (rs===true) rs = ""

        let ids = []

        for (let i = 0; i < this.listDashboardPublicRole01.length; i++) {
          if (this.listDashboardPublicRole01[i].is_check) {
            this.listDashboardPublicRole01[i].cancel_reason = rs
            this.listDashboardPublicRole01[i].status_code = "DELETE"
            this.listDashboardPublicRole01[i].is_deleted = true

            if (true && this.listDashboardPublicRole01[i].id) ids.push(this.listDashboardPublicRole01[i].id)
            // else this.listDashboardPublicRole01.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDashboardPublicRole01Delete(params, ids)
            return;
          }
        } 

        for (let i = 0; i < this.listDashboardPublicRole01.length; i++) {
            this.listDashboardPublicRole01[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
//! <<< Call API >>>
callDashboardPublicRole01Delete(params: any, ids: any[]): void {
  this.DocumentProcessService.DashboardPublicRole01Delete(params).subscribe((data: any) => {
    // if(isValidDashboardPublicRole01DeleteResponse(res)) {

    ids.forEach((id: any) => {
      for (let i = 0; i < this.listDashboardPublicRole01.length; i++) {
        if (this.listDashboardPublicRole01[i].id == id) {
          this.listDashboardPublicRole01.splice(i--, 1);
        }
      }
    });

    for (let i = 0; i < this.listDashboardPublicRole01.length; i++) {
        this.listDashboardPublicRole01[i] = i + 1
    }

              this.onClickDashboardPublicRole01List()
    // Close loading
    this.global.setLoading(false)
  })
}


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.DocumentProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

            this.listDashboardPublicRole01 = data.dashboardpublicrole01_list || []
                this.help.PageSet(data, this.paginateDashboardPublicRole01)

  }

  listData(data: any): void {
            this.listDashboardPublicRole01 = data.list || []
                this.help.PageSet(data, this.paginateDashboardPublicRole01)

  }

  saveData(): any {
    let params = this.input

            params.dashboardpublicrole01_list = this.listDashboardPublicRole01  || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

}
