/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MadridRole07SearchCancelComponent } from './madrid-role07-search-cancel.component';

describe('MadridRole07SearchCancelComponent', () => {
  let component: MadridRole07SearchCancelComponent;
  let fixture: ComponentFixture<MadridRole07SearchCancelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MadridRole07SearchCancelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MadridRole07SearchCancelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
