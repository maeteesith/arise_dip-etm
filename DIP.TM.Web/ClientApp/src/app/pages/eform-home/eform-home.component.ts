import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GlobalService } from "./../../global.service";
import { ROUTE_PATH, CONSTANTS, clone } from "../../helpers";

@Component({
  selector: "app-eform-home",
  templateUrl: "./eform-home.component.html",
  styleUrls: ["./eform-home.component.scss"],
})
export class eFormHomeComponent implements OnInit {
  //TODO >>> Declarations <<<
  public formList: any[];
  public searchText: any;
  public masterFormList: any[];
  public timeout: any;

  constructor(private router: Router, private global: GlobalService) {}

  ngOnInit() {
    this.formList = [
      // Fee
      {
        type: "fee",
        title: "แบบฟอร์ม ก.01",
        description:
          "คำขอจดทะเบียนครื่องหมายการค้า เครื่องหมายบริการ เครื่องหมายรับรอง หรือเครื่องหมายร่วม",
        code: "EFORM_SAVE_01_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.02",
        description: "คำคัดค้านการขอจดทะเบียนเครื่องหมายการค้า",
        code: "EFORM_SAVE_02_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.03",
        description: "คำอุทธรณ์",
        code: "EFORM_SAVE_030_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.04",
        description: "คำขอโอนหรือรับมรดกสิทธิ",
        code: "EFORM_SAVE_040_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.05",
        description: "คำขอจดทะเบียนสัญญาอนุญาตให้ใช้เครื่องหมายการค้า/บริการ",
        code: "EFORM_SAVE_050_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.06",
        description: "คำขอแก้ไขเปลี่ยนแปลงรายการจดทะเบียน",
        code: "EFORM_SAVE_060_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.07",
        description: "คำขอต่ออายุการจดทะเบียน",
        code: "EFORM_SAVE_070_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.08",
        description: "คำขอให้เพิกถอนการจดทะเบียน",
        code: "EFORM_SAVE_080_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.21",
        description: "คำขอให้บันทึกการจดทะเบียนระหว่างประเทศแทนการจดทะเบียนในราชอาณาจักร",
        code: "EFORM_SAVE_210_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.22",
        description: "คำขอจดทะเบียนแปลงทะเบียนระหว่างประเทศเป็นคำขอในราชอาณาจักร",
        code: "EFORM_SAVE_220_PROCESS",
      },
      {
        type: "fee",
        title: "แบบฟอร์ม ก.23",
        description: "แบบฟอร์ม ก.23",
        code: "EFORM_SAVE_230_PROCESS",
      },

      // No Fee
      {
        type: "noFee",
        title: "แบบฟอร์ม ก.02",
        description: "คำโต้แย้ง",
        code: "EFORM_SAVE_021_PROCESS",
      },
      {
        type: "noFee",
        title: "แบบฟอร์ม ก.12",
        description: "หนังสือแสดงการปฏิเสธ",
        code: "EFORM_SAVE_120_PROCESS",
      },
      {
        type: "noFee",
        title: "แบบฟอร์ม ก.14",
        description: "หนังสือแจ้งฟ้องคดีต่อศาล คำพิพากษาและผลคดี",
        code: "EFORM_SAVE_140_PROCESS",
      },
      {
        type: "noFee",
        title: "แบบฟอร์ม ก.15",
        description: "หนังสือชี้แจงการถูกร้องขอให้เพิกถอนการจดทะเบียน",
        code: "EFORM_SAVE_150_PROCESS",
      },
      {
        type: "noFee",
        title: "แบบฟอร์ม ก.19",
        description: "หนังสือขอผ่อนผันการส่งหลักฐาน",
        code: "EFORM_SAVE_190_PROCESS",
      },
      {
        type: "noFee",
        title: "แบบฟอร์ม ก.20",
        description: "หนังสือนำส่งเอกสารหลักฐานและคำชี้แจง",
        code: "EFORM_SAVE_200_PROCESS",
      },
    ];
    this.searchText = "";

    // Add search key
    this.formList.forEach((item: any) => {
      item.searchKey = `${item.title}${item.description}`;
    });

    // Clone list
    this.masterFormList = clone(this.formList);
  }

  //! <<< Event >>>
  onSearch(): void {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (this.searchText) {
        this.formList = this.masterFormList.filter((item: any) =>
          item.searchKey.toLowerCase().includes(this.searchText.toLowerCase())
        );
      } else {
        this.formList = clone(this.masterFormList);
      }
    }, CONSTANTS.DELAY_CALL_API);
  }
  getSizeForm(type: any): any {
    let size = 0;
    this.formList.forEach((item: any) => {
      if (item.type === type) {
        size++;
      }
    });
    return size === 0 ? "ไม่พบ" : size;
  }
  go(code: any): void {
    this.global.setLoading(true);
    this.router.navigate([ROUTE_PATH[code].LINK]);
  }
}
