import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { Router } from "@angular/router";

import {
    ROUTE_PATH,
    CONSTANTS,
    validateService,
    clone,
    getItemCalculator,
    displayMoney,
    displayFormatBytes,
    PapaParseCsvToJson,
    loopDisplayDateServer,
    displayAddress,
    viewPDF,

} from "../../helpers";
import { MatDialog } from '@angular/material';
import { MadridRole07CreateDocModalComponent } from '../madrid-role07-create-doc-modal/madrid-role07-create-doc-modal.component';
import { MadridRole07ModalCheckComponent } from '../madrid-role07-modal-check/madrid-role07-modal-check.component';
import { MadridRole07ModalSaveComponent } from '../madrid-role07-modal-save/madrid-role07-modal-save.component';

@Component({
    selector: "app-madrid-role07-save-list",
    templateUrl: "./madrid-role07-save-list.component.html",
    styleUrls: ["./madrid-role07-save-list.component.scss",
    './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole07SaveListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any
    public role: any


    public modal: any

    public listPublicItem: any = [];
    public doc: any = [];

    public selectedDoc: any = -1;

    ngOnInit() {

        this.role = true;
        this.listPublicItem = [
            { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", statusprogress: "บันทึก", command: "" },
            { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", statusprogress: "รอตรวจสอบ", command: "" }
        ]

        this.doc = [{ value: "1", text: "MMC"},{ value: "2", text: "MMU"}]

        this.validate = {}
        this.input = {
            id: null,
            //instruction_send_start_date: getMoment(),
            //instruction_send_end_date: getMoment(),
            public_type_code: '',
            public_source_code: '',
            public_receiver_by: '',
            public_status_code: '',
            request_number: '',
        }
        this.master = {
            request_type: [{ code: 1, name: '1' }],
            job_reciver: [{ id: 1, name: "Sam" }, { id: 2, name: "Tom" }]
        }

        // this.open();
        this.openModalCheck();
    }



    constructor(
        private help: Help,
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService,
        public dialog: MatDialog
    ) { }


    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }



    //! <<< ----------------- >>>
    //! <<< --- Copy Zone --- >>>
    //! <<< ----------------- >>>

    //! <<< Binding (one way) >>>
    binding(obj: any, name: any, value: any): void {
        this[obj][name] = value
    }

    //! <<< Table >>>
    onSelectColumnInTable(name: string, item: any): void {
        if (!item) {
            this.input.is_check_all = !this.input.is_check_all
            this[name].forEach((item: any) => {
                item.is_check = this.input.is_check_all
            })
        } else {
            item.is_check = !item.is_check
        }
    }
    onChangeInputInTable(item: any, name: any, value: any): void {
        item[name] = value
    }
    toggleInputInTable(item: any, name: any): void {
        item[name] = !item[name]
    }

    //! <<<< Pagination >>>
    managePaginateCallApi(name: string): void {
        if (name === 'paginateDocumentRole02Check') {
            //this.onClickDocumentRole02CheckList()
            this.input.is_check_all = false
        }
    }
    onChangePage(page: any, name: string): void {
        this[name].currentPage = page === '' ? 1 : page
        if ((+page || page === '') && page <= this.getMaxPage(name)) {
            this.managePaginateCallApi(name)
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
        this.managePaginateCallApi(name)
    }
    changePaginateTotal(total: any, name: string): void {
        this[name].totalItems = total
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }


    open(){
        const dialogRef = this.dialog.open(MadridRole07CreateDocModalComponent, {
            width: "calc(100% - 80px)",
            maxWidth: "1000px",
            data: {}
          });
          dialogRef.afterClosed().subscribe(res => {
          });
    }

    openModalCheck(){
        const dialogRef = this.dialog.open(MadridRole07ModalCheckComponent, {
            width: "calc(100% - 80px)",
            maxWidth: "1400px",
            data: {}
          });
          dialogRef.afterClosed().subscribe(res => {
          });
    }
    openModalSave(){
      const dialogRef = this.dialog.open(MadridRole07ModalSaveComponent, {
          width: "calc(100% - 80px)",
          maxWidth: "1000px",
          data: {}
        });
        dialogRef.afterClosed().subscribe(res => {
        });
  }
}
