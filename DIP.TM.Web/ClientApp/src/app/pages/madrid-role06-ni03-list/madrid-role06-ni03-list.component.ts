import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { GlobalService } from './../../global.service';
import { AutomateTest } from './../../test/automate_test';
import { Help } from './../../helpers/help';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-madrid-role06-ni03-list',
  templateUrl: './madrid-role06-ni03-list.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./madrid-role06-ni03-list.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole06Ni03ListComponent implements OnInit {

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,

  ) { }

  ngOnInit() {
  }

}
