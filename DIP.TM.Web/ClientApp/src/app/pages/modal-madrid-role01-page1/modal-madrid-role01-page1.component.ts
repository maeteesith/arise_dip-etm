import { Component, Input, Output, OnInit, EventEmitter, Inject } from "@angular/core";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { CONSTANTS, validateService } from "../../helpers";
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material'

@Component({
  selector: "app-modal-madrid-role01-page1",
  templateUrl: "./modal-madrid-role01-page1.component.html",
  styleUrls: ["./modal-madrid-role01-page1.component.scss",
  './../../../assets/theme/styles/madrid/madrid.scss'],
})
export class ModalMadridRole01Page1Component implements OnInit {
  //TODO >>> Declarations <<<
  

  
  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalMadridRole01Page1Component>,
    
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
   
  }
  closeModal(){
    this.dialogRef.close();
  }
  
}
