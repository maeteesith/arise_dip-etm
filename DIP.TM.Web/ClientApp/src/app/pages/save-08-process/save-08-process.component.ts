import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-save-08-process",
  templateUrl: "./save-08-process.component.html",
  styleUrls: ["./save-08-process.component.scss"]
})
export class Save08ProcessComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List People
  public listPeople: any[]
  public paginatePeople: any
  public perPagePeople: number[]
  // Response People
  public responsePeople: any
  public listPeopleEdit: any

  // List Representative
  public listRepresentative: any[]
  public paginateRepresentative: any
  public perPageRepresentative: number[]
  // Response Representative
  public responseRepresentative: any
  public listRepresentativeEdit: any

  // List ContactAddress
  public listContactAddress: any[]
  public paginateContactAddress: any
  public perPageContactAddress: number[]
  // Response ContactAddress
  public responseContactAddress: any
  public listContactAddressEdit: any

  // List Joiner
  public listJoiner: any[]
  public paginateJoiner: any
  public perPageJoiner: number[]
  // Response Joiner
  public responseJoiner: any
  public listJoinerEdit: any

  // List JoinerRepresentative
  public listJoinerRepresentative: any[]
  public paginateJoinerRepresentative: any
  public perPageJoinerRepresentative: number[]
  // Response JoinerRepresentative
  public responseJoinerRepresentative: any
  public listJoinerRepresentativeEdit: any


  //label_save_combobox || label_save_radio
  public save080RevokeTypeCodeList: any[]
  public addressTypeCodeList: any[]
  public addressCardTypeCodeList: any[]
  public addressReceiverTypeCodeList: any[]
  public addressSexCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  public autocompleteListListLocation: any
  public is_autocomplete_ListLocation_show: any
  public is_autocomplete_ListLocation_load: any
  //Modal Initial
  //Modal Initial

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  // List AddressCopy
  public listAddressCopy: any[]
  public paginateAddressCopy: any


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      registration_number: '',
      make_date: getMoment(),
      irn_number: '',
      request_date_text: '',
      total_price: '',
      save080_revoke_type_code: '',
      remark: '',
    }
    this.listPeople = []
    this.paginatePeople = CONSTANTS.PAGINATION.INIT
    this.perPagePeople = CONSTANTS.PAGINATION.PER_PAGE

    this.listRepresentative = []
    this.paginateRepresentative = CONSTANTS.PAGINATION.INIT
    this.perPageRepresentative = CONSTANTS.PAGINATION.PER_PAGE

    this.listContactAddress = []
    this.paginateContactAddress = CONSTANTS.PAGINATION.INIT
    this.perPageContactAddress = CONSTANTS.PAGINATION.PER_PAGE

    this.listJoiner = []
    this.paginateJoiner = CONSTANTS.PAGINATION.INIT
    this.perPageJoiner = CONSTANTS.PAGINATION.PER_PAGE

    this.listJoinerRepresentative = []
    this.paginateJoinerRepresentative = CONSTANTS.PAGINATION.INIT
    this.perPageJoinerRepresentative = CONSTANTS.PAGINATION.PER_PAGE

    this.listAddressCopy = []
    this.paginateAddressCopy = CONSTANTS.PAGINATION.INIT

    //Master List
    this.master = {
      save080RevokeTypeCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false
    this.autocompleteListListLocation = []
    this.is_autocomplete_ListLocation_show = false
    this.is_autocomplete_ListLocation_load = false


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //
    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave08Process().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)

      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save08Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          pThis.global.setLoading(false)
        })
      }

      this.global.setLoading(false)
    })
  }

  constructor(
    private help: Help,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService
  ) { }

  autocompleteKeyDown(event: any) {
    if (event.key == "Enter") {
      this.autocompleteChangeListNotSent(this.input, 'request_number', 5, 1)
    }
  }


  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: {
          request_number: object[name],
        },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    let pThis = this
    this.SaveProcessService.Save08ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListNotSent = data
        }
      }
    })
    this.input.is_autocomplete_ListNotSent_load = false
  }
  autocompleteChooseListNotSent(data: any): void {
    this.loadData(data)

    this.is_autocomplete_ListNotSent_show = false
  }


  onClickSave08Madrid(): void {
    //if(this.validateSave08Madrid()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave08Madrid(this.saveData())
    //}
  }
  validateSave08Madrid(): boolean {
    let result = validateService('validateSave08Madrid', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave08Madrid(params: any): void {
    let pThis = this
    this.SaveProcessService.Save08Madrid(params).subscribe((data: any) => {
      // if(isValidSave08MadridResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  // Autocomplete
  autocompleteChangeListLocation(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListLocation(params)
    }
  }
  autocompleteBlurListLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListLocation(params: any): void {
    if (this.input.is_autocomplete_ListLocation_load) return
    this.input.is_autocomplete_ListLocation_load = true
    this.global.setLoading(true)
    let pThis = this
    this.SaveProcessService.Save08ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListLocation = data
          this.input.is_autocomplete_ListLocation_load = false
          this.global.setLoading(false)
        }
      }
    })
  }
  autocompleteChooseListLocation(data: any): void {
    this.inputAddress.address_sub_district_code = data.code
    this.inputAddress.address_sub_district_name = data.name
    this.inputAddress.address_district_code = data.district_code
    this.inputAddress.address_district_name = data.district_name
    this.inputAddress.address_province_code = data.province_code
    this.inputAddress.address_province_name = data.province_name
    this.inputAddress.address_country_code = data.country_code
    this.inputAddress.address_country_name = data.country_name

    this.is_autocomplete_ListLocation_show = false
    this.global.setLoading(false)
  }


  onClickSave08Delete(): void {
    //if(this.validateSave08Delete()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave08Delete(this.saveData())
    //}
  }
  validateSave08Delete(): boolean {
    let result = validateService('validateSave08Delete', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave08Delete(params: any): void {
    let pThis = this
    this.SaveProcessService.Save08Delete(params).subscribe((data: any) => {
      // if(isValidSave08DeleteResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave08Save(): void {
    //if(this.validateSave08Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave08Save(this.saveData())
    //}
  }
  validateSave08Save(): boolean {
    let result = validateService('validateSave08Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave08Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save08Save(params).subscribe((data: any) => {
      // if(isValidSave08SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave08Copy01(): void {
    //if(this.validateSave08Copy01()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave08Copy01(this.saveData())
    //}
  }
  validateSave08Copy01(): boolean {
    let result = validateService('validateSave08Copy01', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave08Copy01(params: any): void {
    let pThis = this
    this.SaveProcessService.Save08Copy01(params).subscribe((data: any) => {
      // if(isValidSave08Copy01Response(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickSave08Send(): void {
    //if(this.validateSave08Send()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave08Send(this.saveData())
    //}
  }
  validateSave08Send(): boolean {
    let result = validateService('validateSave08Send', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave08Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save08Send(params).subscribe((data: any) => {
      // if(isValidSave08SendResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }


  onClickSave080RequestBack(): void {
    //if(this.validateSave080Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave080RequestBack(this.saveData())
    //}
  }
  callSave080RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save080RequestBack(params).subscribe((data: any) => {
      // if(isValidSave080SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  onClickSave08PeopleAdd(): void {
    this.listPeople.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listPeople[this.listPeople.length - 1])
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
  }

  onClickSave08PeopleEdit(item: any): void {
    this.inputModalAddressEdit = item
    this.inputModalAddress = clone(item)

    this.toggleModal("isModalPeopleEditOpen")
  }


  onClickSave08PeopleDelete(item: any): void {
    // if(this.validateSave08PeopleDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listPeople.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listPeople.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listPeople.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listPeople.length; i++) {
          if (this.listPeople[i].is_check) {
            this.listPeople[i].cancel_reason = rs
            this.listPeople[i].status_code = "DELETE"
            this.listPeople[i].is_deleted = true

            if (true && this.listPeople[i].id) ids.push(this.listPeople[i].id)
            // else this.listPeople.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave08PeopleDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listPeople.length; i++) {
          this.listPeople[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callSave08PeopleDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save08PeopleDelete(params).subscribe((data: any) => {
      // if(isValidSave08PeopleDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listPeople.length; i++) {
          if (this.listPeople[i].id == id) {
            this.listPeople.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listPeople.length; i++) {
        this.listPeople[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave08RepresentativeAdd(): void {
    this.listRepresentative.push({
      index: this.listRepresentative.length + 1,
      name: null,
      house_number: null,
      telephone: null,
      save080_representative_type_code: this.master.save080RepresentativeTypeCodeList[0].code,

    })
    this.onClickAddressEdit(this.listRepresentative[this.listRepresentative.length - 1], 'ตัวแทน')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
  }

  onClickSave08RepresentativeEdit(item: any): void {
    this.inputModalAddressEdit = item
    this.inputModalAddress = clone(item)

    this.toggleModal("isModalPeopleEditOpen")
  }


  onClickSave08RepresentativeDelete(item: any): void {
    // if(this.validateSave08RepresentativeDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listRepresentative.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listRepresentative.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listRepresentative.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listRepresentative.length; i++) {
          if (this.listRepresentative[i].is_check) {
            this.listRepresentative[i].cancel_reason = rs
            this.listRepresentative[i].status_code = "DELETE"
            this.listRepresentative[i].is_deleted = true

            if (true && this.listRepresentative[i].id) ids.push(this.listRepresentative[i].id)
            // else this.listRepresentative.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave08RepresentativeDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listRepresentative.length; i++) {
          this.listRepresentative[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callSave08RepresentativeDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save08RepresentativeDelete(params).subscribe((data: any) => {
      // if(isValidSave08RepresentativeDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listRepresentative.length; i++) {
          if (this.listRepresentative[i].id == id) {
            this.listRepresentative.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listRepresentative.length; i++) {
        this.listRepresentative[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave08ContactAddressAdd(): void {
    this.listContactAddress.push({

    })
    this.changePaginateTotal(this.listContactAddress.length, 'paginateContactAddress')
  }

  onClickSave08JoinerAdd(): void {
    const params = {
      request_number: this.input.request_number,
    }
    this.paginateAddressCopy.item_per_page = 100

    this.SaveProcessService.Save05ListPage(this.help.GetFilterParams(params, this.paginateAddressCopy)).subscribe((data: any) => {
      if (data) {
        this.listAddressCopy = []
        if (data.list.length > 0) {
          data.list.forEach((item: any) => {
            item.people_list.forEach((people: any) => {
              this.listAddressCopy.push(people)
            })
          })
        } else {
          alert("ไม่พบข้อมูลผู้ได้รับอนุญาตจาก ก.05")
        }

        //console.log(this.listAddressCopy)
        if (this.listAddressCopy.length > 0) {
          this.toggleModal('isModalAddressCopyOpen')
        }
      }
    })
  }
  onClickAddressCopySelect(): void {
    this.listAddressCopy.filter(r => r.is_check).forEach((item: any) => {
      delete item.is_check
      delete item.id
      delete item.save_id

      this.listJoiner.push(item)
    })
    this.toggleModal('isModalAddressCopyOpen')
  }

  onClickSave08JoinerEdit(item: any): void {
    this.inputModalAddressEdit = item
    this.inputModalAddress = clone(item)

    this.toggleModal("isModalPeopleEditOpen")
  }


  onClickSave08JoinerDelete(item: any): void {
    // if(this.validateSave08JoinerDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listJoiner.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listJoiner.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listJoiner.filter(r => r.is_check && r.id).length
      }

      var rs = confirm("คุณต้องการลบรายการ?")
      if (rs) {
        let ids = []

        for (let i = 0; i < this.listJoiner.length; i++) {
          if (this.listJoiner[i].is_check) {
            //this.listJoiner[i].cancel_reason = rs
            //this.listJoiner[i].status_code = "DELETE"
            this.listJoiner[i].is_deleted = true

            if (true && this.listJoiner[i].id) ids.push(this.listJoiner[i].id)
            // else this.listJoiner.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave08JoinerDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listJoiner.length; i++) {
          this.listJoiner[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callSave08JoinerDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save08JoinerDelete(params).subscribe((data: any) => {
      // if(isValidSave08JoinerDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listJoiner.length; i++) {
          if (this.listJoiner[i].id == id) {
            this.listJoiner.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listJoiner.length; i++) {
        this.listJoiner[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave08JoinerRepresentativeAdd(): void {
    this.listJoinerRepresentative.push({
      index: this.listJoinerRepresentative.length + 1,
      name: null,
      house_number: null,
      telephone: null,
      fax: null,

    })
    this.changePaginateTotal(this.listJoinerRepresentative.length, 'paginateJoinerRepresentative')
  }

  onClickSave08JoinerRepresentativeEdit(item: any): void {
    this.inputModalAddressEdit = item
    this.inputModalAddress = clone(item)

    this.toggleModal("isModalPeopleEditOpen")
  }


  onClickSave08JoinerRepresentativeDelete(item: any): void {
    // if(this.validateSave08JoinerRepresentativeDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listJoinerRepresentative.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listJoinerRepresentative.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listJoinerRepresentative.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listJoinerRepresentative.length; i++) {
          if (this.listJoinerRepresentative[i].is_check) {
            this.listJoinerRepresentative[i].cancel_reason = rs
            this.listJoinerRepresentative[i].status_code = "DELETE"
            this.listJoinerRepresentative[i].is_deleted = true

            if (true && this.listJoinerRepresentative[i].id) ids.push(this.listJoinerRepresentative[i].id)
            // else this.listJoinerRepresentative.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave08JoinerRepresentativeDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listJoinerRepresentative.length; i++) {
          this.listJoinerRepresentative[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callSave08JoinerRepresentativeDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save08JoinerRepresentativeDelete(params).subscribe((data: any) => {
      // if(isValidSave08JoinerRepresentativeDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listJoinerRepresentative.length; i++) {
          if (this.listJoinerRepresentative[i].id == id) {
            this.listJoinerRepresentative.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listJoinerRepresentative.length; i++) {
        this.listJoinerRepresentative[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }


  onClickAddressCopy(item_list: any[], copy_item: any): void {
    var index = 1
    item_list.forEach((item: any) => {
      item.index = index++
      if (item === copy_item) {
        var item_new = { ...item }
        item_new.id = null
        item_new.index = index++
        item_list.splice(index + 1, 0, item_new)
      }
    })
  }
  onClickAddressEdit(item: any, name: string = "เจ้าของ"): void {
    this.modalPeopleText = name
    this.modalAddressEdit = item
    this.help.Clone(item, this.modalAddress)

    this.modalAddress.is_representative = name.indexOf("ตัวแทน") >= 0

    this.modal["isModalPeopleEditOpen"] = true

  }
  onClickAddressSave(): void {
    this.help.Clone(this.modalAddress, this.modalAddressEdit)
    this.help.GetAddressInformation(this.modalAddressEdit)

    this.modal["isModalPeopleEditOpen"] = false
  }
  onClickAddressDelete(item_list: any[], item: any): void {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (item_list.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        item_list.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = item_list.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < item_list.length; i++) {
          if (item_list[i].is_check) {
            //item_list[i].cancel_reason = rs
            item_list[i].is_deleted = true
            //console.log(item_list[i])
            //item_list[i].status_code = "DELETE"

            if (true && item_list[i].id) ids.push(item_list[i].id)
            else item_list.splice(i--, 1);
          }
        }
      }
    }
    //console.log(item_list)
    this.global.setLoading(false)
  }


  onClickSaveCopyOld(): void {
    var rs = prompt("กรุณาใส่เลขที่คำขอ");
    if (rs && rs != "") {
      const params = {
        request_number: rs
      }

      this.callSaveCopyOld(params)
    }
  }
  callSaveCopyOld(params: any): void {
    this.SaveProcessService.Save01List(params).subscribe((data: any) => {
      // if(isValidSave01CopyOldResponse(res)) {
      if (data && data.length > 0) {
        this.listPeople = JSON.parse(JSON.stringify(data[0].people_list)) || []
        this.listPeople.forEach((item: any) => {
          delete item.id
          delete item.save_id
        })
        this.listRepresentative = JSON.parse(JSON.stringify(data[0].representative_list)) || []
        this.listRepresentative.forEach((item: any) => {
          delete item.id
          delete item.save_id
        })
        this.help.Clone(data[0].contact_address || {}, this.contactAddress)
        delete this.contactAddress.id
        delete this.contactAddress.save_id
        this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"
        //this.input.trademark_role = data[0].trademark_role

        this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
        this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
      }
      this.global.setLoading(false)
    })
  }





  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    this.SaveProcessService.Save08ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListModalLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListModalLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave08ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }


  onClickSave08DocumentView(): void {
    window.open('/File/RequestDocumentCollect/80/' + this.input.request_number + "_" + this.input.request_id)
  }


  loadData(data: any): void {
    this.input = data

    //this.listPeople = data.people_list || []
    //this.listRepresentative = data.representative_list || []
    this.help.Clone(data.contact_address || {}, this.contactAddress)
    this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"
    //this.listJoiner = data.joiner_list || []

    //this.inputAddress = data.contact_address || {}
    this.listPeople = data.people_list || []
    this.listRepresentative = data.representative_list || []
    this.listContactAddress = data.contact_address_list || []
    this.listJoiner = data.joiner_list || []
    this.listJoinerRepresentative = data.joiner_representative_list || []
    //let index = 1
    //index = 1
    //this.listPeople.map((item: any) => { item.is_check = false; item.index = index++; return item })
    //index = 1
    //this.listRepresentative.map((item: any) => { item.is_check = false; item.index = index++; return item })
    //index = 1
    //this.listContactAddress.map((item: any) => { item.is_check = false; item.index = index++; return item })
    //index = 1
    //this.listJoiner.map((item: any) => { item.is_check = false; item.index = index++; return item })
    //index = 1
    //this.listJoinerRepresentative.map((item: any) => { item.is_check = false; item.index = index++; return item })
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
    this.changePaginateTotal(this.listContactAddress.length, 'paginateContactAddress')
    this.changePaginateTotal(this.listJoiner.length, 'paginateJoiner')
    this.changePaginateTotal(this.listJoinerRepresentative.length, 'paginateJoinerRepresentative')

  }

  listData(data: any): void {
    this.listPeople = data || []
    this.listRepresentative = data || []
    this.listContactAddress = data || []
    this.listJoiner = data || []
    this.listJoinerRepresentative = data || []
    let index = 1
    index = 1
    this.listPeople.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listRepresentative.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listContactAddress.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listJoiner.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listJoinerRepresentative.map((item: any) => { item.is_check = false; item.index = index++; return item })
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
    this.changePaginateTotal(this.listContactAddress.length, 'paginateContactAddress')
    this.changePaginateTotal(this.listJoiner.length, 'paginateJoiner')
    this.changePaginateTotal(this.listJoinerRepresentative.length, 'paginateJoinerRepresentative')

  }

  saveData(): any {
    let params = this.input

    params.contact_address = this.contactAddress
    //params.contact_address = this.inputAddress || []
    params.people_list = this.listPeople || []
    params.representative_list = this.listRepresentative || []
    params.contact_address_list = this.listContactAddress || []
    params.joiner_list = this.listJoiner || []
    params.joiner_representative_list = this.listJoinerRepresentative || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
