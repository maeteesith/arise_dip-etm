import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-madrid-role07-create-doc-modal',
  templateUrl: './madrid-role07-create-doc-modal.component.html',
  styleUrls: ['./madrid-role07-create-doc-modal.component.scss']
})
export class MadridRole07CreateDocModalComponent implements OnInit {
  
  ItemArray : any = [];

  Item: any;

  constructor(
    public dialogRef: MatDialogRef<MadridRole07CreateDocModalComponent>
  ) { }

  ngOnInit() {
  }

  closeModal() {
    this.dialogRef.close();
  }

  addItem(){
    if(this.Item == null)
      return

    this.ItemArray.push(this.Item);
    this.Item = null;
  }

  deleteItem(index : any){
    this.ItemArray.splice(index, 1)
  }

}
