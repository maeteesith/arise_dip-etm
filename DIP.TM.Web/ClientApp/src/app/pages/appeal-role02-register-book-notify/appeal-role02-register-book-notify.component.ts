import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join.service'

@Component({
  selector: 'app-appeal-role02-register-book-notify',
  templateUrl: './appeal-role02-register-book-notify.component.html',
  styleUrls: ['./appeal-role02-register-book-notify.component.scss']
})
export class AppealRole02RegisterBookNotifyComponent implements OnInit {

  constructor(
    private forkJoinService: ForkJoinService,
    private global: GlobalService,
  ) { }

    public input: any
    public master: any
    public appealReasonList: any[]

  ngOnInit() {
    this.input = {
      reason_code: "TYPE01",
    }
    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initAppealBookPage().subscribe((data: any) => {
      if (data) {
        this.appealReasonList = data.appealReasonList
      }
      this.global.setLoading(false)
    })
  }

}
