import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-checking-similar-image",
  templateUrl: "./checking-similar-image.component.html",
  styleUrls: ["./checking-similar-image.component.scss"]
})
export class CheckingSimilarImageComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List CheckingProcess
  public listCheckingProcess: any[]
  public paginateCheckingProcess: any
  public perPageCheckingProcess: number[]
  // Response CheckingProcess
  public responseCheckingProcess: any
  public listCheckingProcessEdit: any


  // List Trademark
  public listTrademark: any[]
  public list_listTrademark: any[]

  public paginateTrademark: any
  public perPageTrademark: number[]
  // Response Trademark
  public responseTrademark: any
  public listTrademarkEdit: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public popup: any

  public image_show_zoom_list: any[]

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      checking_method_type_code: '',
      full_view: {
        checking_similar_wordsoundtranslate_list: []
      },
    }
    this.listCheckingProcess = []
    this.paginateCheckingProcess = CONSTANTS.PAGINATION.INIT
    this.perPageCheckingProcess = CONSTANTS.PAGINATION.PER_PAGE


    this.paginateTrademark = CONSTANTS.PAGINATION.INIT
    this.perPageTrademark = CONSTANTS.PAGINATION.PER_PAGE


    this.image_show_zoom_list = [10, 25, 75, 100, 125, 150, 200, 400, 800, 1600, 2400, 3200, 6400]

    //Master List
    this.master = {
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.popup = {
      image_show_zoom: 100,
    }

    this.dragElement(document.getElementById("div_image_show"))

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)

    this.forkJoinService.initCheckingSimilarImage().subscribe((data: any) => {
      if (data) {
        this.master = data

      }

      if (this.editID) {
        let pThis = this
        this.CheckingProcessService.CheckingSimilarImageLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
            this.onClickCheckingSimilarSave010List()

            //var column_list = [
            //  "search_1_word_first_code",
            //  "search_1_word_sound_last_code",
            //  "search_1_word_mark",
            //  "search_1_word_sound_last_other_code",
            //  "search_1_2_word_syllable_sound",

            //  "search_1_request_number_start",
            //  "search_1_request_number_end",
            //  "search_1_people_name",
            //  "search_1_request_item_sub_type_1_code",
            //  "search_1_request_item_sub_type_1_description_text",
            //  "search_1_1_word_samecondition_code",
            //  "search_1_1_word_mark",
            //  "search_1_2_word_samecondition_code",
            //  "search_1_document_classification_image_code",
            //  "search_1_document_classification_image_condition",
            //  "search_1_document_classification_sound_code",
            //  "search_1_document_classification_sound_condition",
            //]
            //this.route.queryParams.subscribe(params => {
            //  let param = {}

            //  //console.log(params)

            //  param["request01_item_id"] = parseInt(this.editID)
            //  Object.keys(params).forEach((item: any) => {
            //    if (column_list.indexOf(item) >= 0)
            //      param[item] = params[item]
            //  })

            //  param["is_have_image"] = true

            //  this.CheckingProcessService.CheckingSimilarSave010List(param).subscribe((data: any) => {
            //    if (data) {
            //      this.listTrademark = data || []
            //      this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')

            //      //this.listTrademark.push(this.listTrademark[0])
            //      //this.listTrademark.push(this.listTrademark[0])
            //      //this.listTrademark.push(this.listTrademark[0])
            //      //this.listTrademark.push(this.listTrademark[0])
            //      //this.listTrademark.push(this.listTrademark[0])
            //      //this.listTrademark.push(this.listTrademark[0])

            //      this.list_listTrademark = []
            //      while (this.listTrademark.length > 0) {
            //        var trademark = this.listTrademark.splice(0, 5)
            //        while (5 > trademark.length) trademark.push({})
            //        this.list_listTrademark.push(trademark)
            //      }

            //      //this.list_listTrademark.push(this.list_listTrademark[0])
            //      //this.list_listTrademark.push(this.list_listTrademark[0])
            //      //this.list_listTrademark.push(this.list_listTrademark[0])
            //      //this.list_listTrademark.push(this.list_listTrademark[0])

            //      //this.listTrademark.forEach((item: any) => {
            //      //  item.product_text = item.full_view.product_list.map(function (item) {
            //      //    if (param["search_1_request_item_sub_type_1_code"]) {
            //      //      var search_1_request_item_sub_type_1_code = "|" + param["search_1_request_item_sub_type_1_code"] + "|"
            //      //      if (search_1_request_item_sub_type_1_code.indexOf("|" + item.request_item_sub_type_1_code + "|")) {
            //      //        return "<font color='red'>" + item.request_item_sub_type_1_code + "</font>"
            //      //      }
            //      //    }
            //      //    return item.request_item_sub_type_1_code
            //      //  }).join(", ")

            //      //  item.word_mark = item.full_view.document_classification_word_list.map(function (item) {
            //      //    if (param["search_1_1_word_mark"]) {
            //      //      if (item.word_mark.indexOf(param["search_1_1_word_mark"]) >= 0) {
            //      //        return item.word_mark.replace(param["search_1_1_word_mark"], "<font color='red'>" + param["search_1_1_word_mark"] + "</font>")
            //      //      }
            //      //    }

            //      //    return item.word_mark
            //      //  }).join("<br>")

            //      //  item.people_text = item.full_view.people_list.map(function (item) {
            //      //    if (param["search_1_people_name"]) {
            //      //      if (param["search_1_people_name"] == item.name) {
            //      //        return "<font color='red'>" + item.name + "</font>"
            //      //      }
            //      //    }
            //      //    return item.name
            //      //  }).join("<br>")

            //      //  item.soud_text = item.full_view.document_classification_sound_list.map(function (item) {
            //      //    if (param["search_1_document_classification_sound_code"]) {
            //      //      var search_1_document_classification_sound_code = "|" + param["search_1_document_classification_sound_code"] + "|"
            //      //      if (search_1_document_classification_sound_code.indexOf("|" + item.document_classification_sound_code + "|")) {
            //      //        return "<font color='red'>" + item.document_classification_sound_name + "</font>"
            //      //      }
            //      //    }
            //      //    return item.document_classification_sound_name
            //      //  }).join("<br>")
            //      //})
            //    }

            //    this.global.setLoading(false)

            //    //this.CheckingProcessService.CheckingTagSimilarMethod(param).subscribe((data: any) => {
            //    //  if (data) {
            //    //    //this.checkingTagSimilarMethod = data
            //    //  }
            //    //  // Close loading
            //    //  pThis.global.setLoading(false)
            //    //  this.automateTest.test(this)
            //    //})
            //  })

            //});
          }
        })
      } else {
        this.global.setLoading(false)
      }
    })
  }

  constructor(
    private help: Help,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }


  onClickCheckingSimilarSave010List(): void {
    // if(this.validateSave021List()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingSimilarSave010List()
    // }
  }
  //! <<< Call API >>>
  callCheckingSimilarSave010List(): void {
    var column_list = [
      "search_1_word_first_code",
      "search_1_word_sound_last_code",
      "search_1_word_mark",
      "search_1_word_sound_last_other_code",
      "search_1_2_word_syllable_sound",

      "search_1_request_number_start",
      "search_1_request_number_end",
      "search_1_people_name",
      "search_1_request_item_sub_type_1_code",
      "search_1_request_item_sub_type_1_description_text",
      "search_1_1_word_samecondition_code",
      "search_1_1_word_mark",
      "search_1_2_word_samecondition_code",
      "search_1_document_classification_image_code",
      "search_1_document_classification_image_condition",
      "search_1_document_classification_sound_code",
      "search_1_document_classification_sound_condition",
    ]
    this.route.queryParams.subscribe(params => {
      let param = {}

      this.input.is_have_sound = params.is_have_sound

      let pThis = this

      param["request01_item_id"] = parseInt(this.editID)
      Object.keys(params).forEach((item: any) => {
        if (column_list.indexOf(item) >= 0)
          param[item] = params[item]
      })

      this.paginateTrademark.page_index = this.paginateTrademark.page_index || 1
      this.paginateTrademark.item_per_page = +this.paginateTrademark.item_per_page || 10
      param["paginate"] = this.paginateTrademark

      this.list_listTrademark = []
      this.CheckingProcessService.CheckingSimilarSave010List(param).subscribe((data: any) => {
        if (data) {
          this.listTrademark = data.list
          this.help.PageSet(data, this.paginateTrademark)

          this.listTrademark.forEach((item: any) => {
            //console.log(item.full_view.product_list)
            item.request_item_sub_type_1_code_text = item.request_item_sub_type_1_code_text || ""
            item.request_item_sub_type_1_code_text = item.request_item_sub_type_1_code_text.replace(/\|/g, " ").trim().split(" ")
            //item.product_text = item.request_item_sub_type_1_code_text.split("|").map(function (item) {
            //  //  if (param["search_1_request_item_sub_type_1_code"]) {
            //  //    //  console.log(param["search_1_request_item_sub_type_1_code"])
            //  //    var code_list = param["search_1_request_item_sub_type_1_code"].split(" ")
            //  //    console.log(code_list)
            //  //    for (var i = 0; i < code_list.length; i++) {
            //  //      if (code_list[i] == item.request_item_sub_type_1_code) {
            //  //        return "<font color='red'>" + item.request_item_sub_type_1_code + "</font>"
            //  //      }
            //  //    }
            //  //    return item.request_item_sub_type_1_code
            //  //  }
            //  return item
            //}).join(", ")

            item.word_mark_text = item.word_mark_text || ""
            item.word_mark = item.word_mark_text.substr(1).split("|").map(function (item) {
              if (param["search_1_1_word_mark"]) {
                if (item.indexOf(param["search_1_1_word_mark"]) >= 0) {
                  return item.replace(param["search_1_1_word_mark"], "<font color='red'>" + param["search_1_1_word_mark"] + "</font>")
                }
              }
              return item
            }).join("<br>")

            item.name_text = item.name_text || ""
            item.people_text = item.name_text.substr(1).trim().split("|").join("<br>")

            item.document_classification_sound_name_text = item.document_classification_sound_name_text || ""
            item.soud_text = item.document_classification_sound_name_text.substr(1).trim().split("|").join("<br>")

            //item.soud_text = item.full_view.document_classification_sound_list.map(function (item) {
            //  if (param["search_1_document_classification_sound_code"]) {
            //    var search_1_document_classification_sound_code = "|" + param["search_1_document_classification_sound_code"] + "|"
            //    if (search_1_document_classification_sound_code.indexOf("|" + item.document_classification_sound_code + "|") >= 0) {
            //      return "<font color='red'>" + item.document_classification_sound_name + "</font>"
            //    }
            //  }
            //  return item.document_classification_sound_name
            //}).join("<br>")
          })
        }

        while (this.listTrademark.length > 0) {
          var trademark = this.listTrademark.splice(0, 5)
          while (5 > trademark.length) trademark.push({})
          this.list_listTrademark.push(trademark)
        }

        //this.CheckingProcessService.CheckingTagSimilarMethod(param).subscribe((data: any) => {
        //  if (data) {
        //    //console.log(data)
        //    // Manage structure
        //    this.checkingTagSimilarMethod = data
        //    //this.listTrademark.forEach((item: any) => {
        //    //  item.word_mark = item.full_view.document_classification_word_list.map(function (item) { return item.word_mark }).join(", ")
        //    //})
        //  }
        //  // Close loading
        pThis.global.setLoading(false)
        //  //this.automateTest.test(this)
        //})
      })
    })
  }




  onClickCheckingCheckingPrint(): void {
    //if(this.validateCheckingCheckingPrint()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingCheckingPrint(this.saveData())
    //}
  }
  validateCheckingCheckingPrint(): boolean {
    let result = validateService('validateCheckingCheckingPrint', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingCheckingPrint(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingCheckingPrint(params).subscribe((data: any) => {
      // if(isValidCheckingCheckingPrintResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingReportPrint(): void {
    //if(this.validateCheckingReportPrint()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingReportPrint(this.saveData())
    //}
  }
  validateCheckingReportPrint(): boolean {
    let result = validateService('validateCheckingReportPrint', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingReportPrint(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingReportPrint(params).subscribe((data: any) => {
      // if(isValidCheckingReportPrintResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingCancel(): void {
    //if(this.validateCheckingCancel()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingCancel(this.saveData())
    //}
  }
  validateCheckingCancel(): boolean {
    let result = validateService('validateCheckingCancel', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingCancel(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingCancel(params).subscribe((data: any) => {
      // if(isValidCheckingCancelResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingSave(): void {
    //if(this.validateCheckingSave()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingSave(this.saveData())
    //}
  }
  validateCheckingSave(): boolean {
    let result = validateService('validateCheckingSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSave(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingSave(params).subscribe((data: any) => {
      // if(isValidCheckingSaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingBack(): void {
    window.close();
    //var win = window.open("./checking-similar/edit/" + this.editID, "_self")
  }
  validateCheckingBack(): boolean {
    let result = validateService('validateCheckingBack', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingBack(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingBack(params).subscribe((data: any) => {
      // if(isValidCheckingBackResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingAdd(): void {
    this.listCheckingProcess.push({

    })
    this.changePaginateTotal(this.listCheckingProcess.length, 'paginateCheckingProcess')
  }
  onClickCheckingProcesssave010_image(id: any): void { }
  onClickCheckingProcesschecking_image_row_01(id: any): void { }
  onClickCheckingProcesschecking_image_row_02(id: any): void { }
  onClickCheckingProcesschecking_image_row_03(id: any): void { }
  onClickCheckingProcesschecking_image_row_04(id: any): void { }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location




  oneWayDataBindingSave(name: any, value: any, object: any): void {
    this.route.queryParams.subscribe(_params => {
      //let param = {}
      var params = [this.editID, object.id, value, Object.keys(_params)]

      this.CheckingProcessService.CheckingSimilarTagAdd(params).subscribe((data: any) => {
        //console.log(data)
        //this.automateTest.test(this, { TrademarkAdd: 1 })
      })
    })
  }



  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listTrademark = data.trademark_list || []
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')

  }

  listData(data: any): void {
    this.listCheckingProcess = data || []
    this.changePaginateTotal(this.listCheckingProcess.length, 'paginateCheckingProcess')

  }

  saveData(): any {
    let params = this.input

    params.checking_process_list = this.listCheckingProcess || []

    return params
  }

  dragElement(elmnt): void {
    console.log(elmnt)

    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    if (document.getElementById(elmnt.id + "Header")) {
      // if present, the header is where you move the DIV from:
      document.getElementById(elmnt.id + "Header").onmousedown = dragMouseDown;
    } else {
      // otherwise, move the DIV from anywhere inside the DIV:
      elmnt.onmousedown = dragMouseDown;
    }

    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      // set the element's new position:
      elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
      elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
      // stop moving when mouse button is released:
      document.onmouseup = null;
      document.onmousemove = null;
    }
  }

  onMouseDownImageShow(event: any): void {
    this.input.mouse_down = true
    //this.input.mouse_move = [event.layerX, event.layerY]
    ////document.getElementById("div_image_show").scroll(10, 10);
    ////document.getElementById("div_image_show").scrollTo(10, 10);
  }
  onMouseMoveImageShow(event: any): void {
    if (this.input.mouse_down) {
      document.getElementById("div_image_show_image").scroll(
        document.getElementById("div_image_show_image").scrollLeft - event.movementX,
        document.getElementById("div_image_show_image").scrollTop - event.movementY);
    }
  }
  onMouseUpImageShow(event: any): void {
    //document.getElementById("div_image_show").scroll(10, 10);
    //document.getElementById("div_image_show").scrollTo(10, 10);
    this.input.mouse_down = false
  }

  onClickImageShowZoomNext(): void {
    var index = this.image_show_zoom_list.indexOf(+this.popup.image_show_zoom)
    if (index < this.image_show_zoom_list.length - 1) {
      this.popup.image_show_zoom = +this.image_show_zoom_list[index + 1]
    }
  }
  onClickImageShowZoomPrevious(): void {
    var index = this.image_show_zoom_list.indexOf(+this.popup.image_show_zoom)
    if (index > 0) {
      this.popup.image_show_zoom = +this.image_show_zoom_list[index - 1]
    }
  }

  onClickCheckingSimilarWordCompare(row_item: any): void {
    var win = window.open("./checking-similar-compare/edit/" + this.editID + "?compare_id=" + row_item.id)
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

  togglePopup(name: string): void {
    if (!this.popup[name]) { // Is open
      this.popup[name] = true
    } else { // Is close
      this.popup[name] = false
      if (name === 'isPopupConfirmDeleteOpen') {
        this.popup.removeId = 0
      }
    }
  }

}
