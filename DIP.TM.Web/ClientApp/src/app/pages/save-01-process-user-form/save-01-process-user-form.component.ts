import { Component, OnInit } from '@angular/core'
import { GlobalService } from '../../global.service'
import { SaveProcessService } from '../../services/save-process.service'
import {
  CONSTANTS,
} from '../../helpers'

@Component({
  selector: "app-save-01-process-user-form",
  templateUrl: "./save-01-process-user-form.component.html",
  styleUrls: ["./save-01-process-user-form.component.scss"]
})
export class Save01ProcessUserFormComponent implements OnInit {
  //TODO >>> Declarations <<<
  public isSubmited: boolean
  public isValid: boolean

  ngOnInit() {
    this.isSubmited = false
    this.isValid = false
  }

  callInit(): void {

  }

  constructor(
    private global: GlobalService,
    private SaveProcessService: SaveProcessService
  ) { }
}
