import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { TestLoginService } from '../../services/test-login-buffer.service'
import { Router } from '@angular/router'
import {
    CONSTANTS,
    ROUTE_PATH,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-test-login",
    templateUrl: "./test-login.component.html",
    styleUrls: ["./test-login.component.scss"]
})
export class TestLoginComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    ngOnInit() {
        this.validate = {}

        this.input = {
            id: '',
        }

        this.master = {
        }

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initTestLogin().subscribe((data: any) => {
            if (data) {
                this.master = data

            }

            this.global.setLoading(false)
        })





        //Check Cookie/Storage -> No Login
        if (true) {
            //System redirect to login page at DIP and redirect back
            this.router.navigateByUrl("test-login?old_page=xxxx&auth_code=aaaaaaa")
        }

        this.route.queryParams.subscribe(paramsId => {
            let auth_code = paramsId.auth_code;
            if (auth_code) {
                //After redirect back -> Save token to database and Cookie/Storage
                this.TestLoginService.SSO_Token({
                    json: "{\"info\":{\"username\":\"testtmfl3\",\"authorities\":[\"TRADEMARK\",\"SSO\",\"PORTAL\"],\"name\":\"นาย ทีเอ็ม ทีเอ็ม\",\"pid\":\"7868546884901\",\"passport\":null,\"cid\":null,\"email\":\"test@test.com\",\"address\":{\"building\":null,\"floor\":null,\"village\":null,\"lane\":null,\"street\":null,\"postcode\":\"36190\",\"telephone\":null,\"mobile\":\"-\",\"fax\":null,\"house_number\":\"3213\",\"room_number\":null,\"village_number\":null,\"province_id\":25,\"province_name\":\"ชัยภูมิ\",\"district_id\":365,\"district_name\":\"บ้านแท่น\",\"sub_district_id\":3319,\"sub_district_name\":\"สามสวน\",\"telephone_to\":null},\"cert_sn\":\"SN=testtmfl3\",\"user_type\":\"OFFICER\",\"user_type_desc\":\"เจ้าหน้าที่\",\"user_id\":105273,\"title_name\":\"MR\",\"title_name_desc\":\"นาย\",\"first_name\":\"ทีเอ็ม\",\"middle_name\":null,\"last_name\":\"ทีเอ็ม\",\"passport_country_id\":null,\"passport_country_name\":null,\"tax_id_number\":null,\"card_issued_date\":\"2019-11-20T00:00:00\",\"card_expire_date\":null,\"nation_id\":307,\"nation_name\":\"ไทย\",\"birth_date\":\"2019-11-12T00:00:00\",\"contact_address\":{\"building\":null,\"floor\":null,\"village\":null,\"lane\":null,\"street\":null,\"postcode\":\"36190\",\"telephone\":null,\"mobile\":\"-\",\"fax\":null,\"house_number\":\"3213\",\"room_number\":null,\"village_number\":null,\"province_id\":25,\"province_name\":\"ชัยภูมิ\",\"district_id\":365,\"district_name\":\"บ้านแท่น\",\"sub_district_id\":3319,\"sub_district_name\":\"สามสวน\",\"telephone_to\":null}},\"access_token\":\"eyJhbGciOiJSUzI1NiJ9.eyJpZCI6IjJhYzYwZDYzOTkyMTRjNjg4MWM3ZDdiNGY1MTYyMWJhIiwiZXhwIjoxNTc2MTI0NTQ5LCJhaWQiOiIxNzAwMDAwMDE5IiwiaWF0IjoxNTc2MTIwOTQ5LCJzaWQiOiJhNTEyOGJmNTE4NmM0NmFlODQ4NDMwZjBmZDA5YzM3YiIsIl8iOjEwNTI3M30.mKG9n7raWhqwdYVZuEzFIO4vhAKizp_fUB1RVkzojruL6m4IPBXsAhrpbY1V6qn90ffjxojL-HFACFDIU3V1dRathZ9GLpd1IQgmqXJxaJIJ4NsYqDbIuFmQIvR94ieuRZYnYHNs8OdfVOlsJUM9ejvfqbp-U79ifT8jXw8ypO9VUEyreEttTEJtsE6b_ooZ7iuVOJu7BmZBbIelUrgXDnB9YslHYhMaOSYL0CLJxBq12IrLF0GztYgGTh2w8bDn8S0MQq0ZLHI2pV1EsN9jn_IG_iJZXTh2f5EApJ-0vF4OQmsawRLp2LrS43BAbt8UWFLymWoGkkeIOCl3NOJyEA\",\"refresh_token\":\"eyJhbGciOiJSUzM4NCJ9.eyJpZCI6IjJhYzYwZDYzOTkyMTRjNjg4MWM3ZDdiNGY1MTYyMWJhIiwiZXhwIjoxNTc2MTQ5NzQ5LCJhaWQiOiIxNzAwMDAwMDE5IiwiaWF0IjoxNTc2MTIwOTQ5LCJzaWQiOiJhNTEyOGJmNTE4NmM0NmFlODQ4NDMwZjBmZDA5YzM3YiIsIl8iOjEwNTI3M30.C431jBdltHRLRKnc5zlvFd1XPpQSHMrnwB6AA2cjmfYv7II4JECduJK2sgvbmsx-N4wvT_K39icGPGM-SdZGTi-a9YTs3MeYPHMkLpoD502EOvokCDLkx_STz4hxfeCHQfzVNdUVH_Y2dXtJUyM5lkmZ0oO6I7sH8N1Q53c94x-2tb72wFEAWmhRLiijT3rz6eIdMdf9iWfLPMEKtljelN0JiieyvqXzsX6IshoDRzty7sBIJZXJLFPNVkZwZCMCFwfsAwHJglH6hCVCtHHWoSj_SsnHSB115xJxKHSVMsXl97xYSYKgJDobmp9cD4Z0RJAncHrzW5kNY3SgxzFlqw\",\"expires_in\":3600}"
                }).subscribe((data: any) => {
                    let old_page = paramsId.old_page;
                    if (old_page) {
                        //Go to old page
                        this.router.navigate([ROUTE_PATH.DASHBOARD.LINK])
                    } else {
                        //Go to Dashboard
                        this.router.navigate([ROUTE_PATH.DASHBOARD.LINK])
                    }
                })
            }
        });





    }

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private TestLoginService: TestLoginService
    ) { }






    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any): void {
        this.input[name] = value
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this[name] = !this[name]
    }
}
