import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { GlobalService } from "../../global.service";
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from "../../services/fork-join-eform.service";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import {
  ROUTE_PATH,
  clone,
  validateService,
  CONSTANTS,
  displayAddress,
  clearIdAndSaveId,
  viewPDF,
  displayMoney,
  getItemCalculator,
  getParamsOverwrite,
} from "../../helpers";

@Component({
  selector: "app-eform-save-210-process",
  templateUrl: "./eform-save-210-process.component.html",
  styleUrls: ["./eform-save-210-process.component.scss"],
})
export class eFormSave210ProcessComponent
  implements OnInit, DeactivationGuarded {
  //TODO >>> Declarations <<<
  // Init
  public editID: number;
  public menuList: any[];
  public input: any;
  public validate: any;
  public master: any;
  public response: any;
  public modal: any;
  public popup: any;
  // Autocomplete
  public autocompleteList: any;
  public isShowAutocomplete: any;
  public autocompleteFocusIndex: any;
  // Paginate
  public paginateProduct: any;
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubID: number;
  public currentSubStep: number;
  public currentMiniID: number;
  public currentMiniStep: number;
  public progressPercent: number;
  // Other
  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eformSaveProcessService: eFormSaveProcessService
  ) {}

  ngOnInit() {
    // Init
    this.editID = +this.route.snapshot.paramMap.get("id");
    this.menuList = [
      {
        id: 1,
        number: 1,
        name: "บันทึกเพื่อรับไฟล์",
        isShow: true,
        canEdit: false,
      },
      {
        id: 2,
        number: 2,
        name:
          "บันทึกคำขอให้บันทึกการจดทะเบียนระหว่างประเทศแทนการจดทะเบียนในราชอาณาจักร",
        isShow: true,
        canEdit: false,
      },
      {
        id: 3,
        number: 3,
        name: "บันทึกเลขที่ทะเบียนระหว่างประเทศ",
        isShow: true,
        canEdit: false,
      },
      {
        id: 4,
        number: 4,
        name: "เจ้าของ/ตัวแทนเครื่องหมาย",
        isShow: true,
        canEdit: false,
      },
      {
        id: 5,
        number: 5,
        name: "สถานที่ติดต่อภายในประเทศไทย",
        isShow: true,
        canEdit: false,
      },
      {
        id: 6,
        number: 6,
        name: "จำพวกสินค้า/บริการ ",
        isShow: true,
        canEdit: false,
      },
      {
        id: 7,
        number: 7,
        name:
          "ข้อมูลอื่นๆ ที่เกี่ยวกับสิทธิที่ได้รับมาจากการจดทะเบียนในประเทศไทย",
        isShow: true,
        canEdit: false,
      },
      {
        id: 8,
        number: 8,
        name: "เอกสารหลักฐานประกอบคำขอจดทะเบียน",
        isShow: true,
        canEdit: false,
      },
      {
        id: 9,
        number: 9,
        name: "ค่าธรรมเนียม",
        isShow: true,
        canEdit: false,
      },
      {
        id: 10,
        number: 10,
        name: "เสร็จสิ้น",
        isShow: true,
        canEdit: false,
      },
    ];
    this.input = {
      indexEdit: undefined,
      point: "",
      is_check_all: false,
      isAllowEditInterRegistrationNumber: true,
      isAllowEditMultipleInterRegistrationNumber: true,
      search_type_code: "REQUEST_NUMBER",
      listMultipleSearch: [],
      listInterRegistration: [],
      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      save210_representative_condition_type_code: "AND_OR",
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      contact_type_index: 0,
      listAgentMarkRepresentative1: [],
      listAgentMarkRepresentative2: [],
      address: {
        receiver_type_code: "PEOPLE",
      },
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0,
      },
      listDocumentRequestChecked: [
        {
          number: 1,
          isShow: true,
          checked: false,
        },
        {
          number: 2,
          isShow: true,
          checked: false,
        },
        {
          number: 3,
          isShow: true,
          checked: false,
        },
      ],
      isCheckAllOwnerSignature: false,
      isCheckAllAgentSignature: false,
    };
    this.validate = {};
    this.master = {
      addressEformCardTypeCodeList: [],
      addressCountryCodeList: [],
      addressRepresentativeConditionTypeCodeList: [],
      addressTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
      addressCareerCodeList: [],
      addressNationalityCodeList: [],
      representativeTypeCodeList: [],
      searchTypeCodeList: [],
    };
    this.response = {
      load: {},
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
    };
    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
      isWarning: false,
      isSearch: false,
    };
    // Autocomplete
    this.autocompleteList = {
      description: [],
      address_sub_district_name: [],
    };
    this.isShowAutocomplete = {
      description: false,
      address_sub_district_name: false,
    };
    this.autocompleteFocusIndex = {
      description: 0,
    };
    // Paginate
    this.paginateProduct = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateProduct.id = "paginateProduct";
    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubID = 1;
    this.currentSubStep = 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
    this.progressPercent = 0;

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initEForm210Page().subscribe((data: any) => {
      if (data) {
        this.master = data;
        console.log("this.master", this.master);
      }
      if (this.editID) {
        this.eformSaveProcessService
          .eFormSave210Load(this.editID, {})
          .subscribe((data: any) => {
            if (data) {
              this.response.load = {
                eform_number: data.eform_number,
                id: data.id,
                wizard: data.wizard,
              };
              this.input.isAllowEditInterRegistrationNumber = data.inter_registration_number
                ? false
                : true;
              //TODO
              // this.input.isAllowEditMultipleInterRegistrationNumber = data.inter_registration_number
              //   ? false
              //   : true;
              // step 1
              this.input.email = data.email;
              this.input.telephone = data.telephone;
              // step 2
              this.input.search_type_code = data.save210_search_type_code;
              this.input.inter_registration_number =
                data.inter_registration_number;
              this.input.registration_number = data.registration_number;
              // step 3
              //  TODO
              // step 4
              this.input.listOwnerMark = data.people_list;
              this.input.listAgentMark = data.representative_list;
              this.input.save210_representative_condition_type_code = data.save210_representative_condition_type_code
                ? data.save210_representative_condition_type_code
                : "AND_OR";
              // step 5
              this.input.save210_contact_type_code =
                data.save210_contact_type_code;
              this.input.address =
                data.save210_contact_type_code === "OTHERS"
                  ? data.contact_address_list[0]
                  : this.input.address;
              // step 6
              this.input.listProduct = data.product_list;
              // step 7
              this.input.remark_7 = data.remark_7;
              // step 8
              this.input.listDocumentRequestChecked[0].checked = data.is_8_1;
              this.input.listDocumentRequestChecked[1].checked = data.is_8_2;
              this.input.listDocumentRequestChecked[2].checked = data.is_8_3;
              // step 9
              this.input.payer_name = data.payer_name;
              this.input.productSummary.total_price = data.total_price;
              // step 10
              this.input.isCheckAllOwnerSignature =
                data.sign_inform_person_list === "0" ? true : false;
              this.input.isCheckAllAgentSignature =
                data.sign_inform_representative_list === "0" ? true : false;
              this.manageWizard(data.wizard);
              this.updateSummaryProduct();
              this.setSignInform(data.sign_inform_representative_list);
              // Close loading
              this.global.setLoading(false);
            } else {
              // Close loading
              this.global.setLoading(false);
            }
          });
      } else {
        this.onClickAddInterRegistrationNumber();
        // Close loading
        this.global.setLoading(false);
      }
    });
  }

  //! <<< Call API >>>
  callSendEmail210(params: any): void {
    this.eformSaveProcessService
      .eFormSave210Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchRequestNumber(): void {
    this.eformSaveProcessService
      .eFormSave210SearchRequestNumber(this.input.inter_registration_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.registration_number = data.registration_load_number;
          this.input.listOwnerMark = clearIdAndSaveId(data.people_load_list);
          this.input.listAgentMark = clearIdAndSaveId(
            data.representative_load_list
          );
          this.input.save210_representative_condition_type_code =
            data.load_representative_condition_type_code;
          this.input.save210_contact_type_code = data.load_contact_type_code;
          this.input.address = data.contact_address_load_list[0]
            ? data.contact_address_load_list[0]
            : this.input.address;
          this.input.listProduct = clearIdAndSaveId(data.product_load_list);
          this.manageContactTypeIndex();
          this.updateSummaryProduct();
          this.changePaginateTotal(
            this.input.listProduct.length,
            "paginateProduct"
          );
        } else {
          console.warn(`inter_registration_number is invalid.`);
          this.validate.inter_registration_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchInterRegistrationNumber(): void {
    this.eformSaveProcessService
      .eFormSave210SearchInterRegistrationNumber(
        this.input.inter_registration_number,
        {}
      )
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          // TODO wait madrid
        } else {
          console.warn(`inter_registration_number is invalid.`);
          this.validate.inter_registration_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchMultipleInterRegistrationNumber(): void {
    // TODO Change api and logic
    this.eformSaveProcessService
      .eFormSave210MultipleSearch(this.input.listMultipleSearch.toString(), {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          // data.eform_210_date_list.forEach((item: any) => {
          //   this.input.listInterRegistration.push({
          //     request_number: item.request_number,
          //     case_28_date: item.case_28_date,
          //     request_date: item.request_date,
          //   });
          // });
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          // console.warn(`multiple_inter_registration_number is invalid.`);
          // this.validate.multiple_inter_registration_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callEFormSave210Save(params: any): void {
    this.eformSaveProcessService
      .eFormSave210Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.isDeactivation = true;
          // Open toast success
          let toast = CONSTANTS.TOAST.SUCCESS;
          toast.message = "บันทึกข้อมูลสำเร็จ";
          this.global.setToast(toast);
          // Navigate
          this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
        } else {
          // Close loading
          this.global.setLoading(false);
        }
      });
  }
  callSubDistrict(params: any, name: any): void {
    this.eformSaveProcessService
      .SearchLocation(params)
      .subscribe((data: any) => {
        if (data) {
          this.autocompleteList[name] = data;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }
  callProductCatagory01ItemListAutocomplete(params: any, name: any): void {
    this.eformSaveProcessService
      .ProductCatagory01ItemList(params)
      .subscribe((data: any) => {
        if (data) {
          let list = [];
          data.list.forEach((item: any) => {
            list.push({
              request_item_sub_type_1_code: item.code,
              description: item.name,
            });
          });
          this.autocompleteList[name] = list;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }

  //! <<< Prepare Call API >>>
  sendEmail(): void {
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      wizard: this.getWizard(),
      email: this.input.email,
      telephone: this.input.telephone,
    };
    // Call api
    this.callSendEmail210(params);
  }
  onSearch(): void {
    let result = validateService(
      "validateEFormSave210ProcessStep2",
      this.input
    );
    this.validate = result.validate;

    if (result.isValid) {
      // Open loading
      this.global.setLoading(true);
      // Call api
      if (this.input.search_type_code == "REQUEST_NUMBER") {
        this.callSearchRequestNumber();
      } else {
        this.callSearchInterRegistrationNumber();
      }
    } else {
      if (this.input.search_type_code == "REQUEST_NUMBER") {
        this.validate = { inter_registration_number: "กรุณากรอก คำขอเลขที่" };
      }
    }
  }
  onSearchMultiple(index: any): void {
    let result = validateService(
      "validateEFormSave210ProcessStep3",
      this.input.listInterRegistration[index]
    );
    this.validate = result.validate;

    if (result.isValid) {
      // Open loading
      this.global.setLoading(true);
      // Call api
      this.callSearchMultipleInterRegistrationNumber();
    } else {
      this.input.listInterRegistration[index].validate = {
        request_number: "กรุณากรอก เลขคำขอในประเทศ",
      };
    }

    // if (this.input.multiple_inter_registration_number) {
    //   // Add search data to list
    //   this.input.listMultipleSearch.push(
    //     this.input.multiple_inter_registration_number
    //   );
    //   this.input.multiple_inter_registration_number = "";
    // }

    // // Clear filter
    // this.input.listInterRegistration = [];
    // this.clearFliterInterRegistrationNumber();

    // if (this.input.listMultipleSearch.length > 0) {
    //   this.clearAllValidate();
    //   // Open loading
    //   this.global.setLoading(true);
    //   // Call api
    //   this.callSearchMultipleInterRegistrationNumber();
    // } else {
    //   console.warn(`multiple_inter_registration_number is invalid.`);
    //   this.validate.multiple_inter_registration_number =
    //     "กรุณากรอก เลขคำขอในประเทศ";
    // }
  }
  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }
  save(isOverwrite: boolean): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = this.getParamsSave();
    if (!isOverwrite) {
      params = getParamsOverwrite(params);
    }
    // Call api
    this.callEFormSave210Save(params);
  }
  getParamsSave(): any {
    return {
      id: this.response.load.id ? this.response.load.id : null,
      eform_number: this.response.load.eform_number
        ? this.response.load.eform_number
        : null,
      wizard: this.getWizard(),
      // step 1
      email: this.input.email,
      telephone: this.input.telephone,
      // step 2
      save210_search_type_code: this.input.search_type_code,
      inter_registration_number: this.input.inter_registration_number,
      registration_number: this.input.registration_number,
      // step 3
      //TODO
      // step 4
      people_list: this.input.listOwnerMark,
      representative_list: this.input.listAgentMark,
      save210_representative_condition_type_code: this.input
        .save210_representative_condition_type_code,
      // step 5
      save210_contact_type_code: this.input.save210_contact_type_code,
      contact_address_list:
        this.input.save210_contact_type_code === "OTHERS"
          ? [this.input.address]
          : [],
      // step 6
      product_list: this.input.listProduct,
      // step 7
      remark_7: this.input.remark_7 ? this.input.remark_7 : "",
      // step 8
      is_8_1: this.input.listDocumentRequestChecked[0].checked,
      is_8_2: this.input.listDocumentRequestChecked[1].checked,
      is_8_3: this.input.listDocumentRequestChecked[2].checked,
      // step 9
      payer_name: this.input.payer_name,
      total_price: this.input.productSummary.total_price,
      // step 10
      sign_inform_person_list: this.input.isCheckAllOwnerSignature ? "0" : "",
      sign_inform_representative_list:
        this.input.isCheckAllAgentSignature &&
        this.input.save210_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform(),
    };
  }
  onClickViewPdfA21(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM21", this.getParamsSave());
    }
  }
  onClickViewPdfKor18(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM18", this.getParamsSave());
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Event >>>
  getWizard(): String {
    if (this.editID) {
      let wizardList = this.response.load.wizard.split("|");
      let sum = 0;
      let sumNow =
        this.currentID +
        this.currentStep +
        this.currentSubID +
        this.currentSubStep;

      wizardList.forEach((item: any) => {
        sum = +sum + +item;
      });

      if (sumNow > sum) {
        return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
      } else {
        return this.response.load.wizard;
      }
    } else {
      return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
    }
  }
  getSignInform(): String {
    let result = "";
    this.input.listAgentMark.forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input.listAgentMark.forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save010_representative_condition_type_code != "AND") {
        this.input.listAgentMark.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }
  manageContactTypeIndex(): void {
    if (this.input.save210_contact_type_code !== "OTHERS") {
      if (this.input.save210_contact_type_code === "OWNER") {
        this.input.listOwnerMark.forEach((item: any, index: any) => {
          if (item.is_contact_person) {
            this.input.contact_type_index = index;
          }
        });
      } else {
        this.input.listAgentMark.forEach((item: any, index: any) => {
          if (item.is_contact_person) {
            this.input.contact_type_index = index;
          }
        });
      }
    }
  }

  //! <<< Multiple Search (Step 3) >>>
  onClickAddInterRegistrationNumber(): void {
    this.input.listInterRegistration.push({
      request_number: "",
      case_28_date: "",
      request_date: "",
      validate: {},
    });
  }
  onChangeFocusInterRegistrationNumber(): void {
    this.input.listInterRegistration.forEach((item: any) => {
      if (item.request_number == this.input.focusInterRegistrationNumber) {
        this.input.case_28_date = item.case_28_date;
        this.input.request_date = item.request_date;
      }
    });
  }
  onClickRemoveInterRegistrationNumber(
    obj: string,
    name: any,
    index: number
  ): void {
    if (this.input.listInterRegistration.length > 0) {
      this.input.listInterRegistration = this.input.listInterRegistration.filter(
        (item: any) => {
          return item.request_number != this.input.listMultipleSearch[index];
        }
      );
      if (
        this.input.focusInterRegistrationNumber ==
        this.input.listMultipleSearch[index]
      ) {
        this.clearFliterInterRegistrationNumber();
      }
    }
    this[obj][name].splice(index, 1);
  }
  clearFliterInterRegistrationNumber(): void {
    this.input.focusInterRegistrationNumber = undefined;
    this.input.case_28_date = "";
    this.input.request_date = "";
  }

  //! <<< Table (Product) >>>
  onClickAddProduct(): void {
    this.input.listProduct.push({
      isEditType: true,
      request_item_sub_type_1_code: "",
      description: "",
      amount_product: 1,
      total_price: getItemCalculator(1, this.master.priceMasterList),
    });
    this.updateSummaryProduct();
    this.changePaginateTotal(this.input.listProduct.length, "paginateProduct");
  }
  onChangeAmountProduct(item: any): void {
    if (+item.amount_product && +item.amount_product >= 1) {
      item.total_price = getItemCalculator(
        item.amount_product,
        this.master.priceMasterList
      );
      this.updateSummaryProduct();
    } else {
      item.amount_product = 0;
      item.total_price = 0;
      this.updateSummaryProduct();
    }
  }
  onClickRemoveProduct(index: number): void {
    this.input.listProduct.splice(index, 1);
    this.updateSummaryProduct();
    this.changePaginateTotal(this.input.listProduct.length, "paginateProduct");
  }
  updateSummaryProduct(): void {
    let amount_type = this.input.listProduct.length;
    let requestItemList = [];
    let allRequestItemList = [];
    let count = {};
    let amount_product = 0;
    let total_price = 0;

    // Sum amount_product
    this.input.listProduct.forEach((item: any) => {
      allRequestItemList.push(item.request_item_sub_type_1_code);
      if (requestItemList.indexOf(item.request_item_sub_type_1_code) === -1) {
        requestItemList.push(item.request_item_sub_type_1_code);
        amount_product = amount_product + 1;
      }
    });

    // Count sum per request item
    allRequestItemList.forEach(function (i) {
      count[i] = (count[i] || 0) + 1;
    });

    // Sum total_price
    for (const [key, value] of Object.entries(count)) {
      total_price =
        total_price + getItemCalculator(value, this.master.priceMasterList);
    }

    this.input.productSummary = {
      amount_type: amount_type,
      amount_product: amount_product,
      total_price: total_price,
    };
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Table >>>
  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }
  toggleBooleanInTable(item: any, name: any, condition?: any): void {
    item[name] = !item[name];
  }
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all;
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all;
      });
    } else {
      item.is_check = !item.is_check;
    }
  }
  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    if (this.validateSaveItemModalInTable(nameItem)) {
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        this.toggleModal(nameModal);
        this.updatePaginateForTable(nameList);
      }
    }
  }
  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
    this.updatePaginateForTable(name);
  }
  updatePaginateForTable(name: any): void {}
  validateSaveItemModalInTable(nameItem: any): boolean {
    return true;
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    // if (name === "paginateDocument") {
    //   this.onClickDocumentItemList();
    //   this.input.is_check_all = false;
    // }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      // this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    // this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
    }
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
      this.updatePaginateForTable(nameList);
    }

    if (nameList === "listOwnerMark" || nameList === "listAgentMark") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Wizard >>>
  onClickMenu(menu: any): void {
    if (menu.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentSubStep = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickSubMenu(menu: any, sub: any): void {
    if (sub.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = sub.id;
        this.currentSubStep = sub.number;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickNext(): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      if (this.menuList[this.currentID - 1].hasSub) {
        let sizeSubMenu = this.menuList[this.currentID - 1].sizeSubList;

        if (this.currentSubStep < sizeSubMenu) {
          this.nextSubMenu();
        } else {
          if (this.currentSubID > 0) {
            this.menuList[this.currentID - 1].subList[
              this.currentSubID - 1
            ].canEdit = true;
          }
          this.nextMenu();
        }
      } else {
        this.nextMenu();
      }
    }
  }
  nextMenu(): void {
    let indexCurrentMenu = 0;
    let indexNextMenu = 0;

    // Find index current
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Cal Progress Percent
    if (!this.menuList[indexCurrentMenu].canEdit) {
      this.calcProgressPercent(this.currentStep);
    }
    // Set can edit
    this.menuList[indexCurrentMenu].canEdit = true;
    // Next wizard
    this.currentStep++;

    // Find index next
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexNextMenu = i;
      }
    });

    // Update wizard point
    this.currentID = this.menuList[indexNextMenu].id;
    this.currentSubID = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentSubStep = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  nextSubMenu(): void {
    let indexCurrentMenu = 0;
    let indexCurrentSub = 0;
    let indexNextSub = 0;

    // Find index current menu
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Find index current sub menu
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexCurrentSub = i;
      }
    });

    // Set can edit
    if (this.currentSubID > 0) {
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].canEdit = true;
    } else {
      if (this.menuList[indexCurrentMenu].isUseMenuBeforeSubList) {
        this.menuList[indexCurrentMenu].canEdit = true;
      }
    }
    // Next wizard sub
    this.currentSubStep++;

    // Find index sub next
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexNextSub = i;
      }
    });

    // Update wizard sub point
    this.currentSubID = this.menuList[indexCurrentMenu].subList[
      indexNextSub
    ].id;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  changeMiniStep(action: any): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let indexCurrentMini = 0;
      let indexNextMini = 0;
      let sizeMiniList = 0;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
        }
      });

      // Find index current sub menu
      this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
        if (item.number === this.currentSubStep && item.isShow) {
          indexCurrentSub = i;
        }
      });

      // Find index current mini menu
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList.forEach(
        (item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexCurrentMini = i;
          }
        }
      );

      // Get size mini list
      sizeMiniList = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
        .sizeMiniList;

      // Condition
      if (sizeMiniList === this.currentMiniStep && action === "next") {
        // Set can edit
        this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
          indexCurrentMini
        ].canEdit = true;
        // Next menu
        this.onClickNext();
      } else {
        if (action === "next") {
          // Set can edit
          this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
            indexCurrentMini
          ].canEdit = true;
          // Next wizard mini
          this.currentMiniStep++;
        } else {
          // Back wizard mini
          this.currentMiniStep = this.currentMiniStep - 1;
        }

        // Find index next mini menu
        this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList.forEach((item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexNextMini = i;
          }
        });

        // Update wizard mini point
        this.currentMiniID = this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList[indexNextMini].id;
      }
    }
  }
  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    } else if (this.currentID === 2) {
      let result = validateService(
        "validateEFormSave210ProcessStep2",
        this.input
      );
      this.validate = result.validate;

      if (!this.input.listOwnerMark || this.input.listOwnerMark.length === 0) {
        result.isValid = false;
        console.warn(`inter_registration_number is invalid.`);
        this.validate.inter_registration_number = "กรุณาคลิก ค้นหา";
      }

      return result.isValid;
    } else if (this.currentID === 3) {
      let isValid = true;

      this.input.listInterRegistration.forEach((item: any) => {
        let result = validateService(
          "validateEFormSave210ProcessStep3",
          item
        );
        item.validate = result.validate;
        if (!result.isValid) {
          isValid = false;
        }
      });

      return isValid;

      // let isValid = true;
      // if (
      //   !this.input.listInterRegistration ||
      //   this.input.listInterRegistration.length === 0
      // ) {
      //   isValid = false;
      //   console.warn(`multiple_inter_registration_number is invalid.`);
      //   this.validate.multiple_inter_registration_number = "กรุณาคลิก ค้นหา";
      // }
      // return isValid;
    } else if (this.currentID === 4) {
      if (this.input.listOwnerMark.length < 1) {
        console.warn(`step4 is invalid.`);
        this.validate.step4 = "กรุณาเพิ่มเจ้าของเครื่องหมาย";
        return false;
      } else {
        // Condition
        this.conditionWizard("clone_mark_list");
        return true;
      }
    } else if (this.currentID === 5) {
      let result = validateService(
        "validateEFormSave210ProcessStep5",
        this.input
      );
      this.validate = result.validate;

      // Condition
      if (result.isValid) {
        if (this.input.save210_contact_type_code === "OTHERS") {
          let result = validateService(
            "validateEFormSave01ProcessStep4Other",
            this.input.address
          );
          this.validate = result.validate;
          if (
            result.validate.address_district_name ||
            result.validate.address_province_name ||
            result.validate.postal_code
          ) {
            this.validate.address_sub_district_name = "กรุณากรอกและเลือก ตำบล";
          }
          return result.isValid;
        } else {
          // Set is_contact_person
          if (this.input.save210_contact_type_code === "OWNER") {
            this.input.listOwnerMark.forEach((item: any, index: any) => {
              if (+this.input.contact_type_index === index) {
                item.is_contact_person = true;
              } else {
                item.is_contact_person = false;
              }
            });
          } else {
            this.input.listAgentMark.forEach((item: any, index: any) => {
              if (this.input.save210_contact_type_code === "REPRESENTATIVE") {
                // ตัวแทน
                if (
                  this.input.listAgentMarkRepresentative1[
                    +this.input.contact_type_index
                  ] === index
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              } else if (
                this.input.save210_contact_type_code === "REPRESENTATIVE_PERIOD"
              ) {
                // ตัวแทนช่วง
                if (
                  this.input.listAgentMarkRepresentative2[
                    +this.input.contact_type_index
                  ] === index
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              }
            });
          }
        }
      }

      return result.isValid;
    } else if (this.currentID === 9) {
      let result = validateService(
        "validateEFormSave01ProcessStep15",
        this.input
      );
      this.validate = result.validate;
      return result.isValid;
    } else if (this.currentID === 10) {
      let isValid = false;

      if (
        this.input.isCheckAllOwnerSignature ||
        this.input.isCheckAllAgentSignature
      ) {
        isValid = true;
      }

      if (this.input.listAgentMark.length > 0) {
        this.input.listAgentMark.forEach((item: any) => {
          if (item.isCheck) {
            isValid = true;
          }
        });
      }

      if (!isValid) {
        console.warn(`step10 is invalid.`);
        this.validate.step10 = "กรุณาเลือกลงลายมือชื่อ";
      } else {
        // Set can edit
        this.menuList[9].canEdit = true;
      }

      return isValid;
    } else {
      return true;
    }
  }
  conditionWizard(condition: any): void {
    if (condition === "clone_mark_list") {
      let listAgentMarkRepresentative1 = [];
      let listAgentMarkRepresentative2 = [];
      this.input.listAgentMark.forEach((item: any, index: any) => {
        if (item.representative_type_code == "REPRESENTATIVE") {
          listAgentMarkRepresentative1.push(index);
        } else {
          listAgentMarkRepresentative2.push(index);
        }
      });
      this.input.listAgentMarkRepresentative1 = clone(
        listAgentMarkRepresentative1
      );
      this.input.listAgentMarkRepresentative2 = clone(
        listAgentMarkRepresentative2
      );

      if (!this.input.payer_name) {
        this.input.payer_name = this.input.listOwnerMark[0] ? this.input.listOwnerMark[0].name : "";
      }
    }
  }
  manageWizard(wizard: any): void {
    let wizardList = wizard ? wizard.split("|") : [1, 1, 1, 1];
    this.currentID = +wizardList[0];
    this.currentStep = +wizardList[1];
    this.currentSubID = +wizardList[2];
    this.currentSubStep = +wizardList[3];
    this.calcProgressPercent(this.currentStep);

    if (this.currentID >= 4) {
      this.conditionWizard("clone_mark_list");
    }
    if (this.currentID >= 5) {
      if (this.input.save210_contact_type_code !== "OTHERS") {
        if (this.input.save210_contact_type_code === "OWNER") {
          this.input.listOwnerMark.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.contact_type_index = index;
            }
          });
        } else {
          this.input.listAgentMark.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.contact_type_index = index;
            }
          });
        }
      }
    }

    this.menuList.forEach((item: any) => {
      if (this.currentID > item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            sub.canEdit = true;
          });
        }
      }
      if (this.currentID === item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (this.currentSubID >= sub.id) {
              sub.canEdit = true;
            }
          });
        }
      }
    });
  }
  reRunMenuNumber(): void {
    let number = 1;
    let numberSub = 1;
    let numberMini = 1;
    this.menuList.forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
        numberSub = 1;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (sub.isShow) {
              sub.number = numberSub;
              numberSub++;
              numberMini = 1;
              if (sub.miniList) {
                sub.miniList.forEach((mini: any) => {
                  if (mini.isShow) {
                    mini.number = numberMini;
                    numberMini++;
                  }
                });
              }
            }
          });
        }
      }
    });
  }
  calcProgressPercent(currentStep: number): void {
    let lastItem = this.menuList[this.menuList.length - 1];
    let progressPercent = Math.round(
      ((currentStep + 1) / lastItem.number) * 100
    );
    this.progressPercent = progressPercent > 100 ? 100 : progressPercent;
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(name: any, value: any, item: any, index: any): void {
    clearTimeout(this.timeout);
    if (name === "description" && item) {
      this.autocompleteFocusIndex.description = index;
      item.isEditType = true;
      this.clearValidate("description", item);
    }
    if (name === "address_sub_district_name") {
      this.validate[name] = null;
      this.clearValue(name);
    }

    this.timeout = setTimeout(() => {
      if (value) {
        if (name === "description") {
          this.callProductCatagory01ItemListAutocomplete(
            {
              page_index: 1,
              item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page,
              order_by: "created_date",
              is_order_reverse: false,
              filter_queries: [
                `code.Contains("${value}") || name.Contains("${value}")`,
              ],
            },
            name
          );
        }
        if (name === "address_sub_district_name") {
          this.callSubDistrict(
            {
              filter: { name: value },
              paging: {
                item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page,
              },
            },
            name
          );
        }
      } else {
        this.onClickOutsideAutocomplete(name);
      }
    }, CONSTANTS.DELAY_CALL_API);
  }
  onSelectAutocomplete(name: any, item: any, obj: any): void {
    if (name === "description") {
      obj.isEditType = false;
      obj.request_item_sub_type_1_code = item.request_item_sub_type_1_code;
      obj.description = item.description;
      this.clearValidate("request_item_sub_type_1_code", obj);
      this.updateSummaryProduct();
    }
    if (name === "address_sub_district_name") {
      // ตำบล
      this.input.address.address_sub_district_code = item.code;
      this.input.address.address_sub_district_name = item.name;
      // อำเภอ
      this.input.address.address_district_code = item.district_code;
      this.input.address.address_district_name = item.district_name;
      // จังหวัด
      this.input.address.address_province_code = item.province_code;
      this.input.address.address_province_name = item.province_name;
      // รหัสไปรษณีย์
      this.input.address.postal_code = item.postal_code;
    }
    this.onClickOutsideAutocomplete(name);
  }
  onClickOutsideAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = false;
    this.autocompleteList[name] = [];
  }
  clearValue(name: any): void {
    if (name === "address_sub_district_name") {
      // อำเภอ
      this.input.address.address_district_code = "";
      this.input.address.address_district_name = "";
      // จังหวัด
      this.input.address.address_province_code = "";
      this.input.address.address_province_name = "";
      // รหัสไปรษณีย์
      this.input.address.postal_code = "";
    }
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }

  //! <<< Other >>>
  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  displayAddress(value: any): any {
    return displayAddress(value);
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }
  onKey(e: any, name: any, index?: any): void {
    if (e.keyCode === 13) {
      if (name === "inter_registration_number") {
        this.onSearch();
      }
      if (name === "request_number") {
        this.onSearchMultiple(index);
        // this.input.listMultipleSearch.push(
        //   this.input.multiple_inter_registration_number
        // );
        // this.input.multiple_inter_registration_number = "";
      }
    } else {
      if (name === "inter_registration_number") {
        this.input.registration_number = "";
        this.input.listOwnerMark = [];
        this.input.listAgentMark = [];
        this.input.save210_representative_condition_type_code = "AND_OR"
        this.input.save210_contact_type_code = ""
        this.input.address = {}
        this.input.listProduct = []
      }
      if (name === "request_number") {
        this.input.listInterRegistration[index].case_28_date = ""
        this.input.listInterRegistration[index].request_date = ""
      }
    }
  }
}
