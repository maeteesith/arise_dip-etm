/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MadridRole06Ot04ListComponent } from './madrid-role06-ot04-list.component';

describe('MadridRole06Ot04ListComponent', () => {
  let component: MadridRole06Ot04ListComponent;
  let fixture: ComponentFixture<MadridRole06Ot04ListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MadridRole06Ot04ListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MadridRole06Ot04ListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
