/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MadridRole07SearchRecieveIrnComponent } from './madrid-role07-search-recieve-irn.component';

describe('MadridRole07SearchRecieveIrnComponent', () => {
  let component: MadridRole07SearchRecieveIrnComponent;
  let fixture: ComponentFixture<MadridRole07SearchRecieveIrnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MadridRole07SearchRecieveIrnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MadridRole07SearchRecieveIrnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
