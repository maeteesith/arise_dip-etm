import { Component, OnInit } from '@angular/core'
import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser'
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { ReceiptProcessService } from '../../services/receipt-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { EdcService } from '../../services/edc.service'
import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-receipt-daily",
    templateUrl: "./receipt-daily.component.html",
    styleUrls: ["./receipt-daily.component.scss"]
})
export class ReceiptDailyComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any
    public modal: any
    public url: any
    public book_index: any
    public page_index: any

    // // Input Paid
    // public inputPaid: any
    // List Paid
    public listPaid: any[]
    public paginatePaid: any
    public perPagePaid: number[]
    // Response Paid
    public responsePaid: any

    // // Input Item
    // public inputItem: any
    // List Item
    public listItem: any[]
    public paginateItem: any
    public perPageItem: number[]
    // Response Item
    public responseItem: any

    public edcRowItemEdit: any




    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}

        this.input = {
            id: null,
            receiver_name: this.auth.getAuth().name,
            reference_number: '',
            created_name: '',
            receipt_date: getMoment(),
            receive_date: getMoment(),
            payer_name: '',
            book_index: '',
            request_date_text: '',
            page_index: '',
            pay_price: '',
        }
        this.listPaid = []
        this.paginatePaid = CONSTANTS.PAGINATION.INIT
        this.perPagePaid = CONSTANTS.PAGINATION.PER_PAGE

        this.listItem = []
        this.paginateItem = CONSTANTS.PAGINATION.INIT
        this.perPageItem = CONSTANTS.PAGINATION.PER_PAGE

        this.modal = {
            isModalPreviewPdfOpen: false,
        }

        this.master = {
        }


        this.callInit()
    }

    loadData(data: any): void {
        // Set value
        this.input = data

        this.book_index = data.book_index || this.book_index
        this.page_index = data.page_index || this.page_index

        this.input.receiver_name = this.input.receiver_name || this.auth.getAuth().name

        this.response = data
        this.listPaid = data.paid_list || []
        this.listItem = data.item_list || []
        this.changePaginateTotal((this.listPaid || []).length, 'paginatePaid')
        this.changePaginateTotal((this.listItem || []).length, 'paginateItem')

        this.listPaid = data.paid_list || []
        this.listItem = data.item_list || []
        data.paid_list.map((item: any) => { item.is_check = false; return item })
        data.item_list.map((item: any) => { item.is_check = false; return item })
        this.changePaginateTotal((this.listPaid || []).length, 'paginatePaid')
        this.changePaginateTotal((this.listItem || []).length, 'paginateItem')


        if (this.page_index) this.page_index = this.page_index.toString().padStart(3, '0')

        this.listItem.forEach((item: any) => {
            if (item.page_index) item.page_index = item.page_index.toString().padStart(3, '0')
        })
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initReceiptDaily().subscribe((data: any) => {
            if (data) {
                this.master = data
            }
            if (this.editID) {
                this.ReceiptProcessService.LoadUnpaid(this.editID).subscribe((data: any) => {
                    if (data) {
                        this.loadData(data)
                        // Set value
                        // this.requestList = data.item_list
                        // this.response.request01Load = data
                        // this.isHasRequestList = true
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            }

            this.global.setLoading(false)
            this.automateTest.test(this)
        })
    }

    constructor(
        private automateTest: AutomateTest,
        private auth: Auth,
        private route: ActivatedRoute,
        private location: Location,
        public sanitizer: DomSanitizer,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private ReceiptProcessService: ReceiptProcessService,
        private edcService: EdcService,
    ) { }


    onClickLoadUnpaid(): void {
        if (this.validateLoadUnpaid()) {
            // Open loading
            this.global.setLoading(true)
            // Call api
            this.callLoadUnpaid(this.input.reference_number)
        }
    }
    validateLoadUnpaid(): boolean {
        let result = validateService('validateLoadUnpaid', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callLoadUnpaid(param: any): void {
        this.ReceiptProcessService.LoadUnpaid(param).subscribe((data: any) => {
            // if(isValidLoadUnpaidResponse(res)) {
            if (data) {
                this.loadData(data)

                this.automateTest.test(this, { reference_number: data.reference_number })
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickSave(): void {
        //if(this.validateSave()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        //const params = {
        //  id: this.input.id,
        //  reference_number: this.input.reference_number,
        //  created_name: this.input.created_name,
        //  receipt_date: this.input.receipt_date,
        //  receive_date: this.input.receive_date,
        //  payer_name: this.input.payer_name,
        //  book_index: this.input.book_index,
        //  request_date_text: this.input.request_date_text,
        //  page_index: this.input.page_index,
        //  paid_list: this.listPaid || [],
        //  item_list: this.listItem || [],

        //}
        // Call api

        this.input.book_index = this.book_index
        this.input.page_index = this.page_index

        this.callSave(this.input)
        //}
    }

    //! <<< IdCard >>>
    //Timeout in 120 sec
    timeLeft: number = 120;
    timeMsg: string = "";
    interval;
    //This function used to pull data from EDC
    startGetIdCardTimer() {
        this.interval = setInterval(() => {
            //Each 5 second call to check the result
            this.timeLeft -= 5;
            //this.timeMsg = "กรุณาเสียบบัตรประชาชนของพนักงานที่เครื่องอ่านบัตรก่อนทำรายการ [เหลือเวลาอีก " + this.timeLeft + " วินาที]";

            if (this.timeLeft > 0) {
                this.edcService.checkPaidResult("").subscribe((data: any) => {
                    if (data.statusText == "COMPLETED") {
                        if (data.responseMessage.trim() == "APPROVED") {
                            // If recieve result the display on the screen
                            this.edcRowItemEdit.paid_channel_type_code = (data.cardType.trim() == "QR" ? "QRCODE" : "CREDIT")
                            this.edcRowItemEdit.paid_reference_number_2 = (data.ref1 ? data.ref1.trim() : (data.referenceNo ? data.referenceNo.trim() : ""))
                            //this.listPaid.push({
                            //  paid_channel_type_code: (data.cardType.trim() == "QR" ? "QRCODE" : "CREDIT"),
                            //  paid_reference_number_1: (data.ref1 ? data.ref1.trim() : (data.referenceNo ? data.referenceNo.trim() : "")),
                            //  total_price: data.totalPrice,

                            //})
                            console.log(data.approvalCode);
                        } else if (data.responseMessage.trim() == "TXN CANCEL") {
                            alert(data.responseMessage)
                        }
                        this.pauseTimer();
                        this.global.setLoading(false)
                    }
                    else {
                        console.log("Error " + JSON.stringify(data));
                    }

                })
            } else {
                //this.timeMsg = "กรุณาเสียบบัตรประชาชนของพนักงานและกดค้นหาใหม่";
                this.pauseTimer();
            }
        }, 5000)
    }
    pauseTimer() {
        this.timeLeft = 120;
        clearInterval(this.interval);
    }

    //This function used to start interface to EDC server in client PC
    onClickPay(row_item: any): void {
        this.edcRowItemEdit = row_item

        if (row_item.total_price && row_item.total_price > 0) {
            this.global.setLoading(true)
            //alert(this.input.pay_price)
            this.edcService.payAndWait(row_item.total_price, row_item.paid_channel_type_code).subscribe((data: any) => {
                if (data.statusText == "OK") {
                    // If start pay success then tricker timmer to wait the result
                    this.startGetIdCardTimer();
                }
                else {
                    alert("Error " + data.responseMessage);
                    this.global.setLoading(false)
                }

            })
        }
    }
    validateSave(): boolean {
        let result = validateService('validateSave', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callSave(params: any): void {
        this.ReceiptProcessService.Save(params).subscribe((data: any) => {
            // if(isValidSaveResponse(res)) {
            if (data) {
                // Set value
                this.loadData(data)

                // Open popup save success
                let reference_number = this.input.reference_number;
                this.url = '/pdf/Receipt/' + reference_number
                console.log(this.url)
                this.url = this.sanitizer.bypassSecurityTrustResourceUrl(this.url)
                console.log(this.url)
                this.toggleModal('isModalPreviewPdfOpen')

                //this.onClickReset()

                this.automateTest.test(this, { request_id: data.item_list[0].request_id })
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }
    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }



    onClickPaidAdd(): void {
        this.listPaid.push({
            paid_channel_type_code: this.master.paidChannelTypeCodeList[0].code,
            paid_reference_number_1: null,
            total_price: this.listItem.reduce((pre, value) => pre + value.total_price, 0) -
                this.listPaid.filter(r => !r.is_deleted).reduce((pre, value) => pre + value.total_price, 0),

        })
        this.changePaginateTotal(this.listPaid.length, 'paginatePaid')

        this.updateTotalPrice()
    }

    updateTotalPrice(): void {
        this.input.total_price = 0
        this.input.total_change = 0
        this.listPaid.forEach((item: any) => {
            if (item.paid_channel_type_code == "CHANGE") {
                this.input.total_change += parseFloat(item.total_price)
            } else {
                this.input.total_price += parseFloat(item.total_price)
            }
        })
    }

    onClickPaidDelete(item: any): void {
        // if(this.validatePaidDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listPaid.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listPaid.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_Save0item_count = 0
            if (true) {
                delete_Save0item_count = this.listPaid.filter(r => r.is_check && r.true).length
            }

            var rs = delete_Save0item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {

                let ids = []

                for (let i = 0; i < this.listPaid.length; i++) {
                    if (this.listPaid[i].is_check) {
                        //this.listPaid[i].cancel_reason = rs
                        this.listPaid[i].is_deleted = true
                        this.listPaid[i].status_code = "DELETE"

                        if (true && this.listPaid[i].id) ids.push(this.listPaid[i].id)
                        else this.listPaid.splice(i--, 1);
                    }
                }

                if (false) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            //cancel_reason: rs
                        }
                        // Call api
                        this.callPaidDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listPaid.length; i++) {
                    this.listPaid[i].index = i + 1
                }
            }
        }

        this.updateTotalPrice()

        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callPaidDelete(params: any, ids: any[]): void {
        this.ReceiptProcessService.PaidDelete(params).subscribe((data: any) => {
            // if(isValidPaidDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listPaid.length; i++) {
                    if (this.listPaid[i].id == id) {
                        this.listPaid.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listPaid.length; i++) {
                this.listPaid[i] = i + 1
            }


            // Close loading
            this.global.setLoading(false)
        })
    }
    onClickCheckBoxTable3(name: string, index: number, isAll: boolean = false): void {
        if (isAll) {
            this.input.is_table_3_check_all = !this.input.is_table_3_check_all
            this[name].forEach((item: any) => { item.is_check = this.input.is_table_3_check_all })
        } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
    }
    onChangeCheckboxPaid(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
    onChangeSelectPaid(object: any, object_name: string, value: any): void {
        if (object_name.indexOf("price") >= 0) {
            object[object_name] = parseFloat(value)
        } else {
            object[object_name] = value
        }

        this.updateTotalPrice()
    }

    onClickItemAdd(): void {
        this.listItem.push({
            request_number: null,
            requester_name: null,
            request_type_code: null,
            request_type_name: null,
            total_price: null,
            book_index: null,
            page_index: null,

        })
        this.changePaginateTotal(this.listItem.length, 'paginateItem')
    }

    onClickItemDelete(item: any): void {
        // if(this.validateItemDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listItem.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listItem.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_Save0item_count = 0
            if (true) {
                delete_Save0item_count = this.listItem.filter(r => r.is_check && r.true).length
            }

            var rs = delete_Save0item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {

                let ids = []

                for (let i = 0; i < this.listItem.length; i++) {
                    if (this.listItem[i].is_check) {
                        //this.listItem[i].cancel_reason = rs
                        this.listItem[i].is_deleted = true
                        this.listItem[i].status_code = "DELETE"

                        if (true && this.listItem[i].id) ids.push(this.listItem[i].id)
                        else this.listItem.splice(i--, 1);
                    }
                }

                if (false) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callItemDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listItem.length; i++) {
                    this.listItem[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callItemDelete(params: any, ids: any[]): void {
        this.ReceiptProcessService.ItemDelete(params).subscribe((data: any) => {
            // if(isValidItemDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listItem.length; i++) {
                    if (this.listItem[i].id == id) {
                        this.listItem.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listItem.length; i++) {
                this.listItem[i] = i + 1
            }


            // Close loading
            this.global.setLoading(false)
        })
    }
    onClickCheckBoxTable4(name: string, index: number, isAll: boolean = false): void {
        if (isAll) {
            this.input.is_table_4_check_all = !this.input.is_table_4_check_all
            this[name].forEach((item: any) => { item.is_check = this.input.is_table_4_check_all })
        } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
    }
    onChangeCheckboxItem(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
    onChangeSelectItem(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }

    onKeyPressReferenceNumber(event: any): void {
        if (event.key == "Enter") {
            this.onClickLoadUnpaid()
        }
    }


    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }
    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any): void {
        this.input[name] = value
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]

        if (!this.modal[name])
            this.onClickReset()
    }

    NaN(value: any): boolean {
        return isNaN(value)
    }
}
