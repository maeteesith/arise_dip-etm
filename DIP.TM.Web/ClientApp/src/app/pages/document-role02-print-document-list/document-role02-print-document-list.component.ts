import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  clone,
  displayDateServer
} from '../../helpers'

@Component({
  selector: "app-document-role02-print-document-list",
  templateUrl: "./document-role02-print-document-list.component.html",
  styleUrls: ["./document-role02-print-document-list.component.scss"]
})
export class DocumentRole02PrintDocumentListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List DocumentRole02PrintDocument
  public listDocumentRole02PrintDocument: any[]
  public paginateDocumentRole02PrintDocument: any
  public perPageDocumentRole02PrintDocument: number[]
  // Response DocumentRole02PrintDocument
  public responseDocumentRole02PrintDocument: any
  public listDocumentRole02PrintDocumentEdit: any

  public itemEdit: any

  public player: any
  public player_action: any

  // List DocumentProcess
  public listDocumentProcess: any[]
  public paginateDocumentProcess: any
  public perPageDocumentProcess: number[]
  // Response DocumentProcess
  public responseDocumentProcess: any

  //label_save_combobox || label_save_radio
  public departmentCodeList: any[]
  public documentRole02PrintDocumentStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public tableDocumentRole2: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
      //isModalPreviewOpen: true
    }

    this.input = {
      id: null,
      //document_role02_receive_start_date: getMoment(),
      //document_role02_receive_end_date: getMoment(),
      department_code: '',
      rule_count: '',
      document_role02_print_document_status_code: '',
      request_number: '',
    }
    this.listDocumentRole02PrintDocument = []
    this.paginateDocumentRole02PrintDocument = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateDocumentRole02PrintDocument.id = 'paginateDocumentRole02PrintDocument'
    this.perPageDocumentRole02PrintDocument = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listDocumentProcess = []

    this.itemEdit = {}

    //Master List
    this.master = {
      departmentCodeList: [],
      documentRole02PrintDocumentStatusCodeList: [],
    }
    //Master List


    this.tableDocumentRole2 = {
      column_list: {
        index: "#",
        instruction_rule_name: "ประเภทหนังสือ",
        instruction_send_date: "วันที่มีคำสั่ง",
        document_role02_check_date: "วันที่ส่งมา",
        send_address_count: "จำนวนที่อยู่",
        document_role02_print_document_date: "วันที่พิมพ์",
        document_role02_print_document_status_name: "สถานะ",
      },
      button_list: [{
        name: "report",
        title: "พิมพ์ทั้งหมด",
        icon: "printer",
      }],
    }


    //
    //this.autocompleteListListModalLocation = []
    //this.is_autocomplete_ListModalLocation_show = false
    //this.is_autocomplete_ListModalLocation_load = false
    ////this.modal = { isModalPeopleEditOpen: false, }
    //this.inputAddress = {}
    //this.inputModalAddress = {}
    ////

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initDocumentRole02PrintDocumentList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.departmentCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.department_code = "ALL"
        this.master.documentRole02PrintDocumentStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.document_role02_print_document_status_code = "ALL"

      }
      if (this.editID) {
        this.DocumentProcessService.DocumentRole02PrintDocumentLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

      this.onClickDocumentRole02PrintDocumentList()
    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  onClickDocumentRole02PrintDocumentList(): void {
    // if(this.validateDocumentRole02PrintDocumentList()) {
    // Open loading
    // Call api
    this.callDocumentRole02PrintDocumentList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callDocumentRole02PrintDocumentList(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintDocumentList(this.help.GetFilterParams(params, this.paginateDocumentRole02PrintDocument)).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintDocumentListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }

  onClickDocumentRole02PrintDocumentSend(): void {
    // if(this.validateDocumentRole02PrintDocumentSend()) {
    // Open loading
    // Call api
    this.callDocumentRole02PrintDocumentSend(this.listDocumentRole02PrintDocument)
    // }
  }
  //! <<< Call API >>>
  callDocumentRole02PrintDocumentSend(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintDocumentSend(params).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintDocumentSendResponse(res)) {
      if (data) {
        // Set value
        //this.listData(data)
        //this.automateTest.test(this, { list: data.list })
        this.onClickDocumentRole02PrintDocumentList()

        window.open("pdf/TorKor/" + params.filter(r => r.is_check).map(r => r.id).join("|"))
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }


  onClickDocumentRole02PrintDocumentAdd(): void {
    this.listDocumentRole02PrintDocument.push({
      index: this.listDocumentRole02PrintDocument.length + 1,
      request_number: null,
      document_role02_receiver_by_name: null,
      rule_count: null,
      name: null,
      house_number: null,
      document_role02_print_document_date_text: null,
      document_role02_receive_remark: null,
      document_role02_print_document_status_name: null,

    })
    this.changePaginateTotal(this.listDocumentRole02PrintDocument.length, 'paginateDocumentRole02PrintDocument')
  }

  onClickDocumentRole02PrintDocumentEdit(item: any): void {
    console.log(item)

    this.modal.isModalPreviewOpen = true
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintDocumentItemLoad(item.id).subscribe((data: any) => {
      if (data) {

        this.itemEdit = data
        console.log(data.document_role_02_list)
        this.tableDocumentRole2.Set(data.document_role_02_list)
        console.log(this.itemEdit)
        // Set value
      }
      this.global.setLoading(false)
    })
  }


  onClickDocumentRole02PrintDocumentDelete(item: any): void {
    // if(this.validateDocumentRole02PrintDocumentDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listDocumentRole02PrintDocument.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listDocumentRole02PrintDocument.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listDocumentRole02PrintDocument.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listDocumentRole02PrintDocument.length; i++) {
          if (this.listDocumentRole02PrintDocument[i].is_check) {
            this.listDocumentRole02PrintDocument[i].cancel_reason = rs
            this.listDocumentRole02PrintDocument[i].status_code = "DELETE"
            this.listDocumentRole02PrintDocument[i].is_deleted = true

            if (true && this.listDocumentRole02PrintDocument[i].id) ids.push(this.listDocumentRole02PrintDocument[i].id)
            // else this.listDocumentRole02PrintDocument.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole02PrintDocumentDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listDocumentRole02PrintDocument.length; i++) {
          this.listDocumentRole02PrintDocument[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole02PrintDocumentDelete(params: any, ids: any[]): void {
    this.DocumentProcessService.DocumentRole02PrintDocumentDelete(params).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintDocumentDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listDocumentRole02PrintDocument.length; i++) {
          if (this.listDocumentRole02PrintDocument[i].id == id) {
            this.listDocumentRole02PrintDocument.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listDocumentRole02PrintDocument.length; i++) {
        this.listDocumentRole02PrintDocument[i] = i + 1
      }

      this.onClickDocumentRole02PrintDocumentList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  onClickCommand($event): void {
    console.log($event)

    if ($event.command == "report") {
      this.global.setLoading(true)
      var param = { id: this.itemEdit.document_role_02_list[0].id, is_check: true }
      this.DocumentProcessService.DocumentRole02PrintDocumentSend([param]).subscribe((data: any) => {
        if (data) {
          this.onClickDocumentRole02PrintDocumentList()
          window.open("pdf/TorKor/" + this.itemEdit.document_role_02_list[0].id)
          //this.global.setLoading(false)
        }
      })
    }
    //}
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listDocumentRole02PrintDocument = data.documentrole02printdocument_list || []
    this.changePaginateTotal(this.listDocumentRole02PrintDocument.length, 'paginateDocumentRole02PrintDocument')

  }

  listData(data: any): void {
    this.listDocumentRole02PrintDocument = data.list || []
    this.help.PageSet(data, this.paginateDocumentRole02PrintDocument)

  }

  saveData(): any {
    // let params = this.input
    // params.documentrole02printdocument_list = this.listDocumentRole02PrintDocument || []

    const params = {
      document_role02_receive_start_date: this.input.document_role02_receive_start_date,
      document_role02_receive_end_date: this.input.document_role02_receive_end_date,
      department_code: this.input.department_code,
      rule_count: this.input.rule_count,
      document_role02_print_document_status_code: this.input.document_role02_receive_end_date,
      request_number: this.input.request_number,
      //page_index: +this.paginateDocumentRole02PrintDocument.currentPage,
      //item_per_page: +this.paginateDocumentRole02PrintDocument.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'document_role02_receive_date',
      //  value: displayDateServer(this.input.document_role02_receive_start_date),
      //  operation: 3
      //}, {
      //  key: 'document_role02_receive_date',
      //  value: displayDateServer(this.input.document_role02_receive_end_date),
      //  operation: 4
      //}, {
      //  key: 'department_code',
      //  value: this.input.department_code,
      //  operation: 0
      //}, {
      //  key: 'rule_count',
      //  value: this.input.rule_count,
      //  operation: 5
      //}, {
      //  key: 'document_role02_print_document_status_code',
      //  value: this.input.document_role02_print_document_status_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}]
    }

    return params
  }


  togglePlayer(item: any = null): void {
    if (!item) {
      item = this.input
    }

    if (!item.player) {
      item.player = new Audio(item.sound_file_physical_path)
      item.player_action = null
    }

    if (item.player_action) {
      item.player.pause()
    } else {
      item.player.play()
    }
    item.player_action = !item.player_action
  }


  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginateDocumentRole02PrintDocument') {
      this.onClickDocumentRole02PrintDocumentList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
