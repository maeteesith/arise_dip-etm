import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-madrid-role04-search-regis',
  templateUrl: './madrid-role04-search-regis.component.html',
  styleUrls: ['./madrid-role04-search-regis.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole04SearchRegisComponent implements OnInit {

  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any
  public role: any


  public modal: any

  public listPublicItem: any = [];
  public doc: any = [];

  public selectedDoc: any = -1;
  constructor() { }

  ngOnInit() {
    this.role = true;
    this.listPublicItem = [
        { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "AAAAAAAAAAAAAAAA", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอดำเนินการ", subject: "ไม่มาดำเนินการ" },
        { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "BBBBBBBBBBBBB", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอดำเนินการ", subject: "ไม่มาดำเนินการ" },
        { index: "3", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอดำเนินการ", subject: "ไม่มาดำเนินการ" },
        { index: "4", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "DDDDDDDDDDDDDDDDD", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอดำเนินการ", subject: "ไม่มาดำเนินการ" },
        { index: "5", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "พิจารณา", subject: "ไม่มาดำเนินการ" },
        { index: "6", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "FFFFFFFFFFFF", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "พิจารณา", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" },
        { index: "7", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "พิจารณา", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" },
        { index: "8", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "พิจารณา", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" },
        { index: "9", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "พิจารณา", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" },
        { index: "10", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "พิจารณา", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" }

    ]

    this.doc = [{ value: "1", text: "MMC"},{ value: "2", text: "MMU"}]

    this.validate = {}
    this.input = {
        id: null,
        //instruction_send_start_date: getMoment(),
        //instruction_send_end_date: getMoment(),
        public_type_code: '',
        public_source_code: '',
        public_receiver_by: '',
        public_status_code: '',
        request_number: '',
    }
    this.master = {
        request_type: [{ code: 1, name: '1' }],
        job_reciver: [{ id: 1, name: "Sam" }, { id: 2, name: "Tom" }]
    }
  }

}
