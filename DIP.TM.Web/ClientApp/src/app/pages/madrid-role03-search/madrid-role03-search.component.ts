import { ModalMadridRole03FileAttachment } from './../modal-madrid-role03-file-attachment/modal-madrid-role03-file-attachment.component';
import { Component, OnInit } from '@angular/core'
import { MatDialog } from '@angular/material';

@Component({
  selector: "app-madrid-role03-search",
  templateUrl: "./madrid-role03-search.component.html",
  styleUrls: ["./madrid-role03-search.component.scss",
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole03SearchComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    public status :boolean=false;
    // Response
    public response: any
    public role: any
  
  
    public modal: any
  
    public listPublicItem: any = [];
    public doc: any = [];
  
    public selectedDoc: any = -1;
    
    constructor(
      public dialog:MatDialog
    ) { }
  
    ngOnInit() {
      this.role = true;
      this.listPublicItem = [
          { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "AAAAAAAAAAAAAAAA", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "ให้งานแล้ว", subject: "ไม่มาดำเนินการ" },
          { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "BBBBBBBBBBBBB", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "ให้งานแล้ว", subject: "ไม่มาดำเนินการ" },
          { index: "3", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "ให้งานแล้ว", subject: "ไม่มาดำเนินการ" },
          { index: "4", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "DDDDDDDDDDDDDDDDD", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "ให้งานแล้ว", subject: "ไม่มาดำเนินการ" },
          { index: "5", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอให้งาน", subject: "ไม่มาดำเนินการ" },
          { index: "6", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "FFFFFFFFFFFF", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอให้งาน", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" },
          { index: "7", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอให้งาน", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" },
          { index: "8", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอให้งาน", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" },
          { index: "9", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอให้งาน", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" },
          { index: "10", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "รอให้งาน", subject: "จำหน่ายเลขที่อ้างอิงคำขอ" }
  
      ]
  
      this.doc = [{ value: "1", text: "MMC"},{ value: "2", text: "MMU"}]
  
      this.validate = {}
      this.input = {
          id: null,
          //instruction_send_start_date: getMoment(),
          //instruction_send_end_date: getMoment(),
          public_type_code: '',
          public_source_code: '',
          public_receiver_by: '',
          public_status_code: '',
          request_number: '',
      }
      this.master = {
          request_type: [{ code: 1, name: '1' }],
          job_reciver: [{ id: 1, name: "Sam" }, { id: 2, name: "Tom" }]
      }
    }

    Modal1(){
      const dialogRef = this.dialog.open(ModalMadridRole03FileAttachment, {
          width: "calc(100% - 80px)",
          maxWidth: "1000px",
          data: {}
        });
        dialogRef.afterClosed().subscribe(res => {
        });

  }
}
