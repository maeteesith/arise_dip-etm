import { Component, OnInit } from "@angular/core";
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { MyCustomUploadAdapterPlugin } from "../../helpers";

@Component({
  selector: "app-test-ckeditor",
  templateUrl: "./test-ckeditor.component.html",
  styleUrls: ["./test-ckeditor.component.scss"]
})
export class TestCkeditorComponent implements OnInit {
  //TODO >>> Declarations <<<
  public input: any;
  public Editor: any;

  constructor() {}

  ngOnInit() {
    ClassicEditor.create(document.querySelector("#editor"), {
      initialData: "<h2>Initial data</h2>",
      extraPlugins: [MyCustomUploadAdapterPlugin]
    })
      .then((newEditor: any) => {
        this.input = {
          ckeditor: newEditor
        };
      })
      .catch((error: any) => {
        console.error(error);
      });

    this.Editor = ClassicEditor;
  }

  save(): void {
    console.log(this.input.ckeditor.getData());
  }
}
