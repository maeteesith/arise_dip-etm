import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Component, Input, Output, OnInit, EventEmitter, Inject } from "@angular/core";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { CONSTANTS, validateService } from "../../helpers";


@Component({
  selector: "app-modal-madrid-role03-file-attachment",
  templateUrl: "./modal-madrid-role03-file-attachment.component.html",
  styleUrls: ["./modal-madrid-role03-file-attachment.component.scss",
  './../../../assets/theme/styles/madrid/madrid.scss'],
})
export class ModalMadridRole03FileAttachment implements OnInit {

  public role: any
  
  
    
  
    public listPublicItem: any = [];




  constructor( 
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalMadridRole03FileAttachment>,
    
    @Inject(MAT_DIALOG_DATA) public data: any)  {}

  ngOnInit() {
    this.role = true;
      this.listPublicItem = [
          { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "AAAAAAAAAAAAAAAA", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "ให้งานแล้ว", subject: "ไม่มาดำเนินการ" },
          { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "BBBBBBBBBBBBB", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "ให้งานแล้ว", subject: "ไม่มาดำเนินการ" },
          { index: "3", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551",IrnNo:"TH17991551", requestNo: "741150971", details: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "MM2", statusprogress: "ให้งานแล้ว", subject: "ไม่มาดำเนินการ" },
         
  
      ]
    
    
  }

  closeModal() {
    
    this.dialogRef.close();
  }

 
 
  }

