import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { GlobalService } from "../../global.service";
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from "../../services/fork-join-eform.service";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { Router } from "@angular/router";
import {
  ROUTE_PATH,
  CONSTANTS,
  validateService,
  clone,
  displayAddress,
  clearIdAndSaveId,
  viewPDF,
  getParamsOverwrite,
} from "../../helpers";

@Component({
  selector: "app-eform-save-021-process",
  templateUrl: "./eform-save-021-process.component.html",
  styleUrls: ["./eform-save-021-process.component.scss"],
})
export class eFormSave021ProcessComponent implements OnInit, DeactivationGuarded {
  //TODO >>> Declarations <<</'
  public editID: any;
  public input: any;
  public validate: any;
  public master: any;
  public popup: any;
  public menuList: any;
  public stepPass: any;
  public mymaster: any[];
  public telephone: any;
  public email: any;
  public eform_number: any;
  public Next4: any;
  public ContactWord: any;
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubID: number;
  public currentSubStep: number;
  public progressPercent: number;
  // Response
  public response: any;
  public receiver_contact_address: any;
  public autocompleteListListModalLocation: any;
  public is_autocomplete_ListModalLocation_show: any;
  public is_autocomplete_ListModalLocation_load: any;
  public inputModalAddress: any;
  public inputModalAddressEdit: any;
  public modal: any;
  public inputAddress: any;
  public findVal: any;
  public listdropdownContact: any;
  public receiverContactChangeType: string;
  public SAVE021_REPRESENTATIVE_CONDITION_TYPE_CODE: string;
  public titleStep: any;
  public Noliststep2: false;
  public currentP1: number;
  public perpageP1: number;
  public totalP1: number;

  public currentP2: number;
  public perpageP2: number;
  public totalP2: number;

  public saveInput: any;

  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  // Autocomplete
  public autocompleteList: any;
  public isShowAutocomplete: any;

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eFormSaveProcessService: eFormSaveProcessService
  ) {}

  ngOnInit() {
    this.editID = +this.route.snapshot.paramMap.get("id");
    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubID = 1;
    this.currentSubStep = 1;
    this.progressPercent = 0;
    this.stepPass = 1;

    this.menuList = [
      {
        id: 1,
        number: 1,
        name: "บันทึกเพื่อรับไฟล์",
        //required: true
        canedit: false,
      },
      {
        id: 2,
        number: 2,
        name: "คำโต้แย้งคำขอเลขที่",
        required: true,
        canedit: false,
      },
      {
        id: 3,
        number: 3,
        name: "เจ้าของ / ตัวแทน",
        required: true,
        canedit: false,
      },
      {
        id: 4,
        number: 4,
        name: "สถานที่ติดต่อภายในประเทศไทย",
        canedit: false,
      },
      {
        id: 5,
        number: 5,
        name: "ระบุเหตุแห่งการโต้แย้ง",
        canedit: false,
      },
      {
        id: 6,
        number: 6,
        name: "เอกสารหลักฐานประกอบคำโต้เเย้ง",
        canedit: false,
      },

      //{
      //  id: 7,
      //  number: 7,
      //  name: 'ค่าธรรมเนียมน'
      //},
      {
        id: 7,
        number: 7,
        name: "เสร็จสิ้น",
      },
    ];

    this.receiver_contact_address = {
      descritionS5: null,

      address_type_codeS5: null,
      nationality_codeS5: null,
      career_codeS5: null,
      card_type_codeS5: null,
      card_type_nameS5: null,
      card_numberS5: null,
      receiver_type_codeS5: null,
      nameS5: null,
      house_numberS5: null,
      village_numberS5: null,
      alleyS5: null,
      streetS5: null,
      address_sub_district_codeS5: null,
      address_district_codeS5: null,
      address_province_codeS5: null,
      postal_codeS5: null,
      address_country_codeS5: null,
      telephoneS5: null,
      faxS5: null,
      emailS5: null,
      sex_codeS5: null,
    };

    this.currentP1 = 1;
    this.perpageP1 = 1;

    this.currentP2 = 1;
    this.perpageP2 = 1;

    this.validate = {};

    this.titleStep = "คำโต้แย้ง (ก.02)";

    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalDomesticContactAddressOpen: true,
    };

    this.input = {
      id: null,
      indexEdit: undefined,
      point: "",
      isAllowEditRequestNumber: true,
      product_list: [],
      people_list: [],
      people_01_list: [], //not yet field in Table
      representative_list: [],
      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      save021_representative_condition_type_code: "AND_OR",
      contact_type_index: 0,
      listAgentMarkRepresentative1: [],
      listAgentMarkRepresentative2: [],
      address: {
        receiver_type_code: "PEOPLE",
      },
      ownerDomesticContactAddress: 0,
      agentDomesticContactAddress: 0,
      subagentDomesticContactAddress: 0,
      dimension_image: 2,

      is_2_1: false,
      is_2_2: false,
      is_2_3: false,
      is_2_4: false,
      is_2_5: false,
      is_2_6: false,
      is_2_7: false,
      is_2_8: false,
      is_2_9: false,
      is_2_10: false,

      is_6_1: true,
      is_6_2: true,
      is_6_3: false,
      public_start_date: null,
      request_item_sub_type_1_code_text: null,
      public_line_index: null,
      email: null,
      telephone: null,
      save021_contact_type_code: null,

      request_number: null,
      request_date_text: null,

      imageDimension2: {
        file: {},
        blob: null,
      },
      imageDimension3_1: {
        file: {},
        blob: null,
      },
      imageDimension3_2: {
        file: {},
        blob: null,
      },
      imageDimension3_3: {
        file: {},
        blob: null,
      },
      imageDimension3_4: {
        file: {},
        blob: null,
      },
      sizeX: 5,
      sizeY: 5,
      isResize: false,
      audio: {
        input: "",
        validate: "",
        file: {},
        sound: null,
        palyer: null,
        action: false,
      },
      imageNote: {
        file: {},
        blob: null,
      },
      listTranslation: [],
      translationItem: {},
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0,
      },
      imageMark: {
        file: {},
        blob: null,
      },

      product_list_test: [
        {
          item_sub_type_1_code: 1,
          name: "A",
          amount_product: "1000",
        },
        {
          item_sub_type_1_code: 2,
          name: "A",
          amount_product: "1000",
        },
        {
          item_sub_type_1_code: 3,
          name: "A",
          amount_product: "1000",
        },
        {
          item_sub_type_1_code: 4,
          name: "A",
          amount_product: "1000",
        },
        {
          item_sub_type_1_code: 3,
          name: "A",
          amount_product: "1000",
        },
      ],

      isCheckAllOwnerSignature: false,
      isCheckAllAgentSignature: false,
    };

    //Master List
    this.master = {};
    //Master List
    this.response = {
      load: {},
    };

    this.popup = {
      isWarning: false,
      isConfirmSave: false,
      isSearch: false,
    };

    // Autocomplete
    this.autocompleteList = {
      address_sub_district_name: [],
    };
    this.isShowAutocomplete = {
      address_sub_district_name: false,
    };

    this.input.people_01_list = [
      {
        id: 1,
        name: "nanpipat klinpratoom",
        address: "samutprakarn",
        tel: "0858455652",
      },
      {
        id: 2,
        name: "toptop  toptopt",
        address: "asdsadas",
        tel: "08900",
      },
    ];
    this.input.representative_list = [
      // {
      //   id: 1,
      //   name: "Chalie Clark",
      //   address: "samutprakarn",
      //   tel: "0858455652",
      // },
      // {
      //   id: 2,
      //   name: "Edward Levinof",
      //   address: "bangkok",
      //   tel: "08900",
      // },
    ];

    this.input.ListAssignRetaliate_test = [
      {
        protestation_type: "99",
        protestation_no: 1,
        advertise_date: "01/01/2562",
        advertise_finshi_date: "02/02/2562",
      },
      {
        protestation_type: "55",
        protestation_no: 2,
        advertise_date: "09/09/2562",
        advertise_finshi_date: "10/10/2562",
      },
      {
        protestation_type: "69",
        protestation_no: 3,
        advertise_date: "10/10/2562",
        advertise_finshi_date: "12/12/2562",
      },
    ];
    //
    this.autocompleteListListModalLocation = [];
    this.is_autocomplete_ListModalLocation_show = false;
    this.is_autocomplete_ListModalLocation_load = false;

    this.inputAddress = {};
    this.inputModalAddress = {};

    //
    // Other
    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;

    this.callInit();
    this.calOnchangepage();
    this.calOnchangepage2();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initEForm021Page().subscribe((data: any) => {
      if (data) {
        this.master = data;
        console.log("this.master", this.master);
      }
      if (this.editID) {
        this.eFormSaveProcessService
          .eFormSave021Load(this.editID, {})
          .subscribe((data: any) => {
            if (data) {
              this.response.load = {
                eform_number: data.eform_number,
                id: data.id,
                wizard: data.wizard,
              };
              this.input.isAllowEditRequestNumber = data.request_number
                ? false
                : true;
              // step 1
              this.input.email = data.email;
              this.input.telephone = data.telephone;
              //TODO step 2
              this.input.request_date_text = data.request_date_text;
              this.input.request_number = data.request_number;
              this.input.registration_number = data.registration_number;
              // step 3
              this.input.people_list = data.people_list;
              this.input.representative_list = data.representative_list;
              this.input.save021_representative_condition_type_code = data.save021_representative_condition_type_code
                ? data.save021_representative_condition_type_code
                : "AND_OR";
              // step 4
              this.input.save021_contact_type_code =
                data.save021_contact_type_code;
              this.input.address =
                data.save021_contact_type_code === "OTHERS"
                  ? data.contact_address_list[0]
                  : this.input.address;
              //TODO step 5
              // step 6
              this.input.is_6_1 = data.is_6_1;
              this.input.is_6_2 = data.is_6_2;
              this.input.is_6_3 = data.is_6_3;
              // step 7
              this.input.isCheckAllOwnerSignature =
                data.sign_inform_person_list === "0" ? true : false;
              this.input.isCheckAllAgentSignature =
                data.sign_inform_representative_list === "0" ? true : false;
              this.manageWizard(data.wizard);
              this.setSignInform(data.sign_inform_representative_list);
            }
            // Close loading
            this.global.setLoading(false);
          });
      } else {
        this.global.setLoading(false);
      }
    });
  }

  //! <<< Call API >>>
  callSendEmail021(params: any): void {
    this.eFormSaveProcessService
      .eFormSave021Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchRequestNumber(): void {
    this.eFormSaveProcessService
      .eFormSave021Search(this.input.request_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.request_date_text = data.request_date_text;
          this.input.registration_number = data.registration_load_number;
          this.input.people_list = clearIdAndSaveId(data.people_load_list);
          this.input.representative_list = clearIdAndSaveId(
            data.representative_load_list
          );
          this.input.product_list = data.product_load_list;
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callEFormSave021Save(params: any): void {
    this.eFormSaveProcessService
      .eFormSave021Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.isDeactivation = true;
          // Open toast success
          let toast = CONSTANTS.TOAST.SUCCESS;
          toast.message = "บันทึกข้อมูลสำเร็จ";
          this.global.setToast(toast);
          // Navigate
          this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
        } else {
          // Close loading
          this.global.setLoading(false);
        }
      });
  }
  callSubDistrict(params: any, name: any): void {
    this.eFormSaveProcessService
      .SearchLocation(params)
      .subscribe((data: any) => {
        if (data) {
          this.autocompleteList[name] = data;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }

  //! <<< Prepare Call API >>>
  sendEmail(): void {
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      wizard: this.getWizard(),
      email: this.input.email,
      telephone: this.input.telephone,
    };
    // Call api
    this.callSendEmail021(params);
  }
  searchRequestNumber(): void {
    let result = validateService(
      "validateEFormSave140ProcessStep2",
      this.input
    );
    this.validate = result.validate;

    if (result.isValid) {
      // Open loading
      this.global.setLoading(true);
      // Call api
      this.callSearchRequestNumber();
    }
  }
  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }
  save(isOverwrite: boolean): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = this.getParamsSave();
    if (!isOverwrite) {
      params = getParamsOverwrite(params);
    }
    // Call api
    this.callEFormSave021Save(params);
  }
  getParamsSave(): any {
    return {
      id: this.response.load.id ? this.response.load.id : null,
      eform_number: this.response.load.eform_number
        ? this.response.load.eform_number
        : null,
      wizard: this.getWizard(),
      // step 1
      email: this.input.email,
      telephone: this.input.telephone,
      //TODO step 2
      request_date_text: this.input.request_date_text,
      request_number: this.input.request_number,
      registration_number: this.input.registration_number,
      // step 3
      people_list: this.input.people_list,
      representative_list: this.input.representative_list,
      save021_representative_condition_type_code: this.input
        .save021_representative_condition_type_code,
      // step 4
      save021_contact_type_code: this.input.save021_contact_type_code,
      contact_address_list:
        this.input.save021_contact_type_code === "OTHERS"
          ? [this.input.address]
          : [],
      //TODO step 5
      // step 6
      is_6_1: this.input.is_6_1,
      is_6_2: this.input.is_6_2,
      is_6_3: this.input.is_6_3,
      // step 7
      sign_inform_person_list: this.input.isCheckAllOwnerSignature ? "0" : "",
      sign_inform_representative_list:
        this.input.isCheckAllAgentSignature &&
        this.input.save040_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform(),
    };
  }
  onClickViewPdfKor02(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM021", this.getParamsSave());
    }
  }

  //! <<< Event >>>
  getWizard(): String {
    if (this.editID) {
      let wizardList = this.response.load.wizard.split("|");
      let sum = 0;
      let sumNow =
        this.currentID +
        this.currentStep +
        this.currentSubID +
        this.currentSubStep;

      wizardList.forEach((item: any) => {
        sum = +sum + +item;
      });

      if (sumNow > sum) {
        return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
      } else {
        return this.response.load.wizard;
      }
    } else {
      return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
    }
  }
  getSignInform(): String {
    let result = "";
    this.input.representative_list.forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input.representative_list.forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save021_representative_condition_type_code != "AND") {
        this.input.representative_list.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name];
  }
  toggleBooleanInTable(item: any, name: any): void {
    item[name] = !item[name];
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    this.currentP1 = page;
    this.calOnchangepage();
  }
  onChangePerPage(value: number, name: string): void {
    this.perpageP1 = value;
    this.calOnchangepage();
  }
  calOnchangepage() {
    this.totalP1 = Math.ceil(this.input.people_list.length / this.perpageP1);
  }

  onChangePage2(page: any, name: string): void {
    this.currentP2 = page;
    this.calOnchangepage2();
  }
  onChangePerPage2(value: number, name: string): void {
    this.perpageP2 = value;
    this.calOnchangepage2();
  }
  calOnchangepage2() {
    this.totalP2 = Math.ceil(this.input.people_list.length / this.perpageP2);
  }

  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT;
    paginate.totalItems = total;
    this[name] = paginate;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  Save(): void {
    this.saveInput = {};
    console.log("SeeInput", this.input);
    this.saveInput.email = this.input.email;
    this.saveInput.telephone = this.input.telephone;
    this.saveInput.make_date = this.input.request_date_text;
    this.saveInput.request_number = this.input.request_number;
    this.saveInput.register_number = this.input.registration_number;
    this.saveInput.save021_contact_type_code = this.input.save021_contact_type_code;
    this.saveInput.public_start_date = this.input.public_start_date;
    this.saveInput.request_item_sub_type_1_code = this.input.request_item_sub_type_1_code;
    this.saveInput.book_index = this.input.book_index;

    this.saveInput.is_2_1 = this.input.is_2_1;
    this.saveInput.is_2_2 = this.input.is_2_1;
    this.saveInput.is_2_3 = this.input.is_2_1;
    this.saveInput.is_2_4 = this.input.is_2_1;
    this.saveInput.is_2_5 = this.input.is_2_1;
    this.saveInput.is_2_6 = this.input.is_2_1;
    this.saveInput.is_2_7 = this.input.is_2_1;
    this.saveInput.is_2_8 = this.input.is_2_1;
    this.saveInput.is_2_9 = this.input.is_2_1;
    this.saveInput.is_2_10 = this.input.is_2_1;

    this.saveInput.people_list = [];
    this.saveInput.representative_list = [];
    this.saveInput.contact_address_list = [];
    this.saveInput.receiver_people_list = [];
    this.saveInput.receiver_representative_list = [];

    this.saveInput.receiver_contact_address_list = [];
    this.saveInput.is_6_1 = this.input.is_6_1;
    this.saveInput.is_6_2 = this.input.is_6_2;
    this.saveInput.is_6_3 = this.input.is_6_3;

    this.saveInput.SAVE021_REPRESENTATIVE_CONDITION_TYPE_CODE = this.SAVE021_REPRESENTATIVE_CONDITION_TYPE_CODE;
    this.saveInput.payer_name = this.input.payer_name;
    this.saveInput.total_price = this.input.total_price;

    this.saveInput.contact_address_list.push({
      name: this.receiver_contact_address.nameS5,
      house_number: this.receiver_contact_address.house_numberS5,
      receiver_type_codeS5: this.receiver_contact_address.receiver_type_codeS5,
      village_number: this.receiver_contact_address.village_numberS5,
      alley: this.receiver_contact_address.alleyS5,
      street: this.receiver_contact_address.streetS5,
      address_sub_district_name: this.receiver_contact_address
        .address_sub_district_nameS5,
      postal_code: this.receiver_contact_address.postal_codeS5,
      email: this.receiver_contact_address.emailS5,
      telephone: this.receiver_contact_address.telephoneS5,
      fax: this.receiver_contact_address.faxS5,
    });

    this.input.people_list.forEach((obj) => {
      this.saveInput.people_list.push({
        address_type_code: obj.address_type_code,
        nationality_code: obj.nationality_code,
        career_code: obj.career_code,
        card_type_code: obj.card_type_code,
        card_type_name: obj.card_type_name,
        card_number: obj.card_number,
        receiver_type_code: obj.receiver_type_code,
        name: obj.name,
        house_number: obj.house_number,
        village_number: obj.village_number,
        alley: obj.alley,
        street: obj.street,
        address_sub_district_code: obj.address_sub_district_code,
        address_district_code: obj.address_district_code,
        address_province_code: obj.address_province_code,
        postal_code: obj.postal_code,
        address_country_code: obj.address_country_code,
        telephone: obj.telephone,
        fax: obj.fax,
        email: obj.email,
        sex_code: obj.sex_code,
        is_contact_person: obj.is_contact_person,
      });
    });

    this.input.representative_list.forEach((obj) => {
      this.saveInput.representative_list.push({
        address_type_code: obj.address_type_code,
        nationality_code: obj.nationality_code,
        career_code: obj.career_code,
        card_type_code: obj.card_type_code,
        card_type_name: obj.card_type_name,
        card_number: obj.card_number,
        receiver_type_code: obj.receiver_type_code,
        name: obj.name,
        house_number: obj.house_number,
        village_number: obj.village_number,
        alley: obj.alley,
        street: obj.street,
        address_sub_district_code: obj.address_sub_district_code,
        address_district_code: obj.address_district_code,
        address_province_code: obj.address_province_code,
        postal_code: obj.postal_code,
        address_country_code: obj.address_country_code,
        telephone: obj.telephone,
        fax: obj.fax,
        email: obj.email,
        sex_code: obj.sex_code,
        is_contact_person: obj.is_contact_person,
      });
    });

    console.log("DATASAVE", this.saveInput);
    if (this.currentID != 1) {
      this.eFormSaveProcessService
        .eFormSave080Save(this.saveInput)
        .subscribe((data: any) => {
          console.log(data);
          this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
        });
    }
  }

  onSelectColumnProduct(item: any): void {
    item.is_check = !item.is_check;
    this.clearValidate("step2");
  }

  SearchRequesNumber(): void {
    //alert("sssssssssss");
    this.calleFormFromRequestNumberLoad(this.input.request_number);
    this.SAVE021_REPRESENTATIVE_CONDITION_TYPE_CODE = "AND_OR";
  }
  //! <<< Call API >>>
  calleFormFromRequestNumberLoad(params: any): void {
    // this.global.setLoading(true);
    // this.eFormSaveProcessService
    //   .eFormFromRequestNumberLoad(params)
    //   .subscribe((data: any) => {
    //     console.log(data);
    //     // if(isValideFormFromRequestNumberLoadResponse(res)) {
    //     // if(isValideFormFromRequestNumberLoadResponse(res)) {
    //     if (data) {
    //       // Set value
    //       this.listData(data);
    //       this.input.telephone = this.telephone;
    //       this.input.email = this.email;
    //       this.input.ListAssignRetaliate = {
    //         protestation_type: "99",
    //         protestation_no: "1",
    //         advertise_date: "01/01/2562",
    //         advertise_finshi_date: "02/02/2562",
    //       };
    //       this.input.ownerMarkItem = {
    //         receiver_type_code: "PEOPLE",
    //         sex_code: "MALE",
    //         nationality_code: "AF",
    //         career_code: "ค้าขาย",
    //         address_country_code: "TH",
    //       };
    //       this.input.receiverMarkItem = {};
    //       this.input.agenReceiverMarkItem = {
    //         receiver_type_code: "PEOPLE",
    //         sex_code: "MALE",
    //         nationality_code: "AF",
    //         career_code: "ค้าขาย",
    //         address_country_code: "TH",
    //       };
    //       this.input.agentMarkItem = {
    //         receiver_type_code: "PEOPLE",
    //         sex_code: "MALE",
    //         nationality_code: "AF",
    //         career_code: "ค้าขาย",
    //         address_country_code: "TH",
    //       };
    //       this.input.receiverMark = {
    //         receiver_type_code: "PEOPLE",
    //         sex_code: "MALE",
    //         nationality_code: "AF",
    //         career_code: "ค้าขาย",
    //         address_country_code: "TH",
    //       };
    //       this.input.receiver_people_list = [];
    //       this.input.receiver_representative_list = [];
    //     } else {
    //       alert("Data not found.");
    //     }
    //     this.global.setLoading(false);
    //     // }
    //     // Close loading
    //   });
  }

  // Modal Location
  autocompleteChangeListModalLocation(
    object: any,
    name: any,
    item_per_page: any = 5,
    length: any = 2
  ): void {
    if (object[name].length >= length) {
      console.log("d");
      this.is_autocomplete_ListModalLocation_show = true;

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page,
        },
      };

      this.callAutocompleteChangeListModalLocation(params);
    }
  }
  autocompleteBlurListModalLocation(
    object: any,
    name: any,
    item_per_page: any = 5
  ): void {
    let pThis = this;
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false;
    }, 200);
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return;
    this.input.is_autocomplete_ListModalLocation_load = true;
    let pThis = this;
    // this.eFormSaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false;
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code;
    this.inputModalAddress.address_sub_district_name = data.name;
    this.inputModalAddress.address_district_code = data.district_code;
    this.inputModalAddress.address_district_name = data.district_name;
    this.inputModalAddress.address_province_code = data.province_code;
    this.inputModalAddress.address_province_name = data.province_name;
    this.inputModalAddress.address_country_code = data.country_code;
    this.inputModalAddress.address_country_name = data.country_name;
    this.is_autocomplete_ListModalLocation_show = false;
  }

  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item];
    });
    this.toggleModal("isModalPeopleEditOpen");
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList?: string, id?: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null;
        }
      });
    } else {
      this.validate[name] = null;
    }
  }

  loadData(data: any): void {
    this.input = data;
  }

  listData(data: any): void {
    this.input = data;
  }

  saveData(): any {
    let params = this.input;

    return params;
  }
  reRunMenuNumber(): void {
    let number = 1;
    this.menuList.forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
      }
    });
  }

  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    } else if (this.currentID === 2) {
      let result = validateService(
        "validateEFormSave140ProcessStep2",
        this.input
      );
      this.validate = result.validate;

      if (!this.input.people_list || this.input.people_list.length === 0) {
        result.isValid = false;
        console.warn(`request_number is invalid.`);
        this.validate.request_number = "กรุณาคลิก ค้นหา";
      } else {
        if (this.input.product_list.length > 0) {
          let isCheck = false;
          this.input.product_list.forEach((item: any) => {
            if (item.is_check) {
              isCheck = true;
            }
          });
          if (isCheck) {
            this.clearValidate("step2");
            return true;
          } else {
            console.warn(`step2 is invalid.`);
            this.validate.step2 = "กรุณาเลือกผู้คัดค้าน";
            return false;
          }
        } else {
          console.warn(`step2 is invalid.`);
          this.validate.step2 = "กรุณาเลือกผู้คัดค้าน";
          return false;
        }
      }

      return result.isValid;
    } else if (this.currentID === 3) {
      if (this.input.people_list.length < 1) {
        console.warn(`step3 is invalid.`);
        this.validate.step3 = "กรุณาเพิ่มเจ้าของเครื่องหมาย";
        return false;
      } else {
        // Condition
        this.conditionWizard("clone_representative_list");
        return true;
      }
    } else if (this.currentID === 4) {
      let result = validateService(
        "validateEFormSave021ProcessStep4",
        this.input
      );
      this.validate = result.validate;

      // Condition
      if (result.isValid) {
        if (this.input.save021_contact_type_code === "OTHERS") {
          let result = validateService(
            "validateEFormSave01ProcessStep4Other",
            this.input.address
          );
          this.validate = result.validate;
          if (
            result.validate.address_district_name ||
            result.validate.address_province_name ||
            result.validate.postal_code
          ) {
            this.validate.address_sub_district_name = "กรุณากรอกและเลือก ตำบล";
          }
          return result.isValid;
        } else {
          // Set is_contact_person
          if (this.input.save021_contact_type_code === "OWNER") {
            this.input.people_list.forEach((item: any, index: any) => {
              if (+this.input.contact_type_index === index) {
                item.is_contact_person = true;
              } else {
                item.is_contact_person = false;
              }
            });
          } else {
            this.input.representative_list.forEach((item: any, index: any) => {
              if (this.input.save021_contact_type_code === "REPRESENTATIVE") {
                // ตัวแทน
                if (
                  this.input.listAgentMarkRepresentative1[
                    +this.input.contact_type_index
                  ] === index
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              } else if (
                this.input.save021_contact_type_code === "REPRESENTATIVE_PERIOD"
              ) {
                // ตัวแทนช่วง
                if (
                  this.input.listAgentMarkRepresentative2[
                    +this.input.contact_type_index
                  ] === index
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              }
            });
          }
        }
      }

      return result.isValid;
    } else if (this.currentID === 7) {
      let isValid = false;

      if (
        this.input.isCheckAllOwnerSignature ||
        this.input.isCheckAllAgentSignature
      ) {
        isValid = true;
      }

      if (this.input.representative_list.length > 0) {
        this.input.representative_list.forEach((item: any) => {
          if (item.isCheck) {
            isValid = true;
          }
        });
      }

      if (!isValid) {
        console.warn(`step7 is invalid.`);
        this.validate.step7 = "กรุณาเลือกลงลายมือชื่อ";
      } else {
        // Set can edit
        this.menuList[6].canEdit = true;
      }

      return isValid;
    }
    return true;
  }
  conditionWizard(condition: any): void {
    if (condition === "clone_representative_list") {
      let listAgentMarkRepresentative1 = [];
      let listAgentMarkRepresentative2 = [];
      this.input.representative_list.forEach((item: any, index: any) => {
        if (item.representative_type_code == "REPRESENTATIVE") {
          listAgentMarkRepresentative1.push(index);
        } else {
          listAgentMarkRepresentative2.push(index);
        }
      });
      this.input.listAgentMarkRepresentative1 = clone(
        listAgentMarkRepresentative1
      );
      this.input.listAgentMarkRepresentative2 = clone(
        listAgentMarkRepresentative2
      );

      if (!this.input.payer_name) {
        this.input.payer_name = this.input.people_list[0].name;
      }
    }
  }
  manageWizard(wizard: any): void {
    let wizardList = wizard ? wizard.split("|") : [1, 1, 1, 1];
    this.currentID = +wizardList[0];
    this.currentStep = +wizardList[1];
    this.currentSubID = +wizardList[2];
    this.currentSubStep = +wizardList[3];
    this.calcProgressPercent(this.currentStep);

    if (this.currentID >= 3) {
      this.conditionWizard("clone_representative_list");
    }
    if (this.currentID >= 4) {
      if (this.input.save021_contact_type_code !== "OTHERS") {
        if (this.input.save021_contact_type_code === "OWNER") {
          this.input.people_list.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.contact_type_index = index;
            }
          });
        } else {
          this.input.representative_list.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.contact_type_index = index;
            }
          });
        }
      }
    }

    this.menuList.forEach((item: any) => {
      if (this.currentID > item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            sub.canEdit = true;
          });
        }
      }
      if (this.currentID === item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (this.currentSubID >= sub.id) {
              sub.canEdit = true;
            }
          });
        }
      }
    });
  }
  //! <<< Wizard >>>

  changeSubStep(action: number): void {
    if (this.validateWizard()) {
      this.currentSubStep = this.currentSubStep + action;
    }
  }

  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  findClick(): void {
    this.findVal = true;

    this.titleStep = "คำโต้แย้ง";
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value);
    }

    if (object) {
      object[name] = value;
    } else {
      this.input[name] = value;
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => {
      item.is_check = value;
    });
  }
  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalReceiverFormOpen") {
          this.input.receiverMarkItem = clone(
            this.input.receiver_people_list[index]
          );
        } else if (name === "isModalAgentReceiverFormOpen") {
          this.input.agenReceiverMarkItem = clone(
            this.input.receiver_representative_list[index]
          );
        } else if (name === "isModalListAssignRetaliate") {
          this.input.ListAssignRetaliate = clone(
            this.input.ListAssignRetaliate_test[index]
          );
        }
      }
    } else {
      // Is close

      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.receiverMarkItem = {};
      this.input.agenReceiverMarkItem = {};
      this.input.receiverMark = {};
      this.input.receiver_contact_address_list = {};
      this.input.ListAssignRetaliate = {};
    }
  }
  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
  }
  //! <<< Table >>>
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }
  changeStep(action: string): void {
    if (this.validateWizard()) {
      this.menuList[this.currentStep - 1].canedit = true;
      action === "next" ? this.currentStep++ : this.currentStep--;
      this.currentID = this.menuList[this.currentStep - 1].id;
      this.currentSubStep = 1;
      if (this.stepPass == this.currentStep - 1)
        this.stepPass = this.stepPass + 1;
      console.log(
        "currentID",
        this.currentID,
        "currentStep",
        this.currentStep,
        "stepPass",
        this.stepPass
      );
      this.calcProgressPercent(this.currentStep);

      console.log("Noliststep2", this.Noliststep2);
    }
  }

  backtomenu(canedit: any, number: any): void {
    console.log(canedit);
    if (canedit == true) {
      this.currentStep = number;
      this.currentID = this.menuList[this.currentStep - 1].id;

      for (var i = 1; i <= this.menuList.length + 1; i++) {
        if (i <= this.stepPass - 1) {
          this.menuList[i].canedit = true;
        }
      }
    }
  }

  calcProgressPercent(currentStep: number): void {
    this.progressPercent = Math.round(
      (currentStep / this.menuList.length) * 100
    );
  }

  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    this.input.CHECKEDITSTEP3 = "SHOW";
    console.log(this.input.CHECKEDITSTEP3);
    console.log("validate", this.validateSaveItemModal(nameItem));
    if (this.validateSaveItemModal(nameItem)) {
      console.log("indexEdit", this.input.indexEdit);
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        console.log("people_list", this[obj][nameList]);
        this.toggleModal(nameModal);
      }
    }
  }

  validateSaveItemModal(nameItem: any): boolean {
    if (nameItem === "ownerMarkItem") {
      let result = validateService(
        "validateEFormSave01ProcessStep3OwnerMarkItem",
        this.input.ownerMarkItem
      );
      this.validate = result.validate;
      return result.isValid;
    }
    return true;
  }

  contactTypeChange(): void {
    this.Next4 = true;
    console.log("DATARDIO", this.input.save021_contact_type_code);
    switch (this.input.save021_contact_type_code) {
      case "OWNER":
        this.listdropdownContact = this.input.people_list;
        this.ContactWord = "ใช้ที่อยู่ของเจ้าของลำดับที่";
        this.receiverContactChangeType = "OWNER";
        this.receiverContactChange("OWNER", 0);
        console.log("121111111111111111111111111111111");
        break;
      case "REPRESENTATIVE":
        this.listdropdownContact = this.input.representative_list;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนลำดับที่";
        this.receiverContactChangeType = "REPRESENTATIVE";
        console.log("22222222222222222222222222222222");
        break;
      case "REPRESENTATIVE_PERIOD":
        this.listdropdownContact = this.input.representative_list;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนช่วงลำดับที่";
        console.log("33333333333333333333333333333");
        this.receiverContactChangeType = "REPRESENTATIVE_PERIOD";
        break;

      default:
        this.listdropdownContact = null;
        break;
    }
  }

  receiverContactChange(contactType: string, index: number): void {
    console.log("index", index);
    console.log("contactType", contactType);
    if (contactType == "OWNER") {
      this.input.people_list.forEach((data) => {
        console.log("people_list", this.input.people_list);
        data.is_contact_person = false;
      });

      this.input.people_list[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE") {
      this.input.receiver_representative_list.forEach((data) => {
        data.is_contact_person = false;
      });

      this.input.representative_list[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE_PERIOD") {
      this.input.receiver_representative_list.forEach((data) => {
        data.is_contact_person = false;
      });

      this.input.representative_list[index].is_contact_person = true;
    }

    console.log("people_list", this.input.people_list);
    console.log("representative_list2", this.input.representative_list);
  }

  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(name: any, value: any, obj: any): void {
    clearTimeout(this.timeout);
    if (name === "address_sub_district_name") {
      this.validate[name] = null;
      this.clearValue(name, obj);
    }

    this.timeout = setTimeout(() => {
      if (value) {
        if (name === "address_sub_district_name") {
          this.callSubDistrict(
            {
              filter: { name: value },
              paging: {
                item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page,
              },
            },
            name
          );
        }
      } else {
        this.onClickOutsideAutocomplete(name);
      }
    }, CONSTANTS.DELAY_CALL_API);
  }
  onSelectAutocomplete(name: any, item: any, obj: any): void {
    if (name === "address_sub_district_name") {
      // ตำบล
      this.input[obj].address_sub_district_code = item.code;
      this.input[obj].address_sub_district_name = item.name;
      // อำเภอ
      this.input[obj].address_district_code = item.district_code;
      this.input[obj].address_district_name = item.district_name;
      // จังหวัด
      this.input[obj].address_province_code = item.province_code;
      this.input[obj].address_province_name = item.province_name;
      // รหัสไปรษณีย์
      this.input[obj].postal_code = item.postal_code;
    }
    this.onClickOutsideAutocomplete(name);
  }
  onClickOutsideAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = false;
    this.autocompleteList[name] = [];
  }
  clearValue(name: any, obj: any): void {
    if (name === "address_sub_district_name") {
      // อำเภอ
      this.input[obj].address_district_code = "";
      this.input[obj].address_district_name = "";
      // จังหวัด
      this.input[obj].address_province_code = "";
      this.input[obj].address_province_name = "";
      // รหัสไปรษณีย์
      this.input[obj].postal_code = "";
    }
  }

  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
    }

    if (nameList === "people_list" || nameList === "representative_list") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }

  //! <<< Other >>>
  displayAddress(value: any): any {
    return displayAddress(value);
  }
  onKey(e: any, name: any): void {
    if (e.keyCode === 13) {
      if (name === "request_number") {
        this.searchRequestNumber();
      }
    } else {
      if (name === "request_number") {
        this.input.registration_number = "";
        this.input.people_list = [];
        this.input.representative_list = [];
        this.input.product_list = [];
      }
    }
  }
}
