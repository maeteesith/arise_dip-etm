import { Component, OnInit } from "@angular/core";
import { ROUTE_PATH } from "../../helpers";

@Component({
  selector: "app-main-layout-eform",
  templateUrl: "./main-layout-eform.component.html",
  styleUrls: ["./main-layout-eform.component.scss"]
})
export class MainLayoutEFormComponent implements OnInit {
  //TODO >>> Declarations <<<
  public pathHome: any;

  constructor() {}

  ngOnInit() {
    this.pathHome = ROUTE_PATH.EFORM_HOME.LINK;
  }
}
