import { Component, OnInit } from '@angular/core';
import { GlobalService } from 'src/app/global.service';
import { Help } from 'src/app/helpers/help';
import { AutomateTest } from 'src/app/test/automate_test';
import { ActivatedRoute } from '@angular/router';
import { ForkJoinService } from 'src/app/services/fork-join2.service';

@Component({
  selector: 'app-madrid-role02-examination',
  templateUrl: './madrid-role02-examination.component.html',
  styleUrls: ['./madrid-role02-examination.component.scss', './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole02ExaminationComponent implements OnInit {

  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  
  public modal: any

constructor(
  private help: Help,
  private automateTest: AutomateTest,
  private route: ActivatedRoute,
 
  private global: GlobalService,
  private forkJoinService: ForkJoinService,
  // private DocumentProcessService: DocumentProcessService,
  
) { }
//MR2-9-5
  ngOnInit() {
  }
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
}

//! <<< Table >>>
onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
        this.input.is_check_all = !this.input.is_check_all
        this[name].forEach((item: any) => {
            item.is_check = this.input.is_check_all
        })
    } else {
        item.is_check = !item.is_check
    }
}
onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
}
toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
}

//! <<<< Pagination >>>
managePaginateCallApi(name: string): void {
    if (name === 'paginateDocumentRole02Check') {
        //this.onClickDocumentRole02CheckList()
        this.input.is_check_all = false
    }
}
onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
        this.managePaginateCallApi(name)
    }
}
onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
}
changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
}
getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
}

//! <<< Modal >>>
toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
}
}
