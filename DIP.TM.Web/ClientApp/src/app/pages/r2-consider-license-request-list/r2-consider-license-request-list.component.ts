import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-r2-consider-license-request-list',
  templateUrl: './r2-consider-license-request-list.component.html',
  styleUrls: ['./r2-consider-license-request-list.component.scss']
})
export class R2ConsiderLicenseRequestListComponent implements OnInit {

  constructor() { }



  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public master: any;

  public pathChange: any;
  public tableList: any;

  ngOnInit() {
    this.input = {};
    this.validate = {};
    this.master = {
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList2: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList3: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
    };

    this.tableList = {
      column_list: [
        {
          request_id: "741150971",
          consider_date: "",
          petition: "ก.05",
          reference_number: "",
          contract_date: "",
          person: "นางนิจรินทร์ กุหลาบป่า",
          owner: "นายโรจน์ศักดิ์ ตั้งกมลสุข",
          status: "waiting"
        },
        {
          request_id: "712224642",
          consider_date: "",
          petition: "ก.05",
          reference_number: "387",
          contract_date: "29/02/2562",
          person: "นางนิจรินทร์ กุหลาบป่า",
          owner: "นายโรจน์ศักดิ์ ตั้งกมลสุข",
          status: "waiting-fee"
        },
        {
          request_id: "790849099",
          consider_date: "",
          petition: "ก.08",
          reference_number: "386",
          contract_date: "29/02/2562",
          person: "นางนิจรินทร์ กุหลาบป่า",
          owner: "นายโรจน์ศักดิ์ ตั้งกมลสุข",
          status: "waiting"
        },
        {
          request_id: "857583043",
          consider_date: "10/09/2562",
          petition: "ก.08",
          reference_number: "384",
          contract_date: "29/02/2562",
          person: "ซา ซา ด็อท คอม ลิมิเต็ด",
          owner: "นายวรเศรษฐ์ โภชนกุล",
          status: "done"
        },
        {
          request_id: "962023340",
          consider_date: "10/09/2562",
          petition: "ก.05",
          reference_number: "383",
          contract_date: "29/02/2562",
          person: "ซา ซา ด็อท คอม ลิมิเต็ด",
          owner: "นายวรเศรษฐ์ โภชนกุล",
          status: "cancel"
        },
        {
          request_id: "345755881",
          consider_date: "10/09/2562",
          petition: "ก.08",
          reference_number: "382",
          contract_date: "29/02/2562",
          person: "นางนิจรินทร์ กุหลาบป่า",
          owner: "นางสาวมยุรี เลิศคัมภีร์ศีล",
          status: "done"
        },
      ],
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>
  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }

}
