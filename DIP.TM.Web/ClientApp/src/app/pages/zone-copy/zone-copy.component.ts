import { Component, OnInit } from "@angular/core";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem,
} from "@angular/cdk/drag-drop";
import { CONSTANTS, clone } from "../../helpers";

@Component({
  selector: "app-zone-copy",
  templateUrl: "./zone-copy.component.html",
  styleUrls: ["./zone-copy.component.scss"],
})
export class ZoneCopyComponent implements OnInit {
  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public list: any;
  public tab: any;
  public master: any;
  public modal: any;
  // Autocomplete
  public autocompleteList: any;
  public isShowAutocomplete: any;
  // Paginate
  public paginateTranslation: any;
  // Other
  public timeout: any;

  constructor() {}

  ngOnInit() {
    // Init
    this.input = {
      type_code_list: [],
      dimension: "2D",
      audio: {
        sound: null,
        palyer: null,
        action: false,
      },
    };
    this.validate = {};
    this.list = {
      leftList: [
        ["1A", "2A", "3A"],
        ["4A", "5A", "6A"],
        ["7A", "8A", "9A"],
      ],
      rightList: [
        ["1B", "2B", "3B"],
        ["4B", "5B", "6B"],
        ["7B", "8B", "9B"],
      ],
      multipleSearchList: [],
      timeline: [
        {
          label: "10/10/2020",
          isSelected: false,
        },
        {
          label: "15/10/2020",
          isSelected: false,
        },
        {
          label: "20/10/2020",
          isSelected: true,
        },
        {
          label: "25/10/2020",
          isSelected: false,
        },
        {
          label: "30/10/2020",
          isSelected: false,
        },
        {
          label: "31/10/2020",
          isSelected: false,
        },
      ],
    };
    this.tab = {
      registrar: [
        { label: "บันทึกเสนอนายทะเบียน", status: "done", isSelected: true },
        { label: "พิจารณาเอกสาร", status: "edit", isSelected: false },
        { label: "ออกหนังสือ", status: "edit", isSelected: false },
      ],
    };
    this.master = {
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
    };
    this.modal = {
      isModalTestOpen: false,
    };
    // Autocomplete
    this.autocompleteList = {
      address: [],
    };
    this.isShowAutocomplete = {
      address: false,
    };
    // Paginate
    this.paginateTranslation = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateTranslation.id = "paginateTranslation";
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: string, name: string, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: string, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Binding List (one way) >>>
  bindingInList(item: any, name: string, value: any): void {
    item[name] = value;
  }
  toggleBooleanInList(item: any, name: string): void {
    item[name] = !item[name];
  }
  onCheckAllInList(obj: string, name: string): void {
    this.input.is_check_all = !this.input.is_check_all;
    this[obj][name].forEach((item: any) => {
      item.is_check = this.input.is_check_all;
    });
  }
  onRemoveInList(
    obj: string,
    name: string,
    index: number,
    paginName?: string
  ): void {
    this[obj][name].splice(index, 1);
    if (paginName) {
      this.changePaginateTotal(this[obj][name].length, paginName);
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
    } else {
      // Is close
      this.modal[name] = false;
    }
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === "paginateTranslation") {
      this.input.is_check_all = false;
      //TODO call api
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Autocomplete >>>
  callApiAutocomplete(params: any, name: string): void {
    // this.apiService.SearchAutocomplete(params).subscribe((data: any) => {
    //   if (data) {
    //     this.autocompleteList[name] = data;
    //     this.isShowAutocomplete[name] = true;
    //   } else {
    //     this.isShowAutocomplete[name] = false;
    //     this.autocompleteList[name] = [];
    //   }
    // });
    this.autocompleteList[name] = [
      { name: "test1" },
      { name: "test2" },
      { name: "test3" },
    ];
    this.isShowAutocomplete[name] = true;
  }
  onChangeAutocomplete(obj: string, name: string): void {
    clearTimeout(this.timeout);
    this.clearValidate(name);
    this.timeout = setTimeout(() => {
      if (this[obj][name]) {
        if (name === "address") {
          //TODO call api
          this.callApiAutocomplete({ filter: { name: this[obj][name] } }, name);
        }
      } else {
        this.onClickOutsideAutocomplete(name);
      }
    }, CONSTANTS.DELAY_CALL_API);
  }
  onSelectAutocomplete(name: string, item: any): void {
    if (name === "address") {
      //TODO set value
      this.input.address = item.name;
    }
    this.onClickOutsideAutocomplete(name);
  }
  onClickOutsideAutocomplete(name: string): void {
    this.isShowAutocomplete[name] = false;
    this.autocompleteList[name] = [];
  }

  //! <<< Drag&Drop >>>
  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  //! <<< Select Multiple >>>
  onKey(e: any, name: string): void {
    if (e.keyCode === 13) {
      if (name === "search") {
        this.list.multipleSearchList.push(this.input.search);
        this.input.search = "";
      }
    }
  }

  //! <<< Timeline >>>
  manageTimelineCallApi(): void {
    //TODO call api
  }
  onClickTimeline(item: any): void {
    this.clearSelectedTimeline();
    item.isSelected = !item.isSelected;
    this.manageTimelineCallApi();
  }
  onClickNextTimeline(): void {
    let index = this.getCurrentIndexTimeline();
    if (index < this.list.timeline.length - 1) {
      this.clearSelectedTimeline();
      this.list.timeline[index + 1].isSelected = true;
      this.manageTimelineCallApi();
    }
  }
  onClickPreviousTimeline(): void {
    let index = this.getCurrentIndexTimeline();
    if (index > 0) {
      this.clearSelectedTimeline();
      this.list.timeline[index - 1].isSelected = true;
      this.manageTimelineCallApi();
    }
  }
  getCurrentIndexTimeline(): number {
    let index = 0;
    if (this.list.timeline) {
      index = this.list.timeline.findIndex((item: any) => {
        return item.isSelected;
      });
    }
    return index;
  }
  clearSelectedTimeline(): void {
    this.list.timeline.forEach((item: any) => {
      item.isSelected = false;
    });
  }
  getSizeTimeline(): number {
    let size = 0;
    if (this.list.timeline) {
      size = this.list.timeline.length;
    }
    return size;
  }

  //! <<< Profile >>>
  togglePlayer(obj: string, name: string): void {
    this[obj][name].action = !this[obj][name].action;
    if (!this[obj][name].palyer) {
      this[obj][name].palyer = new Audio(this[obj][name].sound);
    }
    this[obj][name].action
      ? this[obj][name].palyer.play()
      : this[obj][name].palyer.pause();
  }

  //! <<< Tab >>>
  onChangeTab(obj: string, name: string, item: any): void {
    this[obj][name].forEach((item: any) => {
      item.isSelected = false;
    });
    item.isSelected = true;
  }
  getIsActiveTab(obj: string, name: string, index: number): boolean {
    if (this[obj][name]) {
      return this[obj][name][index].isSelected;
    }
    return false;
  }
  getStatusTabStyle(code: any): string {
    switch (code) {
      case "edit":
        return "status edit";
      case "done":
        return "status done";
      default:
        return "status edit";
    }
  }
}
