import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { Router } from "@angular/router";

import {
  ROUTE_PATH,
  CONSTANTS,
  validateService,
  clone,
  getItemCalculator,
  displayMoney,
  displayFormatBytes,
  PapaParseCsvToJson,
  loopDisplayDateServer,
  displayAddress,
  viewPDF,

} from "../../helpers";
import { MatDialog } from '@angular/material';
import { ModalAttachFileArrayComponent } from '../modal-attach-file-array/modal-attach-file-array.component';

@Component({
  selector: 'app-madrid-role06-ni06-list',
  templateUrl: './madrid-role06-ni06-list.component.html',
  styleUrls: ['./madrid-role06-ni06-list.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole06Ni06ListComponent implements OnInit {

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,
    public dialog: MatDialog

  ) { }

  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any
  public role: any


  public modal: any

  public listPublicItem: any = [];
  public doc: any = [];
  public Reply: any = [];


  public selectedDoc: any = -1;
  public selectedReply: any = 1;

  ngOnInit() {
    this.role = true;
    this.listPublicItem = [
      { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "TH1799919199", requestNo: "4441123213123", requestNo2: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", statusprogress: "รอการบันทึก", command: "" },
      { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "TH1799919199", requestNo: "7411509744441", requestNo2: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", statusprogress: "เสร็จสิ้น", command: "" }
    ]

    this.doc = [{ value: "1", text: "MMC" }, { value: "2", text: "MMU" }]
    this.Reply = [{ value: "0", text: "เลือก" }, { value: "1", text: "ตอบรับ" }, { value: "2", text: "ตีกลับ" }, { value: "3", text: "ไม่สมบูรณ์" },]


    this.validate = {}
    this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      public_type_code: '',
      public_source_code: '',
      public_receiver_by: '',
      public_status_code: '',
      request_number: '',
    }
    this.master = {
      request_type: [{ code: 1, name: '1' }],
      job_reciver: [{ id: 1, name: "Sam" }, { id: 2, name: "Tom" }]
    }

    this.open();
  }

  open() {
    const dialogRef = this.dialog.open(ModalAttachFileArrayComponent, {
      width: "calc(100% - 80px)",
      maxWidth: "680px",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });
  }

}
