import { Component, OnInit } from '@angular/core'
import { GlobalService } from '../../global.service'
import { ReceiptProcessService } from '../../services/receipt-process.service'
import { ForkJoinService } from '../../services/fork-join.service'
import {
  CONSTANTS,
  validateService
} from '../../helpers'

@Component({
  selector: "app-daily-fee-income-records",
  templateUrl: "./daily-fee-income-records.component.html",
  styleUrls: ["./daily-fee-income-records.component.scss"]
})
export class DailyFeeIncomeRecordsComponent implements OnInit {
  //TODO >>> Declarations <<<
  // Init
  public input: any
  public validate: any
  public master: any

  constructor(
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private receiptProcessService: ReceiptProcessService
  ) { }

  ngOnInit() {
    //TODO >>> Init value <<<
    // Init
    this.input = {
      reference_number: '',
    }
    this.validate = {}
    this.master = {
      receiptChannelTypeList: []
    }

    //TODO >>> Call service <<<
    this.callInit()
  }

  //! <<< Call Init API >>>
  callInit(): void {
    // Open loading
    this.global.setLoading(true)
    // Call API
    this.forkJoinService.initDailyFeeIncomeRecordsPage().subscribe((data: any) => {
      if (data) {
        this.master = data
      }
      // Close loading
      this.global.setLoading(false)
    })
  }

  //! <<< Call API >>>
  callLoad(reference_number: any, params: any): void {
    this.receiptProcessService.LoadUnpaid(reference_number, params).subscribe((data: any) => {
      if (data) {
        // Manage structure

        // Set value
      }
      // Close loading
      this.global.setLoading(false)
    })
  }

  //! <<< Prepare Call API >>>
  onClickSearchReceipt(): void {
    if (this.validateSearchReceipt()) {
      // Open loading
      this.global.setLoading(true)
      // Set param
      let reference_number = this.input.reference_number
      // Call api
      this.callLoad(reference_number, {})
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }
  validateSearchReceipt(): boolean {
    let result = validateService('validateSearchReceipt', this.input)
    this.validate = result.validate
    return result.isValid
  }

  //! <<< Event >>>

  //! <<< Table >>>
  onClickCheckBoxTable(name: string, index: number, isAll: boolean): void {
    if (isAll) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      this[name].forEach((item: any, i: any) => {
        if (index === i) {
          item.is_check = !item.is_check
        }
      })
    }
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
}
