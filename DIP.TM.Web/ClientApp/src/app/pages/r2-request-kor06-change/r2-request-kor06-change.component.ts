import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-r2-request-kor06-change',
  templateUrl: './r2-request-kor06-change.component.html',
  styleUrls: ['./r2-request-kor06-change.component.scss']
})
export class R2RequestKor06ChangeComponent implements OnInit {

  constructor() { }


  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public master: any;

  public pathChange: any;

  public tab_menu_show_index: any
  public tab_menu_show_index2: any

  public tableList: any;
  public tableRequestDetail: any;
  public tableViewDocument: any;
  public tableInfo: any;
  public tableIssueBook: any;
  public tableAgent: any;
  public tableLicense: any;
  public tableProduct: any;
  public tableDetail: any;
  public modal: any;

  // Paginate
  public paginateTranslation: any;

  ngOnInit() {
    this.input = {};
    this.validate = {};
    this.master = {
      checkList: [
        { code: "1", name: "หนังสือแจ้งคำสั่ง" },
        { code: "2", name: "การปฏิบัติตามคำสั่ง" },
        { code: "3", name: "การชำระค่าธรรมเนียม" },
        { code: "4", name: "การแจ้งฟ้องคดี / คำพิพากษา" },
        { code: "5", name: "สถานที่ติดต่อ" },
        { code: "6", name: "ชื่อคู่กรณี (ถ้ามี)" },
        { code: "7", name: "การขอถอน" },
        { code: "8", name: "ใบตอบรับ / ครบกำหนด" },
        { code: "9", name: "มีอุทธรณ์ / คำวินิจฉัยฯ" },
      ],
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList2: [
        { code: "1", name: "รอบที่ 1" },
        { code: "2", name: "รอบที่ 2" },
      ],
      typeCodeList3: [
        { code: "1", name: "อนุญาต" },
        { code: "2", name: "รวมเรื่อง" },
        { code: "3", name: "ไม่อนุญาต" },
      ],
      typeCodeList4: [
        { code: "1", name: "ยังไม่หลุด" },
        { code: "2", name: "ยังไม่หลุด" },
        { code: "3", name: "ยังไม่หลุด" },
      ],
      typeCodeList5: [
        { code: "1", name: "เจ้าของ" },
        { code: "2", name: "ตัวแทน" },
        { code: "3", name: "ตัวแทนช่วง" },
        { code: "4", name: "อื่นๆ" },
        { code: "5", name: "ตามก.ที่ยื่น" },
      ],
      typeCodeList6: [
        { code: "1", name: "ต.ค.5 ข้อ 8" },
        { code: "2", name: "ต.ค.9 ข้อ 10" },
        { code: "3", name: "ต.ค.9 ข้อ 2" },
        { code: "4", name: "ต.ค.9 ข้อ 3" },
      ],
      typeCodeList7: [
        { code: "1", name: "แก้ไขรายละเอียดตัวแทน" },
        { code: "2", name: "แต่งตั้งตัวแทนใหม่" },
        { code: "3", name: "แต่งตั้งตัวแทน (เพิ่ม)" },
        { code: "4", name: "ยกเลิกตัวแทนบางส่วน" },
        { code: "5", name: "ยกเลิกตัวแทนทั้งหมด" },
      ],
      typeCodeList8: [
        { code: "1", name: "มีสิทธิ" },
        { code: "2", name: "ไม่มีสิทธิ" },
      ],
      typeCodeList9: [
        { code: "1", name: "ได้" },
        { code: "2", name: "ไม่ได้" },
      ],
    }

    this.tab_menu_show_index = 5

    this.tab_menu_show_index2 = 1

    this.tableList = {
      column_list: [
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.06",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting"
        },
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.07",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting"
        },
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.20",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting"
        },
      ],
    }

    this.tableRequestDetail = {
      head_column_list: [
        {
          order: "#",
          name: "ชื่อเจ้าของ",
          address: "ที่อยู่",
          tel: "โทรศัพท์",
          fax: "โทรสาร",
          name_old: "ชื่อเจ้าของเดิม",
          operation: "คำสั่ง",
        },
      ],

      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          fax: "-",
          name_old: "ฮันส์โกรส์ เอสอี",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          fax: "-",
          name_old: "ฮันส์โกรส์ เอสอี",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
          fax: "-",
          name_old: "-",
        },
      ]
    }

    this.tableViewDocument = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }

    this.tableInfo = {
      column_list: [
        {
          detail: "ม.7"
        },
        {
          detail: "ม.9"
        },
        {
          detail: "ม.7"
        },
      ]
    }

    this.tableIssueBook = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }

    this.tableAgent = {
      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
        },
      ]
    }

    this.tableLicense = {
      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          editor: "ชื่อผู้ได้รับอนุญาต",
          name_old: "-"
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          editor: "ที่อยู่ผู้ได้รับอนุญาต",
          name_old: "-"
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
          editor: "รายละเอียดสัญญา",
          name_old: "-"
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
          editor: "รายละเอียดสัญญา",
          name_old: "นายพงศกร อาจหาญ"
        },
      ]
    }

    this.tableProduct = {
      head_column_list: [
        {
          order: "#",
          type: "จำพวกสินค้า",
          description: "รายการสินค้า/บริการ"
        },
      ],

      column_list: [
        {
          type: "21"
        },
        {
          type: "3"
        },
      ]
    }

    this.tableDetail = {
      column_list: [
        {
          data: "ทั้งนี้ ให้ดำเนินการภายใน 30 วัน นับแต่วันที่ได้รับหนังสือฉบับนี้ มิฉะนั้น จะพิจารณาคำขอจดทะเบียนนี้ตามหลักฐานและข้อเท็จจริงที่มีอยู่",
        },
        {
          data: "ให้นำส่งต้นฉบับหนังสือสำคัญแสดงการจดทะเบียนเครื่องหมายการค้าประกอบการยื่น คำขอโอน ก.04 ลงวันที่.......ตามกฏกระทรวงข้อ 32",
        },
        {
          data: "ให้นำส่งต้นฉบับหนังสือสำคัญแสดงการจดทะเบียนเครื่องหมายการค้าประกอบการยื่น คำขอเปลี่ยนแปลง ก.06 ลงวันที่.......ตามกฏกระทรวงข้อ 35",
        },
        {
          data: "ให้นำส่งต้นฉบับหนังสือสำคัญแสดงการจดทะเบียนเครื่องหมายการค้าประกอบการยื่น คำขอโอน ก.04 ลงวันที่.......ตามกฏกระทรวงข้อ 32",
        },
        {
          data: "ให้นำส่งต้นฉบับหนังสือสำคัญแสดงการจดทะเบียนเครื่องหมายการค้าประกอบการนื่น คำขอแก้ไขเปลี่ยนแปลง ก.06 ลงวันที่.......ตามกฏกระทรวงข้อ 35",
        },
        {
          data: "ให้ยื่นโอนคำขอ......เนื่องจากเป็นเครื่องหมายชุด",
        },
        {
          data: "ให้นาย......ผู้โอนมาลงลายมือชื่อในสัญญาโอนต่อหน้านายทะเบียน เนื่องจากลายมือชื่อ ได้เปลี่ยนไปจากเดิม",
        },
        {
          data: "โปรตจำหน่าย / ปลดปล่อยค่าขอ (ควบคุมเวลา) ",
        },
        {
          data: "โปรดพิจารณาเอกสาร (ชี้แจงเรื่องการถอนคำขอ / ฟื้นค่าขอ) ตามหนังสือ....",
        },
        {
          data: "โปรดพิจารณาเอกสาร (ชี้แจงทั่วไป) ตามหนังสือ...",
        },
      ],
    }

    this.modal = {
      isModalAlertOpen: false,
      isModalEditOpen: false,
      isModalExampleOpen: false,
    };

  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>
  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
