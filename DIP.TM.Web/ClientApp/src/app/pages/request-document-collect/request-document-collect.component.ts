import { Component, OnInit, ɵɵNgOnChangesFeature, SimpleChange } from "@angular/core"
import { ActivatedRoute } from "@angular/router"
import { fabric } from "fabric"
import { DomSanitizer } from "@angular/platform-browser"
import { GlobalService } from "../../global.service"
import { ForkJoinService } from "../../services/fork-join2.service"
import { RequestDocumentCollectService } from "../../services/request-document-collect-buffer.service"
import { UploadService } from "../../services/upload.service"
import { ScannerService } from '../../services/scanner.service'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  displayMoney,
  displayDateServer,
  getMoment,
  clone
} from "../../helpers"
import { Scanner } from "typescript"

@Component({
  selector: "app-request-document-collect",
  templateUrl: "./request-document-collect.component.html",
  styleUrls: ["./request-document-collect.component.scss"]
})
export class RequestDocumentCollectComponent implements OnInit {
  //TODO >>> Declarations <<<
  // Init
  public editID: any
  public input: any
  public validate: any
  public master: any
  public response: any
  public modal: any
  // List Document
  public listDocument: any[]
  public paginateDocument: any
  public perPageDocument: number[]
  // List RequestDocumentCollect
  public listRequestDocumentCollect: any[]
  public paginateRequestDocumentCollect: any
  public perPageRequestDocumentCollect: number[]
  //label_save_combobox || label_save_radio
  public requestDocumentCollectTypeCodeList: any[]
  public requestDocumentCollectTypeCodeListNoAll: any[]
  // Other
  public canvasStage: any
  public canvas: any
  public canvas_img: any
  public canvas_el: any
  public canvas_rectangle: any
  public canvas_is_erase: any
  public canvas_is_crop: any
  public rowImageEdit: any
  //public canvasImg: any
  //public idNo : number
  // pointer temp
  public tempLeft: number
  public tempTop: number
  public tempWidth: number
  public tempHeight: number
  public tempScale: number
  public tempScaleWidth: number
  public tempScaleHeight: number


  constructor(
    private help: Help,
    public sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private RequestDocumentCollectService: RequestDocumentCollectService,
    private uploadService: UploadService,
    private scannerService: ScannerService,
  ) { }

  ngOnInit() {
    // Init
    this.editID = this.route.snapshot.paramMap.get("id")
    this.input = {
      id: null,
      request_number: "",
      request_document_collect_type_code: "",
      //request_date: getMoment(),
      imageUpload: {
        file: {},
        blob: null
      }
    }
    this.validate = {}
    this.master = {
      requestDocumentCollectTypeCodeList: []
    }
    this.modal = {
      isModalImageEditOpen: false
    }
    // List Document
    this.listDocument = []
    this.paginateDocument = CONSTANTS.PAGINATION.INIT
    this.perPageDocument = CONSTANTS.PAGINATION.PER_PAGE
    // List RequestDocumentCollect
    this.listRequestDocumentCollect = []
    this.paginateRequestDocumentCollect = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateRequestDocumentCollect.id = 'paginatePublicItem'
    this.perPageRequestDocumentCollect = clone(CONSTANTS.PAGINATION.PER_PAGE)
    // Other
    //this.canvas.idNo = 0
    //this.objList = []
    //this.canvas.on('path:created', this.pathCreate)

    //this.RequestDocumentCollectService.Test().subscribe((data: any) => {
    //  console.log("test", data)
    //})

    //TODO >>> Call service <<<
    this.callInit()
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initRequestDocumentCollect().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.requestDocumentCollectTypeCodeListNoAll = this.master.requestDocumentCollectTypeCodeList.map(
          r => r
        )
        this.master.requestDocumentCollectTypeCodeList.unshift({
          code: "",
          name: "ทั้งหมด"
        })
        this.input.request_document_collect_type_code = ""
        //this.input.request_document_collect_type_code = "TRADEMARK_2D"
      }
      if (this.editID) {
        this.input.request_number = this.editID
        this.input.request_date = ""
        this.input.file_id = ["0", 3]
        //this.RequestDocumentCollectService.Load(this.editID).subscribe(
        //  (data: any) => {
        //    if (data) {
        //      // Manage structure
        //      Object.keys(data).forEach((item: any) => {
        //        this.input[item] = data[item]
        //      })

        //      this.listDocument = data.document_list || []
        //      data.document_list.map((item: any) => {
        //        item.is_check = false
        //        return item
        //      })
        //      this.changePaginateTotal(
        //        (this.listDocument || []).length,
        //        "paginateDocument"
        //      )

        //      // Set value
        //      // this.requestList = data.item_list
        //      // this.response.request01Load = data
        //      // this.isHasRequestList = true
        //    }
        //    // Close loading
        //    this.global.setLoading(false)
        //  }
        //)
      }

      this.global.setLoading(false)
      this.onClickList()
    })
  }

  //! <<< Call API >>>
  callList(params: any): void {
    this.RequestDocumentCollectService.List(this.help.GetFilterParams(params, this.paginateRequestDocumentCollect)).subscribe((data: any) => {
      // if(isValidListResponse(res)) {
      if (data) {
        this.listRequestDocumentCollect = data.list || []
        this.help.PageSet(data, this.paginateRequestDocumentCollect)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  callRequestDocumentCollectFileScan(row_item: any): void {
    //this.rowImageEdit = row_item
    //var uid = 10052 //Math.random().toString(36).substr(2, 9)
    //this.callIntervalFileLoad(uid)
    //return

    this.global.setLoading(true)
    this.scannerService.scanAndWait(row_item.request_document_collect_type_file_extension).subscribe((data: any) => {
      // if(isValidRequestDocumentCollectFileScanResponse(res)) {
      console.log(data)
      if (data && data.data) {
        // Set value
        //console.log(row_item)
        console.log(data)
        row_item.file_id = data.data.id
        console.log(row_item)
        this.callRequestDocumentCollectItemSave(row_item)
        //this.listData(data)
        //this.automateTest.test(this, { list: 1 })
      } else {
        this.global.setLoading(false)
      }
      //this.global.setLoading(false)
      // }
      // Close loading
    }, err => {
      this.global.setLoading(false)
      //this.callIntervalFileLoad(uid)
      //this.global.setLoading(false)
      //console.log(err)
    })
  }
  callIntervalFileLoad(uid: any): void {
    var is_run = false
    var pThis = this
    this.global.setLoading(true)
    var interval = setInterval(function () {
      if (!is_run) {
        is_run = true
        pThis.RequestDocumentCollectService.FileLoad(uid).subscribe(
          (data: any) => {
            pThis.ImageEdit(data.physical_path)
            console.log(data)
          }
        )
      }
    }, 1000)
  }
  callUpload(params: any): void {
    console.log(params)
    const formData = new FormData()
    formData.append("file", params.file, params.file.name || "img.jpg")

    this.modal["isModalImageEditOpen"] = false
    this.global.setLoading(true)
    this.uploadService.upload(formData).subscribe((data: any) => {
      //this.global.setLoading(false)
      //this.modal["isModalImageEditOpen"] = false

      if (data) {
        this.rowImageEdit.file_id = data.id
        this.callRequestDocumentCollectItemSave(this.rowImageEdit)
      }
    })
  }
  callRequestDocumentCollectItemSave(row_item: any): void {
    console.log(row_item)
    var params = {
      id: row_item.id,
      rd_id: row_item.rd_id,
      save_id: row_item.save_id,
      request_document_collect_type_code: row_item.request_document_collect_type_code,
      file_id: row_item.file_id
    }

    console.log(row_item)

    this.global.setLoading(true)
    this.RequestDocumentCollectService.RequestDocumentCollectItemSave(
      params
    ).subscribe((data: any) => {
      this.global.setLoading(false)
      this.onClickList()
    })
  }

  //! <<< Prepare Call API >>>
  onClickList(): void {
    // if(this.validateList()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    //const params = {
    //  request_number: this.input.request_number,
    //  request_document_collect_type_code: this.input
    //    .request_document_collect_type_code,
    //  request_date: this.input.request_date,
    //  document_list: this.listDocument || []
    //}

    //const params = {
    //    page_index: +this.paginateDocument.currentPage,
    //    item_per_page: +this.paginateDocument.itemsPerPage,
    //    order_by: 'created_date',
    //    is_order_reverse: false,

    //    search_by: [{
    //        key: 'request_date',
    //        value: displayDateServer(this.input.request_date),
    //        operation: 0
    //    }, {
    //        key: 'request_number',
    //        value: this.input.request_number,
    //        operation: 0
    //    }, {
    //        key: 'request_document_collect_type_code',
    //        value: this.input.request_document_collect_type_code,
    //        operation: 0
    //    }]
    //}
    const params = {
      request_date: this.input.request_date,
      request_number: this.input.request_number,
      request_document_collect_type_code: this.input.request_document_collect_type_code,
      file_id: this.input.file_id,
    }

    // Call api
    this.callList(params)
    // }
  }
  onClickRequestDocumentCollectFileScan(row_item: any): void {
    // if(this.validateRequestDocumentCollectFileScan()) {
    // Open loading
    // Call api
    this.callRequestDocumentCollectFileScan(row_item)
    // }
  }
  onClickRequestDocumentCollectFileOpen(row_item: any): void {
    window.open(row_item.url_path)
    // if(this.validateRequestDocumentCollectFileScan()) {
    // Open loading
    // Call api
    //this.callRequestDocumentCollectFileScan(row_item)
    // }
  }
  onClickSave01ModalAddressSave(): void {
    let saveData = this.canvas.toDataURL("image/jpeg")
    let params = {
      file: this.dataURItoBlob(saveData)
    }
    this.callUpload(params)
  }
  onClickRequestDocumentCollectFileUpload(row_item: any): void {
    this.rowImageEdit = row_item

    if (row_item.request_document_collect_type_file_extension != 'PDF') {
      //console.log(this.input.imageUpload)
      //console.log(this.input.imageUpload.blob)

      // TODO upload file process start
      this.rowImageEdit.file_id = 0
      this.rowImageEdit.physical_path = this.input.imageUpload.blob
      // TODO upload file process end

      this.ImageEdit(this.rowImageEdit.physical_path)
    } else {
      //console.log(row_item.file)
      let params = {
        file: row_item.file
      }
      this.callUpload(params)
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  //! <<< Event >>>
  getMouseCoordinate(evt: any): any {
    return {
      x: evt.pageX,
      y: evt.pageY
    }
  }
  onClickDocumentAdd(): void {
    this.listDocument.push({
      index: this.listDocument.length + 1,
      requst_number: null,
      request_document_collect_type_code: null,
      total_price: null,
      maker_name: null,
      request_date: null
    })
    this.changePaginateTotal(this.listDocument.length, "paginateDocument")
  }
  onChangeCheckboxDocument(object: any, object_name: string, value: any): void {
    object[object_name] = !object[object_name]
  }
  onChangeSelectDocument(object: any, object_name: string, value: any): void {
    if (object_name.indexOf("price") >= 0) {
      object[object_name] = parseFloat(value)
    } else {
      object[object_name] = value
    }
  }
  dataURItoBlob(dataURI: any): any {
    var byteString = atob(dataURI.split(",")[1])

    var mimeString = dataURI
      .split(",")[0]
      .split(":")[1]
      .split("")[0]

    var ab = new ArrayBuffer(byteString.length)
    var ia = new Uint8Array(ab)
    for (var i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i)
    }

    var bb = new Blob([ab], { type: mimeString })
    return bb
  }
  onChangeRequestDocumentCollectTypeCode(row_item: any): void {
    row_item.request_document_collect_type_name = this.requestDocumentCollectTypeCodeListNoAll.filter(
      r => r.code == row_item.request_document_collect_type_code
    )[0].name
  }
  // If check ยางลบ drawingMode = true
  onClickCheckBoxErase(value: boolean): void {
    if (this.canvas_is_crop) return
    this.canvas_is_erase = value

    //var pThis = this
    //setTimeout(() => {
    if (!value) {
      var allObjects = this.canvas.getObjects().slice()
      console.log(allObjects.length)
      var group = new fabric.Group(allObjects)
      this.canvas.clear().renderAll()
      this.canvas.add(group)
      this.canvas.setActiveObject(group)
      //this.canvas_img = this.canvas.getObjects()[0]
      //this.canvas_img.clipTo = (ctx) => {
      //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
      //}

      this.canvas.isDrawingMode = false
    } else {
      this.canvas.isDrawingMode = true
    }
    //}, 100)
  }
  refreshSizeImage(): void {
    //var pThis = this;

    //var allObjects = pThis.canvas.getObjects().slice()
    //var group = new fabric.Group(allObjects)
    //pThis.canvas.clear().renderAll()
    //pThis.canvas.add(group)
    //pThis.canvas.setActiveObject(group)
    //pThis.canvas_img = pThis.canvas.getObjects()[0]

    //pThis.canvas.renderAll();
  }
  onClickImageCrop(value: boolean): void {
    console.log(value)
    if (this.canvas_is_erase) return
    this.canvas_is_crop = value
    //var this = this;
    //var left;
    //var top;
    //var width;
    //var height;

    //var tCVW = this.canvas_img.width;
    //var tCVH = this.canvas_img.height;
    //this.refreshSizeImage();
    //var nW = tCVW / this.canvas_img.width;
    //var nH = tCVH / this.canvas_img.height;
    //this.tempScaleWidth *= nW;
    //this.tempScaleHeight *= nH;
    //console.log("changed W: " + tCVW + " to " + this.canvas_img.width + " Scale: " + nW + " TotalScale:" + this.tempScaleWidth);
    //console.log("changed H: " + tCVH + " to " + this.canvas_img.height + " Scale: " + nH + " TotalScale" + this.tempScaleHeight);

    //if ((tCVW != this.canvas_img.width || tCVH != this.canvas_img.height)) {
    //    if (this.tempScaleWidth < 1) {
    //        left = this.tempLeft / this.tempScaleWidth;
    //        width = this.tempWidth / this.tempScaleWidth;
    //    }
    //    else {
    //        left = this.tempLeft;
    //        width = this.tempWidth;
    //    }
    //    if (this.tempScaleHeight < 1) {
    //        top = this.tempTop / this.tempScaleHeight;
    //        height = this.tempHeight / this.tempScaleHeight;
    //    }
    //    else {
    //        top = this.tempTop;
    //        height = this.tempHeight;
    //    }
    //}
    //else {
    //    left = this.tempLeft;
    //    top = this.tempTop;
    //    width = this.tempWidth;
    //    height = this.tempHeight;
    //}
    //console.log("nL:" + left + ", nT:" + top + ", nW:" + width + ", nH:" + height);

    //this.canvas_rectangle = [left, top, width, height]
    //console.log(this.canvas_rectangle);
    //var allObjects = this.canvas.getObjects().slice()
    //var group = new fabric.Group(allObjects)
    //this.canvas.clear().renderAll()
    //this.canvas.add(group)
    //this.canvas.setActiveObject(group)
    //this.canvas_img = this.canvas.getObjects()[0]
    //this.canvas_img.clipTo = (ctx) => {
    //    ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
    //}
    //this.tempLeft = this.canvas_rectangle[0];
    //this.tempTop = this.canvas_rectangle[1];
    //this.tempWidth = this.canvas_rectangle[2];
    //this.tempHeight = this.canvas_rectangle[3];


    //window.scroll(0, 0)

    if (this.canvas_is_crop) {
      this.ImageEdit(this.canvas.toDataURL("image/jpeg"))
    } else {
      this.onClickImageCropConfirm(false)
    }
    //this.canvas.discardActiveObject().renderAll();
    //this.canvas_img.selectable = !value
  }
  onClickImageCropConfirm(is_confirm: boolean): void {
    console.log("onClickImageCropConfirm")

    if (this.canvas_el) {
      this.canvas.remove(this.canvas_el);
      this.canvas_el = null;
    }
    this.canvas_is_crop = false
    this.canvas_img.selectable = true

    if (is_confirm) {
      //let saveData = this.canvas.toDataURL("image/jpeg")
      //let params = {
      //    file: this.dataURItoBlob(saveData)
      //}
      //console.log(saveData)
      //    if (this.tempScaleWidth > 1) {
      //        this.canvas_rectangle[0] *= this.tempScaleWidth;
      //        this.canvas_rectangle[2] *= this.tempScaleWidth;
      //    }
      //    if (this.tempScaleHeight > 1) {
      //        this.canvas_rectangle[1] *= this.tempScaleHeight;
      //        this.canvas_rectangle[3] *= this.tempScaleHeight;
      //    }
      //setTimeout(() => {
      //this.canvas_img = this.canvas.getObjects()[0]
      //this.canvas_img.clipTo = (ctx) => {
      //  //  console.log("clipTo 2")
      //  //  this.canvas_img.clipTo = null

      //  //  //console.log(this.canvas_rectangle)
      //  //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
      //  //  //ctx.save();
      //ctx.drawImage(this, 100, 100, 100, 100, 100, 100, 100, 100)
      //  //  //var allObjects = this.canvas.getObjects().slice()
      //  //  //var group = new fabric.Group(allObjects)
      //  //  //this.canvas.clear().renderAll()
      //  //  //this.canvas.add(group)
      //  //  //this.canvas.setActiveObject(group)
      //  //  //this.canvas.isDrawingMode = false

      //var imgInstance = new fabric.Image(this.canvas_img, {
      //  left: 10,
      //  top: 10,
      //  width: 10,
      //  height: 10,
      //  clipTo: function (ctx) {
      //    ctx.rect(100, 100, 100, 100);
      //  }
      //});
      //this.canvas.add(imgInstance);

      //this.ImageEdit(this.canvas.toDataURL("image/jpeg"))


      const img = new Image()
      img.src = this.canvas.toDataURL("image/jpeg")
      this.canvas.clear()
      this.canvas.renderAll();

      img.onload = () => {
        var canvas_img = new fabric.Image(img, {
          left: -(this.canvas_rectangle[0] + this.canvas.width / 2),
          top: -(this.canvas_rectangle[1] + this.canvas.height / 2),
          width: this.canvas_rectangle[2] + this.canvas_rectangle[0] + this.canvas.width / 2,
          height: this.canvas_rectangle[3] + this.canvas_rectangle[1] + this.canvas.height / 2,
        })
        this.canvas.add(canvas_img)

        const new_img = new Image()
        new_img.src = this.canvas.toDataURL("image/jpeg")
        this.canvas.clear()
        this.canvas.renderAll();

        new_img.onload = () => {
          this.canvas_img = new fabric.Image(new_img, {
            left: this.canvas_rectangle[0] + this.canvas.width / 2,
            top: this.canvas_rectangle[1] + this.canvas.height / 2,
            width: this.canvas_rectangle[2],
            height: this.canvas_rectangle[3],
          })
          this.canvas.add(this.canvas_img)
          this.canvas.setActiveObject(this.canvas_img)

          //this.canvas_img.clipTo = (ctx) => {
          //  console.log("clipTo 2")
          //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
          //}

          this.canvas.renderAll();
        }
      }

      //}
      //console.log("d")
      //this.canvas.renderAll();
      //}, 100)
    } else {
      var left = this.tempLeft;
      var top = this.tempTop;
      var width = this.tempWidth;
      var height = this.tempHeight;
      this.canvas_rectangle = [left, top, width, height]
    }
    //if (is_confirm) {
    //    this.tempLeft = this.canvas_rectangle[0];
    //    this.tempTop = this.canvas_rectangle[1];
    //    this.tempWidth = this.canvas_rectangle[2];
    //    this.tempHeight = this.canvas_rectangle[3];
    //}
  }
  ImageEdit(physical_path: any): void {
    console.log("canvas - ImageEdit")

    //this.global.setLoading(false)
    this.modal["isModalImageEditOpen"] = true

    //var pThis = this
    //this.canvasStage = {
    //    erase: false
    //}
    //// Clear canvas before init
    if (this.canvas) {
      let canvasContainer = document.getElementById("canvas-container")
      while (canvasContainer.firstChild) {
        canvasContainer.removeChild(canvasContainer.firstChild)
      }
      let canvasStore = document.createElement("canvas")
      canvasStore.id = "canvas"
      canvasStore.width = 800
      canvasStore.height = 800
      document.getElementById("canvas-container").appendChild(canvasStore)
    }

    this.canvas = new fabric.Canvas("canvas", {
      isDrawingMode: false,
      freeDrawingBrush: "vline",
    })
    var canvas = this.canvas
    // Set eraser size and color
    canvas.freeDrawingBrush.width = 25
    canvas.freeDrawingBrush.color = "rgba(255,255,255,1)"
    // Add image into canvas
    const img = new Image()
    img.src = physical_path

    img.onload = () => {
      this.canvas_img = new fabric.Image(img, {
        left: 0,
        top: 0
      })

      console.log("img: " + img.width + " " + img.height)
      //    this.tempScale = 1;
      //    this.tempScaleWidth = 1;
      //    this.tempScaleHeight = 1;

      if (img.width > 800 || img.height > 800) {
        //Scale to width
        if (img.width > img.height) {
          this.canvas_img.scaleToWidth(800)
          this.tempScale = img.width / 800;
        } else {
          //Scale to height
          this.canvas_img.scaleToHeight(800)
          this.tempScale = img.height / 800;
        }

        this.canvas_img.scaleToWidth(img.width / this.tempScale);
        this.canvas_img.scaleToHeight(img.height / this.tempScale);

        canvas.add(this.canvas_img)

        this.ImageEdit(this.canvas.toDataURL("image/jpeg"))

        return
      }

      canvas.add(this.canvas_img)

      //    this.refreshSizeImage();

      var left = -this.canvas_img.width / 2;
      var top = -this.canvas_img.height / 2;
      var width = this.canvas_img.width;
      var height = this.canvas_img.height;
      this.canvas_rectangle = [left, top, width, height]
      this.tempLeft = this.canvas_rectangle[0];
      this.tempTop = this.canvas_rectangle[1];
      this.tempWidth = this.canvas_rectangle[2];
      this.tempHeight = this.canvas_rectangle[3];

      this.canvas_img.clipTo = (ctx) => {
        console.log("clipTo 1")
        ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
      }

      this.canvas_img.selectable = !this.canvas_is_crop
      //console.log(this.canvas_img.selectable)

      //    console.log("nL:" + left + ", nT:" + top + ", nW:" + width + ", nH:" + height);

      //    var allObjects = this.canvas.getObjects().slice()
      //    var group = new fabric.Group(allObjects)
      //    this.canvas.clear().renderAll()
      //    this.canvas.add(group)
      //    this.canvas.setActiveObject(group)
      //    this.canvas_img = this.canvas.getObjects()[0]
      //    this.canvas_img.clipTo = function (ctx) {
      //        ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
      //    }
      //    console.log(this.canvas_rectangle);
    }

    var mousedown_x, mousedown_y, mouseup_x, mouseup_y
    var crop = false
    ////var disabled = false
    canvas.on("mouse:down", (event) => {
      if (!this.canvas_is_crop) return

      var r = document.getElementById('canvas').getBoundingClientRect()
      mousedown_x = event.e.pageX - r.left - window.scrollX
      mousedown_y = event.e.pageY - r.top - window.scrollY
      console.log("mouse:down: " + mousedown_x + " " + mousedown_y)

      crop = true
      if (this.canvas_el) {
        canvas.remove(this.canvas_el);
        this.canvas_el = null;
      }

      //window.scroll(0, 0)
      //setTimeout(() => {
      //}, 500)
    })
    canvas.on("mouse:up", (event) => {
      if (!this.canvas_is_crop) return
      if (!crop) return
      crop = false

      var r = document.getElementById('canvas').getBoundingClientRect()
      mouseup_x = event.e.pageX - r.left - window.scrollX
      mouseup_y = event.e.pageY - r.top - window.scrollY
      console.log("mouse:up: " + mouseup_x + " " + mouseup_y)

      this.canvas_el = new fabric.Rect({
        fill: 'transparent',
        originX: 'left',
        originY: 'top',
        stroke: '#ccc',
        strokeDashArray: [2, 2],
        opacity: 1,
        width: 1,
        height: 1,
        selectable: false,
      })
      canvas.add(this.canvas_el)
      canvas.bringToFront(this.canvas_el)

      //console.log(mousex + " " + mousey)
      //console.log(event.e.pageX + " " + event.e.pageY)
      //console.log(pos)

      this.canvas_el.left = mousedown_x
      this.canvas_el.top = mousedown_y
      this.canvas_el.width = mouseup_x - mousedown_x
      this.canvas_el.height = mouseup_y - mousedown_y

      //console.log(this.canvas_img.left + " " + this.canvas_img.top)
      //console.log(this.canvas_el.left + " " + this.canvas_el.top + " " + this.canvas_el.width + " " + this.canvas_el.height)

      var left = this.canvas_el.left - this.canvas_img.width / 2 - this.canvas_img.left;
      var top = this.canvas_el.top - this.canvas_img.height / 2 - this.canvas_img.top;
      var width = this.canvas_el.width;
      var height = this.canvas_el.height;

      //var left = this.canvas_el.left - this.canvas_img.left;
      //var top = this.canvas_el.top - this.canvas_img.top;
      //var width = this.canvas_el.width;
      //var height = this.canvas_el.height;

      this.canvas_rectangle = [left, top, width, height]
      console.log("Select Area: " + this.canvas_rectangle);

      //this.canvas.renderAll();
    })
  }
  onClickRequestDocumentCollectFileRemove(row_item: any): void {
    //var rs = prompt("Do you want to delete", "Yes / No")
    //if (rs && rs != "") {
    var rs = rs || "test"
    var params = {
      rd_id: row_item.rd_id,
      cance_rason: rs,
    }

    this.global.setLoading(true)
    this.RequestDocumentCollectService.RequestDocumentCollectItemRemove(
      params
    ).subscribe((data: any) => {
      this.global.setLoading(false)
      this.onClickList()
    })
    //}
  }
  onClickRequestDocumentCollectFileAdd(row_item: any, i: any): void {
    var new_item = { ...row_item }
    new_item.rd_id = null
    new_item.request_document_collect_type_code = null
    new_item.file_id = null
    new_item.physical_path = null

    this.listRequestDocumentCollect.splice(i + 1, 0, new_item)
  }
  onClickRequestDocumentCollectFileEdit(row_item: any): void {
    this.rowImageEdit = row_item

    //this.rowImageEdit.file_id = 0
    //this.rowImageEdit.physical_path = row_item.physical_path


    this.canvas_is_erase = false
    this.canvas_is_crop = false

    this.ImageEdit(row_item.url_path)
  }

  // pathCreate(e: any): void {
  //     var your_path = e.path
  //     this.idNo += 1
  //     e.path.id = this.idNo
  // }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name]
  }

  //! <<< Upload (Image) >>>
  onClickUploadImage(event: any, obj: string, name: any, maxSize: any, item: any): void {
    if (event) {
      let file = event.target.files[0]
      event.target.value = ""
      // Check type file
      //if (file.type.includes("image/")) {
      // Has max size
      if (maxSize) {
        // Check size file
        if (file.size <= maxSize) {
          this.setFileData(file, obj, name, item)
        }
      } else {
        this.setFileData(file, obj, name, item)
      }
      //}
    }
  }
  setFileData(file: any, obj: string, name: any, item: any): void {
    this[obj][name].file = file
    let reader = new FileReader()
    reader.onload = e => {
      this[obj][name].blob = reader.result
      item.file = file
      this.onClickRequestDocumentCollectFileUpload(item)
    }
    reader.readAsDataURL(file)
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+ page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

  //! <<< Other >>>
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit()
    this.global.setLoading(false)
  }
}
