import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-document-role02-print-cover-list",
  templateUrl: "./document-role02-print-cover-list.component.html",
  styleUrls: ["./document-role02-print-cover-list.component.scss"]
})
export class DocumentRole02PrintCoverListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List DocumentRole02PrintCover
  public listDocumentRole02PrintCover: any[]
  public paginateDocumentRole02PrintCover: any
  public perPageDocumentRole02PrintCover: number[]
  // Response DocumentRole02PrintCover
  public responseDocumentRole02PrintCover: any
  public listDocumentRole02PrintCoverEdit: any


  // List DocumentProcess
  public listDocumentProcess: any[]
  public paginateDocumentProcess: any
  public perPageDocumentProcess: number[]
  // Response DocumentProcess
  public responseDocumentProcess: any

  //label_save_combobox || label_save_radio
  public departmentCodeList: any[]
  public documentRole02PrintCoverStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //document_role02_print_document_start_date: getMoment(),
      //document_role02_print_document_end_date: getMoment(),
      department_code: '',
      rule_count: '',
      document_role02_print_cover_status_code: '',
      request_number: '',
    }
    this.listDocumentRole02PrintCover = []
    this.paginateDocumentRole02PrintCover = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateDocumentRole02PrintCover.id = 'paginateDocumentRole02PrintCover'
    this.perPageDocumentRole02PrintCover = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listDocumentProcess = []

    //Master List
    this.master = {
      departmentCodeList: [],
      documentRole02PrintCoverStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initDocumentRole02PrintCoverList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.departmentCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.department_code = "ALL"
        this.master.documentRole02PrintCoverStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.document_role02_print_cover_status_code = "WAIT_COVER"

      }
      if (this.editID) {
        this.DocumentProcessService.DocumentRole02PrintCoverLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  onClickDocumentRole02PrintCoverList(): void {
    // if(this.validateDocumentRole02PrintCoverList()) {
    // Open loading
    // Call api
    this.callDocumentRole02PrintCoverList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callDocumentRole02PrintCoverList(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintCoverList(this.help.GetFilterParams(params, this.paginateDocumentRole02PrintCover)).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintCoverListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickDocumentRole02PrintCoverSend(): void {
    // if(this.validateDocumentRole02PrintCoverSend()) {
    // Open loading
    // Call api
    this.callDocumentRole02PrintCoverSend(this.listDocumentRole02PrintCover)
    // }
  }
  //! <<< Call API >>>
  callDocumentRole02PrintCoverSend(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintCoverSend(params).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintCoverSendResponse(res)) {
      if (data) {
        // Set value
        this.onClickDocumentRole02PrintCoverPrint()
        this.onClickDocumentRole02PrintCoverList()
        //this.listData(data)
        //this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onKeyPressDocumentRole02PrintPostNumberSend(row_item: any, key_press: any): void {
    // if(this.validateDocumentRole02PrintCoverSend()) {
    // Open loading
    // Call api
    if (key_press.key == "Enter" && row_item.post_number.length > 0) {
      this.callDocumentRole02PrintPostNumberSend(row_item)
    }
    // }
  }
  //! <<< Call API >>>
  callDocumentRole02PrintPostNumberSend(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintPostNumberSend(params).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintCoverSendResponse(res)) {
      if (data) {
        // Set value
        this.onClickDocumentRole02PrintCoverList()
        //this.listData(data)
        //this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickDocumentRole02PrintCoverAdd(): void {
    this.listDocumentRole02PrintCover.push({
      index: this.listDocumentRole02PrintCover.length + 1,
      request_number: null,
      document_role02_print_document_date_text: null,
      document_role02_receiver_by_name: null,
      rule_count: null,
      name: null,
      house_number: null,
      document_role02_print_post_number_date_text: null,
      document_role02_print_cover_date_text: null,
      post_number: null,
      document_role02_print_cover_status_name: null,

    })
    this.changePaginateTotal(this.listDocumentRole02PrintCover.length, 'paginateDocumentRole02PrintCover')
  }

  onClickDocumentRole02PrintCoverEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickDocumentRole02PrintCoverDelete(item: any): void {
    // if(this.validateDocumentRole02PrintCoverDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listDocumentRole02PrintCover.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listDocumentRole02PrintCover.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listDocumentRole02PrintCover.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listDocumentRole02PrintCover.length; i++) {
          if (this.listDocumentRole02PrintCover[i].is_check) {
            this.listDocumentRole02PrintCover[i].cancel_reason = rs
            this.listDocumentRole02PrintCover[i].status_code = "DELETE"
            this.listDocumentRole02PrintCover[i].is_deleted = true

            if (true && this.listDocumentRole02PrintCover[i].id) ids.push(this.listDocumentRole02PrintCover[i].id)
            // else this.listDocumentRole02PrintCover.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole02PrintCoverDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listDocumentRole02PrintCover.length; i++) {
          this.listDocumentRole02PrintCover[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole02PrintCoverDelete(params: any, ids: any[]): void {
    this.DocumentProcessService.DocumentRole02PrintCoverDelete(params).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintCoverDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listDocumentRole02PrintCover.length; i++) {
          if (this.listDocumentRole02PrintCover[i].id == id) {
            this.listDocumentRole02PrintCover.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listDocumentRole02PrintCover.length; i++) {
        this.listDocumentRole02PrintCover[i] = i + 1
      }

      this.onClickDocumentRole02PrintCoverList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.DocumentProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listDocumentRole02PrintCover = data.documentrole02printcover_list || []
    this.changePaginateTotal(this.listDocumentRole02PrintCover.length, 'paginateDocumentRole02PrintCover')

  }

  listData(data: any): void {
    this.listDocumentRole02PrintCover = data.list || []
    this.help.PageSet(data, this.paginateDocumentRole02PrintCover)
    //this.changePaginateTotal(this.listDocumentRole02PrintCover.length, 'paginateDocumentRole02PrintCover')

  }

  saveData(): any {
    // let params = this.input
    // params.documentrole02printcover_list = this.listDocumentRole02PrintCover || []

    const params = {
      document_role02_print_document_start_date: this.input.document_role02_print_document_start_date,
      document_role02_print_document_end_date: this.input.document_role02_print_document_end_date,
      department_code: this.input.department_code,
      rule_count: this.input.rule_count,
      document_role02_print_cover_status_code: this.input.document_role02_print_cover_status_code,
      request_number: this.input.request_number,
      //page_index: +this.paginateDocumentRole02PrintCover.currentPage,
      //item_per_page: +this.paginateDocumentRole02PrintCover.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'document_role02_print_document_date',
      //  value: displayDateServer(this.input.document_role02_print_document_start_date),
      //  operation: 3
      //}, {
      //  key: 'document_role02_print_document_date',
      //  value: displayDateServer(this.input.document_role02_print_document_end_date),
      //  operation: 4
      //}, {
      //  key: 'department_code',
      //  value: this.input.department_code,
      //  operation: 0
      //}, {
      //  key: 'rule_count',
      //  value: this.input.rule_count,
      //  operation: 5
      //}, {
      //  key: 'document_role02_print_cover_status_code',
      //  value: this.input.document_role02_print_cover_status_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}]
    }

    return params
  }

  onClickDocumentRole02PrintCoverPrint(row_item: any = null): void {
    window.open("/pdf/PostCover/" + (
      row_item ?
        row_item.id :
        this.listDocumentRole02PrintCover.filter(r => r.is_check).map((item) => { return item.id }).join("|"))
    )
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginateDocumentRole02PrintCover') {
      this.onClickDocumentRole02PrintCoverList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
