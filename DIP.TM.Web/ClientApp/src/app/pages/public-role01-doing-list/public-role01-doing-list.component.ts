import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role01-doing-list",
  templateUrl: "./public-role01-doing-list.component.html",
  styleUrls: ["./public-role01-doing-list.component.scss"]
})
export class PublicRole01DoingListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole01Doing
  public listPublicRole01Doing: any[]
  public paginatePublicRole01Doing: any
  public perPagePublicRole01Doing: number[]
  // Response PublicRole01Doing
  public responsePublicRole01Doing: any
  public listPublicRole01DoingEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public publicReceiveDoStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //public_start_start_date: getMoment(),
      //public_start_end_date: getMoment(),
      //public_end_start_date: getMoment(),
      //public_end_end_date: getMoment(),
      public_round_status_code: '',
      request_number: '',
    }
    this.listPublicRole01Doing = []
    this.paginatePublicRole01Doing = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole01Doing.id = 'paginatePublicRole01Doing'
    this.perPagePublicRole01Doing = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      publicReceiveDoStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole01DoingList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.publicRoundStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.public_round_status_code = "DOING_PUBLIC"
      }
      if (this.editID) {
        this.PublicProcessService.PublicRole01DoingLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }

  onClickPublicRole01DoingList(): void {
    // if(this.validatePublicRole01DoingList()) {
    // Open loading
    // Call api
    this.callPublicRole01DoingList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole01DoingList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole01DoingList(this.help.GetFilterParams(params, this.paginatePublicRole01Doing)).subscribe((data: any) => {
      // if(isValidPublicRole01DoingListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: 1 })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickPublicRole01DoingAdd(): void {
    this.listPublicRole01Doing.push({
      index: this.listPublicRole01Doing.length + 1,
      book_index: null,
      public_start_date_text: null,
      public_end_date: null,
      request_item_sub_type_1_count: null,
      save020_count: null,
      public_remain_day: null,
      public_doing_remark: null,
      public_do_status_name: null,

    })
    this.changePaginateTotal(this.listPublicRole01Doing.length, 'paginatePublicRole01Doing')
  }

  onClickPublicRole01DoingEdit(item: any): void {
    var win = window.open("/public-role01-prepare/list/?book_index=" + item.book_index)
  }


  onClickPublicRole01DoingDelete(item: any): void {
    // if(this.validatePublicRole01DoingDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listPublicRole01Doing.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listPublicRole01Doing.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listPublicRole01Doing.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listPublicRole01Doing.length; i++) {
          if (this.listPublicRole01Doing[i].is_check) {
            this.listPublicRole01Doing[i].cancel_reason = rs
            this.listPublicRole01Doing[i].status_code = "DELETE"
            this.listPublicRole01Doing[i].is_deleted = true

            if (true && this.listPublicRole01Doing[i].id) ids.push(this.listPublicRole01Doing[i].id)
            // else this.listPublicRole01Doing.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callPublicRole01DoingDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listPublicRole01Doing.length; i++) {
          this.listPublicRole01Doing[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callPublicRole01DoingDelete(params: any, ids: any[]): void {
    this.PublicProcessService.PublicRole01DoingDelete(params).subscribe((data: any) => {
      // if(isValidPublicRole01DoingDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listPublicRole01Doing.length; i++) {
          if (this.listPublicRole01Doing[i].id == id) {
            this.listPublicRole01Doing.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listPublicRole01Doing.length; i++) {
        this.listPublicRole01Doing[i] = i + 1
      }

      this.onClickPublicRole01DoingList()
      // Close loading
      this.global.setLoading(false)
    })
  }
  

  onClickPublicRole01DoingPublicRoundEnd(): void {
    this.callPublicRole01DoingPublicRoundEnd(this.saveData())
  }
  //! <<< Call API >>>
  callPublicRole01DoingPublicRoundEnd(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRoundEnd().subscribe((data: any) => {
      // if(isValidPublicRole01ItemListResponse(res)) {
      //if (data) {
      // Set value
      this.onClickPublicRole01DoingList()
      //this.automateTest.test(this, { list: data.list })
      //}
      //this.global.setLoading(false)
      // }
      // Close loading
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicRole01Doing = data.publicrole01doing_list || []
    this.changePaginateTotal(this.listPublicRole01Doing.length, 'paginatePublicRole01Doing')

  }

  listData(data: any): void {
    this.listPublicRole01Doing = data.list || []
    this.help.PageSet(data, this.paginatePublicRole01Doing)
    //this.listPublicRole01Doing = data.list || []
    //this.changePaginateTotal(this.listPublicRole01Doing.length, 'paginatePublicRole01Doing')

  }

  saveData(): any {
    // let params = this.input
    // params.publicrole01doing_list = this.listPublicRole01Doing || []

    const params = {
      public_start_start_date: this.input.public_start_start_date,
      public_start_end_date: this.input.public_start_end_date,
      public_end_start_date: this.input.public_end_start_date,
      public_end_end_date: this.input.public_end_end_date,
      public_round_status_code: this.input.public_round_status_code,
      request_number: this.input.request_number,
      //page_index: +this.paginatePublicRole01Doing.currentPage,
      //item_per_page: +this.paginatePublicRole01Doing.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //    key: 'public_start_date',
      //    value: displayDateServer(this.input.public_start_start_date),
      //    operation: 3
      //}, {
      //    key: 'public_start_date',
      //    value: displayDateServer(this.input.public_start_end_date),
      //    operation: 4
      //}, {
      //    key: 'public_end_date',
      //    value: displayDateServer(this.input.public_end_start_date),
      //    operation: 3
      //}, {
      //    key: 'public_end_date',
      //    value: displayDateServer(this.input.public_end_end_date),
      //    operation: 4
      //}, {
      //    key: 'public_round_status_code',
      //    value: this.input.public_round_status_code,
      //    operation: 0
      //}, {
      //    key: 'request_number',
      //    values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //    operation: 5
      //}]
    }

    return params
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicRole01Doing') {
      this.onClickPublicRole01DoingList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
