import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consider-kor04-request-edit',
  templateUrl: './consider-kor04-request-edit.component.html',
  styleUrls: ['./consider-kor04-request-edit.component.scss']
})
export class ConsiderKor04RequestEditComponent implements OnInit {

  constructor() { }
  
  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public master: any;

  public tab_menu_show_index: any
  public tab_menu_show_index2: any
  public tab_menu_show_index3: any

  public tableList: any;
  public tableRequestDetail: any;
  public tableRequestDetail2: any;
  public tableRequestDetail3: any;
  public tableViewDocument: any;
  public tableInfo: any;
  public tableIssueBook: any;
  public tableAgent: any;
  public tableProduct: any;

  ngOnInit() {
    this.input = {};
    this.validate = {};
    this.master = {
      checkList: [
        { code: "1", name: "หนังสือแจ้งคำสั่ง" },
        { code: "2", name: "การปฏิบัติตามคำสั่ง" },
        { code: "3", name: "การชำระค่าธรรมเนียม" },
        { code: "4", name: "การแจ้งฟ้องคดี / คำพิพากษา" },
        { code: "5", name: "สถานที่ติดต่อ" },
        { code: "6", name: "ชื่อคู่กรณี (ถ้ามี)" },
        { code: "7", name: "การขอถอน" },
        { code: "8", name: "ใบตอบรับ / ครบกำหนด" },
        { code: "9", name: "มีอุทธรณ์ / คำวินิจฉัยฯ" },
      ],
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList2: [
        { code: "1", name: "รอบที่ 1" },
        { code: "2", name: "รอบที่ 2" },
      ],
      typeCodeList3: [
        { code: "1", name: "อนุญาต" },
        { code: "2", name: "รวมเรื่อง" },
        { code: "3", name: "ไม่อนุญาต" },
      ],
      typeCodeList4: [
        { code: "1", name: "ยังไม่หลุด" },
        { code: "2", name: "ยังไม่หลุด" },
        { code: "3", name: "ยังไม่หลุด" },
      ],
      typeCodeList5: [
        { code: "1", name: "เจ้าของ" },
        { code: "2", name: "ตัวแทน" },
        { code: "3", name: "ตัวแทนช่วง" },
        { code: "4", name: "อื่นๆ" },
        { code: "5", name: "ตามก.ที่ยื่น" },
      ],
      typeCodeList6: [
        { code: "1", name: "ต.ค.5 ข้อ 8" },
        { code: "2", name: "ต.ค.9 ข้อ 10" },
        { code: "3", name: "ต.ค.9 ข้อ 2" },
        { code: "4", name: "ต.ค.9 ข้อ 3" },
      ],
    }

    this.tab_menu_show_index = 11
    this.tab_menu_show_index2 = 1
    this.tab_menu_show_index3 = 1

    this.tableList = {
      column_list: [
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.06",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting"
        },
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.07",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting"
        },
        {
          request_id: "17011323_02",
          submission_date: "20/07/2561",
          owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
          address: "เลขที่ 719 ถนนสี่พระยา",
          document: "ก.20",
          accept_date: "20/05/2561",
          consider_date: "20/06/2561",
          status: "waiting"
        },
      ],
    }

    this.tableRequestDetail = {
      head_column_list: [
        {
          order: "#",
          command: "คำสั่ง",
          date: "วันที่สั่ง",
          registrar: "นายทะเบียน",
          book_status: "สถานะหนังสือ",
          date_order: "วันที่ส่งคำสั่ง",
          no: "เลข พณ.",
          worker: "ผู้รับงาน",
          note: "หมายเหตุ",
          status: "สถานะ"
        },
      ],

      column_list: [
        {
          command: "ตค. 4 (ม.20)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
        {
          command: "ตค. 2 (ม.13)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
        {
          command: "ตค. 1 (ม.11)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
      ]
    }

    this.tableRequestDetail2 = {
      head_column_list: [
        {
          order: "#",
          name: "ชื่อผู้รับโอน",
          address: "ที่อยู่",
          tel: "โทรศัพท์",
          fax: "โทรสาร",
          operation: "คำสั่ง",
        },
      ],

      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          fax: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
          fax: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
          fax: "-",
        },
      ]
    }

    this.tableRequestDetail3 = {
      head_column_list: [
        {
          order: "#",
          transfer_date: "วันที่โอน",
          assignee: "ผู้โอน",
          transfer: "ผู้รับโอน",
          genera: "จำพวก",
          detail: "รายการสินค้า",
          status: "สถานะ",
        },
      ],

      column_list: [
        {
          transfer_date: "11/10/2562",
          assignee: "นางนิจรินทร์ กุหลาบป่า",
          transfer: "บริษัท สตาร์ฟอร์ด จำกัด",
          genera: "21",
          detail: "กระถาง",
          status: "wait",
        },
        {
          transfer_date: "11/09/2562",
          assignee: "นางนิจรินทร์ กุหลาบป่า",
          transfer: "เฮงเคล คอร์ปอเรชั่น",
          genera: "11",
          detail: "กาต้มน้ำไฟฟ้า , ขวดน้ำร้อน",
          status: "done",
        },
        {
          transfer_date: "11/09/2562",
          assignee: "นางนิจรินทร์ กุหลาบป่า",
          transfer: "เฮงเคล คอร์ปอเรชั่น",
          genera: "10",
          detail: "กาต้มน้ำไฟฟ้า , ขวดน้ำร้อน",
          status: "done",
        },
      ]
    }

    this.tableViewDocument = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }

    this.tableInfo = {
      column_list: [
        {
          detail: "ม.7"
        },
        {
          detail: "ม.9"
        },
        {
          detail: "ม.7"
        },
      ]
    }

    this.tableIssueBook = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }

    this.tableAgent = {
      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เอาสตร้าสเซอ 5-9, 77761 ชิลทาค ประเทศเยอรมนี",
          tel: "-",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
        },
      ]
    }

    this.tableProduct = {
      head_column_list: [
        {
          order: "#",
          type: "จำพวกสินค้า",
          description: "รายการสินค้า/บริการ"
        },
      ],

      column_list: [
        {
          type: "21"
        },
        {
          type: "3"
        },
      ]
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>
  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }
}
