import { Auth } from "../../auth";
import { fabric } from "fabric"
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { UploadService } from "../../services/upload.service"
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-document-process-collect-list",
    templateUrl: "./document-process-collect-list.component.html",
    styleUrls: ["./document-process-collect-list.component.scss"]
})
export class DocumentProcessCollectListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    // List Request
    public listRequest: any[]
    public paginateRequest: any
    public perPageRequest: number[]
    // Response Request
    public responseRequest: any
    public listRequestEdit: any

    // List History
    public listHistory: any[]
    public paginateDocumentProcessCollect: any
    public perPageHistory: number[]
    // Response History
    public responseHistory: any
    public listHistoryEdit: any


    // List DocumentProcess
    public listDocumentProcess: any[]
    public paginateDocumentProcess: any
    public perPageDocumentProcess: number[]
    // Response DocumentProcess
    public responseDocumentProcess: any

    public canvasStage: any
    public canvas: any
    public canvas_img: any
    public canvas_el: any
    public canvas_rectangle: any
    public canvas_is_erase: any
    public canvas_is_crop: any
    public rowImageEdit: any
    //public canvasImg: any
    //public idNo : number
    // pointer temp
    public tempLeft: number
    public tempTop: number
    public tempWidth: number
    public tempHeight: number
    public tempScale: number
    public tempScaleWidth: number
    public tempScaleHeight: number

    public popup: any

    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }
        console.log(this.auth.getAuth())
        this.input = {
            request_number: '',
            sender_by_name: this.auth.getAuth().name,
            department_name: this.auth.getAuth().department_name,
            remark: '',
        }
        this.listRequest = []
        this.paginateRequest = CONSTANTS.PAGINATION.INIT
        this.perPageRequest = CONSTANTS.PAGINATION.PER_PAGE

        this.listHistory = []
        this.paginateDocumentProcessCollect = CONSTANTS.PAGINATION.INIT
        this.perPageHistory = CONSTANTS.PAGINATION.PER_PAGE

        this.listDocumentProcess = []

        //Master List
        this.master = {
        }
        //Master List



        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //


        this.contactAddress = {
            address_type_code: "OWNER"
        }
        this.modalAddress = {}

        this.popup = {
            //is_edit_show: true,
        }

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.route.queryParams.subscribe((param_url: any) => {
        })

        //this.forkJoinService.initDocumentProcessCollectList().subscribe((data: any) => {
        //  if (data) {
        //    this.help.Clone(data, this.master)

        //  }
        //if (this.editID) {
        //this.DocumentProcessService.DocumentProcessCollectLoad(this.editID).subscribe((data: any) => {
        //  if (data) {
        //    // Manage structure
        //    this.loadData(data)
        //  }
        //  // Close loading
        //this.onClickClassificationRequestList()
        this.onClickClassificationHistoryList()
        //this.global.setLoading(false)
        //  this.automateTest.test(this)
        //})
        //  } else {
        //    this.global.setLoading(false)
        //    this.automateTest.test(this)
        //  }

        //})
    }

    constructor(
        private auth: Auth,
        private uploadService: UploadService,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }

    //onClickClassificationLoadFromRequestNumber(): void {
    //  // if(this.validateDocumentProcessCollectList()) {
    //  // Open loading
    //  // Call api
    //  //var param = {
    //  //  request_number: this.input.request_number,
    //  //  //department_name: this.input.department_name,
    //  //  //remark: this.input.remark,
    //  //}
    //  if (this.input.request_number.length > 3) {
    //    this.callClassificationLoadFromRequestNumber(this.input.request_number)
    //  } else {
    //    this.help.Alert("กรุณาใส่เลขที่คำขอ")
    //  }
    //  // }
    //}
    ////! <<< Call API >>>
    //callClassificationLoadFromRequestNumber(params: any): void {
    //  this.global.setLoading(true)
    //  this.DocumentProcessService.ClassificationLoadFromRequestNumber(params).subscribe((data: any) => {
    //    // if(isValidDocumentProcessCollectListResponse(res)) {
    //    if (data) {
    //      // Set value
    //      //this.listData(data)
    //      this.loadData(data)

    //      //this.automateTest.test(this, { list: data.list })
    //    }
    //    this.global.setLoading(false)
    //    // }
    //    // Close loading
    //  })
    //}

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickDocumentProcessCollectRequestAdd(): void {
        var param = {
            request_number: this.input.request_number,
            sender_by_name: this.input.sender_by_name,
            department_name: this.input.department_name,
            remark: this.input.remark,
        }
        this.callDocumentProcessCollectRequestAdd(param)
    }
    //! <<< Call API >>>
    callDocumentProcessCollectRequestAdd(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.CollectRequestAdd(params).subscribe((data: any) => {
            // if(isValidDocumentProcessCollectListResponse(res)) {
            if (data) {
                // Set value
                //this.onClickClassificationRequestList()
                this.onClickClassificationHistoryList()
                //this.loadData(data)

                //this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }


    onClickDocumentProcessCollectRequestDelete(row_item: any): void {
        var rs = confirm("คุณต้องการลบรายการ?")
        if (rs) {
            //var param = {
            //  request_number: this.input.request_number,
            //  sender_by_name: this.input.sender_by_name,
            //  department_name: this.input.department_name,
            //  remark: this.input.remark,
            //}
            this.callDocumentProcessCollectRequestDelete(row_item.id)
        }
    }
    //! <<< Call API >>>
    callDocumentProcessCollectRequestDelete(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.CollectRequestDelete(params).subscribe((data: any) => {
            // if(isValidDocumentProcessCollectListResponse(res)) {
            if (data) {
                // Set value
                this.onClickClassificationRequestList()
                //this.loadData(data)

                //this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }




    onClickClassificationRequestList(): void {
        // if(this.validateDocumentProcessCollectList()) {
        // Open loading
        // Call api
        var param = {
            //request_number: this.input.request_number,
            //department_name: this.input.department_name,
            //remark: this.input.remark,
        }
        //if (this.input.request_number.length > 3) {
        this.callClassificationRequestList(param)
        //} else {
        //  let toast = CONSTANTS.TOAST.ERROR
        //  toast.message = "กรุณาใส่เลขที่คำขอ"
        //  this.global.setToast(toast)
        //}
        // }
    }
    //! <<< Call API >>>
    callClassificationRequestList(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.ClassificationRequestList(this.help.GetFilterParams(params, this.paginateRequest)).subscribe((data: any) => {
            // if(isValidDocumentProcessCollectListResponse(res)) {
            if (data) {
                // Set value
                this.listRequest = data.list || []
                this.help.PageSet(data, this.paginateRequest)
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickDocumentProcessCollectRequestEdit(item: any): void {
        this.onClickDocumentProcessCollectHistoryAdd(item)
    }
    //onClickDocumentProcessCollectRequestDelete(item: any): void {
    //  this.listRequest.splice(this.listRequest.indexOf(item), 1)
    //}
    ////! <<< Call API >>>
    //callDocumentProcessCollectRequestDelete(params: any, ids: any[]): void {
    //  //this.DocumentProcessService.DocumentProcessCollectRequestDelete(params).subscribe((data: any) => {
    //  //  // if(isValidDocumentProcessCollectRequestDeleteResponse(res)) {

    //  //  ids.forEach((id: any) => {
    //  //    for (let i = 0; i < this.listRequest.length; i++) {
    //  //      if (this.listRequest[i].id == id) {
    //  //        this.listRequest.splice(i--, 1);
    //  //      }
    //  //    }
    //  //  });

    //  //  for (let i = 0; i < this.listRequest.length; i++) {
    //  //    this.listRequest[i] = i + 1
    //  //  }

    //  //  //this.onClickDocumentProcessCollectList()
    //  //  // Close loading
    //  //  this.global.setLoading(false)
    //  //})
    //}

    onClickDocumentProcessCollectHistoryAdd(item: any): void {
        var param = {
            ...item,
            file_id: 0,
        }
        this.callDocumentProcessCollectHistoryAdd(param)
    }
    //! <<< Call API >>>
    callDocumentProcessCollectHistoryAdd(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.CollectHistoryAdd(params).subscribe((data: any) => {
            // if(isValidDocumentProcessCollectListResponse(res)) {
            if (data) {
                // Set value
                //this.onClickClassificationRequestList()
                this.onClickClassificationHistoryList()
                //this.loadData(data)

                //this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }



    onClickClassificationHistoryList(): void {
        // if(this.validateDocumentProcessCollectList()) {
        // Open loading
        // Call api
        var param = {
            //request_number: this.input.request_number,
            //department_name: this.input.department_name,
            //remark: this.input.remark,
        }
        //if (this.input.request_number.length > 3) {
        this.callClassificationHistoryList(param)
        //} else {
        //  let toast = CONSTANTS.TOAST.ERROR
        //  toast.message = "กรุณาใส่เลขที่คำขอ"
        //  this.global.setToast(toast)
        //}
        // }
    }
    //! <<< Call API >>>
    callClassificationHistoryList(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.ClassificationHistoryList(this.help.GetFilterParams(params, this.paginateDocumentProcessCollect)).subscribe((data: any) => {
            // if(isValidDocumentProcessCollectListResponse(res)) {
            if (data) {
                // Set value
                //this.listData(data)
                this.listData(data)

                //this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }



    onClickDocumentProcessCollectHistoryEdit(item: any): void {
        //var win = window.open("/" + item.save_id)
        console.log(item)

        this.popup.is_edit_show = true
        this.popup.edit_item = item

        setTimeout(() => {
            this.ImageEdit(item.file_trademark_2d)
        }, 100)
    }



    onClickDocumentProcessCollectHistoryDelete(row_item: any): void {
        //var param = {
        //  request_number: this.input.request_number,
        //  sender_by_name: this.input.sender_by_name,
        //  department_name: this.input.department_name,
        //  remark: this.input.remark,
        //}
        if (confirm("คุณต้องการลบรายการ?")) {
            this.callDocumentProcessCollectHistoryDelete(row_item.id)
        }
    }
    //! <<< Call API >>>
    callDocumentProcessCollectHistoryDelete(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.DocumentProcessCollectHistoryDelete(params).subscribe((data: any) => {
            // if(isValidDocumentProcessCollectListResponse(res)) {
            if (data) {
                // Set value
                //this.onClickClassificationRequestList()
                this.onClickClassificationHistoryList()
                //this.loadData(data)

                //this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }




    //! <<< Upload (Image) >>>
    onClickUploadImage(event: any, obj: string, name: any, maxSize: any, item: any): void {
        if (event) {
            let file = event.target.files[0]
            event.target.value = ""
            //this.setFileData(file, obj, name, item)

            //this[obj][name].file = file
            let reader = new FileReader()
            reader.onload = e => {
                //this[obj][name].blob = reader.result
                //item.file = file
                //this.onClickRequestDocumentCollectFileUpload(item)

                //// TODO upload file process start
                //this.rowImageEdit.file_id = 0
                //this.rowImageEdit.physical_path = this.input.imageUpload.blob
                //// TODO upload file process end

                this.ImageEdit(reader.result)
            }
            reader.readAsDataURL(file)
        }
    }


    ImageEdit(physical_path: any): void {
        console.log("canvas - ImageEdit")

        //this.global.setLoading(false)
        this.modal["isModalImageEditOpen"] = true

        //var pThis = this
        //this.canvasStage = {
        //    erase: false
        //}
        //// Clear canvas before init
        if (this.canvas) {
            let canvasContainer = document.getElementById("canvas-container")
            while (canvasContainer.firstChild) {
                canvasContainer.removeChild(canvasContainer.firstChild)
            }
            let canvasStore = document.createElement("canvas")
            canvasStore.id = "canvas"
            canvasStore.width = 800
            canvasStore.height = 800
            document.getElementById("canvas-container").appendChild(canvasStore)
        }

        this.canvas = new fabric.Canvas("canvas", {
            isDrawingMode: false,
            freeDrawingBrush: "vline",
        })
        var canvas = this.canvas
        // Set eraser size and color
        canvas.freeDrawingBrush.width = 25
        canvas.freeDrawingBrush.color = "rgba(255,255,255,1)"
        // Add image into canvas
        const img = new Image()
        img.src = physical_path

        //console.log(physical_path)

        img.onload = () => {
            this.canvas_img = new fabric.Image(img, {
                left: 0,
                top: 0
            })

            console.log("img: " + img.width + " " + img.height)
            //    this.tempScale = 1;
            //    this.tempScaleWidth = 1;
            //    this.tempScaleHeight = 1;

            if (img.width > 800 || img.height > 800) {
                //Scale to width
                if (img.width > img.height) {
                    this.canvas_img.scaleToWidth(800)
                    this.tempScale = img.width / 800;
                } else {
                    //Scale to height
                    this.canvas_img.scaleToHeight(800)
                    this.tempScale = img.height / 800;
                }

                this.canvas_img.scaleToWidth(img.width / this.tempScale);
                this.canvas_img.scaleToHeight(img.height / this.tempScale);

                canvas.add(this.canvas_img)

                this.ImageEdit(this.canvas.toDataURL("image/jpeg"))

                return
            }

            canvas.add(this.canvas_img)

            //    this.refreshSizeImage();

            var left = -this.canvas_img.width / 2;
            var top = -this.canvas_img.height / 2;
            var width = this.canvas_img.width;
            var height = this.canvas_img.height;
            this.canvas_rectangle = [left, top, width, height]
            this.tempLeft = this.canvas_rectangle[0];
            this.tempTop = this.canvas_rectangle[1];
            this.tempWidth = this.canvas_rectangle[2];
            this.tempHeight = this.canvas_rectangle[3];

            this.canvas_img.clipTo = (ctx) => {
                console.log("clipTo 1")
                ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
            }

            this.canvas_img.selectable = !this.canvas_is_crop
            //console.log(this.canvas_img.selectable)

            //    console.log("nL:" + left + ", nT:" + top + ", nW:" + width + ", nH:" + height);

            //    var allObjects = this.canvas.getObjects().slice()
            //    var group = new fabric.Group(allObjects)
            //    this.canvas.clear().renderAll()
            //    this.canvas.add(group)
            //    this.canvas.setActiveObject(group)
            //    this.canvas_img = this.canvas.getObjects()[0]
            //    this.canvas_img.clipTo = function (ctx) {
            //        ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
            //    }
            //    console.log(this.canvas_rectangle);
        }

        var mousedown_x, mousedown_y, mouseup_x, mouseup_y
        var crop = false
        ////var disabled = false
        canvas.on("mouse:down", (event) => {
            if (!this.canvas_is_crop) return

            var r = document.getElementById('canvas').getBoundingClientRect()
            mousedown_x = event.e.pageX - r.left - window.scrollX
            mousedown_y = event.e.pageY - r.top - window.scrollY
            console.log("mouse:down: " + mousedown_x + " " + mousedown_y)

            crop = true
            if (this.canvas_el) {
                canvas.remove(this.canvas_el);
                this.canvas_el = null;
            }

            //window.scroll(0, 0)
            //setTimeout(() => {
            //}, 500)
        })
        canvas.on("mouse:up", (event) => {
            if (!this.canvas_is_crop) return
            if (!crop) return
            crop = false

            var r = document.getElementById('canvas').getBoundingClientRect()
            mouseup_x = event.e.pageX - r.left - window.scrollX
            mouseup_y = event.e.pageY - r.top - window.scrollY
            console.log("mouse:up: " + mouseup_x + " " + mouseup_y)

            this.canvas_el = new fabric.Rect({
                fill: 'transparent',
                originX: 'left',
                originY: 'top',
                stroke: '#ccc',
                strokeDashArray: [2, 2],
                opacity: 1,
                width: 1,
                height: 1,
                selectable: false,
            })
            canvas.add(this.canvas_el)
            canvas.bringToFront(this.canvas_el)

            //console.log(mousex + " " + mousey)
            //console.log(event.e.pageX + " " + event.e.pageY)
            //console.log(pos)

            this.canvas_el.left = mousedown_x
            this.canvas_el.top = mousedown_y
            this.canvas_el.width = mouseup_x - mousedown_x
            this.canvas_el.height = mouseup_y - mousedown_y

            //console.log(this.canvas_img.left + " " + this.canvas_img.top)
            //console.log(this.canvas_el.left + " " + this.canvas_el.top + " " + this.canvas_el.width + " " + this.canvas_el.height)

            var left = this.canvas_el.left - this.canvas_img.width / 2 - this.canvas_img.left;
            var top = this.canvas_el.top - this.canvas_img.height / 2 - this.canvas_img.top;
            var width = this.canvas_el.width;
            var height = this.canvas_el.height;

            //var left = this.canvas_el.left - this.canvas_img.left;
            //var top = this.canvas_el.top - this.canvas_img.top;
            //var width = this.canvas_el.width;
            //var height = this.canvas_el.height;

            this.canvas_rectangle = [left, top, width, height]
            console.log("Select Area: " + this.canvas_rectangle);

            //this.canvas.renderAll();
        })
    }
    // If check ยางลบ drawingMode = true
    onClickCheckBoxErase(value: boolean): void {
        if (this.canvas_is_crop) return
        this.canvas_is_erase = value

        //var pThis = this
        //setTimeout(() => {
        if (!value) {
            var allObjects = this.canvas.getObjects().slice()
            console.log(allObjects.length)
            var group = new fabric.Group(allObjects)
            this.canvas.clear().renderAll()
            this.canvas.add(group)
            this.canvas.setActiveObject(group)
            //this.canvas_img = this.canvas.getObjects()[0]
            //this.canvas_img.clipTo = (ctx) => {
            //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
            //}

            this.canvas.isDrawingMode = false
        } else {
            this.canvas.isDrawingMode = true
        }
        //}, 100)
    }
    refreshSizeImage(): void {
        //var pThis = this;

        //var allObjects = pThis.canvas.getObjects().slice()
        //var group = new fabric.Group(allObjects)
        //pThis.canvas.clear().renderAll()
        //pThis.canvas.add(group)
        //pThis.canvas.setActiveObject(group)
        //pThis.canvas_img = pThis.canvas.getObjects()[0]

        //pThis.canvas.renderAll();
    }
    onClickImageCrop(value: boolean): void {
        console.log(value)
        if (this.canvas_is_erase) return
        this.canvas_is_crop = value
        //var this = this;
        //var left;
        //var top;
        //var width;
        //var height;

        //var tCVW = this.canvas_img.width;
        //var tCVH = this.canvas_img.height;
        //this.refreshSizeImage();
        //var nW = tCVW / this.canvas_img.width;
        //var nH = tCVH / this.canvas_img.height;
        //this.tempScaleWidth *= nW;
        //this.tempScaleHeight *= nH;
        //console.log("changed W: " + tCVW + " to " + this.canvas_img.width + " Scale: " + nW + " TotalScale:" + this.tempScaleWidth);
        //console.log("changed H: " + tCVH + " to " + this.canvas_img.height + " Scale: " + nH + " TotalScale" + this.tempScaleHeight);

        //if ((tCVW != this.canvas_img.width || tCVH != this.canvas_img.height)) {
        //    if (this.tempScaleWidth < 1) {
        //        left = this.tempLeft / this.tempScaleWidth;
        //        width = this.tempWidth / this.tempScaleWidth;
        //    }
        //    else {
        //        left = this.tempLeft;
        //        width = this.tempWidth;
        //    }
        //    if (this.tempScaleHeight < 1) {
        //        top = this.tempTop / this.tempScaleHeight;
        //        height = this.tempHeight / this.tempScaleHeight;
        //    }
        //    else {
        //        top = this.tempTop;
        //        height = this.tempHeight;
        //    }
        //}
        //else {
        //    left = this.tempLeft;
        //    top = this.tempTop;
        //    width = this.tempWidth;
        //    height = this.tempHeight;
        //}
        //console.log("nL:" + left + ", nT:" + top + ", nW:" + width + ", nH:" + height);

        //this.canvas_rectangle = [left, top, width, height]
        //console.log(this.canvas_rectangle);
        //var allObjects = this.canvas.getObjects().slice()
        //var group = new fabric.Group(allObjects)
        //this.canvas.clear().renderAll()
        //this.canvas.add(group)
        //this.canvas.setActiveObject(group)
        //this.canvas_img = this.canvas.getObjects()[0]
        //this.canvas_img.clipTo = (ctx) => {
        //    ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
        //}
        //this.tempLeft = this.canvas_rectangle[0];
        //this.tempTop = this.canvas_rectangle[1];
        //this.tempWidth = this.canvas_rectangle[2];
        //this.tempHeight = this.canvas_rectangle[3];


        //window.scroll(0, 0)

        if (this.canvas_is_crop) {
            this.ImageEdit(this.canvas.toDataURL("image/jpeg"))
        } else {
            this.onClickImageCropConfirm(false)
        }
        //this.canvas.discardActiveObject().renderAll();
        //this.canvas_img.selectable = !value
    }
    onClickImageCropConfirm(is_confirm: boolean): void {
        console.log("onClickImageCropConfirm")

        if (this.canvas_el) {
            this.canvas.remove(this.canvas_el);
            this.canvas_el = null;
        }
        this.canvas_is_crop = false
        this.canvas_img.selectable = true

        if (is_confirm) {
            //let saveData = this.canvas.toDataURL("image/jpeg")
            //let params = {
            //    file: this.dataURItoBlob(saveData)
            //}
            //console.log(saveData)
            //    if (this.tempScaleWidth > 1) {
            //        this.canvas_rectangle[0] *= this.tempScaleWidth;
            //        this.canvas_rectangle[2] *= this.tempScaleWidth;
            //    }
            //    if (this.tempScaleHeight > 1) {
            //        this.canvas_rectangle[1] *= this.tempScaleHeight;
            //        this.canvas_rectangle[3] *= this.tempScaleHeight;
            //    }
            //setTimeout(() => {
            //this.canvas_img = this.canvas.getObjects()[0]
            //this.canvas_img.clipTo = (ctx) => {
            //  //  console.log("clipTo 2")
            //  //  this.canvas_img.clipTo = null

            //  //  //console.log(this.canvas_rectangle)
            //  //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
            //  //  //ctx.save();
            //ctx.drawImage(this, 100, 100, 100, 100, 100, 100, 100, 100)
            //  //  //var allObjects = this.canvas.getObjects().slice()
            //  //  //var group = new fabric.Group(allObjects)
            //  //  //this.canvas.clear().renderAll()
            //  //  //this.canvas.add(group)
            //  //  //this.canvas.setActiveObject(group)
            //  //  //this.canvas.isDrawingMode = false

            //var imgInstance = new fabric.Image(this.canvas_img, {
            //  left: 10,
            //  top: 10,
            //  width: 10,
            //  height: 10,
            //  clipTo: function (ctx) {
            //    ctx.rect(100, 100, 100, 100);
            //  }
            //});
            //this.canvas.add(imgInstance);

            //this.ImageEdit(this.canvas.toDataURL("image/jpeg"))


            const img = new Image()
            img.src = this.canvas.toDataURL("image/jpeg")
            this.canvas.clear()
            this.canvas.renderAll();

            img.onload = () => {
                var canvas_img = new fabric.Image(img, {
                    left: -(this.canvas_rectangle[0] + this.canvas.width / 2),
                    top: -(this.canvas_rectangle[1] + this.canvas.height / 2),
                    width: this.canvas_rectangle[2] + this.canvas_rectangle[0] + this.canvas.width / 2,
                    height: this.canvas_rectangle[3] + this.canvas_rectangle[1] + this.canvas.height / 2,
                })
                this.canvas.add(canvas_img)

                const new_img = new Image()
                new_img.src = this.canvas.toDataURL("image/jpeg")
                this.canvas.clear()
                this.canvas.renderAll();

                new_img.onload = () => {
                    this.canvas_img = new fabric.Image(new_img, {
                        left: this.canvas_rectangle[0] + this.canvas.width / 2,
                        top: this.canvas_rectangle[1] + this.canvas.height / 2,
                        width: this.canvas_rectangle[2],
                        height: this.canvas_rectangle[3],
                    })
                    this.canvas.add(this.canvas_img)
                    this.canvas.setActiveObject(this.canvas_img)

                    //this.canvas_img.clipTo = (ctx) => {
                    //  console.log("clipTo 2")
                    //  ctx.rect(this.canvas_rectangle[0], this.canvas_rectangle[1], this.canvas_rectangle[2], this.canvas_rectangle[3]);
                    //}

                    this.canvas.renderAll();
                }
            }

            //}
            //console.log("d")
            //this.canvas.renderAll();
            //}, 100)
        } else {
            var left = this.tempLeft;
            var top = this.tempTop;
            var width = this.tempWidth;
            var height = this.tempHeight;
            this.canvas_rectangle = [left, top, width, height]
        }
        //if (is_confirm) {
        //    this.tempLeft = this.canvas_rectangle[0];
        //    this.tempTop = this.canvas_rectangle[1];
        //    this.tempWidth = this.canvas_rectangle[2];
        //    this.tempHeight = this.canvas_rectangle[3];
        //}
    }

    onClickDocumentProcessCollectHistoryImageSave(): void {
        let saveData = this.canvas.toDataURL("image/jpeg")
        let params = {
            file: this.dataURItoBlob(saveData)
        }
        this.callUpload(params)
    }
    dataURItoBlob(dataURI: any): any {
        var byteString = atob(dataURI.split(",")[1])

        var mimeString = dataURI
            .split(",")[0]
            .split(":")[1]
            .split("")[0]

        var ab = new ArrayBuffer(byteString.length)
        var ia = new Uint8Array(ab)
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i)
        }

        var bb = new Blob([ab], { type: mimeString })
        return bb
    }
    callUpload(params: any): void {
        console.log(params)
        const formData = new FormData()
        formData.append("file", params.file, params.file.name || "img.jpg")

        //this.modal["isModalImageEditOpen"] = false
        this.global.setLoading(true)
        this.uploadService.upload(formData).subscribe((data: any) => {
            //this.global.setLoading(false)
            //this.modal["isModalImageEditOpen"] = false

            if (data) {
                //this.rowImageEdit.file_id = data.id
                this.callDocumentProcessCollectHistoryImageSend(data)
            }
        })
    }

    callDocumentProcessCollectHistoryImageSend(data): void {
        if (data.id) {
            console.log(data)
            this.popup.edit_item["file_id"] = data.id

            this.DocumentProcessService.DocumentProcessCollectHistoryImageSend(this.popup.edit_item).subscribe((data: any) => {
                this.onClickClassificationHistoryList()
                this.global.setLoading(false)
                this.popup.is_edit_show = false
            })
        }
    }

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        //this.input = data

        this.listRequest.push({
            ...data,
            department_name: this.input.department_name,
            remark: this.input.remark,
        })

        //this.listRequest = data.request_list || []
        //this.listHistory = data.History_list || []
        //this.help.PageSet(data, this.paginateRequest)
        //this.help.PageSet(data, this.paginateDocumentProcessCollect)

    }

    listData(data: any): void {
        this.listHistory = data.list || []
        this.help.PageSet(data, this.paginateDocumentProcessCollect)

    }

    saveData(): any {
        let params = this.input

        params.request_list = this.listRequest || []
        params.History_list = this.listHistory || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }
    displayMoney(value: any): any {
        return displayMoney(value);
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

}
