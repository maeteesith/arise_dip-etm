import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-madrid-role05-workbox-wipo',
  templateUrl: './madrid-role05-workbox-wipo.component.html',
  styleUrls: ['./madrid-role05-workbox-wipo.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole05WorkboxWipoComponent implements OnInit {
  public input: any
  public validate: any
  public master: any
  public editID: any


  public listPublicItem: any = [];
  constructor() { }

  ngOnInit() {
    this.validate = {}
    this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      public_type_code: '',
      public_source_code: '',
      public_receiver_by: '',
      public_status_code: '',
      request_number: '',
    }
    this.master={
      request_type:[{code:1,name:'1'}],
      job_reciver:[{id:1,name:"Sam"},{id:2,name:"Tom"}]
  }

  this.listPublicItem = [
    { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", statusprogress: "บันทึก", command: "" },
    { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", statusprogress: "รอตรวจสอบ", command: "" }
]

  }

}
