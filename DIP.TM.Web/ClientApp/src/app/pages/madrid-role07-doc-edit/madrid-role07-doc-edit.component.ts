import { Component, OnInit } from '@angular/core';
import { Help } from 'src/app/helpers/help';
import { AutomateTest } from 'src/app/test/automate_test';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/global.service';
import { DialogService } from 'src/app/services/dialogService';
import { ForkJoinService } from 'src/app/services/fork-join2.service';
import { DocumentProcessService } from '../../services/document-process-buffer.service'


@Component({
  selector: 'app-madrid-role07-doc-edit',
  templateUrl: './madrid-role07-doc-edit.component.html',
  styleUrls: ['./madrid-role07-doc-edit.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole07DocEditComponent implements OnInit {
  public input: any
  public validate: any
  public master: any
  public editID: any

  constructor(
    private help: Help,
    private DocumentProcessService: DocumentProcessService
) { }


ngOnInit() {
  this.validate = {}
  this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      public_type_code: '',
      public_source_code: '',
      public_receiver_by: '',
      public_status_code: '',
      request_number: '',
    }
    this.master={
        request_type:[{code:1,name:'1'}],
        job_reciver:[{id:1,name:"Sam"},{id:2,name:"Tom"}]
    }

}

}
