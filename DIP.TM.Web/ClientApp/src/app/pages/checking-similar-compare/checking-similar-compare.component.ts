import { Component, OnInit } from '@angular/core'
import { Auth } from "../../auth";
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-checking-similar-compare",
  templateUrl: "./checking-similar-compare.component.html",
  styleUrls: ["./checking-similar-compare.component.scss"]
})
export class CheckingSimilarCompareComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any[]
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  // List DocumentClassificationWord
  public listDocumentClassification1Word: any[]
  public paginateDocumentClassification1Word: any
  public perPageDocumentClassification1Word: number[]
  // Response DocumentClassificationWord
  //public responseDocumentClassificationWord: any
  //public listDocumentClassification1WordEdit: any

  // List DocumentClassificationImage
  public listDocumentClassification1Image: any[]
  public paginateDocumentClassification1Image: any
  public perPageDocumentClassification1Image: number[]
  // Response DocumentClassificationImage
  //public responseDocumentClassificationImage: any
  //public listDocumentClassification1ImageEdit: any

  // List DocumentClassificationSound
  public listDocumentClassification1Sound: any[]
  public paginateDocumentClassification1Sound: any
  public perPageDocumentClassification1Sound: number[]
  // Response DocumentClassificationSound
  //public responseDocumentClassificationSound: any
  //public listDocumentClassification1SoundEdit: any

  // List DocumentClassificationSmell
  public listDocumentClassification1Smell: any[]
  public paginateDocumentClassification1Smell: any
  public perPageDocumentClassification1Smell: number[]
  // Response DocumentClassificationSmell
  //public responseDocumentClassificationSmell: any
  //public listDocumentClassification1SmellEdit: any

  // List ImageSoundTab
  //public listImageSound1Tab: any[]
  //public paginateImageSound1Tab: any
  //public perPageImageSoundTab: number[]
  // Response ImageSoundTab
  //public responseImageSoundTab: any
  //public listImageSound1TabEdit: any




  // List DocumentClassificationWord
  public listDocumentClassification2Word: any[]
  public paginateDocumentClassification2Word: any
  public perPageDocumentClassification2Word: number[]
  // Response DocumentClassificationWord
  //public responseDocumentClassificationWord: any
  //public listDocumentClassification2WordEdit: any

  // List DocumentClassificationImage
  public listDocumentClassification2Image: any[]
  public paginateDocumentClassification2Image: any
  public perPageDocumentClassification2Image: number[]
  // Response DocumentClassificationImage
  //public responseDocumentClassificationImage: any
  //public listDocumentClassification2ImageEdit: any

  // List DocumentClassificationSound
  public listDocumentClassification2Sound: any[]
  public paginateDocumentClassification2Sound: any
  public perPageDocumentClassification2Sound: number[]
  // Response DocumentClassificationSound
  //public responseDocumentClassificationSound: any
  //public listDocumentClassification2SoundEdit: any

  // List DocumentClassificationSmell
  public listDocumentClassification2Smell: any[]
  public paginateDocumentClassification2Smell: any
  public perPageDocumentClassification2Smell: number[]
  // Response DocumentClassificationSmell
  //public responseDocumentClassificationSmell: any
  //public listDocumentClassification2SmellEdit: any

  // List ImageSoundTab
  //public listImageSound2Tab: any[]
  //public paginateImageSound2Tab: any
  //public perPageImageSoundTab: number[]
  // Response ImageSoundTab
  //public responseImageSoundTab: any
  //public listImageSound2TabEdit: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = [{}, {}]

    ////////////1//////////////
    this.listDocumentClassification1Word = []
    this.paginateDocumentClassification1Word = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentClassification1Word = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentClassification1Image = []
    this.paginateDocumentClassification1Image = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentClassification1Image = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentClassification1Sound = []
    this.paginateDocumentClassification1Sound = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentClassification1Sound = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentClassification1Smell = []
    this.paginateDocumentClassification1Smell = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentClassification1Smell = CONSTANTS.PAGINATION.PER_PAGE

    ////////////2//////////////
    this.listDocumentClassification2Word = []
    this.paginateDocumentClassification2Word = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentClassification2Word = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentClassification2Image = []
    this.paginateDocumentClassification2Image = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentClassification2Image = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentClassification2Sound = []
    this.paginateDocumentClassification2Sound = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentClassification2Sound = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentClassification2Smell = []
    this.paginateDocumentClassification2Smell = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentClassification2Smell = CONSTANTS.PAGINATION.PER_PAGE

    //this.listImageSound1Tab = []
    //this.paginateImageSound1Tab = CONSTANTS.PAGINATION.INIT
    //this.perPageImageSoundTab = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    //this.forkJoinService.initCheckingSimilarCompare().subscribe((data: any) => {
    //  if (data) {
    //    this.help.Clone(data, this.master)

    //  }

    this.route.queryParams.subscribe((param_url: any) => {


      if (this.editID) {
        this.CheckingProcessService.CheckingSimilarLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data, 0)
          }

          this.CheckingProcessService.CheckingSimilarLoad(param_url.compare_id).subscribe((data: any) => {
            if (data) {
              // Manage structure
              this.loadData(data, 1)
            }
            // Close loading
            this.global.setLoading(false)
            this.automateTest.test(this)
          })
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

      //})
    })
  }

  constructor(
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }


  onClickCheckingSimilarCompareBack(): void {
    var win = window.open("./checking-similar/edit/" + this.editID, "_self")
  }
  validateCheckingSimilarCompareBack(): boolean {
    let result = validateService('validateCheckingSimilarCompareBack', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarCompareBack(params: any): void {
    //this.CheckingProcessService.CheckingSimilarCompareBack(params).subscribe((data: any) => {
    //  // if(isValidCheckingSimilarCompareBackResponse(res)) {
    //  if (data) {
    //    // Set value
    //    this.loadData(data)
    //  }
    //  // }
    //  // Close loading
    //  this.global.setLoading(false)
    //})
  }

  onClickCheckingSimilarCompareDocumentClassificationWordAdd(): void {
    this.listDocumentClassification1Word.push({
      index: this.listDocumentClassification1Word.length + 1,
      word_language: null,
      word_mark: null,
      word_first_code: null,
      word_sound_last_code: null,
      word_sound_last_other_code: null,
      word_sound: null,
      word_syllable_sound_count: null,
      word_syllable_sound: null,

    })
    this.changePaginateTotal(this.listDocumentClassification1Word.length, 'paginateDocumentClassification1Word')
  }

  onClickCheckingSimilarCompareDocumentClassificationImageAdd(): void {
    this.listDocumentClassification1Image.push({
      index: this.listDocumentClassification1Image.length + 1,
      document_classification_image_code: null,
      document_classification_image_name: null,

    })
    this.changePaginateTotal(this.listDocumentClassification1Image.length, 'paginateDocumentClassification1Image')
  }

  onClickCheckingSimilarCompareDocumentClassificationSoundAdd(): void {
    this.listDocumentClassification1Sound.push({
      index: this.listDocumentClassification1Sound.length + 1,
      document_classification_sound_code: null,
      document_classification_sound_name: null,

    })
    this.changePaginateTotal(this.listDocumentClassification1Sound.length, 'paginateDocumentClassification1Sound')
  }

  onClickCheckingSimilarCompareDocumentClassificationSmellAdd(): void {
    this.listDocumentClassification1Smell.push({
      index: this.listDocumentClassification1Smell.length + 1,
      document_classification_smell_code: null,
      document_classification_smell_name: null,

    })
    this.changePaginateTotal(this.listDocumentClassification1Smell.length, 'paginateDocumentClassification1Smell')
  }

  onClickCheckingSimilarCompareImageSoundTabAdd(): void {
    //this.listImageSound1Tab.push({

    //})
    //this.changePaginateTotal(this.listImageSound1Tab.length, 'paginateImageSound1Tab')
  }


  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any, index: number): void {
    this.input[index] = data

    this.input[index].allow_role_28_request_item_sub_type_1_code = data.full_view.case28_list.map(r => r.request_item_sub_type_1_code).join(", ")

    if (index == 0) {
      this.listDocumentClassification1Word = data.full_view.document_classification_word_list || []
      this.listDocumentClassification1Image = data.full_view.document_classification_image_list || []
      this.listDocumentClassification1Sound = data.full_view.document_classification_sound_list || []
      //this.listDocumentClassification1Smell = data.full_view.document_classification_smell_list || []
      //this.listImageSound1Tab = data.imagesoundtab_list || []
      this.changePaginateTotal(this.listDocumentClassification1Word.length, 'paginateDocumentClassification1Word')
      this.changePaginateTotal(this.listDocumentClassification1Image.length, 'paginateDocumentClassification1Image')
      this.changePaginateTotal(this.listDocumentClassification1Sound.length, 'paginateDocumentClassification1Sound')
      //this.changePaginateTotal(this.listDocumentClassification1Smell.length, 'paginateDocumentClassification1Smell')
      //this.changePaginateTotal(this.listImageSound1Tab.length, 'paginateImageSound1Tab')
    } else if (index == 1) {
      this.listDocumentClassification2Word = data.full_view.document_classification_word_list || []
      this.listDocumentClassification2Image = data.full_view.document_classification_image_list || []
      this.listDocumentClassification2Sound = data.full_view.document_classification_sound_list || []
      //this.listDocumentClassification2Smell = data.full_view.document_classification_smell_list || []
      //this.listImageSound2Tab = data.imagesoundtab_list || []
      this.changePaginateTotal(this.listDocumentClassification2Word.length, 'paginateDocumentClassification2Word')
      this.changePaginateTotal(this.listDocumentClassification2Image.length, 'paginateDocumentClassification2Image')
      this.changePaginateTotal(this.listDocumentClassification2Sound.length, 'paginateDocumentClassification2Sound')
      //this.changePaginateTotal(this.listDocumentClassification2Smell.length, 'paginateDocumentClassification2Smell')
      //this.changePaginateTotal(this.listImageSound2Tab.length, 'paginateImageSound2Tab')
    }
  }


  togglePlayer(index: any): void {
    var item = this.input[index]

    if (!item.player) {
      item.player = new Audio(item.sound_file_physical_path)
      item.player_action = null
    }

    if (item.player_action) {
      item.player.pause()
    } else {
      item.player.play()
    }
    item.player_action = !item.player_action
  }



  listData(data: any): void {
    //this.listDocumentClassification1Word = data.list || []
    //this.listDocumentClassification1Image = data.list || []
    //this.listDocumentClassification1Sound = data.list || []
    //this.listDocumentClassification1Smell = data.list || []
    ////this.listImageSound1Tab = data.list || []
    //this.changePaginateTotal(this.listDocumentClassification1Word.length, 'paginateDocumentClassification1Word')
    //this.changePaginateTotal(this.listDocumentClassification1Image.length, 'paginateDocumentClassification1Image')
    //this.changePaginateTotal(this.listDocumentClassification1Sound.length, 'paginateDocumentClassification1Sound')
    //this.changePaginateTotal(this.listDocumentClassification1Smell.length, 'paginateDocumentClassification1Smell')
    ////this.changePaginateTotal(this.listImageSound1Tab.length, 'paginateImageSound1Tab')

  }

  saveData(): any {
    //let params = this.input

    //params.documentclassificationword_list = this.listDocumentClassification1Word || []
    //params.documentclassificationimage_list = this.listDocumentClassification1Image || []
    //params.documentclassificationsound_list = this.listDocumentClassification1Sound || []
    //params.documentclassificationsmell_list = this.listDocumentClassification1Smell || []
    //params.imagesoundtab_list = this.listImageSound1Tab || []

    //return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
