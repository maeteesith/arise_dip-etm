import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { GlobalService } from "../../global.service";
import { ForkJoinService } from "../../services/fork-join2.service";
import { ReferenceMasterService } from "../../services/reference-master.service";
import { DocumentProcessService } from "../../services/document-process-buffer.service";
import { CheckingProcessService } from "../../services/checking-process-buffer.service";
import { AutomateTest } from "../../test/automate_test";
import { Help } from '../../helpers/help'
import { CONSTANTS, validateService, clone } from "../../helpers";
import { identifierModuleUrl } from "@angular/compiler";
// import * as html2canvas from "html2canvas"

@Component({
  selector: "app-checking-similar",
  templateUrl: "./checking-similar.component.html",
  styleUrls: ["./checking-similar.component.scss"]
})
export class CheckingSimilarComponent implements OnInit {
  //TODO >>> Declarations <<<
  // Init
  public editID: number;
  public input: any;
  public validate: any;
  public master: any;
  public response: any;
  public modal: any;
  // Autocomplete
  public autocompleteList: any;
  public isShowAutocomplete: any;
  public isSearchAutocomplete: any;
  // Autocomplete
  public autocompleteListListModalLocation: any;
  public is_autocomplete_ListModalLocation_show: any;
  public is_autocomplete_ListModalLocation_load: any;
  public autocompleteListSave010List: any;
  public is_autocomplete_Save010List_show: any;
  public is_autocomplete_Save010List_load: any;
  // List WordMark
  public listWordMark: any[];
  public paginateWordMark: any;
  public perPageWordMark: number[];
  // List ImageMark
  public listImageMark: any[];
  public paginateImageMark: any;
  public perPageImageMark: number[];
  // List SoundMark
  public listSoundMark: any[];
  public paginateSoundMark: any;
  public perPageSoundMark: number[];
  // List WordSoundTranslate
  public listWordSoundTranslate: any[];
  public paginateWordSoundTranslate: any;
  public perPageWordSoundTranslate: number[];
  // List Product
  public listProduct: any[];
  public paginateProduct: any;
  public perPageProduct: number[];

  public paginatePopupProduct: any;
  public perPagePopupProduct: number[];

  // List Pharmacy
  public listPharmacy: any[];
  public paginatePharmacy: any;
  public perPagePharmacy: number[];
  // List Organize
  public listOrganize: any[];
  public paginateOrganize: any;
  public perPageOrganize: number[];
  // Other
  public timeout: any;
  public inputModalAddress: any;
  public inputModalAddressEdit: any;
  public inputAddress: any;
  public checkingWordSameConditionCodeList: any[];
  public checkingOrganizeTypeCodeList: any[];
  public documentClassificationImage: any;
  public documentClassificationImageIndex: any;
  public documentClassificationImageList: any[];
  public documentClassificationImageAutocompleteIndex: any;
  public documentClassificationImageAutocompleteList: any[];
  public documentClassificationSound: any;
  public documentClassificationSoundIndex: any;
  public documentClassificationSoundList: any[];
  public documentClassificationSoundAutocompleteIndex: any;
  public documentClassificationSoundAutocompleteList: any[];

  public player: any
  public player_action: any

  public url: any

  public popup: any

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private referenceMasterService: ReferenceMasterService,
    private CheckingProcessService: CheckingProcessService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  ngOnInit() {
    // Init
    this.editID = +this.route.snapshot.paramMap.get("id");
    this.input = {
      id: null,
      request_number: "",
      name: "",
      request_item_sub_type_1_code: "",
      request_item_sub_type_description: "",
      sound_description: "",
      full_view: {},
      search_1_word_first_code: "d",
    };
    this.validate = {};
    this.master = {
      checkingWordSameConditionCodeList: [],
      checkingOrganizeTypeCodeList: []
    };
    this.response = {};
    this.modal = {
      isModalPeopleEditOpen: false
    };
    // Autocomplete
    this.autocompleteList = {
      search_1_word_first_code: [],
      search_1_word_sound_last_code: [],
      search_1_word_sound_last_other_code: [],
      search_1_1_word_mark: [],
      search_1_2_word_syllable_sound: []
    };
    this.isShowAutocomplete = {
      search_1_word_first_code: false,
      search_1_word_sound_last_code: false,
      search_1_word_sound_last_other_code: false,
      search_1_1_word_mark: false,
      search_1_2_word_syllable_sound: false
    };
    this.isSearchAutocomplete = {
      search_1_word_first_code: false,
      search_1_word_sound_last_code: false,
      search_1_word_sound_last_other_code: false,
      search_1_1_word_mark: false,
      search_1_2_word_syllable_sound: false
    };
    // Autocomplete
    this.autocompleteListSave010List = [];
    this.is_autocomplete_Save010List_show = false;
    this.is_autocomplete_Save010List_load = false;
    this.autocompleteListListModalLocation = [];
    this.is_autocomplete_ListModalLocation_show = false;
    this.is_autocomplete_ListModalLocation_load = false;
    // List WordMark
    this.listWordMark = [];
    this.paginateWordMark = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateWordMark.id = "paginateWordMark";
    this.perPageWordMark = clone(CONSTANTS.PAGINATION.PER_PAGE);
    // List ImageMark
    this.listImageMark = [];
    this.paginateImageMark = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateImageMark.id = "paginateImageMark";
    this.perPageImageMark = clone(CONSTANTS.PAGINATION.PER_PAGE);
    // List SoundMark
    this.listSoundMark = [];
    this.paginateSoundMark = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateSoundMark.id = "paginateSoundMark";
    this.perPageSoundMark = clone(CONSTANTS.PAGINATION.PER_PAGE);
    // List WordSoundTranslate
    this.listWordSoundTranslate = [];
    this.paginateWordSoundTranslate = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateWordSoundTranslate.id = "paginateWordSoundTranslate";
    this.perPageWordSoundTranslate = clone(CONSTANTS.PAGINATION.PER_PAGE);
    // List Product
    this.listProduct = [];
    this.paginateProduct = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateProduct.id = "paginateProduct";
    this.perPageProduct = clone(CONSTANTS.PAGINATION.PER_PAGE);


    this.paginatePopupProduct = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePopupProduct.id = 'paginateRequest'
    this.perPagePopupProduct = clone(CONSTANTS.PAGINATION.PER_PAGE)


    // List Pharmacy
    this.listPharmacy = [];
    this.paginatePharmacy = clone(CONSTANTS.PAGINATION.INIT);
    this.paginatePharmacy.id = "paginatePharmacy";
    this.perPagePharmacy = clone(CONSTANTS.PAGINATION.PER_PAGE);
    // List Organize
    this.listOrganize = [];
    this.paginateOrganize = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateOrganize.id = "paginateOrganize";
    this.perPageOrganize = clone(CONSTANTS.PAGINATION.PER_PAGE);
    // Other
    this.inputAddress = {};
    this.inputModalAddress = {};
    this.documentClassificationImage = {};
    this.documentClassificationImageIndex = 0;
    this.documentClassificationImageList = [];
    //this.documentClassificationImageAutocompleteIndex = -1;
    this.documentClassificationSound = {};
    this.documentClassificationSoundIndex = 0;
    this.documentClassificationSoundList = [];
    //this.documentClassificationSoundAutocompleteIndex = -1;


    this.popup = {
      //is_translate_dictionary_show: true
    }

    //setTimeout(function () {
    //  console.log("d")
    //  //html2canvas(document.body, {
    //  //  onrendered: function (canvas) {
    //  //    var img = canvas.toDataURL()
    //  //    window.open(img);
    //  //  }
    //  //});
    //  //console.log("d")
    //  //html2canvas(document.body).then((canvas) => {
    //  //  window.open().document.write('<img src="' + canvas.toDataURL() + '" />');
    //  //});
    //  html2canvas(document.getElementById("pdf")).then((canvas) => {
    //    window.open().document.write('<img src="' + canvas.toDataURL() + '" />');
    //  });
    //  console.log("d")
    //}, 3000)

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initCheckingSimilar().subscribe((data: any) => {
      if (data) {
        this.master = data;
        //this.master.checkingWordTranslateDictionaryCodeList.push({ "code": "OTHERS", "name": "อื่นๆ" });
      }
      if (this.editID) {
        this.CheckingProcessService.CheckingSimilarLoad(this.editID).subscribe(
          (data: any) => {
            if (data) {
              // Manage structure
              this.loadData(data);

              this.onClickFetchAutocomplete(
                'search_1_request_item_sub_type_1_code'
              )
              this.documentClassificationImageList = [... this.input.full_view.document_classification_image_list]
              this.documentClassificationSoundList = [... this.input.full_view.document_classification_sound_list]

            }
            // Close loading
            this.global.setLoading(false);
            this.automateTest.test(this);
          }
        );
      } else {
        this.global.setLoading(false);
        this.automateTest.test(this);
      }
    });

  }

  //! <<< Call API >>>
  callCheckingSimilarSearchTransactionList(params: any): void {
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarSearchTransactionList(
      params
    ).subscribe((data: any) => {
      // if(isValidCheckingSimilarSearchTransactionListResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data);
      }
      // }
      // Close loading
      this.global.setLoading(false);
    });
  }
  callCheckingSimilarSearchImageList(params: any): void {
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarSearchImageList(
      params
    ).subscribe((data: any) => {
      // if(isValidCheckingSimilarSearchImageListResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data);
      }
      // }
      // Close loading
      this.global.setLoading(false);
    });
  }
  callCheckingSimilarSearchSoundList(type: any, params: any): void {
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarSearchSoundList(
      params
    ).subscribe((data: any) => {
      // if(isValidCheckingSimilarSearchSoundListResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data);
      }
      // }
      // Close loading
      this.global.setLoading(false);
    });
  }
  callCheckingSimilarSearchAsk(params: any): void {
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarSearchAsk(params).subscribe(
      (data: any) => {
        // if(isValidCheckingSimilarSearchAskResponse(res)) {
        if (data) {
          // Set value
          pThis.loadData(data);
        }
        // }
        // Close loading
        this.global.setLoading(false);
      }
    );
  }
  callCheckingSimilarSave010PDFView(params: any): void {
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarSave010PDFView(params).subscribe(
      (data: any) => {
        // if(isValidCheckingSimilarSave010PDFViewResponse(res)) {
        if (data) {
          // Set value
          pThis.loadData(data);
        }
        // }
        // Close loading
        this.global.setLoading(false);
      }
    );
  }
  callCheckingSimilarResultLink(params: any): void {
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarResultLink(params).subscribe(
      (data: any) => {
        // if(isValidCheckingSimilarResultLinkResponse(res)) {
        if (data) {
          // Set value
          pThis.loadData(data);
        }
        // }
        // Close loading
        this.global.setLoading(false);
      }
    );
  }
  callCheckingSimilarWordSoundTranslateDelete(params: any, ids: any[]): void {
    this.CheckingProcessService.CheckingSimilarWordSoundTranslateDelete(
      params
    ).subscribe((data: any) => {
      // if(isValidCheckingSimilarWordSoundTranslateDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listWordSoundTranslate.length; i++) {
          if (this.listWordSoundTranslate[i].id == id) {
            this.listWordSoundTranslate.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listWordSoundTranslate.length; i++) {
        this.listWordSoundTranslate[i] = i + 1;
      }

      // Close loading
      this.global.setLoading(false);
    });
  }

  //! <<< Call API (Autocomplete) >>>
  callSave010DocumentClassificationWordFirst(params: any, name: any): void {
    this.CheckingProcessService
      .ClassificationListWordFirst(params)
      .subscribe((data: any) => {
        if (data && data.list) {
          this.autocompleteList[name] = data.list;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }
  callSave010DocumentClassificationSoundLast(params: any, name: any): void {
    this.referenceMasterService
      .Save010DocumentClassificationSoundLast(params)
      .subscribe((data: any) => {
        if (data) {
          this.autocompleteList[name] = data;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }
  callSave010DocumentClassificationSoundLastOther(
    params: any,
    name: any
  ): void {
    this.CheckingProcessService
      .ClassificationListSoundLastOther(params)
      .subscribe((data: any) => {
        if (data && data.list) {
          this.autocompleteList[name] = data.list;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }
  callSave010DocumentClassificationImage(
    params: any,
    name: any
  ): void {
    this.referenceMasterService
      .Save010DocumentClassificationImage(params)
      .subscribe((data: any) => {
        if (data) {
          this.autocompleteList[name] = data;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }
  callSave010DocumentClassificationSound(
    params: any,
    name: any
  ): void {
    this.referenceMasterService
      .Save010DocumentClassificationSound(params)
      .subscribe((data: any) => {
        if (data) {
          this.autocompleteList[name] = data;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }

  //! <<< Prepare Call API >>>
  saveData(): any {
    let params = this.input;

    params.wordmark_list = this.listWordMark || [];
    params.imagemark_list = this.listImageMark || [];
    params.soundmark_list = this.listSoundMark || [];
    params.checking_similar_wordsoundtranslate_list =
      this.listWordSoundTranslate || [];
    params.product_list = this.listProduct || [];
    params.pharmacy_list = this.listPharmacy || [];
    params.organize_list = this.listOrganize || [];

    return params;
  }
  loadData(data: any): void {
    this.input = data;

    this.input.search_1_1_word_samecondition_code = "WORD_SAME"
    this.input.search_1_2_word_samecondition_code = "WORD_SAME"


    this.input.search_1_document_classification_sound_condition = "AND"
    this.input.search_1_document_classification_image_condition = "AND"

    this.listWordMark = data.wordmark_list || [];
    this.listImageMark = data.imagemark_list || [];
    this.listSoundMark = data.soundmark_list || [];
    this.listWordSoundTranslate =
      data.full_view.checking_similar_wordsoundtranslate_list || [];
    this.listProduct = data.product_list || [];
    this.listPharmacy = data.pharmacy_list || [];
    this.listOrganize = data.organize_list || [];
    this.changePaginateTotal(this.listWordMark.length, "paginateWordMark");
    this.changePaginateTotal(this.listImageMark.length, "paginateImageMark");
    this.changePaginateTotal(this.listSoundMark.length, "paginateSoundMark");
    this.changePaginateTotal(
      this.listWordSoundTranslate.length,
      "paginateWordSoundTranslate"
    );
    this.changePaginateTotal(this.listProduct.length, "paginateProduct");
    this.changePaginateTotal(this.listPharmacy.length, "paginatePharmacy");
    this.changePaginateTotal(this.listOrganize.length, "paginateOrganize");

    if (this.input.sound_file_physical_path && this.input.sound_file_physical_path != '') {
      this.player = new Audio(this.input.sound_file_physical_path)
      this.player_action = null
    }

    this.paginatePopupProduct.item_total = this.input.full_view.product_list.length
    this.paginatePopupProduct.page_index = 1
    this.paginatePopupProduct.item_per_page = 10
    this.help.PageSet({
      list: this.input.full_view.product_list,
      paging: {
        page_index: this.paginatePopupProduct.page_index,
        item_per_page: this.paginatePopupProduct.item_per_page,
      }
    }, this.paginatePopupProduct)
    this.onClickPopupProductList()
  }

  onClickPopupProductList(): void {
    this.input.product_list = []

    for (var i = (this.paginatePopupProduct.page_index - 1) * this.paginatePopupProduct.item_per_page;
      i < this.paginatePopupProduct.page_index * this.paginatePopupProduct.item_per_page;
      i++) {
      if (this.input.full_view.product_list[i])
        this.input.product_list.push(this.input.full_view.product_list[i])
    }
  }

  togglePlayer(): void {
    if (this.player_action) {
      this.player.pause()
    } else {
      this.player.play()
    }
    this.player_action = !this.player_action
  }

  listData(data: any): void {
    this.listWordMark = data || [];
    this.listImageMark = data || [];
    this.listSoundMark = data || [];
    this.listWordSoundTranslate = data || [];
    this.listProduct = data || [];
    this.listPharmacy = data || [];
    this.listOrganize = data || [];
    this.changePaginateTotal(this.listWordMark.length, "paginateWordMark");
    this.changePaginateTotal(this.listImageMark.length, "paginateImageMark");
    this.changePaginateTotal(this.listSoundMark.length, "paginateSoundMark");
    this.changePaginateTotal(
      this.listWordSoundTranslate.length,
      "paginateWordSoundTranslate"
    );
    this.changePaginateTotal(this.listProduct.length, "paginateProduct");
    this.changePaginateTotal(this.listPharmacy.length, "paginatePharmacy");
    this.changePaginateTotal(this.listOrganize.length, "paginateOrganize");
  }
  onClickCheckingSimilarSearchTransactionList(displayType: any, is_have_sound: any = false): void {
    console.log('onClickCheckingSimilarSearchTransactionList click type', displayType)
    var filter_list = [];

    var column_list = [
      "search_1_request_number_start",
      "search_1_request_number_end",
      "search_1_word_mark",
      "search_1_people_name",
      "search_1_request_item_sub_type_1_code",
      "search_1_request_item_sub_type_1_description_text",
      "search_1_word_first_code",
      "search_1_word_sound_last_code",
      "search_1_word_sound_last_other_code",
      "search_1_1_word_samecondition_code",
      "search_1_1_word_mark",
      "search_1_2_word_samecondition_code",
      "search_1_2_word_syllable_sound",
      //"search_1_document_classification_image_code",
      "search_1_document_classification_image_condition",
      //"search_1_document_classification_sound_code",
      "search_1_document_classification_sound_condition"
    ];

    Object.keys(this.input).forEach((item: any) => {
      if (column_list.indexOf(item) >= 0 && this.input[item] != "")
        filter_list.push(item + "=" + this.input[item].replace(/#/g, "_"));
    });

    let document_classification_image_code = [];
    this.documentClassificationImageList
      .filter(r => r.document_classification_image_code && r.is_check)
      .forEach((item: any) => {
        document_classification_image_code.push(
          item.document_classification_image_code
        );
      });
    if (document_classification_image_code.length > 0)
      filter_list.push(
        "search_1_document_classification_image_code=" +
        document_classification_image_code.join("|")
      );

    let document_classification_sound_code = [];
    this.documentClassificationSoundList
      .filter(r => r.document_classification_sound_code && r.is_check)
      .forEach((item: any) => {
        document_classification_sound_code.push(
          item.document_classification_sound_code
        );
      });
    console.log(document_classification_sound_code)
    if (document_classification_sound_code.length > 0)
      filter_list.push(
        "search_1_document_classification_sound_code=" +
        document_classification_sound_code.join("|")
      );

    var win = window.open(
      "./checking-similar-word/edit/" +
      this.editID +
      "?display_type=" + displayType + "&" +
      filter_list.join("&") +
      (is_have_sound ? "&is_have_sound=1" : "")
    );
  }
  onClickCheckingSimilarSearchImageList(displayType: any): void {
    console.log('onClickCheckingSimilarSearchImageList click type', displayType)
    var filter_list = [];
    var column_list = [
      "search_1_request_number_start",
      "search_1_request_number_end",
      "search_1_word_mark",
      "search_1_people_name",
      "search_1_request_item_sub_type_1_code",
      "search_1_request_item_sub_type_1_description_text",
      "search_1_word_first_code",
      "search_1_word_sound_last_code",
      "search_1_word_sound_last_other_code",
      "search_1_1_word_samecondition_code",
      "search_1_1_word_mark",
      "search_1_2_word_samecondition_code",
      "search_1_2_word_syllable_sound",
      "search_1_document_classification_image_code",
      "search_1_document_classification_image_condition",
      "search_1_document_classification_sound_code",
      "search_1_document_classification_sound_condition"
    ];

    Object.keys(this.input).forEach((item: any) => {
      if (column_list.indexOf(item) >= 0 && this.input[item] != "")
        filter_list.push(item + "=" + this.input[item].replace(/#/g, "_"));
    });

    let document_classification_image_code = [];
    this.documentClassificationImageList
      .filter(r => r.document_classification_image_code && r.is_check)
      .forEach((item: any) => {
        document_classification_image_code.push(
          item.document_classification_image_code
        );
      });
    if (document_classification_image_code.length > 0)
      filter_list.push(
        "search_1_document_classification_image_code=" +
        document_classification_image_code.join("|")
      );

    let document_classification_sound_code = [];
    this.documentClassificationSoundList
      .filter(r => r.document_classification_sound_code && r.is_check)
      .forEach((item: any) => {
        document_classification_sound_code.push(
          item.document_classification_sound_code
        );
      });
    console.log(document_classification_sound_code)
    if (document_classification_sound_code.length > 0)
      filter_list.push(
        "search_1_document_classification_sound_code=" +
        document_classification_sound_code.join("|")
      );

    var win = window.open(
      "./checking-similar-image/edit/" +
      this.editID +
      "?display_type=" + displayType + "&" +
      filter_list.join("&")
    );
  }
  onClickCheckingSimilarSearchSoundList(displayType: any): void {
    console.log('onClickCheckingSimilarSearchImageList click displayType', displayType)
    //if(this.validateCheckingSimilarSearchSoundList()) {
    // Open loading
    this.global.setLoading(true);
    // Call api
    this.callCheckingSimilarSearchSoundList(displayType, this.saveData());
    //}
  }
  onClickCheckingSimilarSearchAsk(): void {
    var win = window.open(
      "/request-search/edit/" + this.editID
    );
  }
  onClickCheckingSimilarSave010PDFView(): void {
    //if (this.input.file_save01) {
    //  this.popup.is_image_show = true
    //  this.popup.image_src = this.input.file_save01
    //}
    window.open("File/RequestDocumentCollect/SAVE01/" + this.editID)
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null;
        }
      });
    } else {
      this.validate[name] = null;
    }
  }
  validateCheckingSimilarSearchTransactionList(): boolean {
    let result = validateService(
      "validateCheckingSimilarSearchTransactionList",
      this.input
    );
    this.validate = result.validate;
    return result.isValid;
  }
  validateCheckingSimilarSearchImageList(): boolean {
    let result = validateService(
      "validateCheckingSimilarSearchImageList",
      this.input
    );
    this.validate = result.validate;
    return result.isValid;
  }
  validateCheckingSimilarSearchSoundList(): boolean {
    let result = validateService(
      "validateCheckingSimilarSearchSoundList",
      this.input
    );
    this.validate = result.validate;
    return result.isValid;
  }
  validateCheckingSimilarSearchAsk(): boolean {
    let result = validateService(
      "validateCheckingSimilarSearchAsk",
      this.input
    );
    this.validate = result.validate;
    return result.isValid;
  }
  validateCheckingSimilarSave010PDFView(): boolean {
    let result = validateService(
      "validateCheckingSimilarSave010PDFView",
      this.input
    );
    this.validate = result.validate;
    return result.isValid;
  }
  validateCheckingSimilarResultLink(): boolean {
    let result = validateService(
      "validateCheckingSimilarResultLink",
      this.input
    );
    this.validate = result.validate;
    return result.isValid;
  }

  //! <<< Event >>>
  onClickCheckingSimilarResultLink(): void {
    var win = window.open(
      "./checking-similar-result/edit/" + this.editID
    );
  }
  onClickCheckingSimilarWordMarkAdd(): void {
    this.listWordMark.push({
      index: this.listWordMark.length + 1,
      language: null,
      word_mark: null,
      word_first_code: null,
      word_sound_last_code: null,
      word_sound_last_other_code: null,
      word_sound: null,
      word_syllable_count: null,
      word_syllable_sound: null
    });
    this.changePaginateTotal(this.listWordMark.length, "paginateWordMark");
  }
  onClickCheckingSimilarImageMarkAdd(): void {
    this.listImageMark.push({
      index: this.listImageMark.length + 1,
      document_image_type_code: null,
      document_image_type_name: null
    });
    this.changePaginateTotal(this.listImageMark.length, "paginateImageMark");
  }
  onClickCheckingSimilarSoundMarkAdd(): void {
    this.listSoundMark.push({
      index: this.listSoundMark.length + 1,
      request_sound_type_code: null,
      request_sound_type_name: null
    });
    this.changePaginateTotal(this.listSoundMark.length, "paginateSoundMark");
  }
  onClickCheckingSimilarWordSoundTranslateEdit(item: any): void {
    var win = window.open("/" + item.id);
  }
  onClickCheckingSimilarProductAdd(): void {
    this.listProduct.push({});
    this.changePaginateTotal(this.listProduct.length, "paginateProduct");
  }
  onClickCheckingSimilarPharmacyAdd(): void {
    this.listPharmacy.push({});
    this.changePaginateTotal(this.listPharmacy.length, "paginatePharmacy");
  }
  onClickCheckingSimilarOrganizeAdd(): void {
    this.listOrganize.push({});
    this.changePaginateTotal(this.listOrganize.length, "paginateOrganize");
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item];
    });
    this.toggleModal("isModalPeopleEditOpen");
  }

  onClickCheckingSimilarImageDelete(item: any): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    if (
      this.documentClassificationImageList.filter(r => r.is_check).length > 0 ||
      item
    ) {
      if (item) {
        this.documentClassificationImageList
          .filter(r => r.is_check)
          .forEach((item: any) => {
            item.is_check = false;
          });
        item.is_check = true;
      }

      let delete_save_item_count = 0;
      if (true) {
        delete_save_item_count = this.documentClassificationImageList.filter(
          r => r.is_check && r.id
        ).length;
      }

      var rs =
        confirm("คุณต้องการลบรายการ?");
      if (rs) {
        let ids = [];

        for (let i = 0; i < this.documentClassificationImageList.length; i++) {
          if (this.documentClassificationImageList[i].is_check) {
            this.documentClassificationImageList.splice(i--, 1);
          }
        }
      }
    }
    this.global.setLoading(false);
  }
  onClickCheckingSimilarSoundDelete(item: any): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    if (
      this.documentClassificationSoundList.filter(r => r.is_check).length > 0 ||
      item
    ) {
      if (item) {
        this.documentClassificationSoundList
          .filter(r => r.is_check)
          .forEach((item: any) => {
            item.is_check = false;
          });
        item.is_check = true;
      }

      let delete_save_item_count = 0;
      if (true) {
        delete_save_item_count = this.documentClassificationSoundList.filter(
          r => r.is_check && r.id
        ).length;
      }

      var rs =
        confirm("คุณต้องการลบรายการ?");
      if (rs) {
        let ids = [];

        for (let i = 0; i < this.documentClassificationSoundList.length; i++) {
          if (this.documentClassificationSoundList[i].is_check) {
            this.documentClassificationSoundList.splice(i--, 1);
          }
        }
      }
    }
    this.global.setLoading(false);
  }


  //! <<< Autocomplete >>>
  // Save010List
  autocompleteChangeSave010List(
    object: any,
    name: any,
    item_per_page: any = 5,
    length: any = 9
  ): void {
    if (object[name].length >= length) {
      this.is_autocomplete_Save010List_show = true;

      let params = {
        filter: { request_number: object[name] },
        paging: {
          item_per_page: item_per_page
        }
      };

      this.callAutocompleteChangeSave010List(params);
    }
  }
  autocompleteBlurSave010List(
    object: any,
    name: any,
    item_per_page: any = 5
  ): void {
    let pThis = this;
    setTimeout(function () {
      pThis.is_autocomplete_Save010List_show = false;
    }, 200);
  }
  callAutocompleteChangeSave010List(params: any): void {
    if (this.input.is_autocomplete_Save010List_load) return;
    this.input.is_autocomplete_Save010List_load = true;
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarSave010List(params).subscribe(
      (data: any) => {
        if (data) {
          if (data.length == 1) {
            setTimeout(function () {
              pThis.autocompleteChooseSave010List(data[0]);
            }, 200);
          } else {
            pThis.autocompleteListSave010List = data;
          }
        }
      }
    );
    this.input.is_autocomplete_Save010List_load = false;
  }
  autocompleteChooseSave010List(data: any): void {
    console.log(data);

    this.input = data;

    this.listWordMark = data.wordmark_list || [];
    this.listImageMark = data.imagemark_list || [];
    this.listSoundMark = data.soundmark_list || [];
    this.listWordSoundTranslate =
      data.checking_similar_wordsoundtranslate_list || [];
    this.listProduct = data.product_list || [];
    this.listPharmacy = data.pharmacy_list || [];
    this.listOrganize = data.organize_list || [];
    this.changePaginateTotal(this.listWordMark.length, "paginateWordMark");
    this.changePaginateTotal(this.listImageMark.length, "paginateImageMark");
    this.changePaginateTotal(this.listSoundMark.length, "paginateSoundMark");
    this.changePaginateTotal(
      this.listWordSoundTranslate.length,
      "paginateWordSoundTranslate"
    );
    this.changePaginateTotal(this.listProduct.length, "paginateProduct");
    this.changePaginateTotal(this.listPharmacy.length, "paginatePharmacy");
    this.changePaginateTotal(this.listOrganize.length, "paginateOrganize");

    this.is_autocomplete_Save010List_show = false;
  }
  // ClassificationImage
  onChangeDocumentClassificationImageAutocomplete(object: any): void {
    if (object.document_classification_image_code.length > 0) {
      this.documentClassificationImageAutocompleteList = [];
      let params = {
        filter: { code: object.document_classification_image_code },
        paging: { item_per_page: 5 }
      };
      this.callDocumentClassificationImageAutocomplete(object, params);
    }
  }
  callDocumentClassificationImageAutocomplete(object: any, params: any): void {
    let pthis = this;
    this.DocumentProcessService.ClassificationListImageType(params).subscribe(
      (data: any) => {
        if (data) {
          if (data.length == 1) {
            setTimeout(function () {
              pthis.onClickDocumentClassificationImageAutocompleteLoad(
                object,
                data[0]
              );
            }, 200);
          } else {
            this.documentClassificationImageAutocompleteList = data;
          }
        }
      }
    );
  }
  onClickDocumentClassificationImageAutocompleteLoad(
    object: any,
    data: any
  ): void {
    object.document_classification_image_code = data.code;
    object.document_classification_image_name = data.name;

    this.documentClassificationImageAutocompleteIndex = -1;
  }
  // DocumentClassificationSound
  onChangeDocumentClassificationSoundAutocomplete(object: any): void {
    this.documentClassificationSoundAutocompleteList = [];
    let params = {
      filter: { code: object.document_classification_sound_code },
      paging: { item_per_page: 5 }
    };
    this.callDocumentClassificationSoundAutocomplete(object, params);
  }
  callDocumentClassificationSoundAutocomplete(object: any, params: any): void {
    let pthis = this;
    this.DocumentProcessService.ClassificationListSoundType(params).subscribe(
      (data: any) => {
        if (data) {
          if (data.length == 1) {
            setTimeout(function () {
              pthis.onClickDocumentClassificationSoundAutocompleteLoad(
                object,
                data[0]
              );
            }, 200);
          } else {
            this.documentClassificationSoundAutocompleteList = data;
          }
        }
      }
    );
  }
  onClickDocumentClassificationSoundAutocompleteLoad(
    object: any,
    data: any
  ): void {
    object.document_classification_sound_code = data.code;
    object.document_classification_sound_name = data.name;

    this.documentClassificationSoundAutocompleteIndex = -1;
  }
  // ChangeListModalLocation
  autocompleteChangeListModalLocation(
    object: any,
    name: any,
    item_per_page: any = 5,
    length: any = 2
  ): void {
    if (object[name].length >= length) {
      console.log("d");
      this.is_autocomplete_ListModalLocation_show = true;

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        }
      };

      this.callAutocompleteChangeListModalLocation(params);
    }
  }
  autocompleteBlurListModalLocation(
    object: any,
    name: any,
    item_per_page: any = 5
  ): void {
    let pThis = this;
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false;
    }, 200);
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return;
    this.input.is_autocomplete_ListModalLocation_load = true;
    let pThis = this;
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false;
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code;
    this.inputModalAddress.address_sub_district_name = data.name;
    this.inputModalAddress.address_district_code = data.district_code;
    this.inputModalAddress.address_district_name = data.district_name;
    this.inputModalAddress.address_province_code = data.province_code;
    this.inputModalAddress.address_province_name = data.province_name;
    this.inputModalAddress.address_country_code = data.country_code;
    this.inputModalAddress.address_country_name = data.country_name;
    this.is_autocomplete_ListModalLocation_show = false;
  }




  //////////////////
  onClickCheckingSimilarWordSoundTranslateSave(): void {
    //if(this.validateCheckingSimilarSave010PDFView()) {
    // Open loading
    this.global.setLoading(true);
    // Call api
    this.callCheckingSimilarWordSoundTranslateSave(this.saveData());
    //}
  }
  callCheckingSimilarWordSoundTranslateSave(params: any): void {
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarWordSoundTranslateSave(
      params
    ).subscribe((data: any) => {
      // if(isValidCheckingSimilarSave010PDFViewResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data);
      }
      // }
      // Close loading
      this.global.setLoading(false);
      pThis.automateTest.test(this, { WordSoundTranslateSave: 1 });
    });
  }
  onClickCheckingSimilarWordSoundTranslateAdd(): void {
    this.listWordSoundTranslate.push({
      word_translate_search: this.input.full_view.document_classification_word_list[0].word_mark,
      word_translate_dictionary_code: "OTHERS",
      word_translate_dictionary_other: null,
      word_translate_sound: null,
      word_translate_translate: null,
      checking_word_translate_status_code: this.master
        .checkingWordTranslateStatusCodeList[0].code
    });
    this.changePaginateTotal(
      this.listWordSoundTranslate.length,
      "paginateWordSoundTranslate"
    );
  }
  onClickCheckingSimilarWordSoundTranslateDelete(item: any): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    if (
      this.listWordSoundTranslate.filter(r => r.is_check).length > 0 ||
      item
    ) {
      if (item) {
        this.listWordSoundTranslate
          .filter(r => r.is_check)
          .forEach((item: any) => {
            item.is_check = false;
          });
        item.is_check = true;
      }

      let delete_save_item_count = 0;
      if (true) {
        delete_save_item_count = this.listWordSoundTranslate.filter(
          r => r.is_check && r.id
        ).length;
      }

      var rs =
        confirm("คุณต้องการลบรายการ?");
      if (rs) {
        let ids = [];

        for (let i = 0; i < this.listWordSoundTranslate.length; i++) {
          if (this.listWordSoundTranslate[i].is_check) {
            this.listWordSoundTranslate[i].is_deleted = true;

            if (!this.listWordSoundTranslate[i].id)
              this.listWordSoundTranslate.splice(i--, 1);
          }
        }

        var i = 1
        this.listWordSoundTranslate.filter(r => !r.is_deleted).forEach((item: any) => {
          item.index = i++
        })
      }
    }
    this.global.setLoading(false);
  }
  onClickCheckingSimilarWordTranslateCapture(row_item: any): void {
    var item = this.master.checkingWordTranslateDictionaryCodeList.filter(r => r.code == row_item.word_translate_dictionary_code)[0]
    console.log(item)
    console.log(row_item)
    window.open(item.value_1.replace('[WORD]', row_item.word_translate_search))
  }
  ////////////////// 

  onClickCheckingSimilarHover(row_item: any, name: string): void {
    setTimeout(function () {
      row_item[name + "_hover"] = true
      setTimeout(function () {
        document.getElementById(name + "_" + row_item.index).focus()
      }, 100)
    }, 100)
  }

  onClickCheckingSimilarHoverOutside(row_item: any, name: string): void {
    row_item[name + "_hover"] = false
  }

  //iframLoaded(): void {
  //  setTimeout(function () {
  //    html2canvas(document.querySelector("#capture")).then(canvas => {
  //      document.body.appendChild(canvas)

  //      $("#content").empty().append(canvas);
  //    });
  //  }, 1000)
  //};
  //validateCheckingSimilarWordSoundTranslateSave(): boolean {
  //let result = validateService('validateCheckingSimilarSave010PDFView', this.input)
  //this.validate = result.validate
  //return result.isValid
  //}

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name];
  }

  autoCompleteUpdateBack($event, name: any): void {
    console.log($event)
    console.log(name)

    this.input[name] = $event.value

    if (name == "search_1_document_classification_image_code") {
      this.documentClassificationImageList.push({
        document_classification_image_code: $event.item.code,
        document_classification_image_name: $event.item.name,
      })
    } else if (name == "search_1_document_classification_sound_code") {
      this.documentClassificationSoundList.push({
        document_classification_sound_code: $event.item.code,
        document_classification_sound_name: $event.item.name,
      })
    }
  }

  autoCompleteUpdateFetch($event, name: any): any {
    if (name === "search_1_word_first_code") {
      this.input.full_view.document_classification_word_list.forEach((item: any) => {
        console.log(item)
        $event.result_list.push({
          code: item.word_first_code,
          name: item.word_first_name,
        })
      })
    } else if (name === "search_1_word_sound_last_code") {
      this.input.full_view.document_classification_word_list.forEach((item: any) => {
        console.log(item)
        $event.result_list.push({
          code: item.word_sound_last_code,
        })
      })
    } else if (name === "search_1_word_sound_last_other_code") {
      this.input.full_view.document_classification_word_list.forEach((item: any) => {
        console.log(item)
        $event.result_list.push({
          code: item.word_sound_last_other_code,
          name: item.word_sound_last_other_name,
        })
      })
    } else if (name === "search_1_document_classification_image_code") {
      this.documentClassificationImageList = [... this.input.full_view.document_classification_image_list]
      $event.is_ok.push(true)
    } else if (name === "search_1_document_classification_sound_code") {
      this.documentClassificationSoundList = [... this.input.full_view.document_classification_sound_list]
      $event.is_ok.push(true)
    }
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(obj: any, name: any, time: any = CONSTANTS.DELAY_CALL_API): void {
    //console.log(obj)
    //console.log(name)
    clearTimeout(this.timeout);
    console.log("a")
    this.timeout = setTimeout(() => {
      //console.log(this[obj])
      //console.log(this[obj][name])
      ////if (this[obj][name]) {
      //console.log("a")
      if (name === "search_1_word_first_code") {
        this.callSave010DocumentClassificationWordFirst(
          this.help.GetFilterParams({ code: this[obj][name] }), name
        );
      } else if (name === "search_1_word_sound_last_code") {
        this.callSave010DocumentClassificationSoundLast(
          {
            name: this[obj][name],
            paging: {
              item_per_page: 10
            }
          },
          name
        );
      } else if (name === "search_1_word_sound_last_other_code") {
        this.callSave010DocumentClassificationSoundLastOther(
          this.help.GetFilterParams({ code: this[obj][name] }), name
          //{
          //  name: this[obj][name],
          //  paging: {
          //    item_per_page: 10
          //  }
          //},
          //name
        );
      } else if (name === "search_1_document_classification_image_code") {
        this.callSave010DocumentClassificationImage(
          {
            name: this[obj][name],
            paging: {
              item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page
            }
          },
          name
        );
      } else if (name === "search_1_document_classification_sound_code") {
        console.log("a")
        this.callSave010DocumentClassificationSound(
          {
            name: this[obj][name],
            paging: {
              item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page
            }
          },
          name
        );
      }
      //} else {
      //  this.onClickOutsideAutocomplete(name);
      //}
    }, time);
  }
  onClickFetchAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = true;
    this.isSearchAutocomplete[name] = true;

    if (name === "search_1_word_first_code") {
      this.input.full_view.document_classification_word_list.forEach(
        (item: any) => {
          item.code = item.word_first_code;
          item.name = item.word_first_name;
        }
      );
      this.autocompleteList[
        name
      ] = this.input.full_view.document_classification_word_list;

      if (this.input.full_view.document_classification_word_list.length == 1) {
        this.onSelectAutocomplete(
          name,
          this.autocompleteList[
          name
          ][0]
        )
        this.isShowAutocomplete[name] = false;
        this.isSearchAutocomplete[name] = false;
      }
    } else if (name === "search_1_word_sound_last_code") {
      this.input.full_view.document_classification_word_list.forEach(
        (item: any) => {
          item.code = item.word_sound_last_code;
        }
      );
      this.autocompleteList[
        name
      ] = this.input.full_view.document_classification_word_list;


      if (this.input.full_view.document_classification_word_list.length == 1) {
        this.onSelectAutocomplete(
          name,
          this.autocompleteList[
          name
          ][0]
        )
        this.isShowAutocomplete[name] = false;
        this.isSearchAutocomplete[name] = false;
      }
    } else if (name === "search_1_word_sound_last_other_code") {
      this.input.full_view.document_classification_word_list.forEach(
        (item: any) => {
          item.code = item.word_sound_last_other_code;
          item.name = item.word_sound_last_other_name;
        }
      );
      this.autocompleteList[
        name
      ] = this.input.full_view.document_classification_word_list;


      if (this.input.full_view.document_classification_word_list.length == 1) {
        this.onSelectAutocomplete(
          name,
          this.autocompleteList[
          name
          ][0]
        )
        this.isShowAutocomplete[name] = false;
        this.isSearchAutocomplete[name] = false;
      }
    } else if (name === "search_1_1_word_mark") {
      this.input.full_view.document_classification_word_list.forEach(
        (item: any) => {
          item.code = item.word_mark;
        }
      );
      this.autocompleteList[
        name
      ] = this.input.full_view.document_classification_word_list;
    } else if (name === "search_1_2_word_syllable_sound") {
      this.input.full_view.document_classification_word_list.forEach(
        (item: any) => {
          item.code = item.word_syllable_sound;
        }
      );
      this.autocompleteList[
        name
      ] = this.input.full_view.document_classification_word_list;
    } else if (name === "search_1_request_item_sub_type_1_code") {
      var item_code_list = this.input.full_view.product_list.map(r => r.request_item_sub_type_1_code)

      item_code_list = item_code_list.filter(function (v, i, s) { return s.indexOf(v) === i })

      this.input.search_1_request_item_sub_type_1_code = item_code_list.join(" ")

      this.global.setLoading(true);
      this.CheckingProcessService.CheckingSimilarGetRequestItemSubTypeGroup(this.input.search_1_request_item_sub_type_1_code).subscribe(
        (data: any) => {
          if (data) {
            this.input.search_1_request_item_sub_type_1_code = data
            //// Manage structure
            //this.loadData(data);

            //this.onClickFetchAutocomplete(
            //  'search_1_request_item_sub_type_1_code'
            //)
          }
          // Close loading
          this.global.setLoading(false);
        }
      );






    } else if (name === "search_1_document_classification_image_code") {
      this.documentClassificationImageList = [... this.input.full_view.document_classification_image_list]
      this.isShowAutocomplete[name] = false;
      this.isSearchAutocomplete[name] = false;
    } else if (name === "search_1_document_classification_sound_code") {
      this.documentClassificationSoundList = [... this.input.full_view.document_classification_sound_list]
      this.isShowAutocomplete[name] = false;
      this.isSearchAutocomplete[name] = false;
    }
  }
  onSelectAutocomplete(name: any, item: any): void {
    this.input[name] = item.code;

    if (name == "search_1_document_classification_image_code") {
      this.input[name] = "";
      this.documentClassificationImageList.push({
        document_classification_image_code: item.code,
        document_classification_image_name: item.name,
      })
    } else if (name == "search_1_document_classification_sound_code") {
      this.input[name] = "";
      this.documentClassificationSoundList.push({
        document_classification_sound_code: item.code,
        document_classification_sound_name: item.name,
      })
    }

    this.onClickOutsideAutocomplete(name);
  }
  onClickOutsideAutocomplete(name: any): void {
    if (!this.isSearchAutocomplete[name]) {
      this.autocompleteList[name] = [];
      this.isShowAutocomplete[name] = false;
    }
    this.isSearchAutocomplete[name] = false;
  }

  //! <<< Table >>>
  onSelectColumnInTable(object: any, value: boolean): void {
    console.log(object)
    object.forEach((item: any) => { item.is_check = value })
    console.log(value)
    //if (!item) {
    //  this.input.is_check_all = !this.input.is_check_all;
    //  this[name].forEach((item: any) => {
    //    item.is_check = this.input.is_check_all;
    //  });
    //} else {
    //  item.is_check = !item.is_check;
    //}
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name];
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    // if (name === 'paginateRequest01Item') {
    //     this.onClickRequest01ItemList()
    //     this.input.is_check_all = false
    // }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    // if ((+page || page === '') && page <= this.getMaxPage(name)) {
    //     this.managePaginateCallApi(name)
    // }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    // this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
    } else {
      // Is close
      this.modal[name] = false;
      if (name === "isModalPeopleEditOpen") {
      }
    }
  }

  //! <<< Other >>>
  onClickReset(): void {
    this.global.setLoading(true);
    this.ngOnInit();
    this.global.setLoading(false);
  }
}
