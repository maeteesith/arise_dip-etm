import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { Auth } from "../../auth";

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role02-document-list",
  templateUrl: "./public-role02-document-list.component.html",
  styleUrls: ["./public-role02-document-list.component.scss"]
})
export class PublicRole02DocumentListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole02Document
  public listPublicRole02Document: any[]
  public paginatePublicRole02Document: any
  public perPagePublicRole02Document: number[]
  // Response PublicRole02Document
  public responsePublicRole02Document: any
  public listPublicRole02DocumentEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public publicRole02DocumentStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public popup: any
  public url: any


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      public_role05_start_date: getMoment(),
      public_role05_end_date: getMoment(),
      book_index: '',
      public_page_index: '',
      public_role02_document_status_code: '',
      request_number: '',
      registration_number: '',
    }
    this.listPublicRole02Document = []
    this.paginatePublicRole02Document = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole02Document.id = 'paginatePublicRole02Document'
    this.perPagePublicRole02Document = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      publicRole02DocumentStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.popup = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole02DocumentList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.publicRole02DocumentStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        //this.input.public_role02_document_status_code = "ALL"

      }
      if (this.editID) {
        this.PublicProcessService.PublicRole02DocumentLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private auth: Auth,
    public sanitizer: DomSanitizer,
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }

  onClickPublicRole02DocumentList(): void {
    // if(this.validatePublicRole02DocumentList()) {
    // Open loading
    // Call api
    this.callPublicRole02DocumentList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole02DocumentList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole02DocumentList(this.help.GetFilterParams(params, this.paginatePublicRole02Document)).subscribe((data: any) => {
      // if(isValidPublicRole02DocumentListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickPublicRole02DocumentSend(): void {
    //if(this.validatePublicRole02DocumentSend()) {
    // Open loading
    this.global.setLoading(true)

    this.listPublicRole02Document.forEach((item: any) => {
      item.public_role02_document_date = this.input.public_role02_document_date
    })
    // Call api
    this.callPublicRole02DocumentSend(this.listPublicRole02Document)
    //}
  }
  validatePublicRole02DocumentSend(): boolean {
    let result = validateService('validatePublicRole02DocumentSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole02DocumentSend(params: any): void {
    this.PublicProcessService.PublicRole02DocumentSend(params).subscribe((data: any) => {
      // if(isValidPublicRole02DocumentSendResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.onClickPublicRole02DocumentList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickPublicRole02DocumentAdd(): void {
    this.listPublicRole02Document.push({
      index: this.listPublicRole02Document.length + 1,
      request_number: null,
      register_post_round_instruction_rule_name: null,
      public_role05_date: null,
      registration_number: null,
      name: null,
      house_number: null,
      register_book_number: null,
      register_book_start_date: null,
      public_role02_document_remark: null,
      register_post_round_document_post_status_name: null,

    })
    this.changePaginateTotal(this.listPublicRole02Document.length, 'paginatePublicRole02Document')
  }

  onClickPublicRole02DocumentEdit(item: any): void {
    //var win = window.open("/" + item.id)
    this.popup.is_product_show = true
    this.popup.row_item = item
    //this.popup.request_number = item.request_number
    //this.popup.pr_d_id = item.pr_d_id

    this.url = this.sanitizer.bypassSecurityTrustResourceUrl('/pdf/TorKor/' + this.popup.row_item.pr_d_id)
    console.log(this.url)

    //window.open('/pdf/TorKor/' + this.popup.pr_d_id)
  }


  onClickPublicRole02DocumentDelete(item: any): void {
    // if(this.validatePublicRole02DocumentDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listPublicRole02Document.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listPublicRole02Document.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listPublicRole02Document.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listPublicRole02Document.length; i++) {
          if (this.listPublicRole02Document[i].is_check) {
            this.listPublicRole02Document[i].cancel_reason = rs
            this.listPublicRole02Document[i].status_code = "DELETE"
            this.listPublicRole02Document[i].is_deleted = true

            if (true && this.listPublicRole02Document[i].id) ids.push(this.listPublicRole02Document[i].id)
            // else this.listPublicRole02Document.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callPublicRole02DocumentDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listPublicRole02Document.length; i++) {
          this.listPublicRole02Document[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callPublicRole02DocumentDelete(params: any, ids: any[]): void {
    this.PublicProcessService.PublicRole02DocumentDelete(params).subscribe((data: any) => {
      // if(isValidPublicRole02DocumentDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listPublicRole02Document.length; i++) {
          if (this.listPublicRole02Document[i].id == id) {
            this.listPublicRole02Document.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listPublicRole02Document.length; i++) {
        this.listPublicRole02Document[i] = i + 1
      }

      this.onClickPublicRole02DocumentList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicRole02Document = data.publicrole02document_list || []
    this.changePaginateTotal(this.listPublicRole02Document.length, 'paginatePublicRole02Document')

  }

  listData(data: any): void {
    this.listPublicRole02Document = data.list || []
    this.help.PageSet(data, this.paginatePublicRole02Document)
    //this.changePaginateTotal(this.listPublicRole02Document.length, 'paginatePublicRole02Document')

  }

  saveData(): any {
    // let params = this.input
    // params.publicrole02document_list = this.listPublicRole02Document || []

    const params = {
      //page_index: +this.paginatePublicRole02Document.currentPage,
      //item_per_page: +this.paginatePublicRole02Document.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'public_role05_date',
      //  value: displayDateServer(this.input.public_role05_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_role05_date',
      //  value: displayDateServer(this.input.public_role05_end_date),
      //  operation: 4
      //}, {
      //  key: 'book_index',
      //  value: this.input.book_index,
      //  operation: 0
      //}, {
      public_role05_start_date: this.input.public_role05_start_date,
      public_role05_end_date: this.input.public_role05_end_date,
      book_index: this.input.book_index,
      public_page_index: this.input.public_page_index,
      public_role02_document_status_code: this.input.public_role02_document_status_code,
      request_number: this.input.request_number,
      registration_number: this.input.registration_number,
      //  key: 'public_page_index',
      //  value: this.input.public_page_index,
      //  operation: 0
      //}, {
      //  key: 'public_role02_document_status_code',
      //  value: this.input.public_role02_document_status_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}, {
      //  key: 'registration_number',
      //  value: this.input.registration_number,
      //  operation: 5
      //}]
    }

    return params
  }



  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicRole02Document') {
      this.onClickPublicRole02DocumentList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
