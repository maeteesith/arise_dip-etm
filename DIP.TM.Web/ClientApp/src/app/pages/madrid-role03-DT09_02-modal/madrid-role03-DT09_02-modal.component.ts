import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-madrid-role03-DT09_02-modal',
  templateUrl: './madrid-role03-DT09_02-modal.component.html',
  styleUrls: ['./madrid-role03-DT09_02-modal.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole03_DT09_02ModalComponent implements OnInit {
  /**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<RoleEditDialogComponent>
	 * @param data: any
	 */
  constructor(
    public dialogRef: MatDialogRef<MadridRole03_DT09_02ModalComponent>,
  ) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;
    this.dialogRef.backdropClick().subscribe(_ => {
      // let cn = confirm('Do you want to close this modal without save ? ')
      // if (cn) {
      //   this.dialogRef.close();
      // }
    })
  }
  closeModal(){
    this.dialogRef.close();
  }
}
