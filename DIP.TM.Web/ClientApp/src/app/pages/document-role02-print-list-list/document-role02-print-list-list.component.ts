import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-document-role02-print-list-list",
  templateUrl: "./document-role02-print-list-list.component.html",
  styleUrls: ["./document-role02-print-list-list.component.scss"]
})
export class DocumentRole02PrintListListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List DocumentRole02PrintList
  public listDocumentRole02PrintList: any[]
  public paginateDocumentRole02PrintList: any
  public perPageDocumentRole02PrintList: number[]
  // Response DocumentRole02PrintList
  public responseDocumentRole02PrintList: any
  public listDocumentRole02PrintListEdit: any


  // List DocumentProcess
  public listDocumentProcess: any[]
  public paginateDocumentProcess: any
  public perPageDocumentProcess: number[]
  // Response DocumentProcess
  public responseDocumentProcess: any

  //label_save_combobox || label_save_radio
  public departmentCodeList: any[]
  public documentRole02PrintListStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      document_role02_print_cover_date: getMoment(),
      department_code: '',
      rule_count: '',
      document_role02_print_list_status_code: '',
      request_number: '',
    }
    this.listDocumentRole02PrintList = []
    this.paginateDocumentRole02PrintList = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentRole02PrintList = CONSTANTS.PAGINATION.PER_PAGE

    this.listDocumentProcess = []

    //Master List
    this.master = {
      departmentCodeList: [],
      documentRole02PrintListStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initDocumentRole02PrintListList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.departmentCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.department_code = ""
        this.master.documentRole02PrintListStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.document_role02_print_list_status_code = "WAIT_POST_NUMBER"

      }
      if (this.editID) {
        this.DocumentProcessService.DocumentRole02PrintListLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  onClickDocumentRole02PrintListList(): void {
    // if(this.validateDocumentRole02PrintListList()) {
    // Open loading
    // Call api
    this.callDocumentRole02PrintListList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callDocumentRole02PrintListList(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintListList(this.help.GetFilterParams(params, this.paginateDocumentRole02PrintList)).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintListListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickDocumentRole02PrintListSend(row_item: any = null): void {
    //if(this.validateDocumentRole02PrintListSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    if (row_item) {
      this.listDocumentRole02PrintList.forEach((item: any) => { item.is_check = false })
      row_item.is_check = true
    }
    this.callDocumentRole02PrintListSend(this.listDocumentRole02PrintList)
    //}
  }
  validateDocumentRole02PrintListSend(): boolean {
    let result = validateService('validateDocumentRole02PrintListSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callDocumentRole02PrintListSend(params: any): void {
    this.DocumentProcessService.DocumentRole02PrintListSend(params).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintListSendResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.onClickDocumentRole02PrintListPrint()
        this.onClickDocumentRole02PrintListList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickDocumentRole02PrintListPrint(): void {
    window.open("/excels/PostDocumentList/" + this.listDocumentRole02PrintList.filter(r => r.is_check).map((item: any) => { return item.id }).join("|"))
  }

  onClickDocumentRole02PrintListAdd(): void {
    //this.listDocumentRole02PrintList.push({
    //  index: this.listDocumentRole02PrintList.length + 1,
    //  request_number: null,
    //  document_role02_print_cover_date_text: null,
    //  document_role02_receiver_by_name: null,
    //  rule_count: null,
    //  name: null,
    //  house_number: null,
    //  document_role02_print_cover_date_text: null,
    //  post_number: null,
    //  document_role02_print_list_status_name: null,

    //})
    this.changePaginateTotal(this.listDocumentRole02PrintList.length, 'paginateDocumentRole02PrintList')
  }

  onClickDocumentRole02PrintListEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickDocumentRole02PrintListDelete(item: any): void {
    // if(this.validateDocumentRole02PrintListDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listDocumentRole02PrintList.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listDocumentRole02PrintList.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listDocumentRole02PrintList.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listDocumentRole02PrintList.length; i++) {
          if (this.listDocumentRole02PrintList[i].is_check) {
            this.listDocumentRole02PrintList[i].cancel_reason = rs
            this.listDocumentRole02PrintList[i].status_code = "DELETE"
            this.listDocumentRole02PrintList[i].is_deleted = true

            if (true && this.listDocumentRole02PrintList[i].id) ids.push(this.listDocumentRole02PrintList[i].id)
            // else this.listDocumentRole02PrintList.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole02PrintListDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listDocumentRole02PrintList.length; i++) {
          this.listDocumentRole02PrintList[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole02PrintListDelete(params: any, ids: any[]): void {
    this.DocumentProcessService.DocumentRole02PrintListDelete(params).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintListDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listDocumentRole02PrintList.length; i++) {
          if (this.listDocumentRole02PrintList[i].id == id) {
            this.listDocumentRole02PrintList.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listDocumentRole02PrintList.length; i++) {
        this.listDocumentRole02PrintList[i] = i + 1
      }

      this.onClickDocumentRole02PrintListList()
      // Close loading
      this.global.setLoading(false)
    })
  }

  //onClickDocumentRole02PrintListPrint(row_item: any): void {
  //  window.open()
  //}

  onKeyPressDocumentRole02PrintPostNumberSend(row_item: any): void {
    this.callDocumentRole02PrintPostNumberSend(row_item)
  }
  callDocumentRole02PrintPostNumberSend(param: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02PrintPostNumberSend(param).subscribe((data: any) => {
      // if(isValidDocumentRole02PrintListListResponse(res)) {
      if (data) {
        // Set value
        //this.listData(data)
        this.help.Clone(data, param)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }


  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listDocumentRole02PrintList = data.documentrole02printlist_list || []
    this.changePaginateTotal(this.listDocumentRole02PrintList.length, 'paginateDocumentRole02PrintList')

  }

  listData(data: any): void {
    this.listDocumentRole02PrintList = data.list || []
    this.help.PageSet(data, this.paginateDocumentRole02PrintList)
    //this.changePaginateTotal(this.listDocumentRole02PrintList.length, 'paginateDocumentRole02PrintList')

  }

  saveData(): any {
    let params = {
      request_number: this.input.request_number,
      document_role02_print_cover_date: this.input.document_role02_print_cover_date,
      department_code: this.input.department_code,
      document_role02_print_list_status_code: this.input.document_role02_print_list_status_code,
      rule_count: this.input.rule_count,
    }

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
