import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  clone,
  displayDateServer
} from '../../helpers'

@Component({
  selector: "app-checking-item-list",
  templateUrl: "./checking-item-list.component.html",
  styleUrls: ["./checking-item-list.component.scss"]
})
export class CheckingItemListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List Checking
  public listChecking: any[]
  public paginateChecking: any
  public perPageChecking: number[]
  // Response Checking
  public responseChecking: any
  public listCheckingEdit: any


  // List CheckingProcess
  public listCheckingProcess: any[]
  public paginateCheckingProcess: any
  public perPageCheckingProcess: number[]
  // Response CheckingProcess
  public responseCheckingProcess: any

  //label_save_combobox || label_save_radio
  public checkingTypeCodeList: any[]
  public checkingStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListCheckingItemLoad: any
  public is_autocomplete_CheckingItemLoad_show: any
  public is_autocomplete_CheckingItemLoad_load: any
  public autocompleteListCheckerList: any
  public is_autocomplete_CheckerList_show: any
  public is_autocomplete_CheckerList_load: any
  public autocompleteListCheckerReceiveList: any
  public is_autocomplete_CheckerReceiveList_show: any
  public is_autocomplete_CheckerReceiveList_load: any
  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      document_classification_date: getMoment(),
      checking_type_code: '',
      request_item_sub_type_1_code: '',
      checking_spliter_by_name: '',
      request_number: '',
      checking_status_code: '',
      checking_receiver_by_name: '',
    }
    this.listChecking = []
    this.paginateChecking = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateChecking.id = 'paginateChecking'
    this.perPageChecking = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listCheckingProcess = []

    //Master List
    this.master = {
      checkingTypeCodeList: [],
      checkingStatusCodeList: [],
    }
    //Master List

    this.autocompleteListCheckingItemLoad = []
    this.is_autocomplete_CheckingItemLoad_show = false
    this.is_autocomplete_CheckingItemLoad_load = false
    this.autocompleteListCheckerList = []
    this.is_autocomplete_CheckerList_show = false
    this.is_autocomplete_CheckerList_load = false
    this.autocompleteListCheckerReceiveList = []
    this.is_autocomplete_CheckerReceiveList_show = false
    this.is_autocomplete_CheckerReceiveList_load = false


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initCheckingItemList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.checkingTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" });
        this.master.checkingStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });

      }
      if (this.editID) {
        let pThis = this
        this.CheckingProcessService.CheckingItemLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          pThis.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }
    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }


  // Autocomplete
  autocompleteChangeCheckingItemLoad(object: any, name: any, item_per_page: any = 5, length: any = 1): void {
    if (object[name].length >= length) {
      this.is_autocomplete_CheckingItemLoad_show = true

      let params = {
        filter: { request_item_sub_type_1_code: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeCheckingItemLoad(params)
    }
  }
  autocompleteBlurCheckingItemLoad(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_CheckingItemLoad_show = false
    }, 200)
  }
  callAutocompleteChangeCheckingItemLoad(params: any): void {
    if (this.input.is_autocomplete_CheckingItemLoad_load) return
    this.input.is_autocomplete_CheckingItemLoad_load = true
    let pThis = this
    this.CheckingProcessService.CheckingItemCheckingItemLoad(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseCheckingItemLoad(data[0])
          }, 200)
        } else {
          pThis.autocompleteListCheckingItemLoad = data
        }
      }
    })
    this.input.is_autocomplete_CheckingItemLoad_load = false
  }
  autocompleteChooseCheckingItemLoad(data: any): void {
    this.input = data

    this.listChecking = data.checking_list || []
    this.changePaginateTotal(this.listChecking.length, 'paginateChecking')


    this.is_autocomplete_CheckingItemLoad_show = false
  }


  // Autocomplete
  autocompleteChangeCheckerList(object: any, name: any, item_per_page: any = 5, length: any = 1): void {
    if (object[name].length >= length) {
      this.is_autocomplete_CheckerList_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeCheckerList(params)
    }
  }
  autocompleteBlurCheckerList(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_CheckerList_show = false
    }, 200)
  }
  callAutocompleteChangeCheckerList(params: any): void {
    if (this.input.is_autocomplete_CheckerList_load) return
    this.input.is_autocomplete_CheckerList_load = true
    let pThis = this
    this.CheckingProcessService.CheckingItemCheckerList(params).subscribe((data: any) => {
      console.log(data)
      if (data && data.list) {
        if (data.list.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseCheckerList(data.list[0])
          }, 200)
        } else {
          pThis.autocompleteListCheckerList = data.list
        }
      }
    })
    this.input.is_autocomplete_CheckerList_load = false
  }
  autocompleteChooseCheckerList(data: any): void {
    this.input.checking_receiver_by = data.id
    this.input.checking_receiver_by_name = data.name

    this.is_autocomplete_CheckerList_show = false
  }


  // Autocomplete
  autocompleteChangeCheckerReceiveList(object: any, name: any, item_per_page: any = 5, length: any = 1): void {
    if (object[name].length >= length) {
      this.is_autocomplete_CheckerReceiveList_show = true


      const params = {
        //page_index: +this.paginateRequest01Item.currentPage,
        item_per_page: +item_per_page,
        //order_by: 'created_date',
        //is_order_reverse: false,
        search_by: [{
          key: 'name',
          value: object[name],
          operation: 5
          //}, {
          //  key: 'request_date',
          //  value: displayDateServer(this.input.request_date),
          //  operation: 0
          //}, {
          //  key: 'reference_number',
          //  value: this.input.reference_number,
          //  operation: 5
          //}, {
          //  key: 'request_number',
          //  value: this.input.request_number,
          //  operation: 5
          //}, {
          //  key: 'receipt_status_code',
          //  value: this.input.receipt_status_code,
          //  operation: 0
        }]
      }

      //let params = {
      //  filter: { name: object[name] },
      //  paging: {
      //    item_per_page: item_per_page
      //  },
      //}

      this.callAutocompleteChangeCheckerReceiveList(params)
    }
  }
  autocompleteBlurCheckerReceiveList(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_CheckerReceiveList_show = false
    }, 200)
  }
  callAutocompleteChangeCheckerReceiveList(params: any): void {
    if (this.input.is_autocomplete_CheckerReceiveList_load) return
    this.input.is_autocomplete_CheckerReceiveList_load = true
    let pThis = this
    this.CheckingProcessService.CheckingItemCheckerList(params).subscribe((data: any) => {
      console.log(data)
      if (data && data.list) {
        if (data.list.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseCheckerReceiveList(data.list[0])
            pThis.automateTest.test(pThis, { receiver: 1 })
          }, 200)
        } else {
          pThis.autocompleteListCheckerReceiveList = data.list
        }
      }
    })
    this.input.is_autocomplete_CheckerReceiveList_load = false
  }
  autocompleteChooseCheckerReceiveList(data: any): void {
    console.log(data)

    this.input.checking_receiver_by = data.id
    this.input.checking_receiver_by_name = data.name

    this.is_autocomplete_CheckerReceiveList_show = false
  }

  onClickCheckingItemAutoSplit(): void {
    //if(this.validateCheckingItemAutoSplit()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingItemAutoSplit(this.saveData())
    //}
  }
  validateCheckingItemAutoSplit(): boolean {
    let result = validateService('validateCheckingItemAutoSplit', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingItemAutoSplit(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingItemAutoSplit(params).subscribe((data: any) => {
      // if(isValidCheckingItemAutoSplitResponse(res)) {
      //if (data) {
      //  // Set value
      //  pThis.loadData(data)
      //}
      //this.input.checking_receiver_by = null
      this.onClickCheckingItemList()
      // }
      // Close loading
      this.global.setLoading(false)
      this.automateTest.test(this, { CheckingItemAutoSplit: 1 })
    })
  }


  onClickCheckingItemReceive(): void {
    //if(this.validateCheckingItemReceive()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    var param = {
      ... this.input,
      checking_list: this.listChecking
    }
    this.callCheckingItemReceive(param)
    //}
  }
  validateCheckingItemReceive(): boolean {
    let result = validateService('validateCheckingItemReceive', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingItemReceive(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingItemReceive(params).subscribe((data: any) => {
      // if(isValidCheckingItemReceiveResponse(res)) {
      //if (data) {
      //  // Set value
      //  pThis.loadData(data)
      //}
      //this.input.checking_receiver_by = null
      this.onClickCheckingItemList()
      // }
      // Close loading
      this.global.setLoading(false)
      this.automateTest.test(this, { CheckingItemReceive: 1 })
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }

  onClickCheckingItemList(): void {
    // if(this.validateCheckingItemList()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingItemList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callCheckingItemList(params: any): void {
    let pThis = this
    params.checking_receiver_by = null
    this.CheckingProcessService.CheckingItemList(this.help.GetFilterParams(params, this.paginateChecking)).subscribe((data: any) => {
      // if(isValidCheckingItemListResponse(res)) {
      if (data) {
        // Set value
        pThis.listData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
      this.automateTest.test(this, { list: 1 })
    })
  }


  onClickCheckingItemCheckingAdd(): void {
    this.listChecking.push({
      index: this.listChecking.length + 1,
      document_classification_date_text: null,
      request_number: null,
      request_date_text: null,
      name: null,
      request_item_sub_type_1_code: null,
      checking_type_name: null,
      checking_status_name: null,
      checking_spliter_by_name: null,
      checking_receiver_name: null,

    })
    this.changePaginateTotal(this.listChecking.length, 'paginateChecking')
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listChecking = data.checking_list || []
    this.changePaginateTotal(this.listChecking.length, 'paginateChecking')

  }

  listData(data: any): void {
    this.listChecking = data.list || []
    this.help.PageSet(data, this.paginateChecking)
    //this.listChecking = data.list || []
    //this.changePaginateTotal(this.listChecking.length, 'paginateChecking')
  }

  saveData(): any {
    // let params = { ... this.input }
    // params.checking_list = this.listChecking || []

    const params = {
      document_classification_date: displayDateServer(this.input.document_classification_date),
      checking_type_code: this.input.checking_type_code,
      request_item_sub_type_1_code_text: this.input.request_item_sub_type_1_code,
      checking_spliter_by_name: this.input.checking_spliter_by_name,
      request_number: this.input.request_number,
      checking_status_code: this.input.checking_status_code,
    }

    return params
  }



  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginateChecking') {
      this.onClickCheckingItemList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
