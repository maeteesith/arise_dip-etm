import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-madrid-role07-modal-edit-information',
  templateUrl: './madrid-role07-modal-edit-information.component.html',
  styleUrls: ['./madrid-role07-modal-edit-information.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole07ModalEditInformationComponent implements OnInit {
  /**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<RoleEditDialogComponent>
	 * @param data: any
	 */
  constructor(
    public dialogRef: MatDialogRef<MadridRole07ModalEditInformationComponent>,
  ) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;
    this.dialogRef.backdropClick().subscribe(_ => {
      // let cn = confirm('Do you want to close this modal without save ? ')
      // if (cn) {
      //   this.dialogRef.close();
      // }
    })
  }
  closeModal(){
    this.dialogRef.close();
  }
}
