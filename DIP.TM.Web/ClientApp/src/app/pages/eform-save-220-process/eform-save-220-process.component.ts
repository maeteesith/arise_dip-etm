import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { GlobalService } from "../../global.service";
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from "../../services/fork-join-eform.service";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { UploadService } from "../../services/upload.service";
import {
  ROUTE_PATH,
  CONSTANTS,
  validateService,
  clone,
  getItemCalculator,
  displayMoney,
  displayFormatBytes,
  PapaParseCsvToJson,
  loopDisplayDateServer,
  displayAddress,
  viewPDF,
  clearIdAndSaveId,
  getParamsOverwrite,
} from "../../helpers";

@Component({
  selector: "app-eform-save-220-process",
  templateUrl: "./eform-save-220-process.component.html",
  styleUrls: ["./eform-save-220-process.component.scss"],
})
export class eFormSave220ProcessComponent
  implements OnInit, DeactivationGuarded {
  //TODO >>> Declarations <<<
  // Init
  public editID: number;
  public menuList: any[];
  public input: any;
  public validate: any;
  public master: any;
  public response: any;
  public modal: any;
  public popup: any;
  // List (ProductService)
  public listProductService: any[];
  // Autocomplete
  public autocompleteList: any;
  public isShowAutocomplete: any;
  public autocompleteFocusIndex: any;
  // Paginate
  public paginateTranslation: any;
  public paginateProduct: any;
  public paginateProductService: any;
  public paginateRegistrationRequest: any;
  public paginateProductShow: any;
  public paginateMark: any;
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubID: number;
  public currentSubStep: number;
  public currentMiniID: number;
  public currentMiniStep: number;
  public progressPercent: number;
  // Other
  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eformSaveProcessService: eFormSaveProcessService,
    private uploadService: UploadService
  ) {}

  ngOnInit() {
    // Init
    this.editID = +this.route.snapshot.paramMap.get("id");
    this.menuList = [
      {
        id: 1,
        number: 1,
        isShow: true,
        name: "บันทึกเพื่อรับไฟล์",
        canEdit: false,
      },
      {
        id: 2,
        number: 2,
        isShow: true,
        name:
          "บันทึกคำขอให้บันทึกการจดทะเบียนระหว่างประเทศแทนการจดทะเบียนในราชอาณาจักร",
        canEdit: false,
      },
      {
        id: 3,
        number: 3,
        isShow: true,
        name: "รายละเอียดเกี่ยวกับคําขอระหว่างประเทศ",
        canEdit: false,
      },
      {
        id: 4,
        number: 4,
        isShow: true,
        name: "เจ้าของ / ตัวแทนเครื่องหมาย",
        canEdit: false,
      },
      {
        id: 5,
        number: 5,
        isShow: true,
        name: "สถานที่ติดต่อภายในประเทศไทย",
        canEdit: false,
      },
      {
        id: 6,
        number: 6,
        isShow: true,
        name: "เครื่องหมายที่ขอจดทะเบียน",
        hasSub: true,
        canEdit: false,
        isUseMenuBeforeSubList: false,
        sizeSubList: 2,
        subList: [
          {
            id: 1,
            number: 1,
            isShow: true,
            name: "ภาพเครื่องหมาย",
            canEdit: false,
          },
          {
            id: 2,
            number: 2,
            isShow: true,
            name: "เครื่องหมายเสียง",
            canEdit: false,
          },
        ],
      },
      {
        id: 7,
        number: 7,
        isShow: true,
        name: "คำอ่านและคำแปลภาษาต่างประเทศ",
        canEdit: false,
      },
      {
        id: 8,
        number: 8,
        isShow: true,
        name: "จำพวกสินค้า/บริการ",
        canEdit: false,
      },
      {
        id: 9,
        number: 9,
        isShow: true,
        name: "เครื่องหมายที่มีลักษณะเป็นกลุ่มของสี",
        canEdit: false,
      },
      {
        id: 10,
        number: 10,
        isShow: true,
        name: "เครื่องหมายที่มีลักษณะเป็นรูปร่างหรือรูปทรงของวัตถุ",
        canEdit: false,
      },
      {
        id: 11,
        number: 11,
        isShow: true,
        name: "การใช้เครื่องหมายโดยการจำหน่าย เผยแพร่ หรือโฆษณา",
        canEdit: false,
      },
      {
        id: 12,
        number: 12,
        isShow: true,
        name:
          "การขอให้ถือว่าวันที่ยื่นคำขอนอกราชอาณาจักรครั้งแรกเป็นวันยื่นคําขอในราชอาณาจักรตามมาตรา 28",
        hasSub: true,
        canEdit: false,
        isUseMenuBeforeSubList: true,
        sizeSubList: 2,
        subList: [
          {
            id: 1,
            number: 1,
            isShow: true,
            name: "แบบฟอร์ม ก.10",
            canEdit: false,
            hasMini: true,
            sizeMiniList: 5,
            miniList: [
              {
                id: 1,
                number: 1,
                name: "เจ้าของ",
                isShow: true,
                canEdit: false,
              },
              {
                id: 2,
                number: 2,
                name: "ผู้ขอจดทะเบียนเครื่องหมายการค้า ขอให้ถือว่า",
                isShow: true,
                canEdit: false,
              },
              {
                id: 3,
                number: 3,
                name: "คำขอจดทะเบียนที่ยื่นนอกราชอาณาจักรครั้งแรก",
                isShow: true,
                canEdit: false,
              },
              {
                id: 4,
                number: 4,
                name: "สินค้าที่นำออกแสดงในงานแสดงสินค้าระหว่างประเทศ",
                isShow: true,
                canEdit: false,
              },
              {
                id: 5,
                number: 5,
                name: "เอกสารหลักฐานประกอบคำขอ มีดังต่อไปนี้",
                isShow: true,
                canEdit: false,
              },
            ],
          },
          {
            id: 2,
            number: 2,
            isShow: true,
            name: "แบบฟอร์ม ก.19",
            canEdit: false,
            hasMini: true,
            sizeMiniList: 2,
            miniList: [
              {
                id: 1,
                number: 1,
                name:
                  "* ในกรณีที่มีการเปลี่ยนแปลงข้อมูลเจ้าของ/ตัวแทน ให้ยื่น ก.06 แนบด้วย",
                isShow: true,
                canEdit: false,
              },
              {
                id: 2,
                number: 2,
                name: "รายละเอียดการขอผ่อนผัน",
                isShow: true,
                canEdit: false,
              },
            ],
          },
        ],
      },
      {
        id: 13,
        number: 13,
        isShow: true,
        name: "เอกสารหลักฐานประกอบคำขอจดทะเบียน",
        canEdit: false,
      },
      {
        id: 14,
        number: 14,
        isShow: true,
        name: "ค่าธรรมเนียม",
        canEdit: false,
      },
      {
        id: 15,
        number: 15,
        isShow: true,
        name: "เสร็จสิ้น",
        canEdit: false,
      },
    ];
    this.input = {
      indexEdit: undefined,
      point: "",
      is_check_all: false,
      isAllowEditRequestNumber: true,
      isAllowEditInterRegistrationNumber: true,
      isAllowEditMultipleInterRegistrationNumber: true,
      search_type_code: "REQUEST_NUMBER",
      listMultipleSearch: [],
      listInterRegistration: [],
      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      save220_representative_condition_type_code: "AND_OR",
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      contact_type_index: 0,
      listAgentMarkRepresentative1: [],
      listAgentMarkRepresentative2: [],
      address: {
        receiver_type_code: "PEOPLE",
      },
      save220_img_type_type_code: "2D",
      imageDimension2: {
        id: null,
        file: {},
        blob: null,
      },
      imageDimension3_1: {
        id: null,
        file: {},
        blob: null,
      },
      img_w: 5,
      img_h: 5,
      isResize: false,
      isBrandLogoSound: false,
      is_sound_mark_human: false,
      is_sound_mark_animal: false,
      is_sound_mark_sound: false,
      is_sound_mark_other: false,
      audio: {
        id: null,
        input: "",
        validate: "",
        file: {},
        sound: null,
        palyer: null,
        action: false,
      },
      imageNote: {
        id: null,
        file: {},
        blob: null,
      },
      listTranslation: [],
      translationItem: {},
      searchProductService: "",
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0,
      },
      isLogoColorGroupTutelage: false,
      isLogoThreeDememsionsGroupTutelage: false,
      listOwnerMarkA10: [],
      listAgentMarkA10: [],
      isDeemed1: false,
      isDeemed2: false,
      listRegistrationRequest: [],
      listProductShow: [],
      listIsA10Checked: [
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
      ],
      listOwnerMarkA19: [],
      listAgentMarkA19: [],
      listMark: [],
      markItem: {
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listDocumentRequestChecked: [
        {
          number: 1,
          isShow: true,
          checked: true,
        },
        {
          number: 2,
          isShow: true,
          checked: false,
        },
        {
          number: 3,
          isShow: true,
          checked: false,
        },
        {
          number: 4,
          isShow: true,
          checked: false,
        },
        {
          number: 5,
          isShow: true,
          checked: false,
        },
        {
          number: 6,
          isShow: true,
          checked: false,
        },
        {
          number: 7,
          isShow: true,
          checked: false,
        },
        {
          number: 8,
          isShow: true,
          checked: false,
        },
        {
          number: 9,
          isShow: true,
          checked: false,
        },
        {
          number: 10,
          isShow: true,
          checked: false,
        },
      ],
      authorize: {
        owner_nationality_code: "TH",
        inheritor_nationality_code: "TH",
      },
      indexAuthorize: 0,
      isCheckAllOwnerSignature: false,
      isCheckAllAgentSignature: false,
    };
    this.validate = {};
    this.master = {
      requestSoundTypeList: [],
      inputTypeCodeList: [],
      addressEformCardTypeCodeList: [],
      addressCountryCodeList: [],
      addressRepresentativeConditionTypeCodeList: [],
      addressTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
      addressCareerCodeList: [],
      addressNationalityCodeList: [],
      requestImageMarkTypeCodeList: [],
      translationLanguageCodeList: [],
      representativeTypeCodeList: [],
      assertTypeCodeList: [],
      priceMasterList: [],
      searchTypeCodeList: [],
    };
    this.response = {
      load: {},
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalCommonMarkFormOpen: false,
      isModalDomesticContactAddressOpen: false,
      isModalProductAndServiceListsOpen: false,
      isModalFeeOpen: false,
    };
    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
    };
    // List (ProductService)
    this.listProductService = [];
    // Autocomplete
    this.autocompleteList = {
      description: [],
      address_sub_district_name: [],
    };
    this.isShowAutocomplete = {
      description: false,
      address_sub_district_name: false,
    };
    this.autocompleteFocusIndex = {
      description: 0,
    };
    // Paginate
    this.paginateTranslation = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateTranslation.id = "paginateTranslation";
    this.paginateProduct = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateProduct.id = "paginateProduct";
    this.paginateProductService = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateProductService.id = "paginateProductService";
    this.paginateRegistrationRequest = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateRegistrationRequest.id = "paginateRegistrationRequest";
    this.paginateProductShow = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateProductShow.id = "paginateProductShow";
    this.paginateMark = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateMark.id = "paginateMark";
    // Wizard
    this.currentID = 2;
    this.currentStep = 2;
    this.currentSubID = 2;
    this.currentSubStep = 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
    this.progressPercent = 0;
    // Other
    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initEForm220Page().subscribe((data: any) => {
      if (data) {
        this.master = data;
        console.log("this.master", this.master);
      }
      if (this.editID) {
        this.eformSaveProcessService
          .eFormSave220Load(this.editID, {})
          .subscribe((data: any) => {
            if (data) {
              this.response.load = {
                eform_number: data.eform_number,
                id: data.id,
                wizard: data.wizard,
                kor10_id:
                  data.kor10_list.length > 0 ? data.kor10_list[0].id : null,
                kor10_save_id:
                  data.kor10_list.length > 0
                    ? data.kor10_list[0].save_id
                    : null,
                kor19_id:
                  data.kor10_list.length > 0 &&
                  data.kor10_list[0].kor19_list.length > 0
                    ? data.kor10_list[0].kor19_list[0].id
                    : null,
                kor19_save_id:
                  data.kor10_list.length > 0 &&
                  data.kor10_list[0].kor19_list.length > 0
                    ? data.kor10_list[0].kor19_list[0].save_id
                    : null,
              };
              this.input.isAllowEditRequestNumber = data.request_number
                ? false
                : true;
              // step 1
              this.input.email = data.email;
              this.input.telephone = data.telephone;
              // step 2
              this.input.request_number = data.request_number;
              // step 3
              //  TODO
              // step 4
              this.input.listOwnerMark = data.people_list;
              this.input.listAgentMark = data.representative_list;
              this.input.save220_representative_condition_type_code = data.save220_representative_condition_type_code
                ? data.save220_representative_condition_type_code
                : "AND_OR";
              // step 5
              this.input.save220_contact_type_code =
                data.save220_contact_type_code;
              this.input.address =
                data.save220_contact_type_code === "OTHERS"
                  ? data.contact_address_list[0]
                  : this.input.address;
              // step 6.1
              this.input.save220_img_type_type_code = data.save220_img_type_type_code
                ? data.save220_img_type_type_code
                : "2D";
              this.input.imageDimension2.id = data.img_file_2d_id;
              this.input.imageDimension2.blob = data.img_file_2d_id
                ? `/File/Content/${data.img_file_2d_id}`
                : null;
              this.input.imageDimension3_1.id = data.img_file_3d_id_1;
              this.input.imageDimension3_1.blob = data.img_file_3d_id_1
                ? `/File/Content/${data.img_file_3d_id_1}`
                : null;
              this.input.img_w = data.img_w ? data.img_w : 5;
              this.input.img_h = data.img_h ? data.img_h : 5;
              this.input.remark_5_1_3 = data.remark_5_1_3;
              // step 6.2
              this.input.isBrandLogoSound = data.is_sound_mark;
              this.input.is_sound_mark_human = data.is_sound_mark_human;
              this.input.is_sound_mark_animal = data.is_sound_mark_animal;
              this.input.is_sound_mark_sound = data.is_sound_mark_sound;
              this.input.is_sound_mark_other = data.is_sound_mark_other;
              this.input.remark_5_2_2 = data.remark_5_2_2;
              this.input.audio.id = data.sound_file ? data.sound_file.id : null;
              this.input.audio.file = data.sound_file
                ? {
                    name: data.sound_file.file_name,
                    size: data.sound_file.file_size,
                  }
                : {};
              this.input.audio.sound = data.sound_file
                ? `/File/Content/${data.sound_file.id}`
                : null;
              this.input.imageNote.id = data.sound_jpg_file
                ? data.sound_jpg_file.id
                : null;
              this.input.imageNote.file = data.sound_jpg_file
                ? {
                    name: data.sound_jpg_file.file_name,
                    size: data.sound_jpg_file.file_size,
                  }
                : {};
              this.input.imageNote.blob = data.sound_jpg_file
                ? `/File/Content/${data.sound_jpg_file.id}`
                : null;
              // step 7
              this.input.listTranslation = data.checking_similar_translate_list;
              // step 8
              this.input.listProduct = data.product_list;
              // step 9
              this.input.isLogoColorGroupTutelage = data.is_color_protect;
              this.input.remark_9 = data.remark_9;
              // step 10
              this.input.isLogoThreeDememsionsGroupTutelage =
                data.is_model_protect;
              this.input.remark_10 = data.remark_10;
              // step 11
              this.input.isUseMark = data.is_before_request;
              // step 12
              this.input.save220_assert_type_code =
                data.save220_assert_type_code;
              // step 12.1
              if (data.kor10_list.length > 0) {
                this.input.listOwnerMarkA10 = data.kor10_list[0].people_list;
                this.input.listAgentMarkA10 =
                  data.kor10_list[0].representative_list;
                this.input.isDeemed1 = data.kor10_list[0].is_11_1_2_1;
                this.input.isDeemed2 = data.kor10_list[0].is_11_1_2_2;
                this.input.listRegistrationRequest =
                  data.kor10_list[0].product_list;
                this.input.listProductShow = data.kor10_list[0].event_list;
                this.input.listIsA10Checked = [
                  data.kor10_list[0].is_11_1_1,
                  data.kor10_list[0].is_11_1_2,
                  data.kor10_list[0].is_11_1_3,
                  data.kor10_list[0].is_11_1_4,
                  data.kor10_list[0].is_11_1_5,
                  data.kor10_list[0].is_11_1_6,
                  data.kor10_list[0].is_11_1_7,
                  data.kor10_list[0].is_11_1_8,
                  data.kor10_list[0].is_11_1_9,
                ];
                this.input.remark_12_1_9 = data.kor10_list[0].remark_12_1_9;
                if (data.kor10_list[0].kor19_list.length > 0) {
                  // step 12.2
                  this.input.listOwnerMarkA19 =
                    data.kor10_list[0].kor19_list[0].people_list;
                  this.input.listAgentMarkA19 =
                    data.kor10_list[0].kor19_list[0].representative_list;
                  this.input.detail = data.kor10_list[0].kor19_list[0].remark;
                }
              }
              // step 13
              this.input.listDocumentRequestChecked[0].checked = true;
              this.input.listDocumentRequestChecked[1].checked = data.is_14_2;
              this.input.listDocumentRequestChecked[2].checked = data.is_14_3;
              this.input.listDocumentRequestChecked[3].checked = data.is_14_4;
              this.input.listDocumentRequestChecked[4].checked = data.is_14_5;
              this.input.listDocumentRequestChecked[5].checked = data.is_14_6;
              this.input.listDocumentRequestChecked[6].checked = data.is_14_7;
              this.input.listDocumentRequestChecked[7].checked = data.is_14_8;
              this.input.listDocumentRequestChecked[8].checked = data.is_14_9;
              this.input.listDocumentRequestChecked[9].checked = data.is_14_10;
              // step 14
              this.input.payer_name = data.payer_name;
              this.input.productSummary.total_price = data.total_price;
              // step 15
              this.input.isCheckAllOwnerSignature =
                data.sign_inform_person_list === "0" ? true : false;
              this.input.isCheckAllAgentSignature =
                data.sign_inform_representative_list === "0" ? true : false;
              this.manageWizard(data.wizard);
              this.updateSummaryProduct();
              this.setSignInform(data.sign_inform_representative_list);
              // Close loading
              this.global.setLoading(false);
            } else {
              // Close loading
              this.global.setLoading(false);
            }
          });
      } else {
        // Close loading
        this.global.setLoading(false);
      }
    });
  }

  //! <<< Call API >>>
  callSendEmail220(params: any): void {
    this.eformSaveProcessService
      .eFormSave220Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
            kor10_id: data.kor10_list.length > 0 ? data.kor10_list[0].id : null,
            kor10_save_id:
              data.kor10_list.length > 0 ? data.kor10_list[0].save_id : null,
            kor19_id:
              data.kor10_list.length > 0 &&
              data.kor10_list[0].kor19_list.length > 0
                ? data.kor10_list[0].kor19_list[0].id
                : null,
            kor19_save_id:
              data.kor10_list.length > 0 &&
              data.kor10_list[0].kor19_list.length > 0
                ? data.kor10_list[0].kor19_list[0].save_id
                : null,
          };
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callEFormSave220Save(params: any): void {
    this.eformSaveProcessService
      .eFormSave220Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.isDeactivation = true;
          // Open toast success
          let toast = CONSTANTS.TOAST.SUCCESS;
          toast.message = "บันทึกข้อมูลสำเร็จ";
          this.global.setToast(toast);
          // Navigate
          this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
        } else {
          // Close loading
          this.global.setLoading(false);
        }
      });
  }
  callSearchRequestNumber(): void {
    this.eformSaveProcessService
      .eFormSave220SearchRequestNumber(this.input.inter_registration_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.registration_number = data.registration_load_number;
          this.input.listOwnerMark = clearIdAndSaveId(data.people_load_list);
          this.input.listAgentMark = clearIdAndSaveId(
            data.representative_load_list
          );
          this.input.save220_representative_condition_type_code =
            data.load_representative_condition_type_code;
          this.input.save220_contact_type_code = data.load_contact_type_code;
          this.input.address = data.contact_address_load_list[0]
            ? data.contact_address_load_list[0]
            : this.input.address;
          this.input.listProduct = clearIdAndSaveId(data.product_load_list);
          this.manageContactTypeIndex();
          this.updateSummaryProduct();
          this.changePaginateTotal(
            this.input.listProduct.length,
            "paginateProduct"
          );
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchMultipleInterRegistrationNumber(): void {
    this.eformSaveProcessService
      .eFormSave220MultipleSearch(this.input.listMultipleSearch.toString(), {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          data.eform_220_date_list.forEach((item: any) => {
            this.input.listInterRegistration.push({
              request_number: item.request_number,
              case_28_date: item.case_28_date,
              request_date: item.request_date,
            });
          });
        } else {
          console.warn(`multiple_inter_registration_number is invalid.`);
          this.validate.multiple_inter_registration_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callUpload(params: any, calback: Function): void {
    const formData = new FormData();
    formData.append("file", params.file);

    this.uploadService.guestUpload(formData).subscribe((data: any) => {
      if (data) {
        calback(data);
      }
      // Close loading
      this.global.setLoading(false);
    });
  }
  callSubDistrict(params: any, name: any): void {
    this.eformSaveProcessService
      .SearchLocation(params)
      .subscribe((data: any) => {
        if (data) {
          this.autocompleteList[name] = data;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }
  callProductCatagory01ItemListAutocomplete(params: any, name: any): void {
    this.eformSaveProcessService
      .ProductCatagory01ItemList(params)
      .subscribe((data: any) => {
        if (data) {
          let list = [];
          data.list.forEach((item: any) => {
            list.push({
              request_item_sub_type_1_code: item.code,
              description: item.name,
            });
          });
          this.autocompleteList[name] = list;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }
  callProductCatagory01ItemList(params: any): void {
    this.eformSaveProcessService
      .ProductCatagory01ItemList(params)
      .subscribe((data: any) => {
        if (data) {
          let list = [];
          data.list.forEach((item: any) => {
            list.push({
              request_item_sub_type_1_code: item.code,
              description: item.name,
            });
          });
          this.listProductService = list;
          this.changePaginateTotal(
            data.paging.item_total,
            "paginateProductService"
          );
        }
      });
  }

  //! <<< Prepare Call API >>>
  sendEmail(): void {
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      wizard: this.getWizard(),
      email: this.input.email,
      telephone: this.input.telephone,
    };
    // Call api
    this.callSendEmail220(params);
  }
  onSearch(): void {
    let result = validateService(
      "validateEFormSave220ProcessStep3",
      this.input
    );
    this.validate = result.validate;

    if (result.isValid) {
      // Open loading
      this.global.setLoading(true);
      // Call api
      this.callSearchRequestNumber();
    }
  }
  onSearchMultiple(): void {
    if (this.input.multiple_inter_registration_number) {
      // Add search data to list
      this.input.listMultipleSearch.push(
        this.input.multiple_inter_registration_number
      );
      this.input.multiple_inter_registration_number = "";
    }

    // Clear filter
    this.input.listInterRegistration = [];
    this.clearFliterInterRegistrationNumber();

    if (this.input.listMultipleSearch.length > 0) {
      this.clearAllValidate();
      // Open loading
      this.global.setLoading(true);
      // Call api
      this.callSearchMultipleInterRegistrationNumber();
    } else {
      console.warn(`multiple_inter_registration_number is invalid.`);
      this.validate.multiple_inter_registration_number =
        "กรุณากรอก เลขคำขอในประเทศ";
    }
  }
  prepareCallUpload(file: any, calback: Function): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      file: file,
    };
    // Call api
    this.callUpload(params, calback);
  }
  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }
  save(isOverwrite: boolean): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = this.getParamsSave();
    if (!isOverwrite) {
      params = getParamsOverwrite(params);
    }
    // Call api
    this.callEFormSave220Save(params);
  }
  getParamsSave(): any {
    return {
      id: this.response.load.id ? this.response.load.id : null,
      eform_number: this.response.load.eform_number
        ? this.response.load.eform_number
        : null,
      wizard: this.getWizard(),
      // step 1
      email: this.input.email,
      telephone: this.input.telephone,
      // step 2
      request_number: this.input.request_number,
      // step 3
      // TODO
      // step 4
      people_list: this.input.listOwnerMark,
      representative_list: this.input.listAgentMark,
      save220_representative_condition_type_code: this.input
        .save220_representative_condition_type_code,
      // step 5
      save220_contact_type_code: this.input.save220_contact_type_code,
      contact_address_list:
        this.input.save220_contact_type_code === "OTHERS"
          ? [this.input.address]
          : [],
      // step 6.1
      save220_img_type_type_code: this.input.save220_img_type_type_code,
      img_file_2d_id: this.input.imageDimension2.id,
      img_file_3d_id_1: this.input.imageDimension3_1.id,
      img_w: this.input.img_w,
      img_h: this.input.img_h,
      remark_5_1_3: this.input.remark_5_1_3 ? this.input.remark_5_1_3 : "",
      // step 6.2
      is_sound_mark: this.input.isBrandLogoSound,
      is_sound_mark_human: this.input.is_sound_mark_human,
      is_sound_mark_animal: this.input.is_sound_mark_animal,
      is_sound_mark_sound: this.input.is_sound_mark_sound,
      is_sound_mark_other: this.input.is_sound_mark_other,
      remark_5_2_2: this.input.remark_5_2_2 ? this.input.remark_5_2_2 : "",
      sound_file_id: this.input.audio.id,
      sound_jpg_file_id: this.input.imageNote.id,
      // step 7
      checking_similar_translate_list: this.input.listTranslation,
      // step 8
      product_list: this.input.listProduct,
      // step 9
      is_color_protect: this.input.isLogoColorGroupTutelage,
      remark_9: this.input.remark_9 ? this.input.remark_9 : "",
      // step 10
      is_model_protect: this.input.isLogoThreeDememsionsGroupTutelage,
      remark_10: this.input.remark_10 ? this.input.remark_10 : "",
      // step 11
      is_before_request: this.input.isUseMark,
      // step 12
      save220_assert_type_code: this.input.save220_assert_type_code,
      // step 12.1
      kor10_list: [
        {
          id: this.response.load.kor10_id ? this.response.load.kor10_id : null,
          save_id: this.response.load.kor10_save_id
            ? this.response.load.kor10_save_id
            : null,
          people_list: this.input.listOwnerMarkA10,
          representative_list: this.input.listAgentMarkA10,
          is_11_1_2_1: this.input.isDeemed1,
          is_11_1_2_2: this.input.isDeemed2,
          product_list: loopDisplayDateServer(
            this.input.listRegistrationRequest,
            "request_date"
          ),
          event_list: loopDisplayDateServer(
            this.input.listProductShow,
            "event_date"
          ),
          is_11_1_1: this.input.listIsA10Checked[0],
          is_11_1_2: this.input.listIsA10Checked[1],
          is_11_1_3: this.input.listIsA10Checked[2],
          is_11_1_4: this.input.listIsA10Checked[3],
          is_11_1_5: this.input.listIsA10Checked[4],
          is_11_1_6: this.input.listIsA10Checked[5],
          is_11_1_7: this.input.listIsA10Checked[6],
          is_11_1_8: this.input.listIsA10Checked[7],
          is_11_1_9: this.input.listIsA10Checked[8],
          remark_12_1_9: this.input.remark_12_1_9
            ? this.input.remark_12_1_9
            : "",
          // step 12.2
          kor19_list: [
            {
              id: this.response.load.kor19_id
                ? this.response.load.kor19_id
                : null,
              save_id: this.response.load.kor19_save_id
                ? this.response.load.kor19_save_id
                : null,
              people_list: this.input.listOwnerMarkA19,
              representative_list: this.input.listAgentMarkA19,
              postpone_day: 60,
              remark: this.input.detail,
            },
          ],
        },
      ],
      // step 13
      is_14_1: this.input.listDocumentRequestChecked[0].checked,
      is_14_2: this.input.listDocumentRequestChecked[1].checked,
      is_14_3: this.input.listDocumentRequestChecked[2].checked,
      is_14_4: this.input.listDocumentRequestChecked[3].checked,
      is_14_5: this.input.listDocumentRequestChecked[4].checked,
      is_14_6: this.input.listDocumentRequestChecked[5].checked,
      is_14_7: this.input.listDocumentRequestChecked[6].checked,
      is_14_8: this.input.listDocumentRequestChecked[7].checked,
      is_14_9: this.input.listDocumentRequestChecked[8].checked,
      is_14_10: this.input.listDocumentRequestChecked[9].checked,
      // step 14
      payer_name: this.input.payer_name,
      total_price: this.input.productSummary.total_price,
      // step 15
      sign_inform_person_list: this.input.isCheckAllOwnerSignature ? "0" : "",
      sign_inform_representative_list:
        this.input.isCheckAllAgentSignature &&
        this.input.save020_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform(),
    };
  }
  prepareCallSearchProductService(isSearch: boolean): void {
    const params = {
      page_index: isSearch ? 1 : +this.paginateProductService.currentPage,
      item_per_page: +this.paginateProductService.itemsPerPage,
      order_by: "created_date",
      is_order_reverse: false,
      filter_queries: [
        `code.Contains("${this.input.searchProductService}") || name.Contains("${this.input.searchProductService}")`,
      ],
      // search_by: [
      //   {
      //     key: "request_item_sub_type_1_code",
      //     value: this.input.searchProductService,
      //     operation: 5,
      //   },
      //   {
      //     key: "description",
      //     value: this.input.searchProductService,
      //     operation: 5,
      //   },
      // ],
    };
    this.callProductCatagory01ItemList(params);
  }
  onClickViewPdfKor22(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM22", this.getParamsSave());
    }
  }
  onClickViewPdfKor18(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM18", this.getParamsSave());
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Event >>>
  toggleBooleanSoundType(code: any): void {
    if (code === "HUMAN") {
      this.input.is_sound_mark_human = !this.input.is_sound_mark_human;
    } else if (code === "ANIMAL") {
      this.input.is_sound_mark_animal = !this.input.is_sound_mark_animal;
    } else if (code === "NATURE") {
      this.input.is_sound_mark_sound = !this.input.is_sound_mark_sound;
    } else if (code === "OTHERS") {
      this.input.is_sound_mark_other = !this.input.is_sound_mark_other;
    }
  }
  getCheckedSoundType(code: any): void {
    if (code === "HUMAN") {
      return this.input.is_sound_mark_human;
    } else if (code === "ANIMAL") {
      return this.input.is_sound_mark_animal;
    } else if (code === "NATURE") {
      return this.input.is_sound_mark_sound;
    } else if (code === "OTHERS") {
      return this.input.is_sound_mark_other;
    }
  }
  reRunNumberArrayChecked(name: any): void {
    let number = 1;
    this.input[name].forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
      }
    });
  }
  getSignInform(): String {
    let result = "";
    this.input.listAgentMark.forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input.listAgentMark.forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save220_representative_condition_type_code != "AND") {
        this.input.listAgentMark.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }
  onClickSlideAuthorize(action: any): void {
    if (action === "next") {
      if (this.input.indexAuthorize < this.input.listAgentMark.length - 1) {
        this.input.indexAuthorize++;
        this.cloneAuthorize();
      }
    } else if (action === "back") {
      if (this.input.indexAuthorize !== 0) {
        this.input.indexAuthorize = this.input.indexAuthorize - 1;
        this.cloneAuthorize();
      }
    }
  }
  cloneAuthorize(): void {
    let i = this.input.indexAuthorize;
    this.input.authorize.inheritor_name = this.input.listAgentMark[i].name;
    this.input.authorize.inheritor_nationality_code = this.input.listAgentMark[
      i
    ].nationality_code;
    this.input.authorize.inheritor_house_number = this.input.listAgentMark[
      i
    ].house_number;
    this.input.authorize.inheritor_alley = this.input.listAgentMark[i].alley;
    this.input.authorize.inheritor_street = this.input.listAgentMark[i].street;
    this.input.authorize.inheritor_address_sub_district_name = this.input.listAgentMark[
      i
    ].address_sub_district_name;
    this.input.authorize.inheritor_postal_code = this.input.listAgentMark[
      i
    ].postal_code;
  }
  getWizard(): String {
    if (this.editID) {
      let wizardList = this.response.load.wizard.split("|");
      let sum = 0;
      let sumNow =
        this.currentID +
        this.currentStep +
        this.currentSubID +
        this.currentSubStep;

      wizardList.forEach((item: any) => {
        sum = +sum + +item;
      });

      if (sumNow > sum) {
        return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
      } else {
        return this.response.load.wizard;
      }
    } else {
      return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
    }
  }
  manageContactTypeIndex(): void {
    if (this.input.save210_contact_type_code !== "OTHERS") {
      if (this.input.save210_contact_type_code === "OWNER") {
        this.input.listOwnerMark.forEach((item: any, index: any) => {
          if (item.is_contact_person) {
            this.input.contact_type_index = index;
          }
        });
      } else {
        this.input.listAgentMark.forEach((item: any, index: any) => {
          if (item.is_contact_person) {
            this.input.contact_type_index = index;
          }
        });
      }
    }
  }

  //! <<< Multiple Search (Step 2) >>>
  onChangeFocusInterRegistrationNumber(): void {
    this.input.listInterRegistration.forEach((item: any) => {
      if (item.request_number == this.input.focusInterRegistrationNumber) {
        this.input.case_28_date = item.case_28_date;
        this.input.request_date = item.request_date;
      }
    });
  }
  onClickRemoveInterRegistrationNumber(
    obj: string,
    name: any,
    index: number
  ): void {
    if (this.input.listInterRegistration.length > 0) {
      this.input.listInterRegistration = this.input.listInterRegistration.filter(
        (item: any) => {
          return item.request_number != this.input.listMultipleSearch[index];
        }
      );
      if (
        this.input.focusInterRegistrationNumber ==
        this.input.listMultipleSearch[index]
      ) {
        this.clearFliterInterRegistrationNumber();
      }
    }
    this[obj][name].splice(index, 1);
  }
  clearFliterInterRegistrationNumber(): void {
    this.input.focusInterRegistrationNumber = undefined;
    this.input.case_28_date = "";
    this.input.request_date = "";
  }

  //! <<< Table (Language) >>>
  onClickAddLanguage(): void {
    this.input.listTranslation.forEach((item: any, index: any) => {
      item.isEdit = false;
      item.canEdit = true;
    });
    this.input.listTranslation.push({
      isEdit: false,
      canEdit: true,
      save_index: 0,
      word_translate_search: "",
      translation_language_code: "EN",
      word_translate_sound: "",
      word_translate_sound_2: "",
      word_translate_translate: "",
    });
    this.changePaginateTotal(
      this.input.listTranslation.length,
      "paginateTranslation"
    );
  }
  getLanguageLabel(code: any): string {
    let label = "";
    this.master.translationLanguageCodeList.forEach((item: any) => {
      if (item.code === code) {
        label = item.name;
      }
    });
    return label;
  }
  onClickEditLanguage(item: any): void {
    this.input.listTranslation.forEach((item: any, index: any) => {
      item.isEdit = false;
    });
    item.isEdit = true;
    this.input.translationItem = clone(item);
  }
  onClickCancelEditLanguage(item: any): void {
    item.isEdit = false;
    this.input.translationItem = {};
  }
  onClickSaveLanguage(index: number): void {
    this.input.translationItem.isEdit = false;
    this.input.listTranslation[index] = this.input.translationItem;
  }

  //! <<< Table (Product) >>>
  onClickAddProduct(): void {
    this.input.listProduct.push({
      isEditType: true,
      request_item_sub_type_1_code: "",
      description: "",
      amount_product: 1,
      total_price: getItemCalculator(1, this.master.priceMasterList),
    });
    this.updateSummaryProduct();
    this.changePaginateTotal(this.input.listProduct.length, "paginateProduct");
  }
  onChangeAmountProduct(item: any): void {
    if (+item.amount_product && +item.amount_product >= 1) {
      item.total_price = getItemCalculator(
        item.amount_product,
        this.master.priceMasterList
      );
      this.updateSummaryProduct();
    } else {
      item.amount_product = 0;
      item.total_price = 0;
      this.updateSummaryProduct();
    }
  }
  onClickRemoveProduct(index: number): void {
    this.input.listProduct.splice(index, 1);
    this.updateSummaryProduct();
    this.changePaginateTotal(this.input.listProduct.length, "paginateProduct");
  }
  updateSummaryProduct(): void {
    let amount_type = this.input.listProduct.length;
    let requestItemList = [];
    let allRequestItemList = [];
    let count = {};
    let amount_product = 0;
    let total_price = 0;

    // Sum amount_product
    this.input.listProduct.forEach((item: any) => {
      allRequestItemList.push(item.request_item_sub_type_1_code);
      if (requestItemList.indexOf(item.request_item_sub_type_1_code) === -1) {
        requestItemList.push(item.request_item_sub_type_1_code);
        amount_product = amount_product + 1;
      }
    });

    // Count sum per request item
    allRequestItemList.forEach(function (i) {
      count[i] = (count[i] || 0) + 1;
    });

    // Sum total_price
    for (const [key, value] of Object.entries(count)) {
      total_price =
        total_price + getItemCalculator(value, this.master.priceMasterList);
    }

    this.input.productSummary = {
      amount_type: amount_type,
      amount_product: amount_product,
      total_price: total_price,
    };
  }

  //! <<< Modal (Product) >>>
  onSearchProductService(): void {
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      this.prepareCallSearchProductService(true);
    }, CONSTANTS.DELAY_CALL_API);
  }
  onClickRemoveSearch(): void {
    this.input.searchProductService = "";
    this.prepareCallSearchProductService(true);
  }
  onClickSaveProductService(): void {
    // Remove empty object
    this.input.listProduct = this.input.listProduct.filter((item: any) => {
      return item.request_item_sub_type_1_code != "" || item.description != "";
    });

    // Add more
    this.listProductService.forEach((item: any) => {
      if (item.is_check) {
        this.input.listProduct.push({
          isEditType: false,
          request_item_sub_type_1_code: item.request_item_sub_type_1_code,
          description: item.description,
          amount_product: 1,
          total_price: getItemCalculator(1, this.master.priceMasterList),
        });
        this.updateSummaryProduct();
        this.changePaginateTotal(
          this.input.listProduct.length,
          "paginateProduct"
        );
      }
    });
    this.toggleModal("isModalProductAndServiceListsOpen");
  }

  //! <<< Table (Registration Request) >>>
  onClickAddRegistrationRequest(): void {
    this.input.listRegistrationRequest.push({
      request_number: "",
      request_date: "",
      request_country: "",
      request_nationality: "",
      request_domicile: "",
      product_class: "",
      product: "",
      status: "",
    });
    this.changePaginateTotal(
      this.input.listRegistrationRequest.length,
      "paginateRegistrationRequest"
    );
  }
  onClickRemoveRegistrationRequest(index: number): void {
    this.input.listRegistrationRequest.splice(index, 1);
    this.changePaginateTotal(
      this.input.listRegistrationRequest.length,
      "paginateRegistrationRequest"
    );
  }

  //! <<< Table (Product Show) >>>
  onClickAddProductShow(): void {
    this.input.listProductShow.push({
      product: "",
      event_date: "",
      event_place: "",
      event_organizer: "",
    });
    this.changePaginateTotal(
      this.input.listProductShow.length,
      "paginateProductShow"
    );
  }
  onClickRemoveProductShow(index: number): void {
    this.input.listProductShow.splice(index, 1);
    this.changePaginateTotal(
      this.input.listProductShow.length,
      "paginateProductShow"
    );
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(name: any, value: any, item: any, index: any): void {
    clearTimeout(this.timeout);
    if (name === "description" && item) {
      this.autocompleteFocusIndex.description = index;
      item.isEditType = true;
      this.clearValidate("description", item);
    }
    if (name === "address_sub_district_name") {
      this.validate[name] = null;
      this.clearValue(name);
    }

    this.timeout = setTimeout(() => {
      if (value) {
        if (name === "description") {
          this.callProductCatagory01ItemListAutocomplete(
            {
              page_index: 1,
              item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page,
              order_by: "created_date",
              is_order_reverse: false,
              filter_queries: [
                `code.Contains("${value}") || name.Contains("${value}")`,
              ],
              // search_by: [
              //   {
              //     key: "request_item_sub_type_1_code",
              //     value: value,
              //     operation: 5,
              //   },
              //   {
              //     key: "description",
              //     value: value,
              //     operation: 5,
              //   },
              // ],
            },
            name
          );
        }
        if (name === "address_sub_district_name") {
          this.callSubDistrict(
            {
              filter: { name: value },
              paging: {
                item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page,
              },
            },
            name
          );
        }
      } else {
        this.onClickOutsideAutocomplete(name);
      }
    }, CONSTANTS.DELAY_CALL_API);
  }
  onSelectAutocomplete(name: any, item: any, obj: any): void {
    if (name === "description") {
      obj.isEditType = false;
      obj.request_item_sub_type_1_code = item.request_item_sub_type_1_code;
      obj.description = item.description;
      this.clearValidate("request_item_sub_type_1_code", obj);
      this.updateSummaryProduct();
    }
    if (name === "address_sub_district_name") {
      // ตำบล
      this.input.address.address_sub_district_code = item.code;
      this.input.address.address_sub_district_name = item.name;
      // อำเภอ
      this.input.address.address_district_code = item.district_code;
      this.input.address.address_district_name = item.district_name;
      // จังหวัด
      this.input.address.address_province_code = item.province_code;
      this.input.address.address_province_name = item.province_name;
      // รหัสไปรษณีย์
      this.input.address.postal_code = item.postal_code;
    }
    this.onClickOutsideAutocomplete(name);
  }
  onClickOutsideAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = false;
    this.autocompleteList[name] = [];
  }
  clearValue(name: any): void {
    if (name === "address_sub_district_name") {
      // อำเภอ
      this.input.address.address_district_code = "";
      this.input.address.address_district_name = "";
      // จังหวัด
      this.input.address.address_province_code = "";
      this.input.address.address_province_name = "";
      // รหัสไปรษณีย์
      this.input.address.postal_code = "";
    }
  }

  //! <<< Table >>>
  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }
  toggleBooleanInTable(item: any, name: any, condition?: any): void {
    item[name] = !item[name];
    // if (condition === "cloneStep3") {
    //   this.input.authorize = {
    //     owner_name: this.input.listOwnerMark[0].name,
    //     owner_nationality_code: this.input.listOwnerMark[0].nationality_code,
    //     owner_house_number: this.input.listOwnerMark[0].house_number,
    //     owner_alley: this.input.listOwnerMark[0].alley,
    //     owner_street: this.input.listOwnerMark[0].street,
    //     owner_address_sub_district_name: this.input.listOwnerMark[0]
    //       .address_sub_district_name,
    //     owner_postal_code: this.input.listOwnerMark[0].postal_code,

    //     inheritor_name: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].name
    //       : "",
    //     inheritor_nationality_code: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].nationality_code
    //       : "TH",
    //     inheritor_house_number: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].house_number
    //       : "",
    //     inheritor_alley: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].alley
    //       : "",
    //     inheritor_street: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].street
    //       : "",
    //     inheritor_address_sub_district_name: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].address_sub_district_name
    //       : "",
    //     inheritor_postal_code: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].postal_code
    //       : "",
    //   };
    // }
  }
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all;
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all;
      });
    } else {
      item.is_check = !item.is_check;
    }
  }
  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    if (this.validateSaveItemModalInTable(nameItem)) {
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        this.toggleModal(nameModal);
        this.updatePaginateForTable(nameList);
      }
    }
  }
  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
    this.updatePaginateForTable(name);
  }
  updatePaginateForTable(name: any): void {
    if (name === "listTranslation") {
      this.changePaginateTotal(
        this.input.listTranslation.length,
        "paginateTranslation"
      );
    } else if (name === "listMark") {
      this.changePaginateTotal(this.input.listMark.length, "paginateMark");
    }
  }
  validateSaveItemModalInTable(nameItem: any): boolean {
    // if (nameItem === "ownerMarkItem") {
    //   let result = validateService(
    //     "validateEFormSave01ProcessStep3OwnerMarkItem",
    //     this.input.ownerMarkItem
    //   );
    //   this.validate = result.validate;
    //   return result.isValid;
    // } else if (nameItem === "agentMarkItem") {
    //   let result = validateService(
    //     "validateEFormSave01ProcessStep3AgentMarkItem",
    //     this.input.agentMarkItem
    //   );
    //   this.validate = result.validate;
    //   return result.isValid;
    // } else if (nameItem === "markItem") {
    //   let result = validateService(
    //     "validateEFormSave01ProcessStep3OwnerMarkItem",
    //     this.input.markItem
    //   );
    //   this.validate = result.validate;
    //   return result.isValid;
    // }
    return true;
  }

  //! <<< Upload (Image) >>>
  onChangeUploadImage(data: any, obj: string, name: any): any {
    if (!data.validate) {
      this.clearValidate("step6");
      this.prepareCallUpload(data.file, (res: any) => {
        this[obj][name] = data;
        this[obj][name].id = res.id;
      });
    } else {
      console.warn(`step6 is invalid.`);
      this.validate["step6"] = data.validate;
    }
  }
  onRemoveImage(obj: string, name: any): any {
    this[obj][name] = {
      id: null,
      file: {},
      blob: null,
    };
  }
  onClickUploadImage(event: any, obj: string, name: any, maxSize: any): void {
    if (event) {
      let file = event.target.files[0];
      // Check type file
      if (file.type.includes("image/jpeg") || file.type.includes("image/png")) {
        this.clearValidate(name);
        // Has max size
        if (maxSize) {
          // Check size file
          if (file.size <= maxSize) {
            this.clearValidate(name);
            this.setFileData(file, obj, name);
          } else {
            console.warn(`${name} is invalid.`);
            this.validate[name] = "ขนาดไฟล์เกินกำหนด";
          }
        } else {
          this.setFileData(file, obj, name);
        }
      } else {
        console.warn(`${name} is invalid.`);
        this.validate[name] = "รูปแบบไฟล์ไม่ถูกต้อง";
      }
    }
  }
  setFileData(file: any, obj: string, name: any): void {
    this.prepareCallUpload(file, (res: any) => {
      this[obj][name].file = file;
      this[obj][name].id = res.id;
      let reader = new FileReader();
      reader.onload = (e) => {
        this[obj][name].blob = reader.result;
      };
      reader.readAsDataURL(file);
    });
  }
  onClickRemoveImage(obj: string, name: any): void {
    this[obj][name] = {
      id: null,
      file: {},
      blob: null,
    };
  }

  //! <<< Upload (Audio) >>>
  onClickUploadAudio(event: any, obj: string, name: any): void {
    if (event) {
      let file = event.target.files[0];
      // Check type file
      if (
        file.type.includes("audio/mp3") ||
        file.type.includes("audio/wav") ||
        file.name.includes(".mp3") ||
        file.name.includes(".wav")
      ) {
        this.clearValidate(name);
        // Check size file 5 MB
        if (file.size <= 5242880) {
          this.clearValidate(name);
          this.prepareCallUpload(file, (res: any) => {
            this[obj][name].file = file;
            this[obj][name].id = res.id;
            let reader = new FileReader();
            reader.onload = (e) => {
              this[obj][name].sound = reader.result;
            };
            reader.readAsDataURL(file);
          });
        } else {
          console.warn(`${name} is invalid.`);
          this.validate[name] = "ขนาดไฟล์เกินกำหนด";
        }
      } else {
        console.warn(`${name} is invalid.`);
        this.validate[name] = "รูปแบบไฟล์ไม่ถูกต้อง";
      }
    }
  }
  togglePlayer(obj: string, name: any): void {
    this[obj][name].action = !this[obj][name].action;
    if (!this[obj][name].palyer) {
      this[obj][name].palyer = new Audio(this[obj][name].sound);
    }
    this[obj][name].action
      ? this[obj][name].palyer.play()
      : this[obj][name].palyer.pause();
  }
  onClickRemoveAudio(obj: string, name: any): void {
    if (this[obj][name].palyer) {
      this[obj][name].palyer.pause();
    }
    this[obj][name] = {
      id: null,
      input: "",
      validate: "",
      file: {},
      sound: null,
      palyer: null,
      action: false,
    };
  }

  //! <<< Upload (CSV) >>>
  onClickUploadCSV(event: any): void {
    let file = event.target.files[0];
    PapaParseCsvToJson(file, (results: any) => {
      let result = results.data;

      // Remove empty object
      this.input.listProduct = this.input.listProduct.filter((item: any) => {
        return (
          item.request_item_sub_type_1_code != "" || item.description != ""
        );
      });

      // Add more
      result.forEach((item: any, index: any) => {
        if (index > 0) {
          this.input.listProduct.push({
            request_item_sub_type_1_code: item[0] ? item[0] : "",
            description: item[1] ? item[1] : "",
            amount_product: 1,
            total_price: getItemCalculator(1, this.master.priceMasterList),
          });
        }
      });
      this.updateSummaryProduct();
      this.changePaginateTotal(
        this.input.listProduct.length,
        "paginateProduct"
      );
    });
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === "paginateProductService") {
      this.prepareCallSearchProductService(false);
      this.input.is_check_all = false;
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalCommonMarkFormOpen") {
          this.input.markItem = clone(this.input.listMark[index]);
        }
      }
      if (name === "isModalProductAndServiceListsOpen") {
        this.input.searchProductService = "";
        this.paginateProductService.totalItems = 10;
        this.onChangePage(1, "paginateProductService");
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.is_check_all = false;
      this.input.markItem = {
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
    }
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
      this.updatePaginateForTable(nameList);
    }

    // if (nameList === "listOwnerMark" || nameList === "listAgentMark") {
    this.clearAllValidate();
    // }
  }

  //! <<< Wizard >>>
  onClickMenu(menu: any): void {
    if (menu.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentSubStep = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickSubMenu(menu: any, sub: any): void {
    if (sub.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = sub.id;
        this.currentSubStep = sub.number;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickNext(): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      if (this.menuList[this.currentID - 1].hasSub) {
        let sizeSubMenu = this.menuList[this.currentID - 1].sizeSubList;

        if (this.currentSubStep < sizeSubMenu) {
          this.nextSubMenu();
        } else {
          if (this.currentSubID > 0) {
            this.menuList[this.currentID - 1].subList[
              this.currentSubID - 1
            ].canEdit = true;
          }
          this.nextMenu();
        }
      } else {
        this.nextMenu();
      }
    }
  }
  nextMenu(): void {
    let indexCurrentMenu = 0;
    let indexNextMenu = 0;

    // Find index current
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Cal Progress Percent
    if (!this.menuList[indexCurrentMenu].canEdit) {
      this.calcProgressPercent(this.currentStep);
    }
    // Set can edit
    this.menuList[indexCurrentMenu].canEdit = true;
    // Next wizard
    this.currentStep++;

    // Find index next
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexNextMenu = i;
      }
    });

    // Update wizard point
    this.currentID = this.menuList[indexNextMenu].id;
    this.currentSubID = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentSubStep = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  nextSubMenu(): void {
    let indexCurrentMenu = 0;
    let indexCurrentSub = 0;
    let indexNextSub = 0;

    // Find index current menu
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Find index current sub menu
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexCurrentSub = i;
      }
    });

    // Set can edit
    if (this.currentSubID > 0) {
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].canEdit = true;
    } else {
      if (this.menuList[indexCurrentMenu].isUseMenuBeforeSubList) {
        this.menuList[indexCurrentMenu].canEdit = true;
      }
    }
    // Next wizard sub
    this.currentSubStep++;

    // Find index sub next
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexNextSub = i;
      }
    });

    // Update wizard sub point
    this.currentSubID = this.menuList[indexCurrentMenu].subList[
      indexNextSub
    ].id;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  changeMiniStep(action: any): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let indexCurrentMini = 0;
      let indexNextMini = 0;
      let sizeMiniList = 0;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
        }
      });

      // Find index current sub menu
      this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
        if (item.number === this.currentSubStep && item.isShow) {
          indexCurrentSub = i;
        }
      });

      // Find index current mini menu
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList.forEach(
        (item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexCurrentMini = i;
          }
        }
      );

      // Get size mini list
      sizeMiniList = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
        .sizeMiniList;

      // Condition
      if (sizeMiniList === this.currentMiniStep && action === "next") {
        // Set can edit
        this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
          indexCurrentMini
        ].canEdit = true;
        // Next menu
        this.onClickNext();
      } else {
        if (action === "next") {
          // Set can edit
          this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
            indexCurrentMini
          ].canEdit = true;
          // Next wizard mini
          this.currentMiniStep++;
        } else {
          // Back wizard mini
          this.currentMiniStep = this.currentMiniStep - 1;
        }

        // Find index next mini menu
        this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList.forEach((item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexNextMini = i;
          }
        });

        // Update wizard mini point
        this.currentMiniID = this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList[indexNextMini].id;
      }
    }
  }
  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    } else if (this.currentID === 2) {
      let result = validateService(
        "validateEFormSave140ProcessStep2",
        this.input
      );
      this.validate = result.validate;

      if (!this.input.listOwnerMark || this.input.listOwnerMark.length === 0) {
        result.isValid = false;
        console.warn(`request_number is invalid.`);
        this.validate.request_number = "กรุณาคลิก ค้นหา";
      }

      return result.isValid;

      // let isValid = true;
      // if (
      //   !this.input.listInterRegistration ||
      //   this.input.listInterRegistration.length === 0
      // ) {
      //   isValid = false;
      //   console.warn(`multiple_inter_registration_number is invalid.`);
      //   this.validate.multiple_inter_registration_number = "กรุณาคลิก ค้นหา";
      // }
      // return isValid;
    } else if (this.currentID === 3) {
      let result = validateService(
        "validateEFormSave220ProcessStep3",
        this.input
      );
      this.validate = result.validate;

      // if (!this.input.listOwnerMark || this.input.listOwnerMark.length === 0) {
      //   result.isValid = false;
      //   console.warn(`inter_registration_number is invalid.`);
      //   this.validate.inter_registration_number = "กรุณาคลิก ค้นหา";
      // }

      return result.isValid;
    } else if (this.currentID === 4) {
      if (this.input.listOwnerMark.length < 1) {
        console.warn(`step4 is invalid.`);
        this.validate.step4 = "กรุณาเพิ่มเจ้าของเครื่องหมาย";
        return false;
      } else {
        // Condition
        this.conditionWizard("clone_mark_list");
        return true;
      }
    } else if (this.currentID === 5) {
      let result = validateService(
        "validateEFormSave220ProcessStep5",
        this.input
      );
      this.validate = result.validate;

      // Condition
      if (result.isValid) {
        if (this.input.save220_contact_type_code === "OTHERS") {
          let result = validateService(
            "validateEFormSave01ProcessStep4Other",
            this.input.address
          );
          this.validate = result.validate;
          if (
            result.validate.address_district_name ||
            result.validate.address_province_name ||
            result.validate.postal_code
          ) {
            this.validate.address_sub_district_name = "กรุณากรอกและเลือก ตำบล";
          }
          return result.isValid;
        } else {
          // Set is_contact_person
          if (this.input.save220_contact_type_code === "OWNER") {
            this.input.listOwnerMark.forEach((item: any, index: any) => {
              if (+this.input.contact_type_index === index) {
                item.is_contact_person = true;
              } else {
                item.is_contact_person = false;
              }
            });
          } else {
            this.input.listAgentMark.forEach((item: any, index: any) => {
              if (this.input.save220_contact_type_code === "REPRESENTATIVE") {
                // ตัวแทน
                if (
                  this.input.listAgentMarkRepresentative1[
                    +this.input.contact_type_index
                  ] === index
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              } else if (
                this.input.save220_contact_type_code === "REPRESENTATIVE_PERIOD"
              ) {
                // ตัวแทนช่วง
                if (
                  this.input.listAgentMarkRepresentative2[
                    +this.input.contact_type_index
                  ] === index
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              }
            });
          }
        }
      }

      return result.isValid;
    } else if (this.currentID === 6) {
      if (this.currentSubID === 1) {
        // Condition
        if (this.input.save220_img_type_type_code === "2D") {
          if (this.input.imageDimension2.blob) {
            // Set default radio step 9
            if (!this.editID && !this.menuList[8].canEdit) {
              this.input.isLogoThreeDememsionsGroupTutelage = false;
            }
            return true;
          } else {
            console.warn(`step6 is invalid.`);
            this.validate.step6 = "กรุณาอัพโหลดรูปภาพเครื่องหมาย";
            return false;
          }
        } else {
          if (this.input.imageDimension3_1.blob) {
            // Set default radio step 10
            if (!this.menuList[9].canEdit) {
              this.input.isLogoThreeDememsionsGroupTutelage = true;
            }
            return true;
          } else {
            console.warn(`step6 is invalid.`);
            this.validate.step6 = "กรุณาอัพโหลดรูปภาพเครื่องหมาย";
            return false;
          }
        }
      } else {
        if (this.input.isBrandLogoSound) {
          let result = validateService(
            "validateEFormSave01ProcessStep5Sub2",
            this.input
          );
          this.validate = result.validate;

          if (
            !this.input.is_sound_mark_human &&
            !this.input.is_sound_mark_animal &&
            !this.input.is_sound_mark_sound &&
            !this.input.is_sound_mark_other
          ) {
            console.warn(`save220_sound_mark_type_type_code is invalid.`);
            this.validate.save220_sound_mark_type_type_code =
              "กรุณาเลือก ประเภทเครื่องหมายเสียง";
            result.isValid = false;
            return false;
          }

          if (result.isValid) {
            this.conditionWizard("brand_logo_sound");
          }

          return result.isValid;
        }
        return true;
      }
    } else if (this.currentID === 7) {
      // Condition
      if (this.input.listTranslation.length > 0) {
        let isValid = true;

        this.input.listTranslation.forEach((item: any) => {
          let result = validateService("validateEFormSave01ProcessStep6", item);
          if (!result.isValid) {
            isValid = false;
          }
        });

        if (!isValid) {
          this.validate.step7 = "กรุณากรอกข้อมูลคำอ่าน/คำแปล";
        } else {
          this.input.listTranslation.forEach((item: any, index: any) => {
            item.save_index = index + 1;
          });
        }

        return isValid;
      }
      return true;
    } else if (this.currentID === 8) {
      this.updateSummaryProduct();
      // Condition
      if (this.input.listProduct.length < 1) {
        console.warn(`step8 is invalid.`);
        this.validate.step8 = "กรุณาเพิ่มจำพวกสินค้า/บริการ";
        return false;
      } else {
        let isValid = true;

        this.input.listProduct.forEach((item: any) => {
          let result = validateService("validateEFormSave01ProcessStep7", item);
          item.validate = result.validate;
          if (!result.isValid) {
            isValid = false;
          }
        });

        if (isValid) {
          this.input.listProduct.forEach((item: any, i: any) => {
            this.input.listProduct.forEach((product: any, j: any) => {
              if (
                i !== j &&
                item.request_item_sub_type_1_code ===
                  product.request_item_sub_type_1_code &&
                item.description === product.description
              ) {
                isValid = false;
                console.warn(`step8 is invalid.`);
                this.validate.step8 = "รายการสินค้าซํ้า";
              }
            });
          });
        }

        return isValid;
      }
    } else if (this.currentID === 9) {
      // Condition
      if (this.input.isLogoColorGroupTutelage) {
        let result = validateService(
          "validateEFormSave220ProcessStep9",
          this.input
        );
        this.validate = result.validate;
        return result.isValid;
      }
      return true;
    } else if (this.currentID === 10) {
      // Condition
      if (this.input.isLogoThreeDememsionsGroupTutelage) {
        let result = validateService(
          "validateEFormSave220ProcessStep10",
          this.input
        );
        this.validate = result.validate;
        return result.isValid;
      }
      return true;
    } else if (this.currentID === 11) {
      let result = validateService(
        "validateEFormSave01ProcessStep10",
        this.input
      );
      this.validate = result.validate;

      // Condition
      if (result.isValid) {
        this.conditionWizard("is_use_mark");
      }

      return result.isValid;
    } else if (this.currentID === 12) {
      if (this.currentSubID === 0) {
        let result = validateService(
          "validateEFormSave220ProcessStep11",
          this.input
        );
        this.validate = result.validate;

        // Condition
        if (result.isValid) {
          this.conditionWizard("assert_type");
        }

        return result.isValid;
      } else if (this.currentSubID === 1) {
        if (this.currentMiniID === 1) {
          // Condition
          if (this.input.listOwnerMarkA10.length < 1) {
            console.warn(`step4 is invalid.`);
            this.validate.step4 = "กรุณาเพิ่มเจ้าของเครื่องหมาย";
            return false;
          }
          return true;
        } else if (this.currentMiniID === 2) {
          // Condition
          if (!this.input.isDeemed1 && !this.input.isDeemed2) {
            console.warn(`step12 mini1 is invalid.`);
            this.validate.step12sub1mini2 = "กรุณาเลือกเงื่อนไข";
            return false;
          } else {
            if (this.input.isDeemed1 && !this.input.isDeemed2) {
              this.menuList[10].subList[0].miniList[2].isShow = true;
              this.menuList[10].subList[0].miniList[3].isShow = false;
              this.menuList[10].subList[0].sizeMiniList = 4;
              this.reRunMenuNumber();
            } else if (!this.input.isDeemed1 && this.input.isDeemed2) {
              this.menuList[10].subList[0].miniList[2].isShow = false;
              this.menuList[10].subList[0].miniList[3].isShow = true;
              this.menuList[10].subList[0].sizeMiniList = 4;
              this.reRunMenuNumber();
            } else {
              this.menuList[10].subList[0].miniList[2].isShow = true;
              this.menuList[10].subList[0].miniList[3].isShow = true;
              this.menuList[10].subList[0].sizeMiniList = 5;
              this.reRunMenuNumber();
            }
            return true;
          }
        } else if (this.currentMiniID === 3) {
          // Condition
          if (this.input.listRegistrationRequest.length < 1) {
            console.warn(`step12 sub1 mini3 is invalid.`);
            this.validate.step12sub1mini3 = "กรุณาเพิ่มคำขอจดทะเบียน";
            return false;
          } else {
            let isValid = true;

            this.input.listRegistrationRequest.forEach((item: any) => {
              let result = validateService(
                "validateEFormSave01Processstep12sub1mini3",
                item
              );
              item.validate = result.validate;
              if (!result.isValid) {
                isValid = false;
              }
            });

            return isValid;
          }
        } else if (this.currentMiniID === 4) {
          // Condition
          if (this.input.listProductShow.length < 1) {
            console.warn(`step12 sub1 mini4 is invalid.`);
            this.validate.step12sub1mini4 = "กรุณาเพิ่มสินค้าที่นำออกแสดง";
            return false;
          } else {
            let isValid = true;

            this.input.listProductShow.forEach((item: any) => {
              let result = validateService(
                "validateEFormSave01Processstep12sub1mini4",
                item
              );
              item.validate = result.validate;
              if (!result.isValid) {
                isValid = false;
              }
            });

            return isValid;
          }
        } else {
          if (this.input.listIsA10Checked[8]) {
            let result = validateService(
              "validateEFormSave220ProcessStep11Sub1Mini5",
              this.input
            );
            this.validate = result.validate;
            return result.isValid;
          }
          return true;
        }
      } else {
        if (this.currentMiniID === 1) {
          // Condition
          if (this.input.listOwnerMarkA19.length < 1) {
            console.warn(`step4 is invalid.`);
            this.validate.step4 = "กรุณาเพิ่มเจ้าของเครื่องหมาย";
            return false;
          }
          return true;
        } else {
          let result = validateService(
            "validateEFormSave01ProcessStep11Sub2Mini2",
            this.input
          );
          this.validate = result.validate;
          return result.isValid;
        }
      }
    } else if (this.currentID === 14) {
      let result = validateService(
        "validateEFormSave01ProcessStep15",
        this.input
      );
      this.validate = result.validate;
      return result.isValid;
    } else if (this.currentID === 15) {
      let isValid = false;

      if (
        this.input.isCheckAllOwnerSignature ||
        this.input.isCheckAllAgentSignature
      ) {
        isValid = true;
      }

      if (this.input.listAgentMark.length > 0) {
        this.input.listAgentMark.forEach((item: any) => {
          if (item.isCheck) {
            isValid = true;
          }
        });
      }

      if (!isValid) {
        console.warn(`step15 is invalid.`);
        this.validate.step15 = "กรุณาเลือกลงลายมือชื่อ";
      } else {
        // Set can edit
        this.menuList[14].canEdit = true;
      }

      return isValid;
    } else {
      return true;
    }
  }
  conditionWizard(condition: any): void {
    if (condition === "clone_mark_list") {
      if (
        this.input.listOwnerMarkA10.length === 0 &&
        this.input.listAgentMarkA10.length === 0
      ) {
        this.input.listOwnerMarkA10 = clone(this.input.listOwnerMark);
        this.input.listAgentMarkA10 = clone(this.input.listAgentMark);
      }
      if (
        this.input.listOwnerMarkA19.length === 0 &&
        this.input.listAgentMarkA19.length === 0
      ) {
        this.input.listOwnerMarkA19 = clone(this.input.listOwnerMark);
        this.input.listAgentMarkA19 = clone(this.input.listAgentMark);
      }

      let listAgentMarkRepresentative1 = [];
      let listAgentMarkRepresentative2 = [];
      this.input.listAgentMark.forEach((item: any, index: any) => {
        if (item.representative_type_code == "REPRESENTATIVE") {
          listAgentMarkRepresentative1.push(index);
        } else {
          listAgentMarkRepresentative2.push(index);
        }
      });
      this.input.listAgentMarkRepresentative1 = clone(
        listAgentMarkRepresentative1
      );
      this.input.listAgentMarkRepresentative2 = clone(
        listAgentMarkRepresentative2
      );

      if (!this.input.payer_name) {
        this.input.payer_name = this.input.listOwnerMark[0] ? this.input.listOwnerMark[0].name : "";
      }
    } else if (condition === "brand_logo_sound") {
      if (this.input.isBrandLogoSound) {
        this.input.listDocumentRequestChecked[5].isShow = true;
        this.reRunNumberArrayChecked("listDocumentRequestChecked");
      }
      this.input.listDocumentRequestChecked[5].isShow = false;
      this.reRunNumberArrayChecked("listDocumentRequestChecked");
    } else if (condition === "is_use_mark") {
      if (this.input.isUseMark) {
        this.input.listDocumentRequestChecked[4].isShow = true;
        this.reRunNumberArrayChecked("listDocumentRequestChecked");
      } else {
        this.input.listDocumentRequestChecked[4].isShow = false;
        this.reRunNumberArrayChecked("listDocumentRequestChecked");
      }
    } else if (condition === "assert_type") {
      if (this.input.save220_assert_type_code === "NOT_ASSERT") {
        this.menuList[11].subList[0].isShow = false;
        this.menuList[11].subList[1].isShow = false;
        this.menuList[11].sizeSubList = 0;
        this.reRunMenuNumber();
      } else if (this.input.save220_assert_type_code === "ASSERT_1") {
        this.menuList[11].subList[0].isShow = true;
        this.menuList[11].subList[1].isShow = false;
        this.menuList[11].sizeSubList = 1;
        this.reRunMenuNumber();
      } else {
        this.menuList[11].subList[0].isShow = true;
        this.menuList[11].subList[1].isShow = true;
        this.menuList[11].sizeSubList = 2;
        this.reRunMenuNumber();
      }
    }
  }
  manageWizard(wizard: any): void {
    let wizardList = wizard ? wizard.split("|") : [1, 1, 1, 1];
    this.currentID = +wizardList[0];
    this.currentStep = +wizardList[1];
    this.currentSubID = +wizardList[2];
    this.currentSubStep = +wizardList[3];
    this.calcProgressPercent(this.currentStep);

    if (this.currentID >= 4) {
      this.conditionWizard("clone_mark_list");
    }
    if (this.currentID >= 5) {
      if (this.input.save220_contact_type_code !== "OTHERS") {
        if (this.input.save220_contact_type_code === "OWNER") {
          this.input.listOwnerMark.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.contact_type_index = index;
            }
          });
        } else {
          this.input.listAgentMark.forEach((item: any, index: any) => {
            if (item.is_contact_person) {
              this.input.contact_type_index = index;
            }
          });
        }
      }
    }
    if (this.currentID >= 6) {
      if (
        this.currentID > 6 ||
        (this.currentID === 6 && this.currentSubID === 2)
      ) {
        this.conditionWizard("brand_logo_sound");
      }
    }
    if (this.currentID >= 11) {
      this.conditionWizard("is_use_mark");
    }
    if (this.currentID >= 12) {
      if (
        this.currentID > 12 ||
        (this.currentID === 12 && this.currentSubID === 0)
      ) {
        this.conditionWizard("assert_type");
      }
    }

    this.menuList.forEach((item: any) => {
      if (this.currentID > item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            sub.canEdit = true;
          });
        }
      }
      if (this.currentID === item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (this.currentSubID >= sub.id) {
              sub.canEdit = true;
            }
          });
        }
      }
    });
  }
  reRunMenuNumber(): void {
    let number = 1;
    let numberSub = 1;
    let numberMini = 1;
    this.menuList.forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
        numberSub = 1;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (sub.isShow) {
              sub.number = numberSub;
              numberSub++;
              numberMini = 1;
              if (sub.miniList) {
                sub.miniList.forEach((mini: any) => {
                  if (mini.isShow) {
                    mini.number = numberMini;
                    numberMini++;
                  }
                });
              }
            }
          });
        }
      }
    });
  }
  calcProgressPercent(currentStep: number): void {
    let lastItem = this.menuList[this.menuList.length - 1];
    let progressPercent = Math.round(
      ((currentStep + 1) / lastItem.number) * 100
    );
    this.progressPercent = progressPercent > 100 ? 100 : progressPercent;
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }

  //! <<< Other >>>
  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  displayAddress(value: any): any {
    return displayAddress(value);
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }
  displayFormatBytes(value: any): any {
    return displayFormatBytes(value);
  }
  onKey(e: any, name: any): void {
    if (e.keyCode === 13) {
      if (name === "request_number") {
        this.onSearch();
      }
      // if (name === "multiple_inter_registration_number") {
      //   this.input.listMultipleSearch.push(
      //     this.input.multiple_inter_registration_number
      //   );
      //   this.input.multiple_inter_registration_number = "";
      // }
    } else {
      if (name === "request_number") {
        this.input.listOwnerMark = [];
        this.input.listAgentMark = [];
        // TODO
      }
    }
  }
}
