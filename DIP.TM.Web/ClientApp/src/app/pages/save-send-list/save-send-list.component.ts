import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-save-send-list",
    templateUrl: "./save-send-list.component.html",
    styleUrls: ["./save-send-list.component.scss"]
})
export class SaveSendListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    // List SaveSend
    public listSaveSend: any[]
    public paginateSaveSend: any
    public perPageSaveSend: number[]
    // Response SaveSend
    public responseSaveSend: any
    public listSaveSendEdit: any


    // List SaveProcess
    public listSaveProcess: any[]
    public paginateSaveProcess: any
    public perPageSaveProcess: number[]
    // Response SaveProcess
    public responseSaveProcess: any

    //label_save_combobox || label_save_radio
    public saveStatusCodeList: any[]
    public departmentList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            request_date: getMoment(),
            save_status_code: 'DRAFT',
            request_number: '',
        }
        this.listSaveSend = []
        this.paginateSaveSend = CONSTANTS.PAGINATION.INIT
        this.perPageSaveSend = CONSTANTS.PAGINATION.PER_PAGE

        this.listSaveProcess = []

        //Master List
        this.master = {
            saveStatusCodeList: [],
            departmentList: [],
        }
        //Master List



        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //


        this.contactAddress = {
            address_type_code: "OWNER"
        }
        this.modalAddress = {}

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.route.queryParams.subscribe((param_url: any) => {
        })

        this.forkJoinService.initSaveSendList().subscribe((data: any) => {
            if (data) {
                this.help.Clone(data, this.master)
                this.master.saveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
            }
            if (this.editID) {
                this.SaveProcessService.SaveSendLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }

        })
    }

    constructor(
        private auth: Auth,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private SaveProcessService: SaveProcessService
    ) { }

    onClickSaveSendList(): void {
        // if(this.validateSaveSendList()) {
        // Open loading
        // Call api
        var param = {
            request_date: this.input.request_date,
            save_status_code: this.input.save_status_code,
            request_number: this.input.request_number,

        }

        this.callSaveSendList(param)
        // }
    }
    //! <<< Call API >>>
    callSaveSendList(params: any): void {
        this.global.setLoading(true)
        this.SaveProcessService.SaveSendList(this.help.GetFilterParams(params, this.paginateSaveSend)).subscribe((data: any) => {
            // if(isValidSaveSendListResponse(res)) {
            if (data) {
                // Set value
                this.listData(data)
                this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickSaveOtherSend(): void {
        //if(this.validateSaveOtherSend()) {
        // Open loading
        this.global.setLoading(true)
        // Call api

        this.listSaveSend.forEach((item: any) => {
            item.department_send_code = this.input.department
        })

        this.callSaveOtherSend(this.listSaveSend)
        //}
    }
    validateSaveOtherSend(): boolean {
        let result = validateService('validateSaveOtherSend', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callSaveOtherSend(params: any): void {
        this.SaveProcessService.SaveOtherSend(params).subscribe((data: any) => {
            // if(isValidSaveOtherSendResponse(res)) {
            if (data) {
                // Set value
                this.onClickSaveSendList()
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickSaveSendAdd(): void {
        this.listSaveSend.push({
            index: this.listSaveSend.length + 1,
            request_number: null,
            make_date_text: null,
            request_type_name: null,
            save_status_name: null,
            request_document_collect_link: null,
            evidence_address: null,
            department_send_name: null,
            department_send_date_text: null,

        })
        this.changePaginateTotal(this.listSaveSend.length, 'paginateSaveSend')
    }


    // Modal Location
    autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
        if (object[name].length >= length) {
            this.is_autocomplete_ListModalLocation_show = true

            let params = {
                name: object[name],
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListModalLocation(params)
        }
    }
    autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_ListModalLocation_show = false
        }, 200)
    }
    callAutocompleteChangeListModalLocation(params: any): void {
        if (this.input.is_autocomplete_ListModalLocation_load) return
        this.input.is_autocomplete_ListModalLocation_load = true
        let pThis = this
        // this.SaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
        //   if (data) {
        //     if (data.length == 1) {
        //       setTimeout(function () {
        //         pThis.autocompleteChooseListModalLocation(data[0])
        //       }, 200)
        //     } else {
        //       pThis.autocompleteListListModalLocation = data
        //     }
        //   }
        // })
        this.input.is_autocomplete_ListModalLocation_load = false
    }
    autocompleteChooseListModalLocation(data: any): void {
        this.inputModalAddress.address_sub_district_code = data.code
        this.inputModalAddress.address_sub_district_name = data.name
        this.inputModalAddress.address_district_code = data.district_code
        this.inputModalAddress.address_district_name = data.district_name
        this.inputModalAddress.address_province_code = data.province_code
        this.inputModalAddress.address_province_name = data.province_name
        this.inputModalAddress.address_country_code = data.country_code
        this.inputModalAddress.address_country_name = data.country_name
        this.is_autocomplete_ListModalLocation_show = false
    }
    onClickSave04ModalAddressSave(): void {
        Object.keys(this.inputModalAddress).forEach((item: any) => {
            this.inputModalAddressEdit[item] = this.inputModalAddress[item]
        })
        this.toggleModal("isModalPeopleEditOpen")
    }
    // Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listSaveSend = data.savesend_list || []
        this.help.PageSet(data, this.paginateSaveSend)

    }

    listData(data: any): void {
        this.listSaveSend = data.list || []
        this.help.PageSet(data, this.paginateSaveSend)

    }

    saveData(): any {
        let params = this.input

        params.savesend_list = this.listSaveSend || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }
    displayMoney(value: any): any {
        return displayMoney(value);
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

}
