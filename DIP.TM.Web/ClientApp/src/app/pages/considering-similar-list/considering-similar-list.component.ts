import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-considering-similar-list",
  templateUrl: "./considering-similar-list.component.html",
  styleUrls: ["./considering-similar-list.component.scss"]
})
export class ConsideringSimilarListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List ConsideringSimilar
  public listConsideringSimilar: any[]
  public paginateConsideringSimilar: any
  public perPageConsideringSimilar: number[]
  // Response ConsideringSimilar
  public responseConsideringSimilar: any
  public listConsideringSimilarEdit: any


  // List CheckingProcess
  public listCheckingProcess: any[]
  public paginateCheckingProcess: any
  public perPageCheckingProcess: number[]
  // Response CheckingProcess
  public responseCheckingProcess: any

  //label_save_combobox || label_save_radio
  public consideringReceiveStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  public popup: any

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      //id: null,
      //considering_receive_date: getMoment(),
      considering_receive_status_code: '',
      //request_number: '',
      //is_work_round_1: false,
      //is_work_round_2: false,
      //is_work_from_document: false,
      considering_similar_round_index: 1,
    }
    this.listConsideringSimilar = []
    this.paginateConsideringSimilar = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateConsideringSimilar.id = 'paginateConsideringSimilar'
    this.perPageConsideringSimilar = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listCheckingProcess = []

    //Master List
    this.master = {
      consideringReceiveStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.popup = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initConsideringSimilarList().subscribe((data: any) => {
      if (data) {
        this.master = data
        //this.master.consideringReceiveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });
        this.master.consideringReceiveStatusCodeList = [
          { "code": "", "name": "ทั้งหมด" },
          { "code": "DRAFT", "name": "รอดำเนินการ" }, //WAIT_DONE WAIT_CHANGE
          { "code": "WAIT", "name": "รอแก้ไข" },
          { "code": "SEND", "name": "ส่งงานแล้ว" },
        ];

      }
      if (this.editID) {
        this.CheckingProcessService.ConsideringSimilarLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }
    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }

  onClickConsideringSimilarList(): void {
    // if(this.validateConsideringSimilarList()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callConsideringSimilarList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callConsideringSimilarList(params: any): void {
    params = this.help.GetFilterParams(params, this.paginateConsideringSimilar)
    //params.search_by.forEach((item: any) => {
    //  if (item.key == "considering_receive_status_code") {
    //    if (item.value == "WAIT") {
    //      delete item.value
    //      item.values = ["WAIT_DONE", "WAIT_CHANGE"]
    //    }
    //  }
    //})

    this.CheckingProcessService.ConsideringSimilarListPage(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { List: data })

        this.CheckingProcessService.ConsideringSimilarDashboard({}).subscribe((data: any) => {
          // if(isValidConsideringSimilarListResponse(res)) {
          if (data) {
            this.input.round_wait = 0
            this.input.round_done = 0


            if (data.list && data.list.length > 0) {
              this.input.round_wait = data.list[0]["round_" + this.input.considering_similar_round_index + "_wait"]
              this.input.round_done = data.list[0]["round_" + this.input.considering_similar_round_index + "_done"]

              //console.log(data)
              // Set value
              //this.listData(data)
              //this.automateTest.test(this, { List: data })
            }
          }
          // }
          // Close loading
          this.global.setLoading(false)
        })
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickConsideringSimilarAdd(): void {
    this.listConsideringSimilar.push({
      index: this.listConsideringSimilar.length + 1,
      request_number: null,
      trademark_status_code: null,
      request_date_text: null,
      considering_receive_date_text: null,
      checking_receiver_by_name: null,
      considering_receive_status_name: null,
      considering_send_date_text: null,
      considering_reason_send_back: null,
      considering_remark: null,
      request_image: null,
      request_sound: null,

    })
    this.changePaginateTotal(this.listConsideringSimilar.length, 'paginateConsideringSimilar')
  }

  onClickConsideringSimilarEdit(item: any): void {
    if (item.considering_similar_round_index == 2) {
      var win = window.open("considering-similar-document/edit/" + item.id)
    } else {
      var win = window.open("considering-similar/edit/" + item.id)
    }
  }


  onClickConsideringSimilarDelete(item: any): void {
    // if(this.validateConsideringSimilarDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listConsideringSimilar.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listConsideringSimilar.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listConsideringSimilar.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listConsideringSimilar.length; i++) {
          if (this.listConsideringSimilar[i].is_check) {
            this.listConsideringSimilar[i].cancel_reason = rs
            this.listConsideringSimilar[i].status_code = "DELETE"
            this.listConsideringSimilar[i].is_deleted = true

            if (true && this.listConsideringSimilar[i].id) ids.push(this.listConsideringSimilar[i].id)
            // else this.listConsideringSimilar.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callConsideringSimilarDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listConsideringSimilar.length; i++) {
          this.listConsideringSimilar[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callConsideringSimilarDelete(params: any, ids: any[]): void {
    this.CheckingProcessService.ConsideringSimilarDelete(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listConsideringSimilar.length; i++) {
          if (this.listConsideringSimilar[i].id == id) {
            this.listConsideringSimilar.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listConsideringSimilar.length; i++) {
        this.listConsideringSimilar[i] = i + 1
      }

      this.onClickConsideringSimilarList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      //console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listConsideringSimilar = data.consideringsimilar_list || []
    this.changePaginateTotal(this.listConsideringSimilar.length, 'paginateConsideringSimilar')

  }

  listData(data: any): void {
    this.listConsideringSimilar = data.list || []
    this.help.PageSet(data, this.paginateConsideringSimilar)
    //this.listConsideringSimilar = data || []
    //this.changePaginateTotal(this.listConsideringSimilar.length, 'paginateConsideringSimilar')

  }


  togglePlayer(item: any = null): void {
    if (!item) {
      item = this.input
    }

    if (!item.player) {
      item.player = new Audio(item.sound_file_physical_path)
      item.player_action = null
    }

    if (item.player_action) {
      item.player.pause()
    } else {
      item.player.play()
    }
    item.player_action = !item.player_action
  }

  saveData(): any {
    // let params = this.input
    // params.consideringsimilar_list = this.listConsideringSimilar || []

    const params = {
      considering_receive_date: this.input.considering_receive_date,
      considering_receive_status_code: [this.input.considering_receive_status_code, 5],
      request_number: this.input.request_number,
      considering_similar_round_index: this.input.considering_similar_round_index.toString(),
      //page_index: +this.paginateConsideringSimilar.currentPage,
      //item_per_page: +this.paginateConsideringSimilar.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //    key: 'considering_receive_date',
      //    value: displayDateServer(this.input.considering_receive_date),
      //    operation: 0
      //}, {
      //    key: 'considering_receive_status_code',
      //    value: this.input.considering_receive_status_code,
      //    operation: 0
      //}, {
      //    key: 'request_number',
      //    values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //    operation: 5
      //}]
    }

    return params
  }

  onClickConsideringSimilarChange(): void {
    alert("ระบบกำลังพัฒนา กรุณาติดต่อ System Admin ครับ")
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginateConsideringSimilar') {
      this.onClickConsideringSimilarList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
