import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealRole01AppealKorAllListComponent } from './appeal-role01-appeal-kor-all-list.component';

describe('AppealRole01AppealKorAllListComponent', () => {
  let component: AppealRole01AppealKorAllListComponent;
  let fixture: ComponentFixture<AppealRole01AppealKorAllListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppealRole01AppealKorAllListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealRole01AppealKorAllListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
