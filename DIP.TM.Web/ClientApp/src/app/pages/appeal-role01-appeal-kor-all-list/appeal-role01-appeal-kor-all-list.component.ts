import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../global.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import '../../pages/appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss'
import { Auth } from "../../auth"
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  clone,
} from '../../helpers'


@Component({
  selector: 'app-appeal-role01-appeal-kor-all-list',
  templateUrl: './appeal-role01-appeal-kor-all-list.component.html',
  styleUrls: ['./appeal-role01-appeal-kor-all-list.component.scss']
})
export class AppealRole01AppealKorAllListComponent implements OnInit {

  public editID: any
  public input: any
  public validate: any
  public master: any

  // List RequestOtherItemSub
  public listRequestOtherItemSub: any[]
  public paginateRequestOtherItemSub: any
  public perPageRequestOtherItemSub: number[]
  // Response RequestOtherItemSub
  public responseRequestOtherItemSub: any
  public listRequestOtherItemSubEdit: any

  // List SaveProcess
  public listSaveProcess: any[]
  public paginateSaveProcess: any
  public perPageSaveProcess: number[]

  //check data
  public chkData: any[]

  constructor(
    private route: ActivatedRoute,
    private global: GlobalService,
    private auth: Auth,
    private help: Help,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService,
  ) { }

  ngOnInit() {
    this.input = {
      created_by_name: this.auth.getAuth().name,
      id: null,
      request_number: '',
      request_index: '',
      reference_number: '',
      from_date: getMoment(),
      to_date: getMoment(),
      save_status_code: '',
    }
    this.listSaveProcess = []
    this.paginateSaveProcess = CONSTANTS.PAGINATION.INIT
    this.perPageSaveProcess = CONSTANTS.PAGINATION.PER_PAGE

    //Master List
    this.master = {
      saveStatusCodeList: []
  }
    
    this.validate = {}

    this.callInit()
    this.onClickSave03List()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave03ProcessList().subscribe((data: any) => {
        if (data) {
            this.master = data
            this.master.saveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });

        }
        if (this.editID) {
            let pThis = this
            this.SaveProcessService.Save03Load(this.editID).subscribe((data: any) => {
                if (data) {
                    // Manage structure
                    pThis.loadData(data)
                }
                // Close loading
                pThis.global.setLoading(false)
            })
        }

        this.global.setLoading(false)

        console.log("Check->",this.chkData)
    })
}

  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  onClickSave03List(): void {
    // if(this.validateSave03List()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03List(this.saveData())
    // }
}


  //! <<< Call API >>>
  callSave03List(params: any): void {
    let pThis = this
    console.log("this params", params, this.paginateSaveProcess)

    this.SaveProcessService.Save03ListPage(this.help.GetFilterParams(params, this.paginateSaveProcess)).subscribe((data: any) => {
        // if(isValidSave03ListResponse(res)) {
        if (data) {
            // Set value
            pThis.listData(data)
            console.log("myData:", data)
        }
        // }
        // Close loading
        this.global.setLoading(false)
    })
}

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }

//   onClickSave03SaveProcessEdit(item: any): void {
//     var win = window.open("appeal-role01-appeal-kor03-save/edit/" + item.id)
// }

loadData(data: any): void {
  this.input = data

  this.listSaveProcess = data.save_process_list || []
  let index = 1
  index = 1
  this.listSaveProcess.map((item: any) => { item.is_check = false; item.index = index++; return item })
  this.changePaginateTotal(this.listSaveProcess.length, 'paginateSaveProcess')

}

listData(data: any): void {
  this.listSaveProcess = data.list || []
  this.help.PageSet(data, this.paginateSaveProcess)

  let request_item_sub_type_1_list = []
  this.listSaveProcess.forEach((item: any) => {
      if (item.product_01_list) {
          item.product_01_list.forEach((item_sub: any) => {
              request_item_sub_type_1_list.push(item_sub.request_item_sub_type_1_code)
          })
          item.item_sub_type_code_1 = request_item_sub_type_1_list.join(", ")
          item.save030_appeal_type_name = item.save030_appeal_type_codeNavigation ? item.save030_appeal_type_codeNavigation.name : ""
      }
  })
}

saveData(): any {
  let params = {
    request_number: this.input.request_number,
    request_index: "",
    make_date: this.input.from_date,
    save_status_code: this.input.save_status_code,
  }
  //params.requestotheritemsub_list = this.listRequestOtherItemSub || []

  return params
}

oneWayDataBinding(name: any, value: any, object: any): void {
  if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
  }

  if (object) {
      object[name] = value
  } else {
      this.input[name] = value
  }
  console.log(object)
}

  checkAllCheckBox(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
        this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

}

