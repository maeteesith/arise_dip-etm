import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role05-document",
  templateUrl: "./public-role05-document.component.html",
  styleUrls: ["./public-role05-document.component.scss"]
})
export class PublicRole05DocumentComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public url: any

  //public modalPeopleText: any

  //public contactAddress: any
  //public modalAddress: any
  //public modalAddressEdit: any


  //
  public tab_menu_show_index: any

  public tablePeople: any
  public tableRepresentative: any
  public contactAddress: any
  public tableProduct: any
  public tableJoiner: any
  public tableCertificationFile: any
  public request_mark_feature_code_list: any
  public tableRequestGroup: any
  public tableHistory: any

  public popup: any
  public send_back: any
  //

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
    }

    //Master List
    this.master = {
    }
    //Master List



    this.popup = {}
    this.send_back = {}

    this.tab_menu_show_index = 1

    this.tablePeople = {
      column_list: {
        index: "#",
        name: "ชื่อเจ้าของ",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      is_address_edit: true,
    }

    this.tableRepresentative = {
      column_list: {
        index: "#",
        name: "ชื่อตัวแทน",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      object_name: "ตัวแทน",
      is_address_edit: true,
    }

    this.contactAddress = {
      address_type_code: "OWNER"
    }

    this.tableProduct = {
      column_list: {
        index: "#",
        request_item_sub_type_1_code: "จำพวกสินค้า",
        description: "รายการสินค้า/บริการ",
        count: "จำนวน",
      },
      column_edit_list: {
        request_item_sub_type_1_code: {
          type: "textbox",
          group_by: "description",
        },
        description: {
          type: "textbox",
          type_click: "textarea",
          width: 800,
        },
      },
      can_added: true,
    }


    this.tableJoiner = {
      column_list: {
        index: "#",
        name: "ชื่อผู้ใช้ร่วม",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      object_name: "ผู้ใช้ร่วม",
      is_address_edit: true,
    }

    this.request_mark_feature_code_list = {}

    this.tableCertificationFile = {
      column_list: {
        index: "#",
        created_date_text: "วันที่อัพโหลด",
        file_name: "ชื่อไฟล์",
        file_size: "ขนาดไฟล์",
        remark: "หมายเหตุ",
      },
      column_edit_list: { remark: { type: "textbox", }, },
      can_deleted: true,
      button_list: [{
        name: "file_upload",
        title: "แนบไฟล์",
        icon: "upload-circle",
        file: "application/pdf", // image/*
        is_uploaded: true,
      }],
    }

    this.tableRequestGroup = {
      column_list: {
        index: "#",
        request_number: "คำขอเลขที่",
        registration_number: "ทะเบียนเลขที่",
        trademark_status_code: "สถานะคำขอ",
        name: "ชื่อเจ้าของ",
        save010_registration_group_status_code: "สถานะ",
        make_date: "วันที่มีคำสั่ง",
        allow_date: "วันที่อนุญาต",
      },
      column_edit_list: {
        save010_registration_group_status_code: { type: "combo_box", master: "save010RequestGroupStatusCodeList", },
        make_date: { type: "datetime", },
        allow_date: { type: "datetime", },
      },
      can_deleted: true,
      //can_added: true,
    }

    this.tableHistory = {
      column_list: {
        index: "#",
        make_date_text: "วันที่",
        created_by_name: "ผู้ออกคำสั่ง",
        description: "รายการ",
      },
      command: [{
        name: "history_report",
        title: "แสดงรายงาน",
        icon: "printer",
      }],
      has_link: ["row_item.file_id && row_item.file_id > 0 ? '/file/Content/' + row_item.file_id : null"],
    }

    ////
    //this.autocompleteListListModalLocation = []
    //this.is_autocomplete_ListModalLocation_show = false
    //this.is_autocomplete_ListModalLocation_load = false
    //this.modal = { isModalPeopleEditOpen: false, }
    //this.inputAddress = {}
    //this.inputModalAddress = {}
    ////


    //this.contactAddress = {
    //  address_type_code: "OWNER"
    //}
    //this.modalAddress = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    //this.url = this.sanitizer.bypassSecurityTrustResourceUrl("pdf/PublicRoundItem/" + this.editID);
    console.log(this.url)
    //this.route.queryParams.subscribe((param_url: any) => {
    //})

    this.forkJoinService.initPublicRole05Document().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
        //this.master.publicRole05DocumentInstructionRuleTypeCodeList.unshift({ "code": "", "name": "ไม่มี" })
        //this.input.public_instruction_rule_type_code = ""

        this.send_back["department_code"] = this.master.publicRole01SendBackDepartmentList[0].code
      }
      if (this.editID) {
        this.PublicProcessService.PublicRole02ConsiderPaymentLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)

            console.log('/pdf/KorMor01/' + this.input.request_number)
            this.url = this.sanitizer.bypassSecurityTrustResourceUrl('/pdf/KorMor01/' + this.input.request_number)

            let param = { save_id: this.editID }

            this.PublicProcessService.CertificationFileList(this.help.GetFilterParams(param)).subscribe((data: any) => {
              if (data) {
                this.tableCertificationFile.Set(data.list)

                this.PublicProcessService.HistoryList(this.help.GetFilterParams(param)).subscribe((data: any) => {
                  if (data) {
                    this.tableHistory.Set(data.list)
                  }
                })

                this.global.setLoading(false)
                this.automateTest.test(this)
              }
            })
          }
          // Close loading
          //this.global.setLoading(false)
          //this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }


  onClickPublicRole05DocumentSave(): void {
    //if(this.validatePublicRole05DocumentSave()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callPublicRole05DocumentSave(this.saveData())
    //}
  }
  validatePublicRole05DocumentSave(): boolean {
    let result = validateService('validatePublicRole05DocumentSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole05DocumentSave(params: any): void {
    this.PublicProcessService.PublicRole01CheckSave(params.full_view).subscribe((data: any) => {
      // if(isValidPublicRole05DocumentSaveResponse(res)) {
      if (data) {

        this.PublicProcessService.CertificationFileAdd(this.tableCertificationFile.Get()).subscribe((data: any) => {
          window.location.reload()
          //if (data && data.length > 0) {
          //  this.tableCertificationFile.Add(data[0])
          //}
        })
        // Set value
        //window.open("/public-role01-check/list", "_self")
        //this.loadData(data)
      }
      // }
      // Close loading
      //this.global.setLoading(false)
    })
  }

  onKeyPressPublicRole05DocumentRequestNumberAdd(event): void {
    console.log(event)
    if (event.key == "Enter") {
      var params = {
        request_number: this.tableRequestGroup.request_number_add
      }

      this.tableRequestGroup.request_number_add = ""

      // Call api
      this.callPressPublicRole05DocumentRequestNumberAdd(params)
    }
  }
  callPressPublicRole05DocumentRequestNumberAdd(params: any): void {
    this.PublicProcessService.SaveList(this.help.GetFilterParams(params)).subscribe((data: any) => {
      if (data && data.list && data.list.length > 0) {
        this.tableRequestGroup.Add({
          save_id: +this.editID,
          request_number: data.list[0].request_number,
          registration_number: data.list[0].registration_number,
          trademark_status_code: data.list[0].trademark_status_code,
          name: data.list[0].name,
        })
      }
      this.global.setLoading(false)
    })
  }

  onClickPublicRole05DocumentSend(): void {
    //if(this.validatePublicRole05DocumentSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callPublicRole05DocumentSend(this.saveData())
    //}
  }
  validatePublicRole05DocumentSend(): boolean {
    let result = validateService('validatePublicRole05DocumentSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole05DocumentSend(params: any): void {
    this.PublicProcessService.PublicRole05DocumentSend([params]).subscribe((data: any) => {
      // if(isValidPublicRole05DocumentSendResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.popup.isPopupSendOpen = true
        this.automateTest.test(this, { Send: 1 })
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickPublicRole05DocumentSendChange(): void {
    // Open loading
    this.global.setLoading(true)

    // Call api
    this.callPublicRole05DocumentSendChange(this.saveData())
    //}
  }
  //validatePublicRole05DocumentSendChange(): boolean {
  //  let result = validateService('validatePublicRole05DocumentSendChange', this.input)
  //  this.validate = result.validate
  //  return result.isValid
  //}
  //! <<< Call API >>>
  callPublicRole05DocumentSendChange(params: any): void {
    this.PublicProcessService.PublicRole05DocumentSendChange([params]).subscribe((data: any) => {
      // if(isValidPublicRole05DocumentSendChangeResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.popup.is_send_back_show = false
        this.popup.isPopupSendOpen = true
        this.automateTest.test(this, { Send: 1 })
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }




  onClickCommand($event): void {
    console.log($event)

    if ($event.object_list.length > 0) {
      if ($event.command.name == "instruction_report") {
        window.open("pdf/ConsideringSimilaInstruction/" + this.input.request_number + "_" + $event.object_list.map((item: any) => { return item.id }).join("|"))
      } else if ($event.command.name == "history_report") {
        window.open("pdf/Save010History/" + this.input.request_number + "_" + $event.object_list.map((item: any) => { return item.id }).join("|"))
      } else if ($event.command.name == "file_upload") {
        var param = {
          save_id: +this.editID,
          file_id: +$event.object_list[0].id,
        }
        this.PublicProcessService.CertificationFileAdd([param]).subscribe((data: any) => {
          if (data && data.length > 0) {
            this.tableCertificationFile.Add(data[0])
          }
        })
      }
    }
  }


  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data
    this.input.public_instruction_rule_type_code = ""


    this.help.Clone(data.full_view.contact_address || {}, this.contactAddress)
    //this.contactAddress = data.full_view.contact_address


    this.tablePeople.Set(data.full_view.people_list)
    this.tableRepresentative.Set(data.full_view.representative_list)
    this.tableProduct.Set(data.full_view.product_list)
    this.tableJoiner.Set(data.full_view.joiner_list)
    this.tableRequestGroup.Set(data.full_view.request_group_list)

    this.request_mark_feature_code_list = {}
    if (data.full_view.request_mark_feature_code_list) data.full_view.request_mark_feature_code_list.split(',').forEach((item: any) => { this.request_mark_feature_code_list[item] = 1 })
  }

  listData(data: any): void {

  }

  saveData(): any {
    let params = this.input

    params.full_view.contact_address = this.contactAddress

    params.full_view.request_mark_feature_code_list = []
    Object.keys(this.request_mark_feature_code_list).forEach((item: any) => {
      if (this.request_mark_feature_code_list[item] == 1) {
        params.full_view.request_mark_feature_code_list.push(item)
      }
    })
    params.full_view.request_mark_feature_code_list = params.full_view.request_mark_feature_code_list.join(',')

    params.id = +this.editID
    //params.save_id = +this.editID
    params.department_code = this.send_back.department_code
    params.reason = this.send_back.reason
    params.public_instruction_rule_type_code = this.input.public_instruction_rule_type_code
    params.public_total_price = this.input.public_total_price
    params.public_remark_1 = this.input.public_remark_1
    params.public_remark_2 = this.input.public_remark_2
    params.is_check = true

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

}
