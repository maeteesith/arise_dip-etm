import { Component, OnInit } from '@angular/core';
import { getMoment } from 'src/app/helpers/manageData';
import { ForkJoinService } from '../../services/fork-join.service'
import { GlobalService } from '../../global.service'
import { AppealRequestList } from '../../helpers/appealMaster'

@Component({
  selector: 'app-appeal-role03-send-meeting-agenda-list',
  templateUrl: './appeal-role03-send-meeting-agenda-list.component.html',
  styleUrls: ['./appeal-role03-send-meeting-agenda-list.component.scss']
})
export class AppealRole03SendMeetingAgendaListComponent implements OnInit {

  constructor(
    private forkJoinService: ForkJoinService,
    private global: GlobalService,
  ) { }


  public input: any
  public requestList: any[]

  ngOnInit() {
    this.input = {
      request_number: null,
      request_type: "03",
      start_date: getMoment(),
      end_date: getMoment()
    }
    this.requestList = AppealRequestList.map( Item => {
      return Item
    })
  }
}
