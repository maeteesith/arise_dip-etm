import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-document-role05-release-request",
  templateUrl: "./document-role05-release-request.component.html",
  styleUrls: ["./document-role05-release-request.component.scss"]
})
export class DocumentRole05ReleaseRequestComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  //public contactAddress: any
  //public modalAddress: any
  //public modalAddressEdit: any

  // List CheckingInstructionList
  public listCheckingInstructionList: any[]
  public paginateCheckingInstructionList: any
  public perPageCheckingInstructionList: number[]
  // Response CheckingInstructionList
  public responseCheckingInstructionList: any
  public listCheckingInstructionListEdit: any

  // List AddressList
  public listAddressList: any[]
  public paginateAddressList: any
  public perPageAddressList: number[]
  // Response AddressList
  public responseAddressList: any
  public listAddressListEdit: any

  // List CheckingInstructionDocumentList
  public listCheckingInstructionDocumentList: any[]
  public paginateCheckingInstructionDocumentList: any
  public perPageCheckingInstructionDocumentList: number[]
  // Response CheckingInstructionDocumentList
  public responseCheckingInstructionDocumentList: any
  public listCheckingInstructionDocumentListEdit: any

  public popup: any
  public rowEdit: any

  public request_similar_graph: any
  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public tableList: any

  public player: any
  public player_action: any

  public tab_menu_show_index: any

  public tableInstructionRule: any
  public tableSaveDocument: any
  public tablePeople: any
  public tableRepresentative: any
  public contactAddress: any
  public tableProduct: any
  public tableReceiptItem: any
  public tableHistory: any
  public tableDocumentScan: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      name: '',
      request_item_sub_type_1_code_text: '',
      request_item_type_name: '',
      is_trademark_join: false,
      request_date: '',
      representative_name: '',
      house_number: '',
      is_trademark_sound: false,
      view: {}
    }
    this.listCheckingInstructionList = []
    this.paginateCheckingInstructionList = CONSTANTS.PAGINATION.INIT
    this.perPageCheckingInstructionList = CONSTANTS.PAGINATION.PER_PAGE

    this.listAddressList = []
    this.paginateAddressList = CONSTANTS.PAGINATION.INIT
    this.perPageAddressList = CONSTANTS.PAGINATION.PER_PAGE

    this.listCheckingInstructionDocumentList = []
    this.paginateCheckingInstructionDocumentList = CONSTANTS.PAGINATION.INIT
    this.perPageCheckingInstructionDocumentList = CONSTANTS.PAGINATION.PER_PAGE

    this.popup = {
      row_item_edit: {},
      item_view: {},
    }

    this.tableList = {
      column_list: {
        index: "#",
        save_request_number: "เลขที่คำขอ",
        post_round_instruction_rule_name: "ตค.",
        book_number: "เลข พณ.",
        book_start_date: "ออกหนังสือ",
        book_acknowledge_date: "ตอบรับ",
        book_end_date: "ครบกำหนด",
        considering_instruction_rule_status_name: "สถานะ ตค.",
        trademark_status_code: "สถานะคำขอ",
        save010_case28_count: "ม. 28",
        document_role05_receive_status_name: "สถานะ",
      },
      command_item: [{
        name: "instruction_document_edit",
      }, {
        name: "instruction_document_view",
        icon: "search",
      },],
      can_checked: true,
    }


    this.tab_menu_show_index = 1

    this.tableInstructionRule = {
      column_list: {
        index: "#",
        instruction_rule_name: "คำสั่ง",
        instruction_date: "วันที่สั่ง",
        instructor_by_name: "นายทะเบียน",
        considering_book_status_name: "สถานะหนังสือ",
        instruction_send_date: "วันที่ส่งคำสั่ง",
        book_number: "เลข พณ.",
        document_role02_receiver_by_name: "ผู้รับงาน",
        document_role02_receive_remark: "หมายเหตุ",
        considering_instruction_rule_status_name: "สถานะ",
      },
    }
    this.tableSaveDocument = {
      column_list: {
        index: "#",
        request_type_name: "เอกสาร",
        make_date: "วันที่รับ",
        consider_similar_document_status_name: "พิจารณา",
        consider_similar_document_date: "วันที่พิจารณา",
        //instruction_rule_name: "เอกสารสแกน",
        consider_similar_document_remark: "รายละเอียด",
        //consider_similar_document_remark: "เหตุผล",
      },
    }
    this.tablePeople = {
      column_list: {
        index: "#",
        name: "ชื่อเจ้าของ",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
    }
    this.tableRepresentative = {
      column_list: {
        index: "#",
        name: "ชื่อตัวแทน",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      object_name: "ตัวแทน",
      //is_address_edit: true,
    }
    this.tableProduct = {
      column_list: {
        index: "#",
        request_item_sub_type_1_code: "จำพวกสินค้า",
        description: "รายการสินค้า/บริการ",
        //count: "จำนวน",
      },
    }
    this.tableReceiptItem = {
      column_list: {
        index: "#",
        request_type_name: "รายการ",
        receipt_number: "เลขที่ใบเสร็จ",
        receive_date: "วันที่รับเงิน",
        total_price: "จำนวนเงิน",
        receiver_name: "ผู้รับเงิน",
        receipt_status_name: "สถานะ",
      },
    }
    this.tableHistory = {
      column_list: {
        index: "#",
        make_date: "วันที่",
        created_by_name: "ผู้ออกคำสั่ง",
        description: "รายการ",
      },
    }
    this.tableDocumentScan = {
      column_list: {
        index: "#",
        request_document_collect_type_name: "ชื่อเอกสาร",
        updated_by_name: "เจ้าหน้าที่",
        created_date: "วันที่แนบ",
      },
      //has_link: ["'/File/Content/' + row_item.file_id"],
    }

    //Master List
    this.master = {
    }
    //Master List

    this.request_similar_graph = {
      request_number: "1800000",
      level_index: -1,
      child_list: [{
        request_number: "1700000",
        level_index: 0,
        child_list: [],
        //  child_list: [{
        //    request_number: "1700100",
        //    level_index: 1,
        //    child_list: [{
        //      request_number: "1700110",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700120",
        //      level_index: 2,
        //      child_list: [{
        //        request_number: "1700121",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700122",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700123",
        //        level_index: 3,
        //        child_list: [{
        //          request_number: "1700123-1",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-2",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-3",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-4",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-4",
        //          level_index: 4,
        //        }]
        //      }, {
        //        request_number: "1700124",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700125",
        //        level_index: 3,
        //      }]
        //    }, {
        //      request_number: "1700130",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700140",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700150",
        //      level_index: 2,
        //    }]
        //  }, {
        //    request_number: "1700200",
        //    level_index: 1,
        //    child_list: [{
        //      request_number: "1700110",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700120",
        //      level_index: 2,
        //      child_list: [{
        //        request_number: "1700121",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700122",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700123",
        //        level_index: 3,
        //        child_list: [{
        //          request_number: "1700123-1",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-2",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-3",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-4",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-4",
        //          level_index: 4,
        //        }]
        //      }, {
        //        request_number: "1700124",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700125",
        //        level_index: 3,
        //      }]
        //    }, {
        //      request_number: "1700130",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700140",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700150",
        //      level_index: 2,
        //    }]
        //  }, {
        //    request_number: "1700300",
        //    level_index: 1,
        //    child_list: [{
        //      request_number: "1700110",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700120",
        //      level_index: 2,
        //      child_list: [{
        //        request_number: "1700121",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700122",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700123",
        //        level_index: 3,
        //        child_list: [{
        //          request_number: "1700123-1",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-2",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-3",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-4",
        //          level_index: 4,
        //        }, {
        //          request_number: "1700123-4",
        //          level_index: 4,
        //        }]
        //      }, {
        //        request_number: "1700124",
        //        level_index: 3,
        //      }, {
        //        request_number: "1700125",
        //        level_index: 3,
        //      }]
        //    }, {
        //      request_number: "1700130",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700140",
        //      level_index: 2,
        //    }, {
        //      request_number: "1700150",
        //      level_index: 2,
        //    }]
        //  }, {
        //    request_number: "1700500",
        //    level_index: 1,
        //  }, {
        //    request_number: "1700500",
        //    level_index: 1,
        //  }, {
        //    request_number: "1700600",
        //    level_index: 1,
        //  }, {
        //    request_number: "1700700",
        //    level_index: 1,
        //  }],
      }],
    }

    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    //this.modalAddress = {}

    this.callInit()
  }

  displaytTopLevel() { //level -1
    const element = document.getElementById('outline-level-top');
    const element2 = document.getElementById('level-0');
    const element3 = document.getElementById('outline-level-0');

    if (element2.style.display === 'inline-block') {
      element.style.display = 'none'
      element2.style.display = 'none'
      element3.style.display = 'none'
      document.getElementById('node').innerHTML = ""
    } else {
      element2.style.display = 'inline-block'
      element.style.display = 'inline-block'
    }
  }

  displaytNextLevel(event, id, data) { //level 0
    const element = document.getElementById('outline-level-0');

    if (element.style.display === 'inline-block') {
      document.getElementById('node').innerHTML = ""
      element.style.display = 'none'
    } else {
      element.style.display = 'inline-block'
      this.displayNode(event, id, data, null)
    }

  }

  displayNode2(event, id, data, idLevel1, changeSymbol) { //level >2
    event.stopPropagation();
    const id_div_ui = `${id}-0`
    const element = document.getElementById(id);
    const element2 = document.getElementById(id_div_ui)
    const element3 = document.getElementById(idLevel1)
    if (element2) {
      element3.setAttribute("class", `${element3.className} test`);
      if (data[0].level_index > 2) {
        element3.style.width = `${data[0].level_index - 2}80px`

      } else {
        element3.style.width = `auto`
      }
      changeSymbol()
      element2.parentNode.removeChild(element2);
    } else if (data.length > 0) {
      element3.style.width = `${data[0].level_index - 1}80px`
      const div = document.createElement("div");
      div.setAttribute("class", `div-border-left`);
      div.setAttribute("id", `${id_div_ui}`);
      data.forEach((obj, i) => {
        const idDiv = `${id_div_ui}-${i}`
        const div2 = document.createElement("div");
        const div3 = document.createElement("div");
        const label = document.createElement("label");

        div2.setAttribute("class", `div-border-bottom`);
        div3.setAttribute("id", idDiv);
        label.setAttribute("class", `text-data`);

        let text = obj.request_number
        if (obj.child_list) {
          text = `<div class="btn-symbol2">+</div><label class="label-text">${text}</label>`
          label.style.cursor = "pointer"
          label.addEventListener("click", (event) => {
            label.innerHTML = `<div class="btn-symbol2"> - </div><label class="label-text">${obj.request_number}</label>`
            const changeSymbol = () => {
              label.innerHTML = text
            }
            this.displayNode2(event, idDiv, obj.child_list, idLevel1, changeSymbol)
          });
        }


        label.innerHTML = text
        if (i === data.length - 1) {
          div3.setAttribute("class", `div-border-bottom2`);
          div3.appendChild(label);
        } else {
          div3.appendChild(div2);
          div3.appendChild(label);
        }
        div.appendChild(div3);
      })
      element.appendChild(div);
    }
  }

  displayNode(event, id, data, changeSymbol) {
    //event.stopPropagation();
    const id_div_ui = `div-${id}-0`
    const element = document.getElementById(id);
    const element2 = document.getElementById(id_div_ui)

    if (element2) {
      changeSymbol()
      element2.parentNode.removeChild(element2);
    } else if (data.length > 0) {
      let list = []
      let list2 = []
      data.map((e, i) => {
        list2.push(e)
        if (list2.length == 3 || !data[i + 1]) {
          list.push(list2)
          list2 = []
        }
      })
      const div = document.createElement("div");
      div.setAttribute("id", `${id_div_ui}`);

      list.forEach((e, indexList) => {
        const ul = document.createElement("ul");
        ul.setAttribute("id", `${id_div_ui}-${indexList}`);
        if (list.length !== indexList + 1) {
          ul.setAttribute("class", "ul borderLeft");
        } else {
          ul.setAttribute("class", "ul");
        }
        div.appendChild(ul);
        e.forEach((obj, i) => {
          const id_div_li = `li-${id}-${indexList}-${i}`
          const div2 = document.createElement("div");
          const li = document.createElement("li");
          const span = document.createElement("span");
          if (i == 0) {
            li.setAttribute("class", 't-li line');
          } else {
            li.setAttribute("class", 't-li');
          }
          div2.setAttribute("class", 'outline-bottom');
          span.setAttribute("class", 't-span');

          li.setAttribute("id", id_div_li);
          ul.appendChild(li);
          let text = e[i].request_number
          if (obj.child_list) {
            text = `<div class="btn-symbol">+</div><label class="label-text">${text}</label>`
            span.style.cursor = "pointer"
            span.addEventListener("click", (event) => {
              span.innerHTML = `<div class="btn-symbol"> - </div><label class="label-text">${e[i].request_number}</label>`
              const changeSymbol = () => {
                span.innerHTML = text
              }
              if (obj.level_index >= 1) {
                this.displayNode2(event, id_div_li, obj.child_list, id_div_li, changeSymbol)
              } else {
                this.displayNode(event, id_div_li, obj.child_list, changeSymbol)
              }
            });
          }
          span.innerHTML = text;
          li.appendChild(div2);
          li.appendChild(span);

        });
      })
      element.appendChild(div);

    }
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
    })

    this.forkJoinService.initDocumentRole05ReleaseRequest().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
      }
      if (this.editID) {
        this.DocumentProcessService.DocumentRole05ReleaseRequestLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }


  onClickDocumentRole05ReleaseRequestCheckingInstructionListAdd(): void {
    this.listCheckingInstructionList.push({
      index: this.listCheckingInstructionList.length + 1,
      instruction_rule_name: null,
      book_number: null,
      book_start_date_text: null,
      save010_checking_instruction_document_remark: null,
      considering_instruction_rule_status_name: null,

    })
    this.changePaginateTotal(this.listCheckingInstructionList.length, 'paginateCheckingInstructionList')
  }

  onClickDocumentRole05ReleaseRequestAddressListAdd(): void {
    this.listAddressList.push({
      index: this.listAddressList.length + 1,
      house_number: null,

    })
    this.changePaginateTotal(this.listAddressList.length, 'paginateAddressList')
  }

  onClickDocumentRole05ReleaseRequestAddressListEdit(item: any): void {
    var win = window.open("/" + item.id)
  }



  onClickDocumentRole05ReleaseRequestAddressListDelete(item: any): void {
    // if(this.validateDocumentRole05ReleaseRequestAddressListDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listAddressList.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listAddressList.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listAddressList.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listAddressList.length; i++) {
          if (this.listAddressList[i].is_check) {
            this.listAddressList[i].cancel_reason = rs
            this.listAddressList[i].status_code = "DELETE"
            this.listAddressList[i].is_deleted = true

            if (true && this.listAddressList[i].id) ids.push(this.listAddressList[i].id)
            // else this.listAddressList.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole05ReleaseRequestAddressListDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listAddressList.length; i++) {
          this.listAddressList[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole05ReleaseRequestAddressListDelete(params: any, ids: any[]): void {
    //this.DocumentProcessService.DocumentRole05ReleaseRequestAddressListDelete(params).subscribe((data: any) => {
    //  // if(isValidDocumentRole05ReleaseRequestAddressListDeleteResponse(res)) {

    //  ids.forEach((id: any) => {
    //    for (let i = 0; i < this.listAddressList.length; i++) {
    //      if (this.listAddressList[i].id == id) {
    //        this.listAddressList.splice(i--, 1);
    //      }
    //    }
    //  });

    //  for (let i = 0; i < this.listAddressList.length; i++) {
    //    this.listAddressList[i] = i + 1
    //  }

    //  this.onClickDocumentRole05ReleaseRequestAddressList()
    //  // Close loading
    //  this.global.setLoading(false)
    //})
  }

  onClickDocumentRole05ReleaseRequestCheckingInstructionDocumentListAdd(): void {
    this.listCheckingInstructionDocumentList.push({
      index: this.listCheckingInstructionDocumentList.length + 1,
      instruction_rule_name: null,
      instruction_send_date_text: null,
      book_round_01_start_date_text: null,
      book_round_02_start_date_text: null,
      book_number: null,
      document_role02_receive_status_name: null,

    })
    this.changePaginateTotal(this.listCheckingInstructionDocumentList.length, 'paginateCheckingInstructionDocumentList')
  }

  onClickDocumentRole05ReleaseRequestCheckingInstructionDocumentListEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickDocumentRole05ReleaseRequestCheckingInstructionDocumentListDelete(item: any): void {
    // if(this.validateDocumentRole05ReleaseRequestCheckingInstructionDocumentListDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listCheckingInstructionDocumentList.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listCheckingInstructionDocumentList.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listCheckingInstructionDocumentList.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
          if (this.listCheckingInstructionDocumentList[i].is_check) {
            this.listCheckingInstructionDocumentList[i].cancel_reason = rs
            this.listCheckingInstructionDocumentList[i].status_code = "DELETE"
            this.listCheckingInstructionDocumentList[i].is_deleted = true

            if (true && this.listCheckingInstructionDocumentList[i].id) ids.push(this.listCheckingInstructionDocumentList[i].id)
            // else this.listCheckingInstructionDocumentList.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole05ReleaseRequestCheckingInstructionDocumentListDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
          this.listCheckingInstructionDocumentList[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole05ReleaseRequestCheckingInstructionDocumentListDelete(params: any, ids: any[]): void {
    //this.DocumentProcessService.DocumentRole05ReleaseRequestCheckingInstructionDocumentListDelete(params).subscribe((data: any) => {
    //  // if(isValidDocumentRole05ReleaseRequestCheckingInstructionDocumentListDeleteResponse(res)) {

    //  ids.forEach((id: any) => {
    //    for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
    //      if (this.listCheckingInstructionDocumentList[i].id == id) {
    //        this.listCheckingInstructionDocumentList.splice(i--, 1);
    //      }
    //    }
    //  });

    //  for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
    //    this.listCheckingInstructionDocumentList[i] = i + 1
    //  }

    //  this.onClickDocumentRole05ReleaseRequestCheckingInstructionDocumentList()
    //  // Close loading
    //  this.global.setLoading(false)
    //})
  }


  onClickCommand($event): void {
    console.log($event)

    if ($event.command == "instruction_document_edit") {
      this.popup.is_edit_show = true
      this.rowEdit = $event.object_list[0]

      //console.log($event.object_list[0])
      this.help.Clone($event.object_list[0], this.popup.row_item_edit)
      this.popup.row_item_edit.document_role04_release_request_send_type_code = this.popup.row_item_edit.document_role04_release_request_send_type_code || 'DOCUMENT'
      //console.log(this.popup.row_item_edit)
    } else if ($event.command == "instruction_document_view") {
      //this.popup.row_item_edit = $event.object_list[0]
      //} else if ($event.command == "address_add") {

      //this.popup.is_edit_show = false
      this.global.setLoading(true)
      this.DocumentProcessService.DocumentRole04ReleaseRequestItemLoad($event.object_list[0].save_id).subscribe((data: any) => {
        this.popup.is_view_show = true
        if (data) {
          this.popup.item_view = data.view

          setTimeout(() => {
            this.tableInstructionRule.Set(data.instruction_rule_list)
            this.tableSaveDocument.Set(data.document_list)

            this.tablePeople.Set(data.view.people_list)
            this.tableRepresentative.Set(data.view.representative_list)

            this.help.Clone(data.view.contact_address, this.contactAddress)
            this.tableProduct.Set(data.view.product_list)
            this.tableReceiptItem.Set(data.receipt_item_list)
            this.tableHistory.Set(data.history_list)
            this.tableDocumentScan.Set(data.document_scan_list)
          }, 100)

          this.global.setLoading(false)
        }
        //    // Close loading
        //    this.global.setLoading(false)
      })
    }
  }

  onClickDocumentRole05ReleaseRequestSave(): void {
    this.help.Clone(this.popup.row_item_edit, this.rowEdit)
    this.popup.is_edit_show = false
  }

  onClickDocumentRole05ReleaseRequestSend(row_item = null): void {
    row_item = row_item ? [row_item] : this.tableList.Get().filter(r => r.is_check)

    this.popup.is_edit_show = false

    if (row_item.length > 0) {
      this.global.setLoading(true)
      this.DocumentProcessService.DocumentRole05ReleaseRequestSend(row_item).subscribe((data: any) => {
        if (data) {
          // Manage structure
          //this.loadData(data)
          this.popup.isPopupSendOpen = true

          this.loadData(data, false)
        }
        // Close loading
        this.global.setLoading(false)
      })
    }
  }

  onClickDocumentRole05ReleaseRequestSendChange(): void {
    this.popup.is_edit_show = false
    this.global.setLoading(true)

    this.DocumentProcessService.DocumentRole05ReleaseRequestSendChange([this.popup.row_item_edit]).subscribe((data: any) => {
      if (data) {
        // Manage structure
        //this.loadData(data)
        this.popup.isPopupSendOpen = true

        this.loadData(data, false)
      }
      // Close loading
      this.global.setLoading(false)
    })
  }

  //// Modal Location
  //autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
  //  if (object[name].length >= length) {
  //    this.is_autocomplete_ListModalLocation_show = true

  //    let params = {
  //      name: object[name],
  //      paging: {
  //        item_per_page: item_per_page
  //      },
  //    }

  //    this.callAutocompleteChangeListModalLocation(params)
  //  }
  //}
  //autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
  //  let pThis = this
  //  setTimeout(function () {
  //    pThis.is_autocomplete_ListModalLocation_show = false
  //  }, 200)
  //}
  //callAutocompleteChangeListModalLocation(params: any): void {
  //  if (this.input.is_autocomplete_ListModalLocation_load) return
  //  this.input.is_autocomplete_ListModalLocation_load = true
  //  let pThis = this
  //  // this.DocumentProcessService.Save05ListModalLocation(params).subscribe((data: any) => {
  //  //   if (data) {
  //  //     if (data.length == 1) {
  //  //       setTimeout(function () {
  //  //         pThis.autocompleteChooseListModalLocation(data[0])
  //  //       }, 200)
  //  //     } else {
  //  //       pThis.autocompleteListListModalLocation = data
  //  //     }
  //  //   }
  //  // })
  //  this.input.is_autocomplete_ListModalLocation_load = false
  //}
  //autocompleteChooseListModalLocation(data: any): void {
  //  this.inputModalAddress.address_sub_district_code = data.code
  //  this.inputModalAddress.address_sub_district_name = data.name
  //  this.inputModalAddress.address_district_code = data.district_code
  //  this.inputModalAddress.address_district_name = data.district_name
  //  this.inputModalAddress.address_province_code = data.province_code
  //  this.inputModalAddress.address_province_name = data.province_name
  //  this.inputModalAddress.address_country_code = data.country_code
  //  this.inputModalAddress.address_country_name = data.country_name
  //  this.is_autocomplete_ListModalLocation_show = false
  //}
  //onClickSave05ModalAddressSave(): void {
  //  Object.keys(this.inputModalAddress).forEach((item: any) => {
  //    this.inputModalAddressEdit[item] = this.inputModalAddress[item]
  //  })
  //  this.toggleModal("isModalPeopleEditOpen")
  //}
  //// Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any, is_updated_graph = true): void {
    this.input = data

    this.tableList.Set(data.document_role04_release_request_list)

    if (is_updated_graph) {
      var child_list = data.document_role04_release_request_list.map((item: any) => {
        return {
          request_number: item.save_request_number,
          level_index: 1,
        }
      })

      this.request_similar_graph = {
        request_number: this.input.view.request_number,
        level_index: -1,
        is_hide: true,
        child_list: [{
          request_number: this.input.view.request_number,
          level_index: 0,
          child_list: child_list,
        }]
      }

      this.displaytTopLevel()
      this.displaytNextLevel(null, 'node', this.request_similar_graph.child_list[0].child_list)
    }
    //[{
    //  request_number: "1700000",
    //  level_index: 0,
    //  child_list: [{

    //this.listCheckingInstructionList = data.checkinginstructionlist_list || []
    //this.listAddressList = data.addresslist_list || []
    //this.listCheckingInstructionDocumentList = data.checkinginstructiondocumentlist_list || []
    //this.help.PageSet(data, this.paginateCheckingInstructionList)
    //this.help.PageSet(data, this.paginateAddressList)
    //this.help.PageSet(data, this.paginateCheckingInstructionDocumentList)

  }

  listData(data: any): void {
    this.listCheckingInstructionList = data.list || []
    this.listAddressList = data.list || []
    this.listCheckingInstructionDocumentList = data.list || []
    this.help.PageSet(data, this.paginateCheckingInstructionList)
    this.help.PageSet(data, this.paginateAddressList)
    this.help.PageSet(data, this.paginateCheckingInstructionDocumentList)

  }

  saveData(): any {
    let params = this.input

    params.checkinginstructionlist_list = this.listCheckingInstructionList || []
    params.addresslist_list = this.listAddressList || []
    params.checkinginstructiondocumentlist_list = this.listCheckingInstructionDocumentList || []

    return params
  }

  onClickOpen(url: any, id: any): void {
    window.open(url + id)
  }


  togglePlayer(): void {
    if (this.player_action) {
      this.player.pause()
    } else {
      this.player.play()
    }
    this.player_action = !this.player_action
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+ page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

}
