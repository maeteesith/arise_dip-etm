import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  clone,
  displayDateServer
} from '../../helpers'

@Component({
  selector: "app-document-role02-item-list",
  templateUrl: "./document-role02-item-list.component.html",
  styleUrls: ["./document-role02-item-list.component.scss"]
})
export class DocumentRole02ItemListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List DocumentRole02Item
  public listDocumentRole02Item: any[]
  public paginateDocumentRole02Item: any
  public perPageDocumentRole02Item: number[]
  // Response DocumentRole02Item
  public responseDocumentRole02Item: any
  public listDocumentRole02ItemEdit: any


  // List DocumentProcess
  public listDocumentProcess: any[]
  public paginateDocumentProcess: any
  public perPageDocumentProcess: number[]
  // Response DocumentProcess
  public responseDocumentProcess: any

  //label_save_combobox || label_save_radio
  public postRoundTypeCodeList: any[]
  public departmentGroupCodeList: any[]
  public documentRole02StatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //post_round_document_post_start_date: getMoment(),
      //post_round_document_post_end_date: getMoment(),
      post_round_type_code: '',
      department_group_code: '',
      document_role02_status_code: '',
      request_number: ''
    }
    this.listDocumentRole02Item = []
    this.paginateDocumentRole02Item = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateDocumentRole02Item.id = 'paginateDocumentRole02Item'
    this.perPageDocumentRole02Item = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listDocumentProcess = []

    //Master List
    this.master = {
      postRoundTypeCodeList: [],
      departmentGroupCodeList: [],
      documentRole02StatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initDocumentRole02ItemList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.postRoundTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.post_round_type_code = "ALL"
        this.master.departmentGroupCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.department_group_code = "ALL"
        this.master.documentRole02StatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.document_role02_status_code = "ALL"

      }
      if (this.editID) {
        this.DocumentProcessService.DocumentRole02ItemLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  onClickDocumentRole02ItemList(): void {
    // if(this.validateDocumentRole02ItemList()) {
    // Open loading
    // Call api
    this.callDocumentRole02ItemList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callDocumentRole02ItemList(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole02ItemList(this.help.GetFilterParams(params, this.paginateDocumentRole02Item)).subscribe((data: any) => {
      // if(isValidDocumentRole02ItemListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickDocumentRole02ItemAdd(): void {
    this.listDocumentRole02Item.push({
      index: this.listDocumentRole02Item.length + 1,
      request_number: null,
      request_date_text: null,
      request_item_sub_type_1_code_text: null,
      document_role02_date_text: null,
      department_group_name: null,
      book_by_name: null,
      document_role02_receive_date_text: null,
      document_role02_receiver_by_name: null,
      document_role02_remark: null,
      document_role02_status_name: null,

    })
    this.changePaginateTotal(this.listDocumentRole02Item.length, 'paginateDocumentRole02Item')
  }

  onClickDocumentRole02ItemEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickDocumentRole02ItemDelete(item: any): void {
    // if(this.validateDocumentRole02ItemDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listDocumentRole02Item.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listDocumentRole02Item.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listDocumentRole02Item.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listDocumentRole02Item.length; i++) {
          if (this.listDocumentRole02Item[i].is_check) {
            this.listDocumentRole02Item[i].cancel_reason = rs
            this.listDocumentRole02Item[i].status_code = "DELETE"
            this.listDocumentRole02Item[i].is_deleted = true

            if (true && this.listDocumentRole02Item[i].id) ids.push(this.listDocumentRole02Item[i].id)
            // else this.listDocumentRole02Item.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole02ItemDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listDocumentRole02Item.length; i++) {
          this.listDocumentRole02Item[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole02ItemDelete(params: any, ids: any[]): void {
    this.DocumentProcessService.DocumentRole02ItemDelete(params).subscribe((data: any) => {
      // if(isValidDocumentRole02ItemDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listDocumentRole02Item.length; i++) {
          if (this.listDocumentRole02Item[i].id == id) {
            this.listDocumentRole02Item.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listDocumentRole02Item.length; i++) {
        this.listDocumentRole02Item[i] = i + 1
      }

      this.onClickDocumentRole02ItemList()
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickDocumentRole02ItemListAutoSplit(): void {
    this.DocumentProcessService.DocumentRole02ItemListAutoSplit().subscribe((data: any) => {
      this.onClickDocumentRole02ItemList()
    })
  }

  //// Modal Location
  //autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
  //  if (object[name].length >= length) {
  //    console.log("d")
  //    this.is_autocomplete_ListModalLocation_show = true

  //    let params = {
  //      name: object[name],
  //      paging: {
  //        item_per_page: item_per_page
  //      },
  //    }

  //    this.callAutocompleteChangeListModalLocation(params)
  //  }
  //}
  //autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
  //  let pThis = this
  //  setTimeout(function () {
  //    pThis.is_autocomplete_ListModalLocation_show = false
  //  }, 200)
  //}
  //callAutocompleteChangeListModalLocation(params: any): void {
  //  if (this.input.is_autocomplete_ListModalLocation_load) return
  //  this.input.is_autocomplete_ListModalLocation_load = true
  //  let pThis = this
  //  // this.DocumentProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
  //  //   if (data) {
  //  //     if (data.length == 1) {
  //  //       setTimeout(function () {
  //  //         pThis.autocompleteChooseListModalLocation(data[0])
  //  //       }, 200)
  //  //     } else {
  //  //       pThis.autocompleteListListModalLocation = data
  //  //     }
  //  //   }
  //  // })
  //  this.input.is_autocomplete_ListModalLocation_load = false
  //}
  //autocompleteChooseListModalLocation(data: any): void {
  //  this.inputModalAddress.address_sub_district_code = data.code
  //  this.inputModalAddress.address_sub_district_name = data.name
  //  this.inputModalAddress.address_district_code = data.district_code
  //  this.inputModalAddress.address_district_name = data.district_name
  //  this.inputModalAddress.address_province_code = data.province_code
  //  this.inputModalAddress.address_province_name = data.province_name
  //  this.inputModalAddress.address_country_code = data.country_code
  //  this.inputModalAddress.address_country_name = data.country_name
  //  this.is_autocomplete_ListModalLocation_show = false
  //}
  //onClickSave04ModalAddressSave(): void {
  //  Object.keys(this.inputModalAddress).forEach((item: any) => {
  //    this.inputModalAddressEdit[item] = this.inputModalAddress[item]
  //  })
  //  this.toggleModal("isModalPeopleEditOpen")
  //}
  //// Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listDocumentRole02Item = data.documentrole02item_list || []
    this.changePaginateTotal(this.listDocumentRole02Item.length, 'paginateDocumentRole02Item')

  }

  listData(data: any): void {
    this.listDocumentRole02Item = data.list || []
    this.help.PageSet(data, this.paginateDocumentRole02Item)
    //this.changePaginateTotal(this.listDocumentRole02Item.length, 'paginateDocumentRole02Item')

  }

  saveData(): any {
    // let params = this.input
    // params.documentrole02item_list = this.listDocumentRole02Item  || []

    const params = {
      post_round_document_post_start_date: this.input.post_round_document_post_start_date,
      post_round_document_post_end_date: this.input.post_round_document_post_end_date,
      post_round_type_code: this.input.post_round_type_code,
      department_group_code: this.input.department_group_code,
      document_role02_status_code: this.input.document_role02_status_code,
      request_number: this.input.request_number,
      //  page_index: +this.paginateDocumentRole02Item.currentPage,
      //  item_per_page: +this.paginateDocumentRole02Item.itemsPerPage,
      //  order_by: 'created_date',
      //  is_order_reverse: false,
      //  search_by: [{
      //      key: 'post_round_document_post_date',
      //      value: displayDateServer(this.input.post_round_document_post_start_date),
      //      operation: 3
      //  },{
      //    key: 'post_round_document_post_date',
      //    value: displayDateServer(this.input.post_round_document_post_end_date),
      //    operation: 4
      //}, {
      //      key: 'post_round_type_code',
      //      value: this.input.post_round_type_code,
      //      operation: 0
      //  }, {
      //      key: 'department_group_code',
      //      value: this.input.department_group_code,
      //      operation: 0
      //  }, {
      //      key: 'document_role02_status_code',
      //      value: this.input.document_role02_status_code,
      //      operation: 0
      //  }, {
      //    key: 'request_number',
      //    values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //    operation: 5
      //  }]
    }

    return params
  }



  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginateDocumentRole02Item') {
      this.onClickDocumentRole02ItemList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
