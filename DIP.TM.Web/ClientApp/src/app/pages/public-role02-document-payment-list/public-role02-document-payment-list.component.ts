import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { Auth } from "../../auth";

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role02-document-payment-list",
  templateUrl: "./public-role02-document-payment-list.component.html",
  styleUrls: ["./public-role02-document-payment-list.component.scss"]
})
export class PublicRole02DocumentPaymentListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole02DocumentPayment
  public listPublicRole02DocumentPayment: any[]
  public paginatePublicRole02DocumentPayment: any
  public perPagePublicRole02DocumentPayment: number[]
  // Response PublicRole02DocumentPayment
  public responsePublicRole02DocumentPayment: any
  public listPublicRole02DocumentPaymentEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public postRoundInstructionRuleCodeList: any[]
  public publicRole02DocumentPaymentStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  public popup: any
  public send_back: any
  public url: any

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.popup = {}
    this.send_back = {}

    this.input = {
      id: null,
      //public_role05_consider_payment_start_date: getMoment(),
      //public_role05_consider_payment_end_date: getMoment(),
      public_document_payment_instruction_rule_code: '',
      public_role02_document_payment_status_code: '',
      request_number: '',
      book_start_date: getMoment(),
    }
    this.listPublicRole02DocumentPayment = []
    this.paginatePublicRole02DocumentPayment = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole02DocumentPayment.id = 'paginatePublicRole02DocumentPayment'
    this.perPagePublicRole02DocumentPayment = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      postRoundInstructionRuleCodeList: [],
      publicRole02DocumentPaymentStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole02DocumentPaymentList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.postRoundInstructionRuleCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        //this.input.public_document_payment_instruction_rule_code = "ALL"
        this.master.publicRole02DocumentPaymentStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.public_role02_document_payment_status_code = "WAIT_1"
        this.input.public_role02_document_payment_status_code = ""

        this.send_back["department_code"] = this.master.publicRole01SendBackDepartmentList[0].code
      }
      //if (this.editID) {
      //  this.PublicProcessService.PublicRole02DocumentPaymentLoad(this.editID).subscribe((data: any) => {
      //    if (data) {
      //      // Manage structure
      //      this.loadData(data)
      //    }
      //    // Close loading
      //    this.global.setLoading(false)
      //    this.automateTest.test(this)
      //  })
      //} else {
      this.global.setLoading(false)
      this.automateTest.test(this)

      //For Test
      this.onClickPublicRole02DocumentPaymentList()
      //}

    })
  }

  constructor(
    private auth: Auth,
    public sanitizer: DomSanitizer,
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }

  onClickPublicRole02DocumentPaymentList(): void {
    // if(this.validatePublicRole02DocumentPaymentList()) {
    // Open loading
    // Call api
    this.callPublicRole02DocumentPaymentList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole02DocumentPaymentList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole02DocumentPaymentList(this.help.GetFilterParams(params, this.paginatePublicRole02DocumentPayment)).subscribe((data: any) => {
      // if(isValidPublicRole02DocumentPaymentListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickPublicRole02DocumentPaymentSend(): void {
    //if(this.validatePublicRole02DocumentPaymentSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api

    this.popup.is_show = false

    this.listPublicRole02DocumentPayment.forEach((item: any) => {
      item.book_start_date = this.input.book_start_date
    })

    this.callPublicRole02DocumentPaymentSend(this.listPublicRole02DocumentPayment)
    //}
  }
  validatePublicRole02DocumentPaymentSend(): boolean {
    let result = validateService('validatePublicRole02DocumentPaymentSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole02DocumentPaymentSend(params: any): void {
    this.PublicProcessService.PublicRole02DocumentPaymentSend(params).subscribe((data: any) => {
      // if(isValidPublicRole02DocumentPaymentSendResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        if (data && data.length > 0) {
          this.automateTest.reference_number = data[0].reference_number
          console.log(data[0].reference_number)
        }
        this.onClickPublicRole02DocumentPaymentList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickPublicRole02DocumentPaymentAdd(): void {
    this.listPublicRole02DocumentPayment.push({
      index: this.listPublicRole02DocumentPayment.length + 1,
      request_number: null,
      public_role05_consider_payment_date_text: null,
      public_document_payment_instruction_rule_name: null,
      total_price: null,
      name: null,
      house_number: null,
      book_number: null,
      book_round01_start_date: null,
      book_round02_start_date: null,
      book_expired_day: null,
      book_acknowledge_date_text: null,
      book_payment_date: null,
      public_role02_document_payment_status_name: null,

    })
    this.changePaginateTotal(this.listPublicRole02DocumentPayment.length, 'paginatePublicRole02DocumentPayment')
  }

  onClickPublicRole02DocumentPaymentEdit(row_item: any): void {
    this.url = this.sanitizer.bypassSecurityTrustResourceUrl("pdf/TorKor/" + (row_item.post_round_id_2 || row_item.post_round_id_1));
    console.log(this.url)

    this.listPublicRole02DocumentPayment.forEach((item: any) => {
      item.is_check = item == row_item
    })

    this.popup.is_show = true
    this.popup.row_item = row_item
    //var win = window.open("/public-role02-document-payment/edit/" + item.save_id)
  }

  onClickPublicRole02DocumentPaymentView(item: any): void {
    var win = window.open("/pdf/TorKor/" + (item.post_round_id_2 || item.post_round_id_1))
  }

  onClickPublicRole02DocumentPaymentDelete(item: any): void {
    // if(this.validatePublicRole02DocumentPaymentDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listPublicRole02DocumentPayment.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listPublicRole02DocumentPayment.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listPublicRole02DocumentPayment.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listPublicRole02DocumentPayment.length; i++) {
          if (this.listPublicRole02DocumentPayment[i].is_check) {
            this.listPublicRole02DocumentPayment[i].cancel_reason = rs
            this.listPublicRole02DocumentPayment[i].status_code = "DELETE"
            this.listPublicRole02DocumentPayment[i].is_deleted = true

            if (true && this.listPublicRole02DocumentPayment[i].id) ids.push(this.listPublicRole02DocumentPayment[i].id)
            // else this.listPublicRole02DocumentPayment.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callPublicRole02DocumentPaymentDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listPublicRole02DocumentPayment.length; i++) {
          this.listPublicRole02DocumentPayment[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callPublicRole02DocumentPaymentDelete(params: any, ids: any[]): void {
    this.PublicProcessService.PublicRole02DocumentPaymentDelete(params).subscribe((data: any) => {
      // if(isValidPublicRole02DocumentPaymentDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listPublicRole02DocumentPayment.length; i++) {
          if (this.listPublicRole02DocumentPayment[i].id == id) {
            this.listPublicRole02DocumentPayment.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listPublicRole02DocumentPayment.length; i++) {
        this.listPublicRole02DocumentPayment[i] = i + 1
      }

      this.onClickPublicRole02DocumentPaymentList()
      // Close loading
      this.global.setLoading(false)
    })
  }



  onClickPublicRole02DocumentPaymentSendChange(): void {
    //if(this.validatePublicRole05ConsiderPaymentSendChange()) {
    // Open loading
    this.global.setLoading(true)
    // Call api

    this.popup.is_send_back_show = false

    this.listPublicRole02DocumentPayment.forEach((item: any) => {
      item.book_start_date = this.input.book_start_date
    })

    this.callPublicRole02DocumentPaymentSendChange(this.listPublicRole02DocumentPayment)
    //}
  }
  //validatePublicRole05ConsiderPaymentSendChange(): boolean {
  //  let result = validateService('validatePublicRole05ConsiderPaymentSendChange', this.input)
  //  this.validate = result.validate
  //  return result.isValid
  //}
  //! <<< Call API >>>
  callPublicRole02DocumentPaymentSendChange(params: any): void {
    this.PublicProcessService.PublicRole02DocumentPaymentSendChange(params).subscribe((data: any) => {
      // if(isValidPublicRole05ConsiderPaymentSendChangeResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        //if (data && data.length > 0) {
        //  this.automateTest.reference_number = data[0].reference_number
        //  console.log(data[0].reference_number)
        //}
        this.onClickPublicRole02DocumentPaymentList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }


  


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicRole02DocumentPayment = data.publicrole02documentpayment_list || []
    this.changePaginateTotal(this.listPublicRole02DocumentPayment.length, 'paginatePublicRole02DocumentPayment')

  }

  listData(data: any): void {
    this.listPublicRole02DocumentPayment = data.list || []
    this.help.PageSet(data, this.paginatePublicRole02DocumentPayment)
    //this.changePaginateTotal(this.listPublicRole02DocumentPayment.length, 'paginatePublicRole02DocumentPayment')

  }

  saveData(): any {
    // let params = this.input
    // params.publicrole02documentpayment_list = this.listPublicRole02DocumentPayment || []

    const params = {
      public_role05_consider_payment_start_date: this.input.public_role05_consider_payment_start_date,
      public_role05_consider_payment_end_date: this.input.public_role05_consider_payment_end_date,
      public_document_payment_instruction_rule_code: this.input.public_document_payment_instruction_rule_code,
      public_role02_document_payment_status_code: this.input.public_role02_document_payment_status_code,
      request_number: this.input.request_number,
      //page_index: +this.paginatePublicRole02DocumentPayment.currentPage,
      //item_per_page: +this.paginatePublicRole02DocumentPayment.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'public_role05_consider_payment_date',
      //  value: displayDateServer(this.input.public_role05_consider_payment_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_role05_consider_payment_date',
      //  value: displayDateServer(this.input.public_role05_consider_payment_end_date),
      //  operation: 4
      //}, {
      //  key: 'public_document_payment_instruction_rule_code',
      //  value: this.input.public_document_payment_instruction_rule_code,
      //  operation: 0
      //}, {
      //  key: 'public_role02_document_payment_status_code',
      //  value: this.input.public_role02_document_payment_status_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}]
    }

    return params
  }



  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicRole02DocumentPayment') {
      this.onClickPublicRole02DocumentPaymentList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
