import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MadridRole04MailComponent } from '../madrid-role04-mail/madrid-role04-mail.component';
import { MadridRole04PresentComponent } from '../madrid-role04-present/madrid-role04-present.component';
import { MadridRole04PaymentComponent } from '../madrid-role04-payment/madrid-role04-payment.component';

@Component({
  selector: 'app-madrid-role04-check',
  templateUrl: './madrid-role04-check.component.html',
  styleUrls: ['./madrid-role04-check.component.scss']
})
export class MadridRole04CheckComponent implements OnInit {

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() {
  }

  ModalMail() {
    const dialogRef = this.dialog.open(MadridRole04MailComponent, {
      width: "calc(100%)",
      height: "calc(100% - 80px)",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

  ModalPresent() {
    const dialogRef = this.dialog.open(MadridRole04PresentComponent, {
      width: "calc(85% - 80px)",
      height: "calc(100% - 80px)",
      maxWidth: "1000px",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

  modalPayment() {
    const dialogRef = this.dialog.open(MadridRole04PaymentComponent, {
      width: "calc(85% - 80px)",
      maxWidth: "1000px",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

}
