import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone,  displayDateServer,
} from '../../helpers'

@Component({
  selector: "app-considering-similar",
  templateUrl: "./considering-similar.component.html",
  styleUrls: ["./considering-similar.component.scss"]
})
export class ConsideringSimilarComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List WordSoundTranslate
  public listWordSoundTranslate: any[]
  public paginateWordSoundTranslate: any
  public perPageWordSoundTranslate: number[]

  // Response WordSoundTranslate
  public responseWordSoundTranslate: any
  public listWordSoundTranslateEdit: any

  // List Trademark
  public listTrademark: any[]
  public paginateTrademark: any
  public perPageTrademark: number[]
  // Response Trademark
  public responseTrademark: any
  public listTrademarkEdit: any

  public checkingTagSimilarMethod: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial
  public popup: any
  public send_back: any

  public case28_list: any[]
  public case28_list_show: any[]
  public is_updated_case28_list: any


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      name: '',
      request_item_sub_type_1_code: '',
      request_item_sub_type_description: '',
      sound_description: '',
      full_view: {
        case28_list: []
      },

      checking_save_document_count: 0,
    }
    this.listWordSoundTranslate = []
    this.paginateWordSoundTranslate = CONSTANTS.PAGINATION.INIT
    this.perPageWordSoundTranslate = CONSTANTS.PAGINATION.PER_PAGE

    this.listTrademark = []
    this.paginateTrademark = CONSTANTS.PAGINATION.INIT
    this.perPageTrademark = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //
    this.popup = {}
    this.send_back = {}

    this.checkingTagSimilarMethod = {}

    this.case28_list = []
    this.case28_list_show = []
    this.is_updated_case28_list = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initConsideringSimilar().subscribe((data: any) => {
      if (data) {
        this.master = data

      }
      if (this.editID) {
        let pThis = this
        this.CheckingProcessService.ConsideringSimilarLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)


            var params = {
              request_number: this.input.request_number,
              consider_similar_document_status_code: "WAIT_CONSIDER",
            }
            this.CheckingProcessService.List(this.help.GetFilterParams(params), "vSave010CheckingSaveDocument").subscribe((data: any) => {
              this.input.checking_save_document_count = (data && data.list ? data.list.length : 0)
            })


            this.CheckingProcessService.CheckingSimilarTagList(this.editID).subscribe((data: any) => {
              if (data) {
                console.log(data)
                this.listTrademark = data


                ///////////

                this.listTrademark.forEach((item: any) => {
                  if (item.trademark_status_code == "P") {
                    item.consideringSimilarInstructionRuleCodeList = []
                    item.consideringSimilarInstructionRuleCodeList.push(
                      this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == "ROLE_29")[0]
                    )

                    item.instruction_rule_code = item.instruction_rule_code || item.consideringSimilarInstructionRuleCodeList[0].code
                  } else if (item.trademark_status_code == "R") {
                    item.consideringSimilarInstructionRuleCodeList = []
                    item.consideringSimilarInstructionRuleCodeList.push(
                      this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == "ROLE_13_1")[0]
                    )
                    item.consideringSimilarInstructionRuleCodeList.push(
                      this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == "ROLE_13_2")[0]
                    )

                    item.instruction_rule_code = item.instruction_rule_code || item.consideringSimilarInstructionRuleCodeList[0].code
                  }
                })

                ///////////

                var pThis = this
                this.listTrademark.forEach(function (item: any, index) {
                  item.word_mark = item.full_view.document_classification_word_list.map(function (item) { return item.word_mark }).join(", ")

                  item.full_view.people_list.forEach((people: any) => {
                    if (people.name == pThis.input.name) {
                      item.is_owner_same_auto = true
                      return false
                    }
                  })
                })

                var _index = 0
                this.listTrademark.forEach(function (item: any, index) {
                  if (item.is_owner_same_auto) {
                    var trademark = pThis.listTrademark.splice(index, 1)[0]
                    pThis.listTrademark.splice(_index++, 0, trademark)
                  }
                })

                _index = 0
                this.listTrademark.forEach(function (item: any, index) {
                  if (item.is_owner_same) {
                    var trademark = pThis.listTrademark.splice(index, 1)[0]

                    trademark.considering_remark = "เจ้าของเดียวกัน"

                    pThis.listTrademark.splice(_index++, 0, trademark)
                  }
                })

              }

              let param = {}
              param["request01_item_id"] = parseInt(this.editID)
              this.CheckingProcessService.CheckingTagSimilarMethod(param).subscribe((data: any) => {
                if (data) {
                  //console.log(data)
                  // Manage structure
                  this.checkingTagSimilarMethod = data

                  //this.listTrademark.forEach((item: any) => {
                  //  item.word_mark = item.full_view.document_classification_word_list.map(function (item) { return item.word_mark }).join(", ")
                  //})
                }
                // Close loading
                pThis.global.setLoading(false)
                this.automateTest.test(this)
              })

              //// Close loading
              //pThis.global.setLoading(false)
              //this.automateTest.test(this)



            })
          } else {
            // Close loading
            pThis.global.setLoading(false)
            this.automateTest.test(this)
          }
        })
      }

      //this.global.setLoading(false)
    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }


  onClickConsideringSimilarCheckingSimilar(): void {
    var win = window.open("./checking-similar/edit/" + this.editID, "")
  }
  validateConsideringSimilarCheckingSimilar(): boolean {
    let result = validateService('validateConsideringSimilarCheckingSimilar', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarCheckingSimilar(params: any): void {
    //let pThis = this
    //this.CheckingProcessService.ConsideringSimilarCheckingSimilar(params).subscribe((data: any) => {
    //    // if(isValidConsideringSimilarCheckingSimilarResponse(res)) {
    //    if (data) {
    //        // Set value
    //        pThis.loadData(data)
    //    }
    //    // }
    //    // Close loading
    //    this.global.setLoading(false)
    //})
  }

  onClickConsideringSimilarConsideringDocument(): void {
    ////if(this.validateConsideringSimilarConsideringDocument()) {
    //// Open loading
    //this.global.setLoading(true)
    //// Call api
    //this.callConsideringSimilarConsideringDocument(this.saveData())
    var win = window.open("./considering-similar-document/edit/" + this.editID, "")
    ////}
  }
  validateConsideringSimilarConsideringDocument(): boolean {
    let result = validateService('validateConsideringSimilarConsideringDocument', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarConsideringDocument(params: any): void {
    //let pThis = this
    //this.CheckingProcessService.ConsideringSimilarConsideringDocument(params).subscribe((data: any) => {
    //    // if(isValidConsideringSimilarConsideringDocumentResponse(res)) {
    //    if (data) {
    //        // Set value
    //        pThis.loadData(data)
    //    }
    //    // }
    //    // Close loading
    //    this.global.setLoading(false)
    //})
  }

  onClickConsideringSimilarInstruction(): void {
    //if(this.validateConsideringSimilarInstruction()) {
    // Open loading
    //this.global.setLoading(true)
    // Call api
    //this.callConsideringSimilarInstruction(this.saveData())
    var win = window.open("./considering-similar-instruction/edit/" + this.editID, "")
    //}
  }
  validateConsideringSimilarInstruction(): boolean {
    let result = validateService('validateConsideringSimilarInstruction', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarInstruction(params: any): void {
    //let pThis = this
    //this.CheckingProcessService.ConsideringSimilarInstruction(params).subscribe((data: any) => {
    //    // if(isValidConsideringSimilarInstructionResponse(res)) {
    //    if (data) {
    //        // Set value
    //        pThis.loadData(data)
    //    }
    //    // }
    //    // Close loading
    //    this.global.setLoading(false)
    //})
  }

  onClickConsideringSimilarSearchAsk(): void {
    var win = window.open(
      "/request-search/edit/" + this.editID
    );
  }

  onClickConsideringSimilarSave010PDFView(): void {
    window.open('/File/RequestDocumentCollect/010/' + this.editID)
  }

  onClickConsideringSimilarResultLink(): void {
    var win = window.open(
      "./checking-similar-result/edit/" + this.editID
    );
  }

  onClickConsideringSimilarSave(): void {
    // Call api
    this.callConsideringSimilarSave(this.listTrademark)
    //}
  }
  validateConsideringSimilarSave(): boolean {
    let result = validateService('validateConsideringSimilarSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarSave(params: any): void {
    this.global.setLoading(true)
    this.CheckingProcessService.ConsideringSimilarSave(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarSaveResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        //this.listTrademark = data
      }
      this.automateTest.test(this, { Saved: 1 })
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickConsideringSimilarWordSoundTranslateAdd(): void {
    this.listWordSoundTranslate.push({
      word_translate_search: null,
      word_translate_dictionary: null,
      word_translate_sound: null,
      word_translate_translate: null,
      checking_word_translate_status_name: null,

    })
    this.changePaginateTotal(this.listWordSoundTranslate.length, 'paginateWordSoundTranslate')
  }

  onClickConsideringSimilarTrademarkAdd(): void {
    this.listTrademark.push({
      index: this.listTrademark.length + 1,
      word_mark: null,
      request_item_sub_type_description: null,
      request_number: null,
      name: null,
      trademark_status_code: null,
      image_link: null,
      sound_link: null,
      considering_remark: null,
      is_similar: false,

    })
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  onClickCheckingSimilarLikeSame(row_item: any, name: string, value: boolean) {
    //console.log(value)
    row_item.is_like_approve = row_item.is_same_approve = false
    row_item[name] = value
  }


  onClickCheckingSimilarResultOwnerSame(): void {
    //if(this.validateCheckingSimilarResultOwnerSame()) {
    // Open loading
    // Call api
    this.callCheckingSimilarResultOwnerSame(this.listTrademark)
    //}
  }
  validateCheckingSimilarResultOwnerSame(): boolean {
    let result = validateService('validateCheckingSimilarResultOwnerSame', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarResultOwnerSame(params: any): void {
    this.global.setLoading(true)
    this.CheckingProcessService.CheckingSimilarResultOwnerSame(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarResultOwnerSameResponse(res)) {
      if (data) {
        // Set value
        //pThis.loadData(data)
        this.callInit()
      } else {
        this.global.setLoading(false)
      }
      // }
      // Close loading
    })
  }

  //Start อ่านแปล from checking-similar/edit/167
  onClickCheckingSimilarWordSoundTranslateSave(): void {
    //if(this.validateCheckingSimilarSave010PDFView()) {
    // Open loading
    this.global.setLoading(true);
    // Call api
    this.callCheckingSimilarWordSoundTranslateSave(this.saveData());
    //}
  }
  callCheckingSimilarWordSoundTranslateSave(params: any): void {
    let pThis = this;
    this.CheckingProcessService.CheckingSimilarWordSoundTranslateSave(
      params
    ).subscribe((data: any) => {
      // if(isValidCheckingSimilarSave010PDFViewResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data);
      }
      // }
      // Close loading
      this.global.setLoading(false);
      pThis.automateTest.test(this, { WordSoundTranslateSave: 1 });
    });
  }
  onClickCheckingSimilarWordSoundTranslateAdd(): void {
    this.listWordSoundTranslate.push({
      word_translate_search: this.input.full_view.document_classification_word_list[0].word_mark,
      word_translate_dictionary_code: "OTHERS",
      word_translate_dictionary_other: null,
      word_translate_sound: null,
      word_translate_translate: null,
      checking_word_translate_status_code: this.master
        .checkingWordTranslateStatusCodeList[0].code
    });
    this.changePaginateTotal(
      this.listWordSoundTranslate.length,
      "paginateWordSoundTranslate"
    );
  }
  onClickCheckingSimilarWordSoundTranslateDelete(item: any): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    if (
      this.listWordSoundTranslate.filter(r => r.is_check).length > 0 ||
      item
    ) {
      if (item) {
        this.listWordSoundTranslate
          .filter(r => r.is_check)
          .forEach((item: any) => {
            item.is_check = false;
          });
        item.is_check = true;
      }

      let delete_save_item_count = 0;
      if (true) {
        delete_save_item_count = this.listWordSoundTranslate.filter(
          r => r.is_check && r.id
        ).length;
      }

      var rs =
        confirm("คุณต้องการลบรายการ?");
      if (rs) {
        let ids = [];

        for (let i = 0; i < this.listWordSoundTranslate.length; i++) {
          if (this.listWordSoundTranslate[i].is_check) {
            this.listWordSoundTranslate[i].is_deleted = true;

            if (!this.listWordSoundTranslate[i].id)
              this.listWordSoundTranslate.splice(i--, 1);
          }
        }

        var i = 1
        this.listWordSoundTranslate.filter(r => !r.is_deleted).forEach((item: any) => {
          item.index = i++
        })
      }
    }
    this.global.setLoading(false);
  }
  onClickCheckingSimilarWordTranslateCapture(row_item: any): void {
    var item = this.master.checkingWordTranslateDictionaryCodeList.filter(r => r.code == row_item.word_translate_dictionary_code)[0]
    console.log(item)
    console.log(row_item)
    window.open(item.value_1.replace('[WORD]', row_item.word_translate_search))
  }
  //End อ่านแปล from checking-similar/edit/167

  onClickCheckingSimilarWordSoundTranslateBack() {
    this.popup.is_send_back_show = true
  }
  onClickCheckingSimilarWordSoundTranslateBackSend() {
    var params = {
      id: +this.editID,
      checking_remark: this.input.checking_remark,
    }

    this.callCheckingSimilarWordSoundTranslateBackSend(params)
  }
  callCheckingSimilarWordSoundTranslateBackSend(params: any): void {
    let pThis = this;
    this.global.setLoading(true);
    this.CheckingProcessService.CheckingSimilarWordSoundTranslateBackSend(
      params
    ).subscribe((data: any) => {
      // if(isValidCheckingSimilarSave010PDFViewResponse(res)) {
      if (data) {
        this.popup.is_send_back_show = false
        this.popup.isPopupSendOpen = true
        // Set value
        //pThis.loadData(data);
      }
      // }
      // Close loading
      this.global.setLoading(false);
    });
  }

  onClickCheckingSimilarCase28(row_item: any): void {
    this.popup.product_show = row_item
    //console.log(row_item)

    this.case28_list.length = 0
    //console.log(row_item.full_view.case28_list.map(r => { return r.case_28_date_text + " " + r.address_country_code + " " + r.request_item_sub_type_1_code }))
    row_item.full_view.case28_list.sort((a, b) =>
      a.request_item_sub_type_1_code > b.request_item_sub_type_1_code ? 1 : -1
    )

    //console.log(row_item.full_view.case28_list.map(r => { return r.case_28_date_text + " " + r.address_country_code + " " + r.request_item_sub_type_1_code }))
    row_item.full_view.case28_list.sort((a, b) =>
      displayDateServer(a.case_28_date) > displayDateServer(b.case_28_date) ? 1 :
        (displayDateServer(a.case_28_date) == displayDateServer(b.case_28_date) ? 0 : -1)
    )

    //console.log(row_item.full_view.case28_list.map(r => { return r.case_28_date_text + " " + r.address_country_code + " " + r.request_item_sub_type_1_code }))
    row_item.full_view.case28_list.sort((a, b) =>
      a.address_country_code > b.address_country_code ? 1 :
        (a.address_country_code == b.address_country_code ? 0 : -1)
    )
    //console.log(row_item.full_view.case28_list.map(r => { return r.case_28_date_text + " " + r.address_country_code }))
    //row_item.full_view.case28_list.sort((a, b) =>
    //  a.address_country_code > b.address_country_code ? 1 : -1
    //)
    //console.log(row_item.full_view.case28_list.map(r => { return r.case_28_date_text + " " + r.address_country_code }))
    //row_item.full_view.case28_list.sort((a, b) =>
    //  a.address_country_code > b.address_country_code ? 1 :
    //    (a.case_28_date > b.case_28_date ? 1 : -1)
    //)
    //console.log(row_item.full_view.case28_list.map(r => { return r.case_28_date_text + " " + r.address_country_code }))
    //row_item.full_view.case28_list.sort((a, b) =>
    //  a.address_country_code > b.address_country_code ? 1 :
    //    (a.case_28_date > b.case_28_date ? 1 :
    //      (a.request_item_sub_type_1_code > b.request_item_sub_type_1_code ? 1 : -1)
    //    )
    //)
    //console.log(row_item.full_view.case28_list.map(r => { return r.case_28_date_text + " " + r.address_country_code + " " + r.request_item_sub_type_1_code }))

    row_item.full_view.case28_list.forEach((item: any) => {
      this.case28_list.push(item)
    })
    this.is_updated_case28_list.updated = true
  }

  onClickConsideringSimilarCase28(): void {
    window.open("considering-similar-kor10/edit/" + this.editID)
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listWordSoundTranslate =
      data.full_view.checking_similar_wordsoundtranslate_list || [];
    //this.listWordSoundTranslate = data.checking_similar_wordsoundtranslate_list || []
    this.listTrademark = data.trademark_list || []
    this.changePaginateTotal(this.listWordSoundTranslate.length, 'paginateWordSoundTranslate')
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')

  }

  listData(data: any): void {
    this.listWordSoundTranslate = data || []
    this.listTrademark = data || []
    this.changePaginateTotal(this.listWordSoundTranslate.length, 'paginateWordSoundTranslate')
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')

  }

  saveData(): any {
    let params = this.input

    params.checking_similar_wordsoundtranslate_list = this.listWordSoundTranslate || []
    params.trademark_list = this.listTrademark || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

  //! <<< Table >>>
  onSelectColumnInTable(object: any, value: boolean): void {
    console.log(object)
    object.forEach((item: any) => { item.is_check = value })
    console.log(value)
    //if (!item) {
    //  this.input.is_check_all = !this.input.is_check_all;
    //  this[name].forEach((item: any) => {
    //    item.is_check = this.input.is_check_all;
    //  });
    //} else {
    //  item.is_check = !item.is_check;
    //}
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
}
