import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { Router } from "@angular/router";

import {
  ROUTE_PATH,
  CONSTANTS,
  validateService,
  clone,
  getItemCalculator,
  displayMoney,
  displayFormatBytes,
  PapaParseCsvToJson,
  loopDisplayDateServer,
  displayAddress,
  viewPDF,
} from "../../helpers";

@Component({
  selector: "app-madrid-role02-edit-inter-modal",
  templateUrl: "./madrid-role02-edit-inter-modal.component.html",
  styleUrls: ["./madrid-role02-edit-inter-modal.component.scss", './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole02EditInterModalComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    
    public modal: any
    listPublicItem=[{index:1},{index:2},{index:3},{index:4},{index:5},{index:6},{index:7},]
    listPublicItem2=[{index:1},{index:2},{index:3},{index:4},{index:5},{index:6},{index:7},]
    currentStep = 1;
    approval = 1;
    menuList = [
        {
            number:1,
            isShow: true,
            title:"ข้อมูลทั่วไป"
        },
        {
            number:2,
            isShow: true,
            title:"รายการสินค้า"
        },
        {
            number:3,
            isShow: true,
            title:"พิจารณาเอกสาร"
        }
    ]
    approvalList = [{id:1,name:"อนุมัติ"},{id:2,name:"ไม่อนุมัติ"},{id:3,name:"อนุญาต"}]
    //MR02_03
    ngOnInit() {
        this.validate = {}
        this.input = {
            id: null,
            //instruction_send_start_date: getMoment(),
            //instruction_send_end_date: getMoment(),
            public_type_code: '',
            public_source_code: '',
            public_receiver_by: '',
            public_status_code: '',
            request_number: '',
          }
          this.master={
            request_type:[{code:1,name:'1'}],
            job_reciver:[{id:1,name:"Sam"},{id:2,name:"Tom"}]
        }
        this.currentStep = 3;
        
    }

    

    constructor(
        private help: Help,
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService,
        
    ) { }

    onNextStep(){
        if (this.currentStep==3) return
        this.currentStep+=1;
       
    }
    onBackStep(){
        if (this.currentStep==1)return
        this.currentStep-=1;
    }


    //! <<< ----------------- >>>
    //! <<< --- Copy Zone --- >>>
    //! <<< ----------------- >>>

    //! <<< Binding (one way) >>>
    binding(obj: any, name: any, value: any): void {
        this[obj][name] = value
    }

    //! <<< Table >>>
    onSelectColumnInTable(name: string, item: any): void {
        if (!item) {
            this.input.is_check_all = !this.input.is_check_all
            this[name].forEach((item: any) => {
                item.is_check = this.input.is_check_all
            })
        } else {
            item.is_check = !item.is_check
        }
    }
    onChangeInputInTable(item: any, name: any, value: any): void {
        item[name] = value
    }
    toggleInputInTable(item: any, name: any): void {
        item[name] = !item[name]
    }

    //! <<<< Pagination >>>
    managePaginateCallApi(name: string): void {
        if (name === 'paginateDocumentRole02Check') {
            //this.onClickDocumentRole02CheckList()
            this.input.is_check_all = false
        }
    }
    onChangePage(page: any, name: string): void {
        this[name].currentPage = page === '' ? 1 : page
        if ((+page || page === '') && page <= this.getMaxPage(name)) {
            this.managePaginateCallApi(name)
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
        this.managePaginateCallApi(name)
    }
    changePaginateTotal(total: any, name: string): void {
        this[name].totalItems = total
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
