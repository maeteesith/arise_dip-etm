import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consider-license-request',
  templateUrl: './consider-license-request.component.html',
  styleUrls: ['./consider-license-request.component.scss']
})
export class ConsiderLicenseRequestComponent implements OnInit {

  constructor() { }
  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public master: any;

  public tab_menu_show_index: any
  public tab_menu_show_index2: any
  public tab_menu_show_index3: any

  public tableRequestDetail: any;
  public tableRequestDetail2: any;
  public tableRequestDetail3: any;
  public tableViewDocument: any;
  public tableInfo: any;
  public tableIssueBook: any;
  public tableAgent: any;
  public tableProduct: any;
  public tableAllow: any;

  public modal: any;
  public tableInstructionRule: any
  public tableSaveDocument: any
  public tablePeople: any
  public tableRepresentative: any
  public contactAddress: any
  public tableProduct2: any
  public tableReceiptItem: any
  public tableHistory: any
  public tableDocumentScan: any

  ngOnInit() {
    this.input = {};
    this.validate = {};
    this.master = {
      checkList: [
        { code: "1", name: "หนังสือแจ้งคำสั่ง" },
        { code: "2", name: "การปฏิบัติตามคำสั่ง" },
        { code: "3", name: "การชำระค่าธรรมเนียม" },
        { code: "4", name: "การแจ้งฟ้องคดี / คำพิพากษา" },
        { code: "5", name: "สถานที่ติดต่อ" },
        { code: "6", name: "ชื่อคู่กรณี (ถ้ามี)" },
        { code: "7", name: "การขอถอน" },
        { code: "8", name: "ใบตอบรับ / ครบกำหนด" },
        { code: "9", name: "มีอุทรณ์ / คำวินิจฉัยฯ" },
      ],
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList2: [
        { code: "1", name: "รอบที่ 1" },
        { code: "2", name: "รอบที่ 2" },
      ],
      typeCodeList3: [
        { code: "1", name: "อนุญาต" },
        { code: "2", name: "รวมเรื่อง" },
        { code: "3", name: "ไม่อนุญาต" },
      ],
      typeCodeList4: [
        { code: "1", name: "ยังไม่หลุด" },
        { code: "2", name: "ยังไม่หลุด" },
        { code: "3", name: "ยังไม่หลุด" },
      ],
      typeCodeList5: [
        { code: "1", name: "เจ้าของ" },
        { code: "2", name: "ตัวแทน" },
        { code: "3", name: "ตัวแทนช่วง" },
        { code: "4", name: "อื่นๆ" },
        { code: "5", name: "ตามก.ที่ยื่น" },
      ],
      typeCodeList6: [
        { code: "1", name: "ต.ค.5 ข้อ 8" },
        { code: "2", name: "ต.ค.9 ข้อ 10" },
        { code: "3", name: "ต.ค.9 ข้อ 2" },
        { code: "4", name: "ต.ค.9 ข้อ 3" },
      ],
      typeCodeList7: [
        { code: "1", name: "คำขอจดทะเบียนสัญญาอนุญาตให้ใช้" },
        { code: "2", name: "คำขอจดทะเบียนต่อสัญญาอนุญาตให้ใช้" },
      ],
      typeCodeList8: [
        { code: "1", name: "มีสิทธิ" },
        { code: "2", name: "ไม่มีสิทธิ" },
      ],
      typeCodeList9: [
        { code: "1", name: "ได้" },
        { code: "2", name: "ไม่ได้" },
      ],
    };

    this.modal = {
    }

    this.tab_menu_show_index = 11
    this.tab_menu_show_index2 = 1
    this.tab_menu_show_index3 = 1

    this.tableRequestDetail = {
      head_column_list: [
        {
          order: "#",
          command: "คำสั่ง",
          date: "วันที่สั่ง",
          registrar: "นายทะเบียน",
          book_status: "สถานะหนังสือ",
          date_order: "วันที่ส่งคำสั่ง",
          no: "เลข พณ.",
          worker: "ผู้รับงาน",
          note: "หมายเหตุ",
          status: "สถานะ"
        },
      ],

      column_list: [
        {
          command: "ตค. 4 (ม.20)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
        {
          command: "ตค. 2 (ม.13)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
        {
          command: "ตค. 1 (ม.11)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
      ]
    }

    this.tableRequestDetail2 = {
      head_column_list: [
        {
          order: "#",
          name: "ชื่อผู้รับโอน",
          address: "ที่อยู่",
          tel: "โทรศัพท์",
          fax: "โทรสาร",
          operation: "คำสั่ง",
        },
      ],

      column_list: [
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "111 แขวงศาลเจ้าพ่อเสือ เขตพระนคร กทม. 10220",
          tel: "0987654321",
          fax: "-",
        },
      ]
    }

    this.tableRequestDetail3 = {
      head_column_list: [
        {
          order: "#",
          name: "ชื่อผู้ได้รับอนุญาต",
          address: "ที่อยู่",
          sublicense: "สิทธิ์อนุญาตช่วง",
          by: "ได้รับอนุญาตจาก",
          status: "สถานะ"
        },
      ],

      column_list: [
        {
          name: "บริษัท ที.ซี.เอ็ม อินดัสทรี จำกัด",
          address: "252 ถ.รัชดาภิเษก แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพฯ 10310",
          sublicense: "มี",
          by: "บริษัท จุมพลฟู๊ด จำกัด",
          status: "ผู้ได้รับอนุญาต"
        },
        {
          name: "บริษัท สี่มุม พาวเวอร์ จำกัด",
          address: "21 อาคารทีเอสทีทาวเวอร์ ชั้น 20 ถนนวิภาวดี-รังสิต แขวงจอมพล เขตจตุจักร กรุงเทพมหานคร 10900",
          sublicense: "ไม่มี",
          by: "บริษัท ที.ซี.เอ็ม อินดัสทรี จำกัด",
          status: "ผู้ได้รับอนุญาตช่วง"
        },
        {
          name: "บริษัท จงอี จำกัด",
          address: "315 อาคารอาคเนย์ประกันภัย ชั้น G-7 ถนนสีลม แขวงสีลม เขตบางรัก กรุงเทพฯ 10500",
          sublicense: "มี",
          by: "บริษัท จุมพลฟู๊ด จำกัด",
          status: "ผู้ได้รับอนุญาต"
        },
      ]
    }

    this.tableViewDocument = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }

    this.tableInfo = {
      column_list: [
        {
          detail: "ม.7"
        },
        {
          detail: "ม.9"
        },
        {
          detail: "ม.7"
        },
      ]
    }

    this.tableIssueBook = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }

    this.tableAgent = {
      column_list: [
        {
          name: "บริษัท ติลลิกีแอนด์กิบบินส์ อินเตอร์เนชั่นแนล จำกัด",
          address: "เลขที่ 1011 อาคารศุภาลัย แกรนด์ ทาวเวอร์ ชั้นที่ 20-26 ถนนพระราม 3 แขวงช่องนนทรี เขตยานนาวา กรุงเทพมหานคร 10120",
          tel: "026535555",
        },
        {
          name: "ฮัลล์ โกรเฮอ เอยี",
          address: "เลขที่ 1011 อาคารศุภาลัย แกรนด์ ทาวเวอร์ ชั้นที่ 20-26 ถนนพระราม 3 แขวงช่องนนทรี เขตยานนาวา กรุงเทพมหานคร 10120",
          tel: "-",
        },
      ]
    }

    this.tableProduct = {
      head_column_list: [
        {
          order: "#",
          type: "จำพวกสินค้า",
          description: "รายการสินค้า/บริการ"
        },
      ],

      column_list: [
        {
          type: "21"
        },
        {
          type: "3"
        },
      ]
    }

    this.tableAllow = {
      column_list: [
        {
          no: "171100393",
          request_date: "12/12/2562",
          owner: "เฮงเคล คอร์ปอเรชั่น",
          person: "บริษัท สตาร์ฟอร์ด จำกัด",
        },
        {
          no: "712224642",
          request_date: "12/12/2562",
          owner: "เฮงเคล คอร์ปอเรชั่น",
          person: "บริษัท สตาร์ฟอร์ด จำกัด",
        },
        {
          no: "790849099",
          request_date: "12/12/2562",
          owner: "เฮงเคล คอร์ปอเรชั่น",
          person: "บริษัท สตาร์ฟอร์ด จำกัด",
        },
        {
          no: "851019577",
          request_date: "12/12/2562",
          owner: "เฮงเคล คอร์ปอเรชั่น",
          person: "บริษัท สตาร์ฟอร์ด จำกัด",
        },
      ]
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>
  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
