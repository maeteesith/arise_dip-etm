import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GlobalService } from "./../../global.service";
import { ForkJoinService } from "../../services/fork-join.service";
import {
  ROUTE_PATH,
  CONSTANTS,
  clone,
  displayLabelReferenceMaster,
} from "../../helpers";

@Component({
  selector: "app-include-role01-change-list",
  templateUrl: "./include-role01-change-list.component.html",
  styleUrls: ["./include-role01-change-list.component.scss"],
})
export class IncludeRole01ChangeListComponent implements OnInit {
  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public list: any;
  public master: any;
  public validate: any;
  // Paginate
  public paginateIncludeRole: any;

  constructor(
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService
  ) {}

  ngOnInit() {
    // Init
    this.input = {};
    this.list = {};
    this.master = {};
    this.validate = {};
    // Paginate
    this.paginateIncludeRole = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateIncludeRole.id = "paginateIncludeRole";

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService
      .initIncludeRole01ChangeListPage()
      .subscribe((data: any) => {
        if (data) {
          this.master = data;
          

          //! Mock master data
          this.master = {
            typeCodeList: [
              { code: "type_1", name: "ประเภท 1" },
              { code: "type_2", name: "ประเภท 2" },
            ],
            statusCodeList: [
              { code: "waiting", name: "การดำเนินการ" },
              { code: "done", name: "เสร็จสิ้น" },
            ],
            requestCodeList: [
              { code: "request_1", name: "คำร้อง 1" },
              { code: "request_2", name: "คำร้อง 2" },
            ],
          };
          console.log("this.master", this.master);
        }

        // Call api list
        this.onSearch();
      });
  }

  //! <<< Call API >>>
  callIncludeRole01ChangeList(params: any): void {
    //! Mock data list
    this.list.includeRole = [
      {
        request_id: "741150971",
        submission_date: "10/09/2562",
        proposed_date: "10/09/2562",
        officer: "นางนิจรินทร์ กุหลาบป่า",
        consider_date: "",
        job_type: "เปลี่ยนแปลง",
        petition: "ก.06",
        detail: "",
        status: "waiting",
      },
      {
        request_id: "712224642",
        submission_date: "10/09/2562",
        proposed_date: "10/09/2562",
        officer: "นางนิจรินทร์ กุหลาบป่า",
        consider_date: "",
        job_type: "ต่ออายุ",
        petition: "ก.04 , ก.06 , ก.20",
        detail: "",
        status: "done",
      },
      {
        request_id: "790849099",
        submission_date: "10/09/2562",
        proposed_date: "10/09/2562",
        officer: "นางนิจรินทร์ กุหลาบป่า",
        consider_date: "10/09/2562",
        job_type: "เปลี่ยนแปลง",
        petition: "ก.06",
        detail: "",
        status: "waiting",
      },
    ];
    this.changePaginateTotal(
      this.list.includeRole.length,
      "paginateIncludeRole"
    );
    this.global.setLoading(false);

    // this.includeRole01Service
    //   .List(params)
    //   .subscribe((data: any) => {
    //     if (data) {
    //       // Set data
    //       this.list.includeRole = data.list;
    //       // Update pagin
    //       this.changePaginateTotal(
    //         this.list.includeRole.length,
    //         "paginateIncludeRole"
    //       );
    //     }
    //     // Close loading
    //     this.global.setLoading(false);
    //   });
  }

  //! <<< Prepare Call API >>>
  onSearch(): void {
    // Clear validate
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {};
    // Call api
    this.callIncludeRole01ChangeList(params);
  }
  onReset(): void {
    // Clear validate
    this.clearAllValidate();
    // Clear input
    this.input = {};
    // Call api list
    this.onSearch();
  }

  //! <<< Event >>>
  onClickAction(): void {
    this.router.navigate([ROUTE_PATH.INCLUDE_ROLE01_CHANGE.LINK]);
  }

  //! <<< Style >>>
  getStatusStyle(code: any): string {
    switch (code) {
      case this.master.statusCodeList[0].code:
        return "tag blue";
      case this.master.statusCodeList[1].code:
        return "tag";
      default:
        return "tag";
    }
  }

  getIconStyle(code: any): string {
    switch (code) {
      case "waiting":
        return "dip-icon-play";
      case "done":
        return "dip-icon-search-plus";
      default:
        return "dip-icon-play";  
    }
  }

  getIconColorStyle(code: any): string {
    switch (code) {
      case "waiting":
        return "btn btn-icon btn-brand";
      case "done":
        return "btn btn-icon btn-light-gray";
      default:
        return "btn btn-icon btn-brand";  
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: string, name: string, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: string, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Binding List (one way) >>>
  bindingInList(item: any, name: string, value: any): void {
    item[name] = value;
  }
  toggleBooleanInList(item: any, name: string): void {
    item[name] = !item[name];
  }
  onCheckAllInList(obj: string, name: string): void {
    this.input.is_check_all = !this.input.is_check_all;
    this[obj][name].forEach((item: any) => {
      item.is_check = this.input.is_check_all;
    });
  }
  onRemoveInList(
    obj: string,
    name: string,
    index: number,
    paginName?: string
  ): void {
    this[obj][name].splice(index, 1);
    if (paginName) {
      this.changePaginateTotal(this[obj][name].length, paginName);
    }
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === "paginateIncludeRole") {
      this.input.is_check_all = false;
      this.onSearch();
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Other >>>
  displayLabelReferenceMaster(list: any, code: any): string {
    return displayLabelReferenceMaster(list, code);
  }
}
