import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { Router } from "@angular/router";

import {
  ROUTE_PATH,
  CONSTANTS,
  validateService,
  clone,
  getItemCalculator,
  displayMoney,
  displayFormatBytes,
  PapaParseCsvToJson,
  loopDisplayDateServer,
  displayAddress,
  viewPDF,

} from "../../helpers";
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-madrid-role07-modal-check',
  templateUrl: './madrid-role07-modal-check.component.html',
  styleUrls: ['./madrid-role07-modal-check.component.scss']
})
export class MadridRole07ModalCheckComponent implements OnInit {

  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any
  public role: any


  public modal: any

  public listPublicItem: any = [];
  public doc: any = [];

  public selectedDoc: any = -1;

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<MadridRole07ModalCheckComponent>
  ) { }


  ngOnInit() {
    this.role = true;
    this.listPublicItem = [
      { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", statusprogress: "รอแก้ไขข้อบกพร่อง", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", command: "", status: "รอดำเนินการ" },
      { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", statusprogress: "รอแก้ไขข้อบกพร่อง", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", command: "", status: "ดำเนินการแล้ว" }
    ]

    this.doc = [{ value: "1", text: "MMC" }, { value: "2", text: "MMU" }]

    this.validate = {}
    this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      public_type_code: '',
      public_source_code: '',
      public_receiver_by: '',
      public_status_code: '',
      request_number: '',
    }
    this.master = {
      request_type: [{ code: 1, name: '1' }],
      job_reciver: [{ id: 1, name: "Sam" }, { id: 2, name: "Tom" }]
    }

  }

  closeModal() {
    this.dialogRef.close();
  }
}


