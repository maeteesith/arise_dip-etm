import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from "@ckeditor/ckeditor5-build-classic";
import { MyCustomUploadAdapterPlugin } from "../../helpers";
import { ForkJoinService } from '../../services/fork-join.service'
import { SubCommList, AgendaList } from '../../helpers/appealMaster'
import { Router, ActivatedRoute } from "@angular/router";
import { AppealService } from '../../services/Appeal.service'

@Component({
  selector: 'app-appeal-role02-case-summary',
  templateUrl: './appeal-role02-case-summary.component.html',
  styleUrls: ['./appeal-role02-case-summary.component.scss']
})
export class AppealRole02CaseSummaryComponent implements OnInit {

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private forkJoinService: ForkJoinService,
    private appealServices: AppealService,
  ) { }

  public modal: any
  public input: any
  public Editor: any
  public Kor03: boolean
  public sectionList: any []
  public reasonList: any []
  public box3_input: any
  public agendaList: any []
  public subcommList: any []

  public selectSectionList: any []
  public editID: any;

  public requestListLoad: any

  public checkedList: any []

  public header_box: any
  public item_Evidence_List: any []
  public item_Similar_List: any []
  public item_MarkReference_List: any []
  public item_FkCountry_List: any []
  public item_Section_List: any []
  public item_SubcommProposal_List: any []
  public item_SubcommResolution_List: any []

  ngOnInit() {

    this.editID = this.route.snapshot.paramMap.get("id")
    this.getData(this.editID)

    this.header_box = {
      request_number: null,
      register_number: null,
      request_date: null
    }

    this.modal = {
      isModalAddReason: false,
      isModalSection: false,
      isModalK08Detail: false
    }

    this.box3_input = {
      reason_code: "TYPE01",
      subcomm_code: "",
      agenda_code: "01"
    }

    this.selectSectionList = []
    this.selectSectionList.push({})
    this.texteditor()
    this.callInit()
    this.insertRow()
  }

  getData(params: any){
    this.appealServices.GetDataRequestCaseSummary(params).subscribe((data: any) => {
      if(data){
        this.requestListLoad = data
        console.log(this.requestListLoad);
        this.LoadAllData()
      }
    })
  }

  LoadAllData(){
    this.checkKOR()
    this.LoadHeaderBox()
    let getReason: String = this.requestListLoad.appeal_reason_code
    console.log(getReason)
    this.box3_input = {
      reason_code: getReason,
      subcomm_code: "",
      agenda_code: "01"
    }
    //
  }

  checkKOR(){
    switch(this.requestListLoad.requst_type_code){
      case "30": this.Kor03 = true; this.SetK03Data(); break;
      case "80": this.Kor03 = false; break;
      default: this.Kor03 = true; break; 
    }
  }

  //Data Set for Kor03
  SetK03Data(){
    this.item_Evidence_List = this.requestListLoad.item_evidence.map(Item => {return Item})
    this.item_Similar_List = this.requestListLoad.item_similar.map(Item => {return Item})
    this.item_MarkReference_List = this.requestListLoad.item_mark_referent.map(Item => {return Item})
    this.item_FkCountry_List = this.requestListLoad.item_fk_country.map(Item => {return Item})
    this.item_Section_List = this.requestListLoad.item_section.map(Item => {return Item})
    this.item_SubcommProposal_List = this.requestListLoad.item_subcommittee_proposal.map(Item => {return Item})
    this.item_SubcommResolution_List = this.requestListLoad.item_subcommittee_resulotion.map(Item => {return Item})
  }

  LoadHeaderBox(){
    this.header_box = {
      request_number: this.requestListLoad.reuest_number,
      register_number: this.requestListLoad.reuest_number,
      request_date: this.requestListLoad.request_date_text,
    }
  }

  toggleModal(name: string): void {
    if (!this.modal[name]) {
      this.modal[name] = true
    } else {
      this.modal[name] = false
      switch(name){
      }
    }
  }

  texteditor(){
    ClassicEditor.create(document.querySelector("#editor"), {
      initialData: " ",
      extraPlugins: [MyCustomUploadAdapterPlugin]
    })
      .then((newEditor: any) => {
        this.input = {
          ckeditor: newEditor
        };
      })
      .catch((error: any) => {
        console.error(error);
      });

    this.Editor = ClassicEditor;
  }

  callInit(): void{
    this.forkJoinService.initAppealCaseSummaryPage().subscribe((data: any) => {
      if(data){
        this.sectionList = data.appealSectionList.map(Item => {return Item})
        this.reasonList = data.appealReasonList.map(Item => {return Item})
      }
    })

    this.subcommList = SubCommList.map(Item => {return Item})
    this.agendaList = AgendaList.map(Item => {return Item})
  }

  insertRow(){
    this.selectSectionList = []
    let i: number = 0
    let j: number = 1
    while(i<j) {
      this.selectSectionList.push({
        code: null,
        name: null,
        detail: null
      })
      i++;
    }
  }

  toggleCheck(index): void {
    switch(this.sectionList[index].is_check){
      case true: this.sectionList[index].is_check = false; break;
      case false: this.sectionList[index].is_check = true; break;
    }
  }

  selectItem(){
    this.selectSectionList = []
    for (let i = 0; i < this.sectionList.length; i++) {
      if (this.sectionList[i].is_check == true) {
        this.selectSectionList.push(this.sectionList[i])
      }
    }
    this.selectSectionList.push({code: null, name: null, detail: null})
  }

  remove(code){

    // const remove = this.sectionList.map( x => { x.is_check = code.includes(x.key); return x})

    this.sectionList.find(item => item.code == code).is_check = false

    // switch(this.sectionList.code == code){
    //   case true: this.sectionList[index].is_check = false; break;
    //   case false: this.sectionList[index].is_check = true; break;
    // }
    console.log(this.sectionList)
  }

}
