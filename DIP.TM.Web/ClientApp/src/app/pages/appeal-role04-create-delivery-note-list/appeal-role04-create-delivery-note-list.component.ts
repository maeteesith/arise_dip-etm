import { Component, OnInit } from '@angular/core';
import {
  CONSTANTS,
  getMoment,
  clone,
  validateService,
  displayDate,
  displayMoney,
  displayString,
} from '../../helpers'
import { AppealRole04CreateDeliveryNoteList } from '../../helpers/appealMaster'


@Component({
  selector: 'app-appeal-role04-create-delivery-note-list',
  templateUrl: './appeal-role04-create-delivery-note-list.component.html',
  styleUrls: ['./appeal-role04-create-delivery-note-list.component.scss']
})
export class AppealRole04CreateDeliveryNoteListComponent implements OnInit {

  constructor() { }

  public dummyList: any[]
  public paginateItem: any
  public perPageItem: number[]
  public perPage: any[]

  ngOnInit() {
    this.paginateItem = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateItem.id = 'paginateItem'
    this.perPage = [10, 50, 100]

    this.dummyList = AppealRole04CreateDeliveryNoteList.map( Item => {
      return Item
    })

    this.changePaginateTotal((this.dummyList || []).length, 'paginateItem')
  }

  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }

}
