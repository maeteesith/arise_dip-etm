import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { OneStopServiceProcessService } from '../../services/one-stop-service-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-record-registration-number",
  templateUrl: "./record-registration-number.component.html",
  styleUrls: ["./record-registration-number.component.scss"]
})
export class RecordRegistrationNumberComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  // List RecordRegistrationNumber
  public listRecordRegistrationNumber: any[]
  public paginateRecordRegistrationNumber: any
  public perPageRecordRegistrationNumber: number[]
  // Response RecordRegistrationNumber
  public responseRecordRegistrationNumber: any
  public listRecordRegistrationNumberEdit: any

  public floor3_registrar_list: any[]

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      floor3_proposer_date: getMoment(),
      request_number: '',
    }
    this.listRecordRegistrationNumber = []
    this.paginateRecordRegistrationNumber = CONSTANTS.PAGINATION.INIT
    this.perPageRecordRegistrationNumber = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    //this.forkJoinService.initRecordRegistrationNumber().subscribe((data: any) => {
    //  if (data) {
    //    this.help.Clone(data, this.master)

    //  }
    this.OneStopServiceProcessService.Floor3RegistrarList().subscribe((floor3_registrar_list: any) => {
      this.floor3_registrar_list = floor3_registrar_list.list

      if (this.editID) {
        this.OneStopServiceProcessService.RecordRegistrationNumberLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }
    })

    //})
  }

  constructor(
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private OneStopServiceProcessService: OneStopServiceProcessService
  ) { }


  onClickRecordRegistrationNumberCheckRegisterPayment(): void {
    //if(this.validateRecordRegistrationNumberCheckRegisterPayment()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callRecordRegistrationNumberCheckRegisterPayment(this.saveData())
    //}
  }
  validateRecordRegistrationNumberCheckRegisterPayment(): boolean {
    let result = validateService('validateRecordRegistrationNumberCheckRegisterPayment', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberCheckRegisterPayment(params: any): void {
    this.OneStopServiceProcessService.RecordRegistrationNumberCheckRegisterPayment(params).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberCheckRegisterPaymentResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRecordRegistrationNumberPaymentList(): void {
    //if(this.validateRecordRegistrationNumberPaymentList()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callRecordRegistrationNumberPaymentList(this.saveData())
    //}
  }
  validateRecordRegistrationNumberPaymentList(): boolean {
    let result = validateService('validateRecordRegistrationNumberPaymentList', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberPaymentList(params: any): void {
    this.OneStopServiceProcessService.List(this.help.GetFilterParams(params, this.paginateRecordRegistrationNumber)).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberPaymentListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRecordRegistrationNumberSave(): void {
    //if(this.validateRecordRegistrationNumberSave()) {
    // Open loading
    this.global.setLoading(true)
    // Call api


    if (this.input.floor3_registrar_by && this.input.floor3_registrar_by != "") {
      this.listRecordRegistrationNumber.filter(r => r.is_check).forEach((item: any) => {
        item.floor3_registrar_by = +this.input.floor3_registrar_by
        item.floor3_proposer_date = this.input.floor3_proposer_date
      })
    } else {
      alert("กรุณาเลือกนายทะเบียน")
    }

    this.callRecordRegistrationNumberSave(this.listRecordRegistrationNumber.filter(r => r.is_check))
    //}
  }
  validateRecordRegistrationNumberSave(): boolean {
    let result = validateService('validateRecordRegistrationNumberSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberSave(params: any): void {
    this.OneStopServiceProcessService.RecordRegistrationNumberSave(params).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberSaveResponse(res)) {
      if (data) {
        // Set value
        this.onClickRecordRegistrationNumberPaymentList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRecordRegistrationNumberPrintDocument(): void {
    ////if(this.validateRecordRegistrationNumberPrintDocument()) {
    //// Open loading
    //this.global.setLoading(true)
    //// Call api
    //this.callRecordRegistrationNumberPrintDocument(this.saveData())
    ////}

    console.log(this.listRecordRegistrationNumber)
    var item_check_list = this.listRecordRegistrationNumber.filter(r => r.is_check)
    console.log(item_check_list)

    if (item_check_list.length > 0)
      var win = window.open("/floor3-document/edit/" + item_check_list.map((item: any) => {
        return item.request_number
      }).join("|"))


  }
  validateRecordRegistrationNumberPrintDocument(): boolean {
    let result = validateService('validateRecordRegistrationNumberPrintDocument', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberPrintDocument(params: any): void {
    this.OneStopServiceProcessService.RecordRegistrationNumberPrintDocument(params).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberPrintDocumentResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRecordRegistrationNumberSendRegistrarConsider(): void {
    //if(this.validateRecordRegistrationNumberSendRegistrarConsider()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callRecordRegistrationNumberSendRegistrarConsider(this.saveData())
    //}
  }
  validateRecordRegistrationNumberSendRegistrarConsider(): boolean {
    let result = validateService('validateRecordRegistrationNumberSendRegistrarConsider', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberSendRegistrarConsider(params: any): void {
    this.OneStopServiceProcessService.RecordRegistrationNumberSendRegistrarConsider(params).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberSendRegistrarConsiderResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRecordRegistrationNumberAdd(): void {
    this.listRecordRegistrationNumber.push({
      request_number: null,
      book_index: null,
      public_page_index: null,
      public_role04_date_text: null,
      public_role04_receive_date_text: null,

    })
    this.changePaginateTotal(this.listRecordRegistrationNumber.length, 'paginateRecordRegistrationNumber')
  }

  onClickRecordRegistrationNumberEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickRecordRegistrationNumberDelete(item: any): void {
    // if(this.validateRecordRegistrationNumberDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listRecordRegistrationNumber.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listRecordRegistrationNumber.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listRecordRegistrationNumber.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listRecordRegistrationNumber.length; i++) {
          if (this.listRecordRegistrationNumber[i].is_check) {
            this.listRecordRegistrationNumber[i].cancel_reason = rs
            this.listRecordRegistrationNumber[i].status_code = "DELETE"
            this.listRecordRegistrationNumber[i].is_deleted = true

            if (true && this.listRecordRegistrationNumber[i].id) ids.push(this.listRecordRegistrationNumber[i].id)
            // else this.listRecordRegistrationNumber.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callRecordRegistrationNumberDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listRecordRegistrationNumber.length; i++) {
          this.listRecordRegistrationNumber[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callRecordRegistrationNumberDelete(params: any, ids: any[]): void {
    this.OneStopServiceProcessService.RecordRegistrationNumberDelete(params).subscribe((data: any) => {
      // if(isValidRecordRegistrationNumberDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listRecordRegistrationNumber.length; i++) {
          if (this.listRecordRegistrationNumber[i].id == id) {
            this.listRecordRegistrationNumber.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listRecordRegistrationNumber.length; i++) {
        this.listRecordRegistrationNumber[i] = i + 1
      }

      //this.onClickRecordRegistrationNumberList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.OneStopServiceProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  onClickRecordRegistrationNumberSearch(row_item: any): void {
    var win = window.open("/request-search/edit/" + row_item.save_id)
    //
  }

  onClickRecordRegistrationNumberScan(row_item: any): void {
    var win = window.open("/request-document-collect/edit/" + row_item.request_number)
  }

  loadData(data: any): void {
    this.input = data

    this.listRecordRegistrationNumber = data.recordregistrationnumber_list || []
    this.changePaginateTotal(this.listRecordRegistrationNumber.length, 'paginateRecordRegistrationNumber')

  }

  listData(data: any): void {
    this.listRecordRegistrationNumber = data.list || []
    this.help.PageSet(data, this.paginateRecordRegistrationNumber)
  }

  saveData(): any {
    const params = {
      payment_start_date: this.input.payment_start_date,
      payment_end_date: this.input.payment_end_date,
      request_number: this.input.request_number,
    }

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
