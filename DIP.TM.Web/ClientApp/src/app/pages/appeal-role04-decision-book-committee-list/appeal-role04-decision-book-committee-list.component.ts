import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import '../../pages/appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss'
import { Auth } from "../../auth"
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  clone,
} from '../../helpers'

@Component({
  selector: 'app-appeal-role04-decision-book-committee-list',
  templateUrl: './appeal-role04-decision-book-committee-list.component.html',
  styleUrls: [
    '../appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss',
    './appeal-role04-decision-book-committee-list.component.scss']
})

export class AppealRole04DecisionBookCommitteeListComponent implements OnInit {
  public input: any;
  public validate: any;
  public statusList: any;

  // List Role01Floor3
  public listRole01Floor3: any[]
  public paginateRole01Floor3: any
  public perPageRole01Floor3: number[]

  constructor(
    private route: Router,
    private global: GlobalService,
    private auth: Auth,
    private help: Help,
    private SaveProcessService: SaveProcessService,
  ) { }

  ngOnInit() {
    this.input = {
      created_by_name: this.auth.getAuth().name,
      from_date: getMoment(),
      request_number: '',
      ref_number: '',
    }
    
    this.statusList = {
      typeCodeList: [
        { code: "1", name: "ทั้งหมด" },
        { code: "2", name: "ยังไม่ส่งงาน" },
        { code: "3", name: "ส่งงานแล้ว" },
        { code: "4", name: "ลบ" },
      ],
    };

    this.validate = {}

    this.listRole01Floor3 = []
    this.paginateRole01Floor3 = CONSTANTS.PAGINATION.INIT
    this.perPageRole01Floor3 = CONSTANTS.PAGINATION.PER_PAGE
  }

  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  onClickRole01Floor3List(): void {
    // if(this.validateSave03List()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03List(this.saveData())
    // }
  }

  callSave03List(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03ListPage(this.help.GetFilterParams(params, this.paginateRole01Floor3)).subscribe((data: any) => {
        // if(isValidSave03ListResponse(res)) {
        if (data) {
            // Set value
            pThis.listData(data)
        }
        // }
        // Close loading
        this.global.setLoading(false)
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }

  saveData(): any {
    const params = {
        request_number: this.input.request_number,
        request_index: this.input.request_index,
        make_date: this.input.from_date,
        save_status_code: this.input.save_status_code,
    }

    return params
  }

  onClickSave03SaveProcessEdit(item: any): void {
    var win = window.open("save-03-process/edit/" + item.id)
}

  listData(data: any): void {
    this.listRole01Floor3 = data.list || []
    this.help.PageSet(data, this.paginateRole01Floor3)

    let request_item_sub_type_1_list = []
    this.listRole01Floor3.forEach((item: any) => {
        if (item.product_01_list) {
            item.product_01_list.forEach((item_sub: any) => {
                request_item_sub_type_1_list.push(item_sub.request_item_sub_type_1_code)
            })
            item.item_sub_type_code_1 = request_item_sub_type_1_list.join(", ")
            item.save030_appeal_type_name = item.save030_appeal_type_codeNavigation ? item.save030_appeal_type_codeNavigation.name : ""
        }
    })
  }

  checkAllCheckBox(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
        this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

}

