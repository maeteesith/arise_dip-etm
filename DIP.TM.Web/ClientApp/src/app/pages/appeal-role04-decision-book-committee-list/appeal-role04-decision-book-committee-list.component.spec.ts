import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealRole04DecisionBookCommitteeListComponent } from './appeal-role04-decision-book-committee-list.component';

describe('AppealRole04DecisionBookCommitteeListComponent', () => {
  let component: AppealRole04DecisionBookCommitteeListComponent;
  let fixture: ComponentFixture<AppealRole04DecisionBookCommitteeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppealRole04DecisionBookCommitteeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealRole04DecisionBookCommitteeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
