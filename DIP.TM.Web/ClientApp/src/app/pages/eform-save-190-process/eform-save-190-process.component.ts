import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { GlobalService } from "../../global.service";
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from "../../services/fork-join-eform.service";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import {
  ROUTE_PATH,
  clone,
  validateService,
  CONSTANTS,
  displayAddress,
  clearIdAndSaveId,
  viewPDF,
} from "../../helpers";

@Component({
  selector: "app-eform-save-190-process",
  templateUrl: "./eform-save-190-process.component.html",
  styleUrls: ["./eform-save-190-process.component.scss"],
})
export class eFormSave190ProcessComponent
  implements OnInit, DeactivationGuarded {
  //TODO >>> Declarations <<<
  // Init
  public editID: number;
  public menuList: any[];
  public input: any;
  public validate: any;
  public master: any;
  public response: any;
  public modal: any;
  public popup: any;
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubID: number;
  public currentSubStep: number;
  public currentMiniID: number;
  public currentMiniStep: number;
  public progressPercent: number;
  // Other
  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eformSaveProcessService: eFormSaveProcessService
  ) {}

  ngOnInit() {
    this.editID = +this.route.snapshot.paramMap.get("id");
    this.menuList = [
      {
        id: 1,
        number: 1,
        name: "บันทึกเพื่อรับไฟล์",
        isShow: true,
        canEdit: false,
      },
      {
        id: 2,
        number: 2,
        name: "หนังสือขอผ่อนผันการส่งหลักฐานคำขอเลขที่",
        isShow: true,
        canEdit: false,
      },
      {
        id: 3,
        number: 3,
        name: "ผู้แจ้ง",
        isShow: true,
        canEdit: false,
      },
      {
        id: 4,
        number: 4,
        name: "รายละเอียดขอการผ่อนผัน",
        isShow: true,
        canEdit: false,
      },
      {
        id: 5,
        number: 5,
        name: "เอกสารหลักฐานที่ขอผ่อนผัน",
        isShow: true,
        canEdit: false,
      },
      {
        id: 6,
        number: 6,
        name: "ข้าพเจ้าขอรับรองว่า",
        isShow: true,
        canEdit: false,
      },
      {
        id: 7,
        number: 7,
        name: "เสร็จสิ้น",
        isShow: true,
        canEdit: false,
      },
    ];
    this.input = {
      indexEdit: undefined,
      point: "",
      isAllowEditRequestNumber: true,
      search_type_code: "REQUEST_NUMBER",
      informerTypeCodeList: [],
      informer_type_code: "OWNER",
      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      save190_representative_condition_type_code: "AND_OR",
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listOtherMark: [],
      save190_request_type_code: "010",
      isCheckAllOwnerSignature: false,
      isCheckAllAgentSignature: false,
    };
    this.validate = {};
    this.master = {
      addressEformCardTypeCodeList: [],
      addressCountryCodeList: [],
      addressRepresentativeConditionTypeCodeList: [],
      addressTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
      addressCareerCodeList: [],
      addressNationalityCodeList: [],
      representativeTypeCodeList: [],
      searchTypeCodeList: [],
      informerPeopleTypeCodeList: [],
      informerReceiverTypeCodeList: [],
      informerPeriodTypeCodeList: [],
      informerChallengerTypeCodeList: [],
      requestTypeCodeList: [],
    };
    this.response = {
      load: {},
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
    };
    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
      isWarning: false,
      isSearch: false,
    };
    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubID = 1;
    this.currentSubStep = 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
    this.progressPercent = 0;

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initEForm190Page().subscribe((data: any) => {
      if (data) {
        this.master = data;
        console.log("this.master", this.master);
      }
      if (this.editID) {
        this.eformSaveProcessService
          .eFormSave190Load(this.editID, {})
          .subscribe((data: any) => {
            if (data) {
              this.response.load = {
                eform_number: data.eform_number,
                id: data.id,
                wizard: data.wizard,
              };
              this.input.isAllowEditRequestNumber = data.request_number
                ? false
                : true;
              if (data.save190_search_type_code == "REQUEST_NUMBER") {
                this.input.informerTypeCodeList = this.master.informerPeopleTypeCodeList;
              } else if (data.save190_search_type_code == "CONTRACT_NUMBER") {
                this.input.informerTypeCodeList =
                  data.load_receiver_people_type_code === "RECEIVER"
                    ? this.master.informerReceiverTypeCodeList
                    : this.master.informerPeriodTypeCodeList;
              } else if (data.save190_search_type_code == "CHALLENGER_NUMBER") {
                this.input.informerTypeCodeList = this.master.informerChallengerTypeCodeList;
              } else {
                this.input.informerTypeCodeList = this.master.informerPeopleTypeCodeList;
              }
              // step 1
              this.input.email = data.email;
              this.input.telephone = data.telephone;
              // step 2
              this.input.search_type_code = data.save190_search_type_code
                ? data.save190_search_type_code
                : "REQUEST_NUMBER";
              this.input.request_number =
                data.save190_search_type_code === "REQUEST_NUMBER"
                  ? data.request_number
                  : data.save190_search_type_code === "CONTRACT_NUMBER"
                  ? data.contract_ref_number
                  : data.challenger_ref_number;
              this.input.registration_number = data.registration_number;
              this.input.informer_type_code = data.save190_informer_type_code
                ? data.save190_informer_type_code
                : "OWNER";
              // step 3
              this.input.listOwnerMark = data.people_list;
              this.input.listAgentMark = data.representative_list;
              this.input.save190_representative_condition_type_code = data.save190_representative_condition_type_code
                ? data.save190_representative_condition_type_code
                : "AND_OR";
              // step 4
              this.input.save190_request_type_code = data.save190_request_type_code
                ? data.save190_request_type_code
                : "010";
              this.input.postpone_day = data.postpone_day;
              // step 5
              this.input.remark_5 = data.remark_5;
              // step 6
              this.input.remark_6 = data.remark_6;
              // step 7
              this.input.isCheckAllOwnerSignature =
                data.sign_inform_person_list === "0" ? true : false;
              this.input.isCheckAllAgentSignature =
                data.sign_inform_representative_list === "0" ? true : false;
              this.manageWizard(data.wizard);
              this.setSignInform(
                "listAgentMark",
                data.sign_inform_representative_list
              );
              this.setSignInform("listOtherMark", data.sign_inform_others_list);
              // Close loading
              this.global.setLoading(false);
            } else {
              // Close loading
              this.global.setLoading(false);
            }
          });
      } else {
        this.input.informerTypeCodeList = this.master.informerPeopleTypeCodeList;
        // Close loading
        this.global.setLoading(false);
      }
    });
  }

  //! <<< Call API >>>
  callSendEmail190(params: any): void {
    this.eformSaveProcessService
      .eFormSave190Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchRequestNumber(): void {
    this.eformSaveProcessService
      .eFormSave190SearchRequestNumber(this.input.request_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.registration_number = data.registration_load_number;
          this.input.listOwnerMark = clearIdAndSaveId(data.people_load_list);
          this.input.listAgentMark = clearIdAndSaveId(
            data.representative_load_list
          );
          this.input.informerTypeCodeList = this.master.informerPeopleTypeCodeList;
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchChallengerNumber(): void {
    this.eformSaveProcessService
      .eFormSave190SearchChallengerNumber(this.input.request_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.registration_number = data.registration_load_number;
          this.input.listOwnerMark = clearIdAndSaveId(data.people_load_list);
          this.input.listAgentMark = clearIdAndSaveId(
            data.representative_load_list
          );
          this.input.informerTypeCodeList = this.master.informerChallengerTypeCodeList;
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchContractNumber(): void {
    this.eformSaveProcessService
      .eFormSave190SearchContractNumber(this.input.request_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.registration_number = data.registration_load_number;
          this.input.listOwnerMark = clearIdAndSaveId(data.people_load_list);
          this.input.listAgentMark = clearIdAndSaveId(
            data.representative_load_list
          );
          if (data.load_receiver_people_type_code === "RECEIVER") {
            this.input.informerTypeCodeList = this.master.informerReceiverTypeCodeList;
            this.input.informer_type_code = "RECEIVER";
          } else {
            this.input.informerTypeCodeList = this.master.informerPeriodTypeCodeList;
            this.input.informer_type_code = "RECEIVER_PERIOD";
          }
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callEFormSave190Save(params: any): void {
    this.eformSaveProcessService
      .eFormSave190Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.isDeactivation = true;
          // Open toast success
          let toast = CONSTANTS.TOAST.SUCCESS;
          toast.message = "บันทึกข้อมูลสำเร็จ";
          this.global.setToast(toast);
          // Navigate
          this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
        } else {
          // Close loading
          this.global.setLoading(false);
        }
      });
  }

  //! <<< Prepare Call API >>>
  sendEmail(): void {
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      wizard: this.getWizard(),
      email: this.input.email,
      telephone: this.input.telephone,
    };
    // Call api
    this.callSendEmail190(params);
  }
  onSearch(): void {
    let result = validateService(
      "validateEFormSave140ProcessStep2",
      this.input
    );
    this.validate = result.validate;

    if (result.isValid) {
      // Open loading
      this.global.setLoading(true);
      // Call api
      if (this.input.search_type_code == "REQUEST_NUMBER") {
        this.callSearchRequestNumber();
      } else if (this.input.search_type_code == "CHALLENGER_NUMBER") {
        this.callSearchChallengerNumber();
      } else {
        this.callSearchContractNumber();
      }
    } else {
      if (this.input.search_type_code == "CONTRACT_NUMBER") {
        this.validate = { request_number: "กรุณากรอก สัญญาเลขที่" };
      }
      if (this.input.search_type_code == "CHALLENGER_NUMBER") {
        this.validate = { request_number: "กรุณากรอก ผู้คัดค้านเลขที่" };
      }
    }
  }
  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }
  save(isOverwrite: boolean): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = this.getParamsSave();
    if (!isOverwrite) {
      //params = getParamsOverwrite(params);
    }
    // Call api
    this.callEFormSave190Save(params);
  }
  getParamsSave(): any {
    return {
      id: this.response.load.id ? this.response.load.id : null,
      eform_number: this.response.load.eform_number
        ? this.response.load.eform_number
        : null,
      wizard: this.getWizard(),
      // step 1
      email: this.input.email,
      telephone: this.input.telephone,
      // step 2
      save190_search_type_code: this.input.search_type_code,
      request_number:
        this.input.search_type_code === "REQUEST_NUMBER"
          ? this.input.request_number
          : null,
      contract_ref_number:
        this.input.search_type_code === "CONTRACT_NUMBER"
          ? this.input.request_number
          : null,
      challenger_ref_number:
        this.input.search_type_code === "CHALLENGER_NUMBER"
          ? this.input.request_number
          : null,
      registration_number: this.input.registration_number,
      save190_informer_type_code: this.input.informer_type_code,
      // step 3
      people_list: this.input.listOwnerMark,
      representative_list: this.input.listAgentMark,
      save190_representative_condition_type_code: this.input
        .save190_representative_condition_type_code,
      // step 4
      save190_request_type_code: this.input.save190_request_type_code,
      postpone_day: this.input.postpone_day,
      // step 5
      remark_5: this.input.remark_5,
      // step 6
      remark_6: this.input.remark_6,
      // step 7
      sign_inform_person_list: this.input.isCheckAllOwnerSignature ? "0" : "",
      sign_inform_representative_list:
        this.input.isCheckAllAgentSignature &&
        this.input.save140_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform("listAgentMark"),
      sign_inform_others_list: this.getSignInform("listOtherMark"),
    };
  }
  onClickViewPdfA19(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM19", this.getParamsSave());
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Event >>>
  getWizard(): String {
    if (this.editID) {
      let wizardList = this.response.load.wizard.split("|");
      let sum = 0;
      let sumNow =
        this.currentID +
        this.currentStep +
        this.currentSubID +
        this.currentSubStep;

      wizardList.forEach((item: any) => {
        sum = +sum + +item;
      });

      if (sumNow > sum) {
        return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
      } else {
        return this.response.load.wizard;
      }
    } else {
      return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
    }
  }
  getSignInform(nameList: any): String {
    let result = "";
    this.input[nameList].forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(nameList: any, signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input[nameList].forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save190_representative_condition_type_code != "AND") {
        this.input.listAgentMark.forEach((item: any) => {
          item.isCheck = false;
        });
        this.input.listOtherMark.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }
  onChangeSearchType(): void {
    this.clearValidate("request_number");
    this.input.request_number = "";
    this.input.registration_number = "";
    this.input.informer_type_code = "";
    this.input.listOwnerMark = [];
    this.input.listAgentMark = [];

    if (this.input.search_type_code === "REQUEST_NUMBER") {
      this.input.informerTypeCodeList = this.master.informerPeopleTypeCodeList;
      this.input.informer_type_code = "OWNER";
    } else if (this.input.search_type_code === "CHALLENGER_NUMBER") {
      this.input.informerTypeCodeList = this.master.informerChallengerTypeCodeList;
      this.input.informer_type_code = "CHALLENGER";
    } else if (this.input.search_type_code === "CONTRACT_NUMBER") {
      this.input.informerTypeCodeList = this.master.informerReceiverTypeCodeList;
      this.input.informer_type_code = "RECEIVER";
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Table >>>
  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }
  toggleBooleanInTable(item: any, name: any, condition?: any): void {
    item[name] = !item[name];
  }
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all;
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all;
      });
    } else {
      item.is_check = !item.is_check;
    }
  }
  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    if (this.validateSaveItemModalInTable(nameItem)) {
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        this.toggleModal(nameModal);
        this.updatePaginateForTable(nameList);
      }
    }
  }
  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
    this.updatePaginateForTable(name);
  }
  updatePaginateForTable(name: any): void {}
  validateSaveItemModalInTable(nameItem: any): boolean {
    return true;
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    // if (name === "paginateDocument") {
    //   this.onClickDocumentItemList();
    //   this.input.is_check_all = false;
    // }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      // this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    // this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
    }
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
      this.updatePaginateForTable(nameList);
    }

    if (nameList === "listOwnerMark" || nameList === "listAgentMark") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Wizard >>>
  onClickMenu(menu: any): void {
    if (menu.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentSubStep = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickSubMenu(menu: any, sub: any): void {
    if (sub.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = sub.id;
        this.currentSubStep = sub.number;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickNext(): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      if (this.menuList[this.currentID - 1].hasSub) {
        let sizeSubMenu = this.menuList[this.currentID - 1].sizeSubList;

        if (this.currentSubStep < sizeSubMenu) {
          this.nextSubMenu();
        } else {
          if (this.currentSubID > 0) {
            this.menuList[this.currentID - 1].subList[
              this.currentSubID - 1
            ].canEdit = true;
          }
          this.nextMenu();
        }
      } else {
        this.nextMenu();
      }
    }
  }
  nextMenu(): void {
    let indexCurrentMenu = 0;
    let indexNextMenu = 0;

    // Find index current
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Cal Progress Percent
    if (!this.menuList[indexCurrentMenu].canEdit) {
      this.calcProgressPercent(this.currentStep);
    }
    // Set can edit
    this.menuList[indexCurrentMenu].canEdit = true;
    // Next wizard
    this.currentStep++;

    // Find index next
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexNextMenu = i;
      }
    });

    // Update wizard point
    this.currentID = this.menuList[indexNextMenu].id;
    this.currentSubID = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentSubStep = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  nextSubMenu(): void {
    let indexCurrentMenu = 0;
    let indexCurrentSub = 0;
    let indexNextSub = 0;

    // Find index current menu
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Find index current sub menu
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexCurrentSub = i;
      }
    });

    // Set can edit
    if (this.currentSubID > 0) {
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].canEdit = true;
    } else {
      if (this.menuList[indexCurrentMenu].isUseMenuBeforeSubList) {
        this.menuList[indexCurrentMenu].canEdit = true;
      }
    }
    // Next wizard sub
    this.currentSubStep++;

    // Find index sub next
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexNextSub = i;
      }
    });

    // Update wizard sub point
    this.currentSubID = this.menuList[indexCurrentMenu].subList[
      indexNextSub
    ].id;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  changeMiniStep(action: any): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let indexCurrentMini = 0;
      let indexNextMini = 0;
      let sizeMiniList = 0;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
        }
      });

      // Find index current sub menu
      this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
        if (item.number === this.currentSubStep && item.isShow) {
          indexCurrentSub = i;
        }
      });

      // Find index current mini menu
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList.forEach(
        (item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexCurrentMini = i;
          }
        }
      );

      // Get size mini list
      sizeMiniList = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
        .sizeMiniList;

      // Condition
      if (sizeMiniList === this.currentMiniStep && action === "next") {
        // Set can edit
        this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
          indexCurrentMini
        ].canEdit = true;
        // Next menu
        this.onClickNext();
      } else {
        if (action === "next") {
          // Set can edit
          this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
            indexCurrentMini
          ].canEdit = true;
          // Next wizard mini
          this.currentMiniStep++;
        } else {
          // Back wizard mini
          this.currentMiniStep = this.currentMiniStep - 1;
        }

        // Find index next mini menu
        this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList.forEach((item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexNextMini = i;
          }
        });

        // Update wizard mini point
        this.currentMiniID = this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList[indexNextMini].id;
      }
    }
  }
  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    } else if (this.currentID === 2) {
      let result = validateService(
        "validateEFormSave140ProcessStep2",
        this.input
      );
      this.validate = result.validate;

      if (!this.input.listOwnerMark || this.input.listOwnerMark.length === 0) {
        result.isValid = false;
        console.warn(`request_number is invalid.`);
        this.validate.request_number = "กรุณาคลิก ค้นหา";
      }

      return result.isValid;
    } else if (this.currentID === 3) {
      if (
        this.input.informer_type_code === "OWNER" ||
        this.input.informer_type_code == "RECEIVER" ||
        this.input.informer_type_code == "RECEIVER_PERIOD" ||
        this.input.informer_type_code == "CHALLENGER"
      ) {
        if (this.input.listOwnerMark.length < 1) {
          console.warn(`step3 is invalid.`);
          this.validate.step3 = "กรุณาเพิ่มผู้แจ้ง";
          return false;
        }
      } else if (this.input.informer_type_code === "REPRESENTATIVE") {
        if (this.input.listAgentMark.length < 1) {
          console.warn(`step3 is invalid.`);
          this.validate.step3 = "กรุณาเพิ่มตัวแทนผู้แจ้ง";
          return false;
        }
      } else if (this.input.informer_type_code === "OTHERS") {
        if (this.input.listOtherMark.length < 1) {
          console.warn(`step3 is invalid.`);
          this.validate.step3 = "กรุณาเพิ่มผู้แจ้ง";
          return false;
        }
      }

      return true;
    } else if (this.currentID === 4) {
      let result = validateService(
        "validateEFormSave190ProcessStep4",
        this.input
      );
      this.validate = result.validate;
      return result.isValid;
    } else if (this.currentID === 7) {
      let isValid = false;

      if (
        this.input.informer_type_code === "OWNER" ||
        this.input.informer_type_code == "RECEIVER" ||
        this.input.informer_type_code == "RECEIVER_PERIOD" ||
        this.input.informer_type_code == "CHALLENGER"
      ) {
        if (this.input.isCheckAllOwnerSignature) {
          isValid = true;
        }
      } else if (this.input.informer_type_code === "REPRESENTATIVE") {
        if (this.input.isCheckAllAgentSignature) {
          isValid = true;
        }
        if (this.input.listAgentMark.length > 0) {
          this.input.listAgentMark.forEach((item: any) => {
            if (item.isCheck) {
              isValid = true;
            }
          });
        }
      } else if (this.input.informer_type_code === "OTHERS") {
        if (this.input.listOtherMark.length > 0) {
          this.input.listOtherMark.forEach((item: any) => {
            if (item.isCheck) {
              isValid = true;
            }
          });
        }
      }

      if (!isValid) {
        console.warn(`step7 is invalid.`);
        this.validate.step7 = "กรุณาเลือกลงลายมือชื่อ";
      } else {
        // Set can edit
        this.menuList[6].canEdit = true;
      }

      return isValid;
    } else {
      return true;
    }
  }
  conditionWizard(condition: any): void {}
  manageWizard(wizard: any): void {
    let wizardList = wizard ? wizard.split("|") : [1, 1, 1, 1];
    this.currentID = +wizardList[0];
    this.currentStep = +wizardList[1];
    this.currentSubID = +wizardList[2];
    this.currentSubStep = +wizardList[3];
    this.calcProgressPercent(this.currentStep);

    this.menuList.forEach((item: any) => {
      if (this.currentID > item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            sub.canEdit = true;
          });
        }
      }
      if (this.currentID === item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (this.currentSubID >= sub.id) {
              sub.canEdit = true;
            }
          });
        }
      }
    });
  }
  reRunMenuNumber(): void {
    let number = 1;
    let numberSub = 1;
    let numberMini = 1;
    this.menuList.forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
        numberSub = 1;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (sub.isShow) {
              sub.number = numberSub;
              numberSub++;
              numberMini = 1;
              if (sub.miniList) {
                sub.miniList.forEach((mini: any) => {
                  if (mini.isShow) {
                    mini.number = numberMini;
                    numberMini++;
                  }
                });
              }
            }
          });
        }
      }
    });
  }
  calcProgressPercent(currentStep: number): void {
    let lastItem = this.menuList[this.menuList.length - 1];
    let progressPercent = Math.round(
      ((currentStep + 1) / lastItem.number) * 100
    );
    this.progressPercent = progressPercent > 100 ? 100 : progressPercent;
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }

  //! <<< Other >>>
  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  displayAddress(value: any): any {
    return displayAddress(value);
  }
  onKey(e: any, name: any): void {
    if (e.keyCode === 13) {
      if (name === "request_number") {
        this.onSearch();
      }
    } else {
      if (name === "request_number") {
        this.input.registration_number = "";
        this.input.listOwnerMark = [];
        this.input.listAgentMark = [];
      }
    }
  }
}
