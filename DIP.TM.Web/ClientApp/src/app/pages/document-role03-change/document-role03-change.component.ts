import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  PapaParseCsvToJson,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-document-role03-change",
  templateUrl: "./document-role03-change.component.html",
  styleUrls: ["./document-role03-change.component.scss"]
})
export class DocumentRole03ChangeComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  //public contactAddress: any
  //public modalAddress: any
  //public modalAddressEdit: any

  // List CheckingInstructionList
  public listCheckingInstructionList: any[]
  public paginateCheckingInstructionList: any
  public perPageCheckingInstructionList: number[]
  // Response CheckingInstructionList
  public responseCheckingInstructionList: any
  public listCheckingInstructionListEdit: any

  // List AddressList
  public listAddressList: any[]
  public paginateAddressList: any
  public perPageAddressList: number[]
  // Response AddressList
  public responseAddressList: any
  public listAddressListEdit: any

  // List CheckingInstructionDocumentList
  public listCheckingInstructionDocumentList: any[]
  public paginateCheckingInstructionDocumentList: any
  public perPageCheckingInstructionDocumentList: number[]
  // Response CheckingInstructionDocumentList
  public responseCheckingInstructionDocumentList: any
  public listCheckingInstructionDocumentListEdit: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  public is_loaded: any

  public popup: any
  public send_back: any

  public tab_item_show_index: any
  public tab_menu_show_index: any
  public tab_version_show_index: any

  public tablePeople: any
  public tableRepresentative: any
  public contactAddress: any
  public contactDefaultAddress: any
  public tableProduct: any
  public tableJoiner: any
  public tableCertificationFile: any
  public request_mark_feature_code_list: any
  public tableRequestGroup: any
  public tableHistory: any

  public tableRepresentativeDuplicate: any

  public tableAllow: any

  public document_role03_item_version_list: any[]

  public input_version: any

  public player: any
  public player_action: any


  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }


    this.tab_menu_show_index = 1

    this.tablePeople = {
      column_list: {
        index: "#",
        name: "ชื่อเจ้าของ",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      is_address_edit: true,
    }

    this.tableRepresentative = {
      column_list: {
        index: "#",
        name: "ชื่อตัวแทน",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      command: [{
        title: "ค้นหาตัวแทน",
        name: "representative_duplicate",
        icon: "duplicate",
      }],
      object_name: "ตัวแทน",
      is_address_edit: true,
    }

    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.contactDefaultAddress = {}

    this.tableProduct = {
      column_list: {
        index: "#",
        request_item_sub_type_1_code: "จำพวกสินค้า",
        description: "รายการสินค้า/บริการ",
        count: "จำนวน",
      },
      column_edit_list: {
        request_item_sub_type_1_code: {
          type: "textbox",
          group_by: "description",
        },
        description: {
          type: "textbox",
          type_click: "textarea",
          width: 800,
        },
      },
      button_list: [{
        name: "product_upload",
        file: ".csv",
        title: "แนบไฟล์",
        icon: "upload-circle",
      }],
      can_added: true,
    }


    this.tableJoiner = {
      column_list: {
        index: "#",
        name: "ชื่อผู้ใช้ร่วม",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      object_name: "ผู้ใช้ร่วม",
      is_address_edit: true,
    }

    this.request_mark_feature_code_list = {}

    this.tableCertificationFile = {
      column_list: {
        index: "#",
        created_date: "วันที่อัพโหลด",
        file_name: "ชื่อไฟล์",
        file_size: "ขนาดไฟล์",
        remark: "หมายเหตุ",
      },
      column_edit_list: { remark: { type: "textbox", }, },
      can_deleted: true,
      button_list: [{
        name: "file_upload",
        title: "แนบไฟล์",
        icon: "upload-circle",
        file: "application/pdf",
        is_uploaded: true,
      }],
    }

    this.tableRequestGroup = {
      column_list: {
        index: "#",
        request_number: "คำขอเลขที่",
        registration_number: "ทะเบียนเลขที่",
        trademark_status_code: "สถานะคำขอ",
        name: "ชื่อเจ้าของ",
        save010_registration_group_status_code: "สถานะ",
        make_date: "วันที่มีคำสั่ง",
        allow_date: "วันที่อนุญาต",
      },
      column_edit_list: {
        save010_registration_group_status_code: { type: "combo_box", master: "save010RequestGroupStatusCodeList", },
        make_date: { type: "datetime", },
        allow_date: { type: "datetime", },
      },
      can_deleted: true,
      //can_added: true,
    }
    this.tableHistory = {
      column_list: {
        index: "#",
        make_date: "วันที่",
        created_by_name: "ผู้ออกคำสั่ง",
        description: "รายการ",
      },
      command: [{
        name: "history_report",
        title: "แสดงรายงาน",
        icon: "printer",
      }],
      has_link: ["row_item.file_id && row_item.file_id > 0 ? '/file/Content/' + row_item.file_id : null"],
    }

    this.tableAllow = {
      column_list: {
        index: "#",
        name: "คำสั่ง",
        value_1: "รายละเอียด",
      },
      can_checked: true,
      //command: [{
      //  name: "history_report",
      //  title: "แสดงรายงาน",
      //  icon: "printer",
      //}],
      //has_link: ["row_item.file_id && row_item.file_id > 0 ? '/file/Content/' + row_item.file_id : null"],
    }


    this.tableRepresentativeDuplicate = {
      column_list: {
        index: "#",
        name: "ชื่อตัวแทน",
        card_number: "เลขที่บัตร",
        address_information: "ที่อยู่",
      }, command: [{
        name: "representative_duplicate_item",
        icon: "duplicate",
      }],
    }


    this.input = {
      view: {},
      //id: null,
      //request_number: '',
      //name: '',
      //request_item_sub_type_1_code_text: '',
      //request_item_type_name: '',
      //is_trademark_join: false,
      //request_date: '',
      //representative_name: '',
      //house_number: '',
      //is_trademark_sound: false,
    }
    this.listCheckingInstructionList = []
    this.paginateCheckingInstructionList = CONSTANTS.PAGINATION.INIT
    this.perPageCheckingInstructionList = CONSTANTS.PAGINATION.PER_PAGE

    this.listAddressList = []
    this.paginateAddressList = CONSTANTS.PAGINATION.INIT
    this.perPageAddressList = CONSTANTS.PAGINATION.PER_PAGE

    this.listCheckingInstructionDocumentList = []
    this.paginateCheckingInstructionDocumentList = CONSTANTS.PAGINATION.INIT
    this.perPageCheckingInstructionDocumentList = CONSTANTS.PAGINATION.PER_PAGE

    this.input_version = {
      view: {},
      tab_edit_show: {},
      document_role03_item: {},
    }
    //Master List
    this.master = {
    }
    //Master List


    this.popup = {
      document_role03_change_kor_120_type: {},
      representative_name_search: "",
    }
    this.send_back = {}

    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    //this.modalAddress = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
    })

    this.forkJoinService.initDocumentRole03Change().subscribe((data: any) => {
      if (data) {
        data.addressTypeCodeList.splice(2, 1)
        this.help.Clone(data, this.master)

      }
      if (this.editID) {
        this.DocumentProcessService.DocumentRole03ChangeLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure

            var document_role03_item_list = []
            var document_role03_item_list_request_type_name = {}

            data.document_role03_item_list.forEach((item: any) => {
              document_role03_item_list_request_type_name[item.request_type_code] = item.request_type_name
            })

            Object.keys(document_role03_item_list_request_type_name).forEach((item: any) => {
              document_role03_item_list.push({
                request_type_code: item,
                request_type_name: document_role03_item_list_request_type_name[item],
              })
            })

            data.document_role03_item_list = document_role03_item_list

            this.input = data

            //this.loadData(data)

            //this.onClickDocumentRole03ChangeVersionList(data.document_role03_item_list[2])
            this.onClickDocumentRole03ChangeVersionList(data.document_role03_item_list[0])
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

      this.is_loaded = true

    })
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }


  onClickDocumentRole03ChangeCheckingInstructionListAdd(): void {
    this.listCheckingInstructionList.push({
      index: this.listCheckingInstructionList.length + 1,
      instruction_rule_name: null,
      book_number: null,
      book_start_date_text: null,
      save010_checking_instruction_document_remark: null,
      considering_instruction_rule_status_name: null,

    })
    this.changePaginateTotal(this.listCheckingInstructionList.length, 'paginateCheckingInstructionList')
  }

  onClickDocumentRole03ChangeAddressListAdd(): void {
    this.listAddressList.push({
      index: this.listAddressList.length + 1,
      house_number: null,

    })
    this.changePaginateTotal(this.listAddressList.length, 'paginateAddressList')
  }

  onClickDocumentRole03ChangeAddressListEdit(item: any): void {
    var win = window.open("/" + item.id)
  }

  onClickDocumentRole03ChangeVersionList(row_item: any): void {
    this.tab_item_show_index = row_item
    this.callDocumentRole03ChangeVersionList(this.editID.split("_")[0], row_item.request_type_code)
  }
  callDocumentRole03ChangeVersionList(save_id: any, request_type_code: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole03ChangeVersionList(save_id, request_type_code).subscribe((data: any) => {
      this.global.setLoading(false)
      if (data) {
        this.document_role03_item_version_list = data

        this.onClickDocumentRole03ChangeVersionLoad(this.document_role03_item_version_list[0])
      }
    })
  }


  onClickDocumentRole03ChangeVersionLoad(row_item: any): void {
    this.tab_version_show_index = row_item
    this.callDocumentRole03ChangeVersionLoad(this.editID.split("_")[0], row_item.id, row_item.request_type_code)
  }
  callDocumentRole03ChangeVersionLoad(save_id: any, id: any, request_type_code: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole03ChangeVersionLoad(save_id, id, request_type_code).subscribe((data: any) => {
      this.global.setLoading(false)
      if (data) {
        this.loadData(data)
        //this.document_role03_item_version_list = data
      }
    })
  }


  onClickDocumentRole03ChangeSend(): void {
    if (this.tableAllow.Get().filter(r => !r.is_check).length > 0) {
      alert("กรุณาดำเนินการตามคำสั่งนายทะเบียนให้ครบทุกขั้นตอน")
      return;
    }
    this.callDocumentRole03ChangeSend(this.saveData())
  }
  callDocumentRole03ChangeSend(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole03ChangeSend(params).subscribe((data: any) => {
      this.global.setLoading(false)
      if (data) {
        this.tab_version_show_index.document_role03_receive_status_code = 'SEND'
        this.onClickDocumentRole03ChangeVersionLoad(this.tab_version_show_index)
        //this.loadData(data)
        //this.document_role03_item_version_list = data
      }
    })
  }

  onClickDocumentRole03ChangeDuplicate(): void {
    this.global.setLoading(true)
    if (this.input.request_number_duplicate != "") {
      this.DocumentProcessService.ClassificationLoadFromRequestNumber(this.input.request_number_duplicate).subscribe((data: any) => {
        if (data) {
          data.people_list.forEach((item: any) => {
            item.id = null
            item.save_id = null
          })
          data.representative_list.forEach((item: any) => {
            item.id = null
            item.save_id = null
          })

          this.tablePeople.Set(data.people_list)
          this.tableRepresentative.Set(data.representative_list)
        }
        this.global.setLoading(false)
      })
    }
  }

  togglePlayer(): void {
    if (this.player_action) {
      this.player.pause()
    } else {
      this.player.play()
    }
    this.player_action = !this.player_action
  }

  onClickDocumentRole03ChangeAddressListDelete(item: any): void {
    // if(this.validateDocumentRole03ChangeAddressListDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listAddressList.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listAddressList.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listAddressList.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listAddressList.length; i++) {
          if (this.listAddressList[i].is_check) {
            this.listAddressList[i].cancel_reason = rs
            this.listAddressList[i].status_code = "DELETE"
            this.listAddressList[i].is_deleted = true

            if (true && this.listAddressList[i].id) ids.push(this.listAddressList[i].id)
            // else this.listAddressList.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole03ChangeAddressListDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listAddressList.length; i++) {
          this.listAddressList[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole03ChangeAddressListDelete(params: any, ids: any[]): void {
    //this.DocumentProcessService.DocumentRole03ChangeAddressListDelete(params).subscribe((data: any) => {
    //  // if(isValidDocumentRole03ChangeAddressListDeleteResponse(res)) {

    //  ids.forEach((id: any) => {
    //    for (let i = 0; i < this.listAddressList.length; i++) {
    //      if (this.listAddressList[i].id == id) {
    //        this.listAddressList.splice(i--, 1);
    //      }
    //    }
    //  });

    //  for (let i = 0; i < this.listAddressList.length; i++) {
    //    this.listAddressList[i] = i + 1
    //  }

    //  this.onClickDocumentRole03ChangeAddressList()
    //  // Close loading
    //  this.global.setLoading(false)
    //})
  }

  onClickDocumentRole03ChangeCheckingInstructionDocumentListAdd(): void {
    this.listCheckingInstructionDocumentList.push({
      index: this.listCheckingInstructionDocumentList.length + 1,
      instruction_rule_name: null,
      instruction_send_date_text: null,
      book_round_01_start_date_text: null,
      book_round_02_start_date_text: null,
      book_number: null,
      document_role02_receive_status_name: null,

    })
    this.changePaginateTotal(this.listCheckingInstructionDocumentList.length, 'paginateCheckingInstructionDocumentList')
  }

  onClickDocumentRole03ChangeCheckingInstructionDocumentListEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickDocumentRole03ChangeCheckingInstructionDocumentListDelete(item: any): void {
    // if(this.validateDocumentRole03ChangeCheckingInstructionDocumentListDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listCheckingInstructionDocumentList.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listCheckingInstructionDocumentList.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listCheckingInstructionDocumentList.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
          if (this.listCheckingInstructionDocumentList[i].is_check) {
            this.listCheckingInstructionDocumentList[i].cancel_reason = rs
            this.listCheckingInstructionDocumentList[i].status_code = "DELETE"
            this.listCheckingInstructionDocumentList[i].is_deleted = true

            if (true && this.listCheckingInstructionDocumentList[i].id) ids.push(this.listCheckingInstructionDocumentList[i].id)
            // else this.listCheckingInstructionDocumentList.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole03ChangeCheckingInstructionDocumentListDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
          this.listCheckingInstructionDocumentList[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole03ChangeCheckingInstructionDocumentListDelete(params: any, ids: any[]): void {
    //this.DocumentProcessService.DocumentRole03ChangeCheckingInstructionDocumentListDelete(params).subscribe((data: any) => {
    //  // if(isValidDocumentRole03ChangeCheckingInstructionDocumentListDeleteResponse(res)) {

    //  ids.forEach((id: any) => {
    //    for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
    //      if (this.listCheckingInstructionDocumentList[i].id == id) {
    //        this.listCheckingInstructionDocumentList.splice(i--, 1);
    //      }
    //    }
    //  });

    //  for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
    //    this.listCheckingInstructionDocumentList[i] = i + 1
    //  }

    //  this.onClickDocumentRole03ChangeCheckingInstructionDocumentList()
    //  // Close loading
    //  this.global.setLoading(false)
    //})
  }

  onClickOpen(url: any, id: any = null): void {
    id = id || this.editID.split("_")[0]
    window.open(url + id)
  }


  onClickCommand($event): void {
    console.log($event)

    if ($event.object_list.length > 0) {
      if ($event.command.name == "instruction_report") {
        window.open("pdf/ConsideringSimilaInstruction/" + this.input.request_number + "_" + $event.object_list.map((item: any) => { return item.id }).join("|"))
      } else if ($event.command.name == "history_report") {
        window.open("pdf/Save010History/" + this.input.request_number + "_" + $event.object_list.map((item: any) => { return item.id }).join("|"))
      } else if ($event.command.name == "file_upload") {
        var param = {
          //save_id: +this.editID,
          file_id: +$event.object_list[0].id,
          created_date: $event.object_list[0].created_date,
          created_date_text: $event.object_list[0].created_date_text,
          file_name: $event.object_list[0].file_name,
          file_size: $event.object_list[0].file_size,
          remark: $event.object_list[0].remark,
        }
        //this.input_version.certification_file_list
        //this.PublicProcessService.CertificationFileAdd([param]).subscribe((data: any) => {
        //  if (data && data.length > 0) {
        this.tableCertificationFile.Add(param)
        //  }
        //})
        //var param = {
        //  save_id: +this.editID,
        //  file_id: +$event.object_list[0].id,
        //}
        //this.PublicProcessService.CertificationFileAdd([param]).subscribe((data: any) => {
        //  if (data && data.length > 0) {
        //    this.tableCertificationFile.Add(data[0])
        //  }
        //})
      } else if ($event.command.name == "representative_duplicate_item") {
        $event.object_list.forEach((item: any) => {
          var new_item = {
            ...item,
            id: null,
            save_id: null,
            is_check: null,
          }
          this.tableRepresentative.Add(new_item)
        })
        this.popup.is_representative_show = false
      } else if ($event.command.name == "product_upload") {
        this.onClickUploadCSV($event.object_list[0])
      }
    }

    if ($event.command.name == "representative_duplicate") {
      if ($event.object_list.length > 0) {
        $event.object_list.forEach((item: any) => {
          var new_item = {
            ...item,
            id: null,
            save_id: null,
            is_check: null,
          }
          this.tableRepresentative.Add(new_item)
        })
      } else {
        this.popup.is_representative_show = true
      }
    }
  }


  onKeyPressDocumentRole03ChangeRequestNumberAdd(event): void {
    console.log(event)
    if (event.key == "Enter") {
      var params = {
        request_number: this.tableRequestGroup.request_number_add
      }

      this.tableRequestGroup.request_number_add = ""

      // Call api
      this.callPressDocumentRole03ChangeRequestNumberAdd(params)
    }
  }
  callPressDocumentRole03ChangeRequestNumberAdd(params: any): void {
    this.DocumentProcessService.SaveList(this.help.GetFilterParams(params)).subscribe((data: any) => {
      if (data && data.list && data.list.length > 0) {
        this.tableRequestGroup.Add({
          save_id: +this.editID,
          request_number: data.list[0].request_number,
          registration_number: data.list[0].registration_number,
          trademark_status_code: data.list[0].trademark_status_code,
          name: data.list[0].name,
        })
      }
      this.global.setLoading(false)
    })
  }


  onClickDocumentRole03ChangeSave(): void {
    this.callDocumentRole03ChangeSave(this.saveData())
  }
  callDocumentRole03ChangeSave(params: any): void {
    this.global.setLoading(true)
    this.DocumentProcessService.DocumentRole03ChangeSave(params).subscribe((data: any) => {
      this.global.setLoading(false)
      if (data) {
        this.tab_version_show_index.document_role03_receive_status_code = 'SAVE'
        this.onClickDocumentRole03ChangeVersionLoad(this.tab_version_show_index)
        //this.loadData(data)
        //this.document_role03_item_version_list = data
      }
    })
  }

  onClickDocumentRole03ChangeDocumentScan(): void {
    window.open(this.input_version.document_role03_item.consider_similar_document_file_id)
  }

  onClickDocumentRole03ChangeVersionKor120List(): void {
    this.input_version.document_role03_item.value_01 = this.input_version.document_role03_item.value_01 || ""
    this.input_version.document_role03_item.value_01 = this.input_version.document_role03_item.value_01.trim()

    this.master.documentRole02ChangeKor120TypeCodeList.forEach((item: any) => {
      if (this.popup.document_role03_change_kor_120_type[item.code])
        this.input_version.document_role03_item.value_01 += "\n" + item.name
    })

    //console.log(this.popup.document_role03_change_kor_120_type)

    this.input_version.document_role03_item.value_01 = this.input_version.document_role03_item.value_01.trim()
    this.popup.is_document_role03_change_kor_120_type_show = false
  }

  onClickDocumentRole03ChangeCertificationMarkScan(): void {
    var win = window.open(this.input.certification_mark_file_physical_path)
  }

  onClickDocumentRole03ChangeRepresentativeSearch(): void {
    var param = {
      name: this.popup.representative_name_search,
    }

    //this.popup.representative_name_search = "dd"

    console.log(this.popup)
    console.log(param)
    var params = this.help.GetFilterParams(param)
    console.log(params)
    params.paging = this.tableRepresentativeDuplicate.paginate
    console.log(params)
    this.DocumentProcessService.List("SaveAddressRepresentative", params).subscribe((data: any) => {
      console.log(data)
      this.tableRepresentativeDuplicate.SetDataList(data)
      //if (data && data.list && data.list.length > 0) {
      //    this.tableRequestGroup.Add({
      //        save_id: +this.editID,
      //        request_number: data.list[0].request_number,
      //        registration_number: data.list[0].registration_number,
      //        trademark_status_code: data.list[0].trademark_status_code,
      //        name: data.list[0].name,
      //    })
      //}
      this.global.setLoading(false)
    })
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input_version = data
    this.input_version.tab_edit_show = {}

    setTimeout(() => {
      this.help.Clone(data.view.contact_address || {}, this.contactAddress)
      //this.contactAddress = data.full_view.contact_address

      //console.log(this.tablePeople)
      //console.log(data.view.people_list)

      this.contactDefaultAddress.people_list = data.view.people_list
      this.contactDefaultAddress.representative_list = data.view.representative_list

      this.tablePeople.Set(data.view.people_list)
      this.tableRepresentative.Set(data.view.representative_list)
      this.tableProduct.Set(data.view.product_list)
      this.tableJoiner.Set(data.view.joiner_list)

      this.tableCertificationFile.Set(data.certification_file_list)
      this.tableRequestGroup.Set(data.request_group_list)
      this.tableHistory.Set(data.history_list)

      this.request_mark_feature_code_list = {}
      if (data.view.request_mark_feature_code_list) data.view.request_mark_feature_code_list.split(',').forEach((item: any) => { this.request_mark_feature_code_list[item] = 1 })
      //sound_description
      this.tableAllow.Set(data.allow_list);

      this.input_version.tab_edit_show = {
        owner: this.input_version.allow_list.filter(r => r.code == 'OWNER' || r.code == 'TRANSFER').length > 0,
        representative: this.input_version.allow_list.filter(r => r.code == 'REPRESENTATIVE' || r.code == 'TRANSFER').length > 0,
        contact: this.input_version.allow_list.filter(r => r.code == 'CONTACT').length > 0,
        request_item_type_01: this.input_version.allow_list.filter(r => r.code == 'REQUEST_ITEM_TYPE_01' || r.code == 'PRODUCT_DESCRIPTION').length > 0,

        //contact: this.input_version.allow_list.filter(r => r.code == 'CONTACT').length > 0,
        //contact: this.input_version.allow_list.filter(r => r.code == 'CONTACT').length > 0,
      }

      //console.log(this.input_version.tab_edit_show)
      if (this.input_version.tab_edit_show.owner) {
        this.tab_menu_show_index = 1
      } else if (this.input_version.tab_edit_show.representative) {
        this.tab_menu_show_index = 2
      } else if (this.input_version.tab_edit_show.contact) {
        this.tab_menu_show_index = 3
      } else if (this.input_version.tab_edit_show.request_item_type_01) {
        this.tab_menu_show_index = 4
      } else if (this.input_version.tab_edit_show.representative) {
        this.tab_menu_show_index = 2
      }

      //OWNER
      //REPRESENTATIVE
      //CONTACT
      //REQUEST_ITEM_TYPE_01
      //PRODUCT_DESCRIPTION

      //RULE
      //SOUND_TRANSLATE
      //TRADEMARK

      //console.log(data.allow_list)
    }, 100)
  }

  listData(data: any): void {
    this.listCheckingInstructionList = data.list || []
    this.listAddressList = data.list || []
    this.listCheckingInstructionDocumentList = data.list || []
    this.help.PageSet(data, this.paginateCheckingInstructionList)
    this.help.PageSet(data, this.paginateAddressList)
    this.help.PageSet(data, this.paginateCheckingInstructionDocumentList)

  }

  saveData(): any {
    let params = this.input_version

    params.view.contact_address = this.contactAddress

    params.view.request_mark_feature_code_list = []
    console.log(params.view)
    Object.keys(this.request_mark_feature_code_list).forEach((item: any) => {
      if (this.request_mark_feature_code_list[item] == 1) {
        params.view.request_mark_feature_code_list.push(item)
      }
    })
    params.view.request_mark_feature_code_list = params.view.request_mark_feature_code_list.join(',')


    params.save_id = +this.editID
    params.department_code = this.send_back.department_code
    params.reason = this.send_back.reason

    //this.input_version

    //params.view.contact_address = this.contactAddress

    //params.view.request_mark_feature_code_list = []
    ////console.log(params.view)
    //Object.keys(this.request_mark_feature_code_list).forEach((item: any) => {
    //  if (this.request_mark_feature_code_list[item] == 1) {
    //    params.view.request_mark_feature_code_list.push(item)
    //  }
    //})
    //params.view.request_mark_feature_code_list = params.view.request_mark_feature_code_list.join(',')



    //params.save_id = +this.editID
    //params.department_code = this.send_back.department_code
    //params.reason = this.send_back.reason

    return params
  }

  onClickUploadCSV(file: any): void {
    //let file = event.target.files[0];
    PapaParseCsvToJson(file, (results: any) => {
      let result = results.data;

      // Remove empty object
      //this.input.listProduct = this.input.listProduct.filter((item: any) => {
      //  return (
      //    item.request_item_sub_type_1_code != "" || item.description != ""
      //  );
      //});

      console.log(this.tableProduct.Get())
      var product_list = this.tableProduct.Get();
      // Add more
      result.forEach((item: any, index: any) => {
        //console.log(item)
        //console.log(index)
        if (index > 0) {
          var product = product_list.filter(r => r.request_item_sub_type_1_code == item[0])
          if (!product[0]) {
            product.push({
              request_item_sub_type_1_code: item[0],
              description: item[1],
            })
            this.tableProduct.Add(product[0])
          } else {
            product[0].description += "  " + item[1]
          }
        }
      });
    })
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }

    //console.log(object)
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

}
