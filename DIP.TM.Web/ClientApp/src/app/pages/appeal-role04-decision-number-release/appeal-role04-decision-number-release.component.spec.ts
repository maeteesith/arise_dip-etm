import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealRole04DecisionNumberReleaseComponent } from './appeal-role04-decision-number-release.component';

describe('AppealRole04DecisionNumberReleaseComponent', () => {
  let component: AppealRole04DecisionNumberReleaseComponent;
  let fixture: ComponentFixture<AppealRole04DecisionNumberReleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppealRole04DecisionNumberReleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealRole04DecisionNumberReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
