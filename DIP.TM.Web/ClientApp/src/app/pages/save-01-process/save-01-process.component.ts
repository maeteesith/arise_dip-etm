import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  clone,
} from '../../helpers'

@Component({
  selector: "app-save-01-process",
  templateUrl: "./save-01-process.component.html",
  styleUrls: ["./save-01-process.component.scss"]
})
export class Save01ProcessComponent implements OnInit {

  //TODO >>> Declarations <<<
  // Init
  public editID: number
  public input: any
  public validate: any
  public master: any
  public response: any
  public modal: any
  // List People
  public listPeople: any[]
  public paginatePeople: any
  public perPagePeople: number[]
  //public peopleItem: any
  // List Representative
  public listRepresentative: any[]
  public paginateRepresentative: any
  public perPageRepresentative: number[]
  //// List ContactAddress
  //public listContactAddress: any[]
  //public paginateContactAddress: any
  //public perPageContactAddress: number[]
  // List Product
  public listProduct: any[]
  public paginateProduct: any
  public perPageProduct: number[]
  // List Joiner
  public listJoiner: any[]
  public paginateJoiner: any
  public perPageJoiner: number[]
  // List SoundMark
  public listSoundMark: any[]
  public paginateSoundMark: any
  public perPageSoundMark: number[]
  // List Save010Case28
  public listSave010Case28: any[]
  public paginateSave010Case28: any
  public perPageSave010Case28: number[]
  // Autocomplete
  public autocompleteList: any
  public isShowAutocomplete: any
  // Other autocomplete
  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  public autocompleteListListCommercialAffairsProvince: any
  public is_autocomplete_ListCommercialAffairsProvince_show: any
  public is_autocomplete_ListCommercialAffairsProvince_load: any
  //public autocompleteListListLocation: any
  //public is_autocomplete_ListLocation_show: any
  //public is_autocomplete_ListLocation_load: any
  //public autocompleteListListModalLocation: any
  //public is_autocomplete_ListModalLocation_show: any
  //public is_autocomplete_ListModalLocation_load: any
  public autocompleteListListItemSubType1Code: any
  public is_autocomplete_ListItemSubType1Code_show: any
  public is_autocomplete_ListItemSubType1Code_load: any
  public autocompleteListListItemSubType2Code: any
  public is_autocomplete_ListItemSubType2Code_show: any
  public is_autocomplete_ListItemSubType2Code_load: any
  // Other modal data
  //public inputModalAddress: any
  //public inputModalAddressEdit: any
  //public inputAddress: any
  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any
  public is_representative: any

  // Other
  public timeout: any

  public product_select: any

  public request_mark_feature_code_list: any

  //public listAddress: any

  constructor(
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private help: Help,
    private SaveProcessService: SaveProcessService
  ) { }

  ngOnInit() {
    // Init

    this.editID = +this.route.snapshot.paramMap.get('id')
    this.input = {
      is_check_all: false,
      id: null,
      request_number: '',
      request_item_type_code: '',
      request_source_code: '',
      save_otop_type_code: '',
      evidence_address: '',
      request_date: '',
      total_price: '',
      commercial_affairs_province_name: '',
      otop_reference_number: '',
      irn_number: '',
      receive_date: getMoment(),
      address_country_code: '',
      trademark_role: '',
      index: '',
      address_type_code: '',
      address_card_type_code: '',
      address_receiver_type_code: '',
      name: '',
      house_number: '',
      address_sub_district_code: '',
      address_postal_code: '',
      telephone: '',
      email: '',
      card_number: '',
      address_sex_code: '',
      address_district_code: '',
      address_provice_code: '',
      fax: '',
    }
    this.validate = {}
    this.master = {
      requestSoundTypeList: [],
      requestItemTypeCodeList: [],
      requestSourceCodeList: [],
      saveOTOPTypeCodeList: [],
      addressCountryCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
    }
    this.response = {}
    this.modal = {
      isModalPeopleEditOpen: false,
    }
    // List People
    this.listPeople = []
    this.paginatePeople = CONSTANTS.PAGINATION.INIT
    this.perPagePeople = CONSTANTS.PAGINATION.PER_PAGE
    //this.peopleItem = {}
    // List Representative
    this.listRepresentative = []
    this.paginateRepresentative = CONSTANTS.PAGINATION.INIT
    this.perPageRepresentative = CONSTANTS.PAGINATION.PER_PAGE
    // List ContactAddress
    //this.listContactAddress = []
    //this.paginateContactAddress = CONSTANTS.PAGINATION.INIT
    //this.perPageContactAddress = CONSTANTS.PAGINATION.PER_PAGE
    // List Product
    this.listProduct = []
    this.paginateProduct = CONSTANTS.PAGINATION.INIT
    this.perPageProduct = CONSTANTS.PAGINATION.PER_PAGE
    // List Save010Case28
    this.listSave010Case28 = []
    this.paginateSave010Case28 = CONSTANTS.PAGINATION.INIT
    this.perPageSave010Case28 = CONSTANTS.PAGINATION.PER_PAGE
    // List Joiner
    this.listJoiner = []
    this.paginateJoiner = CONSTANTS.PAGINATION.INIT
    this.perPageJoiner = CONSTANTS.PAGINATION.PER_PAGE
    // List SoundMark
    this.listSoundMark = []
    this.paginateSoundMark = CONSTANTS.PAGINATION.INIT
    this.perPageSoundMark = CONSTANTS.PAGINATION.PER_PAGE
    // Autocomplete
    this.autocompleteList = {
      address_sub_district_name: []
    }
    this.isShowAutocomplete = {
      address_sub_district_name: false
    }
    // Other autocomplete
    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false
    this.autocompleteListListCommercialAffairsProvince = []
    this.is_autocomplete_ListCommercialAffairsProvince_show = false
    this.is_autocomplete_ListCommercialAffairsProvince_load = false
    //this.autocompleteListListLocation = []
    //this.is_autocomplete_ListLocation_show = false
    //this.is_autocomplete_ListLocation_load = false
    //this.autocompleteListListModalLocation = []
    //this.is_autocomplete_ListModalLocation_show = false
    //this.is_autocomplete_ListModalLocation_load = false
    // Other modal data
    //this.inputAddress = {}
    //this.inputModalAddress = {}


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.request_mark_feature_code_list = {}
    //TODO >>> Call service <<<
    this.callInit()

  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave01Process().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
        this.master.addressCountryCodeList.unshift({ "code": "", "name": "ไม่พบข้อมูล" });
      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save01Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            //Object.keys(data).forEach((item: any) => {
            //    this.input[item] = data[item]
            //})

            pThis.autocompleteChooseListNotSent(data)
          }
          // Close loading
          pThis.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }
    })
  }

  //! <<< Call API >>>
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    let pthis = this
    this.SaveProcessService.Save01ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pthis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else if (data.length == 0) {
          pthis.reset()
        } else {
          pthis.autocompleteListListNotSent = data
        }
      }
    })
    this.input.is_autocomplete_ListNotSent_load = false
  }
  callSave01CopyOld(params: any): void {
    this.global.setLoading(true)
    this.SaveProcessService.Save01List(params).subscribe((data: any) => {
      // if(isValidSave01CopyOldResponse(res)) {
      if (data && data.length > 0) {
        //this.help.Clone()
        this.listPeople = JSON.parse(JSON.stringify(data[0].people_list)) || []
        this.listPeople.forEach((item: any) => {
          delete item.id
          delete item.save_id
        })
        this.listRepresentative = JSON.parse(JSON.stringify(data[0].representative_list)) || []
        this.listRepresentative.forEach((item: any) => {
          delete item.id
          delete item.save_id
        })
        this.listProduct = JSON.parse(JSON.stringify(data[0].product_list)) || []
        this.listProduct.forEach((item: any) => {
          delete item.id
          delete item.save_id
        })
        this.listJoiner = JSON.parse(JSON.stringify(data[0].joiner_list)) || []
        this.listJoiner.forEach((item: any) => {
          delete item.id
          delete item.save_id
        })
        this.help.Clone(data[0].contact_address || {}, this.contactAddress)
        delete this.contactAddress.id
        delete this.contactAddress.save_id
        this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"
        this.input.trademark_role = data[0].trademark_role


        console.log(data[0].request_mark_feature_code_list)
        this.request_mark_feature_code_list = {}
        if (data[0].request_mark_feature_code_list) data[0].request_mark_feature_code_list.split(',').forEach((item: any) => {
          this.request_mark_feature_code_list[item] = 1
        })
        this.listSoundMark = []
        data[0].sound_mark_list = data[0].sound_mark_list || ""
        data[0].sound_mark_list.split(" ").forEach(item => this.listSoundMark.push({ "request_sound_type": item }))

        this.listSave010Case28 = JSON.parse(JSON.stringify(data[0].case28_list)) || []
        this.listSave010Case28.forEach((item: any) => {
          delete item.id
          delete item.save_id
        })

        ////this.inputAddress = data.contact_address || {}
        //this.listPeople = data.people_list || []
        //this.listRepresentative = data.representative_list || []
        //this.help.Clone(data.contact_address || {}, this.contactAddress)
        //this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"
        //this.listJoiner = data.joiner_list || []

        //this.listProduct = data.product_list || []
        //this.listSave010Case28 = data.case28_list || []
        //this.listSoundMark = []
        //data.sound_mark_list = data.sound_mark_list || ""
        //data.sound_mark_list.split(" ").forEach(item => this.listSoundMark.push({ "request_sound_type": item }))
        //this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
        //this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
        ////this.changePaginateTotal((this.listContactAddress || []).length, 'paginateContactAddress')
        //this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')
        //this.changePaginateTotal((this.listSave010Case28 || []).length, 'paginateSave010Case28')
        //this.changePaginateTotal((this.listJoiner || []).length, 'paginateJoiner')
        //this.changePaginateTotal((this.listSoundMark || []).length, 'paginateSoundMark')
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  callAutocompleteChangeListCommercialAffairsProvince(params: any): void {
    if (this.input.is_autocomplete_ListCommercialAffairsProvince_load) return
    this.input.is_autocomplete_ListCommercialAffairsProvince_load = true
    let pthis = this
    this.SaveProcessService.Save01ListCommercialAffairsProvince(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pthis.autocompleteChooseListCommercialAffairsProvince(data[0])
          }, 200)
        } else {
          this.autocompleteListListCommercialAffairsProvince = data
        }
      }
    })
    this.input.is_autocomplete_ListCommercialAffairsProvince_load = false
  }
  callSave01Madrid(params: any): void {
    this.SaveProcessService.Save01Madrid(params).subscribe((data: any) => {
      // if(isValidSave01MadridResponse(res)) {
      if (data) {
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  callAutocompleteChangeListItemSubType1Code(object: any, params: any): void {
    console.log(object)
    if (this.input.is_autocomplete_ListItemSubType1Code_load) return
    this.input.is_autocomplete_ListItemSubType1Code_load = true
    let pthis = this
    this.SaveProcessService.ListItemSubType1Code(params).subscribe((data: any) => {
      if (data) {
        setTimeout(function () {
          if (data.length == 0) {
            pthis.autocompleteChooseListItemSubType1Code(object, null)
          } else {
            pthis.autocompleteListListItemSubType1Code = data
          }
        }, 200)
      }
    })
    this.input.is_autocomplete_ListItemSubType1Code_load = false
  }
  callAutocompleteChangeListItemSubType2Code(object: any, params: any): void {
    if (this.input.is_autocomplete_ListItemSubType2Code_load) return
    this.input.is_autocomplete_ListItemSubType2Code_load = true
    let pthis = this
    this.SaveProcessService.ListItemSubType2Code(params).subscribe((data: any) => {
      if (data) {
        setTimeout(function () {
          if (data.length == 0) {
            pthis.autocompleteChooseListItemSubType2Code(object, null)
          } else {
            pthis.autocompleteListListItemSubType2Code = data
          }
        }, 200)
      }
    })
    this.input.is_autocomplete_ListItemSubType2Code_load = false
  }

  callSave01Delete(params: any): void {
    let pThis = this
    this.SaveProcessService.Save01Delete(params).subscribe((data: any) => {
      // if(isValidSave01DeleteResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)

      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }
  callSave01DocumentView(params: any): void {
    this.SaveProcessService.Save01DocumentView(params).subscribe((data: any) => {
      // if(isValidSave01DocumentViewResponse(res)) {
      if (data) {
        // Set value
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  callSave01Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save01Save(params).subscribe((data: any) => {
      // if(isValidSave01SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }
  callSave01Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save01Send(params).subscribe((data: any) => {
      // if(isValidSave01SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)

        this.automateTest.test(this, { request_number: data.request_number })
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  callSave01RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save01RequestBack(params).subscribe((data: any) => {
      // if(isValidSave01SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  callSave01PeopleDelete(params: any, ids: any[]): void {
    let pThis = this
    this.SaveProcessService.Save01PeopleDelete(params).subscribe((data: any) => {
      // if(isValidSave01PeopleDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < pThis.listPeople.length; i++) {
          if (pThis.listPeople[i].id == id) {
            pThis.listPeople.splice(i--, 1);
          }
        }
      });

      //for (let i = 0; i < pThis.listPeople.length; i++) {
      //  pThis.listPeople[i] = i + 1
      //}


      // Close loading
      pThis.global.setLoading(false)
    })
  }
  callSave01RepresentativeDelete(params: any, ids: any[]): void {
    let pThis = this
    this.SaveProcessService.Save01RepresentativeDelete(params).subscribe((data: any) => {
      // if(isValidSave01RepresentativeDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < pThis.listRepresentative.length; i++) {
          if (pThis.listRepresentative[i].id == id) {
            pThis.listRepresentative.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < pThis.listRepresentative.length; i++) {
        pThis.listRepresentative[i] = i + 1
      }


      // Close loading
      pThis.global.setLoading(false)
    })
  }
  callSave01JoinerDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save01JoinerDelete(params).subscribe((data: any) => {
      // if(isValidSave01JoinerDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listJoiner.length; i++) {
          if (this.listJoiner[i].id == id) {
            this.listJoiner.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listJoiner.length; i++) {
        this.listJoiner[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }
  callSave01SoundMarkDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save01SoundMarkDelete(params).subscribe((data: any) => {
      // if(isValidSave01SoundMarkDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listSoundMark.length; i++) {
          if (this.listSoundMark[i].id == id) {
            this.listSoundMark.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listSoundMark.length; i++) {
        this.listSoundMark[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }

  //! <<< Prepare Call API >>>
  onClickSave01CopyOld(): void {
    var rs = prompt("กรุณาใส่เลขที่คำขอ");
    if (rs && rs != "") {
      const params = {
        request_number: rs
      }

      this.callSave01CopyOld(params)
    }
  }
  onClickSave01Madrid(): void {
    //if(this.validateSave01Madrid()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callSave01Madrid(params)
    //}
  }
  onClickSave01Delete(): void {
    //if(this.validateSave01Delete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callSave01Delete(params)
    //}
  }
  onClickSave01DocumentView(): void {
    window.open('/File/RequestDocumentCollect/010/' + this.input.id)
  }
  onClickSave01Save(): void {
    //if (this.validateSave01Save()) {
    // Open loading
    this.global.setLoading(true)
    // Set param

    // Call api
    var pThis = this
    setTimeout(function () {
      pThis.callSave01Save(pThis.saveData())
    }, 200)
    //}
  }
  onClickSave01Send(): void {
    //if(this.validateSave01Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave01Send(this.saveData())
    //}
  }
  onClickSave01RequestBack(): void {
    //if(this.validateSave01Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave01RequestBack(this.saveData())
    //}
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }
  validateSave01CopyOld(): boolean {
    let result = validateService('validateSave01CopyOld', this.input)
    this.validate = result.validate
    return result.isValid
  }
  validateSave01Madrid(): boolean {
    let result = validateService('validateSave01Madrid', this.input)
    this.validate = result.validate
    return result.isValid
  }
  validateSave01Delete(): boolean {
    let result = validateService('validateSave01Delete', this.input)
    this.validate = result.validate
    return result.isValid
  }
  validateSave01DocumentView(): boolean {
    let result = validateService('validateSave01DocumentView', this.input)
    this.validate = result.validate
    return result.isValid
  }
  validateSave01Save(): boolean {
    let result = validateService('validateSave01Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  validateSave01Send(): boolean {
    let result = validateService('validateSave01Save', this.input)
    this.validate = result.validate
    return result.isValid
  }

  //! <<< Event >>>
  onClickProductDelete(): void {
    if (this.listProduct.filter(r => r.is_check).length > 0) {
      var rs = confirm("คุณต้องการลบรายการ?")
      if (rs) {
        for (let i = 0; i < this.listProduct.length; i++) {
          if (this.listProduct[i].is_check) {
            if (this.listProduct[i].id) {
              this.listProduct[i].is_deleted = true
            } else {
              this.listProduct.splice(i--, 1);
            }
          }
        }
        console.log(this.listProduct)

        //for (let i = 0; i < this.listProduct.length; i++) {
        //  this.listProduct[i].index = i + 1
        //}
      }
    }
  }

  onClickSave010Case28Delete(item: any = null): void {
    if (item) item.is_check = true
    if (this.listSave010Case28.filter(r => r.is_check).length > 0) {
      var rs = confirm("คุณต้องการลบรายการ?")
      if (rs) {
        for (let i = 0; i < this.listSave010Case28.length; i++) {
          if (this.listSave010Case28[i].is_check) {
            if (this.listSave010Case28[i].id) {
              this.listSave010Case28[i].is_deleted = true
            } else {
              this.listSave010Case28.splice(i--, 1);
            }
          }
        }
      }
    }
  }

  saveData(): any {
    const params = this.input
    //params.contact_address = this.inputAddress
    params.people_list = this.listPeople
    params.representative_list = this.listRepresentative
    params.contact_address = this.contactAddress
    params.case28_list = this.listSave010Case28
    params.joiner_list = this.listJoiner
    params.sound_mark_list = this.listSoundMark.map(item => item.request_sound_type).join(" ")


    params.product_list = []
    this.listProduct.forEach((item: any) => {
      item.description.split("  ").forEach((product: any) => {
        params.product_list.push({
          request_item_sub_type_1_code: item.request_item_sub_type_1_code,
          description: product,
        })
      })
    })

    params.request_mark_feature_code_list = []
    Object.keys(this.request_mark_feature_code_list).forEach((item: any) => {
      if (this.request_mark_feature_code_list[item] == 1) {
        params.request_mark_feature_code_list.push(item)
      }
    })
    params.request_mark_feature_code_list = params.request_mark_feature_code_list.join(',')

    //console.log(this.listSoundMark)
    //console.log(this.listSoundMark.map(item => item.request_sound_type))
    console.log(params.sound_mark_list)
    return params
  }

  onClickSave01PeopleAdd(): void {
    this.listPeople.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listPeople[this.listPeople.length - 1])
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
  }
  onClickSave01RepresentativeAdd(): void {
    this.listRepresentative.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listRepresentative[this.listRepresentative.length - 1], 'ตัวแทน')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
  }
  onClickSave01JoinerAdd(): void {
    this.listJoiner.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listJoiner[this.listJoiner.length - 1], 'ผู้ร่วมใช้')
    this.changePaginateTotal(this.listJoiner.length, 'paginateJoiner')
  }

  onClickAddressCopy(item_list: any[], copy_item: any): void {
    var index = 1
    item_list.forEach((item: any) => {
      item.index = index++
      if (item === copy_item) {
        var item_new = { ...item }
        item_new.id = null
        item_new.index = index++
        item_list.splice(index + 1, 0, item_new)
      }
    })
  }
  onClickAddressEdit(item: any, name: string = "เจ้าของ"): void {
    this.modalPeopleText = name
    this.modalAddressEdit = item
    this.help.Clone(item, this.modalAddress)

    this.modalAddress.is_representative = name.indexOf("ตัวแทน") >= 0

    this.modal["isModalPeopleEditOpen"] = true
  }
  onClickAddressSave(): void {
    this.help.Clone(this.modalAddress, this.modalAddressEdit)
    this.help.GetAddressInformation(this.modalAddressEdit)

    this.modal["isModalPeopleEditOpen"] = false
  }
  onClickAddressDelete(item_list: any[], item: any): void {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (item_list.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        item_list.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = item_list.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < item_list.length; i++) {
          if (item_list[i].is_check) {
            //item_list[i].cancel_reason = rs
            item_list[i].is_deleted = true
            //console.log(item_list[i])
            //item_list[i].status_code = "DELETE"

            if (true && item_list[i].id) ids.push(item_list[i].id)
            else item_list.splice(i--, 1);
          }
        }
      }
    }
    //console.log(item_list)
    this.global.setLoading(false)
  }

  onClickCheckBoxTable2(object: any, value: boolean): void {
    //if (isAll) {
    //console.log(value)
    this[object].forEach((item: any) => {
      item.is_check = value
      //console.log(item)
    })
    //} else {
    //  object.forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } })
    //}
  }
  onClickCheckBoxTable3(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_3_check_all = !this.input.is_table_3_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_3_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onClickSave01ProductAdd(): void {
    this.listProduct.push({
      index: this.listProduct.length + 1,
      request_item_sub_type_1_code: null,
      request_item_sub_type_2_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
  }
  onClickSave010Case28Add(): void {
    this.listSave010Case28.push({
      //index: this.listSave010Case28.length + 1,
      //request_item_sub_type_1_code: null,
      //request_item_sub_type_2_code: null,
      //description: null,
    })
    this.changePaginateTotal(this.listSave010Case28.length, 'paginateSave010Case28')
  }
  onClickCheckBoxTable5(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_5_check_all = !this.input.is_table_5_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_5_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxProduct(object: any, object_name: string, value: any): void {
    object[object_name] = !object[object_name]
  }
  onChangeSelectProduct(object: any, object_name: string, value: any): void {
    console.log(object)
    if (object_name.indexOf("price") >= 0) {
      object[object_name] = parseFloat(value)
    } else {
      object[object_name] = value
    }
  }
  onClickCheckBoxTable7(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_7_check_all = !this.input.is_table_7_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_7_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxJoiner(object: any, object_name: string, value: any): void {
    object[object_name] = !object[object_name]
  }
  onChangeSelectJoiner(object: any, object_name: string, value: any): void {
    if (object_name.indexOf("price") >= 0) {
      object[object_name] = parseFloat(value)
    } else {
      object[object_name] = value
    }
  }
  onClickSave01SoundMarkAdd(): void {
    this.listSoundMark.push({
      index: this.listSoundMark.length + 1,
      request_sound_type: this.master.requestSoundTypeList[0].code,

    })
    this.changePaginateTotal(this.listSoundMark.length, 'paginateSoundMark')
  }
  onClickSave01SoundMarkDelete(item: any): void {
    // if(this.validateSave01SoundMarkDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listSoundMark.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listSoundMark.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (false) {
        delete_save_item_count = this.listSoundMark.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < this.listSoundMark.length; i++) {
          if (this.listSoundMark[i].is_check) {
            this.listSoundMark[i].cancel_reason = rs
            this.listSoundMark[i].status_code = "DELETE"

            if (false && this.listSoundMark[i].id) ids.push(this.listSoundMark[i].id)
            else this.listSoundMark.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave01SoundMarkDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listSoundMark.length; i++) {
          this.listSoundMark[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  onClickCheckBoxTable8(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_8_check_all = !this.input.is_table_8_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_8_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxSoundMark(object: any, object_name: string, value: any): void {
    object[object_name] = !object[object_name]
  }
  onChangeSelectSoundMark(object: any, object_name: string, value: any): void {
    if (object_name.indexOf("price") >= 0) {
      object[object_name] = parseFloat(value)
    } else {
      object[object_name] = value
    }
  }

  //! <<< Autocomplete (NotSent) >>>
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: { request_number: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    setTimeout(function () {
      this.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  autocompleteChooseListNotSent(data: any): void {
    this.help.Clone(data, this.input)

    //this.input = data
    this.request_mark_feature_code_list = {}
    if (this.input.request_mark_feature_code_list) this.input.request_mark_feature_code_list.split(',').forEach((item: any) => { this.request_mark_feature_code_list[item] = 1 })

    //this.inputAddress = data.contact_address || {}
    this.listPeople = data.people_list || []
    this.listRepresentative = data.representative_list || []
    this.help.Clone(data.contact_address || {}, this.contactAddress)
    this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"
    this.listJoiner = data.joiner_list || []

    data.product_list = data.product_list || []
    this.listProduct = []
    data.product_list.filter(r => !r.is_deleted).forEach((item: any) => {
      var product_list = this.listProduct.filter(r => r.request_item_sub_type_1_code == item.request_item_sub_type_1_code)
      if (product_list.length > 0) product_list[0].description += "  " + item.description
      else this.listProduct.push({
        request_item_sub_type_1_code: item.request_item_sub_type_1_code,
        description: item.description,
      })
    })

    this.listSave010Case28 = data.case28_list || []
    this.listSoundMark = []
    data.sound_mark_list = data.sound_mark_list || ""
    data.sound_mark_list.split(" ").forEach(item => this.listSoundMark.push({ "request_sound_type": item }))
    this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
    this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
    //this.changePaginateTotal((this.listContactAddress || []).length, 'paginateContactAddress')
    this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')
    this.changePaginateTotal((this.listSave010Case28 || []).length, 'paginateSave010Case28')
    this.changePaginateTotal((this.listJoiner || []).length, 'paginateJoiner')
    this.changePaginateTotal((this.listSoundMark || []).length, 'paginateSoundMark')

    this.is_autocomplete_ListNotSent_show = false
  }

  //! <<< Autocomplete (CommercialAffairsProvince) >>>
  autocompleteChangeListCommercialAffairsProvince(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListCommercialAffairsProvince_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListCommercialAffairsProvince(params)
    }
  }
  autocompleteBlurListCommercialAffairsProvince(object: any, name: any, item_per_page: any = 5): void {
    setTimeout(function () {
      this.is_autocomplete_ListCommercialAffairsProvince_show = false
    }, 200)
  }
  autocompleteChooseListCommercialAffairsProvince(data: any): void {
    this.input.commercial_affairs_province_name = data.name
    this.input.commercial_affairs_province_code = data.code

    this.is_autocomplete_ListCommercialAffairsProvince_show = false
  }

  //! <<< Autocomplete (ItemSubType1Code) >>>
  autocompleteChangeListItemSubType1Code(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    console.log(object)
    if (object[name].length >= length) {
      this.is_autocomplete_ListItemSubType1Code_show = object.index

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListItemSubType1Code(object, params)
    }
  }
  autocompleteChooseListItemSubType1Code(object: any, data: any): void {
    object.request_item_sub_type_1_code = data ? data.code : ""

    this.is_autocomplete_ListItemSubType1Code_show = false
  }

  //! <<< Autocomplete (ItemSubType2Code) >>>
  autocompleteChangeListItemSubType2Code(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListItemSubType2Code_show = object.index

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListItemSubType2Code(object, params)
    }
  }
  autocompleteChooseListItemSubType2Code(object: any, data: any): void {
    object.request_item_sub_type_2_code = data ? data.code : ""

    this.is_autocomplete_ListItemSubType2Code_show = false
  }


  onClickRequestMarkFeatureCode(code, value: boolean): void {
    if (value) {
      this.request_mark_feature_code_list[code] = 1
    } else {
      this.request_mark_feature_code_list[code] = 0
    }
  }

  onClickProductSelect(row_item: any, is_selected = true) {
    if (is_selected) {
      var pThis = this
      setTimeout(function () {
        row_item.description = row_item.description.replace(/  /g, '\n').trim()
        pThis.product_select = row_item
      }, 100)
    } else {
      if (this.product_select == row_item) {
        row_item.description = row_item.description.replace(/\n/g, '  ').trim()
        this.product_select = null
      }
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name]
  }

  //! <<< Autocomplete >>>
  //onChangeAutocomplete(obj: any, name: any): void {
  //  this.validate[name] = null
  //  clearTimeout(this.timeout)
  //  this.timeout = setTimeout(() => {
  //    if (this[obj][name]) {
  //      if (name === 'address_sub_district_name') {
  //        this.callAutocompleteChangeListModalLocation({
  //          filter: { name: this[obj][name] },
  //          paging: {
  //            item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page
  //          }
  //        }, name)
  //      }
  //    } else {
  //      this.onClickOutsideAutocomplete(name)
  //    }
  //  }, CONSTANTS.DELAY_CALL_API)
  //}
  //onSelectAutocomplete(name: any, item: any): void {
  //  if (name === 'address_sub_district_name') {
  //    this.inputModalAddress.postal_code = item.postal_code
  //    this.inputModalAddress.postal_name = item.postal_name
  //    this.inputModalAddress.address_sub_district_code = item.code
  //    this.inputModalAddress.address_sub_district_name = item.name
  //    this.inputModalAddress.address_district_code = item.district_code
  //    this.inputModalAddress.address_district_name = item.district_name
  //    this.inputModalAddress.address_province_code = item.province_code
  //    this.inputModalAddress.address_province_name = item.province_name
  //    this.inputModalAddress.address_country_code = item.country_code
  //    this.inputModalAddress.address_country_name = item.country_name
  //    //this.is_autocomplete_ListModalLocation_show = false
  //  }
  //  this.onClickOutsideAutocomplete(name)
  //}
  //onClickOutsideAutocomplete(name: any): void {
  //  this.isShowAutocomplete[name] = false
  //  this.autocompleteList[name] = []
  //}

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    if (!this.modal[name]) { // Is open
      this.modal[name] = true
    } else { // Is close
      this.modal[name] = false
    }
  }

  //! <<< Other >>>
  reset(): void {
    this.global.setLoading(true)
    this.ngOnInit()
    this.global.setLoading(false)
  }
}
