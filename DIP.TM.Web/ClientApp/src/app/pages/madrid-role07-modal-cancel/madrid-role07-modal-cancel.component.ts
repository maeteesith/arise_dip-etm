import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-madrid-role07-modal-cancel',
  templateUrl: './madrid-role07-modal-cancel.component.html',
  styleUrls: ['./madrid-role07-modal-cancel.component.scss']
})
export class MadridRole07ModalCancelComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<MadridRole07ModalCancelComponent>) { }

  ngOnInit() {
  }

  closeModal() {
    this.dialogRef.close();
  }

}
