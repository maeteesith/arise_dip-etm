import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { DomSanitizer } from '@angular/platform-browser'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role04-check-list",
  templateUrl: "./public-role04-check-list.component.html",
  styleUrls: ["./public-role04-check-list.component.scss"]
})
export class PublicRole04CheckListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole04Check
  public listPublicRole04Check: any[]
  public paginatePublicRole04Check: any
  public perPagePublicRole04Check: number[]
  // Response PublicRole04Check
  public responsePublicRole04Check: any
  public listPublicRole04CheckEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public publicRole04ReceiveStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  public popup: any
  public rowEdit: any
  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //public_role04_receive_start_date: getMoment(),
      //public_role04_receive_end_date: getMoment(),
      book_index: '',
      public_role04_receive_status_code: '',
      request_number: '',
    }
    this.listPublicRole04Check = []
    this.paginatePublicRole04Check = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole04Check.id = 'paginatePublicRole04Check'
    this.perPagePublicRole04Check = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      publicRole04ReceiveStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.popup = {}
    this.rowEdit = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole04CheckList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.publicRole04ReceiveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.public_role04_receive_status_code = "NOT_YET"
      }
      if (this.editID) {
        this.PublicProcessService.PublicRole04CheckLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

      //this.onClickPublicRole04CheckList()
    })
  }

  constructor(
    public sanitizer: DomSanitizer,
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }

  onClickPublicRole04CheckList(): void {
    // if(this.validatePublicRole04CheckList()) {
    // Open loading
    // Call api
    this.callPublicRole04CheckList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole04CheckList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole04CheckList(this.help.GetFilterParams(params, this.paginatePublicRole04Check)).subscribe((data: any) => {
      // if(isValidPublicRole04CheckListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })

        //if (this.listPublicRole04Check.length > 0) this.onClickPublicRole04CheckEdit(this.listPublicRole04Check[0])
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickPublicRole04CheckSend(row_item: any = null): void {
    //if(this.validatePublicRole04CheckSend()) {
    // Open loading
    this.popup.is_product_show = false

    this.global.setLoading(true)
    // Call api
    if (row_item) {
      //console.log(row_item.is_check)
      this.listPublicRole04Check.forEach((item: any) => {
        item.is_check = item.id == row_item.id
      })
      //row_item.is_check = true
      //console.log(this.listPublicRole04Check)
    }
    this.callPublicRole04CheckSend(this.listPublicRole04Check)
    //}
  }
  validatePublicRole04CheckSend(): boolean {
    let result = validateService('validatePublicRole04CheckSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole04CheckSend(params: any): void {
    this.PublicProcessService.PublicRole04CheckSend(params).subscribe((data: any) => {
      // if(isValidPublicRole04CheckSendResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.onClickPublicRole04CheckList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickPublicRole04CheckAdd(): void {
    this.listPublicRole04Check.push({
      index: this.listPublicRole04Check.length + 1,
      request_number: null,
      book_start_date_text: null,
      book_end_date_text: null,
      book_index: null,
      request_item_sub_type_1_code_text: null,
      public_role05_by_name: null,
      public_role05_date_text: null,
      public_role04_remark: null,
      public_role04_receive_status_name: null,

    })
    this.changePaginateTotal(this.listPublicRole04Check.length, 'paginatePublicRole04Check')
  }

  onClickPublicRole04CheckEdit(item: any): void {
    window.open("/public-role04-check/edit/" + item.id)

    //var params = {
    //  id: item.id.toString()
    //}
    //this.global.setLoading(true)
    //this.PublicProcessService.SaveList(this.help.GetFilterParams(params)).subscribe((data: any) => {
    //  this.popup.is_product_show = true
    //  if (data && data.list) {
    //      this.rowEdit = data.list[0]

    //      this.rowEdit.pdf_url = this.sanitizer.bypassSecurityTrustResourceUrl('/pdf/KorMor01/' + this.rowEdit.request_number)
    //  }
    //  this.global.setLoading(false)
    //})
  }

  onClickPublicRole04CheckSendBack(): void {
    this.popup.is_product_show = false
    this.popup.is_send_back_show = true

    this.popup.department_code = this.master.publicRole04SendBackDepartmentList[0].code
    this.popup.reason = ""
  }


  onClickPublicRole04DocumentSendChange(): void {
    //if(this.validatePublicRole04DocumentSendChange()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    var params = {
      save_id: this.rowEdit.id,
      department_code: this.popup.department_code,
      reason: this.popup.reason,
    }
    this.callPublicRole04DocumentSendChange(params)
    //}
  }
  validatePublicRole04DocumentSendChange(): boolean {
    let result = validateService('validatePublicRole04DocumentSendChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole04DocumentSendChange(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole04DocumentSendChange(params).subscribe((data: any) => {
      // if(isValidPublicRole04DocumentSendChangeResponse(res)) {
      if (data) {
        // Set value
        this.popup.is_send_back_show = false
        this.onClickPublicRole04CheckList()
        //window.open("/public-role01-check/list", "_self")
        //this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }



  onClickPublicRole04CheckDelete(item: any): void {
    // if(this.validatePublicRole04CheckDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listPublicRole04Check.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listPublicRole04Check.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listPublicRole04Check.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listPublicRole04Check.length; i++) {
          if (this.listPublicRole04Check[i].is_check) {
            this.listPublicRole04Check[i].cancel_reason = rs
            this.listPublicRole04Check[i].status_code = "DELETE"
            this.listPublicRole04Check[i].is_deleted = true

            if (true && this.listPublicRole04Check[i].id) ids.push(this.listPublicRole04Check[i].id)
            // else this.listPublicRole04Check.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callPublicRole04CheckDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listPublicRole04Check.length; i++) {
          this.listPublicRole04Check[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callPublicRole04CheckDelete(params: any, ids: any[]): void {
    this.PublicProcessService.PublicRole04CheckDelete(params).subscribe((data: any) => {
      // if(isValidPublicRole04CheckDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listPublicRole04Check.length; i++) {
          if (this.listPublicRole04Check[i].id == id) {
            this.listPublicRole04Check.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listPublicRole04Check.length; i++) {
        this.listPublicRole04Check[i] = i + 1
      }

      this.onClickPublicRole04CheckList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicRole04Check = data.publicrole04check_list || []
    this.changePaginateTotal(this.listPublicRole04Check.length, 'paginatePublicRole04Check')

  }

  listData(data: any): void {
    this.listPublicRole04Check = data.list || []
    this.help.PageSet(data, this.paginatePublicRole04Check)
    //this.changePaginateTotal(this.listPublicRole04Check.length, 'paginatePublicRole04Check')

  }

  saveData(): any {
    // let params = this.input
    // params.publicrole04check_list = this.listPublicRole04Check || []

    const params = {
      public_role04_receive_start_date: this.input.public_role04_receive_start_date,
      public_role04_receive_end_date: this.input.public_role04_receive_end_date,
      book_index: this.input.book_index,
      public_role04_receive_status_code: this.input.public_role04_receive_status_code,
      request_number: this.input.request_number,
      //page_index: +this.paginatePublicRole04Check.currentPage,
      //item_per_page: +this.paginatePublicRole04Check.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'public_role04_receive_date',
      //  value: displayDateServer(this.input.public_role04_receive_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_role04_receive_date',
      //  value: displayDateServer(this.input.public_role04_receive_end_date),
      //  operation: 4
      //}, {
      //  key: 'book_index',
      //  value: this.input.book_index,
      //  operation: 0
      //}, {
      //  key: 'public_role04_receive_status_code',
      //  value: this.input.public_role04_receive_status_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}]
    }

    return params
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicRole04Check') {
      this.onClickPublicRole04CheckList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
