import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'
import { observable } from 'rxjs';

@Component({
  selector: "app-checking-similar-word",
  templateUrl: "./checking-similar-word.component.html",
  styleUrls: ["./checking-similar-word.component.scss"]
})
export class CheckingSimilarWordComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List Trademark
  public listTrademark: any[]
  public paginateTrademark: any
  public perPageTrademark: number[]
  // Response Trademark
  public responseTrademark: any
  public listTrademarkEdit: any

  public checkingTagSimilarMethod: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  public player: any
  public player_action: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      name: '',
      request_item_sub_type_1_code_text: '',
      request_item_sub_type_description: '',
      sound_description: '',
      is_checking_word_first_last: false,
      is_checking_word_mark: false,
      is_checking_word_last_other: false,
      is_checking_word_syllable_sound: false,
      is_checking_sound: false,
      is_checking_smell: false,
      full_view: {},
    }
    this.listTrademark = []
    this.paginateTrademark = CONSTANTS.PAGINATION.INIT
    this.perPageTrademark = CONSTANTS.PAGINATION.PER_PAGE

    this.checkingTagSimilarMethod = {}


    //Master List
    this.master = {
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initCheckingSimilarWord().subscribe((data: any) => {
      if (data) {
        this.master = data

      }
      if (this.editID) {
        let pThis = this
        this.CheckingProcessService.CheckingSimilarWordLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
            pThis.onClickCheckingSimilarSave010List()
          }
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }
    })
  }


  onClickCheckingSimilarSave010List(): void {
    // if(this.validateSave021List()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingSimilarSave010List()
    // }
  }
  //! <<< Call API >>>
  callCheckingSimilarSave010List(): void {
    var column_list = [
      "search_1_word_first_code",
      "search_1_word_sound_last_code",
      "search_1_word_mark",
      "search_1_word_sound_last_other_code",
      "search_1_2_word_syllable_sound",

      "search_1_request_number_start",
      "search_1_request_number_end",
      "search_1_people_name",
      "search_1_request_item_sub_type_1_code",
      "search_1_request_item_sub_type_1_description_text",
      "search_1_1_word_samecondition_code",
      "search_1_1_word_mark",
      "search_1_2_word_samecondition_code",
      "search_1_document_classification_image_code",
      "search_1_document_classification_image_condition",
      "search_1_document_classification_sound_code",
      "search_1_document_classification_sound_condition",
    ]
    this.route.queryParams.subscribe(params => {
      let param = {}
      console.log('params', params)
      this.input.is_have_sound = params.is_have_sound

      let pThis = this

      param["request01_item_id"] = parseInt(this.editID)
      Object.keys(params).forEach((item: any) => {
        if (column_list.indexOf(item) >= 0)
          param[item] = params[item]
      })
      param["display_type"] = params.display_type;
      this.paginateTrademark.page_index = this.paginateTrademark.page_index || 1
      param["paginate"] = this.paginateTrademark

      this.CheckingProcessService.CheckingSimilarSave010List(param).subscribe((data: any) => {
        if (data) {
          this.listTrademark = data.list
          this.help.PageSet(data, this.paginateTrademark)

          this.listTrademark.forEach((item: any) => {
            //console.log(item.full_view.product_list)
            item.request_item_sub_type_1_code_text = item.request_item_sub_type_1_code_text || ""
            item.request_item_sub_type_1_code_text = item.request_item_sub_type_1_code_text.replace(/\|/g, " ").trim()
            //item.product_text = item.request_item_sub_type_1_code_text.split("|").map(function (item) {
            //  //  if (param["search_1_request_item_sub_type_1_code"]) {
            //  //    //  console.log(param["search_1_request_item_sub_type_1_code"])
            //  //    var code_list = param["search_1_request_item_sub_type_1_code"].split(" ")
            //  //    console.log(code_list)
            //  //    for (var i = 0; i < code_list.length; i++) {
            //  //      if (code_list[i] == item.request_item_sub_type_1_code) {
            //  //        return "<font color='red'>" + item.request_item_sub_type_1_code + "</font>"
            //  //      }
            //  //    }
            //  //    return item.request_item_sub_type_1_code
            //  //  }
            //  return item
            //}).join(", ")

            item.word_mark_text = item.word_mark_text || ""
            item.word_mark = item.word_mark_text.substr(1).split("|").map(function (item) {
              if (param["search_1_1_word_mark"]) {
                if (item.indexOf(param["search_1_1_word_mark"]) >= 0) {
                  return item.replace(param["search_1_1_word_mark"], "<font color='red'>" + param["search_1_1_word_mark"] + "</font>")
                }
              }
              return item
            }).join("<br>")

            item.name_text = item.name_text || ""
            item.people_text = item.name_text.substr(1).trim().split("|").join("<br>")

            item.document_classification_sound_name_text = item.document_classification_sound_name_text || ""
            item.soud_text = item.document_classification_sound_name_text.substr(1).trim().split("|").join("<br>")

            //item.soud_text = item.full_view.document_classification_sound_list.map(function (item) {
            //  if (param["search_1_document_classification_sound_code"]) {
            //    var search_1_document_classification_sound_code = "|" + param["search_1_document_classification_sound_code"] + "|"
            //    if (search_1_document_classification_sound_code.indexOf("|" + item.document_classification_sound_code + "|") >= 0) {
            //      return "<font color='red'>" + item.document_classification_sound_name + "</font>"
            //    }
            //  }
            //  return item.document_classification_sound_name
            //}).join("<br>")
          })
        }


        this.CheckingProcessService.CheckingTagSimilarMethod(param).subscribe((data: any) => {
          if (data) {
            //console.log(data)
            // Manage structure
            this.checkingTagSimilarMethod = data
            //this.listTrademark.forEach((item: any) => {
            //  item.word_mark = item.full_view.document_classification_word_list.map(function (item) { return item.word_mark }).join(", ")
            //})
          }
          // Close loading
          pThis.global.setLoading(false)
          this.automateTest.test(this)
        })
      })
    })
  }



  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }


  onClickCheckingSimilarWordSave(): void {
    //if(this.validateCheckingSimilarWordSave()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingSimilarWordSave(this.saveData())
    //}
  }
  validateCheckingSimilarWordSave(): boolean {
    let result = validateService('validateCheckingSimilarWordSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarWordSave(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingSimilarWordSave(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarWordSaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingSimilarWordPrint(): void {
    //if(this.validateCheckingSimilarWordPrint()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callCheckingSimilarWordPrint(this.saveData())
    //}
  }
  validateCheckingSimilarWordPrint(): boolean {
    let result = validateService('validateCheckingSimilarWordPrint', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarWordPrint(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingSimilarWordPrint(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarWordPrintResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingSimilarWordBack(): void {
    var win = window.open("./checking-similar/edit/" + this.editID, "_self")
  }
  validateCheckingSimilarWordBack(): boolean {
    let result = validateService('validateCheckingSimilarWordBack', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCheckingSimilarWordBack(params: any): void {
    let pThis = this
    this.CheckingProcessService.CheckingSimilarWordBack(params).subscribe((data: any) => {
      // if(isValidCheckingSimilarWordBackResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCheckingSimilarWordTrademarkAdd(): void {
    this.listTrademark.push({
      index: this.listTrademark.length + 1,
      request_number: null,
      trademark_status_code: null,
      request_item_sub_type_description: null,
      word_mark: null,
      name: null,
      sound_link: null,

    })
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')
  }

  onClickCheckingBack(): void {
    window.close()
    //var win = window.open("./checking-similar/edit/" + this.editID, "_self")
  }

  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  onClickCheckingSimilarWordCompare(row_item: any): void {
    var win = window.open("./checking-similar-compare/edit/" + this.editID + "?compare_id=" + row_item.id)
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    //this.input.full_view.document_classification_search_method.forEach((item: any) => {
    //  this.input["is_checking_" + item.search_method] = true;
    //})

    this.listTrademark = data.trademark_list || []
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')

    //if (this.input.sound_file_physical_path && this.input.sound_file_physical_path != '') {
    //  this.player = new Audio(this.input.sound_file_physical_path)
    //  this.player_action = null
    //}
  }

  togglePlayer(item: any = null): void {
    if (!item) {
      item = this.input
    }

    if (!item.player) {
      item.player = new Audio("File/Content/" + item.sound_file_id)
      item.player_action = null
    }

    if (item.player_action) {
      item.player.pause()
    } else {
      item.player.play()
    }
    item.player_action = !item.player_action
  }


  listData(data: any): void {
    this.listTrademark = data || []
    this.changePaginateTotal(this.listTrademark.length, 'paginateTrademark')

  }

  saveData(): any {
    let params = this.input

    params.trademark_list = this.listTrademark || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataBindingSave(name: any, value: any, object: any): void {
    this.route.queryParams.subscribe(_params => {
      //let param = {}
      var params = [this.editID, object.id, value, Object.keys(_params)]

      this.CheckingProcessService.CheckingSimilarTagAdd(params).subscribe((data: any) => {
        console.log(data)
        this.automateTest.test(this, { TrademarkAdd: 1 })
      })
    })
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => {
      item.is_check = value
      this.oneWayDataBindingSave('is_check', value, item)
    })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
