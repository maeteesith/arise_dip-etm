import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  displayMoney,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role01-item-list",
  templateUrl: "./public-role01-item-list.component.html",
  styleUrls: ["./public-role01-item-list.component.scss"]
})
export class PublicRole01ItemListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicItem
  public listPublicItem: any[]
  public paginatePublicItem: any
  public perPagePublicItem: number[]
  // Response PublicItem
  public responsePublicItem: any
  public listPublicItemEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public publicTypeCodeList: any[]
  public publicSourceCodeList: any[]
  public publicStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListPublicReceiverList: any
  public is_autocomplete_PublicReceiverList_show: any
  public is_autocomplete_PublicReceiverList_load: any
  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      public_type_code: '',
      public_source_code: '',
      public_receiver_by: '',
      public_status_code: '',
      request_number: '',
    }
    this.listPublicItem = []
    this.paginatePublicItem = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicItem.id = 'paginatePublicItem'
    this.perPagePublicItem = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      publicTypeCodeList: [],
      publicSourceCodeList: [],
      publicStatusCodeList: [],
    }
    //Master List

    this.autocompleteListPublicReceiverList = []
    this.is_autocomplete_PublicReceiverList_show = false
    this.is_autocomplete_PublicReceiverList_load = false


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole01ItemList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.publicTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.public_type_code = "ALL"
        this.master.publicSourceCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.public_source_code = "ALL"
        this.master.publicStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.public_status_code = "ALL"

      }
      if (this.editID) {
        this.PublicProcessService.PublicRole01ItemLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }


  // Autocomplete
  autocompleteChangePublicReceiverList(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_PublicReceiverList_show = true

      let params = {
        public_receiver_by: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangePublicReceiverList(params)
    }
  }
  autocompleteBlurPublicReceiverList(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_PublicReceiverList_show = false
    }, 200)
  }
  callAutocompleteChangePublicReceiverList(params: any): void {
    if (this.input.is_autocomplete_PublicReceiverList_load) return
    this.input.is_autocomplete_PublicReceiverList_load = true
    let pThis = this
    this.PublicProcessService.PublicRole01ItemPublicReceiverList(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChoosePublicReceiverList(data[0])
          }, 200)
        } else {
          pThis.autocompleteListPublicReceiverList = data
        }
      }
    })
    this.input.is_autocomplete_PublicReceiverList_load = false
  }
  autocompleteChoosePublicReceiverList(data: any): void {
    console.log(data)

    this.input.public_receiver_by = data.public_receiver_by
    this.input.public_receiver_name = data.public_receiver_name

    this.is_autocomplete_PublicReceiverList_show = false
  }

  onClickPublicRole01ItemList(): void {
    // if(this.validatePublicRole01ItemList()) {
    // Open loading
    // Call api
    this.callPublicRole01ItemList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole01ItemList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole01ItemList(this.help.GetFilterParams(params, this.paginatePublicItem)).subscribe((data: any) => {
      // if(isValidPublicRole01ItemListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickPublicRole01ItemPublicItemAdd(): void {
    this.listPublicItem.push({
      index: this.listPublicItem.length + 1,
      request_number: null,
      public_type_name: null,
      public_source_name: null,
      instruction_send_date_text: null,
      public_receive_date_text: null,
      public_type: null,
      public_receiver_name: null,
      public_remark: null,
      public_status_name: null,

    })
    this.changePaginateTotal(this.listPublicItem.length, 'paginatePublicItem')
  }
  onClickPublicItemId(id: any): void { }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  onClickPublicRole01ItemAutoSplit(): void {
    this.PublicProcessService.PublicRole01ItemAutoSplit().subscribe((data: any) => {
      this.listData(data)
    })
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicItem = data.publicitem_list || []
    this.changePaginateTotal(this.listPublicItem.length, 'paginatePublicItem')

  }

  listData(data: any): void {
    this.listPublicItem = data.list || []
    this.help.PageSet(data, this.paginatePublicItem)
    //this.listPublicItem = data.list || []
    //this.changePaginateTotal(this.listPublicItem.length, 'paginatePublicItem')

  }

  saveData(): any {
    // let params = this.input
    // params.publicitem_list = this.listPublicItem || []

    const params = {
      instruction_send_start_date: this.input.instruction_send_start_date,
      instruction_send_end_date: this.input.instruction_send_end_date,
      public_type_code: this.input.public_type_code,
      public_source_code: this.input.public_source_code,
      public_receiver_by: this.input.public_receiver_by,
      public_status_code: this.input.public_status_code,
      request_number: this.input.request_number,
      //page_index: +this.paginatePublicItem.currentPage,
      //item_per_page: +this.paginatePublicItem.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'instruction_send_date',
      //  value: displayDateServer(this.input.instruction_send_start_date),
      //  operation: 3
      //}, {
      //  key: 'instruction_send_date',
      //  value: displayDateServer(this.input.instruction_send_end_date),
      //  operation: 4
      //}, {
      //  key: 'public_type_code',
      //  value: this.input.public_type_code,
      //  operation: 0
      //}, {
      //  key: 'public_source_code',
      //  value: this.input.public_source_code,
      //  operation: 0
      //}, {
      //  key: 'public_receiver_by',
      //  value: this.input.public_receiver_by,
      //  operation: 5
      //}, {
      //  key: 'public_status_code',
      //  value: this.input.public_status_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}]
    }

    return params
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicItem') {
      this.onClickPublicRole01ItemList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }


  displayMoney(value: any): any {
    return displayMoney(value)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
