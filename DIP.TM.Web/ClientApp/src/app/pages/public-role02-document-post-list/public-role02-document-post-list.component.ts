import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayDateServer,
    clone
} from '../../helpers'

@Component({
    selector: "app-public-role02-document-post-list",
    templateUrl: "./public-role02-document-post-list.component.html",
    styleUrls: ["./public-role02-document-post-list.component.scss"]
})
export class PublicRole02DocumentPostListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    // List PublicRole02DocumentPost
    public listPublicRole02DocumentPost: any[]
    public paginatePublicRole02DocumentPost: any
    public perPagePublicRole02DocumentPost: number[]
    // Response PublicRole02DocumentPost
    public responsePublicRole02DocumentPost: any
    public listPublicRole02DocumentPostEdit: any


    // List PublicProcess
    public listPublicProcess: any[]
    public paginatePublicProcess: any
    public perPagePublicProcess: number[]
    // Response PublicProcess
    public responsePublicProcess: any

    //label_save_combobox || label_save_radio
    public publicDocumentPaymentInstructionRuleCodeList: any[]
    public publicRole02DocumentPostStatusCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            //public_end_start_date: getMoment(),
            //public_end_end_date: getMoment(),
            //public_role05_consider_payment_start_date: getMoment(),
            //public_role05_consider_payment_end_date: getMoment(),
            public_document_payment_instruction_rule_code: '',
            book_public_role02_document_post_status_code: '',
            request_number: '',
            book_number: '',
        }
        this.listPublicRole02DocumentPost = []
        this.paginatePublicRole02DocumentPost = clone(CONSTANTS.PAGINATION.INIT)
        this.paginatePublicRole02DocumentPost.id = 'paginatePublicRole02DocumentPost'
        this.perPagePublicRole02DocumentPost = clone(CONSTANTS.PAGINATION.PER_PAGE)

        this.listPublicProcess = []

        //Master List
        this.master = {
            publicDocumentPaymentInstructionRuleCodeList: [],
            publicRole02DocumentPostStatusCodeList: [],
        }
        //Master List



        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initPublicRole02DocumentPostList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.publicDocumentPaymentInstructionRuleCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.public_document_payment_instruction_rule_code = "ALL"
                this.master.publicRole02DocumentPostStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.post_round_document_post_status_code = "WAIT"

            }
            if (this.editID) {
                this.PublicProcessService.PublicRole02DocumentPostLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }

        })
    }

    constructor(
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private PublicProcessService: PublicProcessService
    ) { }

    onClickPublicRole02DocumentPostList(): void {
        // if(this.validatePublicRole02DocumentPostList()) {
        // Open loading
        // Call api
        this.callPublicRole02DocumentPostList(this.saveData())
        // }
    }
    //! <<< Call API >>>
    callPublicRole02DocumentPostList(params: any): void {
        this.global.setLoading(true)
        this.PublicProcessService.PublicRole02DocumentPostList(params).subscribe((data: any) => {
            // if(isValidPublicRole02DocumentPostListResponse(res)) {
            if (data) {
                // Set value
                this.listData(data)
                this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickPublicRole02DocumentPostSend(): void {
        //if(this.validatePublicRole02DocumentPostSend()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callPublicRole02DocumentPostSend(this.listPublicRole02DocumentPost)
        //}
    }
    validatePublicRole02DocumentPostSend(): boolean {
        let result = validateService('validatePublicRole02DocumentPostSend', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callPublicRole02DocumentPostSend(params: any): void {
        this.PublicProcessService.PublicRole02DocumentPostSend(params).subscribe((data: any) => {
            // if(isValidPublicRole02DocumentPostSendResponse(res)) {
            if (data) {
                // Set value
                //this.loadData(data)
                this.onClickPublicRole02DocumentPostList()
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickPublicRole02DocumentPostAdd(): void {
        this.listPublicRole02DocumentPost.push({
            index: this.listPublicRole02DocumentPost.length + 1,
            request_number: null,
            request_date_text: null,
            book_number: null,
            public_document_payment_instruction_rule_name: null,
            book_start_date_text: null,
            name: null,
            house_number: null,
            post_number: null,
            book_remark: null,
            book_public_role02_document_post_status_name: null,

        })
        this.changePaginateTotal(this.listPublicRole02DocumentPost.length, 'paginatePublicRole02DocumentPost')
    }

    onClickPublicRole02DocumentPostEdit(item: any): void {
        var win = window.open("/" + item.id)
    }


    onClickPublicRole02DocumentPostDelete(item: any): void {
        // if(this.validatePublicRole02DocumentPostDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listPublicRole02DocumentPost.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listPublicRole02DocumentPost.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listPublicRole02DocumentPost.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {
                if (rs === true) rs = ""

                let ids = []

                for (let i = 0; i < this.listPublicRole02DocumentPost.length; i++) {
                    if (this.listPublicRole02DocumentPost[i].is_check) {
                        this.listPublicRole02DocumentPost[i].cancel_reason = rs
                        this.listPublicRole02DocumentPost[i].status_code = "DELETE"
                        this.listPublicRole02DocumentPost[i].is_deleted = true

                        if (true && this.listPublicRole02DocumentPost[i].id) ids.push(this.listPublicRole02DocumentPost[i].id)
                        // else this.listPublicRole02DocumentPost.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callPublicRole02DocumentPostDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listPublicRole02DocumentPost.length; i++) {
                    this.listPublicRole02DocumentPost[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callPublicRole02DocumentPostDelete(params: any, ids: any[]): void {
        this.PublicProcessService.PublicRole02DocumentPostDelete(params).subscribe((data: any) => {
            // if(isValidPublicRole02DocumentPostDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listPublicRole02DocumentPost.length; i++) {
                    if (this.listPublicRole02DocumentPost[i].id == id) {
                        this.listPublicRole02DocumentPost.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listPublicRole02DocumentPost.length; i++) {
                this.listPublicRole02DocumentPost[i] = i + 1
            }

            this.onClickPublicRole02DocumentPostList()
            // Close loading
            this.global.setLoading(false)
        })
    }


    // Modal Location
    autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
        if (object[name].length >= length) {
            console.log("d")
            this.is_autocomplete_ListModalLocation_show = true

            let params = {
                name: object[name],
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListModalLocation(params)
        }
    }
    autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_ListModalLocation_show = false
        }, 200)
    }
    callAutocompleteChangeListModalLocation(params: any): void {
        if (this.input.is_autocomplete_ListModalLocation_load) return
        this.input.is_autocomplete_ListModalLocation_load = true
        let pThis = this
        // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
        //   if (data) {
        //     if (data.length == 1) {
        //       setTimeout(function () {
        //         pThis.autocompleteChooseListModalLocation(data[0])
        //       }, 200)
        //     } else {
        //       pThis.autocompleteListListModalLocation = data
        //     }
        //   }
        // })
        this.input.is_autocomplete_ListModalLocation_load = false
    }
    autocompleteChooseListModalLocation(data: any): void {
        this.inputModalAddress.address_sub_district_code = data.code
        this.inputModalAddress.address_sub_district_name = data.name
        this.inputModalAddress.address_district_code = data.district_code
        this.inputModalAddress.address_district_name = data.district_name
        this.inputModalAddress.address_province_code = data.province_code
        this.inputModalAddress.address_province_name = data.province_name
        this.inputModalAddress.address_country_code = data.country_code
        this.inputModalAddress.address_country_name = data.country_name
        this.is_autocomplete_ListModalLocation_show = false
    }
    onClickSave04ModalAddressSave(): void {
        Object.keys(this.inputModalAddress).forEach((item: any) => {
            this.inputModalAddressEdit[item] = this.inputModalAddress[item]
        })
        this.toggleModal("isModalPeopleEditOpen")
    }
    // Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listPublicRole02DocumentPost = data.publicrole02documentpost_list || []
        this.changePaginateTotal(this.listPublicRole02DocumentPost.length, 'paginatePublicRole02DocumentPost')

    }

    listData(data: any): void {
        this.listPublicRole02DocumentPost = data.list || []
        this.changePaginateTotal(this.listPublicRole02DocumentPost.length, 'paginatePublicRole02DocumentPost')

    }

    saveData(): any {
        // let params = this.input
        // params.publicrole02documentpost_list = this.listPublicRole02DocumentPost || []

        const params = {
            page_index: +this.paginatePublicRole02DocumentPost.currentPage,
            item_per_page: +this.paginatePublicRole02DocumentPost.itemsPerPage,
            order_by: 'created_date',
            is_order_reverse: false,
            search_by: [{
                key: 'public_end_date',
                value: displayDateServer(this.input.public_end_start_date),
                operation: 3
            }, {
                key: 'public_end_date',
                value: displayDateServer(this.input.public_end_end_date),
                operation: 4
            }, {
                key: 'public_role05_consider_payment_date',
                value: displayDateServer(this.input.public_role05_consider_payment_start_date),
                operation: 3
            }, {
                key: 'public_role05_consider_payment_date',
                value: displayDateServer(this.input.public_role05_consider_payment_end_date),
                operation: 4
            }, {
                key: 'public_document_payment_instruction_rule_code',
                value: this.input.public_document_payment_instruction_rule_code,
                operation: 0
            }, {
                key: 'post_round_document_post_status_code',
                value: this.input.post_round_document_post_status_code,
                operation: 0
            }, {
                key: 'request_number',
                values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
                operation: 5
            }, {
                key: 'book_number',
                value: this.input.book_number,
                operation: 5
            }]
        }

        return params
    }



    //! <<< ----------------- >>>
    //! <<< --- Copy Zone --- >>>
    //! <<< ----------------- >>>

    //! <<< Binding (one way) >>>
    binding(obj: any, name: any, value: any): void {
        this[obj][name] = value
    }

    //! <<< Table >>>
    onSelectColumnInTable(name: string, item: any): void {
        if (!item) {
            this.input.is_check_all = !this.input.is_check_all
            this[name].forEach((item: any) => {
                item.is_check = this.input.is_check_all
            })
        } else {
            item.is_check = !item.is_check
        }
    }
    onChangeInputInTable(item: any, name: any, value: any): void {
        item[name] = value
    }
    toggleInputInTable(item: any, name: any): void {
        item[name] = !item[name]
    }

    //! <<<< Pagination >>>
    managePaginateCallApi(name: string): void {
        if (name === 'paginatePublicRole02DocumentPost') {
            this.onClickPublicRole02DocumentPostList()
            this.input.is_check_all = false
        }
    }
    onChangePage(page: any, name: string): void {
        this[name].currentPage = page === '' ? 1 : page
        if ((+page || page === '') && page <= this.getMaxPage(name)) {
            this.managePaginateCallApi(name)
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
        this.managePaginateCallApi(name)
    }
    changePaginateTotal(total: any, name: string): void {
        this[name].totalItems = total
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
