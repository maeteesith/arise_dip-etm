import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-document-role04-create-list",
  templateUrl: "./document-role04-create-list.component.html",
  styleUrls: ["./document-role04-create-list.component.scss"]
})
export class DocumentRole04CreateListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  public input_add: any
  public input_search: any

  public tableList: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
    }
    this.input_add = {}
    this.input_search = {}

    //Master List
    this.master = {
    }
    //Master List

    if (!this.tableList) {
      this.tableList = {
        column_list: {
          index: "#",
          request_number: "เลขที่คำขอ",
          name: "ชื่อเจ้าของ",
          created_date: "วันที่สร้าง / ส่งมา",
          document_role04_create_type_name: "เรื่อง",
          value_01: "รายละเอียด",
          document_role04_create_status_name: "สถานะ",
        },
        command_item: [{
          name: "item_edit"
        }],
      }
    }


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
    })

    this.forkJoinService.initDocumentRole04CreateList().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
        this.master.documentRole04CreateStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input_search.document_role04_create_status_code = ""

        this.master.documentRole04CreateTypeCodeAllList = []
        this.master.documentRole04CreateTypeCodeList.forEach((item: any) => {
          this.master.documentRole04CreateTypeCodeAllList.push(item)
        })
        this.master.documentRole04CreateTypeCodeAllList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input_add.document_role04_create_type_code = this.master.documentRole04CreateTypeCodeList[0].code
        this.input_search.document_role04_create_type_code = ""
      }
      if (this.editID) {
        this.DocumentProcessService.DocumentRole04CreateLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

      this.onClickDocumentRole04CreateList()
    })
  }

  onClickDocumentRole04CreateAdd(): void {
    if (this.input.id) {
      var param = {
        save_id: this.input.id,
        document_role04_create_type_code: this.input_add.document_role04_create_type_code,
        request_number: this.input_add.request_number,
        value_01: this.input_add.value_01,
      }

      this.global.setLoading(true)
      this.DocumentProcessService.DocumentRole04CreateAdd(param).subscribe((data: any) => {
        if (data) {
          // Manage structure
          //this.loadData(data)
          this.tableList.Add(data)
        }
        // Close loading
        this.global.setLoading(false)
        this.automateTest.test(this)
      })
    }
  }

  onClickDocumentRole04CreateList(): void {
    var param = {

    }
    this.DocumentProcessService.List("DocumentRole04Create", this.help.GetFilterParams(param)).subscribe((data: any) => {
      if (data) {
        // Manage structure
        //this.loadData(data)
        this.tableList.SetDataList(data)
      }
      // Close loading
      this.global.setLoading(false)
      this.automateTest.test(this)
    })
  }

  onClickCommand($event) {
    console.log($event)
    if ($event) {
      if ($event.command) {
        if ($event.command == "item_edit") {
          window.open("document-role04-create/edit/" + $event.object_list[0].id)
        }
        ////} else {
        ////  this.onClickDocumentRole04CheckList($event)
      }
    }
  }

  onClickReset(): void {
    this.ngOnInit()
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }

  onChangeDocumentRole04CreateRequestNumber(event): void {
    console.log(event)
    if (this.input_add.request_number.length > 0) {
      var param = {
        request_number: this.input_add.request_number,
      }
      //this.input_add.request_number = ""
      this.global.setLoading(true)
      this.DocumentProcessService.List("Save010", this.help.GetFilterParams(param)).subscribe((data: any) => {
        if (data && data.list && data.list.length > 0) {
          this.input = data.list[0]
          // Manage structure
          //this.loadData(data)
        }
        // Close loading
        this.global.setLoading(false)
        this.automateTest.test(this)
      })
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data


  }

  listData(data: any): void {

  }

  saveData(): any {
    let params = this.input


    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

}
