import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
import { Auth } from "../../auth";
import { ForkJoinService } from '../../services/fork-join.service'
import { Help } from '../../helpers/help'
import { AutoComplete } from "../../helpers/autocomplete";


//Services
import { RegistrationRequestService } from "../../services/registration-request.service";
import { RequestProcessService } from '../../services/request-process-buffer.service'
import { RequestAgencyService } from '../../services/request-agency.service'

//scss
import '../appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss'

import { 
  CONSTANTS,
  getMoment,
  validateService,
  displayDate,
  clone,
 } from '../../helpers';

 import { DomSanitizer} from '@angular/platform-browser';

@Component({
  selector: 'app-appeal-role01-kor-other-floor3',
  templateUrl: './appeal-role01-kor-other-floor3.component.html',
  styleUrls: [
    '../appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss',
    './appeal-role01-kor-other-floor3.component.scss',
  
]
})
export class AppealRole01KorOtherFloor3Component implements OnInit {

  //Init
  public editID: any
  public input: any
  public modal: any
  public validate: any
  public master: any

  // Response
  public response: any

  //AutoFill
  public smartCardData: any
  public autoFill: any
  

  //List
  public listItem: any[]
  public requestItem: any

  public listRequest01Item: any[]
  public paginateRequest01Item: any
  public perPageRequest01Item: number[]

  //RequestType
  public paginateRequest: any
  public requestTypeList: any[]
  public requestTypeItemList: any[]
  public searchRequestType: any
  public requestType: any[]

  //Agency
  public paginateAgency: any
  public agencyList: any[]
  public searchAgency: any

  //Print Receipt
  public receiptData: any
  

  public searchRepresentative: any

  
  public selectedRow: Number;
  public setClickedRow: Function;

  public perPage: any[]
  public popup: any


  public setTableIndex: any
  public TotalAmount: string
  

  public paginateList: any[]

  public paginateItem: any
  public perPageItem: number[]

  // Autocomplete
  public autocompleteList: any
  public isShowAutocomplete: any

  // Other
  public isHasRequestList: boolean
  public timeout: any
  public isShow: boolean
  public url: any
  public officerIdcard: any
  public autoFillToken: any
  public is_gdx_error: any 

   
  

  constructor(
    private router: Router,
    private global: GlobalService,
    private help: Help,
    private autoComplete: AutoComplete,
    private auth: Auth,
    private forkJoinService: ForkJoinService,
    private RegistrationRequestService: RegistrationRequestService,
    private RequestProcessService: RequestProcessService,
    private requestAgencyService: RequestAgencyService,
    public sanitizer:DomSanitizer
    
  ) { }

  ngOnInit(
  ) {
    this.input = {
      id: null,
      requester_name: '',
      created_by_name: this.auth.getAuth().name,
      request_date: getMoment(),
      payment_date: getMoment(),
      is_via_post: false,
      is_wave_fee: false,
      commercial_affairs_province_code: "FLOOR3",
    }
    this.listRequest01Item = []
    this.listRequest01Item.push({})
    this.listRequest01Item.push({})
    this.listRequest01Item.push({})
    this.listRequest01Item.push({})
    this.listRequest01Item.push({})
    this.paginateRequest01Item = CONSTANTS.PAGINATION.INIT
    this.perPageRequest01Item = CONSTANTS.PAGINATION.PER_PAGE
    
    // this.listItem = []
    // this.listItem.push({})
    // this.listItem.push({})
    // this.listItem.push({})
    // this.listItem.push({})
    // this.listItem.push({})
    // this.listItem.push({})
    // this.listItem.push({})
    // this.listItem.push({})
    // this.listItem.push({})
    // this.listItem.push({})
    // this.paginateItem = CONSTANTS.PAGINATION.INIT
    // this.perPageItem = CONSTANTS.PAGINATION.PER_PAGE

    this.insertRow()
    
    this.validate = {}
    this.perPage = [10, 50, 100]

    this.modal = {
      isModalAutoFill: false,
      isModalRequest: false,
      isModalPreviewPdfOpen: false,
    }

    //Pagination
    this.paginateRequest = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateRequest.id = 'paginateRequest' 
    this.paginateItem = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateItem.id = 'paginateItem'
    this.paginateAgency = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateAgency.id = 'paginateAgency' 
    this.paginateItem = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateItem.id = 'paginateItem'

    this.sumTotal
    this.callInit()

    //AutoComplete
    this.autocompleteList = {
      requester_name: []
    }
    this.isShowAutocomplete = {
      requester_name: false
    }

    this.master = {
    }

    // Auto fill
    this.smartCardData = {
      connect: false,
      type: 1,
      officer: {
        connect: false,
        info: {}
      },
      people: {
        connect: false,
        info: {
          image: '',
          id_card: '',
          name_th: '',
          name_en: '',
          birthday: '',
          issue_date: '',
          expiration_date: '',
          address: ''
        }
      }
    }
    this.requestItem = {
      audio: {
        input: '',
        validate: '',
        file: {},
        sound: null,
        palyer: null,
        action: false
      },
      people_code: 'NAT_ID'
    }
    
    var pThis = this
    this.autoComplete.Initial()
    this.autoComplete.Add("/RequestProcess/RequestTypeCodeList", "รหัสรายการ", ["code", "name"], [0, 1], 0, this.input, [
      ["request_type_code", "code"]],
      function (row_item) {
        pThis.popup.warning_message_show_list = []
        //console.log(pThis.listRequest01Item)
        //console.log(row_item)
        //console.log(pThis.listRequest01Item)

        //if (!row_item.requestOtherItemSub) {
        //console.log(row_item)
        //pThis.help.Clear(row_item)
        row_item.requestOtherItemSub = []
        row_item.is_disabled = true

        pThis.listRequest01Item.forEach((item: any) => {
          console.log(item)
          if (item.request_number && item.request_number != "") {
            if (row_item.request_type_code != "70") {
              row_item.requestOtherItemSub.push({
                request_number: item.request_number,
              })
            } else {
              //console.log(item)
              var product_list = pThis.help.Distinct(item.product_list, "request_item_sub_type_1_code")
              //console.log(product_list)
              Object.keys(product_list).forEach((product: any) => {
                row_item.requestOtherItemSub.push({
                  request_number: item.request_number,
                  item_sub_type_1_code: product,
                  product_count: product_list[product],
                })
              })
              pThis.updateRequestOtherItemSub(row_item.requestOtherItemSub)
            }
          }

          if (item.request_number && item.request_number != "") {
            if (row_item.request_type_code == "70") {
              console.log(item.request_number)
              console.log(item.reference_number)
              //console.log(getMoment())
              //console.log(item.trademark_expired_end_date)
              //console.log(getMoment())


              var trademark_expired_start_date = getMoment(item.trademark_expired_start_date, "YYYY-MM-DDTHH:mm:ss")
              var trademark_expired_date = getMoment(item.trademark_expired_date, "YYYY-MM-DDTHH:mm:ss")
              var trademark_expired_end_date = getMoment(item.trademark_expired_end_date, "YYYY-MM-DDTHH:mm:ss")

              if (!item.registration_number || item.registration_number == "") {
                var msg = {
                  message: "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ได้เลขทะเบียน",
                  is_warning: true,
                }
                console.log("warnning_message")
                if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                row_item.warnning_message += msg.message
                pThis.popup.warning_message_show_list.push(msg)
                row_item.is_deactivated = true
              } else if (item.trademark_expired_start_date && trademark_expired_start_date > getMoment()) {
                var msg = {
                  message: "เลขคำขอ <b>" + item.request_number + "</b> " +
                    "ทะเบียนเลขที่ <b>" + item.registration_number + "</b> " +
                    "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
                    "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
                    "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
                    "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
                    "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
                  is_warning: true,
                }
                console.log("warnning_message")
                row_item.warnning_message = row_item.warnning_message || ""
                if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ถึงกำหนดการต่ออายุวันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>"
                pThis.popup.warning_message_show_list.push(msg)
              } else if (item.trademark_expired_end_date && getMoment() > trademark_expired_end_date) {
                console.log("warnning_message")

                if (item.last_extend_date) {
                  var last_extend_date = getMoment(item.last_extend_date, "YYYY-MM-DDTHH:mm:ss")
                  if (last_extend_date > trademark_expired_start_date) {
                    msg = {
                      message: "เลขคำขอ <b>" + item.request_number + "</b> " +
                        "ยื่นต่ออายุครั้งล่าสุดเมื่อวันที่ <b>" + displayDate(last_extend_date) + "</b> ",
                      is_warning: false,
                    }
                    pThis.popup.warning_message_show_list.push(msg)
                  } else {
                    row_item.is_deactivated = true
                  }
                } else {
                  row_item.is_deactivated = true
                }

                if (row_item.is_deactivated) {
                  var msg = {
                    message: "เลขคำขอ <b>" + item.request_number + "</b> " +
                      "ทะเบียนเลขที่ <b>" + item.registration_number + "</b> " +
                      "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
                      "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
                      "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
                      "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
                      "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
                    is_warning: true,
                  }
                  row_item.warnning_message = row_item.warnning_message || ""
                  if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                  row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ไม่สามารถต่ออายุได้ เนื่องจากเกินกำหนดระยะเวลา"
                  pThis.popup.warning_message_show_list.push(msg)
                }
              }
            } else if (row_item.request_type_code == "60") {
              if (item.registration_number && item.registration_number != "") {
                var msg = {
                  message: "เลขคำขอ <b>" + item.request_number + "</b> ได้เลขทะเบียนแล้ว",
                  is_warning: true,
                }
                console.log("warnning_message")
                row_item.warnning_message = row_item.warnning_message || ""
                if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                row_item.warnning_message += msg.message
                pThis.popup.warning_message_show_list.push(msg)
              }
            } else if (row_item.request_type_code == "61") {
              if (!item.registration_number || item.registration_number == "") {
                var msg = {
                  message: "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ได้เลขทะเบียน",
                  is_warning: true,
                }
                console.log("warnning_message")
                row_item.warnning_message = row_item.warnning_message || ""
                if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                row_item.warnning_message += msg.message
                pThis.popup.warning_message_show_list.push(msg)
              }
            }
          }
        })

        //row_item.

        //console.log(row_item)

        pThis.listRequest01Item.forEach((item: any) => { item.reference_number = "" })

        pThis.updateItem()
        
        console.log("this is a book:",this.row_item)
      }
    )

    //this.listRequestOtherItemSub = []

    this.popup = {
      warning_message_show_list: [],
    }

    this.callInit()

    
  }

  updateRequestOtherItemSub(item_list: any[]): void {
    setTimeout(function () {
      //console.log(item_list)
      item_list.forEach((item: any) => {
        item.total_price = item.product_count ? (item.product_count > 5 ? 18000 : item.product_count * 2000) : 0
        item.fine = 0
      })
    }, 200)
  }

  updateItem(): void {
    this.global.setLoading(true)
    let pThis = this
    setTimeout(function () {
      let total_price = 0
      pThis.listItem.forEach((item: any) => {
        //console.log(item.requestOtherItemSub)

        let request_type_list = pThis.master.requestTypeList.filter(r => r.code == item.request_type_code);

        if (request_type_list.length == 0) {
          item.request_type_code = ""
          item.request_type_description = ""
          item.item_count = 0
          item.item_sub_type_1_count = 0
          item.product_count = 0
          item.total_price = 0
          item.fine = 0
          item.is_evidence_floor7 = false
        } else {
          //console.log(item)

          item.request_type_description = request_type_list[0].name
          //console.log(item.requestOtherItemSub)
          item.item_count = item.requestOtherItemSub ? pThis.help.CountDistinct(item.requestOtherItemSub, "request_number") : 0
          item.item_sub_type_1_count = item.request_type_code != "70" ? 0 :
            pThis.help.CountDistinct(item.requestOtherItemSub, "item_sub_type_1_code")
          item.product_count = item.request_type_code != "70" ? 0 :
            item.requestOtherItemSub.reduce(function (a, b) {
              return a + parseFloat(b.product_count || 1)
            }, 0)
          if (pThis.input.is_wave_fee) {
            item.total_price = 0
          } else {
            if (item.request_type_code != "70") {
              item.requestOtherItemSub.forEach((item_sub: any) => {
                item_sub.total_price = +request_type_list[0].value_2
              })

              item.total_price =
                item.requestOtherItemSub.reduce(function (a, b) {
                  return a + parseFloat(b.total_price || 0)
                }, 0)
            } else {
              item.total_price =
                item.requestOtherItemSub.reduce(function (a, b) {
                  return a + parseFloat(b.total_price || 0)
                }, 0)
            }
          }

          item.fine = 0
        }

        total_price += item.total_price
        total_price += item.fine
      })
      pThis.input.total_price = total_price
      pThis.input.item_list = pThis.listItem
    }, 200)
    this.global.setLoading(false)
  }
  //method
  callInit(): void{
    this.global.setLoading(true)
    this.forkJoinService.initAppealKOROtherFloor3Page().subscribe((data: any) => {
      if(data) {
        this.master = data
        this.requestTypeList = data.requestTypeList,
        this.setPagination(this.requestTypeList, this.paginateRequest.id),
        this.agencyList = data.agencyList,
        this.setPagination(this.agencyList, this.paginateAgency.id)
        console.log('a',this.agencyList)
        console.log('b',this.requestTypeList)
      }
      if (this.editID) {
        this.RequestProcessService.RequestOtherLoad(this.editID, {}).subscribe((data: any) => {
          if (data) {
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)

          //For Test
          //this.onClickRequestOtherItemEdit(this.listItem[0])
          //this.onClickRequestOtherItemSubAdd()
        })
      }
      this.global.setLoading(false)
    })
  }


  onClickRequest01ItemRequestSearch(row_item: any): void {
    // console.log(row_item)
    window.open()
  }

  onChangeSelectRequest01Item(row_item: any): void {
    this.global.setLoading(true)

    let pThis = this
    setTimeout(function () {
      //pThis.listRequest01Item = pThis.listRequest01Item || []
      //console.log(this.listRequest01Item)
      if (row_item.request_number.trim() == "" || pThis.listRequest01Item.filter(r => r.request_number == row_item.request_number).length > 1) {
        pThis.listRequest01Item.splice(pThis.listRequest01Item.indexOf(row_item), 1)
        pThis.global.setLoading(false)
        pThis.listRequest01Item.forEach((item: any) => { item.reference_number = "" })
        pThis.updateItem()
        return;
      }

      var params = pThis.help.GetFilterParams({ request_number: row_item.request_number })

      pThis.RequestProcessService.Request01ItemList(params).subscribe((data: any) => {
        if (data && data.list && data.list.length > 0) {
          Object.keys(data.list[0]).forEach((item: any) => {
            if (item == "id") return true;
            if (item == "reference_number") return true;
            row_item[item] = data.list[0][item]
          })
          if (pThis.input.requester_name == "") {
            pThis.input.requester_name = data.list[0].name
          }
          row_item.is_disabled = true
          pThis.listRequest01Item.forEach((item: any) => { item.reference_number = "" })

          pThis.listItem.forEach((item: any) => {
            console.log(item)
            if (item.request_type_code && item.request_type_code != "") {
              if (item.request_type_code != "70") {
                item.requestOtherItemSub.push({
                  request_number: data.list[0].request_number,
                })
              } else {
                var product_list = pThis.help.Distinct(data.list[0].product_list, "request_item_sub_type_1_code")
                Object.keys(product_list).forEach((product: any) => {
                  item.requestOtherItemSub.push({
                    request_number: data.list[0].request_number,
                    item_sub_type_1_code: product,
                    product_count: product_list[product],
                  })
                })
                pThis.updateRequestOtherItemSub(item.requestOtherItemSub)
              }
            }
          })

          //} else
          if (row_item.trademark_expired_date) {
            var trademark_expired_start_date = getMoment(row_item.trademark_expired_start_date, "YYYY-MM-DDTHH:mm:ss")
            var trademark_expired_date = getMoment(row_item.trademark_expired_date, "YYYY-MM-DDTHH:mm:ss")
            var trademark_expired_end_date = getMoment(row_item.trademark_expired_end_date, "YYYY-MM-DDTHH:mm:ss")

            if (getMoment() > trademark_expired_date) {
              pThis.popup.warning_message_show_list.push({
                message: "เลขคำขอ <b>" + row_item.request_number + "</b> " +
                  "ทะเบียนเลขที่ <b>" + row_item.registration_number + "</b> " +
                  "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
                  "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
                  "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
                  "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
                  "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
                is_warning: true,
              })

              if (row_item.last_extend_date) {
                var last_extend_date = getMoment(row_item.last_extend_date, "YYYY-MM-DDTHH:mm:ss")
                if (last_extend_date > trademark_expired_start_date) {
                  var msg = {
                    message: "เลขคำขอ <b>" + row_item.request_number + "</b> " +
                      "ยื่นต่ออายุครั้งล่าสุดเมื่อวันที่ <b>" + displayDate(last_extend_date) + "</b> ",
                    is_warning: false,
                  }
                  pThis.popup.warning_message_show_list.push(msg)
                }
              }
            }
          }

          pThis.updateItem()
        } else {
          row_item.request_number = ""
        }
        pThis.global.setLoading(false)
      })
    }, 200)
  }
  onClickRequestOtherItemAdd(): void {
    this.listItem.push({
      //index: this.listItem.length + 1,
      request_type_code: null,
      request_type_description: null,
      item_count: null,
      item_sub_type_1_count: null,
      book_index: null,
      page_index: null,
      product_count: null,
      total_price: null,
      fine: null,
      is_evidence_floor7: false,
      product_list_text: null,
      is_sue_text: null,

    })
    this.changePaginateTotal(this.listItem.length, 'paginateItem')
  }

  onClickRequestOtherMakerAutoFill(): void {
    if (this.validateRequestOtherMakerAutoFill()) {
      // Open loading
      this.global.setLoading(true)
      // Set param
      const params = {
        id: this.input.id,
        requester_name: this.input.requester_name,
        request_01_item_list: this.listRequest01Item || [],
        item_list: this.listItem || [],

      }
      // Call api
      this.callRequestOtherMakerAutoFill(params)
    }
  }
  validateRequestOtherMakerAutoFill(): boolean {
    let result = validateService('validateRequestOtherMakerAutoFill', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestOtherMakerAutoFill(params: any): void {
    this.RequestProcessService.RequestOtherMakerAutoFill(params).subscribe((data: any) => {
      // if(isValidRequestOtherMakerAutoFillResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listRequest01Item = data.request_01_item_list || []
        this.listItem = data.item_list || []
        this.changePaginateTotal((this.listRequest01Item || []).length, 'paginateRequest01Item')
        this.changePaginateTotal((this.listItem || []).length, 'paginateItem')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestOtherRequest01ItemAdd(): void {
    this.listRequest01Item.push({
      //index: this.listRequest01Item.length + 1,
      request_number: null,
      total_price: null,
      reference_number: null,
      owner_name: null,
      facilitation_act_status_code: this.master.facilitationActStatusCodeList[0].code,

    })
    this.changePaginateTotal(this.listRequest01Item.length, 'paginateRequest01Item')
    this.updateItem()
  }


  onClickRequestOtherSave(): void {
    //if (this.validateRequestOtherSave()) {
      // Open loading
      this.global.setLoading(true)
      // Set param
      for (let i = 0; i < this.listItem.length; i++) {
        if (this.listItem[i].request_type_code == "") {
          this.listItem.splice(i--)
        }
      }
      const params = {
        id: this.input.id,
        request_date: this.input.request_date,
        requester_name: this.input.requester_name,
        is_via_post: this.input.is_via_post,
        is_wave_fee: this.input.is_wave_fee,
        //is_commercial_affairs_province: this.input.is_commercial_affairs_province,
        commercial_affairs_province_code: this.input.commercial_affairs_province_code,
        total_price: parseInt(this.TotalAmount),
        request_01_item_list: this.listRequest01Item || [],
        item_list: this.listItem || [],
      }
      console.log('show price '+ params.total_price) 
      console.log('data send to backend ', params) 
      this.callRequestOtherSave(params)
      this.global.setLoading(false);
      
    //}
  }
  validateRequestOtherSave(): boolean {
    let result = validateService('validateRequestOtherSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestOtherSave(params: any): void {
    this.RequestProcessService.RequestOtherSave(params).subscribe((data: any) => {
      if (data) {
        console.log('load data response ', data);
        this.loadData(data)
        this.toggleModal('isPopupSaveOpen')

        //get data
        this.receiptData = data
      }
    })
  }

  onClickPrintReceipt(): void {
    if (this.receiptData.total_price > 0) {
      this.url = '/pdf/ReceiptOtherReference/' + this.receiptData.id
      console.log(this.url)
      this.toggleModal('isModalPreviewPdfOpen')

      console.log('pdf data', this.receiptData.total_price)
    }
    else {
      this.toggleModal('isPopupNotFound')
    }
    
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(obj: any, name: any): void {
    this.validate[name] = null
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      if (this[obj][name]) {
        if (name === 'requester_name') {
          this.callListView({
            name: this[obj][name],
            paging: {
              item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page
            }
          }, name)
        }
      } else {
        this.onClickOutsideAutocomplete(name)
      }
    }, CONSTANTS.DELAY_CALL_API)
  }
  callListView(params: any, name: any): void {
    this.requestAgencyService.ListView(params).subscribe((data: any) => {
      if (data) {
        this.autocompleteList[name] = data
        this.isShowAutocomplete[name] = true
      } else {
        this.isShowAutocomplete[name] = false
        this.autocompleteList[name] = []
      }
    })
  }
  onSelectAutocomplete(name: any, item: any): void {
    if (name === 'requester_name') {
      this.input[name] = item.name
      this.input.is_wave_fee = item.is_wave_fee
      this.updateRequestList()
    }
    this.onClickOutsideAutocomplete(name)
  }
  onClickOutsideAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = false
    this.autocompleteList[name] = []
  }
  updateRequestList(): void {
    if (this.input.is_wave_fee) {
      //this.input.total_price = 0
      //this.requestList.forEach(request => {
      //  request.total_price = 0
      //  request.size_over_price = 0
      //  request.item_sub_list.forEach((item: any) => {
      //    item.total_price = 0
      //  })
      //  request.size_over_price = 0
      //})
    } else {
      //let total = 0
      //this.requestList.forEach((request: any) => {
      //  let total_price_request = 0
      //  request.item_sub_list.forEach((item: any) => {
      //    let total = getItemCalculator(item.product_count, this.master.priceMasterList)
      //    item.total_price = total
      //    total_price_request = total_price_request + total
      //  })
      //  request.total_price = total_price_request
      //  total = total + total_price_request

      //  request.size_over_price = request.size_over_cm * 200
      //  total = total + (request.size_over_price || 0) * request.amount_product

      //})
      //this.input.total_price = total
    }
  }

  setIndex(index): void {
    this.setTableIndex = index
  }
  selectItemRequest(value): void {

    const item_list_sub = [{
      request_number: this.listRequest01Item[0].request_number,
      item_sub_type_1_code: this.listRequest01Item[0].request_item_sub_type_1_code,
      product_count: this.listRequest01Item[0].product_count,
      total_price: this.listRequest01Item[0].total_price
    }]

    console.log('list item 01', item_list_sub)
    
    let listItemData = {
      code: value.code,
      name: value.name,
      value_1: value.value_1,
      value_2: value.value_2,
      //index: this.listItem.length + 1,
      request_type_code: value.code,
      request_type_description: value.name,
      item_count: null,
      item_sub_type_1_count: null,
      product_count: null,
      total_price: parseInt(value.value_2) || parseInt(value.value_1),
      fine: null,
      is_evidence_floor7: false,
      product_list_text: null,
      is_sue_text: null,
      requestOtherItemSub: item_list_sub
    }
    this.listItem[this.setTableIndex] = listItemData

    //this.listItem.push({
    //  value.
    //})

    console.log('list item value ', listItemData)
    
    this.sumTotal()
    this.setPagination(this.requestTypeItemList, this.paginateRequest.id)
  }
    
  sumTotal(): void{
    let defaultSum: number = 0.00
    for(let i = 0; i < this.listItem.length; i++){
      if(this.listItem[i].value_2 != null && this.listItem[i].value_2 != '' && this.listItem[i].value_2 != false){
        defaultSum += parseFloat(this.listItem[i].value_2)
      }
      else{
        defaultSum += parseFloat('0')
      }
    }
    this.TotalAmount = defaultSum.toFixed(2)
  }
//insert table row dummy
insertRow(){
  this.listItem = []
  let i: number = 0
  let j: number = 10
  while(i<j) {
    this.listItem.push({
      code: null,
      name: null,
      //index: this.listItem.length + 1,
      request_type_code: '',
      request_type_description: null,
      item_count: null,
      item_sub_type_1_count: null,
      product_count: null,
      total_price: null,
      fine: null,
      is_evidence_floor7: false,
      product_list_text: null,
      is_sue_text: null,
    })
    i++;
  }
  //this.setPagination(this.listItem, this.paginateItem.id)
}

  removeItemIndex(index): void {
    this.listItem[index] = {}
    this.sumTotal()
  }

  toggleModal(name: string): void {
    if (!this.modal[name]) { // Is open
      this.modal[name] = true
    } else { // Is close
      this.modal[name] = false
      console.log(name)
      switch(name){
        case ' isModalAgent':
          this.searchAgency = ''
          this.setPagination(this.agencyList, this.paginateAgency.id)
        break;
        case 'isModalAutoFill':
          console.log("hello autoFill")
        break;
        case 'isModalRequest':
          this.searchRequestType = ''
          this.setPagination(this.requestTypeList, this.paginateRequest.id)
        break;
      }
    
    }
  }

  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name]
  }

  loadData(data: any) {
    // Manage structure
    Object.keys(data).forEach((item: any) => {
      this.input[item] = data[item]
    })

    this.listRequest01Item = data.request_01_item_list || []
    this.listItem = data.item_list || []
    data.request_01_item_list.map((item: any) => {
      item.is_check = false
      item.is_disabled = true
      return item
    })
    data.item_list.map((item: any) => {
      item.code = item.request_type_code
      item.name = item.request_type_name
      item.is_check = false
      item.is_disabled = true

      item.requestOtherItemSub.map((item_sub: any) => {
        var request_item = this.listRequest01Item.filter(r => r.request_number == item_sub.request_number)
        if (request_item.length == 1) {
          request_item[0].request_index_list = request_item[0].request_index_list || []
          if (item_sub.request_index && request_item[0].request_index_list.indexOf(item.request_type_code + ": " + item_sub.request_index) < 0)
            request_item[0].request_index_list.push(item.request_type_code + ": " + item_sub.request_index)
        }
      })


      return item
    })
    this.changePaginateTotal((this.listRequest01Item || []).length, 'paginateRequest01Item')
    this.changePaginateTotal((this.listItem || []).length, 'paginateItem')

    //console.log(this.listItem)
    //this.autoComplete.Update(this.listItem)


    // Set value
    // this.requestList = data.item_list
    // this.response.request01Load = data
    // this.isHasRequestList = true
    this.updateItem()
  }

  //Pagination prop
  setPagination(list: any[], paginate: string): void {
    switch(paginate){
      case this.paginateRequest.id: this.requestTypeItemList = list; break;
      case this.paginateAgency.id: this.agencyList = list; break;
    }
    this.changePaginateTotal(list.length, paginate)
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }

  //! <<< Auto fill >>>
  onClickRadio(name: any, value: any): void {
    this.smartCardData[name] = value
    this.onChangeSmartCard()
  }
  onChangeSmartCard(): void {
    this.smartCardData = {
      connect: true,
      type: 1,
      officer: {
        connect: true,
        info: {}
      },
      people: {
        connect: true/*  ,
        info: {
          image: 'https://komchadluek.sgp1.cdn.digitaloceanspaces.com/media/img/size1/2019/03/22/8be76h8ikb6acaic57i9d.jpg',
          id_card: '1509901200214',
          name_th: 'นที วารีย์',
          name_en: 'Natee Varee',
          birthday: '07/08/1992',
          issue_date: '07/08/2014',
          expiration_date: '06/08/2022',
          address: '131 ถ.มหิดล ต.หนองหอย อ.เมือง จ.เชียงใหม่ 50000' 
        }*/
      }
    }
  }

  //! <<< IdCard >>>
  timeLeft: number = 30;
  timeMsg: string = "";
  interval;
  startGetIdCardTimer() {
    this.interval = setInterval(() => {
      this.timeLeft -= 5;
      this.timeMsg = "กรุณาเสียบบัตรประชาชนของพนักงานที่เครื่องอ่านบัตรก่อนทำรายการ [เหลือเวลาอีก " + this.timeLeft + " วินาที]";

      if (this.timeLeft > 0) {
        this.getIdCard();
      } else {
        this.timeMsg = "กรุณาเสียบบัตรประชาชนของพนักงานและกดค้นหาใหม่";
        this.pauseTimer();
      }
    }, 5000)
  }
  pauseTimer() {
    this.timeLeft = 30;
    clearInterval(this.interval);
  }
  bindingAutoFill(data) {
    this.timeMsg = "การค้นหาสำเร็จ";
    this.autoFillToken = data.auto_fill_token

    this.input.name_th = data.thai_name;
    this.input.name_en = data.eng_name;
    this.input.birthday = data.birth_date;
    this.input.issue_date = data.issue_date;
    this.input.expiration_date = data.expire_date;
    this.input.address = data.address;
  }
  getIdCard() {
    this.RegistrationRequestService.GetIDCard({}).subscribe((data: any) => {
      //alert(data.citizenId);
      if (data.length > 0) {
        console.log(data)

        data = data[0]
        if (data.citizenId) {
          this.pauseTimer()
          this.officerIdcard = data.citizenId

          this.AutoFillFindID()
        }
      }
    })
  }

  //! <<< GDX Search >>>
  /*
  1. When click find button on registration-request
  2. Validate officer input citizen id card
  */
  onClickGDXSearch(): void {
    // Validate id card is mandatory
    if (!this.input.id_card) {
      alert("กรุณากรอกเลขบัตรประชาชน");
      return null;
    }
    else {
      // Find auto fill token from somewhere
      // Dummy set auto fill token to 1234
      //autoFillToken = "1234";
      //this.officerIdcard = "123";
      let pThis = this
      // If can't find auto fill token
      // Officer need to insert his card to card reader
      if (!this.autoFillToken && !this.officerIdcard) {
        if (confirm("กรุณาเสียบบัตรประชาชนของพนักงานที่เครื่องอ่านบัตรก่อนทำรายการ")) {
          //TODO get officer id card from card reader
          //officerIdcard = "1739900081202";
          this.RegistrationRequestService.DeleteIDCardReader({}, function () {
            pThis.is_gdx_error = true
          }).subscribe((data: any) => {
            console.log("GDX: Already deleted");
            pThis.is_gdx_error = false
            this.startGetIdCardTimer();
          })
        }
      }
      else {
        this.AutoFillFindID()
      }
    }
  }
  AutoFillFindID(): void {
    this.RegistrationRequestService.AutoFillFindID({
      officer_idcard: this.officerIdcard,
      citizen_idcard: this.input.id_card,
      auto_fill_token: this.autoFillToken,
      people_type: this.requestItem["people_code"],
    }).subscribe((data: any) => {
      console.log(data)
      if (data) {
        // If service return unauthorized
        // Clear officer id card and token
        if (data.return_status && data.return_status != "") {
          if (data.return_status == "unauthorized") {
            alert("Token หมดอายุกรุณาเสียบบัตรประชาชนและทำรายการใหม่อีกครั้ง")
          } else {
            alert("GDX Return Status: " + data.return_status)
          }
          this.officerIdcard = null
          this.autoFillToken = null
          this.onClickGDXSearch()
        } else {
          this.bindingAutoFill(data)
        }

      }
    })
  }

  onClickCheckBoxTable3(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_3_check_all = !this.input.is_table_3_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_3_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxItem(object: any, object_name: string, value: string): void { object[object_name] = !object[object_name] }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  oneWayDataBinding(name: any, value: any, object: any): void {
    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }

    if (name == "is_wave_fee") {
      this.listRequest01Item.forEach((item: any) => { item.reference_number = "" })
      this.updateItem()
    }
  }

   //reset
   reset(): void {
    this.ngOnInit()
  }

  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  
}