import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
    CONSTANTS,
    getMoment,
    clone,
    displayDateServer
} from '../../helpers'

@Component({
    selector: "app-document-role02-check-list",
    templateUrl: "./document-role02-check-list.component.html",
    styleUrls: ["./document-role02-check-list.component.scss"]
})
export class DocumentRole02CheckListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    // List DocumentRole02Check
    public listDocumentRole02Check: any[]
    public paginateDocumentRole02Check: any
    public perPageDocumentRole02Check: number[]
    // Response DocumentRole02Check
    public responseDocumentRole02Check: any
    public listDocumentRole02CheckEdit: any


    // List DocumentProcess
    public listDocumentProcess: any[]
    public paginateDocumentProcess: any
    public perPageDocumentProcess: number[]
    // Response DocumentProcess
    public responseDocumentProcess: any

    //label_save_combobox || label_save_radio
    public departmentCodeList: any[]
    public save010InstructionRuleDocumentReceiveStatusCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            //post_round_document_post_start_date: getMoment(),
            //post_round_document_post_end_date: getMoment(),
            department_code: '',
            document_role02_receive_status_code: '',
            request_number: '',
        }
        this.listDocumentRole02Check = []
        this.paginateDocumentRole02Check = clone(CONSTANTS.PAGINATION.INIT)
        this.paginateDocumentRole02Check.id = 'paginateDocumentRole02Check'
        this.perPageDocumentRole02Check = clone(CONSTANTS.PAGINATION.PER_PAGE)

        this.listDocumentProcess = []

        //Master List
        this.master = {
            departmentCodeList: [],
            save010InstructionRuleDocumentReceiveStatusCodeList: [],
        }
        //Master List



        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initDocumentRole02CheckList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.departmentCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.department_code = "ALL"
                this.master.save010InstructionRuleDocumentReceiveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.master.documentRole02ReceiveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.document_role02_receive_status_code = ""

            }
            if (this.editID) {
                this.DocumentProcessService.DocumentRole02CheckLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }

        })
    }

    constructor(
        private help: Help,
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }

    onClickDocumentRole02CheckList(): void {
        // if(this.validateDocumentRole02CheckList()) {
        // Open loading
        // Call api
        this.callDocumentRole02CheckList(this.saveData())
        // }
    }
    //! <<< Call API >>>
    callDocumentRole02CheckList(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.DocumentRole02CheckList(this.help.GetFilterParams(params, this.paginateDocumentRole02Check)).subscribe((data: any) => {
            // if(isValidDocumentRole02CheckListResponse(res)) {
            if (data) {
                // Set value
                this.listData(data)
                this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickDocumentRole02CheckAdd(): void {
        this.listDocumentRole02Check.push({
            index: this.listDocumentRole02Check.length + 1,
            request_number: null,
            post_round_document_post_date_text: null,
            created_by_name: null,
            rule_count: null,
            name: null,
            house_number: null,
            is_add_address: null,
            document_role02_receive_remark: null,
            document_receive_status_name: null,

        })
        this.changePaginateTotal(this.listDocumentRole02Check.length, 'paginateDocumentRole02Check')
    }

    onClickDocumentRole02CheckEdit(item: any): void {
        var win = window.open("/document-role02-check/edit/" + item.id)
    }


    onClickDocumentRole02CheckDelete(item: any): void {
        // if(this.validateDocumentRole02CheckDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listDocumentRole02Check.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listDocumentRole02Check.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listDocumentRole02Check.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {
                if (rs === true) rs = ""

                let ids = []

                for (let i = 0; i < this.listDocumentRole02Check.length; i++) {
                    if (this.listDocumentRole02Check[i].is_check) {
                        this.listDocumentRole02Check[i].cancel_reason = rs
                        this.listDocumentRole02Check[i].status_code = "DELETE"
                        this.listDocumentRole02Check[i].is_deleted = true

                        if (true && this.listDocumentRole02Check[i].id) ids.push(this.listDocumentRole02Check[i].id)
                        // else this.listDocumentRole02Check.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callDocumentRole02CheckDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listDocumentRole02Check.length; i++) {
                    this.listDocumentRole02Check[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callDocumentRole02CheckDelete(params: any, ids: any[]): void {
        this.DocumentProcessService.DocumentRole02CheckDelete(params).subscribe((data: any) => {
            // if(isValidDocumentRole02CheckDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listDocumentRole02Check.length; i++) {
                    if (this.listDocumentRole02Check[i].id == id) {
                        this.listDocumentRole02Check.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listDocumentRole02Check.length; i++) {
                this.listDocumentRole02Check[i] = i + 1
            }

            this.onClickDocumentRole02CheckList()
            // Close loading
            this.global.setLoading(false)
        })
    }


    // Modal Location
    autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
        if (object[name].length >= length) {
            console.log("d")
            this.is_autocomplete_ListModalLocation_show = true

            let params = {
                name: object[name],
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListModalLocation(params)
        }
    }
    autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_ListModalLocation_show = false
        }, 200)
    }
    callAutocompleteChangeListModalLocation(params: any): void {
        if (this.input.is_autocomplete_ListModalLocation_load) return
        this.input.is_autocomplete_ListModalLocation_load = true
        let pThis = this
        // this.DocumentProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
        //   if (data) {
        //     if (data.length == 1) {
        //       setTimeout(function () {
        //         pThis.autocompleteChooseListModalLocation(data[0])
        //       }, 200)
        //     } else {
        //       pThis.autocompleteListListModalLocation = data
        //     }
        //   }
        // })
        this.input.is_autocomplete_ListModalLocation_load = false
    }
    autocompleteChooseListModalLocation(data: any): void {
        this.inputModalAddress.address_sub_district_code = data.code
        this.inputModalAddress.address_sub_district_name = data.name
        this.inputModalAddress.address_district_code = data.district_code
        this.inputModalAddress.address_district_name = data.district_name
        this.inputModalAddress.address_province_code = data.province_code
        this.inputModalAddress.address_province_name = data.province_name
        this.inputModalAddress.address_country_code = data.country_code
        this.inputModalAddress.address_country_name = data.country_name
        this.is_autocomplete_ListModalLocation_show = false
    }
    onClickSave04ModalAddressSave(): void {
        Object.keys(this.inputModalAddress).forEach((item: any) => {
            this.inputModalAddressEdit[item] = this.inputModalAddress[item]
        })
        this.toggleModal("isModalPeopleEditOpen")
    }
    // Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listDocumentRole02Check = data.documentrole02check_list || []
        this.changePaginateTotal(this.listDocumentRole02Check.length, 'paginateDocumentRole02Check')

    }

    listData(data: any): void {
        this.listDocumentRole02Check = data.list || []
        this.help.PageSet(data, this.paginateDocumentRole02Check)
        //this.changePaginateTotal(this.listDocumentRole02Check.length, 'paginateDocumentRole02Check')

    }

    saveData(): any {
        // let params = this.input
        // params.documentrole02check_list = this.listDocumentRole02Check || []

        const params = {
            post_round_document_post_start_date: this.input.post_round_document_post_start_date,
            post_round_document_post_end_date: this.input.post_round_document_post_end_date,
            department_code: this.input.department_code,
            document_role02_receive_status_code: this.input.document_role02_receive_status_code,
            request_number: this.input.request_number,
            //page_index: +this.paginateDocumentRole02Check.currentPage,
            //item_per_page: +this.paginateDocumentRole02Check.itemsPerPage,
            //order_by: 'created_date',
            //is_order_reverse: false,
            //search_by: [{
            //  key: 'post_round_document_post_date',
            //  value: displayDateServer(this.input.post_round_document_post_start_date),
            //  operation: 3
            //}, {
            //  key: 'post_round_document_post_date',
            //  value: displayDateServer(this.input.post_round_document_post_end_date),
            //  operation: 4
            //}, {
            //  key: 'department_code',
            //  value: this.input.department_code,
            //  operation: 0
            //}, {
            //  key: 'document_role02_receive_status_code',
            //  value: this.input.document_role02_receive_status_code,
            //  operation: 0
            //}, {
            //  key: 'request_number',
            //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
            //  operation: 5
            //}]
        }

        return params
    }



    //! <<< ----------------- >>>
    //! <<< --- Copy Zone --- >>>
    //! <<< ----------------- >>>

    //! <<< Binding (one way) >>>
    binding(obj: any, name: any, value: any): void {
        this[obj][name] = value
    }

    //! <<< Table >>>
    onSelectColumnInTable(name: string, item: any): void {
        if (!item) {
            this.input.is_check_all = !this.input.is_check_all
            this[name].forEach((item: any) => {
                item.is_check = this.input.is_check_all
            })
        } else {
            item.is_check = !item.is_check
        }
    }
    onChangeInputInTable(item: any, name: any, value: any): void {
        item[name] = value
    }
    toggleInputInTable(item: any, name: any): void {
        item[name] = !item[name]
    }

    //! <<<< Pagination >>>
    managePaginateCallApi(name: string): void {
        if (name === 'paginateDocumentRole02Check') {
            this.onClickDocumentRole02CheckList()
            this.input.is_check_all = false
        }
    }
    onChangePage(page: any, name: string): void {
        this[name].currentPage = page === '' ? 1 : page
        if ((+page || page === '') && page <= this.getMaxPage(name)) {
            this.managePaginateCallApi(name)
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
        this.managePaginateCallApi(name)
    }
    changePaginateTotal(total: any, name: string): void {
        this[name].totalItems = total
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
