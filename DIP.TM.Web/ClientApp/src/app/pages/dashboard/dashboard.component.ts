import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'


@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.scss"]
})
export class DashboardComponent implements OnInit {

  //TODO >>> Declarations <<<

  constructor(
    private router: Router,
    private global: GlobalService
  ) { }

  ngOnInit() {
    //TODO >>> Init value <<<
  }

}
