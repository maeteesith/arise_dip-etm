/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MadridRole07SearchChangeComponent } from './madrid-role07-search-change.component';

describe('MadridRole07SearchChangeComponent', () => {
  let component: MadridRole07SearchChangeComponent;
  let fixture: ComponentFixture<MadridRole07SearchChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MadridRole07SearchChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MadridRole07SearchChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
