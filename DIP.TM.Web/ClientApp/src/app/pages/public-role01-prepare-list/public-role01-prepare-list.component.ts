import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role01-prepare-list",
  templateUrl: "./public-role01-prepare-list.component.html",
  styleUrls: ["./public-role01-prepare-list.component.scss"]
})
export class PublicRole01PrepareListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole01Check
  public listPublicRole01Check: any[]
  public paginatePublicRole01Check: any
  public perPagePublicRole01Check: number[]
  // Response PublicRole01Check
  public responsePublicRole01Check: any
  public listPublicRole01CheckEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public requestItemSubType1CodeList: any[]
  public addressNationalityCodeList: any[]
  //label_save_combobox || label_save_radio

  public url_param: any

  public popup: any

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //public_send_start_date: getMoment(),
      //public_send_end_date: getMoment(),
      request_item_sub_type_1_code: '',
      address_nationality_code: '',
      request_number: '',
    }
    this.listPublicRole01Check = []
    this.paginatePublicRole01Check = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole01Check.id = 'paginatePublicRole01Check'
    this.perPagePublicRole01Check = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      requestItemSubType1CodeList: [],
      addressNationalityCodeList: [],
    }
    //Master List


    this.url_param = {}

    this.popup = {}

    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole01PrepareList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.requestItemSubType1CodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.master.publicReceiveDoStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.public_receive_do_status_code = ""
        // this.input.request_item_sub_type_1_code = "ALL"


        this.popup["department_code"] = this.master.publicRole01SendBackDepartmentList[0].code

      }
      if (this.editID) {
        this.PublicProcessService.PublicRole01PrepareLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.route.queryParams.subscribe((param_url: any) => {
          this.url_param = {}
          var is_have_param = false
          Object.keys(param_url).forEach((item: any) => {
            if (item != "test" && item != "save_id") {
              this.url_param[item] = param_url[item]
              //var param = {
              //  key: item,
              //  value: param_url[item],
              //}
              //params.search_by.push(param)
              is_have_param = true
            }
          })
          if (is_have_param)
            this.callPublicRole01PrepareList(this.url_param)
        })

        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }

  onClickPublicRole01PrepareList(): void {
    // if(this.validatePublicRole01PrepareList()) {
    // Open loading
    // Call api
    this.callPublicRole01PrepareList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole01PrepareList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole01PrepareList(this.help.GetFilterParams(params, this.paginatePublicRole01Check)).subscribe((data: any) => {
      // if(isValidPublicRole01PrepareListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: 1 })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickPublicRole01PrepareAdd(): void {
    this.listPublicRole01Check.push({
      index: this.listPublicRole01Check.length + 1,
      request_number: null,
      request_item_sub_type_1_code: null,
      address_nationality_code: null,
      request_date_text: null,
      trademark: null,
      trademark_sound: null,
      public_send_date: null,
      public_remark: null,
      public_do_status_name: null,

    })
    this.changePaginateTotal(this.listPublicRole01Check.length, 'paginatePublicRole01Check')
  }

  onClickPublicRole01PrepareEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickPublicRole01PrepareDelete(row_item: any = null): void {
    if (this.listPublicRole01Check.filter(r => r.is_check).length > 0 || row_item) {
      if (row_item) {
        this.listPublicRole01Check.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = item == row_item
        })
      }

      this.popup.is_send_back_show = true
      this.popup.send_back_item_list = this.listPublicRole01Check.filter(r => r.is_check)
    }
  }
  onClickSendChange(): void {
    this.callPublicRole01PrepareDelete({
      department_code: this.popup.department_code,
      cancel_reason: this.popup.reason,
      ids: this.listPublicRole01Check.map((item: any) => { return item.id }),
    })
  }
  //! <<< Call API >>>
  callPublicRole01PrepareDelete(params: any): void {
    this.PublicProcessService.PublicRole01PrepareDelete(params).subscribe((data: any) => {
      this.onClickPublicRole01PrepareList()
      // Close loading
      this.popup.is_send_back_show = false
      this.global.setLoading(false)
    })
  }


  onClickPublicRole01PreparePublicRoundAdd(): void {
    this.callPublicRole01PreparePublicRoundAdd(this.saveData())
  }
  //! <<< Call API >>>
  callPublicRole01PreparePublicRoundAdd(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole01PreparePublicRoundAdd().subscribe((data: any) => {
      this.onClickPublicRole01PrepareList()
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicRole01Check = data.publicrole01check_list || []
    this.changePaginateTotal(this.listPublicRole01Check.length, 'paginatePublicRole01Check')

  }

  listData(data: any): void {
    this.listPublicRole01Check = data.list || []
    this.help.PageSet(data, this.paginatePublicRole01Check)
    //this.listPublicRole01Check = data.list || []
    //this.changePaginateTotal(this.listPublicRole01Check.length, 'paginatePublicRole01Check')

  }

  saveData(): any {
    // let params = this.input
    // params.publicrole01check_list = this.listPublicRole01Check || []

    const params = {
      public_send_start_date: this.input.public_send_start_date,
      public_send_end_date: this.input.public_send_end_date,
      request_item_sub_type_1_code: this.input.request_item_sub_type_1_code,
      //address_nationality_code: this.input.address_nationality_code,
      request_number: this.input.request_number,
      public_receive_do_status_code: this.input.public_receive_do_status_code,
    }

    //const params = {
    //  page_index: +this.paginatePublicRole01Check.currentPage, 
    //  item_per_page: +this.paginatePublicRole01Check.itemsPerPage,
    //  order_by: 'created_date',
    //  is_order_reverse: false,
    //  search_by: [{
    //    key: 'public_send_date',
    //    value: displayDateServer(this.input.public_send_start_date),
    //    operation: 3
    //  }, {
    //    key: 'public_send_date',
    //    value: displayDateServer(this.input.public_send_end_date),
    //    operation: 4
    //  }, {
    //    key: 'request_item_sub_type_1_code',
    //    value: this.input.request_item_sub_type_1_code,
    //    operation: 0
    //  }, {
    //    key: 'address_nationality_code',
    //    value: this.input.address_nationality_code,
    //    operation: 0
    //  }, {
    //    key: 'request_number',
    //    values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
    //    operation: 5
    //  }]
    //}

    return params
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicRole01Check') {
      this.onClickPublicRole01PrepareList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
