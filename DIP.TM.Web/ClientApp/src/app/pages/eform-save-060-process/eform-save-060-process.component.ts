import { Component, OnInit, HostListener } from "@angular/core";
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from '../../services/fork-join2.service'
import { eFormSaveProcessService } from '../../services/eform-save-process.service'
import { ActivatedRoute, Router } from '@angular/router';
import {
  ROUTE_PATH,
  validateService,
  clone,
  displayMoney,
  displayFormatBytes,
  viewPDF,
  getParamsOverwrite,
  CONSTANTS,
} from "../../helpers";

@Component({
  selector: "app-eform-save-060-process",
  templateUrl: "./eform-save-060-process.component.html",
  styleUrls: ["./eform-save-060-process.component.scss"]
})
export class eFormSave060ProcessComponent implements OnInit, DeactivationGuarded {

  // Init
  public editID: any;
  public menuList: any[];
  public requestItemTypeCodeList: any[];
  public userList: any[];
  public userList3: any[];
  public input: any;
  public validate: any;
  public master: any;
  public modal: any;
  public response: any;
  public popup: any;
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubstep: number;
  public progressPercent: number;
  // Other
  public isSubmited: any;
  public isValid: any;

  public findVal: any;
  public saveInput: any;

  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eFormSaveProcessService: eFormSaveProcessService
  ) { }

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.menuList = [
      {
        id: 1,
        number: 1,
        name: 'บันทึกเพื่อรับไฟล์',
        required: true,
        canedit: false
      },
      {
        id: 2,
        number: 2,
        name: 'บันทึกคำขอแก้ไขเปลี่ยนแปลงคำขอเลขที่',
        required: true,
        canedit: false
      },
      {
        id: 3,
        number: 3,
        name: 'ประเภทเครื่องหมาย',
        required: true,
        canedit: false
      },
      {
        id: 4,
        number: 4,
        name: 'รายการเปลี่ยนแปลง',
        required: true,
        canedit: false
      },
      {
        id: 5,
        number: 5,
        name: 'เจ้าของเดิม',
        canedit: false
      },
      {
        id: 6,
        number: 6,
        name: 'การเปลี่ยนแปลงข้อมูล',
        canedit: false
      },
      {
        id: 7,
        number: 7,
        name: 'รายละเอียดการเปลี่ยนแปลง รายการจดทะเบียน',
        canedit: false
      },
      {
        id: 8,
        number: 8,
        name: 'เอกสารหลักฐานประกอบคำขอแก้ไข (บางรายการ)',
        canedit: false
      },
      {
        id: 9,
        number: 9,
        name: 'ค่าธรรมเนียม',
        canedit: false
      },
      {
        id: 10,
        number: 10,
        name: 'เสร็จสิ้น',
        canedit: false
      },
    ]
    this.response = {
      load: {},
    };
    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
      isSearch: false,
      isWarning: false,
    };
    this.requestItemTypeCodeList = [
      {
        code: 1,
        name: 'จดทะเบียนแล้ว'
      },
      {
        code: 2,
        name: 'ต่ออายุ'
      },
      {
        code: 3,
        name: 'แต่งตั้งตัวแทนผู้รับโอนหลังจดทะเบียน'
      },
    ]

    this.userList = [
      {
        id: 1,
        name: 'nanpipat klinpratoom',
        address: 'samutprakarn',
        tel: '0858455652'
      },
      {
        id: 2,
        name: 'toptop  toptopt',
        address: 'asdsadas',
        tel: '08900'
      },


    ]
    this.userList3 = [];

    this.input = {
      indexEdit: undefined,
      listOwnerMark: [],
      ownerMarkItem: {},
      ownerMarkItem2: {},
      listAgentMark: [],
      agentMarkItem: {},
      ownerDomesticContactAddress: 0,
      agentDomesticContactAddress: 0,
      subagentDomesticContactAddress: 0,
      dimension_image: 2,
      imageDimension2: {
        file: {},
        blob: null
      },
      imageDimension3_1: {
        file: {},
        blob: null
      },
      imageDimension3_2: {
        file: {},
        blob: null
      },
      imageDimension3_3: {
        file: {},
        blob: null
      },
      imageDimension3_4: {
        file: {},
        blob: null
      },
      sizeX: 5,
      sizeY: 5,
      isResize: false,
      audio: {
        input: "",
        validate: "",
        file: {},
        sound: null,
        palyer: null,
        action: false
      },
      imageNote: {
        file: {},
        blob: null
      },
      listTranslation: [],
      translationItem: {},
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0
      },
      imageMark: {
        file: {},
        blob: null
      }
    };
    this.validate = {};
    this.master = {
      requestSoundTypeList: [],
      inputTypeCodeList: [],
      requestSourceCodeList: [],
      saveOTOPTypeCodeList: [],
      addressCountryCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
      languageCodeList: [],
      priceMasterList: []
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalOwnerFormOpen2: false,
      isModalAgentFormOpen2: false,
      isModalDomesticContactAddressOpen: true
    };
    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubstep = 1;
    this.progressPercent = 0;
    // Other
    this.isSubmited = false;
    this.isValid = false;
    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;

  }

  callInit(): void {
  }

  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }

  save(isOverwrite: boolean): void {
    //Open loading
    // this.global.setLoading(true);
    // Set param
    // this.getParamsSave();
    // if (!isOverwrite) {
    //   this.saveInput = getParamsOverwrite(this.saveInput);
    // }

    //Call api
    // this.calleFormSave060Save(this.saveInput);
  }

  getParamsSave():void {
    this.saveInput = {};
  }

  calleFormSave060Save(params: any): void {
    // this.global.setLoading(true)
    // this.eFormSaveProcessService.eFormSave060Save(params).subscribe((data: any) => {
    //   if (data) {
    //     this.isDeactivation = true;
    //     // Open toast success
    //     let toast = CONSTANTS.TOAST.SUCCESS;
    //     toast.message = "บันทึกข้อมูลสำเร็จ";
    //     this.global.setToast(toast);
    //     // Navigate
    //     this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
    //   }else {
    //     // Close loading
    //     this.global.setLoading(false);
    //   }      
    // })
  }

  sendEmail(): void {
    // this.global.setLoading(true);
    // this.getParamsSave();
    
    // console.log("SaveInput", this.saveInput);

    // this.eFormSaveProcessService.eFormSave060Save(this.saveInput).subscribe((data: any) => {
    //     if (data) {
    //       this.response.load = {
    //         eform_number: data.eform_number,
    //         id: data.id,
    //       };
    //     }
    //     // Close loading
    //     this.global.setLoading(false);
    // })
  }

  onClickViewPdfKor06(): void {
    if (this.validateWizard()) {
      this.getParamsSave();
      console.log(this.saveInput)
      viewPDF("ViewPDF/TM06", this.saveInput);
    }
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name];
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null;
        }
      });
    } else {
      this.validate[name] = null;
    }
  }

  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentStep === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      // Check certification mark
      if (result.isValid) {
        if (this.input.request_item_type_code !== "CERTIFICATION_MARK") {
          this.menuList.splice(11, 1)
          this.menuList.forEach((item: any, index: any) => {
            item.number = index + 1
          });
        }
      }

      return result.isValid;
    }
    return true;
  }

  //! <<< Wizard >>>
  changeStep(action: string): void {
    //if (this.validateWizard()) {
    console.log(this.currentStep);
    this.menuList[this.currentStep-1].canedit = true;

    action === "next" ? this.currentStep++ : this.currentStep--;
    this.currentID = this.menuList[this.currentStep - 1].id;
    
    this.currentSubstep = 1;
    this.calcProgressPercent(this.currentStep);

    
    //}
  }
  changeSubStep(action: number): void {
    if (this.validateWizard()) {
      this.currentSubstep = this.currentSubstep + action;
    }
  }

  backtomenu(canedit: any, number: any): void {
    console.log(canedit);
    if (canedit == true) {
      this.currentStep = number;
      this.currentID = this.menuList[this.currentStep - 1].id

      for (var i = 0; i < this.menuList.length; i++) {
        if ((i + 1) >= number) {
          this.menuList[i].canedit = false;
        }
      }
    }
   
  }
  calcProgressPercent(currentStep: number): void {
    this.progressPercent = Math.round((currentStep / this.menuList.length) * 100);
  }

  findClick(): void {
    this.findVal = true;
  }

  //! <<< Other >>>
  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }
  displayFormatBytes(value: any): any {
    return displayFormatBytes(value);
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined): void {
   
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if(name === "isModalOwnerFormOpen"){
          this.input.ownerMarkItem = clone(this.input.listOwnerMark[index]);
        }else if (name === "isModalOwnerFormOpen2") {
          this.input.ownerMarkItem2 = clone(this.input.listOwnerMark[index]);
        } else if (name === "isModalAgentFormOpen2") {
          this.input.agentMarkItem = clone(this.input.listAgentMark[index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {};
      this.input.ownerMarkItem2 = {};
      this.input.agentMarkItem = {};
    }
  }

  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }
}
