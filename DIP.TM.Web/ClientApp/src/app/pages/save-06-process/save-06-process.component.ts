import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-save-06-process",
  templateUrl: "./save-06-process.component.html",
  styleUrls: ["./save-06-process.component.scss"]
})
export class Save06ProcessComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  // List People01
  public listPeople01: any[]
  public paginatePeople01: any
  public perPagePeople01: number[]
  // Response People01
  public responsePeople01: any
  public listPeople01Edit: any

  // List Representative01
  public listRepresentative01: any[]
  public paginateRepresentative01: any
  public perPageRepresentative01: number[]
  // Response Representative01
  public responseRepresentative01: any
  public listRepresentative01Edit: any

  // List Product01
  public listProduct01: any[]
  public paginateProduct01: any
  public perPageProduct01: number[]
  // Response Product01
  public responseProduct01: any
  public listProduct01Edit: any

  // List People
  public listPeople: any[]
  public paginatePeople: any
  public perPagePeople: number[]
  // Response People
  public responsePeople: any
  public listPeopleEdit: any

  // List Representative
  public listRepresentative: any[]
  public paginateRepresentative: any
  public perPageRepresentative: number[]
  // Response Representative
  public responseRepresentative: any
  public listRepresentativeEdit: any

  // List ContactAddress
  public listContactAddress: any[]
  public paginateContactAddress: any
  public perPageContactAddress: number[]
  // Response ContactAddress
  public responseContactAddress: any
  public listContactAddressEdit: any

  // List Product
  public listProduct: any[]
  public paginateProduct: any
  public perPageProduct: number[]
  // Response Product
  public responseProduct: any
  public listProductEdit: any

  // List TrademarkWarranty
  public listTrademarkWarranty: any[]
  public paginateTrademarkWarranty: any
  public perPageTrademarkWarranty: number[]
  // Response TrademarkWarranty
  public responseTrademarkWarranty: any
  public listTrademarkWarrantyEdit: any

  // List Joiner
  public listJoiner: any[]
  public paginateJoiner: any
  public perPageJoiner: number[]
  // Response Joiner
  public responseJoiner: any
  public listJoinerEdit: any

  // List AllowPeople
  public listAllowPeople: any[]
  public paginateAllowPeople: any
  public perPageAllowPeople: number[]
  // Response AllowPeople
  public responseAllowPeople: any
  public listAllowPeopleEdit: any

  //// List ContactAddress06
  //public listContactAddress06: any[]
  //public paginateContactAddress06: any
  //public perPageContactAddress06: number[]
  //// Response ContactAddress06
  //public responseContactAddress06: any
  //public listContactAddress06Edit: any


  //label_save_combobox || label_save_radio
  public save060PeriodRequestCodeList: any[]
  public addressTypeCodeList: any[]
  public addressCardTypeCodeList: any[]
  public addressReceiverTypeCodeList: any[]
  public addressSexCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  public autocompleteListListLocation: any
  public is_autocomplete_ListLocation_show: any
  public is_autocomplete_ListLocation_load: any
  //Modal Initial
  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any
  public inputAddress2: any
  //Modal Initial

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  public contactAddress06: any

  public product_select: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      register_number: '',
      make_date: getMoment(),
      save060_period_request_code: '',
      modify_description: '',
      is_oppose_modify_description: false,
      request_date_text: '',
      total_price: '',
      request_index: '',
      irn_number: '',
    }
    this.listPeople01 = []
    this.paginatePeople01 = CONSTANTS.PAGINATION.INIT
    this.perPagePeople01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listRepresentative01 = []
    this.paginateRepresentative01 = CONSTANTS.PAGINATION.INIT
    this.perPageRepresentative01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct01 = []
    this.paginateProduct01 = CONSTANTS.PAGINATION.INIT
    this.perPageProduct01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listPeople = []
    this.paginatePeople = CONSTANTS.PAGINATION.INIT
    this.perPagePeople = CONSTANTS.PAGINATION.PER_PAGE

    this.listRepresentative = []
    this.paginateRepresentative = CONSTANTS.PAGINATION.INIT
    this.perPageRepresentative = CONSTANTS.PAGINATION.PER_PAGE

    this.listContactAddress = []
    this.paginateContactAddress = CONSTANTS.PAGINATION.INIT
    this.perPageContactAddress = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct = []
    this.paginateProduct = CONSTANTS.PAGINATION.INIT
    this.perPageProduct = CONSTANTS.PAGINATION.PER_PAGE

    this.listTrademarkWarranty = []
    this.paginateTrademarkWarranty = CONSTANTS.PAGINATION.INIT
    this.perPageTrademarkWarranty = CONSTANTS.PAGINATION.PER_PAGE

    this.listJoiner = []
    this.paginateJoiner = CONSTANTS.PAGINATION.INIT
    this.perPageJoiner = CONSTANTS.PAGINATION.PER_PAGE

    this.listAllowPeople = []
    this.paginateAllowPeople = CONSTANTS.PAGINATION.INIT
    this.perPageAllowPeople = CONSTANTS.PAGINATION.PER_PAGE

    //this.listContactAddress06 = []
    //this.paginateContactAddress06 = CONSTANTS.PAGINATION.INIT
    //this.perPageContactAddress06 = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
      save060PeriodRequestCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false
    this.autocompleteListListLocation = []
    this.is_autocomplete_ListLocation_show = false
    this.is_autocomplete_ListLocation_load = false
    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputAddress2 = {}
    this.inputModalAddress = {}


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.contactAddress06 = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave06Process().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save06Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          this.automateTest.test(this)
          pThis.global.setLoading(false)
        })
      } else {
        this.automateTest.test(this)
        this.global.setLoading(false)
      }
    })
  }

  constructor(
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private help: Help,
    private SaveProcessService: SaveProcessService
  ) { }


  autocompleteKeyDown(event: any) {
    if (event.key == "Enter") {
      this.autocompleteChangeListNotSent(this.input, 'request_number', 5, 1)
    }
  }

  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: { request_number: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    this.global.setLoading(true)
    let pThis = this
    this.SaveProcessService.Save06ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListNotSent = data
          this.input.is_autocomplete_ListNotSent_load = false
          this.global.setLoading(false)
        }
      }
    })
  }
  autocompleteChooseListNotSent(data: any): void {
    this.loadData(data)

    this.is_autocomplete_ListNotSent_show = false
    this.global.setLoading(false)
  }


  onClickSave06Madrid(): void {
    //if(this.validateSave06Madrid()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave06Madrid(this.saveData())
    //}
  }
  validateSave06Madrid(): boolean {
    let result = validateService('validateSave06Madrid', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave06Madrid(params: any): void {
    let pThis = this
    this.SaveProcessService.Save06Madrid(params).subscribe((data: any) => {
      // if(isValidSave06MadridResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  // Autocomplete
  autocompleteChangeListLocation(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListLocation(params)
    }
  }
  autocompleteBlurListLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListLocation(params: any): void {
    if (this.input.is_autocomplete_ListLocation_load) return
    this.input.is_autocomplete_ListLocation_load = true
    let pThis = this
    this.SaveProcessService.Save06ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListLocation_load = false
  }
  autocompleteChooseListLocation(data: any): void {
    this.inputAddress.address_sub_district_code = data.code
    this.inputAddress.address_sub_district_name = data.name
    this.inputAddress.address_district_code = data.district_code
    this.inputAddress.address_district_name = data.district_name
    this.inputAddress.address_province_code = data.province_code
    this.inputAddress.address_province_name = data.province_name
    this.inputAddress.address_country_code = data.country_code
    this.inputAddress.address_country_name = data.country_name

    this.is_autocomplete_ListLocation_show = false
  }

  // Autocomplete
  autocompleteChangeListLocation2(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListLocation2(params)
    }
  }
  autocompleteBlurListLocation2(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListLocation2(params: any): void {
    if (this.input.is_autocomplete_ListLocation_load) return
    this.input.is_autocomplete_ListLocation_load = true
    this.global.setLoading(true)
    let pThis = this
    this.SaveProcessService.Save06ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListLocation2(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListLocation = data
          this.global.setLoading(false)
        }
      }
    })
    this.input.is_autocomplete_ListLocation_load = false
  }
  autocompleteChooseListLocation2(data: any): void {
    this.inputAddress2.address_sub_district_code = data.code
    this.inputAddress2.address_sub_district_name = data.name
    this.inputAddress2.address_district_code = data.district_code
    this.inputAddress2.address_district_name = data.district_name
    this.inputAddress2.address_province_code = data.province_code
    this.inputAddress2.address_province_name = data.province_name
    this.inputAddress2.address_country_code = data.country_code
    this.inputAddress2.address_country_name = data.country_name

    this.is_autocomplete_ListLocation_show = false
    this.global.setLoading(false)
  }

  onClickSave06Delete(): void {
    //if(this.validateSave06Delete()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave06Delete(this.saveData())
    //}
  }
  validateSave06Delete(): boolean {
    let result = validateService('validateSave06Delete', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave06Delete(params: any): void {
    let pThis = this
    this.SaveProcessService.Save06Delete(params).subscribe((data: any) => {
      // if(isValidSave06DeleteResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave06Save(): void {
    //if(this.validateSave06Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave06Save(this.saveData())
    //}
  }
  validateSave06Save(): boolean {
    let result = validateService('validateSave06Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave06Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save06Save(params).subscribe((data: any) => {
      // if(isValidSave06SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickSave06FileScanView(): void {
    window.open('/File/RequestDocumentCollect/60/' + this.input.request_number + "_" + this.input.request_id)
    ////if(this.validateSave06FileScanView()) {
    //// Open loading
    //this.global.setLoading(true)
    //// Call api
    //this.callSave06FileScanView(this.saveData())
    ////}
  }
  validateSave06FileScanView(): boolean {
    let result = validateService('validateSave06FileScanView', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave06FileScanView(params: any): void {
    let pThis = this
    this.SaveProcessService.Save06FileScanView(params).subscribe((data: any) => {
      // if(isValidSave06FileScanViewResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave06Send(): void {
    if (!this.input.save060_period_request_code) {
      this.input.save060_period_request_code =
        this.input.registration_number &&
          this.input.registration_number != "" ? "TYPE2" : "TYPE1"
    }

    if (this.input.save060_period_request_code == "TYPE2") {
      this.input.select_save060_send_department_code = "EXTEND"
      this.toggleModal('isModalDeparementOpen')
    } else {
      this.global.setLoading(true)
      this.callSave06Send(this.saveData())
    }
    ////if(this.validateSave06Send()) {
    //// Open loading
    //// Call api
    ////}
  }

  onClickDeparmentSend(): void {
    this.toggleModal('isModalDeparementOpen')
    ////if(this.validateSave06Send()) {
    //// Open loading
    this.global.setLoading(true)
    var params = this.saveData()
    params.save060_send_department_code = this.input.select_save060_send_department_code
    //// Call api
    this.callSave06Send(params)
    ////}
  }

  validateSave06Send(): boolean {
    let result = validateService('validateSave06Send', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave06Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save06Send(params).subscribe((data: any) => {
      // if(isValidSave06SendResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }


  onClickSave060RequestBack(): void {
    //if(this.validateSave060Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave060RequestBack(this.saveData())
    //}
  }
  callSave060RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save060RequestBack(params).subscribe((data: any) => {
      // if(isValidSave060SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  onClickSave06WorkGroupChange(): void {
    //if(this.validateSave06WorkGroupChange()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave06WorkGroupChange(this.saveData())
    //}
  }
  validateSave06WorkGroupChange(): boolean {
    let result = validateService('validateSave06WorkGroupChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave06WorkGroupChange(params: any): void {
    let pThis = this
    this.SaveProcessService.Save06WorkGroupChange(params).subscribe((data: any) => {
      // if(isValidSave06WorkGroupChangeResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave06Product01Add(): void {
    this.listProduct01.push({
      index: this.listProduct01.length + 1,
      request_item_sub_type_1_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
  }


  onClickSave06ProductDelete(item: any): void {
    // if(this.validateSave06ProductDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listProduct.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listProduct.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listProduct.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < this.listProduct.length; i++) {
          if (this.listProduct[i].is_check) {
            this.listProduct[i].cancel_reason = rs
            this.listProduct[i].status_code = "DELETE"
            this.listProduct[i].is_deleted = true

            if (true && this.listProduct[i].id) ids.push(this.listProduct[i].id)
            //else this.listProduct.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave06ProductDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listProduct.length; i++) {
          this.listProduct[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callSave06ProductDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save06ProductDelete(params).subscribe((data: any) => {
      // if(isValidSave06ProductDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listProduct.length; i++) {
          if (this.listProduct[i].id == id) {
            this.listProduct.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listProduct.length; i++) {
        this.listProduct[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave06TrademarkWarrantyAdd(): void {
    this.listTrademarkWarranty.push({

    })
    this.changePaginateTotal(this.listTrademarkWarranty.length, 'paginateTrademarkWarranty')
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    this.global.setLoading(true)
    let pThis = this
    this.SaveProcessService.Save06ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListModalLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListModalLocation = data
          this.input.is_autocomplete_ListModalLocation_load = false
          this.global.setLoading(false)
        }
      }
    })
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
    this.global.setLoading(false)
  }
  onClickSave06ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  onClickSave06PeopleAdd(): void {
    this.listPeople.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listPeople[this.listPeople.length - 1])
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
  }
  onClickSave06RepresentativeAdd(): void {
    this.listRepresentative.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listRepresentative[this.listRepresentative.length - 1], 'ตัวแทน')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
  }
  onClickSave06JoinerAdd(): void {
    this.listJoiner.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listJoiner[this.listJoiner.length - 1], 'ผู้ร่วมใช้')
    this.changePaginateTotal(this.listJoiner.length, 'paginateJoiner')
  }
  onClickSave06AllowPeopleAdd(): void {
    this.listAllowPeople.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listJoiner[this.listAllowPeople.length - 1], 'ผู้ได้รับอนุญาต')
    this.changePaginateTotal(this.listAllowPeople.length, 'paginateJoiner')
  }

  onClickAddressCopy(item_list: any[], copy_item: any): void {
    var index = 1
    item_list.forEach((item: any) => {
      item.index = index++
      if (item === copy_item) {
        var item_new = { ...item }
        item_new.id = null
        item_new.index = index++
        item_list.splice(index + 1, 0, item_new)
      }
    })
  }
  onClickAddressEdit(item: any, name: string = "เจ้าของ"): void {
    this.modalPeopleText = name
    this.modalAddressEdit = item
    this.help.Clone(item, this.modalAddress)

    this.modalAddress.is_representative = name.indexOf("ตัวแทน") >= 0

    this.modal["isModalPeopleEditOpen"] = true

  }
  onClickAddressSave(): void {
    this.help.Clone(this.modalAddress, this.modalAddressEdit)
    this.help.GetAddressInformation(this.modalAddressEdit)

    this.modal["isModalPeopleEditOpen"] = false
  }
  onClickAddressDelete(item_list: any[], item: any): void {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (item_list.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        item_list.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = item_list.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < item_list.length; i++) {
          if (item_list[i].is_check) {
            //item_list[i].cancel_reason = rs
            item_list[i].is_deleted = true
            //console.log(item_list[i])
            //item_list[i].status_code = "DELETE"

            if (true && item_list[i].id) ids.push(item_list[i].id)
            else item_list.splice(i--, 1);
          }
        }
      }
    }
    //console.log(item_list)
    this.global.setLoading(false)
  }

  onClickSave06ProductAdd(): void {
    this.listProduct.push({
      //index: this.listProduct.length + 1,
      request_item_sub_type_1_code: null,
      request_item_sub_type_2_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    if (!this.input.save060_period_request_code) {
      this.input.save060_period_request_code =
        this.input.registration_number &&
          this.input.registration_number != "" ? "TYPE2" : "TYPE1"
    }

    this.inputAddress2 = data.contact_address_06 || {}

    this.listPeople = data.people_list || []
    this.listRepresentative = data.representative_list || []
    this.help.Clone(data.contact_address, this.contactAddress)
    this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"
    this.listJoiner = data.joiner_list || []

    //this.help.Clone(data.contact_address_06, this.contactAddress)

    this.listAllowPeople = data.allow_people_list || []
    this.help.Clone(data.contact_address_06, this.contactAddress06)
    this.contactAddress06.address_type_code = this.contactAddress06.address_type_code || "OWNER"

    this.listPeople01 = data.people_01_list || []
    console.log(data.people_01_list)
    console.log(this.listPeople01)
    this.listRepresentative01 = data.representative_01_list || []
    this.listProduct01 = data.product_01_list || []

    data.product_list = data.product_list || []
    this.listProduct = []
    data.product_list.filter(r => !r.is_deleted).forEach((item: any) => {
      var product_list = this.listProduct.filter(r => r.request_item_sub_type_1_code == item.request_item_sub_type_1_code)
      if (product_list.length > 0) product_list[0].description += "  " + item.description
      else this.listProduct.push({
        request_item_sub_type_1_code: item.request_item_sub_type_1_code,
        description: item.description,
      })
    })

    this.listTrademarkWarranty = data.trademark_warranty_list || []
    //this.listPeople01.map((item: any) => { item.is_check = false; return item })
    //this.listRepresentative01.map((item: any) => { item.is_check = false; return item })
    //this.listProduct01.map((item: any) => { item.is_check = false; return item })
    //this.listPeople.map((item: any) => { item.is_check = false; return item })
    //this.listRepresentative.map((item: any) => { item.is_check = false; return item })
    //this.listContactAddress.map((item: any) => { item.is_check = false; return item })
    //this.listProduct.map((item: any) => { item.is_check = false; return item })
    //this.listTrademarkWarranty.map((item: any) => { item.is_check = false; return item })
    //this.listJoiner.map((item: any) => { item.is_check = false; return item })
    //this.listAllowPeople.map((item: any) => { item.is_check = false; return item })
    //this.listContactAddress06.map((item: any) => { item.is_check = false; return item })

    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
    this.changePaginateTotal(this.listRepresentative01.length, 'paginateRepresentative01')
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
    this.changePaginateTotal(this.listContactAddress.length, 'paginateContactAddress')
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
    this.changePaginateTotal(this.listTrademarkWarranty.length, 'paginateTrademarkWarranty')
    this.changePaginateTotal(this.listJoiner.length, 'paginateJoiner')
    this.changePaginateTotal(this.listAllowPeople.length, 'paginateAllowPeople')
    //this.changePaginateTotal(this.listContactAddress06.length, 'paginateContactAddress06')

  }

  listData(data: any): void {
    this.listPeople01 = data || []
    this.listRepresentative01 = data || []
    this.listProduct01 = data || []
    this.listPeople = data || []
    this.listRepresentative = data || []
    this.listContactAddress = data || []
    this.listProduct = data || []
    this.listTrademarkWarranty = data || []
    this.listJoiner = data || []
    this.listAllowPeople = data || []
    //this.listContactAddress06 = data || []
    //let index = 1
    //index = 1
    //this.listPeople01.map((item: any) => { item.is_check = false; return item })
    //this.listRepresentative01.map((item: any) => { item.is_check = false; return item })
    //this.listProduct01.map((item: any) => { item.is_check = false; return item })
    //this.listPeople.map((item: any) => { item.is_check = false; return item })
    //this.listRepresentative.map((item: any) => { item.is_check = false; return item })
    //this.listContactAddress.map((item: any) => { item.is_check = false; return item })
    //this.listProduct.map((item: any) => { item.is_check = false; return item })
    //this.listTrademarkWarranty.map((item: any) => { item.is_check = false; return item })
    //this.listJoiner.map((item: any) => { item.is_check = false; return item })
    //this.listAllowPeople.map((item: any) => { item.is_check = false; return item })
    //this.listContactAddress06.map((item: any) => { item.is_check = false; return item })
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
    this.changePaginateTotal(this.listRepresentative01.length, 'paginateRepresentative01')
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
    this.changePaginateTotal(this.listContactAddress.length, 'paginateContactAddress')
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
    this.changePaginateTotal(this.listTrademarkWarranty.length, 'paginateTrademarkWarranty')
    this.changePaginateTotal(this.listJoiner.length, 'paginateJoiner')
    this.changePaginateTotal(this.listAllowPeople.length, 'paginateAllowPeople')
    //this.changePaginateTotal(this.listContactAddress06.length, 'paginateContactAddress06')

  }

  saveData(): any {
    let params = this.input

    params.people_list = this.listPeople
    params.representative_list = this.listRepresentative
    params.contact_address = this.contactAddress

    params.contact_address_06 = this.contactAddress06 || []
    params.allow_people_list = this.listAllowPeople || []
    params.joiner_list = this.listJoiner || []

    //params.people_01_list = this.listPeople01 || []
    //params.representative_01_list = this.listRepresentative01 || []
    //params.product_01_list = this.listProduct01 || []

    params.product_list = []
    this.listProduct.forEach((item: any) => {
      item.description.split("  ").forEach((product: any) => {
        params.product_list.push({
          request_item_sub_type_1_code: item.request_item_sub_type_1_code,
          description: product,
        })
      })
    })

    params.trademark_warranty_list = this.listTrademarkWarranty || []
    //params.contact_address_06_list = this.listContactAddress06 || []

    return params
  }


  onClickProductSelect(row_item: any, is_selected = true) {
    if (is_selected) {
      var pThis = this
      setTimeout(function () {
        row_item.description = row_item.description.replace(/  /g, '\n').trim()
        pThis.product_select = row_item
      }, 100)
    } else {
      if (this.product_select == row_item) {
        row_item.description = row_item.description.replace(/\n/g, '  ').trim()
        this.product_select = null
      }
    }
  }


  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
