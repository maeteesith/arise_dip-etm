import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-save-04-process-list",
    templateUrl: "./save-04-process-list.component.html",
    styleUrls: ["./save-04-process-list.component.scss"]
})
export class Save04ProcessListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public modal: any

    // List SaveProcess
    public listSaveProcess: any[]
    public paginateSaveProcess: any
    public perPageSaveProcess: number[]
    // Response SaveProcess
    public responseSaveProcess: any
    public listSaveProcessEdit: any


    //label_save_combobox || label_save_radio
    public saveStatusCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            request_number: '',
            index: '',
            make_date: getMoment(),
            save_status_code: '',
        }
        this.listSaveProcess = []
        this.paginateSaveProcess = CONSTANTS.PAGINATION.INIT
        this.perPageSaveProcess = CONSTANTS.PAGINATION.PER_PAGE


        //Master List
        this.master = {
            saveStatusCodeList: [],
        }
        //Master List



        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initSave04ProcessList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.saveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });

            }
            if (this.editID) {
                let pThis = this
                this.SaveProcessService.Save04Load(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        pThis.loadData(data)
                    }
                    // Close loading
                    pThis.global.setLoading(false)
                })
            }

            this.global.setLoading(false)
        })
    }

    constructor(
        private help: Help,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private SaveProcessService: SaveProcessService
    ) { }

    onClickSave04List(): void {
        // if(this.validateSave04List()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callSave04List(this.saveData())
        // }
    }
    //! <<< Call API >>>
    callSave04List(params: any): void {
        let pThis = this
        this.SaveProcessService.Save04ListPage(this.help.GetFilterParams(params, this.paginateSaveProcess)).subscribe((data: any) => {
            // if(isValidSave04ListResponse(res)) {
            if (data) {
                // Set value
                pThis.listData(data)
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickSave04Add(): void {
        this.listSaveProcess.push({
            index: this.listSaveProcess.length + 1,
            make_date_text: null,
            request_number: null,
            request_date_text: null,
            people_name: null,
            save_status_name: null,
            cancel_reason: null,

        })
        this.changePaginateTotal(this.listSaveProcess.length, 'paginateSaveProcess')
    }

    onClickSave04Edit(item: any): void {
        var win = window.open("save-04-process/edit/" + item.id)
    }


    onClickSave04Delete(item: any): void {
        // if(this.validateSave04Delete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listSaveProcess.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listSaveProcess.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listSaveProcess.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {

                let ids = []

                for (let i = 0; i < this.listSaveProcess.length; i++) {
                    if (this.listSaveProcess[i].is_check) {
                        this.listSaveProcess[i].cancel_reason = rs
                        this.listSaveProcess[i].status_code = "DELETE"

                        if (true && this.listSaveProcess[i].id) ids.push(this.listSaveProcess[i].id)
                        else this.listSaveProcess.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callSave04Delete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listSaveProcess.length; i++) {
                    this.listSaveProcess[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callSave04Delete(params: any, ids: any[]): void {
        this.SaveProcessService.Save04Delete(params).subscribe((data: any) => {
            // if(isValidSave04DeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listSaveProcess.length; i++) {
                    if (this.listSaveProcess[i].id == id) {
                        this.listSaveProcess.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listSaveProcess.length; i++) {
                this.listSaveProcess[i] = i + 1
            }

            this.onClickSave04List()
            // Close loading
            this.global.setLoading(false)
        })
    }



    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listSaveProcess = data.save_process_list || []
        let index = 1
        index = 1
        this.listSaveProcess.map((item: any) => { item.is_check = false; item.index = index++; return item })
        this.changePaginateTotal(this.listSaveProcess.length, 'paginateSaveProcess')

    }

    listData(data: any): void {
        this.listSaveProcess = data.list || []
        this.help.PageSet(data, this.paginateSaveProcess)
    }

    saveData(): any {
        const params = {
            request_number: this.input.request_number,
            request_index: this.input.request_index,
            make_date: this.input.make_date,
            save_status_code: this.input.save_status_code,
        }

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxBinding(name: any, value: any, object: any): void {
        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
