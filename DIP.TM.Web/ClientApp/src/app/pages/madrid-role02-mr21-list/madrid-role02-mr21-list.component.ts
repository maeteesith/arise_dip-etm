import { Help } from './../../helpers/help';
import { AutomateTest } from './../../test/automate_test';
import { GlobalService } from './../../global.service';
import { Component, OnInit } from '@angular/core';
import {
  CONSTANTS,
  getMoment,
  clone,
  displayDateServer
} from '../../helpers'
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-madrid-role02-mr21-list',
  templateUrl: './madrid-role02-mr21-list.component.html',
  styleUrls: ['./madrid-role02-mr21-list.component.scss']
})
export class MadridRole02Mr21ListComponent implements OnInit {

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,

  ) { }

  ngOnInit() {



  }

}
