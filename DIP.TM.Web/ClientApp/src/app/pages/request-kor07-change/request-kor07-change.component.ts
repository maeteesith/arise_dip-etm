import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-request-kor07-change',
  templateUrl: './request-kor07-change.component.html',
  styleUrls: ['./request-kor07-change.component.scss']
})
export class RequestKor07ChangeComponent implements OnInit {

  constructor() { }

  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public master: any;

  public tab_menu_show_index: any
  public tab_menu_show_index2: any

  public tableViewDocument: any;
  public tableInfo: any;
  public tableIssueBook: any;
  public tableProduct: any;

  ngOnInit() {
    this.input = {};
    this.validate = {};
    this.master = {
      checkList: [
        { code: "1", name: "หนังสือแจ้งคำสั่ง" },
        { code: "2", name: "การปฏิบัติตามคำสั่ง" },
        { code: "3", name: "การชำระค่าธรรมเนียม" },
        { code: "4", name: "การแจ้งฟ้องคดี / คำพิพากษา" },
        { code: "5", name: "สถานที่ติดต่อ" },
        { code: "6", name: "ชื่อคู่กรณี (ถ้ามี)" },
        { code: "7", name: "การขอถอน" },
        { code: "8", name: "ใบตอบรับ / ครบกำหนด" },
        { code: "9", name: "มีอุทธรณ์ / คำวินิจฉัยฯ" },
      ],
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList2: [
        { code: "1", name: "รอบที่ 1" },
        { code: "2", name: "รอบที่ 2" },
      ],
      typeCodeList3: [
        { code: "1", name: "อนุญาต" },
        { code: "2", name: "รวมเรื่อง" },
        { code: "3", name: "ไม่อนุญาต" },
      ],
      typeCodeList4: [
        { code: "1", name: "ยังไม่หลุด" },
        { code: "2", name: "ยังไม่หลุด" },
        { code: "3", name: "ยังไม่หลุด" },
      ],
      typeCodeList5: [
        { code: "1", name: "เจ้าของ" },
        { code: "2", name: "ตัวแทน" },
        { code: "3", name: "ตัวแทนช่วง" },
        { code: "4", name: "อื่นๆ" },
        { code: "5", name: "ตามก.ที่ยื่น" },
      ],
      typeCodeList6: [
        { code: "1", name: "ต.ค.5 ข้อ 8" },
        { code: "2", name: "ต.ค.9 ข้อ 10" },
        { code: "3", name: "ต.ค.9 ข้อ 2" },
        { code: "4", name: "ต.ค.9 ข้อ 3" },
      ],
      typeCodeList7: [
        { code: "1", name: "แก้ไขรายละเอียดตัวแทน" },
        { code: "2", name: "แต่งตั้งตัวแทนใหม่" },
        { code: "3", name: "แต่งตั้งตัวแทน (เพิ่ม)" },
        { code: "4", name: "ยกเลิกตัวแทนบางส่วน" },
        { code: "5", name: "ยกเลิกตัวแทนทั้งหมด" },
      ],
    }

    this.tab_menu_show_index = 1
    this.tab_menu_show_index2 = 1

    this.tableViewDocument = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }
    this.tableInfo = {
      column_list: [
        {
          detail: "ม.7"
        },
        {
          detail: "ม.9"
        },
        {
          detail: "ม.7"
        },
      ]
    }
    this.tableIssueBook = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }

    this.tableProduct = {
      head_column_list: [
        {
          order: "#",
          type: "จำพวกสินค้า",
          description: "รายการสินค้า/บริการ"
        },
      ],

      column_list: [
        {
          type: "21"
        },
        {
          type: "3"
        },
      ]
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>
  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }
}
