import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { Auth } from "../../auth";
import { Location } from '@angular/common'
import { DomSanitizer } from '@angular/platform-browser'
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join.service'
import { RequestProcessService } from '../../services/request-process.service'
import { RequestAgencyService } from '../../services/request-agency.service'
import { RegistrationRequestService } from '../../services/registration-request.service'
import { UploadService } from '../../services/upload.service'
import { AutomateTest } from '../../test/automate_test'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  displayFormatBytes,
  // displayDateServer,
  clone,
  getItemCalculator,
  print,
  download
} from '../../helpers'

@Component({
  selector: "app-registration-request",
  templateUrl: "./registration-request.component.html",
  styleUrls: ["./registration-request.component.scss"]
})
export class RegistrationRequestComponent implements OnInit {

  //TODO >>> Declarations <<<
  // Init
  public editID: number
  public input: any
  public validate: any
  public master: any
  public response: any
  public modal: any
  public popup: any
  public requestEvidenceList: any[]
  // Autocomplete
  public autocompleteList: any
  public isShowAutocomplete: any
  // Auto fill
  public smartCardData: any
  // List (request)
  public requestList: any[]
  public paginateRequest: any
  public perPageRequestList: number[]
  public requestItem: any
  public payList: any[]
  // List (sub item type)
  public subItemList: any[]
  public paginateSubItem: any
  public perPageSubItemList: number[]
  // Other
  public isHasRequestList: boolean
  public timeout: any
  public isShow: boolean
  public url: any
  public officerIdcard: any
  public autoFillToken: any
  public is_gdx_error: any

  public evidenceId: any
  constructor(
    public sanitizer: DomSanitizer,
    private route: ActivatedRoute,
    private location: Location,
    private auth: Auth,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private requestAgencyService: RequestAgencyService,
    private requestProcessService: RequestProcessService,
    private registrationRequestService: RegistrationRequestService,
    private uploadService: UploadService,
    private automateTest: AutomateTest
  ) { }

  ngOnInit() {
    //TODO >>> Init value <<<
    // Init
    this.editID = +this.route.snapshot.paramMap.get('id')
    this.input = {
      request_date: getMoment(),
      created_by_name: this.auth.getAuth().name,
      is_via_post: false,
      total_price: 0,
      is_wave_fee: false,
      is_check_all: false,
      commercial_affairs_province_code: "FLOOR3",
      search: {
        agentValue: ''
      },
    }
    this.validate = {}
    this.master = {
      itemTypeList: [],
      soundTypeList: [],
      facilitationActStatusList: [],
      priceMasterList: []
    }
    this.requestEvidenceList = [
      { code: 'A10', name: 'ก. 10', isSelect: false },
      { code: 'A12', name: 'ก. 12', isSelect: false },
      { code: 'A13', name: 'ก. 13', isSelect: false },
      { code: 'A18', name: 'ก. 18', isSelect: false },
      { code: 'A19', name: 'ก. 19', isSelect: false },
      { code: 'A20', name: 'ก. 20', isSelect: false },
      { code: 'OTHER_FLOOR7', name: 'หลักฐานอื่นๆ (ชั้น 7)', isSelect: false },
      { code: 'OTHER_ATTACHMENT', name: 'หลักฐานอื่นๆที่แนบกับคำขอ', isSelect: false },
    ];
    this.response = {}
    this.modal = {
      isModalSearchAgentOpen: false,
      isModalAutoFillOpen: false,
      isModalEditCategoryOpen: false,
      isModalPreviewPdfOpen: false
    }
    this.popup = {
      isPopupSaveOpen: false,
      isPopupConfirmDeleteOpen: false,
      removeId: 0,
      confirmRemove: () => { }
    }
    // Autocomplete
    this.autocompleteList = {
      requester_name: []
    }
    this.isShowAutocomplete = {
      requester_name: false
    }
    // Auto fill
    this.smartCardData = {
      connect: false,
      type: 1,
      officer: {
        connect: false,
        info: {}
      },
      people: {
        connect: false,
        info: {
          image: '',
          id_card: '',
          name_th: '',
          name_en: '',
          birthday: '',
          issue_date: '',
          expiration_date: '',
          address: ''
        }
      }
    }
    // List (request)
    this.requestList = []
    this.paginateRequest = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateRequest.id = 'paginateRequest'
    this.perPageRequestList = clone(CONSTANTS.PAGINATION.PER_PAGE)
    this.requestItem = {
      audio: {
        input: '',
        validate: '',
        file: {},
        sound: null,
        palyer: null,
        action: false
      },
      people_code: 'NAT_ID'
    }
    // List (sub item type)
    this.subItemList = []
    this.paginateSubItem = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateSubItem.id = 'paginateSubItem'
    this.perPageSubItemList = clone(CONSTANTS.PAGINATION.PER_PAGE)
    // Other
    this.isHasRequestList = false

    console.log('auth', this.auth.getAuth())

    //TODO >>> Call service <<<
    this.callInit()
  }

  //! <<< Call Init API >>>
  callInit(): void {
    // Open loading
    this.global.setLoading(true)
    // Call API
    this.forkJoinService.initRegistrationRequestPage().subscribe((data: any) => {
      if (data) {
        this.master = data
      }
      if (this.editID) {
        this.requestProcessService.Request01Load(this.editID, {}).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        // Close loading
        this.global.setLoading(false)
        this.automateTest.test(this)
      }
    })
  }

  //! <<< Call API >>>
  callListView(params: any, name: any): void {
    this.requestAgencyService.ListView(params).subscribe((data: any) => {
      if (data) {
        this.autocompleteList[name] = data
        this.isShowAutocomplete[name] = true
      } else {
        this.isShowAutocomplete[name] = false
        this.autocompleteList[name] = []
      }
    })
  }
  callUpload(params: any): void {
    const formData = new FormData()
    formData.append('file', params.file)

    this.uploadService.upload(formData).subscribe((data: any) => {
      if (data) {
        this.requestItem.sound_file_id = data.id
        this.requestItem.sound_file_url = data.file_url
        this.requestItem.sound_file_name = this.requestItem.audio.file.name
        this.requestItem.sound_file_size = this.requestItem.audio.file.size
        this.updateItemTypeToRequestTable()
      }
      // Close loading
      this.global.setLoading(false)
    })
  }

  loadData(data: any): void {
    this.editID = data.id

    data.item_list.map((item: any) => {
      item.key = item.id
      item.is_check = false
      item.amount_type = item.item_sub_list.length
      item.amount_product = item.item_sub_list.reduce((prev: any, cur: any) => {
        return prev + cur.product_count
      }, 0)
      item.audio = {
        input: '',
        validate: '',
        file: {},
        sound: null,
        palyer: null,
        action: false
      }

      var evidence_select_list = item.request_evidence_list.split(', ')
      item.requestEvidenceList = this.requestEvidenceList.map((e) => {
        var evident = { ...e }
        if (evidence_select_list.indexOf(evident.code) >= 0) {
          evident.isSelect = true
        }
        return evident
      })

      return item
    })

    // Set value
    this.input.requester_name = data.requester_name
    this.input.item_count = data.item_count
    this.input.is_via_post = data.is_via_post
    this.input.request_date = data.request_date
    this.input.commercial_affairs_province_code = data.commercial_affairs_province_code
    this.input.telephone = data.telephone
    this.input.is_wave_fee = data.is_wave_fee
    this.requestList = data.item_list
    this.changePaginateTotal(data.item_list.length, 'paginateRequest')
    this.input.total_price = data.total_price
    this.response.request01Load = data
    this.isHasRequestList = true
  }

  callRequest01Split(params: any): void {
    this.global.setLoading(true)
    this.requestProcessService.Request01Split(params).subscribe((data: any) => {
      if (data) {
        // Manage structure
        window.open("registration-request/edit/" + data.id)
        this.callInit()


        //this.loadData(data)


        //let is_check = this.requestList.filter(r => r.is_check).length > 0

        //if (is_check) {
        // Open popup save success
        //let is_check_list = this.requestList.filter(r => r.reference_number && r.reference_number != "")
        //let reference_number = is_check_list.reduce((pre, cur) => cur.reference_number > pre.reference_number ? cur.reference_number : pre.reference_number);

        //console.log(is_check_list)
        //console.log(reference_number.reference_number || reference_number)
        //this.automateTest.test(this, { reference_number: (reference_number.reference_number || reference_number) })

        //this.url = '/pdf/Receipt01Reference/' + (reference_number.reference_number || reference_number)
        //this.toggleModal('isModalPreviewPdfOpen')

        //} else {
        //  this.togglePopup('isPopupSaveOpen')
        //}
      }

      // Close loading
      this.global.setLoading(false)
    })
  }
  callRequest01Save(params: any): void {
    this.requestProcessService.Request01Save(params).subscribe((data: any) => {
      if (data) {
        // Manage structure
        this.loadData(data)

        //let is_check = this.requestList.filter(r => r.is_check).length > 0

        //if (is_check) {
        // Open popup save success
        //let is_check_list = this.requestList.filter(r => r.reference_number && r.reference_number != "")
        //let reference_number = is_check_list.reduce((pre, cur) => cur.reference_number > pre.reference_number ? cur.reference_number : pre.reference_number);

        //console.log(is_check_list)
        //console.log(reference_number.reference_number || reference_number)
        //console.log(reference_number)
        this.automateTest.test(this, { reference_number: this.requestList[0].reference_number })

        this.url = '/pdf/Receipt01Reference/' + this.requestList[0].reference_number
        console.log(this.url)
        this.toggleModal('isModalPreviewPdfOpen')

        //} else {
        //  this.togglePopup('isPopupSaveOpen')
        //}
      }

      // Close loading
      this.global.setLoading(false)
    })
  }
  callRequest01Add(params: any): void {
    // this.requestProcessService.Request01Add(params).subscribe((data: any) => {
    //   if (data) {
    //     // Manage structure
    //     data.item_list.map((item: any) => {
    //       item.is_check = false
    //       item.amount_type = item.item_sub_list.length
    //       item.amount_product = item.item_sub_list.reduce((prev: any, cur: any) => {
    //         return prev + cur.product_count
    //       }, 0)
    //       item.audio = {
    //         input: '',
    //         validate: '',
    //         file: {},
    //         sound: null,
    //         palyer: null,
    //         action: false
    //       }
    //       return item
    //     })

    //     // Set value
    //     this.requestList = data.item_list
    //     this.changePaginateTotal(data.item_list.length, 'paginateRequest')
    //     this.input.total_price = data.total_price
    //     this.response.request01Add = data
    //     this.isHasRequestList = true

    //     // Open popup save success
    //     this.togglePopup('isPopupSaveOpen')
    //   }
    //   // Close loading
    //   this.global.setLoading(false)
    // })
  }

  //! <<< Prepare Call API >>>
  onClickSaveItemType(): void {
    if (this.validateSaveItemType()) {
      if (this.requestItem.audio.file.type) { // is upload file
        // Open loading
        this.global.setLoading(true)
        // Set param
        let params = {
          file: this.requestItem.audio.file
        }
        // Call api
        this.callUpload(params)
      } else {
        this.updateItemTypeToRequestTable()
      }
    }
  }

  onClickSplitRequest(): void {
    if (this.validateAddRequest()) {
      // Open loading
      this.global.setLoading(true)
      // Set param
      let params = this.editID ? this.response.request01Load : (this.response.request01Save ? this.response.request01Save : {})
      params.requester_name = this.input.requester_name
      params.is_via_post = this.input.is_via_post
      params.request_date = this.input.request_date
      params.commercial_affairs_province_code = this.input.commercial_affairs_province_code
      params.telephone = this.input.telephone
      params.is_wave_fee = this.input.is_wave_fee
      params.total_price = this.input.total_price
      params.item_list = this.requestList
      params.item_list.forEach((item: any) => {
        item.request_evidence_list = ""
        if (item.requestEvidenceList)
          item.requestEvidenceList.forEach((item_sub: any) => {
            if (item_sub.isSelect) item.request_evidence_list += item_sub.code + ", "
          })
      })
      params.update_by = 0
      params.update_by_name = null
      params.updated_date = getMoment()

      console.log(params)
      if (params.id) {
        if (params.item_list.filter(r => r.is_check).length > 0) {
          // Call api
          this.callRequest01Split(params)
        } else {
          console.log("requestList: not is_check")
          this.global.setLoading(false)
        }
      } else {
        console.log("reques: not this.input.id")
        this.global.setLoading(false)
      }
      // console.log('params', params)
    }
  }
  onClickSaveRequest(): void {
    if (this.validateAddRequest()) {
      // Open loading
      this.global.setLoading(true)
      // Set param
      let params = this.editID ? this.response.request01Load : (this.response.request01Save ? this.response.request01Save : {})
      params.requester_name = this.input.requester_name
      params.is_via_post = this.input.is_via_post
      params.commercial_affairs_province_code = this.input.commercial_affairs_province_code
      params.request_date = this.input.request_date
      params.telephone = this.input.telephone
      params.is_wave_fee = this.input.is_wave_fee
      params.total_price = this.input.total_price
      params.item_list = this.requestList
      params.item_list.forEach((item: any) => {
        item.request_evidence_list = ""
        if (item.requestEvidenceList)
          item.requestEvidenceList.forEach((item_sub: any) => {
            if (item_sub.isSelect) item.request_evidence_list += item_sub.code + ", "
          })
      })
      params.update_by = 0
      params.update_by_name = null
      params.updated_date = getMoment()
      // console.log('params', params)
      // Call api
      this.callRequest01Save(params)
    }
  }
  onClickDeleteRequest(): void {
    var check_list = this.requestList.filter(r => r.is_check)
    var check_have_request_number_list = check_list.filter(r => r.request_number && r.request_number != "")

    if (check_list.length > 0) {
      var rs = check_have_request_number_list.length > 0 ? prompt('Do you want to delete') : confirm('Do you want to delete')
      if (rs) {
        check_list.forEach((item) => {
          if (item.request_number && item.request_number != "") {
            item.cancel_reason = rs
            item.status_code = 'DELETE'
          } else {
            var index = this.requestList.indexOf(item)
            this.requestList.splice(index, 1)
          }
        })
      }
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }
  validateAddRequest(): boolean {
    let result = validateService('validateAddRequest', this.input)
    this.validate = result.validate
    return result.isValid
  }
  validateSaveItemType(): boolean {
    let isValid = true

    this.subItemList.forEach(item => {
      let result = validateService('validateSaveItemType', item)
      item.validate = result.validate
      if (!result.isValid) {
        isValid = false
      }
    })

    //this.updateRequestList()

    return isValid
  }

  //! <<< Event >>>
  onClickWaveFree(value: any): void {
    this.input.is_wave_fee = value
    //console.log(this.input.is_wave_fee)
    //if (!this.input[name]) { // Is check
    //  this.input[name] = true
    this.updateRequestList()
    //} else { // Is uncheck
    //  this.input[name] = false
    //  this.updateRequestList(false)
    //}
  }
  onClickAddRequest(): void {
    if (this.validateAddRequest()) {
      // // Open loading
      // this.global.setLoading(true)
      // // Set param
      // const params = {
      //    requester_name: this.input.requester_name,
      //    item_count: this.input.item_count,
      //    is_via_post: this.input.is_via_post,
      //    request_date: displayDateServer(this.input.request_date),
      //    telephone: this.input.telephone,
      //    is_wave_fee: this.input.is_wave_fee
      // }
      // // Call api
      // this.callRequest01Add(params)

      let requestList = []
      let price = this.input.is_wave_fee ? 0 : getItemCalculator(1, this.master.priceMasterList)
      for (let i = 1; i <= this.input.item_count; i++) {
        requestList.push({
          key: i,
          is_check: false,
          total_price: price,
          item_type_code: 'TRADE_MARK_AND_SERVICE_MARK',
          item_type_name: this.master.itemTypeList.find((type: any) => {
            return type.code === 'TRADE_MARK_AND_SERVICE_MARK'
          }).name,
          income_type: '010',
          income_description: 'ค่าขอจดทะเบียนเครื่องหมายการค้า/บริการ/รับรอง/เครื่องหมายร่วมใช้',
          sound_type_code: 'HUMAN',
          sound_description: '',
          facilitation_act_status_code: 'DOCUMENT_COMPLETED',
          is_otop: false,
          status_code: 'DRAFT',
          amount_type: 1,
          amount_product: 1,
          item_sub_list: [{
            key: i,
            item_sub_index: 1,
            product_count: 1,
            total_price: price
          }],
          audio: {
            input: '',
            validate: '',
            file: {},
            sound: null,
            palyer: null,
            action: false,
          },
          requestEvidenceList: this.requestEvidenceList.map((e) => {
            return { ...e }
          })
        })
      }
      // Set value
      this.requestList = requestList
      this.changePaginateTotal(requestList.length, 'paginateRequest')
      this.input.total_price = this.input.is_wave_fee ? 0 : price * this.input.item_count
      this.isHasRequestList = true
    }
  }
  updateRequestList(): void {
    if (this.input.is_wave_fee) {
      this.input.total_price = 0
      this.requestList.forEach(request => {
        request.total_price = 0
        request.size_over_price = 0
        request.item_sub_list.forEach((item: any) => {
          item.total_price = 0
        })
        request.size_over_price = 0
      })
    } else {
      let total = 0
      this.requestList.forEach((request: any) => {
        let total_price_request = 0
        request.item_sub_list.forEach((item: any) => {
          let total = getItemCalculator(item.product_count, this.master.priceMasterList)
          item.total_price = total
          total_price_request = total_price_request + total
        })
        request.total_price = total_price_request
        total = total + total_price_request

        request.size_over_price = request.size_over_cm * 200
        total = total + (request.size_over_price || 0)

      })
      this.input.total_price = total
    }
  }
  onChangeSearchAgent(newValue: any): void {
    this.input.search.agentValue = newValue;
  }
  onClickAddPay(item): void {
    //const index = (this.paginateRequest.currentPage - 1) * this.paginateRequest.itemsPerPage+id
    //console.log(item)
    // console.log("id1", index)
    //let payList = this.requestList[index]
    item.size_over_cm = 1;
    item.size_over_income_type = '210';
    item.size_over_price = 200;
    item.size_over_income_description = 'แม่พิมพ์รูปเครื่องหมายการค้า/บริการ/รับรอง/ร่วมใช้ ที่เกิน 5 ซม.';

    //console.log(item)
    //console.log(this.requestList)
    //this.requestList[index] = payList
    this.updateRequestList()
  }
  onClickCancelPay(item): void {
    //const index = (this.paginateRequest.currentPage - 1) * this.paginateRequest.itemsPerPage + id
    // console.log("id2", index)
    //let payList = this.requestList[index]
    item.size_over_cm = 0;
    item.size_over_income_type = undefined;
    item.size_over_price = 0;
    item.size_over_income_description = undefined;
    //item = {
    //  ...item,
    //  income_type2: undefined,
    //  total_price2: undefined,
    //  income_description2: undefined
    //}
    //this.requestList[index] = payList

    this.updateRequestList()
  }
  amountPay(item): void {
    //console.log(item)
    //const index = (this.paginateRequest.currentPage - 1) * this.paginateRequest.itemsPerPage + id
    //// console.log("id3", index)
    //let payList = this.requestList[index]
    //payList = {
    //  ...payList,
    //  amountPay: value,
    //  total_price2: (200 * value),
    //}

    if (item.reference_number) {
      var reference_number = item.reference_number
      this.requestList.forEach((request) => {
        if (item.reference_number == reference_number) {
          item.reference_number = ""
        }
      })
    }

    this.updateRequestList()
    //this.requestList[index] = payList
  }
  selectEvidence(item, name, value): void {
    //const index = (this.paginateRequest.currentPage - 1) * this.paginateRequest.itemsPerPage + id
    //// console.log("id4", index)
    //let payList = this.requestList[index]
    //payList.requestEvidenceList[evidenceIndex].isSelect = value
    //this.requestList[index] = payList
    item[name].isSelect = value
  }
  closeEvidence(): void {
    this.evidenceId = undefined
  }
  displayEvidence(item): void {
    //// console.log("id5", id)
    //if (this.evidenceId !== id) {
    setTimeout(() => {
      //this.evidenceId = id
      this.evidenceId = item
    }, 100);
    //}

  }

  //! <<< Auto fill >>>
  onClickRadio(name: any, value: any): void {
    this.smartCardData[name] = value
    this.onChangeSmartCard()
  }
  onChangeSmartCard(): void {
    this.smartCardData = {
      connect: true,
      type: 1,
      officer: {
        connect: true,
        info: {}
      },
      people: {
        connect: true/*  ,
        info: {
         image: 'https://komchadluek.sgp1.cdn.digitaloceanspaces.com/media/img/size1/2019/03/22/8be76h8ikb6acaic57i9d.jpg',
          id_card: '1509901200214',
          name_th: 'นที วารีย์',
          name_en: 'Natee Varee',
          birthday: '07/08/1992',
          issue_date: '07/08/2014',
          expiration_date: '06/08/2022',
          address: '131 ถ.มหิดล ต.หนองหอย อ.เมือง จ.เชียงใหม่ 50000' 
        }*/
      }
    }
  }

  //! <<< Table Request >>>
  onClickItemType(item: any): void {
    this.requestItem = clone(item)
    this.subItemList = this.requestItem.item_sub_list.map((item: any) => {
      item.key = item.id
      item.item_sub_type_1_code = displayString(item.item_sub_type_1_code)
      item.validate = {}
      return item
    })
    if (this.requestItem.sound_file_url) {
      this.requestItem.audio.file = {
        name: this.requestItem.sound_file_name,
        size: this.requestItem.sound_file_size
      }
      this.requestItem.audio.sound = this.requestItem.sound_file_url
    }
    this.changePaginateTotal(this.subItemList.length, 'paginateSubItem')
    this.toggleModal('isModalEditCategoryOpen')
  }

  //! <<< Modal (Item Type) >>>
  onChangeSelectItem(value: string): void {
    this.requestItem.item_type_code = value
    this.requestItem.item_type_name = this.master.itemTypeList.find((type: any) => {
      return type.code === value
    }).name
  }
  onChangeCountItem(index: number, count: number): void {
    if (+count && !this.input.is_wave_fee) {
      let price = getItemCalculator(count, this.master.priceMasterList)
      this.subItemList[index].total_price = price
      this.updateSummaryItemType()
    } else {
      this.subItemList[index].total_price = 0
      this.requestItem.total_price = 0
      this.requestItem.amount_product = 0
    }
  }
  onClickAddItemType(): void {
    let key = this.subItemList.length > 0 ? this.subItemList.length + 1 : 1
    this.subItemList = this.subItemList.concat({
      key: key,
      product_count: 1,
      item_sub_type_1_code: '',
      total_price: getItemCalculator(1, this.master.priceMasterList),
      validate: {}
    })
    this.updateSummaryItemType()
  }
  onClickRemoveItemType(key: number): void {
    this.popup.removeId = key
    // this.openConfirmRemove(() => {
    let removeIndex = this.subItemList.map((item) => { return item.key }).indexOf(this.popup.removeId)
    this.subItemList.splice(removeIndex, 1)
    this.updateSummaryItemType()
    //   this.togglePopup('isPopupConfirmDeleteOpen')
    // })
  }
  updateSummaryItemType(): void {
    let total_price = 0
    let amount_product = 0
    let length = this.subItemList.length

    this.subItemList.forEach(item => {
      total_price = total_price + item.total_price
      amount_product = amount_product + item.product_count
    })

    this.requestItem.total_price = this.input.is_wave_fee ? 0 : total_price
    this.requestItem.amount_type = length
    this.requestItem.amount_product = amount_product
    this.changePaginateTotal(length, 'paginateSubItem')
  }
  onClickUploadAudio(event: any): void {
    let file = event.target.files[0]
    //console.log(file)
    if (file.name.endsWith(".mp3")) {
      this.requestItem.audio.validate = ''
      this.requestItem.audio.file = file
      let reader = new FileReader()
      reader.onload = (e) => {
        this.requestItem.audio.sound = reader.result
      }
      reader.readAsDataURL(file)
    } else {
      this.requestItem.audio.validate = 'กรุณาเลือกไฟล์ mp3'
    }
  }
  onClickRemoveAudio(): void {
    if (this.requestItem.audio.palyer) {
      this.requestItem.audio.palyer.pause()
    }
    this.requestItem.audio = {
      input: '',
      validate: '',
      file: {},
      sound: null,
      palyer: null,
      action: false
    }
    this.requestItem.sound_file_url = null
    this.requestItem.sound_file_name = null
    this.requestItem.sound_file_size = null
  }
  togglePlayer(): void {
    this.requestItem.audio.action = !this.requestItem.audio.action
    if (!this.requestItem.audio.palyer) {
      this.requestItem.audio.palyer = new Audio(this.requestItem.audio.sound)
    }
    this.requestItem.audio.action ? this.requestItem.audio.palyer.play() : this.requestItem.audio.palyer.pause()
  }
  updateItemTypeToRequestTable(): void {
    this.requestItem.audio = {
      input: '',
      validate: '',
      file: {},
      sound: null,
      palyer: null,
      action: false
    }
    // Set data request table
    let requestItem = this.requestItem
    requestItem.item_sub_list = this.subItemList
    this.requestList.forEach((request, index, set) => {
      if (request.key === requestItem.key) {
        set[index] = requestItem
      }
    })

    if (requestItem.reference_number) {
      var reference_number = requestItem.reference_number
      this.requestList.forEach((request) => {
        if (requestItem.reference_number == reference_number) {
          requestItem.reference_number = ""
        }
      })
    }
    //this.input.total_price = this.requestList.reduce((prev: any, cur: any) => {
    //  return prev + cur.total_price
    //}, 0)
    this.updateRequestList()
    // Close modal
    this.toggleModal('isModalEditCategoryOpen')
  }

  //! <<< IdCard >>>
  timeLeft: number = 30;
  timeMsg: string = "";
  interval;
  pauseTimer() {
    this.timeLeft = 30;
    clearInterval(this.interval);
  }
  bindingAutoFill(data) {
    console.log("bindingAutoFill")

    this.timeMsg = "การค้นหาสำเร็จ";
    this.autoFillToken = data.auto_fill_token

    this.input.name_th = data.thai_name;
    this.input.name_en = data.eng_name;
    this.input.birthday = data.birth_date;
    this.input.issue_date = data.issue_date;
    this.input.expiration_date = data.expire_date;
    this.input.address = data.address;
  }


  onClickGDXSave(): void {
    this.input.requester_name = this.input.name_th
    this.toggleModal('isModalAutoFillOpen')
  }

  //////! <<< GDX Search >>>
  /////*
  ////1. When click find button on registration-request
  ////2. Validate officer input citizen id card
  ////*/
  onClickGDXSearch(): void {
    console.log("onClickGDXSearch")
    // Validate id card is mandatory
    //if (!this.input.id_card) {
    //  alert("กรุณากรอกเลขบัตรประชาชน");
    //  return null;
    //}
    //else {
    // Find auto fill token from somewhere
    // Dummy set auto fill token to 1234
    //autoFillToken = "1234";
    //this.officerIdcard = "123";
    let pThis = this
    // If can't find auto fill token
    // Officer need to insert his card to card reader
    if (!this.autoFillToken && !this.officerIdcard) {
      if (confirm("กรุณาเสียบบัตรประชาชนของพนักงานที่เครื่องอ่านบัตรก่อนทำรายการ")) {
        //TODO get officer id card from card reader
        //officerIdcard = "1739900081202";
        this.registrationRequestService.DeleteIDCardReader({}, function () {
          pThis.is_gdx_error = true
        }).subscribe((data: any) => {
          console.log("GDX: Already deleted");
          pThis.is_gdx_error = false
          this.startGetStaffIdCardTimer();
        })
      }
    }
    else {
      this.AutoFillPeopleFindID()
    }
    //}
  }
  startGetStaffIdCardTimer() {
    console.log("startGetStaffIdCardTimer")
    this.pauseTimer()
    this.interval = setInterval(() => {
      this.timeLeft -= 5;
      this.timeMsg = "กรุณาเสียบบัตรประชาชนของพนักงานที่เครื่องอ่านบัตรก่อนทำรายการ [เหลือเวลาอีก " + this.timeLeft + " วินาที]";

      if (this.timeLeft > 0) {
        this.getStaffIdCard();
      } else {
        this.timeMsg = "กรุณาเสียบบัตรประชาชนของพนักงานและกดค้นหาใหม่";
        this.pauseTimer();
      }
    }, 5000)
  }
  getStaffIdCard() {
    console.log("getStaffIdCard")
    this.registrationRequestService.GetIDCard({}).subscribe((data: any) => {
      //alert(data.citizenId);
      if (data.length > 0) {
        console.log(data)

        data = data[0]
        if (data.citizenId) {
          this.pauseTimer()
          this.officerIdcard = data.citizenId
          this.AutoFillPeopleFindID()
        }
      }
    })
  }
  AutoFillPeopleFindID(): void {
    console.log("AutoFillPeopleFindID: " + this.input.id_card)
    if (this.input.id_card) {
      this.registrationRequestService.AutoFillFindID({
        officer_idcard: this.officerIdcard,
        citizen_idcard: this.input.id_card,
        auto_fill_token: this.autoFillToken,
        people_type: this.requestItem["people_code"],
      }).subscribe((data: any) => {
        console.log(data)
        if (data) {
          // If service return unauthorized
          // Clear officer id card and token
          if (data.return_status && data.return_status != "") {
            if (data.return_status == "unauthorized") {
              alert("Token หมดอายุกรุณาเสียบบัตรประชาชนและทำรายการใหม่อีกครั้ง")
            } else {
              alert("GDX Return Status: " + data.return_status)
            }
            this.officerIdcard = null
            this.autoFillToken = null
            this.onClickGDXSearch()
          } else {
            this.bindingAutoFill(data)
          }

        }
      })
    } else {
      if (confirm("กรุณาเสียบบัตรประชาชนของประชาชนที่เครื่องอ่านบัตรก่อนทำรายการ")) {
        //TODO get officer id card from card reader
        //officerIdcard = "1739900081202";
        this.registrationRequestService.DeleteIDCardReader({}, () => {
          this.is_gdx_error = true
        }).subscribe((data: any) => {
          console.log("GDX: Already deleted");
          this.is_gdx_error = false
          this.startGetPeopleIdCardTimer();
        })
      }
    }
  }

  startGetPeopleIdCardTimer() {
    console.log("startGetPeopleIdCardTimer")
    this.pauseTimer()
    this.interval = setInterval(() => {
      this.timeLeft -= 5;
      this.timeMsg = "กรุณาเสียบบัตรประชาชนของประชาชนที่เครื่องอ่านบัตรก่อนทำรายการ [เหลือเวลาอีก " + this.timeLeft + " วินาที]";

      if (this.timeLeft > 0) {
        this.getPeopleIdCard();
      } else {
        this.timeMsg = "กรุณาเสียบบัตรประชาชนของประชาชนและกดค้นหาใหม่";
        this.pauseTimer();
      }
    }, 5000)
  }

  getPeopleIdCard() {
    console.log("getPeopleIdCard")
    this.registrationRequestService.GetIDCard({}).subscribe((data: any) => {
      //alert(data.citizenId);
      if (data.length > 0) {
        console.log(data)

        data = data[0]
        if (data.citizenId) {
          this.pauseTimer()
          this.input.id_card = data.citizenId
          this.AutoFillPeopleFindID()
        }
      }
    })

    //console.log("getPeopleIdCard")
    //this.registrationRequestService.GetIDCard({}).subscribe((data: any) => {
    //  //alert(data.citizenId);
    //  if (data.length > 0) {
    //    console.log(data)

    //    data = data[0]
    //    if (data.citizenId) {
    //      this.pauseTimer()
    //      this.input.id_card = data.citizenId


    //      if (confirm("กรุณาเสียบบัตรประชาชนขอประชาชนที่เครื่องอ่านบัตรก่อนทำรายการ")) {
    //        //TODO get officer id card from card reader
    //        //officerIdcard = "1739900081202";
    //        this.registrationRequestService.DeleteIDCardReader({}, () => {
    //          this.is_gdx_error = true
    //        }).subscribe((data: any) => {
    //          console.log("GDX: Already deleted");
    //          this.is_gdx_error = false
    //          this.AutoFillPeopleFindID();
    //        })
    //      }
    //      //this.AutoFillPeopleFindID()
    //    }
    //  }
    //})
  }


  //onClickGetIDFromReader(): void {
  //}

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name]
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(obj: any, name: any): void {
    this.validate[name] = null
    clearTimeout(this.timeout)
    this.timeout = setTimeout(() => {
      if (this[obj][name]) {
        if (name === 'requester_name') {
          this.callListView({
            name: this[obj][name],
            paging: {
              item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page
            }
          }, name)
        }
      } else {
        this.onClickOutsideAutocomplete(name)
      }
    }, CONSTANTS.DELAY_CALL_API)
  }
  onSelectAutocomplete(name: any, item: any): void {
    if (name === 'requester_name') {
      this.input[name] = item.name
      this.input.is_wave_fee = item.is_wave_fee
      this.updateRequestList()
    }
    this.onClickOutsideAutocomplete(name)
  }
  onClickOutsideAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = false
    this.autocompleteList[name] = []
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    // if (name === 'paginateRequest01Item') {
    //     this.onClickRequest01ItemList()
    //     this.input.is_check_all = false
    // }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    // if ((+page || page === '') && page <= this.getMaxPage(name)) {
    //     this.managePaginateCallApi(name)
    // }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    // this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    if (!this.modal[name]) { // Is open
      this.modal[name] = true
    } else { // Is close
      this.modal[name] = false
      if (name === 'isModalEditCategoryOpen') {
        if (this.requestItem.audio.palyer) {
          this.requestItem.audio.palyer.pause()
        }
        this.requestItem = {
          audio: {
            input: '',
            validate: '',
            file: {},
            sound: null,
            palyer: null,
            action: false
          }
        }
        this.subItemList = []
      }
      if (name === 'isModalPreviewPdfOpen') {
        this.togglePopup('isPopupSaveOpen')
      }
    }
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) { // Is open
      this.popup[name] = true
    } else { // Is close
      this.popup[name] = false
      if (name === 'isPopupConfirmDeleteOpen') {
        this.popup.removeId = 0
      }
    }
  }
  openConfirmRemove(callback: Function): void {
    this.togglePopup('isPopupConfirmDeleteOpen')
    this.popup.confirmRemove = callback
  }

  //! <<< Other >>>
  back(): void {
    this.location.back()
  }
  reset(): void {
    this.ngOnInit()
  }
  displayMoney(value: any): any {
    return displayMoney(value)
  }
  displayFormatBytes(value: any): any {
    return displayFormatBytes(value)
  }
  print(id: any): any {
    return print(id)
  }
  download(url: any, name: any): any {
    return download(url, name)
  }
}
