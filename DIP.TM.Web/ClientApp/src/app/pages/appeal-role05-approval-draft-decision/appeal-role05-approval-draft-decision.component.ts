import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appeal-role05-approval-draft-decision',
  templateUrl: './appeal-role05-approval-draft-decision.component.html',
  styleUrls: ['./appeal-role05-approval-draft-decision.component.scss']
})
export class AppealRole05ApprovalDraftDecisionComponent implements OnInit {

  constructor() { }
  
  public modal: any
  public input: any;
  public Editor: any;

  ngOnInit() {
    this.modal = {
      isModalSendEdit: false
    }
  }

  toggleModal(name: string): void {
    if (!this.modal[name]) {
      this.modal[name] = true
    } else {
      this.modal[name] = false
      switch(name){
      }
    }
  }

}
