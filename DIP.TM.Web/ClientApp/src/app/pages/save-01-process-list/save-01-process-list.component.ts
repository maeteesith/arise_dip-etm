import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-save-01-process-list",
    templateUrl: "./save-01-process-list.component.html",
    styleUrls: ["./save-01-process-list.component.scss"]
})
export class Save01ProcessListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    // // Input Save01
    // public inputSave01: any
    // List Save01
    public listSave01: any[]
    public paginateSave01: any
    public perPageSave01: number[]
    // Response Save01
    public responseSave01: any


    // // Input SaveProcess
    // public inputSaveProcess: any
    // List SaveProcess
    public listSaveProcess: any[]
    public paginateSaveProcess: any
    public perPageSaveProcess: number[]
    // Response SaveProcess
    public responseSaveProcess: any

    public requestItemTypeCodeList: any[]
    public saveStatusCodeList: any[]
    public requestSourceCodeList: any[]



    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}

        this.input = {
            id: null,
            request_number: '',
            request_item_type_code: '',
            save_status_code: '',
            request_date: getMoment(),
            request_source_code: '',
        }
        this.listSave01 = []
        this.paginateSave01 = CONSTANTS.PAGINATION.INIT
        this.perPageSave01 = CONSTANTS.PAGINATION.PER_PAGE

        this.listSaveProcess = []

        this.master = {
            requestItemTypeCodeList: [],
            saveStatusCodeList: [],
            requestSourceCodeList: [],
        }



        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initSave01ProcessList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.requestItemTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" });
                this.master.saveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });
                this.master.requestSourceCodeList.unshift({ "code": "", "name": "ทั้งหมด" });

            }
            if (this.editID) {
                this.SaveProcessService.Save01Load(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        Object.keys(data).forEach((item: any) => {
                            this.input[item] = data[item]
                        })

                        this.listSave01 = data.save01_list || []
                        data.save01_list.map((item: any) => { item.is_check = false; return item })
                        //this.changePaginateTotal((this.listSave01 || []).length, 'paginateSave01')


                        // Set value
                        // this.requestList = data.item_list
                        // this.response.request01Load = data
                        // this.isHasRequestList = true
                    }
                    // Close loading
                    this.global.setLoading(false)
                })
            }

            this.global.setLoading(false)
            //this.onClickSave01List()
        })
    }

    constructor(
        private route: ActivatedRoute,
        private help: Help,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private SaveProcessService: SaveProcessService
    ) { }

    onClickSave01List(): void {
        // if(this.validateSave01List()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        const params = {
            request_number: this.input.request_number,
            request_item_type_code: this.input.request_item_type_code,
            save_status_code: this.input.save_status_code,
            request_date: this.input.request_date,
            request_source_code: this.input.request_source_code,
        }
        // Call api
        this.callSave01List(params)
        // }
    }

    //! <<< Call API >>>
    callSave01List(params: any): void {
        this.SaveProcessService.Save01ListPage(this.help.GetFilterParams(params, this.paginateSave01)).subscribe((data: any) => {
            // if(isValidSave01ListResponse(res)) {
            if (data) {
                // Set value
                this.listSave01 = data.list || []
                this.help.PageSet(data, this.paginateSave01)
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }
    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }



    onClickSave01Add(): void {
        this.listSave01.push({
            request_number: null,
            request_date_text: null,
            item_type_name: null,
            people_name: null,
            item_sub_type_1_code: null,
            save_status_name: null,
            cancel_reason: null,

        })
        //this.changePaginateTotal(this.listSave01.length, 'paginateSave01')
    }

    onClickSave01Edit(item: any): void {
        var win = window.open("save-01-process/edit/" + item.id)
    }


    onClickSave01Delete(item: any): void {
        // if(this.validateSave01Delete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listSave01.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listSave01.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listSave01.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {

                let ids = []

                for (let i = 0; i < this.listSave01.length; i++) {
                    if (this.listSave01[i].is_check) {
                        this.listSave01[i].cancel_reason = rs
                        this.listSave01[i].status_code = "DELETE"

                        if (true && this.listSave01[i].id) ids.push(this.listSave01[i].id)
                        else this.listSave01.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callSave01Delete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listSave01.length; i++) {
                    this.listSave01[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callSave01Delete(params: any, ids: any[]): void {
        this.SaveProcessService.Save01Delete(params).subscribe((data: any) => {
            // if(isValidSave01DeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listSave01.length; i++) {
                    if (this.listSave01[i].id == id) {
                        this.listSave01.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listSave01.length; i++) {
                this.listSave01[i] = i + 1
            }

            this.onClickSave01List()
            // Close loading
            this.global.setLoading(false)
        })
    }
    onClickCheckBoxTable2(name: string, index: number, isAll: boolean = false): void {
        if (isAll) {
            this.input.is_table_2_check_all = !this.input.is_table_2_check_all
            this[name].forEach((item: any) => { item.is_check = this.input.is_table_2_check_all })
        } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
    }
    onChangeCheckboxSave01(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
    onChangeSelectSave01(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }



    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }
    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any): void {
        this.input[name] = value
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this[name] = !this[name]
    }
}
