import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-save-08-process-list",
    templateUrl: "./save-08-process-list.component.html",
    styleUrls: ["./save-08-process-list.component.scss"]
})
export class Save08ProcessListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    // List SaveProcess
    public listSaveProcess: any[]
    public paginateSaveProcess: any
    public perPageSaveProcess: number[]
    // Response SaveProcess
    public responseSaveProcess: any
    public listSaveProcessEdit: any


    //label_save_combobox || label_save_radio
    public saveStatusCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            request_number: '',
            save_status_code: '',
            make_date: getMoment(),
        }
        this.listSaveProcess = []
        this.paginateSaveProcess = CONSTANTS.PAGINATION.INIT
        this.perPageSaveProcess = CONSTANTS.PAGINATION.PER_PAGE


        //Master List
        this.master = {
            saveStatusCodeList: [],
        }
        //Master List



        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initSave08ProcessList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.saveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });

            }
            if (this.editID) {
                let pThis = this
                this.SaveProcessService.Save08Load(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        pThis.loadData(data)
                    }
                    // Close loading
                    pThis.global.setLoading(false)
                })
            }

            this.global.setLoading(false)
        })
    }

    constructor(
        private help: Help,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private SaveProcessService: SaveProcessService
    ) { }

    onClickSave08List(): void {
        // if(this.validateSave08List()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callSave08List(this.saveData())
        // }
    }
    //! <<< Call API >>>
    callSave08List(params: any): void {
        let pThis = this
        this.SaveProcessService.Save08ListPage(this.help.GetFilterParams(params, this.paginateSaveProcess)).subscribe((data: any) => {
            // if(isValidSave08ListResponse(res)) {
            if (data) {
                // Set value
                pThis.listData(data)
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickSave08RequestIndexNew(): void {
        //if(this.validateSave08RequestIndexNew()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callSave08RequestIndexNew(this.saveData())
        //}
    }
    validateSave08RequestIndexNew(): boolean {
        let result = validateService('validateSave08RequestIndexNew', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callSave08RequestIndexNew(params: any): void {
        let pThis = this
        this.SaveProcessService.Save08RequestIndexNew(params).subscribe((data: any) => {
            // if(isValidSave08RequestIndexNewResponse(res)) {
            if (data) {
                // Set value
                pThis.loadData(data)
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickSave08RequestIndexSave(): void {
        //if(this.validateSave08RequestIndexSave()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callSave08RequestIndexSave(this.saveData())
        //}
    }
    validateSave08RequestIndexSave(): boolean {
        let result = validateService('validateSave08RequestIndexSave', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callSave08RequestIndexSave(params: any): void {
        let pThis = this
        this.SaveProcessService.Save08RequestIndexSave(params).subscribe((data: any) => {
            // if(isValidSave08RequestIndexSaveResponse(res)) {
            if (data) {
                // Set value
                pThis.loadData(data)
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickSave08Add(): void {
        this.listSaveProcess.push({
            index: this.listSaveProcess.length + 1,
            make_date_text: null,
            request_number: null,
            request_date_text: null,
            save080_revoke_type_name: null,
            index_1: null,
            index_2: null,
            save_status_name: null,
            cancel_reason: null,

        })
        this.changePaginateTotal(this.listSaveProcess.length, 'paginateSaveProcess')
    }

    onClickSave08Edit(item: any): void {
        var win = window.open("save-08-process/edit/" + item.id)
    }


    onClickSave08Delete(item: any): void {
        // if(this.validateSave08Delete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listSaveProcess.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listSaveProcess.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listSaveProcess.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {
                if (rs === true) rs = ""

                let ids = []

                for (let i = 0; i < this.listSaveProcess.length; i++) {
                    if (this.listSaveProcess[i].is_check) {
                        this.listSaveProcess[i].cancel_reason = rs
                        this.listSaveProcess[i].status_code = "DELETE"
                        this.listSaveProcess[i].is_deleted = true

                        if (true && this.listSaveProcess[i].id) ids.push(this.listSaveProcess[i].id)
                        // else this.listSaveProcess.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callSave08Delete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listSaveProcess.length; i++) {
                    this.listSaveProcess[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callSave08Delete(params: any, ids: any[]): void {
        this.SaveProcessService.Save08Delete(params).subscribe((data: any) => {
            // if(isValidSave08DeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listSaveProcess.length; i++) {
                    if (this.listSaveProcess[i].id == id) {
                        this.listSaveProcess.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listSaveProcess.length; i++) {
                this.listSaveProcess[i] = i + 1
            }

            this.onClickSave08List()
            // Close loading
            this.global.setLoading(false)
        })
    }


    // Modal Location
    autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
        if (object[name].length >= length) {
            console.log("d")
            this.is_autocomplete_ListModalLocation_show = true

            let params = {
                filter: { name: object[name] },
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListModalLocation(params)
        }
    }
    autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_ListModalLocation_show = false
        }, 200)
    }
    callAutocompleteChangeListModalLocation(params: any): void {
        if (this.input.is_autocomplete_ListModalLocation_load) return
        this.input.is_autocomplete_ListModalLocation_load = true
        let pThis = this
        this.SaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
            if (data) {
                if (data.length == 1) {
                    setTimeout(function () {
                        pThis.autocompleteChooseListModalLocation(data[0])
                    }, 200)
                } else {
                    pThis.autocompleteListListModalLocation = data
                }
            }
        })
        this.input.is_autocomplete_ListModalLocation_load = false
    }
    autocompleteChooseListModalLocation(data: any): void {
        this.inputModalAddress.address_sub_district_code = data.code
        this.inputModalAddress.address_sub_district_name = data.name
        this.inputModalAddress.address_district_code = data.district_code
        this.inputModalAddress.address_district_name = data.district_name
        this.inputModalAddress.address_province_code = data.province_code
        this.inputModalAddress.address_province_name = data.province_name
        this.inputModalAddress.address_country_code = data.country_code
        this.inputModalAddress.address_country_name = data.country_name
        this.is_autocomplete_ListModalLocation_show = false
    }
    onClickSave04ModalAddressSave(): void {
        Object.keys(this.inputModalAddress).forEach((item: any) => {
            this.inputModalAddressEdit[item] = this.inputModalAddress[item]
        })
        this.toggleModal("isModalPeopleEditOpen")
    }
    // Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listSaveProcess = data.save_process_list || []
        let index = 1
        index = 1
        this.listSaveProcess.map((item: any) => { item.is_check = false; item.index = index++; return item })
        this.changePaginateTotal(this.listSaveProcess.length, 'paginateSaveProcess')

    }

    listData(data: any): void {
        this.listSaveProcess = data.list || []
        this.help.PageSet(data, this.paginateSaveProcess)
    }

    saveData(): any {
        const params = {
            request_number: this.input.request_number,
            request_index: this.input.request_index,
            make_date: this.input.make_date,
            save_status_code: this.input.save_status_code,
        }

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
