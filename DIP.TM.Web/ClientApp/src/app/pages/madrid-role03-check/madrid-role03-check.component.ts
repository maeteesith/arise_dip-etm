import { ModalMadridRole03PresentHeader } from './../modal-madrid-role03-present-header/modal-madrid-role03-present-header.component';
import { ModalMadridRole03EditDetail } from './../modal-madrid-role03-edit-detail/modal-madrid-role03-edit-detail.component';
import { ModalMadridRole03Holder } from './../modal-madrid-role03-holder/modal-madrid-role03-holder.component';
import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { MadridRole03PresentComponent } from '../madrid-role03-present/madrid-role03-present.component';
import { MadridRole03PaymentComponent } from '../madrid-role03-payment/madrid-role03-payment.component';
import { MadridRole03MailComponent } from '../madrid-role03-mail/madrid-role03-mail.component';
import { MadridRole03OrderComponent } from '../madrid-role03-order/madrid-role03-order.component';
import { MadridRole03DocumentComponent } from '../madrid-role03-document/madrid-role03-document.component';
import { MadridRole03ViComponent } from '../madrid-role03-vi/madrid-role03-vi.component';

@Component({
  selector: 'app-madrid-role03-check',
  templateUrl: './madrid-role03-check.component.html',
  styleUrls: ['./madrid-role03-check.component.scss']
})
export class MadridRole03CheckComponent implements OnInit {

  constructor(
    public dialog:MatDialog
  ) { }

  ngOnInit() {
  }

  Modal1(){
      const dialogRef = this.dialog.open(ModalMadridRole03Holder, {
          width: "calc(100% - 80px)",
          maxWidth: "1000px",
          data: {}
        });
        dialogRef.afterClosed().subscribe(res => {
        });

  }

  ModalPresent() {
    const dialogRef = this.dialog.open(MadridRole03PresentComponent, {
      width: "calc(85% - 80px)",
      height: "calc(100% - 80px)",
      maxWidth: "1000px",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

  modalPayment() {
    const dialogRef = this.dialog.open(MadridRole03PaymentComponent, {
      width: "calc(85% - 80px)",
      maxWidth: "1000px",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

  Modaleditdetail() {
    const dialogRef = this.dialog.open(ModalMadridRole03EditDetail, {
      width: "calc(100% - 80px)",
      maxWidth: "1000px",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

  ModalMail() {
    const dialogRef = this.dialog.open(MadridRole03MailComponent, {
      width: "calc(100%)",
      height: "calc(100% - 80px)",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

  ModalOrder() {
    const dialogRef = this.dialog.open(MadridRole03OrderComponent, {
      width: "calc(85% - 80px)",
      maxWidth: "1000px",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

  ModalDocument() {
    const dialogRef = this.dialog.open(MadridRole03DocumentComponent, {
      width: "calc(40% - 80px)",
      height: "calc(100% - 80px)",
      maxWidth: "1000px",
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }

  ModalVI() {
    const dialogRef = this.dialog.open(MadridRole03ViComponent, {
      width: "calc(100%)",
     
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });

  }
  ModalPresentheader(){
    const dialogRef = this.dialog.open(ModalMadridRole03PresentHeader, {
      width: "calc(100%)",
     
      data: {}
    });
    dialogRef.afterClosed().subscribe(res => {
    });



  }
  
}
