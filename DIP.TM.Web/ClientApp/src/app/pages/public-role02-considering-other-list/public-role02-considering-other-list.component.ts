import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-public-role02-considering-other-list",
    templateUrl: "./public-role02-considering-other-list.component.html",
    styleUrls: ["./public-role02-considering-other-list.component.scss"]
})
export class PublicRole02ConsideringOtherListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    // List PublicRole02ConsideringOther
    public listPublicRole02ConsideringOther: any[]
    public paginatePublicRole02ConsideringOther: any
    public perPagePublicRole02ConsideringOther: number[]
    // Response PublicRole02ConsideringOther
    public responsePublicRole02ConsideringOther: any
    public listPublicRole02ConsideringOtherEdit: any


    // List PublicProcess
    public listPublicProcess: any[]
    public paginatePublicProcess: any
    public perPagePublicProcess: number[]
    // Response PublicProcess
    public responsePublicProcess: any

    ////label_save_combobox || label_save_radio
    //public postRoundTypeCodeList: any[]
    //public departmentGroupCodeList: any[]
    //public publicRole05StatusCodeList: any[]
    ////label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial

    public tableList: any


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            ////post_round_public_post_date: getMoment(),
            //post_round_type_code: '',
            //department_group_code: '',
            //document_role05_status_code: '',
            //request_number: '',
        }
        this.listPublicRole02ConsideringOther = []
        this.paginatePublicRole02ConsideringOther = CONSTANTS.PAGINATION.INIT
        this.perPagePublicRole02ConsideringOther = CONSTANTS.PAGINATION.PER_PAGE

        this.listPublicProcess = []

        //Master List
        this.master = {
            //postRoundTypeCodeList: [],
            //departmentGroupCodeList: [],
            //publicRole05StatusCodeList: [],
        }
        //Master List


        if (!this.tableList) {
            this.tableList = {
                column_list: {
                    index: "#",
                    request_number: "เลขที่คำขอ",
                    public_start_date: "วันที่ประกาศ",
                    document_role04_receive_date: "วันที่เสนอ",
                    book_index: "เล่มที่",
                    post_round_type_name: "ประเภท",
                    document_role05_receive_send_date: "วันที่พิจารณา",
                    document_role05_receiver_by_name: "ผู้พิจารณา",
                    public_considering_other_remark: "รายละเอียดเพิ่มเติม",
                    document_role04_receive_status_name: "สถานะ",
                },
                command_item: [{
                    name: "item_edit"
                }],
            }
        }


        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //


        this.contactAddress = {
            address_type_code: "OWNER"
        }
        this.modalAddress = {}

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.route.queryParams.subscribe((param_url: any) => {
        })

        this.forkJoinService.initPublicRole02ConsideringOtherList().subscribe((data: any) => {
            if (data) {
                this.help.Clone(data, this.master)
                this.master.documentRole04ReceiveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.document_role04_receive_status_code = ""
                this.master.postRoundTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.post_round_type_code = ""
            }
            //if (this.editID) {
            //  this.PublicProcessService.PublicRole02ConsideringOtherLoad(this.editID).subscribe((data: any) => {
            //    if (data) {
            //      // Manage structure
            //      this.loadData(data)
            //    }
            //    // Close loading
            this.global.setLoading(false)
            //    this.automateTest.test(this)
            //  })
            //} else {
            //  this.global.setLoading(false)
            //  this.automateTest.test(this)
            //}

        })
    }

    constructor(
        private auth: Auth,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private PublicProcessService: PublicProcessService
    ) { }

    onClickCommand($event) {
        console.log($event)
        if ($event) {
            if ($event.command) {
                if ($event.command == "item_edit") {
                    window.open("public-role02-considering-other/edit/" + $event.object_list[0].id)
                }
            } else {
                this.onClickPublicRole02ConsideringOtherList($event)
            }
        }
    }
    onClickPublicRole02ConsideringOtherList(paging = null): void {
        console.log(paging)
        // if(this.validatePublicRole02ConsideringOtherList()) {
        // Call api
        var param = {}

        Object.keys(this.tableList.column_list).forEach((item: any) => {
            console.log(item)
            if (this.input[item]) {
                console.log(item)
                param[item] = this.input[item]
            }

            if (item.endsWith("_name")) {
                var code = item.replace("_name", "_code")
                if (this.input[code]) {
                    param[code] = this.input[code]
                }
            }

            if (item.endsWith("_date")) {
                var start_date = item.replace("_date", "_start_date")
                if (this.input[start_date]) {
                    param[start_date] = this.input[start_date]
                }
                var end_date = item.replace("_date", "_end_date")
                if (this.input[end_date]) {
                    param[end_date] = this.input[end_date]
                }
            }
        })

        this.callPublicRole02ConsideringOtherList(this.help.GetFilterParams(param, paging))
        // }
    }
    //! <<< Call API >>>
    callPublicRole02ConsideringOtherList(params: any): void {
        this.global.setLoading(true)
        this.PublicProcessService.List("PublicConsideringOther", params).subscribe((data: any) => {
            // if(isValidPublicRole02ConsideringOtherListResponse(res)) {
            if (data) {
                // Set value
                //this.tableList.SetPaging(data)
                this.tableList.SetDataList(data)
                //this.listData(data)
                this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    //onClickPublicRole02ConsideringOtherPublicRole02ConsideringOtherAdd(): void {
    //    this.listPublicRole02ConsideringOther.push({
    //        index: this.listPublicRole02ConsideringOther.length + 1,
    //        request_number: null,
    //        request_date_text: null,
    //        request_item_sub_type_1_code_text: null,
    //        document_role05_date_text: null,
    //        department_group_name: null,
    //        book_by_name: null,
    //        document_role05_receive_date_text: null,
    //        document_role05_receiver_by_name: null,
    //        document_role05_remark: null,
    //        document_role05_status_name: null,

    //    })
    //    this.changePaginateTotal(this.listPublicRole02ConsideringOther.length, 'paginatePublicRole02ConsideringOther')
    //}

    onClickPublicRole02ConsideringOtherPublicRole02ConsideringOtherEdit(item: any): void {
        var win = window.open("/" + item.id)
    }

    //onClickPublicRole02ConsideringOtherAutoSplit(): void {
    //  this.PublicProcessService.PublicRole02ConsideringOtherAutoSplit().subscribe((data: any) => {
    //    // if(isValidPublicRole02ConsideringOtherListResponse(res)) {
    //    if (data) {
    //      // Set value
    //      //this.tableList.SetPaging(data)
    //      this.onClickPublicRole02ConsideringOtherList()
    //      //this.tableList.SetDataList(data)
    //      //this.listData(data)
    //      //this.automateTest.test(this, { list: data.list })
    //    }
    //    this.global.setLoading(false)
    //    // }
    //    // Close loading
    //  })
    //}

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        //this.listPublicRole02ConsideringOther = data.publicRole05item_list || []
        //this.help.PageSet(data, this.paginatePublicRole02ConsideringOther)

    }

    //listData(data: any): void {
    //  //this.listPublicRole02ConsideringOther = data.list || []
    //  //this.help.PageSet(data, this.paginatePublicRole02ConsideringOther)
    //  this.tableList.SetDataList(data)
    //}

    saveData(): any {
        let params = this.input

        //params.publicRole05item_list = this.listPublicRole02ConsideringOther || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalChecks / this[name].itemsPerPage)
    }
    displayMoney(value: any): any {
        return displayMoney(value);
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

}
