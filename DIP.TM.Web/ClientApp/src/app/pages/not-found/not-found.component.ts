import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { ROUTE_PATH } from "../../helpers";

@Component({
  selector: "app-not-found",
  templateUrl: "./not-found.component.html",
  styleUrls: ["./not-found.component.scss"]
})
export class NotFoundComponent {
  constructor(private router: Router) {
    let isEForm = location.pathname.includes("eform");
    if (isEForm) {
      this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
    } else {
      this.router.navigate([ROUTE_PATH.DASHBOARD.LINK]);
    }
  }
}
