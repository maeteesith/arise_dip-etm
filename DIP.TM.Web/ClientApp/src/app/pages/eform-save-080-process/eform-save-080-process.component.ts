import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from '../../services/fork-join2.service'
import { eFormSaveProcessService } from '../../services/eform-save-process.service'
import { AutomateTest } from '../../test/automate_test'

import {
  CONSTANTS,
  ROUTE_PATH,
  validateService,
  displayDate,
  clone,
  viewPDF,
  displayAddress,
  getParamsOverwrite
} from '../../helpers'

@Component({
  selector: "app-eform-save-080-process",
  templateUrl: "./eform-save-080-process.component.html",
  styleUrls: ["./eform-save-080-process.component.scss"]
})
export class eFormSave080ProcessComponent implements OnInit, DeactivationGuarded {
  //TODO >>> Declarations <<</'
  public menuList: any[];
  public editID: any;
  public input: any;
  public seachData: any;
  public beforsearch: any;
  public validate: any;
  public master: any;
  public saveInput: any;
  public eform_number: any;
  public Next4: any;
  public listdropdownContact: any;
  public receiver_contact_address: any;
  public firstOwnerName: string;
  public firstOwnerTel: string;
  public popup: any;
  public contact_index: number;
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any
  public ContactWord: any;

  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubstep: number;
  public progressPercent: number;
  public stepPass: number;
  public email: string;
  public telephone: string;
  public save080_request_item_type_code: string;
  public save080_revoke_type_code: string;
  public SAVE080_REPRESENTATIVE_CONDITION_TYPE_CODE: string;
  public Feedback: false;
  public findVal: any;
  public findVal2: any;
  public rule_number: string;

  public ContactChangeType: string;
  public listdropdownReceiverContact: any;
  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;


  ngOnInit() {
    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubstep = 1;
    this.progressPercent = 0;
    this.stepPass = 1;

    this.menuList = [
      {
        id: 1,
        number: 1,
        name: 'บันทึกเพื่อขอรับไฟล์',
        required: true,
        canedit: false
      },

      {
        id: 2,
        number: 2,
        name: 'ประเภทการเพิกถอน',
        required: true,
        canedit: false
      },
      {
        id: 3,
        number: 3,
        name: 'คำขอให้เพิกถอนการจดทะเบียนคำขอเลขเลขที่',
        required: true,
        canedit: false
      },
      {
        id: 4,
        number: 4,
        name: 'ประเภทการยื่น',
        required: true,
        canedit: false
      },
      {
        id: 5,
        number: 5,
        name: 'ผู้ขอ',
        required: true,
        canedit: false
      },
      {
        id: 6,
        number: 6,
        name: 'สถานที่ติดต่อในประเทสไทย',
        canedit: false
        //hasSub: true,
        //subList: [
        //  {
        //    number: 1,
        //    name: 'ภาพเครื่องหมาย'
        //  },
        //  {
        //    number: 2,
        //    name: 'เครื่องหมายเสียง'
        //  }
        //]
      },
      {
        id: 7,
        number: 7,
        name: 'ระบุเหตุในการขอให้เพิกถอน',
        canedit: false
      },
      {
        id: 8,
        number: 8,
        name: 'บัญชีเอกสารหรือหลักฐานประกอบอุทธรณ์',
        canedit: false
      },

      {
        id: 9,
        number: 9,
        name: 'เอกสารหลักฐานประกอบคำขอจดทะเบียน',
        canedit: false
      },
      {
        id: 10,
        number: 10,
        name: 'ค่าธรรมเนียม',
        canedit: false
      },
      {
        id: 11,
        number: 11,
        name: 'สำเร็จ',
        canedit: false
      }
    ]

    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}

    this.response = {
      load: {},
    };

    //Master List
    this.master = {
    }
    //Master List



    //
    this.receiver_contact_address =
    {
      descritionS5: null,

      address_type_codeS5: null,
      nationality_codeS5: null,
      career_codeS5: null,
      card_type_codeS5: null,
      card_type_nameS5: null,
      card_numberS5: null,
      receiver_type_codeS5: null,
      nameS5: null,
      house_numberS5: null,
      village_numberS5: null,
      alleyS5: null,
      streetS5: null,
      address_sub_district_codeS5: null,
      address_district_codeS5: null,
      address_province_codeS5: null,
      postal_codeS5: null,
      address_country_codeS5: null,
      telephoneS5: null,
      faxS5: null,
      emailS5: null,
      sex_codeS5: null,

    }

    this.beforsearch = {
      save080_request_item_type_code: null,
      save080_revoke_type_code: null,
      email_beforsearch: null,
      telephone_beforsearch: null
    }
    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
      isSearch: false,
      isWarning: false,
    };

    this.input = {
      indexEdit: undefined,
      point: "",
      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },

      receiverMark: {
        nameother: null,

      },

      save080_submit_type_code: null,
      rule_number: "",
      save080_representative_condition_type_code: "AND_OR",

     
      CHECKEDITSTEP3: null,
      ownerDomesticContactAddress: 0,
      agentDomesticContactAddress: 0,
      subagentDomesticContactAddress: 0,
      save080_contact_type_code: null,
      //save080_contact_index: null,
      dimension_image: 2,
      make_date: null,
      request_number: null,
      search_number: "" ,
      register_number: null,
      load_receiver_people_type_code: "",
      TEST_TEST2: null,
      save080_people_type_code: null,
      people_list: [],
      representative_list: [],
      save080PeopleTypeList: null,
      Feedback: null,
      remark_7: null,

      evidence_name: "",
      evidence_telephone: "",

      is_8_1: false,
      is_8_2: false,
      is_8_3: false,
      is_8_4: false,
      is_8_5: false,
      is_8_6: false,
      is_8_7: false,
      is_8_8: false,
      is_8_9: false,
      is_8_10: false,




      is_6_1: false,
      is_6_2: false,
      is_6_3: false,
      is_6_4: false,
      payer_name: null,
      total_price: null,
      imageDimension2: {
        file: {},
        blob: null
      },
      imageDimension3_1: {
        file: {},
        blob: null
      },
      imageDimension3_2: {
        file: {},
        blob: null
      },
      imageDimension3_3: {
        file: {},
        blob: null
      },
      imageDimension3_4: {
        file: {},
        blob: null
      },
      sizeX: 5,
      sizeY: 5,
      isResize: false,
      audio: {
        input: "",
        validate: "",
        file: {},
        sound: null,
        palyer: null,
        action: false
      },
      imageNote: {
        file: {},
        blob: null
      },
      listTranslation: [],
      translationItem: {},
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0
      },
      imageMark: {
        file: {},
        blob: null
      }
    };

    this.input.listEvidence = [{
      evidence_type: "",
      evidence_subject: "",
      number: "",
      note: "",
    }];

    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalDomesticContactAddressOpen: true
    };

    this.SAVE080_REPRESENTATIVE_CONDITION_TYPE_CODE = "AND_OR";
    //

    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initeFormSave080Process().subscribe((data: any) => {
      if (data) {
        this.master = data;
        data.requestTypeList.forEach((item: any) => {
          if (item.code === "80" && !this.editID) {
            this.input.total_price = item.value_2;
          }
        });
        console.log(this.master)
        this.route.queryParams.subscribe(params => {
          //this.editID = params['eform_number']; //[edit]comment_tag: comment

          console.log("ID", this.editID)
          this.eform_number = { eform_number: this.editID };

        });
      }

      if (this.editID) {
        this.eFormSaveProcessService.eFormSave080Load(this.editID).subscribe((data: any) => { //[edit]comment_tag: ("", this.eform_number) to (this.editID)
          console.log(data)
          if (data) {
            // Manage structure

            this.loadData(data);
            this.response.load = {
              eform_number: data.eform_number,
              id: data.id,
              wizard: data.wizard,
            };

            this.input.search_number = data.request_number;
            this.input.listOwnerMark = data.people_list;
            this.input.listAgentMark = data.representative_list;

            if (this.input.receiver_contact_address_list) {
              this.receiver_contact_address = clone(this.input.receiver_contact_address_list[0]);
            }

            this.input.agenReceiverMarkItem = {
              receiver_type_code: "PEOPLE",
              sex_code: "MALE",
              nationality_code: "AF",
              career_code: "ค้าขาย",
              address_country_code: "TH"
            }

            this.input.ownerMarkItem = {
              is_contact_person: false,
              receiver_type_code: "PEOPLE",
              nationality_code: "TH",
              address_country_code: "TH",
            },

            this.input.agentMarkItem = {
              receiver_type_code: "PEOPLE",
              sex_code: "MALE",
              nationality_code: "AF",
              career_code: "ค้าขาย",
              address_country_code: "TH"
            }
            this.input.receiverMark = {
              receiver_type_code: "PEOPLE",
              sex_code: "MALE",
              nationality_code: "AF",
              career_code: "ค้าขาย",
              address_country_code: "TH"
            }

          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private router: Router,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eFormSaveProcessService: eFormSaveProcessService
  ) { }



  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }


  getLanguageLabel(code: any): string {
    let label = "";
    this.master.languageCodeList.forEach((item: any) => {
      if (item.code === code) {
        label = item.name;
      }
    });
    return label;
  }

  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.eFormSaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    

    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }



  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    } else if (this.currentID === 2) {
      let result = validateService(
        "validateEFormSave08ProcessStep2",
        this.beforsearch


      );

      this.validate = result.validate;
      console.log(result.validate)


      return result.isValid;

    } else if (this.currentID === 7) {
      let result = validateService(
        "validateRemark7",
        this.input
      );


      this.validate = result.validate;

      return result.isValid
    } else if (this.currentID === 6) {
      console.log("Name", this.receiver_contact_address.nameS5);

      if (this.input.save080_contact_type_code == 'OTHERS') {

        let result = validateService(
          "validateEFormSave08ProcessStep5",
          this.receiver_contact_address
        );


        this.validate = result.validate;
        console.log(result.validate)
        return result.isValid;
      }
      //if (this.input.save080_contact_type_code != 'OTHERS') {

      //  let result = validateService(
      //    "validateEFormSave08ProcessStep5part1",
      //    console.log("iiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii",this.input.save080_contact_type_code),
      //    this.input
      //  );
      //  this.validate = result.validate;
      //  console.log(result.validate)
      //  return result.isValid;
      //}


    }
    return true;



  }
  //! <<< Wizard >>>
  changeStep(action: string): void {
    console.log("beforsearch.save080_request_item_type_code", this.beforsearch.save080_request_item_type_code)
    console.log("input.save080_contact_type_codeh", this.input.save080_contact_type_code)
    if (this.validateWizard()) {

      //STEP3

      if (this.currentID == 3) {
        console.log("tttttttttttttttttttttttttttttttttttttttttt")
        this.input.save080_request_item_type_code = null
        this.input.save080_revoke_type_code = null

        this.input.save080_request_item_type_code = this.beforsearch.save080_request_item_type_code
        this.input.save080_revoke_type_code = this.beforsearch.save080_revoke_type_code




        if (this.beforsearch.save080_request_item_type_code == "TYPE1") {

          //this.input.save080_contact_type_code = this.beforsearch.save080_revoke_type_code
        }

        if (this.beforsearch.save080_request_item_type_code == "TYPE2") {
          
          this.input.save080_revoke_type_code = null
          this.beforsearch.save080_revoke_type_code = null
        }

        //this.input.save080PeopleTypeList = this.input.save080_revoke_type_code

        if (this.beforsearch.save080_revoke_type_code == "TYPE1") {

          this.input.save080_contact_type_code = "OWNER"
        }

        if (this.beforsearch.save080_revoke_type_code == "TYPE2") {

          this.input.save080_contact_type_code = "CONCERN"
        }
        if (this.beforsearch.save080_revoke_type_code == "TYPE3") {

          this.input.save080_contact_type_code = "ALLOW"
        }
        //END STEP3


        console.log("tttttttttttttttttttttttttttttttttttttttttt", this.input.save080_contact_type_code)
        //console.log("tttttttttttttttttttttttttttttttttttttttttt", this.input.save080_request_item_type_code)
        //console.log("tttttttttttttttttttttttttttttttttttttttttt", this.input.save080_revoke_type_code)

      }

      //STEP5


      this.menuList[this.currentStep - 1].canedit = true;

      console.log(this.input.save080_request_item_type_code)
      console.log(this.input.save080_revoke_type_code)

      console.log(this.input)
      console.log("DATAADDRESS", this.receiver_contact_address)


      action === "next" ? this.currentStep++ : this.currentStep--;
      this.currentID = this.menuList[this.currentStep - 1].id
      this.currentSubstep = 1;
      this.stepPass = this.stepPass + 1;
      this.calcProgressPercent(this.currentStep);

      if (this.stepPass == (this.currentStep - 1))


        if (this.currentID == 5) {
          //this.input.save080_contact_type_code = this.input.save080PeopleTypeList
          //console.log("ggggggggggggggggggggggggggggggggggggg", this.input.save080_contact_type_code)
        }
      //STEP5

      

    }

    this.contactTypeChange()
  }
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
    console.log(name, value);
    console.log("SAVExxx_REPRESENTATIVE_CONDITION_TYPE_CODE :",this.input.SAVExxx_REPRESENTATIVE_CONDITION_TYPE_CODE)
    //console.log(this.input.save080_revoke_type_code)
    if (this.currentStep == 2) {
      this.input.save080_request_item_type_code = this.beforsearch.save080_request_item_type_code
      this.input.save080_revoke_type_code = this.beforsearch.save080_revoke_type_code
    }

    if (this.currentID == 5) {
      console.log("CurrentStep == 5");
      //this.input.save080_contact_type_code = this.input.save080PeopleTypeList
    }
  }




  reRunMenuNumber(): void {
    let number = 1;
    this.menuList.forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
      }
    });
  }

  backtomenu(canedit: any, number: any): void {
    console.log(canedit);
    if (canedit == true) {
      this.currentStep = number;
      this.currentID = this.menuList[this.currentStep - 1].id

      for (var i = 1; i <= this.menuList.length + 1; i++) {
        if (i <= this.stepPass - 1) {
          this.menuList[i].canedit = true;
        }
      }
    }
  }

  changeSubStep(action: number): void {
    if (this.validateWizard()) {
      this.currentSubstep = this.currentSubstep + action;
    }
  }
  calcProgressPercent(currentStep: number): void {
    this.progressPercent = Math.round((currentStep / this.menuList.length) * 100);
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }



  findClick(): void {
    this.findVal = true;
  }
  findClick2(): void {
    this.findVal2 = true;
  }


  searchNumber(): void {
    this.beforsearch.email_beforsearch = this.input.email
    this.beforsearch.telephone_beforsearch = this.input.telephone
    if (this.beforsearch.save080_request_item_type_code === 'TYPE1'){
      this.callSearchRequestNumber(this.input.search_number)
    } else{
      this.callSearchContractNumber(this.input.search_number)
    }
  }

  //! <<< Call API >>>
  callSearchRequestNumber(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave080SearchRequestNumber(params).subscribe((data: any) => {
      this.loadConsideringSimilarDocument(data);
    })
  }

  callSearchContractNumber(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave080SearcContractNumber(params).subscribe((data: any) => {
      this.loadConsideringSimilarDocument(data);
    })
  }

  loadConsideringSimilarDocument(data: any) {
    if (data && data.is_search_success) {
      // Set value
      this.seachData = data;
      //this.listData(data)
      this.automateTest.test(this, { list: data.list })
      this.input.telephone = this.telephone;
      this.input.email = this.email;
      this.input.ownerMarkItem = {
        receiver_type_code: "PEOPLE",
        sex_code: "MALE",
        nationality_code: "AF",
        career_code: "ค้าขาย",
        address_country_code: "TH"
      }

      this.input.receiverMarkItem = {


      }

      this.input.agenReceiverMarkItem = {
        receiver_type_code: "PEOPLE",
        sex_code: "MALE",
        nationality_code: "AF",
        career_code: "ค้าขาย",
        address_country_code: "TH"
      }

      this.input.agentMarkItem = {
        receiver_type_code: "PEOPLE",
        sex_code: "MALE",
        nationality_code: "AF",
        career_code: "ค้าขาย",
        address_country_code: "TH"
      }
      this.input.receiverMark = {
        receiver_type_code: "PEOPLE",
        sex_code: "MALE",
        nationality_code: "AF",
        career_code: "ค้าขาย",
        address_country_code: "TH"
      }

      this.input.receiver_people_list = [];
      this.input.receiver_representative_list = [];

      this.input.save080_request_item_type_code = this.beforsearch.save080_request_item_type_code
      this.input.save080_revoke_type_code = this.beforsearch.save080_revoke_type_code
      this.input.email = this.beforsearch.email_beforsearch
      this.input.telephone = this.beforsearch.telephone_beforsearch

      if (this.beforsearch.save080_revoke_type_code == "TYPE1") {

        this.input.save080_contact_type_code = "OWNER"
      }
      if (this.beforsearch.save080_revoke_type_code == "TYPE2") {

        this.input.save080_contact_type_code = "CONCERN"
      }
      if (this.beforsearch.save080_revoke_type_code == "TYPE3") {

        this.input.save080_contact_type_code = "ALLOW"
      }

      //this.input.save080PeopleTypeList = this.input.save080PeopleTypeList;
      
      this.input.request_date_text = this.seachData.request_date_text;
      this.input.request_number = this.seachData.save010.request_number;
      if (this.seachData.save010.people_list.length > 0)
      {
       this.input.evidence_name = this.seachData.save010.people_list[0].name;
       this.input.evidence_telephone = this.seachData.save010.people_list[0].telephone;
      }
      this.input.registration_number = this.seachData.registration_load_number;
      this.input.load_receiver_people_type_code = this.seachData.load_receiver_people_type_code;      
      this.input.listOwnerMark = this.seachData.people_load_list;
      this.input.listAgentMark = this.seachData.representative_load_list;
      if (this.input.listOwnerMark.length > 0){
        this.input.payer_name = this.input.listOwnerMark[0].name;
      }
    } else {
      console.warn(`request_number is invalid.`);
      this.input.request_date_text = "";
      this.validate.request_number = data.alert_msg;
      this.togglePopup("isSearch");
    }

    this.global.setLoading(false)
  }

  // calleFormFromRequestNumberLoad(params: any): void {
  //   this.global.setLoading(true)
  //   this.eFormSaveProcessService.eFormFromRequestNumberLoad(params).subscribe((data: any) => {
  //     console.log(data);

  //     if (data && data.is_search_success) {
  //       // Set value
  //       this.listData(data)
  //       this.automateTest.test(this, { list: data.list })
  //       this.input.telephone = this.telephone;
  //       this.input.email = this.email;
  //       this.input.ownerMarkItem = {
  //         receiver_type_code: "PEOPLE",
  //         sex_code: "MALE",
  //         nationality_code: "AF",
  //         career_code: "ค้าขาย",
  //         address_country_code: "TH"
  //       }

  //       this.input.receiverMarkItem = {


  //       }

  //       this.input.agenReceiverMarkItem = {
  //         receiver_type_code: "PEOPLE",
  //         sex_code: "MALE",
  //         nationality_code: "AF",
  //         career_code: "ค้าขาย",
  //         address_country_code: "TH"
  //       }

  //       this.input.agentMarkItem = {
  //         receiver_type_code: "PEOPLE",
  //         sex_code: "MALE",
  //         nationality_code: "AF",
  //         career_code: "ค้าขาย",
  //         address_country_code: "TH"
  //       }
  //       this.input.receiverMark = {
  //         receiver_type_code: "PEOPLE",
  //         sex_code: "MALE",
  //         nationality_code: "AF",
  //         career_code: "ค้าขาย",
  //         address_country_code: "TH"
  //       }


        
  //       this.input.receiver_people_list = [];
  //       this.input.receiver_representative_list = [];

  //       this.input.save080_request_item_type_code = this.beforsearch.save080_revoke_type_code
  //       this.input.save080_revoke_type_code = this.beforsearch.save080_revoke_type_code
  //       this.input.email = this.beforsearch.email_beforsearch
  //       this.input.telephone = this.beforsearch.telephone_beforsearch

  //       ////STEP03
  //       if (this.beforsearch.save080_revoke_type_code == "TYPE1") {

  //         this.input.save080PeopleTypeList = "OWNER"
  //       }
  //       if (this.beforsearch.save080_revoke_type_code == "TYPE2") {

  //         this.input.save080PeopleTypeList = "CONCERN"
  //       }
  //       if (this.beforsearch.save080_revoke_type_code == "TYPE3") {

  //         this.input.save080PeopleTypeList = "ALLOW"
  //       }

  //       this.input.save080PeopleTypeList = this.input.save080PeopleTypeList
  //       /////END STEP03

  //       if (data) {
          

  //         this.popup.warning_message_show_list = []




  //         //this.input.receiver_people_list = [];
  //         //this.input.receiver_representative_list = [];

  //       } 

  //     } else {
  //       console.warn(`request_number is invalid.`);
  //       this.input.request_date_text = "";
  //       this.validate.request_number = data.alert_msg;
  //       //this.togglePopup("isSearch");
  //     }
  //     this.global.setLoading(false)
  //     // }
  //     // Close loading

  //     // if (!this.input.registration_number) {
  //     //   this.menuList = [
  //     //     {
  //     //       id: 1,
  //     //       number: 1,
  //     //       name: 'บันทึกเพื่อขอรับไฟล์',
  //     //       required: true,
  //     //       canedit: false
  //     //     },

  //     //     {
  //     //       id: 2,
  //     //       number: 2,
  //     //       name: 'ประเภทการเพิกถอน',
  //     //       required: true,
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 3,
  //     //       number: 3,
  //     //       name: 'คำขอให้เพิกถอนการจดทะเบียนคำขอเลขเลขที่',
  //     //       required: true,
  //     //       canedit: false
  //     //     },
  //     //     //{
  //     //     //  id: 4,
  //     //     //  number: 4,
  //     //     //  name: 'ประเภทการยื่น',
  //     //     //  required: true,
  //     //     //  canedit: false
  //     //     //},
  //     //     {
  //     //       id: 5,
  //     //       number: 4,
  //     //       name: 'ผู้ขอ',
  //     //       required: true,
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 6,
  //     //       number: 5,
  //     //       name: 'สถานที่ติดต่อในประเทสไทย',
  //     //       canedit: false
  //     //       //hasSub: true,
  //     //       //subList: [
  //     //       //  {
  //     //       //    number: 1,
  //     //       //    name: 'ภาพเครื่องหมาย'
  //     //       //  },
  //     //       //  {
  //     //       //    number: 2,
  //     //       //    name: 'เครื่องหมายเสียง'
  //     //       //  }
  //     //       //]
  //     //     },
  //     //     {
  //     //       id: 7,
  //     //       number: 6,
  //     //       name: 'ระบุเหตุในการขอให้เพิกถอน',
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 8,
  //     //       number: 7,
  //     //       name: 'บัญชีเอกสารหรือหลักฐานประกอบอุทธรณ์',
  //     //       canedit: false
  //     //     },

  //     //     {
  //     //       id: 9,
  //     //       number: 8,
  //     //       name: 'เอกสารหลักฐานประกอบคำขอจดทะเบียน',
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 10,
  //     //       number: 9,
  //     //       name: 'ค่าธรรมเนียม',
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 11,
  //     //       number: 10,
  //     //       name: 'สำเร็จ',
  //     //       canedit: false
  //     //     }
  //     //   ]
  //     // }
  //     // else {
  //     //   this.menuList = [
  //     //     {
  //     //       id: 1,
  //     //       number: 1,
  //     //       name: 'บันทึกเพื่อขอรับไฟล์',
  //     //       required: true,
  //     //       canedit: false
  //     //     },

  //     //     {
  //     //       id: 2,
  //     //       number: 2,
  //     //       name: 'ประเภทการเพิกถอน',
  //     //       required: true,
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 3,
  //     //       number: 3,
  //     //       name: 'คำขอให้เพิกถอนการจดทะเบียนคำขอเลขเลขที่',
  //     //       required: true,
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 4,
  //     //       number: 4,
  //     //       name: 'ประเภทการยื่น',
  //     //       required: true,
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 5,
  //     //       number: 5,
  //     //       name: 'ผู้ขอ',
  //     //       required: true,
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 6,
  //     //       number: 6,
  //     //       name: 'สถานที่ติดต่อในประเทสไทย',
  //     //       canedit: false
  //     //       //hasSub: true,
  //     //       //subList: [
  //     //       //  {
  //     //       //    number: 1,
  //     //       //    name: 'ภาพเครื่องหมาย'
  //     //       //  },
  //     //       //  {
  //     //       //    number: 2,
  //     //       //    name: 'เครื่องหมายเสียง'
  //     //       //  }
  //     //       //]
  //     //     },
  //     //     {
  //     //       id: 7,
  //     //       number: 7,
  //     //       name: 'ระบุเหตุในการขอให้เพิกถอน',
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 8,
  //     //       number: 8,
  //     //       name: 'บัญชีเอกสารหรือหลักฐานประกอบอุทธรณ์',
  //     //       canedit: false
  //     //     },

  //     //     {
  //     //       id: 9,
  //     //       number: 9,
  //     //       name: 'เอกสารหลักฐานประกอบคำขอจดทะเบียน',
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 10,
  //     //       number: 10,
  //     //       name: 'ค่าธรรมเนียม',
  //     //       canedit: false
  //     //     },
  //     //     {
  //     //       id: 11,
  //     //       number: 11,
  //     //       name: 'สำเร็จ',
  //     //       canedit: false
  //     //     }
  //     //   ]
  //     // }

  //   })
  // }



  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalReceiverFormOpen") {
          this.input.receiverMarkItem = clone(this.input.listOwnerMark[index]);
        } else if (name === "isModalAgentReceiverFormOpen") {
          this.input.agenReceiverMarkItem = clone(this.input.listAgentMark[index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.receiverMarkItem = {};
      this.input.agenReceiverMarkItem = {};
      this.input.receiverMark = {};
      this.input.receiver_contact_address_list = {};
    }
  }

  loadData(data: any): void {
    this.input = data
    console.log(this.input)

  }

  listData(data: any): void {
    this.input = data;
  }

  saveData(): any {
    let params = this.input


    return params
  }
  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
  }

  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    this.input.CHECKEDITSTEP3 = "SHOW"
    console.log(this.input.CHECKEDITSTEP3)
    console.log("validate", this.validateSaveItemModal(nameItem))
    if (this.validateSaveItemModal(nameItem)) {
      console.log("indexEdit", this.input.indexEdit)
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        this.toggleModal(nameModal);
      }
    }
  }

  validateSaveItemModal(nameItem: any): boolean {
    if (nameItem === "ownerMarkItem") {
      let result = validateService('validateEFormSave01ProcessStep3OwnerMarkItem', this.input.ownerMarkItem)
      this.validate = result.validate
      return result.isValid
    }
    return true
  }

  ValidateBymyselft(value): void {
    if (value == null) { }
  }

  getParamsSave():void {
    this.saveInput = {};
    console.log("SeeInput", this.input);
    this.saveInput.id = this.response.load.id ? this.response.load.id : null;
    this.saveInput.eform_number = this.response.load.eform_number
        ? this.response.load.eform_number
        : null;
    this.saveInput.email = this.input.email;
    this.saveInput.telephone = this.input.telephone;
    this.saveInput.make_date = this.input.request_date_text;
    this.saveInput.request_number = this.input.request_number;
    this.saveInput.register_number = this.input.registration_number;
    this.saveInput.save080_request_item_type_code = this.input.save080_request_item_type_code;
    this.saveInput.save080_revoke_type_code = this.input.save080_revoke_type_code;
    this.saveInput.save080_people_type_code = this.input.save080_people_type_code;
    this.saveInput.save080_submit_type_code = this.input.save080_submit_type_code;
    this.saveInput.rule_number = (this.input.save080_submit_type_code === "INSTRUCTION") ? this.input.rule_number : "";

    this.saveInput.people_list = [];
    this.saveInput.representative_list = [];
    this.saveInput.contact_address_list = [];
    this.saveInput.receiver_people_list = [];
    this.saveInput.receiver_representative_list = [];
    this.saveInput.save080_contact_type_code = this.input.save080_contact_type_code;
    //this.saveInput.save080_contact_index = this.input.save080_contact_index;
    this.saveInput.remark_7 = this.input.remark_7;

    this.saveInput.evidence_name = this.input.evidence_name;
    this.saveInput.evidence_telephone = this.input.evidence_telephone;

    this.saveInput.is_8_1 = this.input.is_8_1;
    this.saveInput.is_8_2 = this.input.is_8_1;
    this.saveInput.is_8_3 = this.input.is_8_1;
    this.saveInput.is_8_4 = this.input.is_8_1;
    this.saveInput.is_8_5 = this.input.is_8_1;
    this.saveInput.is_8_6 = this.input.is_8_1;
    this.saveInput.is_8_7 = this.input.is_8_1;
    this.saveInput.is_8_8 = this.input.is_8_1;
    this.saveInput.is_8_9 = this.input.is_8_1;
    this.saveInput.is_8_10 = this.input.is_8_1;

    this.saveInput.receiver_contact_address_list = [];
    this.saveInput.is_6_1 = this.input.is_6_1;
    this.saveInput.is_6_2 = this.input.is_6_2
    this.saveInput.is_6_3 = this.input.is_6_3
    this.saveInput.is_6_4 = this.input.is_6_4

    this.saveInput.payer_name = this.input.payer_name;
    this.saveInput.total_price = this.input.total_price;
    this.saveInput.save080_representative_condition_type_code = this.input.save080_representative_condition_type_code;

    this.saveInput.receiver_contact_address_list.push({
      "name": this.receiver_contact_address.nameS5,
      "house_number": this.receiver_contact_address.house_numberS5,
      "receiver_type_codeS5": this.receiver_contact_address.receiver_type_codeS5,
      "village_number": this.receiver_contact_address.village_numberS5,
      "alley": this.receiver_contact_address.alleyS5,
      "street": this.receiver_contact_address.streetS5,
      "address_sub_district_name": this.receiver_contact_address.address_sub_district_nameS5,
      "postal_code": this.receiver_contact_address.postal_codeS5,
      "email": this.receiver_contact_address.emailS5,
      "telephone": this.receiver_contact_address.telephoneS5,
      "fax": this.receiver_contact_address.faxS5,
    })

    this.input.listOwnerMark.forEach(obj => {
      this.saveInput.people_list.push({
        "address_type_code": obj.address_type_code,
        "nationality_code": obj.nationality_code,
        "career_code": obj.career_code,
        "card_type_code": obj.card_type_code,
        "card_type_name": obj.card_type_name,
        "card_number": obj.card_number,
        "receiver_type_code": obj.receiver_type_code,
        "name": obj.name,
        "house_number": obj.house_number,
        "village_number": obj.village_number,
        "alley": obj.alley,
        "street": obj.street,
        "address_sub_district_code": obj.address_sub_district_code,
        "address_district_code": obj.address_district_code,
        "address_province_code": obj.address_province_code,
        "postal_code": obj.postal_code,
        "address_country_code": obj.address_country_code,
        "telephone": obj.telephone,
        "fax": obj.fax,
        "email": obj.email,
        "sex_code": obj.sex_code,
        "is_contact_person": obj.is_contact_person

      })
    });

    this.input.listAgentMark.forEach(obj => {
      this.saveInput.representative_list.push({
        "address_type_code": obj.address_type_code,
        "nationality_code": obj.nationality_code,
        "career_code": obj.career_code,
        "card_type_code": obj.card_type_code,
        "card_type_name": obj.card_type_name,
        "card_number": obj.card_number,
        "receiver_type_code": obj.receiver_type_code,
        "name": obj.name,
        "house_number": obj.house_number,
        "village_number": obj.village_number,
        "alley": obj.alley,
        "street": obj.street,
        "address_sub_district_code": obj.address_sub_district_code,
        "address_district_code": obj.address_district_code,
        "address_province_code": obj.address_province_code,
        "postal_code": obj.postal_code,
        "address_country_code": obj.address_country_code,
        "telephone": obj.telephone,
        "fax": obj.fax,
        "email": obj.email,
        "sex_code": obj.sex_code,
        "is_contact_person": obj.is_contact_person
      })
    });
  }

  onClickViewPdfKor08(): void {
    if (this.validateWizard()) {
      this.getParamsSave();
      console.log(this.saveInput)
      viewPDF("ViewPDF/TM08", this.saveInput);
    }
  }

  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }

  save(isOverwrite: boolean): void {
    //Open loading
    this.global.setLoading(true);
    // Set param
    this.getParamsSave();
    if (!isOverwrite) {
      this.saveInput = getParamsOverwrite(this.saveInput);
    }

    //Call api
    this.calleFormSave080Save(this.saveInput);
  }

  calleFormSave080Save(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave080Save(params).subscribe((data: any) => {
      if (data) {
        this.isDeactivation = true;
        // Open toast success
        let toast = CONSTANTS.TOAST.SUCCESS;
        toast.message = "บันทึกข้อมูลสำเร็จ";
        this.global.setToast(toast);
        // Navigate
        this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
      }else {
        // Close loading
        this.global.setLoading(false);
      }      
    })
  }

  sendEmail(): void {
    this.global.setLoading(true);
    this.getParamsSave();
    
    console.log("SaveInput", this.saveInput);

    this.eFormSaveProcessService.eFormSave080Save(this.saveInput).subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
    })
  }

  ContactChange(contactType: string, index: number): void {
    console.log("index", index)
    console.log("contactType", contactType)

    this.input.listOwnerMark.forEach(data => {
      data.is_contact_person = false;
    });

    this.input.listAgentMark.forEach(data => {
      data.is_contact_person = false;
    });

    if (contactType == "OWNER") {
      this.input.listOwnerMark[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE") {
      this.input.listAgentMark[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE_PERIOD") {
      this.input.listAgentMark[index].is_contact_person = true;
    }


    console.log("people_list", this.input.listOwnerMark)
    console.log("representative_list", this.input.listAgentMark)
  }

  contactTypeChange(): void {
    this.Next4 = true;
    console.log("DATARDIO", this.input.save080_contact_type_code)
    switch (this.input.save080_contact_type_code) {
      case "OWNER":
        this.listdropdownContact = this.input.listOwnerMark;
        this.ContactWord = "ใช้ที่อยู่ของเจ้าของลำดับที่";
        this.ContactChangeType = "OWNER";
        console.log("121111111111111111111111111111111")
        break;
      case "REPRESENTATIVE":
        this.listdropdownContact = this.input.listAgentMark;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนลำดับที่"
        this.ContactChangeType = "REPRESENTATIVE";
        break;
      case "REPRESENTATIVE_PERIOD":
        this.listdropdownContact = this.input.listAgentMark;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนช่วงลำดับที่"
        console.log("33333333333333333333333333333")
        this.ContactChangeType = "REPRESENTATIVE_PERIOD";
        break;
      case "RECEIVER":
        this.listdropdownContact = this.input.listOwnerMark;
        this.ContactWord = "ใช้ที่อยู่ของผู้ได้รับอนุญาตลำดับที่";
        break;
      case "RECEIVER_PERIOD":
        this.listdropdownContact = this.input.listOwnerMark;
        this.ContactWord = "ใช้ที่อยู่ของผู้ได้รับอนุญาตช่วงลำดับที่"
        break;
      case "REVOKER":
      case "REGISTRAR":
      case "CONCERN":
        this.listdropdownContact = this.input.listOwnerMark;
        this.ContactWord = "ใช้ที่อยู่ของผู้ขอเพิกถอนลำดับที่"
        break;

      default: this.listdropdownContact = null
        break;
    }

    if (this.currentStep == 6) {
      this.contact_index = 0;
      this.ContactChange(this.input.save080_contact_type_code, this.contact_index);
    }
  }

  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! List Evidence
  onClickAddEvidence(): void {
    this.input.listEvidence.push({
      evidence_type: "",
      evidence_subject: "",
      number: "",
      note: "",
    });
  }

  onClickRemoveEvidence(index: number): void {
    this.input.listEvidence.splice(index, 1);
  }

  displayAddress(value: any): any {
    return displayAddress(value);
  }

  clearAllValidate(): void {
    this.validate = {};
  }

  getSignInform(): String {
    let result = "";
    this.input.listAgentMark.forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input.listAgentMark.forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save080_representative_condition_type_code != "AND") {
        this.input.listAgentMark.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
    }

    if (nameList === "listOwnerMark" || nameList === "listAgentMark") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }
}
