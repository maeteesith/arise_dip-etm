import { Component, OnInit } from '@angular/core';
@Component({
  selector: 'app-consider-revising-request',
  templateUrl: './consider-revising-request.component.html',
  styleUrls: ['./consider-revising-request.component.scss']
})
export class ConsiderRevisingRequestComponent implements OnInit {

  constructor() { }

  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public master: any;

  public tableList: any;

  public modal: any;
  public tableInstructionRule: any
  public tableSaveDocument: any
  public tablePeople: any
  public tableRepresentative: any
  public contactAddress: any
  public tableProduct: any
  public tableReceiptItem: any
  public tableHistory: any
  public tableDocumentScan: any
  public tab_menu_show_index: any

  public tableRequestDetail: any

  ngOnInit() {
    this.input = {};
    this.validate = {};
    this.master = {
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
    };

    this.tableList = {
      column_list: [
        {
          request_id: "741150971",
          owner: "บริษัท มังกรเจ็ดน้ำ จำกัด",
          status: "waiting"
        },
        {
          request_id: "712224642",
          owner: "บริษัท มังกรเจ็ดน้ำ จำกัด",
          status: "waiting"
        },
        {
          request_id: "790849099",
          owner: "บริษัท มังกรเจ็ดน้ำ จำกัด",
          status: "waiting"
        },
        {
          request_id: "790849099",
          owner: "บริษัท มังกรเจ็ดน้ำ จำกัด",
          status: "waiting"
        },
        {
          request_id: "851019577",
          owner: "ไอสไตล์ อิงค์",
          status: "waiting"
        },
        {
          request_id: "345773881",
          owner: "ไอสไตล์ อิงค์",
          status: "waiting-consider"
        },
        {
          request_id: "962023340",
          owner: "นางสาวนุจรี ยิ่งสิทธิสวัสดิ์",
          status: "done"
        },
        {
          request_id: "962023340",
          owner: "นางสาวนุจรี ยิ่งสิทธิสวัสดิ์",
          status: "done"
        },
        {
          request_id: "345755881",
          owner: "บริษัท ไอพี เฮิร์บ เอเชีย จำกัด",
          status: "done"
        }
      ],
    };

    this.modal = {
    };

    this.tab_menu_show_index = 1

    this.tableRequestDetail = {
      head_column_list: [
        {
          order: "#",
          command: "คำสั่ง",
          date: "วันที่สั่ง",
          registrar: "นายทะเบียน",
          book_status: "สถานะหนังสือ",
          date_order: "วันที่ส่งคำสั่ง",
          no: "เลข พณ.",
          worker: "ผู้รับงาน",
          note: "หมายเหตุ",
          status: "สถานะ"
        },
      ],

      column_list: [
        {
          command: "ตค. 4 (ม.20)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
        {
          command: "ตค. 2 (ม.13)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
        {
          command: "ตค. 1 (ม.11)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
      ]
    }

    this.tableInstructionRule = {
      column_list: {
        index: "#",
        instruction_rule_name: "คำสั่ง",
        instruction_date: "วันที่สั่ง",
        created_by_name: "นายทะเบียน",
        considering_book_status_name: "สถานะหนังสือ",
        instruction_send_date: "วันที่ส่งคำสั่ง",
        book_number: "เลข พณ.",
        document_role02_receiver_by_name: "ผู้รับงาน",
        document_role02_receive_remark: "หมายเหตุ",
        considering_instruction_rule_status_name: "สถานะ",
      },
    }

    this.tableSaveDocument = {
      column_list: {
        index: "#",
        request_type_name: "เอกสาร",
        make_date: "วันที่รับ",
        consider_similar_document_status_name: "พิจารณา",
        consider_similar_document_date: "วันที่พิจารณา",
        //instruction_rule_name: "เอกสารสแกน",
        consider_similar_document_remark: "รายละเอียด",
        //consider_similar_document_remark: "เหตุผล",
      },
    }

    this.tablePeople = {
      column_list: {
        index: "#",
        name: "ชื่อเจ้าของ",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
    }

    this.tableRepresentative = {
      column_list: {
        index: "#",
        name: "ชื่อตัวแทน",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      object_name: "ตัวแทน",
        //is_address_edit: true,
    }

    this.tableProduct = {
      column_list: {
        index: "#",
        request_item_sub_type_1_code: "จำพวกสินค้า",
        description: "รายการสินค้า/บริการ",
        //count: "จำนวน",
      },
    }

    this.tableReceiptItem = {
      column_list: {
        index: "#",
        request_type_name: "รายการ",
        receipt_number: "เลขที่ใบเสร็จ",
        receive_date: "วันที่รับเงิน",
        total_price: "จำนวนเงิน",
        receiver_name: "ผู้รับเงิน",
        receipt_status_name: "สถานะ",
      },
    }

    this.tableHistory = {
      column_list: {
        index: "#",
        make_date: "วันที่",
        created_by_name: "ผู้ออกคำสั่ง",
        description: "รายการ",
      },
    }

    this.tableDocumentScan = {
      column_list: {
        index: "#",
        request_document_collect_type_name: "ชื่อเอกสาร",
        updated_by_name: "เจ้าหน้าที่",
        created_date: "วันที่แนบ",
      },
      //has_link: ["'/File/Content/' + row_item.file_id"],
    }

    this.contactAddress = {
      address_type_code: "OWNER"
    }

  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>
  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
