/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { ModalMadridRole06LicenseComponent } from './modal-madrid-role06-license.component';

describe('ModalMadridRole06LicenseComponent', () => {
  let component: ModalMadridRole06LicenseComponent;
  let fixture: ComponentFixture<ModalMadridRole06LicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalMadridRole06LicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalMadridRole06LicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
