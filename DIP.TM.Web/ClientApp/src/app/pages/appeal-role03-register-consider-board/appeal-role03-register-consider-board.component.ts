import { Component, OnInit } from '@angular/core';
import { ForkJoinService } from '../../services/fork-join.service'

@Component({
  selector: 'app-appeal-role03-register-consider-board',
  templateUrl: './appeal-role03-register-consider-board.component.html',
  styleUrls: ['./appeal-role03-register-consider-board.component.scss']
})
export class AppealRole03RegisterConsiderBoardComponent implements OnInit {

  constructor(
    private forkJoinService: ForkJoinService,
  ) { }

  public tab_search: any
  public tab_show: any
  public appealBoardSolutionList: any []
  public tab_input: any

  ngOnInit() {
    this.tab_input = {
      similar: 'CONFIRM',
      oppose: 'CONFIRM',
      revoke: 'CONFIRM'
    }
    this.tab_search = false
    this.tab_show = true
    this.callInit()
  }

  toggle_show(){
    switch(this.tab_search){
      case true: 
        this.tab_search = false; this.tab_show = true
      break;
      case false:
        this.tab_search = true; this.tab_show = false
      break;
    }
  }

  callInit(): void{
    this.forkJoinService.initAppealReason().subscribe((data: any) => {
      if (data) {
        this.appealBoardSolutionList = data.appealBoardSolutionList.map( Item => {
          return Item
        })
      }     
    })
  }

}
