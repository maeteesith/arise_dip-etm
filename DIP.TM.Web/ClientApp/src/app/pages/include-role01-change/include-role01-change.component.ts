import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { GlobalService } from "./../../global.service";
import { ForkJoinService } from "../../services/fork-join.service";
import {
  ROUTE_PATH,
  CONSTANTS,
  clone,
  displayLabelReferenceMaster,
} from "../../helpers";
@Component({
  selector: 'app-include-role01-change',
  templateUrl: './include-role01-change.component.html',
  styleUrls: ['./include-role01-change.component.scss']
})
export class IncludeRole01ChangeComponent implements OnInit {
  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public list: any;
  public validate: any;
  public master: any;

  // Paginate
  public paginateIncludeRole: any;

  public pathChange: any;

  public tab_menu_show_index: any
  public tab_menu_file_show_index: any

  public tableList: any;
  public tableRequestDetail: any;

  constructor(
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService
  ) { }

  ngOnInit() {
    // Init
    this.input = {};
    this.validate = {};
    this.list = {};
    this.master = {};
    // Paginate
    this.paginateIncludeRole = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateIncludeRole.id = "paginateIncludeRole";

    this.pathChange = ROUTE_PATH.INCLUDE_ROLE01_CHANGE.LINK;

    this.tab_menu_show_index = 7
    this.tab_menu_file_show_index = 7

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {

    this.global.setLoading(true);
    this.forkJoinService
      .initIncludeRole01ChangePage()
      .subscribe((data: any) => {
        if (data) {
          this.master = data;

          //! Mock master data
          this.master = {

            statusCodeList: [
              { code: "waiting", name: "รอพิจารณา" },
              { code: "done", name: "เสร็จสิ้น" },
            ],
            checkList: [
              { code: "1", name: "หนังสือแจ้งคำสั่ง" },
              { code: "2", name: "การปฏิบัติตามคำสั่ง" },
              { code: "3", name: "การชำระค่าธรรมเนียม" },
              { code: "4", name: "การแจ้งฟ้องคดี / คำพิพากษา" },
              { code: "5", name: "สถานที่ติดต่อ" },
              { code: "6", name: "ชื่อคู่กรณี (ถ้ามี)" },
              { code: "7", name: "การขอถอน" },
              { code: "8", name: "ใบตอบรับ / ครบกำหนด" },
              { code: "9", name: "มีอุทธรณ์ / คำวินิจฉัยฯ" },
            ],
            headTableRequestDetail: [
              {
                order: "#",
                command: "คำสั่ง",
                date: "วันที่สั่ง",
                registrar: "นายทะเบียน",
                book_status: "สถานะหนังสือ",
                date_order: "วันที่ส่งคำสั่ง",
                no: "เลข พณ.",
                worker: "ผู้รับงาน",
                note: "หมายเหตุ",
                status: "สถานะ"
              },
            ],
          };
          console.log("this.master", this.master);
        }

      });

    // Clear validate
    this.clearAllValidate();
    // Set param
    let params = {};
    // Call api
    this.callIncludeRole01Change();
  }

  //! <<< Call API >>>
  callIncludeRole01Change(): void {
    //! Mock data list
    // this.list.trademark = [
    //   {
    //     request_id: "",
    //     request_date: "",
    //     submission_date: "",
    //     registration_id: "",
    //     due_date: "",
    //     documents: "",
    //     category: "",
    //     rin_id: ""
    //   }
    // ],
    this.list.includeRole = [
      {
        request_id: "17011323_02",
        submission_date: "20/07/2561",
        owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
        address: "เลขที่ 719 ถนนสี่พระยา",
        document: "ก.06",
        accept_date: "20/05/2561",
        consider_date: "20/06/2561",
        status: "waiting"
      },
      {
        request_id: "17011323_02",
        submission_date: "20/07/2561",
        owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
        address: "เลขที่ 719 ถนนสี่พระยา",
        document: "ก.07",
        accept_date: "20/05/2561",
        consider_date: "20/06/2561",
        status: "waiting"
      },
      {
        request_id: "17011323_02",
        submission_date: "20/07/2561",
        owner_name: "บริษัท สำนักกฎหมายสมเกียรติ จำกัด",
        address: "เลขที่ 719 ถนนสี่พระยา",
        document: "ก.20",
        accept_date: "20/05/2561",
        consider_date: "20/06/2561",
        status: "done"
      },
    ];


    this.list.tableRequestDetail = [
      {
        command: "ตค. 4 (ม.20)",
        date: "11/10/2562",
        registrar: "วีรยุทธ ศรีนักสิทธ",
        book_status: "ปกติ",
        date_order: "11/10/2562",
        no: "0704/27759",
        worker: "",
        note: "",
        status: "ordered"
      },
      {
        command: "ตค. 2 (ม.13)",
        date: "11/10/2562",
        registrar: "วีรยุทธ ศรีนักสิทธ",
        book_status: "ปกติ",
        date_order: "11/10/2562",
        no: "0704/27759",
        worker: "",
        note: "",
        status: "ordered"
      },
      {
        command: "ตค. 1 (ม.11)",
        date: "11/10/2562",
        registrar: "วีรยุทธ ศรีนักสิทธ",
        book_status: "ปกติ",
        date_order: "11/10/2562",
        no: "0704/27759",
        worker: "",
        note: "",
        status: "ordered"
      },
    ];

    this.changePaginateTotal(
      this.list.includeRole.length,
      "paginateIncludeRole"
    );

    this.global.setLoading(false);

    // this.includeRole01Service
    //   .List(params)
    //   .subscribe((data: any) => {
    //     if (data) {
    //       // Set data
    //       this.list.includeRole = data.list;
    //       // Update pagin
    //       this.changePaginateTotal(
    //         this.list.includeRole.length,
    //         "paginateIncludeRole"
    //       );
    //     }
    //     // Close loading
    //     this.global.setLoading(false);
    //   });
  }

  //! <<< Style >>>
  getStatusStyle(code: any): string {
    switch (code) {
      case "waiting":
        return "tag blue";
      case "done":
        return "tag";
      default:
        return "tag";
    }
  }



  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>
  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  onClickSlideAuthorize(action: any): void {
    if (action === "next") {
      // if (this.input.indexAuthorize < this.input.listAgentMark.length - 1) {
      //   this.input.indexAuthorize++;
      //   this.cloneAuthorize();
      // }
    } else if (action === "back") {
      // if (this.input.indexAuthorize !== 0) {
      //   this.input.indexAuthorize = this.input.indexAuthorize - 1;
      //   this.cloneAuthorize();
      // }
    }
  }

  cloneAuthorize(): void {
    let i = this.input.indexAuthorize;
    this.input.authorize.inheritor_name = this.input.listAgentMark[i].name;
    this.input.authorize.inheritor_nationality_code = this.input.listAgentMark[
      i
    ].nationality_code;
    this.input.authorize.inheritor_house_number = this.input.listAgentMark[
      i
    ].house_number;
    this.input.authorize.inheritor_alley = this.input.listAgentMark[i].alley;
    this.input.authorize.inheritor_street = this.input.listAgentMark[i].street;
    this.input.authorize.inheritor_address_sub_district_name = this.input.listAgentMark[
      i
    ].address_sub_district_name;
    this.input.authorize.inheritor_postal_code = this.input.listAgentMark[
      i
    ].postal_code;
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === "paginateIncludeRole") {
      this.input.is_check_all = false;
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Other >>>
  displayLabelReferenceMaster(list: any, code: any): string {
    return displayLabelReferenceMaster(list, code);
  }

  objLength(key: any): number {
    return Object.keys(key).length;
   }
}
