import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { GlobalService } from "../../global.service";
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from "../../services/fork-join-eform.service";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import {
  ROUTE_PATH,
  clone,
  validateService,
  CONSTANTS,
  loopDisplayDateServer,
  displayAddress,
  clearIdAndSaveId,
  viewPDF,
  getParamsOverwrite,
} from "../../helpers";

@Component({
  selector: "app-eform-save-140-process",
  templateUrl: "./eform-save-140-process.component.html",
  styleUrls: ["./eform-save-140-process.component.scss"],
})
export class eFormSave140ProcessComponent
  implements OnInit, DeactivationGuarded {
  //TODO >>> Declarations <<<
  // Init
  public editID: number;
  public menuList: any[];
  public input: any;
  public validate: any;
  public master: any;
  public response: any;
  public modal: any;
  public popup: any;
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubID: number;
  public currentSubStep: number;
  public currentMiniID: number;
  public currentMiniStep: number;
  public progressPercent: number;
  // Other
  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eformSaveProcessService: eFormSaveProcessService
  ) {}

  ngOnInit() {
    // Init
    this.editID = +this.route.snapshot.paramMap.get("id");
    this.menuList = [
      {
        id: 1,
        number: 1,
        name: "บันทึกเพื่อรับไฟล์",
        isShow: true,
        canEdit: false,
      },
      {
        id: 2,
        number: 2,
        name: "หนังสือแจ้งฟ้องคดีต่อศาลคำขอเลขที่",
        isShow: true,
        canEdit: false,
      },
      {
        id: 3,
        number: 3,
        name: "ประเภทหนังสือแจ้ง",
        isShow: true,
        canEdit: false,
      },
      {
        id: 4,
        number: 4,
        name: "ผู้แจ้ง",
        isShow: true,
        canEdit: false,
      },
      {
        id: 5,
        number: 5,
        name: "ประเภทการยื่น",
        isShow: true,
        canEdit: false,
      },
      {
        id: 6,
        number: 6,
        name: "ขอแจ้งการฟ้องร้องดำเนินคดีต่อศาล",
        isShow: true,
        canEdit: false,
      },
      {
        id: 7,
        number: 7,
        name:
          "เอกสารหลักฐานประกอบหนังสือแจ้งการฟ้องคดีหรือคำพิพากษาหรือผลคดีถึงที่สุด",
        isShow: true,
        canEdit: false,
      },
      {
        id: 8,
        number: 8,
        name: "เสร็จสิ้น",
        isShow: true,
        canEdit: false,
      },
    ];
    this.input = {
      indexEdit: undefined,
      point: "",
      isAllowEditRequestNumber: true,

      informer_type_code: "OWNER",

      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      save140_representative_condition_type_code: "AND_OR",
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listOtherMark: [],

      save140_department_code: "REGISTRAR",

      listSue: [],
      listJudgment: [],
      listFinalJudgment: [],

      listDocumentRequestChecked: [
        {
          number: 1,
          isShow: true,
          checked: false,
        },
        {
          number: 2,
          isShow: true,
          checked: false,
        },
        {
          number: 3,
          isShow: true,
          checked: false,
        },
      ],

      isCheckAllOwnerSignature: false,
      isCheckAllAgentSignature: false,
    };
    this.validate = {};
    this.master = {
      addressEformCardTypeCodeList: [],
      addressCountryCodeList: [],
      addressRepresentativeConditionTypeCodeList: [],
      addressTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
      addressCareerCodeList: [],
      addressNationalityCodeList: [],
      representativeTypeCodeList: [],
      informerTypeCodeList: [],
      informTypeCodeList: [],
      submitTypeCodeList: [],
      depertmentTypeCodeList: [],
    };
    this.response = {
      load: {},
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalDomesticContactAddressOpen: true,
      isModalAdsFormOpen: false,
    };
    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
      isWarning: false,
      isSearch: false,
    };
    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubID = 1;
    this.currentSubStep = 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
    this.progressPercent = 0;

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initEForm140Page().subscribe((data: any) => {
      if (data) {
        this.master = data;
        console.log("this.master", this.master);
      }
      if (this.editID) {
        this.eformSaveProcessService
          .eFormSave140Load(this.editID, {})
          .subscribe((data: any) => {
            if (data) {
              this.response.load = {
                eform_number: data.eform_number,
                id: data.id,
                wizard: data.wizard,
              };
              this.input.isAllowEditRequestNumber = data.request_number
                ? false
                : true;
              // step 1
              this.input.email = data.email;
              this.input.telephone = data.telephone;
              // step 2
              this.input.request_number = data.request_number;
              this.input.registration_number = data.registration_number;
              this.input.informer_type_code = data.save140_informer_type_code
                ? data.save140_informer_type_code
                : "OWNER";
              // step 3
              this.input.inform_type_code = data.save140_inform_type_code;
              // step 4
              this.input.listOwnerMark = data.people_list;
              this.input.listAgentMark = data.representative_list;
              this.input.save140_representative_condition_type_code = data.save140_representative_condition_type_code
                ? data.save140_representative_condition_type_code
                : "AND_OR";
              this.input.listOtherMark = data.contact_address_list;
              // step 5
              this.input.save140_submit_type_code =
                data.save140_submit_type_code;
              this.input.rule_number = data.rule_number;
              this.input.save140_department_code = data.save140_department_code
                ? data.save140_department_code
                : "REGISTRAR";
              // step 6
              if (data.sue_list.length > 0) {
                this.input.listSue = data.sue_list;
              } else {
                this.onClickAddCourt("listSue");
              }
              if (data.judgment_list.length > 0) {
                this.input.listJudgment = data.judgment_list;
              } else {
                this.onClickAddCourt("listJudgment");
              }
              if (data.final_judgment_list.length > 0) {
                this.input.listFinalJudgment = data.final_judgment_list;
              } else {
                this.onClickAddCourt("listFinalJudgment");
              }
              this.input.remark_6_3 = data.remark_6_3;
              // step 7
              this.input.listDocumentRequestChecked[0].checked = data.is_7_1;
              this.input.listDocumentRequestChecked[1].checked = data.is_7_2;
              this.input.listDocumentRequestChecked[2].checked = data.is_7_3;
              // step 8
              this.input.isCheckAllOwnerSignature =
                data.sign_inform_person_list === "0" ? true : false;
              this.input.isCheckAllAgentSignature =
                data.sign_inform_representative_list === "0" ? true : false;
              this.manageWizard(data.wizard);
              this.setSignInform(
                "listAgentMark",
                data.sign_inform_representative_list
              );
              this.setSignInform("listOtherMark", data.sign_inform_others_list);
              // Close loading
              this.global.setLoading(false);
            } else {
              // Close loading
              this.global.setLoading(false);
            }
          });
      } else {
        this.onClickAddCourt("listSue");
        this.onClickAddCourt("listJudgment");
        this.onClickAddCourt("listFinalJudgment");
        // Close loading
        this.global.setLoading(false);
      }
    });
  }

  //! <<< Call API >>>
  callSendEmail140(params: any): void {
    this.eformSaveProcessService
      .eFormSave140Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchRequestNumber(): void {
    this.eformSaveProcessService
      .eFormSave140Search(this.input.request_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.registration_number = data.registration_load_number;
          this.input.listOwnerMark = clearIdAndSaveId(data.people_load_list);
          this.input.listAgentMark = clearIdAndSaveId(
            data.representative_load_list
          );
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callEFormSave140Save(params: any): void {
    this.eformSaveProcessService
      .eFormSave140Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.isDeactivation = true;
          // Open toast success
          let toast = CONSTANTS.TOAST.SUCCESS;
          toast.message = "บันทึกข้อมูลสำเร็จ";
          this.global.setToast(toast);
          // Navigate
          this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
        } else {
          // Close loading
          this.global.setLoading(false);
        }
      });
  }

  //! <<< Prepare Call API >>>
  sendEmail(): void {
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      wizard: this.getWizard(),
      email: this.input.email,
      telephone: this.input.telephone,
    };
    // Call api
    this.callSendEmail140(params);
  }
  searchRequestNumber(): void {
    let result = validateService(
      "validateEFormSave140ProcessStep2",
      this.input
    );
    this.validate = result.validate;

    if (result.isValid) {
      // Open loading
      this.global.setLoading(true);
      // Call api
      this.callSearchRequestNumber();
    }
  }
  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }
  save(isOverwrite: boolean): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = this.getParamsSave();
    if (!isOverwrite) {
      params = getParamsOverwrite(params);
    }
    // Call api
    this.callEFormSave140Save(params);
  }
  getParamsSave(): any {
    return {
      id: this.response.load.id ? this.response.load.id : null,
      eform_number: this.response.load.eform_number
        ? this.response.load.eform_number
        : null,
      wizard: this.getWizard(),
      // step 1
      email: this.input.email,
      telephone: this.input.telephone,
      // step 2
      request_number: this.input.request_number,
      registration_number: this.input.registration_number,
      save140_informer_type_code: this.input.informer_type_code,
      // step 3
      save140_inform_type_code: this.input.inform_type_code,
      // step 4
      people_list: this.input.listOwnerMark,
      representative_list: this.input.listAgentMark,
      contact_address_list: this.input.listOtherMark,
      save140_representative_condition_type_code: this.input
        .save140_representative_condition_type_code,
      // step 5
      save140_submit_type_code: this.input.save140_submit_type_code,
      rule_number:
        this.input.save140_submit_type_code === "INSTRUCTION"
          ? this.input.rule_number
          : "",
      save140_department_code:
        this.input.save140_submit_type_code !== "INSTRUCTION"
          ? this.input.save140_department_code
          : null,
      // step 6
      sue_list:
        this.input.inform_type_code != "JUDGMENT"
          ? loopDisplayDateServer(this.input.listSue, "sue_date")
          : [],
      judgment_list:
        this.input.inform_type_code == "JUDGMENT"
          ? this.input.listJudgment
          : [],
      final_judgment_list:
        this.input.inform_type_code == "JUDGMENT"
          ? this.input.listFinalJudgment
          : [],
      remark_6_3: this.input.remark_6_3,
      // step 7
      is_7_1: this.input.listDocumentRequestChecked[0].checked,
      is_7_2: this.input.listDocumentRequestChecked[1].checked,
      is_7_3: this.input.listDocumentRequestChecked[2].checked,
      // step 8
      sign_inform_person_list: this.input.isCheckAllOwnerSignature ? "0" : "",
      sign_inform_representative_list:
        this.input.isCheckAllAgentSignature &&
        this.input.save140_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform("listAgentMark"),
      sign_inform_others_list: this.getSignInform("listOtherMark"),
    };
  }
  onClickViewPdfA14(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM14", this.getParamsSave());
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Event >>>
  getWizard(): String {
    if (this.editID) {
      let wizardList = this.response.load.wizard.split("|");
      let sum = 0;
      let sumNow =
        this.currentID +
        this.currentStep +
        this.currentSubID +
        this.currentSubStep;

      wizardList.forEach((item: any) => {
        sum = +sum + +item;
      });

      if (sumNow > sum) {
        return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
      } else {
        return this.response.load.wizard;
      }
    } else {
      return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
    }
  }
  getSignInform(nameList: any): String {
    let result = "";
    this.input[nameList].forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(nameList: any, signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input[nameList].forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save140_representative_condition_type_code != "AND") {
        this.input.listAgentMark.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }

  //! List Court
  onClickAddCourt(name: any): void {
    if (name === "listSue") {
      this.input.listSue.push({
        court_name: "",
        black_number_1: "",
        black_number_2: "",
        sue_date: "",
      });
    } else {
      this.input[name].push({
        court_name: "",
        black_number_1: "",
        black_number_2: "",
        red_number_1: "",
        red_number_2: "",
      });
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Table >>>
  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }
  toggleBooleanInTable(item: any, name: any, condition?: any): void {
    item[name] = !item[name];
  }
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all;
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all;
      });
    } else {
      item.is_check = !item.is_check;
    }
  }
  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    if (this.validateSaveItemModalInTable(nameItem)) {
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        this.toggleModal(nameModal);
        this.updatePaginateForTable(nameList);
      }
    }
  }
  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
    this.updatePaginateForTable(name);
  }
  updatePaginateForTable(name: any): void {}
  validateSaveItemModalInTable(nameItem: any): boolean {
    return true;
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    // if (name === "paginateDocument") {
    //   this.onClickDocumentItemList();
    //   this.input.is_check_all = false;
    // }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      // this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    // this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
    }
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
      this.updatePaginateForTable(nameList);
    }

    if (nameList === "listOwnerMark" || nameList === "listAgentMark") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Wizard >>>
  onClickMenu(menu: any): void {
    if (menu.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentSubStep = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickSubMenu(menu: any, sub: any): void {
    if (sub.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = sub.id;
        this.currentSubStep = sub.number;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickNext(): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      if (this.menuList[this.currentID - 1].hasSub) {
        let sizeSubMenu = this.menuList[this.currentID - 1].sizeSubList;

        if (this.currentSubStep < sizeSubMenu) {
          this.nextSubMenu();
        } else {
          if (this.currentSubID > 0) {
            this.menuList[this.currentID - 1].subList[
              this.currentSubID - 1
            ].canEdit = true;
          }
          this.nextMenu();
        }
      } else {
        this.nextMenu();
      }
    }
  }
  nextMenu(): void {
    let indexCurrentMenu = 0;
    let indexNextMenu = 0;

    // Find index current
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Cal Progress Percent
    if (!this.menuList[indexCurrentMenu].canEdit) {
      this.calcProgressPercent(this.currentStep);
    }
    // Set can edit
    this.menuList[indexCurrentMenu].canEdit = true;
    // Next wizard
    this.currentStep++;

    // Find index next
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexNextMenu = i;
      }
    });

    // Update wizard point
    this.currentID = this.menuList[indexNextMenu].id;
    this.currentSubID = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentSubStep = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  nextSubMenu(): void {
    let indexCurrentMenu = 0;
    let indexCurrentSub = 0;
    let indexNextSub = 0;

    // Find index current menu
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Find index current sub menu
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexCurrentSub = i;
      }
    });

    // Set can edit
    if (this.currentSubID > 0) {
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].canEdit = true;
    } else {
      if (this.menuList[indexCurrentMenu].isUseMenuBeforeSubList) {
        this.menuList[indexCurrentMenu].canEdit = true;
      }
    }
    // Next wizard sub
    this.currentSubStep++;

    // Find index sub next
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexNextSub = i;
      }
    });

    // Update wizard sub point
    this.currentSubID = this.menuList[indexCurrentMenu].subList[
      indexNextSub
    ].id;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  changeMiniStep(action: any): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let indexCurrentMini = 0;
      let indexNextMini = 0;
      let sizeMiniList = 0;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
        }
      });

      // Find index current sub menu
      this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
        if (item.number === this.currentSubStep && item.isShow) {
          indexCurrentSub = i;
        }
      });

      // Find index current mini menu
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList.forEach(
        (item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexCurrentMini = i;
          }
        }
      );

      // Get size mini list
      sizeMiniList = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
        .sizeMiniList;

      // Condition
      if (sizeMiniList === this.currentMiniStep && action === "next") {
        // Set can edit
        this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
          indexCurrentMini
        ].canEdit = true;
        // Next menu
        this.onClickNext();
      } else {
        if (action === "next") {
          // Set can edit
          this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
            indexCurrentMini
          ].canEdit = true;
          // Next wizard mini
          this.currentMiniStep++;
        } else {
          // Back wizard mini
          this.currentMiniStep = this.currentMiniStep - 1;
        }

        // Find index next mini menu
        this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList.forEach((item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexNextMini = i;
          }
        });

        // Update wizard mini point
        this.currentMiniID = this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList[indexNextMini].id;
      }
    }
  }
  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    } else if (this.currentID === 2) {
      let result = validateService(
        "validateEFormSave140ProcessStep2",
        this.input
      );
      this.validate = result.validate;

      if (!this.input.listOwnerMark || this.input.listOwnerMark.length === 0) {
        result.isValid = false;
        console.warn(`request_number is invalid.`);
        this.validate.request_number = "กรุณาคลิก ค้นหา";
      }

      return result.isValid;
    } else if (this.currentID === 3) {
      let result = validateService(
        "validateEFormSave140ProcessStep3",
        this.input
      );
      this.validate = result.validate;
      return result.isValid;
    } else if (this.currentID === 4) {
      if (this.input.informer_type_code === "OWNER") {
        if (this.input.listOwnerMark.length < 1) {
          console.warn(`step4 is invalid.`);
          this.validate.step4 = "กรุณาเพิ่มผู้แจ้ง";
          return false;
        }
      } else if (this.input.informer_type_code === "REPRESENTATIVE") {
        if (this.input.listAgentMark.length < 1) {
          console.warn(`step4 is invalid.`);
          this.validate.step4 = "กรุณาเพิ่มตัวแทนผู้แจ้ง";
          return false;
        }
      } else if (this.input.informer_type_code === "OTHERS") {
        if (this.input.listOtherMark.length < 1) {
          console.warn(`step4 is invalid.`);
          this.validate.step4 = "กรุณาเพิ่มผู้แจ้ง";
          return false;
        }
      }

      return true;
    } else if (this.currentID === 5) {
      let result = validateService(
        "validateEFormSave140ProcessStep5",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        this.input.save140_submit_type_code == "INSTRUCTION"
      ) {
        result = validateService(
          "validateEFormSave140ProcessStep5Registrar",
          this.input
        );
        this.validate = result.validate;
      }

      return result.isValid;
    } else if (this.currentID === 6) {
      let isValid = true;

      if (this.input.inform_type_code !== "JUDGMENT") {
        this.input.listSue.forEach((item: any) => {
          let result = validateService(
            "validateEFormSave140ProcessStep6Sue",
            item
          );
          item.validate = result.validate;
          if (!result.isValid) {
            isValid = false;
          }
        });
      } else {
        this.input.listJudgment.forEach((item: any) => {
          let result = validateService(
            "validateEFormSave140ProcessStep6Judgment",
            item
          );
          item.validate = result.validate;
          if (!result.isValid) {
            isValid = false;
          }
        });

        this.input.listFinalJudgment.forEach((item: any) => {
          let result = validateService(
            "validateEFormSave140ProcessStep6Judgment",
            item
          );
          item.validate = result.validate;
          if (!result.isValid) {
            isValid = false;
          }
        });

        let result = validateService(
          "validateEFormSave140ProcessStep6Remark",
          this.input
        );
        this.validate = result.validate;
        if (!result.isValid) {
          isValid = false;
        }
      }

      return isValid;
    } else if (this.currentID === 8) {
      let isValid = false;

      if (this.input.informer_type_code === "OWNER") {
        if (this.input.isCheckAllOwnerSignature) {
          isValid = true;
        }
      } else if (this.input.informer_type_code === "REPRESENTATIVE") {
        if (this.input.isCheckAllAgentSignature) {
          isValid = true;
        }
        if (this.input.listAgentMark.length > 0) {
          this.input.listAgentMark.forEach((item: any) => {
            if (item.isCheck) {
              isValid = true;
            }
          });
        }
      } else if (this.input.informer_type_code === "OTHERS") {
        if (this.input.listOtherMark.length > 0) {
          this.input.listOtherMark.forEach((item: any) => {
            if (item.isCheck) {
              isValid = true;
            }
          });
        }
      }

      if (!isValid) {
        console.warn(`step8 is invalid.`);
        this.validate.step8 = "กรุณาเลือกลงลายมือชื่อ";
      } else {
        // Set can edit
        this.menuList[7].canEdit = true;
      }

      return isValid;
    } else {
      return true;
    }
  }
  conditionWizard(condition: any): void {}
  manageWizard(wizard: any): void {
    let wizardList = wizard ? wizard.split("|") : [1, 1, 1, 1];
    this.currentID = +wizardList[0];
    this.currentStep = +wizardList[1];
    this.currentSubID = +wizardList[2];
    this.currentSubStep = +wizardList[3];
    this.calcProgressPercent(this.currentStep);

    this.menuList.forEach((item: any) => {
      if (this.currentID > item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            sub.canEdit = true;
          });
        }
      }
      if (this.currentID === item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (this.currentSubID >= sub.id) {
              sub.canEdit = true;
            }
          });
        }
      }
    });
  }
  reRunMenuNumber(): void {
    let number = 1;
    let numberSub = 1;
    let numberMini = 1;
    this.menuList.forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
        numberSub = 1;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (sub.isShow) {
              sub.number = numberSub;
              numberSub++;
              numberMini = 1;
              if (sub.miniList) {
                sub.miniList.forEach((mini: any) => {
                  if (mini.isShow) {
                    mini.number = numberMini;
                    numberMini++;
                  }
                });
              }
            }
          });
        }
      }
    });
  }
  calcProgressPercent(currentStep: number): void {
    let lastItem = this.menuList[this.menuList.length - 1];
    let progressPercent = Math.round(
      ((currentStep + 1) / lastItem.number) * 100
    );
    this.progressPercent = progressPercent > 100 ? 100 : progressPercent;
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }

  //! <<< Other >>>
  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  displayAddress(value: any): any {
    return displayAddress(value);
  }
  onKey(e: any, name: any): void {
    if (e.keyCode === 13) {
      if (name === "request_number") {
        this.searchRequestNumber();
      }
    } else {
      if (name === "request_number") {
        this.input.registration_number = "";
        this.input.listOwnerMark = [];
        this.input.listAgentMark = [];
      }
    }
  }
}
