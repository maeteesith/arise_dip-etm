import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
import { Auth } from "../../auth";
import { ForkJoinService } from '../../services/fork-join.service'
import { AppealCommercialProvinceFilter } from './appeal-role01-kor-other-commercial-province-filter.pipe'
import { RequestProcessService } from '../../services/request-process-buffer.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  clone,
  validateService,
  displayDate,
  displayMoney,
  displayString,

} from '../../helpers'
import { DomSanitizer } from '@angular/platform-browser';
import { AutoComplete } from '../../helpers/autocomplete';
import { RequestAgencyService } from '../../services/request-agency.service';

@Component({
  selector: 'app-appeal-role01-kor-other-commercial-province',
  templateUrl: './appeal-role01-kor-other-commercial-province.component.html',
  styleUrls: ['./appeal-role01-kor-other-commercial-province.component.scss'],
  providers: [ AppealCommercialProvinceFilter ]
})
export class AppealRole01KorOtherCommercialProvinceComponent implements OnInit {
  public input: any
  public modal: any
  public master: any
  public validate: any
  //CommercialProvince
  public paginateCommercialProvince: any
  public commercialAffairsProvinceList: any[]
  public commercialAffairsProvinceItemList: any[]
  public searchCommercialProvince: any
  //RequestType
  public paginateRequest: any
  public requestTypeList: any[]
  public requestTypeItemList: any[]
  public searchRequestType: any
  //Representative
  public searchRepresentative: any

  public perPage: any[]

  public requestType: any[]
  public selectedRow: Number;
  public setClickedRow: Function;

  public paginateList: any[]

  public listRequest01Item: any[]
  //Item table dummy
  public listItem: any[]
  public requestOtherItemSub: any[]

  public paginateItem: any
  public perPageItem: number[]

  public setTableIndex: any

  public TotalAmount: string
    
  public url: string

  public popup: any

  public msgShow: any

  constructor(
    private router: Router,
    private global: GlobalService,
    private auth: Auth,
    private forkJoinService: ForkJoinService,
    private AppealCommercialProvinceFilter: AppealCommercialProvinceFilter,
    private RequestProcessService: RequestProcessService,
    private requestAgencyService: RequestAgencyService,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private SaveProcessService: SaveProcessService,
    private help: Help,
  ) { }



  ngOnInit() {

    this.input = {
      id: null,
      requester_name: '',
      created_by_name: this.auth.getAuth().name,
      telephone: '',
      request_date: getMoment(),
      payment_date: getMoment(),
      is_via_post: false,
      is_wave_fee: false,
      is_commercial_affairs_province: false,
      commercial_affairs_province_code: '',
  
      
    }
    //request01list
    this.listRequest01Item = []
    this.popup = {
      warning_message_show_list: [],
    }

    this.modal = {
      isModalCommercialProvince: false,
      isModalRequest: false,
      isModalRepresentative: false,
      isModalPreviewPdfOpen: false,
    }
    this.searchCommercialProvince = ''
    this.searchRequestType = ''
    //Pagination 分页
    this.paginateCommercialProvince = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateCommercialProvince.id = 'paginateCommercialProvince'
    this.paginateRequest = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateRequest.id = 'paginateRequest'
    this.paginateItem = clone(CONSTANTS.PAGINATION.INIT)
    this.paginateItem.id = 'paginateItem'


    this.validate = {}  
    this.perPage = [10, 50, 100]
    this.insertRow()
    this.sumTotal()
    //Get Master data and commercial list
    this.callInit()

    
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initAppealKORCommercialProvincePage().subscribe((data: any) => {
      if (data) {
        this.commercialAffairsProvinceList = data.commercialAffairsProvinceList
        this.setPagination(this.commercialAffairsProvinceList, this.paginateCommercialProvince.id)

        this.requestTypeList = data.requestTypeList
        this.setPagination(this.requestTypeList, this.paginateRequest.id)
      }
      this.global.setLoading(false)
    })
  }

  //oneWayData
  oneWayDataBinding(name: any, value: any, object: any): void {
    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }


  //

  loadData(data: any) {
    // Manage structure
    Object.keys(data).forEach((item: any) => {
      this.input[item] = data[item]
    })
    this.listItem = data.item_list || []
    data.item_list.map((item: any) => {
      item.code = item.request_type_code
      item.name = item.request_type_name
      item.is_check = false
      item.is_disabled = true

      return item
    })
    this.changePaginateTotal((this.listItem || []).length, 'paginateItem')




    //console.log(this.listItem)
    this.autoComplete.Update(this.listItem)


    // Set value
    // this.requestList = data.item_list
    // this.response.request01Load = data
    // this.isHasRequestList = true

  }


  //insert table row dummy
  insertRow(){
    this.listItem = []
    let i: number = 0
    let j: number = 10
    while(i<j) {
      this.listItem.push({
        code: null,
        name: null,
        //index: this.listItem.length + 1,
        request_type_code: '',
        request_type_description: null,
        item_count: null,
        item_sub_type_1_count: null,
        product_count: null,
        total_price: null,
        fine: null,
        is_evidence_floor7: false,
        product_list_text: null,
        is_sue_text: null,
      })
      i++;
    }
    this.setPagination(this.listItem, this.paginateItem.id)
  }

  //Toggle modal & boolean
  toggleModal(name: string): void {
    if (!this.modal[name]) {
      this.modal[name] = true
    } else {
      this.modal[name] = false
      switch(name){

        case 'isModalCommercialProvince': 
          this.searchCommercialProvince = ''
          this.setPagination(this.commercialAffairsProvinceList, this.paginateCommercialProvince.id)
        break;

        case 'isModalRequest':
          this.searchRequestType = ''
          this.setPagination(this.requestTypeList, this.paginateRequest.id)
        break;

        case 'isModalRepresentative':
          this.searchRepresentative = ''
        break;

      }
    }
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name]
  }

  //select, remove item in table
  setIndex(index): void {
    this.setTableIndex = index
  }
  removeItemIndex(index): void {
    this.listItem[index] = {}
    this.sumTotal()
  }
  removeInputCommercial(): void {
    this.input.commercial_province = null
  }
  selectItem(value,code): void {
    this.input.commercial_province = value
    this.input.commercial_affairs_province_code = code;
    this.setPagination(this.commercialAffairsProvinceList, this.paginateCommercialProvince.id)
  }
  selectItemRequest(value): void {

    const item_list_sub = [{
      request_number: this.listRequest01Item[0].request_number,
      item_sub_type_1_code: this.listRequest01Item[0].item_sub_type_1_code,
      product_count: this.listRequest01Item[0].product_count,
      total_price: this.listRequest01Item[0].total_price
    }]

    console.log('list item 01', item_list_sub)

    let listItemData = {
      code: value.code,
      name: value.name,
      //index: this.listItem.length + 1,
      request_type_code: value.code,
      request_type_description: value.name,
      item_count: null,
      item_sub_type_1_count: null,
      product_count: null,
      total_price: parseInt(value.value_2) || parseInt(value.value_1),
      fine: null,
      is_evidence_floor7: false,
      product_list_text: null,
      is_sue_text: null,
      requestOtherItemSub: item_list_sub
    }
    this.listItem[this.setTableIndex] = listItemData

    //this.listItem.push({
    //  value.
    //})


    console.log('list item value ', listItemData)
    
    this.sumTotal()
    this.setPagination(this.requestTypeItemList, this.paginateRequest.id)
  }

  //sum total amount
  sumTotal(): void{
    let defaultSum: number = 0.00
    for (let i = 0; i < this.listItem.length; i++){
      console.log('total price show ' + this.listItem[i].total_price)
      //if (this.listItem[i].total_price != null && this.listItem[i].total_price != '' && this.listItem[i].total_price != false)
      if (this.listItem[i].total_price != null && this.listItem[i].total_price != '')
      {
        defaultSum += +this.listItem[i].total_price
      } else {
        defaultSum += +0
      }
    }
    this.TotalAmount = defaultSum.toFixed(2)
  }

  //Pagination prop
  filterData(list: any[], value: string, paginate: string): void {
    this.setPagination(this.AppealCommercialProvinceFilter.transform(list, value), paginate)
  }
  setPagination(list: any[], paginate: string): void {
    switch(paginate){
      case this.paginateCommercialProvince.id: this.commercialAffairsProvinceItemList = list; break;
      case this.paginateRequest.id: this.requestTypeItemList = list; break;
      case this.paginateItem.id: this.listItem = list; break;
    }
    this.changePaginateTotal(list.length, paginate)
    this[paginate].currentPage = 1 
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }

  //reset
  reset(): void {
    this.ngOnInit()
  }

  resetTable(): void{
    this.global.setLoading(true)
    this.insertRow()
    this.global.setLoading(false)
  }

  //clear
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }
  //onClickGetRequestNumber
  onClickGetRequestNumber(RequstNumber): void {

    this.global.setLoading(true)
  }

  //save request
  onClickRequestOtherSave(): void {
    //if (this.validateRequestOtherSave()) {
      // Open loading
      this.global.setLoading(true)
      // Set param
    for (let i = 0; i < this.listItem.length; i++) {
        if (this.listItem[i].request_type_code == "") {
          this.listItem.splice(i--)
        }
    }


      const params = {
        id: this.input.id,
        request_date: this.input.request_date,
        requester_name: this.input.requester_name,
        telephone: '',
        is_via_post: this.input.is_via_post,
        is_wave_fee: this.input.is_wave_fee,
        is_commercial_affairs_province: this.input.is_commercial_affairs_province,
        commercial_affairs_province_code: this.input.commercial_affairs_province_code,
        total_price: parseInt(this.TotalAmount),
        item_list: this.listItem || [],
        request_01_item_list: this.listRequest01Item || [],
      }
    // Call api

    console.log('show price '+ parseInt(this.TotalAmount)) 
    console.log('data send to backend ', params) 
    this.callRequestOtherSave(params)
    this.global.setLoading(false);
    //}
  }
  //validate
  validateRequestOtherSave(): boolean {
    let result = validateService('validateRequestOtherSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestOtherSave(params: any): void {
    this.RequestProcessService.RequestOtherSave(params).subscribe((data: any) => {
      if (data) {
        console.log('load data response ', data);
        this.loadData(data)
        if (data.total_price > 0) {
          this.url = '/pdf/ReceiptOtherReference/' + data.id
          console.log(this.url)
          this.toggleModal('isModalPreviewPdfOpen')
        } else {
          this.toggleModal('isPopupSaveOpen')
        }
      }
    })
  }

  //! <<< Call API >>>
  getRequestNumber(object: any, name: any): void
  {
    console.log('show data ' + object[name]);

    var params = this.help.GetFilterParams({ request_number: object[name] })
    this.RequestProcessService.Request01ItemList(params).subscribe((data: any) => {
      if (data && data.list && data.list.length > 0) {
        console.log('data request number : ', data.list)


        this.input = {
          id: null,
          created_by_name: this.auth.getAuth().name,
          telephone: '',
          request_date: getMoment(),
          payment_date: getMoment(),
          is_via_post: false,
          is_wave_fee: false,
          is_commercial_affairs_province: false,
          commercial_affairs_province_code: '',
          request_number: data.list[0].request_number,
          ref_number: data.list[0].reference_number,
          requester_name: data.list[0].requester_name
        }
        this.requestOtherItemSub = []
        this.listRequest01Item = data.list
      } else {
        this.input = {
          id: null,
          created_by_name: this.auth.getAuth().name,
          telephone: '',
          request_date: getMoment(),
          payment_date: getMoment(),
          is_via_post: false,
          is_wave_fee: false,
          is_commercial_affairs_province: false,
          commercial_affairs_province_code: '',
          request_number: '',
          ref_number: '',
          requester_name: ''
        }


        this.listRequest01Item = []
        this.msgShow = "ไม่พบเลขที่คำขอนี้ " + object[name];
        this.toggleModal('isPopupNotFound')
      }
    })
    //this.input.is_autocomplete_ListNotSent_load = false
  }


  showPopup() {
    console.log('show popup event')
    this.toggleModal('isPopupNotFound')
    //this.toggleModal.isPopupNotFound = true
    //this.popup.warning_message_show_list.push("isPopupSendOpen")
  }

}
