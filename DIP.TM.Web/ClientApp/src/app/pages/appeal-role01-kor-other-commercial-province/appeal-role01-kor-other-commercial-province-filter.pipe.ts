import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'appealCommercialProvinceFilter'
})
export class AppealCommercialProvinceFilter implements PipeTransform {
  transform(list: any[], value: string) {

    return value ? list.filter(
        item => item.code && item.code.toLowerCase().indexOf(value.toString().toLowerCase().trim()) != -1 || 
        item.name && item.name.toLowerCase().indexOf(value.toString().toLowerCase().trim()) != -1
    ) : list;
  }

}