import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-madrid-role02-limitations-modal',
  templateUrl: './madrid-role02-limitations-modal.component.html',
  styleUrls: ['./madrid-role02-limitations-modal.component.scss', './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole02LimitationsModalComponent implements OnInit {
//MR 2-1
ItemArray : any = [{ index: "MODEL FORM 11",value:"Declaration That a Change in Ownership Has No Effect (Rule 27(4) of the Common Regulations) "},
                   { index: "MODEL FORM 12",value:"Final Decision That a Change in Ownership Has No Effect (Rule 27(4)(e) of the Common Regulations) "},
                   { index: "MODEL FORM 13",value:"Declaration That a Limitation Has No Effect (Rule 27(5) of the Common Regulations) "},
                   { index: "MODEL FORM 14",value:"Final Decision Stating That a Limitation Has No Effect (Rule 27(5)(e) of the Common Regulations)  "}];

Item: any;
  constructor() { }s

  ngOnInit() {
  }

}
