import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
import { Auth } from "../../auth";
import { ForkJoinService } from '../../services/fork-join.service'
import {
  CONSTANTS,
  getMoment,
  clone,
} from '../../helpers'

@Component({
  selector: 'app-appeal-role01-register-revoke-kor08',
  templateUrl: './appeal-role01-register-revoke-kor08.component.html',
  styleUrls: ['./appeal-role01-register-revoke-kor08.component.scss'],
})
export class AppealRole01RegisterRevokeKor08Component implements OnInit {
  constructor(
    private router: Router,
    private global: GlobalService,
    private auth: Auth,
    private forkJoinService: ForkJoinService,
  ) { }

  public input: any
  public modal: any
  public master: any
  public requestRevokeCodeList: any[]

  ngOnInit() {
    this.input = {
      request_date: getMoment(),
      requestRevokeCode: "TYPE1"
    }
    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initAppealRegisterRevokeKOR08Page().subscribe((data: any) => {
      if (data) {
        this.requestRevokeCodeList = data.requestRevokeCodeList
      }
      this.global.setLoading(false)
    })
  }


}
