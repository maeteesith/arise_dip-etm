import { Component, OnInit } from '@angular/core';
import { Help } from 'src/app/helpers/help';
import { AutomateTest } from 'src/app/test/automate_test';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/global.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material'

@Component({
  selector: 'app-madrid-role05-jop-transfer',
  templateUrl: './madrid-role05-jop-transfer.component.html',
  styleUrls: ['./madrid-role05-jop-transfer.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole05JopTransferComponent implements OnInit {

  /**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<RoleEditDialogComponent>
	 * @param data: any
	 */

  public master: any
  public validate: any
  public input: any
  public listPublicItem: any = [];

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<MadridRole05JopTransferComponent>

    ) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;
    this.dialogRef.backdropClick().subscribe(_ => {
      // let cn = confirm('Do you want to close this modal without save ? ')
      // if (cn) {
      //   this.dialogRef.close();
      // }
    })

  this.validate = {};
  this.listPublicItem = [
    { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", statusprogress: "บันทึก", command: "" },
    { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", doc: "MM2", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAAAAAAAAAAAAAAAAA", statusprogress: "รอตรวจสอบ", command: "" }
]

  this.input = {
    id: null,
    public_type_code: '',
    public_source_code: '',
    public_receiver_by: '',
    public_status_code: '',
    request_number: '',
  };

  }

  closeModal(){
    this.dialogRef.close();
  }


}
