import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { RequestProcessService } from '../../services/request-process-buffer.service'
import { Help } from '../../helpers/help'
import {
    CONSTANTS,
    getMoment,
    displayDateServer,
    clone
} from '../../helpers'

@Component({
    selector: "app-request-01-item-list",
    templateUrl: "./request-01-item-list.component.html",
    styleUrls: ["./request-01-item-list.component.scss"]
})
export class Request01ItemListComponent implements OnInit {

    //TODO >>> Declarations <<<
    // Init
    public editID: any
    public input: any
    public validate: any
    public master: any
    // List (Request01Item)
    public listRequest01Item: any[]
    public paginateRequest01Item: any
    public perPageRequest01Item: number[]
    // List (RequestProcess)
    public listRequestProcess: any[]
    public paginateRequestProcess: any
    public perPageRequestProcess: number[]

    constructor(
        private help: Help,
        private route: ActivatedRoute,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private RequestProcessService: RequestProcessService
    ) { }

    ngOnInit() {
        //TODO >>> Init value <<<
        // Init
        this.editID = this.route.snapshot.paramMap.get('id')
        this.input = {
            id: null,
            created_by_name: '',
            request_date: getMoment(),
            reference_number: '',
            request_number: '',
            receipt_status_code: '',
            is_check_all: false,
        }
        this.validate = {}
        this.master = {
            receiptStatusCodeList: [],
        }
        // List (Request01Item)
        this.listRequest01Item = []
        this.paginateRequest01Item = clone(CONSTANTS.PAGINATION.INIT)
        this.paginateRequest01Item.id = 'paginateRequest01Item'
        this.perPageRequest01Item = clone(CONSTANTS.PAGINATION.PER_PAGE)
        // List (RequestProcess)
        this.listRequestProcess = []
        this.paginateRequestProcess = clone(CONSTANTS.PAGINATION.INIT)
        this.paginateRequestProcess.id = 'paginateRequestProcess'
        this.perPageRequestProcess = clone(CONSTANTS.PAGINATION.PER_PAGE)

        //TODO >>> Call service <<<
        this.callInit()
    }

    //! <<< Call Init API >>>
    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initRequest01ItemList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.receiptStatusCodeList.unshift({ code: '', name: 'ทั้งหมด' })
            }
            if (this.editID) {
                this.RequestProcessService.Request01ItemLoad(this.editID, {}).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        Object.keys(data).forEach((item: any) => {
                            this.input[item] = data[item]
                        })

                        this.listRequest01Item = data.request01item_list || []
                        data.request01item_list.map((item: any) => { item.is_check = false; return item })
                        this.changePaginateTotal((this.listRequest01Item || []).length, 'paginateRequest01Item')


                        // Set value
                        // this.requestList = data.item_list
                        // this.response.request01Load = data
                        // this.isHasRequestList = true
                    }
                    // Close loading
                    this.global.setLoading(false)
                })
            }

            this.global.setLoading(false)
        })
    }

    //! <<< Call API >>>
    callRequest01ItemList(params: any): void {
        this.RequestProcessService.Request01ItemList(this.help.GetFilterParams(params, this.paginateRequest01Item)).subscribe((data: any) => {
            if (data) {
                // Set value
                this.listRequest01Item = data.list || []
                this.help.PageSet(data, this.paginateRequest01Item)
            }
            // Close loading
            this.global.setLoading(false)
        })
    }
    callRequest01ItemDelete(params: any, ids: any[]): void {
        this.RequestProcessService.Request01ItemDelete(params).subscribe((data: any) => {
            ids.forEach((id: any) => {
                for (let i = 0; i < this.listRequest01Item.length; i++) {
                    if (this.listRequest01Item[i].id == id) {
                        this.listRequest01Item.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listRequest01Item.length; i++) {
                this.listRequest01Item[i] = i + 1
            }

            this.onClickRequest01ItemList()
            // Close loading
            this.global.setLoading(false)
        })
    }
    onClickRequest01ItemDelete(item: any): void {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listRequest01Item.filter(r => r.is_check).length > 0 || item) {
            var rs = prompt("Do you want to delete", "Yes / No")
            if (rs && rs != "") {
                if (item) {
                    this.listRequest01Item.filter(r => r.is_check).forEach((item: any) => {
                        item.is_check = false
                    });
                    item.is_check = true
                }

                let ids = []

                for (let i = 0; i < this.listRequest01Item.length; i++) {
                    if (this.listRequest01Item[i].is_check) {
                        this.listRequest01Item[i].cancel_reason = rs
                        this.listRequest01Item[i].status_code = "DELETE"

                        if (true && this.listRequest01Item[i].id) ids.push(this.listRequest01Item[i].id)
                        else this.listRequest01Item.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callRequest01ItemDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listRequest01Item.length; i++) {
                    this.listRequest01Item[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }

    //! <<< Prepare Call API >>>
    onClickRequest01ItemList(): void {
        // Open loading
        this.global.setLoading(true)
        // Set param
        //const params = {
        //    page_index: +this.paginateRequest01Item.currentPage,
        //    item_per_page: +this.paginateRequest01Item.itemsPerPage,
        //    order_by: 'created_date',
        //    is_order_reverse: false,
        //    search_by: [{
        //        key: 'created_by_name',
        //        value: this.input.created_by_name,
        //        operation: 5
        //    }, {
        //        key: 'request_date',
        //        value: displayDateServer(this.input.request_date),
        //        operation: 0
        //    }, {
        //        key: 'reference_number',
        //        value: this.input.reference_number,
        //        operation: 5
        //    }, {
        //        key: 'request_number',
        //        values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
        //        operation: 5
        //    }, {
        //        key: 'receipt_status_code',
        //        value: this.input.receipt_status_code,
        //        operation: 0
        //    }]
        //}
        // Call api
        const params = {
            created_by_name: this.input.created_by_name,
            request_date: this.input.request_date,
            reference_number: this.input.reference_number,
            request_number: this.input.request_number,
            receipt_status_code: this.input.receipt_status_code,
        }
        this.callRequest01ItemList(params)
    }

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    //! <<< Event >>>
    onClickRequest01ItemAdd(): void {
        this.listRequest01Item.push({
            request_date_text: null,
            request_number: null,
            requester_name: null,
            income_type: null,
            total_price: null,
            item_sub_type_1_count: null,
            product_count: null,
            size_over_cm: null,
            created_by_name: null,
            created_date_text: null,
            receipt_number: null,
            cancel_reason: null,

        })
        this.changePaginateTotal(this.listRequest01Item.length, 'paginateRequest01Item')
    }
    onClickRequest01ItemEdit(item: any): void {
        if (item.eform_id) {
            var win = window.open("request-eform/edit/" + item.request01_id)
        } else {
            var win = window.open("registration-request/edit/" + item.request01_id)
        }
    }
    onClickRequest01ItemRequest01ItemAdd(): void {
        var win = window.open("registration-request")
    }
    onClickCheckBoxTable2(name: string, index: number, isAll: boolean = false): void {
        if (isAll) {
            this.input.is_table_2_check_all = !this.input.is_table_2_check_all
            this[name].forEach((item: any) => { item.is_check = this.input.is_table_2_check_all })
        } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
    }
    onChangeCheckboxRequest01Item(object: any, object_name: string, value: string): void {
        object[object_name] = !object[object_name]
    }
    onChangeSelectRequest01Item(object: any, object_name: string, value: string): void {
        object[object_name] = value
    }



    //! <<< ----------------- >>>
    //! <<< --- Copy Zone --- >>>
    //! <<< ----------------- >>>

    //! <<< Binding (one way) >>>
    binding(obj: any, name: any, value: any): void {
        this[obj][name] = value
    }

    //! <<< Table >>>
    onSelectColumnInTable(name: string, item: any): void {
        if (!item) {
            this.input.is_check_all = !this.input.is_check_all
            this[name].forEach((item: any) => {
                item.is_check = this.input.is_check_all
            })
        } else {
            item.is_check = !item.is_check
        }
    }
    onChangeInputInTable(item: any, name: any, value: any): void {
        item[name] = value
    }
    toggleInputInTable(item: any, name: any): void {
        item[name] = !item[name]
    }

    //! <<<< Pagination >>>
    managePaginateCallApi(name: string): void {
        if (name === 'paginateRequest01Item') {
            this.onClickRequest01ItemList()
            this.input.is_check_all = false
        }
    }
    onChangePage(page: any, name: string): void {
        this[name].currentPage = page === '' ? 1 : page
        if ((+page || page === '') && page <= this.getMaxPage(name)) {
            this.managePaginateCallApi(name)
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
        this.managePaginateCallApi(name)
    }
    changePaginateTotal(total: any, name: string): void {
        this[name].totalItems = total
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }
}
