import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Component, Input, Output, OnInit, EventEmitter, Inject } from "@angular/core";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { CONSTANTS, validateService } from "../../helpers";


@Component({
  selector: "app-modal-madrid-role01-consider",
  templateUrl: "./modal-madrid-role01-consider.component.html",
  styleUrls: ["./modal-madrid-role01-consider.component.scss",
  './../../../assets/theme/styles/madrid/madrid.scss'],
})
export class ModalMadridRole01ConsiderComponent implements OnInit {

  
 



  constructor(
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalMadridRole01ConsiderComponent>,
    
    @Inject(MAT_DIALOG_DATA) public data: any) {}

  ngOnInit() {
    
    
  }

  closeModal() {
    
    this.dialogRef.close();
  }

 
 
  }

