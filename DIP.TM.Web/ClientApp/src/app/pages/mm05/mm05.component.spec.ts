/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Mm05Component } from './mm05.component';

describe('Mm05Component', () => {
  let component: Mm05Component;
  let fixture: ComponentFixture<Mm05Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mm05Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mm05Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
