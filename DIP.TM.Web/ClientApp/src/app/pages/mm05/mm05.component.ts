import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';

import { Help } from './../../helpers/help';
import { AutomateTest } from './../../test/automate_test';
import { GlobalService } from './../../global.service';
import {
  CONSTANTS,
  getMoment,
  clone,
  displayDateServer
} from '../../helpers'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-mm05',
  templateUrl: './mm05.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./mm05.component.scss',
    "./../../../assets/theme/styles/madrid/madrid.scss"]
})
export class Mm05Component implements OnInit {

  constructor(private dialog: MatDialog,
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,) { }

  ngOnInit() {
  }

}
