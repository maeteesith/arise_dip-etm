import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealRole04DecisionNumberReleaseListComponent } from './appeal-role04-decision-number-release-list.component';

describe('AppealRole04DecisionNumberReleaseListComponent', () => {
  let component: AppealRole04DecisionNumberReleaseListComponent;
  let fixture: ComponentFixture<AppealRole04DecisionNumberReleaseListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppealRole04DecisionNumberReleaseListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealRole04DecisionNumberReleaseListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
