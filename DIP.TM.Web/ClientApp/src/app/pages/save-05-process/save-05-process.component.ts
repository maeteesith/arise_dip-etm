import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-save-05-process",
  templateUrl: "./save-05-process.component.html",
  styleUrls: ["./save-05-process.component.scss"]
})
export class Save05ProcessComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public modal: any


  // List People01
  public listPeople01: any[]
  public paginatePeople01: any
  public perPagePeople01: number[]
  // Response People01
  public responsePeople01: any
  public listPeople01Edit: any

  // List Product01
  public listProduct01: any[]
  public paginateProduct01: any
  public perPageProduct01: number[]
  // Response Product01
  public responseProduct01: any
  public listProduct01Edit: any

  // List People
  public listPeople: any[]
  public paginatePeople: any
  public perPagePeople: number[]
  // Response People
  public responsePeople: any
  public listPeopleEdit: any

  // List Representative
  public listRepresentative: any[]
  public paginateRepresentative: any
  public perPageRepresentative: number[]
  // Response Representative
  public responseRepresentative: any
  public listRepresentativeEdit: any

  // List ContactAddress
  public listContactAddress: any[]
  public paginateContactAddress: any
  public perPageContactAddress: number[]
  // Response ContactAddress
  public responseContactAddress: any
  public listContactAddressEdit: any

  // List Product
  public listProduct: any[]
  public paginateProduct: any
  public perPageProduct: number[]
  // Response Product
  public responseProduct: any
  public listProductEdit: any


  //label_save_combobox || label_save_radio
  public requestItemTypeCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  //Modal Initial
  //Modal Initial

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  public product_select: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      register_number: '',
      make_date: getMoment(),
      irn_number: '',
      request_date_tex: '',
      total_price: '',
      request_index: '',
      contract_number: '',
      is_allow_contract: false,
      is_extend_contract: false,
      contract_date: getMoment(),
      request_item_type_code: '',
    }
    this.listPeople01 = []
    this.paginatePeople01 = CONSTANTS.PAGINATION.INIT
    this.perPagePeople01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct01 = []
    this.paginateProduct01 = CONSTANTS.PAGINATION.INIT
    this.perPageProduct01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listPeople = []
    this.paginatePeople = CONSTANTS.PAGINATION.INIT
    this.perPagePeople = CONSTANTS.PAGINATION.PER_PAGE

    this.listRepresentative = []
    this.paginateRepresentative = CONSTANTS.PAGINATION.INIT
    this.perPageRepresentative = CONSTANTS.PAGINATION.PER_PAGE

    this.listContactAddress = []
    this.paginateContactAddress = CONSTANTS.PAGINATION.INIT
    this.perPageContactAddress = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct = []
    this.paginateProduct = CONSTANTS.PAGINATION.INIT
    this.perPageProduct = CONSTANTS.PAGINATION.PER_PAGE

    //Master List
    this.master = {
      requestItemTypeCodeList: [],
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false

    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave05Process().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save05Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.autocompleteChooseListNotSent(data)
          }
          // Close loading
          pThis.global.setLoading(false)
        })
      }

      this.global.setLoading(false)
    })
  }

  constructor(
    private help: Help,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService
  ) { }

  autocompleteKeyDown(event: any) {
    if (event.key == "Enter") {
      this.autocompleteChangeListNotSent(this.input, 'request_number', 5, 1)
    }
  }


  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: { request_number: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    let pThis = this
    this.SaveProcessService.Save05ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListNotSent = data
        }
      }

      this.input.is_autocomplete_ListNotSent_load = false
    })
  }
  autocompleteChooseListNotSent(data: any): void {
    this.input = data

    data.product_list = data.product_list || []
    this.listProduct = []
    data.product_list.filter(r => !r.is_deleted).forEach((item: any) => {
      var product_list = this.listProduct.filter(r => r.request_item_sub_type_1_code == item.request_item_sub_type_1_code)
      if (product_list.length > 0) product_list[0].description += "  " + item.description
      else this.listProduct.push({
        request_item_sub_type_1_code: item.request_item_sub_type_1_code,
        description: item.description,
      })
    })

    this.is_autocomplete_ListNotSent_show = false
  }


  onClickSave05Madrid(): void {
    //if(this.validateSave05Madrid()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave05Madrid(this.saveData())
    //}
  }
  validateSave05Madrid(): boolean {
    let result = validateService('validateSave05Madrid', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave05Madrid(params: any): void {
    let pThis = this
    this.SaveProcessService.Save05Madrid(params).subscribe((data: any) => {
      // if(isValidSave05MadridResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave05Save(): void {
    //if(this.validateSave05Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave05Save(this.saveData())
    //}
  }
  validateSave05Save(): boolean {
    let result = validateService('validateSave05Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave05Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save05Save(params).subscribe((data: any) => {
      // if(isValidSave05SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickSave05Send(): void {
    //if(this.validateSave05Send()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave05Send(this.saveData())
    //}
  }
  validateSave05Send(): boolean {
    let result = validateService('validateSave05Send', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave05Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save05Send(params).subscribe((data: any) => {
      // if(isValidSave05SendResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave050RequestBack(): void {
    //if(this.validateSave050Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave050RequestBack(this.saveData())
    //}
  }
  callSave050RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save050RequestBack(params).subscribe((data: any) => {
      // if(isValidSave050SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  onClickSave05Product01Add(): void {
    this.listProduct01.push({
      //index: this.listProduct01.length + 1,
      request_item_sub_type_1_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
  }


  onClickProductClone(list, paginate): void {
    this.input.product_01_list.forEach((item: any) => {
      list.push({
        request_item_sub_type_1_code: item.request_item_sub_type_1_code,
        description: item.description,
      })
    })
    this.changePaginateTotal(list.length, paginate)
  }

  onClickSave05ProductAdd(): void {
    this.listProduct.push({
      //index: this.listProduct.length + 1,
      is_check: true,
      request_item_sub_type_1_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
  }

  onClickSave05ProductEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickSave05ProductDelete(item: any): void {
    // if(this.validateSave05ProductDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listProduct.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listProduct.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listProduct.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < this.listProduct.length; i++) {
          if (this.listProduct[i].is_check) {
            this.listProduct[i].cancel_reason = rs
            this.listProduct[i].status_code = "DELETE"

            if (true && this.listProduct[i].id) this.listProduct[i].is_deleted = true
            else this.listProduct.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave05ProductDelete(params, ids)
            return;
          }
        }

        //for (let i = 0; i < this.listProduct.length; i++) {
        //    if (!this.listProduct[i].is_hide) {
        //        //this.listProduct[i].index = i + 1
        //    }
        //}
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callSave05ProductDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save05ProductDelete(params).subscribe((data: any) => {
      // if(isValidSave05ProductDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listProduct.length; i++) {
          if (this.listProduct[i].id == id) {
            this.listProduct.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listProduct.length; i++) {
        this.listProduct[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }


  onClickSave05DocumentView(): void {
    window.open('/File/RequestDocumentCollect/50/' + this.input.request_number + "_" + this.input.request_id)
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPeople = data.people_list || []
    this.listRepresentative = data.representative_list || []
    this.help.Clone(data.contact_address || {}, this.contactAddress)
    this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"

    data.product_list = data.product_list || []
    this.listProduct = []
    data.product_list.filter(r => !r.is_deleted).forEach((item: any) => {
      var product_list = this.listProduct.filter(r => r.request_item_sub_type_1_code == item.request_item_sub_type_1_code)
      if (product_list.length > 0) product_list[0].description += "  " + item.description
      else this.listProduct.push({
        request_item_sub_type_1_code: item.request_item_sub_type_1_code,
        description: item.description,
      })
    })

    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []

    this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
    this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
    this.changePaginateTotal((this.listProduct || []).length, 'paginatePeople01')

    this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')

    this.is_autocomplete_ListNotSent_show = false
  }

  saveData(): any {
    const params = this.input
    params.people_list = this.listPeople
    params.representative_list = this.listRepresentative
    params.contact_address = this.contactAddress

    params.product_list = []
    this.listProduct.forEach((item: any) => {
      item.description.split("  ").forEach((product: any) => {
        params.product_list.push({
          request_item_sub_type_1_code: item.request_item_sub_type_1_code,
          description: product,
        })
      })
    })

    return params
  }

  onClickSave050PeopleAdd(): void {
    this.listPeople.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listPeople[this.listPeople.length - 1])
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
  }
  onClickSave050RepresentativeAdd(): void {
    this.listRepresentative.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listRepresentative[this.listRepresentative.length - 1], 'ตัวแทน')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
  }

  onClickAddressCopy(item_list: any[], copy_item: any): void {
    var index = 1
    item_list.forEach((item: any) => {
      item.index = index++
      if (item === copy_item) {
        var item_new = { ...item }
        item_new.id = null
        item_new.index = index++
        item_list.splice(index + 1, 0, item_new)
      }
    })
  }
  onClickAddressEdit(item: any, name: string = "เจ้าของ"): void {
    this.modalPeopleText = name
    this.modalAddressEdit = item
    this.help.Clone(item, this.modalAddress)

    this.modalAddress.is_representative = name.indexOf("ตัวแทน") >= 0

    this.modal["isModalPeopleEditOpen"] = true

  }
  onClickAddressSave(): void {
    this.help.Clone(this.modalAddress, this.modalAddressEdit)
    this.help.GetAddressInformation(this.modalAddressEdit)

    this.modal["isModalPeopleEditOpen"] = false
  }
  onClickAddressDelete(item_list: any[], item: any): void {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (item_list.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        item_list.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = item_list.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < item_list.length; i++) {
          if (item_list[i].is_check) {
            //item_list[i].cancel_reason = rs
            item_list[i].is_deleted = true
            //console.log(item_list[i])
            //item_list[i].status_code = "DELETE"

            if (true && item_list[i].id) ids.push(item_list[i].id)
            else item_list.splice(i--, 1);
          }
        }
      }
    }
    //console.log(item_list)
    this.global.setLoading(false)
  }



  onClickProductSelect(row_item: any, is_selected = true) {
    if (is_selected) {
      var pThis = this
      setTimeout(function () {
        row_item.description = row_item.description.replace(/  /g, '\n').trim()
        pThis.product_select = row_item
      }, 100)
    } else {
      if (this.product_select == row_item) {
        row_item.description = row_item.description.replace(/\n/g, '  ').trim()
        this.product_select = null
      }
    }
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxBinding(name: any, value: any, object: any): void {
    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
