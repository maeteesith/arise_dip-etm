import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-save-140-process",
  templateUrl: "./save-140-process.component.html",
  styleUrls: ["./save-140-process.component.scss"]
})
export class Save140ProcessComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List People01
  public listPeople01: any[]
  public paginatePeople01: any
  public perPagePeople01: number[]
  // Response People01
  public responsePeople01: any
  public listPeople01Edit: any

  // List Product01
  public listProduct01: any[]
  public paginateProduct01: any
  public perPageProduct01: number[]
  // Response Product01
  public responseProduct01: any
  public listProduct01Edit: any


  //label_save_combobox || label_save_radio
  public save140SueTypeCodeList: any[]
  public save140SueReportTypeCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      registration_number: '',
      make_date: getMoment(),
      save140_sue_type_code: '',
      black_index_1: '',
      black_index_2: '',
      red_index_1: '',
      red_index_2: '',
      sue_date: getMoment(),
      created_date_text: '',
      description: '',
      save140_sue_report_type_code: '',
    }
    this.listPeople01 = []
    this.paginatePeople01 = CONSTANTS.PAGINATION.INIT
    this.perPagePeople01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct01 = []
    this.paginateProduct01 = CONSTANTS.PAGINATION.INIT
    this.perPageProduct01 = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
      save140SueTypeCodeList: [],
      save140SueReportTypeCodeList: [],
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave140Process().subscribe((data: any) => {
      if (data) {
        this.master = data

      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save140Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          pThis.global.setLoading(false)
        })
      }

      this.global.setLoading(false)
    })
  }

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService
  ) { }

  autocompleteKeyDown(event: any) {
    if (event.key == "Enter") {
      this.autocompleteChangeListNotSent(this.input, 'request_number', 5, 1)
    }
  }


  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: {
          request_number: object[name],
        },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    this.global.setLoading(true)
    let pThis = this
    this.SaveProcessService.Save140ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else if (data.length == 0) {
          this.onClickReset()
        } else {
          pThis.autocompleteListListNotSent = data
          this.input.is_autocomplete_ListNotSent_load = false
          this.global.setLoading(false)
        }
      }
    })
  }
  autocompleteChooseListNotSent(data: any): void {
    this.input = data

    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')


    this.is_autocomplete_ListNotSent_show = false
    this.global.setLoading(false)
  }


  onClickSave140Delete(): void {
    //if(this.validateSave140Delete()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave140Delete(this.saveData())
    //}
  }
  validateSave140Delete(): boolean {
    let result = validateService('validateSave140Delete', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave140Delete(params: any): void {
    let pThis = this
    this.SaveProcessService.Save140Delete(params).subscribe((data: any) => {
      // if(isValidSave140DeleteResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave140Save(): void {
    //if(this.validateSave140Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave140Save(this.saveData())
    //}
  }
  validateSave140Save(): boolean {
    let result = validateService('validateSave140Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave140Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save140Save(params).subscribe((data: any) => {
      // if(isValidSave140SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickSave140Send(): void {
    //if(this.validateSave140Send()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave140Send(this.saveData())
    //}
  }
  validateSave140Send(): boolean {
    let result = validateService('validateSave140Send', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave140Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save140Send(params).subscribe((data: any) => {
      // if(isValidSave140SendResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave140RequestBack(): void {
    //if(this.validateSave140Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave140RequestBack(this.saveData())
    //}
  }
  callSave140RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save140RequestBack(params).subscribe((data: any) => {
      // if(isValidSave140SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  onClickSave140People01Add(): void {
    this.listPeople01.push({
      card_type_name: null,
      card_number: null,
      name: null,
      house_number: null,
      telephone: null,

    })
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
  }

  onClickSave140Product01Add(): void {
    this.listProduct01.push({
      index: this.listProduct01.length + 1,
      request_item_sub_type_1_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    this.SaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListModalLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListModalLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location



  onClickSave140DocumentView(): void {
    window.open('/File/RequestDocumentCollect/140/' + this.input.request_number + "_" + this.input.request_id)
  }


  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []
    let index = 1
    index = 1
    this.listPeople01.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listProduct01.map((item: any) => { item.is_check = false; item.index = index++; return item })
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')

  }

  listData(data: any): void {
    this.listPeople01 = data || []
    this.listProduct01 = data || []
    let index = 1
    index = 1
    this.listPeople01.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listProduct01.map((item: any) => { item.is_check = false; item.index = index++; return item })
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')

  }

  saveData(): any {
    let params = this.input

    params.people_01_list = this.listPeople01 || []
    params.product_01_list = this.listProduct01 || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
