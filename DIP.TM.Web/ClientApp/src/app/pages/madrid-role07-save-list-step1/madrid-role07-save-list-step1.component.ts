import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-madrid-role07-save-list-step1',
  templateUrl: './madrid-role07-save-list-step1.component.html',
  styleUrls: ['./madrid-role07-save-list-step1.component.scss']
})
export class MadridRole07SaveListStep1Component implements OnInit {

  ItemArray : any = ["AAA", "VVV", "OOO"];

  constructor() { }

  currentStep = 1;
  approval = 1;
  menuList = [
    {
      number: 1,
      isShow: true,
      title: "REGISTER"
    },
    {
      number: 2,
      isShow: true,
      title: "REGISTER PAGE 2"
    },
    {
      number: 3,
      isShow: true,
      title: "REGISTER PAGE 3"
    },
    {
      number: 4,
      isShow: true,
      title: "CALCULATION"
    }
  ]
  approvalList = [{ id: 1, name: "อนุมัติ" }, { id: 2, name: "ไม่อนุมัติ" }, { id: 3, name: "อนุญาต" }]

  ngOnInit() {
  }

}
