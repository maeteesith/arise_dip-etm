/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { MadridRole07SaveListStep1Component } from './madrid-role07-save-list-step1.component';

describe('MadridRole07SaveListStep1Component', () => {
  let component: MadridRole07SaveListStep1Component;
  let fixture: ComponentFixture<MadridRole07SaveListStep1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MadridRole07SaveListStep1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MadridRole07SaveListStep1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
