import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-considering-similar-kor10",
  templateUrl: "./considering-similar-kor10.component.html",
  styleUrls: ["./considering-similar-kor10.component.scss"]
})
export class ConsideringSimilarKor10Component implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public case28_all_list: any[]
  public case28_list: any[]
  public case28_2_list: any[]

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  // List ProductKor10
  public listProductKor10: any[]
  public paginateProductKor10: any
  public perPageProductKor10: number[]
  // Response ProductKor10
  public responseProductKor10: any
  public listProductKor10Edit: any

  // List Product
  public listProduct: any[]
  public paginateProduct: any
  public perPageProduct: number[]
  // Response Product
  public responseProduct: any
  public listProductEdit: any


  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
    }
    this.listProductKor10 = []
    this.paginateProductKor10 = CONSTANTS.PAGINATION.INIT
    this.perPageProductKor10 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct = []
    this.paginateProduct = CONSTANTS.PAGINATION.INIT
    this.perPageProduct = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List

    this.case28_all_list = []
    this.case28_list = []
    this.case28_2_list = []

    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
    })

    this.forkJoinService.initConsideringSimilarKor10().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)

      }
      if (this.editID) {
        this.CheckingProcessService.ConsideringSimilarKor10Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }


  onClickConsideringSimilarKor10ProductKor10Add(): void {
    this.listProductKor10.push({
      index: this.listProductKor10.length + 1,
      name: null,
      created_date_text: null,
      remark: null,
      is_allow_role_28: false,

    })
    this.changePaginateTotal(this.listProductKor10.length, 'paginateProductKor10')
  }

  onClickConsideringSimilarKor10ProductAdd(): void {
    this.listProduct.push({
      index: this.listProduct.length + 1,
      name: null,
      created_date_text: null,
      remark: null,
      is_allow_role_28: false,
      allow_role_28_description: null,

    })
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
  }

  onClickConsideringSimilarKor10Save(): void {
    this.callConsideringSimilarKor10Save(this.input)
  }
  callConsideringSimilarKor10Save(params): void {
    this.global.setLoading(true)
    this.CheckingProcessService.ConsideringSimilarKor10Save(params).subscribe((data: any) => {
      console.log(params)
      this.loadData(params)
      //this.togglePopup('isPopupSaveOpen')

      //var win = window.open("./checking-similar/list", "_self")
      this.global.setLoading(false)
      this.automateTest.test(this, { OwnerSame: 1 })
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  //onClickSave04ModalAddressSave(): void {
  //  Object.keys(this.inputModalAddress).forEach((item: any) => {
  //    this.inputModalAddressEdit[item] = this.inputModalAddress[item]
  //  })
  //  this.toggleModal("isModalPeopleEditOpen")
  //}


  onClickConsideringSimilarBack() {
    window.close()
  }




  // Modal Location



  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    //console.log("loadData")

    this.listProductKor10 = data.productkor10_list || []
    this.listProduct = data.product_list || []
    this.help.PageSet(data, this.paginateProductKor10)
    this.help.PageSet(data, this.paginateProduct)

    this.case28_all_list.length = 0
    this.input.case28_list.forEach((item: any) => {
      var code_list = this.master.addressCountryCodeList.filter(r => r.code == item.address_country_code)
      item.address_country_name = code_list.length > 0 ? code_list[0].name : ""
      this.case28_all_list.push(item)
    })

    console.log(this.case28_all_list)
  }

  listData(data: any): void {
    this.listProductKor10 = data.list || []
    this.listProduct = data.list || []
    this.help.PageSet(data, this.paginateProductKor10)
    this.help.PageSet(data, this.paginateProduct)

  }

  saveData(): any {
    let params = this.input

    params.productkor10_list = this.listProductKor10 || []
    params.product_list = this.listProduct || []

    return params
  }


  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

}
