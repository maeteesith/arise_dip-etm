import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role01-check-do",
  templateUrl: "./public-role01-check-do.component.html",
  styleUrls: ["./public-role01-check-do.component.scss"]
})
export class PublicRole01CheckDoComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List DocumentScanList
  public listDocumentScanList: any[]
  public paginateDocumentScanList: any
  public perPageDocumentScanList: number[]
  // Response DocumentScanList
  public responseDocumentScanList: any
  public listDocumentScanListEdit: any

  // List InstructionList
  public listInstructionList: any[]
  public paginateInstructionList: any
  public perPageInstructionList: number[]
  // Response InstructionList
  public responseInstructionList: any
  public listInstructionListEdit: any

  // List ReceiptList
  public listReceiptList: any[]
  public paginateReceiptList: any
  public perPageReceiptList: number[]
  // Response ReceiptList
  public responseReceiptList: any
  public listReceiptListEdit: any

  public tab_menu_show_index: any


  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public tablePeople: any
  public tableRepresentative: any
  public contactAddress: any
  public tableProduct: any
  public tableJoiner: any
  public tableCertificationFile: any
  public request_mark_feature_code_list: any
  public tableRequestGroup: any
  public tableHistory: any

  public tableDocumentScan: any
  public tableInstructionRule: any
  public tableReceipt: any

  public popup: any

  public send_back: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }


    this.input = {
      id: null,
      request_number: '',
      request_date_text: '',
      expired_date_text: '',
      irr_madrid_number: '',
      reference_number: '',
      rule_28_date_text: '',
      request_source_name: '',
      checking_receive_by_name: '',
      considering_receive_by_name: '',
      people_name: '',
      representative_name: '',
      request_item_sub_type_1_code_text: '',
      address_text: '',
      request_item_type_name: '',
      request_item_type_code: '',
      sound_mark_list: '',
      full_view: {},
    }
    this.listDocumentScanList = []
    this.paginateDocumentScanList = CONSTANTS.PAGINATION.INIT
    this.perPageDocumentScanList = CONSTANTS.PAGINATION.PER_PAGE

    this.listInstructionList = []
    this.paginateInstructionList = CONSTANTS.PAGINATION.INIT
    this.perPageInstructionList = CONSTANTS.PAGINATION.PER_PAGE

    this.listReceiptList = []
    this.paginateReceiptList = CONSTANTS.PAGINATION.INIT
    this.perPageReceiptList = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.popup = {}
    this.send_back = {}

    this.tab_menu_show_index = 1

    this.tablePeople = {
      column_list: {
        index: "#",
        name: "ชื่อเจ้าของ",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      is_address_edit: true,
    }

    this.tableRepresentative = {
      column_list: {
        index: "#",
        name: "ชื่อตัวแทน",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      object_name: "ตัวแทน",
      is_address_edit: true,
    }

    this.contactAddress = {
      address_type_code: "OWNER"
    }

    this.tableProduct = {
      column_list: {
        index: "#",
        request_item_sub_type_1_code: "จำพวกสินค้า",
        description: "รายการสินค้า/บริการ",
        count: "จำนวน",
      },
      column_edit_list: {
        request_item_sub_type_1_code: {
          type: "textbox",
          group_by: "description",
        },
        description: {
          type: "textbox",
          type_click: "textarea",
          width: 800,
        },
      },
      can_added: true,
    }


    this.tableJoiner = {
      column_list: {
        index: "#",
        name: "ชื่อผู้ใช้ร่วม",
        address_information: "ที่อยู่",
        telephone: "โทรศัพท์",
        fax: "โทรสาร",
      },
      object_name: "ผู้ใช้ร่วม",
      is_address_edit: true,
    }

    this.request_mark_feature_code_list = {}

    this.tableCertificationFile = {
      column_list: {
        index: "#",
        created_date: "วันที่อัพโหลด",
        file_name: "ชื่อไฟล์",
        file_size: "ขนาดไฟล์",
        remark: "หมายเหตุ",
      },
      column_edit_list: { remark: { type: "textbox", }, },
      can_deleted: true,
      button_list: [{
        name: "file_upload",
        title: "แนบไฟล์",
        icon: "upload-circle",
        file: "application/pdf", // image/*
        is_uploaded: true,
      }],
    }

    this.tableRequestGroup = {
      column_list: {
        index: "#",
        request_number: "คำขอเลขที่",
        registration_number: "ทะเบียนเลขที่",
        trademark_status_code: "สถานะคำขอ",
        name: "ชื่อเจ้าของ",
        save010_registration_group_status_code: "สถานะ",
        make_date: "วันที่มีคำสั่ง",
        allow_date: "วันที่อนุญาต",
      },
      column_edit_list: {
        save010_registration_group_status_code: { type: "combo_box", master: "save010RequestGroupStatusCodeList", },
        make_date: { type: "datetime", },
        allow_date: { type: "datetime", },
      },
      can_deleted: true,
      //can_added: true,
    }


    //
    this.tableDocumentScan = {
      column_list: {
        index: "#",
        request_document_collect_type_name: "ชื่อเอกสาร",
        updated_by_name: "เจ้าหน้าที่",
        created_date: "วันที่แนบ",
      },
      has_link: ["'/File/Content/' + row_item.file_id"],
    }

    this.tableInstructionRule = {
      column_list: {
        index: "#",
        instruction_date: "วันที่สั่งคำสั่ง",
        instruction_rule_name: "คำสั่ง",
        value_1: "รายละเอียด",
        instructor_by_name: "นายทะเบียน",
        considering_instruction_rule_status_name: "สถานะ",
      },
      has_link: ["'/File/Content/' + row_item.file_id"],
      command: [{
        name: "instruction_report",
        title: "แสดงรายงาน",
        icon: "printer",
      }],
    }

    this.tableReceipt = {
      column_list: {
        index: "#",
        request_type_name: "ชื่อเอกสาร",
        receiver_name: "เจ้าหน้าที่",
        created_date: "วันที่บันทึก",
        total_price: "จำนวน",
        receipt_status_name: "สถานะ",
      },
      has_link: ["'/pdf/Receipt/' + row_item.reference_number"],
    }

    this.tableHistory = {
      column_list: {
        index: "#",
        make_date: "วันที่",
        created_by_name: "ผู้ออกคำสั่ง",
        description: "รายการ",
      },
      command: [{
        name: "history_report",
        title: "แสดงรายงาน",
        icon: "printer",
      }],
      has_link: ["row_item.file_id && row_item.file_id > 0 ? '/file/Content/' + row_item.file_id : null"],
    }

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole01CheckDo().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
        this.master.addressCountryCodeList.unshift({ "code": "", "name": "ไม่พบข้อมูล" });
        this.send_back["department_code"] = this.master.publicRole01SendBackDepartmentList[0].code
      }

      if (this.editID) {
        this.PublicProcessService.PublicRole01CheckLoad(this.editID).subscribe((data: any) => {
          if (data) {
            //console.log(data)
            this.loadData(data)
          }

          let param = { save_id: this.editID }

          this.PublicProcessService.CertificationFileList(this.help.GetFilterParams(param)).subscribe((data: any) => {
            if (data) {
              this.tableCertificationFile.Set(data.list)

              this.PublicProcessService.HistoryList(this.help.GetFilterParams(param)).subscribe((data: any) => {
                if (data) {
                  this.tableHistory.Set(data.list)

                  this.PublicProcessService.RequestDocumentCollectList(this.help.GetFilterParams(param)).subscribe((data: any) => {
                    if (data) {
                      //console.log(data)
                      this.tableDocumentScan.Set(data.list)

                      var instrunction_param = { save_id: this.editID }
                      this.PublicProcessService.PublicInstructionList(this.help.GetFilterParams(instrunction_param)).subscribe((data: any) => {
                        if (data) {
                          //console.log(data)
                          this.tableInstructionRule.Set(data.list)

                          this.PublicProcessService.ReceiptList(this.help.GetFilterParams(param)).subscribe((data: any) => {
                            if (data) {
                              //console.log(data)
                              this.tableReceipt.Set(data.list)
                              //this.listReceiptList = data.list
                            }
                            this.global.setLoading(false)
                            this.automateTest.test(this)
                          })
                        }
                      })
                    }
                  })
                }
              })
            }
          })
          //this.global.setLoading(false)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  onKeyPressPublicRole01CheckRequestNumberAdd(event): void {
    console.log(event)
    if (event.key == "Enter") {
      var params = {
        request_number: this.tableRequestGroup.request_number_add
      }

      this.tableRequestGroup.request_number_add = ""

      // Call api
      this.callPressPublicRole01CheckRequestNumberAdd(params)
    }
  }
  callPressPublicRole01CheckRequestNumberAdd(params: any): void {
    this.PublicProcessService.SaveList(this.help.GetFilterParams(params)).subscribe((data: any) => {
      if (data && data.list && data.list.length > 0) {
        this.tableRequestGroup.Add({
          save_id: +this.editID,
          request_number: data.list[0].request_number,
          registration_number: data.list[0].registration_number,
          trademark_status_code: data.list[0].trademark_status_code,
          name: data.list[0].name,
        })
      }
      this.global.setLoading(false)
    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }


  onClickPublicRole01CheckSave(): void {
    //if(this.validatePublicRole01CheckSave()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callPublicRole01CheckSave(this.saveData())
    //}
  }
  validatePublicRole01CheckSave(): boolean {
    let result = validateService('validatePublicRole01CheckSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole01CheckSave(params: any): void {
    this.PublicProcessService.PublicRole01CheckSave(params.full_view).subscribe((data: any) => {
      // if(isValidPublicRole01CheckSaveResponse(res)) {
      if (data) {

        this.PublicProcessService.CertificationFileAdd(this.tableCertificationFile.Get()).subscribe((data: any) => {
          window.location.reload()
          //if (data && data.length > 0) {
          //  this.tableCertificationFile.Add(data[0])
          //}
        })
        // Set value
        //window.open("/public-role01-check/list", "_self")
        //this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickPublicRole01CheckSend(): void {
    //if(this.validatePublicRole01CheckSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callPublicRole01CheckSend(this.editID)
    //}
  }
  validatePublicRole01CheckSend(): boolean {
    let result = validateService('validatePublicRole01CheckSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole01CheckSend(params: any): void {
    this.PublicProcessService.PublicRole01CheckSend(params).subscribe((data: any) => {
      // if(isValidPublicRole01CheckSendResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.popup.isPopupSendOpen = true
        this.automateTest.test(this, { Send: 1 })
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }







  onClickPublicRole01CheckSendChange(): void {
    // Open loading
    this.global.setLoading(true)

    // Call api
    this.callPublicRole01CheckSendChange(this.saveData())
    //}
  }
  //validatePublicRole01CheckSendChange(): boolean {
  //  let result = validateService('validatePublicRole01CheckSendChange', this.input)
  //  this.validate = result.validate
  //  return result.isValid
  //}
  //! <<< Call API >>>
  callPublicRole01CheckSendChange(params: any): void {
    this.PublicProcessService.PublicRole01CheckSendChange(params).subscribe((data: any) => {
      // if(isValidPublicRole01CheckSendChangeResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.popup.isPopupSendOpen = true
        this.automateTest.test(this, { Send: 1 })
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }









  onClickPublicRole01CheckDoAuto(): void {
    //if(this.validatePublicRole01CheckDoAuto()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callPublicRole01CheckDoAuto(this.saveData())
    //}
  }
  validatePublicRole01CheckDoAuto(): boolean {
    let result = validateService('validatePublicRole01CheckDoAuto', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole01CheckDoAuto(params: any): void {
    this.PublicProcessService.PublicRole01CheckDoAuto(params).subscribe((data: any) => {
      // if(isValidPublicRole01CheckDoAutoResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickPublicRole01CheckDocumentScanListAdd(): void {
    this.listDocumentScanList.push({
      index: this.listDocumentScanList.length + 1,
      request_document_scan_type_name: null,
      created_by_name: null,
      created_date_text: null,
      command: null,

    })
    this.changePaginateTotal(this.listDocumentScanList.length, 'paginateDocumentScanList')
  }

  onClickPublicRole01CheckInstructionListAdd(): void {
    this.listInstructionList.push({
      index: this.listInstructionList.length + 1,
      considering_similar_instruction_rule_name: null,
      book_number: null,
      book_sent_date_text: null,
      people_received_date_text: null,
      people_expired_date_text: null,
      rule_remark: null,
      considering_instruction_status_name: null,

    })
    this.changePaginateTotal(this.listInstructionList.length, 'paginateInstructionList')
  }

  onClickPublicRole01CheckReceiptListAdd(): void {
    this.listReceiptList.push({
      index: this.listReceiptList.length + 1,
      receipt_document_name: null,
      created_by_name: null,
      created_date_text: null,
      total_price: null,
      receipt_status_name: null,
      command: null,

    })
    this.changePaginateTotal(this.listReceiptList.length, 'paginateReceiptList')
  }

  onClickCommand($event): void {
    console.log($event)

    if ($event.object_list.length > 0) {
      if ($event.command.name == "instruction_report") {
        window.open("pdf/ConsideringSimilaInstruction/" + this.input.request_number + "_" + $event.object_list.map((item: any) => { return item.id }).join("|"))
      } else if ($event.command.name == "history_report") {
        window.open("pdf/Save010History/" + this.input.request_number + "_" + $event.object_list.map((item: any) => { return item.id }).join("|"))
      } else if ($event.command.name == "file_upload") {
        var param = {
          save_id: +this.editID,
          file_id: +$event.object_list[0].id,
        }
        this.PublicProcessService.CertificationFileAdd([param]).subscribe((data: any) => {
          if (data && data.length > 0) {
            this.tableCertificationFile.Add(data[0])
          }
        })
      }
    }
  }

  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //onClickPublicRole01CheckPublicDraftOpen(): void {
  //    window.open("/pdf/PublicRole01Check/" + this.editID)
  //}

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.help.Clone(data.full_view.contact_address || {}, this.contactAddress)
    //this.contactAddress = data.full_view.contact_address


    this.tablePeople.Set(data.full_view.people_list)
    this.tableRepresentative.Set(data.full_view.representative_list)
    this.tableProduct.Set(data.full_view.product_list)
    this.tableJoiner.Set(data.full_view.joiner_list)
    this.tableRequestGroup.Set(data.full_view.request_group_list)

    this.request_mark_feature_code_list = {}
    if (data.full_view.request_mark_feature_code_list) data.full_view.request_mark_feature_code_list.split(',').forEach((item: any) => { this.request_mark_feature_code_list[item] = 1 })

    //data.sound_mark_list = data.sound_mark_list || ""
    //data.sound_mark_list.split(" ").forEach(item => this.listSoundMark.push({ "request_sound_type": item }))

    //this.listDocumentScanList = data.documentscanlist_list || []
    //this.listInstructionList = data.instructionlist_list || []
    //this.listReceiptList = data.receiptlist_list || []
    //this.changePaginateTotal(this.listDocumentScanList.length, 'paginateDocumentScanList')
    //this.changePaginateTotal(this.listInstructionList.length, 'paginateInstructionList')
    //this.changePaginateTotal(this.listReceiptList.length, 'paginateReceiptList')

  }

  listData(data: any): void {
    this.listDocumentScanList = data.list || []
    this.listInstructionList = data.list || []
    this.listReceiptList = data.list || []
    this.changePaginateTotal(this.listDocumentScanList.length, 'paginateDocumentScanList')
    this.changePaginateTotal(this.listInstructionList.length, 'paginateInstructionList')
    this.changePaginateTotal(this.listReceiptList.length, 'paginateReceiptList')

  }

  saveData(): any {
    let params = this.input

    params.full_view.contact_address = this.contactAddress

    params.full_view.request_mark_feature_code_list = []
    console.log(params.full_view)
    Object.keys(this.request_mark_feature_code_list).forEach((item: any) => {
      if (this.request_mark_feature_code_list[item] == 1) {
        params.full_view.request_mark_feature_code_list.push(item)
      }
    })
    params.full_view.request_mark_feature_code_list = params.full_view.request_mark_feature_code_list.join(',')



    params.save_id = +this.editID
    params.department_code = this.send_back.department_code
    params.reason = this.send_back.reason
    //this.send_back

    //params.documentscanlist_list = this.listDocumentScanList || []
    //params.instructionlist_list = this.listInstructionList || []
    //params.receiptlist_list = this.listReceiptList || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }


  onClickOpen(link: string): void {
    window.open(link + this.editID)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
  displayMoney(value: any): any {
    return displayMoney(value)
  }
}
