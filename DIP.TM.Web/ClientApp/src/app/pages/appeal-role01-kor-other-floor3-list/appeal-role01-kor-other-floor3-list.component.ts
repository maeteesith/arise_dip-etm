import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
import { RequestProcessService } from '../../services/request-process-buffer.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import '../../pages/appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss'
import { Auth } from "../../auth"
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  clone,
} from '../../helpers'

@Component({
  selector: 'app-appeal-role01-kor-other-floor3-list',
  templateUrl: './appeal-role01-kor-other-floor3-list.component.html',
  styleUrls: [
    '../appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss',
    './appeal-role01-kor-other-floor3-list.component.scss']
})

export class AppealRole01KorOtherFloor3ListComponent implements OnInit {

  public editID: any
  public input: any
  public validate: any
  public master: any

  // List RequestOtherItemSub
  public listRequestOtherItemSub: any[]
  public paginateRequestOtherItemSub: any
  public perPageRequestOtherItemSub: number[]
  // Response RequestOtherItemSub
  public responseRequestOtherItemSub: any
  public listRequestOtherItemSubEdit: any

  // List SaveProcess
  public listSaveProcess: any[]
  public paginateSaveProcess: any
  public perPageSaveProcess: number[]

  constructor(
    private route: Router,
    private global: GlobalService,
    private auth: Auth,
    private help: Help,
    private forkJoinService: ForkJoinService,
    private RequestProcessService: RequestProcessService
  ) { }

  ngOnInit() {
    this.input = {
      created_by_name: this.auth.getAuth().name,
      from_date: getMoment(),
      to_date: getMoment(),
      request_number: '',
      reference_number: '',
      receipt_status_code: '',
    }

    //Master List
    this.master = {
      receiptStatusCodeList: [],
      itemTypeList: [],
  }
    

    this.validate = {}

    this.listSaveProcess = []
    this.paginateSaveProcess = CONSTANTS.PAGINATION.INIT
    this.perPageSaveProcess = CONSTANTS.PAGINATION.PER_PAGE

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initRequestOtherItemList().subscribe((data: any) => {
        if (data) {
            this.master = data
            this.master.receiptStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })

            this.master.itemTypeList.unshift({ "code": "", "name": "ทั้งหมด" });
            console.log('this is a', this.master)

        }
        if (this.editID) {
            let pThis = this
            this.RequestProcessService.RequestOtherItemSubLoad(this.editID).subscribe((data: any) => {
                if (data) {
                    // Manage structure
                    pThis.loadData(data)
                }
                // Close loading
                pThis.global.setLoading(false)
            })
        }

        this.global.setLoading(false)
    })
}

  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  onClickRequestOtherItemSubList(): void {
    // if(this.validateRequestOtherItemSubList()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callRequestOtherItemSubList(this.saveData())
    // }
}
//! <<< Call API >>>
callRequestOtherItemSubList(params: any): void {
  let pThis = this
  this.RequestProcessService.RequestOtherItemSubList(this.help.GetFilterParams(params, this.paginateRequestOtherItemSub)).subscribe((data: any) => {
      // if(isValidRequestOtherItemSubListResponse(res)) {
      if (data) {
          // Set value
          pThis.listData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
  })
}


  callSave03List(params: any): void {
    let pThis = this
    console.log("this params", params, this.paginateRequestOtherItemSub)
    this.RequestProcessService.RequestOtherItemSubList(this.help.GetFilterParams(params, this.paginateRequestOtherItemSub)).subscribe((data: any) =>{
        // if(isValidSave03ListResponse(res)) {
        if (data) {
            // Set value
            pThis.listData(data)
        }
        // }
        // Close loading
        this.global.setLoading(false)
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }

//   onClickSave03SaveProcessEdit(item: any): void {
//     var win = window.open("appeal-role01-appeal-kor03-save/edit/" + item.id)
// }

loadData(data: any): void {
  this.input = data

  this.listRequestOtherItemSub = data.requestotheritemsub_list || []
  let index = 1
  index = 1
  this.listRequestOtherItemSub.map((item: any) => { item.is_check = false; item.index = index++; return item })
  this.changePaginateTotal(this.listRequestOtherItemSub.length, 'paginateRequestOtherItemSub')

}

listData(data: any): void {
  this.listRequestOtherItemSub = data.list || []
  this.help.PageSet(data, this.paginateRequestOtherItemSub)

  //this.listRequestOtherItemSub = data || []
  //let index = 1
  //index = 1
  //this.listRequestOtherItemSub.map((item: any) => { item.is_check = false; item.index = index++; return item })
  //this.changePaginateTotal(this.listRequestOtherItemSub.length, 'paginateRequestOtherItemSub')

}

saveData(): any {
  let params = {
      created_by_name: this.input.created_by_name,
      request_number: this.input.request_number,
      request_index: this.input.request_index,
      request_date: this.input.from_date,
      receipt_status_code: this.input.receipt_status_code,
  }
  //params.requestotheritemsub_list = this.listRequestOtherItemSub || []

  return params
}

oneWayDataBinding(name: any, value: any, object: any): void {
  if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
  }

  if (object) {
      object[name] = value
  } else {
      this.input[name] = value
  }
  console.log(object)
}

  checkAllCheckBox(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
        this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

}
