import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role05-consider-payment-list",
  templateUrl: "./public-role05-consider-payment-list.component.html",
  styleUrls: ["./public-role05-consider-payment-list.component.scss"]
})
export class PublicRole05ConsiderPaymentListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole05ConsiderPayment
  public listPublicRole05ConsiderPayment: any[]
  public paginatePublicRole05ConsiderPayment: any
  public perPagePublicRole05ConsiderPayment: number[]
  // Response PublicRole05ConsiderPayment
  public responsePublicRole05ConsiderPayment: any
  public listPublicRole05ConsiderPaymentEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public publicRole05ConsiderPaymentStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //public_role02_consider_payment_start_date: getMoment(),
      //public_role02_consider_payment_end_date: getMoment(),
      //public_role05_consider_payment_start_date: getMoment(),
      //public_role05_consider_payment_end_date: getMoment(),
      book_index: '',
      page_index: '',
      request_number: '',
      public_role05_consider_payment_status_code: '',
      public_role05_consider_payment_send_date: getMoment(),
    }
    this.listPublicRole05ConsiderPayment = []
    this.paginatePublicRole05ConsiderPayment = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole05ConsiderPayment.id = 'paginatePublicRole05ConsiderPayment'
    this.perPagePublicRole05ConsiderPayment = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      publicRole05ConsiderPaymentStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole05ConsiderPaymentList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.publicRole05ConsiderPaymentStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.public_role05_consider_payment_status_code = "WAIT"
      }
      if (this.editID) {
        this.PublicProcessService.PublicRole02ConsiderPaymentLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }

  onClickPublicRole05ConsiderPaymentList(): void {
    // if(this.validatePublicRole05ConsiderPaymentList()) {
    // Open loading
    // Call api
    this.callPublicRole05ConsiderPaymentList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole05ConsiderPaymentList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole05ConsiderPaymentList(this.help.GetFilterParams(params, this.paginatePublicRole05ConsiderPayment)).subscribe((data: any) => {
      // if(isValidPublicRole05ConsiderPaymentListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickPublicRole05ConsiderPaymentSend(): void {
    //if(this.validatePublicRole05ConsiderPaymentSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api

    this.listPublicRole05ConsiderPayment.forEach((item: any) => {
      item.public_role05_consider_payment_date = this.input.public_role05_consider_payment_send_date
    })

    this.callPublicRole05ConsiderPaymentSend(this.listPublicRole05ConsiderPayment)
    //}
  }
  validatePublicRole05ConsiderPaymentSend(): boolean {
    let result = validateService('validatePublicRole05ConsiderPaymentSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole05ConsiderPaymentSend(params: any): void {
    this.PublicProcessService.PublicRole05ConsiderPaymentSend(params).subscribe((data: any) => {
      // if(isValidPublicRole05ConsiderPaymentSendResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.onClickPublicRole05ConsiderPaymentList()
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickPublicRole05ConsiderPaymentAdd(): void {
    this.listPublicRole05ConsiderPayment.push({
      index: this.listPublicRole05ConsiderPayment.length + 1,
      request_number: null,
      public_start_date_text: null,
      public_end_date_text: null,
      book_index: null,
      page_index_line_index: null,
      request_item_sub_type_1_code_text: null,
      total_amount: null,
      public_role02_consider_payment_by_name: null,
      public_role02_consider_payment_date: null,
      public_role05_consider_payment_date: null,
      save010_public_payment_remark: null,
      public_role05_consider_payment_status_name: null,

    })
    this.changePaginateTotal(this.listPublicRole05ConsiderPayment.length, 'paginatePublicRole05ConsiderPayment')
  }

  onClickPublicRole05ConsiderPaymentEdit(item: any): void {
    var win = window.open("/public-role05-consider-payment/edit/" + item.save_id)
  }


  onClickPublicRole05ConsiderPaymentDelete(item: any): void {
    // if(this.validatePublicRole05ConsiderPaymentDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listPublicRole05ConsiderPayment.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listPublicRole05ConsiderPayment.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listPublicRole05ConsiderPayment.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listPublicRole05ConsiderPayment.length; i++) {
          if (this.listPublicRole05ConsiderPayment[i].is_check) {
            this.listPublicRole05ConsiderPayment[i].cancel_reason = rs
            this.listPublicRole05ConsiderPayment[i].status_code = "DELETE"
            this.listPublicRole05ConsiderPayment[i].is_deleted = true

            if (true && this.listPublicRole05ConsiderPayment[i].id) ids.push(this.listPublicRole05ConsiderPayment[i].id)
            // else this.listPublicRole05ConsiderPayment.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callPublicRole05ConsiderPaymentDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listPublicRole05ConsiderPayment.length; i++) {
          this.listPublicRole05ConsiderPayment[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callPublicRole05ConsiderPaymentDelete(params: any, ids: any[]): void {
    this.PublicProcessService.PublicRole05ConsiderPaymentDelete(params).subscribe((data: any) => {
      // if(isValidPublicRole05ConsiderPaymentDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listPublicRole05ConsiderPayment.length; i++) {
          if (this.listPublicRole05ConsiderPayment[i].id == id) {
            this.listPublicRole05ConsiderPayment.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listPublicRole05ConsiderPayment.length; i++) {
        this.listPublicRole05ConsiderPayment[i] = i + 1
      }

      this.onClickPublicRole05ConsiderPaymentList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicRole05ConsiderPayment = data.publicrole05considerpayment_list || []
    this.changePaginateTotal(this.listPublicRole05ConsiderPayment.length, 'paginatePublicRole05ConsiderPayment')

  }

  listData(data: any): void {
    this.listPublicRole05ConsiderPayment = data.list || []
    this.help.PageSet(data, this.paginatePublicRole05ConsiderPayment)
    //this.listPublicRole05ConsiderPayment = data.list || []
    //this.changePaginateTotal(this.listPublicRole05ConsiderPayment.length, 'paginatePublicRole05ConsiderPayment')

  }

  saveData(): any {
    // let params = this.input
    // params.publicrole05considerpayment_list = this.listPublicRole05ConsiderPayment || []

    const params = {
      public_role02_consider_payment_start_date: this.input.public_role02_consider_payment_start_date,
      public_role02_consider_payment_end_date: this.input.public_role02_consider_payment_end_date,
      public_role05_consider_payment_start_date: this.input.public_role05_consider_payment_start_date,
      public_role05_consider_payment_end_date: this.input.public_role05_consider_payment_end_date,
      book_index: this.input.book_index,
      page_index: this.input.page_index,
      request_number: this.input.request_number,
      public_role05_consider_payment_status_code: this.input.public_role05_consider_payment_status_code,
      //page_index: +this.paginatePublicRole05ConsiderPayment.currentPage,
      //item_per_page: +this.paginatePublicRole05ConsiderPayment.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'public_role02_consider_payment_date',
      //  value: displayDateServer(this.input.public_role02_consider_payment_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_role02_consider_payment_date',
      //  value: displayDateServer(this.input.public_role02_consider_payment_end_date),
      //  operation: 4
      //}, {
      //  key: 'public_role05_consider_payment_date',
      //  value: displayDateServer(this.input.public_role05_consider_payment_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_role05_consider_payment_date',
      //  value: displayDateServer(this.input.public_role05_consider_payment_end_date),
      //  operation: 4
      //}, {
      //  key: 'book_index',
      //  value: this.input.book_index,
      //  operation: 0
      //}, {
      //  key: 'page_index',
      //  value: this.input.page_index,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}, {
      //  key: 'public_role05_consider_payment_status_code',
      //  value: this.input.public_role05_consider_payment_status_code,
      //  operation: 0
      //}]
    }

    return params
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicRole05ConsiderPayment') {
      this.onClickPublicRole05ConsiderPaymentList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
