import { Component, OnInit } from '@angular/core';
import {
  ROUTE_PATH
} from '../../helpers'

@Component({
  selector: 'app-consider-kor04-request-list',
  templateUrl: './consider-kor04-request-list.component.html',
  styleUrls: ['./consider-kor04-request-list.component.scss']
})
export class ConsiderKor04RequestListComponent implements OnInit {

  constructor() { }

  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public master: any;

  public pathChange: any;
  public tableList: any;

  ngOnInit() {
    this.input = {};
    this.validate = {};
    this.master = {
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList2: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList3: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
    };

    this.pathChange = ROUTE_PATH.INCLUDE_ROLE01_CHANGE.LINK;

    this.tableList = {
      column_list: [
        {
          request_id: "741150971",
          submission_date: "10/09/2562",
          proposed_date: "10/09/2562",
          officer: "นางนิจรินทร์ กุหลาบป่า",
          consider_date: "",
          job_type: "เปลี่ยนแปลง",
          petition: "ก.06",
          detail: "",
          status: "waiting"
        },
        {
          request_id: "712224642",
          submission_date: "10/09/2562",
          proposed_date: "10/09/2562",
          officer: "นางนิจรินทร์ กุหลาบป่า",
          consider_date: "",
          job_type: "ต่ออายุ",
          petition: "ก.04 , ก.06 , ก.20",
          detail: "",
          status: "done"
        },
        {
          request_id: "790849099",
          submission_date: "10/09/2562",
          proposed_date: "10/09/2562",
          officer: "นางนิจรินทร์ กุหลาบป่า",
          consider_date: "10/09/2562",
          job_type: "เปลี่ยนแปลง",
          petition: "ก.06",
          detail: "",
          status: "waiting"
        }
      ],
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>
  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
}
