import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
// import { MadridRole05ModalComponent } from '../madrid-role05-modal/madrid-role05-modal.component'
// import { MadridRole05DocAttachComponent } from '../madrid-role05-doc-attach/madrid-role05-doc-attach.component';
// import { MadridRole05PaymentComponent } from '../madrid-role05-payment/madrid-role05-payment.component'
// import { MadridRole05FeeComponent } from '../madrid-role05-fee/madrid-role05-fee.component'
import { MatDialog } from '@angular/material'
import { DialogService } from '../../services/dialogService'

import {
    CONSTANTS,
    getMoment,
    clone,
    displayDateServer
} from '../../helpers'

@Component({
  selector: 'app-madrid-role05-work-payment',
  templateUrl: './madrid-role05-work-payment.component.html',
  styleUrls: ['./madrid-role05-work-payment.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole05WorkPaymentComponent implements OnInit {
  public input: any
  public validate: any
  public master: any
  public editID: any

  listPublicItem=[{
    index:1
  }]

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private _dialog: DialogService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService,
    private dialog: MatDialog,
) { }

ngOnInit() {
  this.validate = {}
  this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      public_type_code: '',
      public_source_code: '',
      public_receiver_by: '',
      public_status_code: '',
      request_number: '',
    }
    this.master={
        request_type:[{code:1,name:'1'}],
        job_reciver:[{id:1,name:"Sam"},{id:2,name:"Tom"}]
    }

}

  onClickModal(){
    //console.log(resp, "selected");
		// const dialogRef = this.dialog.open(MadridRole05ModalComponent, {
    //   width: "calc(100% - 80px)",
    //   height: "calc(100% - 80px)",
    //   maxWidth: "100%",
		// 	data: {}
		// });
		// dialogRef.afterClosed().subscribe(res => {
		// 	// this.router.navigate(['eordering/delivery-note/create-request']);
		// 	// if (!res) return;

		// 	// this.router.navigate(['eordering/delivery-note/create-request']);
		// 	// this.navToAddProduct(<ApiProductGroupDisplay>res);
		// });
  }

  onEdpopup(){
    // let button = [
    //   //id คือค่าที่จะส่งกลับมาถ้าเรา subscribe , butclass คือ icon หน้าข้อความ เช่นคำว่า ยกเลิกหรือตกลง, border ทำไว้เผื่อปุ่มไม่มีสี 
    //   { id: "CANCEL",name: "ยกเลิก", butclass: "dip-icon-close", border: "solid 1px #C5C7C9", color: "#C5C7C9"}, 
    //   { id: "YES", name: "ส่งแก้ไข",butclass: "dip-icon-check-2", color: "#ffffff", backgroundColor: "#0C84C8" }
    // ]

    // this._dialog.showEditorConfirm(
    //   "ส่งแก้ไข", //ข้อความ header
    //   "dip-icon-edit", //icon header
    //   "<h4 style='color: #0C84C8;font-size: medium;'>รายละเอียด</h4>" + 
    //   "<div class='border-div'><p>ปัทวเหตุโยโย่ฟรุต สวีทเคลมสะบึม ฉลุยโต๋เต๋ ปฏิสัมพันธ์ว้อดก้าแช่แข็ง ไรเฟิลเวอร์โพลารอยด์ โฮปแอคทีฟโรแมนติคท็อปบูต ฮากกาสะบึมส์มอคค่าแรงใจเบญจมบพิตร ซินโดรมล็อบบี้กิมจิกีวีเซอร์วิส</p>"+
    //   "<p class='pad-ft'>- สติกเกอร์แคทวอล์คบ็อกซ์</p>"+
    //   "<p class='pad-ft'>- นางแบบสติกเกอร์ถูกต้องมินต์ว้อย</p>"+
    //   "<p class='pad-ft'>- สต็อกแชมพู ฮองเฮามุมมองอัลตรา</p>"+
    //   "<p class='pad-ft'>- เซอร์ไพรส์ พูล ซีนีเพล็กซ์แบล็คเกรย์</p>"+
    //   "<p class='pad-ft'>- แบรนด์เบอร์เกอร์ฮิมิลค์</p>"+
    //   "</div>",  //body can input as html
    //   button // ปุ่ม
    // ).subscribe(x => {
    //   console.log(x);
    // })

  }
  onConfirm(){
    // let button = [
    //   //id คือค่าที่จะส่งกลับมาถ้าเรา subscribe , butclass คือ icon หน้าข้อความ เช่นคำว่า ยกเลิกหรือตกลง, border ทำไว้เผื่อปุ่มไม่มีสี 
    //   { id: "CANCEL",name: "ยกเลิก", butclass: "dip-icon-close", border: "solid 1px #C5C7C9", color: "#C5C7C9"}, 
    //   { id: "YES", name: "ตกลง",butclass: "dip-icon-check-2", color: "#ffffff", backgroundColor: "#0C84C8" }
    // ]

    // this._dialog.showDecisionConfirm(
    //   "พิจารณาอนุมัติ", //title
    //   "#0C84C8", //สีของ title
    //   "ยืนยันการส่งงานไปยัง WIPO", //description
    //   button //ปุ่มแต่ละปุ่ม
    //   ).subscribe(x => {
    //     console.log(x);
    //   });
  }

  docAttach(){
    // const dialogRef = this.dialog.open(MadridRole05DocAttachComponent, {
    //   width: "1100px",
    //   height: "auto",
    //   maxWidth: "100%",
		// 	data: {}
		// });
		// dialogRef.afterClosed().subscribe(res => {
		// 	// this.router.navigate(['eordering/delivery-note/create-request']);
		// 	// if (!res) return;

		// 	// this.router.navigate(['eordering/delivery-note/create-request']);
		// 	// this.navToAddProduct(<ApiProductGroupDisplay>res);
		// });
  }

  paymentShow(){
    // const dialogRef = this.dialog.open(MadridRole05PaymentComponent, {
    //   width: "740px",
    //   height: "auto",
    //   maxWidth: "100%",
		// 	data: {}
		// });
		// dialogRef.afterClosed().subscribe(res => {
		// 	// this.router.navigate(['eordering/delivery-note/create-request']);
		// 	// if (!res) return;

		// 	// this.router.navigate(['eordering/delivery-note/create-request']);
		// 	// this.navToAddProduct(<ApiProductGroupDisplay>res);
		// });
  }

  feeShow() {
    // const dialogRef = this.dialog.open(MadridRole05FeeComponent, {
    //   width: "calc(90% - 80px)",
    //   height: "calc(100% - 80px)",
    //   maxWidth: "100%",
		// 	data: {}
		// });
		// dialogRef.afterClosed().subscribe(res => {
		// 	// this.router.navigate(['eordering/delivery-note/create-request']);
		// 	// if (!res) return;

		// 	// this.router.navigate(['eordering/delivery-note/create-request']);
		// 	// this.navToAddProduct(<ApiProductGroupDisplay>res);
		// });
  }

}
