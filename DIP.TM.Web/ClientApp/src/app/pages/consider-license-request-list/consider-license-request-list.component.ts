import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GlobalService } from "./../../global.service";
import { ForkJoinService } from "../../services/fork-join.service";
import {
  ROUTE_PATH,
  CONSTANTS,
  clone,
  displayLabelReferenceMaster,
} from "../../helpers";

@Component({
  selector: "app-consider-license-request-list",
  templateUrl: "./consider-license-request-list.component.html",
  styleUrls: ["./consider-license-request-list.component.scss"],
})
export class ConsiderLicenseRequestListComponent implements OnInit {
  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public list: any;
  public master: any;
  public validate: any;
  // Paginate
  public paginateConsiderLicenseRequest: any;

  constructor(
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService
  ) {}

  ngOnInit() {
    // Init
    this.input = {};
    this.list = {};
    this.master = {};
    this.validate = {};
    // Paginate
    this.paginateConsiderLicenseRequest = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateConsiderLicenseRequest.id = "paginateConsiderLicenseRequest";

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService
      .initConsiderLicenseRequestPage()
      .subscribe((data: any) => {
        if (data) {
          this.master = data;
          console.log("this.master", this.master);

          //! Mock master data
          this.master = {
            statusCodeList: [
              { code: "waiting", name: "การดำเนินการ" },
              { code: "done", name: "เสร็จสิ้น" },
              { code: "cancel", name: "เพิกถอน" },
            ],
            paymentCodeList: [
              { code: "payment_1", name: "การชำระเงิน 1" },
              { code: "payment_2", name: "การชำระเงิน 2" },
            ],
          };
        }

        // Call api list
        this.onSearch();
      });
  }

  //! <<< Call API >>>
  callConsiderLicenseRequestList(params: any): void {
    //! Mock data list
    this.list.considerLicenseRequest = [
      {
        request_id: "741150971",
        submission_date: "30/03/2562",
        proposed_date: "31/03/2562",
        consider_date: "",
        petition: "ก.05",
        reference_number: "",
        contract_date: "",
        person: "นางนิจรินทร์ กุหลาบป่า",
        owner: "นายโรจน์ศักดิ์ ตั้งกมลสุข",
        status: "waiting",
      },
      {
        request_id: "712224642",
        consider_date: "",
        petition: "ก.05",
        reference_number: "387",
        contract_date: "29/02/2562",
        person: "นางนิจรินทร์ กุหลาบป่า",
        owner: "นายโรจน์ศักดิ์ ตั้งกมลสุข",
        status: "waiting",
      },
      {
        request_id: "790849099",
        consider_date: "",
        petition: "ก.08",
        reference_number: "386",
        contract_date: "29/02/2562",
        person: "นางนิจรินทร์ กุหลาบป่า",
        owner: "นายโรจน์ศักดิ์ ตั้งกมลสุข",
        status: "waiting",
      },
      {
        request_id: "857583043",
        consider_date: "10/09/2562",
        petition: "ก.08",
        reference_number: "384",
        contract_date: "29/02/2562",
        person: "ซา ซา ด็อท คอม ลิมิเต็ด",
        owner: "นายวรเศรษฐ์ โภชนกุล",
        status: "done",
      },
      {
        request_id: "962023340",
        consider_date: "10/09/2562",
        petition: "ก.05",
        reference_number: "383",
        contract_date: "29/02/2562",
        person: "ซา ซา ด็อท คอม ลิมิเต็ด",
        owner: "นายวรเศรษฐ์ โภชนกุล",
        status: "cancel",
      },
      {
        request_id: "345755881",
        consider_date: "10/09/2562",
        petition: "ก.08",
        reference_number: "382",
        contract_date: "29/02/2562",
        person: "นางนิจรินทร์ กุหลาบป่า",
        owner: "นางสาวมยุรี เลิศคัมภีร์ศีล",
        status: "done",
      },
    ];
    this.changePaginateTotal(
      this.list.considerLicenseRequest.length,
      "paginateConsiderLicenseRequest"
    );
    this.global.setLoading(false);

    // this.includeRole01Service
    //   .List(params)
    //   .subscribe((data: any) => {
    //     if (data) {
    //       // Set data
    //       this.list.considerLicenseRequest = data.list;
    //       // Update pagin
    //       this.changePaginateTotal(
    //         this.list.considerLicenseRequest.length,
    //         "paginateConsiderLicenseRequest"
    //       );
    //     }
    //     // Close loading
    //     this.global.setLoading(false);
    //   });
  }

  //! <<< Prepare Call API >>>
  onSearch(): void {
    // Clear validate
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {};
    // Call api
    this.callConsiderLicenseRequestList(params);
  }
  onReset(): void {
    // Clear validate
    this.clearAllValidate();
    // Clear input
    this.input = {};
    // Call api list
    this.onSearch();
  }

  //! <<< Event >>>
  onClickAction(): void {
    this.router.navigate([ROUTE_PATH.INCLUDE_ROLE01_CHANGE.LINK]);
  }

  //! <<< Style >>>
  getStatusStyle(code: any): string {
    switch (code) {
      case this.master.statusCodeList[0].code:
        return "tag blue";
      case this.master.statusCodeList[1].code:
        return "tag";
      case this.master.statusCodeList[2].code:
        return "tag red";
      default:
        return "tag";
    }
  }
  getButtonStyle(code: any): string {
    switch (code) {
      case this.master.statusCodeList[0].code:
        return "btn btn-icon btn-brand";
      case this.master.statusCodeList[1].code:
        return "btn btn-icon btn-light-gray";
      case this.master.statusCodeList[2].code:
        return "btn btn-icon btn-light-gray";
      default:
        return "btn btn-icon btn-light-gray";
    }
  }
  getIconStyle(code: any): string {
    switch (code) {
      case this.master.statusCodeList[0].code:
        return "dip-icon-play";
      case this.master.statusCodeList[1].code:
        return "dip-icon-search-plus";
      case this.master.statusCodeList[2].code:
        return "dip-icon-search-plus";
      default:
        return "dip-icon-search-plus";
    }
  }
  getIsDisableIcon(code: any): boolean {
    switch (code) {
      case this.master.statusCodeList[0].code:
        return false;
      case this.master.statusCodeList[1].code:
        return true;
      case this.master.statusCodeList[2].code:
        return true;
      default:
        return true;
    }
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Binding (one way) >>>
  binding(obj: string, name: string, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: string, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Binding List (one way) >>>
  bindingInList(item: any, name: string, value: any): void {
    item[name] = value;
  }
  toggleBooleanInList(item: any, name: string): void {
    item[name] = !item[name];
  }
  onCheckAllInList(obj: string, name: string): void {
    this.input.is_check_all = !this.input.is_check_all;
    this[obj][name].forEach((item: any) => {
      item.is_check = this.input.is_check_all;
    });
  }
  onRemoveInList(
    obj: string,
    name: string,
    index: number,
    paginName?: string
  ): void {
    this[obj][name].splice(index, 1);
    if (paginName) {
      this.changePaginateTotal(this[obj][name].length, paginName);
    }
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === "paginateConsiderLicenseRequest") {
      this.input.is_check_all = false;
      this.onSearch();
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Other >>>
  displayLabelReferenceMaster(list: any, code: any): string {
    return displayLabelReferenceMaster(list, code);
  }
}
