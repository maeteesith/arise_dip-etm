/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { Mm08Component } from './mm08.component';

describe('Mm08Component', () => {
  let component: Mm08Component;
  let fixture: ComponentFixture<Mm08Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Mm08Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Mm08Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
