import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from '../../services/fork-join2.service'
import { eFormSaveProcessService } from '../../services/eform-save-process.service'
import { AutomateTest } from '../../test/automate_test'

import {
  CONSTANTS,
  ROUTE_PATH,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone,
  clearIdAndSaveId,
  viewPDF,
  displayAddress,
  getParamsOverwrite,
} from '../../helpers'

@Component({
  selector: "app-eform-save-030-process",
  templateUrl: "./eform-save-030-process.component.html",
  styleUrls: ["./eform-save-030-process.component.scss"]
})
export class eFormSave030ProcessComponent implements OnInit, DeactivationGuarded {
  //Init
  public menuList: any[];
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubID: number;
  public currentSubStep: number;
  public progressPercent: number;
  //TODO >>> Declarations <<</'
  public editID: any;
  public input: any;
  public seachData: any;
  public item: any;
  public validate: any;
  public master: any;
  // Response
  public response: any;
  public email: string;
  public telephone: string;
  public perpage: number;
  public currentPage: number;
  public total: number;
  public saveInput: any;
  public contactAddress: any;
  public save030_contact_index: number;
  public firstOwnerName: string;
  public firstOwnerTel: string;

  public autocompleteListListModalLocation: any;
  public is_autocomplete_ListModalLocation_show: any;
  public is_autocomplete_ListModalLocation_load: any;
  public inputModalAddress: any;
  public inputModalAddressEdit: any;
  public modal: any;
  public inputAddress: any;

  public findVal: any;
  public isSubmited: any;
  public isValid: any;
  public stepPass: number;
  public listdropdownContact: any;
  public ContactWord: any;
  public contactChangeType: string;

  public lawList: any[];
  public validateObj: any;
  public popup: any;
  public instruction_rule_list: any[];

  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private router: Router,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eFormSaveProcessService: eFormSaveProcessService
  ) { }

  ngOnInit() {

    this.menuList = [
      {
        id: 1,
        number: 1,
        name: 'บันทึกเพื่อรับไฟล์',
        required: true,
        canedit: false
      },
      {
        id: 2,
        number: 2,
        name: 'คำอุทธรณ์',
        required: true,
        canedit: false
      },
      {
        id: 3,
        number: 3,
        name: 'ผู้อุทธรณ์/ตัวแทน',
        required: true,
        canedit: false
      },
      {
        id: 4,
        number: 4,
        name: 'สถานที่ติดต่อภายในประเทศไทย',
        canedit: false
      },
      {
        id: 5,
        number: 5,
        name: 'อุทธรณ์คำสั่ง',
        canedit: false
      },
      {
        id: 6,
        number: 6,
        name: 'ระบุเหตุแห่งการอุทธรณ์',
        canedit: false
      },
      {
        id: 7,
        number: 7,
        name: 'บัญชีเอกสารหรือหลักฐานประกอบอุทธรณ์',
        canedit: false
      },
      {
        id: 8,
        number: 8,
        name: 'เอกสารคำอุทธรณ์',
        canedit: false
      },
      {
        id: 9,
        number: 9,
        name: 'ค่าธรรมเนียม',
        canedit: false
      },
      {
        id: 10,
        number: 10,
        name: 'เสร็จสิ้น',
        canedit: false
      },
    ];

    this.editID = this.route.snapshot.paramMap.get('id');
    this.validate = {};
    this.modal = {};
    this.item = {};
    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
      isSearch: false,
      isWarning: false,
    };
    this.response = {
      load: {},
    };
    this.input = {
      indexEdit: undefined,
      point: "",
      listOwnerMark: [],
      product_list: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listAgentMark: [],
      people_list:[],
      representative_list: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      ownerDomesticContactAddress: 0,
      agentDomesticContactAddress: 0,
      subagentDomesticContactAddress: 0,
      dimension_image: 2,
      countryVal: '1',
      personalTypeVal: '1',
      agent_personal_typeVal: '1',
      agent_countryVal: '1',
      make_date: "",
      payer_name: "",
      fee: "",
      fine: "",
      total_price: "",
      request_number: null,
      register_number: "",
      search_number: "",
      is_search_success: false,
      save030_representative_condition_type_code: "AND_OR",
      save030_search_type_code: "REQUEST_NUMBER",
      load_receiver_people_type_code: "",
      listEvidence: [],
      imageDimension2: {
        file: {},
        blob: null
      },
      imageDimension3_1: {
        file: {},
        blob: null
      },
      imageDimension3_2: {
        file: {},
        blob: null
      },
      imageDimension3_3: {
        file: {},
        blob: null
      },
      imageDimension3_4: {
        file: {},
        blob: null
      },
      sizeX: 5,
      sizeY: 5,
      isResize: false,
      audio: {
        input: "",
        validate: "",
        file: {},
        sound: null,
        palyer: null,
        action: false
      },
      imageNote: {
        file: {},
        blob: null
      },
      listTranslation: [],
      translationItem: {},
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0
      },
      imageMark: {
        file: {},
        blob: null
      }
    };
    this.validate = {};
    this.master = {
      requestSoundTypeList: [],
      inputTypeCodeList: [],
      requestSourceCodeList: [],
      saveOTOPTypeCodeList: [],
      addressCountryCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressRepresentativeConditionTypeCodeList: [],
      addressSexCodeList: [],
      languageCodeList: [],
      priceMasterList: [],
      save030SearchTypeCode: []
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalDomesticContactAddressOpen: true
    };

    //
    this.autocompleteListListModalLocation = [];
    this.is_autocomplete_ListModalLocation_show = false;
    this.is_autocomplete_ListModalLocation_load = false;
    this.modal = { isModalPeopleEditOpen: false, };
    this.inputAddress = {};
    this.inputModalAddress = {};
    this.contactAddress = {};
    //

    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubID = 1;
    this.currentSubStep = 1;
    this.progressPercent = 0;
    this.stepPass = 1;
    this.isSubmited = false;
    this.isValid = false;

    this.input.listEvidence = [{
      evidence_type: "",
      evidence_subject: "",
      number: "",
      note: "",
    }];

    this.input.listProduct.push({
      law_code: "",
      law_name: "มาตรา",
      law_desc: "",
    });

    // Other
    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;
    
    this.callInit();
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initeFormSave030Process().subscribe((data: any) => {
      if (data) {
        this.master = data;
        data.requestTypeList.forEach((item: any) => {
          if (item.code === "30" && !this.editID) {
            this.input.total_price = item.value_2;
          }
        });
        console.log("Master", this.master)

        this.master.addressReceiverTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.address_receiver_type_code = ""
      }
      if (this.editID) {
        this.eFormSaveProcessService.eFormSave030Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data);
            this.response.load = {
              eform_number: data.eform_number,
              id: data.id,
              wizard: data.wizard,
            };
            this.input.search_number = data.request_number;

            //step3
            this.input.listOwnerMark = data.people_list;
            this.input.listAgentMark = data.representative_list;

            this.input.save030_representative_condition_type_code = data.save030_representative_condition_type_code
              ? data.save030_representative_condition_type_code
              : "AND_OR";

            this.input.ownerMarkItem = {
              receiver_type_code: "PEOPLE",
              sex_code: "MALE",
              nationality_code: "AF",
              career_code: "ค้าขาย",
              address_country_code: "TH"
            };

            this.input.agentMarkItem = {
              receiver_type_code: "PEOPLE",
              sex_code: "MALE",
              nationality_code: "AF",
              career_code: "ค้าขาย",
              address_country_code: "TH"
            }

            //step 7  
            if (data.people_list.length > 0)
            {
                this.firstOwnerName = data.people_list[0].name;
                this.firstOwnerTel = data.people_list[0].telephone;
            }

            if (data.evidence_list.length > 0){
              this.input.listEvidence = data.evidence_list
            } else{
              this.input.listEvidence = [{
                evidence_type: "",
                evidence_subject: "",
                number: "",
                note: "",
              }];
            }
            
            //step 10
            this.input.isCheckAllOwnerSignature = data.sign_inform_person_list === "0" ? true : false;
            this.input.isCheckAllAgentSignature = data.sign_inform_representative_list === "0" ? true : false;
            this.manageWizard(data.wizard);
            this.setSignInform(data.sign_inform_representative_list);
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  contactTypeChange(): void {
    //this.Next4 = true;
    switch (this.input.save030_contact_type_code) {
      case "APPEAL":
        this.listdropdownContact = this.input.listOwnerMark;
        this.ContactWord = "ใช้ที่อยู่ของเจ้าของลำดับที่";
        this.contactChangeType = "APPEAL";
        break;
      case "REPRESENTATIVE":
        this.listdropdownContact = this.input.listAgentMark;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนลำดับที่"
        this.contactChangeType = "REPRESENTATIVE";
        break;
      case "REPRESENTATIVE_PERIOD":
        this.listdropdownContact = this.input.listAgentMark;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนช่วงลำดับที่"
        this.contactChangeType = "REPRESENTATIVE_PERIOD";
        break;
      default:
        break;
    }

    this.save030_contact_index = 0;
    this.contactChange(this.input.save030_contact_type_code, this.save030_contact_index);
  }

  contactChange(contactType: string, index: number): void {
    console.log("index", index)

    this.input.listOwnerMark.forEach(data => {
      data.is_contact_person = false;
    });

    this.input.listAgentMark.forEach(data => {
      data.is_contact_person = false;
    });

    if (contactType == "APPEAL") {
      this.input.listOwnerMark[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE") {
      this.input.listAgentMark[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE_PERIOD") {

      this.input.listAgentMark[index].is_contact_person = true;
    }
  }

  validateSaveItemModal(nameItem: any): boolean {
    if (nameItem === "ownerMarkItem") {
      if (this.input.ownerMarkItem.address_country_code != "TH") {
        this.validateObj = {
          receiver_type_code: this.input.ownerMarkItem.receiver_type_code,
          card_number: this.input.ownerMarkItem.card_number,
          name: this.input.ownerMarkItem.name,
          sex_code: this.input.ownerMarkItem.sex_code,
          nationality_code: this.input.ownerMarkItem.nationality_code,
          career_code: this.input.ownerMarkItem.career_code,
          address_country_code: this.input.ownerMarkItem.address_country_code,
          house_number: this.input.ownerMarkItem.house_number,
          address_sub_district_name: "not validate",
          postal_code: "not validate",
          email: this.input.ownerMarkItem.email,
          telephone: this.input.ownerMarkItem.telephone,
        }
      } else
      {
        this.validateObj = {
          receiver_type_code: this.input.ownerMarkItem.receiver_type_code,
          card_number: this.input.ownerMarkItem.card_number,
          name: this.input.ownerMarkItem.name,
          sex_code: this.input.ownerMarkItem.sex_code,
          nationality_code: this.input.ownerMarkItem.nationality_code,
          career_code: this.input.ownerMarkItem.career_code,
          address_country_code: this.input.ownerMarkItem.address_country_code,
          house_number: this.input.ownerMarkItem.house_number,
          address_sub_district_name: this.input.ownerMarkItem.address_sub_district_name,
          postal_code: this.input.ownerMarkItem.postal_code,
          email: this.input.ownerMarkItem.email,
          telephone: this.input.ownerMarkItem.telephone,
        }
      }
      let result = validateService('validateEFormSave04ProcessStep5OwnerMarkItem', this.validateObj)
      this.validate = result.validate
      return result.isValid
    }
    if (nameItem === "agentMarkItem") {
      if (this.input.agentMarkItem.address_country_code != "TH") {
        this.validateObj = {
          receiver_type_codeAgent: this.input.agentMarkItem.receiver_type_code,
          card_numberAgent: this.input.agentMarkItem.card_number,
          nameAgent: this.input.agentMarkItem.name,
          sex_codeAgent: this.input.agentMarkItem.sex_code,
          nationality_codeAgent: this.input.agentMarkItem.nationality_code,
          career_codeAgent: this.input.agentMarkItem.career_code,
          address_country_codeAgent: this.input.agentMarkItem.address_country_code,
          house_numberAgent: this.input.agentMarkItem.house_number,
          address_sub_district_nameAgent: "not validate",
          postal_codeAgent: "not validate",
          emailAgent: this.input.agentMarkItem.email,
          telephoneAgent: this.input.agentMarkItem.telephone,
        }
      } else
      {
        this.validateObj = {
          receiver_type_codeAgent: this.input.agentMarkItem.receiver_type_code,
          card_numberAgent: this.input.agentMarkItem.card_number,
          nameAgent: this.input.agentMarkItem.name,
          sex_codeAgent: this.input.agentMarkItem.sex_code,
          nationality_codeAgent: this.input.agentMarkItem.nationality_code,
          career_codeAgent: this.input.agentMarkItem.career_code,
          address_country_codeAgent: this.input.agentMarkItem.address_country_code,
          house_numberAgent: this.input.agentMarkItem.house_number,
          address_sub_district_nameAgent: this.input.agentMarkItem.address_sub_district_name,
          postal_codeAgent: this.input.agentMarkItem.postal_code,
          emailAgent: this.input.agentMarkItem.email,
          telephoneAgent: this.input.agentMarkItem.telephone,
        }
      }
      
      let result = validateService('validateEFormSave04ProcessStep5AgentMarkItem', this.validateObj)
      this.validate = result.validate
      return result.isValid
    }

    if (nameItem === "receiverMarkItem") {
      this.validateObj = {
        receiver_type_codeReceiver: this.input.receiverMarkItem.receiver_type_code,
        card_numberReceiver: this.input.receiverMarkItem.card_number,
        nameReceiver: this.input.receiverMarkItem.name,
        sex_codeReceiver: this.input.receiverMarkItem.sex_code,
        nationality_codeReceiver: this.input.receiverMarkItem.nationality_code,
        career_codeReceiver: this.input.receiverMarkItem.career_code,
        address_country_codeReceiver: this.input.receiverMarkItem.address_country_code,
        house_numberReceiver: this.input.receiverMarkItem.house_number,
        address_sub_district_nameReceiver: this.input.receiverMarkItem.address_sub_district_name,
        postal_codeReceiver: this.input.receiverMarkItem.postal_code,
        emailReceiver: this.input.receiverMarkItem.email,
        telephoneReceiver: this.input.receiverMarkItem.telephone,
      }
      let result = validateService('validateEFormSave04ProcessStep7receiverMarkItem', this.validateObj)
      this.validate = result.validate
      return result.isValid
    }

    if (nameItem === "agenReceiverMarkItem") {
      this.validateObj = {
        receiver_type_codeAgenReceiver: this.input.agenReceiverMarkItem.receiver_type_code,
        card_numberAgenReceiver: this.input.agenReceiverMarkItem.card_number,
        nameAgenReceiver: this.input.agenReceiverMarkItem.name,
        sex_codeAgenReceiver: this.input.agenReceiverMarkItem.sex_code,
        nationality_codeAgenReceiver: this.input.agenReceiverMarkItem.nationality_code,
        career_codeAgenReceiver: this.input.agenReceiverMarkItem.career_code,
        address_country_codeAgenReceiver: this.input.agenReceiverMarkItem.address_country_code,
        house_numberAgenReceiver: this.input.agenReceiverMarkItem.house_number,
        address_sub_district_nameAgenReceiver: this.input.agenReceiverMarkItem.address_sub_district_name,
        postal_codeAgenReceiver: this.input.agenReceiverMarkItem.postal_code,
        emailAgenReceiver: this.input.agenReceiverMarkItem.email,
        telephoneAgenReceiver: this.input.agenReceiverMarkItem.telephone,
      }
      let result = validateService('validateEFormSave04ProcessStep7agenReceiverMarkItem', this.validateObj)
      this.validate = result.validate
      return result.isValid
    }

    return true
  }

  onClickCheckBoxTable2(object: any, value: boolean): void {
    //if (isAll) {
    //console.log(value)
    this[object].forEach((item: any) => {
      item.is_check = value
      //console.log(item)
    })
    //} else {
    //  object.forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } })
    //}
  }

  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    console.log("validate", this.validateSaveItemModal(nameItem))
    if (this.validateSaveItemModal(nameItem)) {
      console.log("indexEdit", this.input.indexEdit)
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        console.log("people_list", this[obj][nameList])
        this.toggleModal(nameModal);
      }

      alert("กรณีที่ทีการแก้ไขเปลี่ยนแปลงข้อมูลเจ้าของ/ตัวแทน ให้ยื่น ก.06 แนบมาด้วย");
    }
  }

  sendEmail(): void {
    this.global.setLoading(true);
    this.getParamsSave();
    
    console.log("SaveInput", this.saveInput);

    this.eFormSaveProcessService.eFormSave030Save(this.saveInput).subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
    })
  }

  onClickViewPdfKor03(): void {
    if (this.validateWizard()) {
      this.getParamsSave();
      console.log("SaveInput", this.saveInput);
      viewPDF("ViewPDF/TM03", this.saveInput);
    }
  }

  getParamsSave():void {
    console.log("SeeInput", this.input);
    this.saveInput = {};
    this.saveInput.id = this.response.load.id ? this.response.load.id : null;
    this.saveInput.eform_number = this.response.load.eform_number
        ? this.response.load.eform_number
        : null;
    this.saveInput.wizard = this.getWizard();
    this.saveInput.email = this.input.email;
    this.saveInput.telephone = this.input.telephone;
    this.saveInput.save030_search_type_code = this.input.save030_search_type_code;
    this.saveInput.register_date = this.input.request_date_text;
    this.saveInput.request_number = this.input.search_number;
    this.saveInput.register_number = this.input.registration_number;
    //this.saveInput.save040_transfer_type_code = this.input.save040_transfer_type_code;
    this.saveInput.people_list = [];
    this.saveInput.representative_list = [];
    this.saveInput.contact_address_list = [];
    //this.saveInput.receiver_people_list = [];
    //this.saveInput.receiver_representative_list = [];
    this.saveInput.save030_representative_condition_type_code = this.input.save030_representative_condition_type_code;
    this.saveInput.save030_contact_type_code = this.input.save030_contact_type_code;
    //this.saveInput.save040_receiver_contact_type_code = this.input.save040_receiver_contact_type_code;
    //this.saveInput.save040_transfer_form_code = this.input.save040_transfer_form_code;
    //this.saveInput.save040_transfer_part_code = this.input.save040_transfer_part_code;
    //this.saveInput.save040_receiver_contact_index = this.input.save040_receiver_contact_index;
    this.saveInput.appeal_order = this.input.appeal_order;
    //this.saveInput.receiver_contact_address_list = [];
    this.saveInput.is_8_1 = this.input.is_8_1;
    this.saveInput.is_8_2 = this.input.is_8_2;
    this.saveInput.is_8_3 = this.input.is_8_3;
    this.saveInput.is_8_4 = this.input.is_8_4;

    this.saveInput.kor_17_list = [];
    this.saveInput.payer_name = this.input.payer_name;
    this.saveInput.total_price = this.input.total_price;

    this.saveInput.contact_address_list.push({
      "name": this.contactAddress.name,
      "house_number": this.contactAddress.house_number,
      "village_number": this.contactAddress.village_number,
      "alley": this.contactAddress.alley,
      "street": this.contactAddress.street,
      "address_sub_district_name": this.contactAddress.address_sub_district_name,
      "postal_code": this.contactAddress.postal_code,
      "email": this.contactAddress.email,
      "telephone": this.contactAddress.telephone,
      "fax": this.contactAddress.fax,
    })

    this.input.listOwnerMark.forEach(obj => {
      this.saveInput.people_list.push({
        "address_type_code": obj.address_type_code,
        "nationality_code": obj.nationality_code,
        "career_code": obj.career_code,
        "card_type_code": obj.card_type_code,
        "card_type_name": obj.card_type_name,
        "card_number": obj.card_number,
        "receiver_type_code": obj.receiver_type_code,
        "name": obj.name,
        "house_number": obj.house_number,
        "village_number": obj.village_number,
        "alley": obj.alley,
        "street": obj.street,
        "address_sub_district_code": obj.address_sub_district_code,
        "address_district_code": obj.address_district_code,
        "address_province_code": obj.address_province_code,
        "postal_code": obj.postal_code,
        "address_country_code": obj.address_country_code,
        "telephone": obj.telephone,
        "fax": obj.fax,
        "email": obj.email,
        "sex_code": obj.sex_code,
        "is_contact_person": obj.is_contact_person
      })
    });

    this.input.listAgentMark.forEach(obj => {
      this.saveInput.representative_list.push({
        "address_type_code": obj.address_type_code,
        "nationality_code": obj.nationality_code,
        "career_code": obj.career_code,
        "card_type_code": obj.card_type_code,
        "card_type_name": obj.card_type_name,
        "card_number": obj.card_number,
        "receiver_type_code": obj.receiver_type_code,
        "name": obj.name,
        "house_number": obj.house_number,
        "village_number": obj.village_number,
        "alley": obj.alley,
        "street": obj.street,
        "address_sub_district_code": obj.address_sub_district_code,
        "address_district_code": obj.address_district_code,
        "address_province_code": obj.address_province_code,
        "postal_code": obj.postal_code,
        "address_country_code": obj.address_country_code,
        "telephone": obj.telephone,
        "fax": obj.fax,
        "email": obj.email,
        "sex_code": obj.sex_code,
        "is_contact_person": obj.is_contact_person
      })
    });

    this.saveInput.sign_inform_person_list = this.input.isCheckAllOwnerSignature ? "0" : "";
    this.saveInput.sign_inform_representative_list =
        this.input.isCheckAllAgentSignature &&
        this.input.save030_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform();
  }

  onClickeFormFromRequestNumberLoad(): void {
    // if(this.validateeFormFromRequestNumberLoad()) {
    // Open loading
    // Call api
    //this.calleFormFromRequestNumberLoad(this.input.request_number)
    // }
  }

  CheckRule(): void
  {
    let RuleSelect: string;
    this.instruction_rule_list.forEach(data => {
      if (data.is_check)
      {
        if (RuleSelect) {
          RuleSelect = RuleSelect + ", " + data.instruction_rule_name;
        } else
        {
          RuleSelect = data.instruction_rule_name;
        }
      }
    })

    this.input.appeal_order = RuleSelect;
    this.toggleModal("isModalAgentFormOpen");
  }

  //! <<< Call API >>>
  callSearchRequestNumber(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave030SearchRequestNumber(params).subscribe((data: any) => {
      this.loadConsideringSimilarDocument(data);
    })
  }

  callSearchChallengerNumber(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave030SearchChallengerNumber(params).subscribe((data: any) => {
      this.loadConsideringSimilarDocument(data);
    })
  }

  callSearchContractNumber(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave030SearcContractNumber(params).subscribe((data: any) => {
      this.loadConsideringSimilarDocument(data);
    })
  }

  loadConsideringSimilarDocument(data: any) {
    if (data && data.is_search_success) {
      // Set value
      this.eFormSaveProcessService.eFormSave030LoadConsideringSimilarDocument(data.save010.id).subscribe((considering_similar_document: any) => {
        if (considering_similar_document && considering_similar_document.instruction_rule_list)
          this.instruction_rule_list = considering_similar_document.instruction_rule_list

        this.instruction_rule_list = this.instruction_rule_list || []
        console.log("rule", this.instruction_rule_list);
        this.seachData = data;
        this.automateTest.test(this, { list: data.list })
        this.input.telephone = this.telephone;
        this.input.email = this.email;

        this.input.save030_change_06_type_code = null;
        this.input.save030_contact_type_code = null;
        this.input.is_8_1 = true;
        this.input.is_8_2 = true;

        this.input.request_date_text = this.seachData.request_date_text;
        this.input.request_number = this.seachData.save010.request_number;
        if (this.seachData.save010.people_list.length > 0)
        {
          this.firstOwnerName = this.seachData.save010.people_list[0].name;
          this.firstOwnerTel = this.seachData.save010.people_list[0].telephone;
        }
        this.input.registration_number = this.seachData.registration_load_number;
        this.input.load_receiver_people_type_code = this.seachData.load_receiver_people_type_code;      
        this.input.listOwnerMark = this.seachData.people_load_list;
        this.input.listAgentMark = this.seachData.representative_load_list;
        if (this.input.listOwnerMark.length > 0){
          this.input.payer_name = this.input.listOwnerMark[0].name;
        }
      })
    }
    else{
      console.warn(`request_number is invalid.`);
      this.input.request_date_text = "";
      this.validate.request_number = data.alert_msg;
      this.togglePopup("isSearch");
    }

    this.global.setLoading(false)
  }

  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }

  save(isOverwrite: boolean): void {
    //Open loading
    this.global.setLoading(true);
    // Set param
    this.getParamsSave();
    if (!isOverwrite) {
      this.saveInput = getParamsOverwrite(this.saveInput);
    }

    //Call api
    this.calleFormSave030Save(this.saveInput);
  }

  //! <<< Call API >>>
  calleFormSave030Save(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave030Save(params).subscribe((data: any) => {
      if (data) {
        this.isDeactivation = true;
        // Open toast success
        let toast = CONSTANTS.TOAST.SUCCESS;
        toast.message = "บันทึกข้อมูลสำเร็จ";
        this.global.setToast(toast);
        // Navigate
        this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
      }else {
        // Close loading
        this.global.setLoading(false);
      }      
    })
  }

  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.eFormSaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  clearAllValidate(): void {
    this.validate = {};
  }

  loadData(data: any): void {
    this.input = data;
  }

  listData(data: any): void {
    this.input = data;
  }

  saveData(): any {
    let params = this.input


    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    this.currentPage = page;
    this.calOnchangepageMain();
  }
  onChangePerPage(value: number, name: string): void {
    this.perpage = value;
    this.calOnchangepageMain();
  }

  calOnchangepageMain() {
    this.total = Math.ceil(this.instruction_rule_list.length / this.perpage);
  }
  //onChangePerPage(value: number, name: string): void {
  //  this[name].itemsPerPage = value
  //}
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }

  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalSectionOpen") {
          this.input.agentMarkItem = clone(this.input.listAgentMark[index]);
        }
      }

      if (this.currentID == 5)
      {
        this.perpage = 5;
        this.calOnchangepageMain();
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
    }
  }

  getWizard(): String {
    if (this.editID) {
      let wizardList = this.response.load.wizard.split("|");
      let sum = 0;
      let sumNow =
        this.currentID +
        this.currentStep +
        this.currentSubID +
        this.currentSubStep;

      wizardList.forEach((item: any) => {
        sum = +sum + +item;
      });

      if (sumNow > sum) {
        return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
      } else {
        return this.response.load.wizard;
      }
    } else {
      return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
    }
  }

  getSignInform(): String {
    let result = "";
    this.input.listAgentMark.forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input.listAgentMark.forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save030_representative_condition_type_code != "AND") {
        this.input.listAgentMark.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }

  //! <<< Wizard >>>
  changeStep(action: string): void {
    if (this.validateWizard()) {
      this.menuList[this.currentStep - 1].canedit = true;

      action === "next" ? this.currentStep++ : this.currentStep--;
      this.currentID = this.menuList[this.currentStep - 1].id;
      this.currentSubStep = 1;
      if (this.stepPass == (this.currentStep - 1))
        this.stepPass = this.stepPass + 1;

      this.calcProgressPercent(this.currentStep);
    }
  }

  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentStep === 1) {
      this.validateObj = { email: this.input.email, telephone: this.input.telephone }
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.validateObj
      );
      this.validate = result.validate;

      this.email = this.input.email;
      this.telephone = this.input.telephone;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.input.id
      ) {
        this.sendEmail();
      }

      // Check certification mark
      if (result.isValid) {
        if (this.input.request_item_type_code !== "CERTIFICATION_MARK") {
          this.menuList.splice(12, 1)
          this.menuList.forEach((item: any, index: any) => {
            item.number = index + 1
          });
        }
      }

      return result.isValid;
    }

    return true;
  }

  manageWizard(wizard: any): void {
    let wizardList = wizard ? wizard.split("|") : [1, 1, 1, 1];
    this.currentID = +wizardList[0];
    this.currentStep = +wizardList[1];
    this.currentSubID = +wizardList[2];
    this.currentSubStep = +wizardList[3];
    this.calcProgressPercent(this.currentStep);

    this.menuList.forEach((item: any) => {
      if (this.currentID > item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            sub.canEdit = true;
          });
        }
      }
      if (this.currentID === item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (this.currentSubID >= sub.id) {
              sub.canEdit = true;
            }
          });
        }
      }
    });
  }

  searchNumber(): void {
    if(this.input.save030_search_type_code === "REQUEST_NUMBER"){
      this.callSearchRequestNumber(this.input.search_number);
    }else if(this.input.save030_search_type_code === "CHALLENGER_NUMBER") {
      this.callSearchChallengerNumber(this.input.search_number);
    }else{
      this.callSearchContractNumber(this.input.search_number);
    }
  }

  calcProgressPercent(currentStep: number): void {
    this.progressPercent = Math.round((currentStep / this.menuList.length) * 100);
  }

  findClick(): void {
    this.findVal = true;
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any, item: any): void {
    if (item) {
      this[obj][item][name] = value;
    } else {
      this[obj][name] = value;
    }
  }

  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name];
  }

  toggleBooleanInTable(item: any, name: any): void {
    item[name] = !item[name];
  }

  backtomenu(canedit: any, number: any): void {
    console.log(canedit);
    if (canedit == true) {
      this.currentStep = number;
      this.currentID = this.menuList[this.currentStep - 1].id

      for (var i = 1; i <= this.menuList.length + 1; i++) {
        if (i <= this.stepPass - 1) {
          this.menuList[i].canedit = true;
        }
      }
    }
  }

  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
  }

  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }

  displayAddress(value: any): any {
    return displayAddress(value);
  }

  //! <<< Modal Eform >>>
 onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
  if (this.input.indexEdit >= 0) {
    // Is Edit
    this.input[nameList][this.input.indexEdit] = item;
    this.toggleModal(nameModal);
  } else {
    // Is Add
    this.input[nameList].push(item);
    this.toggleModal(nameModal);
  }

  if (nameList === "listOwnerMark" || nameList === "listAgentMark") {
    this.togglePopup("isWarning");
    this.clearAllValidate();
  }
}

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

 //! List Evidence
 onClickAddEvidence(): void {
  this.input.listEvidence.push({
    evidence_type: "",
    evidence_subject: "",
    number: "",
    note: "",
  });
 }

 onClickRemoveEvidence(index: number): void {
  this.input.listEvidence.splice(index, 1);
 }

  onChangeSearchType(): void {
    this.input.request_date_text = '';
    this.input.listOwnerMark = [];
    this.input.listAgentMark = [];
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }
}
