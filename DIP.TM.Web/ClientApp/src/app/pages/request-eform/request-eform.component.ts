import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { RequestProcessService } from '../../services/requestprocess-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-request-eform",
    templateUrl: "./request-eform.component.html",
    styleUrls: ["./request-eform.component.scss"]
})
export class RequestEformComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    public tableRequest: any
    public tableRequestItem: any
    public tableRequestOther: any

    public model: any
    public requestItem: any

    public popup: any
    public url: any

    public player: any
    public player_action: any
    //label_save_combobox || label_save_radio
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            request_date: getMoment(),
            created_by_name: this.auth.getAuth().name,

            //eform_number_other_add: 2006000247,
        }

        this.popup = {
            row_item_edit: {}
        }

        this.tableRequest = {
            column_list: {
                index: "#",
                item_type_name: "ประเภท",
                income_type: "รหัสรายได้",
                income_description: "รายการรับเงิน",
                request_number: "เลขที่คำขอ",
                size_over_cm: "ส่วนเกิน ซม.",
                request_01_item_sub_count: "จำนวนจำพวก",
                request_01_product_count: "จำนวนสินค้า",
                total_price: "จำนวนเงิน",
                is_otop: "OTOP",
                reference_number: "เลขอ้างอิง",
                cancel_reason: "การยกเลิก",
                facilitation_act_status_code: "พรบอำนวย",
            },
            column_edit_list: {
                is_otop: { type: "checkbox", },
                facilitation_act_status_code: { type: "combo_box", master: "facilitationActStatusCodeList", },
            },
            button_list: [{
                //    name: "request-item-split",
                //    title: "แยกเลขที่คำขอ",
                //    icon: "layer",
                //}, {
                name: "request-item-send",
                title: "บันทึก / ตรวจสอบเอกสารอ้างอิง",
                icon: "printer",
            },],
            command_item: [{
                name: "request-item-edit",
                icon: "edit-pen",
            }],
            can_deleted: true,
        }

        this.tableRequestItem = {
            column_list: {
                index: "#",
                request_item_sub_type_1_code: "รหัสจำพวก",
                product_count: "จำนวนสินค้า",
                total_price: "จำนวนเงิน",
            },
        }


        this.tableRequestOther = {
            column_list: {
                index: "#",
                request_number: "เลขที่คำขอ",
                save_total_price: "ค่าธรรมเนียม (ก.01)",
                reference_number: "เลขที่อ้างอิง",
                name: "เจ้าของ",
                registration_number: "เลขทะเบียน",
                index_description: "ลำดับ",
                remark: "หมายเหตุ",
                request_type_code: "รหัสรายการ",
                request_type_name: "รายการรับเงิน	",
                item_sub_type_1_count: "จำนวนจำพวก",
                product_count: "จำนวนสินค้า",
                total_price: "จำนวนเงิน",
                fine: "ค่าปรับ",
                is_evidence_floor7: "หลักฐาน (ชั้น 7)",
                //request_type_code: "รายการสินค้า",
                //request_type_code: "",
                //request_type_code: "",
            },
            column_edit_list: {
                is_evidence_floor7: { type: "checkbox", },
                //    facilitation_act_status_code: { type: "combo_box", master: "facilitationActStatusCodeList", },
            },
            //button_list: [{
            //    //    name: "request-item-split",
            //    //    title: "แยกเลขที่คำขอ",
            //    //    icon: "layer",
            //    //}, {
            //    name: "request-item-send",
            //    title: "บันทึก / ตรวจสอบเอกสารอ้างอิง",
            //    icon: "printer",
            //},],
            //command_item: [{
            //    name: "request-item-edit",
            //    icon: "edit-pen",
            //}],
            can_deleted: true,
        }

        //Master List
        this.master = {
        }
        //Master List

        this.model = {

        }
        this.requestItem = {}


        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //


        this.contactAddress = {
            address_type_code: "OWNER"
        }
        this.modalAddress = {}

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.route.queryParams.subscribe((param_url: any) => {
        })

        this.forkJoinService.initRequestEform().subscribe((data: any) => {
            if (data) {
                this.help.Clone(data, this.master)

            }
            if (this.editID) {
                this.RequestProcessService.Request01Load(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.tableRequest.Set([])

                this.global.setLoading(false)
                this.automateTest.test(this)
            }

            //this.onChangeEFormNumberOtherAdd()
        })
    }

    constructor(
        private auth: Auth,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private RequestProcessService: RequestProcessService
    ) { }

    onClickCommand($event): void {
        console.log($event)

        if ($event.command == "request-item-edit") {
            this.popup.is_row_item_edit_show = true

            var param = {
                eform_id: $event.object_list[0].eform_id,
            }
            this.global.setLoading(true)
            this.RequestProcessService.List("EForm_eForm_Save010_Product", this.help.GetFilterParams(param)).subscribe((data: any) => {
                if (data && data.list) {
                    data.list.forEach((item: any) => {
                        item.total_price = item.product_count > 5 ? 9000 : item.product_count * 1000
                    })
                    this.tableRequestItem.Set(data.list)

                    this.popup.row_item_edit = $event.object_list[0]

                    if (this.popup.row_item_edit.sound_file_physical_path && this.popup.row_item_edit.sound_file_physical_path != '') {
                        this.player = new Audio(this.popup.row_item_edit.sound_file_physical_path)
                        this.player_action = null
                    }

                    console.log(this.popup.row_item_edit)
                }
                // Close loading
                this.global.setLoading(false)
            })
        } else if ($event.command == "request-item-send") {
            this.global.setLoading(true)
            var input_param = {
                ...this.input,
                item_list: this.tableRequest.Get(),
                //item_other_list: this.tableRequestOther.Get(),
            }
            this.RequestProcessService.RequestEformSend(input_param).subscribe((data: any) => {
                if (data) {
                    console.log(data)
                    this.input = data
                    this.tableRequest.Set(data.item_list)

                    if (data.item_list.length > 0) {
                        window.open('/pdf/Receipt01Reference/' + data.item_list[0].reference_number)
                    }

                    //this.url = this.sanitizer.bypassSecurityTrustResourceUrl('/pdf/ReceiptEFormReference/' + data.request01_item_list[0].reference_number)
                    //this.toggleModal('isModalPreviewPdfOpen')
                    //    data.list.forEach((item: any) => {
                    //        item.total_price = item.product_count > 5 ? 9000 : item.product_count * 1000
                    //    })
                    //this.tableRequestItem.Set(data.list)

                    //    this.popup.row_item_edit = $event.object_list[0]

                    //    if (this.popup.row_item_edit.sound_file_physical_path && this.popup.row_item_edit.sound_file_physical_path != '') {
                    //        this.player = new Audio(this.popup.row_item_edit.sound_file_physical_path)
                    //        this.player_action = null
                    //    }

                    //    console.log(this.popup.row_item_edit)
                }
                // Close loading
                this.global.setLoading(false)
            })
        }
    }

    onClickWaveFree(is_wave_fee): void {
        console.log(is_wave_fee)
        this.input.is_wave_fee = is_wave_fee
    }

    onChangeEFormNumberAdd(event = null): void {
        if (event) {
            if (event.code != "Enter") {
                return;
            }
        }

        var param = {
            eform_number: this.input.eform_number_add.toString(),
        }
        this.global.setLoading(true)
        this.RequestProcessService.List("EForm_eForm_Save010", this.help.GetFilterParams(param)).subscribe((data: any) => {
            if (data && data.list && data.list.length > 0) {
                this.tableRequest.Add(data.list[0])
                // Manage structure
                //this.loadData(data)
            }
            // Close loading
            this.global.setLoading(false)
            this.automateTest.test(this)

            //this.onClickCommand({
            //    command: "request-item-send",
            //    object_list: data.list,
            //})
        })

        this.input.eform_number_add = ""
    }


    onChangeEFormNumberOtherAdd(event = null): void {
        if (event) {
            if (event.code != "Enter") {
                return;
            }
        }

        var param = {
            eform_number: this.input.eform_number_other_add.toString(),
        }
        this.global.setLoading(true)
        this.RequestProcessService.List("EForm_eForm_SaveOther", this.help.GetFilterParams(param)).subscribe((data: any) => {
            if (data && data.list && data.list.length > 0) {
                data.list[0].remark = ""
                if (data.list[0].trademark_expired_date) {
                    data.list[0].remark += "วันสิ้นอายุ: " + data.list[0].trademark_expired_date_text
                }
                this.tableRequestOther.Add(data.list[0])
                // Manage structure
                //this.loadData(data)
            }
            // Close loading
            this.global.setLoading(false)
            this.automateTest.test(this)

            this.onClickCommand({
                command: "request-item-send",
                object_list: data.list,
            })
        })

        this.input.eform_number_other_add = ""
    }

    autoCompleteUpdateBack($event, name: any): void {
        console.log("autoCompleteUpdateBack")
        console.log($event)
        console.log(name)

        if (name == "RequestAgency") {
            this.input.requester_name = $event.item.name
            this.onClickWaveFree($event.item.is_wave_fee)
            //this.input
            //    this.documentClassificationImageList.push({
            //        document_classification_image_code: $event.item.code,
            //        document_classification_image_name: $event.item.name,
            //    })
            //} else if (name == "search_1_document_classification_sound_code") {
            //    this.documentClassificationSoundList.push({
            //        document_classification_sound_code: $event.item.code,
            //        document_classification_sound_name: $event.item.name,
            //    })
        }
    }

    autoCompleteUpdateFocusout($event, name: any): void {
        console.log("autoCompleteUpdateFocusout")
        console.log($event)
        console.log(name)

        if (name == "RequestAgency") {
            this.input.requester_name = $event
        }
    }



    togglePlayer(): void {
        if (this.player_action) {
            this.player.pause()
        } else {
            this.player.play()
        }
        this.player_action = !this.player_action
    }

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        //data.item_list.forEach((item: any) => {
        //    item.total_price = item.product_count > 5 ? 9000 : item.product_count * 1000
        //})
        this.tableRequest.Set(data.item_list)

        //this.popup.row_item_edit = $event.object_list[0]

        //if (this.popup.row_item_edit.sound_file_physical_path && this.popup.row_item_edit.sound_file_physical_path != '') {
        //    this.player = new Audio(this.popup.row_item_edit.sound_file_physical_path)
        //    this.player_action = null
        //}
    }

    listData(data: any): void {

    }

    saveData(): any {
        let params = this.input


        return params
    }

    reset(): any {
        this.callInit()
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }
    displayMoney(value: any): any {
        return displayMoney(value);
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

}
