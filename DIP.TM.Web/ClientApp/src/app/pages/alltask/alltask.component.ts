import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'


@Component({
  selector: "app-alltask",
  templateUrl: "./alltask.component.html",
  styleUrls: ["./alltask.component.scss"]
})
export class AllTaskComponent implements OnInit {

  //TODO >>> Declarations <<<
  public modal: any

  constructor(
    private router: Router,
    private global: GlobalService
  ) { }

  ngOnInit() {
    //TODO >>> Init value <<<
    this.modal = {
      isModalTransferJobOpen: true
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
    } else {
      // Is close
      this.modal[name] = false;
    }
  }

}
