import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appeal-role03-board-decision-summary-list',
  templateUrl: './appeal-role03-board-decision-summary-list.component.html',
  styleUrls: ['./appeal-role03-board-decision-summary-list.component.scss']
})
export class AppealRole03BoardDecisionSummaryListComponent implements OnInit {

  constructor() { }

  public tab_search: any
  public tab_show: any


  ngOnInit() {
    this.tab_search = true
    this.tab_show = false
  }

  toggle_show(){
    switch(this.tab_search){
      case true: 
        this.tab_search = false; this.tab_show = true
      break;
      case false:
        this.tab_search = true; this.tab_show = false
      break;
    }
  }

}
