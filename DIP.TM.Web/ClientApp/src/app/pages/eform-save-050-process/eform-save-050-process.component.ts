import { Component, OnInit, HostListener } from "@angular/core";
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from '../../services/fork-join2.service'
import { eFormSaveProcessService } from '../../services/eform-save-process.service'
import { AutomateTest } from '../../test/automate_test'
import { ActivatedRoute, Router } from '@angular/router';

import {
  CONSTANTS,
  ROUTE_PATH,
  validateService,
  clone,
  viewPDF,
  displayAddress,
  getParamsOverwrite,
} from '../../helpers'

@Component({
  selector: "app-eform-save-050-process",
  templateUrl: "./eform-save-050-process.component.html",
  styleUrls: ["./eform-save-050-process.component.scss"]
})
export class eFormSave050ProcessComponent implements OnInit, DeactivationGuarded {

  //public contact_address_list: any
  public fileReaded: any;
  public csv: any;
  public productList: any[];
  public inputFileStep7: boolean;
  public typeS2: any;


  //TODO >>> Declarations <<</'
  public editID: any;
  public saveInput: any;
  public input: any;
  public seachData: any;
  public validate: any;
  public master: any;
  public validateObj: any;
  public contact_address_list: any;
  public contact_index: number;
  // Response
  public response: any
  public menuList: any[];

  public autocompleteListListModalLocation: any;
  public is_autocomplete_ListModalLocation_show: any;
  public is_autocomplete_ListModalLocation_load: any;
  public inputModalAddress: any;
  public inputModalAddressEdit: any;
  public modal: any;
  public inputAddress: any;
  public conjunction: any;
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubstep: number;
  public progressPercent: number;
  public currentMiniStep: number;

  public findVal: any;
  public currentSubStep: number;

  public listdropdownContact: any;
  public ContactWord: any;
  public contactChangeType: string;

  public currentP1: number;
  public perpageP1: number;
  public totalP1: number;

  public currentP2: number;
  public perpageP2: number;
  public totalP2: number;

  public type1: any;
  public type2: any;
  public type3: any;

  public Next3: boolean;
  public Next6: boolean;
  public Next8: boolean;

  public popup: any;
  public itemList: any[];

  public checkdateContract: boolean;
  public stepPass: number;
  public telephone: string;
  public email: string;
  public rule_number: string;

  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  ngOnInit() {
    this.inputFileStep7 = false;
    this.checkdateContract = false;
    this.Next3 = false;
    this.Next6 = false;
    this.Next8 = false;

    this.contact_address_list = {}

    this.menuList = this.loadMenuList();

    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
      isSearch: false,
      isWarning: false,
    };

    this.response = {
      load: {},
    };

    this.input = {
      save050_representative_condition_type_code: "AND_OR",
      indexEdit: undefined,
      point: "",
      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listReceiverMark: [],
      receiverMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      listAgentReceiverMark: [],
      agenReceiverMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      ownerDomesticContactAddress: 0,
      agentDomesticContactAddress: 0,
      subagentDomesticContactAddress: 0,
      dimension_image: 2,
      imageDimension2: {
        file: {},
        blob: null
      },
      imageDimension3_1: {
        file: {},
        blob: null
      },
      imageDimension3_2: {
        file: {},
        blob: null
      },
      imageDimension3_3: {
        file: {},
        blob: null
      },
      imageDimension3_4: {
        file: {},
        blob: null
      },
      sizeX: 5,
      sizeY: 5,
      isResize: false,
      audio: {
        input: "",
        validate: "",
        file: {},
        sound: null,
        palyer: null,
        action: false
      },
      imageNote: {
        file: {},
        blob: null
      },
      listTranslation: [],
      translationItem: {},
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0
      },
      imageMark: {
        file: {},
        blob: null
      },
      email: "",
      telephone: "",
      register_date: "",
      request_number: null,
      register_number: "",
      search_number: "",
      //request_item_type_code: "",
      save050_submit_type_code: null,
      rule_number: "",
      save050_allow_type_code: null,
      save050_people_type_code: null,
      people_list: [],
      representative_list: [],
      save050_receiver_people_type_code: null,
      receiver_people_list: [],
      receiver_representative_list: [],
      save050_contact_type_code: null,
      //contact_address_list: [],
      save050_transfer_form_code: null,
      contract_date: null,
      contract_start_date: null,
      is_contract: false,
      contract_end_date: null,
      contract_conduration: null,
      remark_7_2: null,
      remark_7_3: null,
      is_7_4_1: false,
      is_7_4_2: false,
      is_7_4_3: false,
      is_7_4_4: false,
      save050_extend_type_code: null,
      is_9_1: false,
      is_9_2: false,
      is_9_3: false,
      payer_name: "",
      total_price: "",
      search_type_code: "REQUEST_NUMBER"
    };

    this.input.people_list = []

    this.input.representative_list = []
    this.input.receiver_people_list = []
    this.input.receiver_representative_list = []

    this.validate = {};
    this.master = {
      requestSoundTypeList: [],
      inputTypeCodeList: [],
      requestSourceCodeList: [],
      saveOTOPTypeCodeList: [],
      addressCountryCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressRepresentativeConditionTypeCodeList: [],
      addressSexCodeList: [],
      languageCodeList: [],
      priceMasterList: [],
      save050SearchTypeCodeList: []
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalDomesticContactAddressOpen: true
    };


    this.itemList = []
    // Wizard

    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubStep = 1;
    this.currentMiniStep = 1;
    this.progressPercent = 0;
    this.stepPass = 1;



    this.currentP2 = 1;
    this.perpageP2 = 2;

    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}

    this.contact_address_list = {};

    this.modal = {
    }





    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //
    // Other
    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;

    this.callInit()
  }

  onChangePage(page: any, name: string): void {
    this.currentP1 = page;
    this.calOnchangepage();
  }
  onChangePerPage(value: number, name: string): void {
    this.perpageP1 = value;
    this.calOnchangepage();
  }

  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save050_representative_condition_type_code != "AND") {
        this.input.listAgentMark.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }

  //calOnchangepage() {
  //  this.totalP1 = Math.ceil(this.itemList.length / this.perpageP1);
  //}

  calOnchangepage() {
    this.totalP1 = Math.ceil(this.productList.length / this.perpageP1);
  }
  onChangePage2(page: any, name: string): void {
    this.currentP2 = page;
    console.log(this.currentP2);
    this.calOnchangepage2();
  }
  onChangePerPage2(value: number, name: string): void {
    this.perpageP2 = value;
    this.calOnchangepage2();
  }
  calOnchangepage2() {
    this.totalP2 = Math.ceil(this.itemList.length / this.perpageP2);
    console.log(this.totalP2);
  }

  onClickcheckdateContract(): void {
    this.checkdateContract = !this.checkdateContract;
    console.log("value = " + this.checkdateContract)
  }
  onClickViewPdfA18(): void {
    if (this.validateWizard()) {
      window.open("/pdf/tm18");
    }
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //callInit(): void {
  //  //this.global.setLoading(true)
  //  this.forkJoinService.initeFormSave050Process().subscribe((data: any) => {
  //    if (data) {
  //      this.master = data
  // }
  //    if (this.editID) {
  //      this.eFormSaveProcessService.eFormSave050Load(this.editID).subscribe((data: any) => {
  //        if (data) {
  //          // Manage structure
  //          this.loadData(data)
  //        }
  //        // Close loading
  //        this.global.setLoading(false)
  //        this.automateTest.test(this)
  //      })
  //    } else {
  //      this.global.setLoading(false)
  //      this.automateTest.test(this)
  //    }

  //  })
  //}

  callInit(): void {
    this.global.setLoading(true)

    this.forkJoinService.initeFormSave050Process().subscribe((data: any) => {
      if (data) {
        this.master = data;
        data.requestTypeList.forEach((item: any) => {
          if (item.code === "50" && !this.editID) {
            this.input.total_price = item.value_2;
          }
        });
        console.log(this.master, "master")
      }

      console.log("ID", this.editID)
      if (this.editID) {
        this.eFormSaveProcessService.eFormSave050Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data);
            this.response.load = {
              eform_number: data.eform_number,
              id: data.id,
              wizard: data.wizard,
            };

            this.input.search_number = data.request_number;
            this.input.listOwnerMark = data.people_list;
            this.input.listAgentMark = data.representative_list;
            this.input.listReceiverMark = data.receiver_people_list;
            this.input.listAgentReceiverMark = data.receiver_representative_list;

            this.input.save050_representative_condition_type_code = data.save050_representative_condition_type_code
              ? data.save050_representative_condition_type_code
              : "AND_OR";
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  contactTypeChange(): void {
    //this.Next4 = true;
    switch (this.input.save050_contact_type_code) {
      case "OWNER":
        this.listdropdownContact = this.input.listOwnerMark;
        this.ContactWord = "ใช้ที่อยู่ของเจ้าของลำดับที่";
        this.contactChangeType = "OWNER";
        break;
      case "REPRESENTATIVE":
        this.listdropdownContact = this.input.listAgentMark;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนลำดับที่"
        this.contactChangeType = "REPRESENTATIVE";
        break;
      case "REPRESENTATIVE_PERIOD":
        this.listdropdownContact = this.input.listAgentMark;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนช่วงลำดับที่"
        this.contactChangeType = "REPRESENTATIVE_PERIOD";
        break;
      case "RECEIVER":
        this.listdropdownContact = this.input.listReceiverMark;
        this.ContactWord = "ใช้ที่อยู่ของผู้ได้รับอนุญาตลำดับที่";
        this.contactChangeType = "RECEIVER";
        break;
      case "RECEIVER_REPRESENTATIVE":
        this.listdropdownContact = this.input.listAgentReceiverMark;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนผู้ได้รับอนุญาตลำดับที่";
        this.contactChangeType = "RECEIVER_REPRESENTATIVE";
        break;
      case "RECEIVER_REPRESENTATIVE_PERIOD":
        this.listdropdownContact = this.input.listAgentReceiverMark;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนช่วงผู้ได้รับอนุญาตลำดับที่";
        this.contactChangeType = "RECEIVER_REPRESENTATIVE_PERIOD";
        break;
      default:
        break;
    }

    console.log(this.contact_index);

    this.contact_index = 0;
    this.contactChange(this.input.save050_contact_type_code, this.contact_index);
  }

  contactChange(contactType: string, index: number): void {
    console.log("index", index)

    this.input.listOwnerMark.forEach(data => {
      data.is_contact_person = false;
    });

    this.input.listAgentMark.forEach(data => {
      data.is_contact_person = false;
    });

    this.input.listReceiverMark.forEach(data => {
      data.is_contact_person = false;
    });

    this.input.listAgentReceiverMark.forEach(data => {
      data.is_contact_person = false;
    });

    if (contactType == "OWNER") {
      this.input.listOwnerMark[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE") {
      this.input.listAgentMark[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE_PERIOD") {

      this.input.listAgentMark[index].is_contact_person = true;
    }

    if (contactType == "RECEIVER") {
      this.input.listReceiverMark[index].is_contact_person = true;
    }

    if (contactType == "RECEIVER_REPRESENTATIVE") {
      this.input.listAgentReceiverMark[index].is_contact_person = true;
    }

    if (contactType == "RECEIVER_REPRESENTATIVE_PERIOD") {

      this.input.listAgentReceiverMark[index].is_contact_person = true;
    }


    console.log(this.input.listOwnerMark[index].is_contact_person);
    console.log(this.input.listAgentMark[index].is_contact_person);
  }

  constructor(
    private router: Router,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eFormSaveProcessService: eFormSaveProcessService
  ) { }



  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  openInput(): void {
    console.log(">>>>>>>")
    let element: any;
    element = document.getElementById('fileInput') as HTMLElement;
    element.click();
  }

  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.eFormSaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave05ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string): void {
    this.validate[name] = null;
  }

  loadData(data: any): void {
    this.input = data


  }

  listData(data: any): void {
    this.input = data
  }

  saveData(): any {
    let params = this.input


    return params
  }

  getLanguageLabel(code: any): string {
    let label = "";
    this.master.languageCodeList.forEach((item: any) => {
      if (item.code === code) {
        label = item.name;
      }
    });
    return label;
  }


  findClick(): void {
    this.findVal = true;
  }
  //calcProgressPercent(currentStep: number): void {
  //  this.progressPercent = Math.round((currentStep / this.menuList.length) * 100);
  //}
  //changeStep(action: string): void {
  //  //if (this.validateWizard()) {
  //  this.menuList[this.currentStep - 1].canedit = true;
  //  action === "next" ? this.currentStep++ : this.currentStep--;
  //  this.currentID = this.menuList[this.currentStep - 1].id
  //  this.currentSubstep = 1;
  //  this.calcProgressPercent(this.currentStep);
  //  //}
  //}

  changeStep(action: string): void {
    if (this.validateWizard()) {
      this.menuList[this.currentStep - 1].canedit = true;

      action === "next" ? this.currentStep++ : this.currentStep--;
      this.currentID = this.menuList[this.currentStep - 1].id
      this.currentSubstep = 1;
      if (this.stepPass == (this.currentStep - 1))
        this.stepPass = this.stepPass + 1;
      console.log("currentID", this.currentID, "currentStep", this.currentStep, "stepPass", this.stepPass)
      this.calcProgressPercent(this.currentStep);

      if(this.currentID == 5) {
        this.loadNewMenuForAllowTypeCode();
      }

      if (this.currentID == 9) {

        if (this.input.is_9_1 != true) {
          this.input.is_9_1 = false;
        }
        if (this.input.is_9_2 != true) {
          this.input.is_9_2 = false;
        }
        if (this.input.is_9_3 != true) {
          this.input.is_9_3 = false;
        }
      }
    }
    //}
  }

  loadMenuList() {
    return [
      {
        id: 1,
        number: 1,
        name: 'บันทึกเพื่อรับไฟล์',
        required: true,
        canedit: false
      },
      {
        id: 2,
        number: 2,
        name: 'คำขอจดทะเบียนสัญญาอนุญาตคำขอเลขที่',
        required: true,
        canedit: false
      },
      {
        id: 3,
        number: 3,
        name: 'ประเภทการยื่น',
        canedit: false
      },
      {
        id: 4,
        number: 4,
        name: 'ประเภทคำขอ',
        required: true,
        canedit: false
      },
      {
        id: 5,
        number: 5,
        name: 'เจ้าของ/ผู้ได้รับอนุญาต/ตัวแทน',
        canedit: false
      },
      {
        id: 6,
        number: 6,
        name: 'ผู้ได้รับอนุญาต/ผู้ได้รับอนุญาตช่วง/ตัวแทน',
        canedit: false
      },
      {
        id: 7,
        number: 7,
        name: 'สถานที่ติดต่อภายในประเทศ',
        canedit: false
      },
      {
        id: 8,
        number: 8,
        name: 'รายการจดทะเบียนสัญญาอนุญาตให้ใช้เครื่องหมายการค้า/บริการ',
        canedit: false
      },
      {
        id: 9,
        number: 9,
        name: 'การขอต่ออายุทะเบียนสัญญาอนุญาต',
        canedit: false
      },
      {
        id: 10,
        number: 10,
        name: 'เอกสารหลักฐานประกอบคำขอจดทะเบียนสัญญาอนุญาตให้ใช้สิทธิ',
        canedit: false
      },
      {
        id: 11,
        number: 11,
        name: 'ค่าธรรมเนียม',
        canedit: false
      },
      {
        id: 12,
        number: 12,
        name: 'เสร็จสิ้น',
        canedit: false
      },
    ]
  }

  loadNewMenuForAllowTypeCode(): void {
    if(this.input.save050_allow_type_code === "EXTEND"){
      this.menuList=this.loadMenuList();
      for(let i=0;i<this.currentStep;i++){
        this.menuList[i].canedit=true;
      }
    } else {
      let index = 8
      if(this.menuList[index].id == 9){
        this.menuList.splice(index, 1);

        for (let i = index; i < this.menuList.length; i++) {
          this.menuList[i].number = this.menuList[i].number - 1;
        }
      }
    }
  }

  searchNumber(): void {
    if(this.input.search_type_code === "REQUEST_NUMBER"){
      this.calleFormFromRequestNumberLoad(this.input.search_number);
    } else{
      this.calleFormFromContractNumberLoad(this.input.search_number);
    }
  }

  //! <<< Call API >>>
  calleFormFromRequestNumberLoad(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave050SearchRequestNumber(params).subscribe((data: any) => {
      console.log('Search', data);
      this.loadConsideringSimilarDocument(data);
      this.setValueStep4('OWNER');

      // if (!this.input.registration_number) {
      //   this.menuList = [
      //     {
      //       id: 1,
      //       number: 1,
      //       name: 'บันทึกเพื่อรับไฟล์',
      //       required: true,
      //       canedit: false
      //     },
      //     {
      //       id: 2,
      //       number: 2,
      //       name: 'คำขอจดทะเบียนสัญญาอนุญาตคำขอเลขที่',
      //       required: true,
      //       canedit: false
      //     },
         
      //     {
      //       id: 4,
      //       number: 3,
      //       name: 'ประเภทคำขอ',
      //       required: true,
      //       canedit: false
      //     },
      //     {
      //       id: 5,
      //       number: 4,
      //       name: 'เจ้าของ/ผู้ได้รับอนุญาต/ตัวแทน',
      //       canedit: false
      //     },
      //     {
      //       id: 6,
      //       number: 5,
      //       name: 'ผู้ได้รับอนุญาต/ผู้ได้รับอนุญาตช่วง/ตัวแทน',
      //       canedit: false
      //     },
      //     {
      //       id: 7,
      //       number: 6,
      //       name: 'สถานที่ติดต่อภายในประเทศ',
      //       canedit: false
      //     },
      //     {
      //       id: 8,
      //       number: 7,
      //       name: 'รายการจดทะเบียนสัญญาอนุญาตให้ใช้เครื่องหมายการค้า/บริการ',
      //       canedit: false
      //     },
      //     {
      //       id: 9,
      //       number: 8,
      //       name: 'การขอต่ออายุทะเบียนสัญญาอนุญาต',
      //       canedit: false
      //     },
      //     {
      //       id: 10,
      //       number: 9,
      //       name: 'เอกสารหลักฐานประกอบคำขอจดทะเบียนสัญญาอนุญาตให้ใช้สิทธิ',
      //       canedit: false
      //     },
      //     {
      //       id: 11,
      //       number: 10,
      //       name: 'ค่าธรรมเนียม',
      //       canedit: false
      //     },
      //     {
      //       id: 12,
      //       number: 11,
      //       name: 'เสร็จสิ้น',
      //       canedit: false
      //     },
      //   ]
      // } else {
      //   this.menuList = [
      //     {
      //       id: 1,
      //       number: 1,
      //       name: 'บันทึกเพื่อรับไฟล์',
      //       required: true,
      //       canedit: false
      //     },
      //     {
      //       id: 2,
      //       number: 2,
      //       name: 'คำขอจดทะเบียนสัญญาอนุญาตคำขอเลขที่',
      //       required: true,
      //       canedit: false
      //     },
      //     {
      //       id: 3,
      //       number: 3,
      //       name: 'ประเภทการยื่น',
      //       canedit: false
      //     },
      //     {
      //       id: 4,
      //       number: 4,
      //       name: 'ประเภทคำขอ',
      //       required: true,
      //       canedit: false
      //     },
      //     {
      //       id: 5,
      //       number: 5,
      //       name: 'เจ้าของ/ผู้ได้รับอนุญาต/ตัวแทน',
      //       canedit: false
      //     },
      //     {
      //       id: 6,
      //       number: 6,
      //       name: 'ผู้ได้รับอนุญาต/ผู้ได้รับอนุญาตช่วง/ตัวแทน',
      //       canedit: false
      //     },
      //     {
      //       id: 7,
      //       number: 7,
      //       name: 'สถานที่ติดต่อภายในประเทศ',
      //       canedit: false
      //     },
      //     {
      //       id: 8,
      //       number: 8,
      //       name: 'รายการจดทะเบียนสัญญาอนุญาตให้ใช้เครื่องหมายการค้า/บริการ',
      //       canedit: false
      //     },
      //     {
      //       id: 9,
      //       number: 9,
      //       name: 'การขอต่ออายุทะเบียนสัญญาอนุญาต',
      //       canedit: false
      //     },
      //     {
      //       id: 10,
      //       number: 10,
      //       name: 'เอกสารหลักฐานประกอบคำขอจดทะเบียนสัญญาอนุญาตให้ใช้สิทธิ',
      //       canedit: false
      //     },
      //     {
      //       id: 11,
      //       number: 11,
      //       name: 'ค่าธรรมเนียม',
      //       canedit: false
      //     },
      //     {
      //       id: 12,
      //       number: 12,
      //       name: 'เสร็จสิ้น',
      //       canedit: false
      //     },
      //   ]
      // }
    })
  }

  calleFormFromContractNumberLoad(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave050SearcContractNumber(params).subscribe((data: any) => {
      this.loadConsideringSimilarDocument(data);
      this.setValueStep4('RECEIVER');
    })
  }

  loadConsideringSimilarDocument(data: any): void {
    if (data && data.is_search_success) {
      // Set value
      this.seachData = data;
      
      this.automateTest.test(this, { list: data.list })
      this.input.telephone = this.telephone;
      this.input.email = this.email;

      this.input.ownerMarkItem = {
        receiver_type_code: "PEOPLE",
        sex_code: "MALE",
        nationality_code: "AF",
        career_code: "ค้าขาย",
        address_country_code: "TH",
        is_contact_person: false
      }

      this.input.receiverMarkItem = {
        receiver_type_code: "PEOPLE",
        sex_code: "MALE",
        nationality_code: "AF",
        career_code: "ค้าขาย",
        address_country_code: "TH",
        is_contact_person: false
      }

      this.input.agenReceiverMarkItem = {
        receiver_type_code: "PEOPLE",
        sex_code: "MALE",
        nationality_code: "AF",
        career_code: "ค้าขาย",
        address_country_code: "TH",
        is_contact_person: false
      }

      this.input.agentMarkItem = {
        receiver_type_code: "PEOPLE",
        sex_code: "MALE",
        nationality_code: "AF",
        career_code: "ค้าขาย",
        address_country_code: "TH",
        is_contact_person: false
      }

      this.input.receiver_people_list = [];
      this.input.receiver_representative_list = [];
      this.input.save050_receiver_people_type_code = [];

      this.input.request_date_text = this.seachData.request_date_text;
      this.input.request_number = this.seachData.save010.request_number;

      this.input.registration_number = this.seachData.registration_load_number;
      this.input.load_receiver_people_type_code = this.seachData.load_receiver_people_type_code;      
      this.input.listOwnerMark = this.seachData.people_load_list;
      this.input.listAgentMark = this.seachData.representative_load_list;
      this.input.listReceiverMark = [];
      this.input.listAgentReceiverMark = [];
      if (this.input.listOwnerMark.length > 0){
        this.input.payer_name = this.input.listOwnerMark[0].name;
      }
    } else {
      console.warn(`request_number is invalid.`);
      this.input.request_date_text = "";
      this.validate.request_number = data.alert_msg;
      this.togglePopup("isSearch");
    }
    this.global.setLoading(false);
  }

  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name];
  }

  setValueStep4(value: string): void {
    if (value == "OWNER") {
      this.input.save050_people_type_code = value;
      this.input.save050_receiver_people_type_code = "RECEIVER";
    }

    if (value == "RECEIVER") {
      this.input.save050_people_type_code = value;
      this.input.save050_receiver_people_type_code = "RECEIVER_PERIOD";
    }
  }

  csv2Array(fileInput: any) {
    //read file from input

    console.log(">>>>>>>>>>>>>>>>>");
    this.fileReaded = fileInput.target.files[0];

    let reader: FileReader = new FileReader();
    reader.readAsText(this.fileReaded);

    reader.onload = (e) => {
      this.csv = reader.result;
      let allTextLines = this.csv.split(/\r|\n|\r/);
      let headers = allTextLines[0].split(',');
      this.productList = [];

      for (let i = 1; i < allTextLines.length; i++) {
        // split content based on comma
        let data = allTextLines[i].split(',');
        if (data.length === headers.length) {
          let tarr = [];
          for (let j = 0; j < headers.length; j++) {
            tarr.push(data[j]);
          }
          this.productList.push({ is_check: false, cls: data[0], product: data[1] });
          // log each row to see output 
          console.log(tarr);
          this.inputFileStep7 = true;
        }
      }
      // all rows in the csv file 
      console.log(">>>>>>>>>>>>>>>>>", this.productList);
      let element: any;
      element = document.getElementById('fileInput') as HTMLElement;
      element.value = "";
      this.perpageP1 = 5;
      this.calOnchangepage();
    }
  }

  validateSaveItemModal(nameItem: any): boolean {
      if (nameItem === "ownerMarkItem") {

      if (this.input.ownerMarkItem.address_country_code != "TH") {
        this.input.ownerMarkItem.address_sub_district_name = "No Thailand"
        this.input.ownerMarkItem.postal_code = "00000"
      }
      

      if (this.input.ownerMarkItem.receiver_type_code != "PEOPLE" && this.input.ownerMarkItem.receiver_type_code != "PEOPLE_FOREIGNER") {
        this.input.ownerMarkItem.sex_code = "No Gender";
      }
     

      console.log("Owner People", this.input.ownerMarkItem);
        this.validateObj = {
          receiver_type_code_Owner: this.input.ownerMarkItem.receiver_type_code,
          card_number_Owner: this.input.ownerMarkItem.card_number,
          name_Owner: this.input.ownerMarkItem.name,
          sex_code_Owner: this.input.ownerMarkItem.sex_code,
          nationality_code_Owner: this.input.ownerMarkItem.nationality_code,
          career_code_Owner: this.input.ownerMarkItem.career_code,
          address_country_code_Owner: this.input.ownerMarkItem.address_country_code,
          house_number_Owner: this.input.ownerMarkItem.house_number,
          address_sub_district_name_Owner: this.input.ownerMarkItem.address_sub_district_name,
          postal_code_Owner: this.input.ownerMarkItem.postal_code,
          email_Owner: this.input.ownerMarkItem.email,
          telephone_Owner: this.input.ownerMarkItem.telephone,
          is_contact_person_Owner: this.input.ownerMarkItem.is_contact_person,
        }

        let result = validateService('validateEFormSave05ProcessStep4OwnerMarkItem', this.validateObj)
        this.validate = result.validate
        return result.isValid
      }

      if (nameItem === "agentMarkItem") {
        console.log("agentMarkItem People", this.input.agentMarkItem);
        if (this.input.agentMarkItem.address_country_code != "TH") {
          this.input.agentMarkItem.address_sub_district_name = "No Thailand"
          this.input.agentMarkItem.postal_code = "00000"
        }
        if (this.input.agentMarkItem.receiver_type_code != "PEOPLE" && this.input.agentMarkItem.receiver_type_code != "PEOPLE_FOREIGNER") {
          this.input.agentMarkItem.sex_code = "No Gender";
        }
        this.validateObj = {
          //rights_agent: this.input.agentMarkItem.rights,
          receiver_type_code_agent: this.input.agentMarkItem.receiver_type_code,
          card_number_agent: this.input.agentMarkItem.card_number,
          name_agent: this.input.agentMarkItem.name,
          sex_code_agent: this.input.agentMarkItem.sex_code,
          nationality_code_agent: this.input.agentMarkItem.nationality_code,
          career_code_agent: this.input.agentMarkItem.career_code,
          address_country_code_agent: this.input.agentMarkItem.address_country_code,
          house_number_agent: this.input.agentMarkItem.house_number,
          address_sub_district_name_agent: this.input.agentMarkItem.address_sub_district_name,
          postal_code_agent: this.input.agentMarkItem.postal_code,
          email_agent: this.input.agentMarkItem.email,
          telephone_agent: this.input.agentMarkItem.telephone,

        }

        let result = validateService('validateEFormSave05ProcessStep4AgentMarkItem', this.validateObj)
        this.validate = result.validate
        return result.isValid
      }

      if (nameItem === "receiverMarkItem") {
        if (this.input.receiverMarkItem.address_country_code != "TH") {
          this.input.receiverMarkItem.address_sub_district_name = "No Thailand"
          this.input.receiverMarkItem.postal_code = "00000"

        }
        if (this.input.receiverMarkItem.receiver_type_code != "PEOPLE" && this.input.receiverMarkItem.receiver_type_code != "PEOPLE_FOREIGNER") {
          this.input.receiverMarkItem.sex_code = "No Gender";
        }

       
        this.validateObj = {
          receiver_type_code_receiver: this.input.receiverMarkItem.receiver_type_code,
          card_number_receiver: this.input.receiverMarkItem.card_number,
          name_receiver: this.input.receiverMarkItem.name,
          sex_code_receiver: this.input.receiverMarkItem.sex_code,
          nationality_code_receiver: this.input.receiverMarkItem.nationality_code,
          career_code_receiver: this.input.receiverMarkItem.career_code,
          address_country_code_receiver: this.input.receiverMarkItem.address_country_code,
          house_number_receiver: this.input.receiverMarkItem.house_number,
          address_sub_district_name_receiver: this.input.receiverMarkItem.address_sub_district_name,
          postal_code_receiver: this.input.receiverMarkItem.postal_code,
          email_receiver: this.input.receiverMarkItem.email,
          telephone_receiver: this.input.receiverMarkItem.telephone,


        }
        let result = validateService('validateEFormSave05ProcessStep5receiverMarkItem', this.validateObj)
        this.validate = result.validate
        return result.isValid
      }

      if (nameItem === "agenReceiverMarkItem") {
        if (this.input.agenReceiverMarkItem.address_country_code != "TH") {
          this.input.agenReceiverMarkItem.address_sub_district_name = "No Thailand"
          this.input.agenReceiverMarkItem.postal_code = "00000"

        }

        if (this.input.agenReceiverMarkItem.receiver_type_code != "PEOPLE" && this.input.agenReceiverMarkItem.receiver_type_code != "PEOPLE_FOREIGNER") {
          this.input.agenReceiverMarkItem.sex_code = "No Gender";
        }

        console.log(this.input.agenReceiverMarkItem);
        this.validateObj = {
          //rights_agenReceiver: this.input.agenReceiverMarkItem.rights,
          receiver_type_code_agenReceiver: this.input.agenReceiverMarkItem.receiver_type_code,
          card_number_agenReceiver: this.input.agenReceiverMarkItem.card_number,
          name_agenReceiver: this.input.agenReceiverMarkItem.name,
          sex_code_agenReceiver: this.input.agenReceiverMarkItem.sex_code,
          nationality_code_agenReceiver: this.input.agenReceiverMarkItem.nationality_code,
          career_code_agenReceiver: this.input.agenReceiverMarkItem.career_code,
          address_country_code_agenReceiver: this.input.agenReceiverMarkItem.address_country_code,
          house_number_agenReceiver: this.input.agenReceiverMarkItem.house_number,
          address_sub_district_name_agenReceiver: this.input.agenReceiverMarkItem.address_sub_district_name,
          postal_code_agenReceiver: this.input.agenReceiverMarkItem.postal_code,
          email_agenReceiver: this.input.agenReceiverMarkItem.email,
          telephone_agenReceiver: this.input.agenReceiverMarkItem.telephone,



        }

        let result = validateService('validateEFormSave05ProcessStep5agenReceiverMarkItem', this.validateObj)
        this.validate = result.validate
        return result.isValid
      }
      return true
    }
  

    //onClickSaveItemModalInTable(
    //  obj: string,
    //  nameList: any,
    //  nameItem: any,
    //  nameModal: any
    //): void {
    //  console.log("validate", this.validateSaveItemModal(nameItem))
    //  if (this.validateSaveItemModal(nameItem)) {
    //    console.log("indexEdit", this.input.indexEdit)
    //    if (this.input.indexEdit >= 0) {
    //      // Is Edit
    //      this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
    //      this.toggleModal(nameModal);
    //    } else {
    //      // Is Add
    //      this[obj][nameList].push(this[obj][nameItem]);
    //      console.log("people_list", this[obj][nameList])
    //      this.toggleModal(nameModal);
    //    }
    //  }
    //}
  
  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    console.log("validate", this.validateSaveItemModal(nameItem))
    if (this.validateSaveItemModal(nameItem)) {
      console.log("indexEdit", this.input.indexEdit)
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
        alert("กรณีมีการแก้ไขเปลี่ยนแปลงข้อมูลเจ้าของ/ตัวแทน ให้ยื่นก.06 แนบมาด้วย");
        console.log(alert);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        console.log("people_list", this[obj][nameList])
        this.toggleModal(nameModal);
      }
    }
  }

  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
  }

  //! <<<< Pagination >>>
  //onChangePage(page: any, name: string): void {
  //  if (+page) {
  //    this[name].currentPage = page
  //  }
  //}
  //onChangePerPage(value: number, name: string): void {
  //  this[name].itemsPerPage = value
  //}
  //changePaginateTotal(total: any, name: string): void {
  //  let paginate = CONSTANTS.PAGINATION.INIT
  //  paginate.totalItems = total
  //  this[name] = paginate
  //}
  //getMaxPage(name: string): number {
  //  return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  //}

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
    console.log(this.input.save050_people_type_code);
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  backtomenu(canedit: any, number: any): void {
    console.log(canedit);
    if (canedit == true) {
      this.currentStep = number;
      this.currentID = this.menuList[this.currentStep - 1].id

      for (var i = 1; i <= this.menuList.length + 1; i++) {
        if (i <= this.stepPass - 1) {
          this.menuList[i].canedit = true;
        }
      }
    }
  }
 
  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any, item: any): void {

    if (item) {
      this[obj][item][name] = value;
    } else {
      this[obj][name] = value;
    }


    console.log(this.master, "Master")


    console.log(this.input, "Input")
    console.log(this.input.save050_allow_type_code)




    if (this.currentID == 3) {
      this.Next3 = true;
    }

    if (this.currentID == 5) {
      console.log("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa")
      console.log("DATA", this.input.save050_receiver_people_type_code);
    }

    if (this.currentID == 6)
      this.Next6 = true;

    if (this.currentID == 8)
      this.Next8 = true;
  }
  //! <<< Prepare Call API >>>
  prepareCallAPI(callback: Function): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {};
    // Call api
    if (this.currentStep === 1) {
      this.callAPI(params, callback);
    } else if (this.currentStep === 2) {
      this.callAPI(params, callback);
    } else {
      this.global.setLoading(false);
      callback();
    }
  }

  //! <<< Call API >>>
  callAPI(params: any, callback: Function): void {
    this.global.setLoading(false);
    callback();

    // this.forkJoinService.initEForm01Page(params).subscribe((data: any) => {
    //   if (data) {
    //     callback();
    //   }
    //   this.global.setLoading(false);
    // });
  }

  //! <<< Wizard >>>
  nextMenu(): void {
    if (!this.menuList[this.currentStep - 1].canEdit) {
      this.calcProgressPercent(this.currentStep);
    }
    this.menuList[this.currentStep - 1].canEdit = true;
    this.currentStep++;
    this.currentID = this.menuList[this.currentStep - 1].id;
    this.currentSubStep = 1;
  }
  onClickNext(): void {
    if (this.validateWizard()) {
      this.prepareCallAPI(() => {
        if (this.menuList[this.currentStep - 1].hasSub) {
          let sizeSubMenu = this.menuList[this.currentStep - 1].subList.length;

          if (this.currentSubStep < sizeSubMenu) {
            this.menuList[this.currentStep - 1].subList[
              this.currentSubStep - 1
            ].canEdit = true;
            this.currentSubStep++;
          } else {
            this.menuList[this.currentStep - 1].subList[
              this.currentSubStep - 1
            ].canEdit = true;
            this.nextMenu();
          }
        } else {
          this.nextMenu();
        }
      });
    }
  }
  //onClickMenu(menu: any): void {
  //  if (menu.canEdit) {
  //    if (this.validateWizard()) {
  //      this.prepareCallAPI(() => {
  //        this.currentStep = menu.number;
  //        this.currentID = menu.id;
  //        this.currentSubStep = 1;
  //      });
  //    }
  //  }
  //}
  //onClickSubMenu(menu: any, sub: any): void {
  //  if (sub.canEdit) {
  //    if (this.validateWizard()) {
  //      this.prepareCallAPI(() => {
  //        this.currentStep = menu.number;
  //        this.currentID = menu.id;
  //        this.currentSubStep = sub.number;
  //      });
  //    }
  //  }
  //}

  //changeMiniStep(action: number): void {
  //  if (this.validateWizard()) {
  //    this.menuList[this.currentStep - 1].subList[
  //      this.currentSubStep - 1
  //    ].miniList[this.currentMiniStep - 1].canEdit = true;

  //    this.currentMiniStep = this.currentMiniStep + action;
  //  }
  //}

  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    }

    if (this.currentStep === 6) {

      if (this.input.save050_contact_type_code == 'OTHERS') {

        console.log("Name", this.contact_address_list);

        this.validateObj = {
          nameS6: this.contact_address_list.name,
          house_numberS6: this.contact_address_list.house_number,
          postal_codeS6: this.contact_address_list.postal_code,
          emailS6: this.contact_address_list.email,
          telephoneS6: this.contact_address_list.telephone,
          address_sub_district_nameS6: this.contact_address_list.address_sub_district_name,
        }

        let result = validateService("validateEFormSave05ProcessStep6", this.validateObj);

        this.validate = result.validate;
        return result.isValid;
      }

    }
    return true;
  }


  reRunMenuNumber(): void {
    let number = 1;
    this.menuList.forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
      }
    });
  }
  calcProgressPercent(currentStep: number): void {
    this.progressPercent = Math.round(
      (currentStep / this.menuList.length) * 100
    );
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalReceiverFormOpen") {
          console.log(this.input.receiver_people_list);
          this.input.receiverMarkItem = clone(this.input.receiver_people_list[index]);
        } else if (name === "isModalAgentReceiverFormOpen") {
          this.input.agenReceiverMarkItem = clone(this.input.receiver_representative_list[index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.receiverMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agenReceiverMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
    }
  }

  getParamsSave(): void {
    this.saveInput = {};
    console.log("SeeInput", this.input);
    this.saveInput.id = this.response.load.id ? this.response.load.id : null;
    this.saveInput.eform_number = this.response.load.eform_number
        ? this.response.load.eform_number
        : null;

    this.saveInput.email = this.input.email;
    this.saveInput.telephone = this.input.telephone;
    this.saveInput.register_date = this.input.request_date_text;
    this.saveInput.request_number = this.input.request_number;
    this.saveInput.register_number = this.input.registration_number;
    this.saveInput.rule_number = this.rule_number;

    this.saveInput.save050_submit_type_code = this.input.save050_submit_type_code;
    this.saveInput.save050_allow_type_code = this.input.save050_allow_type_code;
    this.saveInput.save050_people_type_code = this.input.save050_people_type_code;
    this.saveInput.people_list = [];
    this.saveInput.representative_list = [];
    this.saveInput.save050_receiver_people_type_code = this.input.save050_receiver_people_type_code;
    this.saveInput.save050_representative_condition_type_code = this.input.save050_representative_condition_type_code;
    this.saveInput.receiver_people_list = [];
    this.saveInput.receiver_representative_list = [];
    this.saveInput.save050_contact_type_code = this.input.save050_contact_type_code;
    this.saveInput.save050_transfer_form_code = this.input.save050_transfer_form_code;
    this.saveInput.contract_date = this.input.contract_date;
    this.saveInput.contract_start_date = this.input.contract_start_date;
    this.saveInput.is_contract = this.input.is_contract;
    this.saveInput.contract_end_date = this.input.contract_end_date;
    this.saveInput.contract_conduration = this.input.contract_conduration;
    this.saveInput.remark_7_2 = this.input.remark_7_2;
    this.saveInput.remark_7_3 = this.input.remark_7_3;
    this.saveInput.is_7_4_1 = this.input.is_7_4_1;
    this.saveInput.is_7_4_2 = this.input.is_7_4_2;
    this.saveInput.is_7_4_3 = this.input.is_7_4_3;
    this.saveInput.is_7_4_4 = this.input.is_7_4_4;
    this.saveInput.save050_extend_type_code = this.input.save050_extend_type_code;
    this.saveInput.is_9_1 = this.input.is_9_1;
    this.saveInput.is_9_2 = this.input.is_9_2;
    this.saveInput.is_9_3 = this.input.is_9_3;
    this.saveInput.payer_name = this.input.payer_name;
    this.saveInput.total_price = this.input.total_price;


    this.input.listOwnerMark.forEach(obj => {
      this.saveInput.people_list.push({
        "address_type_code": obj.address_type_code,
        "nationality_code": obj.nationality_code,
        "career_code": obj.career_code,
        "card_type_code": obj.card_type_code,
        "card_type_name": obj.card_type_name,
        "card_number": obj.card_number,
        "receiver_type_code": obj.receiver_type_code,
        "name": obj.name,
        "house_number": obj.house_number,
        "village_number": obj.village_number,
        "alley": obj.alley,
        "street": obj.street,
        "address_sub_district_code": obj.address_sub_district_code,
        "address_district_code": obj.address_district_code,
        "address_province_code": obj.address_province_code,
        "postal_code": obj.postal_code,
        "address_country_code": obj.address_country_code,
        "telephone": obj.telephone,
        "fax": obj.fax,
        "email": obj.email,
        "sex_code": obj.sex_code,
        "is_contact_person": obj.is_contact_person
      })
    });

    this.input.listAgentMark.forEach(obj => {
      this.saveInput.representative_list.push({
        "address_type_code": obj.address_type_code,
        "nationality_code": obj.nationality_code,
        "career_code": obj.career_code,
        "card_type_code": obj.card_type_code,
        "card_type_name": obj.card_type_name,
        "card_number": obj.card_number,
        "receiver_type_code": obj.receiver_type_code,
        "name": obj.name,
        "house_number": obj.house_number,
        "village_number": obj.village_number,
        "alley": obj.alley,
        "street": obj.street,
        "address_sub_district_code": obj.address_sub_district_code,
        "address_district_code": obj.address_district_code,
        "address_province_code": obj.address_province_code,
        "postal_code": obj.postal_code,
        "address_country_code": obj.address_country_code,
        "telephone": obj.telephone,
        "fax": obj.fax,
        "email": obj.email,
        "sex_code": obj.sex_code,
        "is_contact_person": obj.is_contact_person
      })
    });

    this.input.listReceiverMark.forEach(obj => {
      this.saveInput.receiver_people_list.push({
        "address_type_code": obj.address_type_code,
        "nationality_code": obj.nationality_code,
        "career_code": obj.career_code,
        "card_type_code": obj.card_type_code,
        "card_type_name": obj.card_type_name,
        "card_number": obj.card_number,
        "receiver_type_code": obj.receiver_type_code,
        "name": obj.name,
        "house_number": obj.house_number,
        "village_number": obj.village_number,
        "alley": obj.alley,
        "street": obj.street,
        "address_sub_district_code": obj.address_sub_district_code,
        "address_district_code": obj.address_district_code,
        "address_province_code": obj.address_province_code,
        "postal_code": obj.postal_code,
        "address_country_code": obj.address_country_code,
        "telephone": obj.telephone,
        "fax": obj.fax,
        "email": obj.email,
        "sex_code": obj.sex_code,
        "is_contact_person": obj.is_contact_person
      })
    });

    this.input.listAgentReceiverMark.forEach(obj => {
      this.saveInput.receiver_representative_list.push({
        "address_type_code": obj.address_type_code,
        "nationality_code": obj.nationality_code,
        "career_code": obj.career_code,
        "card_type_code": obj.card_type_code,
        "card_type_name": obj.card_type_name,
        "card_number": obj.card_number,
        "receiver_type_code": obj.receiver_type_code,
        "name": obj.name,
        "house_number": obj.house_number,
        "village_number": obj.village_number,
        "alley": obj.alley,
        "street": obj.street,
        "address_sub_district_code": obj.address_sub_district_code,
        "address_district_code": obj.address_district_code,
        "address_province_code": obj.address_province_code,
        "postal_code": obj.postal_code,
        "address_country_code": obj.address_country_code,
        "telephone": obj.telephone,
        "fax": obj.fax,
        "email": obj.email,
        "sex_code": obj.sex_code,
        "is_contact_person": obj.is_contact_person
      })
    });
  }

  onClickViewPdfKor05(): void {
    if (this.validateWizard()) {
      this.getParamsSave();
      console.log(this.saveInput)
      viewPDF("ViewPDF/TM05", this.saveInput);
    }
  }

  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }

  save(isOverwrite: boolean): void {
    //Open loading
    this.global.setLoading(true);
    // Set param
    this.getParamsSave();
    if (!isOverwrite) {
      this.saveInput = getParamsOverwrite(this.saveInput);
    }

    //Call api
    this.calleFormSave050Save(this.saveInput);
  }

  calleFormSave050Save(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave050Save(params).subscribe((data: any) => {
      if (data) {
        this.isDeactivation = true;
        // Open toast success
        let toast = CONSTANTS.TOAST.SUCCESS;
        toast.message = "บันทึกข้อมูลสำเร็จ";
        this.global.setToast(toast);
        // Navigate
        this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
      }else {
        // Close loading
        this.global.setLoading(false);
      }      
    })
  }

  sendEmail(): void {
    this.global.setLoading(true);
    this.getParamsSave();
    
    console.log("SaveInput", this.saveInput);

    this.eFormSaveProcessService.eFormSave050Save(this.saveInput).subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
    })
  }

  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  
   //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }
  
  onChangeSearchType(): void {
    this.input.request_date_text = '';
    this.input.listOwnerMark = [];
    this.input.listAgentMark = [];
  }

  displayAddress(value: any): any {
    return displayAddress(value);
  }

  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
    }

    if (nameList === "listOwnerMark" || nameList === "listAgentMark") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }
}


