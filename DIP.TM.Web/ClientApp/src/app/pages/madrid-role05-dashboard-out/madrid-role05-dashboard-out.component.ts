import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-madrid-role05-dashboard-out',
  templateUrl: './madrid-role05-dashboard-out.component.html',
  styleUrls: ['./madrid-role05-dashboard-out.component.scss']
})
export class MadridRole05DashboardOutComponent implements OnInit {
  public notify=110;
  public notifyText = this.notify > 99? "99+" : this.notify.toString()   ;

  constructor() { }

  ngOnInit() {
  }

}
