import { Component, OnInit } from '@angular/core'

@Component({
  selector: "app-eform-owner-and-agent",
  templateUrl: "./owner-and-agent.component.html",
  styleUrls: ["./owner-and-agent.component.scss"]
})
export class EFormOwnerAndAgentComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public modal: any
  public isSubmited: any
  public isValid: any

  ngOnInit() {
    this.modal = {
      isModalOwnerFormOpen: true,
      isModalAgentFormOpen: false,
    }
    this.isSubmited = false
    this.isValid = false
  }

  constructor() { }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    if (!this.modal[name]) { // Is open
      this.modal[name] = true
    } else { // Is close
      this.modal[name] = false
    }
  }
}
