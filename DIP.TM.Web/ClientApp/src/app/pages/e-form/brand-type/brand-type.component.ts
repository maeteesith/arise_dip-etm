import { Component, OnInit, EventEmitter, Output } from '@angular/core'

@Component({
  selector: "app-eform-brand-type",
  templateUrl: "./brand-type.component.html",
  styleUrls: ["./brand-type.component.scss"]
})
export class EFormBrandTypeComponent implements OnInit {
  //TODO >>> Declarations <<</'

  ngOnInit() {
  }

  constructor() { }

  _onClickNext() {
    this.onClickNext.emit(null);
  }

  // Component Event Props
  @Output() onClickNext = new EventEmitter();
}
