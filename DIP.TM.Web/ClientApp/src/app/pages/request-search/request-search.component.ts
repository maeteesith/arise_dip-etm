import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { RequestSearchService } from '../../services/request-search-buffer.service'
import { RequestSearchPeopleService } from '../../services/request-search-people-buffer.service'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-request-search",
  templateUrl: "./request-search.component.html",
  styleUrls: ["./request-search.component.scss"]
})
export class RequestSearchComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public request_number_select: any
  public request_number_list: any[]
  public is_vourcher: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  // List Product
  public listProduct: any[]
  public paginateProduct: any
  public perPageProduct: number[]
  // Response Product
  public responseProduct: any
  public listProductEdit: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  public autocompleteListRequestSearchLoad: any
  public is_autocomplete_RequestSearchLoad_show: any
  public is_autocomplete_RequestSearchLoad_load: any
  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}

    this.input = {
      id: null,
      request_number: '',
      registration_number: '',
      country_registration_number: '',
      rule_28_date: '',
      people_name: '',
      request_item_type_name: '',
      reference_number_rule_07: '',
      register_exam_name: '',
      trademark: '',
      request_sound_type: '',
      request_sound_description: '',
      trademark_status_code: '',
      make_date: '',
      trademark_expired_end_date: '',
      reference_number: '',
      request_source_name: '',
      full_view: {},
    }
    this.listProduct = []
    this.paginateProduct = CONSTANTS.PAGINATION.INIT
    this.perPageProduct = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List

    this.autocompleteListRequestSearchLoad = []
    this.is_autocomplete_RequestSearchLoad_show = false
    this.is_autocomplete_RequestSearchLoad_load = false


    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
      if (param_url.request_check_voucher_number) {
        this.is_vourcher = true

        this.RequestSearchPeopleService.RequestCheckVoucherNumberLoad(param_url.request_check_voucher_number).subscribe((data: any) => {
          if (data) {
            this.request_number_list = data.request_check_request_number_list.filter(r => r.is_2_7).map((item: any) => { return item.request_number })
            if (this.request_number_list.length > 0) {
              this.request_number_select = this.request_number_list[0];
              this.onChangeRequestNumber(this.request_number_select)
            }
          } else {
            alert("ไม่พบเลข Voucher");
            window.open("/request-search-people", "_self")
          }
          // Close loading
          this.global.setLoading(false)
        })
      } else {
        if (this.editID) {
          this.RequestSearchService.Load(this.editID).subscribe((data: any) => {
            if (data) {
              // Manage structure
              Object.keys(data).forEach((item: any) => {
                this.input[item] = data[item]
              })
              this.input.file_trademark_2d = this.input.file_trademark_2d && this.input.file_trademark_2d != '' ? this.input.file_trademark_2d : 'assets/images/contents/no-image.jpg'
            }
            // Close loading
            this.global.setLoading(false)
          })
        } else {
          this.global.setLoading(false)
        }
      }
    })
  }

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private RequestSearchService: RequestSearchService,
    private RequestSearchPeopleService: RequestSearchPeopleService
  ) { }


  // Autocomplete
  autocompleteChangeRequestSearchLoad(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      //this.is_autocomplete_RequestSearchLoad_show = true

      //let params = {
      //    filter: { request_number: object[name] },
      //    paging: {
      //        item_per_page: item_per_page
      //    },
      //}

      this.callAutocompleteChangeRequestSearchLoad(object[name])
    }
  }
  autocompleteBlurRequestSearchLoad(object: any, name: any, item_per_page: any = 5): void {
    setTimeout(function () {
      this.is_autocomplete_RequestSearchLoad_show = false
    }, 200)
  }
  callAutocompleteChangeRequestSearchLoad(params: any): void {
    //if (this.input.is_autocomplete_RequestSearchLoad_load) return
    //this.input.is_autocomplete_RequestSearchLoad_load = true
    let pthis = this
    this.RequestSearchService.RequestSearchLoad(params).subscribe((data: any) => {
      console.log(data)
      if (data) {
        //if (data.length == 1) {
        //setTimeout(function () {
        pthis.autocompleteChooseRequestSearchLoad(data)
        //}, 200)
        //} else {
        //  this.autocompleteListRequestSearchLoad = data
        //}
      }
    })
    //this.input.is_autocomplete_RequestSearchLoad_load = false
  }
  autocompleteChooseRequestSearchLoad(data: any): void {
    console.log(data)
    this.input = data
    this.input.file_trademark_2d = this.input.file_trademark_2d && this.input.file_trademark_2d != '' ? this.input.file_trademark_2d : 'assets/images/contents/no-image.jpg'

    this.listProduct = data.product_list || []
    this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')


    //this.is_autocomplete_RequestSearchLoad_show = false
  }

  onChangeRequestNumber(request_number_select: any): void {
    this.global.setLoading(true)
    this.RequestSearchPeopleService.RequestSearchLoad(request_number_select).subscribe((data: any) => {
      console.log(data)
      if (data) {
        this.autocompleteChooseRequestSearchLoad(data)
        this.global.setLoading(false)
      }
    })
  }

  onClickOwner(): void {
    //if(this.validateOwner()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callOwner(params)
    //}
  }
  validateOwner(): boolean {
    let result = validateService('validateOwner', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callOwner(params: any): void {
    this.RequestSearchService.Owner(params).subscribe((data: any) => {
      // if(isValidOwnerResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRepresentative(): void {
    //if(this.validateRepresentative()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callRepresentative(params)
    //}
  }
  validateRepresentative(): boolean {
    let result = validateService('validateRepresentative', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRepresentative(params: any): void {
    this.RequestSearchService.Representative(params).subscribe((data: any) => {
      // if(isValidRepresentativeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCard(): void {
    //if(this.validateCard()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callCard(params)
    //}
  }
  validateCard(): boolean {
    let result = validateService('validateCard', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCard(params: any): void {
    this.RequestSearchService.Card(params).subscribe((data: any) => {
      // if(isValidCardResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestGroup(): void {
    //if(this.validateRequestGroup()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callRequestGroup(params)
    //}
  }
  validateRequestGroup(): boolean {
    let result = validateService('validateRequestGroup', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestGroup(params: any): void {
    this.RequestSearchService.RequestGroup(params).subscribe((data: any) => {
      // if(isValidRequestGroupResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickAllowContact(): void {
    //if(this.validateAllowContact()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callAllowContact(params)
    //}
  }
  validateAllowContact(): boolean {
    let result = validateService('validateAllowContact', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callAllowContact(params: any): void {
    this.RequestSearchService.AllowContact(params).subscribe((data: any) => {
      // if(isValidAllowContactResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickTrademarkWarranty(): void {
    //if(this.validateTrademarkWarranty()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callTrademarkWarranty(params)
    //}
  }
  validateTrademarkWarranty(): boolean {
    let result = validateService('validateTrademarkWarranty', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callTrademarkWarranty(params: any): void {
    this.RequestSearchService.TrademarkWarranty(params).subscribe((data: any) => {
      // if(isValidTrademarkWarrantyResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickTrademarkJoin(): void {
    //if(this.validateTrademarkJoin()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callTrademarkJoin(params)
    //}
  }
  validateTrademarkJoin(): boolean {
    let result = validateService('validateTrademarkJoin', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callTrademarkJoin(params: any): void {
    this.RequestSearchService.TrademarkJoin(params).subscribe((data: any) => {
      // if(isValidTrademarkJoinResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickTrademark07(): void {
    //if(this.validateTrademark07()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callTrademark07(params)
    //}
  }
  validateTrademark07(): boolean {
    let result = validateService('validateTrademark07', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callTrademark07(params: any): void {
    this.RequestSearchService.Trademark07(params).subscribe((data: any) => {
      // if(isValidTrademark07Response(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickStatusLog(): void {
    //if(this.validateStatusLog()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callStatusLog(params)
    //}
  }
  validateStatusLog(): boolean {
    let result = validateService('validateStatusLog', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callStatusLog(params: any): void {
    this.RequestSearchService.StatusLog(params).subscribe((data: any) => {
      // if(isValidStatusLogResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickFee(): void {
    //if(this.validateFee()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callFee(params)
    //}
  }
  validateFee(): boolean {
    let result = validateService('validateFee', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callFee(params: any): void {
    this.RequestSearchService.Fee(params).subscribe((data: any) => {
      // if(isValidFeeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickFileAddress(): void {
    //if(this.validateFileAddress()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callFileAddress(params)
    //}
  }
  validateFileAddress(): boolean {
    let result = validateService('validateFileAddress', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callFileAddress(params: any): void {
    this.RequestSearchService.FileAddress(params).subscribe((data: any) => {
      // if(isValidFileAddressResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickBookSend(): void {
    //if(this.validateBookSend()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callBookSend(params)
    //}
  }
  validateBookSend(): boolean {
    let result = validateService('validateBookSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callBookSend(params: any): void {
    this.RequestSearchService.BookSend(params).subscribe((data: any) => {
      // if(isValidBookSendResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRule(): void {
    //if(this.validateRule()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callRule(params)
    //}
  }
  validateRule(): boolean {
    let result = validateService('validateRule', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRule(params: any): void {
    this.RequestSearchService.Rule(params).subscribe((data: any) => {
      // if(isValidRuleResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRegisterPermit(): void {
    //if(this.validateRegisterPermit()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callRegisterPermit(params)
    //}
  }
  validateRegisterPermit(): boolean {
    let result = validateService('validateRegisterPermit', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRegisterPermit(params: any): void {
    this.RequestSearchService.RegisterPermit(params).subscribe((data: any) => {
      // if(isValidRegisterPermitResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRequestInstead(): void {
    //if(this.validateRequestInstead()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callRequestInstead(params)
    //}
  }
  validateRequestInstead(): boolean {
    let result = validateService('validateRequestInstead', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRequestInstead(params: any): void {
    this.RequestSearchService.RequestInstead(params).subscribe((data: any) => {
      // if(isValidRequestInsteadResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickOwnerChange(): void {
    //if(this.validateOwnerChange()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callOwnerChange(params)
    //}
  }
  validateOwnerChange(): boolean {
    let result = validateService('validateOwnerChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callOwnerChange(params: any): void {
    this.RequestSearchService.OwnerChange(params).subscribe((data: any) => {
      // if(isValidOwnerChangeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickRepresentativeChange(): void {
    //if(this.validateRepresentativeChange()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callRepresentativeChange(params)
    //}
  }
  validateRepresentativeChange(): boolean {
    let result = validateService('validateRepresentativeChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callRepresentativeChange(params: any): void {
    this.RequestSearchService.RepresentativeChange(params).subscribe((data: any) => {
      // if(isValidRepresentativeChangeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickCardChange(): void {
    //if(this.validateCardChange()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callCardChange(params)
    //}
  }
  validateCardChange(): boolean {
    let result = validateService('validateCardChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callCardChange(params: any): void {
    this.RequestSearchService.CardChange(params).subscribe((data: any) => {
      // if(isValidCardChangeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickProductChange(): void {
    //if(this.validateProductChange()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callProductChange(params)
    //}
  }
  validateProductChange(): boolean {
    let result = validateService('validateProductChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callProductChange(params: any): void {
    this.RequestSearchService.ProductChange(params).subscribe((data: any) => {
      // if(isValidProductChangeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickTrademarkWarrantyChange(): void {
    //if(this.validateTrademarkWarrantyChange()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callTrademarkWarrantyChange(params)
    //}
  }
  validateTrademarkWarrantyChange(): boolean {
    let result = validateService('validateTrademarkWarrantyChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callTrademarkWarrantyChange(params: any): void {
    this.RequestSearchService.TrademarkWarrantyChange(params).subscribe((data: any) => {
      // if(isValidTrademarkWarrantyChangeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickTrademarkJoinChange(): void {
    //if(this.validateTrademarkJoinChange()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callTrademarkJoinChange(params)
    //}
  }
  validateTrademarkJoinChange(): boolean {
    let result = validateService('validateTrademarkJoinChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callTrademarkJoinChange(params: any): void {
    this.RequestSearchService.TrademarkJoinChange(params).subscribe((data: any) => {
      // if(isValidTrademarkJoinChangeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickTransferLogChange(): void {
    //if(this.validateTransferLogChange()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callTransferLogChange(params)
    //}
  }
  validateTransferLogChange(): boolean {
    let result = validateService('validateTransferLogChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callTransferLogChange(params: any): void {
    this.RequestSearchService.TransferLogChange(params).subscribe((data: any) => {
      // if(isValidTransferLogChangeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSplitRequestLogChange(): void {
    //if(this.validateSplitRequestLogChange()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callSplitRequestLogChange(params)
    //}
  }
  validateSplitRequestLogChange(): boolean {
    let result = validateService('validateSplitRequestLogChange', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSplitRequestLogChange(params: any): void {
    this.RequestSearchService.SplitRequestLogChange(params).subscribe((data: any) => {
      // if(isValidSplitRequestLogChangeResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listProduct = data.product_list || []
        this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickProductAdd(): void {
    this.listProduct.push({
      index: this.listProduct.length + 1,
      item_sub_type_1_code: null,
      product_list_name: null,

    })
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
  }
  onChangeCheckboxProduct(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
  onChangeSelectProduct(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }



  onClickRequestDocumentDollectLink(): void {
    window.open("/request-document-collect/edit/" + this.input.request_number)
  }

  onClickBack(): void {
    window.close()
  }


  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }
  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any): void {
    this.input[name] = value
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this[name] = !this[name]
  }
}
