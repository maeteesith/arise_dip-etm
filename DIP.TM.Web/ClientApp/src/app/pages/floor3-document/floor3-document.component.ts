import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { Floor3DocumentService } from '../../services/floor3document-buffer.service'
import { AutomateTest } from '../../test/automate_test'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-floor3-document",
    templateUrl: "./floor3-document.component.html",
    styleUrls: ["./floor3-document.component.scss"]
})
export class Floor3DocumentComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    // List RequestNumberList
    public listRequestNumberList: any[]
    public paginateRequestNumberList: any
    public perPageRequestNumberList: number[]
    // Response RequestNumberList
    public responseRequestNumberList: any
    public listRequestNumberListEdit: any


    //label_save_combobox || label_save_radio
    public floor3DocumentTypeCodeList: any[]
    public requestItemTypeSplitCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial

    public public05_registrar_list: any[]


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            floor3_document_type_code: '',
            request_item_type_split_code: '',
            registrar_by_name: '',
            registrar_position_name: '',
            request_register_date: getMoment(),
            //request_extend_done_date: getMoment(),
            choose_signatur: '',
        }
        this.listRequestNumberList = []
        this.paginateRequestNumberList = CONSTANTS.PAGINATION.INIT
        this.perPageRequestNumberList = CONSTANTS.PAGINATION.PER_PAGE


        //Master List
        this.master = {
            floor3DocumentTypeCodeList: [],
            requestItemTypeSplitCodeList: [],
        }
        //Master List



        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initFloor3Document().subscribe((data: any) => {
            this.Floor3DocumentService.Floor3RegistrarList().subscribe((public05_registrar_list: any) => {
                if (data) {
                    this.master = data
                    this.master.floor3DocumentTypeCodeList.unshift({ "code": "", "name": "เลือกประเภทรายงาน" })
                    this.input.floor3_document_type_code = ""
                    this.master.requestItemTypeSplitCodeList.unshift({ "code": "", "name": "ประเภทเครื่องหมาย" })
                    this.input.request_item_type_split_code = ""
                }

                this.public05_registrar_list = public05_registrar_list.list

                if (this.editID) {
                    console.log(this.editID)
                    this.Floor3DocumentService.Floor3DocumentLoad(this.editID).subscribe((data: any) => {
                        if (data) {
                            this.input.floor3_document_type_code = 'TYPE_02'
                            this.listRequestNumberList = data.list
                        }
                        // Close loading
                        this.global.setLoading(false)
                        this.automateTest.test(this)
                    })
                } else {
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                }

            })
        })
    }

    constructor(
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private Floor3DocumentService: Floor3DocumentService
    ) { }


    onClickRequestExtendDoneList(): void {
        //if(this.validateRequestExtendDoneList()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callRequestExtendDoneList(this.saveData())
        //}
    }
    validateRequestExtendDoneList(): boolean {
        let result = validateService('validateRequestExtendDoneList', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callRequestExtendDoneList(params: any): void {
        this.Floor3DocumentService.RequestExtendDoneList(params).subscribe((data: any) => {
            // if(isValidRequestExtendDoneListResponse(res)) {
            if (data) {
                // Set value
                this.loadData(data)
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickPrint(item_list: any[]): void {
        console.log(item_list)
        this.input.public05_registrar_by_name = this.input.public05_registrar_by_name || ""

        if (this.input.floor3_document_type_code == 'TYPE_01') {
            window.open("/pdf/Kor09RegisterForm/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
        } else if (this.input.floor3_document_type_code == 'TYPE_02') {
            //var item_01_list = item_list.filter(r => r.request_item_type_code == "TRADE_MARK_AND_SERVICE_MARK")
            //var item_01_list = item_list
            //if (item_01_list.length > 0)
            window.open("/pdf/KorMor01/" + item_list.map((item: any) => { return item.request_number }).join("|") + "&" + this.input.public05_registrar_by_name, "_blank")

            //var item_02_list = item_list.filter(r => r.request_item_type_code == "CERTIFICATION_MARK")
            //if (item_02_list.length > 0)
            //    window.open("/pdf/KorMor02/" + item_02_list.map((item: any) => { return item.request_number }).join("|") + "&" + this.input.public05_registrar_by_name, "_blank")

            //var item_03_list = item_list.filter(r => r.request_item_type_code == "COLLECTIVE_MARK")
            //if (item_03_list.length > 0)
            //    window.open("/pdf/KorMor03/" + item_03_list.map((item: any) => { return item.request_number }).join("|") + "&" + this.input.public05_registrar_by_name, "_blank")
        } else if (this.input.floor3_document_type_code == 'TYPE_03') {
            window.open("/pdf/KorMor04/" + item_list.map((item: any) => { return item.request_number }).join("|") + "&" + this.input.public05_registrar_by_name, "_blank")
        } else if (this.input.floor3_document_type_code == 'TYPE_07') {
            window.open("/pdf/KorMor01_JoinerName/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
        } else if (this.input.floor3_document_type_code == 'TYPE_09') {
            window.open("/pdf/KorMor01_Product/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
        } else if (this.input.floor3_document_type_code == 'TYPE_10') {
            window.open("/pdf/KorMor01_Owner/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
        } else if (this.input.floor3_document_type_code == 'TYPE_11') {
            window.open("/pdf/KorMor01_Description/" + item_list.map((item: any) => { return item.request_number }).join("|"), "_blank")
        }
    }
    onClickPrintSelect(): void {
        var item_list = this.listRequestNumberList.filter(r => r.is_check)
        this.onClickPrint(item_list)
    }
    onClickPrintAll(): void {
        var item_list = this.listRequestNumberList
        this.onClickPrint(item_list)
    }
    onClickPrintBySave(): void {
    }

    onClickRequestNumberListAdd(): void {
        this.listRequestNumberList.push({
            index: this.listRequestNumberList.length + 1,
            request_number: null,
            request_item_type_code: null,
            name: null,
            registration_number: null,
            book_index: null,
            public_page_line_index: null,
            support_english_book: null,

        })
        this.changePaginateTotal(this.listRequestNumberList.length, 'paginateRequestNumberList')
    }

    onClickRequestNumberListDelete(item: any): void {
        // if(this.validateRequestNumberListDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listRequestNumberList.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listRequestNumberList.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listRequestNumberList.filter(r => r.is_check && r.id).length
            }

            var rs = //delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") :
                confirm("คุณต้องการลบรายการ?")
            if (rs) {
                let ids = []

                for (let i = 0; i < this.listRequestNumberList.length; i++) {
                    if (this.listRequestNumberList[i].is_check) {
                        //this.listRequestNumberList[i].cancel_reason = rs
                        //this.listRequestNumberList[i].status_code = "DELETE"
                        this.listRequestNumberList[i].is_deleted = true

                        //if (true && this.listRequestNumberList[i].id) ids.push(this.listRequestNumberList[i].id)
                        // else
                        this.listRequestNumberList.splice(i--, 1);
                    }
                }

                //if (true) {
                //  if (ids.length > 0) {
                //    let params = {
                //      ids: ids,
                //      cancel_reason: rs
                //    }
                //    // Call api
                //    this.callRequestNumberListDelete(params, ids)
                //    return;
                //  }
                //}

                //for (let i = 0; i < this.listRequestNumberList.length; i++) {
                //  this.listRequestNumberList[i].index = i + 1
                //}
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callRequestNumberListDelete(params: any, ids: any[]): void {
        this.Floor3DocumentService.RequestNumberListDelete(params).subscribe((data: any) => {
            // if(isValidRequestNumberListDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listRequestNumberList.length; i++) {
                    if (this.listRequestNumberList[i].id == id) {
                        this.listRequestNumberList.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listRequestNumberList.length; i++) {
                this.listRequestNumberList[i] = i + 1
            }

            //this.onClickDelete()
            // Close loading
            this.global.setLoading(false)
        })
    }


    // Modal Location
    autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
        if (object[name].length >= length) {
            console.log("d")
            this.is_autocomplete_ListModalLocation_show = true

            let params = {
                name: object[name],
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListModalLocation(params)
        }
    }
    autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_ListModalLocation_show = false
        }, 200)
    }
    callAutocompleteChangeListModalLocation(params: any): void {
        if (this.input.is_autocomplete_ListModalLocation_load) return
        this.input.is_autocomplete_ListModalLocation_load = true
        let pThis = this
        // this.Floor3DocumentService.Save04ListModalLocation(params).subscribe((data: any) => {
        //   if (data) {
        //     if (data.length == 1) {
        //       setTimeout(function () {
        //         pThis.autocompleteChooseListModalLocation(data[0])
        //       }, 200)
        //     } else {
        //       pThis.autocompleteListListModalLocation = data
        //     }
        //   }
        // })
        this.input.is_autocomplete_ListModalLocation_load = false
    }
    autocompleteChooseListModalLocation(data: any): void {
        this.inputModalAddress.address_sub_district_code = data.code
        this.inputModalAddress.address_sub_district_name = data.name
        this.inputModalAddress.address_district_code = data.district_code
        this.inputModalAddress.address_district_name = data.district_name
        this.inputModalAddress.address_province_code = data.province_code
        this.inputModalAddress.address_province_name = data.province_name
        this.inputModalAddress.address_country_code = data.country_code
        this.inputModalAddress.address_country_name = data.country_name
        this.is_autocomplete_ListModalLocation_show = false
    }
    onClickSave04ModalAddressSave(): void {
        Object.keys(this.inputModalAddress).forEach((item: any) => {
            this.inputModalAddressEdit[item] = this.inputModalAddress[item]
        })
        this.toggleModal("isModalPeopleEditOpen")
    }
    // Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }



    onClickRequestCheckRequestNumberKeyPress(event: any): void {
        if (event.key == "Enter") {
            this.callRequestCheckRequestNumberKeyPress()
        }
    }
    callRequestCheckRequestNumberKeyPress(): void {
        //let param = {
        //    request_number: this.input.request_number,
        //    //instruction_rule_id: this.editID ? +this.editID : null,
        //    //request_number: row_item.request_number,
        //    //is_deleted: row_item.is_deleted,
        //}
        this.global.setLoading(true)
        this.Floor3DocumentService.Floor3DocumentLoad(this.input.request_number).subscribe((data: any) => {
            // if(isValidRequestCheckRequestNumberKeyPressResponse(res)) {
            if (data && data.list && data.list.length == 1) {
                //var request_check = {}
                //Object.keys(data).forEach((item: any) => {
                //    if (item != "id") request_check[item] = data[item]
                //})
                this.listRequestNumberList.push(data.list[0])
                // Set value
                //this.loadData(data)
                //this.automateTest.test(this, { Send: 1 });
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onChangeRegistrarBy(): void {
        var item_list = this.public05_registrar_list.filter(r => r.id == this.input.public05_registrar_by)
        console.log(item_list)
        this.input.registrar_position_name =
            item_list[0].department_name
    }

    loadData(data: any): void {
        this.input = data

        this.listRequestNumberList = data.requestnumberlist_list || []
        this.changePaginateTotal(this.listRequestNumberList.length, 'paginateRequestNumberList')

    }

    listData(data: any): void {
        this.listRequestNumberList = data.list || []
        this.changePaginateTotal(this.listRequestNumberList.length, 'paginateRequestNumberList')

    }

    saveData(): any {
        let params = this.input

        params.requestnumberlist_list = this.listRequestNumberList || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
