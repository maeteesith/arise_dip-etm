import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'
import { observable } from 'rxjs';

@Component({
  selector: "app-considering-similar-document",
  templateUrl: "./considering-similar-document.component.html",
  styleUrls: ["./considering-similar-document.component.scss"]
})
export class ConsideringSimilarDocumentComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List ConsideringSimilarDocument
  public listConsideringSimilarDocument: any[]
  public paginateConsideringSimilarDocument: any
  public perPageConsideringSimilarDocument: number[]
  // Response ConsideringSimilarDocument
  public responseConsideringSimilarDocument: any
  public listConsideringSimilarDocumentEdit: any

  // List ConsideringSimilarInstruction
  public listConsideringSimilarInstruction: any[]
  public paginateConsideringSimilarInstruction: any
  public perPageConsideringSimilarInstruction: number[]
  // Response ConsideringSimilarInstruction
  public responseConsideringSimilarInstruction: any
  public listConsideringSimilarInstructionEdit: any

  public saveItemEdit: any

  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
    }
    this.listConsideringSimilarDocument = []
    this.paginateConsideringSimilarDocument = CONSTANTS.PAGINATION.INIT
    this.perPageConsideringSimilarDocument = CONSTANTS.PAGINATION.PER_PAGE

    this.listConsideringSimilarInstruction = []
    this.paginateConsideringSimilarInstruction = CONSTANTS.PAGINATION.INIT
    this.perPageConsideringSimilarInstruction = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalDocumentOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.saveItemEdit = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initConsideringSimilarDocument().subscribe((data: any) => {
      if (data) {
        this.master = data

      }
      if (this.editID) {
        this.CheckingProcessService.ConsideringSimilarDocumentLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }


  onClickConsideringSimilarDocumentSave(): void {
    //if(this.validateConsideringSimilarDocumentSave()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callConsideringSimilarDocumentSave(this.saveData())
    //}
  }
  validateConsideringSimilarDocumentSave(): boolean {
    let result = validateService('validateConsideringSimilarDocumentSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarDocumentSave(params: any): void {
    this.CheckingProcessService.ConsideringSimilarDocumentSave(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarDocumentSaveResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  //onClickConsideringSimilarDocumentSave(): void {
  //    //if(this.validateConsideringSimilarDocumentSave()) {
  //    // Open loading
  //    this.global.setLoading(true)
  //    // Call api
  //    this.callConsideringSimilarDocumentSave(this.saveData())
  //    //}
  //}
  //validateConsideringSimilarDocumentSave(): boolean {
  //    let result = validateService('validateConsideringSimilarDocumentSave', this.input)
  //    this.validate = result.validate
  //    return result.isValid
  //}
  ////! <<< Call API >>>
  //callConsideringSimilarDocumentSave(params: any): void {
  //    this.CheckingProcessService.ConsideringSimilarDocumentSave(params).subscribe((data: any) => {
  //        // if(isValidConsideringSimilarDocumentSaveResponse(res)) {
  //        if (data) {
  //            // Set value
  //            this.loadData(data)
  //        }
  //        // }
  //        // Close loading
  //        this.global.setLoading(false)
  //    })
  //}

  onClickConsideringSimilarDocumentAdd(): void {
    this.listConsideringSimilarDocument.push({
      index: this.listConsideringSimilarDocument.length + 1,
      considering_document_name: null,

    })
    this.changePaginateTotal(this.listConsideringSimilarDocument.length, 'paginateConsideringSimilarDocument')
  }

  onClickConsideringSimilarDocumentConsideringSimilarInstructionAdd(): void {
    this.listConsideringSimilarInstruction.push({
      index: this.listConsideringSimilarInstruction.length + 1,

    })
    this.changePaginateTotal(this.listConsideringSimilarInstruction.length, 'paginateConsideringSimilarInstruction')
  }

  onClickConsideringSimilarDocumentEdit(row_item: any): void {
    this.modal.isModalDocumentOpen = true
    //console.log(row_item.consider_similar_document_item_status_list.consider_similar_document_item_status_list)
    row_item.consider_similar_document_item_status_list = row_item.consider_similar_document_item_status_list || {}
    this.saveItemEdit = row_item
    //this.listConsideringSimilarDocument.push({
    //  index: this.listConsideringSimilarDocument.length + 1,
    //  considering_document_name: null,

    //})
    //this.changePaginateTotal(this.listConsideringSimilarDocument.length, 'paginateConsideringSimilarDocument')
  }
  onClickConsideringSimilarDocumentChange(code: any, value: any): void {
    console.log(code)
    console.log(value)

    //if (value)
    this.saveItemEdit.consider_similar_document_item_status_list = this.saveItemEdit.consider_similar_document_item_status_list || {}
    this.saveItemEdit.consider_similar_document_item_status_list[code] = value

    var is_have_allow, is_have_not_allow, is_have_empty
    this.master.save010ConsideringSimilarDocument06TypeCodeList.forEach((item: any) => {
      //console.log(item)
      if (this.saveItemEdit.consider_similar_document_item_status_list[item.code] == "ALLOW") {
        is_have_allow = true
      } else if (this.saveItemEdit.consider_similar_document_item_status_list[item.code] == "NOT_ALLOW") {
        is_have_not_allow = true
      } else {
        is_have_empty = true
      }
    })

    //console.log(this.saveItemEdit.consider_similar_document_item_status_list)

    if (!is_have_empty) {
      if (is_have_allow && is_have_not_allow) {
        this.saveItemEdit.consider_similar_document_status_code = "ALLOW_SOME"
      } else if (is_have_allow) {
        this.saveItemEdit.consider_similar_document_status_code = "ALLOW"
      } else if (is_have_not_allow) {
        this.saveItemEdit.consider_similar_document_status_code = "NOT_ALLOW"
      }
    }

    //console.log(is_have_allow)
    //console.log(is_have_not_allow)
    //console.log(is_have_empty)
    //console.log(this.saveItemEdit)
    //console.log(this.input.save_list[1].consider_similar_document_item_status_list)
    //console.log(this.saveItemEdit)
    //this.modal.isModalDocumentOpen = true

    //row_item.consider_similar_document_item_status_list = row_item.consider_similar_document_item_status_list || []
    //this.saveItemEdit = row_item
    ////this.listConsideringSimilarDocument.push({
    //  index: this.listConsideringSimilarDocument.length + 1,
    //  considering_document_name: null,

    //})
    //this.changePaginateTotal(this.listConsideringSimilarDocument.length, 'paginateConsideringSimilarDocument')
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      //console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.CheckingProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalDocumentOpen")
  }

  onClickConsideringSimilar(): void {
    var win = window.open("./considering-similar/edit/" + this.editID, "")
  }

  onClickConsideringSimilarInstruction(): void {
    window.open("considering-similar-instruction/edit/" + this.editID)
  }

  onClickConsideringSimilarCase28(): void {
    window.open("considering-similar-kor10/edit/" + this.editID)
  }

  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.input.save_list.forEach((item: any) => {
      //console.log(item.consider_similar_document_item_status_list)
      item.consider_similar_document_item_status_list = JSON.parse(item.consider_similar_document_item_status_list)
      //item.consider_similar_document_item_status_list = JSON.parse(item.consider_similar_document_item_status_list)
      //console.log(item.consider_similar_document_item_status_list)
    })
    //console.log(data)
    //console.log(data.save_list)

    this.listConsideringSimilarDocument = data.save_list || []
    this.listConsideringSimilarInstruction = data.instruction_rule_list || []
    this.changePaginateTotal(this.listConsideringSimilarDocument.length, 'paginateConsideringSimilarDocument')
    this.changePaginateTotal(this.listConsideringSimilarInstruction.length, 'paginateConsideringSimilarInstruction')

  }

  listData(data: any): void {
    this.listConsideringSimilarDocument = data || []
    this.listConsideringSimilarInstruction = data || []
    this.changePaginateTotal(this.listConsideringSimilarDocument.length, 'paginateConsideringSimilarDocument')
    this.changePaginateTotal(this.listConsideringSimilarInstruction.length, 'paginateConsideringSimilarInstruction')

  }

  saveData(): any {
    //console.log(this.saveItemEdit.consider_similar_document_item_status_list)
    //console.log(this.input.save_list[1].consider_similar_document_item_status_list)

    let params = this.input

    this.input.save_list.forEach((item: any) => {
      //var consider_similar_document_item_status_list = {}
      //item.consider_similar_document_item_status_list.forEach((item: any) => { consider_similar_document_item_status_list[item})
      //console.log(item)
      //console.log(item.consider_similar_document_item_status_list)
      //console.log(JSON.stringify(item.consider_similar_document_item_status_list))
      //var consider_similar_document_item_status_list = JSON.stringify(item.consider_similar_document_item_status_list)
      item.consider_similar_document_item_status_list = JSON.stringify(item.consider_similar_document_item_status_list)
      //console.log(item.consider_similar_document_item_status_list)
    })

    //console.log(this.input)

    //params.consideringsimilardocument_list = this.input.save_list || []
    //params.consideringsimilarinstruction_list = this.input.instruction_rule_list || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
