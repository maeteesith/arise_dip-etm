import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { RequestProcessService } from '../../services/request-process-buffer.service'
import { Help } from '../../helpers/help'
import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-request-other-item-list",
    templateUrl: "./request-other-item-list.component.html",
    styleUrls: ["./request-other-item-list.component.scss"]
})
export class RequestOtherItemListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public modal: any

    // List RequestOtherItemSub
    public listRequestOtherItemSub: any[]
    public paginateRequestOtherItemSub: any
    public perPageRequestOtherItemSub: number[]
    // Response RequestOtherItemSub
    public responseRequestOtherItemSub: any
    public listRequestOtherItemSubEdit: any


    // List RequestProcess
    public listRequestProcess: any[]
    public paginateRequestProcess: any
    public perPageRequestProcess: number[]
    // Response RequestProcess
    public responseRequestProcess: any

    //label_save_combobox || label_save_radio
    public receiptStatusCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            created_by_name: '',
            request_date: getMoment(),
            reference_number: '',
            request_number: '',
            receipt_status_code: '',
        }
        this.listRequestOtherItemSub = []
        this.paginateRequestOtherItemSub = CONSTANTS.PAGINATION.INIT
        this.perPageRequestOtherItemSub = CONSTANTS.PAGINATION.PER_PAGE

        this.listRequestProcess = []

        //Master List
        this.master = {
            receiptStatusCodeList: [],
        }
        //Master List



        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initRequestOtherItemList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.receiptStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" });
                console.log('come on', this.master)

            }
            if (this.editID) {
                let pThis = this
                this.RequestProcessService.RequestOtherItemSubLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        pThis.loadData(data)
                    }
                    // Close loading
                    pThis.global.setLoading(false)
                })
            }

            this.global.setLoading(false)
        })
    }

    constructor(
        private help: Help,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private RequestProcessService: RequestProcessService
    ) { }

    onClickRequestOtherItemSubList(): void {
        // if(this.validateRequestOtherItemSubList()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callRequestOtherItemSubList(this.saveData())
        // }
    }
    //! <<< Call API >>>
    callRequestOtherItemSubList(params: any): void {
        let pThis = this
        this.RequestProcessService.RequestOtherItemSubList(this.help.GetFilterParams(params, this.paginateRequestOtherItemSub)).subscribe((data: any) => {
            // if(isValidRequestOtherItemSubListResponse(res)) {
            if (data) {
                // Set value
                pThis.listData(data)
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickRequestOtherItemSubAdd(): void {
        this.listRequestOtherItemSub.push({
            request_date_text: null,
            request_number: null,
            requester_name: null,
            request_type_code: null,
            item_sub_type_1_count: null,
            total_price: null,
            created_by_name: null,
            created_date_text: null,
            reference_number: null,
            receipt_status_name: null,
            people_name: null,
            cancel_reason: null,

        })
        this.changePaginateTotal(this.listRequestOtherItemSub.length, 'paginateRequestOtherItemSub')
    }

    onClickRequestOtherItemSubEdit(item: any): void {
        if (item.eform_id) {
            var win = window.open("request-other-eform/edit/" + item.request_other_process_id)
        } else {
            var win = window.open("registration-other-request/edit/" + item.request_other_process_id)
        }
    }


    onClickRequestOtherItemSubDelete(item: any): void {
        // if(this.validateRequestOtherItemSubDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listRequestOtherItemSub.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listRequestOtherItemSub.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listRequestOtherItemSub.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {

                let ids = []

                for (let i = 0; i < this.listRequestOtherItemSub.length; i++) {
                    if (this.listRequestOtherItemSub[i].is_check) {
                        this.listRequestOtherItemSub[i].cancel_reason = rs
                        this.listRequestOtherItemSub[i].status_code = "DELETE"

                        if (true && this.listRequestOtherItemSub[i].id) ids.push(this.listRequestOtherItemSub[i].id)
                        else this.listRequestOtherItemSub.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callRequestOtherItemSubDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listRequestOtherItemSub.length; i++) {
                    this.listRequestOtherItemSub[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callRequestOtherItemSubDelete(params: any, ids: any[]): void {
        this.RequestProcessService.RequestOtherItemSubDelete(params).subscribe((data: any) => {
            // if(isValidRequestOtherItemSubDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listRequestOtherItemSub.length; i++) {
                    if (this.listRequestOtherItemSub[i].id == id) {
                        this.listRequestOtherItemSub.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listRequestOtherItemSub.length; i++) {
                this.listRequestOtherItemSub[i] = i + 1
            }

            this.onClickRequestOtherItemSubList()
            // Close loading
            this.global.setLoading(false)
        })
    }



    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listRequestOtherItemSub = data.requestotheritemsub_list || []
        let index = 1
        index = 1
        this.listRequestOtherItemSub.map((item: any) => { item.is_check = false; item.index = index++; return item })
        this.changePaginateTotal(this.listRequestOtherItemSub.length, 'paginateRequestOtherItemSub')

    }

    listData(data: any): void {
        this.listRequestOtherItemSub = data.list || []
        this.help.PageSet(data, this.paginateRequestOtherItemSub)

        //this.listRequestOtherItemSub = data || []
        //let index = 1
        //index = 1
        //this.listRequestOtherItemSub.map((item: any) => { item.is_check = false; item.index = index++; return item })
        //this.changePaginateTotal(this.listRequestOtherItemSub.length, 'paginateRequestOtherItemSub')

    }

    saveData(): any {
        let params = {
            created_by_name: this.input.created_by_name,
            request_date: this.input.request_date,
            reference_number: this.input.reference_number,
            request_number: this.input.request_number,
            receipt_status_code: this.input.receipt_status_code,
        }
        //params.requestotheritemsub_list = this.listRequestOtherItemSub || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxBinding(name: any, value: any, object: any): void {
        if (object) {
            object[name] = value == "on"
        } else {
            this.input[name] = value == "on"
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
