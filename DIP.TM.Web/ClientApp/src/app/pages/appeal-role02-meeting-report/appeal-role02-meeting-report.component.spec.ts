import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealRole02MeetingReportComponent } from './appeal-role02-meeting-report.component';

describe('AppealRole02MeetingReportComponent', () => {
  let component: AppealRole02MeetingReportComponent;
  let fixture: ComponentFixture<AppealRole02MeetingReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppealRole02MeetingReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealRole02MeetingReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
