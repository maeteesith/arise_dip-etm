import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-consider-abandon-petition',
  templateUrl: './consider-abandon-petition.component.html',
  styleUrls: ['./consider-abandon-petition.component.scss']
})
export class ConsiderAbandonPetitionComponent implements OnInit {

  constructor() { }

  //TODO >>> Declarations <<<
  // Init
  public input: any;
  public validate: any;
  public master: any;
  public modal: any;

  public tab_menu_show_index: any;
  public tab_menu_show_index2: any

  public tableRequestDetail: any;

  // Start บันทึกเสนอนายทะเบียน
  public tableViewDocument: any;
  public tableInfo: any;
  public tableIssueBook: any;
  // End บันทึกเสนอนายทะเบียน

  ngOnInit() {
    this.input = {};
    this.validate = {};
    this.master = {
      checkList: [
        { code: "1", name: "หนังสือแจ้งคำสั่ง" },
        { code: "2", name: "การปฏิบัติตามคำสั่ง" },
        { code: "3", name: "การชำระค่าธรรมเนียม" },
        { code: "4", name: "สถานที่ติดต่อ" },
        { code: "5", name: "การขอถอน" },
        { code: "6", name: "ใบตอบรับ / ครบกำหนด" },
        { code: "7", name: "มีอุทรณ์ / คำวินิจฉัยฯ" },
      ],

      // Start บันทึกเสนอนายทะเบียน
      typeCodeList: [
        { code: "1", name: "ประเภท 1" },
        { code: "2", name: "ประเภท 2" },
      ],
      typeCodeList2: [
        { code: "1", name: "รอบที่ 1" },
        { code: "2", name: "รอบที่ 2" },
      ],
      typeCodeList3: [
        { code: "1", name: "อนุญาต" },
        { code: "2", name: "รวมเรื่อง" },
        { code: "3", name: "ไม่อนุญาต" },
      ],
      typeCodeList4: [
        { code: "1", name: "ยังไม่หลุด" },
        { code: "2", name: "ยังไม่หลุด" },
        { code: "3", name: "ยังไม่หลุด" },
      ],
      typeCodeList5: [
        { code: "1", name: "เจ้าของ" },
        { code: "2", name: "ตัวแทน" },
        { code: "3", name: "ตัวแทนช่วง" },
        { code: "4", name: "อื่นๆ" },
        { code: "5", name: "ตามก.ที่ยื่น" },
      ],
      typeCodeList6: [
        { code: "1", name: "ต.ค.5 ข้อ 8" },
        { code: "2", name: "ต.ค.9 ข้อ 10" },
        { code: "3", name: "ต.ค.9 ข้อ 2" },
        { code: "4", name: "ต.ค.9 ข้อ 3" },
      ],
      // End บันทึกเสนอนายทะเบียน
    }

    this.tab_menu_show_index = 1
    this.tab_menu_show_index2 = 1

    this.tableRequestDetail = {
      head_column_list: [
        {
          order: "#",
          command: "คำสั่ง",
          date: "วันที่สั่ง",
          registrar: "นายทะเบียน",
          book_status: "สถานะหนังสือ",
          date_order: "วันที่ส่งคำสั่ง",
          no: "เลข พณ.",
          worker: "ผู้รับงาน",
          note: "หมายเหตุ",
          status: "สถานะ"
        },
      ],

      column_list: [
        {
          command: "ตค. 4 (ม.20)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
        {
          command: "ตค. 2 (ม.13)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
        {
          command: "ตค. 1 (ม.11)",
          date: "11/10/2562",
          registrar: "วีรยุทธ ศรีนักสิทธ",
          book_status: "ปกติ",
          date_order: "11/10/2562",
          no: "0704/27759",
          worker: "",
          note: "",
          status: "ordered"
        },
      ]
    }

    // Start บันทึกเสนอนายทะเบียน
    this.tableViewDocument = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }
    this.tableInfo = {
      column_list: [
        {
          detail: "ม.7"
        },
        {
          detail: "ม.9"
        },
        {
          detail: "ม.7"
        },
      ]
    }
    this.tableIssueBook = {
      column_list: [
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
        {
          date: "10/10/2550"
        },
      ]
    }
    // End บันทึกเสนอนายทะเบียน

    this.modal = {
      isModalEditOpen: false,
      isModalExampleOpen: false,
    };
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
