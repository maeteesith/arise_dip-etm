import { Component, OnInit,ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-mm02',
  templateUrl: './mm02_01-4-modal.component.html',
  
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./mm02_01-4-modal.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class Mm02_01_4_modalComponent implements OnInit {

  constructor() { }

  ItemArray : any = [];

  Item: any;
  ngOnInit() {
  }

  
  addItem(){
    if(this.Item == null)
      return

    this.ItemArray.push(this.Item);
    this.Item = null;
  }

  deleteItem(index : any){
    this.ItemArray.splice(index, 1)
  }

}
