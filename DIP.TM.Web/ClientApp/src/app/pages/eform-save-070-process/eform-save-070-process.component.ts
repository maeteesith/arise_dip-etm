import { Component, OnInit, HostListener } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { GlobalService } from "../../global.service";
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from "../../services/fork-join2.service";
import { eFormSaveProcessService } from "../../services/eform-save-process-buffer.service";
import { AutomateTest } from "../../test/automate_test";
import {
  CONSTANTS,
  ROUTE_PATH,
  validateService,
  clone,
  displayMoney,
  displayFormatBytes,
  getMoment,
  displayDate,
  viewPDF,
  displayAddress,
  getParamsOverwrite
} from "../../helpers";

@Component({
  selector: "app-eform-save-070-process",
  templateUrl: "./eform-save-070-process.component.html",
  styleUrls: ["./eform-save-070-process.component.scss"],
})
export class eFormSave070ProcessComponent implements OnInit, DeactivationGuarded {
  // Init
  public menuList: any[];
  public requestItemTypeCodeList: any[];
  public userList: any[];
  public userList2: any[];
  public itemList: any[];
  public itemList1: any[];
  public input: any;
  public validate: any;
  public saveInput: any;
  public master: any;
  public modal: any;
  public validateObj: any;
  public listdropdownContact: any;
  public ContactWord: any;
  public contactAddress: any;
  public editID: any;
  public eform_number: any;
  public popup: any;
  public contactChangeType: string;
  public response: any
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubstep: number;
  public progressPercent: number;
  public stepPass: number;
  // Other
  public isSubmited: any;
  public isValid: any;
  public conjunction: string;
  public findVal: any;
  public email: string;
  public telephone: string;
  public rule_number: string;

  //table

  public currentP1: number;
  public perpageP1: number;
  public totalP1: number;

  public currentP2: number;
  public perpageP2: number;
  public totalP2: number;

  public type1: any;
  public type2: any;
  public type3: any;

  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eFormSaveProcessService: eFormSaveProcessService,
    private automateTest: AutomateTest
  ) {}

  ngOnInit() {
    this.popup = {
      warning_message_show_list: [],
      isPopupDeactivation: false,
      isConfirmSave: false,
      isSearch: false,
      isWarning: false,
    };

    this.response = {
      load: {},
    };

    this.menuList = [
      {
        id: 1,
        number: 1,
        name: "บันทึกเพื่อรับไฟล์",
        canedit: false,
      },
      {
        id: 2,
        number: 2,
        name: "คำขอต่ออายุการจดทะเบียนเครื่องหมายการค้า",
        required: true,
        canedit: false,
      },
      {
        id: 3,
        number: 3,
        name: "ประเภทการยื่น",
        required: true,
        canedit: false,
      },
      {
        id: 4,
        number: 4,
        name: "เจ้าของ/ตัวแทน",
        required: true,
        canedit: false,
      },
      {
        id: 5,
        number: 5,
        name: "สถานที่ติดต่อภายในประเทศไทย",
        canedit: false,
      },
      {
        id: 6,
        number: 6,
        name: "จำพวกและรายการสินค้าและบริการ (ก่อนต่ออายุ)",
        canedit: false,
      },
      {
        id: 7,
        number: 7,
        name: "ค่าธรรมเนียม",
        canedit: false,
      },
      {
        id: 8,
        number: 8,
        name: "เสร็จสิ้น",
        canedit: false,
      },
    ];

    this.requestItemTypeCodeList = [
      {
        code: 1,
        name: "เครื่องหมายการค้า",
      },
      {
        code: 2,
        name: "เครื่องหมายรับรอง",
      },
      {
        code: 3,
        name: "เครื่องหมายร่วม",
      },
    ];

    this.userList = [
      {
        id: 1,
        name: "nanpipat klinpratoom",
        address: "samutprakarn",
        tel: "0858455652",
      },
      {
        id: 2,
        name: "toptop  toptopt",
        address: "asdsadas",
        tel: "08900",
      },
    ];

    this.userList2 = [
      {
        id: 1,
        name: "nanpipat klinpratoom",
        address: "samutprakarn",
        tel: "0858455652",
      },
    ];

    this.itemList = [
      {
        item_sub_type_1_code: 1,
        name: "A",
        amount_product: "1000",
      },
      {
        item_sub_type_1_code: 2,
        name: "A",
        amount_product: "1000",
      },
      {
        item_sub_type_1_code: 3,
        name: "A",
        amount_product: "1000",
      },
      {
        item_sub_type_1_code: 4,
        name: "A",
        amount_product: "1000",
      },
      {
        item_sub_type_1_code: 3,
        name: "A",
        amount_product: "1000",
      },
    ];

    this.itemList1 = [];

    this.input = {
      indexEdit: undefined,
      listOwnerMark: [],
      product_list: [],
      people_list: [],
      representative_list: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      ownerMarkItem2: {},
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      ownerDomesticContactAddress: 0,
      agentDomesticContactAddress: 0,
      subagentDomesticContactAddress: 0,
      save070_submit_type_code: null,
      save070_extend_type_code: null,
      rule_number: "",
      save070_representative_condition_type_code: "AND_OR",
      dimension_image: 2,
      countryVal: "1",
      personalTypeVal: "1",
      agent_personal_typeVal: "1",
      agent_countryVal: "1",
      make_date: "",
      payer_name: "",
      fee: "",
      fine: "",
      total_price: "",
      request_number: null,
      register_number: "",
      imageDimension2: {
        file: {},
        blob: null,
      },
      imageDimension3_1: {
        file: {},
        blob: null,
      },
      imageDimension3_2: {
        file: {},
        blob: null,
      },
      imageDimension3_3: {
        file: {},
        blob: null,
      },
      imageDimension3_4: {
        file: {},
        blob: null,
      },
      sizeX: 5,
      sizeY: 5,
      isResize: false,
      audio: {
        input: "",
        validate: "",
        file: {},
        sound: null,
        palyer: null,
        action: false,
      },
      imageNote: {
        file: {},
        blob: null,
      },
      listTranslation: [],
      translationItem: {},
      listProduct: [],
      productSummary: {
        amount_type: 0,
        amount_product: 0,
        total_price: 0,
      },
      imageMark: {
        file: {},
        blob: null,
      },
      isCheckAllOwnerSignature: false,
      isCheckAllAgentSignature: false,
    };
    this.validate = {};
    this.master = {
      requestSoundTypeList: [],
      inputTypeCodeList: [],
      requestSourceCodeList: [],
      saveOTOPTypeCodeList: [],
      addressCountryCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
      languageCodeList: [],
      priceMasterList: [],
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalOwnerFormOpen2: false,
      isModalAgentFormOpen2: false,
      isModalDomesticContactAddressOpen: true,
    };
    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubstep = 1;
    this.progressPercent = 0;
    this.stepPass = 1;
    // Other
    this.isSubmited = false;
    this.isValid = false;
    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;

    //this.paginateTb1 = 5
    //this.perPageTb1 = 2
    this.currentP1 = 1;
    this.perpageP1 = 1;

    this.currentP2 = 1;
    this.perpageP2 = 1;
    this.calOnchangepage();
    this.calOnchangepage2();

    this.type1 = true;
    this.type2 = true;
    this.type3 = true;

    this.contactAddress = {};
    this.master = {};
    this.callInit();

    this.editID = this.route.snapshot.paramMap.get("id"); //[edit]comment_tag: new
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initeFormSave070Process().subscribe((data: any) => {
      if (data) {
        this.master = data;
        console.log("Master", this.master);
      }

      this.route.queryParams.subscribe((params) => {
        //this.editID = params['eform_number']; //[edit]comment_tag: comment

        console.log("ID", this.editID);
        this.eform_number = { eform_number: this.editID };
      });
      if (this.editID) {
        this.eFormSaveProcessService
          .eFormSave070Load(this.editID)
          .subscribe((data: any) => {
            //[edit]comment_tag: ("", this.eform_number) to (this.editID)
            if (data) {
              // Manage structure
              this.loadData(data);
              this.response.load = {
                eform_number: data.eform_number,
                id: data.id,
                wizard: data.wizard,
              };
              //this.input.request_date_text = "test";

              if (this.input.contact_address_list) {
                this.contactAddress = clone(
                  this.input.contact_address_list[0]
                );
              }

              this.input.save070_representative_condition_type_code = "AND_OR";

              this.input.ownerMarkItem = {
                receiver_type_code: "PEOPLE",
                sex_code: "MALE",
                nationality_code: "AF",
                career_code: "ค้าขาย",
                address_country_code: "TH",
              };

              this.input.agentMarkItem = {
                receiver_type_code: "PEOPLE",
                sex_code: "MALE",
                nationality_code: "AF",
                career_code: "ค้าขาย",
                address_country_code: "TH",
              };

              this.input.isCheckAllOwnerSignature =
                data.sign_inform_person_list === "0" ? true : false;
              this.input.isCheckAllAgentSignature =
                data.sign_inform_representative_list === "0" ? true : false;
              
              this.setSignInform(data.sign_inform_representative_list);
            }
            // Close loading
            this.global.setLoading(false);
            this.automateTest.test(this);
          });
      } else {
        this.global.setLoading(false);
        this.automateTest.test(this);
      }
    });
  }

  loadData(data: any): void {
    this.input = data;
    console.log("data", this.input);
  }

  conditionTable(name) {
    if (name == "con1") {
      this.type1 = true;
      this.type2 = true;
      this.type3 = false;
    } else if (name == "con2") {
      this.type1 = true;
      this.type2 = false;
      this.type3 = false;
    } else if (name == "con3") {
      this.type1 = false;
      this.type2 = true;
      this.type3 = false;
    } else if (name == "con4") {
      this.type1 = false;
      this.type2 = false;
      this.type3 = false;
    }
  }

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any, item: any): void {
    if (item) {
      this[obj][item][name] = value;
    } else {
      this[obj][name] = value;
    }
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name];
  }
  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }
  toggleBooleanInTable(item: any, name: any): void {
    item[name] = !item[name];
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null;
        }
      });
    } else {
      this.validate[name] = null;
    }
  }

  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    }

    if (this.currentID === 4) {
      if (this.input.people_list.length < 1) {
        console.warn(`step4 is invalid.`);
        this.validate.step4 = "กรุณาเพิ่มเจ้าของเครื่องหมาย";
        return false;
      }
      return true;
    }

    if (this.currentStep === 6) {
      if (this.input.save070_contact_type_code == "OTHERS") {
        this.validateObj = {
          nameS6: this.contactAddress.name,
          house_numberS6: this.contactAddress.house_number,
          postal_codeS6: this.contactAddress.postal_code,
          emailS6: this.contactAddress.email,
          telephoneS6: this.contactAddress.telephone,
          address_sub_district_nameS6: this.contactAddress
            .address_sub_district_name,
        };
        let result = validateService(
          "validateEFormSave07ProcessStep5",
          this.validateObj
        );
        this.validate = result.validate;
        return result.isValid;
      }
    }

    if (this.currentID === 8) {
      let isValid = false;

      if (
        this.input.isCheckAllOwnerSignature ||
        this.input.isCheckAllAgentSignature
      ) {
        isValid = true;
      }

      if (this.input.representative_list.length > 0) {
        this.input.representative_list.forEach((item: any) => {
          if (item.isCheck) {
            isValid = true;
          }
        });
      }

      if (!isValid) {
        console.warn(`step8 is invalid.`);
        this.validate.step8 = "กรุณาเลือกลงลายมือชื่อ";
      } else {
        // Set can edit
        this.menuList[7].canEdit = true;
      }

      return isValid;
    }

    return true;
  }

  //! <<< Wizard >>>
  changeStep(action: string): void {
    if (this.validateWizard()) {
      console.log(
        "currentStep = ",
        this.currentStep,
        "currentID = ",
        this.currentID,
        "stepPass = ",
        this.stepPass
      );
      this.menuList[this.currentStep - 1].canedit = true;

      action === "next" ? this.currentStep++ : this.currentStep--;
      this.currentID = this.menuList[this.currentStep - 1].id;
      if (this.stepPass == this.currentStep - 1)
        this.stepPass = this.stepPass + 1;
      console.log(this.stepPass);
      this.currentSubstep = 1;
      this.calcProgressPercent(this.currentStep);
    }
  }
  changeSubStep(action: number): void {
    if (this.validateWizard()) {
      this.currentSubstep = this.currentSubstep + action;
    }
  }

  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    console.log("validate", this.validateSaveItemModal(nameItem));
    if (this.validateSaveItemModal(nameItem)) {
      console.log("indexEdit", this.input.indexEdit);
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        console.log("people_list", this[obj][nameList]);
        this.toggleModal(nameModal);
      }

      alert(
        "กรณีที่ทีการแก้ไขเปลี่ยนแปลงข้อมูลเจ้าของ/ตัวแทน ให้ยื่น ก.06 แนบมาด้วย"
      );
    }
  }

  contactTypeChange(): void {
    //this.Next4 = true;
    switch (this.input.save070_contact_type_code) {
      case "OWNER":
        this.listdropdownContact = this.input.people_list;
        this.ContactWord = "ใช้ที่อยู่ของเจ้าของลำดับที่";
        this.contactChangeType = "OWNER";
        break;
      case "REPRESENTATIVE":
        this.listdropdownContact = this.input.representative_list;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนลำดับที่";
        this.contactChangeType = "REPRESENTATIVE";
        break;
      case "REPRESENTATIVE_PERIOD":
        this.listdropdownContact = this.input.representative_list;
        this.ContactWord = "ใช้ที่อยู่ของตัวแทนช่วงลำดับที่";
        this.contactChangeType = "REPRESENTATIVE_PERIOD";
        break;
      default:
        break;
    }

    this.input.save070_contact_index = 0;
    this.contactChange(this.input.save070_contact_type_code, this.input.save070_contact_index);
  }

  validateSaveItemModal(nameItem: any): boolean {
    if (nameItem === "ownerMarkItem") {
      if (this.input.ownerMarkItem.address_country_code != "TH") {
        this.validateObj = {
          receiver_type_code: this.input.ownerMarkItem.receiver_type_code,
          card_number: this.input.ownerMarkItem.card_number,
          name: this.input.ownerMarkItem.name,
          sex_code: this.input.ownerMarkItem.sex_code,
          nationality_code: this.input.ownerMarkItem.nationality_code,
          career_code: this.input.ownerMarkItem.career_code,
          address_country_code: this.input.ownerMarkItem.address_country_code,
          house_number: this.input.ownerMarkItem.house_number,
          address_sub_district_name: "not validate",
          postal_code: "not validate",
          email: this.input.ownerMarkItem.email,
          telephone: this.input.ownerMarkItem.telephone,
        };
      } else {
        this.validateObj = {
          receiver_type_code: this.input.ownerMarkItem.receiver_type_code,
          card_number: this.input.ownerMarkItem.card_number,
          name: this.input.ownerMarkItem.name,
          sex_code: this.input.ownerMarkItem.sex_code,
          nationality_code: this.input.ownerMarkItem.nationality_code,
          career_code: this.input.ownerMarkItem.career_code,
          address_country_code: this.input.ownerMarkItem.address_country_code,
          house_number: this.input.ownerMarkItem.house_number,
          address_sub_district_name: this.input.ownerMarkItem
            .address_sub_district_name,
          postal_code: this.input.ownerMarkItem.postal_code,
          email: this.input.ownerMarkItem.email,
          telephone: this.input.ownerMarkItem.telephone,
        };
      }
      let result = validateService(
        "validateEFormSave07ProcessStep5OwnerMarkItem",
        this.validateObj
      );
      this.validate = result.validate;
      return result.isValid;
    }
    if (nameItem === "agentMarkItem") {
      if (this.input.agentMarkItem.address_country_code != "TH") {
        this.validateObj = {
          receiver_type_codeAgent: this.input.agentMarkItem.receiver_type_code,
          card_numberAgent: this.input.agentMarkItem.card_number,
          nameAgent: this.input.agentMarkItem.name,
          sex_codeAgent: this.input.agentMarkItem.sex_code,
          nationality_codeAgent: this.input.agentMarkItem.nationality_code,
          career_codeAgent: this.input.agentMarkItem.career_code,
          address_country_codeAgent: this.input.agentMarkItem
            .address_country_code,
          house_numberAgent: this.input.agentMarkItem.house_number,
          address_sub_district_nameAgent: "not validate",
          postal_codeAgent: "not validate",
          emailAgent: this.input.agentMarkItem.email,
          telephoneAgent: this.input.agentMarkItem.telephone,
        };
      } else {
        this.validateObj = {
          receiver_type_codeAgent: this.input.agentMarkItem.receiver_type_code,
          card_numberAgent: this.input.agentMarkItem.card_number,
          nameAgent: this.input.agentMarkItem.name,
          sex_codeAgent: this.input.agentMarkItem.sex_code,
          nationality_codeAgent: this.input.agentMarkItem.nationality_code,
          career_codeAgent: this.input.agentMarkItem.career_code,
          address_country_codeAgent: this.input.agentMarkItem
            .address_country_code,
          house_numberAgent: this.input.agentMarkItem.house_number,
          address_sub_district_nameAgent: this.input.agentMarkItem
            .address_sub_district_name,
          postal_codeAgent: this.input.agentMarkItem.postal_code,
          emailAgent: this.input.agentMarkItem.email,
          telephoneAgent: this.input.agentMarkItem.telephone,
        };
      }

      let result = validateService(
        "validateEFormSave04ProcessStep5AgentMarkItem",
        this.validateObj
      );
      this.validate = result.validate;
      return result.isValid;
    }

    return true;
  }

  backtomenu(canedit: any, number: any): void {
    console.log(canedit);
    if (canedit == true) {
      this.currentStep = number; //direct to
      this.currentID = this.menuList[this.currentStep - 1].id;

      for (var i = 1; i <= this.menuList.length + 1; i++) {
        if (i <= this.stepPass - 1) {
          this.menuList[i].canedit = true;
        }
      }
    }
  }
  calcProgressPercent(currentStep: number): void {
    this.progressPercent = Math.round(
      (currentStep / this.menuList.length) * 100
    );
  }

  findClick(): void {
    this.findVal = true;
  }

  //! <<< Other >>>
  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }
  displayFormatBytes(value: any): any {
    return displayFormatBytes(value);
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
    }
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    this.currentP1 = page;
    this.calOnchangepage();
  }
  onChangePerPage(value: number, name: string): void {
    this.perpageP1 = value;
    this.calOnchangepage();
  }
  calOnchangepage() {
    this.totalP1 = Math.ceil(this.itemList.length / this.perpageP1);
  }

  onChangePage2(page: any, name: string): void {
    this.currentP2 = page;
    this.calOnchangepage2();
  }
  onChangePerPage2(value: number, name: string): void {
    this.perpageP2 = value;
    this.calOnchangepage2();
  }
  calOnchangepage2() {
    this.totalP2 = Math.ceil(this.itemList.length / this.perpageP2);
  }

  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT;
    paginate.totalItems = total;
    this[name] = paginate;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  listData(data: any): void {
    this.input = data;
    delete this.input.id;
  }

  SearchRequesNumber(): void {
    this.calleFormFromRequestNumberLoad(this.input.request_number);
    this.conjunction = "AND_OR";
  }

  //! <<< Call API >>>
  calleFormFromRequestNumberLoad(params: any): void {
    this.global.setLoading(true);
    this.eFormSaveProcessService
      .eForm070FromRequestNumberLoad(params)
      .subscribe((data: any) => {
        console.log(data);

        // if(isValideFormFromRequestNumberLoadResponse(res)) {
        if (data) {
          // Set value
          this.listData(data);
          this.automateTest.test(this, { list: data.list });
          this.input.telephone = this.telephone;
          this.input.email = this.email;
          this.input.payer_name =
            (data.people_list) ?
            data.people_list[0].name :
            ""
            ;

          this.input.ownerMarkItem = {
            receiver_type_code: "PEOPLE",
            sex_code: "MALE",
            nationality_code: "AF",
            career_code: "ค้าขาย",
            address_country_code: "TH",
          };

          this.input.save070_representative_condition_type_code = data.save070_representative_condition_type_code
            ? data.save070_representative_condition_type_code
            : "AND_OR";

          this.input.receiverMarkItem = {
            receiver_type_code: "PEOPLE",
            sex_code: "MALE",
            nationality_code: "AF",
            career_code: "ค้าขาย",
            address_country_code: "TH",
          };

          this.input.agenReceiverMarkItem = {
            receiver_type_code: "PEOPLE",
            sex_code: "MALE",
            nationality_code: "AF",
            career_code: "ค้าขาย",
            address_country_code: "TH",
          };

          this.input.agentMarkItem = {
            receiver_type_code: "PEOPLE",
            sex_code: "MALE",
            nationality_code: "AF",
            career_code: "ค้าขาย",
            address_country_code: "TH",
          };

          this.popup.warning_message_show_list = [];

          console.log("Expire", this.input.trademark_expired_date);

          if (
            data.trademark_expired_date &&
            data.trademark_expired_date != ""
          ) {
            var trademark_expired_date = getMoment(
              data.trademark_expired_date,
              "YYYY-MM-DDTHH:mm:ss"
            );
            var trademark_expired_start_date = getMoment(
              data.trademark_expired_start_date,
              "YYYY-MM-DDTHH:mm:ss"
            );
            var trademark_expired_end_date = getMoment(
              data.trademark_expired_end_date,
              "YYYY-MM-DDTHH:mm:ss"
            );

            if (getMoment() > trademark_expired_date) {
              var is_show_expired = true;

              if (data.last_extend_date && data.last_extend_date != "") {
                var last_extend_date = getMoment(
                  data.last_extend_date,
                  "YYYY-MM-DDTHH:mm:ss"
                );

                if (last_extend_date > trademark_expired_start_date) {
                  var msg = {
                    message:
                      "เลขคำขอ <b>" +
                      data.request_number +
                      "</b> " +
                      "ทะเบียนเลขที่ <b>" +
                      data.registration_number +
                      "</b> " +
                      "ยังพิจารณาต่ออายุไม่เสร็จ",
                    is_warning: true,
                  };
                  //console.log("warnning_message")
                  //row_item.warnning_message = row_item.warnning_message || ""
                  //if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                  //row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ถึงกำหนดการต่ออายุวันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>"
                  this.popup.warning_message_show_list.push(msg);
                  is_show_expired = false;
                }
              }

              if (is_show_expired) {
                var msg = {
                  message:
                    "เลขคำขอ <b>" +
                    data.request_number +
                    "</b> " +
                    "ทะเบียนเลขที่ <b>" +
                    data.registration_number +
                    "</b> " +
                    "จะสิ้นสุดอายุในวันที่ <b>" +
                    displayDate(trademark_expired_date) +
                    "</b><br />" +
                    "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
                    "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" +
                    displayDate(trademark_expired_start_date) +
                    "</b>) <br /> " +
                    "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" +
                    displayDate(trademark_expired_end_date) +
                    "</b>)<br />" +
                    "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
                  is_warning: true,
                };
                //console.log("warnning_message")
                //row_item.warnning_message = row_item.warnning_message || ""
                //if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
                //row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ถึงกำหนดการต่ออายุวันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>"
                this.popup.warning_message_show_list.push(msg);
              }
              //var msg = {
              //  message: "เลขคำขอ <b>" + item.request_number + "</b> " +
              //    "ทะเบียนเลขที่ <b>" + item.registration_number + "</b> " +
              //    "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(trademark_expired_date) + "</b><br />" +
              //    "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
              //    "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>) <br /> " +
              //    "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(trademark_expired_end_date) + "</b>)<br />" +
              //    "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
              //  is_warning: true,
              //}
              //console.log("warnning_message")
              //row_item.warnning_message = row_item.warnning_message || ""
              //if (row_item.warnning_message && row_item.warnning_message != "") row_item.warnning_message += "<br />";
              //row_item.warnning_message += "เลขคำขอ <b>" + item.request_number + "</b> ยังไม่ถึงกำหนดการต่ออายุวันที่ <b>" + displayDate(trademark_expired_start_date) + "</b>"
              //pThis.popup.warning_message_show_list.push(msg)
              this.global.setLoading(false);
              return;
            }
          }
          //  if (this.input.trademark_expired_date)
          //  {

          //    let date: Date;
          //    let expired_date: Date;
          //    date = new Date();
          //    expired_date = new Date(this.input.trademark_expired_date);

          //    console.log("date", expired_date.setDate(expired_date.getMonth() + 6), expired_date.setDate(expired_date.getMonth()));

          //    if (expired_date < date && (expired_date.setMonth(6)) > date.getMonth()) {
          //      swal.fire({
          //        title: "แจ้งเตือน",
          //        html: "เลขคำขอ <b>" + this.input.request_number + "</b> " +
          //          "ทะเบียนเลขที่ <b>" + this.input.registration_number + "</b> " +
          //          "จะสิ้นสุดอายุในวันที่ <b>" + displayDate(this.input.trademark_expired_date) + "</b><br />" +
          //          "หากท่านประสงค์จะต่ออายุการจดทะเบียนเครื่องหมายดังกล่าว <br />" +
          //          "จะต้องยื่นคำขอต่ออายุภายในสามเดือนก่อนวันสิ้นอายุ (วันที่ <b>" + displayDate(this.input.trademark_expired_start_date) + "</b>) <br /> " +
          //          "หรือภายในหกเดือนนับแต่วันสิ้นอายุ (วันที่ <b>" + displayDate(this.input.trademark_expired_end_date) + "</b>)<br />" +
          //          "กรณียื่นขอต่ออายุภายหลังวันสิ้นอายุผู้ขอจะต้องชำระค่าธรรมเนียมร้อยละยี่สิบของจำนวนค่าธรรมเนียมการต่อ",
          //        icon: "warning",
          //      });

          //    } else if (this.input.trademark_expired_date.setMonth(this.input.trademark_expired_date.getMonth() + 6) < date)
          //    {
          //      this.input.request_date_text = null;

          //      swal.fire({
          //        title: "แจ้งเตือน",
          //        html: "หมดอายุเกิน 6 เดือน : ไม่สามารถยื่นต่ออายุได้ เนื่องจากเกินระยะเวลากำหนด",
          //        icon: "warning",
          //      });
          //    }
          //  }
          //  //this.input.receiver_people_list = [];
          //  //this.input.receiver_representative_list = [];

          //} else {
          //  swal.fire({
          //    title: "แจ้งเตือน",
          //    html: "Data not found.",
          //    icon: "warning",
          //  });
          //}
          this.global.setLoading(false);
        }
        // }
        // Close loading
      });
  }

  //! <<< Event >>>
  // displayAddress(item: any): string {
  //   return `${item.house_number ? item.house_number + " " : ""}${
  //     item.village_number ? item.village_number + " " : ""
  //   }${item.alley ? item.alley + " " : ""}${
  //     item.street ? item.street + " " : ""
  //   }${
  //     item.address_sub_district_name ? item.address_sub_district_name + " " : ""
  //   }`;
  // }

  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
  }

  contactChange(contactType: string, index: number): void {
    console.log("index", index);

    this.input.people_list.forEach((data) => {
      data.is_contact_person = false;
    });

    this.input.representative_list.forEach((data) => {
      data.is_contact_person = false;
    });

    if (contactType == "OWNER") {
      this.input.people_list[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE") {
      this.input.representative_list[index].is_contact_person = true;
    }

    if (contactType == "REPRESENTATIVE_PERIOD") {
      this.input.representative_list[index].is_contact_person = true;
    }
  }

  getParamsSave(): void {
    this.saveInput = {};
    console.log("SeeInput", this.input);
    this.saveInput.id = this.response.load.id ? this.response.load.id : null;
    this.saveInput.eform_number = this.response.load.eform_number
        ? this.response.load.eform_number
        : null;
    this.saveInput.email = this.input.email;
    this.saveInput.telephone = this.input.telephone;
    this.saveInput.register_date = this.input.request_date_text;
    this.saveInput.request_number = this.input.request_number;
    this.saveInput.register_number = this.input.registration_number;
    this.saveInput.rule_number = this.rule_number;

    this.saveInput.save070_submit_type_code = this.input.save070_submit_type_code;
    this.saveInput.save070_extend_type_code = this.input.save070_extend_type_code;
    this.saveInput.people_list = [];
    this.saveInput.representative_list = [];
    this.saveInput.contact_address_list = [];
    this.saveInput.receiver_people_list = [];
    this.saveInput.receiver_representative_list = [];
    this.saveInput.save040_contact_type_code = this.input.save040_contact_type_code;
    this.saveInput.save040_receiver_contact_type_code = this.input.save040_receiver_contact_type_code;
    this.saveInput.save040_transfer_form_code = this.input.save040_transfer_form_code;
    this.saveInput.save040_transfer_part_code = this.input.save040_transfer_part_code;
    this.saveInput.save040_receiver_contact_index = this.input.save040_receiver_contact_index;
    this.saveInput.save040_contact_index = this.input.save040_contact_index;
    this.saveInput.kor_17_list = [];
    this.saveInput.payer_name = this.input.payer_name;
    this.saveInput.total_price = this.input.total_price;
    this.saveInput.save070_representative_condition_type_code = this.input.save070_representative_condition_type_code;

    this.saveInput.contact_address_list.push({
      name: this.contactAddress.name,
      house_number: this.contactAddress.house_number,
      village_number: this.contactAddress.village_number,
      alley: this.contactAddress.alley,
      street: this.contactAddress.street,
      address_sub_district_name: this.contactAddress.address_sub_district_name,
      postal_code: this.contactAddress.postal_code,
      email: this.contactAddress.email,
      telephone: this.contactAddress.telephone,
      fax: this.contactAddress.fax,
    });

    this.input.people_list.forEach((obj) => {
      this.saveInput.people_list.push({
        address_type_code: obj.address_type_code,
        nationality_code: obj.nationality_code,
        career_code: obj.career_code,
        card_type_code: obj.card_type_code,
        card_type_name: obj.card_type_name,
        card_number: obj.card_number,
        receiver_type_code: obj.receiver_type_code,
        name: obj.name,
        house_number: obj.house_number,
        village_number: obj.village_number,
        alley: obj.alley,
        street: obj.street,
        address_sub_district_code: obj.address_sub_district_code,
        address_district_code: obj.address_district_code,
        address_province_code: obj.address_province_code,
        postal_code: obj.postal_code,
        address_country_code: obj.address_country_code,
        telephone: obj.telephone,
        fax: obj.fax,
        email: obj.email,
        sex_code: obj.sex_code,
        is_contact_person: obj.is_contact_person,
      });
    });

    this.input.representative_list.forEach((obj) => {
      this.saveInput.representative_list.push({
        address_type_code: obj.address_type_code,
        nationality_code: obj.nationality_code,
        career_code: obj.career_code,
        card_type_code: obj.card_type_code,
        card_type_name: obj.card_type_name,
        card_number: obj.card_number,
        receiver_type_code: obj.receiver_type_code,
        name: obj.name,
        house_number: obj.house_number,
        village_number: obj.village_number,
        alley: obj.alley,
        street: obj.street,
        address_sub_district_code: obj.address_sub_district_code,
        address_district_code: obj.address_district_code,
        address_province_code: obj.address_province_code,
        postal_code: obj.postal_code,
        address_country_code: obj.address_country_code,
        telephone: obj.telephone,
        fax: obj.fax,
        email: obj.email,
        sex_code: obj.sex_code,
        is_contact_person: obj.is_contact_person,
      });
    });

    this.saveInput.sign_inform_person_list = this.input.isCheckAllOwnerSignature ? "0" : "";
    this.saveInput.sign_inform_representative_list =
        this.input.isCheckAllAgentSignature &&
        this.input.save070_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform();
  }

  onClickViewPdfKor07(): void {
    if (this.validateWizard()) {
      this.getParamsSave();
      console.log(this.saveInput);
      //viewPDF("ViewPDF/TM07", this.saveInput);
    }
  }

  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }

  save(isOverwrite: boolean): void {
    //Open loading
    this.global.setLoading(true);
    // Set param
    this.getParamsSave();
    if (!isOverwrite) {
      this.saveInput = getParamsOverwrite(this.saveInput);
    }

    //Call api
    this.calleFormSave070Save(this.saveInput);
  }

  calleFormSave070Save(params: any): void {
    this.global.setLoading(true)
    this.eFormSaveProcessService.eFormSave070Save(params).subscribe((data: any) => {
      if (data) {
        this.isDeactivation = true;
        // Open toast success
        let toast = CONSTANTS.TOAST.SUCCESS;
        toast.message = "บันทึกข้อมูลสำเร็จ";
        this.global.setToast(toast);
        // Navigate
        this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
      }else {
        // Close loading
        this.global.setLoading(false);
      }      
    })
  }

  sendEmail(): void {
    this.global.setLoading(true);
    this.getParamsSave();
    
    console.log("SaveInput", this.saveInput);

    this.eFormSaveProcessService.eFormSave070Save(this.saveInput).subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
    })
  }

  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
    }

    if (nameList === "people_list" || nameList === "representative_list") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! <<< Other >>>
  displayAddress(value: any): any {
    return displayAddress(value);
  }
  getSignInform(): String {
    let result = "";
    this.input.representative_list.forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input.representative_list.forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save070_representative_condition_type_code != "AND") {
        this.input.representative_list.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }
}
