import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { DialogService } from '../../services/dialogService'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material'

@Component({
  selector: 'app-madrid-role04-payment',
  templateUrl: './madrid-role04-payment.component.html',
  styleUrls: ['./madrid-role04-payment.component.scss',
    './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole04PaymentComponent implements OnInit {

  /**
  * Component constructor
  *
  * @param dialogRef: MatDialogRef<RoleEditDialogComponent>
  * @param data: any
  */

  constructor(private help: Help,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<MadridRole04PaymentComponent>,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private _dialog: DialogService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;
    this.dialogRef.backdropClick().subscribe(_ => {
      // let cn = confirm('Do you want to close this modal without save ? ')
      // if (cn) {
      //   this.dialogRef.close();
      // }
    })
  }

  closeModal() {
    this.dialogRef.close();
  }

}
