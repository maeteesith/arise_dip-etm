import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-document-role05-check-list",
    templateUrl: "./document-role05-check-list.component.html",
    styleUrls: ["./document-role05-check-list.component.scss"]
})
export class DocumentRole05CheckListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    // List DocumentRole05Check
    public listDocumentRole05Check: any[]
    public paginateDocumentRole05Check: any
    public perPageDocumentRole05Check: number[]
    // Response DocumentRole05Check
    public responseDocumentRole05Check: any
    public listDocumentRole05CheckEdit: any


    // List DocumentProcess
    public listDocumentProcess: any[]
    public paginateDocumentProcess: any
    public perPageDocumentProcess: number[]
    // Response DocumentProcess
    public responseDocumentProcess: any

    //label_save_combobox || label_save_radio
    public postRoundTypeCodeList: any[]
    public departmentGroupCodeList: any[]
    public documentRole05StatusCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial

    public tableList: any


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            //post_round_document_post_date: getMoment(),
            post_round_type_code: '',
            department_group_code: '',
            document_Role05_status_code: '',
            request_number: '',
        }
        this.listDocumentRole05Check = []
        this.paginateDocumentRole05Check = CONSTANTS.PAGINATION.INIT
        this.perPageDocumentRole05Check = CONSTANTS.PAGINATION.PER_PAGE

        this.listDocumentProcess = []

        //Master List
        this.master = {
            postRoundTypeCodeList: [],
            departmentGroupCodeList: [],
            documentRole05StatusCodeList: [],
        }
        //Master List


        if (!this.tableList) {
            this.tableList = {
                column_list: {
                    index: "#",
                    request_number: "เลขที่คำขอ",
                    book_end_date: "วันที่ส่งมา",
                    document_role04_from_department_name: "กลุ่มงานที่ส่งมา",
                    document_role04_receive_send_date: "วันที่เสนอพิจารณา",
                    document_role04_receiver_by_name: "เจ้าหน้าที่",
                    document_role05_receive_send_date: "วันที่พิจารณา",
                    document_role04_receive_remark: "รายละเอียดเพิ่มเติม",
                    document_role05_receive_status_name: "สถานะ",
                },
                command_item: [{
                    name: "item_edit"
                }],
            }
        }


        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //


        this.contactAddress = {
            address_type_code: "OWNER"
        }
        this.modalAddress = {}

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.route.queryParams.subscribe((param_url: any) => {
        })

        this.forkJoinService.initDocumentRole05CheckList().subscribe((data: any) => {
            if (data) {
                this.help.Clone(data, this.master)
                //this.master.documentRole4TypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.document_role05_type_code = ""
                this.master.documentRole05ReceiveStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.document_role05_receive_status_code = ""
                //this.master.departmentGroupCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.department_group_code = ""
                //this.master.documentRole05StatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                //this.input.document_Role05_status_code = ""

            }
            //if (this.editID) {
            //  this.DocumentProcessService.DocumentRole05CheckLoad(this.editID).subscribe((data: any) => {
            //    if (data) {
            //      // Manage structure
            //      this.loadData(data)
            //    }
            //    // Close loading
            this.global.setLoading(false)
            //    this.automateTest.test(this)
            //  })
            //} else {
            //  this.global.setLoading(false)
            //  this.automateTest.test(this)
            //}

        })
    }

    constructor(
        private auth: Auth,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }

    onClickCommand($event) {
        console.log($event)
        if ($event) {
            if ($event.command) {
                if ($event.command == "item_edit") {
                    window.open("document-role05-check/edit/" + $event.object_list[0].id)
                }
            } else {
                this.onClickDocumentRole05CheckList($event)
            }
        }
    }
    onClickDocumentRole05CheckList(paging = null): void {
        console.log(paging)
        // if(this.validateDocumentRole05CheckList()) {
        // Call api
        var param = {}

        Object.keys(this.tableList.column_list).forEach((item: any) => {
            console.log(item)
            if (this.input[item]) {
                console.log(item)
                param[item] = this.input[item]
            }

            if (item.endsWith("_name")) {
                var code = item.replace("_name", "_code")
                if (this.input[code]) {
                    param[code] = this.input[code]
                }
            }

            if (item.endsWith("_date")) {
                var start_date = item.replace("_date", "_start_date")
                if (this.input[start_date]) {
                    param[start_date] = this.input[start_date]
                }
                var end_date = item.replace("_date", "_end_date")
                if (this.input[end_date]) {
                    param[end_date] = this.input[end_date]
                }
            }
        })


        var params = this.help.GetFilterParams(param, paging)
        params.filter_queries = ["!string.IsNullOrEmpty(document_role05_receive_status_code)"];

        this.callDocumentRole05CheckList(params)
        //this.callDocumentRole05CheckList(this.help.GetFilterParams(param, paging))
        // }
    }
    //! <<< Call API >>>
    callDocumentRole05CheckList(params: any): void {
        this.global.setLoading(true)
        this.DocumentProcessService.List("DocumentRole04", params).subscribe((data: any) => {
            // if(isValidDocumentRole05CheckListResponse(res)) {
            if (data) {
                // Set value
                //this.tableList.SetPaging(data)
                this.tableList.SetDataList(data)
                //this.listData(data)
                this.automateTest.test(this, { list: data.list })
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickDocumentRole05CheckDocumentRole05CheckAdd(): void {
        this.listDocumentRole05Check.push({
            index: this.listDocumentRole05Check.length + 1,
            request_number: null,
            request_date_text: null,
            request_item_sub_type_1_code_text: null,
            document_Role05_date_text: null,
            department_group_name: null,
            book_by_name: null,
            document_Role05_receive_date_text: null,
            document_Role05_receiver_by_name: null,
            document_Role05_remark: null,
            document_Role05_status_name: null,

        })
        this.changePaginateTotal(this.listDocumentRole05Check.length, 'paginateDocumentRole05Check')
    }

    onClickDocumentRole05CheckDocumentRole05CheckEdit(item: any): void {
        var win = window.open("/" + item.id)
    }

    //onClickDocumentRole05CheckAutoSplit(): void {
    //  this.DocumentProcessService.DocumentRole05CheckAutoSplit().subscribe((data: any) => {
    //    // if(isValidDocumentRole05CheckListResponse(res)) {
    //    if (data) {
    //      // Set value
    //      //this.tableList.SetPaging(data)
    //      this.onClickDocumentRole05CheckList()
    //      //this.tableList.SetDataList(data)
    //      //this.listData(data)
    //      //this.automateTest.test(this, { list: data.list })
    //    }
    //    this.global.setLoading(false)
    //    // }
    //    // Close loading
    //  })
    //}

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data

        this.listDocumentRole05Check = data.documentRole05item_list || []
        this.help.PageSet(data, this.paginateDocumentRole05Check)

    }

    //listData(data: any): void {
    //  //this.listDocumentRole05Check = data.list || []
    //  //this.help.PageSet(data, this.paginateDocumentRole05Check)
    //  this.tableList.SetDataList(data)
    //}

    saveData(): any {
        let params = this.input

        params.documentRole05item_list = this.listDocumentRole05Check || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalChecks / this[name].itemsPerPage)
    }
    displayMoney(value: any): any {
        return displayMoney(value);
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }

}
