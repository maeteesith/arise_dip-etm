import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { UploadService } from "../../services/upload.service"

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayDateServer,
    clone
} from '../../helpers'

@Component({
    selector: "app-public-role02-action-post-list",
    templateUrl: "./public-role02-action-post-list.component.html",
    styleUrls: ["./public-role02-action-post-list.component.scss"]
})
export class PublicRole02ActionPostListComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    // List PublicRole02DocumentPost
    public listPublicRole02DocumentPost: any[]
    public paginatePublicRole02DocumentPost: any
    public perPagePublicRole02DocumentPost: number[]
    // Response PublicRole02DocumentPost
    public responsePublicRole02DocumentPost: any
    public listPublicRole02DocumentPostEdit: any


    // List PublicProcess
    public listPublicProcess: any[]
    public paginatePublicProcess: any
    public perPagePublicProcess: number[]
    // Response PublicProcess
    public responsePublicProcess: any

    //label_save_combobox || label_save_radio
    public postRoundInstructionRuleCodeList: any[]
    public postRoundTypeCodeList: any[]
    public postRoundActionPostStatusCodeList: any[]
    //label_save_combobox || label_save_radio

    public popup: any
    public ImageListEdit: any[]
    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
        }

        this.input = {
            id: null,
            //public_end_start_date: getMoment(),
            //public_end_end_date: getMoment(),
            //book_acknowledge_start_date: getMoment(),
            //book_acknowledge_end_date: getMoment(),
            post_round_instruction_rule_code: '',
            post_round_type_code: '',
            post_round_action_post_status_code: '',
            request_number: '',
            book_number: '',
        }
        this.listPublicRole02DocumentPost = []
        this.paginatePublicRole02DocumentPost = clone(CONSTANTS.PAGINATION.INIT)
        this.paginatePublicRole02DocumentPost.id = 'paginatePublicRole02DocumentPost'
        this.perPagePublicRole02DocumentPost = clone(CONSTANTS.PAGINATION.PER_PAGE)

        this.listPublicProcess = []

        //Master List
        this.master = {
            postRoundInstructionRuleCodeList: [],
            postRoundTypeCodeList: [],
            postRoundActionPostStatusCodeList: [],
        }
        //Master List



        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //

        this.popup = {}
        this.ImageListEdit = []

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initPublicRole02ActionPostList().subscribe((data: any) => {
            if (data) {
                this.master = data
                this.master.postRoundInstructionRuleCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                // this.input.post_round_instruction_rule_code = "ALL"
                this.master.postRoundTypeCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                // this.input.post_round_type_code = "ALL"
                this.master.postRoundActionPostStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
                this.input.post_round_action_post_status_code = "WAIT"

            }
            if (this.editID) {
                this.PublicProcessService.PublicRole02ActionPostLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }

            //this.onClickPublicRole02ActionPostList()

        })
    }

    constructor(
        public sanitizer: DomSanitizer,
        private help: Help,
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private PublicProcessService: PublicProcessService,
        private uploadService: UploadService,
    ) { }

    onClickPublicRole02ActionPostList(): void {
        // if(this.validatePublicRole02ActionPostList()) {
        // Open loading
        // Call api
        this.callPublicRole02ActionPostList(this.saveData())
        // }
    }
    //! <<< Call API >>>
    callPublicRole02ActionPostList(params: any): void {
        this.global.setLoading(true)
        this.PublicProcessService.PublicRole02ActionPostList(this.help.GetFilterParams(params, this.paginatePublicRole02DocumentPost)).subscribe((data: any) => {
            // if(isValidPublicRole02ActionPostListResponse(res)) {
            if (data) {
                // Set value
                this.listData(data)
                this.automateTest.test(this, { list: data.list })

                //console.log(this.listPublicRole02DocumentPost)
                //this.onClickPublicRole02ActionPostImageOpen(this.listPublicRole02DocumentPost[0])
            }
            this.global.setLoading(false)
            // }
            // Close loading
        })
    }

    onClickReset(): void {
        this.global.setLoading(true)
        this.ngOnInit();
        this.global.setLoading(false)
    }


    onClickPublicRole02ActionPostSend(): void {
        //if(this.validatePublicRole02ActionPostSend()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callPublicRole02ActionPostSend(this.listPublicRole02DocumentPost)
        //}
    }
    validatePublicRole02ActionPostSend(): boolean {
        let result = validateService('validatePublicRole02ActionPostSend', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callPublicRole02ActionPostSend(params: any): void {
        this.PublicProcessService.PublicRole02ActionPostSend(params).subscribe((data: any) => {
            // if(isValidPublicRole02ActionPostSendResponse(res)) {
            if (data) {
                // Set value
                //this.loadData(data)
                this.onClickPublicRole02ActionPostList()
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickPublicRole02ActionPostPublicRole02DocumentPostAdd(): void {
        this.listPublicRole02DocumentPost.push({
            index: this.listPublicRole02DocumentPost.length + 1,
            request_number: null,
            book_number: null,
            public_document_payment_instruction_rule_name: null,
            book_start_date_text: null,
            post_round_type_name: null,
            post_round_action_post_type_code: this.master.postRoundActionPostTypeCodeList[0].code,
            book_acknowledge_date: null,
            post_round_remark: null,
            post_round_action_post_status_name: null,

        })
        this.changePaginateTotal(this.listPublicRole02DocumentPost.length, 'paginatePublicRole02DocumentPost')
    }

    onClickPublicRole02ActionPostPublicRole02DocumentPostEdit(item: any): void {
        var win = window.open("/" + item.id)
    }


    onClickPublicRole02ActionPostPublicRole02DocumentPostDelete(item: any): void {
        // if(this.validatePublicRole02ActionPostPublicRole02DocumentPostDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listPublicRole02DocumentPost.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listPublicRole02DocumentPost.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listPublicRole02DocumentPost.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {
                if (rs === true) rs = ""

                let ids = []

                for (let i = 0; i < this.listPublicRole02DocumentPost.length; i++) {
                    if (this.listPublicRole02DocumentPost[i].is_check) {
                        this.listPublicRole02DocumentPost[i].cancel_reason = rs
                        this.listPublicRole02DocumentPost[i].status_code = "DELETE"
                        this.listPublicRole02DocumentPost[i].is_deleted = true

                        if (true && this.listPublicRole02DocumentPost[i].id) ids.push(this.listPublicRole02DocumentPost[i].id)
                        // else this.listPublicRole02DocumentPost.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callPublicRole02ActionPostPublicRole02DocumentPostDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listPublicRole02DocumentPost.length; i++) {
                    this.listPublicRole02DocumentPost[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callPublicRole02ActionPostPublicRole02DocumentPostDelete(params: any, ids: any[]): void {
        this.PublicProcessService.PublicRole02ActionPostPublicRole02DocumentPostDelete(params).subscribe((data: any) => {
            // if(isValidPublicRole02ActionPostPublicRole02DocumentPostDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listPublicRole02DocumentPost.length; i++) {
                    if (this.listPublicRole02DocumentPost[i].id == id) {
                        this.listPublicRole02DocumentPost.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listPublicRole02DocumentPost.length; i++) {
                this.listPublicRole02DocumentPost[i] = i + 1
            }

            this.onClickPublicRole02ActionPostList()
            // Close loading
            this.global.setLoading(false)
        })
    }


    // Modal Location
    autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
        if (object[name].length >= length) {
            console.log("d")
            this.is_autocomplete_ListModalLocation_show = true

            let params = {
                name: object[name],
                paging: {
                    item_per_page: item_per_page
                },
            }

            this.callAutocompleteChangeListModalLocation(params)
        }
    }
    autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
        let pThis = this
        setTimeout(function () {
            pThis.is_autocomplete_ListModalLocation_show = false
        }, 200)
    }
    callAutocompleteChangeListModalLocation(params: any): void {
        if (this.input.is_autocomplete_ListModalLocation_load) return
        this.input.is_autocomplete_ListModalLocation_load = true
        let pThis = this
        // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
        //   if (data) {
        //     if (data.length == 1) {
        //       setTimeout(function () {
        //         pThis.autocompleteChooseListModalLocation(data[0])
        //       }, 200)
        //     } else {
        //       pThis.autocompleteListListModalLocation = data
        //     }
        //   }
        // })
        this.input.is_autocomplete_ListModalLocation_load = false
    }
    autocompleteChooseListModalLocation(data: any): void {
        this.inputModalAddress.address_sub_district_code = data.code
        this.inputModalAddress.address_sub_district_name = data.name
        this.inputModalAddress.address_district_code = data.district_code
        this.inputModalAddress.address_district_name = data.district_name
        this.inputModalAddress.address_province_code = data.province_code
        this.inputModalAddress.address_province_name = data.province_name
        this.inputModalAddress.address_country_code = data.country_code
        this.inputModalAddress.address_country_name = data.country_name
        this.is_autocomplete_ListModalLocation_show = false
    }
    onClickSave04ModalAddressSave(): void {
        Object.keys(this.inputModalAddress).forEach((item: any) => {
            this.inputModalAddressEdit[item] = this.inputModalAddress[item]
        })
        this.toggleModal("isModalPeopleEditOpen")
    }
    // Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, row_item: any = null): void {
        if (row_item) row_item.is_check = true
        //if (nameList && id) {
        //  this[nameList].forEach((item: any) => {
        //    if (item.id === id) {
        //      item.validate[name] = null
        //    }
        //  })
        //} else {
        //  this.validate[name] = null
        //}
    }

    loadData(data: any): void {
        this.input = data

        this.listPublicRole02DocumentPost = data.publicrole02documentpost_list || []
        this.changePaginateTotal(this.listPublicRole02DocumentPost.length, 'paginatePublicRole02DocumentPost')

    }

    listData(data: any): void {
        this.listPublicRole02DocumentPost = data.list || []
        this.listPublicRole02DocumentPost.forEach((row_item: any) => {
            row_item.post_round_instruction_rule_name = row_item.post_round_instruction_rule_name.split(",").join('<br>')
        })
        this.help.PageSet(data, this.paginatePublicRole02DocumentPost)
        //this.changePaginateTotal(this.listPublicRole02DocumentPost.length, 'paginatePublicRole02DocumentPost')

    }

    onChangeSelectPublicRole02DocumentPost(row_item: any): void {
        row_item.is_check = true
    }

    saveData(): any {
        // let params = this.input
        // params.publicrole02documentpost_list = this.listPublicRole02DocumentPost || []

        const params = {
            //page_index: +this.paginatePublicRole02DocumentPost.currentPage,
            //item_per_page: +this.paginatePublicRole02DocumentPost.itemsPerPage,
            //order_by: 'created_date',
            //is_order_reverse: false,
            //search_by: [{
            //  key: 'public_end_date',
            //  value: displayDateServer(this.input.public_end_start_date),
            //  operation: 3
            //}, {
            //  key: 'public_end_date',
            //  value: displayDateServer(this.input.public_end_end_date),
            //  operation: 4
            //}, {
            //  key: 'book_acknowledge_date',
            //  value: displayDateServer(this.input.book_acknowledge_start_date),
            //  operation: 3
            //}, {
            public_end_start_date: this.input.public_end_start_date,
            public_end_end_date: this.input.public_end_end_date,
            book_acknowledge_start_date: this.input.book_acknowledge_start_date,
            book_acknowledge_end_date: this.input.book_acknowledge_end_date,
            post_round_instruction_rule_code: this.input.post_round_instruction_rule_code,
            post_round_type_code: this.input.post_round_type_code,
            post_round_action_post_status_code: this.input.post_round_action_post_status_code,
            request_number: this.input.request_number,
            book_number: this.input.book_number,
            //  key: 'book_acknowledge_date',
            //  value: displayDateServer(this.input.book_acknowledge_end_date),
            //  operation: 4
            //}, {
            //  key: 'post_round_instruction_rule_code',
            //  value: this.input.post_round_instruction_rule_code,
            //  operation: 0
            //}, {
            //  key: 'post_round_type_code',
            //  value: this.input.post_round_type_code,
            //  operation: 0
            //}, {
            //  key: 'post_round_action_post_status_code',
            //  value: this.input.post_round_action_post_status_code,
            //  operation: 0
            //}, {
            //  key: 'request_number',
            //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
            //  operation: 5
            //}, {
            //  key: 'book_number',
            //  value: this.input.book_number,
            //  operation: 5
            //}]
        }

        return params
    }

    onClickPublicRole02ActionPostImageOpen(row_item: any): void {
        this.popup.is_upload_show = true
        this.input.post_round_id = row_item.id
        this.ImageListEdit = []

        this.global.setLoading(true)
        this.PublicProcessService.PostRoundActionImageList(row_item.id).subscribe((data: any) => {
            //  //this.global.setLoading(false)
            //  //this.modal["isModalImageEditOpen"] = false

            if (data && data.list) {
                this.ImageListEdit = data.list || []
                //  item.file_id = data.id
                //  this.onClickPublicRole02ActionPostImageSave()
                //  //    this.callRequestDocumentCollectItemSave(this.rowImageEdit)
            }

            this.global.setLoading(false)
        })
    }

    onClickUploadImage(event: any, obj: string, name: any, maxSize: any, item: any): void {
        if (event) {
            let file = event.target.files[0]
            event.target.value = ""
            // Check type file
            //if (file.type.includes("image/")) {
            // Has max size
            if (maxSize) {
                // Check size file
                if (file.size <= maxSize) {
                    this.setFileData(file, obj, name, item)
                }
            } else {
                this.setFileData(file, obj, name, item)
            }
            //}
        }
    }
    setFileData(file: any, obj: string, name: any, item: any): void {
        //this[obj][name].file = file
        let reader = new FileReader()
        reader.onload = e => {
            this.ImageListEdit.push({
                post_round_id: this.input.post_round_id,
                blob: reader.result,
            })
        }
        reader.readAsDataURL(file)
    }

    onClickPublicRole02ActionPostImageSave(): void {
        this.global.setLoading(true)

        var image_upload_list = this.ImageListEdit.filter(r => !r.file_id && !r.is_deleted)
        if (image_upload_list.length > 0) {
            this.callUpload(image_upload_list[0])
        } else {
            this.PublicProcessService.PostRoundActionImageSave(this.ImageListEdit).subscribe((data: any) => {
                //  //this.global.setLoading(false)
                //  //this.modal["isModalImageEditOpen"] = false

                if (data) {
                    this.popup.is_upload_show = false
                    //  this.ImageListEdit = data.list
                    //  //  item.file_id = data.id
                    //  //  this.onClickPublicRole02ActionPostImageSave()
                    //  //  //    this.callRequestDocumentCollectItemSave(this.rowImageEdit)
                }
                this.global.setLoading(false)
            })
        }
    }

    callUpload(item: any): void {
        let params = {
            file: this.dataURItoBlob(item.blob)
        }

        //console.log(params)
        const formData = new FormData()
        formData.append("file", params.file, "img.jpg")

        //this.modal["isModalImageEditOpen"] = false
        //this.global.setLoading(true)
        this.uploadService.upload(formData).subscribe((data: any) => {
            //  //this.global.setLoading(false)
            //  //this.modal["isModalImageEditOpen"] = false

            if (data) {
                item.file_id = data.id
                this.onClickPublicRole02ActionPostImageSave()
                //    this.callRequestDocumentCollectItemSave(this.rowImageEdit)
            }
        })
    }
    dataURItoBlob(dataURI: any): any {
        var byteString = atob(dataURI.split(",")[1])

        var mimeString = dataURI
            .split(",")[0]
            .split(":")[1]
            .split("")[0]

        var ab = new ArrayBuffer(byteString.length)
        var ia = new Uint8Array(ab)
        for (var i = 0; i < byteString.length; i++) {
            ia[i] = byteString.charCodeAt(i)
        }

        var bb = new Blob([ab], { type: mimeString })
        return bb
    }


    //! <<< ----------------- >>>
    //! <<< --- Copy Zone --- >>>
    //! <<< ----------------- >>>

    //! <<< Binding (one way) >>>
    binding(obj: any, name: any, value: any, row_item: any = null): void {
        if (row_item) row_item.is_check = true
        this[obj][name] = value
    }

    //! <<< Table >>>
    onSelectColumnInTable(name: string, item: any): void {
        if (!item) {
            this.input.is_check_all = !this.input.is_check_all
            this[name].forEach((item: any) => {
                item.is_check = this.input.is_check_all
            })
        } else {
            item.is_check = !item.is_check
        }
    }
    onChangeInputInTable(item: any, name: any, value: any): void {
        item[name] = value
    }
    toggleInputInTable(item: any, name: any): void {
        item[name] = !item[name]
    }

    //! <<<< Pagination >>>
    managePaginateCallApi(name: string): void {
        if (name === 'paginatePublicRole02DocumentPost') {
            this.onClickPublicRole02ActionPostList()
            this.input.is_check_all = false
        }
    }
    onChangePage(page: any, name: string): void {
        this[name].currentPage = page === '' ? 1 : page
        if ((+page || page === '') && page <= this.getMaxPage(name)) {
            this.managePaginateCallApi(name)
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
        this.managePaginateCallApi(name)
    }
    changePaginateTotal(total: any, name: string): void {
        this[name].totalItems = total
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
