import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-madrid-role02-protection312-modal',
  templateUrl: './madrid-role02-protection312-modal.component.html',
  styleUrls: ['./madrid-role02-protection312-modal.component.scss']
})
export class MadridRole02Protection312ModalComponent implements OnInit {

  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any
  public role: any


  public modal: any

  public listPublicItem: any = [];
  public doc: any = [];

  public selectedDoc: any = -1;
  constructor( public dialog: MatDialog,
    public dialogRef: MatDialogRef<MadridRole02Protection312ModalComponent>) { }
//MR3-12
ngOnInit() {
  this.role = true;
  this.listPublicItem = [
    { index: "1", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", statusprogress: "รอแก้ไขข้อบกพร่อง", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAA", command: "", status: "ไม่อนุญาต" },
    { index: "2", img: "https://www.ipthailand.go.th/images/001/DIP-Logo.png", refNo: "17991551", requestNo: "741150971", requestNo2: "", statusprogress: "รอแก้ไขข้อบกพร่อง", createdate: "2020/06/25", pic: "นางสมหวัง หวังน้อย", approvedate: "2020/06/25", desc: "AAAAAAAAAA", command: "", status: "ไม่อนุญาต (คุมเวลาจำหน่าย)" }
  ]

  this.doc = [{ value: "1", text: "MMC" }, { value: "2", text: "MMU" }]

  this.validate = {}
  this.input = {
    id: null,
    //instruction_send_start_date: getMoment(),
    //instruction_send_end_date: getMoment(),
    public_type_code: '',
    public_source_code: '',
    public_receiver_by: '',
    public_status_code: '',
    request_number: '',
  }
  this.master = {
    request_type: [{ code: 1, name: '1' }],
    job_reciver: [{ id: 1, name: "Sam" }, { id: 2, name: "Tom" }]
  }

}

closeModal() {
  this.dialogRef.close();
}

}



