import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { DomSanitizer } from '@angular/platform-browser'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-document-role02-check",
    templateUrl: "./document-role02-check.component.html",
    styleUrls: ["./document-role02-check.component.scss"]
})
export class DocumentRole02CheckComponent implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public InstructionEdit: any
    public inputAddress: any

    // List CheckingInstructionList
    public listCheckingInstructionList: any[]
    public paginateCheckingInstructionList: any
    public perPageCheckingInstructionList: number[]
    // Response CheckingInstructionList
    public responseCheckingInstructionList: any
    public listCheckingInstructionListEdit: any

    // List AddressList
    public listAddressList: any[]
    public paginateAddressList: any
    public perPageAddressList: number[]
    // Response AddressList
    public responseAddressList: any
    public listAddressListEdit: any

    // List CheckingInstructionDocumentList
    public listCheckingInstructionDocumentList: any[]
    public paginateCheckingInstructionDocumentList: any
    public perPageCheckingInstructionDocumentList: number[]
    // Response CheckingInstructionDocumentList
    public responseCheckingInstructionDocumentList: any
    public listCheckingInstructionDocumentListEdit: any


    //label_save_combobox || label_save_radio
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial

    //public instruction_rule_list: any

    public tableInstructionRule: any
    public tableAddressContact: any
    public tableInstructionDocument: any

    public tablePopupAddress: any



    public popup: any

    ngOnInit() {
        this.editID = this.route.snapshot.paramMap.get('id')
        this.validate = {}
        this.modal = {
            isModalInstruction: false,
        }

        this.InstructionEdit = {}

        this.input = {
            id: null,
            request_number: '',
            name: '',
            request_item_sub_type_1_code_text: '',
            request_item_type_name: '',
            is_trademark_join: false,
            request_date: '',
            representative_name: '',
            house_number: '',
            is_trademark_sound: false,

            people_list: [{}],
        }
        this.listCheckingInstructionList = []
        this.paginateCheckingInstructionList = CONSTANTS.PAGINATION.INIT
        this.perPageCheckingInstructionList = CONSTANTS.PAGINATION.PER_PAGE

        this.listAddressList = []
        this.paginateAddressList = CONSTANTS.PAGINATION.INIT
        this.perPageAddressList = CONSTANTS.PAGINATION.PER_PAGE

        this.listCheckingInstructionDocumentList = []
        this.paginateCheckingInstructionDocumentList = CONSTANTS.PAGINATION.INIT
        this.perPageCheckingInstructionDocumentList = CONSTANTS.PAGINATION.PER_PAGE

        this.popup = {}

        //Master List
        this.master = {
        }
        //Master List



        this.tableInstructionRule = {
            column_list: {
                index: "#",
                instruction_rule_name: "ประเภทหนังสือ",
                book_number_text: "เลข พณ.",
                book_start_date: "วันที่ออกหนังสือ",
                save010_checking_instruction_document_remark: "รายละเอียด",
                considering_instruction_rule_status_name: "สถานะ",
            },
            command_item: [{
                name: "instruction_rule_edit",
                object_id: "post_round_id",
                icon: "file-pdf",
            }],
        }

        this.tableAddressContact = {
            column_list: {
                index: "#",
                address_information: "ที่อยู่การจัดส่ง",
            },
            can_added: "address_add",
            can_deleted: true,
        }

        this.tableInstructionDocument = {
            column_list: {
                index: "#",
                instruction_rule_name: "ตค. ที่",
                instruction_send_date: "วันที่มีคำสั่ง",
                book_round_01_start_date: "ออกหนังสือรอบ 1",
                book_round_02_start_date: "ออกหนังสือรอบ 2",
                book_number_text: "เลข พณ.",
                document_role02_check_status_name: "สถานะ",
            },
            command_item: [{
                name: "instruction_document_edit"
            }],
        }


        this.tablePopupAddress = {
            column_list: {
                index: "#",
                address_information: "ที่อยู่การจัดส่ง",
            },
            can_checked: true,
        }


        //
        this.autocompleteListListModalLocation = []
        this.is_autocomplete_ListModalLocation_show = false
        this.is_autocomplete_ListModalLocation_load = false
        this.modal = { isModalPeopleEditOpen: false, }
        this.inputAddress = {}
        this.inputModalAddress = {}
        //

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)
        this.forkJoinService.initDocumentRole02Check().subscribe((data: any) => {
            if (data) {
                this.master = data

            }
            if (this.editID) {
                this.DocumentProcessService.DocumentRole02CheckLoad(this.editID).subscribe((data: any) => {
                    if (data) {
                        // Manage structure
                        this.loadData(data)

                        var param = {
                            save_id: data.view.id.toString(),
                        }
                        this.DocumentProcessService.List("Save010CheckingInstructionDocument", this.help.GetFilterParams(param)).subscribe((data: any) => {
                            if (data && data.list) {
                                // Manage structure
                                data.list.forEach((item: any) => {
                                    item.book_number_text = item.book_number
                                    if (item.book_number_text) {
                                        item.book_number_text = this.sanitizer.bypassSecurityTrustHtml("<span class='red' style='color: red;'>*</span> " + item.book_number_text)
                                    }
                                })
                                this.tableInstructionRule.Set(data.list)
                            }
                            // Close loading
                            this.global.setLoading(false)
                            this.automateTest.test(this)
                        })
                    }
                    // Close loading
                    this.global.setLoading(false)
                    this.automateTest.test(this)
                })
            } else {
                this.global.setLoading(false)
                this.automateTest.test(this)
            }

        })
    }

    constructor(
        public sanitizer: DomSanitizer,
        private help: Help,
        private automateTest: AutomateTest,
        private route: ActivatedRoute,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }


    onClickDocumentRole02CheckCheckingInstructionListAdd(): void {
        this.listCheckingInstructionList.push({
            index: this.listCheckingInstructionList.length + 1,
            instruction_rule_name: null,
            book_number: null,
            book_start_date_text: null,
            save010_checking_instruction_document_remark: null,
            considering_instruction_rule_status_name: null,

        })
        this.changePaginateTotal(this.listCheckingInstructionList.length, 'paginateCheckingInstructionList')
    }

    onClickDocumentRole02CheckAddressListAdd(): void {
        this.listAddressList.push({
            index: this.listAddressList.length + 1,
            house_number: null,

        })
        this.changePaginateTotal(this.listAddressList.length, 'paginateAddressList')
    }

    onClickDocumentRole02CheckAddressListEdit(item: any): void {
        var win = window.open("/" + item.id)
    }


    onClickDocumentRole02CheckAddressListDelete(item: any): void {
        // if(this.validateDocumentRole02CheckAddressListDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listAddressList.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listAddressList.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listAddressList.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {
                if (rs === true) rs = ""

                let ids = []

                for (let i = 0; i < this.listAddressList.length; i++) {
                    if (this.listAddressList[i].is_check) {
                        this.listAddressList[i].cancel_reason = rs
                        this.listAddressList[i].status_code = "DELETE"
                        this.listAddressList[i].is_deleted = true

                        if (true && this.listAddressList[i].id) ids.push(this.listAddressList[i].id)
                        // else this.listAddressList.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callDocumentRole02CheckAddressListDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listAddressList.length; i++) {
                    this.listAddressList[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callDocumentRole02CheckAddressListDelete(params: any, ids: any[]): void {
        this.DocumentProcessService.DocumentRole02CheckAddressListDelete(params).subscribe((data: any) => {
            // if(isValidDocumentRole02CheckAddressListDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listAddressList.length; i++) {
                    if (this.listAddressList[i].id == id) {
                        this.listAddressList.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listAddressList.length; i++) {
                this.listAddressList[i] = i + 1
            }

            //this.onClickDocumentRole02CheckAddressList()
            // Close loading
            this.global.setLoading(false)
        })
    }

    onClickDocumentRole02CheckCheckingInstructionDocumentListAdd(): void {
        this.listCheckingInstructionDocumentList.push({
            index: this.listCheckingInstructionDocumentList.length + 1,
            instruction_rule_name: null,
            instruction_send_date_text: null,
            book_round_01_start_date_text: null,
            book_round_02_start_date_text: null,
            book_number: null,
            document_role02_receive_status_name: null,

        })
        this.changePaginateTotal(this.listCheckingInstructionDocumentList.length, 'paginateCheckingInstructionDocumentList')
    }

    onClickDocumentRole02CheckCheckingInstructionDocumentListEdit(item: any): void {
        //var win = window.open("/" + item.id)
        this.toggleModal("isModalInstruction")
        this.InstructionEdit = item
    }


    onClickDocumentRole02CheckCheckingInstructionDocumentListDelete(item: any): void {
        // if(this.validateDocumentRole02CheckCheckingInstructionDocumentListDelete()) {
        // Open loading
        this.global.setLoading(true)
        // Set param
        if (this.listCheckingInstructionDocumentList.filter(r => r.is_check).length > 0 || item) {
            if (item) {
                this.listCheckingInstructionDocumentList.filter(r => r.is_check).forEach((item: any) => {
                    item.is_check = false
                });
                item.is_check = true
            }

            let delete_save_item_count = 0
            if (true) {
                delete_save_item_count = this.listCheckingInstructionDocumentList.filter(r => r.is_check && r.id).length
            }

            var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
            if (rs && rs != "") {
                if (rs === true) rs = ""

                let ids = []

                for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
                    if (this.listCheckingInstructionDocumentList[i].is_check) {
                        this.listCheckingInstructionDocumentList[i].cancel_reason = rs
                        this.listCheckingInstructionDocumentList[i].status_code = "DELETE"
                        this.listCheckingInstructionDocumentList[i].is_deleted = true

                        if (true && this.listCheckingInstructionDocumentList[i].id) ids.push(this.listCheckingInstructionDocumentList[i].id)
                        // else this.listCheckingInstructionDocumentList.splice(i--, 1);
                    }
                }

                if (true) {
                    if (ids.length > 0) {
                        let params = {
                            ids: ids,
                            cancel_reason: rs
                        }
                        // Call api
                        this.callDocumentRole02CheckCheckingInstructionDocumentListDelete(params, ids)
                        return;
                    }
                }

                for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
                    this.listCheckingInstructionDocumentList[i].index = i + 1
                }
            }
        }
        this.global.setLoading(false)
    }
    //! <<< Call API >>>
    callDocumentRole02CheckCheckingInstructionDocumentListDelete(params: any, ids: any[]): void {
        this.DocumentProcessService.DocumentRole02CheckCheckingInstructionDocumentListDelete(params).subscribe((data: any) => {
            // if(isValidDocumentRole02CheckCheckingInstructionDocumentListDeleteResponse(res)) {

            ids.forEach((id: any) => {
                for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
                    if (this.listCheckingInstructionDocumentList[i].id == id) {
                        this.listCheckingInstructionDocumentList.splice(i--, 1);
                    }
                }
            });

            for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
                this.listCheckingInstructionDocumentList[i] = i + 1
            }

            //this.onClickDocumentRole02CheckCheckingInstructionDocumentList()
            // Close loading
            this.global.setLoading(false)
        })
    }



    onClickDocumentRole02CheckInstructionRuleSend(): void {
        //if(this.validateClickDocumentRole02CheckInstructionRuleSend()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callDocumentRole02CheckInstructionRuleSend(this.InstructionEdit)
        //}
    }
    validateClickDocumentRole02CheckInstructionRuleSend(): boolean {
        let result = validateService('validateClickDocumentRole02CheckInstructionRuleSend', this.input)
        this.validate = result.validate
        return result.isValid
    }
    //! <<< Call API >>>
    callDocumentRole02CheckInstructionRuleSend(params: any): void {
        this.DocumentProcessService.DocumentRole02CheckInstructionRuleSend(params).subscribe((data: any) => {
            // if(isValidClickDocumentRole02CheckInstructionRuleSendResponse(res)) {
            if (data) {
                // Set value
                //this.loadData(data)
                //this.onClickPublicRole05DocumentList()
                this.toggleModal('isModalInstruction')
                this.callInit()
            }
            // }
            // Close loading
            this.global.setLoading(false)
        })
    }


    onClickDocumentRole02CheckSendChange(): void {
        //if(this.validateClickonClickDocumentRole02CheckSendChange()) {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callDocumentRole02CheckSendChange({
            id: this.popup.row_item_edit.id,
            object_id: this.popup.row_item_edit.instruction_rule_id,
            department_code: this.popup.row_item_edit.department_code,
            reason: this.popup.row_item_edit.reason,
        })
        //}
    }
    //validateDocumentRole02CheckSendChange(): boolean {
    //  let result = validateService('validateClickonClickDocumentRole02CheckSendChange', this.input)
    //  this.validate = result.validate
    //  return result.isValid
    //}
    //! <<< Call API >>>
    callDocumentRole02CheckSendChange(params: any): void {
        this.DocumentProcessService.DocumentRole02CheckSendChange([params]).subscribe((data: any) => {
            if (data) {
                this.popup.is_row_item_edit_show = false
                this.popup.is_send_back_show = false
                this.popup.isPopupSendOpen = true
            }
            this.global.setLoading(false)
        })
    }


    onClickDocumentRole02CheckSend(): void {
        //if(this.validateClickonClickDocumentRole02CheckSend()) {
        // Open loading
        this.global.setLoading(true)

        var param = {
            id: +this.editID,
            post_round_address_list: this.tableAddressContact.Get(),
            document_role02_print_document_date: this.input.document_role02_print_document_date,
        }
        // Call api
        this.callDocumentRole02CheckSend(param)
        //}
    }
    //validateDocumentRole02CheckSend(): boolean {
    //  let result = validateService('validateClickonClickDocumentRole02CheckSend', this.input)
    //  this.validate = result.validate
    //  return result.isValid
    //}
    //! <<< Call API >>>
    callDocumentRole02CheckSend(params: any): void {
        this.DocumentProcessService.DocumentRole02CheckSend([params]).subscribe((data: any) => {
            if (data) {
                this.popup.is_row_item_edit_show = false
                this.popup.is_send_back_show = false
                this.popup.isPopupSendOpen = true
            }
            this.global.setLoading(false)
        })
    }



    onClickDocumentRole02CheckIntructionRuleSave(): void {
        // Open loading
        this.global.setLoading(true)
        // Call api
        this.callDocumentRole02CheckIntructionRuleSave(this.popup.row_item_edit)
        //}
    }
    //validateDocumentRole02CheckIntructionRuleSave(): boolean {
    //  let result = validateService('validateClickonClickDocumentRole02CheckIntructionRuleSave', this.input)
    //  this.validate = result.validate
    //  return result.isValid
    //}
    //! <<< Call API >>>
    callDocumentRole02CheckIntructionRuleSave(params: any): void {
        this.DocumentProcessService.DocumentRole02CheckIntructionRuleSave(params).subscribe((data: any) => {
            if (data) {
                this.popup.is_row_item_edit_show = false
                //this.popup.is_send_back_show = false
                //this.popup.isPopupSendOpen = true
            }
            this.global.setLoading(false)
        })
    }

    onEnterDocumentRole02CheckPopupAdddressAdd(): void {
        if (this.popup.address_value != "") {
            this.tablePopupAddress.Add({
                address_information: this.popup.address_value,
            })

            setTimeout(() => {
                this.popup.address_value = ""
            }, 100)
        }
    }
    onClickDocumentRole02CheckPopupAdddressSave(): void {
        this.tablePopupAddress.Get().filter(r => r.is_check).forEach((item: any) => {
            this.tableAddressContact.Add({
                address_information: item.address_information,
            })
        })

        console.log(this.tableAddressContact.Get())
        this.popup.is_address_show = false
    }

    onClickCommand($event): void {
        console.log($event)

        if ($event.command == "instruction_document_edit") {
            this.popup.is_row_item_edit_show = true
            this.popup.row_item_edit = $event.object_list[0]
        } else if ($event.command == "address_add") {
            this.popup.is_address_show = true
            this.global.setLoading(true)

            this.DocumentProcessService.DocumentRole02CheckLoad(this.editID).subscribe((data: any) => {
                if (data) {
                    // Manage structure
                    this.loadData(data)

                    var param = {
                        save_id: this.input.id.toString(),
                    }
                    this.DocumentProcessService.List("SaveAddressAll", this.help.GetFilterParams(param)).subscribe((data: any) => {
                        if (data && data.list) {
                            // Manage structure
                            this.tablePopupAddress.Set(data.list)
                        }
                        // Close loading
                        this.global.setLoading(false)
                    })
                }
                // Close loading
                this.global.setLoading(false)
            })
        } else if ($event.command == "instruction_rule_edit") {
            window.open("/pdf/TorKor/" + $event.object_list[0].post_round_id)
        }
    }

    onClickDocumentRole02CheckTorkorOpen(): void {
        window.open("/pdf/TorKor/" + this.editID)
    }

    onClickOpen(url: any, id: any = null): void {
        id = id || this.editID.split("_")[0]
        window.open(url + id)
    }

    //// Modal Location
    //autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    //  if (object[name].length >= length) {
    //    console.log("d")
    //    this.is_autocomplete_ListModalLocation_show = true

    //    let params = {
    //      name: object[name],
    //      paging: {
    //        item_per_page: item_per_page
    //      },
    //    }

    //    this.callAutocompleteChangeListModalLocation(params)
    //  }
    //}
    //autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    //  let pThis = this
    //  setTimeout(function () {
    //    pThis.is_autocomplete_ListModalLocation_show = false
    //  }, 200)
    //}
    //callAutocompleteChangeListModalLocation(params: any): void {
    //  if (this.input.is_autocomplete_ListModalLocation_load) return
    //  this.input.is_autocomplete_ListModalLocation_load = true
    //  let pThis = this
    //  // this.DocumentProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //  //   if (data) {
    //  //     if (data.length == 1) {
    //  //       setTimeout(function () {
    //  //         pThis.autocompleteChooseListModalLocation(data[0])
    //  //       }, 200)
    //  //     } else {
    //  //       pThis.autocompleteListListModalLocation = data
    //  //     }
    //  //   }
    //  // })
    //  this.input.is_autocomplete_ListModalLocation_load = false
    //}
    //autocompleteChooseListModalLocation(data: any): void {
    //  this.inputModalAddress.address_sub_district_code = data.code
    //  this.inputModalAddress.address_sub_district_name = data.name
    //  this.inputModalAddress.address_district_code = data.district_code
    //  this.inputModalAddress.address_district_name = data.district_name
    //  this.inputModalAddress.address_province_code = data.province_code
    //  this.inputModalAddress.address_province_name = data.province_name
    //  this.inputModalAddress.address_country_code = data.country_code
    //  this.inputModalAddress.address_country_name = data.country_name
    //  this.is_autocomplete_ListModalLocation_show = false
    //}
    //onClickSave04ModalAddressSave(): void {
    //  Object.keys(this.inputModalAddress).forEach((item: any) => {
    //    this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    //  })
    //  this.toggleModal("isModalPeopleEditOpen")
    //}
    //// Modal Location

    //! <<< Validate >>>
    clearValidate(name: string, nameList: string, id: number): void {
        if (nameList && id) {
            this[nameList].forEach((item: any) => {
                if (item.id === id) {
                    item.validate[name] = null
                }
            })
        } else {
            this.validate[name] = null
        }
    }

    loadData(data: any): void {
        this.input = data.view
        //this.instruction_rule_list = data.instruction_rule_list

        this.input.request_mark_feature_code_list = this.input.request_mark_feature_code_list || ""

        this.input.request_mark_feature_code_is_sound = this.input.request_mark_feature_code_list.indexOf('SOUND') >= 0 ? 'ใช่' : ''
        this.input.is_trademark_join = this.input.joiner_list.length > 0 ? 'ใช่' : ''

        //console.log(data.view.contact_address_information)

        if (data.post_round_address_list.length > 0) {
            this.tableAddressContact.Set(data.post_round_address_list)
        } else {
            this.tableAddressContact.Set([{ address_information: data.view.contact_address_information }])
            //console.log(data.view)
            //console.log(data.view.contact_address_information)
        }
        //console.log(this.tableAddressContact.Get())

        this.input.request_mark_feature_code_list.indexOf('SOUND') >= 0 ? 'ใช่' : ''


        data.instruction_rule_list.forEach((item: any) => {
            item.book_number_text = item.book_number
            if (item.book_number_text) {
                item.book_number_text = this.sanitizer.bypassSecurityTrustHtml("<span class='red' style='color: red;'>*</span> " + item.book_number_text)
            }
        })

        this.tableInstructionDocument.Set(data.instruction_rule_list)
        //this.listCheckingInstructionList = data.instruction_rule_list
        //this.listAddressList = [data.view.contact_address_01]
        //this.listCheckingInstructionDocumentList = data.instruction_rule_list

        //this.listCheckingInstructionList = data.checkinginstructionlist_list || []
        //this.listAddressList = data.addresslist_list || []
        //this.listCheckingInstructionDocumentList = data.checkinginstructiondocumentlist_list || []
        //this.changePaginateTotal(this.listCheckingInstructionList.length, 'paginateCheckingInstructionList')
        //this.changePaginateTotal(this.listAddressList.length, 'paginateAddressList')
        //this.changePaginateTotal(this.listCheckingInstructionDocumentList.length, 'paginateCheckingInstructionDocumentList')

    }

    listData(data: any): void {
        this.listCheckingInstructionList = data.list || []
        this.listAddressList = data.list || []
        this.listCheckingInstructionDocumentList = data.list || []
        this.changePaginateTotal(this.listCheckingInstructionList.length, 'paginateCheckingInstructionList')
        this.changePaginateTotal(this.listAddressList.length, 'paginateAddressList')
        this.changePaginateTotal(this.listCheckingInstructionDocumentList.length, 'paginateCheckingInstructionDocumentList')

    }

    saveData(): any {
        let params = this.tableInstructionDocument.Get()

        params.forEach((item: any) => {
            item.is_check = true
        })


        //params.checkinginstructionlist_list = this.listCheckingInstructionList || []
        //params.addresslist_list = this.listAddressList || []
        //params.checkinginstructiondocumentlist_list = this.listCheckingInstructionDocumentList || []

        return params
    }

    //! <<<< Pagination >>>
    onChangePage(page: any, name: string): void {
        if (+page) {
            this[name].currentPage = page
        }
    }
    onChangePerPage(value: number, name: string): void {
        this[name].itemsPerPage = value
    }
    changePaginateTotal(total: any, name: string): void {
        let paginate = CONSTANTS.PAGINATION.INIT
        paginate.totalItems = total
        this[name] = paginate
    }
    getMaxPage(name: string): number {
        return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
    }

    oneWayDataBinding(name: any, value: any, object: any): void {
        if (name.indexOf("price") >= 0) {
            value = parseFloat(value)
        }

        if (object) {
            object[name] = value
        } else {
            this.input[name] = value
        }
    }
    oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
        object_list.forEach((item: any) => { item.is_check = value })
    }
    //! <<< Modal >>>
    toggleModal(name: string): void {
        this.modal[name] = !this.modal[name]
    }
}
