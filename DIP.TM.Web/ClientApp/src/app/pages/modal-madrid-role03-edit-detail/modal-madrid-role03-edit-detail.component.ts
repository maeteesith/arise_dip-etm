import { MatDialogRef, MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { Component, Input, Output, OnInit, EventEmitter, Inject } from "@angular/core";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { CONSTANTS, validateService } from "../../helpers";


@Component({
  selector: "app-modal-madrid-role03-edit-detail",
  templateUrl: "./modal-madrid-role03-edit-detail.component.html",
  styleUrls: ["./modal-madrid-role03-edit-detail.component.scss",
  './../../../assets/theme/styles/madrid/madrid.scss'],
})
export class ModalMadridRole03EditDetail implements OnInit {

  public listdetail: any = [];
 
  constructor( 
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<ModalMadridRole03EditDetail>,
    
    @Inject(MAT_DIALOG_DATA) public data: any)  {}

  ngOnInit() {
    
    
    this.listdetail = [
        { index: "1",day:"2019/1/1",ay:"2019/1/1", title: "รายละเอียดคำขอ", remark: "โปรเจกเตอร์เพลซ อัลบัมโหงวเฮ้งแทงโก้ อ่วมรามาธิบดีเจล แผดเผาสหัสวรรษ อุปนายกออร์แกนิก รีทัชแป๋วคาสิโนแกสโซฮอล์ซิมโฟนี โหงวเฮ้งรีโมทสแตนเลสแป๋ว โกลด์เกรย์โดมิโน อีแต๋นซัพพลายเออร์ บลูเบอร์รี่ไทม์แชเชือนฮิ บัตเตอร์ แพกเกจล็อตเรซิน ฮองเฮาเปปเปอร์มินต์จูนเที่ยงวันแพกเกจ ซื่อบื้อซีเนียร์สโตร์เชฟเวิร์ก ซาตานระโงกสไตรค์เวิร์ลด์ซัมเมอร์ ธุหร่ำ" },
        { index: "2",day:"2019/2/2", title: "ENTITLEMENT TO FILE", remark: "17991551"},
        { index: "3",day:"2019/3/3", title: "ENTITLEMENT TO FILE", remark: "https://www.ipthailand.go.th/images/001/DIP-Logo.png"},
       

    ]
  
    
    
  }

  closeModal() {
    
    this.dialogRef.close();
  }

 
 
  }

