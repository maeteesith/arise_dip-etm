import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-save-03-process",
  templateUrl: "./save-03-process.component.html",
  styleUrls: ["./save-03-process.component.scss"]
})
export class Save03ProcessComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  // List People01
  public listPeople01: any[]
  public paginatePeople01: any
  public perPagePeople01: number[]
  // Response People01
  public responsePeople01: any
  public listPeople01Edit: any

  // List Product01
  public listProduct01: any[]
  public paginateProduct01: any
  public perPageProduct01: number[]
  // Response Product01
  public responseProduct01: any
  public listProduct01Edit: any

  // List Product
  public listProduct: any[]
  public paginateProduct: any
  public perPageProduct: number[]
  // Response Product
  public responseProduct: any
  public listProductEdit: any

  // List People
  public listPeople: any[]
  public paginatePeople: any
  public perPagePeople: number[]
  // Response People
  public responsePeople: any
  public listPeopleEdit: any

  // List Representative
  public listRepresentative: any[]
  public paginateRepresentative: any
  public perPageRepresentative: number[]
  // Response Representative
  public responseRepresentative: any
  public listRepresentativeEdit: any

  // List ContactAddress
  public listContactAddress: any[]
  public paginateContactAddress: any
  public perPageContactAddress: number[]
  // Response ContactAddress
  public responseContactAddress: any
  public listContactAddressEdit: any


  //label_save_combobox || label_save_radio
  public save030AppealTypeCodeList: any[]
  public save030AppealMakerTypeCodeList: any[]
  public save030AppealReasonCodeList: any[]
  public addressTypeCodeList: any[]
  public addressCardTypeCodeList: any[]
  public addressReceiverTypeCodeList: any[]
  public addressSexCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  public autocompleteListListLocation: any
  public is_autocomplete_ListLocation_show: any
  public is_autocomplete_ListLocation_load: any
  //Modal Initial
  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any
  //Modal Initial

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}

    this.input = {
      id: null,
      request_number: '',
      register_number: '',
      make_date: getMoment(),
      request_status_name: '',
      request_date_text: '',
      total_price: '',
      index_1: '',
      index_2: '',
      irn_number: '',
      save030_appeal_type_code: '',
      save030_appeal_maker_type_code: '',
      save030_appeal_reason_code: '',
      section_index: '',
      remark: '',
    }
    this.listPeople01 = []
    this.paginatePeople01 = CONSTANTS.PAGINATION.INIT
    this.perPagePeople01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct01 = []
    this.paginateProduct01 = CONSTANTS.PAGINATION.INIT
    this.perPageProduct01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listRepresentative = []
    this.paginateRepresentative = CONSTANTS.PAGINATION.INIT
    this.perPageRepresentative = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct = []
    this.paginateProduct = CONSTANTS.PAGINATION.INIT
    this.perPageProduct = CONSTANTS.PAGINATION.PER_PAGE

    this.listPeople = []
    this.paginatePeople = CONSTANTS.PAGINATION.INIT
    this.perPagePeople = CONSTANTS.PAGINATION.PER_PAGE

    this.listContactAddress = []
    this.paginateContactAddress = CONSTANTS.PAGINATION.INIT
    this.perPageContactAddress = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
      save030AppealTypeCodeList: [],
      save030AppealMakerTypeCodeList: [],
      save030AppealReasonCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false
    this.autocompleteListListLocation = []
    this.is_autocomplete_ListLocation_show = false
    this.is_autocomplete_ListLocation_load = false
    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}

    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}


    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave03Process().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save03Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          pThis.global.setLoading(false)
        })
      }

      this.global.setLoading(false)
    })
  }

  constructor(
    private help: Help,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService
  ) { }


  autocompleteKeyDown(event: any) {
    if (event.key == "Enter") {
      this.autocompleteChangeListNotSent(this.input, 'request_number', 5, 1)
    }
  }

  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: { request_number: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    let pThis = this
    this.SaveProcessService.Save03ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListNotSent = data
        }
      }
    })
    this.input.is_autocomplete_ListNotSent_load = false
  }
  autocompleteChooseListNotSent(data: any): void {
    this.loadData(data)

    this.is_autocomplete_ListNotSent_show = false
  }


  onClickSave03Madrid(): void {
    //if(this.validateSave03Madrid()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03Madrid(this.saveData())
    //}
  }
  validateSave03Madrid(): boolean {
    let result = validateService('validateSave03Madrid', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave03Madrid(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03Madrid(params).subscribe((data: any) => {
      // if(isValidSave03MadridResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  // Autocomplete
  autocompleteChangeListLocation(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListLocation(params)
    }
  }
  autocompleteBlurListLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListLocation(params: any): void {
    if (this.input.is_autocomplete_ListLocation_load) return
    this.input.is_autocomplete_ListLocation_load = true
    let pThis = this
    this.SaveProcessService.Save03ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListLocation_load = false
  }
  autocompleteChooseListLocation(data: any): void {
    this.inputAddress.address_sub_district_code = data.code
    this.inputAddress.address_sub_district_name = data.name
    this.inputAddress.address_district_code = data.district_code
    this.inputAddress.address_district_name = data.district_name
    this.inputAddress.address_province_code = data.province_code
    this.inputAddress.address_province_name = data.province_name
    this.inputAddress.address_country_code = data.country_code
    this.inputAddress.address_country_name = data.country_name

    this.is_autocomplete_ListLocation_show = false
  }

  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    this.SaveProcessService.Save03ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListModalLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListModalLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave03ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location


  onClickSave03Delete(): void {
    //if(this.validateSave03Delete()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03Delete(this.saveData())
    //}
  }
  validateSave03Delete(): boolean {
    let result = validateService('validateSave03Delete', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave03Delete(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03Delete(params).subscribe((data: any) => {
      // if(isValidSave03DeleteResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave03Save(): void {
    //if(this.validateSave03Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03Save(this.saveData())
    //}
  }
  validateSave03Save(): boolean {
    let result = validateService('validateSave03Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave03Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03Save(params).subscribe((data: any) => {
      // if(isValidSave03SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickSave03DocumentView(): void {
    window.open('/File/RequestDocumentCollect/30/' + this.input.request_number + "_" + this.input.request_id)
  }
  validateSave03DocumentView(): boolean {
    let result = validateService('validateSave03DocumentView', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave03DocumentView(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03DocumentView(params).subscribe((data: any) => {
      // if(isValidSave03DocumentViewResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave03Send(): void {
    //if(this.validateSave03Send()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03Send(this.saveData())
    //}
  }
  validateSave03Send(): boolean {
    let result = validateService('validateSave03Send', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave03Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03Send(params).subscribe((data: any) => {
      // if(isValidSave03SendResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }


  onClickSave030RequestBack(): void {
    //if(this.validateSave030Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave030RequestBack(this.saveData())
    //}
  }
  callSave030RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save030RequestBack(params).subscribe((data: any) => {
      // if(isValidSave030SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  onClickSave03CopyRequest01(): void {
    //if(this.validateSave03CopyRequest01()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03CopyRequest01(this.saveData())
    //}
  }
  validateSave03CopyRequest01(): boolean {
    let result = validateService('validateSave03CopyRequest01', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave03CopyRequest01(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03CopyRequest01(params).subscribe((data: any) => {
      // if(isValidSave03CopyRequest01Response(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave03CopyRequest020(): void {
    //if(this.validateSave03CopyRequest020()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03CopyRequest020(this.saveData())
    //}
  }
  validateSave03CopyRequest020(): boolean {
    let result = validateService('validateSave03CopyRequest020', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave03CopyRequest020(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03CopyRequest020(params).subscribe((data: any) => {
      // if(isValidSave03CopyRequest020Response(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave03ProductAdd(): void {
    this.listProduct.push({
      index: this.listProduct.length + 1,
      request_item_sub_type_1_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
  }


  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    console.log(this.input)
    console.log(this.input.trademark_status_code)

    this.listPeople = data.people_list || []
    this.listRepresentative = data.representative_list || []
    this.help.Clone(data.contact_address || {}, this.contactAddress)
    this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"

    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []

    this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
    this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')

    this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')

    this.is_autocomplete_ListNotSent_show = false
  }

  saveData(): any {
    const params = this.input
    params.people_list = this.listPeople
    params.representative_list = this.listRepresentative
    params.contact_address = this.contactAddress

    return params
  }

  onClickSave030PeopleAdd(): void {
    this.listPeople.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listPeople[this.listPeople.length - 1])
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
  }
  onClickSave030RepresentativeAdd(): void {
    this.listRepresentative.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listRepresentative[this.listRepresentative.length - 1], 'ตัวแทน')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
  }

  onClickAddressCopy(item_list: any[], copy_item: any): void {
    var index = 1
    item_list.forEach((item: any) => {
      item.index = index++
      if (item === copy_item) {
        var item_new = { ...item }
        item_new.id = null
        item_new.index = index++
        item_list.splice(index + 1, 0, item_new)
      }
    })
  }
  onClickAddressEdit(item: any, name: string = "เจ้าของ"): void {
    this.modalPeopleText = name
    this.modalAddressEdit = item
    this.help.Clone(item, this.modalAddress)

    this.modalAddress.is_representative = name.indexOf("ตัวแทน") >= 0

    this.modal["isModalPeopleEditOpen"] = true

  }
  onClickAddressSave(): void {
    this.help.Clone(this.modalAddress, this.modalAddressEdit)
    this.help.GetAddressInformation(this.modalAddressEdit)

    this.modal["isModalPeopleEditOpen"] = false
  }
  onClickAddressDelete(item_list: any[], item: any): void {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (item_list.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        item_list.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = item_list.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < item_list.length; i++) {
          if (item_list[i].is_check) {
            //item_list[i].cancel_reason = rs
            item_list[i].is_deleted = true
            //console.log(item_list[i])
            //item_list[i].status_code = "DELETE"

            if (true && item_list[i].id) ids.push(item_list[i].id)
            else item_list.splice(i--, 1);
          }
        }
      }
    }
    //console.log(item_list)
    this.global.setLoading(false)
  }




  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxBinding(name: any, value: any, object: any): void {
    if (object) {
      object[name] = value == "on"
    } else {
      this.input[name] = value == "on"
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value == "on" })
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
