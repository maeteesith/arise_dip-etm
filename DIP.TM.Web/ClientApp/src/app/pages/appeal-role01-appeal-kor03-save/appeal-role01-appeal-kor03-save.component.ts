import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../global.service'
import { Auth } from "../../auth";
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'

import '../../pages/appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: 'app-appeal-role01-appeal-kor03-save',
  templateUrl: './appeal-role01-appeal-kor03-save.component.html',
  styleUrls: [
    '../appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss',
    './appeal-role01-appeal-kor03-save.component.scss'
  ]
})

export class AppealRole01AppealKor03SaveComponent implements OnInit {

  public input: any
  public editID: any
  public master: any
  public locationPoint: any;
  public validate: any

   // Response
   public response: any

   // List People01
   public listPeople01: any[]
   public paginatePeople01: any
   public perPagePeople01: number[]
   // Response People01
   public responsePeople01: any
   public listPeople01Edit: any
 
   // List Product01
   public listProduct01: any[]
   public paginateProduct01: any
   public perPageProduct01: number[]
   // Response Product01
   public responseProduct01: any
   public listProduct01Edit: any
 
   // List Product
   public listProduct: any[]
   public paginateProduct: any
   public perPageProduct: number[]
   // Response Product
   public responseProduct: any
   public listProductEdit: any
 
   // List People
   public listPeople: any[]
   public paginatePeople: any
   public perPagePeople: number[]
   // Response People
   public responsePeople: any
   public listPeopleEdit: any
 
   // List Representative
   public listRepresentative: any[]
   public paginateRepresentative: any
   public perPageRepresentative: number[]
   // Response Representative
   public responseRepresentative: any
   public listRepresentativeEdit: any
 
   // List ContactAddress
   public listContactAddress: any[]
   public paginateContactAddress: any
   public perPageContactAddress: number[]
   // Response ContactAddress
   public responseContactAddress: any
   public listContactAddressEdit: any
 
 
   //label_save_combobox || label_save_radio
   public save030AppealTypeCodeList: any[]
   public save030AppealMakerTypeCodeList: any[]
   public save030AppealReasonCodeList: any[]
   public addressTypeCodeList: any[]
   public addressCardTypeCodeList: any[]
   public addressReceiverTypeCodeList: any[]
   public addressSexCodeList: any[]
   //label_save_combobox || label_save_radio
 
   public autocompleteListListNotSent: any
   public is_autocomplete_ListNotSent_show: any
   public is_autocomplete_ListNotSent_load: any
   public autocompleteListListLocation: any
   public is_autocomplete_ListLocation_show: any
   public is_autocomplete_ListLocation_load: any
   //Modal Initial
   public autocompleteListListModalLocation: any
   public is_autocomplete_ListModalLocation_show: any
   public is_autocomplete_ListModalLocation_load: any
   public inputModalAddress: any
   public inputModalAddressEdit: any
   public modal: any
   public inputAddress: any
   //Modal Initial
 
   public modalPeopleText: any
 
   public contactAddress: any
   public modalAddress: any
   public modalAddressEdit: any

   //AppealModule
   public createNewOwner: any
 

  constructor(
    private help: Help,
    private route: ActivatedRoute,
    private global: GlobalService,
    private auth: Auth,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService
  ) { }

  ngOnInit() {
    this.input = {
      id: null,
      request_number: '',
      register_number: '',
      make_date: getMoment(),
      request_status_name: '',
      request_date_text: '',
      total_price: '',
      index_1: '',
      index_2: '',
      irn_number: '',
      save030_appeal_type_code: '',
      save030_appeal_maker_type_code: '',
      save030_appeal_reason_code: '',
      section_index: '',
      remark: '',
      created_by_name: this.auth.getAuth().name,
      appeal_date: getMoment()
    }
    
    this.locationPoint = {
      typeCodeList: [
        { code: "1", name: "เจ้าของ" },
        { code: "2", name: "ตัวแทน" },
        { code: "3", name: "ตัวแทนช่วง" },
        { code: "4", name: "อื่นๆ" },
      ],
    };

    this.listPeople01 = []
    this.paginatePeople01 = CONSTANTS.PAGINATION.INIT
    this.perPagePeople01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct01 = []
    this.paginateProduct01 = CONSTANTS.PAGINATION.INIT
    this.perPageProduct01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listRepresentative = []
    this.paginateRepresentative = CONSTANTS.PAGINATION.INIT
    this.perPageRepresentative = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct = []
    this.paginateProduct = CONSTANTS.PAGINATION.INIT
    this.perPageProduct = CONSTANTS.PAGINATION.PER_PAGE

    this.listPeople = []
    this.paginatePeople = CONSTANTS.PAGINATION.INIT
    this.perPagePeople = CONSTANTS.PAGINATION.PER_PAGE

    this.listContactAddress = []
    this.paginateContactAddress = CONSTANTS.PAGINATION.INIT
    this.perPageContactAddress = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
      save030AppealTypeCodeList: [],
      save030AppealMakerTypeCodeList: [],
      save030AppealReasonCodeList: [],
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false
    this.autocompleteListListLocation = []
    this.is_autocomplete_ListLocation_show = false
    this.is_autocomplete_ListLocation_load = false
    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}

    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.validate = {}

    //Master <mat-list>
    this.master = {
      save030AppealReasonCodeList: [],
    }

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave03Process().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.save030AppealReasonCodeList
        console.log("come on", this.master.save030AppealReasonCodeList)
      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save03Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          pThis.global.setLoading(false)
        })
      }

      this.global.setLoading(false)
    })
  }

  loadData(data: any): void {
    this.input = data

    console.log(this.input)
    console.log(this.input.trademark_status_code)

    this.listPeople = data.people_list || []
    this.listRepresentative = data.representative_list || []
    this.help.Clone(data.contact_address || {}, this.contactAddress)
    this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"

    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []

    this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
    this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')

    this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')

    this.is_autocomplete_ListNotSent_show = false
  }

  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: { request_number: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    let pThis = this
    this.SaveProcessService.Save03ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListNotSent = data
        }
      }
    })
    this.input.is_autocomplete_ListNotSent_load = false
  }
  autocompleteChooseListNotSent(data: any): void {
    this.loadData(data)

    this.is_autocomplete_ListNotSent_show = false
  }

  saveData(): any {
    const params = this.input
    params.people_list = this.listPeople
    params.representative_list = this.listRepresentative
    params.contact_address = this.contactAddress

    return params
  }

  onClickSave03Save(): void {
    //if(this.validateSave03Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave03Save(this.saveData())
    //}
  }
  validateSave03Save(): boolean {
    let result = validateService('validateSave03Save', this.input)
    this.validate = result.validate
    return result.isValid
  }

  //! <<< Call API >>>
  callSave03Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save03Save(params).subscribe((data: any) => {
      // if(isValidSave03SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }

  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }

  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  //Toggle modal & boolean
  toggleModal(name: string): void {
    if (!this.modal[name]) {
      this.modal[name] = true
    } else {
      this.modal[name] = false
      switch(name){
        
        case 'createNewOwner': 
          this.createNewOwner = ''
        break;

      }
    }
  }
  toggleBoolean(obj: string, name: any): void {
    this[obj][name] = !this[obj][name]
  }

}
