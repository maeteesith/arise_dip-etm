import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role02-check-list",
  templateUrl: "./public-role02-check-list.component.html",
  styleUrls: ["./public-role02-check-list.component.scss"]
})
export class PublicRole02CheckListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole02Check
  public listPublicRole02Check: any[]
  public paginatePublicRole02Check: any
  public perPagePublicRole02Check: number[]
  // Response PublicRole02Check
  public responsePublicRole02Check: any
  public listPublicRole02CheckEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public publicRole02CheckStatusCodeList: any[]
  public addressNationalityCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //public_end_start_date: getMoment(),
      //public_end_end_date: getMoment(),
      //public_role02_check_start_date: getMoment(),
      //public_role02_check_end_date: getMoment(),
      address_nationality_code: '',
      request_number: '',
      book_index: '',
    }
    this.listPublicRole02Check = []
    this.paginatePublicRole02Check = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole02Check.id = 'paginatePublicRole02Check'
    this.perPagePublicRole02Check = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      publicRole02CheckStatusCodeList: [],
      addressNationalityCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole02CheckList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.publicRole02CheckStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.public_role02_check_status_code = "WAIT"
        this.master.addressNationalityCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        // this.input.address_nationality_code = "ALL"
      }
      if (this.editID) {
        this.PublicProcessService.PublicRole02CheckLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }

  onClickPublicRole02CheckList(): void {
    // if(this.validatePublicRole02CheckList()) {
    // Open loading
    // Call api
    this.callPublicRole02CheckList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole02CheckList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole02CheckList(this.help.GetFilterParams(params, this.paginatePublicRole02Check)).subscribe((data: any) => {
      // if(isValidPublicRole02CheckListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickPublicRole02CheckAutoDo(): void {
    //if(this.validatePublicRole02CheckAutoDo()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callPublicRole02CheckAutoDo(this.listPublicRole02Check)
    //}
  }
  validatePublicRole02CheckAutoDo(): boolean {
    let result = validateService('validatePublicRole02CheckAutoDo', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callPublicRole02CheckAutoDo(params: any): void {
    this.PublicProcessService.PublicRole02CheckAutoDo(params).subscribe((data: any) => {
      // if(isValidPublicRole02CheckAutoDoResponse(res)) {
      if (data) {
        // Set value
        //this.loadData(data)
        this.onClickPublicRole02CheckList();
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickPublicRole02CheckAdd(): void {
    this.listPublicRole02Check.push({
      index: this.listPublicRole02Check.length + 1,
      request_number: null,
      request_date_text: null,
      trademark: null,
      trademark_sound: null,
      book_index: null,
      page_index_line_index: null,
      request_item_sub_type_1_code_text: null,
      address_nationality_code: null,
      public_end_date: null,
      public_role02_check_remark: null,
      public_role02_check_status_name: null,

    })
    this.changePaginateTotal(this.listPublicRole02Check.length, 'paginatePublicRole02Check')
  }

  onClickPublicRole02CheckEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickPublicRole02CheckDelete(item: any): void {
    // if(this.validatePublicRole02CheckDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listPublicRole02Check.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listPublicRole02Check.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listPublicRole02Check.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listPublicRole02Check.length; i++) {
          if (this.listPublicRole02Check[i].is_check) {
            this.listPublicRole02Check[i].cancel_reason = rs
            this.listPublicRole02Check[i].status_code = "DELETE"
            this.listPublicRole02Check[i].is_deleted = true

            if (true && this.listPublicRole02Check[i].id) ids.push(this.listPublicRole02Check[i].id)
            // else this.listPublicRole02Check.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callPublicRole02CheckDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listPublicRole02Check.length; i++) {
          this.listPublicRole02Check[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callPublicRole02CheckDelete(params: any, ids: any[]): void {
    this.PublicProcessService.PublicRole02CheckDelete(params).subscribe((data: any) => {
      // if(isValidPublicRole02CheckDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listPublicRole02Check.length; i++) {
          if (this.listPublicRole02Check[i].id == id) {
            this.listPublicRole02Check.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listPublicRole02Check.length; i++) {
        this.listPublicRole02Check[i] = i + 1
      }

      this.onClickPublicRole02CheckList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listPublicRole02Check = data.publicrole02check_list || []
    this.changePaginateTotal(this.listPublicRole02Check.length, 'paginatePublicRole02Check')

  }

  listData(data: any): void {
    this.listPublicRole02Check = data.list || []
    this.help.PageSet(data, this.paginatePublicRole02Check)
    //this.listPublicRole02Check = data.list || []
    //this.changePaginateTotal(this.listPublicRole02Check.length, 'paginatePublicRole02Check')

  }

  saveData(): any {
    // let params = this.input
    // params.publicrole02check_list = this.listPublicRole02Check || []

    const params = {
      public_end_start_date: this.input.public_end_start_date,
      public_end_end_date: this.input.public_end_end_date,
      public_role02_check_start_date: this.input.public_role02_check_start_date,
      public_role02_check_end_date: this.input.public_role02_check_end_date,
      public_role02_check_status_code: this.input.public_role02_check_status_code,
      //address_nationality_code: this.input.address_nationality_code,
      request_number: this.input.request_number,

      //page_index: +this.paginatePublicRole02Check.currentPage,
      //item_per_page: +this.paginatePublicRole02Check.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'public_end_date',
      //  value: displayDateServer(this.input.public_end_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_end_date',
      //  value: displayDateServer(this.input.public_end_end_date),
      //  operation: 4
      //}, {
      //  key: 'public_role02_check_date',
      //  value: displayDateServer(this.input.public_role02_check_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_role02_check_date',
      //  value: displayDateServer(this.input.public_role02_check_end_date),
      //  operation: 4
      //}, {
      //  key: 'public_role02_check_status_code',
      //  value: this.input.public_role02_check_status_code,
      //  operation: 0
      //}, {
      //  key: 'address_nationality_code',
      //  value: this.input.address_nationality_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}, {
      //  key: 'book_index',
      //  value: this.input.book_index,
      //  operation: 5
      //}]
    }

    return params
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === 'paginatePublicRole02Check') {
      this.onClickPublicRole02CheckList()
      this.input.is_check_all = false
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === '' ? 1 : page
    if ((+page || page === '') && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name)
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
    this.managePaginateCallApi(name)
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
