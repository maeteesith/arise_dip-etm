import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { CheckingProcessService } from '../../services/checking-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayDate,
  displayMoney,
  displayString,
  clone
} from '../../helpers'
import { observable } from 'rxjs';

@Component({
  selector: "app-considering-similar-instruction",
  templateUrl: "./considering-similar-instruction.component.html",
  styleUrls: ["./considering-similar-instruction.component.scss"]
})
export class ConsideringSimilarInstructionComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteItemSubType1List: any
  public is_autocomplete_ItemSubType1SuggestionList_show: any
  public is_autocomplete_ItemSubType1SuggestionList_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List ConsideringSimilarInstruction
  public listConsideringSimilarInstruction: any[]
  public paginateConsideringSimilarInstruction: any
  public perPageConsideringSimilarInstruction: number[]
  // Response ConsideringSimilarInstruction
  public responseConsideringSimilarInstruction: any
  public listConsideringSimilarInstructionEdit: any


  //label_save_combobox || label_save_radio
  public consideringSimilarInstructionTypeCodeList: any[]
  public consideringSimilarEvidence27CodeList: any[]
  //label_save_combobox || label_save_radio

  public InstructionFocus: any
  public InstructionEdit: any
  public consideringSimilarInstructionRuleItemCodeList: any[]

  public consideringSimilarInstructionRuleSelect: any

  public ItemSubType1SuggestionList: any[]

  //Modal Initial
  //Modal Initial
  public role9_item_sub_type1_code_show: any

  public instruction_rule_list: any[]
  public instruction_rule_list_show: any[]
  public is_updated_instruction_rule_list: any


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
      isModalInstruction: false,
    }

    this.input = {
      id: null,
      request_number: '',
      request_date_text: '',
      name: '',
      request_item_sub_type_1_code_text: '',
      request_item_sub_type_1_description: '',
      sound_description: '',
      considering_receiver_by_name: '',
      considering_similar_instruction_type_code: '',
      considering_similar_evidence_27_code: '',
      considering_similar_instruction_description: '',
      checking_receiver_by_name: '',
      full_view: { instruction_rule_list: [] },
    }
    //this.input.full_view.instruction_rule_list = []
    this.paginateConsideringSimilarInstruction = CONSTANTS.PAGINATION.INIT
    this.perPageConsideringSimilarInstruction = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
      consideringSimilarInstructionTypeCodeList: [],
      consideringSimilarEvidence27CodeList: [],
      consideringSimilarInstructionRuleCodeList: [],
      consideringSimilarInstructionRuleItemCodeList: [],
    }
    //Master List

    this.InstructionFocus = {}
    this.InstructionEdit = {}
    this.consideringSimilarInstructionRuleItemCodeList = []

    //
    this.autocompleteItemSubType1List = []
    this.is_autocomplete_ItemSubType1SuggestionList_show = false
    this.is_autocomplete_ItemSubType1SuggestionList_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.consideringSimilarInstructionRuleSelect = {}

    this.role9_item_sub_type1_code_show = 'ALL'

    this.instruction_rule_list = []
    this.instruction_rule_list_show = []
    this.is_updated_instruction_rule_list = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initConsideringSimilarInstruction().subscribe((data: any) => {
      if (data) {
        this.master = data

      }
      if (this.editID) {
        this.CheckingProcessService.ConsideringSimilarInstructionLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)

          //// For Test
          //this.onClickConsideringSimilarInstructionAdd()
          //this.InstructionEdit["instruction_rule_code"] = "ROLE_9"
          //this.ItemUpdate()
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private CheckingProcessService: CheckingProcessService
  ) { }

  onChangeConsideringSimilarInstructionRuleCodeList(code: any): void {
    //this.InstructionEdit.rule_code = code
    this.InstructionEdit.item_list = [{}]
    this.ItemUpdate()
  }

  onClickConsideringSimilarInstructionSend(): void {
    //if(this.validateConsideringSimilarInstructionSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callConsideringSimilarInstructionSend(this.editID)
    //}
  }
  validateConsideringSimilarInstructionSend(): boolean {
    let result = validateService('validateConsideringSimilarInstructionSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarInstructionSend(params: any): void {
    this.CheckingProcessService.ConsideringSimilarInstructionSend(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarInstructionSendResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
        this.automateTest.test(this, { Send: 1 });
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickConsideringSimilarInstructionSave(): void {
    //if(this.validateConsideringSimilarInstructionSave()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callConsideringSimilarInstructionSave(this.saveData())
    //}
  }
  validateConsideringSimilarInstructionSave(): boolean {
    let result = validateService('validateConsideringSimilarInstructionSave', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarInstructionSave(params: any): void {
    this.CheckingProcessService.ConsideringSimilarInstructionSave(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarInstructionSaveResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickConsideringSimilarInstructionAdd(): void {
    var item = {
      save_id: +this.editID,
      instruction_date: getMoment(),
      item_list: [{}],
    }

    //this.input.full_view.instruction_rule_list.push(item)

    this.onClickConsideringSimilarInstructionEdit(item)

    this.changePaginateTotal(this.input.full_view.instruction_rule_list.length, 'paginateConsideringSimilarInstruction')
  }

  onClickConsideringSimilarInstructionEdit(item: any): void {
    this.InstructionFocus = item

    this.InstructionEdit = {
      ...this.InstructionFocus,
    }

    this.toggleModal("isModalInstruction")

    this.InstructionEdit["instruction_rule_code"] = this.InstructionEdit["instruction_rule_code"] || "ROLE_7"
    this.ItemUpdate()
  }

  onClickConsideringSimilarInstructionRuleSave(): void {
    //this.ItemUpdate()

    //Object.keys(this.InstructionEdit).forEach((item: any) => {
    //  this.InstructionFocus[item] = this.InstructionEdit[item]
    //})
    this.callConsideringSimilarInstructionRuleSave(this.saveData())
  }

  callConsideringSimilarInstructionRuleSave(code: any): void {
    //this.InstructionEdit.instruction_rule_code = code
    //this.ItemUpdate()
    this.global.setLoading(true)
    this.CheckingProcessService.ConsideringSimilarInstructionRuleSave(this.InstructionEdit).subscribe((data: any) => {
      // if(isValidConsideringSimilarInstructionRuleSaveResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
        this.automateTest.test(this, { Add: 1 })
      }
      // }
      // Close loading
      this.global.setLoading(false)
      this.toggleModal("isModalInstruction")
    })
  }

  ItemUpdate(item: any = this.InstructionEdit): void {
    this.consideringSimilarInstructionRuleSelect = { ... this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == item.instruction_rule_code)[0] };
    console.log(this.consideringSimilarInstructionRuleSelect)
    //item["instruction_date_text"] = displayDate(item.instruction_date)
    //item["instruction_rule_description"] = this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == item.instruction_rule_code)[0].description
    //item["instruction_rule_value_1"] = this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == item.instruction_rule_code)[0].value_1
    //item["instruction_rule_value_2"] = this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == item.instruction_rule_code)[0].value_2
    //item["instruction_rule_value_3"] = this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == item.instruction_rule_code)[0].value_3
    //item["instruction_rule_value_4"] = this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == item.instruction_rule_code)[0].value_4
    //item["instruction_rule_value_5"] = this.master.consideringSimilarInstructionRuleCodeList.filter(r => r.code == item.instruction_rule_code)[0].value_5
    this.consideringSimilarInstructionRuleItemCodeList = this.master.consideringSimilarInstructionRuleItemCodeList.filter(r => r.parent_code == item.instruction_rule_code)

    if (this.consideringSimilarInstructionRuleSelect.code == "ROLE_9") {
      this.consideringSimilarInstructionRuleItemCodeList = []

      this.ItemSubType1SuggestionList = []
      this.InstructionEdit.role9_item_sub_type1_list = this.ItemSubType1SuggestionList

      this.global.setLoading(true)
      this.CheckingProcessService.ItemSubType1SuggestionLoad(this.editID).subscribe((data: any) => {
        if (data) {
          this.ItemSubType1SuggestionList = data
          this.InstructionEdit.role9_item_sub_type1_list = this.ItemSubType1SuggestionList

          // Find role9_item_sub_type1_distinct_list
          var buffer = {}
          this.InstructionEdit.role9_item_sub_type1_list.forEach((item: any) => {
            buffer[item.request_item_sub_type_1_code] = item.request_item_sub_type_1_code
          })
          this.InstructionEdit.role9_item_sub_type1_distinct_list = []
          Object.keys(buffer).forEach((item: any) => {
            this.InstructionEdit.role9_item_sub_type1_distinct_list.push({
              code: buffer[item],
            })
          })

          // Default item_code and item_name
          this.InstructionEdit.role9_item_sub_type1_list.forEach((item: any) => {
            if (!item.item_code) {
              if (item.suggestion_list && item.suggestion_list.length > 0) {
                item.item_code = item.suggestion_list[0].code
                item.item_name = item.suggestion_list[0].name
              }
            }
          })

          //this.ItemSubType1SuggestionList[0].suggestion_item_id = "0"
          //this.loadData(data)
        }
        this.global.setLoading(false)
      })
    }
  }

  onClickConsideringSimilarInstructionDelete(item: any): void {
    // if(this.validateConsideringSimilarInstructionDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.input.full_view.instruction_rule_list.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.input.full_view.instruction_rule_list.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.input.full_view.instruction_rule_list.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.input.full_view.instruction_rule_list.length; i++) {
          if (this.input.full_view.instruction_rule_list[i].is_check) {

            if (this.input.full_view.instruction_rule_list[i].id) {
              ids.push(this.input.full_view.instruction_rule_list[i].id)

              this.input.full_view.instruction_rule_list[i].cancel_reason = rs
              this.input.full_view.instruction_rule_list[i].status_code = "DELETE"
              //this.input.full_view.instruction_rule_list[i].is_deleted = true
            } else
              this.input.full_view.instruction_rule_list.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            //Call api
            this.callConsideringSimilarInstructionDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.input.full_view.instruction_rule_list.length; i++) {
          this.input.full_view.instruction_rule_list[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callConsideringSimilarInstructionDelete(params: any, ids: any[]): void {
    this.CheckingProcessService.ConsideringSimilarInstructionDelete(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarInstructionDeleteResponse(res)) {

      this.loadData(data)

      //ids.forEach((id: any) => {
      //    for (let i = 0; i < this.input.full_view.instruction_rule_list.length; i++) {
      //        if (this.input.full_view.instruction_rule_list[i].id == id) {
      //            this.input.full_view.instruction_rule_list.splice(i--, 1);
      //        }
      //    }
      //});

      //for (let i = 0; i < this.input.full_view.instruction_rule_list.length; i++) {
      //    this.input.full_view.instruction_rule_list[i] = i + 1
      //}


      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickConsideringSimilar(): void {
    var win = window.open("./considering-similar/edit/" + this.editID, "")
  }

  onClickConsideringSimilarDocument(): void {
    var win = window.open("./considering-similar-document/edit/" + this.editID, "")
  }


  onClickConsideringSimilarInstructionPublicAdd(): void {
    //if(this.validateConsideringSimilarInstructionSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callConsideringSimilarInstructionPublicAdd(this.editID)
    //}
  }
  validateConsideringSimilarInstructionPublicAdd(): boolean {
    let result = validateService('validateConsideringSimilarInstructionSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarInstructionPublicAdd(params: any): void {
    this.CheckingProcessService.ConsideringSimilarInstructionPublicAdd(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarInstructionSendResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
        this.automateTest.test(this, { Public: 1 });
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickConsideringSimilarInstructionPublicDelete(): void {
    //if(this.validateConsideringSimilarInstructionSend()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callConsideringSimilarInstructionPublicDelete(this.editID)
    //}
  }
  validateConsideringSimilarInstructionPublicDelete(): boolean {
    let result = validateService('validateConsideringSimilarInstructionSend', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callConsideringSimilarInstructionPublicDelete(params: any): void {
    this.CheckingProcessService.ConsideringSimilarInstructionPublicDelete(params).subscribe((data: any) => {
      // if(isValidConsideringSimilarInstructionSendResponse(res)) {
      if (data) {
        // Set value
        this.loadData(data)
        this.automateTest.test(this, { Public: 1 });
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  // Modal Location
  autocompleteChangeItemSubType1SuggestionList(row_item: any, value: any, item_per_page: any = 5, length: any = 2): void {
    row_item.item_id = null
    row_item.item_code = null
    console.log(value)
    if (value.length >= length) {
      this.is_autocomplete_ItemSubType1SuggestionList_show = row_item

      let params = {
        search_by: [{
          key: "name",
          value: value,
          operation: 5,
          item_per_page: item_per_page,
        }],
        item_per_page: item_per_page,
      }

      this.callAutocompleteChangeItemSubType1SuggestionList(row_item, params)
    }
  }
  autocompleteBlurItemSubType1SuggestionList(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ItemSubType1SuggestionList_show = false
    }, 200)
  }
  callAutocompleteChangeItemSubType1SuggestionList(row_item: any, params: any): void {
    if (this.input.is_autocomplete_ItemSubType1SuggestionList_load) return
    this.input.is_autocomplete_ItemSubType1SuggestionList_load = true
    let pThis = this
    this.CheckingProcessService.ItemSubType1SuggestionList(params).subscribe((data: any) => {
      if (data && data.list) {
        if (data.list.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseItemSubType1SuggestionList(row_item, data.list[0])
          }, 200)
        } else {
          pThis.autocompleteItemSubType1List = data.list
        }
      }
    })
    this.input.is_autocomplete_ItemSubType1SuggestionList_load = false
  }
  autocompleteChooseItemSubType1SuggestionList(row_item: any, data: any): void {
    row_item.item_id = data.id
    row_item.item_code = data.code
    row_item.item_name = data.name
    //this.inputModalAddress.address_sub_district_code = data.code
    //this.inputModalAddress.address_sub_district_name = data.name
    //this.inputModalAddress.address_district_code = data.district_code
    //this.inputModalAddress.address_district_name = data.district_name
    //this.inputModalAddress.address_province_code = data.province_code
    //this.inputModalAddress.address_province_name = data.province_name
    //this.inputModalAddress.address_country_code = data.country_code
    //this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ItemSubType1SuggestionList_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    if (data.full_view.instruction_rule_list) {
      this.instruction_rule_list.length = 0
      data.full_view.instruction_rule_list.forEach((item: any) => {
        item.request_number_save_list = item.request_number_save_list || [{}]

        this.instruction_rule_list.push(item)
        this.is_updated_instruction_rule_list.updated = true
      })
    }
    this.input = data

    //this.input.full_view.instruction_rule_list.forEach((item: any) => {
    //    //item.instruction_date_text = displayDate(item.instruction_date)
    //    item.instruction_rule_code_text = "มาตรา " + item.instruction_rule_code
    //})

    //console.log(this.input.full_view.instruction_rule_list)

    //this.input.full_view.instruction_rule_list = data.considering_similar_instruction_list || []
    this.changePaginateTotal(this.input.full_view.instruction_rule_list.length, 'paginateConsideringSimilarInstruction')

  }

  listData(data: any): void {
    this.input.full_view.instruction_rule_list = data || []
    this.changePaginateTotal(this.input.full_view.instruction_rule_list.length, 'paginateConsideringSimilarInstruction')

  }

  saveData(): any {
    let params = this.input
    //console.log(this.ItemSubType1SuggestionList)
    if (this.InstructionEdit.role9_item_sub_type1_list && this.InstructionEdit.role9_item_sub_type1_list.length > 0) {
      //    this.InstructionEdit.role9_item_sub_type1_list = []
      //    params.InstructionEdit = this.InstructionEdit
      this.InstructionEdit.role9_item_sub_type1_list.forEach((item: any) => {
        item.suggestion_item_id = item.suggestion_item_id && item.suggestion_item_id != "" ? +item.suggestion_item_id : null
        //        if (item.id && item.id > 0) {
        //            this.InstructionEdit.role9_item_sub_type1_list.push({
        //                save010_product_id: item.id,
        //                item_id: item.autocomplete_request_item_sub_type_1_id,
        //                item_code: item.autocomplete_request_item_sub_type_1_code,
        //                item_name: item.autocomplete_request_item_sub_type_1_name,
        //            })
        //        }
      })
    }

    //params.considering_similar_instruction_list = this.input.full_view.instruction_rule_list || []

    return params
  }

  onChangeConsideringSimilarInstructionRuleItemCodeChange(row_item): any {
    row_item.instruction_rule_description = this.consideringSimilarInstructionRuleItemCodeList.filter(r => r.code == row_item.instruction_rule_item_code)[0].name
  }


  onClickShowRole9ItemSubType(code: any, row_item: any): void {
    //console.log(code)
    row_item.ShowRole9ItemSubType = code
    this.onShowRole9Filter(row_item)

    //if (code == 'ALL') {
    //  this.ItemSubType1SuggestionList = this.InstructionEdit.role9_item_sub_type1_list
    //} else {
    //  this.ItemSubType1SuggestionList = this.InstructionEdit.role9_item_sub_type1_list.filter(r => r.request_item_sub_type_1_code == code)
    //}
  }

  onClickShowRole9Automatching(code: any, row_item: any): void {
    row_item.ShowRole9Automatching = code
    this.onShowRole9Filter(row_item)
    //console.log(code)
    //if (code == 'ALL') {
    //  this.ItemSubType1SuggestionList = this.InstructionEdit.role9_item_sub_type1_list
    //} else {
    //  this.ItemSubType1SuggestionList = this.InstructionEdit.role9_item_sub_type1_list.filter(r => r.request_item_sub_type_1_code == code)
    //}
  }

  onShowRole9Filter(row_item: any): void {
    row_item.ShowRole9ItemSubType = row_item.ShowRole9ItemSubType || "ALL"
    row_item.ShowRole9Automatching = row_item.ShowRole9Automatching || "ALL"

    this.ItemSubType1SuggestionList = this.InstructionEdit.role9_item_sub_type1_list

    if (row_item.ShowRole9ItemSubType != 'ALL') {
      this.ItemSubType1SuggestionList = this.ItemSubType1SuggestionList.filter(r =>
        r.request_item_sub_type_1_code == row_item.ShowRole9ItemSubType
      )
    }
    if (row_item.ShowRole9Automatching == 'YES') {
      this.ItemSubType1SuggestionList = this.ItemSubType1SuggestionList.filter(r =>
        !r.suggestion_list || r.suggestion_list.length == 0 || r.suggestion_list[0].code != r.request_item_sub_type_1_code
      )
    } else if (row_item.ShowRole9Automatching == 'NO') {
      this.ItemSubType1SuggestionList = this.ItemSubType1SuggestionList.filter(r =>
        r.suggestion_list && r.suggestion_list.length > 0 && r.suggestion_list[0].code == r.request_item_sub_type_1_code
      )
    }
  }

  onClickConsideringSimilarInstructionRequestNumberInsert(): void {
    console.log(this.InstructionEdit)
    this.InstructionEdit.request_number_save_list = this.InstructionEdit.request_number_save_list || []
    this.InstructionEdit.request_number_save_list.push({})
  }

  onClickConsideringSimilarInstructionRequestNumberDelete(row_item: any): void {
    row_item.is_deleted = true
  }

  onClickConsideringSimilarInstructionRequestNumberKeyPress(row_item: any, event: any): void {
    console.log(event)
    if (event.key == "Enter") {
      this.callConsideringSimilarInstructionRequestNumberAdd(row_item)
    }
  }
  callConsideringSimilarInstructionRequestNumberAdd(row_item: any): void {
    let param = {
      request_number: row_item.request_number,
      //instruction_rule_id: this.editID ? +this.editID : null,
      //request_number: row_item.request_number,
      //is_deleted: row_item.is_deleted,
    }
    this.global.setLoading(true)
    this.CheckingProcessService.ConsideringSimilarInstructionRequestNumberAdd(param).subscribe((data: any) => {
      // if(isValidConsideringSimilarInstructionRequestNumberAddResponse(res)) {
      //console.log(data)
      if (data) {
        Object.keys(data).forEach((item: any) => {
          if (item != "id") row_item[item] = data[item]
        })
        // Set value
        //this.loadData(data)
        //this.automateTest.test(this, { Send: 1 });
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }


  onClickConsideringSimilarPrint(): void {
    window.open('/pdf/ConsideringSimilaInstruction/' + this.input.request_number)
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
