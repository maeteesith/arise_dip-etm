import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-save-07-process",
  templateUrl: "./save-07-process.component.html",
  styleUrls: ["./save-07-process.component.scss"]
})
export class Save07ProcessComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List People01
  public listPeople01: any[]
  public paginatePeople01: any
  public perPagePeople01: number[]
  // Response People01
  public responsePeople01: any
  public listPeople01Edit: any

  // List Product01
  public listProduct01: any[]
  public paginateProduct01: any
  public perPageProduct01: number[]
  // Response Product01
  public responseProduct01: any
  public listProduct01Edit: any

  // List Product
  public listProduct: any[]
  public paginateProduct: any
  public perPageProduct: number[]
  // Response Product
  public responseProduct: any
  public listProductEdit: any

  // List ContactAddress
  public listContactAddress: any[]
  public paginateContactAddress: any
  public perPageContactAddress: number[]
  // Response ContactAddress
  public responseContactAddress: any
  public listContactAddressEdit: any


  //label_save_combobox || label_save_radio
  public addressTypeCodeList: any[]
  public addressCardTypeCodeList: any[]
  public addressReceiverTypeCodeList: any[]
  public addressSexCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  public autocompleteListListLocation: any
  public is_autocomplete_ListLocation_show: any
  public is_autocomplete_ListLocation_load: any
  //Modal Initial
  //Modal Initial

  public contactAddress: any

  public product_select: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      registration_number: '',
      make_date: getMoment(),
      request_number_from: '',
      request_index: '',
      request_date_text: '',
      total_price: '',
      fee: '',
      irn_number: '',
    }
    this.listPeople01 = []
    this.paginatePeople01 = CONSTANTS.PAGINATION.INIT
    this.perPagePeople01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct01 = []
    this.paginateProduct01 = CONSTANTS.PAGINATION.INIT
    this.perPageProduct01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct = []
    this.paginateProduct = CONSTANTS.PAGINATION.INIT
    this.perPageProduct = CONSTANTS.PAGINATION.PER_PAGE

    this.listContactAddress = []
    this.paginateContactAddress = CONSTANTS.PAGINATION.INIT
    this.perPageContactAddress = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false
    this.autocompleteListListLocation = []
    this.is_autocomplete_ListLocation_show = false
    this.is_autocomplete_ListLocation_load = false


    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.contactAddress = {
      address_type_code: "OWNER"
    }

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave07Process().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
        this.master.addressCountryCodeList.unshift({ "code": "", "name": "ไม่พบข้อมูล" });
      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save07Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            pThis.loadData(data)
          }
          // Close loading
          pThis.global.setLoading(false)
        })
      }

      this.global.setLoading(false)
    })
  }

  constructor(
    private help: Help,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService
  ) { }


  autocompleteKeyDown(event: any) {
    if (event.key == "Enter") {
      this.autocompleteChangeListNotSent(this.input, 'request_number', 5, 1)
    }
  }

  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    console.log(object[name])
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: {
          request_number: object[name],
        },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    this.global.setLoading(true)
    let pThis = this
    this.SaveProcessService.Save07ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListNotSent = data
          this.is_autocomplete_ListNotSent_show = false
          this.global.setLoading(false)
        }
      }
    })
    this.input.is_autocomplete_ListNotSent_load = false
  }
  autocompleteChooseListNotSent(data: any): void {
    this.loadData(data)

    this.is_autocomplete_ListNotSent_show = false
    this.global.setLoading(false)
  }


  onClickSave07Madrid(): void {
    //if(this.validateSave07Madrid()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave07Madrid(this.saveData())
    //}
  }
  validateSave07Madrid(): boolean {
    let result = validateService('validateSave07Madrid', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave07Madrid(params: any): void {
    let pThis = this
    this.SaveProcessService.Save07Madrid(params).subscribe((data: any) => {
      // if(isValidSave07MadridResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  // Autocomplete
  autocompleteChangeListLocation(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListLocation(params)
    }
  }
  autocompleteBlurListLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListLocation(params: any): void {
    if (this.input.is_autocomplete_ListLocation_load) return
    this.input.is_autocomplete_ListLocation_load = true
    let pThis = this
    this.SaveProcessService.Save07ListLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListLocation_load = false
  }
  autocompleteChooseListLocation(data: any): void {
    this.inputAddress.address_sub_district_code = data.code
    this.inputAddress.address_sub_district_name = data.name
    this.inputAddress.address_district_code = data.district_code
    this.inputAddress.address_district_name = data.district_name
    this.inputAddress.address_province_code = data.province_code
    this.inputAddress.address_province_name = data.province_name
    this.inputAddress.address_country_code = data.country_code
    this.inputAddress.address_country_name = data.country_name

    this.is_autocomplete_ListLocation_show = false
  }


  onClickSave07Delete(): void {
    //if(this.validateSave07Delete()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave07Delete(this.saveData())
    //}
  }
  validateSave07Delete(): boolean {
    let result = validateService('validateSave07Delete', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave07Delete(params: any): void {
    let pThis = this
    this.SaveProcessService.Save07Delete(params).subscribe((data: any) => {
      // if(isValidSave07DeleteResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave07Save(): void {
    //if(this.validateSave07Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave07Save(this.saveData())
    //}
  }
  validateSave07Save(): boolean {
    let result = validateService('validateSave07Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave07Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save07Save(params).subscribe((data: any) => {
      // if(isValidSave07SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickSave07DocumentView(): void {
    window.open('/File/RequestDocumentCollect/70/' + this.input.request_number + "_" + this.input.request_id)
  }
  validateSave07DocumentView(): boolean {
    let result = validateService('validateSave07DocumentView', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave07DocumentView(params: any): void {
    let pThis = this
    this.SaveProcessService.Save07DocumentView(params).subscribe((data: any) => {
      // if(isValidSave07DocumentViewResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave07Send(): void {
    //if(this.validateSave07Send()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave07Send(this.saveData())
    //}
  }
  validateSave07Send(): boolean {
    let result = validateService('validateSave07Send', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave07Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save07Send(params).subscribe((data: any) => {
      // if(isValidSave07SendResponse(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave070RequestBack(): void {
    //if(this.validateSave070Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave070RequestBack(this.saveData())
    //}
  }
  callSave070RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save070RequestBack(params).subscribe((data: any) => {
      // if(isValidSave070SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  onClickSave07CopyProductt01(): void {
    //if(this.validateSave07CopyProductt01()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave07CopyProductt01(this.saveData())
    //}
  }
  validateSave07CopyProductt01(): boolean {
    let result = validateService('validateSave07CopyProductt01', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave07CopyProductt01(params: any): void {
    let pThis = this
    this.SaveProcessService.Save07CopyProductt01(params).subscribe((data: any) => {
      // if(isValidSave07CopyProductt01Response(res)) {
      if (data) {
        // Set value
        pThis.loadData(data)
      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave07People01Add(): void {
    this.listPeople01.push({
      card_type_name: null,
      card_number: null,
      name: null,
      house_number: null,
      telephone: null,

    })
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
  }

  onClickSave07Product01Add(): void {
    this.listProduct01.push({
      index: this.listProduct01.length + 1,
      request_item_sub_type_1_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
  }

  onClickSave07ProductAdd(): void {
    this.listProduct.push({
      index: this.listProduct.length + 1,
      request_item_sub_type_1_code: null,
      request_item_sub_type_2_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
  }

  onClickProductClone(list, paginate): void {
    this.input.product_01_list.forEach((item: any) => {
      list.push({
        request_item_sub_type_1_code: item.request_item_sub_type_1_code,
        description: item.description,
      })
    })
    this.changePaginateTotal(list.length, paginate)
  }

  onClickSave07ProductDelete(item: any): void {
    // if(this.validateSave07ProductDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listProduct.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listProduct.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listProduct.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listProduct.length; i++) {
          if (this.listProduct[i].is_check) {
            this.listProduct[i].cancel_reason = rs
            this.listProduct[i].status_code = "DELETE"
            this.listProduct[i].is_deleted = true

            if (true && this.listProduct[i].id) ids.push(this.listProduct[i].id)
            // else this.listProduct.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave07ProductDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listProduct.length; i++) {
          this.listProduct[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callSave07ProductDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save07ProductDelete(params).subscribe((data: any) => {
      // if(isValidSave07ProductDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listProduct.length; i++) {
          if (this.listProduct[i].id == id) {
            this.listProduct.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listProduct.length; i++) {
        this.listProduct[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave07ContactAddressAdd(): void {
    this.listContactAddress.push({

    })
    this.changePaginateTotal(this.listContactAddress.length, 'paginateContactAddress')
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    this.SaveProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pThis.autocompleteChooseListModalLocation(data[0])
          }, 200)
        } else {
          pThis.autocompleteListListModalLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.help.Clone(data.contact_address || {}, this.contactAddress)
    this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"

    //this.inputAddress = data.contact_address || {}
    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []

    data.product_list = data.product_list || []
    this.listProduct = []
    data.product_list.filter(r => !r.is_deleted).forEach((item: any) => {
      var product_list = this.listProduct.filter(r => r.request_item_sub_type_1_code == item.request_item_sub_type_1_code)
      if (product_list.length > 0) product_list[0].description += "  " + item.description
      else this.listProduct.push({
        request_item_sub_type_1_code: item.request_item_sub_type_1_code,
        description: item.description,
      })
    })


    this.listContactAddress = data.contact_address_list || []
    let index = 1
    index = 1
    this.listPeople01.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listProduct01.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listProduct.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listContactAddress.map((item: any) => { item.is_check = false; item.index = index++; return item })
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
    this.changePaginateTotal(this.listContactAddress.length, 'paginateContactAddress')

  }

  listData(data: any): void {
    this.listPeople01 = data || []
    this.listProduct01 = data || []


    data.product_list = data.product_list || []
    this.listProduct = []
    data.product_list.filter(r => !r.is_deleted).forEach((item: any) => {
      var product_list = this.listProduct.filter(r => r.request_item_sub_type_1_code == item.request_item_sub_type_1_code)
      if (product_list.length > 0) product_list[0].description += "  " + item.description
      else this.listProduct.push({
        request_item_sub_type_1_code: item.request_item_sub_type_1_code,
        description: item.description,
      })
    })


    this.listContactAddress = data || []
    let index = 1
    index = 1
    this.listPeople01.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listProduct01.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listProduct.map((item: any) => { item.is_check = false; item.index = index++; return item })
    index = 1
    this.listContactAddress.map((item: any) => { item.is_check = false; item.index = index++; return item })
    this.changePaginateTotal(this.listPeople01.length, 'paginatePeople01')
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
    this.changePaginateTotal(this.listProduct.length, 'paginateProduct')
    this.changePaginateTotal(this.listContactAddress.length, 'paginateContactAddress')

  }

  saveData(): any {
    let params = this.input

    params.contact_address = this.contactAddress
    //params.contact_address = this.inputAddress || []
    params.people_01_list = this.listPeople01 || []
    params.product_01_list = this.listProduct01 || []

    params.product_list = []
    this.listProduct.forEach((item: any) => {
      item.description.split("  ").forEach((product: any) => {
        params.product_list.push({
          request_item_sub_type_1_code: item.request_item_sub_type_1_code,
          description: product,
        })
      })
    })


    params.contact_address_list = this.listContactAddress || []

    return params
  }



  onClickProductSelect(row_item: any, is_selected = true) {
    if (is_selected) {
      var pThis = this
      setTimeout(function () {
        row_item.description = row_item.description.replace(/  /g, '\n').trim()
        pThis.product_select = row_item
      }, 100)
    } else {
      if (this.product_select == row_item) {
        row_item.description = row_item.description.replace(/\n/g, '  ').trim()
        this.product_select = null
      }
    }
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
