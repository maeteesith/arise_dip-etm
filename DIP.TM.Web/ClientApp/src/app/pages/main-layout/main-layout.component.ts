import { Component, OnInit } from '@angular/core'
import { GlobalService } from './../../global.service'
import { Router, NavigationEnd } from '@angular/router'
import { Auth } from '../../auth'
import {
  ROUTE_PATH
} from '../../helpers'

@Component({
  selector: "app-main-layout",
  templateUrl: "./main-layout.component.html",
  styleUrls: ["./main-layout.component.scss"]
})
export class MainLayoutComponent implements OnInit {

  //TODO >>> Declarations <<<
  public breadcrumbList: any
  public breadcrumb: any
  public pathHome: any;

  constructor(
    private global: GlobalService,
    private router: Router,
    private auth: Auth
  ) {
    //TODO >>> Detect global change <<<
    this.global.watchUserInfo().subscribe((data: any) => {
      this.initBreadcrumb(data)
    })
    //TODO >>> Detect route change <<<
    this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.breadcrumb = this.manageBreadcrumb()
      }
    })
  }

  ngOnInit() {
    //TODO >>> Init value <<<
    this.breadcrumbList = []
    this.breadcrumb = {}
    this.pathHome = ROUTE_PATH.DASHBOARD.LINK;
    this.initBreadcrumb(this.global.getUserInfo())
  }

  //! <<< Event >>>
  initBreadcrumb(data: any): any {
    if (data) {
      data.uM_PageMenu.forEach((menu: any) => {
        menu.uM_PageMenuSub.forEach((sub: any) => {
          sub.uM_Page.forEach((page: any) => {
            if (ROUTE_PATH[page.code]) {
              this.breadcrumbList.push({
                route: ROUTE_PATH[page.code],
                header: page.name,
                list: [
                  { name: menu.name, code: menu.code },
                  { name: sub.name, code: sub.code },
                  { name: page.name, code: page.code }
                ],
                link: page.page_link ? JSON.parse(page.page_link) : [],
              })
            }
          })
        })
      })

      this.breadcrumb = this.manageBreadcrumb()
      //console.log(this.breadcrumb)
    }
  }
  manageBreadcrumb(): any {
    let result = { list: [] }

    var pathname = "/" + location.pathname.split("/")[1]
    let isEditPage = location.pathname.includes(pathname + "/edit")
    let isListPage = location.pathname.includes(pathname + "/list")
    var pathname_check = pathname + (isEditPage ? "/edit" : "") + (isListPage ? "/list" : "")

    //console.log(pathname)
    //console.log(isEditPage)
    //console.log(isListPage)
    //console.log(pathname_check)

    //console.log(this.breadcrumbList.map((item: any) => { return item.route.LINK }))
    var result_list = this.breadcrumbList.filter(r => r.route.LINK == pathname_check)
    //console.log(result_list)
    this.breadcrumbList.filter(r => r.route.LINK == pathname).forEach((item: any) => {
      result_list.push(item)
    })
    //console.log(result_list)

    if (result_list.length > 0) {
      result = result_list[0]
    }
    //console.log(result_list)
    //console.log(result)

    //var result = 
    //for (let index = 0; index < this.breadcrumbList.length; index++) {
    //  if (isEditPage) {
    //    if (location.pathname.includes(this.breadcrumbList[index].route.LINK)) {
    //      result = this.breadcrumbList[index]
    //      console.log(result)
    //      break
    //    }
    //  } else {
    //    if (location.pathname === this.breadcrumbList[index].route.LINK) {
    //      result = this.breadcrumbList[index]
    //      console.log(result)
    //      break
    //    }
    //  }
    //}

    return result
  }
  getLink(code: any): string {
    return ROUTE_PATH[code] ? ROUTE_PATH[code].LINK : '/'
  }
  isStayDashboard(): boolean {
    return location.pathname === ROUTE_PATH.DASHBOARD.LINK
  }
  logout(): void {
    this.auth.logout()
  }

}
