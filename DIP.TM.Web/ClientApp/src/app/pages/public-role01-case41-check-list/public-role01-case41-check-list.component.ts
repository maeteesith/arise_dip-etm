import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { PublicProcessService } from '../../services/public-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayDateServer,
  clone
} from '../../helpers'

@Component({
  selector: "app-public-role01-case41-check-list",
  templateUrl: "./public-role01-case41-check-list.component.html",
  styleUrls: ["./public-role01-case41-check-list.component.scss"]
})
export class PublicRole01Case41CheckListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  // List PublicRole01Case41Check
  public listPublicRole01Case41Check: any[]
  public paginatePublicRole01Case41Check: any
  public perPagePublicRole01Case41Check: number[]
  // Response PublicRole01Case41Check
  public responsePublicRole01Case41Check: any
  public listPublicRole01Case41CheckEdit: any


  // List PublicProcess
  public listPublicProcess: any[]
  public paginatePublicProcess: any
  public perPagePublicProcess: number[]
  // Response PublicProcess
  public responsePublicProcess: any

  //label_save_combobox || label_save_radio
  public publicReceiveStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial

  public popup: any

  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      //public_receive_start_date: getMoment(),
      //public_receive_end_date: getMoment(),
      public_receive_status_code: '',
      request_number: '',
    }
    this.listPublicRole01Case41Check = []
    this.paginatePublicRole01Case41Check = clone(CONSTANTS.PAGINATION.INIT)
    this.paginatePublicRole01Case41Check.id = 'paginatePublicRole01Case41Check'
    this.perPagePublicRole01Case41Check = clone(CONSTANTS.PAGINATION.PER_PAGE)

    this.listPublicProcess = []

    //Master List
    this.master = {
      publicReceiveStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //

    this.popup = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initPublicRole01Case41CheckList().subscribe((data: any) => {
      if (data) {
        this.master = data
        this.master.publicRole01Case41StatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.public_role01_case41_status_code = "DRAFT"
        //this.master.publicReceiveStatusCodeList.splice(2, 1)
        //this.master.publicReceiveStatusCodeList.splice(3, 1)
      }
      if (this.editID) {
        this.PublicProcessService.PublicRole01Case41CheckLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.listData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private PublicProcessService: PublicProcessService
  ) { }

  onClickPublicRole01Case41CheckList(): void {
    // if(this.validatePublicRole01Case41CheckList()) {
    // Open loading
    // Call api
    this.callPublicRole01Case41CheckList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callPublicRole01Case41CheckList(params: any): void {
    this.global.setLoading(true)
    this.PublicProcessService.PublicRole01Case41CheckList(this.help.GetFilterParams(params, this.paginatePublicRole01Case41Check)).subscribe((data: any) => {
      // if(isValidPublicRole01Case41CheckListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  //onClickPublicRole01Case41CheckDoAuto(): void {
  //  //if(this.validatePublicRole01Case41CheckDoAuto()) {
  //  // Open loading
  //  this.global.setLoading(true)
  //  // Call api
  //  this.callPublicRole01Case41CheckDoAuto(this.listPublicRole01Case41Check)
  //  //}
  //}
  //validatePublicRole01Case41CheckDoAuto(): boolean {
  //  let result = validateService('validatePublicRole01Case41CheckDoAuto', this.input)
  //  this.validate = result.validate
  //  return result.isValid
  //}
  ////! <<< Call API >>>
  //callPublicRole01Case41CheckDoAuto(params: any): void {
  //  this.PublicProcessService.PublicRole01Case41CheckDoAuto(params).subscribe((data: any) => {
  //    // if(isValidPublicRole01Case41CheckDoAutoResponse(res)) {
  //    if (data) {
  //      // Set value
  //      this.onClickPublicRole01Case41CheckList()
  //    }
  //    // }
  //    // Close loading
  //    this.global.setLoading(false)
  //  })
  //}

  //onClickPublicRole01Case41CheckAdd(): void {
  //  this.listPublicRole01Case41Check.push({
  //    index: this.listPublicRole01Case41Check.length + 1,
  //    request_number: null,
  //    request_date_text: null,
  //    instruction_send_date_text: null,
  //    public_receive_date_text: null,
  //    trademark: null,
  //    trademark_sound: null,
  //    type: null,
  //    public_remark: null,
  //    public_status_name: null,

  //  })
  //  this.changePaginateTotal(this.listPublicRole01Case41Check.length, 'paginatePublicRole01Case41Check')
  //}
  onClickPublicRole01Case41CheckEdit(row_item: any): void {
    this.popup.is_item_show = true
    this.popup.rowEdit = row_item
    //var win = window.open("pdf/PublicRoundItem/" + row_item.id);
  }


  //// Modal Location
  //autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
  //  if (object[name].length >= length) {
  //    console.log("d")
  //    this.is_autocomplete_ListModalLocation_show = true

  //    let params = {
  //      name: object[name],
  //      paging: {
  //        item_per_page: item_per_page
  //      },
  //    }

  //    this.callAutocompleteChangeListModalLocation(params)
  //  }
  //}
  //autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
  //  let pThis = this
  //  setTimeout(function () {
  //    pThis.is_autocomplete_ListModalLocation_show = false
  //  }, 200)
  //}
  //callAutocompleteChangeListModalLocation(params: any): void {
  //  if (this.input.is_autocomplete_ListModalLocation_load) return
  //  this.input.is_autocomplete_ListModalLocation_load = true
  //  let pThis = this
  //  // this.PublicProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
  //  //   if (data) {
  //  //     if (data.length == 1) {
  //  //       setTimeout(function () {
  //  //         pThis.autocompleteChooseListModalLocation(data[0])
  //  //       }, 200)
  //  //     } else {
  //  //       pThis.autocompleteListListModalLocation = data
  //  //     }
  //  //   }
  //  // })
  //  this.input.is_autocomplete_ListModalLocation_load = false
  //}
  //autocompleteChooseListModalLocation(data: any): void {
  //  this.inputModalAddress.address_sub_district_code = data.code
  //  this.inputModalAddress.address_sub_district_name = data.name
  //  this.inputModalAddress.address_district_code = data.district_code
  //  this.inputModalAddress.address_district_name = data.district_name
  //  this.inputModalAddress.address_province_code = data.province_code
  //  this.inputModalAddress.address_province_name = data.province_name
  //  this.inputModalAddress.address_country_code = data.country_code
  //  this.inputModalAddress.address_country_name = data.country_name
  //  this.is_autocomplete_ListModalLocation_show = false
  //}
  //onClickSave04ModalAddressSave(): void {
  //  Object.keys(this.inputModalAddress).forEach((item: any) => {
  //    this.inputModalAddressEdit[item] = this.inputModalAddress[item]
  //  })
  //  this.toggleModal("isModalPeopleEditOpen")
  //}
  //// Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  //loadData(data: any): void {
  //  this.input = data

  //  this.listPublicRole01Case41Check = data.PublicRole01Case41Check_list || []
  //  this.changePaginateTotal(this.listPublicRole01Case41Check.length, 'paginatePublicRole01Case41Check')

  //}

  listData(data: any): void {
    this.listPublicRole01Case41Check = data.list || []
    this.help.PageSet(data, this.paginatePublicRole01Case41Check)
    //this.listPublicRole01Case41Check = data.list || []
    //this.changePaginateTotal(this.listPublicRole01Case41Check.length, 'paginatePublicRole01Case41Check')

  }

  saveData(): any {
    // let params = this.input
    // params.PublicRole01Case41Check_list = this.listPublicRole01Case41Check || []

    const params = {
      //instruction_send_start_date: this.input.instruction_send_start_date,
      //instruction_send_end_date: this.input.instruction_send_end_date,
      //public_receive_start_date: this.input.public_receive_start_date,
      //public_receive_end_date: this.input.public_receive_end_date,
      //public_receive_status_code: [this.input.public_receive_status_code, 6],
      //request_number: this.input.request_number,
      //page_index: +this.paginatePublicRole01Case41Check.currentPage,
      //item_per_page: +this.paginatePublicRole01Case41Check.itemsPerPage,
      //order_by: 'created_date',
      //is_order_reverse: false,
      //search_by: [{
      //  key: 'instruction_send_date',
      //  value: displayDateServer(this.input.instruction_send_start_date),
      //  operation: 3
      //}, {
      //  key: 'instruction_send_date',
      //  value: displayDateServer(this.input.instruction_send_end_date),
      //  operation: 4
      //}, {
      //  key: 'public_receive_date',
      //  value: displayDateServer(this.input.public_receive_start_date),
      //  operation: 3
      //}, {
      //  key: 'public_receive_date',
      //  value: displayDateServer(this.input.public_receive_end_date),
      //  operation: 4
      //}, {
      //  key: 'public_receive_status_code',
      //  value: this.input.public_receive_status_code,
      //  operation: 0
      //}, {
      //  key: 'request_number',
      //  values: this.input.request_number ? this.input.request_number.split(/[ ,]+/) : [],
      //  operation: 5
      //}]
    }

    return params
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value
  }

  //! <<< Table >>>
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all
      })
    } else {
      item.is_check = !item.is_check
    }
  }
  onChangeInputInTable(item: any, name: any, value: any): void {
    item[name] = value
  }
  toggleInputInTable(item: any, name: any): void {
    item[name] = !item[name]
  }

  ////! <<<< Pagination >>>
  //managePaginateCallApi(name: string): void {
  //  if (name === 'paginatePublicRole01Case41Check') {
  //    this.onClickPublicRole01Case41CheckList()
  //    this.input.is_check_all = false
  //  }
  //}
  //onChangePage(page: any, name: string): void {
  //  this[name].currentPage = page === '' ? 1 : page
  //  if ((+page || page === '') && page <= this.getMaxPage(name)) {
  //    this.managePaginateCallApi(name)
  //  }
  //}
  //onChangePerPage(value: number, name: string): void {
  //  this[name].itemsPerPage = value
  //  this.managePaginateCallApi(name)
  //}
  //changePaginateTotal(total: any, name: string): void {
  //  this[name].totalItems = total
  //}
  //getMaxPage(name: string): number {
  //  return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  //}

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
