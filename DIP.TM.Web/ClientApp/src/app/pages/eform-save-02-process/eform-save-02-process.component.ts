import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { GlobalService } from "../../global.service";
import { DeactivationGuarded } from "../../can-deactivate-guard.service";
import { ForkJoinService } from "../../services/fork-join-eform.service";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { UploadService } from "../../services/upload.service";
import {
  ROUTE_PATH,
  clone,
  validateService,
  CONSTANTS,
  displayFormatBytes,
  download,
  displayDateEform,
  displayAddress,
  viewPDF,
  getParamsOverwrite,
} from "../../helpers";

@Component({
  selector: "app-eform-save-02-process",
  templateUrl: "./eform-save-02-process.component.html",
  styleUrls: ["./eform-save-02-process.component.scss"],
})
export class eFormSave02ProcessComponent
  implements OnInit, DeactivationGuarded {
  //TODO >>> Declarations <<<
  // Init
  public editID: number;
  public menuList: any[];
  public input: any;
  public validate: any;
  public master: any;
  public response: any;
  public modal: any;
  public popup: any;
  // Wizard
  public currentID: number;
  public currentStep: number;
  public currentSubID: number;
  public currentSubStep: number;
  public currentMiniID: number;
  public currentMiniStep: number;
  public progressPercent: number;
  // Other
  public timeout: any;
  public isDeactivation: boolean;
  public nextStateUrl: any;
  public isCloseBeforeunload: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private eformSaveProcessService: eFormSaveProcessService,
    private uploadService: UploadService
  ) {}

  ngOnInit() {
    // Init
    this.editID = +this.route.snapshot.paramMap.get("id");
    this.menuList = [
      {
        id: 1,
        number: 1,
        isShow: true,
        name: "บันทึกเพื่อรับไฟล์",
        canEdit: false,
      },
      {
        id: 2,
        number: 2,
        isShow: true,
        name: "คำคัดค้านคำขอเลขที่",
        canEdit: false,
      },
      {
        id: 3,
        number: 3,
        isShow: true,
        name: "ผู้คัดค้าน / ตัวแทน",
        required: true,
        canEdit: false,
      },
      {
        id: 4,
        number: 4,
        isShow: true,
        name: "สถานที่ติดต่อภายในประเทศไทย",
        canEdit: false,
      },
      {
        id: 5,
        number: 5,
        isShow: true,
        name: "ระบุเหตุแห่งการคัดค้าน",
        canEdit: false,
      },
      {
        id: 6,
        number: 6,
        isShow: true,
        name: "เอกสารหลักฐานประกอบคำคัดค้าน",
        canEdit: false,
      },
      {
        id: 7,
        number: 7,
        isShow: true,
        name: "ค่าธรรมเนียม",
        canEdit: false,
      },
      {
        id: 8,
        number: 8,
        isShow: true,
        name: "เสร็จสิ้น",
        canEdit: false,
      },
    ];
    this.input = {
      indexEdit: undefined,
      point: "",
      isAllowEditRequestNumber: true,
      listOwnerMark: [],
      ownerMarkItem: {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      save020_representative_condition_type_code: "AND_OR",
      listAgentMark: [],
      agentMarkItem: {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      },
      contact_type_index: 0,
      listAgentMarkRepresentative1: [],
      listAgentMarkRepresentative2: [],
      address: {
        receiver_type_code: "PEOPLE",
      },
      file_11: {
        id: null,
        file: {},
        url: null,
      },
      listAdsMark: [],
      adsMarkItem: {},
      listDocumentRequestChecked: [
        {
          number: 1,
          isShow: true,
          checked: false,
        },
        {
          number: 2,
          isShow: true,
          checked: false,
        },
        {
          number: 3,
          isShow: true,
          checked: false,
        },
        {
          number: 4,
          isShow: true,
          checked: false,
        },
        {
          number: 5,
          isShow: true,
          checked: false,
        },
        {
          number: 6,
          isShow: true,
          checked: false,
        },
      ],
      authorize: {
        owner_nationality_code: "TH",
        inheritor_nationality_code: "TH",
      },
      indexAuthorize: 0,
      fee: 0,
      isCheckAllOwnerSignature: false,
      isCheckAllAgentSignature: false,
    };
    this.validate = {};
    this.master = {
      addressEformCardTypeCodeList: [],
      addressCountryCodeList: [],
      addressRepresentativeConditionTypeCodeList: [],
      addressTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
      addressCareerCodeList: [],
      addressNationalityCodeList: [],
      representativeTypeCodeList: [],
      requestTypeList: [],
    };
    this.response = {
      load: {},
    };
    this.modal = {
      isModalOwnerFormOpen: false,
      isModalAgentFormOpen: false,
      isModalDomesticContactAddressOpen: true,
      isModalAdsFormOpen: false,
    };
    this.popup = {
      isPopupDeactivation: false,
      isConfirmSave: false,
      isSearch: false,
      isWarning: false,
    };
    // Wizard
    this.currentID = 1;
    this.currentStep = 1;
    this.currentSubID = 1;
    this.currentSubStep = 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
    this.progressPercent = 0;
    // Other
    this.isDeactivation = false;
    this.nextStateUrl = ROUTE_PATH.EFORM_HOME.LINK;
    this.isCloseBeforeunload = false;

    //TODO >>> Call service <<<
    this.callInit();
  }

  //! <<< Call Init API >>>
  callInit(): void {
    this.global.setLoading(true);
    this.forkJoinService.initEForm02Page().subscribe((data: any) => {
      if (data) {
        this.master = data;
        data.requestTypeList.forEach((item: any) => {
          if (item.code === "20" && !this.editID) {
            this.input.fee = item.value_2;
          }
        });
        console.log("this.master", this.master);
      }
      if (this.editID) {
        this.eformSaveProcessService
          .eFormSave020Load(this.editID, {})
          .subscribe((data: any) => {
            if (data) {
              this.response.load = {
                eform_number: data.eform_number,
                id: data.id,
                wizard: data.wizard,
              };
              this.input.isAllowEditRequestNumber = data.request_number
                ? false
                : true;
              // step 1
              this.input.email = data.email;
              this.input.telephone = data.telephone;
              // step 2
              this.input.request_number = data.request_number;
              this.input.end_date = data.public_round
                ? data.public_round.public_end_date
                : "";
              // step 3
              this.input.listOwnerMark = data.people_list;
              this.input.listAgentMark = data.representative_list;
              this.input.save020_representative_condition_type_code = data.save020_representative_condition_type_code
                ? data.save020_representative_condition_type_code
                : "AND_OR";
              // step 4
              this.input.save020_contact_type_code =
                data.save020_contact_type_code;
              this.input.address =
                data.save020_contact_type_code === "OTHERS"
                  ? data.contact_address_list[0]
                  : this.input.address;
              // step 5
              this.input.listAdsMark = data.public_round
                ? data.public_round.public_round_item
                : [];
              if (
                data.public_round &&
                data.public_round.public_round_item.length > 0
              ) {
                this.input.request_date = data.public_round.public_start_date;
                this.input.request_type =
                  data.public_round.public_round_item[0].request_item_sub_type_1_code;
                this.input.request_index =
                  data.public_round.public_round_item[0].line_index;
              }
              this.input.file_11.id = data.evidence_file
                ? data.evidence_file.id
                : null;
              this.input.file_11.file = data.evidence_file
                ? {
                    name: data.evidence_file.file_name,
                    size: data.evidence_file.file_size,
                  }
                : {};
              this.input.file_11.url = data.evidence_file
                ? `/File/Content/${data.evidence_file.id}`
                : null;
              // step 6
              this.input.listDocumentRequestChecked[0].checked = data.is_6_1;
              this.input.listDocumentRequestChecked[1].checked = data.is_6_2;
              this.input.listDocumentRequestChecked[2].checked = data.is_6_3;
              this.input.listDocumentRequestChecked[3].checked = data.is_6_4;
              this.input.listDocumentRequestChecked[4].checked = data.is_6_5;
              this.input.listDocumentRequestChecked[5].checked = data.is_6_6;
              // step 7
              this.input.receipt_name = data.payer_name;
              this.input.fee = data.total_price;
              // step 8
              this.input.isCheckAllOwnerSignature =
                data.sign_inform_person_list === "0" ? true : false;
              this.input.isCheckAllAgentSignature =
                data.sign_inform_representative_list === "0" ? true : false;
              this.manageWizard(data.wizard);
              this.setSignInform(data.sign_inform_representative_list);
              // Close loading
              this.global.setLoading(false);
            } else {
              // Close loading
              this.global.setLoading(false);
            }
          });
      } else {
        // Close loading
        this.global.setLoading(false);
      }
    });
  }

  //! <<< Call API >>>
  callSendEmail020(params: any): void {
    this.eformSaveProcessService
      .eFormSave020Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.response.load = {
            eform_number: data.eform_number,
            id: data.id,
          };
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callSearchRequestNumber(): void {
    this.eformSaveProcessService
      .eFormSave020Search(this.input.request_number, {})
      .subscribe((data: any) => {
        if (data && data.is_search_success) {
          this.input.end_date = data.public_round.public_end_date;
          this.input.listAdsMark = data.public_round.public_round_item;
          this.input.request_date = data.public_round.public_start_date;
          this.input.request_type =
            data.public_round.public_round_item[0].request_item_sub_type_1_code;
          this.input.request_index =
            data.public_round.public_round_item[0].line_index;
        } else {
          console.warn(`request_number is invalid.`);
          this.validate.request_number = data.alert_msg;
          this.togglePopup("isSearch");
        }
        // Close loading
        this.global.setLoading(false);
      });
  }
  callEFormSave020Save(params: any): void {
    this.eformSaveProcessService
      .eFormSave020Save(params)
      .subscribe((data: any) => {
        if (data) {
          this.isDeactivation = true;
          // Open toast success
          let toast = CONSTANTS.TOAST.SUCCESS;
          toast.message = "บันทึกข้อมูลสำเร็จ";
          this.global.setToast(toast);
          // Navigate
          this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
        } else {
          // Close loading
          this.global.setLoading(false);
        }
      });
  }
  callUpload(params: any, calback: Function): void {
    const formData = new FormData();
    formData.append("file", params.file);

    this.uploadService.guestUpload(formData).subscribe((data: any) => {
      if (data) {
        calback(data);
      }
      // Close loading
      this.global.setLoading(false);
    });
  }

  //! <<< Prepare Call API >>>
  sendEmail(): void {
    this.clearAllValidate();
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      wizard: this.getWizard(),
      email: this.input.email,
      telephone: this.input.telephone,
    };
    // Call api
    this.callSendEmail020(params);
  }
  searchRequestNumber(): void {
    let result = validateService("validateEFormSave02ProcessStep2", this.input);
    this.validate = result.validate;

    if (result.isValid) {
      // Open loading
      this.global.setLoading(true);
      // Call api
      this.callSearchRequestNumber();
    }
  }
  prepareCallUpload(file: any, calback: Function): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = {
      file: file,
    };
    // Call api
    this.callUpload(params, calback);
  }
  onClickSave(): void {
    if (this.popup.isPopupDeactivation) {
      this.togglePopup("isPopupDeactivation");
    }
    if (this.validateWizard(true)) {
      this.clearAllValidate();
      if (this.editID) {
        this.togglePopup("isConfirmSave");
      } else {
        this.save(true);
      }
    }
  }
  save(isOverwrite: boolean): void {
    // Open loading
    this.global.setLoading(true);
    // Set param
    let params = this.getParamsSave();
    if (!isOverwrite) {
      params = getParamsOverwrite(params);
    }
    // Call api
    this.callEFormSave020Save(params);
  }
  getParamsSave(): any {
    return {
      id: this.response.load.id ? this.response.load.id : null,
      eform_number: this.response.load.eform_number
        ? this.response.load.eform_number
        : null,
      wizard: this.getWizard(),
      // step 1
      email: this.input.email,
      telephone: this.input.telephone,
      // step 2
      request_number: this.input.request_number,
      // step 3
      people_list: this.input.listOwnerMark,
      representative_list: this.input.listAgentMark,
      save020_representative_condition_type_code: this.input
        .save020_representative_condition_type_code,
      // step 4
      save020_contact_type_code: this.input.save020_contact_type_code,
      contact_address_list:
        this.input.save020_contact_type_code === "OTHERS"
          ? [this.input.address]
          : [],
      // step 5
      evidence_file_id: this.input.file_11.id,
      // step 6
      is_6_1: this.input.listDocumentRequestChecked[0].checked,
      is_6_2: this.input.listDocumentRequestChecked[1].checked,
      is_6_3: this.input.listDocumentRequestChecked[2].checked,
      is_6_4: this.input.listDocumentRequestChecked[3].checked,
      is_6_5: this.input.listDocumentRequestChecked[4].checked,
      is_6_6: this.input.listDocumentRequestChecked[5].checked,
      // step 7
      payer_name: this.input.receipt_name,
      total_price: this.input.fee,
      // step 8
      sign_inform_person_list: this.input.isCheckAllOwnerSignature ? "0" : "",
      sign_inform_representative_list:
        this.input.isCheckAllAgentSignature &&
        this.input.save020_representative_condition_type_code === "AND"
          ? "0"
          : this.getSignInform(),
    };
  }
  onClickViewPdfA02(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM02", this.getParamsSave());
    }
  }
  onClickViewPdfA18(): void {
    if (this.validateWizard()) {
      viewPDF("ViewPDF/TM18", this.getParamsSave());
    }
  }

  //! <<< Validate >>>
  clearValidate(name: string, item?: any): void {
    if (item && item.validate) {
      item.validate[name] = null;
    } else {
      this.validate[name] = null;
    }
  }
  clearAllValidate(): void {
    this.validate = {};
  }

  //! <<< Event >>>
  getWizard(): String {
    if (this.editID) {
      let wizardList = this.response.load.wizard.split("|");
      let sum = 0;
      let sumNow =
        this.currentID +
        this.currentStep +
        this.currentSubID +
        this.currentSubStep;

      wizardList.forEach((item: any) => {
        sum = +sum + +item;
      });

      if (sumNow > sum) {
        return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
      } else {
        return this.response.load.wizard;
      }
    } else {
      return `${this.currentID}|${this.currentStep}|${this.currentSubID}|${this.currentSubStep}`;
    }
  }
  getSignInform(): String {
    let result = "";
    this.input.listAgentMark.forEach((item: any, index: any) => {
      if (item.isCheck) {
        result = result !== "" ? `${result}|${index + 1}` : `${index + 1}`;
      }
    });
    return result;
  }
  setSignInform(signinform: any): void {
    if (signinform !== "0") {
      let signinformList = signinform ? signinform.split("|") : [];
      signinformList.forEach((signin: any) => {
        this.input.listAgentMark.forEach((item: any, index: any) => {
          if (signin - 1 === index) {
            item.isCheck = true;
          }
        });
      });
    }
  }
  onCheckSignature(obj: string, name: any): void {
    if (name === "isCheckAllOwnerSignature") {
      this[obj].isCheckAllAgentSignature = false;
      if (this.input.save020_representative_condition_type_code != "AND") {
        this.input.listAgentMark.forEach((item: any) => {
          item.isCheck = false;
        });
      }
    }
    if (name === "isCheckAllAgentSignature") {
      this[obj].isCheckAllOwnerSignature = false;
    }
    this[obj][name] = !this[obj][name];
  }
  onCheckSignatureList(item: any, name: any): void {
    item[name] = !item[name];
    this.input.isCheckAllOwnerSignature = false;
  }
  onClickSlideAuthorize(action: any): void {
    if (action === "next") {
      if (this.input.indexAuthorize < this.input.listAgentMark.length - 1) {
        this.input.indexAuthorize++;
        this.cloneAuthorize();
      }
    } else if (action === "back") {
      if (this.input.indexAuthorize !== 0) {
        this.input.indexAuthorize = this.input.indexAuthorize - 1;
        this.cloneAuthorize();
      }
    }
  }
  cloneAuthorize(): void {
    let i = this.input.indexAuthorize;
    this.input.authorize.inheritor_name = this.input.listAgentMark[i].name;
    this.input.authorize.inheritor_nationality_code = this.input.listAgentMark[
      i
    ].nationality_code;
    this.input.authorize.inheritor_house_number = this.input.listAgentMark[
      i
    ].house_number;
    this.input.authorize.inheritor_alley = this.input.listAgentMark[i].alley;
    this.input.authorize.inheritor_street = this.input.listAgentMark[i].street;
    this.input.authorize.inheritor_address_sub_district_name = this.input.listAgentMark[
      i
    ].address_sub_district_name;
    this.input.authorize.inheritor_postal_code = this.input.listAgentMark[
      i
    ].postal_code;
  }

  //! <<< ----------------- >>>
  //! <<< --- Copy Zone --- >>>
  //! <<< ----------------- >>>

  //! <<< Binding (one way) >>>
  binding(obj: any, name: any, value: any): void {
    this[obj][name] = value;
  }
  toggleBoolean(obj: string, name: any, index?: any): void {
    if (index || index === 0) {
      this[obj][name][index] = !this[obj][name][index];
    } else {
      this[obj][name] = !this[obj][name];
    }
  }

  //! <<< Table >>>
  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
  }
  toggleBooleanInTable(item: any, name: any, condition?: any): void {
    item[name] = !item[name];
    // if (condition === "cloneStep3") {
    //   this.input.authorize = {
    //     owner_name: this.input.listOwnerMark[0].name,
    //     owner_nationality_code: this.input.listOwnerMark[0].nationality_code,
    //     owner_house_number: this.input.listOwnerMark[0].house_number,
    //     owner_alley: this.input.listOwnerMark[0].alley,
    //     owner_street: this.input.listOwnerMark[0].street,
    //     owner_address_sub_district_name: this.input.listOwnerMark[0]
    //       .address_sub_district_name,
    //     owner_postal_code: this.input.listOwnerMark[0].postal_code,

    //     inheritor_name: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].name
    //       : "",
    //     inheritor_nationality_code: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].nationality_code
    //       : "TH",
    //     inheritor_house_number: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].house_number
    //       : "",
    //     inheritor_alley: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].alley
    //       : "",
    //     inheritor_street: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].street
    //       : "",
    //     inheritor_address_sub_district_name: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].address_sub_district_name
    //       : "",
    //     inheritor_postal_code: this.input.listAgentMark[0]
    //       ? this.input.listAgentMark[0].postal_code
    //       : "",
    //   };
    // }
  }
  onSelectColumnInTable(name: string, item: any): void {
    if (!item) {
      this.input.is_check_all = !this.input.is_check_all;
      this[name].forEach((item: any) => {
        item.is_check = this.input.is_check_all;
      });
    } else {
      item.is_check = !item.is_check;
    }
  }
  onClickSaveItemModalInTable(
    obj: string,
    nameList: any,
    nameItem: any,
    nameModal: any
  ): void {
    if (this.validateSaveItemModalInTable(nameItem)) {
      if (this.input.indexEdit >= 0) {
        // Is Edit
        this[obj][nameList][this.input.indexEdit] = this[obj][nameItem];
        this.toggleModal(nameModal);
      } else {
        // Is Add
        this[obj][nameList].push(this[obj][nameItem]);
        this.toggleModal(nameModal);
        this.updatePaginateForTable(nameList);
      }
    }
  }
  onClickRemoveInTable(obj: string, name: any, index: number): void {
    this[obj][name].splice(index, 1);
    this.updatePaginateForTable(name);
  }
  updatePaginateForTable(name: any): void {}
  validateSaveItemModalInTable(nameItem: any): boolean {
    return true;
  }

  //! <<< Upload (File) >>>
  onClickUploadFile(event: any, obj: string, name: any, maxSize: any): void {
    if (event) {
      let file = event.target.files[0];
      console.log("file", file);
      // Check type file
      if (
        file.type.includes("doc") ||
        file.type.includes("docx") ||
        file.type === ""
      ) {
        this.clearValidate(name);
        // Has max size
        if (maxSize) {
          // Check size file
          if (file.size <= maxSize) {
            this.clearValidate(name);
            this.setFileData(file, obj, name);
          } else {
            console.warn(`${name} is invalid.`);
            this.validate[name] = "ขนาดไฟล์เกินกำหนด";
          }
        } else {
          this.setFileData(file, obj, name);
        }
      } else {
        console.warn(`${name} is invalid.`);
        this.validate[name] = "รูปแบบไฟล์ไม่ถูกต้อง";
      }
    }
  }
  setFileData(file: any, obj: string, name: any): void {
    this.prepareCallUpload(file, (res: any) => {
      this[obj][name].file = file;
      this[obj][name].id = res.id;
      this[obj][name].url = res.file_url;
    });
  }
  onClickRemoveFile(obj: string, name: any): void {
    this[obj][name] = {
      id: null,
      file: {},
      url: null,
    };
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    // if (name === "paginateDocument") {
    //   this.onClickDocumentItemList();
    //   this.input.is_check_all = false;
    // }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      // this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    // this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }

  //! <<< Modal >>>
  toggleModal(name: string, index: number = undefined, point?: any): void {
    if (!this.modal[name]) {
      // Is open
      this.modal[name] = true;
      if (point) {
        this.input.point = point;
      }
      if (index >= 0) {
        // Is Edit
        this.input.indexEdit = index;
        if (name === "isModalOwnerFormOpen") {
          this.input.ownerMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAgentFormOpen") {
          this.input.agentMarkItem = clone(this.input[point][index]);
        } else if (name === "isModalAdsFormOpen") {
          this.input.adsMarkItem = clone(this.input.listAdsMark[index]);
        }
      }
    } else {
      // Is close
      this.modal[name] = false;
      this.input.indexEdit = undefined;
      this.input.ownerMarkItem = {
        is_contact_person: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.agentMarkItem = {
        is_contact_person: false,
        isCheck: false,
        receiver_type_code: "PEOPLE",
        nationality_code: "TH",
        address_country_code: "TH",
      };
      this.input.adsMarkItem = {};
    }
  }

  //! <<< Modal Eform >>>
  onClickSaveModalEform(item: any, nameList: any, nameModal: any): void {
    if (this.input.indexEdit >= 0) {
      // Is Edit
      this.input[nameList][this.input.indexEdit] = item;
      this.toggleModal(nameModal);
    } else {
      // Is Add
      this.input[nameList].push(item);
      this.toggleModal(nameModal);
      this.updatePaginateForTable(nameList);
    }

    if (nameList === "listOwnerMark" || nameList === "listAgentMark") {
      this.togglePopup("isWarning");
      this.clearAllValidate();
    }
  }

  //! <<< Wizard >>>
  onClickMenu(menu: any): void {
    if (menu.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentSubStep = menu.isUseMenuBeforeSubList ? 0 : 1;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickSubMenu(menu: any, sub: any): void {
    if (sub.canEdit) {
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let isCanEdit = false;
      let isValid = true;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
          isCanEdit = this.menuList[indexCurrentMenu].canEdit;
        }
      });

      // Find index current sub menu
      if (this.menuList[indexCurrentMenu].subList) {
        this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
          if (item.number === this.currentSubStep && item.isShow) {
            indexCurrentSub = i;
            isCanEdit = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
              .canEdit;
          }
        });
      }

      // Validate when can edit
      if (isCanEdit) {
        isValid = this.validateWizard();
      }

      if (isValid) {
        this.clearAllValidate();
        this.currentStep = menu.number;
        this.currentID = menu.id;
        this.currentSubID = sub.id;
        this.currentSubStep = sub.number;
        this.currentMiniID = 1;
        this.currentMiniStep = 1;
      }
    }
  }
  onClickNext(): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      if (this.menuList[this.currentID - 1].hasSub) {
        let sizeSubMenu = this.menuList[this.currentID - 1].sizeSubList;

        if (this.currentSubStep < sizeSubMenu) {
          this.nextSubMenu();
        } else {
          if (this.currentSubID > 0) {
            this.menuList[this.currentID - 1].subList[
              this.currentSubID - 1
            ].canEdit = true;
          }
          this.nextMenu();
        }
      } else {
        this.nextMenu();
      }
    }
  }
  nextMenu(): void {
    let indexCurrentMenu = 0;
    let indexNextMenu = 0;

    // Find index current
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Cal Progress Percent
    if (!this.menuList[indexCurrentMenu].canEdit) {
      this.calcProgressPercent(this.currentStep);
    }
    // Set can edit
    this.menuList[indexCurrentMenu].canEdit = true;
    // Next wizard
    this.currentStep++;

    // Find index next
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexNextMenu = i;
      }
    });

    // Update wizard point
    this.currentID = this.menuList[indexNextMenu].id;
    this.currentSubID = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentSubStep = this.menuList[indexNextMenu].isUseMenuBeforeSubList
      ? 0
      : 1;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  nextSubMenu(): void {
    let indexCurrentMenu = 0;
    let indexCurrentSub = 0;
    let indexNextSub = 0;

    // Find index current menu
    this.menuList.forEach((item: any, i: any) => {
      if (item.number === this.currentStep && item.isShow) {
        indexCurrentMenu = i;
      }
    });

    // Find index current sub menu
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexCurrentSub = i;
      }
    });

    // Set can edit
    if (this.currentSubID > 0) {
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].canEdit = true;
    } else {
      if (this.menuList[indexCurrentMenu].isUseMenuBeforeSubList) {
        this.menuList[indexCurrentMenu].canEdit = true;
      }
    }
    // Next wizard sub
    this.currentSubStep++;

    // Find index sub next
    this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
      if (item.number === this.currentSubStep && item.isShow) {
        indexNextSub = i;
      }
    });

    // Update wizard sub point
    this.currentSubID = this.menuList[indexCurrentMenu].subList[
      indexNextSub
    ].id;
    this.currentMiniID = 1;
    this.currentMiniStep = 1;
  }
  changeMiniStep(action: any): void {
    if (this.validateWizard()) {
      this.clearAllValidate();
      let indexCurrentMenu = 0;
      let indexCurrentSub = 0;
      let indexCurrentMini = 0;
      let indexNextMini = 0;
      let sizeMiniList = 0;

      // Find index current menu
      this.menuList.forEach((item: any, i: any) => {
        if (item.number === this.currentStep && item.isShow) {
          indexCurrentMenu = i;
        }
      });

      // Find index current sub menu
      this.menuList[indexCurrentMenu].subList.forEach((item: any, i: any) => {
        if (item.number === this.currentSubStep && item.isShow) {
          indexCurrentSub = i;
        }
      });

      // Find index current mini menu
      this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList.forEach(
        (item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexCurrentMini = i;
          }
        }
      );

      // Get size mini list
      sizeMiniList = this.menuList[indexCurrentMenu].subList[indexCurrentSub]
        .sizeMiniList;

      // Condition
      if (sizeMiniList === this.currentMiniStep && action === "next") {
        // Set can edit
        this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
          indexCurrentMini
        ].canEdit = true;
        // Next menu
        this.onClickNext();
      } else {
        if (action === "next") {
          // Set can edit
          this.menuList[indexCurrentMenu].subList[indexCurrentSub].miniList[
            indexCurrentMini
          ].canEdit = true;
          // Next wizard mini
          this.currentMiniStep++;
        } else {
          // Back wizard mini
          this.currentMiniStep = this.currentMiniStep - 1;
        }

        // Find index next mini menu
        this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList.forEach((item: any, i: any) => {
          if (item.number === this.currentMiniStep && item.isShow) {
            indexNextMini = i;
          }
        });

        // Update wizard mini point
        this.currentMiniID = this.menuList[indexCurrentMenu].subList[
          indexCurrentSub
        ].miniList[indexNextMini].id;
      }
    }
  }
  validateWizard(isClickSave?: boolean): boolean {
    if (this.currentID === 1) {
      let result = validateService(
        "validateEFormSave01ProcessStep1",
        this.input
      );
      this.validate = result.validate;

      if (
        result.isValid &&
        !isClickSave &&
        !this.editID &&
        !this.response.load.id
      ) {
        this.sendEmail();
      }

      return result.isValid;
    } else if (this.currentID === 2) {
      let result = validateService(
        "validateEFormSave02ProcessStep2",
        this.input
      );
      this.validate = result.validate;

      if (!this.input.listAdsMark || this.input.listAdsMark.length === 0) {
        result.isValid = false;
        console.warn(`request_number is invalid.`);
        this.validate.request_number = "กรุณาคลิก ค้นหา";
      }

      return result.isValid;
    } else if (this.currentID === 3) {
      if (this.input.listOwnerMark.length < 1) {
        console.warn(`step3 is invalid.`);
        this.validate.step3 = "กรุณาเพิ่มเจ้าของเครื่องหมาย";
        return false;
      } else {
        // Condition
        this.conditionWizard("clone_mark_list");
        return true;
      }
    } else if (this.currentID === 4) {
      let result = validateService(
        "validateEFormSave02ProcessStep4",
        this.input
      );
      this.validate = result.validate;

      // Condition
      if (result.isValid) {
        if (this.input.save020_contact_type_code === "OTHERS") {
          let result = validateService(
            "validateEFormSave01ProcessStep4Other",
            this.input.address
          );
          this.validate = result.validate;
          return result.isValid;
          // if (
          //   this.input.address.receiver_type_code == "PEOPLE" ||
          //   this.input.address.receiver_type_code == "PEOPLE_FOREIGNER"
          // ) {
          //   let result = validateService(
          //     "validateEFormSave01ProcessStep4OtherPeople",
          //     this.input.address
          //   );
          //   this.validate = result.validate;
          //   return result.isValid;
          // } else {
          //   let result = validateService(
          //     "validateEFormSave01ProcessStep4OtherNotPeople",
          //     this.input.address
          //   );
          //   this.validate = result.validate;
          //   return result.isValid;
          // }
        } else {
          // Set is_contact_person
          if (this.input.save020_contact_type_code === "OWNER") {
            this.input.listOwnerMark.forEach((item: any, index: any) => {
              if (+this.input.contact_type_index === index) {
                item.is_contact_person = true;
              } else {
                item.is_contact_person = false;
              }
            });
          } else {
            this.input.listAgentMark.forEach((item: any, index: any) => {
              if (this.input.save020_contact_type_code === "REPRESENTATIVE") {
                // ตัวแทน
                if (
                  this.input.listAgentMarkRepresentative1[
                    +this.input.contact_type_index
                  ] === index
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              } else if (
                this.input.save020_contact_type_code === "REPRESENTATIVE_PERIOD"
              ) {
                // ตัวแทนช่วง
                if (
                  this.input.listAgentMarkRepresentative2[
                    +this.input.contact_type_index
                  ] === index
                ) {
                  item.is_contact_person = true;
                } else {
                  item.is_contact_person = false;
                }
              }
            });
          }
        }
      }

      return result.isValid;
    } else if (this.currentID === 7) {
      let result = validateService(
        "validateEFormSave02ProcessStep7",
        this.input
      );
      this.validate = result.validate;
      return result.isValid;
    } else if (this.currentID === 8) {
      let isValid = false;

      if (
        this.input.isCheckAllOwnerSignature ||
        this.input.isCheckAllAgentSignature
      ) {
        isValid = true;
      }

      if (this.input.listAgentMark.length > 0) {
        this.input.listAgentMark.forEach((item: any) => {
          if (item.isCheck) {
            isValid = true;
          }
        });
      }

      if (!isValid) {
        console.warn(`step8 is invalid.`);
        this.validate.step8 = "กรุณาเลือกลงลายมือชื่อ";
      } else {
        // Set can edit
        this.menuList[7].canEdit = true;
      }

      return isValid;
    } else {
      return true;
    }
  }
  conditionWizard(condition: any): void {
    if (condition === "clone_mark_list") {
      let listAgentMarkRepresentative1 = [];
      let listAgentMarkRepresentative2 = [];
      this.input.listAgentMark.forEach((item: any, index: any) => {
        if (item.representative_type_code == "REPRESENTATIVE") {
          listAgentMarkRepresentative1.push(index);
        } else {
          listAgentMarkRepresentative2.push(index);
        }
      });
      this.input.listAgentMarkRepresentative1 = clone(
        listAgentMarkRepresentative1
      );
      this.input.listAgentMarkRepresentative2 = clone(
        listAgentMarkRepresentative2
      );

      if (!this.input.receipt_name) {
        this.input.receipt_name = this.input.listOwnerMark[0] ? this.input.listOwnerMark[0].name : "";
      }
    }
  }
  manageWizard(wizard: any): void {
    let wizardList = wizard ? wizard.split("|") : [1, 1, 1, 1];
    this.currentID = +wizardList[0];
    this.currentStep = +wizardList[1];
    this.currentSubID = +wizardList[2];
    this.currentSubStep = +wizardList[3];
    this.calcProgressPercent(this.currentStep);

    this.menuList.forEach((item: any) => {
      if (this.currentID > item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            sub.canEdit = true;
          });
        }
      }
      if (this.currentID === item.id) {
        item.canEdit = true;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (this.currentSubID >= sub.id) {
              sub.canEdit = true;
            }
          });
        }
      }
    });
  }
  reRunMenuNumber(): void {
    let number = 1;
    let numberSub = 1;
    let numberMini = 1;
    this.menuList.forEach((item: any) => {
      if (item.isShow) {
        item.number = number;
        number++;
        numberSub = 1;
        if (item.subList) {
          item.subList.forEach((sub: any) => {
            if (sub.isShow) {
              sub.number = numberSub;
              numberSub++;
              numberMini = 1;
              if (sub.miniList) {
                sub.miniList.forEach((mini: any) => {
                  if (mini.isShow) {
                    mini.number = numberMini;
                    numberMini++;
                  }
                });
              }
            }
          });
        }
      }
    });
  }
  calcProgressPercent(currentStep: number): void {
    let lastItem = this.menuList[this.menuList.length - 1];
    let progressPercent = Math.round(
      ((currentStep + 1) / lastItem.number) * 100
    );
    this.progressPercent = progressPercent > 100 ? 100 : progressPercent;
  }

  //! <<< Popup >>>
  togglePopup(name: string): void {
    if (!this.popup[name]) {
      // Is open
      this.popup[name] = true;
    } else {
      // Is close
      this.popup[name] = false;
    }
  }

  //! <<< Deactivate >>>
  canDeactivate(nextStateUrl: any): boolean {
    if (nextStateUrl) {
      this.nextStateUrl = nextStateUrl;
    }
    this.global.setLoading(false);
    this.togglePopup("isPopupDeactivation");
    return this.isDeactivation;
  }
  @HostListener("window:beforeunload", ["$event"])
  beforeunload(e: any): void {
    this.global.setLoading(true);
    this.isCloseBeforeunload = true;
    e.preventDefault();
    e.returnValue = "";
  }
  @HostListener("window:focus")
  onCloseBeforeunload() {
    if (this.isCloseBeforeunload) {
      this.isCloseBeforeunload = false;
      this.timeout = setTimeout(() => {
        this.global.setLoading(false);
        this.togglePopup("isPopupDeactivation");
      }, CONSTANTS.DELAY_OPEN_POPUP_EFORM);
    }
  }
  onExitPopupDeactivation(): void {
    this.isDeactivation = true;
    this.router.navigate([this.nextStateUrl]);
  }

  //! <<< Other >>>
  cancel(): void {
    this.router.navigate([ROUTE_PATH.EFORM_HOME.LINK]);
  }
  displayAddress(value: any): any {
    return displayAddress(value);
  }
  displayFormatBytes(value: any): any {
    return displayFormatBytes(value);
  }
  download(url: any, name: any): any {
    return download(url, name);
  }
  onKey(e: any, name: any): void {
    if (e.keyCode === 13) {
      if (name === "request_number") {
        this.searchRequestNumber();
      }
    } else {
      if (name === "request_number") {
        this.input.end_date = "";
        this.input.listAdsMark = [];
        this.input.request_date = "";
        this.input.request_type = "";
        this.input.request_index = "";
      }
    }
  }
  displayDateEform(value: any): any {
    return displayDateEform(value);
  }
}
