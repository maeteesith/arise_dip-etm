import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-document-role04-dashboard",
  templateUrl: "./document-role04-dashboard.component.html",
  styleUrls: ["./document-role04-dashboard.component.scss"]
})
export class DocumentRole04DashboardComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  // List CheckingInstructionList
  public listCheckingInstructionList: any[]
  public paginateCheckingInstructionList: any
  public perPageCheckingInstructionList: number[]
  // Response CheckingInstructionList
  public responseCheckingInstructionList: any
  public listCheckingInstructionListEdit: any

  // List AddressList
  public listAddressList: any[]
  public paginateAddressList: any
  public perPageAddressList: number[]
  // Response AddressList
  public responseAddressList: any
  public listAddressListEdit: any

  // List CheckingInstructionDocumentList
  public listCheckingInstructionDocumentList: any[]
  public paginateCheckingInstructionDocumentList: any
  public perPageCheckingInstructionDocumentList: number[]
  // Response CheckingInstructionDocumentList
  public responseCheckingInstructionDocumentList: any
  public listCheckingInstructionDocumentListEdit: any


  //label_save_combobox || label_save_radio
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      request_number: '',
      name: '',
      request_item_sub_type_1_code_text: '',
      request_item_type_name: '',
      is_trademark_join: false,
      request_date: '',
      representative_name: '',
      house_number: '',
      is_trademark_sound: false,
    }
    this.listCheckingInstructionList = []
    this.paginateCheckingInstructionList = CONSTANTS.PAGINATION.INIT
    this.perPageCheckingInstructionList = CONSTANTS.PAGINATION.PER_PAGE

    this.listAddressList = []
    this.paginateAddressList = CONSTANTS.PAGINATION.INIT
    this.perPageAddressList = CONSTANTS.PAGINATION.PER_PAGE

    this.listCheckingInstructionDocumentList = []
    this.paginateCheckingInstructionDocumentList = CONSTANTS.PAGINATION.INIT
    this.perPageCheckingInstructionDocumentList = CONSTANTS.PAGINATION.PER_PAGE


    //Master List
    this.master = {
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
    })

    //this.forkJoinService.initDocumentRole04Appeal().subscribe((data: any) => {
    //  if (data) {
    //    this.help.Clone(data, this.master)

    //  }
    if (this.editID) {
      this.DocumentProcessService.DocumentRole04AppealLoad(this.editID).subscribe((data: any) => {
        if (data) {
          // Manage structure
          this.loadData(data)
        }
        // Close loading
        this.global.setLoading(false)
        this.automateTest.test(this)
      })
    } else {
      this.global.setLoading(false)
      this.automateTest.test(this)
    }

    //})
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService
  ) { }


  onClickDocumentRole04AppealCheckingInstructionListAdd(): void {
    this.listCheckingInstructionList.push({
      index: this.listCheckingInstructionList.length + 1,
      instruction_rule_name: null,
      book_number: null,
      book_start_date_text: null,
      save010_checking_instruction_document_remark: null,
      considering_instruction_rule_status_name: null,

    })
    this.changePaginateTotal(this.listCheckingInstructionList.length, 'paginateCheckingInstructionList')
  }

  onClickDocumentRole04AppealAddressListAdd(): void {
    this.listAddressList.push({
      index: this.listAddressList.length + 1,
      house_number: null,

    })
    this.changePaginateTotal(this.listAddressList.length, 'paginateAddressList')
  }

  onClickDocumentRole04AppealAddressListEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickDocumentRole04AppealAddressListDelete(item: any): void {
    // if(this.validateDocumentRole04AppealAddressListDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listAddressList.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listAddressList.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listAddressList.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listAddressList.length; i++) {
          if (this.listAddressList[i].is_check) {
            this.listAddressList[i].cancel_reason = rs
            this.listAddressList[i].status_code = "DELETE"
            this.listAddressList[i].is_deleted = true

            if (true && this.listAddressList[i].id) ids.push(this.listAddressList[i].id)
            // else this.listAddressList.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole04AppealAddressListDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listAddressList.length; i++) {
          this.listAddressList[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole04AppealAddressListDelete(params: any, ids: any[]): void {
    //this.DocumentProcessService.DocumentRole04AppealAddressListDelete(params).subscribe((data: any) => {
    //  // if(isValidDocumentRole04AppealAddressListDeleteResponse(res)) {

    //  ids.forEach((id: any) => {
    //    for (let i = 0; i < this.listAddressList.length; i++) {
    //      if (this.listAddressList[i].id == id) {
    //        this.listAddressList.splice(i--, 1);
    //      }
    //    }
    //  });

    //  for (let i = 0; i < this.listAddressList.length; i++) {
    //    this.listAddressList[i] = i + 1
    //  }

    //  this.onClickDocumentRole04AppealAddressList()
    //  // Close loading
    //  this.global.setLoading(false)
    //})
  }

  onClickDocumentRole04AppealCheckingInstructionDocumentListAdd(): void {
    this.listCheckingInstructionDocumentList.push({
      index: this.listCheckingInstructionDocumentList.length + 1,
      instruction_rule_name: null,
      instruction_send_date_text: null,
      book_round_01_start_date_text: null,
      book_round_02_start_date_text: null,
      book_number: null,
      document_role02_receive_status_name: null,

    })
    this.changePaginateTotal(this.listCheckingInstructionDocumentList.length, 'paginateCheckingInstructionDocumentList')
  }

  onClickDocumentRole04AppealCheckingInstructionDocumentListEdit(item: any): void {
    var win = window.open("/" + item.id)
  }


  onClickDocumentRole04AppealCheckingInstructionDocumentListDelete(item: any): void {
    // if(this.validateDocumentRole04AppealCheckingInstructionDocumentListDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listCheckingInstructionDocumentList.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listCheckingInstructionDocumentList.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listCheckingInstructionDocumentList.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
          if (this.listCheckingInstructionDocumentList[i].is_check) {
            this.listCheckingInstructionDocumentList[i].cancel_reason = rs
            this.listCheckingInstructionDocumentList[i].status_code = "DELETE"
            this.listCheckingInstructionDocumentList[i].is_deleted = true

            if (true && this.listCheckingInstructionDocumentList[i].id) ids.push(this.listCheckingInstructionDocumentList[i].id)
            // else this.listCheckingInstructionDocumentList.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callDocumentRole04AppealCheckingInstructionDocumentListDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
          this.listCheckingInstructionDocumentList[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callDocumentRole04AppealCheckingInstructionDocumentListDelete(params: any, ids: any[]): void {
    //this.DocumentProcessService.DocumentRole04AppealCheckingInstructionDocumentListDelete(params).subscribe((data: any) => {
    //  // if(isValidDocumentRole04AppealCheckingInstructionDocumentListDeleteResponse(res)) {

    //  ids.forEach((id: any) => {
    //    for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
    //      if (this.listCheckingInstructionDocumentList[i].id == id) {
    //        this.listCheckingInstructionDocumentList.splice(i--, 1);
    //      }
    //    }
    //  });

    //  for (let i = 0; i < this.listCheckingInstructionDocumentList.length; i++) {
    //    this.listCheckingInstructionDocumentList[i] = i + 1
    //  }

    //  this.onClickDocumentRole04AppealCheckingInstructionDocumentList()
    //  // Close loading
    //  this.global.setLoading(false)
    //})
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.DocumentProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    this.input = data

    this.listCheckingInstructionList = data.checkinginstructionlist_list || []
    this.listAddressList = data.addresslist_list || []
    this.listCheckingInstructionDocumentList = data.checkinginstructiondocumentlist_list || []
    this.help.PageSet(data, this.paginateCheckingInstructionList)
    this.help.PageSet(data, this.paginateAddressList)
    this.help.PageSet(data, this.paginateCheckingInstructionDocumentList)

  }

  listData(data: any): void {
    this.listCheckingInstructionList = data.list || []
    this.listAddressList = data.list || []
    this.listCheckingInstructionDocumentList = data.list || []
    this.help.PageSet(data, this.paginateCheckingInstructionList)
    this.help.PageSet(data, this.paginateAddressList)
    this.help.PageSet(data, this.paginateCheckingInstructionDocumentList)

  }

  saveData(): any {
    let params = this.input

    params.checkinginstructionlist_list = this.listCheckingInstructionList || []
    params.addresslist_list = this.listAddressList || []
    params.checkinginstructiondocumentlist_list = this.listCheckingInstructionDocumentList || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }
  displayMoney(value: any): any {
    return displayMoney(value);
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }

}
