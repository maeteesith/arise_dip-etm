import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../../helpers'

@Component({
    selector: "app-dashboard-document-role-01",
    templateUrl: "./dashboard-document-role-01.component.html",
    styleUrls: ["./dashboard-document-role-01.component.scss"]
})
export class DashboardDocumentRole01Component implements OnInit {
    //TODO >>> Declarations <<</'
    public editID: any
    public input: any
    public validate: any
    public master: any
    // Response
    public response: any

    public autocompleteListListModalLocation: any
    public is_autocomplete_ListModalLocation_show: any
    public is_autocomplete_ListModalLocation_load: any
    public inputModalAddress: any
    public inputModalAddressEdit: any
    public modal: any
    public inputAddress: any

    public modalPeopleText: any

    public contactAddress: any
    public modalAddress: any
    public modalAddressEdit: any

    // List DashboardPublicRole01
    public listDashboardPublicRole01: any[]
    public paginateDashboardPublicRole01: any
    public perPageDashboardPublicRole01: number[]
    // Response DashboardPublicRole01
    public responseDashboardPublicRole01: any
    public listDashboardPublicRole01Edit: any


    // List DocumentProcess
    public listDocumentProcess: any[]
    public paginateDocumentProcess: any
    public perPageDocumentProcess: number[]
    // Response DocumentProcess
    public responseDocumentProcess: any

    //label_save_combobox || label_save_radio
    public departmentCodeList: any[]
    public documentRole05PrintListStatusCodeList: any[]
    //label_save_combobox || label_save_radio

    //Modal Initial
    //Modal Initial


    ngOnInit() {
        this.input = {}

        this.callInit()
    }

    callInit(): void {
        this.global.setLoading(true)

        this.DocumentProcessService.DashboardDocumentRole01Load().subscribe((data: any) => {
            this.help.Clone(data.list[0], this.input)
            this.global.setLoading(false)
        })
    }

    constructor(
        private auth: Auth,
        private route: ActivatedRoute,
        public sanitizer: DomSanitizer,
        private autoComplete: AutoComplete,
        private help: Help,
        private automateTest: AutomateTest,
        private location: Location,
        private global: GlobalService,
        private forkJoinService: ForkJoinService,
        private DocumentProcessService: DocumentProcessService
    ) { }

    onClickDocumentItem(query: any): void {
        window.open("document-item/list?query=" + query)
    }

    onClickDocumentProcessClassification(query: any): void {
        window.open("document-process-classification?query=" + query)
    }
}
