import { Component, OnInit, ViewEncapsulation } from "@angular/core";
import { Mm02_01_4_modalComponent } from "../mm02_01-4-modal/mm02_01-4-modal.component";
import { MatDialog } from "@angular/material";
@Component({
  selector: "app-madrid-role03-DT07-03",
  templateUrl: "./madrid-role03-DT07-03.component.html",
  styleUrls: [
    "./madrid-role03-DT07-03.component.scss",
    "./../../../assets/theme/styles/madrid/madrid.scss",
  ],
})
export class MadridRole03DT07_03Component implements OnInit {
  constructor(private dialog: MatDialog) {}

  ItemArray: any = [];

  Item: any;

  checkboxIV
  ngOnInit() {
    this.checkboxIV = false;
    console.log(this.checkboxIV);
  }

  addItem() {
    if (this.Item == null) return;

    this.ItemArray.push(this.Item);
    this.Item = null;
  }

  deleteItem(index: any) {
    this.ItemArray.splice(index, 1);
  }

  onClickModal01_4() {
    //console.log(resp, "selected");
    const dialogRef = this.dialog.open(Mm02_01_4_modalComponent, {
      width: "calc(100% - 80px)",
      maxWidth: "1500px",
      // height: "calc(80% - 20px)",
      data: {},
    });
    dialogRef.afterClosed().subscribe((res) => {
      // this.router.navigate(['eordering/delivery-note/create-request']);
      // if (!res) return;
      // this.router.navigate(['eordering/delivery-note/create-request']);
      // this.navToAddProduct(<ApiProductGroupDisplay>res);
    });
  }
}
