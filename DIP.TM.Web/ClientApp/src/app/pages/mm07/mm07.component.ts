import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mm07',
  templateUrl: './mm07.component.html',
  styleUrls: ['./mm07.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class Mm07Component implements OnInit {
  public input: any
  constructor() { }
  public doc: any = [];
  ngOnInit() {
    this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      public_type_code: '',
      public_source_code: '',
      public_receiver_by: '',
      public_status_code: '',
      request_number: '',
    }
    this.doc = [{ value: "1", text: "MMC" }, { value: "2", text: "MMU" }]
  }

}
