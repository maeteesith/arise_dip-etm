import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { Help } from 'src/app/helpers/help';
import { AutomateTest } from 'src/app/test/automate_test';
import { ActivatedRoute } from '@angular/router';
import { GlobalService } from 'src/app/global.service';

@Component({
  selector: 'app-mm06',
  templateUrl: './mm06.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./mm06.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class Mm06Component implements OnInit {
  public input: any
  currentStep = 1;

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,
) { }

  public doc: any = [];

  menuList = [
    {
      number: 1,
      isShow: true,
      title: "REGISTER"
    },
    {
      number: 2,
      isShow: true,
      title: "CALCULATION"
    }
  ]

  ngOnInit() {
    this.input = {
      id: null,
      //instruction_send_start_date: getMoment(),
      //instruction_send_end_date: getMoment(),
      public_type_code: '',
      public_source_code: '',
      public_receiver_by: '',
      public_status_code: '',
      request_number: '',
    }
    this.doc = [{ value: "1", text: "MMC" }, { value: "2", text: "MMU" }]
  }

}
