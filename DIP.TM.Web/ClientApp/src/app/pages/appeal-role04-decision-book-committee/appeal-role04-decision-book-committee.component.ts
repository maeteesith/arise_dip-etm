import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
import { Auth } from "../../auth";
import '../../pages/appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss'
import {
  CONSTANTS,
  getMoment,
  clone,
} from '../../helpers'

@Component({
  selector: 'app-appeal-role04-decision-book-committee',
  templateUrl: './appeal-role04-decision-book-committee.component.html',
  styleUrls: [
    '../appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.component.scss',
    './appeal-role04-decision-book-committee.component.scss']
})
export class AppealRole04DecisionBookCommitteeComponent implements OnInit {

  public input: any
  public uthornPoint: any
  public bookType:any
  public validate: any

  constructor(
    private router: Router,
    private global: GlobalService,
    private auth: Auth,
  ) { }

  ngOnInit() {
    this.input = {
      created_by_name: this.auth.getAuth().name,
      appeal_date: getMoment()
    }
    this.uthornPoint = {
      typeCodeList: [
        { code: "1", name: "บ่งเฉพาะ" },
        { code: "2", name: "เหมือนคล้าย" },
        { code: "3", name: "คัดค้าน" },
        { code: "4", name: "เพิกถอน" },
      ],
    };
    this.bookType = {
      typeCodeList: [
        { code: "1", name: "ออกหนังสือแจ้งการอุทธรณ์ (ตค.9 ข้อ5)" },
        { code: "2", name: "ออกหนังสือแจ้งเพื่อทราบ (ตค.9 ข้อ10)" },
        { code: "3", name: "ออกหนังสือเครื่องหมายที่มีชื่อเสียงแพร่หลาย (ตค.9 ข้อ14)" },
      ],
    };
    this.validate = {}
  }

  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

}
