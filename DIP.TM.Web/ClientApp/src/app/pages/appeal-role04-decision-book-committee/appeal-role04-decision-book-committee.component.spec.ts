import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppealRole04DecisionBookCommitteeComponent } from './appeal-role04-decision-book-committee.component';

describe('AppealRole04DecisionBookCommitteeComponent', () => {
  let component: AppealRole04DecisionBookCommitteeComponent;
  let fixture: ComponentFixture<AppealRole04DecisionBookCommitteeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppealRole04DecisionBookCommitteeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppealRole04DecisionBookCommitteeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
