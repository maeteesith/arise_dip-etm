import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-appeal-role03-case-summary',
  templateUrl: './appeal-role03-case-summary.component.html',
  styleUrls: ['./appeal-role03-case-summary.component.scss']
})
export class AppealRole03CaseSummaryComponent implements OnInit {

  constructor() { }

  public modal: any
  public Kor: boolean

  ngOnInit() {
    this.modal = {
      isModalAddReason: false,
      isModalSection: false,
      isModalK08Detail: false
    }
    this.checkKOR()
  }


  toggleModal(name: string): void {
    if (!this.modal[name]) {
      this.modal[name] = true
    } else {
      this.modal[name] = false
      switch(name){
      }
    }
  }

  checkKOR(){
    this.Kor = true
  }

}
