import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'
import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-save-020-process",
  templateUrl: "./save-020-process.component.html",
  styleUrls: ["./save-020-process.component.scss"]
})
export class Save020ProcessComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  // List People01
  public listPeople01: any[]
  public paginatePeople01: any
  public perPagePeople01: number[]
  // Response People01
  public responsePeople01: any
  public listPeople01Edit: any

  // List Product01
  public listProduct01: any[]
  public paginateProduct01: any
  public perPageProduct01: number[]
  // Response Product01
  public responseProduct01: any
  public listProduct01Edit: any

  // List People
  public listPeople: any[]
  public paginatePeople: any
  public perPagePeople: number[]
  // Response People
  public responsePeople: any
  public listPeopleEdit: any

  // List Representative
  public listRepresentative: any[]
  public paginateRepresentative: any
  public perPageRepresentative: number[]
  // Response Representative
  public responseRepresentative: any
  public listRepresentativeEdit: any

  // List ContactAddress
  public listContactAddress: any[]
  public paginateContactAddress: any
  public perPageContactAddress: number[]
  // Response ContactAddress
  public responseContactAddress: any
  public listContactAddressEdit: any

  // List OppositionRequest
  public listOppositionRequest: any[]
  public paginateOppositionRequest: any
  public perPageOppositionRequest: number[]
  // Response OppositionRequest
  public responseOppositionRequest: any
  public listOppositionRequestEdit: any


  //label_save_combobox || label_save_radio
  public addressTypeCodeList: any[]
  public addressCardTypeCodeList: any[]
  public addressReceiverTypeCodeList: any[]
  public addressSexCodeList: any[]
  //label_save_combobox || label_save_radio

  public autocompleteListListNotSent: any
  public is_autocomplete_ListNotSent_show: any
  public is_autocomplete_ListNotSent_load: any
  public autocompleteListListLocation: any
  public is_autocomplete_ListLocation_show: any
  public is_autocomplete_ListLocation_load: any
  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  //Modal Initial
  public inputModalAddress: any
  public inputModalAddressEdit: any
  //Modal Initial
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}

    this.input = {
      id: null,
      request_number: '',
      total_price: '',
      make_date: getMoment(),
      public_date_text: '',
      request_date_text: '',
      irn_number: '',
      request_index: '',
      book_number: '',
      page_index: '',
    }
    this.listPeople01 = []
    this.paginatePeople01 = CONSTANTS.PAGINATION.INIT
    this.perPagePeople01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listProduct01 = []
    this.paginateProduct01 = CONSTANTS.PAGINATION.INIT
    this.perPageProduct01 = CONSTANTS.PAGINATION.PER_PAGE

    this.listPeople = []
    this.paginatePeople = CONSTANTS.PAGINATION.INIT
    this.perPagePeople = CONSTANTS.PAGINATION.PER_PAGE

    this.listRepresentative = []
    this.paginateRepresentative = CONSTANTS.PAGINATION.INIT
    this.perPageRepresentative = CONSTANTS.PAGINATION.PER_PAGE

    this.listContactAddress = []
    this.paginateContactAddress = CONSTANTS.PAGINATION.INIT
    this.perPageContactAddress = CONSTANTS.PAGINATION.PER_PAGE

    this.listOppositionRequest = []
    this.paginateOppositionRequest = CONSTANTS.PAGINATION.INIT
    this.perPageOppositionRequest = CONSTANTS.PAGINATION.PER_PAGE

    //Master List
    this.master = {
      addressTypeCodeList: [],
      addressCardTypeCodeList: [],
      addressReceiverTypeCodeList: [],
      addressSexCodeList: [],
    }
    //Master List

    this.autocompleteListListNotSent = []
    this.is_autocomplete_ListNotSent_show = false
    this.is_autocomplete_ListNotSent_load = false
    this.autocompleteListListLocation = []
    this.is_autocomplete_ListLocation_show = false
    this.is_autocomplete_ListLocation_load = false
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false

    this.modal = { isModalPeopleEditOpen: false, }

    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}


    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.forkJoinService.initSave020Process().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
      }
      if (this.editID) {
        let pThis = this
        this.SaveProcessService.Save020Load(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            //Object.keys(data).forEach((item: any) => {
            //    this.input[item] = data[item]
            //})

            pThis.autocompleteChooseListNotSent(data)

            //this.listPeople = data.people_list || []
            //this.listRepresentative = data.representative_list || []
            //this.listContactAddress = data.contact_address_list || []
            //this.listProduct = data.product_list || []
            //this.listJoiner = data.joiner_list || []
            //this.listSoundMark = data.sound_mark_list || []
            //data.people_list.map((item: any) => { item.is_check = false; return item })
            //data.representative_list.map((item: any) => { item.is_check = false; return item })
            //data.contact_address_list.map((item: any) => { item.is_check = false; return item })
            //data.product_list.map((item: any) => { item.is_check = false; return item })
            //data.joiner_list.map((item: any) => { item.is_check = false; return item })
            //data.sound_mark_list.map((item: any) => { item.is_check = false; return item })
            //this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
            //this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
            //this.changePaginateTotal((this.listContactAddress || []).length, 'paginateContactAddress')
            //this.changePaginateTotal((this.listProduct || []).length, 'paginateProduct')
            //this.changePaginateTotal((this.listJoiner || []).length, 'paginateJoiner')
            //this.changePaginateTotal((this.listSoundMark || []).length, 'paginateSoundMark')


            // Set value
            // this.requestList = data.item_list
            // this.response.request01Load = data
            // this.isHasRequestList = true
          }
          // Close loading
          pThis.global.setLoading(false)
        })
      } else {
        this.global.setLoading(false)
      }
    })
  }

  constructor(
    private help: Help,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private SaveProcessService: SaveProcessService
  ) { }


  autocompleteKeyDown(event: any) {
    if (event.key == "Enter") {
      this.autocompleteChangeListNotSent(this.input, 'request_number', 5, 1)
    }
  }

  // Autocomplete
  autocompleteChangeListNotSent(object: any, name: any, item_per_page: any = 5, length: any = 9): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListNotSent_show = true

      let params = {
        filter: { request_number: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListNotSent(params)
    }
  }
  autocompleteBlurListNotSent(object: any, name: any, item_per_page: any = 5): void {
    setTimeout(function () {
      this.is_autocomplete_ListNotSent_show = false
    }, 200)
  }
  callAutocompleteChangeListNotSent(params: any): void {
    if (this.input.is_autocomplete_ListNotSent_load) return
    this.input.is_autocomplete_ListNotSent_load = true
    let pthis = this
    this.SaveProcessService.Save020ListNotSent(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pthis.autocompleteChooseListNotSent(data[0])
          }, 200)
        } else if (data.length == 0) {
          pthis.onClickReset()
        } else {
          this.autocompleteListListNotSent = data
        }
      }
    })
    this.input.is_autocomplete_ListNotSent_load = false
  }
  autocompleteChooseListNotSent(data: any): void {
    this.input = data

    this.listPeople = data.people_list || []
    this.listRepresentative = data.representative_list || []
    this.help.Clone(data.contact_address || {}, this.contactAddress)
    this.contactAddress.address_type_code = this.contactAddress.address_type_code || "OWNER"

    this.listPeople01 = data.people_01_list || []
    this.listProduct01 = data.product_01_list || []


    this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
    this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
    //this.changePaginateTotal((this.listContactAddress || []).length, 'paginateContactAddress')

    this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')
    this.changePaginateTotal((this.listProduct01 || []).length, 'paginateProduct01')

    this.is_autocomplete_ListNotSent_show = false
  }


  // Autocomplete
  autocompleteChangeListLocation(object: any, name: any, item_per_page: any = 5, length: any = 3): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListLocation(params)
    }
  }
  autocompleteBlurListLocation(object: any, name: any, item_per_page: any = 5): void {
    setTimeout(function () {
      this.is_autocomplete_ListLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListLocation(params: any): void {
    if (this.input.is_autocomplete_ListLocation_load) return
    this.input.is_autocomplete_ListLocation_load = true
    let pthis = this
    this.SaveProcessService.Save020ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pthis.autocompleteChooseListLocation(data[0])
          }, 200)
        } else {
          pthis.autocompleteListListLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListLocation_load = false
  }
  autocompleteChooseListLocation(data: any): void {
    this.inputAddress.address_sub_district_code = data.code
    this.inputAddress.address_sub_district_name = data.name
    this.inputAddress.address_district_code = data.district_code
    this.inputAddress.address_district_name = data.district_name
    this.inputAddress.address_province_code = data.province_code
    this.inputAddress.address_province_name = data.province_name
    this.inputAddress.address_country_code = data.country_code
    this.inputAddress.address_country_name = data.country_name

    this.is_autocomplete_ListLocation_show = false
  }

  // Autocomplete
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        filter: { name: object[name] },
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    setTimeout(function () {
      this.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pthis = this
    this.SaveProcessService.Save020ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        if (data.length == 1) {
          setTimeout(function () {
            pthis.autocompleteChooseListModalLocation(data[0])
          }, 200)
        } else {
          this.autocompleteListListModalLocation = data
        }
      }
    })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  // Autocomplete


  onClickSave020Delete(): void {
    //if(this.validateSave020Delete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    const params = this.input

    // Call api
    this.callSave020Delete(params)
    //}
  }
  validateSave020Delete(): boolean {
    let result = validateService('validateSave020Delete', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave020Delete(params: any): void {
    let pThis = this
    this.SaveProcessService.Save020Delete(params).subscribe((data: any) => {
      // if(isValidSave020DeleteResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
        //this.response = data
        //this.listPeople01 = data.people_01_list || []
        //this.listProduct01 = data.product_01_list || []
        //this.listPeople = data.people_list || []
        //this.listRepresentative = data.representative_list || []
        //this.listContactAddress = data.contact_address_list || []
        //this.listOppositionRequest = data.opposition_request_list || []
        //this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')
        //this.changePaginateTotal((this.listProduct01 || []).length, 'paginateProduct01')
        //this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
        //this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
        //this.changePaginateTotal((this.listContactAddress || []).length, 'paginateContactAddress')
        //this.changePaginateTotal((this.listOppositionRequest || []).length, 'paginateOppositionRequest')

      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  onClickSave020DocumentView(): void {
    window.open('/File/RequestDocumentCollect/20/' + this.input.request_number + "_" + this.input.request_id)
  }
  validateSave020DocumentView(): boolean {
    let result = validateService('validateSave020DocumentView', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave020DocumentView(params: any): void {
    this.SaveProcessService.Save020DocumentView(params).subscribe((data: any) => {
      // if(isValidSave020DocumentViewResponse(res)) {
      if (data) {
        // Set value
        this.input = data
        this.response = data
        this.listPeople01 = data.people_01_list || []
        this.listProduct01 = data.product_01_list || []
        this.listPeople = data.people_list || []
        this.listRepresentative = data.representative_list || []
        this.listContactAddress = data.contact_address_list || []
        this.listOppositionRequest = data.opposition_request_list || []
        this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')
        this.changePaginateTotal((this.listProduct01 || []).length, 'paginateProduct01')
        this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
        this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
        this.changePaginateTotal((this.listContactAddress || []).length, 'paginateContactAddress')
        this.changePaginateTotal((this.listOppositionRequest || []).length, 'paginateOppositionRequest')

      }
      // }
      // Close loading
      this.global.setLoading(false)
    })
  }

  onClickSave020Save(): void {
    //if(this.validateSave020Save()) {
    // Open loading
    this.global.setLoading(true)
    //// Set param
    //const params = this.input
    //params.contact_address = this.inputAddress
    //params.people_list = this.listPeople
    //params.representative_list = this.listRepresentative
    //params.contact_address_list = this.listContactAddress

    // Call api
    this.callSave020Save(this.saveData())
    //}
  }
  validateSave020Save(): boolean {
    let result = validateService('validateSave020Save', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave020Save(params: any): void {
    let pThis = this
    this.SaveProcessService.Save020Save(params).subscribe((data: any) => {
      // if(isValidSave020SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
        this.response = data
        this.listPeople01 = data.people_01_list || []
        this.listProduct01 = data.product_01_list || []
        this.listPeople = data.people_list || []
        this.listRepresentative = data.representative_list || []
        this.listContactAddress = data.contact_address_list || []
        this.listOppositionRequest = data.opposition_request_list || []
        this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')
        this.changePaginateTotal((this.listProduct01 || []).length, 'paginateProduct01')
        this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
        this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
        this.changePaginateTotal((this.listContactAddress || []).length, 'paginateContactAddress')
        this.changePaginateTotal((this.listOppositionRequest || []).length, 'paginateOppositionRequest')

      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }

  onClickSave020Send(): void {
    //if(this.validateSave020Send()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    //const params = this.input
    //params.contact_address = this.inputAddress
    //params.people_list = this.listPeople
    //params.representative_list = this.listRepresentative

    // Call api
    this.callSave020Send(this.saveData())
    //}
  }
  validateSave020Send(): boolean {
    let result = validateService('validateSave020Send', this.input)
    this.validate = result.validate
    return result.isValid
  }
  //! <<< Call API >>>
  callSave020Send(params: any): void {
    let pThis = this
    this.SaveProcessService.Save020Send(params).subscribe((data: any) => {
      // if(isValidSave020SendResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
        this.response = data
        this.listPeople01 = data.people_01_list || []
        this.listProduct01 = data.product_01_list || []
        this.listPeople = data.people_list || []
        this.listRepresentative = data.representative_list || []
        this.listContactAddress = data.contact_address_list || []
        this.listOppositionRequest = data.opposition_request_list || []
        this.changePaginateTotal((this.listPeople01 || []).length, 'paginatePeople01')
        this.changePaginateTotal((this.listProduct01 || []).length, 'paginateProduct01')
        this.changePaginateTotal((this.listPeople || []).length, 'paginatePeople')
        this.changePaginateTotal((this.listRepresentative || []).length, 'paginateRepresentative')
        this.changePaginateTotal((this.listContactAddress || []).length, 'paginateContactAddress')
        this.changePaginateTotal((this.listOppositionRequest || []).length, 'paginateOppositionRequest')

      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }
  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickSave020RequestBack(): void {
    //if(this.validateSave020Save()) {
    // Open loading
    this.global.setLoading(true)
    // Call api
    this.callSave020RequestBack(this.saveData())
    //}
  }
  callSave020RequestBack(params: any): void {
    let pThis = this
    this.SaveProcessService.Save020RequestBack(params).subscribe((data: any) => {
      // if(isValidSave020SaveResponse(res)) {
      if (data) {
        // Set value
        pThis.autocompleteChooseListNotSent(data)
      }
      // }
      // Close loading
      pThis.global.setLoading(false)
    })
  }


  onClickSave020Product01Add(): void {
    this.listProduct01.push({
      index: this.listProduct01.length + 1,
      request_item_sub_type_1_code: null,
      description: null,

    })
    this.changePaginateTotal(this.listProduct01.length, 'paginateProduct01')
  }
  onChangeCheckboxProduct01(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
  onChangeSelectProduct01(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }

  onClickCheckBoxTable5(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_5_check_all = !this.input.is_table_5_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_5_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxPeople(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
  onChangeSelectPeople(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }

  onClickCheckBoxTable6(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_6_check_all = !this.input.is_table_6_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_6_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxRepresentative(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
  onChangeSelectRepresentative(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }

  onClickSave020OppositionRequestAdd(): void {
    this.listOppositionRequest.push({
      index: this.listOppositionRequest.length + 1,
      request_number: null,
      name: null,
      request_date: null,

    })
    this.changePaginateTotal(this.listOppositionRequest.length, 'paginateOppositionRequest')
  }

  onClickSave020OppositionRequestDelete(item: any): void {
    // if(this.validateSave020OppositionRequestDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listOppositionRequest.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listOppositionRequest.filter(r => r.is_check).forEach((item: any) => {
          item.is_check = false
        });
        item.is_check = true
      }

      let delete_save_item_count = 0
      if (false) {
        delete_save_item_count = this.listOppositionRequest.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < this.listOppositionRequest.length; i++) {
          if (this.listOppositionRequest[i].is_check) {
            this.listOppositionRequest[i].cancel_reason = rs
            this.listOppositionRequest[i].status_code = "DELETE"

            if (false && this.listOppositionRequest[i].id) ids.push(this.listOppositionRequest[i].id)
            else this.listOppositionRequest.splice(i--, 1);
          }
        }

        if (false) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callSave020OppositionRequestDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listOppositionRequest.length; i++) {
          this.listOppositionRequest[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callSave020OppositionRequestDelete(params: any, ids: any[]): void {
    this.SaveProcessService.Save020OppositionRequestDelete(params).subscribe((data: any) => {
      // if(isValidSave020OppositionRequestDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listOppositionRequest.length; i++) {
          if (this.listOppositionRequest[i].id == id) {
            this.listOppositionRequest.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listOppositionRequest.length; i++) {
        this.listOppositionRequest[i] = i + 1
      }


      // Close loading
      this.global.setLoading(false)
    })
  }
  onClickCheckBoxTable8(name: string, index: number, isAll: boolean = false): void {
    if (isAll) {
      this.input.is_table_8_check_all = !this.input.is_table_8_check_all
      this[name].forEach((item: any) => { item.is_check = this.input.is_table_8_check_all })
    } else { this[name].forEach((item: any, i: any) => { if (index === i) { item.is_check = !item.is_check } }) }
  }
  onChangeCheckboxOppositionRequest(object: any, object_name: string, value: any): void { object[object_name] = !object[object_name] }
  onChangeSelectOppositionRequest(object: any, object_name: string, value: any): void { if (object_name.indexOf("price") >= 0) { object[object_name] = parseFloat(value) } else { object[object_name] = value } }

  saveData(): any {
    const params = this.input
    //params.contact_address = this.inputAddress
    params.people_list = this.listPeople
    params.representative_list = this.listRepresentative
    params.contact_address = this.contactAddress

    return params
  }

  onClickSave020PeopleAdd(): void {
    this.listPeople.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listPeople[this.listPeople.length - 1])
    this.changePaginateTotal(this.listPeople.length, 'paginatePeople')
  }
  onClickSave020RepresentativeAdd(): void {
    this.listRepresentative.push({
      address_country_code: 'TH',
      nationality_code: 'TH',
    })
    this.onClickAddressEdit(this.listRepresentative[this.listRepresentative.length - 1], 'ตัวแทน')
    this.changePaginateTotal(this.listRepresentative.length, 'paginateRepresentative')
  }

  onClickAddressCopy(item_list: any[], copy_item: any): void {
    var index = 1
    item_list.forEach((item: any) => {
      item.index = index++
      if (item === copy_item) {
        var item_new = { ...item }
        item_new.id = null
        item_new.index = index++
        item_list.splice(index + 1, 0, item_new)
      }
    })
  }
  onClickAddressEdit(item: any, name: string = "เจ้าของ"): void {
    this.modalPeopleText = name
    this.modalAddressEdit = item
    this.help.Clone(item, this.modalAddress)

    this.modalAddress.is_representative = name.indexOf("ตัวแทน") >= 0

    this.modal["isModalPeopleEditOpen"] = true

  }
  onClickAddressSave(): void {
    this.help.Clone(this.modalAddress, this.modalAddressEdit)
    this.help.GetAddressInformation(this.modalAddressEdit)

    this.modal["isModalPeopleEditOpen"] = false
  }
  onClickAddressDelete(item_list: any[], item: any): void {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (item_list.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        item_list.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = item_list.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {

        let ids = []

        for (let i = 0; i < item_list.length; i++) {
          if (item_list[i].is_check) {
            //item_list[i].cancel_reason = rs
            item_list[i].is_deleted = true
            //console.log(item_list[i])
            //item_list[i].status_code = "DELETE"

            if (true && item_list[i].id) ids.push(item_list[i].id)
            else item_list.splice(i--, 1);
          }
        }
      }
    }
    //console.log(item_list)
    this.global.setLoading(false)
  }

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }
  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }


  oneWayDataBinding(name: any, value: any, object: any): void {
    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }

  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }
}
