import { Help } from './../../helpers/help';
import { AutomateTest } from './../../test/automate_test';
import { GlobalService } from './../../global.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  CONSTANTS,
  getMoment,
  clone,
  displayDateServer
} from '../../helpers'
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-madrid-role02-mr23-step',
  templateUrl: './madrid-role02-mr23-step.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./madrid-role02-mr23-step.component.scss',
    './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole02Mr23StepComponent implements OnInit {

  currentStep = 1;

  constructor(
    private help: Help,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private global: GlobalService,

  ) { }

  ngOnInit() {



  }
}
