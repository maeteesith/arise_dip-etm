import { Auth } from "../../auth";
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { ReceiptProcessService } from '../../services/receipt-process-buffer.service'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { AutoComplete } from '../../helpers/autocomplete'

import {
  CONSTANTS,
  getMoment,
  validateService,
  displayMoney,
  displayString,
  clone
} from '../../helpers'

@Component({
  selector: "app-receipt-daily-list",
  templateUrl: "./receipt-daily-list.component.html",
  styleUrls: ["./receipt-daily-list.component.scss"]
})
export class ReceiptDailyListComponent implements OnInit {
  //TODO >>> Declarations <<</'
  public editID: any
  public input: any
  public validate: any
  public master: any
  // Response
  public response: any

  public autocompleteListListModalLocation: any
  public is_autocomplete_ListModalLocation_show: any
  public is_autocomplete_ListModalLocation_load: any
  public inputModalAddress: any
  public inputModalAddressEdit: any
  public modal: any
  public inputAddress: any

  public modalPeopleText: any

  public contactAddress: any
  public modalAddress: any
  public modalAddressEdit: any

  // List ReceiptDaily
  public listReceiptDaily: any[]
  public paginateReceiptDaily: any
  public perPageReceiptDaily: number[]
  // Response ReceiptDaily
  public responseReceiptDaily: any
  public listReceiptDailyEdit: any


  // List ReceiptProcess
  public listReceiptProcess: any[]
  public paginateReceiptProcess: any
  public perPageReceiptProcess: number[]
  // Response ReceiptProcess
  public responseReceiptProcess: any

  //label_save_combobox || label_save_radio
  public receiptStatusCodeList: any[]
  //label_save_combobox || label_save_radio

  //Modal Initial
  //Modal Initial


  ngOnInit() {
    this.editID = this.route.snapshot.paramMap.get('id')
    this.validate = {}
    this.modal = {
    }

    this.input = {
      id: null,
      receipt_number: '',
      created_by_name: '',
      request_number: '',
      receive_date: getMoment(),
      reference_number: '',
      receipt_status_code: '',
    }
    this.listReceiptDaily = []
    this.paginateReceiptDaily = CONSTANTS.PAGINATION.INIT
    this.perPageReceiptDaily = CONSTANTS.PAGINATION.PER_PAGE

    this.listReceiptProcess = []

    //Master List
    this.master = {
      receiptStatusCodeList: [],
    }
    //Master List



    //
    this.autocompleteListListModalLocation = []
    this.is_autocomplete_ListModalLocation_show = false
    this.is_autocomplete_ListModalLocation_load = false
    this.modal = { isModalPeopleEditOpen: false, }
    this.inputAddress = {}
    this.inputModalAddress = {}
    //


    this.contactAddress = {
      address_type_code: "OWNER"
    }
    this.modalAddress = this.modalAddress || {}

    this.callInit()
  }

  callInit(): void {
    this.global.setLoading(true)
    this.route.queryParams.subscribe((param_url: any) => {
    })

    this.forkJoinService.initReceiptDailyList().subscribe((data: any) => {
      if (data) {
        this.help.Clone(data, this.master)
        this.master.receiptStatusCodeList.unshift({ "code": "", "name": "ทั้งหมด" })
        this.input.receipt_status_code = ""

      }
      if (this.editID) {
        this.ReceiptProcessService.ReceiptDailyLoad(this.editID).subscribe((data: any) => {
          if (data) {
            // Manage structure
            this.loadData(data)
          }
          // Close loading
          this.global.setLoading(false)
          this.automateTest.test(this)
        })
      } else {
        this.global.setLoading(false)
        this.automateTest.test(this)
      }

    })
  }

  constructor(
    private auth: Auth,
    private route: ActivatedRoute,
    public sanitizer: DomSanitizer,
    private autoComplete: AutoComplete,
    private help: Help,
    private automateTest: AutomateTest,
    private location: Location,
    private global: GlobalService,
    private forkJoinService: ForkJoinService,
    private ReceiptProcessService: ReceiptProcessService
  ) { }

  onClickReceiptDailyList(): void {
    // if(this.validateReceiptDailyList()) {
    // Open loading
    // Call api
    this.callReceiptDailyList(this.saveData())
    // }
  }
  //! <<< Call API >>>
  callReceiptDailyList(params: any): void {
    this.global.setLoading(true)
    this.ReceiptProcessService.ReceiptDailyList(this.help.GetFilterParams(params, this.paginateReceiptDaily)).subscribe((data: any) => {
      // if(isValidReceiptDailyListResponse(res)) {
      if (data) {
        // Set value
        this.listData(data)
        this.automateTest.test(this, { list: data.list })
      }
      this.global.setLoading(false)
      // }
      // Close loading
    })
  }

  onClickReset(): void {
    this.global.setLoading(true)
    this.ngOnInit();
    this.global.setLoading(false)
  }


  onClickReceiptDailyAdd(): void {
    //this.listReceiptDaily.push({
    //  index: this.listReceiptDaily.length + 1,
    //  reference_number: null,
    //  request_number: null,
    //  receipt_number: null,
    //  people_name: null,
    //  requester_name: null,
    //  receive_date: null,
    //  total_price: null,
    //  created_by_name: null,
    //  receipt_status_code: null,

    //})
    //this.changePaginateTotal(this.listReceiptDaily.length, 'paginateReceiptDaily')
  }

  onClickReceiptDailyEdit(item: any): void {
    var win = window.open("/receipt-daily/edit/" + item.reference_number)
  }


  onClickReceiptDailyDelete(item: any): void {
    // if(this.validateReceiptDailyDelete()) {
    // Open loading
    this.global.setLoading(true)
    // Set param
    if (this.listReceiptDaily.filter(r => r.is_check).length > 0 || item) {
      if (item) {
        this.listReceiptDaily.forEach((_item: any) => { _item.is_check = _item == item })
      }

      let delete_save_item_count = 0
      if (true) {
        delete_save_item_count = this.listReceiptDaily.filter(r => r.is_check && r.id).length
      }

      var rs = delete_save_item_count > 0 ? prompt("คุณต้องการลบรายการ?", "กรุณาใส่เหตุผล") : confirm("คุณต้องการลบรายการ?")
      if (rs && rs != "") {
        if (rs === true) rs = ""

        let ids = []

        for (let i = 0; i < this.listReceiptDaily.length; i++) {
          if (this.listReceiptDaily[i].is_check) {
            this.listReceiptDaily[i].cancel_reason = rs
            this.listReceiptDaily[i].status_code = "DELETE"
            this.listReceiptDaily[i].is_deleted = true

            if (true && this.listReceiptDaily[i].id) ids.push(this.listReceiptDaily[i].id)
            // else this.listReceiptDaily.splice(i--, 1);
          }
        }

        if (true) {
          if (ids.length > 0) {
            let params = {
              ids: ids,
              cancel_reason: rs
            }
            // Call api
            this.callReceiptDailyDelete(params, ids)
            return;
          }
        }

        for (let i = 0; i < this.listReceiptDaily.length; i++) {
          this.listReceiptDaily[i].index = i + 1
        }
      }
    }
    this.global.setLoading(false)
  }
  //! <<< Call API >>>
  callReceiptDailyDelete(params: any, ids: any[]): void {
    this.ReceiptProcessService.ReceiptDailyDelete(params).subscribe((data: any) => {
      // if(isValidReceiptDailyDeleteResponse(res)) {

      ids.forEach((id: any) => {
        for (let i = 0; i < this.listReceiptDaily.length; i++) {
          if (this.listReceiptDaily[i].id == id) {
            this.listReceiptDaily.splice(i--, 1);
          }
        }
      });

      for (let i = 0; i < this.listReceiptDaily.length; i++) {
        this.listReceiptDaily[i] = i + 1
      }

      this.onClickReceiptDailyList()
      // Close loading
      this.global.setLoading(false)
    })
  }


  // Modal Location
  autocompleteChangeListModalLocation(object: any, name: any, item_per_page: any = 5, length: any = 2): void {
    if (object[name].length >= length) {
      console.log("d")
      this.is_autocomplete_ListModalLocation_show = true

      let params = {
        name: object[name],
        paging: {
          item_per_page: item_per_page
        },
      }

      this.callAutocompleteChangeListModalLocation(params)
    }
  }
  autocompleteBlurListModalLocation(object: any, name: any, item_per_page: any = 5): void {
    let pThis = this
    setTimeout(function () {
      pThis.is_autocomplete_ListModalLocation_show = false
    }, 200)
  }
  callAutocompleteChangeListModalLocation(params: any): void {
    if (this.input.is_autocomplete_ListModalLocation_load) return
    this.input.is_autocomplete_ListModalLocation_load = true
    let pThis = this
    // this.ReceiptProcessService.Save04ListModalLocation(params).subscribe((data: any) => {
    //   if (data) {
    //     if (data.length == 1) {
    //       setTimeout(function () {
    //         pThis.autocompleteChooseListModalLocation(data[0])
    //       }, 200)
    //     } else {
    //       pThis.autocompleteListListModalLocation = data
    //     }
    //   }
    // })
    this.input.is_autocomplete_ListModalLocation_load = false
  }
  autocompleteChooseListModalLocation(data: any): void {
    this.inputModalAddress.address_sub_district_code = data.code
    this.inputModalAddress.address_sub_district_name = data.name
    this.inputModalAddress.address_district_code = data.district_code
    this.inputModalAddress.address_district_name = data.district_name
    this.inputModalAddress.address_province_code = data.province_code
    this.inputModalAddress.address_province_name = data.province_name
    this.inputModalAddress.address_country_code = data.country_code
    this.inputModalAddress.address_country_name = data.country_name
    this.is_autocomplete_ListModalLocation_show = false
  }
  onClickSave04ModalAddressSave(): void {
    Object.keys(this.inputModalAddress).forEach((item: any) => {
      this.inputModalAddressEdit[item] = this.inputModalAddress[item]
    })
    this.toggleModal("isModalPeopleEditOpen")
  }
  // Modal Location

  //! <<< Validate >>>
  clearValidate(name: string, nameList: string, id: number): void {
    if (nameList && id) {
      this[nameList].forEach((item: any) => {
        if (item.id === id) {
          item.validate[name] = null
        }
      })
    } else {
      this.validate[name] = null
    }
  }

  loadData(data: any): void {
    //this.input = data

    //this.listReceiptDaily = data.receiptdaily_list || []
    //this.changePaginateTotal(this.listReceiptDaily.length, 'paginateReceiptDaily')

  }

  listData(data: any): void {
    this.listReceiptDaily = data.list || []
    this.help.PageSet(data, this.paginateReceiptDaily)
    //this.listReceiptDaily = data.list || []
    //this.changePaginateTotal(this.listReceiptDaily.length, 'paginateReceiptDaily')
  }

  saveData(): any {
    //var params = this.input

    const params = {
      receipt_number: this.input.receipt_number,
      created_by_name: this.input.created_by_name,
      request_number: this.input.request_number,
      receive_date: this.input.receive_date,
      reference_number: this.input.reference_number,
      receipt_status_code: this.input.receipt_status_code,
    }
    //params = this.help.GetFilterParams(params)

    //params.receiptdaily_list = this.listReceiptDaily || []

    return params
  }

  //! <<<< Pagination >>>
  onChangePage(page: any, name: string): void {
    if (+page) {
      this[name].currentPage = page
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value
  }
  changePaginateTotal(total: any, name: string): void {
    let paginate = CONSTANTS.PAGINATION.INIT
    paginate.totalItems = total
    this[name] = paginate
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage)
  }

  oneWayDataBinding(name: any, value: any, object: any): void {
    if (name.indexOf("price") >= 0) {
      value = parseFloat(value)
    }

    if (object) {
      object[name] = value
    } else {
      this.input[name] = value
    }
  }
  oneWayDataCheckboxAllBinding(object_list: any[], value: any): void {
    object_list.forEach((item: any) => { item.is_check = value })
  }
  //! <<< Modal >>>
  toggleModal(name: string): void {
    this.modal[name] = !this.modal[name]
  }



  displayMoney(value: any): any {
    return displayMoney(value)
  }

}
