import { Component, OnInit, Inject, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { GlobalService } from '../../global.service'
import { ForkJoinService } from '../../services/fork-join2.service'
import { DocumentProcessService } from '../../services/document-process-buffer.service'
import { DialogService } from '../../services/dialogService'
import { AutomateTest } from '../../test/automate_test'
import { Help } from '../../helpers/help'
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material'

@Component({
  selector: 'app-madrid-role05-modal',
  templateUrl: './madrid-role05-modal.component.html',
  styleUrls: ['./madrid-role05-modal.component.scss',
  './../../../assets/theme/styles/madrid/madrid.scss']
})
export class MadridRole05ModalComponent implements OnInit {

  /**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<RoleEditDialogComponent>
	 * @param data: any
	 */

  constructor(private help: Help,
    public dialog: MatDialog,
    public dialogRef: MatDialogRef<MadridRole05ModalComponent>,
    private automateTest: AutomateTest,
    private route: ActivatedRoute,
    private location: Location,
    private global: GlobalService,
    private _dialog: DialogService,
    private forkJoinService: ForkJoinService,
    private DocumentProcessService: DocumentProcessService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit() {
    this.dialogRef.disableClose = true;
    this.dialogRef.backdropClick().subscribe(_ => {
      // let cn = confirm('Do you want to close this modal without save ? ')
      // if (cn) {
      //   this.dialogRef.close();
      // }
    })
  }

  closeModal(){
    this.dialogRef.close();
  }

  onClickModal() {

    let button = [
      //id คือค่าที่จะส่งกลับมาถ้าเรา subscribe , butclass คือ icon หน้าข้อความ เช่นคำว่า ยกเลิกหรือตกลง, border ทำไว้เผื่อปุ่มไม่มีสี 
      { id: "CANCEL",name: "ยกเลิก", butclass: "dip-icon-close", border: "solid 1px #C5C7C9", color: "#C5C7C9"}, 
      { id: "YES", name: "ส่งแก้ไข",butclass: "dip-icon-check", color: "#ffffff", backgroundColor: "#0C84C8" }
    ]
    
    // this._dialog.openDecisionConfirm(
    //   "พิจารณาอนุมัติ", //title
    //   "#0C84C8", //สีของ title
    //   "ยืนยันการส่งงานไปยัง WIPO", //description
    //   button //ปุ่มแต่ละปุ่ม
    //   ).subscribe();

    this._dialog.showEditorConfirm(
      "ส่งแก้ไข",
      "dip-icon-edit",
      "<h4 style='color: #0C84C8;font-size: medium;'>รายละเอียด</h4>" + 
      "<div class='border-div'><p>ปัทวเหตุโยโย่ฟรุต สวีทเคลมสะบึม ฉลุยโต๋เต๋ ปฏิสัมพันธ์ว้อดก้าแช่แข็ง ไรเฟิลเวอร์โพลารอยด์ โฮปแอคทีฟโรแมนติคท็อปบูต ฮากกาสะบึมส์มอคค่าแรงใจเบญจมบพิตร ซินโดรมล็อบบี้กิมจิกีวีเซอร์วิส</p>"+
      "<p class='pad-ft'>- สติกเกอร์แคทวอล์คบ็อกซ์</p>"+
      "<p class='pad-ft'>- นางแบบสติกเกอร์ถูกต้องมินต์ว้อย</p>"+
      "<p class='pad-ft'>- สต็อกแชมพู ฮองเฮามุมมองอัลตรา</p>"+
      "<p class='pad-ft'>- เซอร์ไพรส์ พูล ซีนีเพล็กซ์แบล็คเกรย์</p>"+
      "<p class='pad-ft'>- แบรนด์เบอร์เกอร์ฮิมิลค์</p>"+
      "</div>",
      button
    )
  }

}
