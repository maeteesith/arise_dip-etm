import { Mm06Module } from './modules/mm06/mm06.module';
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ROUTE_PATH } from "./helpers";

const routes: Routes = [
  {
    path: `${ROUTE_PATH.DASHBOARD.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard/dashboard.module").then(
        m => m.DashboardModule
      )
  },

  {
    path: `${ROUTE_PATH.EFORM_HOME.ROUTE}`,
    loadChildren: () =>
      import("./modules/eform-home/eform-home.module").then(
        m => m.eFormHomeModule
      )
  },

  {
    path: `${ROUTE_PATH.TEST.ROUTE}`,
    loadChildren: () =>
      import("./modules/test/test.module").then(m => m.TestModule)
  },

  {
    path: `${ROUTE_PATH.ZONE_COPY.ROUTE}`,
    loadChildren: () =>
      import("./modules/zone-copy/zone-copy.module").then(
        m => m.ZoneCopyModule
      )
  },

  {
    path: `${ROUTE_PATH.REGISTRATION_REQUEST.ROUTE}`,
    loadChildren: () =>
      import("./modules/registration-request/registration-request.module").then(
        m => m.RegistrationRequestModule
      )
  },

  {
    path: `${ROUTE_PATH.REQUEST_01_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-01-item/request-01-item.module").then(
        m => m.Request01ItemModule
      )
  },

  {
    path: `${ROUTE_PATH.DAILY_FEE_INCOME_RECORDS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/daily-fee-income-records/daily-fee-income-records.module"
      ).then(m => m.DailyFeeIncomeRecordsModule)
  },

  {
    path: `${ROUTE_PATH.REPRESENTATIVE_MANAGEMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/representative-management/representative-management.module"
      ).then(m => m.RepresentativeManagementModule)
  },

  {
    path: `${ROUTE_PATH.REGISTRATION_OTHER_REQUEST.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/registration-other-request/registration-other-request.module"
      ).then(m => m.RegistrationOtherRequestModule)
  },

  {
    path: `${ROUTE_PATH.REQUEST_OTHER_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-other-item/request-other-item.module").then(
        m => m.RequestOtherItemModule
      )
  },

  {
    path: `${ROUTE_PATH.RECEIPT_DAILY.ROUTE}`,
    loadChildren: () =>
      import("./modules/receipt-daily/receipt-daily.module").then(
        m => m.ReceiptDailyModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_01_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-01-process/save-01-process.module").then(
        m => m.Save01ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_020_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-020-process/save-020-process.module").then(
        m => m.Save020ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_021_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-021-process/save-021-process.module").then(
        m => m.Save021ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_03_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-03-process/save-03-process.module").then(
        m => m.Save03ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_04_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-04-process/save-04-process.module").then(
        m => m.Save04ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_05_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-05-process/save-05-process.module").then(
        m => m.Save05ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_06_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-06-process/save-06-process.module").then(
        m => m.Save06ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_07_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-07-process/save-07-process.module").then(
        m => m.Save07ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_08_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-08-process/save-08-process.module").then(
        m => m.Save08ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.SAVE_14_PROCESS.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-140-process/save-140-process.module").then(
        m => m.Save140ProcessModule
      )
  },

  {
    path: `${ROUTE_PATH.REQUEST_SEARCH.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-search/request-search.module").then(
        m => m.RequestSearchModule
      )
  },

  {
    path: `${ROUTE_PATH.REQUEST_DOCUMENT_COLLECT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/request-document-collect/request-document-collect.module"
      ).then(m => m.RequestDocumentCollectModule)
  },

  {
    path: `${ROUTE_PATH.ALL_TASK.ROUTE}`,
    loadChildren: () =>
      import("./modules/all-task/all-task.module").then(m => m.AllTaskModule)
  },

  {
    path: `${ROUTE_PATH.DOCUMENT_PROCESS_CLASSIFICATION.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/document-process-classification/document-process-classification.module"
      ).then(m => m.DocumentProcessClassificationModule)
  },

  {
    path: `${ROUTE_PATH.DOCUMENT_PROCESS_CLASSIFICATION_DO.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/document-process-classification-do/document-process-classification-do.module"
      ).then(m => m.DocumentProcessClassificationDoModule)
  },

  {
    path: `${ROUTE_PATH.CHECKING_SIMILAR_IMAGE.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/checking-similar-image/checking-similar-image.module"
      ).then(m => m.CheckingSimilarImageModule)
  },

  {
    path: `${ROUTE_PATH.CHECKING_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/checking-item/checking-item.module").then(
        m => m.CheckingItemListModule
      )
  },

  {
    path: `${ROUTE_PATH.CHECKING_SIMILAR.ROUTE}`,
    loadChildren: () =>
      import("./modules/checking-similar/checking-similar.module").then(
        m => m.CheckingSimilarListModule
      )
  },

  {
    path: `${ROUTE_PATH.CHECKING_SIMILAR.ROUTE}`,
    loadChildren: () =>
      import("./modules/checking-similar/checking-similar.module").then(
        m => m.CheckingSimilarListModule
      )
  },

  {
    path: `${ROUTE_PATH.CHECKING_SIMILAR_WORD.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/checking-similar-word/checking-similar-word.module"
      ).then(m => m.CheckingSimilarWordModule)
  },

  {
    path: `${ROUTE_PATH.CHECKING_SIMILAR_RESULT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/checking-similar-result/checking-similar-result.module"
      ).then(m => m.CheckingSimilarResultModule)
  },

  {
    path: `${ROUTE_PATH.CONSIDERING_SIMILAR.ROUTE}`,
    loadChildren: () =>
      import("./modules/considering-similar/considering-similar.module").then(
        m => m.ConsideringSimilarModule
      )
  },

  {
    path: `${ROUTE_PATH.PUBLIC_DASHBOARD_ROLE_01.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-dashboard-role-01/public-dashboard-role-01.module"
      ).then(m => m.PublicDashboardRole01Module)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_01_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-01-process/eform-save-01-process.module"
      ).then(m => m.eFormSave01ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_02_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-02-process/eform-save-02-process.module"
      ).then(m => m.eFormSave02ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM.ROUTE}`,
    loadChildren: () =>
      import("./modules/eform/eform.module").then(m => m.EFormModule)
  },

  {
    path: `${ROUTE_PATH.DOCUMENT_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-item/document-item.module").then(
        m => m.DocumentItemModule
      )
  },

  {
    path: `${ROUTE_PATH.CONSIDERING_SIMILAR_INSTRUCTION.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/considering-similar-instruction/considering-similar-instruction.module"
      ).then(m => m.ConsideringSimilarInstructionModule)
  },

  {
    path: `${ROUTE_PATH.CONSIDERING_SIMILAR_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/considering-similar-document/considering-similar-document.module"
      ).then(m => m.ConsideringSimilarDocumentModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE01_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role01-item/public-role01-item.module").then(
        m => m.PublicRole01ItemModule
      )
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE01_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role01-check/public-role01-check.module").then(
        m => m.PublicRole01CheckModule
      )
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE01_CHECK_DO.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role01-check-do/public-role01-check-do.module"
      ).then(m => m.PublicRole01CheckDoModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE01_PREPARE.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role01-prepare/public-role01-prepare.module"
      ).then(m => m.PublicRole01PrepareModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE01_DOING.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role01-doing/public-role01-doing.module").then(
        m => m.PublicRole01DoingModule
      )
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role02-check/public-role02-check.module").then(
        m => m.PublicRole02CheckModule
      )
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_CONSIDER_PAYMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role02-consider-payment/public-role02-consider-payment.module"
      ).then(m => m.PublicRole02ConsiderPaymentModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE05_CONSIDER_PAYMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role05-consider-payment/public-role05-consider-payment.module"
      ).then(m => m.PublicRole05ConsiderPaymentModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_DOCUMENT_PAYMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role02-document-payment/public-role02-document-payment.module"
      ).then(m => m.PublicRole02DocumentPaymentModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_DOCUMENT_POST.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role02-document-post/public-role02-document-post.module"
      ).then(m => m.PublicRole02DocumentPostModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_ACTION_POST.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role02-action-post/public-role02-action-post.module"
      ).then(m => m.PublicRole02ActionPostModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE04_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role04-item/public-role04-item.module").then(
        m => m.PublicRole04itemModule
      )
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE04_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role04-check/public-role04-check.module").then(
        m => m.PublicRole04CheckModule
      )
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE05_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role05-document/public-role05-document.module"
      ).then(m => m.PublicRole05DocumentModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE04_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role04-document/public-role04-document.module"
      ).then(m => m.PublicRole04DocumentModule)
  },

  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/public-role02-document/public-role02-document.module"
      ).then(m => m.PublicRole02DocumentModule)
  },

  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE02_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role02-item/document-role02-item.module").then(
        m => m.DocumentRole02ItemModule
      )
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_140_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-140-process/eform-save-140-process.module"
      ).then(m => m.eFormSave140ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_150_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-150-process/eform-save-150-process.module"
      ).then(m => m.eFormSave150ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_190_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-190-process/eform-save-190-process.module"
      ).then(m => m.eFormSave190ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_200_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-200-process/eform-save-200-process.module"
      ).then(m => m.eFormSave200ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_210_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-210-process/eform-save-210-process.module"
      ).then(m => m.eFormSave210ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_220_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-220-process/eform-save-220-process.module"
      ).then(m => m.eFormSave220ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_230_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-230-process/eform-save-230-process.module"
      ).then(m => m.eFormSave230ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_030_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-030-process/eform-save-030-process.module"
      ).then(m => m.eFormSave030ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_040_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-040-process/eform-save-040-process.module"
      ).then(m => m.eFormSave040ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_050_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-050-process/eform-save-050-process.module"
      ).then(m => m.eFormSave050ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_060_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-060-process/eform-save-060-process.module"
      ).then(m => m.eFormSave060ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_070_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-070-process/eform-save-070-process.module"
      ).then(m => m.eFormSave070ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_080_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-080-process/eform-save-080-process.module"
      ).then(m => m.eFormSave080ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_021_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-021-process/eform-save-021-process.module"
      ).then(m => m.eFormSave021ProcessModule)
  },

  {
    path: `${ROUTE_PATH.EFORM_SAVE_120_PROCESS.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/eform-save-120-process/eform-save-120-process.module"
      ).then(m => m.eFormSave120ProcessModule)
  },

  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE02_CHECK.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/document-role02-check/document-role02-check.module"
      ).then(m => m.DocumentRole02CheckModule)
  },

  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE02_PRINT_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/document-role02-print-document/document-role02-print-document.module"
      ).then(m => m.DocumentRole02PrintDocumentModule)
  },

  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE02_PRINT_COVER.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/document-role02-print-cover/document-role02-print-cover.module"
      ).then(m => m.DocumentRole02PrintCoverModule)
  },

  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE02_PRINT_LIST.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/document-role02-print-list/document-role02-print-list.module"
      ).then(m => m.DocumentRole02PrintModule)
  },

  {
    path: `${ROUTE_PATH.REQUEST_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-check/request-check.module").then(
        m => m.RequestCheckModule
      )
  },

  {
    path: `${ROUTE_PATH.REQUEST_SEARCH_PEOPLE.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/request-search-people/request-search-people.module"
      ).then(m => m.RequestSearchPeopleModule)
  },

  {
    path: `${ROUTE_PATH.FLOOR3_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import("./modules/floor3-document/floor3-document.module").then(
        m => m.Floor3DocumentModule
      )
  },

  {
    path: `${ROUTE_PATH.RECORD_REGISTRATION_NUMBER.ROUTE}`,
    loadChildren: () =>
      import("./modules/record-registration-number/record-registration-number.module").then(
        m => m.RecordRegistrationNumberModule
      )
  },
  {
    path: `${ROUTE_PATH.RECORD_REGISTRATION_NUMBER_ALLOW.ROUTE}`,
    loadChildren: () =>
      import("./modules/record-registration-number-allow/record-registration-number-allow.module").then(
        m => m.RecordRegistrationNumberAllowModule
      )
  },
  {
    path: `${ROUTE_PATH.CHECKING_SIMILAR_COMPARE.ROUTE}`,
    loadChildren: () =>
      import("./modules/checking-similar-compare/checking-similar-compare.module").then(
        m => m.CheckingSimilarCompareModule
      )
  },
  {
    path: `${ROUTE_PATH.SAVE_SEND.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-send/save-send.module").then(
        m => m.SaveSendModule
      )
  },
  {
    path: `${ROUTE_PATH.SAVE_SEND_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-send/save-send.module").then(
        m => m.SaveSendModule
      )
  },
  {
    path: `${ROUTE_PATH.SAVE_SEND_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-send/save-send.module").then(
        m => m.SaveSendModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_PRINT_COVER.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-print-cover/request-print-cover.module").then(
        m => m.RequestPrintCoverModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_PRINT_COVER_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-print-cover/request-print-cover.module").then(
        m => m.RequestPrintCoverModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_PRINT_COVER_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-print-cover/request-print-cover.module").then(
        m => m.RequestPrintCoverModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE03_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role03-change/document-role03-change.module").then(
        m => m.DocumentRole03ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE03_CHANGE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role03-change/document-role03-change.module").then(
        m => m.DocumentRole03ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE03_CHANGE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role03-change/document-role03-change.module").then(
        m => m.DocumentRole03ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_DASHBOARD.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-dashboard/document-role04-dashboard.module").then(
        m => m.DocumentRole04DashboardModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_DOCUMENT_INSTRUCTION.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-document-instruction/document-role04-document-instruction.module").then(
        m => m.DocumentRole04DocumentInstructionModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_DOCUMENT_INSTRUCTION_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-document-instruction/document-role04-document-instruction.module").then(
        m => m.DocumentRole04DocumentInstructionModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_DOCUMENT_INSTRUCTION_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-document-instruction/document-role04-document-instruction.module").then(
        m => m.DocumentRole04DocumentInstructionModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_APPEAL.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-appeal/document-role04-appeal.module").then(
        m => m.DocumentRole04AppealModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_APPEAL_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-appeal/document-role04-appeal.module").then(
        m => m.DocumentRole04AppealModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_APPEAL_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-appeal/document-role04-appeal.module").then(
        m => m.DocumentRole04AppealModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE_REQUEST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release-request/document-role04-release-request.module").then(
        m => m.DocumentRole04ReleaseRequestModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE_REQUEST_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release-request/document-role04-release-request.module").then(
        m => m.DocumentRole04ReleaseRequestModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE_REQUEST_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release-request/document-role04-release-request.module").then(
        m => m.DocumentRole04ReleaseRequestModule
      )
  },
  //{
  //  path: `${ROUTE_PATH.DOCUMENT_ROLE05_ITEM.ROUTE}`,
  //  loadChildren: () =>
  //    import("./modules/document-role05-item/document-role05-item.module").then(
  //      m => m.DocumentRole05ItemModule
  //    )
  //},
  //{
  //  path: `${ROUTE_PATH.DOCUMENT_ROLE05_ITEM_EDIT.ROUTE}`,
  //  loadChildren: () =>
  //    import("./modules/document-role05-item/document-role05-item.module").then(
  //      m => m.DocumentRole05ItemModule
  //    )
  //},
  {
    path: `${ROUTE_PATH.DASHBOARD_PUBLIC_ROLE_01.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-public-role-01/dashboard-public-role-01.module").then(
        m => m.DashboardPublicRole01Module
      )
  },
  {
    path: `${ROUTE_PATH.DASHBOARD_PUBLIC_ROLE_01_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-public-role-01/dashboard-public-role-01.module").then(
        m => m.DashboardPublicRole01Module
      )
  },
  {
    path: `${ROUTE_PATH.DASHBOARD_PUBLIC_ROLE_01_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-public-role-01/dashboard-public-role-01.module").then(
        m => m.DashboardPublicRole01Module
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-change/document-role05-change.module").then(
        m => m.DocumentRole05ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CHANGE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-change/document-role05-change.module").then(
        m => m.DocumentRole05ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CHANGE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-change/document-role05-change.module").then(
        m => m.DocumentRole05ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CONSIDER_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-consider-document/document-role05-consider-document.module").then(
        m => m.DocumentRole05ConsiderDocumentModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CONSIDER_DOCUMENT_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-consider-document/document-role05-consider-document.module").then(
        m => m.DocumentRole05ConsiderDocumentModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CONSIDER_DOCUMENT_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-consider-document/document-role05-consider-document.module").then(
        m => m.DocumentRole05ConsiderDocumentModule
      )
  },
  {
    path: `${ROUTE_PATH.DASHBOARD_PUBLIC_ROLE_04.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-public-role-04/dashboard-public-role-04.module").then(
        m => m.DashboardPublicRole04Module
      )
  },
  {
    path: `${ROUTE_PATH.DASHBOARD_PUBLIC_ROLE_04_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-public-role-04/dashboard-public-role-04.module").then(
        m => m.DashboardPublicRole04Module
      )
  },
  {
    path: `${ROUTE_PATH.DASHBOARD_PUBLIC_ROLE_04_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-public-role-04/dashboard-public-role-04.module").then(
        m => m.DashboardPublicRole04Module
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-change/document-role04-change.module").then(
        m => m.DocumentRole04ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CHANGE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-change/document-role04-change.module").then(
        m => m.DocumentRole04ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CHANGE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-change/document-role04-change.module").then(
        m => m.DocumentRole04ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_PROCESS_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-process-change/document-process-change.module").then(
        m => m.DocumentProcessChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_PROCESS_CHANGE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-process-change/document-process-change.module").then(
        m => m.DocumentProcessChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_PROCESS_CHANGE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-process-change/document-process-change.module").then(
        m => m.DocumentProcessChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_PROCESS_COLLECT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-process-collect/document-process-collect.module").then(
        m => m.DocumentProcessCollectModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_PROCESS_COLLECT_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-process-collect/document-process-collect.module").then(
        m => m.DocumentProcessCollectModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_PROCESS_COLLECT_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-process-collect/document-process-collect.module").then(
        m => m.DocumentProcessCollectModule
      )
  },
  {
    path: `${ROUTE_PATH.DASHBOARD_DOCUMENT_ROLE_01.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-document-role-01/dashboard-document-role-01.module").then(
        m => m.DashboardDocumentRole01Module
      )
  },
  {
    path: `${ROUTE_PATH.DASHBOARD_DOCUMENT_ROLE_01_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-document-role-01/dashboard-document-role-01.module").then(
        m => m.DashboardDocumentRole01Module
      )
  },
  {
    path: `${ROUTE_PATH.DASHBOARD_DOCUMENT_ROLE_01_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/dashboard-document-role-01/dashboard-document-role-01.module").then(
        m => m.DashboardDocumentRole01Module
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDERING_SIMILAR_KOR10.ROUTE}`,
    loadChildren: () =>
      import("./modules/considering-similar-kor10/considering-similar-kor10.module").then(
        m => m.ConsideringSimilarKor10Module
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDERING_SIMILAR_KOR10_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/considering-similar-kor10/considering-similar-kor10.module").then(
        m => m.ConsideringSimilarKor10Module
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDERING_SIMILAR_KOR10_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/considering-similar-kor10/considering-similar-kor10.module").then(
        m => m.ConsideringSimilarKor10Module
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE01_CASE41_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role01-case41-check/public-role01-case41-check.module").then(
        m => m.PublicRole01Case41CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE01_CASE41_CHECK_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role01-case41-check/public-role01-case41-check.module").then(
        m => m.PublicRole01Case41CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE01_CASE41_CHECK_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role01-case41-check/public-role01-case41-check.module").then(
        m => m.PublicRole01Case41CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-item/document-role04-item.module").then(
        m => m.DocumentRole04ItemModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_ITEM_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-item/document-role04-item.module").then(
        m => m.DocumentRole04ItemModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_ITEM_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-item/document-role04-item.module").then(
        m => m.DocumentRole04ItemModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-check/document-role04-check.module").then(
        m => m.DocumentRole04CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CHECK_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-check/document-role04-check.module").then(
        m => m.DocumentRole04CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_ITEM_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-item/document-role05-item.module").then(
        m => m.DocumentRole05ItemModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-check/document-role05-check.module").then(
        m => m.DocumentRole05CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CHECK_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-check/document-role05-check.module").then(
        m => m.DocumentRole05CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CHECK_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-check/document-role05-check.module").then(
        m => m.DocumentRole05CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CHECK_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-check/document-role04-check.module").then(
        m => m.DocumentRole04CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE_REQUEST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release-request/document-role05-release-request.module").then(
        m => m.DocumentRole05ReleaseRequestModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE_REQUEST_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release-request/document-role05-release-request.module").then(
        m => m.DocumentRole05ReleaseRequestModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE_REQUEST_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release-request/document-role05-release-request.module").then(
        m => m.DocumentRole05ReleaseRequestModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CREATE.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-create/document-role04-create.module").then(
        m => m.DocumentRole04CreateModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CREATE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-create/document-role04-create.module").then(
        m => m.DocumentRole04CreateModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_CREATE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-create/document-role04-create.module").then(
        m => m.DocumentRole04CreateModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CREATE.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-create/document-role05-create.module").then(
        m => m.DocumentRole05CreateModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CREATE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-create/document-role05-create.module").then(
        m => m.DocumentRole05CreateModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_CREATE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-create/document-role05-create.module").then(
        m => m.DocumentRole05CreateModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release/document-role04-release.module").then(
        m => m.DocumentRole04ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release/document-role04-release.module").then(
        m => m.DocumentRole04ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release/document-role04-release.module").then(
        m => m.DocumentRole04ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE_20.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release-20/document-role04-release-20.module").then(
        m => m.DocumentRole04Release20Module
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE_20_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release-20/document-role04-release-20.module").then(
        m => m.DocumentRole04Release20Module
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE04_RELEASE_20_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role04-release-20/document-role04-release-20.module").then(
        m => m.DocumentRole04Release20Module
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE_20.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release-20/document-role05-release-20.module").then(
        m => m.DocumentRole05Release20Module
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE_20_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release-20/document-role05-release-20.module").then(
        m => m.DocumentRole05Release20Module
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE_20_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release-20/document-role05-release-20.module").then(
        m => m.DocumentRole05Release20Module
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release/document-role05-release.module").then(
        m => m.DocumentRole05ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release/document-role05-release.module").then(
        m => m.DocumentRole05ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE05_RELEASE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role05-release/document-role05-release.module").then(
        m => m.DocumentRole05ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE03_ITEM.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role03-item/document-role03-item.module").then(
        m => m.DocumentRole03ItemModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE03_ITEM_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role03-item/document-role03-item.module").then(
        m => m.DocumentRole03ItemModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE03_ITEM_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role03-item/document-role03-item.module").then(
        m => m.DocumentRole03ItemModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_EFORM.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-eform/request-eform.module").then(
        m => m.RequestEformModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_EFORM_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-eform/request-eform.module").then(
        m => m.RequestEformModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_EFORM_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-eform/request-eform.module").then(
        m => m.RequestEformModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_OTHER_EFORM.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-other-eform/request-other-eform.module").then(
        m => m.RequestOtherEformModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_OTHER_EFORM_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-other-eform/request-other-eform.module").then(
        m => m.RequestOtherEformModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_OTHER_EFORM_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-other-eform/request-other-eform.module").then(
        m => m.RequestOtherEformModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE05_OTHER.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role05-other/public-role05-other.module").then(
        m => m.PublicRole05OtherModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE05_OTHER_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role05-other/public-role05-other.module").then(
        m => m.PublicRole05OtherModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE05_OTHER_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role05-other/public-role05-other.module").then(
        m => m.PublicRole05OtherModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE01_IMPORT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role01-import/madrid-role01-import.module").then(
        m => m.MadridRole01ImportModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE01_IMPORT_SEARCH.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role01-import-search/madrid-role01-import-search.module").then(
        m => m.MadridRole01ImportSearchModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE01_MM02.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role01-mm02/madrid-role01-mm02.module").then(
        m => m.MadridRole01Mm02Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_DASHBOARD.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-dashboard/madrid-role02-dashboard.module").then(
        m => m.MadridRole02DashboardModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_SEARCH.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-search/madrid-role02-search.module").then(
        m => m.MadridRole02SearchModule
      )
  },

  {
    path: `${ROUTE_PATH.MADRID_ROLE02_SEARCH_REGIS.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-search-regis/madrid-role02-search-regis.module").then(
        m => m.MadridRole02SearcRegishModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE03_DASHBOARD.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-dashboard/madrid-role03-dashboard.module").then(
        m => m.MadridRole03DashboardModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE03_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-check/madrid-role03-check.module").then(
        m => m.MadridRole03CheckModule
      )
  },
  {
    path: `${ROUTE_PATH. MADRID_ROLE03_DT07_03.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-DT07_03/madrid-role03-DT07_03.module").then(
        m => m.MadridRole03DT07_03Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE03_SEARCH.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-search/madrid-role03-search.module").then(
        m => m.MadridRole03SearchModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE03_SEARCH_GROUP_LEADER.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-search-group-leader/madrid-role03-search-group-leader.module").then(
        m => m.MadridRole03SearchGroupLeaderModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE03_SEARCH_REGIS.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-search-regis/madrid-role03-search-regis.module").then(
        m => m.MadridRole03SearchRegisModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE03_SEARCH_WIPO.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-search-wipo/madrid-role03-search-wipo.module").then(
        m => m.MadridRole03SearchWIPOModule
      )
  },
  {
  path: `${ROUTE_PATH.MADRID_ROLE03_SEARCH_LIST.ROUTE}`,
  loadChildren: () =>
    import("./modules/madrid-role03-search-list/madrid-role03-search-list.module").then(
      m => m.MadridRole03SearchListModule
    )
},
{
  path: `${ROUTE_PATH.MADRID_ROLE03_SEARCH_FINISH_WORK.ROUTE}`,
  loadChildren: () =>
    import("./modules/madrid-role03-search-finish-work/madrid-role03-search-finish-work.module").then(
      m => m.MadridRole03SearchFinishWorkModule
    )
},
  {
    path: `${ROUTE_PATH.MADRID_ROLE03_SEARCH_WIPO_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-search-wipo-change/madrid-role03-search-wipo-change.module").then(
        m => m.MadridRole03SearchWIPOChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE03_SEARCH_CONSIDER.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role03-search-consider/madrid-role03-search-consider.module").then(
        m => m.MadridRole03SearchConsiderModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_WAIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-wait/madrid-role02-wait.module").then(
        m => m.MadridRole02WaitModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_CHECK2.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-check2-list/madrid-role02-check2-list.module").then(
        m => m.MadridRole02Check2ListModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_SEARCH_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-search-edit/madrid-role02-search-edit.module").then(
        m => m.MadridRole02SearchEditModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-change/madrid-role02-change.module").then(
        m => m.MadridRole02ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_ANNOUNCE.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-announce/madrid-role02-announce.module").then(
        m => m.MadridRole02AnnounceModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_OTHER.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role02-other/public-role02-other.module").then(
        m => m.PublicRole02OtherModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_OTHER_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role02-other/public-role02-other.module").then(
        m => m.PublicRole02OtherModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_OTHER_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role02-other/public-role02-other.module").then(
        m => m.PublicRole02OtherModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_CONSIDERING_OTHER.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role02-considering-other/public-role02-considering-other.module").then(
        m => m.PublicRole02ConsideringOtherModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_CONSIDERING_OTHER_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role02-considering-other/public-role02-considering-other.module").then(
        m => m.PublicRole02ConsideringOtherModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_CONSIDERING_OTHER_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role02-considering-other/public-role02-considering-other.module").then(
        m => m.PublicRole02ConsideringOtherModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_REJECT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-reject/madrid-role02-reject.module").then(
        m => m.MadridRole02RejectModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_OTHER.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-other/madrid-role02-other.module").then(
        m => m.MadridRole02OtherModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_REGIS_INTER.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-regis-inter/madrid-role02-regis-inter.module").then(
        m => m.MadridRole02RegisInterModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_EDIT_INTER.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-edit-inter/madrid-role02-edit-inter.module").then(
        m => m.MadridRole02EditInterModule
      )
  },
  //    {
  //     path: `${ROUTE_PATH.MADRID_ROLE02_REGIS_INTER_ADD.ROUTE}`,
  //     loadChildren: () =>
  //       import("./modules/madrid-role02-regis-inter/madrid-role02-regis-inter.module").then(
  //         m => m.MadridRole02RegisInterModule
  //       )
  //   },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_DASHBOARD.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-dashboard/madrid-role04-dashboard.module").then(
        m => m.MadridRole04DashboardModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_EXAMINATION.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-examination/madrid-role02-examination.module").then(
        m => m.MadridRole02ExaminationModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_SEARCH.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-search/madrid-role04-search.module").then(
        m => m.MadridRole04SearchModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_CHECK.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-check/madrid-role04-check.module").then(
        m => m.MadridRole04CheckModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_SEARCH_GROUP_LEADER.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-search-group-leader/madrid-role04-search-group-leader.module").then(
        m => m.MadridRole04SearchGroupLeaderModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_SEARCH_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-search-list/madrid-role04-search-list.module").then(
        m => m.MadridRole04SearchListModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_SEARCH_WIPO.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-search-wipo/madrid-role04-search-wipo.module").then(
        m => m.MadridRole04SearchWIPOModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_SEARCH_REGIS.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-search-regis/madrid-role04-search-regis.module").then(
        m => m.MadridRole04SearchRegisModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_SEARCH_WIPO_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-search-wipo-change/madrid-role04-search-wipo-change.module").then(
        m => m.MadridRole04SearchWIPOChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_SEARCH_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-search-change/madrid-role04-search-change.module").then(
        m => m.MadridRole04SearchChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE04_SEARCH_FINISH_WORK.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role04-search-finish-work/madrid-role04-search-finish-work.module").then(
        m => m.MadridRole04SearchFinishWorkModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_DASHBOARD.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-dashboard/madrid-role05-dashboard.module").then(
        m => m.MadridRole05DashboardModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_DASHBOARD_OUT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-dashboard-out/madrid-role05-dashboard-out.module").then(
        m => m.MadridRole05DashboardOutModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_DASHBOARD_INCOMING_ALL.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-search-incoming-all/madrid-role05-search-incoming-all.module").then(
        m => m.MadridRole05SearchIncomingAllModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_WORKBOX_WIPO.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-workbox-wipo/madrid-role05-workbox-wipo.module").then(
        m => m.MadridRole05WorkboxWipoModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_SEARCH.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-search/madrid-role05-search.module").then(
        m => m.MadridRole05SearchModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_REQUEST_IN.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-request-in/madrid-role05-request-in.module").then(
        m => m.MadridRole05RequestInModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_FINISH.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-finish/madrid-role05-finish.module").then(
        m => m.MadridRole05FinishModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_WORK_PAYMENT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-work-payment/madrid-role05-work-payment.module").then(
        m => m.MadridRole05WorkPaymentModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE05_BOX_REF.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role05-box-ref/madrid-role05-box-ref.module").then(
        m => m.MadridRole05BoxRefModule
      )
  },
  // {
  //   path: `${ROUTE_PATH.MADRID_ROLE05_JOP_TRANSFER.ROUTE}`,
  //   loadChildren: () =>
  //     import("./modules/madrid-role05-search/madrid-role05-jop-transfer.module").then(
  //       m => m.MadridRole05JopTransferModule
  //     )
  // },
  // {
  //   path: `${ROUTE_PATH.MADRID_ROLE05_MODAL.ROUTE}`,
  //   loadChildren: () =>
  //     import("./modules/madrid-role05-modal/madrid-role05-modal.module").then(
  //       m => m.MadridRole05ModalModule
  //     )
  // },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE03_RELEASE.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role03-release/public-role03-release.module").then(
        m => m.PublicRole03ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE03_RELEASE_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role03-release/public-role03-release.module").then(
        m => m.PublicRole03ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE03_RELEASE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role03-release/public-role03-release.module").then(
        m => m.PublicRole03ReleaseModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_DASHBOARD.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ni-dashboard/madrid-role06-ni-dashboard.module").then(
        m => m.MadridRole06NiDashboardModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_NI01_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ni01/madrid-role06-ni01.module").then(
        m => m.MadridRole06Ni01Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_NI02_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ni02/madrid-role06-ni02.module").then(
        m => m.MadridRole06Ni02Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_NI03_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ni03/madrid-role06-ni03.module").then(
        m => m.MadridRole06Ni03Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_NI04_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ni04/madrid-role06-ni04.module").then(
        m => m.MadridRole06Ni04Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_OT02_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ot02-list/madrid-role06-ot02-list.module").then(
        m => m.MadridRole06OT02Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_OT03_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ot03-list/madrid-role06-ot03-list.module").then(
        m => m.MadridRole06OT03Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_OT04_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ot04-list/madrid-role06-ot04-list.module").then(
        m => m.MadridRole06OT04Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SAVE_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-save-list/madrid-role07-save-list.module").then(
        m => m.MadridRole07SaveListModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SEARCH_CANCEL.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-search-cancel/madrid-role07-search-cancel.module").then(
        m => m.MadridRole07SearchCancelModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SEARCH_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-search-edit/madrid-role07-search-edit.module").then(
        m => m.MadridRole07SaveListModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SEARCH_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-search-change/madrid-role07-search-change.module").then(
        m => m.MadridRole07SearchChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SEARCH_EDIT_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-search-edit-change/madrid-role07-search-edit-change.module").then(
        m => m.MadridRole07SearchEditChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SEARCH_NOTIFY.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-search-notify/madrid-role07-search-notify.module").then(
        m => m.MadridRole07SeachNotifyModule
      )
  },
  {
    path: `${ROUTE_PATH.MM06.ROUTE}`,
    loadChildren: () =>
      import("./modules/mm06/mm06.module").then(
        m => m.Mm06Module
      )
  },
  {
    path: `${ROUTE_PATH.MM05.ROUTE}`,
    loadChildren: () =>
      import("./modules/mm05/mm05.module").then(
        m => m.Mm05Module
      )
  },
  {
    path: `${ROUTE_PATH.MM07.ROUTE}`,
    loadChildren: () =>
      import("./modules/mm07/mm07.module").then(
        m => m.Mm07Module
      )
  },
  {
    path: `${ROUTE_PATH.MM08.ROUTE}`,
    loadChildren: () =>
      import("./modules/mm08/mm08.module").then(
        m => m.Mm08Module
      )
  },
  {
    path: `${ROUTE_PATH.MM09.ROUTE}`,
    loadChildren: () =>
      import("./modules/mm09/mm09.module").then(
        m => m.Mm09Module
      )
  },
  {
    path: `${ROUTE_PATH.MM02.ROUTE}`,
    loadChildren: () =>
      import("./modules/mm02/mm02.module").then(
        m => m.Mm02Module
      )
  },
  {
    path: `${ROUTE_PATH.MM04.ROUTE}`,
    loadChildren: () =>
      import("./modules/mm04/mm04.module").then(
        m => m.Mm04Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SEARCH_RECIEVE_IRN.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-search-recieve-irn/madrid-role07-search-recieve-irn.module").then(
        m => m.MadridRole07SearchRecieveIrnModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SEARCH_WAIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-search-wait/madrid-role07-search-wait.module").then(
        m => m.MadridRole07SearchWaitModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_SAVE_LIST_STEP1.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-save-list-step1/madrid-role07-save-list-step1.module").then(
        m => m.MadridRole07SaveListStep1Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE07_DOC_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role07-doc-edit/madrid-role07-doc-edit.module").then(
        m => m.MadridRole07DocEditModule
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_NI05_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ni05/madrid-role06-ni05.module").then(
        m => m.MadridRole06Ni05Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE06_NI06_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role06-ni06/madrid-role06-ni06.module").then(
        m => m.MadridRole06Ni06Module
      )
  },
  {
    path: `${ROUTE_PATH.MADRID_ROLE02_MR2.ROUTE}`,
    loadChildren: () =>
      import("./modules/madrid-role02-mr2/madrid-role02-mr2.module").then(
        m => m.MadridRole02Mr2Module
      )
  },
  {
    path: `${ROUTE_PATH.INCLUDE_ROLE01_CHANGE.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/include-role01-change/include-role01-change.module"
      ).then((m) => m.IncludeRole01ChangeModule),
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR06_CHANGE.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/request-kor06-change/request-kor06-change.module"
      ).then((m) => m.RequestKor06ChangeModule),
  },
  {
    path: `${ROUTE_PATH.PUBLIC_ROLE02_PRINT_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import("./modules/public-role02-print-document/public-role02-print-document.module").then(
        m => m.PublicRole02PrintDocumentModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE02_ACTION_POST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role02-action-post/document-role02-action-post.module").then(
        m => m.DocumentRole02ActionPostModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE02_ACTION_POST_EDIT.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role02-action-post/document-role02-action-post.module").then(
        m => m.DocumentRole02ActionPostModule
      )
  },
  {
    path: `${ROUTE_PATH.DOCUMENT_ROLE02_ACTION_POST_LIST.ROUTE}`,
    loadChildren: () =>
      import("./modules/document-role02-action-post/document-role02-action-post.module").then(
        m => m.DocumentRole02ActionPostModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR04_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-kor04-change/request-kor04-change.module").then(
        m => m.RequestKor04ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR07_CHANGE.ROUTE}`,
    loadChildren: () =>
      import(
        "./modules/request-kor07-change/request-kor07-change.module"
      ).then((m) => m.RequestKor07ChangeModule),
  },
  {
    path: `${ROUTE_PATH.CONSIDER_KOR04_REQUEST.ROUTE}`,
    loadChildren: () =>
      import("./modules/consider-kor04-request/consider-kor04-request.module"   ).then(
        m => m.ConsiderKor04RequestModule
      )
  },
  {
    path: `${ROUTE_PATH.APPROVE_KOR20_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import("./modules/approve-kor20-document/approve-kor20-document.module").then(
        m => m.ApproveKor20DocumentModule
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDER_REVISING_REQUEST.ROUTE}`,
    loadChildren: () =>
      import("./modules/consider-revising-request/consider-revising-request.module").then(
        m => m.ConsiderRevisingRequestModule
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDER_SECTION55_P2.ROUTE}`,
    loadChildren: () =>
      import("./modules/consider-section55-p2/consider-section55-p2.module").then(
        m => m.ConsiderSection55P2Module
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDER_REVOCATION_SECTION56.ROUTE}`,
    loadChildren: () =>
      import("./modules/consider-revocation-section56/consider-revocation-section56.module").then(
        m => m.ConsiderRevocationSection56Module
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDER_SECTION56.ROUTE}`,
    loadChildren: () =>
      import("./modules/consider-section56/consider-section56.module").then(
        m => m.ConsiderSection56Module
      )
  },
  {
    path: `${ROUTE_PATH.CHECK_ISSUE_BOOK.ROUTE}`,
    loadChildren: () =>
      import("./modules/check-issue-book/check-issue-book.module").then(
        m => m.CheckIssueBookModule
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDER_DOCUMENT.ROUTE}`,
    loadChildren: () =>
      import("./modules/consider-document/consider-document.module").then(
        m => m.ConsiderDocumentModule
      )
  },
  {
    path: `${ROUTE_PATH.ROLE2_REQUEST_KOR04_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/r2-request-kor04-change/r2-request-kor04-change.module").then(
        m => m.R2RequestKor04ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.ROLE2_REQUEST_KOR06_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/r2-request-kor06-change/r2-request-kor06-change.module").then(
        m => m.R2RequestKor06ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.ROLE2_REQUEST_KOR07_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/r2-request-kor07-change/r2-request-kor07-change.module").then(
        m => m.R2RequestKor07ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.ROLE2_REQUEST_KOR20_CHANGE.ROUTE}`,
    loadChildren: () =>
      import("./modules/r2-request-kor20-change/r2-request-kor20-change.module").then(
        m => m.R2RequestKor20ChangeModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR08_REGISTRATION.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-kor08-registration/request-kor08-registration.module").then(
        m => m.RequestKor08RegistrationModule
      )
  },
  {
    path: `${ROUTE_PATH.ROLE2_CONSIDER_LICENSE_REQUEST.ROUTE}`,
    loadChildren: () =>
      import("./modules/r2-consider-license-request/r2-consider-license-request.module").then(
        m => m.R2ConsiderLicenseRequestModule
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDER_ABANDON_PETITION.ROUTE}`,
    loadChildren: () =>
      import("./modules/consider-abandon-petition/consider-abandon-petition.module").then(
        m => m.ConsiderAbandonPetitionModule
      )
  },
  {
    path: `${ROUTE_PATH.CONSIDER_LICENSE_REQUEST.ROUTE}`,
    loadChildren: () =>
      import("./modules/consider-license-request/consider-license-request.module").then(
        m => m.ConsiderLicenseRequestModule
      )
  },
  {
    path: `${ROUTE_PATH.ISSUE_IMPORTANT_DOCUMENTS.ROUTE}`,
    loadChildren: () =>
      import("./modules/issue-important-documents/issue-important-documents.module").then(
        m => m.IssueImportantDocumentsModule
      )
  },
  {
    path: `${ROUTE_PATH.RENEW.ROUTE}`,
    loadChildren: () =>
      import("./modules/renew/renew.module").then(
        m => m.RenewModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_PREVIOUS.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-previous/request-previous.module").then(
        m => m.RequestPreviousModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR04_PREVIOUS.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-kor04-previous/request-kor04-previous.module").then(
        m => m.RequestKor04PreviousModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR05_PREVIOUS.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-kor05-previous/request-kor05-previous.module").then(
        m => m.RequestKor05PreviousModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR06_PREVIOUS.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-kor06-previous/request-kor06-previous.module").then(
        m => m.RequestKor06PreviousModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR07_PREVIOUS.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-kor07-previous/request-kor07-previous.module").then(
        m => m.RequestKor07PreviousModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR08_PREVIOUS.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-kor08-previous/request-kor08-previous.module").then(
        m => m.RequestKor08PreviousModule
      )
  },
  {
    path: `${ROUTE_PATH.REQUEST_KOR14_PREVIOUS.ROUTE}`,
    loadChildren: () =>
      import("./modules/request-kor14-previous/request-kor14-previous.module").then(
        m => m.RequestKor14PreviousModule
      )
  },
  {
    path: `${ROUTE_PATH.PRINT_LOG.ROUTE}`,
    loadChildren: () =>
      import("./modules/print-log/print-log.module").then(
        m => m.PrintLogModule
      )
  },
  {
    path: `${ROUTE_PATH.PRINT_BOOK.ROUTE}`,
    loadChildren: () =>
      import("./modules/print-book/print-book.module").then(
        m => m.PrintBookModule
      )
  },
  {
    path: `${ROUTE_PATH.PRINT_COVER.ROUTE}`,
    loadChildren: () =>
      import("./modules/print-cover/print-cover.module").then(
        m => m.PrintCoverModule
      )
  },
  {
    path: `${ROUTE_PATH.CREATE_DELIVERY.ROUTE}`,
    loadChildren: () =>
      import("./modules/create-delivery/create-delivery.module").then(
        m => m.CreateDeliveryModule
      )
  },
  {
    path: `${ROUTE_PATH.SAVE_RECEIPT_POSTAL.ROUTE}`,
    loadChildren: () =>
      import("./modules/save-receipt-postal/save-receipt-postal.module").then(
        m => m.SaveReceiptPostalModule
      )
  },

  //APPEAL
  {
    path: `${ROUTE_PATH.APPEAL_ROLE01_KOR_OTHER_COMMERCIAL_PROVINCE.ROUTE}`,
    loadChildren: () =>
    import("./modules/appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province.module").then(
    m => m.AppealRole01KorOtherCommercialProvinceModule
   )
},
{
    path: `${ROUTE_PATH.APPEAL_ROLE01_REGISTER_REVOKE_KOR08.ROUTE}`,
    loadChildren: () =>
    import("./modules/appeal-role01-register-revoke-kor08/appeal-role01-register-revoke-kor08.module").then(
    m => m.AppealRole01RegisterRevokeKor08Module
   )
},



{
    path: `${ROUTE_PATH.APPEAL_ROLE01_KOR_OTHER_FLOOR3.ROUTE}`,
    loadChildren: () =>
    import("./modules/appeal-role01-kor-other-floor3/appeal-role01-kor-other-floor3.module").then(
   m => m.AppealRole01KorOtherFloor3Module
   )
    },
{
    path: `${ROUTE_PATH.APPEAL_ROLE01_KOR_OTHER_FLOOR3_LIST.ROUTE}`,
    loadChildren: () =>
    import("./modules/appeal-role01-kor-other-floor3/appeal-role01-kor-other-floor3.module").then(
    m => m.AppealRole01KorOtherFloor3Module
    )
    },
{
    path: `${ROUTE_PATH.APPEAL_ROLE01_APPEAL_KOR03_SAVE_EDIT.ROUTE}`,
    loadChildren: () =>
    import("./modules/appeal-role01-appeal-kor03-save/appeal-role01-appeal-kor03-save.module").then(
    m => m.AppealRole01AppealKor03SaveModule
    )
    },
    {
      path: `${ROUTE_PATH.APPEAL_ROLE01_APPEAL_KOR_ALL_LIST.ROUTE}`,
      loadChildren: () =>
      import("./modules/appeal-role01-appeal-kor-all-list/appeal-role01-appeal-kor-all-list.module").then(
      m => m.AppealRole01AppealKorAllListModule
      )
      },


    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_CASE_SUMMARY.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-case-summary/appeal-role02-case-summary.module").then(
        m => m.AppealRole02CaseSummaryModule
        )
    },
    {
      path: `${ROUTE_PATH.APPEAL_ROLE02_CASE_SUMMARY_EDIT.ROUTE}`,
      loadChildren: () =>
      import("./modules/appeal-role02-case-summary/appeal-role02-case-summary.module").then(
      m => m.AppealRole02CaseSummaryModule
      )
  },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_CASE_SUMMARY_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-case-summary/appeal-role02-case-summary.module").then(
        m => m.AppealRole02CaseSummaryModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_REGISTER_CONSIDER_BOARD.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-register-consider-board/appeal-role02-register-consider-board.module").then(
        m => m.AppealRole02RegisterConsiderBoardModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_MEETING_REPORT.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-meeting-report/appeal-role02-meeting-report.module").then(
        m => m.AppealRole02MeetingReportModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_MEETING_REPORT_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-meeting-report/appeal-role02-meeting-report.module").then(
        m => m.AppealRole02MeetingReportModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_BOARD_DECISION.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-board-decision/appeal-role02-board-decision.module").then(
        m => m.AppealRole02BoardDecisionModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_BOARD_DECISION_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-board-decision/appeal-role02-board-decision.module").then(
        m => m.AppealRole02BoardDecisionModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_REGISTER_BOOK_NOTIFY.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-register-book-notify/appeal-role02-register-book-notify.module").then(
        m => m.AppealRole02RegisterBookNotifyModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE02_REGISTER_COMMENT.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role02-register-comment/appeal-role02-register-comment.module").then(
        m => m.AppealRole02RegisterCommentModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE03_REGISTER_COMMENT.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role03-register-comment/appeal-role03-register-comment.module").then(
        m => m.AppealRole03RegisterCommentModule
        )
    },
    {
      path: `${ROUTE_PATH.APPEAL_ROLE03_CASE_SUMMARY.ROUTE}`,
      loadChildren: () =>
      import("./modules/appeal-role03-case-summary/appeal-role03-case-summary.module").then(
      m => m.AppealRole03CaseSummaryModule
      )
  },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE03_CASE_SUMMARY_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role03-case-summary/appeal-role03-case-summary.module").then(
        m => m.AppealRole03CaseSummaryModule
        )
    },

    {
        path: `${ROUTE_PATH.APPEAL_ROLE03_SEND_MEETING_AGENDA_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role03-send-meeting-agenda/appeal-role03-send-meeting-agenda.module").then(
        m => m.AppealRole03SendMeetingAgendaModule
        )
    },
    {
      path: `${ROUTE_PATH.APPEAL_ROLE03_REGISTER_CONSIDER_BOARD.ROUTE}`,
      loadChildren: () =>
      import("./modules/appeal-role03-register-consider-board/appeal-role03-register-consider-board.module").then(
      m => m.AppealRole03RegisterConsiderBoardModule
      )
  },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE03_BOARD_DECISION_SUMMARY_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role03-board-decision-summary/appeal-role03-board-decision-summary.module").then(
        m => m.AppealRole03BoardDecisionSummaryModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE03_MEETING_SUMMARY.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role03-meeting-summary/appeal-role03-meeting-summary.module").then(
        m => m.AppealRole03MeetingSummaryModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE03_MEETING_SUMMARY_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role03-meeting-summary/appeal-role03-meeting-summary.module").then(
        m => m.AppealRole03MeetingSummaryModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE03_CHECK_BEFORE_ISSUING_BOOK.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role03-check-before-issuing-book/appeal-role03-check-before-issuing-book.module").then(
        m => m.AppealRole03CheckBeforeIssuingBookModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE03_CHECK_BEFORE_ISSUING_BOOK_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role03-check-before-issuing-book/appeal-role03-check-before-issuing-book.module").then(
        m => m.AppealRole03CheckBeforeIssuingBookModule
        )
    },

    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_CASE_SUMMARY_K03.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-case-summary-k03/appeal-role04-case-summary-k03.module").then(
        m => m.AppealRole04CaseSummaryK03Module
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_CASE_SUMMARY_K03_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-case-summary-k03/appeal-role04-case-summary-k03.module").then(
        m => m.AppealRole04CaseSummaryK03Module
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_BOARD_APPROVAL.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-board-approval/appeal-role04-board-approval.module").then(
        m => m.AppealRole04BoardApprovalModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_BOARD_APPROVAL_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-board-approval/appeal-role04-board-approval.module").then(
        m => m.AppealRole04BoardApprovalModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_DECISION_NUMBER_RELEASE.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-decision-number-release/appeal-role04-decision-number-release.module").then(
        m => m.AppealRole04DecisionNumberReleaseModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_DECISION_NUMBER_RELEASE_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-decision-number-release/appeal-role04-decision-number-release.module").then(
        m => m.AppealRole04DecisionNumberReleaseModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_ISSUING_BOOK_ROUND2.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-issuing-book-round2/appeal-role04-issuing-book-round2.module").then(
        m => m.AppealRole04IssuingBookRound2Module
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_ISSUING_BOOK_ROUND2_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-issuing-book-round2/appeal-role04-issuing-book-round2.module").then(
        m => m.AppealRole04IssuingBookRound2Module
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_ISSUING_BOOK_ROUND2.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-issuing-book-round2/appeal-role04-issuing-book-round2.module").then(
        m => m.AppealRole04IssuingBookRound2Module
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_ISSUING_BOOK_ROUND2_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-issuing-book-round2/appeal-role04-issuing-book-round2.module").then(
        m => m.AppealRole04IssuingBookRound2Module
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_DECISION_BOOK_COMMITTEE.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-decision-book-committee/appeal-role04-decision-book-committee.module").then(
        m => m.AppealRole04DecisionBookCommitteeModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_DECISION_BOOK_COMMITTEE_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-decision-book-committee/appeal-role04-decision-book-committee.module").then(
        m => m.AppealRole04DecisionBookCommitteeModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_RESPOND_BOOK_REVOKE.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-respond-book-revoke/appeal-role04-respond-book-revoke.module").then(
        m => m.AppealRole04RespondBookRevokeModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_RESPOND_BOOK_REVOKE_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-respond-book-revoke/appeal-role04-respond-book-revoke.module").then(
        m => m.AppealRole04RespondBookRevokeModule
        )
    },
    {
      path: `${ROUTE_PATH.APPEAL_ROLE04_BOOK_CONSIDER_BOARD_DECISION.ROUTE}`,
      loadChildren: () =>
      import("./modules/appeal-role04-book-consider-board-decision/appeal-role04-book-consider-board-decision.module").then(
      m => m.AppealRole04BookConsiderBoardDecisionModule
      )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_BOOK_CONSIDER_BOARD_DECISION_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-book-consider-board-decision/appeal-role04-book-consider-board-decision.module").then(
        m => m.AppealRole04BookConsiderBoardDecisionModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_RESPOND_BOOK_APPEAL.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-respond-book-appeal/appeal-role04-respond-book-appeal.module").then(
        m => m.AppealRole04RespondBookAppealModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE04_RESPOND_BOOK_APPEAL_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role04-respond-book-appeal/appeal-role04-respond-book-appeal.module").then(
          m => m.AppealRole04RespondBookAppealModule
          )
    },
    {
      path: `${ROUTE_PATH.APPEAL_ROLE04_PRINT_BOOK_LIST.ROUTE}`,
      loadChildren: () =>
      import("./modules/appeal-role04-print-book/appeal-role04-print-book.module").then(
      m => m.AppealRole04PrintBookModule
      )
  },
  {
      path: `${ROUTE_PATH.APPEAL_ROLE04_PRINT_COVER_LIST.ROUTE}`,
      loadChildren: () =>
      import("./modules/appeal-role04-print-cover/appeal-role04-print-cover.module").then(
      m => m.AppealRole04PrintCoverModule
      )
  },
  {
    path: `${ROUTE_PATH.APPEAL_ROLE04_CREATE_DELIVERY_NOTE_LIST.ROUTE}`,
    loadChildren: () =>
    import("./modules/appeal-role04-create-delivery-note/appeal-role04-create-delivery-note.module").then(
    m => m.AppealRole04CreateDeliveryNoteModule
    )
},
{
  path: `${ROUTE_PATH.APPEAL_ROLE04_SAVE_POSTAL_RECEIPT_LIST.ROUTE}`,
  loadChildren: () =>
  import("./modules/appeal-role04-save-postal-receipt/appeal-role04-save-postal-receipt.module").then(
  m => m.AppealRole04SavePostalReceiptModule
  )
},


    {
        path: `${ROUTE_PATH.APPEAL_ROLE05_CERTIFIED_MEETING_REPORT.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role05-certified-meeting-report/appeal-role05-certified-meeting-report.module").then(
        m => m.AppealRole05CertifiedMeetingReportModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE05_CERTIFIED_MEETING_REPORT_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role05-certified-meeting-report/appeal-role05-certified-meeting-report.module").then(
        m => m.AppealRole05CertifiedMeetingReportModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE05_APPROVAL_DRAFT_DECISION.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role05-approval-draft-decision/appeal-role05-approval-draft-decision.module").then(
        m => m.AppealRole05ApprovalDraftDecisionModule
        )
    },
    {
        path: `${ROUTE_PATH.APPEAL_ROLE05_APPROVAL_DRAFT_DECISION_LIST.ROUTE}`,
        loadChildren: () =>
        import("./modules/appeal-role05-approval-draft-decision/appeal-role05-approval-draft-decision.module").then(
        m => m.AppealRole05ApprovalDraftDecisionModule
        )
    },

  // Default page
  { path: "", redirectTo: `${ROUTE_PATH.NOT_FOUND.LINK}`, pathMatch: "full" },
  // Page not found
  {
    path: "**",
    loadChildren: () =>
      import("./modules/not-found/not-found.module").then(m => m.NotFoundModule)
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
