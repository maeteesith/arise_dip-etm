import * as FileSaver from "file-saver";

export const print = (id: any) => {
  const isIFrame = (input: HTMLElement | null): input is HTMLIFrameElement =>
    input !== null && input.tagName === "IFRAME";
  let frame = document.getElementById(id);
  if (isIFrame(frame) && frame.contentWindow) {
    frame.contentWindow.print();
  }
};

export const download = (url: any, name: any) => {
  FileSaver.saveAs(url, name);
};

export const viewPDF = (path: any, params: any) => {
  let form = document.createElement("form");
  let inputPayload = document.createElement("input");
  console.log(`%cparams[POST] ${path}`, "color: lime", params);

  // Crate form data
  form.setAttribute("method", "post");
  form.setAttribute("action", path);
  form.setAttribute("target", "_blank");

  // Set param
  inputPayload.setAttribute("type", "hidden");
  inputPayload.setAttribute("name", "payload");
  inputPayload.setAttribute("value", JSON.stringify(params));

  // Submit form
  form.appendChild(inputPayload);
  document.body.appendChild(form);
  form.submit();
};
