import {
  encrypt,
  decrypt
} from './crypto'

export const setLocalStorage = (name: string, value: string) => {
  localStorage.setItem(name, encrypt(value))
}

export const getLocalStorage = (name: string) => {
  return decrypt(localStorage.getItem(name))
}

export const removeLocalStorage = (name: string) => {
  localStorage.removeItem(name)
}