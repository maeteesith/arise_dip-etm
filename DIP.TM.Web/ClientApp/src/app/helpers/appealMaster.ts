export const AppealRequestList = [
    { name: 'ก.03', code: '03' },
    { name: 'ก.08', code: '08' },
]

export const SubCommList = [
    { name: 'รอเข้าอนุกรรมการ', code: '01' },
    { name: 'ผ่านอนุกรรมการ', code: '02' },
]

export const AgendaList = [
    { name: 'รอจัดทำ', code: '01' },
    { name: 'ไม่จัดทำ', code: '02' },
]

export const AppealRole04SavePostalReceiptList = [
    { 
        request_number: '741150971', pornor:'1432/234', torkor:'ตค.5(3)', book_date:'10/10/2560', due_date: '10/12/2560', accept: '', 
        accept_date:'', detail: '', status: 'รอการบันทึก' 
    },
    {
        request_number: '741150971', pornor:'1432/234', torkor:'ตค.9(1)', book_date:'10/10/2560', due_date: '10/12/2560', accept: 'ตอบรับ', 
        accept_date:'10/10/2560', detail: '', status: 'เสร็จสิ้น' 
    },
];

export const AppealRole04RespondBookRevokeList = [
    { request_number: '741150971', book: 'ตค.8(1)', revoke: 'พงศกร สุวรรณ', owner_name: 'ฮัลล์ โกลเฮอเอยี', detail: '', status: 'รอดำเนินการ' },
    { request_number: '741150972', book: 'ตค.8(4)', revoke: 'พงศกร สุวรรณ', owner_name: 'ฮัลล์ โกลเฮอเอยี', detail: '', status: 'เสร็จสิ้น' },
];

export const AppealRole04RespondBookAppealList = [
    { request_number: '741150971', book: 'ตค.9(12)', subject: 'บ่งเฉพาะ', owner_name: 'ฮัลล์ โกลเฮอเอยี', detail: '', status: 'รอดำเนินการ' },
    { request_number: '741150972', book: 'ตค.9(13)', subject: 'เพิกถอน', owner_name: 'ฮัลล์ โกลเฮอเอยี', detail: '', status: 'เสร็จสิ้น' },
];

export const AppealRole04PrintCoverList = [
    { 
      request_number: '741150971', send_date: '10/09/2562', inspector: 'นายมานพ มามาก', book_many: '1', receiver: 'นางสาวกมลทิพย์ มรกต', 
      print_date: '10/09/2562', detail: '', status: 'รอพิมพ์ปกจดหมาย'
    },
    { 
      request_number: '741150972', send_date: '10/09/2562', inspector: 'นายมานพ มามาก', book_many: '1', receiver: 'นางสาวสมันตา มีแก้ว', 
      print_date: '10/09/2562', detail: '', status: 'เสร็จสิ้น'
    },
];

export const AppealRole04PrintBookList = [
    { 
      request_number: '741150971', send_date: '10/09/2562', inspector: 'นายมานพ มามาก', book_many: '1', receiver: 'นางสาวกมลทิพย์ มรกต', 
      print_date: '10/09/2562', detail: '', status: 'รอดำเนินการ'
    },
    { 
      request_number: '741150972', send_date: '10/09/2562', inspector: 'นายมานพ มามาก', book_many: '1', receiver: 'นางสาวสมันตา มีแก้ว', 
      print_date: '10/09/2562', detail: '', status: 'เสร็จสิ้น'
    },
];


export const AppealRole04CreateDeliveryNoteList = [
    { 
      request_number: '741150971', send_date: '10/09/2562', inspector: 'นายมานพ มามาก', book_many: '4', receiver: 'นางสาวกมลทิพย์ มรกต', 
      create_date: '10/09/2562', number_13: '6475869774563', status: 'รอพิมพ์ปกจดหมาย'
    },
    { 
      request_number: '741150972', send_date: '10/09/2562', inspector: 'นายมานพ มามาก', book_many: '2', receiver: 'นางสาวสมันตา มีแก้ว', 
      create_date: '10/09/2562', number_13: 'มารับเอง', status: 'เสร็จสิ้น'
    },
    { 
      request_number: '741150972', send_date: '10/09/2562', inspector: 'นายมานพ มามาก', book_many: '3', receiver: 'นางสาวสมันตา มีแก้ว', 
      create_date: '10/09/2562', number_13: '', status: 'รอบันทึกเลข'
    },
];

export const AppealRole04BookConsiderBoardDecisionList = [
    { request_number: '741150971', book: 'ตค.6(3)', subject: 'เหมือนคล้าย', owner_name: 'ฮัลล์ โกลเฮอเอยี', detail: '', status: 'รอดำเนินการ' },
    { request_number: '741150972', book: 'ตค.6(3)', subject: 'เพิกถอน', owner_name: 'ฮัลล์ โกลเฮอเอยี', detail: '', status: 'เสร็จสิ้น' },
];