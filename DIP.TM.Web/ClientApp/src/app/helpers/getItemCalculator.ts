export const getItemCalculator = (qty: number, logic: any = defaultLogic) => {
  if(qty && logic){
    for (let index = 0; index < logic.length; index++) {
      let item = logic[index]
      if(!item.is_deleted) {
        if(between(+qty, +item.min_qty, +item.max_qty)) {
          return item.is_total_price ? item.price : ( qty*item.price )
        }
      }
    }
  }
  return 0
}

const between = (value: number, min: number, max: number) => {
  max = max === 0 ? Infinity : max
  return value >= min && value <= max
}

let defaultLogic = [
  {
    min_qty: 1,
    max_qty: 5,
    is_total_price: false,
    price: 1000,
    is_deleted: false
  },
  {
    min_qty: 6,
    max_qty: 0,
    is_total_price: true,
    price: 9000,
    is_deleted: false
  }
]