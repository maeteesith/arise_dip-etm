import * as moment from "moment";

export const getMoment = (date: any = null, format: any = null) => {
  if (date && format) {
    return moment(date, format);
  }
  if (date) {
    return moment(date);
  }
  return moment();
};

export const clearIdAndSaveId = (list: any) => {
  if (list) {
    list.forEach((item: any) => {
      item.id = 0;
      item.save_id = 0;
    });
    return list;
  }
  return list;
};

export const getParamsOverwrite = (params: any) => {
  if (params) {
    for (const [key, value] of Object.entries(params)) {
      params.id = null;
      params.eform_number = null;
      if (Array.isArray(params[key])) {
        params[key].forEach((item: any) => {
          for (const [key, value] of Object.entries(item)) {
            if (Array.isArray(item[key])) {
              item[key].forEach((itemIn: any) => {
                if (itemIn.id || itemIn.save_id) {
                  itemIn.id = null;
                  itemIn.save_id = null;
                }
              });
            } else if (key == "id" || key == "save_id") {
              item.id = null;
              item.save_id = null;
            }
          }
        });
      }
    }
    return params;
  }
  return params;
};
