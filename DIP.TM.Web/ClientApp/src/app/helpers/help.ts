import { GlobalService } from '../global.service'
import { Injectable } from '@angular/core'
import { isArray } from 'util'
import { CONSTANTS, clone } from "../helpers";

@Injectable({
    providedIn: 'root'
})
export class Help {
    constructor(
        private global: GlobalService,
    ) { }

    Distinct(item_list: any[], attr: string): any {
        var attr_list = {}

        if (item_list) {
            item_list.forEach((item: any) => {
                if (item[attr])
                    attr_list[item[attr]] = (attr_list[item[attr]] || 0) + 1
            })
        }

        return attr_list
    }

    CountDistinct(item_list: any[], attr: string): any {
        return Object.keys(this.Distinct(item_list, attr)).length
    }

    GetAddressInformation(address: any) {
        address.address_information =
            (address.house_number && address.house_number != "" ? address.house_number + " " : "") +
            (address.address_country_code == "TH" ? (
                (address.address_sub_district_name && address.address_sub_district_name != "" ? "ตำบล" + address.address_sub_district_name + " " : "") +
                (address.address_district_name && address.address_district_name != "" ? "อำเภอ" + address.address_district_name + " " : "") +
                (address.address_province_name && address.address_province_name != "" ? "จังหวัด" + address.address_province_name + " " : "") +
                (address.postal_code && address.postal_code != "" ? address.postal_code + " " : "")
            ) : "");
    }

    GetFilterParams(params: any, paging: any = clone(CONSTANTS.PAGINATION.INIT)): any {
        const result = {
            order_by: 'created_date',
            is_order_reverse: false,
            search_by: [],
            filter_queries: [],
            paging: {},
            page_index: paging && paging.page_index ? +paging.page_index : 1,
            item_per_page: paging && paging.item_per_page ? +paging.item_per_page : 10,
        }

        // value [0]: value
        // value [1]: operation


        //var filter_queries = this.item_list[index].search_key_index.map((item: any) => {
        //  return this.item_list[index].column_list[item] + ".Contains(\"" + this.item_list[index].value_display + "\")"
        //}).join(" OR ")
        ////this.item_list[index].search_key_index.forEach((item: any) => {
        //params.filter_queries.push(filter_queries)


        Object.keys(params).forEach((item: any) => {
            if (item.endsWith("_filter_queries")) {
                var filter_queries = params[item].column_list.map((column: any) => {
                    return column + ".Contains(\"" + params[item].value + "\")"
                }).join(" OR ")
                result.filter_queries.push(filter_queries)

            } else {
                var key = item.replace("end_date", "date").replace("start_date", "date")
                var value = (isArray(params[item]) && params[item].length == 2) ? params[item][0] : params[item]
                var operation =
                    (isArray(params[item]) && params[item].length == 2) ? params[item][1] : (
                        item.endsWith("_number") || item.endsWith("_code") || item.endsWith("_by") ? 0 : (
                            item.endsWith("start_date") ? 3 : (
                                item.endsWith("end_date") ? 4 : (
                                    item.endsWith("_date") ? 0 : (
                                        item == "id" || item.endsWith("_id") ? 0 : (
                                            item.endsWith("_index") ? 0 : 5
                                        )
                                    )
                                )
                            )
                        )
                    )

                if (operation == "LIKE") operation = 5
                else if (operation == "LIKE_FRONT") operation = 6
                else if (operation == "LIKE_BACK") operation = 7
                else if (operation == "WORD_SAME") operation = 0

                result.search_by.push({
                    key: key,
                    value: typeof value === "number" ? value.toString() : value,
                    operation: operation,
                })
            }
        })

        return result
    }

    GetArrayFromPaging(data: any[], paging: any): any[] {
        var result = []

        for (var i = (paging.page_index - 1) * paging.item_per_page; i < paging.page_index * paging.item_per_page && i < paging.item_total; i++) {
            result.push(data[i])
        }

        console.log(result)

        return result
    }

    PageSet(data: any, paging: any) {
        if (data && data.list && data.paging) {
            data.paging.item_start = Math.min(data.paging.item_total, (data.paging.page_index - 1) * data.paging.item_per_page + 1)
            data.paging.item_end = Math.max(data.paging.item_total, data.paging.page_index * data.paging.item_per_page)

            data.list.forEach((item: any, index) => {
                item.index = data.paging.item_start + index
            })

            data.paging.page_max = Math.ceil(data.paging.item_total / data.paging.item_per_page)

            this.Clone(data.paging, paging, false)
        }
    }

    Clone(from: any, to: any, is_deleted: boolean = true) {
        from = from || {}
        to = to || {}
        if (is_deleted) {
            Object.keys(to).forEach((item: any) => {
                delete to[item]
            })
        }
        Object.keys(from).forEach((item: any) => {
            to[item] = from[item]
        })
    }

    Clear(item: any) {
        Object.keys(item).forEach((key: any) => {
            delete item[key]
        })
    }

    Alert(message: string, toast: any = null) {
        toast = toast || CONSTANTS.TOAST.ERROR
        toast.message = message
        this.global.setToast(toast)
    }

    Open(url: string) {
        window.open(url)
    }

    Close(): void {
        close()
    }
}
