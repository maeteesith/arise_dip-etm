import { VALIDATE } from "./constants";

const isRequired = (value: any) => {
  if (value || value === 0 || value === false) {
    // return /^.+$/.test(value)
    return JSON.stringify(value).length > 0;
  }
};

const isRequiredOption = (value: any, option: any, input: any) => {
  if (option && option.name && option.value && input) {
    let isRequired = false;

    // Check condition value for validate
    option.value.forEach((item: any) => {
      if (item === input[option.name]) {
        isRequired = true;
      }
    });

    if (isRequired) {
      if (value || value === 0 || value === false) {
        return JSON.stringify(value).length > 0;
      }
    } else {
      return true;
    }
  }
};

const isFullName = (value: any) => {
  if (value || value === 0) {
    return /^[ก-๙]+(\s?[ก-๙]+)?(\s?[ก-๙]+)?$/.test(value);
  }
  return true;
};

const isNumber = (value: any) => {
  if (value || value === 0) {
    return /^[\d]*$/.test(value);
  }
};

const isNumberOption = (value: any, option: any, input: any) => {
  if (option && option.name && option.value && input) {
    let isRequired = false;

    // Check condition value for validate
    option.value.forEach((item: any) => {
      if (item === input[option.name]) {
        isRequired = true;
      }
    });

    if (isRequired) {
      if (value || value === 0) {
        return /^[\d]*$/.test(value);
      }
    } else {
      return true;
    }
  }
};

const isNumberRange = (value: any, option: any) => {
  if (value || value === 0) {
    if (option && option.min && option.max) {
      if (value >= option.min && value <= option.max) {
        return true;
      }
      return false;
    }
  }
};

const isCount = (value: any) => {
  if (value || value === 0) {
    return +value ? (+value > 0 ? true : false) : false;
  }
};

const isPhone = (value: any) => {
  if (value || value === 0) {
    return /^[0]{1}[\d]{8,11}$/.test(value);
  }
};

const isPhoneOption = (value: any, option: any, input: any) => {
  if (option && option.name && option.value && input) {
    let isRequired = false;

    // Check condition value for validate
    option.value.forEach((item: any) => {
      if (item === input[option.name]) {
        isRequired = true;
      }
    });

    if (isRequired) {
      if (value || value === 0) {
        return /^[0]{1}[\d]{8,11}$/.test(value);
      }
    } else {
      return true;
    }
  }
};

const isEmail = (value: any) => {
  if (value || value === 0) {
    return /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
      value
    );
  }
};

const isPostalCode = (value: any) => {
  if (value || value === 0) {
    return /^[\d]{5,6}$/.test(value);
  }
};

//!------>>> Validate Store <<<------!//
const valdiateStore = (
  select: string,
  param: any,
  options?: any,
  input?: any
) => {
  switch (select) {
    case "isRequired":
      return isRequired(param);
    case "isRequiredOption":
      return isRequiredOption(param, options, input);
    case "isFullName":
      return isFullName(param);
    case "isNumber":
      return isNumber(param);
    case "isNumberOption":
      return isNumberOption(param, options, input);
    case "isNumberRange":
      return isNumberRange(param, options);
    case "isCount":
      return isCount(param);
    case "isPhone":
      return isPhone(param);
    case "isPhoneOption":
      return isPhoneOption(param, options, input);
    case "isEmail":
      return isEmail(param);
    case "isPostalCode":
      return isPostalCode(param);
    default:
      return isRequired(param);
  }
};

//!------>>> Validate Service <<<------!//
export const validateService = (key: string, input: any) => {
  if (VALIDATE[key]) {
    let result = { isValid: true, validate: {} };
    VALIDATE[key].forEach((item: any) => {
      let validateList = item[1].split(" | ");
      let textList = item[2].split(" | ");
      let options = item[3] ? item[3] : {};
      for (let index = 0; index < validateList.length; index++) {
        if (
          !valdiateStore(validateList[index], input[item[0]], options, input)
        ) {
          result.isValid = false;
          result.validate[item[0]] = textList[index];
          console.warn(`${item[0]} is invalid (${validateList[index]}).`);
          break;
        }
      }
    });
    return result;
  }
  return { isValid: false, validate: {} };
};
