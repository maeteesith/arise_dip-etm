import * as Papa from "papaparse/papaparse.min.js";

export const PapaParseCsvToJson = (file: any, callback: any) => {
  if (file) {
    Papa.parse(file, {
      complete: callback
    });
  }
  return {};
};
