import { Injectable } from '@angular/core'
import { ApiService } from '../services/apiService'
//import * as moment from 'moment'
//import * as numeral from 'numeral'

@Injectable({
  providedIn: 'root'
})
export class AutoComplete {
  constructor(private apiService: ApiService) {
    this.Initial()
  }
  //public pthis: any

  //initial(pthis: any): void {
  //  this.pthis = pthis
  //}
  public item_list: any[]

  Initial(): void {
    this.item_list = []
  }

  Add(
    service_name: string,
    label_text: string,
    column_list: any[],
    search_key_index: number[],
    display_index: number,
    value_result: any,
    map_list: any[],
    function_after_select: any = null,

    item_per_page: number = 5,
    length: number = 0): void {

    this.item_list.push({
      service_name: service_name,
      label_text: label_text,
      column_list: column_list,
      search_key_index: search_key_index,
      display_index: display_index,
      value_result: value_result,
      map_list: map_list,
      function_after_select: function_after_select,
      item_per_page: item_per_page,
      length: length,
      result_list: [],
      result_01_list: [],
      result_02_list: [],
      result_03_list: [],
      result_10_list: [],
      is_table_show: false,
      is_loading: false,
    })
  }

  Blur(index: number, row_item: any = null): void {
    let pThis = this
    setTimeout(function () {
      if (row_item) {
        row_item.is_table_show = false
      }
      pThis.item_list[index].is_table_show = false
    }, 200)
  }

  Click(index: number, row_item: any = null): void {
    //console.log(row_item)
    if (row_item) {
      //console.log(this.item_list)
      if (this.item_list[index].length == 0 ||
        (row_item[this.item_list[index].column_list[this.item_list[index].display_index]] &&
          row_item[this.item_list[index].column_list[this.item_list[index].display_index]].length >= this.item_list[index].length)) {

        //console.log("Click")
        //console.log(row_item)
        row_item.is_table_show = true
        this.item_list[index].is_table_show = true

        //console.log(this.item_list[index].function_after_select)
        row_item.function_after_select = this.item_list[index].function_after_select
        //console.log(index)
        //console.log(this.item_list)
        //console.log(this.item_list[index].column_list)
        //console.log(this.item_list[index].column_list)
        row_item.search_variable = this.item_list[index].column_list[this.item_list[index].display_index]
        //console.log(row_item.search_variable)
        //console.log(row_item[row_item.search_variable])
        //row_item.search_word = row_item[row_item.search_variable]
        //console.log(row_item.search_variable)
        row_item.map_list = this.item_list[index].map_list
        //row_item.display_variable = this.item_list[index].column_list[this.item_list[index].se]

        row_item.search_word = row_item[this.item_list[index].map_list[0][0]]
        //console.log(this.item_list[index].map_list)
        //console.log(row_item.search_word)

        this.Call(index, row_item)
      }
    } else {
      console.log("Click")
      if (this.item_list[index].length == 0 ||
        (this.item_list[index].value_display && this.item_list[index].value_display.length >= this.item_list[index].length)) {
        //console.log("Click")
        //  object[name] = object[name] || ""

        this.item_list[index].is_table_show = true

        this.Call(index, row_item)
      }
    }
  }

  Call(index: number, row_item: any = null): void {
    if (row_item) {
      if (row_item.is_loading) return;
      row_item.is_loading = true

      this.item_list[index].result_list = []
      this.item_list[index].result_01_list = []
      this.item_list[index].result_02_list = []
      this.item_list[index].result_03_list = []
      this.item_list[index].result_10_list = [[], [], [], [], [], [], [], [], [], []]
      const params = {
        item_per_page: this.item_list[index].item_per_page,
        //order_by: 'name',
        //is_order_reverse: false,
        search_by: [],
        filter_queries: [],
      }

      if (row_item.search_word) {
        if (this.item_list[index].search_key_index.length == 1) {
          params.search_by.push({
            key: this.item_list[index].column_list[this.item_list[index].search_key_index[0]],
            value: row_item.search_word,
            operation: 5
          })
        } else {
          var filter_queries = this.item_list[index].search_key_index.map((item: any) => {
            return this.item_list[index].column_list[item] + ".Contains(\"" + row_item.search_word + "\")"
          }).join(" OR ")
          params.filter_queries.push(filter_queries)
        }
      }

      this.apiService.post(this.item_list[index].service_name, params).subscribe((data: any) => {
        row_item.is_loading = false

        if (data && data.list) {
          if (data.list.length == 1) {
            this.Select(data.list[0], row_item)
          } else if (data.list.length == 0) {
            row_item.is_table_show = false
            this.item_list[index].is_table_show = false
          } else {
            this.item_list[index].result_list = data.list

            this.item_list[index].result_01_list = []
            this.item_list[index].result_02_list = []
            this.item_list[index].result_03_list = []
            this.item_list[index].result_10_list = [[], [], [], [], [], [], [], [], [], []]
            var pthis = this
            data.list.forEach(function (item, _index) {
              if (_index % 3 == 0) {
                pthis.item_list[index].result_01_list.push(item)
              } else if (_index % 3 == 1) {
                pthis.item_list[index].result_02_list.push(item)
              } else if (_index % 3 == 2) {
                pthis.item_list[index].result_03_list.push(item)
              }
              pthis.item_list[index].result_10_list[_index % 10].push(item)
            })

            console.log(pthis.item_list[index].result_10_list)
            //console.log(this.item_list[index].result_list)
            //console.log(this.item_list[index].column_list)
          }
        }
      })
    } else {
      if (this.item_list[index].is_loading) return;
      this.item_list[index].is_loading = true

      this.item_list[index].result_list = []
      this.item_list[index].result_01_list = []
      this.item_list[index].result_02_list = []
      this.item_list[index].result_03_list = []
      this.item_list[index].result_10_list = [[], [], [], [], [], [], [], [], [], []]
      const params = {
        item_per_page: this.item_list[index].item_per_page,
        //order_by: 'name',
        //is_order_reverse: false,
        search_by: [],
        filter_queries: [],
      }

      if (this.item_list[index].value_display) {
        if (this.item_list[index].search_key_index.length == 1) {
          params.search_by.push({
            key: this.item_list[index].column_list[this.item_list[index].search_key_index[0]],
            value: this.item_list[index].value_display,
            operation: 5
          })
        } else {
          var filter_queries = this.item_list[index].search_key_index.map((item: any) => {
            return this.item_list[index].column_list[item] + ".Contains(\"" + this.item_list[index].value_display + "\")"
          }).join(" OR ")
          //this.item_list[index].search_key_index.forEach((item: any) => {
          params.filter_queries.push(filter_queries)
          //params.filter_queries.push({
          //  key: this.item_list[index].column_list[[0]],
          //  value: this.item_list[index].value_display,
          //  operation: 5
          //})
          //})
        }
      }

      //if (this.input.is_autocomplete_RegistrarList_load) return
      //this.input.is_autocomplete_RegistrarList_load = true
      this.apiService.post(this.item_list[index].service_name, params).subscribe((data: any) => {
        this.item_list[index].is_loading = false

        if (data && data.list) {
          if (data.list.length == 1) {
            this.Select(data.list[0], row_item)
          } else if (data.list.length == 0) {
            this.item_list[index].is_table_show = false
          } else {
            this.item_list[index].result_list = data.list

            this.item_list[index].result_01_list = []
            this.item_list[index].result_02_list = []
            this.item_list[index].result_03_list = []
            this.item_list[index].result_10_list = [[], [], [], [], [], [], [], [], [], []]
            var pthis = this
            data.list.forEach(function (item, _index) {
              if (_index % 3 == 0) {
                pthis.item_list[index].result_01_list.push(item)
              } else if (_index % 3 == 1) {
                pthis.item_list[index].result_02_list.push(item)
              } else if (_index % 3 == 2) {
                pthis.item_list[index].result_03_list.push(item)
              }

              pthis.item_list[index].result_10_list[_index % 10].push(item)
            })

            console.log(pthis.item_list[index].result_10_list)
          }
        }
      })
    }
  }

  Select(result: any, row_item: any = null): void {
    if (row_item) {
      console.log("select 1")

      row_item.is_table_show = false
      this.item_list.filter(r => r.is_table_show).forEach((item: any) => { item.is_table_show = false })

      //row_item.is_loading

      //item.value_display = result[item.column_list[item.display_index]]
      row_item[row_item.search_variable] = result[row_item.search_variable]

      row_item.map_list.forEach((r: any) => {
        row_item[r[0]] = result[r[1]]
      })

      if (row_item.function_after_select) {
        row_item.function_after_select(row_item)
      }
    } else {
      console.log("select 2")

      var item = this.item_list.filter(r => r.is_table_show)[0]
      item.is_table_show = false

      item.value_display = result[item.column_list[item.display_index]]

      item.map_list.forEach((r: any) => {
        item.value_result[r[0]] = result[r[1]]
      })

      if (item.function_after_select) {
        item.function_after_select(item)
      }
    }
  }

  Update(pthis: any): void {
    this.item_list.forEach((item: any) => {
      //console.log(pthis)
      //console.log(item)
      //console.log(item.map_list.filter(r => r[1] == item.column_list[item.display_index])[0][0])

      Object.keys(pthis).forEach((key: any) => {
        item.value_result[key] = pthis[key]
      })
      //item.map_list.forEach((r: any) => {
      //console.log(item)
      item.value_display = item.value_result[item.map_list.filter(r => r[1] == item.column_list[item.display_index])[0][0]]
      //item.value_display = "test"
      //console.log(item)
      //})
    })
  }
}
