import * as moment from "moment";
import * as numeral from "numeral";

export const displayDate = (date: any) => {
  if (date) {
    return moment(date).format("DD-MM-YYYY");
  }
  return "";
};

export const displayDateEform = (date: any) => {
  if (date) {
    return moment(date).format("DD/MM/YYYY");
  }
  return "";
};

export const displayDateServer = (date: any) => {
  if (date) {
    return moment(date).format("YYYY-MM-DD");
  }
  return "";
};

export const loopDisplayDateServer = (list: any, name: any) => {
  if (list && name) {
    list.forEach((item: any) => {
      if (item[name]) {
        item[name] = moment(item[name]).format("YYYY-MM-DD");
      }
    });
    return list;
  }
  return list;
};

export const displayMoney = (value: any) => {
  if (value) {
    return numeral(value).format("0,0[.]00");
  } else {
    return 0;
  }
};

export const displayString = (value: any) => {
  if (value) {
    return `${value}`;
  } else {
    return "";
  }
};

export const displayFormatBytes = (bytes: any, decimals: any = 2) => {
  if (bytes && bytes !== 0) {
    const k = 1024;
    const dm = decimals < 0 ? 0 : decimals;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    const i = Math.floor(Math.log(bytes) / Math.log(k));

    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + " " + sizes[i];
  }
  return "0 MB";
};

export const displayAddress = (item: any) => {
  if (item) {
    return `${item.house_number ? item.house_number + " " : ""}${
      item.village_number ? "หมู่ที่ " + item.village_number + " " : ""
    }${item.alley ? "ซอย " + item.alley + " " : ""}${
      item.street ? "ถนน " + item.street + " " : ""
    }${
      item.address_sub_district_name
        ? "ตำบล " + item.address_sub_district_name + " "
        : ""
    }${
      item.address_district_name
        ? "อำเภอ " + item.address_district_name + " "
        : ""
    }${
      item.address_province_name
        ? "จังหวัด " + item.address_province_name + " "
        : ""
    }${item.postal_code ? "รหัสไปรษณีย์ " + item.postal_code + " " : ""}`;
  } else {
    return "";
  }
};

export const displayPathCode = (value: any) => {
  if (value) {
    if (!value.startsWith("0")) {
      return `0${value}`;
    }
  }
  return value;
};

export const displayLabelReferenceMaster = (list: any, code: any) => {
  if (list && code) {
    const result = list.find((item: any) => {
      return item.code === code;
    });
    return result.name ? result.name : "";
  }
  return "";
};
