import { SECRET_KEY } from './constants'
import * as CryptoJS from 'crypto-js'

export const encrypt = (value: any) => {
  if(value) {
    return CryptoJS.AES.encrypt(JSON.stringify(value), SECRET_KEY).toString()
  }
}

export const decrypt = (value: any) => {
  if(value) {
    let bytes  = CryptoJS.AES.decrypt(value, SECRET_KEY)
    return JSON.parse(bytes.toString(CryptoJS.enc.Utf8))
  }
}