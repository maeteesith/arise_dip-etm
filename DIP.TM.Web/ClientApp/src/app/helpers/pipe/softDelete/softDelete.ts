import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "softDelete",
  pure: false,
})
export class SoftDeletePipe implements PipeTransform {
  transform(items: any[]): any {
    if (!items) {
      return items;
    }
    return items.filter((item) => !item.is_deleted);
  }
}
