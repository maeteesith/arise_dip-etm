import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
    MyFilterPipe
} from "./myfilter/myfilter";
import {
    MyPagingPipe
} from "./mypaging/mypaging";
import {
    SoftDeletePipe
} from "./softDelete/softDelete";

@NgModule({
    imports: [CommonModule],
    declarations: [
        MyFilterPipe,
        MyPagingPipe,
        SoftDeletePipe,
    ],
    exports: [
        MyFilterPipe,
        MyPagingPipe,
        SoftDeletePipe,
    ]
})
export class PipeModule { }
