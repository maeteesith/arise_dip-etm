// export const ROUTE_PATH = {
//   DASHBOARD: {
//     TEXT: 'dashboard',
//     ROUTE: 'dashboard',
//     LINK: '/dashboard'
//   },

//   REGISTRATION_REQUEST: {
//     TEXT: 'Registration Request',
//     ROUTE: 'registration-request',
//     LINK: '/registration-request'
//   },
//   REGISTRATION_REQUEST_ADD: {
//     TEXT: 'Registration Request',
//     ROUTE: 'registration-request/add',
//     LINK: '/registration-request/add'
//   },
//   REQUEST_01_ITEM_LIST: {
//     TEXT: 'Request 01 Item|List',
//     ROUTE: 'request-01-item/list',
//     LINK: '/request-01-item/list'
//   },
//   REGISTRATION_REQUEST_EDIT: {
//     TEXT: 'Registration Request',
//     ROUTE: 'registration-request/edit',
//     LINK: '/registration-request/edit'
//   },
//   DAILY_FEE_INCOME_RECORDS: {
//     TEXT: 'daily-fee-income-records',
//     ROUTE: 'daily-fee-income-records',
//     LINK: '/daily-fee-income-records'
//   },
//   REPRESENTATIVE_MANAGEMENT: {
//     TEXT: 'Representative Management',
//     ROUTE: 'representative-management',
//     LINK: '/representative-management'
//   },
//   REGISTRATION_OTHER_REQUEST: {
//     TEXT: 'Registration Other Request',
//     ROUTE: 'registration-other-request',
//     LINK: '/registration-other-request'
//   },
//   REGISTRATION_OTHER_REQUEST_ADD: {
//     TEXT: 'Registration Other Request|Add',
//     ROUTE: 'registration-other-request/add',
//     LINK: '/registration-other-request/add'
//   },
//   REGISTRATION_OTHER_REQUEST_EDIT: {
//     TEXT: 'Registration Other Request|Edit',
//     ROUTE: 'registration-other-request/edit',
//     LINK: '/registration-other-request/edit'
//   },
//   REQUEST_OTHER_ITEM_LIST: {
//     TEXT: 'Request Other Item|List',
//     ROUTE: 'request-other-item/list',
//     LINK: '/request-other-item/list'
//   },
//   TEST_LOGIN: {
//     TEXT: 'Test Login',
//     ROUTE: 'test-login',
//     LINK: '/test-login'
//   },
//   RECEIPT_DAILY: {
//     TEXT: 'Receipt Daily',
//     ROUTE: 'receipt-daily',
//     LINK: '/receipt-daily'
//   },
//   RECEIPT_DAILY_EDIT: {
//     TEXT: 'Receipt Daily|Edit',
//     ROUTE: 'receipt-daily/edit',
//     LINK: '/receipt-daily/edit'
//   },
//   SAVE_01_PROCESS_LIST: {
//     TEXT: 'Save 01 Process|List',
//     ROUTE: 'save-01-process/list',
//     LINK: '/save-01-process/list'
//   },
//   SAVE_01_PROCESS: {
//     TEXT: 'Save 01 Process',
//     ROUTE: 'save-01-process',
//     LINK: '/save-01-process'
//   },
//   SAVE_01_PROCESS_ADD: {
//     TEXT: 'Save 01 Process|Add',
//     ROUTE: 'save-01-process/add',
//     LINK: '/save-01-process/add'
//   },
//   SAVE_01_PROCESS_EDIT: {
//     TEXT: 'Save 01 Process|Edit',
//     ROUTE: 'save-01-process/edit',
//     LINK: '/save-01-process/edit'
//   },
//   SAVE_020_PROCESS: {
//     TEXT: 'Save 020 Process',
//     ROUTE: 'save-020-process',
//     LINK: '/save-020-process'
//   },
//   SAVE_020_PROCESS_ADD: {
//     TEXT: 'Save 020 Process|Add',
//     ROUTE: 'save-020-process/add',
//     LINK: '/save-020-process/add'
//   },
//   SAVE_020_PROCESS_EDIT: {
//     TEXT: 'Save 020 Process|Edit',
//     ROUTE: 'save-020-process/edit',
//     LINK: '/save-020-process/edit'
//   },
//   SAVE_020_PROCESS_LIST: {
//     TEXT: 'Save 020 Process|List',
//     ROUTE: 'save-020-process/list',
//     LINK: '/save-020-process/list'
//   },
//   SAVE_021_PROCESS: {
//     TEXT: 'Save 021 Process',
//     ROUTE: 'save-021-process',
//     LINK: '/save-021-process'
//   },
//   SAVE_021_PROCESS_ADD: {
//     TEXT: 'Save 021 Process|Add',
//     ROUTE: 'save-021-process/add',
//     LINK: '/save-021-process/add'
//   },
//   SAVE_021_PROCESS_EDIT: {
//     TEXT: 'Save 021 Process|Edit',
//     ROUTE: 'save-021-process/edit',
//     LINK: '/save-021-process/edit'
//   },
//   SAVE_021_PROCESS_LIST: {
//     TEXT: 'Save 021 Process|List',
//     ROUTE: 'save-021-process/list',
//     LINK: '/save-021-process/list'
//   },
//   SAVE_03_PROCESS: {
//     TEXT: 'Save 03 Process',
//     ROUTE: 'save-03-process',
//     LINK: '/save-03-process'
//   },
//   SAVE_03_PROCESS_LIST: {
//     TEXT: 'Save 03 Process|List',
//     ROUTE: 'save-03-process/list',
//     LINK: '/save-03-process/list'
//   },
//   SAVE_04_PROCESS_LIST: {
//     TEXT: 'Save 04 Process|List',
//     ROUTE: 'save-04-process/list',
//     LINK: '/save-04-process/list'
//   },
//   SAVE_05_PROCESS_LIST: {
//     TEXT: 'Save 05 Process|List',
//     ROUTE: 'save-05-process/list',
//     LINK: '/save-05-process/list'
//   },
//   SAVE_06_PROCESS: {
//     TEXT: 'Save 06 Process',
//     ROUTE: 'save-06-process',
//     LINK: '/save-06-process'
//   },
//   SAVE_06_PROCESS_LIST: {
//     TEXT: 'Save 06 Process|List',
//     ROUTE: 'save-06-process/list',
//     LINK: '/save-06-process/list'
//   },
//   SAVE_07_PROCESS_LIST: {
//     TEXT: 'Save 07 Process|List',
//     ROUTE: 'save-07-process/list',
//     LINK: '/save-07-process/list'
//   },
//   SAVE_08_PROCESS_LIST: {
//     TEXT: 'Save 08 Process|List',
//     ROUTE: 'save-08-process/list',
//     LINK: '/save-08-process/list'
//   },
//   REQUEST_SEARCH: {
//     TEXT: 'Request Search',
//     ROUTE: 'request-search',
//     LINK: '/request-search'
//   },
//   REQUEST_SEARCH_LOAD: {
//     TEXT: 'Request Search|Load',
//     ROUTE: 'request-search/load',
//     LINK: '/request-search/load'
//   },
//   REQUEST_DOCUMENT_COLLECT: {
//     TEXT: 'Request Document Collect',
//     ROUTE: 'request-document-collect',
//     LINK: '/request-document-collect'
//   },
//   ALL_TASK: {
//     TEXT: 'All Task',
//     ROUTE: 'all-task',
//     LINK: '/all-task'
//   },
//   DOCUMENT_PROCESS_CLASSIFICATION: {
//     TEXT: 'Document Process Classification',
//     ROUTE: 'document-process-classification',
//     LINK: '/document-process-classification'
//   },
//   DOCUMENT_PROCESS_CLASSIFICATION_DO: {
//     TEXT: 'Document Process Classification Do',
//     ROUTE: 'document-process-classification-do',
//     LINK: '/document-process-classification-do'
//   },
//   DOCUMENT_PROCESS_CLASSIFICATION_DO_EDIT: {
//     TEXT: 'Document Process Classification Do|Edit',
//     ROUTE: 'document-process-classification-do/edit',
//     LINK: '/document-process-classification-do/edit'
//   },
//   SAVE_04_PROCESS: {
//     TEXT: 'Save 04 Process',
//     ROUTE: 'save-04-process',
//     LINK: '/save-04-process'
//   },
//   SAVE_04_PROCESS_ADD: {
//     TEXT: 'Save 04 Process|Add',
//     ROUTE: 'save-04-process/add',
//     LINK: '/save-04-process/add'
//   },
//   SAVE_04_PROCESS_EDIT: {
//     TEXT: 'Save 04 Process|Edit',
//     ROUTE: 'save-04-process/edit',
//     LINK: '/save-04-process/edit'
//   },
//   SAVE_05_PROCESS: {
//     TEXT: 'Save 05 Process',
//     ROUTE: 'save-05-process',
//     LINK: '/save-05-process'
//   },
//   SAVE_05_PROCESS_ADD: {
//     TEXT: 'Save 05 Process|Add',
//     ROUTE: 'save-05-process/add',
//     LINK: '/save-05-process/add'
//   },
//   SAVE_05_PROCESS_EDIT: {
//     TEXT: 'Save 05 Process|Edit',
//     ROUTE: 'save-05-process/edit',
//     LINK: '/save-05-process/edit'
//   },
//   SAVE_07_PROCESS: {
//     TEXT: 'Save 07 Process',
//     ROUTE: 'save-07-process',
//     LINK: '/save-07-process'
//   },
//   SAVE_07_PROCESS_ADD: {
//     TEXT: 'Save 07 Process|Add',
//     ROUTE: 'save-07-process/add',
//     LINK: '/save-07-process/add'
//   },
//   SAVE_07_PROCESS_EDIT: {
//     TEXT: 'Save 07 Process|Edit',
//     ROUTE: 'save-07-process/edit',
//     LINK: '/save-07-process/edit'
//   },
//   SAVE_08_PROCESS: {
//     TEXT: 'Save 08 Process',
//     ROUTE: 'save-08-process',
//     LINK: '/save-08-process'
//   },
//   SAVE_08_PROCESS_ADD: {
//     TEXT: 'Save 08 Process|Add',
//     ROUTE: 'save-08-process/add',
//     LINK: '/save-08-process/add'
//   },
//   SAVE_08_PROCESS_EDIT: {
//     TEXT: 'Save 08 Process|Edit',
//     ROUTE: 'save-08-process/edit',
//     LINK: '/save-08-process/edit'
//   },
//   SAVE_140_PROCESS: {
//     TEXT: 'Save 140 Process',
//     ROUTE: 'save-140-process',
//     LINK: '/save-140-process'
//   },
//   SAVE_140_PROCESS_ADD: {
//     TEXT: 'Save 140 Process|Add',
//     ROUTE: 'save-140-process/add',
//     LINK: '/save-140-process/add'
//   },
//   SAVE_140_PROCESS_EDIT: {
//     TEXT: 'Save 140 Process|Edit',
//     ROUTE: 'save-140-process/edit',
//     LINK: '/save-140-process/edit'
//   },
//   SAVE_140_PROCESS_LIST: {
//     TEXT: 'Save 140 Process|List',
//     ROUTE: 'save-140-process/list',
//     LINK: '/save-140-process/list'
//   },
//   CHECKING_SIMILAR_IMAGE: {
//     TEXT: 'Checking Similar Image',
//     ROUTE: 'checking-similar-image',
//     LINK: '/checking-similar-image'
//   },
//   CHECKING_ITEM_LIST: {
//     TEXT: 'Checking Item|List',
//     ROUTE: 'checking-item/list',
//     LINK: '/checking-item/list'
//   },
//   CHECKING_SIMILAR_LIST: {
//     TEXT: 'Checking Similar|List',
//     ROUTE: 'checking-similar/list',
//     LINK: '/checking-similar/list'
//   },
//   CHECKING_SIMILAR: {
//     TEXT: 'Checking Similar',
//     ROUTE: 'checking-similar',
//     LINK: '/checking-similar'
//   },
//   CHECKING_SIMILAR_ADD: {
//     TEXT: 'Checking Similar|Add',
//     ROUTE: 'checking-similar/add',
//     LINK: '/checking-similar/add'
//   },
//   CHECKING_SIMILAR_EDIT: {
//     TEXT: 'Checking Similar|Edit',
//     ROUTE: 'checking-similar/edit',
//     LINK: '/checking-similar/edit'
//   },
//   CHECKING_SIMILAR_WORD: {
//     TEXT: 'Checking Similar Word',
//     ROUTE: 'checking-similar-word',
//     LINK: '/checking-similar-word'
//   },
//   CHECKING_SIMILAR_WORD_ADD: {
//     TEXT: 'Checking Similar Word|Add',
//     ROUTE: 'checking-similar-word/add',
//     LINK: '/checking-similar-word/add'
//   },
//   CHECKING_SIMILAR_WORD_EDIT: {
//     TEXT: 'Checking Similar Word|Edit',
//     ROUTE: 'checking-similar-word/edit',
//     LINK: '/checking-similar-word/edit'
//   },
//   CHECKING_SIMILAR_RESULT: {
//     TEXT: 'Checking Similar Result',
//     ROUTE: 'checking-similar-result',
//     LINK: '/checking-similar-result'
//   },
//   CHECKING_SIMILAR_RESULT_ADD: {
//     TEXT: 'Checking Similar Result|Add',
//     ROUTE: 'checking-similar-result/add',
//     LINK: '/checking-similar-result/add'
//   },
//   CHECKING_SIMILAR_RESULT_EDIT: {
//     TEXT: 'Checking Similar Result|Edit',
//     ROUTE: 'checking-similar-result/edit',
//     LINK: '/checking-similar-result/edit'
//   },
//   CONSIDERING_SIMILAR_LIST: {
//     TEXT: 'Considering Similar|List',
//     ROUTE: 'considering-similar/list',
//     LINK: '/considering-similar/list'
//   },
//   CONSIDERING_SIMILAR: {
//     TEXT: 'Considering Similar',
//     ROUTE: 'considering-similar',
//     LINK: '/considering-similar'
//   },
//   CONSIDERING_SIMILAR_ADD: {
//     TEXT: 'Considering Similar|Add',
//     ROUTE: 'considering-similar/add',
//     LINK: '/considering-similar/add'
//   },
//   CONSIDERING_SIMILAR_EDIT: {
//     TEXT: 'Considering Similar|Edit',
//     ROUTE: 'considering-similar/edit',
//     LINK: '/considering-similar/edit'
//   },
//   PUBLIC_DASHBOARD_ROLE_01: {
//     TEXT: 'Public Dashboard Role 01',
//     ROUTE: 'public-dashboard-role-01',
//     LINK: '/public-dashboard-role-01'
//   },
//   EFORM_SAVE_01_PROCESS: {
//     TEXT: 'eForm Save 01 Process',
//     ROUTE: 'eform-save-01-process',
//     LINK: '/eform-save-01-process'
//   },
//   EFORM_SAVE_02_PROCESS: {
//     TEXT: 'eForm Save 02 Process',
//     ROUTE: 'eform-save-02-process',
//     LINK: '/eform-save-02-process'
//   },
//   EFORM_HOME: {
//     TEXT: 'eForm Home',
//     ROUTE: 'eform-home',
//     LINK: '/eform-home'
//   },
//   EFORM_SAVE_01_PROCESS_ADD: {
//     TEXT: 'eForm Save 01 Process|Add',
//     ROUTE: 'eform-save-01-process/add',
//     LINK: '/eform-save-01-process/add'
//   },
//   EFORM_SAVE_01_PROCESS_EDIT: {
//     TEXT: 'eForm Save 01 Process|Edit',
//     ROUTE: 'eform-save-01-process/edit',
//     LINK: '/eform-save-01-process/edit'
//   },
//   EFORM_BRAND_TYPE: {
//     TEXT: 'eForm Brand Type',
//     ROUTE: 'eform/brand-type',
//     LINK: '/eform/brand-type'
//   },
//   EFORM_OWNER_AND_AGENT: {
//     TEXT: 'eForm Owner',
//     ROUTE: 'eform/owner-and-agent',
//     LINK: '/eform/owner-and-agent'
//   },
//   DOCUMENT_ITEM_LIST: {
//     TEXT: 'Document Item|List',
//     ROUTE: 'document-item/list',
//     LINK: '/document-item/list'
//   },
//   CONSIDERING_SIMILAR_INSTRUCTION: {
//     TEXT: 'Considering Similar Instruction',
//     ROUTE: 'considering-similar-instruction',
//     LINK: '/considering-similar-instruction'
//   },
//   CONSIDERING_SIMILAR_INSTRUCTION_ADD: {
//     TEXT: 'Considering Similar Instruction|Add',
//     ROUTE: 'considering-similar-instruction/add',
//     LINK: '/considering-similar-instruction/add'
//   },
//   CONSIDERING_SIMILAR_INSTRUCTION_EDIT: {
//     TEXT: 'Considering Similar Instruction|Edit',
//     ROUTE: 'considering-similar-instruction/edit',
//     LINK: '/considering-similar-instruction/edit'

//   },
//   CONSIDERING_SIMILAR_DOCUMENT: {
//     TEXT: 'Considering Similar Document',
//     ROUTE: 'considering-similar-document',
//     LINK: '/considering-similar-document'
//   },
//   CONSIDERING_SIMILAR_DOCUMENT_ADD: {
//     TEXT: 'Considering Similar Document|Add',
//     ROUTE: 'considering-similar-document/add',
//     LINK: '/considering-similar-document/add'
//   },
//   CONSIDERING_SIMILAR_DOCUMENT_EDIT: {
//     TEXT: 'Considering Similar Document|Edit',
//     ROUTE: 'considering-similar-document/edit',
//     LINK: '/considering-similar-document/edit'
//   },
//   PUBLIC_ITEM_LIST: {
//     TEXT: 'Public Item|List',
//     ROUTE: 'public-item/list',
//     LINK: '/public-item/list'
//   },
//   PUBLIC_ROLE01_ITEM_LIST: {
//     TEXT: 'Public Role01 Item|List',
//     ROUTE: 'public-role01-item/list',
//     LINK: '/public-role01-item/list'
//   },
//   PUBLIC_ROLE01_CHECK_LIST: {
//     TEXT: 'Public Role01 Check|List',
//     ROUTE: 'public-role01-check/list',
//     LINK: '/public-role01-check/list'
//   },
//   PUBLIC_ROLE01_CHECK_DO: {
//     TEXT: 'Public Role01 Check Do',
//     ROUTE: 'public-role01-check-do',
//     LINK: '/public-role01-check-do'
//   },
//   PUBLIC_ROLE01_CHECK_DO_ADD: {
//     TEXT: 'Public Role01 Check Do|Add',
//     ROUTE: 'public-role01-check-do/add',
//     LINK: '/public-role01-check-do/add'
//   },
//   PUBLIC_ROLE01_CHECK_DO_EDIT: {
//     TEXT: 'Public Role01 Check Do|Edit',
//     ROUTE: 'public-role01-check-do/edit',
//     LINK: '/public-role01-check-do/edit'
//   },
//   PUBLIC_ROLE01_PREPARE_LIST: {
//     TEXT: 'Public Role01 Prepare|List',
//     ROUTE: 'public-role01-prepare/list',
//     LINK: '/public-role01-prepare/list'
//   },
//   PUBLIC_ROLE01_DOING_LIST: {
//     TEXT: 'Public Role01 Doing|List',
//     ROUTE: 'public-role01-doing/list',
//     LINK: '/public-role01-doing/list'
//   },
//   PUBLIC_ROLE02_CHECK_LIST: {
//     TEXT: 'Public Role02 Check|List',
//     ROUTE: 'public-role02-check/list',
//     LINK: '/public-role02-check/list'
//   },
//   PUBLIC_ROLE02_CONSIDER_PAYMENT_LIST: {
//     TEXT: 'Public Role02 Consider Payment|List',
//     ROUTE: 'public-role02-consider-payment/list',
//     LINK: '/public-role02-consider-payment/list'
//   },
//   PUBLIC_ROLE05_CONSIDER_PAYMENT_LIST: {
//     TEXT: 'Public Role05 Consider Payment|List',
//     ROUTE: 'public-role05-consider-payment/list',
//     LINK: '/public-role05-consider-payment/list'
//   },
//   PUBLIC_ROLE02_DOCUMENT_PAYMENT_LIST: {
//     TEXT: 'Public Role02 Document Payment|List',
//     ROUTE: 'public-role02-document-payment/list',
//     LINK: '/public-role02-document-payment/list'
//   },
//   PUBLIC_ROLE02_DOCUMENT_POST_LIST: {
//     TEXT: 'Public Role02 Document Post|List',
//     ROUTE: 'public-role02-document-post/list',
//     LINK: '/public-role02-document-post/list'
//   },
//   PUBLIC_ROLE02_ACTION_POST_LIST: {
//     TEXT: 'Public Role02 Action Post|List',
//     ROUTE: 'public-role02-action-post/list',
//     LINK: '/public-role02-action-post/list'
//   },
//   PUBLIC_ROLE04_ITEM_LIST: {
//     TEXT: 'Public Role04 item|List',
//     ROUTE: 'public-role04-item/list',
//     LINK: '/public-role04-item/list'
//   },
//   PUBLIC_ROLE04_CHECK_LIST: {
//     TEXT: 'Public Role04 Check|List',
//     ROUTE: 'public-role04-check/list',
//     LINK: '/public-role04-check/list'
//   },
//   PUBLIC_ROLE05_DOCUMENT_LIST: {
//     TEXT: 'Public Role05 Document|List',
//     ROUTE: 'public-role05-document/list',
//     LINK: '/public-role05-document/list'
//   },
//   PUBLIC_ROLE04_DOCUMENT_LIST: {
//     TEXT: 'Public Role04 Document|List',
//     ROUTE: 'public-role04-document/list',
//     LINK: '/public-role04-document/list'
//   },
//   PUBLIC_ROLE02_DOCUMENT_LIST: {
//     TEXT: 'Public Role02 Document|List',
//     ROUTE: 'public-role02-document/list',
//     LINK: '/public-role02-document/list'
//   },
//   DOCUMENT_ROLE02_ITEM_LIST: {
//     TEXT: 'Document Role02 Item|List',
//     ROUTE: 'document-role02-item/list',
//     LINK: '/document-role02-item/list'
//   },
//   EFORM_SAVE_140_PROCESS: {
//     TEXT: 'eForm Save 140 Process',
//     ROUTE: 'eform-save-140-process',
//     LINK: '/eform-save-140-process'
//   },
//   EFORM_SAVE_140_PROCESS_ADD: {
//     TEXT: 'eForm Save 140 Process|Add',
//     ROUTE: 'eform-save-140-process/add',
//     LINK: '/eform-save-140-process/add'
//   },
//   EFORM_SAVE_140_PROCESS_EDIT: {
//     TEXT: 'eForm Save 140 Process|Edit',
//     ROUTE: 'eform-save-140-process/edit',
//     LINK: '/eform-save-140-process/edit'
//   },
//   EFORM_SAVE_150_PROCESS: {
//     TEXT: 'eForm Save 150 Process',
//     ROUTE: 'eform-save-150-process',
//     LINK: '/eform-save-150-process'
//   },
//   EFORM_SAVE_150_PROCESS_ADD: {
//     TEXT: 'eForm Save 150 Process|Add',
//     ROUTE: 'eform-save-150-process/add',
//     LINK: '/eform-save-150-process/add'
//   },
//   EFORM_SAVE_150_PROCESS_EDIT: {
//     TEXT: 'eForm Save 150 Process|Edit',
//     ROUTE: 'eform-save-150-process/edit',
//     LINK: '/eform-save-150-process/edit'
//   },
//   EFORM_SAVE_190_PROCESS: {
//     TEXT: 'eForm Save 190 Process',
//     ROUTE: 'eform-save-190-process',
//     LINK: '/eform-save-190-process'
//   },
//   EFORM_SAVE_190_PROCESS_ADD: {
//     TEXT: 'eForm Save 190 Process|Add',
//     ROUTE: 'eform-save-190-process/add',
//     LINK: '/eform-save-190-process/add'
//   },
//   EFORM_SAVE_190_PROCESS_EDIT: {
//     TEXT: 'eForm Save 190 Process|Edit',
//     ROUTE: 'eform-save-190-process/edit',
//     LINK: '/eform-save-190-process/edit'
//   },
//   EFORM_SAVE_200_PROCESS: {
//     TEXT: 'eForm Save 200 Process',
//     ROUTE: 'eform-save-200-process',
//     LINK: '/eform-save-200-process'
//   },
//   EFORM_SAVE_200_PROCESS_ADD: {
//     TEXT: 'eForm Save 200 Process|Add',
//     ROUTE: 'eform-save-200-process/add',
//     LINK: '/eform-save-200-process/add'
//   },
//   EFORM_SAVE_200_PROCESS_EDIT: {
//     TEXT: 'eForm Save 200 Process|Edit',
//     ROUTE: 'eform-save-200-process/edit',
//     LINK: '/eform-save-200-process/edit'
//   },
//   EFORM_SAVE_030_PROCESS: {
//     TEXT: 'eForm Save 030 Process',
//     ROUTE: 'eform-save-030-process',
//     LINK: '/eform-save-030-process'
//   },
//   EFORM_SAVE_030_PROCESS_ADD: {
//     TEXT: 'eForm Save 030 Process|Add',
//     ROUTE: 'eform-save-030-process/add',
//     LINK: '/eform-save-030-process/add'
//   },
//   EFORM_SAVE_030_PROCESS_EDIT: {
//     TEXT: 'eForm Save 030 Process|Edit',
//     ROUTE: 'eform-save-030-process/edit',
//     LINK: '/eform-save-030-process/edit'
//   },
//   EFORM_SAVE_040_PROCESS: {
//     TEXT: 'eForm Save 040 Process',
//     ROUTE: 'eform-save-040-process',
//     LINK: '/eform-save-040-process'
//   },
//   EFORM_SAVE_040_PROCESS_ADD: {
//     TEXT: 'eForm Save 040 Process|Add',
//     ROUTE: 'eform-save-040-process/add',
//     LINK: '/eform-save-040-process/add'
//   },
//   EFORM_SAVE_040_PROCESS_EDIT: {
//     TEXT: 'eForm Save 040 Process|Edit',
//     ROUTE: 'eform-save-040-process/edit',
//     LINK: '/eform-save-040-process/edit'
//   },
//   EFORM_SAVE_050_PROCESS: {
//     TEXT: 'eForm Save 050 Process',
//     ROUTE: 'eform-save-050-process',
//     LINK: '/eform-save-050-process'
//   },
//   EFORM_SAVE_050_PROCESS_ADD: {
//     TEXT: 'eForm Save 050 Process|Add',
//     ROUTE: 'eform-save-050-process/add',
//     LINK: '/eform-save-050-process/add'
//   },
//   EFORM_SAVE_050_PROCESS_EDIT: {
//     TEXT: 'eForm Save 050 Process|Edit',
//     ROUTE: 'eform-save-050-process/edit',
//     LINK: '/eform-save-050-process/edit'
//   },
//   EFORM_SAVE_060_PROCESS: {
//     TEXT: 'eForm Save 060 Process',
//     ROUTE: 'eform-save-060-process',
//     LINK: '/eform-save-060-process'
//   },
//   EFORM_SAVE_060_PROCESS_ADD: {
//     TEXT: 'eForm Save 060 Process|Add',
//     ROUTE: 'eform-save-060-process/add',
//     LINK: '/eform-save-060-process/add'
//   },
//   EFORM_SAVE_060_PROCESS_EDIT: {
//     TEXT: 'eForm Save 060 Process|Edit',
//     ROUTE: 'eform-save-060-process/edit',
//     LINK: '/eform-save-060-process/edit'
//   },
//   EFORM_SAVE_070_PROCESS: {
//     TEXT: 'eForm Save 070 Process',
//     ROUTE: 'eform-save-070-process',
//     LINK: '/eform-save-070-process'
//   },
//   EFORM_SAVE_070_PROCESS_ADD: {
//     TEXT: 'eForm Save 070 Process|Add',
//     ROUTE: 'eform-save-070-process/add',
//     LINK: '/eform-save-070-process/add'
//   },
//   EFORM_SAVE_070_PROCESS_EDIT: {
//     TEXT: 'eForm Save 070 Process|Edit',
//     ROUTE: 'eform-save-070-process/edit',
//     LINK: '/eform-save-070-process/edit'
//   },
//   EFORM_SAVE_080_PROCESS: {
//     TEXT: 'eForm Save 080 Process',
//     ROUTE: 'eform-save-080-process',
//     LINK: '/eform-save-080-process'
//   },
//   EFORM_SAVE_080_PROCESS_ADD: {
//     TEXT: 'eForm Save 080 Process|Add',
//     ROUTE: 'eform-save-080-process/add',
//     LINK: '/eform-save-080-process/add'
//   },
//   EFORM_SAVE_080_PROCESS_EDIT: {
//     TEXT: 'eForm Save 080 Process|Edit',
//     ROUTE: 'eform-save-080-process/edit',
//     LINK: '/eform-save-080-process/edit'
//   },
//   EFORM_SAVE_021_PROCESS: {
//     TEXT: 'eForm Save 021 Process',
//     ROUTE: 'eform-save-021-process',
//     LINK: '/eform-save-021-process'
//   },
//   EFORM_SAVE_021_PROCESS_ADD: {
//     TEXT: 'eForm Save 021 Process|Add',
//     ROUTE: 'eform-save-021-process/add',
//     LINK: '/eform-save-021-process/add'
//   },
//   EFORM_SAVE_021_PROCESS_EDIT: {
//     TEXT: 'eForm Save 021 Process|Edit',
//     ROUTE: 'eform-save-021-process/edit',
//     LINK: '/eform-save-021-process/edit'
//   },
//   EFORM_SAVE_120_PROCESS: {
//     TEXT: 'eForm Save 120 Process',
//     ROUTE: 'eform-save-120-process',
//     LINK: '/eform-save-120-process'
//   },
//   EFORM_SAVE_120_PROCESS_ADD: {
//     TEXT: 'eForm Save 120 Process|Add',
//     ROUTE: 'eform-save-120-process/add',
//     LINK: '/eform-save-120-process/add'
//   },
//   EFORM_SAVE_120_PROCESS_EDIT: {
//     TEXT: 'eForm Save 120 Process|Edit',
//     ROUTE: 'eform-save-120-process/edit',
//     LINK: '/eform-save-120-process/edit'
//   },
//   DOCUMENT_ROLE02_CHECK_LIST: {
//     TEXT: 'Document Role02 Check|List',
//     ROUTE: 'document-role02-check/list',
//     LINK: '/document-role02-check/list'
//   },
//   DOCUMENT_ROLE02_CHECK: {
//     TEXT: 'Document Role02 Check',
//     ROUTE: 'document-role02-check',
//     LINK: '/document-role02-check'
//   },
//   DOCUMENT_ROLE02_CHECK_ADD: {
//     TEXT: 'Document Role02 Check|Add',
//     ROUTE: 'document-role02-check/add',
//     LINK: '/document-role02-check/add'
//   },
//   DOCUMENT_ROLE02_CHECK_EDIT: {
//     TEXT: 'Document Role02 Check|Edit',
//     ROUTE: 'document-role02-check/edit',
//     LINK: '/document-role02-check/edit'
//   },
//   DOCUMENT_ROLE02_PRINT_DOCUMENT_LIST: {
//     TEXT: 'Document Role02 Print Document|List',
//     ROUTE: 'document-role02-print-document/list',
//     LINK: '/document-role02-print-document/list'
//   },
//   DOCUMENT_ROLE02_PRINT_COVER_LIST: {
//     TEXT: 'Document Role02 Print Cover|List',
//     ROUTE: 'document-role02-print-cover/list',
//     LINK: '/document-role02-print-cover/list'
//   },
//   DOCUMENT_ROLE02_PRINT_LIST_LIST: {
//     TEXT: 'Document Role02 Print List|List',
//     ROUTE: 'document-role02-print-list/list',
//     LINK: '/document-role02-print-list/list'
//   },
//   REQUEST_CHECK: {
//     TEXT: 'Request Check',
//     ROUTE: 'request-check',
//     LINK: '/request-check'
//   },
//   REQUEST_CHECK_ADD: {
//     TEXT: 'Request Check|Add',
//     ROUTE: 'request-check/add',
//     LINK: '/request-check/add'
//   },
//   REQUEST_CHECK_EDIT: {
//     TEXT: 'Request Check|Edit',
//     ROUTE: 'request-check/edit',
//     LINK: '/request-check/edit'
//   },
//   REQUEST_SEARCH_PEOPLE: {
//     TEXT: 'Request Search People',
//     ROUTE: 'request-search-people',
//     LINK: '/request-search-people'
//   },
//   FLOOR3_DOCUMENT: {
//     TEXT: 'Floor3 Document',
//     ROUTE: 'floor3-document',
//     LINK: '/floor3-document'
//   }
// }

// // TODO Refactor

// export const ROOT_URL = window.location.protocol + "//" + window.location.host + "/";

// export const LOGIN_URL = 'https://sso.ipthailand.go.th/authorize?app_id=1700000019&redirect_url=' + ROOT_URL + 'api/Auth/AuthCallBack?last_action=' + window.location.href.replace('api/Auth/AuthCallBack?last_action','')

// export const SECRET_KEY = 'DIP_SECRET_KEY'

// export const LOCAL_STORAGE = {
//   USER_AUTH: 'DIP_AUTH_KEY',
//   USER_INFO: 'DIP_AUTH_INFO_KEY'
// }

// export const CONSTANTS = {
//   TOAST: {
//     ERROR: {
//       action: 'error',
//       header: 'เกิดข้อผิดพลาด',
//       message: ''
//     }
//   },

//   PAGINATION: {
//     INIT: {
//       id: 'paginate',
//       currentPage: 1,
//       itemsPerPage: 10,
//       totalItems: 0
//     },
//     PER_PAGE: [10, 20, 30, 50, 100]
//   },

//   AUTOCOMPLETE: {
//     item_per_page: 5
//   },

//   DELAY_CALL_API: 300
// }

// export const VALIDATE = {
//   validateAddRequest: [
//     [
//       'request_date',
//       'isRequired',
//       'กรุณาเลือก วันที่ยื่นคำขอ'
//     ],
//     [
//       'requester_name',
//       'isRequired',
//       //'isRequired | isFullName',
//       'กรุณากรอก ผู้ยื่นคำขอ'
//       //'กรุณากรอก ผู้ยื่นคำขอ | กรุณากรอก ตัวอักษรภาษาไทย'
//     ],
//     [
//       'item_count',
//       'isRequired | isCount',
//       'กรุณากรอก จำนวนคำขอ | กรุณากรอก ตัวเลขมากกว่า 0'
//     ],
//     [
//       'telephone',
//       'isRequired',
//       //'isRequired | isPhone',
//       'กรุณากรอก เบอร์โทร'
//       //'กรุณากรอก เบอร์โทร | เบอร์โทรไม่ถูกต้องตามรูปแบบที่กำหนด'
//     ]
//   ],
//   validateRequestOtherSave: [
//     //[
//     //    'request_date',
//     //    'isRequired',
//     //    'กรุณาเลือก วันที่ยื่นคำขอ'
//     //],
//     [
//       'requester_name',
//       'isRequired',
//       //'isRequired | isFullName',
//       'กรุณากรอก ผู้ยื่นคำขอ'
//       //'กรุณากรอก ผู้ยื่นคำขอ | กรุณากรอก ตัวอักษรภาษาไทย'
//     ],
//     [
//       'telephone',
//       'isRequired',
//       //'isRequired | isPhone',
//       'กรุณากรอก เบอร์โทร'
//       //'กรุณากรอก เบอร์โทร | เบอร์โทรไม่ถูกต้องตามรูปแบบที่กำหนด'
//     ]
//   ],
//   validateLoadUnpaid: [
//     [
//       'reference_number',
//       'isRequired',
//       //'isRequired | isFullName',
//       'กรุณากรอก เลขที่อ้างอิงการชำระเงิน'
//       //'กรุณากรอก ผู้ยื่นคำขอ | กรุณากรอก ตัวอักษรภาษาไทย'
//     ],
//   ],
//   validateSaveItemType: [
//     //[
//     //  'item_sub_type_1_code',
//     //  'isRequired',
//     //  'กรุณากรอก รหัสจำพวก'
//     //],
//     [
//       'product_count',
//       'isRequired | isCount',
//       'กรุณากรอก จำนวนสินค้า | กรุณากรอก ตัวเลขมากกว่า 0'
//     ]
//   ],
//   validateSearchReceipt: [
//     [
//       'reference_number',
//       'isRequired',
//       'กรุณากรอก เลขที่อ้างอิงการชำระเงิน'
//     ]
//   ],

//   // Eform
//   validateEFormSave01ProcessStep1: [
//     [
//       'request_item_type_code',
//       'isRequired',
//       'กรุณาเลือก ประเภทเครื่องหมาย'
//     ]
//   ]
// }





































