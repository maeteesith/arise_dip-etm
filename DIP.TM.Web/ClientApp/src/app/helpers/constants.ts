export const ROUTE_PATH = {
  DASHBOARD: {
    TEXT: 'dashboard',
    ROUTE: 'dashboard',
    LINK: '/dashboard'
  },
  EFORM_HOME: {
    TEXT: 'eForm Home',
    ROUTE: 'eform-home',
    LINK: '/eform-home'
  },
  NOT_FOUND: {
    TEXT: 'Not Found',
    ROUTE: 'not-found',
    LINK: '/not-found'
  },
  TEST: {
    TEXT: 'Test',
    ROUTE: 'test',
    LINK: '/test'
  },
  ZONE_COPY: {
    TEXT: 'Zone Copy',
    ROUTE: 'zone-copy',
    LINK: '/zone-copy'
  },
  REGISTRATION_REQUEST: {
    TEXT: 'Registration Request',
    ROUTE: 'registration-request',
    LINK: '/registration-request'
  },
  REGISTRATION_REQUEST_ADD: {
    TEXT: 'Registration Request',
    ROUTE: 'registration-request/add',
    LINK: '/registration-request/add'
  },
  REQUEST_01_ITEM: {
    TEXT: 'Request 01 Item|List',
    ROUTE: 'request-01-item',
    LINK: '/request-01-item'
  },
  REQUEST_01_ITEM_LIST: {
    TEXT: 'Request 01 Item|List',
    ROUTE: 'request-01-item/list',
    LINK: '/request-01-item/list'
  },
  REGISTRATION_REQUEST_EDIT: {
    TEXT: 'Registration Request',
    ROUTE: 'registration-request/edit',
    LINK: '/registration-request/edit'
  },
  DAILY_FEE_INCOME_RECORDS: {
    TEXT: 'daily-fee-income-records',
    ROUTE: 'daily-fee-income-records',
    LINK: '/daily-fee-income-records'
  },
  REPRESENTATIVE_MANAGEMENT: {
    TEXT: 'Representative Management',
    ROUTE: 'representative-management',
    LINK: '/representative-management'
  },
  REGISTRATION_OTHER_REQUEST: {
    TEXT: 'Registration Other Request',
    ROUTE: 'registration-other-request',
    LINK: '/registration-other-request'
  },
  REGISTRATION_OTHER_REQUEST_ADD: {
    TEXT: 'Registration Other Request|Add',
    ROUTE: 'registration-other-request/add',
    LINK: '/registration-other-request/add'
  },
  REGISTRATION_OTHER_REQUEST_EDIT: {
    TEXT: 'Registration Other Request|Edit',
    ROUTE: 'registration-other-request/edit',
    LINK: '/registration-other-request/edit'
  },
  REQUEST_OTHER_ITEM: {
    TEXT: 'Request Other Item|List',
    ROUTE: 'request-other-item',
    LINK: '/request-other-item'
  },
  REQUEST_OTHER_ITEM_LIST: {
    TEXT: 'Request Other Item|List',
    ROUTE: 'request-other-item/list',
    LINK: '/request-other-item/list'
  },
  RECEIPT_DAILY: {
    TEXT: 'Receipt Daily',
    ROUTE: 'receipt-daily',
    LINK: '/receipt-daily'
  },
  RECEIPT_DAILY_EDIT: {
    TEXT: 'Receipt Daily|Edit',
    ROUTE: 'receipt-daily/edit',
    LINK: '/receipt-daily/edit'
  },
  SAVE_01_PROCESS_LIST: {
    TEXT: 'Save 01 Process|List',
    ROUTE: 'save-01-process/list',
    LINK: '/save-01-process/list'
  },
  SAVE_01_PROCESS: {
    TEXT: 'Save 01 Process',
    ROUTE: 'save-01-process',
    LINK: '/save-01-process'
  },
  SAVE_01_PROCESS_ADD: {
    TEXT: 'Save 01 Process|Add',
    ROUTE: 'save-01-process/add',
    LINK: '/save-01-process/add'
  },
  SAVE_01_PROCESS_EDIT: {
    TEXT: 'Save 01 Process|Edit',
    ROUTE: 'save-01-process/edit',
    LINK: '/save-01-process/edit'
  },
  SAVE_020_PROCESS: {
    TEXT: 'Save 020 Process',
    ROUTE: 'save-020-process',
    LINK: '/save-020-process'
  },
  SAVE_020_PROCESS_ADD: {
    TEXT: 'Save 020 Process|Add',
    ROUTE: 'save-020-process/add',
    LINK: '/save-020-process/add'
  },
  SAVE_020_PROCESS_EDIT: {
    TEXT: 'Save 020 Process|Edit',
    ROUTE: 'save-020-process/edit',
    LINK: '/save-020-process/edit'
  },
  SAVE_020_PROCESS_LIST: {
    TEXT: 'Save 020 Process|List',
    ROUTE: 'save-020-process/list',
    LINK: '/save-020-process/list'
  },
  SAVE_021_PROCESS: {
    TEXT: 'Save 021 Process',
    ROUTE: 'save-021-process',
    LINK: '/save-021-process'
  },
  SAVE_021_PROCESS_ADD: {
    TEXT: 'Save 021 Process|Add',
    ROUTE: 'save-021-process/add',
    LINK: '/save-021-process/add'
  },
  SAVE_021_PROCESS_EDIT: {
    TEXT: 'Save 021 Process|Edit',
    ROUTE: 'save-021-process/edit',
    LINK: '/save-021-process/edit'
  },
  SAVE_021_PROCESS_LIST: {
    TEXT: 'Save 021 Process|List',
    ROUTE: 'save-021-process/list',
    LINK: '/save-021-process/list'
  },
  SAVE_03_PROCESS: {
    TEXT: 'Save 03 Process',
    ROUTE: 'save-03-process',
    LINK: '/save-03-process'
  },
  SAVE_03_PROCESS_LIST: {
    TEXT: 'Save 03 Process|List',
    ROUTE: 'save-03-process/list',
    LINK: '/save-03-process/list'
  },
  SAVE_04_PROCESS_LIST: {
    TEXT: 'Save 04 Process|List',
    ROUTE: 'save-04-process/list',
    LINK: '/save-04-process/list'
  },
  SAVE_04_PROCESS: {
    TEXT: 'Save 04 Process',
    ROUTE: 'save-04-process',
    LINK: '/save-04-process'
  },
  SAVE_05_PROCESS: {
    TEXT: 'Save 05 Process',
    ROUTE: 'save-05-process',
    LINK: '/save-05-process'
  },
  SAVE_05_PROCESS_LIST: {
    TEXT: 'Save 05 Process|List',
    ROUTE: 'save-05-process/list',
    LINK: '/save-05-process/list'
  },
  SAVE_06_PROCESS: {
    TEXT: 'Save 06 Process',
    ROUTE: 'save-06-process',
    LINK: '/save-06-process'
  },
  SAVE_07_PROCESS: {
    TEXT: 'Save 07 Process',
    ROUTE: 'save-07-process',
    LINK: '/save-07-process'
  },
  SAVE_08_PROCESS: {
    TEXT: 'Save 08 Process',
    ROUTE: 'save-08-process',
    LINK: '/save-08-process'
  },
  SAVE_14_PROCESS: {
    TEXT: 'Save 140 Process',
    ROUTE: 'save-140-process',
    LINK: '/save-140-process'
  },
  SAVE_06_PROCESS_LIST: {
    TEXT: 'Save 06 Process|List',
    ROUTE: 'save-06-process/list',
    LINK: '/save-06-process/list'
  },
  SAVE_07_PROCESS_LIST: {
    TEXT: 'Save 07 Process|List',
    ROUTE: 'save-07-process/list',
    LINK: '/save-07-process/list'
  },
  SAVE_08_PROCESS_LIST: {
    TEXT: 'Save 08 Process|List',
    ROUTE: 'save-08-process/list',
    LINK: '/save-08-process/list'
  },
  REQUEST_SEARCH: {
    TEXT: 'Request Search',
    ROUTE: 'request-search',
    LINK: '/request-search'
  },
  REQUEST_SEARCH_LOAD: {
    TEXT: 'Request Search|Load',
    ROUTE: 'request-search/load',
    LINK: '/request-search/load'
  },
  REQUEST_DOCUMENT_COLLECT: {
    TEXT: 'Request Document Collect',
    ROUTE: 'request-document-collect',
    LINK: '/request-document-collect'
  },
  ALL_TASK: {
    TEXT: 'All Task',
    ROUTE: 'all-task',
    LINK: '/all-task'
  },
  DOCUMENT_PROCESS_CLASSIFICATION: {
    TEXT: 'Document Process Classification',
    ROUTE: 'document-process-classification',
    LINK: '/document-process-classification'
  },
  DOCUMENT_PROCESS_CLASSIFICATION_DO: {
    TEXT: 'Document Process Classification Do',
    ROUTE: 'document-process-classification-do',
    LINK: '/document-process-classification-do'
  },
  DOCUMENT_PROCESS_CLASSIFICATION_DO_EDIT: {
    TEXT: 'Document Process Classification Do|Edit',
    ROUTE: 'document-process-classification-do/edit',
    LINK: '/document-process-classification-do/edit'
  },
  SAVE_04_PROCESS_ADD: {
    TEXT: 'Save 04 Process|Add',
    ROUTE: 'save-04-process/add',
    LINK: '/save-04-process/add'
  },
  SAVE_04_PROCESS_EDIT: {
    TEXT: 'Save 04 Process|Edit',
    ROUTE: 'save-04-process/edit',
    LINK: '/save-04-process/edit'
  },
  SAVE_05_PROCESS_ADD: {
    TEXT: 'Save 05 Process|Add',
    ROUTE: 'save-05-process/add',
    LINK: '/save-05-process/add'
  },
  SAVE_05_PROCESS_EDIT: {
    TEXT: 'Save 05 Process|Edit',
    ROUTE: 'save-05-process/edit',
    LINK: '/save-05-process/edit'
  },
  SAVE_07_PROCESS_ADD: {
    TEXT: 'Save 07 Process|Add',
    ROUTE: 'save-07-process/add',
    LINK: '/save-07-process/add'
  },
  SAVE_07_PROCESS_EDIT: {
    TEXT: 'Save 07 Process|Edit',
    ROUTE: 'save-07-process/edit',
    LINK: '/save-07-process/edit'
  },
  SAVE_08_PROCESS_ADD: {
    TEXT: 'Save 08 Process|Add',
    ROUTE: 'save-08-process/add',
    LINK: '/save-08-process/add'
  },
  SAVE_08_PROCESS_EDIT: {
    TEXT: 'Save 08 Process|Edit',
    ROUTE: 'save-08-process/edit',
    LINK: '/save-08-process/edit'
  },
  SAVE_140_PROCESS: {
    TEXT: 'Save 140 Process',
    ROUTE: 'save-140-process',
    LINK: '/save-140-process'
  },
  SAVE_140_PROCESS_ADD: {
    TEXT: 'Save 140 Process|Add',
    ROUTE: 'save-140-process/add',
    LINK: '/save-140-process/add'
  },
  SAVE_140_PROCESS_EDIT: {
    TEXT: 'Save 140 Process|Edit',
    ROUTE: 'save-140-process/edit',
    LINK: '/save-140-process/edit'
  },
  SAVE_140_PROCESS_LIST: {
    TEXT: 'Save 140 Process|List',
    ROUTE: 'save-140-process/list',
    LINK: '/save-140-process/list'
  },
  CHECKING_SIMILAR_IMAGE: {
    TEXT: 'Checking Similar Image',
    ROUTE: 'checking-similar-image',
    LINK: '/checking-similar-image'
  },
  CHECKING_ITEM: {
    TEXT: 'Checking Item|List',
    ROUTE: 'checking-item',
    LINK: '/checking-item'
  },
  CHECKING_ITEM_LIST: {
    TEXT: 'Checking Item|List',
    ROUTE: 'checking-item/list',
    LINK: '/checking-item/list'
  },
  CHECKING_SIMILAR_LIST: {
    TEXT: 'Checking Similar|List',
    ROUTE: 'checking-similar/list',
    LINK: '/checking-similar/list'
  },
  CHECKING_SIMILAR: {
    TEXT: 'Checking Similar',
    ROUTE: 'checking-similar',
    LINK: '/checking-similar'
  },
  CHECKING_SIMILAR_ADD: {
    TEXT: 'Checking Similar|Add',
    ROUTE: 'checking-similar/add',
    LINK: '/checking-similar/add'
  },
  CHECKING_SIMILAR_EDIT: {
    TEXT: 'Checking Similar|Edit',
    ROUTE: 'checking-similar/edit',
    LINK: '/checking-similar/edit'
  },
  CHECKING_SIMILAR_WORD: {
    TEXT: 'Checking Similar Word',
    ROUTE: 'checking-similar-word',
    LINK: '/checking-similar-word'
  },
  CHECKING_SIMILAR_WORD_ADD: {
    TEXT: 'Checking Similar Word|Add',
    ROUTE: 'checking-similar-word/add',
    LINK: '/checking-similar-word/add'
  },
  CHECKING_SIMILAR_WORD_EDIT: {
    TEXT: 'Checking Similar Word|Edit',
    ROUTE: 'checking-similar-word/edit',
    LINK: '/checking-similar-word/edit'
  },
  CHECKING_SIMILAR_RESULT: {
    TEXT: 'Checking Similar Result',
    ROUTE: 'checking-similar-result',
    LINK: '/checking-similar-result'
  },
  CHECKING_SIMILAR_RESULT_ADD: {
    TEXT: 'Checking Similar Result|Add',
    ROUTE: 'checking-similar-result/add',
    LINK: '/checking-similar-result/add'
  },
  CHECKING_SIMILAR_RESULT_EDIT: {
    TEXT: 'Checking Similar Result|Edit',
    ROUTE: 'checking-similar-result/edit',
    LINK: '/checking-similar-result/edit'
  },
  CONSIDERING_SIMILAR_LIST: {
    TEXT: 'Considering Similar|List',
    ROUTE: 'considering-similar/list',
    LINK: '/considering-similar/list'
  },
  CONSIDERING_SIMILAR: {
    TEXT: 'Considering Similar',
    ROUTE: 'considering-similar',
    LINK: '/considering-similar'
  },
  CONSIDERING_SIMILAR_ADD: {
    TEXT: 'Considering Similar|Add',
    ROUTE: 'considering-similar/add',
    LINK: '/considering-similar/add'
  },
  CONSIDERING_SIMILAR_EDIT: {
    TEXT: 'Considering Similar|Edit',
    ROUTE: 'considering-similar/edit',
    LINK: '/considering-similar/edit'
  },
  PUBLIC_DASHBOARD_ROLE_01: {
    TEXT: 'Public Dashboard Role 01',
    ROUTE: 'public-dashboard-role-01',
    LINK: '/public-dashboard-role-01'
  },
  EFORM_SAVE_01_PROCESS: {
    TEXT: 'eForm Save 01 Process',
    ROUTE: 'eform-save-01-process',
    LINK: '/eform-save-01-process'
  },
  EFORM_SAVE_02_PROCESS: {
    TEXT: 'eForm Save 02 Process',
    ROUTE: 'eform-save-02-process',
    LINK: '/eform-save-02-process'
  },
  EFORM: {
    TEXT: 'eForm',
    ROUTE: 'eform',
    LINK: '/eform'
  },
  EFORM_SAVE_01_PROCESS_ADD: {
    TEXT: 'eForm Save 01 Process|Add',
    ROUTE: 'eform-save-01-process/add',
    LINK: '/eform-save-01-process/add'
  },
  EFORM_SAVE_01_PROCESS_EDIT: {
    TEXT: 'eForm Save 01 Process|Edit',
    ROUTE: 'eform-save-01-process/edit',
    LINK: '/eform-save-01-process/edit'
  },
  EFORM_BRAND_TYPE: {
    TEXT: 'eForm Brand Type',
    ROUTE: 'eform/brand-type',
    LINK: '/eform/brand-type'
  },
  EFORM_OWNER_AND_AGENT: {
    TEXT: 'eForm Owner',
    ROUTE: 'eform/owner-and-agent',
    LINK: '/eform/owner-and-agent'
  },
  DOCUMENT_ITEM: {
    TEXT: 'Document Item|List',
    ROUTE: 'document-item',
    LINK: '/document-item'
  },
  DOCUMENT_ITEM_LIST: {
    TEXT: 'Document Item|List',
    ROUTE: 'document-item/list',
    LINK: '/document-item/list'
  },
  CONSIDERING_SIMILAR_INSTRUCTION: {
    TEXT: 'Considering Similar Instruction',
    ROUTE: 'considering-similar-instruction',
    LINK: '/considering-similar-instruction'
  },
  CONSIDERING_SIMILAR_INSTRUCTION_ADD: {
    TEXT: 'Considering Similar Instruction|Add',
    ROUTE: 'considering-similar-instruction/add',
    LINK: '/considering-similar-instruction/add'
  },
  CONSIDERING_SIMILAR_INSTRUCTION_EDIT: {
    TEXT: 'Considering Similar Instruction|Edit',
    ROUTE: 'considering-similar-instruction/edit',
    LINK: '/considering-similar-instruction/edit'

  },
  CONSIDERING_SIMILAR_DOCUMENT: {
    TEXT: 'Considering Similar Document',
    ROUTE: 'considering-similar-document',
    LINK: '/considering-similar-document'
  },
  CONSIDERING_SIMILAR_DOCUMENT_ADD: {
    TEXT: 'Considering Similar Document|Add',
    ROUTE: 'considering-similar-document/add',
    LINK: '/considering-similar-document/add'
  },
  CONSIDERING_SIMILAR_DOCUMENT_EDIT: {
    TEXT: 'Considering Similar Document|Edit',
    ROUTE: 'considering-similar-document/edit',
    LINK: '/considering-similar-document/edit'
  },
  PUBLIC_ROLE01_ITEM: {
    TEXT: 'Public Role01 Item|List',
    ROUTE: 'public-role01-item',
    LINK: '/public-role01-item'
  },
  PUBLIC_ROLE01_CHECK: {
    TEXT: 'Public Role01 Check|List',
    ROUTE: 'public-role01-check',
    LINK: '/public-role01-check'
  },
  PUBLIC_ROLE01_ITEM_LIST: {
    TEXT: 'Public Role01 Item|List',
    ROUTE: 'public-role01-item/list',
    LINK: '/public-role01-item/list'
  },
  PUBLIC_ROLE01_CHECK_LIST: {
    TEXT: 'Public Role01 Check|List',
    ROUTE: 'public-role01-check/list',
    LINK: '/public-role01-check/list'
  },
  PUBLIC_ROLE01_CHECK_DO: {
    TEXT: 'Public Role01 Check Do',
    ROUTE: 'public-role01-check-do',
    LINK: '/public-role01-check-do'
  },
  PUBLIC_ROLE01_CHECK_DO_ADD: {
    TEXT: 'Public Role01 Check Do|Add',
    ROUTE: 'public-role01-check-do/add',
    LINK: '/public-role01-check-do/add'
  },
  PUBLIC_ROLE01_CHECK_DO_EDIT: {
    TEXT: 'Public Role01 Check Do|Edit',
    ROUTE: 'public-role01-check-do/edit',
    LINK: '/public-role01-check-do/edit'
  },
  PUBLIC_ROLE01_PREPARE: {
    TEXT: 'Public Role01 Prepare|List',
    ROUTE: 'public-role01-prepare',
    LINK: '/public-role01-prepare'
  },
  PUBLIC_ROLE01_DOING: {
    TEXT: 'Public Role01 Doing|List',
    ROUTE: 'public-role01-doing',
    LINK: '/public-role01-doing'
  },
  PUBLIC_ROLE02_CHECK: {
    TEXT: 'Public Role02 Check|List',
    ROUTE: 'public-role02-check',
    LINK: '/public-role02-check'
  },
  PUBLIC_ROLE02_CONSIDER_PAYMENT: {
    TEXT: 'Public Role02 Consider Payment|List',
    ROUTE: 'public-role02-consider-payment',
    LINK: '/public-role02-consider-payment'
  },
  PUBLIC_ROLE05_CONSIDER_PAYMENT: {
    TEXT: 'Public Role05 Consider Payment|List',
    ROUTE: 'public-role05-consider-payment',
    LINK: '/public-role05-consider-payment'
  },
  PUBLIC_ROLE02_DOCUMENT_POST: {
    TEXT: 'Public Role02 Document Post|List',
    ROUTE: 'public-role02-document-post',
    LINK: '/public-role02-document-post'
  },
  PUBLIC_ROLE02_ACTION_POST: {
    TEXT: 'Public Role02 Action Post|List',
    ROUTE: 'public-role02-action-post',
    LINK: '/public-role02-action-post'
  },
  PUBLIC_ROLE04_ITEM: {
    TEXT: 'Public Role04 item|List',
    ROUTE: 'public-role04-item',
    LINK: '/public-role04-item'
  },
  PUBLIC_ROLE04_DOCUMENT: {
    TEXT: 'Public Role04 Document|List',
    ROUTE: 'public-role04-document',
    LINK: '/public-role04-document'
  },
  PUBLIC_ROLE02_DOCUMENT: {
    TEXT: 'Public Role02 Document|List',
    ROUTE: 'public-role02-document',
    LINK: '/public-role02-document'
  },
  DOCUMENT_ROLE02_ITEM: {
    TEXT: 'Document Role02 Item|List',
    ROUTE: 'document-role02-item',
    LINK: '/document-role02-item'
  },
  PUBLIC_ROLE01_PREPARE_LIST: {
    TEXT: 'Public Role01 Prepare|List',
    ROUTE: 'public-role01-prepare/list',
    LINK: '/public-role01-prepare/list'
  },
  PUBLIC_ROLE01_DOING_LIST: {
    TEXT: 'Public Role01 Doing|List',
    ROUTE: 'public-role01-doing/list',
    LINK: '/public-role01-doing/list'
  },
  PUBLIC_ROLE02_CHECK_LIST: {
    TEXT: 'Public Role02 Check|List',
    ROUTE: 'public-role02-check/list',
    LINK: '/public-role02-check/list'
  },
  PUBLIC_ROLE02_CONSIDER_PAYMENT_LIST: {
    TEXT: 'Public Role02 Consider Payment|List',
    ROUTE: 'public-role02-consider-payment/list',
    LINK: '/public-role02-consider-payment/list'
  },
  PUBLIC_ROLE05_CONSIDER_PAYMENT_LIST: {
    TEXT: 'Public Role05 Consider Payment|List',
    ROUTE: 'public-role05-consider-payment/list',
    LINK: '/public-role05-consider-payment/list'
  },
  PUBLIC_ROLE02_DOCUMENT_POST_LIST: {
    TEXT: 'Public Role02 Document Post|List',
    ROUTE: 'public-role02-document-post/list',
    LINK: '/public-role02-document-post/list'
  },
  PUBLIC_ROLE02_ACTION_POST_LIST: {
    TEXT: 'Public Role02 Action Post|List',
    ROUTE: 'public-role02-action-post/list',
    LINK: '/public-role02-action-post/list'
  },
  PUBLIC_ROLE04_ITEM_LIST: {
    TEXT: 'Public Role04 item|List',
    ROUTE: 'public-role04-item/list',
    LINK: '/public-role04-item/list'
  },
  PUBLIC_ROLE04_DOCUMENT_LIST: {
    TEXT: 'Public Role04 Document|List',
    ROUTE: 'public-role04-document/list',
    LINK: '/public-role04-document/list'
  },
  PUBLIC_ROLE02_DOCUMENT_LIST: {
    TEXT: 'Public Role02 Document|List',
    ROUTE: 'public-role02-document/list',
    LINK: '/public-role02-document/list'
  },
  DOCUMENT_ROLE02_ITEM_LIST: {
    TEXT: 'Document Role02 Item|List',
    ROUTE: 'document-role02-item/list',
    LINK: '/document-role02-item/list'
  },
  EFORM_SAVE_140_PROCESS: {
    TEXT: 'eForm Save 140 Process',
    ROUTE: 'eform-save-140-process',
    LINK: '/eform-save-140-process'
  },
  EFORM_SAVE_140_PROCESS_ADD: {
    TEXT: 'eForm Save 140 Process|Add',
    ROUTE: 'eform-save-140-process/add',
    LINK: '/eform-save-140-process/add'
  },
  EFORM_SAVE_140_PROCESS_EDIT: {
    TEXT: 'eForm Save 140 Process|Edit',
    ROUTE: 'eform-save-140-process/edit',
    LINK: '/eform-save-140-process/edit'
  },
  EFORM_SAVE_150_PROCESS: {
    TEXT: 'eForm Save 150 Process',
    ROUTE: 'eform-save-150-process',
    LINK: '/eform-save-150-process'
  },
  EFORM_SAVE_150_PROCESS_ADD: {
    TEXT: 'eForm Save 150 Process|Add',
    ROUTE: 'eform-save-150-process/add',
    LINK: '/eform-save-150-process/add'
  },
  EFORM_SAVE_150_PROCESS_EDIT: {
    TEXT: 'eForm Save 150 Process|Edit',
    ROUTE: 'eform-save-150-process/edit',
    LINK: '/eform-save-150-process/edit'
  },
  EFORM_SAVE_190_PROCESS: {
    TEXT: 'eForm Save 190 Process',
    ROUTE: 'eform-save-190-process',
    LINK: '/eform-save-190-process'
  },
  EFORM_SAVE_190_PROCESS_ADD: {
    TEXT: 'eForm Save 190 Process|Add',
    ROUTE: 'eform-save-190-process/add',
    LINK: '/eform-save-190-process/add'
  },
  EFORM_SAVE_190_PROCESS_EDIT: {
    TEXT: 'eForm Save 190 Process|Edit',
    ROUTE: 'eform-save-190-process/edit',
    LINK: '/eform-save-190-process/edit'
  },
  EFORM_SAVE_200_PROCESS: {
    TEXT: 'eForm Save 200 Process',
    ROUTE: 'eform-save-200-process',
    LINK: '/eform-save-200-process'
  },
  EFORM_SAVE_200_PROCESS_ADD: {
    TEXT: 'eForm Save 200 Process|Add',
    ROUTE: 'eform-save-200-process/add',
    LINK: '/eform-save-200-process/add'
  },
  EFORM_SAVE_200_PROCESS_EDIT: {
    TEXT: 'eForm Save 200 Process|Edit',
    ROUTE: 'eform-save-200-process/edit',
    LINK: '/eform-save-200-process/edit'
  },
  EFORM_SAVE_210_PROCESS: {
    TEXT: 'eForm Save 210 Process',
    ROUTE: 'eform-save-210-process',
    LINK: '/eform-save-210-process'
  },
  EFORM_SAVE_210_PROCESS_ADD: {
    TEXT: 'eForm Save 210 Process|Add',
    ROUTE: 'eform-save-210-process/add',
    LINK: '/eform-save-210-process/add'
  },
  EFORM_SAVE_210_PROCESS_EDIT: {
    TEXT: 'eForm Save 210 Process|Edit',
    ROUTE: 'eform-save-210-process/edit',
    LINK: '/eform-save-210-process/edit'
  },
  EFORM_SAVE_220_PROCESS: {
    TEXT: 'eForm Save 220 Process',
    ROUTE: 'eform-save-220-process',
    LINK: '/eform-save-220-process'
  },
  EFORM_SAVE_220_PROCESS_ADD: {
    TEXT: 'eForm Save 220 Process|Add',
    ROUTE: 'eform-save-220-process/add',
    LINK: '/eform-save-220-process/add'
  },
  EFORM_SAVE_220_PROCESS_EDIT: {
    TEXT: 'eForm Save 220 Process|Edit',
    ROUTE: 'eform-save-220-process/edit',
    LINK: '/eform-save-220-process/edit'
  },
  EFORM_SAVE_230_PROCESS: {
    TEXT: 'eForm Save 230 Process',
    ROUTE: 'eform-save-230-process',
    LINK: '/eform-save-230-process'
  },
  EFORM_SAVE_230_PROCESS_ADD: {
    TEXT: 'eForm Save 230 Process|Add',
    ROUTE: 'eform-save-230-process/add',
    LINK: '/eform-save-230-process/add'
  },
  EFORM_SAVE_230_PROCESS_EDIT: {
    TEXT: 'eForm Save 230 Process|Edit',
    ROUTE: 'eform-save-230-process/edit',
    LINK: '/eform-save-230-process/edit'
  },
  EFORM_SAVE_030_PROCESS: {
    TEXT: 'eForm Save 030 Process',
    ROUTE: 'eform-save-030-process',
    LINK: '/eform-save-030-process'
  },
  EFORM_SAVE_030_PROCESS_ADD: {
    TEXT: 'eForm Save 030 Process|Add',
    ROUTE: 'eform-save-030-process/add',
    LINK: '/eform-save-030-process/add'
  },
  EFORM_SAVE_030_PROCESS_EDIT: {
    TEXT: 'eForm Save 030 Process|Edit',
    ROUTE: 'eform-save-030-process/edit',
    LINK: '/eform-save-030-process/edit'
  },
  EFORM_SAVE_040_PROCESS: {
    TEXT: 'eForm Save 040 Process',
    ROUTE: 'eform-save-040-process',
    LINK: '/eform-save-040-process'
  },
  EFORM_SAVE_040_PROCESS_ADD: {
    TEXT: 'eForm Save 040 Process|Add',
    ROUTE: 'eform-save-040-process/add',
    LINK: '/eform-save-040-process/add'
  },
  EFORM_SAVE_040_PROCESS_EDIT: {
    TEXT: 'eForm Save 040 Process|Edit',
    ROUTE: 'eform-save-040-process/edit',
    LINK: '/eform-save-040-process/edit'
  },
  EFORM_SAVE_050_PROCESS: {
    TEXT: 'eForm Save 050 Process',
    ROUTE: 'eform-save-050-process',
    LINK: '/eform-save-050-process'
  },
  EFORM_SAVE_050_PROCESS_ADD: {
    TEXT: 'eForm Save 050 Process|Add',
    ROUTE: 'eform-save-050-process/add',
    LINK: '/eform-save-050-process/add'
  },
  EFORM_SAVE_050_PROCESS_EDIT: {
    TEXT: 'eForm Save 050 Process|Edit',
    ROUTE: 'eform-save-050-process/edit',
    LINK: '/eform-save-050-process/edit'
  },
  EFORM_SAVE_060_PROCESS: {
    TEXT: 'eForm Save 060 Process',
    ROUTE: 'eform-save-060-process',
    LINK: '/eform-save-060-process'
  },
  EFORM_SAVE_060_PROCESS_ADD: {
    TEXT: 'eForm Save 060 Process|Add',
    ROUTE: 'eform-save-060-process/add',
    LINK: '/eform-save-060-process/add'
  },
  EFORM_SAVE_060_PROCESS_EDIT: {
    TEXT: 'eForm Save 060 Process|Edit',
    ROUTE: 'eform-save-060-process/edit',
    LINK: '/eform-save-060-process/edit'
  },
  EFORM_SAVE_070_PROCESS: {
    TEXT: 'eForm Save 070 Process',
    ROUTE: 'eform-save-070-process',
    LINK: '/eform-save-070-process'
  },
  EFORM_SAVE_070_PROCESS_ADD: {
    TEXT: 'eForm Save 070 Process|Add',
    ROUTE: 'eform-save-070-process/add',
    LINK: '/eform-save-070-process/add'
  },
  EFORM_SAVE_070_PROCESS_EDIT: {
    TEXT: 'eForm Save 070 Process|Edit',
    ROUTE: 'eform-save-070-process/edit',
    LINK: '/eform-save-070-process/edit'
  },
  EFORM_SAVE_080_PROCESS: {
    TEXT: 'eForm Save 080 Process',
    ROUTE: 'eform-save-080-process',
    LINK: '/eform-save-080-process'
  },
  EFORM_SAVE_080_PROCESS_ADD: {
    TEXT: 'eForm Save 080 Process|Add',
    ROUTE: 'eform-save-080-process/add',
    LINK: '/eform-save-080-process/add'
  },
  EFORM_SAVE_080_PROCESS_EDIT: {
    TEXT: 'eForm Save 080 Process|Edit',
    ROUTE: 'eform-save-080-process/edit',
    LINK: '/eform-save-080-process/edit'
  },
  EFORM_SAVE_021_PROCESS: {
    TEXT: 'eForm Save 021 Process',
    ROUTE: 'eform-save-021-process',
    LINK: '/eform-save-021-process'
  },
  EFORM_SAVE_021_PROCESS_ADD: {
    TEXT: 'eForm Save 021 Process|Add',
    ROUTE: 'eform-save-021-process/add',
    LINK: '/eform-save-021-process/add'
  },
  EFORM_SAVE_021_PROCESS_EDIT: {
    TEXT: 'eForm Save 021 Process|Edit',
    ROUTE: 'eform-save-021-process/edit',
    LINK: '/eform-save-021-process/edit'
  },
  EFORM_SAVE_120_PROCESS: {
    TEXT: 'eForm Save 120 Process',
    ROUTE: 'eform-save-120-process',
    LINK: '/eform-save-120-process'
  },
  EFORM_SAVE_120_PROCESS_ADD: {
    TEXT: 'eForm Save 120 Process|Add',
    ROUTE: 'eform-save-120-process/add',
    LINK: '/eform-save-120-process/add'
  },
  EFORM_SAVE_120_PROCESS_EDIT: {
    TEXT: 'eForm Save 120 Process|Edit',
    ROUTE: 'eform-save-120-process/edit',
    LINK: '/eform-save-120-process/edit'
  },
  DOCUMENT_ROLE02_CHECK_LIST: {
    TEXT: 'Document Role02 Check|List',
    ROUTE: 'document-role02-check/list',
    LINK: '/document-role02-check/list'
  },
  DOCUMENT_ROLE02_CHECK: {
    TEXT: 'Document Role02 Check',
    ROUTE: 'document-role02-check',
    LINK: '/document-role02-check'
  },
  DOCUMENT_ROLE02_CHECK_ADD: {
    TEXT: 'Document Role02 Check|Add',
    ROUTE: 'document-role02-check/add',
    LINK: '/document-role02-check/add'
  },
  DOCUMENT_ROLE02_CHECK_EDIT: {
    TEXT: 'Document Role02 Check|Edit',
    ROUTE: 'document-role02-check/edit',
    LINK: '/document-role02-check/edit'
  },
  DOCUMENT_ROLE02_PRINT_COVER: {
    TEXT: 'Document Role02 Print Cover|List',
    ROUTE: 'document-role02-print-cover',
    LINK: '/document-role02-print-cover'
  },
  DOCUMENT_ROLE02_PRINT_LIST: {
    TEXT: 'Document Role02 Print List|List',
    ROUTE: 'document-role02-print-list',
    LINK: '/document-role02-print-list'
  },
  PUBLIC_ROLE02_PRINT_COVER_LIST: {
    TEXT: 'Document Role02 Print Cover|List',
    ROUTE: 'document-role02-print-cover/list',
    LINK: '/document-role02-print-cover/list'
  },

  PUBLIC_ROLE02_PRINT_DOCUMENT_LIST: {
    TEXT: 'Pubic Role02 Print Document|List',
    ROUTE: 'public-role02-print-document/list',
    LINK: '/public-role02-print-document/list'
  },
  PUBLIC_ROLE02_PRINT_DOCUMENT: {
    TEXT: 'Pubic Role02 Print Document',
    ROUTE: 'public-role02-print-document',
    LINK: '/public-role02-print-document'
  },

  DOCUMENT_ROLE02_PRINT_DOCUMENT_LIST: {
    TEXT: 'Document Role02 Print Document|List',
    ROUTE: 'document-role02-print-document/list',
    LINK: '/document-role02-print-document/list'
  },
  DOCUMENT_ROLE02_PRINT_DOCUMENT: {
    TEXT: 'Document Role02 Print Document',
    ROUTE: 'document-role02-print-document',
    LINK: '/document-role02-print-document'
  },

  DOCUMENT_ROLE02_PRINT_COVER_LIST: {
    TEXT: 'Document Role02 Print Cover|List',
    ROUTE: 'document-role02-print-cover/list',
    LINK: '/document-role02-print-cover/list'
  },
  DOCUMENT_ROLE02_PRINT_LIST_LIST: {
    TEXT: 'Document Role02 Print List|List',
    ROUTE: 'document-role02-print-list/list',
    LINK: '/document-role02-print-list/list'
  },
  PUBLIC_ROLE02_PRINT_LIST_LIST: {
    TEXT: 'Document Role02 Print List|List',
    ROUTE: 'document-role02-print-list/list',
    LINK: '/document-role02-print-list/list'
  },


  PUBLIC_ROLE03_CHECK: {
    TEXT: 'Public Role03 Check',
    ROUTE: 'document-role04-check',
    LINK: '/document-role04-check'
  },
  PUBLIC_ROLE03_CHECK_ADD: {
    TEXT: 'Public Role03 Check|Add',
    ROUTE: 'document-role04-check/add',
    LINK: '/document-role04-check/add'
  },
  PUBLIC_ROLE03_CHECK_EDIT: {
    TEXT: 'Public Role03 Check|Edit',
    ROUTE: 'document-role04-check/edit',
    LINK: '/document-role04-check/edit'
  },
  PUBLIC_ROLE03_CHECK_LIST: {
    TEXT: 'Public Role03 Check List|List',
    ROUTE: 'document-role04-check/list',
    LINK: '/document-role04-check/list'
  },















  PUBLIC_ROLE05_CHECK: {
    TEXT: 'Public Role05 Check',
    ROUTE: 'document-role05-check',
    LINK: '/document-role05-check'
  },
  PUBLIC_ROLE05_CHECK_ADD: {
    TEXT: 'Public Role05 Check|Add',
    ROUTE: 'document-role05-check/add',
    LINK: '/document-role05-check/add'
  },
  PUBLIC_ROLE05_CHECK_EDIT: {
    TEXT: 'Public Role05 Check|Edit',
    ROUTE: 'document-role05-check/edit',
    LINK: '/document-role05-check/edit'
  },
  PUBLIC_ROLE05_CHECK_LIST: {
    TEXT: 'Public Role05 Check List|List',
    ROUTE: 'document-role05-check/list',
    LINK: '/document-role05-check/list'
  },
  PUBLIC_ROLE05_RELEASE: {
    TEXT: 'Public Role05 Release',
    ROUTE: 'document-role05-release',
    LINK: '/document-role05-release'
  },
  PUBLIC_ROLE05_RELEASE_ADD: {
    TEXT: 'Public Role05 Release|Add',
    ROUTE: 'document-role05-release/add',
    LINK: '/document-role05-release/add'
  },
  PUBLIC_ROLE05_RELEASE_EDIT: {
    TEXT: 'Public Role05 Release|Edit',
    ROUTE: 'document-role05-release/edit',
    LINK: '/document-role05-release/edit'
  },
  PUBLIC_ROLE05_RELEASE_LIST: {
    TEXT: 'Public Role05 Release List|List',
    ROUTE: 'document-role05-release/list',
    LINK: '/document-role05-release/list'
  },









  //PUBLIC_ROLE03_RELEASE: {
  //    TEXT: 'Public Role03 Release',
  //    ROUTE: 'document-role04-release',
  //    LINK: '/document-role04-release'
  //},
  //PUBLIC_ROLE03_RELEASE_ADD: {
  //    TEXT: 'Public Role03 Release|Add',
  //    ROUTE: 'document-role04-release/add',
  //    LINK: '/document-role04-release/add'
  //},
  //PUBLIC_ROLE03_RELEASE_EDIT: {
  //    TEXT: 'Public Role03 Release|Edit',
  //    ROUTE: 'document-role04-release/edit',
  //    LINK: '/document-role04-release/edit'
  //},
  //PUBLIC_ROLE03_RELEASE_LIST: {
  //    TEXT: 'Public Role03 Release List|List',
  //    ROUTE: 'document-role04-release/list',
  //    LINK: '/document-role04-release/list'
  //},




  REQUEST_CHECK: {
    TEXT: 'Request Check',
    ROUTE: 'request-check',
    LINK: '/request-check'
  },
  REQUEST_CHECK_ADD: {
    TEXT: 'Request Check|Add',
    ROUTE: 'request-check/add',
    LINK: '/request-check/add'
  },
  REQUEST_CHECK_EDIT: {
    TEXT: 'Request Check|Edit',
    ROUTE: 'request-check/edit',
    LINK: '/request-check/edit'
  },
  REQUEST_SEARCH_PEOPLE: {
    TEXT: 'Request Search People',
    ROUTE: 'request-search-people',
    LINK: '/request-search-people'
  },
  FLOOR3_DOCUMENT: {
    TEXT: 'Floor3 Document',
    ROUTE: 'floor3-document',
    LINK: '/floor3-document'
  },

  // No has route
  PUBLIC_ITEM_LIST: {
    TEXT: 'Public Item|List',
    ROUTE: 'public-item/list',
    LINK: '/public-item/list'
  },
  RECORD_REGISTRATION_NUMBER: {
    TEXT: 'Record Registration Number',
    ROUTE: 'record-registration-number',
    LINK: '/record-registration-number'
  },
  RECORD_REGISTRATION_NUMBER_ALLOW: {
    TEXT: 'Record Registration Number Allow',
    ROUTE: 'record-registration-number-allow',
    LINK: '/record-registration-number-allow'
  },
  CHECKING_SIMILAR_COMPARE: {
    TEXT: 'Checking Similar Compare',
    ROUTE: 'checking-similar-compare',
    LINK: '/checking-similar-compare'
  },
  CHECKING_SIMILAR_COMPARE_ADD: {
    TEXT: 'Checking Similar Compare|Add',
    ROUTE: 'checking-similar-compare/add',
    LINK: '/checking-similar-compare/add'
  },
  CHECKING_SIMILAR_COMPARE_EDIT: {
    TEXT: 'Checking Similar Compare|Edit',
    ROUTE: 'checking-similar-compare/edit',
    LINK: '/checking-similar-compare/edit'
  },
  RECEIPT_DAILY_LIST: {
    TEXT: 'Receipt Daily|List',
    ROUTE: 'receipt-daily/list',
    LINK: '/receipt-daily/list'
  },
  SAVE_SEND: {
    TEXT: 'Save Send',
    ROUTE: 'save-send',
    LINK: '/save-send'
  },
  SAVE_SEND_LIST: {
    TEXT: 'Save Send|List',
    ROUTE: 'save-send/list',
    LINK: '/save-send/list'
  },
  SAVE_SEND_EDIT: {
    TEXT: 'Save Send|Edit',
    ROUTE: 'save-send',
    LINK: '/save-send'
  },
  REQUEST_PRINT_COVER: {
    TEXT: 'Request Print Cover',
    ROUTE: 'request-print-cover',
    LINK: '/request-print-cover'
  },
  REQUEST_PRINT_COVER_LIST: {
    TEXT: 'Request Print Cover|List',
    ROUTE: 'request-print-cover/list',
    LINK: '/request-print-cover/list'
  },
  REQUEST_PRINT_COVER_EDIT: {
    TEXT: 'Request Print Cover|Edit',
    ROUTE: 'request-print-cover',
    LINK: '/request-print-cover'
  },
  DOCUMENT_ROLE03_CHANGE: {
    TEXT: 'Document Role03 Change',
    ROUTE: 'document-role03-change',
    LINK: '/document-role03-change'
  },
  DOCUMENT_ROLE03_CHANGE_LIST: {
    TEXT: 'Document Role03 Change|List',
    ROUTE: 'document-role03-change/list',
    LINK: '/document-role03-change/list'
  },

  DOCUMENT_ROLE03_CHANGE_EDIT: {
    TEXT: 'Document Role03 Change|Edit',
    ROUTE: 'document-role03-change',
    LINK: '/document-role03-change'
  },
  DOCUMENT_ROLE04_DOCUMENT_INSTRUCTION: {
    TEXT: 'Document Role04 Document Instruction',
    ROUTE: 'document-role04-document-instruction',
    LINK: '/document-role04-document-instruction'
  },
  DOCUMENT_ROLE04_DOCUMENT_INSTRUCTION_LIST: {
    TEXT: 'Document Role04 Document Instruction|List',
    ROUTE: 'document-role04-document-instruction/list',
    LINK: '/document-role04-document-instruction/list'
  },
  DOCUMENT_ROLE04_DOCUMENT_INSTRUCTION_EDIT: {
    TEXT: 'Document Role04 Document Instruction|Edit',
    ROUTE: 'document-role04-document-instruction',
    LINK: '/document-role04-document-instruction'
  },
  DOCUMENT_ROLE04_APPEAL: {
    TEXT: 'Document Role04 Appeal',
    ROUTE: 'document-role04-appeal',
    LINK: '/document-role04-appeal'
  },
  DOCUMENT_ROLE04_APPEAL_LIST: {
    TEXT: 'Document Role04 Appeal|List',
    ROUTE: 'document-role04-appeal/list',
    LINK: '/document-role04-appeal/list'
  },
  DOCUMENT_ROLE04_APPEAL_EDIT: {
    TEXT: 'Document Role04 Appeal|Edit',
    ROUTE: 'document-role04-appeal',
    LINK: '/document-role04-appeal'
  },
  DOCUMENT_ROLE04_RELEASE_REQUEST: {
    TEXT: 'Document Role04 Release Request',
    ROUTE: 'document-role04-release-request',
    LINK: '/document-role04-release-request'
  },
  DOCUMENT_ROLE04_RELEASE_REQUEST_LIST: {
    TEXT: 'Document Role04 Release Request|List',
    ROUTE: 'document-role04-release-request/list',
    LINK: '/document-role04-release-request/list'
  },
  DOCUMENT_ROLE04_RELEASE_REQUEST_EDIT: {
    TEXT: 'Document Role04 Release Request|Edit',
    ROUTE: 'document-role04-release-request',
    LINK: '/document-role04-release-request'
  },
  DOCUMENT_ROLE05_ITEM_EDIT: {
    TEXT: 'Document Role05 Item|Edit',
    ROUTE: 'document-role05-item',
    LINK: '/document-role05-item'
  },
  DOCUMENT_ROLE05_ITEM: {
    TEXT: 'Document Role05 Item',
    ROUTE: 'document-role05-item',
    LINK: '/document-role05-item'
  },
  DOCUMENT_ROLE05_ITEM_LIST: {
    TEXT: 'Document Role05 Item|List',
    ROUTE: 'document-role05-item',
    LINK: '/document-role05-item/list'
  },
  DOCUMENT_ROLE05_ITEM_CHANGE: {
    TEXT: 'Document Role05 Item|Edit',
    ROUTE: 'document-role05-change',
    LINK: '/document-role05-change/list'
  },
  DOCUMENT_ROLE05_ITEM_CONSIDER_DOCUMENT: {
    TEXT: 'Document Role05 Item|Edit',
    ROUTE: 'document-role05-consider-document',
    LINK: '/document-role05-consider-document/list'
  },
  DASHBOARD_PUBLIC_ROLE_01: {
    TEXT: 'Dashboard Public Role 01',
    ROUTE: 'dashboard-public-role-01',
    LINK: '/dashboard-public-role-01'
  },
  DASHBOARD_PUBLIC_ROLE_01_LIST: {
    TEXT: 'Dashboard Public Role 01|List',
    ROUTE: 'dashboard-public-role-01/list',
    LINK: '/dashboard-public-role-01/list'
  },
  DASHBOARD_PUBLIC_ROLE_01_EDIT: {
    TEXT: 'Dashboard Public Role 01|Edit',
    ROUTE: 'dashboard-public-role-01',
    LINK: '/dashboard-public-role-01'
  },



  DOCUMENT_ROLE04_DASHBOARD: {
    TEXT: 'Dashboard Public Role 01',
    ROUTE: 'document-role04-dashboard',
    LINK: '/document-role04-dashboard'
  },
  DOCUMENT_ROLE04_DASHBOARD_LIST: {
    TEXT: 'Dashboard Public Role 01|List',
    ROUTE: 'document-role04-dashboard',
    LINK: '/document-role04-dashboard'
  },
  DOCUMENT_ROLE04_DASHBOARD_EDIT: {
    TEXT: 'Dashboard Public Role 01|Edit',
    ROUTE: 'document-role04-dashboard',
    LINK: '/document-role04-dashboard'
  },



  DOCUMENT_ROLE05_CHANGE: {
    TEXT: 'Document Role05 Change',
    ROUTE: 'document-role05-change',
    LINK: '/document-role05-change'
  },
  DOCUMENT_ROLE05_CHANGE_LIST: {
    TEXT: 'Document Role05 Change|List',
    ROUTE: 'document-role05-change/list',
    LINK: '/document-role05-change/list'
  },
  DOCUMENT_ROLE05_CHANGE_EDIT: {
    TEXT: 'Document Role05 Change|Edit',
    ROUTE: 'document-role05-change',
    LINK: '/document-role05-change'
  },
  DOCUMENT_ROLE05_CONSIDER_DOCUMENT: {
    TEXT: 'Document Role05 Consider Document',
    ROUTE: 'document-role05-consider-document',
    LINK: '/document-role05-consider-document'
  },
  DOCUMENT_ROLE05_CONSIDER_DOCUMENT_LIST: {
    TEXT: 'Document Role05 Consider Document|List',
    ROUTE: 'document-role05-consider-document/list',
    LINK: '/document-role05-consider-document/list'
  },
  DOCUMENT_ROLE05_CONSIDER_DOCUMENT_EDIT: {
    TEXT: 'Document Role05 Consider Document|Edit',
    ROUTE: 'document-role05-consider-document',
    LINK: '/document-role05-consider-document'
  },
  DASHBOARD_PUBLIC_ROLE_04: {
    TEXT: 'Dashboard Public Role 04',
    ROUTE: 'dashboard-public-role-04',
    LINK: '/dashboard-public-role-04'
  },
  DASHBOARD_PUBLIC_ROLE_04_LIST: {
    TEXT: 'Dashboard Public Role 04|List',
    ROUTE: 'dashboard-public-role-04/list',
    LINK: '/dashboard-public-role-04/list'
  },
  DASHBOARD_PUBLIC_ROLE_04_EDIT: {
    TEXT: 'Dashboard Public Role 04|Edit',
    ROUTE: 'dashboard-public-role-04',
    LINK: '/dashboard-public-role-04'
  },
  DOCUMENT_ROLE04_ITEM: {
    TEXT: 'Document Role04 Item',
    ROUTE: 'document-role04-item',
    LINK: '/document-role04-item'
  },
  DOCUMENT_ROLE04_ITEM_LIST: {
    TEXT: 'Document Role04 Item|List',
    ROUTE: 'document-role04-item/list',
    LINK: '/document-role04-item/list'
  },
  DOCUMENT_ROLE04_ITEM_EDIT: {
    TEXT: 'Document Role04 Item|Edit',
    ROUTE: 'document-role04-item',
    LINK: '/document-role04-item'
  },
  DOCUMENT_ROLE04_CHANGE: {
    TEXT: 'Document Role04 Change',
    ROUTE: 'document-role04-change',
    LINK: '/document-role04-change'
  },
  DOCUMENT_ROLE04_CHANGE_LIST: {
    TEXT: 'Document Role04 Change|List',
    ROUTE: 'document-role04-change/list',
    LINK: '/document-role04-change/list'
  },
  DOCUMENT_ROLE04_CHANGE_EDIT: {
    TEXT: 'Document Role04 Change|Edit',
    ROUTE: 'document-role04-change',
    LINK: '/document-role04-change'
  },
  DOCUMENT_PROCESS_CHANGE: {
    TEXT: 'Document Process Change',
    ROUTE: 'document-process-change',
    LINK: '/document-process-change'
  },
  DOCUMENT_PROCESS_CHANGE_LIST: {
    TEXT: 'Document Process Change|List',
    ROUTE: 'document-process-change/list',
    LINK: '/document-process-change/list'
  },
  DOCUMENT_PROCESS_CHANGE_EDIT: {
    TEXT: 'Document Process Change|Edit',
    ROUTE: 'document-process-change',
    LINK: '/document-process-change'
  },
  DOCUMENT_PROCESS_COLLECT: {
    TEXT: 'Document Process Collect',
    ROUTE: 'document-process-collect',
    LINK: '/document-process-collect'
  },
  DOCUMENT_PROCESS_COLLECT_LIST: {
    TEXT: 'Document Process Collect|List',
    ROUTE: 'document-process-collect/list',
    LINK: '/document-process-collect/list'
  },
  DOCUMENT_PROCESS_COLLECT_EDIT: {
    TEXT: 'Document Process Collect|Edit',
    ROUTE: 'document-process-collect',
    LINK: '/document-process-collect'
  },
  DASHBOARD_DOCUMENT_ROLE_01: {
    TEXT: 'Dashboard Document Role 01',
    ROUTE: 'dashboard-document-role-01',
    LINK: '/dashboard-document-role-01'
  },
  DASHBOARD_DOCUMENT_ROLE_01_LIST: {
    TEXT: 'Dashboard Document Role 01|List',
    ROUTE: 'dashboard-document-role-01/list',
    LINK: '/dashboard-document-role-01/list'
  },
  DASHBOARD_DOCUMENT_ROLE_01_EDIT: {
    TEXT: 'Dashboard Document Role 01|Edit',
    ROUTE: 'dashboard-document-role-01',
    LINK: '/dashboard-document-role-01'
  },
  CONSIDERING_SIMILAR_KOR10: {
    TEXT: 'Considering Similar Kor10',
    ROUTE: 'considering-similar-kor10',
    LINK: '/considering-similar-kor10'
  },
  CONSIDERING_SIMILAR_KOR10_LIST: {
    TEXT: 'Considering Similar Kor10|List',
    ROUTE: 'considering-similar-kor10/list',
    LINK: '/considering-similar-kor10/list'
  },
  CONSIDERING_SIMILAR_KOR10_EDIT: {
    TEXT: 'Considering Similar Kor10|Edit',
    ROUTE: 'considering-similar-kor10',
    LINK: '/considering-similar-kor10'
  },
  PUBLIC_ROLE02_DOCUMENT_PAYMENT: {
    TEXT: 'Public Role02 Document Payment',
    ROUTE: 'public-role02-document-payment',
    LINK: '/public-role02-document-payment'
  },
  PUBLIC_ROLE02_DOCUMENT_PAYMENT_LIST: {
    TEXT: 'Public Role02 Document Payment|List',
    ROUTE: 'public-role02-document-payment/list',
    LINK: '/public-role02-document-payment/list'
  },
  PUBLIC_ROLE02_DOCUMENT_PAYMENT_EDIT: {
    TEXT: 'Public Role02 Document Payment|Edit',
    ROUTE: 'public-role02-document-payment',
    LINK: '/public-role02-document-payment'
  },
  PUBLIC_ROLE02_CONSIDER_PAYMENT_EDIT: {
    TEXT: 'Public Role02 Consider Payment|Edit',
    ROUTE: 'public-role02-consider-payment',
    LINK: '/public-role02-consider-payment'
  },
  PUBLIC_ROLE05_CONSIDER_PAYMENT_EDIT: {
    TEXT: 'Public Role05 Consider Payment|Edit',
    ROUTE: 'public-role05-consider-payment',
    LINK: '/public-role05-consider-payment'
  },
  PUBLIC_ROLE04_CHECK: {
    TEXT: 'Public Role04 Check|List',
    ROUTE: 'public-role04-check',
    LINK: '/public-role04-check'
  },
  PUBLIC_ROLE04_CHECK_LIST: {
    TEXT: 'Public Role04 Check|List',
    ROUTE: 'public-role04-check/list',
    LINK: '/public-role04-check/list'
  },
  PUBLIC_ROLE04_CHECK_EDIT: {
    TEXT: 'Public Role04 Check|Edit',
    ROUTE: 'public-role04-check',
    LINK: '/public-role04-check'
  },
  PUBLIC_ROLE05_DOCUMENT: {
    TEXT: 'Public Role05 Document|List',
    ROUTE: 'public-role05-document',
    LINK: '/public-role05-document'
  },
  PUBLIC_ROLE05_DOCUMENT_LIST: {
    TEXT: 'Public Role05 Document|List',
    ROUTE: 'public-role05-document/list',
    LINK: '/public-role05-document/list'
  },
  PUBLIC_ROLE05_DOCUMENT_EDIT: {
    TEXT: 'Public Role05 Document|Edit',
    ROUTE: 'public-role05-document',
    LINK: '/public-role05-document'
  },
  PUBLIC_ROLE01_CASE41_CHECK: {
    TEXT: 'Public Role01 Case41 Check',
    ROUTE: 'public-role01-case41-check',
    LINK: '/public-role01-case41-check'
  },
  PUBLIC_ROLE01_CASE41_CHECK_LIST: {
    TEXT: 'Public Role01 Case41 Check|List',
    ROUTE: 'public-role01-case41-check/list',
    LINK: '/public-role01-case41-check/list'
  },
  PUBLIC_ROLE01_CASE41_CHECK_EDIT: {
    TEXT: 'Public Role01 Case41 Check|Edit',
    ROUTE: 'public-role01-case41-check',
    LINK: '/public-role01-case41-check'
  },
  DOCUMENT_ROLE04_CHECK: {
    TEXT: 'Document Role04 Check',
    ROUTE: 'document-role04-check',
    LINK: '/document-role04-check'
  },
  DOCUMENT_ROLE04_CHECK_LIST: {
    TEXT: 'Document Role04 Check|List',
    ROUTE: 'document-role04-check/list',
    LINK: '/document-role04-check/list'
  },
  DOCUMENT_ROLE04_CHECK_EDIT: {
    TEXT: 'Document Role04 Check|Edit',
    ROUTE: 'document-role04-check',
    LINK: '/document-role04-check'
  },
  DOCUMENT_ROLE05_CHECK: {
    TEXT: 'Document Role05 Check',
    ROUTE: 'document-role05-check',
    LINK: '/document-role05-check'
  },
  DOCUMENT_ROLE05_CHECK_LIST: {
    TEXT: 'Document Role05 Check|List',
    ROUTE: 'document-role05-check/list',
    LINK: '/document-role05-check/list'
  },
  DOCUMENT_ROLE05_CHECK_EDIT: {
    TEXT: 'Document Role05 Check|Edit',
    ROUTE: 'document-role05-check',
    LINK: '/document-role05-check'
  },
  DOCUMENT_ROLE05_RELEASE_REQUEST: {
    TEXT: 'Document Role05 Release Request',
    ROUTE: 'document-role05-release-request',
    LINK: '/document-role05-release-request'
  },
  DOCUMENT_ROLE05_RELEASE_REQUEST_LIST: {
    TEXT: 'Document Role05 Release Request|List',
    ROUTE: 'document-role05-release-request/list',
    LINK: '/document-role05-release-request/list'
  },
  DOCUMENT_ROLE05_RELEASE_REQUEST_EDIT: {
    TEXT: 'Document Role05 Release Request|Edit',
    ROUTE: 'document-role05-release-request',
    LINK: '/document-role05-release-request'
  },
  DOCUMENT_ROLE04_CREATE: {
    TEXT: 'Document Role04 Create',
    ROUTE: 'document-role04-create',
    LINK: '/document-role04-create'
  },
  DOCUMENT_ROLE04_CREATE_LIST: {
    TEXT: 'Document Role04 Create|List',
    ROUTE: 'document-role04-create/list',
    LINK: '/document-role04-create/list'
  },
  DOCUMENT_ROLE04_CREATE_EDIT: {
    TEXT: 'Document Role04 Create|Edit',
    ROUTE: 'document-role04-create',
    LINK: '/document-role04-create'
  },
  DOCUMENT_ROLE05_CREATE: {
    TEXT: 'Document Role05 Create',
    ROUTE: 'document-role05-create',
    LINK: '/document-role05-create'
  },
  DOCUMENT_ROLE05_CREATE_LIST: {
    TEXT: 'Document Role05 Create|List',
    ROUTE: 'document-role05-create/list',
    LINK: '/document-role05-create/list'
  },
  DOCUMENT_ROLE05_CREATE_EDIT: {
    TEXT: 'Document Role05 Create|Edit',
    ROUTE: 'document-role05-create',
    LINK: '/document-role05-create'
  },
  DOCUMENT_ROLE04_RELEASE: {
    TEXT: 'Document Role04 Release',
    ROUTE: 'document-role04-release',
    LINK: '/document-role04-release'
  },
  DOCUMENT_ROLE04_RELEASE_LIST: {
    TEXT: 'Document Role04 Release|List',
    ROUTE: 'document-role04-release/list',
    LINK: '/document-role04-release/list'
  },
  DOCUMENT_ROLE04_RELEASE_EDIT: {
    TEXT: 'Document Role04 Release|Edit',
    ROUTE: 'document-role04-release',
    LINK: '/document-role04-release'
  },
  DOCUMENT_ROLE04_RELEASE_20: {
    TEXT: 'Document Role04 Release 20',
    ROUTE: 'document-role04-release-20',
    LINK: '/document-role04-release-20'
  },
  DOCUMENT_ROLE04_RELEASE_20_LIST: {
    TEXT: 'Document Role04 Release 20|List',
    ROUTE: 'document-role04-release-20/list',
    LINK: '/document-role04-release-20/list'
  },
  DOCUMENT_ROLE04_RELEASE_20_EDIT: {
    TEXT: 'Document Role04 Release 20|Edit',
    ROUTE: 'document-role04-release-20',
    LINK: '/document-role04-release-20'
  },
  DOCUMENT_ROLE05_RELEASE_20: {
    TEXT: 'Document Role05 Release 20',
    ROUTE: 'document-role05-release-20',
    LINK: '/document-role05-release-20'
  },
  DOCUMENT_ROLE05_RELEASE_20_LIST: {
    TEXT: 'Document Role05 Release 20|List',
    ROUTE: 'document-role05-release-20/list',
    LINK: '/document-role05-release-20/list'
  },
  DOCUMENT_ROLE05_RELEASE_20_EDIT: {
    TEXT: 'Document Role05 Release 20|Edit',
    ROUTE: 'document-role05-release-20',
    LINK: '/document-role05-release-20'
  },
  DOCUMENT_ROLE05_RELEASE: {
    TEXT: 'Document Role05 Release',
    ROUTE: 'document-role05-release',
    LINK: '/document-role05-release'
  },
  DOCUMENT_ROLE05_RELEASE_LIST: {
    TEXT: 'Document Role05 Release|List',
    ROUTE: 'document-role05-release/list',
    LINK: '/document-role05-release/list'
  },
  DOCUMENT_ROLE05_RELEASE_EDIT: {
    TEXT: 'Document Role05 Release|Edit',
    ROUTE: 'document-role05-release',
    LINK: '/document-role05-release'
  },
  DOCUMENT_ROLE03_ITEM: {
    TEXT: 'Document Role03 Item',
    ROUTE: 'document-role03-item',
    LINK: '/document-role03-item'
  },
  DOCUMENT_ROLE03_ITEM_LIST: {
    TEXT: 'Document Role03 Item|List',
    ROUTE: 'document-role03-item/list',
    LINK: '/document-role03-item/list'
  },
  DOCUMENT_ROLE03_ITEM_EDIT: {
    TEXT: 'Document Role03 Item|Edit',
    ROUTE: 'document-role03-item',
    LINK: '/document-role03-item'
  },
  REQUEST_EFORM: {
    TEXT: 'Request Eform',
    ROUTE: 'request-eform',
    LINK: '/request-eform'
  },
  REQUEST_EFORM_LIST: {
    TEXT: 'Request Eform|List',
    ROUTE: 'request-eform/list',
    LINK: '/request-eform/list'
  },
  REQUEST_EFORM_EDIT: {
    TEXT: 'Request Eform|Edit',
    ROUTE: 'request-eform',
    LINK: '/request-eform'
  },
  REQUEST_OTHER_EFORM: {
    TEXT: 'Request Other Eform',
    ROUTE: 'request-other-eform',
    LINK: '/request-other-eform'
  },
  REQUEST_OTHER_EFORM_LIST: {
    TEXT: 'Request Other Eform|List',
    ROUTE: 'request-other-eform/list',
    LINK: '/request-other-eform/list'
  },
  REQUEST_OTHER_EFORM_EDIT: {
    TEXT: 'Request Other Eform|Edit',
    ROUTE: 'request-other-eform',
    LINK: '/request-other-eform'
  },
  PUBLIC_ROLE05_OTHER: {
    TEXT: 'Public Role05 Other',
    ROUTE: 'public-role05-other',
    LINK: '/public-role05-other'
  },
  PUBLIC_ROLE05_OTHER_LIST: {
    TEXT: 'Public Role05 Other|List',
    ROUTE: 'public-role05-other/list',
    LINK: '/public-role05-other/list'
  },
  PUBLIC_ROLE05_OTHER_EDIT: {
    TEXT: 'Public Role05 Other|Edit',
    ROUTE: 'public-role05-other',
    LINK: '/public-role05-other'
  },
  PUBLIC_ROLE02_OTHER: {
    TEXT: 'Public Role02 Other',
    ROUTE: 'public-role02-other',
    LINK: '/public-role02-other'
  },
  PUBLIC_ROLE02_OTHER_LIST: {
    TEXT: 'Public Role02 Other|List',
    ROUTE: 'public-role02-other/list',
    LINK: '/public-role02-other/list'
  },
  PUBLIC_ROLE02_OTHER_EDIT: {
    TEXT: 'Public Role02 Other|Edit',
    ROUTE: 'public-role02-other',
    LINK: '/public-role02-other'
  },
  PUBLIC_ROLE02_CONSIDERING_OTHER: {
    TEXT: 'Public Role02 Considering Other',
    ROUTE: 'public-role02-considering-other',
    LINK: '/public-role02-considering-other'
  },
  PUBLIC_ROLE02_CONSIDERING_OTHER_LIST: {
    TEXT: 'Public Role02 Considering Other|List',
    ROUTE: 'public-role02-considering-other/list',
    LINK: '/public-role02-considering-other/list'
  },
  PUBLIC_ROLE02_CONSIDERING_OTHER_EDIT: {
    TEXT: 'Public Role02 Considering Other|Edit',
    ROUTE: 'public-role02-considering-other',
    LINK: '/public-role02-considering-other'
  },
  MADRID_ROLE01_IMPORT: {
    TEXT: 'Madrid Role01 import',
    ROUTE: 'madrid-role01-import',
    LINK: '/madrid-role01-import'
  },
  MADRID_ROLE01_IMPORT_SEARCH: {
    TEXT: 'Madrid Role01 Import Search ',
    ROUTE: 'madrid-role01-import-search',
    LINK: '/madrid-role01-import-search'
  },
  MADRID_ROLE01_MM02: {
    TEXT: 'Madrid Role01 MM02 ',
    ROUTE: 'madrid-role01-mm02',
    LINK: '/madrid-role01-mm02'
  },
  MADRID_ROLE02_DASHBOARD: {
    TEXT: 'Madrid Role02 Dashboard',
    ROUTE: 'madrid-role02-dashboard',
    LINK: '/madrid-role02-dashboard'
  },
  MADRID_ROLE02_SEARCH: {
    TEXT: 'Madrid Role02 Search',
    ROUTE: 'madrid-role02-search',
    LINK: '/madrid-role02-search'
  },
  MADRID_ROLE02_SEARCH_REGIS: {
    TEXT: 'Madrid Role02 Regis',
    ROUTE: 'madrid-role02-regis/list',
    LINK: '/madrid-role02-regis/list'
  },
  MADRID_ROLE02_SEARCH_EDIT: {
    TEXT: 'Madrid Role02 Regis',
    ROUTE: 'madrid-role02-edit/list',
    LINK: '/madrid-role02-edit/list'
  },
  MADRID_ROLE03_DASHBOARD: {
    TEXT: 'Madrid Role03 Dashboard',
    ROUTE: 'madrid-role03-dashboard',
    LINK: '/madrid-role03-dashboard'
  },
  MADRID_ROLE03_CHECK: {
    TEXT: 'Madrid Role03 Check',
    ROUTE: 'madrid-role03-check',
    LINK: '/madrid-role03-check'
  },

  MADRID_ROLE03_DT07_03: {
    TEXT: 'Madrid DT07_03',
    ROUTE: 'madrid-DT07-03',
    LINK: '/madrid-DT07-03'
  },

  MADRID_ROLE03_SEARCH: {
    TEXT: 'Madrid Role03 Search',
    ROUTE: 'madrid-role03-search',
    LINK: '/madrid-role03-search'
  },
  MADRID_ROLE03_SEARCH_GROUP_LEADER: {
    TEXT: 'Madrid Role03 Search Group Leader',
    ROUTE: 'madrid-role03-search-group-leader',
    LINK: '/madrid-role03-search-group-leader'
  },
  MADRID_ROLE03_SEARCH_REGIS: {
    TEXT: 'Madrid Role03 Search Regis',
    ROUTE: 'madrid-role03-search-regis',
    LINK: '/madrid-role03-search-regis'
  },
  MADRID_ROLE03_SEARCH_LIST: {
    TEXT: 'Madrid Role03 Search List',
    ROUTE: 'madrid-role03-search-list',
    LINK: '/madrid-role03-search-list'
  },
  MADRID_ROLE03_SEARCH_FINISH_WORK: {
    TEXT: 'Madrid Role03 Search Finish Work',
    ROUTE: 'madrid-role03-search-finish-work',
    LINK: '/madrid-role03-search-finish-work'
  },
  MADRID_ROLE03_SEARCH_WIPO: {
    TEXT: 'Madrid Role03 Search Wipo',
    ROUTE: 'madrid-role03-search-wipo',
    LINK: '/madrid-role03-search-wipo'
  },
  MADRID_ROLE03_SEARCH_WIPO_CHANGE: {
    TEXT: 'Madrid Role03 Search Wipo Change',
    ROUTE: 'madrid-role03-search-wipo-change',
    LINK: '/madrid-role03-search-wipo-change'
  },

  MADRID_ROLE03_SEARCH_CONSIDER: {
    TEXT: 'Madrid Role03 Search Consider',
    ROUTE: 'madrid-role03-search-consider',
    LINK: '/madrid-role03-search-consider'
  },
  MADRID_ROLE03_SEARCH_REQUEST: {
    TEXT: 'Madrid Role03 Search Request',
    ROUTE: 'madrid-role03-request/list',
    LINK: '/madrid-role03-request/list'
  },
  MADRID_ROLE02_CHECK2: {
    TEXT: 'Madrid Role02 Check2',
    ROUTE: 'madrid-role02-check2/list',
    LINK: '/madrid-role02-check2/list'
  },
  MADRID_ROLE02_WAIT: {
    TEXT: 'Madrid Role02 Wait',
    ROUTE: 'madrid-role02-wait/list',
    LINK: '/madrid-role02-wait/list'
  },
  MADRID_ROLE02_CHANGE: {
    TEXT: 'Madrid Role02 Change',
    ROUTE: 'madrid-role02-change/list',
    LINK: '/madrid-role02-change/list'
  },
  MADRID_ROLE02_ANNOUNCE: {
    TEXT: 'Madrid Role02 Announce',
    ROUTE: 'madrid-role02-announce/list',
    LINK: '/madrid-role02-announce/list'
  },
  PUBLIC_ROLE03_RELEASE: {
    TEXT: 'Public Role03 Release',
    ROUTE: 'public-role03-release',
    LINK: '/public-role03-release'
  },
  PUBLIC_ROLE03_RELEASE_LIST: {
    TEXT: 'Public Role03 Release|List',
    ROUTE: 'public-role03-release/list',
    LINK: '/public-role03-release/list'
  },
  PUBLIC_ROLE03_RELEASE_EDIT: {
    TEXT: 'Public Role03 Release|Edit',
    ROUTE: 'public-role03-release',
    LINK: '/public-role03-release'
  },
  MADRID_ROLE02_REJECT: {
    TEXT: 'Madrid Role02 Reject',
    ROUTE: 'madrid-role02-reject/list',
    LINK: '/madrid-role02-reject/list'
  },
  MADRID_ROLE02_OTHER: {
    TEXT: 'Madrid Role02 Other',
    ROUTE: 'madrid-role02-other/list',
    LINK: '/madrid-role02-other/list'
  },
  MADRID_ROLE02_REGIS_INTER: {
    TEXT: 'Madrid Role02 Other',
    ROUTE: 'madrid-role02-regis-inter',
    LINK: '/madrid-role02-regis-inter'
  },
  MADRID_ROLE02_REGIS_INTER_LIST: {
    TEXT: 'Madrid Role02 Other',
    ROUTE: 'madrid-role02-regis-inter/list',
    LINK: '/madrid-role02-regis-inter/list'
  },
  MADRID_ROLE02_REGIS_INTER_ADD: {
    TEXT: 'Madrid Role02 Register|Add',
    ROUTE: 'madrid-role02-regis-inter/add',
    LINK: '/madrid-role02-regis-inter/add'
  },
  MADRID_ROLE02_EDIT_INTER: {
    TEXT: 'Madrid Role02 Other',
    ROUTE: 'madrid-role02-edit-inter',
    LINK: '/madrid-role02-edit-inter'
  },
  MADRID_ROLE02_EXAMINATION: {
    TEXT: 'Madrid Role02 examination',
    ROUTE: 'madrid-role02-examination',
    LINK: '/madrid-role02-examination'
  },
  MADRID_ROLE02_EDIT_INTER_LIST: {
    TEXT: 'Madrid Role02 Other',
    ROUTE: 'madrid-role02-edit-inter/list',
    LINK: '/madrid-role02-edit-inter/list'
  },
  MADRID_ROLE02_EDIT_INTER_ADD: {
    TEXT: 'Madrid Role02 Edit|Add',
    ROUTE: 'madrid-role02-edit-inter/add',
    LINK: '/madrid-role02-edit-inter/add'
  },
  MADRID_ROLE04_DASHBOARD: {
    TEXT: 'Madrid Role04 Dashboard',
    ROUTE: 'madrid-role04-dashboard',
    LINK: '/madrid-role04-dashboard'
  },
  MADRID_ROLE04_SEARCH: {
    TEXT: 'Madrid Role04 Search',
    ROUTE: 'madrid-role04-search',
    LINK: '/madrid-role04-search'
  },
  MADRID_ROLE04_CHECK: {
    TEXT: 'Madrid Role04 Check',
    ROUTE: 'madrid-role04-check',
    LINK: '/madrid-role04-check'
  },
  MADRID_ROLE04_SEARCH_CHANGE: {
    TEXT: 'Madrid Role04 Search Change',
    ROUTE: 'madrid-role04-search-change',
    LINK: '/madrid-role04-search-change'
  },
  MADRID_ROLE04_SEARCH_WIPO: {
    TEXT: 'Madrid Role04 Search WIPO',
    ROUTE: 'madrid-role04-search-wipo',
    LINK: '/madrid-role04-search-wipo'
  },
  MADRID_ROLE04_SEARCH_WIPO_CHANGE: {
    TEXT: 'Madrid Role04 Search WIPO Change',
    ROUTE: 'madrid-role04-search-wipo-change',
    LINK: '/madrid-role04-search-wipo-change'
  },
  MADRID_ROLE04_SEARCH_REGIS: {
    TEXT: 'Madrid Role04 Search WIPO Change',
    ROUTE: 'madrid-role04-search-regis',
    LINK: '/madrid-role04-search-regis'
  },
  MADRID_ROLE04_SEARCH_LIST: {
    TEXT: 'Madrid Role04 Search List',
    ROUTE: 'madrid-role04-search-list',
    LINK: '/madrid-role04-search-list'
  },
  MADRID_ROLE04_SEARCH_GROUP_LEADER: {
    TEXT: 'Madrid Role04 Search Group Leader',
    ROUTE: 'madrid-role04-search-group-leader',
    LINK: '/madrid-role04-search-group-leader'
  },
  MADRID_ROLE04_SEARCH_FINISH_WORK: {
    TEXT: 'Madrid Role04 Search Finish Work',
    ROUTE: 'madrid-role04-search-finish-work',
    LINK: '/madrid-role04-search-finish-work'
  },
  MADRID_ROLE05_DASHBOARD: {
    TEXT: 'Madrid Role05 Dashboard',
    ROUTE: 'madrid-role05-dashboard',
    LINK: '/madrid-role05-dashboard'
  },
  MADRID_ROLE05_SEARCH: {
    TEXT: 'Madrid Role05 Search',
    ROUTE: 'madrid-role05-search',
    LINK: '/madrid-role05-search'
  },
  MADRID_ROLE05_REQUEST_IN: {
    TEXT: 'Madrid Role05 Request In',
    ROUTE: 'madrid-role05-request-in',
    LINK: '/madrid-role05-request-in'
  },
  MADRID_ROLE05_FINISH: {
    TEXT: 'Madrid Role05 Finish',
    ROUTE: 'madrid-role05-finish',
    LINK: '/madrid-role05-finish'
  },
  MADRID_ROLE05_WORK_PAYMENT: {
    TEXT: 'Madrid Role05 Work Payment',
    ROUTE: 'madrid-role05-work-payment',
    LINK: '/madrid-role05-work-payment'
  },
  MADRID_ROLE05_DASHBOARD_INCOMING_ALL: {
    TEXT: 'Madrid Role05 Search Incoming All',
    ROUTE: 'madrid-role05-search-incoming-all',
    LINK: '/madrid-role05-search-incoming-all'
  },
  MADRID_ROLE05_MODAL: {
    TEXT: 'Madrid Role05 Modal',
    ROUTE: 'madrid-role05-modal',
    LINK: '/madrid-role05-modal'
  },
  MADRID_ROLE05_DASHBOARD_OUT: {
    TEXT: 'Madrid Role05 Dashboard Out',
    ROUTE: 'madrid-role05-dashboard/out',
    LINK: '/madrid-role05-dashboard/out'
  },
  MADRID_ROLE05_DASHBOARD_JOP_TRANSFER: {
    TEXT: 'Madrid Role05 Jop Transfer',
    ROUTE: 'madrid-role05-jop-transfer',
    LINK: '/madrid-role05-jop-transfer'
  },
  MADRID_ROLE05_WORKBOX_WIPO: {
    TEXT: 'Madrid Role05 Workbox Wipo',
    ROUTE: 'madrid-role05-workbox-wipo',
    LINK: '/madrid-role05-workbox-wipo'
  },
  MADRID_ROLE05_BOX_REF: {
    TEXT: 'Madrid Role05 Box Ref',
    ROUTE: 'madrid-role05-box-ref',
    LINK: '/madrid-role05-box-ref'
  },
  MADRID_ROLE06_DASHBOARD: {
    TEXT: 'Madrid Role06 Dashboard',
    ROUTE: 'madrid-role06-dashboard',
    LINK: '/madrid-role06-dashboard'
  },
  MADRID_ROLE06_NI01_LIST: {
    TEXT: 'Madrid Role06 NI01 List',
    ROUTE: 'madrid-role06-ni01-list',
    LINK: '/madrid-role06-ni01-list'
  },
  MADRID_ROLE06_NI02_LIST: {
    TEXT: 'Madrid Role06 NI02 List',
    ROUTE: 'madrid-role06-ni02-list',
    LINK: '/madrid-role06-ni02-list'
  },
  MADRID_ROLE06_NI03_LIST: {
    TEXT: 'Madrid Role06 NI03 List',
    ROUTE: 'madrid-role06-ni03-list',
    LINK: '/madrid-role06-ni03-list'
  },
  MADRID_ROLE06_OT02_LIST: {
    TEXT: 'Madrid Role06 OT02 List',
    ROUTE: 'madrid-role06-ot02-list',
    LINK: '/madrid-role06-ot02-list'
  },
  MADRID_ROLE06_OT03_LIST: {
    TEXT: 'Madrid Role06 OT03 List',
    ROUTE: 'madrid-role06-ot03-list',
    LINK: '/madrid-role06-ot03-list'
  },
  MADRID_ROLE06_OT04_LIST: {
    TEXT: 'Madrid Role06 OT04 List',
    ROUTE: 'madrid-role06-ot04-list',
    LINK: '/madrid-role06-ot04-list'
  },
  MADRID_ROLE07_SAVE_LIST: {
    TEXT: 'Madrid Role07 Save List',
    ROUTE: 'madrid-role07-save-list',
    LINK: '/madrid-role07-save-list'
  },
  MADRID_ROLE07_SEARCH_CANCEL: {
    TEXT: 'Madrid Role07 Search Cancel',
    ROUTE: 'madrid-role07-search-cancel',
    LINK: '/madrid-role07-search-cancel'
  },
  MADRID_ROLE07_SEARCH_EDIT: {
    TEXT: 'Madrid Role07 Search Edit',
    ROUTE: 'madrid-role07-search-edit',
    LINK: '/madrid-role07-search-edit'
  },
  MADRID_ROLE07_SEARCH_CHANGE: {
    TEXT: 'Madrid Role07 Search Change',
    ROUTE: 'madrid-role07-search-change',
    LINK: '/madrid-role07-search-change'
  },
  MADRID_ROLE07_SEARCH_EDIT_CHANGE: {
    TEXT: 'Madrid Role07 Search Edit Change',
    ROUTE: 'madrid-role07-search-edit-change',
    LINK: '/madrid-role07-search-edit-change'
  },
  MM06: {
    TEXT: 'Mm06',
    ROUTE: 'mm06',
    LINK: '/mm06'
  },
  MM05: {
    TEXT: 'Mm05',
    ROUTE: 'mm05',
    LINK: '/mm05'
  },
  MM07: {
    TEXT: 'Mm07',
    ROUTE: 'mm07',
    LINK: '/mm07'
  },
  MM08: {
    TEXT: 'Mm08',
    ROUTE: 'mm08',
    LINK: '/mm08'
  },
  MM09: {
    TEXT: 'Mm09',
    ROUTE: 'mm09',
    LINK: '/mm09'
  },
  MM02: {
    TEXT: 'Mm02',
    ROUTE: 'mm02',
    LINK: '/mm02'
  },
  MM04: {
    TEXT: 'Mm04',
    ROUTE: 'mm04',
    LINK: '/mm04'
  },
  MADRID_ROLE07_SEARCH_NOTIFY: {
    TEXT: 'Madrid Role07 Search Notify',
    ROUTE: 'madrid-role07-search-notify',
    LINK: '/madrid-role07-search-notify'
  },
  MADRID_ROLE07_SEARCH_RECIEVE_IRN: {
    TEXT: 'Madrid Role07 Search Recieve IRN',
    ROUTE: 'madrid-role07-search-recieve-irn',
    LINK: '/madrid-role07-search-recieve-irn'
  },
  MADRID_ROLE07_SEARCH_WAIT: {
    TEXT: 'Madrid Role07 Search Wait',
    ROUTE: 'madrid-role07-search-wait',
    LINK: '/madrid-role07-search-wait'
  },
  MADRID_ROLE07_DOC_EDIT: {
    TEXT: 'Madrid Role07 Doc Edit',
    ROUTE: 'madrid-role07-doc-edit',
    LINK: '/madrid-role07-doc-edit'
  },
  MADRID_ROLE07_SAVE_LIST_STEP1: {
    TEXT: 'Madrid Role07 Save List Step1',
    ROUTE: 'madrid-role07-save-list-step1',
    LINK: '/madrid-role07-save-list-step1'
  },
  MADRID_ROLE06_NI04_LIST: {
    TEXT: 'Madrid Role06 NI04 List',
    ROUTE: 'madrid-role06-ni04-list',
    LINK: '/madrid-role06-ni04-list'
  },
  MADRID_ROLE06_NI05_LIST: {
    TEXT: 'Madrid Role06 NI05 List',
    ROUTE: 'madrid-role06-ni05-list',
    LINK: '/madrid-role06-ni05-list'
  },
  MADRID_ROLE06_NI06_LIST: {
    TEXT: 'Madrid Role06 NI06 List',
    ROUTE: 'madrid-role06-ni06-list',
    LINK: '/madrid-role06-ni06-list'
  },
  MADRID_ROLE02_MR2: {
    TEXT: 'Madrid Role02 MR2',
    ROUTE: 'madrid-role02-mr2',
    LINK: '/madrid-role02-mr2'
  },

  INCLUDE_ROLE01_CHANGE: {
    TEXT: "Include Role01 Change",
    ROUTE: "include-role01-change",
    LINK: "/include-role01-change",
  },
  INCLUDE_ROLE01_CHANGE_LIST: {
    TEXT: "Include Role01 Change|List",
    ROUTE: "include-role01-change/list",
    LINK: "/include-role01-change/list",
  },
  REQUEST_KOR06_CHANGE: {
    TEXT: "Request Kor06 Change",
    ROUTE: "request-kor06-change",
    LINK: "/request-kor06-change",
  },
  DOCUMENT_ROLE02_ACTION_POST: {
    TEXT: 'Document Role02 Action Post',
    ROUTE: 'document-role02-action-post',
    LINK: '/document-role02-action-post'
  },
  DOCUMENT_ROLE02_ACTION_POST_LIST: {
    TEXT: 'Document Role02 Action Post|List',
    ROUTE: 'document-role02-action-post/list',
    LINK: '/document-role02-action-post/list'
  },
  DOCUMENT_ROLE02_ACTION_POST_EDIT: {
    TEXT: 'Document Role02 Action Post|Edit',
    ROUTE: 'document-role02-action-post',
    LINK: '/document-role02-action-post'
  },
  REQUEST_KOR04_CHANGE: {
    TEXT: 'Request Kor04 Change',
    ROUTE: 'request-kor04-change',
    LINK: '/request-kor04-change'
  },
  REQUEST_KOR04_CHANGE_LIST: {
    TEXT: 'Request Kor04 Change|List',
    ROUTE: 'request-kor04-change/list',
    LINK: '/request-kor04-change/list'
  },
  REQUEST_KOR07_CHANGE: {
    TEXT: "Request Kor07 Change",
    ROUTE: "request-kor07-change",
    LINK: "/request-kor07-change",
  },
  REQUEST_KOR07_CHANGE_LIST: {
    TEXT: "Request Kor07 Change|List",
    ROUTE: "request-kor07-change/list",
    LINK: "/request-kor07-change/list",
  },
  CONSIDER_KOR04_REQUEST: {
    TEXT: 'Consider Kor04 Request',
    ROUTE: 'consider-kor04-request',
    LINK: '/consider-kor04-request'
  },
  CONSIDER_KOR04_REQUEST_EDIT: {
    TEXT: 'Consider Kor04 Request|Edit',
    ROUTE: 'consider-kor04-request/edit',
    LINK: '/consider-kor04-request/edit'
  },
  CONSIDER_KOR04_REQUEST_LIST: {
    TEXT: 'Consider Kor04 Request|List',
    ROUTE: 'consider-kor04-request/list',
    LINK: '/consider-kor04-request/list'
  },
  APPROVE_KOR20_DOCUMENT: {
    TEXT: 'Approve Kor20 Document',
    ROUTE: 'approve-kor20-document',
    LINK: '/approve-kor20-document'
  },
  CONSIDER_REVISING_REQUEST: {
    TEXT: 'Consider Revising Request',
    ROUTE: 'consider-revising-request',
    LINK: '/consider-revising-request'
  },
  CONSIDER_SECTION55_P2: {
    TEXT: 'Consider Section55 P2',
    ROUTE: 'consider-section55-p2',
    LINK: '/consider-section55-p2'
  },
  CONSIDER_SECTION55_P2_LIST: {
    TEXT: 'Consider Section55 P2|List',
    ROUTE: 'consider-section55-p2/list',
    LINK: '/consider-section55-p2/list'
  },
  CONSIDER_REVOCATION_SECTION56: {
    TEXT: 'Consider Section56',
    ROUTE: 'consider-revocation-section56',
    LINK: '/consider-revocation-section56'
  },
  CONSIDER_REVOCATION_SECTION56_LIST: {
    TEXT: 'Consider Section56|List',
    ROUTE: 'consider-revocation-section56/list',
    LINK: '/consider-revocation-section56/list'
  },
  CONSIDER_SECTION56: {
    TEXT: 'Consider Section56',
    ROUTE: 'consider-section56',
    LINK: '/consider-section56'
  },
  CONSIDER_SECTION56_LIST: {
    TEXT: 'Consider Section56|List',
    ROUTE: 'consider-section56/list',
    LINK: '/consider-section56/list'
  },
  CHECK_ISSUE_BOOK: {
    TEXT: 'Check Issue Book',
    ROUTE: 'check-issue-book',
    LINK: '/check-issue-book'
  },
  CHECK_ISSUE_BOOK_LIST: {
    TEXT: 'Check Issue Book|List',
    ROUTE: 'check-issue-book/list',
    LINK: '/check-issue-book/list'
  },
  CONSIDER_DOCUMENT: {
    TEXT: 'Consider Document',
    ROUTE: 'consider-document',
    LINK: '/consider-document'
  },
  CONSIDER_DOCUMENT_LIST: {
    TEXT: 'Consider Document|List',
    ROUTE: 'consider-document/List',
    LINK: '/consider-document/List'
  },
  ROLE2_REQUEST_KOR04_CHANGE: {
    TEXT: 'role2 Request Kor04 Change',
    ROUTE: 'r2-request-kor04-change',
    LINK: '/r2-request-kor04-change'
  },
  ROLE2_REQUEST_KOR06_CHANGE: {
    TEXT: 'role2 Request Kor06 Change',
    ROUTE: 'r2-request-kor06-change',
    LINK: '/r2-request-kor06-change'
  },
  ROLE2_REQUEST_KOR07_CHANGE: {
    TEXT: 'role2 Request Kor07 Change',
    ROUTE: 'r2-request-kor07-change',
    LINK: '/r2-request-kor07-change'
  },
  ROLE2_REQUEST_KOR20_CHANGE: {
    TEXT: 'role2 Request Kor20 Change',
    ROUTE: 'r2-request-kor20-change',
    LINK: '/r2-request-kor20-change'
  },
  REQUEST_KOR08_REGISTRATION: {
    TEXT: 'Request Kor08 Registration',
    ROUTE: 'request-kor08-registration',
    LINK: '/request-kor08-registration'
  },
  ROLE2_CONSIDER_LICENSE_REQUEST: {
    TEXT: 'Role2 Cconsider License Request',
    ROUTE: 'r2-consider-license-request',
    LINK: '/r2-consider-license-request'
  },
  ROLE2_CONSIDER_LICENSE_REQUEST_LIST: {
    TEXT: 'Role2 Cconsider License Request|List',
    ROUTE: 'r2-consider-license-request/list',
    LINK: '/r2-consider-license-request/list'
  },
  CONSIDER_ABANDON_PETITION: {
    TEXT: 'Consider Abandon Petition',
    ROUTE: 'consider-abandon-petition',
    LINK: '/consider-abandon-petition'
  },
  CONSIDER_ABANDON_PETITION_List: {
    TEXT: 'Consider Abandon Petition|List',
    ROUTE: 'consider-abandon-petitionlist',
    LINK: '/consider-abandon-petition/list'
  },
  CONSIDER_LICENSE_REQUEST: {
    TEXT: 'Consider License Request',
    ROUTE: 'consider-license-request',
    LINK: '/consider-license-request'
  },
  CONSIDER_LICENSE_REQUEST_LIST: {
    TEXT: 'Consider License Request|List',
    ROUTE: 'consider-license-request/list',
    LINK: '/consider-license-request/list'
  },
  ISSUE_IMPORTANT_DOCUMENTS: {
    TEXT: 'Issue Important Documents',
    ROUTE: 'issue-important-documents',
    LINK: '/issue-important-documents'
  },
  ISSUE_IMPORTANT_DOCUMENTS_LIST: {
    TEXT: 'Issue Important Documents|List',
    ROUTE: 'issue-important-documents/list',
    LINK: '/issue-important-documents/list'
  },
  RENEW: {
    TEXT: 'Renew',
    ROUTE: 'renew',
    LINK: '/renew'
  },
  REQUEST_PREVIOUS: {
    TEXT: 'Request Previous',
    ROUTE: 'request-previous',
    LINK: '/request-previous'
  },
  REQUEST_KOR04_PREVIOUS: {
    TEXT: 'Request Kor04 Previous',
    ROUTE: 'request-kor04-previous',
    LINK: '/request-kor04-previous'
  },
  REQUEST_KOR05_PREVIOUS: {
    TEXT: 'Request Kor05 Previous',
    ROUTE: 'request-kor05-previous',
    LINK: '/request-kor05-previous'
  },
  REQUEST_KOR06_PREVIOUS: {
    TEXT: 'Request Kor06 Previous',
    ROUTE: 'request-kor06-previous',
    LINK: '/request-kor06-previous'
  },
  REQUEST_KOR07_PREVIOUS: {
    TEXT: 'Request Kor07 Previous',
    ROUTE: 'request-kor07-previous',
    LINK: '/request-kor07-previous'
  },
  REQUEST_KOR08_PREVIOUS: {
    TEXT: 'Request Kor08 Previous',
    ROUTE: 'request-kor08-previous',
    LINK: '/request-kor08-previous'
  },
  REQUEST_KOR14_PREVIOUS: {
    TEXT: 'Request Kor14 Previous',
    ROUTE: 'request-kor14-previous',
    LINK: '/request-kor14-previous'
  },
  PRINT_LOG: {
    TEXT: 'Print Log',
    ROUTE: 'print-log',
    LINK: '/print-log'
  },
  PRINT_BOOK: {
    TEXT: 'Print Book',
    ROUTE: 'print-book',
    LINK: '/print-book'
  },
  PRINT_COVER: {
    TEXT: 'Print Cover',
    ROUTE: 'print-cover',
    LINK: '/print-cover'
  },
  CREATE_DELIVERY: {
    TEXT: 'Create Delivery',
    ROUTE: 'create-delivery',
    LINK: '/create-delivery'
  },
  SAVE_RECEIPT_POSTAL: {
    TEXT: 'Save Receipt Postal',
    ROUTE: 'save-receipt-postal',
    LINK: '/save-receipt-postal'
  },

  //APPEAL
  
  APPEAL_ROLE01_APPEAL_KOR03_SAVE_EDIT: {
    TEXT: 'Appeal Role01 Appeal Kor03 Save|Edit',
    ROUTE: 'appeal-role01-appeal-kor03-save/edit',
    LINK: '/appeal-role01-appeal-kor03-save/edit'
},
APPEAL_ROLE01_KOR_OTHER_COMMERCIAL_PROVINCE: {
    TEXT: 'Appeal Role01 Kor Other Commercial Province',
    ROUTE: 'appeal-role01-kor-other-commercial-province',
    LINK: '/appeal-role01-kor-other-commercial-province'
},
APPEAL_ROLE01_REGISTER_REVOKE_KOR08: {
    TEXT: 'Appeal Role01 Register Revoke Kor08',
    ROUTE: 'appeal-role01-register-revoke-kor08',
    LINK: '/appeal-role01-register-revoke-kor08'
},
APPEAL_ROLE01_KOR_OTHER_FLOOR3: {
    TEXT: 'Appeal Role01 Kor Other Floor3',
    ROUTE: 'appeal-role01-kor-other-floor3',
    LINK: '/appeal-role01-kor-other-floor3'
},
APPEAL_ROLE01_KOR_OTHER_FLOOR3_LIST: {
    TEXT: 'Appeal Role01 Kor Other Floor3|List',
    ROUTE: 'appeal-role01-kor-other-floor3/list',
    LINK: '/appeal-role01-kor-other-floor3/list'
},
APPEAL_ROLE01_APPEAL_KOR_ALL_LIST: {
  TEXT: 'Appeal Role01 Appeal Kor All List|List',
  ROUTE: 'appeal-role01-appeal-kor-all-list/list',
  LINK: '/appeal-role01-appeal-kor-all-list/list'
},


APPEAL_ROLE02_CASE_SUMMARY:{
    TEXT: 'Appeal Role02 Case Summary',
    ROUTE: 'appeal-role02-case-summary',
    LINK: '/appeal-role02-case-summary'
},
APPEAL_ROLE02_CASE_SUMMARY_EDIT:{
  TEXT: 'Appeal Role02 Case Summary|Edit',
  ROUTE: 'appeal-role02-case-summary/edit',
  LINK: '/appeal-role02-case-summary/edit'
},
APPEAL_ROLE02_CASE_SUMMARY_LIST:{
    TEXT: 'Appeal Role02 Case Summary|List',
    ROUTE: 'appeal-role02-case-summary/list',
    LINK: '/appeal-role02-case-summary/list'
},
APPEAL_ROLE02_REGISTER_CONSIDER_BOARD:{
    TEXT: 'Appeal Role02 Register Consider Board',
    ROUTE: 'appeal-role02-register-consider-board',
    LINK: '/appeal-role02-register-consider-board'
},
APPEAL_ROLE02_MEETING_REPORT:{
    TEXT: 'Appeal Role02 Meeting Report',
    ROUTE: 'appeal-role02-meeting-report',
    LINK: '/appeal-role02-meeting-report'
},
APPEAL_ROLE02_MEETING_REPORT_LIST:{
    TEXT: 'Appeal Role02 Meeting Repor|List',
    ROUTE: 'appeal-role02-meeting-report/list',
    LINK: '/appeal-role02-meeting-report/list'
},
APPEAL_ROLE02_BOARD_DECISION:{
    TEXT: 'Appeal Role02 Board Decision',
    ROUTE: 'appeal-role02-board-decision',
    LINK: '/appeal-role02-board-decision'
},
APPEAL_ROLE02_BOARD_DECISION_LIST:{
    TEXT: 'Appeal Role02 Board Decision|List',
    ROUTE: 'appeal-role02-board-decision/list',
    LINK: '/appeal-role02-board-decision/list'
},
APPEAL_ROLE02_REGISTER_BOOK_NOTIFY:{
    TEXT: 'Appeal Role02 Register Book Notify',
    ROUTE: 'appeal-role02-register-book-notify',
    LINK: '/appeal-role02-register-book-notify'
},
APPEAL_ROLE02_REGISTER_COMMENT:{
    TEXT: 'Appeal Role02 Register Comment',
    ROUTE: 'appeal-role02-register-comment',
    LINK: '/appeal-role02-register-comment'
},

APPEAL_ROLE03_REGISTER_COMMENT:{
    TEXT: 'Appeal Role03 Register Comment',
    ROUTE: 'appeal-role03-register-comment',
    LINK: '/appeal-role03-register-comment'
},
APPEAL_ROLE03_CASE_SUMMARY:{
    TEXT: 'Appeal Role03 Case Summary',
    ROUTE: 'appeal-role03-case-summary',
    LINK: '/appeal-role03-case-summary'
},
APPEAL_ROLE03_CASE_SUMMARY_LIST:{
    TEXT: 'Appeal Role03 Case Summary|List',
    ROUTE: 'appeal-role03-case-summary/list',
    LINK: '/appeal-role03-case-summary/list'
},
APPEAL_ROLE03_SEND_MEETING_AGENDA_LIST:{
    TEXT: 'Appeal Role03 Send Meeting Agenda|List',
    ROUTE: 'appeal-role03-send-meeting-agenda/list',
    LINK: '/appeal-role03-send-meeting-agenda/list'
},
APPEAL_ROLE03_REGISTER_CONSIDER_BOARD:{
  TEXT: 'Appeal Role03 Register Consider Board',
  ROUTE: 'appeal-role03-register-consider-board',
  LINK: '/appeal-role03-register-consider-board'
},
APPEAL_ROLE03_BOARD_DECISION_SUMMARY_LIST:{
    TEXT: 'Appeal Role03 Board Decision Summary|List',
    ROUTE: 'appeal-role03-board-decision-summary/list',
    LINK: '/appeal-role03-board-decision-summary/list'
},
APPEAL_ROLE03_MEETING_SUMMARY:{
    TEXT: 'Appeal Role03 Meeting Summary',
    ROUTE: 'appeal-role03-meeting-summary',
    LINK: '/appeal-role03-meeting-summary'
},
APPEAL_ROLE03_MEETING_SUMMARY_LIST:{
    TEXT: 'Appeal Role03 Meeting Summary|List',
    ROUTE: 'appeal-role03-meeting-summary/list',
    LINK: '/appeal-role03-meeting-summary/list'
},
APPEAL_ROLE03_CHECK_BEFORE_ISSUING_BOOK: {
    TEXT: 'Appeal Role03 Check Before Issuing Book',
    ROUTE: 'appeal-role03-check-before-issuing-book',
    LINK: '/appeal-role03-check-before-issuing-book'
},
APPEAL_ROLE03_CHECK_BEFORE_ISSUING_BOOK_LIST: {
    TEXT: 'Appeal Role03 Check Before Issuing Book|List',
    ROUTE: 'appeal-role03-check-before-issuing-book/list',
    LINK: '/appeal-role03-check-before-issuing-book/list'
},

APPEAL_ROLE04_CASE_SUMMARY_K03:{
    TEXT: 'Appeal Role04 Case Summary K03',
    ROUTE: 'appeal-role04-case-summary-k03',
    LINK: '/appeal-role04-case-summary-k03'
},
APPEAL_ROLE04_CASE_SUMMARY_K03_LIST:{
    TEXT: 'Appeal Role04 Case Summary K03|List',
    ROUTE: 'appeal-role04-case-summary-k03/list',
    LINK: '/appeal-role04-case-summary-k03/list'
},
APPEAL_ROLE04_BOARD_APPROVAL:{
    TEXT: 'Appeal Role04 Board Approval',
    ROUTE: 'appeal-role04-board-approval',
    LINK: '/appeal-role04-board-approval'
},
APPEAL_ROLE04_BOARD_APPROVAL_LIST:{
    TEXT: 'Appeal Role04 Board Approval|List',
    ROUTE: 'appeal-role04-board-approval/list',
    LINK: '/appeal-role04-board-approval/list'
},
APPEAL_ROLE04_DECISION_NUMBER_RELEASE:{
    TEXT: 'Appeal Role04 Decision Number Release',
    ROUTE: 'appeal-role04-decision-number-release',
    LINK: '/appeal-role04-decision-number-release'
},
APPEAL_ROLE04_DECISION_NUMBER_RELEASE_LIST:{
    TEXT: 'Appeal Role04 Decision Number Release|List',
    ROUTE: 'appeal-role04-decision-number-release/list',
    LINK: '/appeal-role04-decision-number-release/list'
},
APPEAL_ROLE04_ISSUING_BOOK_ROUND2:{
    TEXT: 'Appeal Role04 Issuing Book Round2',
    ROUTE: 'appeal-role04-issuing-book-round2',
    LINK: '/appeal-role04-issuing-book-round2'
},
APPEAL_ROLE04_ISSUING_BOOK_ROUND2_LIST:{
    TEXT: 'Appeal Role04 Issuing Book Round2|List',
    ROUTE: 'appeal-role04-issuing-book-round2/list',
    LINK: '/appeal-role04-issuing-book-round2/list'
},
APPEAL_ROLE04_DECISION_BOOK_COMMITTEE:{
    TEXT: 'Appeal Role04 Decision Book Committee',
    ROUTE: 'appeal-role04-decision-book-committee',
    LINK: '/appeal-role04-decision-book-committee'
},
APPEAL_ROLE04_DECISION_BOOK_COMMITTEE_LIST:{
    TEXT: 'Appeal Role04 Decision Book Committee|List',
    ROUTE: 'appeal-role04-decision-book-committee/list',
    LINK: '/appeal-role04-decision-book-committee/list'
},
APPEAL_ROLE04_RESPOND_BOOK_REVOKE:{
    TEXT: 'Appeal Role04 Respond Book Revoke',
    ROUTE: 'appeal-role04-respond-book-revoke',
    LINK: '/appeal-role04-respond-book-revoke'
},
APPEAL_ROLE04_RESPOND_BOOK_REVOKE_LIST:{
    TEXT: 'Appeal Role04 Respond Book Revoke|List',
    ROUTE: 'appeal-role04-respond-book-revoke/list',
    LINK: '/appeal-role04-respond-book-revoke/list'
},
APPEAL_ROLE04_BOOK_CONSIDER_BOARD_DECISION:{
  TEXT: 'Appeal Role04 Book Consider Board Decision',
  ROUTE: 'appeal-role04-book-consider-board-decision',
  LINK: '/appeal-role04-book-consider-board-decision'
},
APPEAL_ROLE04_BOOK_CONSIDER_BOARD_DECISION_LIST:{
  TEXT: 'Appeal Role04 Book Consider Board Decision|List',
  ROUTE: 'appeal-role04-book-consider-board-decision/list',
  LINK: '/appeal-role04-book-consider-board-decision/list'
},
APPEAL_ROLE04_RESPOND_BOOK_APPEAL:{
  TEXT: 'Appeal Role04 Respond Book Appeal',
  ROUTE: 'appeal-role04-respond-book-appeal',
  LINK: '/appeal-role04-respond-book-appeal'
},
APPEAL_ROLE04_RESPOND_BOOK_APPEAL_LIST:{
  TEXT: 'Appeal Role04 Respond Book Appeal|List',
  ROUTE: 'appeal-role04-respond-book-appeal/list',
  LINK: '/appeal-role04-respond-book-appeal/list'
},
APPEAL_ROLE04_PRINT_BOOK_LIST:{
  TEXT: 'Appeal Role04 Print Book|List',
  ROUTE: 'appeal-role04-print-book/list',
  LINK: '/appeal-role04-print-book/list'
},
APPEAL_ROLE04_PRINT_COVER_LIST:{
  TEXT: 'Appeal Role04 Print Cover|List',
  ROUTE: 'appeal-role04-print-cover/list',
  LINK: '/appeal-role04-print-cover/list'
},
APPEAL_ROLE04_CREATE_DELIVERY_NOTE_LIST:{
  TEXT: 'Appeal Role04 Create Delivery Note|List',
  ROUTE: 'appeal-role04-create-delivery-note/list',
  LINK: '/appeal-role04-create-delivery-note/list'
},
APPEAL_ROLE04_SAVE_POSTAL_RECEIPT_LIST:{
  TEXT: 'Appeal Role04 Save Postal Receipt|List',
  ROUTE: 'appeal-role04-save-postal-receipt/list',
  LINK: '/appeal-role04-save-postal-receipt/list'
},

APPEAL_ROLE05_CERTIFIED_MEETING_REPORT:{
    TEXT: 'Appeal Role05 Certified Meeting Report',
    ROUTE: 'appeal-role05-certified-meeting-report',
    LINK: '/appeal-role05-certified-meeting-report'
},
APPEAL_ROLE05_CERTIFIED_MEETING_REPORT_LIST:{
    TEXT: 'Appeal Role05 Certified Meeting Report|List',
    ROUTE: 'appeal-role05-certified-meeting-report/list',
    LINK: '/appeal-role05-certified-meeting-report/list'
},
APPEAL_ROLE05_APPROVAL_DRAFT_DECISION:{
    TEXT: 'Appeal Role05 Approval Draft Decision',
    ROUTE: 'appeal-role05-approval-draft-decision',
    LINK: '/appeal-role05-approval-draft-decision'
},
APPEAL_ROLE05_APPROVAL_DRAFT_DECISION_LIST:{
    TEXT: 'Appeal Role05 Approval Draft Decision|List',
    ROUTE: 'appeal-role05-approval-draft-decision/list',
    LINK: '/appeal-role05-approval-draft-decision/list'
}
}

// TODO Refactor

export const ROOT_URL =
  window.location.protocol + "//" + window.location.host + "/";

export const LOGIN_URL =
  "https://sso.ipthailand.go.th/authorize?app_id=1700000019&redirect_url=" +
  ROOT_URL +
  "api/Auth/AuthCallBack?last_action=" +
  window.location.href.replace("api/Auth/AuthCallBack?last_action", "");

export const LOGOUT_URL =
  " https://sso.ipthailand.go.th/logout?app_id=1700000019&redirect_url=http://ipthailand.go.th";

export const SECRET_KEY = "DIP_SECRET_KEY";

export const LOCAL_STORAGE = {
  USER_AUTH: 'DIP_AUTH_KEY',
  USER_INFO: 'DIP_AUTH_INFO_KEY'
}

export const CONSTANTS = {
  TOAST: {
    ERROR: {
      action: 'error',
      header: 'เกิดข้อผิดพลาด',
      message: ''
    },
    SUCCESS: {
      action: 'success',
      header: 'สำเร็จ',
      message: ''
    }
  },

  PAGINATION: {
    INIT: {
      id: 'paginate',
      currentPage: 1,
      itemsPerPage: 10,
      totalItems: 0
    },
    PER_PAGE: [1, 10, 20, 30, 50, 100]
  },

  AUTOCOMPLETE: {
    item_per_page: 5
  },

  DELAY_CALL_API: 1000,
  DELAY_OPEN_POPUP_EFORM: 2500,

  UID: 1,
  GetUID(): number {
    return this.UID++
  }
}

export const VALIDATE = {
  validateAddRequest: [
    [
      'request_date',
      'isRequired',
      'กรุณาเลือก วันที่ยื่นคำขอ'
    ],
    [
      'requester_name',
      'isRequired',
      //'isRequired | isFullName',
      'กรุณากรอก ผู้ยื่นคำขอ'
      //'กรุณากรอก ผู้ยื่นคำขอ | กรุณากรอก ตัวอักษรภาษาไทย'
    ],
    [
      'item_count',
      'isRequired | isCount',
      'กรุณากรอก จำนวนคำขอ | กรุณากรอก ตัวเลขมากกว่า 0'
    ],
    [
      'telephone',
      'isRequired',
      //'isRequired | isPhone',
      'กรุณากรอก เบอร์โทร'
      //'กรุณากรอก เบอร์โทร | เบอร์โทรไม่ถูกต้องตามรูปแบบที่กำหนด'
    ]
  ],
  validateRequestOtherSave: [
    //[
    //    'request_date',
    //    'isRequired',
    //    'กรุณาเลือก วันที่ยื่นคำขอ'
    //],
    [
      'requester_name',
      'isRequired',
      //'isRequired | isFullName',
      'กรุณากรอก ผู้ยื่นคำขอ'
      //'กรุณากรอก ผู้ยื่นคำขอ | กรุณากรอก ตัวอักษรภาษาไทย'
    ],
    [
      'telephone',
      'isRequired',
      //'isRequired | isPhone',
      'กรุณากรอก เบอร์โทร'
      //'กรุณากรอก เบอร์โทร | เบอร์โทรไม่ถูกต้องตามรูปแบบที่กำหนด'
    ]
  ],
  validateLoadUnpaid: [
    [
      'reference_number',
      'isRequired',
      //'isRequired | isFullName',
      'กรุณากรอก เลขที่อ้างอิงการชำระเงิน'
      //'กรุณากรอก ผู้ยื่นคำขอ | กรุณากรอก ตัวอักษรภาษาไทย'
    ],
  ],
  validateSaveItemType: [
    //[
    //  'item_sub_type_1_code',
    //  'isRequired',
    //  'กรุณากรอก รหัสจำพวก'
    //],
    [
      'product_count',
      'isRequired | isCount',
      'กรุณากรอก จำนวนสินค้า | กรุณากรอก ตัวเลขมากกว่า 0'
    ]
  ],
  validateSearchReceipt: [
    [
      'reference_number',
      'isRequired',
      'กรุณากรอก เลขที่อ้างอิงการชำระเงิน'
    ]
  ],
  validateRemark: [
    [
      'remark',
      'isRequired ',
      'กรุณากรอกรายละเอียด '
    ]

  ],
  validateRemark7: [
    [
      'remark_7',
      'isRequired ',
      'กรุณากรอกรายละเอียด '
    ]

  ],

  // Eform
  validateEFormModalOwner: [
    [
      'receiver_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_type_code',
      'isRequiredOption',
      'กรุณาเลือก ประเภทบัตร',
      {
        name: 'receiver_type_code',
        value: ['PEOPLE', 'CORPORATE']
      }
    ],
    [
      'card_number',
      'isRequiredOption',
      'กรุณากรอก เลขที่บัตร',
      {
        name: 'receiver_type_code',
        value: ['PEOPLE', 'CORPORATE']
      }
    ],
    [
      'name',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'sex_code',
      'isRequiredOption',
      'กรุณาเลือก เพศ',
      {
        name: 'receiver_type_code',
        value: ['PEOPLE', 'PEOPLE_FOREIGNER']
      }
    ],
    [
      'nationality_code',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_code',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_code',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_number',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_name',
      'isRequiredOption',
      'กรุณากรอกและเลือก ตำบล',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'address_district_name',
      'isRequiredOption',
      'กรุณากรอก อำเภอ',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'address_province_name',
      'isRequiredOption',
      'กรุณากรอก จังหวัด',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'postal_code',
      'isRequiredOption',
      'กรุณากรอก รหัสไปรษณีย์',
      {
        name: 'address_country_code',
        value: ['TH']
      }
      // 'isRequired | isNumber | isPostalCode',
      // 'กรุณากรอก รหัสไปรษณีย์ | กรุณากรอก ตัวเลข | รหัสไปรษณีย์ไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'email',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephone',
      'isRequiredOption | isNumberOption | isPhoneOption',
      'กรุณากรอก เบอร์โทรศัพท์ | กรุณากรอก ตัวเลข | เบอร์โทรศัพท์ไม่ถูกต้องตามรูปแบบที่กำหนด',
      {
        name: 'receiver_type_code',
        value: ['PEOPLE', 'CORPORATE', 'GOVERNMENT']
      }
    ],
  ],
  validateEFormModalAgent: [
    [
      'representative_type_code',
      'isRequired',
      'กรุณาเลือก ตัวแทน'
    ],
    [
      'receiver_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_type_code',
      'isRequiredOption',
      'กรุณาเลือก ประเภทบัตร',
      {
        name: 'receiver_type_code',
        value: ['PEOPLE', 'CORPORATE']
      }
    ],
    [
      'card_number',
      'isRequiredOption',
      'กรุณากรอก เลขที่บัตร',
      {
        name: 'receiver_type_code',
        value: ['PEOPLE', 'CORPORATE']
      }
    ],
    [
      'name',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'sex_code',
      'isRequiredOption',
      'กรุณาเลือก เพศ',
      {
        name: 'receiver_type_code',
        value: ['PEOPLE', 'PEOPLE_FOREIGNER']
      }
    ],
    [
      'nationality_code',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_code',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_code',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_number',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_name',
      'isRequiredOption',
      'กรุณากรอกและเลือก ตำบล',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'address_district_name',
      'isRequiredOption',
      'กรุณากรอก อำเภอ',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'address_province_name',
      'isRequiredOption',
      'กรุณากรอก จังหวัด',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'postal_code',
      'isRequiredOption',
      'กรุณากรอก รหัสไปรษณีย์',
      {
        name: 'address_country_code',
        value: ['TH']
      }
      // 'isRequired | isNumber | isPostalCode',
      // 'กรุณากรอก รหัสไปรษณีย์ | กรุณากรอก ตัวเลข | รหัสไปรษณีย์ไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'email',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephone',
      'isRequired | isNumber | isPhone',
      'กรุณากรอก เบอร์โทรศัพท์ | กรุณากรอก ตัวเลข | เบอร์โทรศัพท์ไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
  ],
  validateEFormModalAddressOnly: [
    [
      'address_country_code',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_number',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_name',
      'isRequiredOption',
      'กรุณากรอกและเลือก ตำบล',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'address_district_name',
      'isRequiredOption',
      'กรุณากรอก อำเภอ',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'address_province_name',
      'isRequiredOption',
      'กรุณากรอก จังหวัด',
      {
        name: 'address_country_code',
        value: ['TH']
      }
    ],
    [
      'postal_code',
      'isRequiredOption',
      'กรุณากรอก รหัสไปรษณีย์',
      {
        name: 'address_country_code',
        value: ['TH']
      }
      // 'isRequired | isNumber | isPostalCode',
      // 'กรุณากรอก รหัสไปรษณีย์ | กรุณากรอก ตัวเลข | รหัสไปรษณีย์ไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'email',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephone',
      'isRequiredOption | isNumberOption | isPhoneOption',
      'กรุณากรอก เบอร์โทรศัพท์ | กรุณากรอก ตัวเลข | เบอร์โทรศัพท์ไม่ถูกต้องตามรูปแบบที่กำหนด',
      {
        name: 'receiver_type_code',
        value: ['PEOPLE', 'CORPORATE', 'GOVERNMENT']
      }
    ],
  ],

  // Eform 01
  validateEFormSave01ProcessStep1: [
    [
      'email',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephone',
      'isRequired | isPhone',
      'กรุณากรอก เบอร์โทรศัพท์ | เบอร์โทรศัพท์ไม่ถูกต้องตามรูปแบบที่กำหนด'
    ]
  ],
  validateEFormSave01ProcessStep2: [
    [
      'save010_mark_type_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทเครื่องหมาย'
    ]
  ],
  validateEFormSave01ProcessStep4: [
    [
      'save010_contact_type_code',
      'isRequired',
      'กรุณาเลือก สถานที่ติดต่อภายในประเทศไทย'
    ]
  ],
  validateEFormSave01ProcessStep4Other: [
    [
      'receiver_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'is_specific',
      'isRequired',
      'กรุณาเลือก ตัวแทนเฉพาะการ'
    ],
    [
      'name',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'house_number',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_name',
      'isRequired',
      'กรุณากรอกและเลือก ตำบล'
    ],
    [
      'address_district_name',
      'isRequired',
      'กรุณากรอก อำเภอ'
    ],
    [
      'address_province_name',
      'isRequired',
      'กรุณากรอก จังหวัด'
    ],
    [
      'postal_code',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
      // 'isRequired | isNumber | isPostalCode',
      // 'กรุณากรอก รหัสไปรษณีย์ | กรุณากรอก ตัวเลข | รหัสไปรษณีย์ไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'email',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephone',
      'isRequired | isNumber | isPhone',
      'กรุณากรอก เบอร์โทรศัพท์ | กรุณากรอก ตัวเลข | เบอร์โทรศัพท์ไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
  ],
  validateEFormSave01ProcessStep5Sub2: [
    // [
    //   'save010_sound_mark_type_type_code',
    //   'isRequired',
    //   'กรุณาเลือก ประเภทเครื่องหมายเสียง'
    // ],
    [
      'remark_5_2_2',
      'isRequired',
      'กรุณากรอก การออกเสียงหรือคำบรรยายเสียง'
    ]
  ],
  validateEFormSave01ProcessStep6: [
    [
      'word_translate_sound',
      'isRequired',
      'กรุณากรอก คำอ่าน'
    ],
    [
      'word_translate_translate',
      'isRequired',
      'กรุณากรอก คำแปล'
    ]
  ],
  validateEFormSave01ProcessStep7: [
    [
      'request_item_sub_type_1_code',
      'isRequired',
      'กรุณากรอก จำพวก'
    ],
    [
      'description',
      'isRequired',
      'กรุณากรอก รายการสินค้า/บริการ'
    ]
  ],
  validateEFormSave01ProcessStep8: [
    [
      'remark_8',
      'isRequired',
      'กรุณากรอก คำบรรยายลักษณะกลุ่มของสี'
    ]
  ],
  validateEFormSave01ProcessStep9: [
    [
      'remark_9',
      'isRequired',
      'กรุณากรอก คำบรรยายลักษณะรูปร่างหรือรูปทรง'
    ]
  ],
  validateEFormSave01ProcessStep10: [
    [
      'isUseMark',
      'isRequired',
      'กรุณาเลือก การใช้เครื่องหมาย'
    ]
  ],
  validateEFormSave01ProcessStep11: [
    [
      'save010_assert_type_code',
      'isRequired',
      'กรุณาเลือก เงื่อนไขสิทธิ'
    ]
  ],
  validateEFormSave01ProcessStep11Sub1Mini3: [
    [
      'request_number',
      'isRequired',
      'กรุณากรอก เลขที่คำขอ'
    ],
    [
      'request_date',
      'isRequired',
      'กรุณากรอก วันที่ยื่นคำขอ'
    ],
    [
      'request_country',
      'isRequired',
      'กรุณากรอก ประเทศที่ยื่นคำขอ'
    ],
    [
      'request_nationality',
      'isRequired',
      'กรุณากรอก สัญชาติผู้ขอ'
    ],
    [
      'request_domicile',
      'isRequired',
      'กรุณากรอก ภูมิลำเนาของผู้ขอ'
    ],
    [
      'product_class',
      'isRequired',
      'กรุณากรอก จำพวกที่'
    ],
    [
      'product',
      'isRequired',
      'กรุณากรอก รายการสินค้า/บริการ'
    ],
    [
      'status',
      'isRequired',
      'กรุณากรอก สถานะคำขอ'
    ],
  ],
  validateEFormSave01ProcessStep11Sub1Mini4: [
    [
      'product',
      'isRequired',
      'กรุณากรอก รายการสินค้า/บริการ'
    ],
    [
      'event_date',
      'isRequired',
      'กรุณากรอก วัน/เดือน/ปีงานแสดงสินค้า'
    ],
    [
      'event_place',
      'isRequired',
      'กรุณากรอก สถานที่จัดงานแสดงสินค้า/บริการ'
    ],
    [
      'event_organizer',
      'isRequired',
      'กรุณากรอก ผู้จัดงานแสดงสินค้า/บริการ'
    ],
  ],
  validateEFormSave01ProcessStep11Sub1Mini5: [
    [
      'remark_11_1_9',
      'isRequired',
      'กรุณากรอก รายการเอกสาร'
    ],
  ],
  validateEFormSave01ProcessStep11Sub2Mini2: [
    // [
    //   'day',
    //   'isRequired | isNumber | isNumberRange',
    //   'กรุณากรอก จำนวนวัน | กรุณากรอก ตัวเลข | กรุณากรอก ตัวเลขระหว่าง 1 ถึง 60',
    //   { min: 1, max: 60 }
    // ],
    [
      'detail',
      'isRequired',
      'กรุณากรอก เอกสารหลักฐานที่ขอผ่อนผัน'
    ],
  ],
  validateEFormSave01ProcessStep12: [
    [
      'otop_number',
      'isRequired',
      'กรุณากรอก เลขทะเบียน OTOP'
    ],
  ],
  validateEFormSave01ProcessStep15: [
    [
      'payer_name',
      'isRequired',
      'กรุณากรอก ชื่อที่จะออกในใบเสร็จรับเงิน'
    ],
  ],

  // Eform 02
  validateEFormSave02ProcessStep2: [
    [
      'request_number',
      'isRequired',
      'กรุณากรอก คำคัดค้านการขอจดทะเบียนคำขอเลขที่'
    ],
  ],
  validateEFormSave02ProcessStep4: [
    [
      'save020_contact_type_code',
      'isRequired',
      'กรุณาเลือก สถานที่ติดต่อภายในประเทศไทย'
    ]
  ],
  validateEFormSave02ProcessStep7: [
    [
      'receipt_name',
      'isRequired',
      'กรุณากรอก ชื่อที่จะออกในใบเสร็จรับเงิน'
    ],
  ],

  // Eform 021
  validateEFormSave021ProcessStep4: [
    [
      'save021_contact_type_code',
      'isRequired',
      'กรุณาเลือก สถานที่ติดต่อภายในประเทศไทย'
    ]
  ],
  validateEFormSave021ProcessStep5: [
    [
      'nameS5',
      'isRequired',
      'กรุณากรอก ชื่อ-นามสกุล'
    ],
    [
      'house_numberS5',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],

    [
      'receiver_type_codeS5',
      'isRequired',
      'กรุณากรอก ประเภทบุคคล'
    ],

    [
      'ddress_sub_district_codeS5',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_codeS5',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'emailS5',
      'isRequired',
      'กรุณากรอก email'
    ],
    [
      'telephoneS5',
      'isRequired',
      'กรุณากรอก เบอร์ โทรศัพท์'
    ],


  ],
  validateEFormSave021ProcessStep5part1: [
    [
      'save021_contact_type_code',
      'isRequired',
      'กรุณาเลือกสถานที่ติดต่อภายในประเทศ'
    ],

  ],

  // Eform 04
  validateEFormSave04ProcessStep3: [
    [
      'save040_submit_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทการยื่น'
    ],
  ],
  validateEFormSave04ProcessStep5: [
    [
      'save040_contact_type_code',
      'isRequired',
      'กรุณาเลือก สถานที่ติดต่อภายในประเทศไทย'
    ]
  ],
  validateEFormSave04ProcessStep7: [
    [
      'save040_receiver_contact_type_code',
      'isRequired',
      'กรุณาเลือก สถานที่ติดต่อภายในประเทศไทย'
    ]
  ],
  validateEFormSave04ProcessStep5OwnerMarkItem: [
    [
      'receiver_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_number',
      'isRequired',
      'กรุณากรอก หมายเลข'
    ],
    [
      'name',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'sex_code',
      'isRequired',
      'กรุณาเลือก เพศ'
    ],
    [
      'nationality_code',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_code',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_code',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_number',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_name',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_code',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'email',
      'isRequired',
      'กรุณากรอก อีเมล'
    ],
    [
      'telephone',
      'isRequired',
      'กรุณากรอก โทรศัพท์'
    ],
  ],
  validateEFormSave04ProcessStep5AgentMarkItem: [
    [
      'receiver_type_codeAgent',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_numberAgent',
      'isRequired',
      'กรุณากรอก หมายเลข'
    ],
    [
      'nameAgent',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'sex_codeAgent',
      'isRequired',
      'กรุณาเลือก เพศ'
    ],
    [
      'nationality_codeAgent',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_codeAgent',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_codeAgent',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_numberAgent',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_nameAgent',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_codeAgent',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'emailAgent',
      'isRequired',
      'กรุณากรอก อีเมล'
    ],
    [
      'telephoneAgent',
      'isRequired',
      'กรุณากรอก โทรศัพท์'
    ],
  ],
  validateEFormSave04ProcessStep7receiverMarkItem: [
    [
      'receiver_type_codeReceiver',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_numberReceiver',
      'isRequired',
      'กรุณากรอก หมายเลข'
    ],
    [
      'nameReceiver',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'sex_codeReceiver',
      'isRequired',
      'กรุณาเลือก เพศ'
    ],
    [
      'nationality_codeReceiver',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_codeReceiver',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_codeReceiver',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_numberReceiver',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_nameReceiver',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_codeReceiver',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'emailReceiver',
      'isRequired',
      'กรุณากรอก อีเมล'
    ],
    [
      'telephoneReceiver',
      'isRequired',
      'กรุณากรอก โทรศัพท์'
    ],
  ],
  validateEFormSave04ProcessStep7agenReceiverMarkItem: [
    [
      'receiver_type_codeAgenReceiver',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_numberAgenReceiver',
      'isRequired',
      'กรุณากรอก หมายเลข'
    ],
    [
      'nameAgenReceiver',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'sex_codeAgenReceiver',
      'isRequired',
      'กรุณาเลือก เพศ'
    ],
    [
      'nationality_codeAgenReceiver',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_codeAgenReceiver',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_codeAgenReceiver',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_numberAgenReceiver',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_nameAgenReceiver',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_codeAgenReceiver',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'emailAgenReceiver',
      'isRequired',
      'กรุณากรอก อีเมล'
    ],
    [
      'telephoneAgenReceiver',
      'isRequired',
      'กรุณากรอก โทรศัพท์'
    ],
  ],

  // Eform 05
  validateEFormSave05ProcessStep1: [
    [
      'emailS1',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephoneS1',
      'isRequired',
      // 'isRequired | isPhone',
      'กรุณากรอก เบอร์โทรศัพท์',
      // 'กรุณากรอก เบอร์โทรศัพท์ | เบอร์โทรศัพท์ไม่ถูกต้องตามรูปแบบที่กำหนด'
    ]
  ],
  validateEFormSave05ProcessStep4OwnerMarkItem: [
    [
      'receiver_type_code_Owner',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_number_Owner',
      'isRequired',
      'กรุณากรอก หมายเลข'
    ],
    [
      'name_Owner',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'nationality_code_Owner',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_code_Owner',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_code_Owner',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_number_Owner',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'email_Owner',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephone_Owner',
      'isRequired',
      'กรุณากรอก เบอร์โทรศัพท์'
    ],
    [
      'address_sub_district_name_Owner',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_code_Owner',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'sex_code_Owner',
      'isRequired',
      'กรุณาเลือก เพศ'
    ]
  ],
  validateEFormSave05ProcessStep4AgentMarkItem: [
    //[
    //  'rights_agent',
    //  'isRequired',
    //  'กรุณาเลือก สิทธิ'
    //],
    [
      'receiver_type_code_agent',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_number_agent',
      'isRequired',
      'กรุณากรอก เลขประจำตัวผู้เสียภาษี'
    ],
    [
      'name_agent',
      'isRequired',
      'กรุณากรอก ชื่อหน่วยงาน'
    ],
    [
      'sex_code_agent',
      'isRequired',
      'กรุณาเลือก เพศ'
    ],
    [
      'nationality_code_agent',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_code_agent',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_code_agent',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],

    [
      'house_number_agent',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_name_agent',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_code_agent',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'email_agent',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephone_agent',
      'isRequired',
      'กรุณากรอก เบอร์โทรศัพท์'
    ],

  ],
  validateEFormSave05ProcessStep5receiverMarkItem: [
    [
      'receiver_type_code_receiver',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_number_receiver',
      'isRequired',
      'กรุณากรอก หมายเลข'
    ],
    [
      'name_receiver',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'nationality_code_receiver',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_code_receiver',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_code_receiver',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_number_receiver',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'email_receiver',
      'isRequired',
      'กรุณากรอก อีเมล'
    ],
    [
      'telephone_receiver',
      'isRequired',
      'กรุณากรอก เบอร์โทรศัพท์'
    ],
    [
      'address_sub_district_name_receiver',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_code_receiver',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'sex_code_receiver',
      'isRequired',
      'กรุณาเลือก เพศ'
    ]
  ],
  validateEFormSave05ProcessStep5agenReceiverMarkItem: [
    //[
    //  'rights_agenReceiver',
    //  'isRequired',
    //  'กรุณาเลือก สิทธิ'
    //],
    [
      'receiver_type_code_agenReceiver',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_number_agenReceiver',
      'isRequired',
      'กรุณากรอก เลขประจำตัวผู้เสียภาษี'
    ],
    [
      'name_agenReceiver',
      'isRequired',
      'กรุณากรอก ชื่อหน่วยงาน'
    ],
    [
      'sex_code_agenReceiver',
      'isRequired',
      'กรุณาเลือก เพศ'
    ],
    [
      'nationality_code_agenReceiver',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_code_agenReceiver',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_code_agenReceiver',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],

    [
      'house_number_agenReceiver',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_name_agenReceiver',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_code_agenReceiver',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'email_agenReceiver',
      'isRequired',
      'กรุณากรอก อีเมล'
    ],
    [
      'telephone_agenReceiver',
      'isRequired',
      'กรุณากรอก เบอร์โทรศัพท์'
    ],
  ],
  validateEFormSave05ProcessStep6: [
    [
      'nameS6',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'house_numberS6',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_nameS6',
      'isRequired',
      'กรุณากรอก ตำบล อำเภอ และจังหวัด'
    ],
    [
      'postal_codeS6',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'emailS6',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephoneS6',
      'isRequired',
      'กรุณากรอก โทรศัพท์'
    ],
  ],

  // Eform 07
  validateEFormSave07ProcessStep5: [
    [
      'nameS6',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'house_numberS6',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_nameS6',
      'isRequired',
      'กรุณากรอก ตำบล อำเภอ และจังหวัด'
    ],
    [
      'postal_codeS6',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'emailS6',
      'isRequired | isEmail',
      'กรุณากรอก อีเมล | อีเมลไม่ถูกต้องตามรูปแบบที่กำหนด'
    ],
    [
      'telephoneS6',
      'isRequired',
      'กรุณากรอก โทรศัพท์'
    ],
  ],
  validateEFormSave07ProcessStep5OwnerMarkItem: [
    [
      'receiver_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_number',
      'isRequired',
      'กรุณากรอก หมายเลข'
    ],
    [
      'name',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'sex_code',
      'isRequired',
      'กรุณาเลือก เพศ'
    ],
    [
      'nationality_code',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_code',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_code',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_number',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_name',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_code',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'email',
      'isRequired',
      'กรุณากรอก อีเมล'
    ],
    [
      'telephone',
      'isRequired',
      'กรุณากรอก โทรศัพท์'
    ],
  ],
  validateEFormSave07ProcessStep5AgentMarkItem: [
    [
      'receiver_type_codeAgent',
      'isRequired',
      'กรุณาเลือก ประเภทบุคคล'
    ],
    [
      'card_numberAgent',
      'isRequired',
      'กรุณากรอก หมายเลข'
    ],
    [
      'nameAgent',
      'isRequired',
      'กรุณากรอก ชื่อ'
    ],
    [
      'sex_codeAgent',
      'isRequired',
      'กรุณาเลือก เพศ'
    ],
    [
      'nationality_codeAgent',
      'isRequired',
      'กรุณาเลือก สัญชาติ'
    ],
    [
      'career_codeAgent',
      'isRequired',
      'กรุณาเลือก อาชีพ'
    ],
    [
      'address_country_codeAgent',
      'isRequired',
      'กรุณาเลือก ประเทศ'
    ],
    [
      'house_numberAgent',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],
    [
      'address_sub_district_nameAgent',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_codeAgent',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'emailAgent',
      'isRequired',
      'กรุณากรอก อีเมล'
    ],
    [
      'telephoneAgent',
      'isRequired',
      'กรุณากรอก โทรศัพท์'
    ],
  ],

  // Eform 08
  validateEFormSave08ProcessStep2: [
    [
      'save080_request_item_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทเครื่องหมาย'
    ]
  ],
  validateEFormSave08ProcessStep5: [
    [
      'nameS5',
      'isRequired',
      'กรุณากรอก ชื่อ-นามสกุล'
    ],
    [
      'house_numberS5',
      'isRequired',
      'กรุณากรอก ที่อยู่'
    ],

    [
      'receiver_type_codeS5',
      'isRequired',
      'กรุณากรอก ประเภทบุคคล'
    ],

    [
      'ddress_sub_district_codeS5',
      'isRequired',
      'กรุณากรอก ตำบล/อำเภอ/จังหวัด'
    ],
    [
      'postal_codeS5',
      'isRequired',
      'กรุณากรอก รหัสไปรษณีย์'
    ],
    [
      'emailS5',
      'isRequired',
      'กรุณากรอก email'
    ],
    [
      'telephoneS5',
      'isRequired',
      'กรุณากรอก เบอร์ โทรศัพท์'
    ],


  ],
  validateEFormSave08ProcessStep5part1: [
    [
      'save08_contact_type_code',
      'isRequired',
      'กรุณาเลือกสถานที่ติดต่อภายในประเทศ'
    ],

  ],

  // Eform 120
  validateEFormSave120ProcessStep2: [
    [
      'save120_submit_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทการยื่น'
    ],
  ],
  validateEFormSave120ProcessStep3: [
    [
      'request_number',
      'isRequired',
      'กรุณากรอก คำขอเลขที่'
    ],
  ],

  // Eform 140
  validateEFormSave140ProcessStep2: [
    [
      'request_number',
      'isRequired',
      'กรุณากรอก คำขอเลขที่'
    ],
  ],
  validateEFormSave140ProcessStep3: [
    [
      'inform_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทหนังสือแจ้ง'
    ],
  ],
  validateEFormSave140ProcessStep5: [
    [
      'save140_submit_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทการยื่น'
    ],
  ],
  validateEFormSave140ProcessStep5Registrar: [
    [
      'rule_number',
      'isRequired',
      'กรุณาเลือก ประเภทการยื่น'
    ],
  ],
  validateEFormSave140ProcessStep6Sue: [
    [
      'court_name',
      'isRequired',
      'กรุณากรอก ชื่อศาล'
    ],
    [
      'black_number_1',
      'isRequired',
      'กรุณากรอก คดีหมายเลขดำ'
    ],
    [
      'black_number_2',
      'isRequired',
      'กรุณากรอก คดีหมายเลขดำ'
    ],
    [
      'sue_date',
      'isRequired',
      'กรุณาเลือก วันที่'
    ],
  ],
  validateEFormSave140ProcessStep6Judgment: [
    [
      'court_name',
      'isRequired',
      'กรุณากรอก ชื่อศาล'
    ],
    [
      'black_number_1',
      'isRequired',
      'กรุณากรอก คดีหมายเลขดำ'
    ],
    [
      'black_number_2',
      'isRequired',
      'กรุณากรอก คดีหมายเลขดำ'
    ],
    [
      'red_number_1',
      'isRequired',
      'กรุณากรอก คดีหมายเลขแดง'
    ],
    [
      'red_number_2',
      'isRequired',
      'กรุณากรอก คดีหมายเลขแดง'
    ],
  ],
  validateEFormSave140ProcessStep6Remark: [
    [
      'remark_6_3',
      'isRequired',
      'กรุณากรอก รายละเอียดผลฟ้องร้องคดี'
    ],
  ],

  // Eform 150
  validateEFormSave150ProcessStep3: [
    [
      'informer_type_code',
      'isRequired',
      'กรุณาเลือก เจ้าของ/ตัวแทน'
    ],
  ],

  // Eform 190
  validateEFormSave190ProcessStep4: [
    [
      'postpone_day',
      'isRequired | isNumber | isNumberRange',
      'กรุณากรอก จำนวนวัน | กรุณากรอก ตัวเลข | กรุณากรอก ตัวเลขระหว่าง 1 ถึง 60',
      { min: 1, max: 60 }
    ],
  ],

  // Eform 200
  validateEFormSave200ProcessStep3: [
    [
      'save200_subject_type_code',
      'isRequired',
      'กรุณาเลือก ประเด็นการยื่น'
    ],
  ],
  validateEFormSave200ProcessStep4: [
    [
      'rule_number',
      'isRequired',
      'กรุณากรอก เลขที่อ้างอิงตค.'
    ],
  ],
  validateEFormSave200ProcessStep4Request: [
    [
      'save200_request_type_code',
      'isRequired',
      'กรุณาเลือก แบบฟอร์ม'
    ],
  ],
  validateEFormSave200ProcessStep4Other: [
    [
      'others_request_type',
      'isRequired',
      'กรุณากรอก ประเภทคำขอ'
    ],
  ],

  // Eform 210
  validateEFormSave210ProcessStep2: [
    [
      'inter_registration_number',
      'isRequired',
      'กรุณากรอก เลขทะเบียนระหว่างประเทศ'
    ],
  ],
  validateEFormSave210ProcessStep3: [
    [
      'request_number',
      'isRequired',
      'กรุณากรอก เลขคำขอในประเทศ'
    ],
  ],
  validateEFormSave210ProcessStep5: [
    [
      'save210_contact_type_code',
      'isRequired',
      'กรุณาเลือก สถานที่ติดต่อภายในประเทศไทย'
    ]
  ],

  // Eform 220
  validateEFormSave220ProcessStep3: [
    [
      'inter_registration_number',
      'isRequired',
      'กรุณากรอก เลขทะเบียนระหว่างประเทศที่ถูกยกเลิก'
    ],
  ],
  validateEFormSave220ProcessStep5: [
    [
      'save220_contact_type_code',
      'isRequired',
      'กรุณาเลือก สถานที่ติดต่อภายในประเทศไทย'
    ]
  ],
  validateEFormSave220ProcessStep9: [
    [
      'remark_9',
      'isRequired',
      'กรุณากรอก คำบรรยายลักษณะรูปร่างหรือรูปทรง'
    ]
  ],
  validateEFormSave220ProcessStep10: [
    [
      'remark_10',
      'isRequired',
      'กรุณากรอก คำบรรยายลักษณะกลุ่มของสี'
    ]
  ],
  validateEFormSave220ProcessStep11: [
    [
      'save220_assert_type_code',
      'isRequired',
      'กรุณาเลือก เงื่อนไขสิทธิ'
    ]
  ],
  validateEFormSave220ProcessStep11Sub1Mini5: [
    [
      'remark_12_1_9',
      'isRequired',
      'กรุณากรอก รายการเอกสาร'
    ],
  ],

  // Eform 230
  validateEFormSave230ProcessStep3: [
    [
      'save230_document_type_code',
      'isRequired',
      'กรุณาเลือก ประเภทการยื่น'
    ],
  ],

}























































































































































