import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
// import { StaffService } from '../../services'

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})

export class ModalComponent implements OnInit {

  //TODO >>> Declarations <<<
  private _className: string
  private _modalSize: string = ''
  private _modalVerticalCenter: any = false

  constructor(
    private router: Router,
    private global: GlobalService,
    //private staffService: StaffService
  ) { }

  get className(): string {
    if (this._className != null || undefined) {
      return this._className.toString().replace(/,/g, " ")
    }
  }

  get modalSize(): string {
    switch (this._modalSize) {
      case 'long':
        return 'modal-lg'
      case 'large':
        return 'modal-lg'
      case 'lg':
        return 'modal-lg'
      case 'short':
        return 'modal-sm'
      case 'small':
        return 'modal-sm'
      case 'sm':
        return 'modal-sm'
      case 'fiuld':
        return 'modal-fiuld'
      case 'full':
        return 'modal-fiuld'
      case 'xl':
        return 'modal-xl'
      default:
        return null
    }
  }

  get modalVerticalCenter(): string {
    if (this._modalVerticalCenter) {
      return 'modal-dialog-centered'
    }
    else {
      return null
    }
  }

  _onClickClose() {
    this.onClickClose.emit(null);
  }

  // Component Props
  @Input() set className(className: string) { this._className = className }
  @Input() set modalSize(modalSize: string) { this._modalSize = modalSize }
  @Input() set modalVerticalCenter(modalVerticalCenter) { this._modalVerticalCenter = modalVerticalCenter }
  @Input() title: string
  @Input() hideCloseCross: boolean = false
  @Input() closeButtonText: string = 'ยกเลิก'

  // Component Event Props
  @Output() onClickClose = new EventEmitter();

  ngOnInit() {
    //TODO >>> Init value <<<
  }
}
