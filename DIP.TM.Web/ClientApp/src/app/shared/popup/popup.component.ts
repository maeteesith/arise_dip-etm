import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
// import { StaffService } from '../../services'

@Component({
  selector: 'app-popup',
  templateUrl: './popup.component.html',
  styleUrls: ['./popup.component.scss']
})

export class PopupComponent implements OnInit {

  //TODO >>> Declarations <<<
  private _className: string
  private _popupSize: string = ''
  private _popupVerticalCenter: any = true

  constructor(
    private router: Router,
    private global: GlobalService,
    //private staffService: StaffService
  ) { }

  get className(): string {
    if (this._className != null || undefined) {
      return this._className.toString().replace(/,/g, " ")
    }
  }

  get popupSize(): string {
    switch (this._popupSize) {
      case 'long':
        return 'popup-lg'
      case 'large':
        return 'popup-lg'
      case 'lg':
        return 'popup-lg'
      case 'normal':
        return 'popup-md'
      case 'medium':
        return 'popup-md'
      case 'md':
        return 'popup-md'
      case 'short':
        return 'popup-sm'
      case 'small':
        return 'popup-sm'
      case 'sm':
        return 'popup-sm'
      case 'fiuld':
        return 'popup-fiuld'
      case 'full':
        return 'popup-fiuld'
      default:
        return null
    }
  }

  get popupVerticalCenter(): string {
    if (this._popupVerticalCenter) {
      return 'modal-dialog-centered'
    }
    else {
      return null
    }
  }

  _onClickClose() {
    this.onClickClose.emit(null);
  }

  // Component Props
  @Input() set className(className: string) { this._className = className }
  @Input() set popupSize(popupSize: string) { this._popupSize = popupSize }
  @Input() set popupVerticleCenter(popupVerticleCenter) { this._popupVerticalCenter = popupVerticleCenter }
  @Input() title: string
  @Input() hideHeaderIcon: boolean = false
  @Input() closeButtonText: string = 'ยกเลิก'
  @Input() Icon: string

  // Component Event Props
  @Output() onClickClose = new EventEmitter();

  ngOnInit() {
    //TODO >>> Init value <<<

    this._popupSize = 'sm'
  }
}
