import { Component, Input, Output, OnInit, EventEmitter } from "@angular/core";
import { CONSTANTS, clone } from "../../helpers";

@Component({
  selector: "app-pagination",
  templateUrl: "./pagination.component.html",
  styleUrls: ["./pagination.component.scss"]
})
export class PaginationComponent implements OnInit {
  //TODO >>> Declarations <<<
  public perPageList: number[];

  //TODO >>> Declarations (Input) <<<
  @Input() pagin: any;

  //TODO >>> Declarations (Output) <<<
  @Output() onChangePage = new EventEmitter();
  @Output() onChangePerPage = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.perPageList = clone(CONSTANTS.PAGINATION.PER_PAGE);
  }

  //! <<<< Emit >>>
  _onChangePage(value: any) {
    this.onChangePage.emit(value);
  }
  _onChangePerPage(value: any) {
    this.onChangePerPage.emit(value);
  }

  //! <<<< Event >>>
  getMaxPage() {
    return Math.ceil(this.pagin.totalItems / this.pagin.itemsPerPage);
  }
}
