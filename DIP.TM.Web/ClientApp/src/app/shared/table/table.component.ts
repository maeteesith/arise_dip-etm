import { UploadService } from "../../services/upload.service"
import { Component, Input, EventEmitter, OnInit, Output, NgModule, } from '@angular/core';
import { Help } from '../../helpers/help'
import { DomSanitizer } from '@angular/platform-browser'

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styles: ['.red { color: red; }'],
  styleUrls: ['./table.component.scss']
})

export class TableComponent {
  constructor(
    public sanitizer: DomSanitizer,
    private uploadService: UploadService,
    private help: Help,
  ) { }

  //{column_list, column_edit_list, object_list, is_address_edit,
  //can_added, can_checked, can_deleted, can_selected, can_ordered, has_link, width, 
  //command, command_item, button_list}

  //column_edit_list {type, type_click, group_by, width}
  //type {textbox, datetime, combo_box, checkbox}

  //command_item {name, icon, object_id}

  //command: {is_showed}

  @Input() title: any
  @Input() master: any = {}
  @Input() initial_table: any = {}
  @Output() onClickEdit = new EventEmitter();
  @Output() onClickCommand = new EventEmitter<any>();

  public object_length: any = 0
  public paginate: any = {}

  public object_list: any[] = []
  public object_parent_list: any[] = []
  public object_show_list: any[] = []
  public updated: any = {}

  public row_item_click: any
  public row_item_group_by: any
  public tableGroupBy: any = {}

  public popup: any

  //Address
  public modal: any
  public modalPeopleText: any
  public modalAddressEdit: any
  public modalAddress: any

  ngOnInit() {
    this.modal = {}
    this.modalAddress = {}

    this.popup = {
      duplicate_index: 0,
    }
    if (this.initial_table.is_address_edit) {
      this.initial_table.can_added = true
      this.initial_table.can_deleted = true
    }

    this.initial_table.Set = (object_list) => {
      if (this.initial_table.column_edit_list) {
        Object.keys(this.initial_table.column_edit_list).forEach((column_name: any) => {
          if (this.initial_table.column_edit_list[column_name].group_by) {
            this.row_item_group_by = this.initial_table.column_edit_list[column_name].group_by
            var new_list = []

            object_list.forEach((item: any) => {
              var found_list = new_list.filter(r => r[column_name] == item[column_name])
              var found

              if (found_list.length > 0) {
                found = found_list[0]
              } else {
                found = {}
                found[column_name] = item[column_name]
                new_list.push(found)
              }

              found[this.initial_table.column_edit_list[column_name].group_by] = found[this.initial_table.column_edit_list[column_name].group_by] || ""
              found[this.initial_table.column_edit_list[column_name].group_by] += "  " + item[this.initial_table.column_edit_list[column_name].group_by]
              found[this.initial_table.column_edit_list[column_name].group_by] = found[this.initial_table.column_edit_list[column_name].group_by].trim()
              found.count = found.count || 0
              found.count++
            })

            object_list.length = 0
            new_list.forEach((item: any) => {
              object_list.push(item)
            })
          }
        })
      }

      this.object_parent_list = object_list

      this.object_list.length = 0
      this.object_parent_list.forEach((item: any) => {
        this.object_list.push(item)
      })
      this.updated.updated = true
    }
    this.initial_table.Add = (object) => {
      this.object_list.push(object)
      this.object_parent_list.push(object)
      this.updated.updated = true
    }
    this.initial_table.Get = () => {
      return this.object_parent_list
    }

    this.initial_table.SetDataList = (data_list) => {
      console.log(data_list)

      this.object_show_list.length = 0
      if (data_list.list) {
        data_list.list.forEach((item: any) => {
          this.object_show_list.push(item)
        })
        this.help.PageSet(data_list, this.paginate)
      }

      //this.object_list = data.list || []
      //this.help.PageSet(data, this.paginateDocumentRole04Item)
    }
  }

  onClickAddressEdit(item: any, name: string = "เจ้าของ"): void {
    this.modalPeopleText = this.initial_table.object_name || name
    this.modalAddressEdit = item
    this.help.Clone(item, this.modalAddress)
    this.modal["isModalPeopleEditOpen"] = true
  }

  onClickAddressSave(): void {
    this.help.Clone(this.modalAddress, this.modalAddressEdit)
    this.help.GetAddressInformation(this.modalAddressEdit)

    this.modal["isModalPeopleEditOpen"] = false
  }

  onClickTextbox(row_item: any, column_name: any): void {
    this.tableGroupBy.column_list = {}
    this.tableGroupBy.column_edit_list = {}
    Object.keys(this.initial_table.column_edit_list).forEach((column: any) => {
      if (this.initial_table.column_edit_list[column].group_by == column_name) {
        this.popup.group_by_show = row_item

        this.popup.group_by_title = this.initial_table.column_list[column] + ": " + row_item[column]

        this.tableGroupBy.column_list["index"] = "#"
        this.tableGroupBy.column_list[this.initial_table.column_edit_list[column].group_by] = this.initial_table.column_list[this.initial_table.column_edit_list[column].group_by]
        this.tableGroupBy.column_edit_list[this.initial_table.column_edit_list[column].group_by] = {
          type: "textbox",
        }
        this.tableGroupBy.can_added = true
        this.tableGroupBy.can_deleted = true
        this.tableGroupBy.can_ordered = true

        var value_list = []
        row_item[this.initial_table.column_edit_list[column].group_by].split("  ").forEach((item: any) => {
          var value = {}
          value[this.initial_table.column_edit_list[column].group_by] = item
          value_list.push(value)
        })

        setTimeout(() => {
          this.tableGroupBy.Set(value_list)
        }, 10)
      }
    })
  }

  onClickGroupBySave(): void {
    Object.keys(this.initial_table.column_edit_list).forEach((column: any) => {
      if (this.initial_table.column_edit_list[column].group_by) {
        this.popup.group_by_show[this.initial_table.column_edit_list[column].group_by] = this.tableGroupBy.Get().map((item: any) => {
          return item[this.initial_table.column_edit_list[column].group_by]
        }).join("  ")

        this.popup.group_by_show.count = this.tableGroupBy.Get().length

        this.popup.group_by_show = null
      }
    })
  }

  FileUpload(command: any, event: any = null): void {
    let file = event.target.files[0]
    event.target.value = ""

    if (command.is_uploaded) {
      let reader = new FileReader()
      reader.onload = e => {
        const formData = new FormData()
        formData.append("file", file, file.name)
        this.uploadService.upload(formData).subscribe((data: any) => {
          if (data) {
            this.Command(command, data)
          }
        })
      }
      reader.readAsDataURL(file)
    } else {
      this.Command(command, file)
    }
  }

  Command(command: any, row_item: any = null): void {
    //console.log(command)
    //console.log(row_item)

    this.onClickCommand.emit({
      command: command,
      object_list: row_item ? [row_item] :
        (this.object_list.length > 0 ? this.object_list.filter(r => r.is_check) :
          (this.object_show_list ? this.object_show_list.filter(r => r.is_check) :
            []
          )
        ),
    })
  }

  onClickCheckBoxSelectAll(value): void {
    console.log(value)
    this.object_parent_list.filter(r => !r.is_deleted).forEach((item: any) => {
      item.is_check = value
    })
    this.object_show_list.filter(r => !r.is_deleted).forEach((item: any) => {
      item.is_check = value
    })
  }
  onClickRadioBox(row_item): void {
    this.initial_table.row_item_selected = row_item
  }

  onClickAdd(): void {
    //console.log("onClickAdd")
    //console.log(this.initial_table)

    //console.log(row_item)
    if (this.initial_table.can_added === true) {
      //console.log(this.initial_table.Get())
      this.initial_table.Add({})
      //console.log(this.initial_table.Get())
    } else if (typeof this.initial_table.can_added == "object") {
      this.initial_table.Add(this.initial_table.can_added)
    } else {
      this.Command(this.initial_table.can_added)
    }
  }
  onClickDelete(item: any = null): void {
    if (item) {
      this.object_parent_list.forEach((object: any) => {
        object.is_check = (object == item)
      })
    }

    var is_have_id
    this.object_parent_list.filter(r => r.is_check).forEach((item: any) => {
      if (item.id) {
        is_have_id = true
      }
    })

    var rs, cancel_reason
    if (is_have_id) {
      cancel_reason = prompt("คุณต้องการลบรายการ?")
      if (cancel_reason) rs = true

      if (cancel_reason == "") {
        alert("กรุณาใส่เหตุผลการลบรายการ")
        return
      }
    } else {
      rs = confirm("คุณต้องการลบรายการ?")
    }

    if (rs) {
      this.object_parent_list.filter(r => r.is_check).forEach((item: any) => {
        if (item.id) {
          if (!cancel_reason) item.is_deleted = true
          item.cancel_reason = cancel_reason
        } else {
          this.object_parent_list.splice(this.object_parent_list.indexOf(item), 1)
          this.object_list.splice(this.object_list.indexOf(item), 1)
        }
      })

      if (this.initial_table.can_deleted !== true) {
        this.onClickCommand.emit({
          command: this.initial_table.can_deleted,
          object_list: this.object_parent_list.filter(r => r.is_check),
        })
      }

      this.updated.updated = true
    }
  }
  onClickIndexUp(row_item: any): void {
    var object_list = this.object_list
    var index = object_list.indexOf(row_item)
    if (index > 0) {
      object_list.splice(index - 1, 0, object_list.splice(index, 1)[0])
    }

    object_list = this.object_parent_list
    var index = object_list.indexOf(row_item)
    if (index > 0) {
      object_list.splice(index - 1, 0, object_list.splice(index, 1)[0])
    }

    object_list = this.object_show_list
    var index = object_list.indexOf(row_item)
    if (index > 0) {
      object_list.splice(index - 1, 0, object_list.splice(index, 1)[0])
    }
  }
  onClickIndexDown(row_item: any): void {
    var object_list = this.object_list
    var index = object_list.indexOf(row_item)
    if (object_list.length > index + 1) {
      object_list.splice(index, 0, object_list.splice(index + 1, 1)[0])
    }

    object_list = this.object_parent_list
    var index = object_list.indexOf(row_item)
    if (object_list.length > index + 1) {
      object_list.splice(index, 0, object_list.splice(index + 1, 1)[0])
    }

    object_list = this.object_show_list
    var index = object_list.indexOf(row_item)
    if (object_list.length > index + 1) {
      object_list.splice(index, 0, object_list.splice(index + 1, 1)[0])
    }
  }
  onClickOpenLink(row_item: any): void {
    window.open(eval(this.initial_table.has_link[0]))
  }

  onClickList(): void {
    this.onClickCommand.emit(this.paginate)
  }

  onClickDuplicateItemSave(): void {
    var object = this.object_parent_list[this.popup.duplicate_index]

    Object.keys(object).forEach((item: any) => {
      if (item != "save_id" || item != "id" || item != "is_contact_person") {
        this.modalAddress[item] = object[item]
      }
    })

    this.modal.isModalPeopleEditOpen = true
    this.popup.is_duplicate_index_show = false
  }

  toggleModal(name: string): void {
    if (!this.modal[name]) { // Is open
      this.modal[name] = true
    } else { // Is close
      this.modal[name] = false
    }
  }

  Keys(object: any): any[] {
    return Object.keys(object)
  }

  eval(data: any, row_item: any) {
    return eval(data)
    //return false
  }
}

