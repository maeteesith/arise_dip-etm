import {
  Component,
  Input,
  Output,
  OnInit,
  AfterContentInit,
  ViewChild,
  ElementRef,
  AfterViewInit,
  EventEmitter,
} from '@angular/core';
import { AppealBlueTabComponent } from './tab/tab.component'
import { Router } from '@angular/router'

@Component({
  selector: 'appeal-blue-tabs',
  templateUrl: './blue-tabs.component.html',
  styleUrls: [
    './blue-tabs.component.scss',
  ]
})
export class AppealBlueTabsComponent implements OnInit {
   //TODO >>> Declarations <<<
   private _className: string
   private _navClassName: string
   tabs: AppealBlueTabComponent[] = []
 
   constructor(
     private router: Router,
     //private staffService: StaffService
   ) { }
 
   get className(): string {
     if (this._className != null || undefined) {
       return this._className.toString().replace(/,/g, " ")
     }
   }
 
   get navClassName(): string {
     if (this._navClassName != null || undefined) {
       return this._navClassName.toString().replace(/,/g, " ")
     }
   }
 
   selectTab(tab: AppealBlueTabComponent) {
     this.tabs.forEach((tab) => {
       tab.active = false;
     });
     tab.active = true;
   }
 
   selectTabDefault() {
     if ((this.tabs.filter(tab => tab.activeDefault === true)).length > 0) {
       this.tabs.forEach((tab) => {
         tab.active = false;
         if (tab.activeDefault === true) {
           tab.active = true;
         }
       });
     }
   }
 
   addTab(tab: AppealBlueTabComponent) {
     if (this.tabs.length === 0) {
       tab.active = true;
     }
 
     if (tab.index === null || undefined) {
       tab.index = this.tabs.length + 1
     }
 
     this.tabs.push(tab);
   }
 
   // Component Props
   @Input() set className(className: string) { this._className = className }
   @Input() set navClassName(navClassName: string) { this._navClassName = navClassName }
   @Input() showTabIndex: boolean = false
   @Input() showAddNewVersion: boolean = false
 
   // Declarations (Output) <<<
   @Output() onClickAddNewVersion = new EventEmitter();
 
   ngOnInit() {
     //TODO >>> Init value <<<
   }
 
   //! <<<< Emit >>>
   _onClickAddNewVersion() {
     this.onClickAddNewVersion.emit(null);
   }
 
   ngAfterContentInit() {
     this.selectTabDefault()
   }

}
