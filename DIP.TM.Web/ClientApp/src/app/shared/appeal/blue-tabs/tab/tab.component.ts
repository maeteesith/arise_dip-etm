import {
  Component,
  Input,
  OnInit,
  AfterContentInit,
} from '@angular/core';
import { Router } from '@angular/router'

import { AppealBlueTabsComponent } from '../blue-tabs.component'

@Component({
  selector: 'appeal-blue-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss']
})
export class AppealBlueTabComponent implements OnInit {
  //TODO >>> Declarations <<<
  private _className: string
  private _tabIndexCustom: string
  active: boolean
  index: any = null

  constructor(tabs: AppealBlueTabsComponent) {
    tabs.addTab(this)
  }

  get className(): string {
    if (this._className != null || undefined) {
      return this._className.toString().replace(/,/g, " ")
    }
  }

  // Component Props
  @Input() set className(className: string) { this._className = className }
  @Input() set tabIndexCustom(tabIndexCustom: any) { this._tabIndexCustom = tabIndexCustom }
  @Input() activeDefault: boolean = false
  @Input() recent: boolean = false
  @Input() title: string

  ngOnInit() {
    if (this._tabIndexCustom != null || undefined) {
      this.index = this._tabIndexCustom
    }
  }

  ngAfterContentInit() {
    // this.tabs.addTab(this)
  }

}
