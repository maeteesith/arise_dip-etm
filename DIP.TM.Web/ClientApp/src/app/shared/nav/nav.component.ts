import { Component, OnInit } from "@angular/core";
import { Router, NavigationStart } from '@angular/router'
import { GlobalService } from "./../../global.service";
import { ROUTE_PATH } from "../../helpers";

@Component({
    selector: "app-nav",
    templateUrl: "./nav.component.html",
    styleUrls: ["./nav.component.scss"]
})
export class NavComponent implements OnInit {
    //TODO >>> Declarations <<<
    public nav: any;
    public isNavFocused: any;
    public userInfo: any;

    constructor(private router: Router, private global: GlobalService) {
        //TODO >>> Detect global change <<<
        this.global.watchUserInfo().subscribe((data: any) => {
            this.userInfo = data;
        });
        //TODO >>> Detect route change <<<
        this.router.events.subscribe((event: any) => {
            if (event instanceof NavigationStart) {
                this.clickOutside();
            }
        })
    }

    ngOnInit() {
        //TODO >>> Init value <<<
        let userInfo = this.global.getUserInfo();
        this.nav = {
            isNavSecondSelected: false,
            isNavThirdSelected: false
        };
        this.isNavFocused = false;
        this.userInfo = {
            uM_PageMenu: []
        };
        this.userInfo = userInfo ? this.initNav(userInfo) : {};
    }

    //! <<< Event >>>
    goDashboard(): void {
        this.clickOutside();
        this.router.navigate([ROUTE_PATH.DASHBOARD.LINK]);
    }
    isStayDashboard(): boolean {
        return location.pathname === ROUTE_PATH.DASHBOARD.LINK;
    }
    getLink(page: any): string {
        if (page && page.is_show_menu) {
            return ROUTE_PATH[`${page.code}`.trim()] ? ROUTE_PATH[`${page.code}`.trim()].LINK : ''
        }
    }
    go(code: any): void {
        this.router.navigate([ROUTE_PATH[`${code}`.trim()].LINK]);
    }

    //! <<< Nav >>>
    initNav(userInfo: any): void {
        if (userInfo) {
            userInfo.uM_PageMenu.forEach((menu: any) => {
                menu.uM_PageMenuSub.forEach((sub: any) => {
                    sub.isOpen = false;
                    sub.uM_Page.forEach((item: any) => {
                        item.isActive = false;
                        if (ROUTE_PATH[item.code]) {
                            //if (item.code == "DOCUMENT_ROLE02_PRINT_DOCUMENT_LIST") {
                            //console.log(item)
                            //}
                            if (location.pathname.includes(ROUTE_PATH[item.code].LINK)) {
                                sub.isOpen = true;
                                item.isActive = true;
                            }
                        }
                    });
                });
            });
        }
        return userInfo;
    }
    selectNav(name: string): void {
        for (let key in this.nav) {
            this.nav[key] = false;
        }
        this.nav[name] = true;
        this.isNavFocused = true;
        this.userInfo = this.initNav(this.userInfo);
    }
    toggleSubNav(sub: any, name: string): void {
        sub[name] = !sub[name];
    }
    setActive(item: any, value: boolean): void {
        item.isActive = value;
    }
    clickOutside(): void {
        this.isNavFocused = false;
        for (let key in this.nav) {
            this.nav[key] = false;
        }
    }
}
