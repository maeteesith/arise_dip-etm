// Angular
import { Component, OnInit, Inject, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
// RxJS
import { of, Subscription, Subject, Observable } from 'rxjs';
// NGRX

// State

// Services


@Component({
	selector: 'kt-confirmation-dialog',
	templateUrl: './confirmation.dialog.component.html',
	styleUrls: ['./confirmation.dialog.component.scss'],
	changeDetection: ChangeDetectionStrategy.Default,
})
export class ConfirmationDialogComponent implements OnInit, OnDestroy, AfterViewInit {
	public $loadingSubject: Subject<boolean> = new Subject<boolean>();
	public $loading: Observable<boolean> = of(false);

	// Public properties
	public iconUrl: string = null;
	public title: string = null;
	public titlecolor: string = null;
	public description: string = null;
	public buttons: any[] = [];

	// Private properties
	private componentSubscriptions: Subscription;

	/**
	 * Component constructor
	 *
	 * @param dialogRef: MatDialogRef<RoleEditDialogComponent>
	 * @param data: any
	 */
	constructor(
		private changeDetectorRef: ChangeDetectorRef,
		private router: Router,
		private http: HttpClient,
		public dialogRef: MatDialogRef<ConfirmationDialogComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any) {

		this.iconUrl = data.iconUrl;
		this.title = data.title;
		this.description = data.description;
		this.buttons = data.buttons;
		this.titlecolor = data.titlecolor

		this.buttons.forEach(button => {
			button.color = button.color || 'rgba(0, 0, 0, 0.87)';
			button.backgroundColor = button.backgroundColor || 'rgba(0, 0, 0, 0)';
		});
	}

	/**
	 * @ Lifecycle sequences => https://angular.io/guide/lifecycle-hooks
	 */

	/**
	 * On init
	 */
	ngOnInit() {
	}

	ngAfterViewInit() {
	}

	/**
	 * On destroy
	 */
	ngOnDestroy() {
		if (this.componentSubscriptions) {
			this.componentSubscriptions.unsubscribe();
		}
	}

	onClickButton(button: any) {
		this.dialogRef.close(button.id);
	}
}
