import { Component, Input, OnInit, EventEmitter, Output, } from '@angular/core';

//import {
//  CONSTANTS,
//} from '../../helpers'

@Component({
  selector: 'app-paging',
  templateUrl: './paging.component.html',
  styleUrls: ['./paging.component.scss']
})

export class PagingComponent {
  constructor(
    //private SaveProcessService: SaveProcessService
  ) { }

  ////TODO >>> Declarations <<<
  ////private _className: string
  ////active: boolean
  //public uid: any
  //public input: any
  //public address: any
  //public master: any

  //public timeout: any
  //public is_running: any
  //public showAutocomplete: string
  //public autocompleteList: any

  @Input() paginate: any
  @Output() onClickList = new EventEmitter();
  @Input() minimum: boolean = false

  @Input() item_all_list: any[]
  @Input() item_list: any[]
  @Input() is_updated: any


  public item_all_list_count: any
  public perPage: any[]

  ////get className(): string {
  ////  if (this._className != null || undefined) {
  ////    return this._className.toString().replace(/,/g, " ")
  ////  }
  ////}

  ////// Component Props
  ////@Input() set className(className: string) { this._className = className }

  ngOnInit() {
    //    this.uid = CONSTANTS.GetUID()

    //    this.address = this.params[0]
    //    this.master = this.params[1]

    this.perPage = [10, 50, 100, 500, 1000]
    this.item_all_list_count = 0
    if (!this.paginate)
      this.paginate = {
        page_index: 1,
        item_per_page: +this.perPage[0],
        item_total: 0,
      }

    //    //console.log(this.master.addressCountryCodeList)
    //    //TODO >>> Init value <<<

    var pThis = this
    if (pThis.item_all_list) {
      pThis.is_updated = pThis.is_updated || {}

      var interval = setInterval(function () {
        var item_all_list = pThis.item_all_list.filter(r => !r.is_deleted)
        if (pThis.is_updated.updated || pThis.item_all_list_count != item_all_list.length) {
          //console.log(pThis.item_all_list)

          //console.log(pThis.is_updated)
          //console.log(pThis.is_updated)

          //console.log("b")
          pThis.is_updated.updated = false
          pThis.item_all_list_count = item_all_list.length

          pThis.paginate = {
            page_index: 1,
            item_per_page: +pThis.perPage[0],
            item_total: item_all_list.length,
          }
          pThis.onCallBack()
          //clearInterval(interval)
        }
      }, 10);
    }
  }

  onCallBack() {
    //console.log("onCallBack")
    //console.log(this.paginate)
    if (this.paginate && this.paginate.item_per_page) {
      this.paginate.item_per_page = +this.paginate.item_per_page
    }

    if ((!this.paginate || !this.paginate.item_start)) {
      //console.log("onCallBack1")
      //console.log(this.item_all_list)

      this.item_list = this.item_list || []

      this.item_list.length = 0
      var item_start = this.getItemStart()
      var item_end = this.getItemEnd()
      //console.log(item_start)
      //console.log(item_end)
      var index_show = 1
      this.item_all_list.filter(r => !r.is_deleted).forEach((item, index) => {
        //console.log(index)
        if (item_start <= index + 1 && index + 1 <= item_end) {
          item.index = index + 1
          item.index_show = index_show++
          this.item_list.push(item)
        }
      })
    } else {
      //console.log("onCallBack2")
      this.onClickList.emit(null)
    }
  }

  getItemStart(): any {
    return Math.min(this.paginate.item_total, (this.paginate.page_index - 1) * this.paginate.item_per_page + 1)
  }

  getItemEnd(): any {
    return Math.min(this.paginate.item_total, this.paginate.page_index * this.paginate.item_per_page)
  }

  getMaxPage(): any {
    //console.log(this.paginate.item_total)
    //console.log(this.paginate.item_per_page)
    var max_page = Math.ceil(this.paginate.item_total / this.paginate.item_per_page)
    //console.log(max_page)
    //var pThis = this
    //setTimeout(function () {
    //  console.log(max_page)
    //  console.log(pThis.paginate.page_index)
    this.paginate.page_index = Math.min(max_page, this.paginate.page_index)
    //}, 100)
    return max_page
  }
}

