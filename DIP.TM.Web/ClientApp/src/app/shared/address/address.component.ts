import { Component, Input, OnInit, } from '@angular/core';
import { SaveProcessService } from '../../services/save-process-buffer.service'
import { Help } from '../../helpers/help'

import {
  CONSTANTS,
} from '../../helpers'

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})

export class AddressComponent {
  constructor(
    private help: Help,
    private SaveProcessService: SaveProcessService
  ) { }

  //TODO >>> Declarations <<<
  //private _className: string
  //active: boolean
  public uid: any
  public input: any
  public address: any
  public master: any
  public default: any

  public timeout: any
  public is_running: any
  public showAutocomplete: string
  public autocompleteList: any

  @Input() params: any[]

  //get className(): string {
  //  if (this._className != null || undefined) {
  //    return this._className.toString().replace(/,/g, " ")
  //  }
  //}

  //// Component Props
  //@Input() set className(className: string) { this._className = className }

  ngOnInit() {
    this.uid = CONSTANTS.GetUID()

    this.address = this.params[0]
    this.master = this.params[1]
    if (this.params.length > 2) {
      this.default = this.params[2]
    }

    this.autocompleteList = {}

    setInterval(() => {
      //console.log("d")
      this.onChangeAddressTypeCode()
    }, 1000)

    //console.log(this.master.addressCountryCodeList)
    //TODO >>> Init value <<<
  }

  //! <<< Autocomplete >>>
  onChangeAutocomplete(name: any): void {
    var pThis = this
    //console.log("a")
    if (pThis.timeout) {
      clearTimeout(pThis.timeout)
      pThis.timeout = null
    }

    if (!pThis.is_running) {
      console.log("b")
      pThis.timeout = setTimeout(() => {
        console.log("c")
        pThis.is_running = true
        pThis.showAutocomplete = name
        if (name == 'address_sub_district_name') {
          pThis.callAutocomplete({
            filter: { name: pThis.address[name] },
            paging: {
              item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page
            }
          }, name)
        }
      }, CONSTANTS.DELAY_CALL_API)
    }
  }

  callAutocomplete(params: any, name: any): void {
    this.SaveProcessService.Save01ListModalLocation(params).subscribe((data: any) => {
      if (data) {
        console.log(data)
        if (data.length == 1) {
          this.onSelectAutocomplete('address_sub_district_name', data[0])
        } else if (data.length > 1) {
          this.autocompleteList[name] = data
        } else {
          this.showAutocomplete = ""
        }
        //    this.isShowAutocomplete[name] = true
      } else {
        //    this.isShowAutocomplete[name] = false
        //    this.autocompleteList[name] = []
      }
      this.timeout = null
      this.is_running = null
    })
  }
  onSelectAutocomplete(name: any, item: any): void {
    if (name === 'address_sub_district_name') {
      this.address.postal_code = item.postal_code
      this.address.postal_name = item.postal_name
      this.address.address_sub_district_code = item.code
      this.address.address_sub_district_name = item.name
      this.address.address_district_code = item.district_code
      this.address.address_district_name = item.district_name
      this.address.address_province_code = item.province_code
      this.address.address_province_name = item.province_name
      this.address.address_country_code = item.country_code
      this.address.address_country_name = item.country_name
    }
    this.onClickOutsideAutocomplete()
  }
  onClickOutsideAutocomplete(): void {
    this.showAutocomplete = ""
    //this.autocompleteList[name] = []
  }

  onChangeAddressTypeCode(): void {
    if (this.default) {
      //console.log(event)
      //console.log(this.address.address_type_code)
      var address_type_code = this.address.address_type_code

      if (this.address.address_type_code == "OWNER") {
        if (this.default.people_list.length > 0) {
          this.help.Clone(this.default.people_list[0], this.address)
          this.address.address_type_code = address_type_code;
        } else {
          alert("ไม่พบเจ้าของ")
          this.address.address_type_code = "OTHERS"
        }
      } else if (this.address.address_type_code == "REPRESENTATIVE") {
        if (this.default.representative_list.length > 0) {
          this.help.Clone(this.default.representative_list[0], this.address)
          this.address.address_type_code = address_type_code;
        } else {
          alert("ไม่พบตัวแทน")
          this.address.address_type_code = "OTHERS"
        }
      }
    }
  }

  binding(key: any, value: any): void {
    this.address[key] = value
  }
}

