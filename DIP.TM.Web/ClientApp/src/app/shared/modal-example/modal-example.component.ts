import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
import { CONSTANTS, clone } from "../../helpers";

@Component({
  selector: 'app-modal-example',
  templateUrl: './modal-example.component.html',
  styleUrls: ['./modal-example.component.scss']
})
export class ModalExampleComponent implements OnInit {

  //TODO >>> Declarations <<<
  private _className: string
  private _modalSize: string = ''
  private _modalVerticalCenter: any = false

  // Init
  public input: any;

  // Paginate
  public paginateTranslation: any;

  public tableDetail: any;

  constructor(
    private router: Router,
    private global: GlobalService,
    //private staffService: StaffService
  ) { }

  get className(): string {
    if (this._className != null || undefined) {
      return this._className.toString().replace(/,/g, " ")
    }
  }

  get modalSize(): string {
    switch (this._modalSize) {
      case 'long':
        return 'modal-lg'
      case 'large':
        return 'modal-lg'
      case 'lg':
        return 'modal-lg'
      case 'short':
        return 'modal-sm'
      case 'small':
        return 'modal-sm'
      case 'sm':
        return 'modal-sm'
      case 'fiuld':
        return 'modal-fiuld'
      case 'full':
        return 'modal-fiuld'
      case 'xl':
        return 'modal-xl'
      default:
        return null
    }
  }

  get modalVerticalCenter(): string {
    if (this._modalVerticalCenter) {
      return 'modal-dialog-centered'
    }
    else {
      return null
    }
  }

  _onClickClose() {
    this.onClickClose.emit(null);
  }

  // Component Props
  @Input() set className(className: string) { this._className = className }
  @Input() set modalSize(modalSize: string) { this._modalSize = modalSize }
  @Input() set modalVerticalCenter(modalVerticalCenter) { this._modalVerticalCenter = modalVerticalCenter }
  @Input() title: string
  @Input() hideCloseCross: boolean = false
  @Input() closeButtonText: string = 'ยกเลิก'

  // Component Event Props
  @Output() onClickClose = new EventEmitter();

  ngOnInit() {
    this.input = {};
    
    // Paginate
    this.paginateTranslation = clone(CONSTANTS.PAGINATION.INIT);
    this.paginateTranslation.id = "paginateTranslation";

    this.tableDetail = {
      column_list: [
        {
          data: "ทั้งนี้ ให้ดำเนินการภายใน 30 วัน นับแต่วันที่ได้รับหนังสือฉบับนี้ มิฉะนั้น จะพิจารณาคำขอจดทะเบียนนี้ตามหลักฐานและข้อเท็จจริงที่มีอยู่",
        },
        {
          data: "ให้นำส่งต้นฉบับหนังสือสำคัญแสดงการจดทะเบียนเครื่องหมายการค้าประกอบการยื่น คำขอโอน ก.04 ลงวันที่.......ตามกฏกระทรวงข้อ 32",
        },
        {
          data: "ให้นำส่งต้นฉบับหนังสือสำคัญแสดงการจดทะเบียนเครื่องหมายการค้าประกอบการยื่น คำขอเปลี่ยนแปลง ก.06 ลงวันที่.......ตามกฏกระทรวงข้อ 35",
        },
        {
          data: "ให้นำส่งต้นฉบับหนังสือสำคัญแสดงการจดทะเบียนเครื่องหมายการค้าประกอบการยื่น คำขอโอน ก.04 ลงวันที่.......ตามกฏกระทรวงข้อ 32",
        },
        {
          data: "ให้นำส่งต้นฉบับหนังสือสำคัญแสดงการจดทะเบียนเครื่องหมายการค้าประกอบการนื่น คำขอแก้ไขเปลี่ยนแปลง ก.06 ลงวันที่.......ตามกฏกระทรวงข้อ 35",
        },
        {
          data: "ให้ยื่นโอนคำขอ......เนื่องจากเป็นเครื่องหมายชุด",
        },
        {
          data: "ให้นาย......ผู้โอนมาลงลายมือชื่อในสัญญาโอนต่อหน้านายทะเบียน เนื่องจากลายมือชื่อ ได้เปลี่ยนไปจากเดิม",
        },
        {
          data: "โปรตจำหน่าย / ปลดปล่อยค่าขอ (ควบคุมเวลา) ",
        },
        {
          data: "โปรดพิจารณาเอกสาร (ชี้แจงเรื่องการถอนคำขอ / ฟื้นค่าขอ) ตามหนังสือ....",
        },
        {
          data: "โปรดพิจารณาเอกสาร (ชี้แจงทั่วไป) ตามหนังสือ...",
        },
      ],
    }
  }

  //! <<<< Pagination >>>
  managePaginateCallApi(name: string): void {
    if (name === "paginateTranslation") {
      this.input.is_check_all = false;
      //TODO call api
    }
  }
  onChangePage(page: any, name: string): void {
    this[name].currentPage = page === "" ? 1 : page;
    if ((+page || page === "") && page <= this.getMaxPage(name)) {
      this.managePaginateCallApi(name);
    }
  }
  onChangePerPage(value: number, name: string): void {
    this[name].itemsPerPage = value;
    this.managePaginateCallApi(name);
  }
  changePaginateTotal(total: any, name: string): void {
    this[name].totalItems = total;
  }
  getMaxPage(name: string): number {
    return Math.ceil(this[name].totalItems / this[name].itemsPerPage);
  }
}
