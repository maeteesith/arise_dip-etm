import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-dragdrop-image',
  templateUrl: './dragdrop-image.component.html',
  styleUrls: ['./dragdrop-image.component.scss']
})

export class DragdropImageComponent implements OnInit {

  files: any[] = [];
  public imagePath: any;
  imgURL: any;
  public message: string;
  @Input() blob: any
  @Input() maxSize: any
  @Output() onChangeUploadImage = new EventEmitter();
  @Output() onRemoveImage = new EventEmitter();

  ngOnInit() {
    //TODO >>> Init value <<<
    this.setInput()
  }

  ngOnChanges() {
    this.setInput()
  }

  setInput() {
    if(this.blob) {
      this.imgURL = this.blob;
    }
    this.maxSize = this.maxSize ? this.maxSize : 5242880 // 5 MB
  }

  /**
   * on file drop handler
   */
  onFileDropped($event) {
    this.prepareFilesList($event);
  }

  /**
   * handle file from browsing
   */
  fileBrowseHandler(files) {
    this.prepareFilesList(files);
  }

  /**
   * Delete file from files list
   * @param index (File index)
   */
  deleteFile() {
    this.imgURL = null
    this.onRemoveImage.emit()
    // this.files.splice(index, 1);
  }

  /**
   * Simulate the upload process
   */
  uploadFilesSimulator(index: number) {
    setTimeout(() => {
      if (index === this.files.length) {
        return;
      } else {
        const progressInterval = setInterval(() => {
          if (this.files[index].progress === 100) {
            clearInterval(progressInterval);
            this.uploadFilesSimulator(index + 1);
          } else {
            this.files[index].progress += 5;
          }
        }, 200);
      }
    }, 1000);
  }

  /**
   * Convert Files list to normal array list
   * @param files (Files List)
   */
  prepareFilesList(files: Array<any>) {
    if(files[0].type.includes('image/jpeg') || files[0].type.includes('image/png')) { // Check type file
      if(files[0].size <= this.maxSize) { // Check size file
        for (const item of files) {
          item.progress = 0;
          this.files.push(item);
        }
        this.uploadFilesSimulator(0);
    
        var reader = new FileReader();
        this.imagePath = files;
        reader.readAsDataURL(files[0]);
        reader.onload = (_event) => {
          this.imgURL = reader.result;
          this.onChangeUploadImage.emit({file: files[0], blob: reader.result});
        }
      } else {
        this.onChangeUploadImage.emit({validate: "ขนาดไฟล์เกินกำหนด"});
      }
    } else {
      this.onChangeUploadImage.emit({validate: "รูปแบบไฟล์ไม่ถูกต้อง"});
    }
  }

  /**
   * format bytes
   * @param bytes (File size in bytes)
   * @param decimals (Decimals point)
   */
  formatBytes(bytes, decimals) {
    if (bytes === 0) {
      return '0 Bytes';
    }
    const k = 1024;
    const dm = decimals <= 0 ? 0 : decimals || 2;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    const i = Math.floor(Math.log(bytes) / Math.log(k));
    return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
  }

  // preview(files) {
  //   if (files.length === 0)
  //     return;

  //   var mimeType = files[0].type;
  //   if (mimeType.match(/image\/*/) == null) {
  //     this.message = "Only images are supported.";
  //     return;
  //   }

  //   var reader = new FileReader();
  //   this.imagePath = files;
  //   reader.readAsDataURL(files[0]);
  //   reader.onload = (_event) => {
  //     this.imgURL = reader.result;
  //   }
  // }
}
