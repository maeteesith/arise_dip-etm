import { Component, Input, OnInit, EventEmitter, Output, HostListener, ElementRef, } from '@angular/core';
import { ApiService } from '../../services/apiService'
import { Help } from '../../helpers/help'
import { clone, CONSTANTS } from '../../helpers'
//import { SaveProcessService } from '../../services/save-process-buffer.service'

//import {
//  CONSTANTS,
//} from '../../helpers'

@Component({
    selector: 'app-autocomplete',
    templateUrl: './autocomplete.component.html',
    styleUrls: ['./autocomplete.component.scss']
})

export class AutocompleteComponent {
    constructor(
        private eRef: ElementRef,
        private apiService: ApiService,
        private help: Help,
        //private SaveProcessService: SaveProcessService
    ) { }

    //TODO >>> Declarations <<<
    //public uid: any
    //public address: any
    //public master: any

    //public timeout: any
    public buffer: any

    public is_show_result: boolean
    public is_have_result: boolean
    public result_list: any[]

    public search_value: string
    public search_paging: any

    //public showAutocomplete: string
    //public autocompleteList: any

    @Input() title: string
    @Input() url: string
    //@Input('variable') variable: any
    @Input() table_count: number = 1
    @Input() column_search_list: string[]
    @Input() column_list: string[]
    @Input() column_show: string
    @Input() column_value: string

    @Input() search_item_per_page: number
    @Input() search_orderby: string

    @Output() updateBack = new EventEmitter<any>()
    @Output() updateFetch = new EventEmitter<any>()
    @Output() updateFocusout = new EventEmitter<any>()

    //@Input() params: any[]

    //get className(): string {
    //  if (this._className != null || undefined) {
    //    return this._className.toString().replace(/,/g, " ")
    //  }
    //}

    //// Component Props
    //@Input() set className(className: string) { this._className = className }

    ngOnInit() {
        this.buffer = {}
        console.log(this.updateFetch)
        //this.uid = CONSTANTS.GetUID()

        //this.address = this.params[0]
        //this.master = this.params[1]

        //this.autocompleteList = {}

        this.column_search_list = this.column_search_list || this.column_list
        this.column_show = this.column_show || this.column_list[0]
        this.column_value = this.column_value || this.column_list[0]

        this.search_paging = clone(CONSTANTS.PAGINATION.INIT)
        if (this.search_item_per_page) this.search_paging.item_per_page = this.search_item_per_page
        if (this.search_orderby) this.search_paging.orderby = this.search_orderby

        //console.log(this.variable)

        //this.onClickAutocompleteList()
    }

    onClickAutocompleteList(): void {
        if (!this.buffer.isPosting) {
            clearTimeout(this.buffer.timeoutAutocompleteList)

            var pThis = this
            this.buffer.timeoutAutocompleteList = setTimeout(function () {
                pThis.buffer.isPosting = true
                pThis.is_show_result = true
                pThis.is_have_result = true

                var param = {
                    code_filter_queries: {
                        column_list: pThis.column_search_list,
                        value: pThis.search_value || ""
                    }
                }
                pThis.apiService.post(pThis.url, pThis.help.GetFilterParams(param, pThis.search_paging)).subscribe((data: any) => {
                    pThis.listData(data)
                    pThis.buffer.isPosting = false
                })
            }, 300)
        }
    }

    onClickAutocompleteFocusout(): void {
        console.log("onClickAutocompleteFocusout")
        this.updateFocusout.emit(this.search_value)
    }

    listData(data: any) {
        this.is_show_result = true

        this.help.PageSet(data, this.search_paging)

        this.result_list = new Array(this.table_count)
        if (data && data.list && data.list.length > 0) {
            var pThis = this
            data.list.forEach(function (item, index) {
                pThis.result_list[index % pThis.table_count] = pThis.result_list[index % pThis.table_count] || []
                pThis.result_list[index % pThis.table_count].push(item)
            })

            if (data.list.length == 1) {
                this.onClickAutocompleteSelect(data.list[0])
            }
        } else {
            this.is_have_result = false
        }
    }

    onClickAutocompleteFetch(): void {
        this.is_show_result = true
        this.is_have_result = true

        var result_list = []
        var is_ok = []

        this.updateFetch.emit({ result_list: result_list, is_ok: is_ok })
        this.listData({
            list: result_list,
            paging: {
                page_index: 1,
                item_per_page: this.search_item_per_page,
                item_total: result_list.length,
            }
        })

        console.log(is_ok)
        if (is_ok.length > 0) {
            this.is_show_result = false
        }

        //if (this.result_list.length == 0) {

        //}

        console.log(this.result_list)
    }

    onClickAutocompleteChange(): void {
        this.onClickAutocompleteList()
    }

    onClickAutocompleteSelect(row_item: any): void {
        this.search_value = row_item[this.column_show]
        this.updateBack.emit({ item: row_item, value: row_item[this.column_value] })

        this.is_show_result = false
    }


    @HostListener('document:click', ['$event'])
    clickout(event) {
        if (!this.eRef.nativeElement.contains(event.target)) {
            this.is_show_result = false
        }
    }
}

