import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GlobalService } from "../../global.service";
// import { StaffService } from "../../services";
import {
  ROUTE_PATH,
  LOCAL_STORAGE,
  setLocalStorage,
  TEXT_VALIDATE,
  isInvalidRequired
} from "../../helpers";

@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent implements OnInit {
  //TODO >>> Declarations <<<
  public validate: any
  public model: any

  constructor(
    private router: Router,
    private global: GlobalService,
    //private staffService: StaffService
  ) { }

  ngOnInit() {
    //TODO >>> Init value <<<
  }
}
