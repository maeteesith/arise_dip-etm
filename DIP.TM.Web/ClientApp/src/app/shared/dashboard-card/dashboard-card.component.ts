import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../global.service'
// import { StaffService } from '../../services'

@Component({
  selector: 'app-dashboard-card',
  templateUrl: './dashboard-card.component.html',
  styleUrls: ['./dashboard-card.component.scss']
})

export class DashboardCardComponent implements OnInit {

  //TODO >>> Declarations <<<
  public _className: string
  public _theme: string
  public _iconMain: any
  public _iconMainSize: string
  public _iconSub: any
  public _iconSubSize: string

  constructor(
    private router: Router,
    private global: GlobalService,
    //private staffService: StaffService
  ) { }

  get className(): string {
    if (this._className != null || undefined) {
      return this._className.toString().replace(/,/g, " ")
    }
  }

  ngOnInit() {
    //TODO >>> Init value <<<
  }

  get theme(): string {
    switch (this._theme) {
      case 'blue':
        return 'theme-blue'
      case 'orange':
        return 'theme-orange'
      case 'violet':
        return 'theme-violet'
      case 'red':
        return 'theme-red'
      case 'green':
        return 'theme-green'
      case 'black':
        return 'theme-black'
      case 'sky':
        return 'theme-sky'
      case 'pink':
        return 'theme-pink'
        case 'blackgray':
        return 'theme-blackgray'
        case 'darkorange':
          return 'theme-darkorange'
      case 'gray':
          return 'theme-gray'
       case 'lightblack':
          return 'theme-lightblack'
      case 'bluelight':
          return 'theme-bluelight'


      default:
        return null
    }
  }

  get iconMain(): string {
    switch (this._iconMain) {
      case 'list-board':
        this._iconMainSize = '55'
        return 'dip-icon-list-board'
      case 'recycle-rectangle':
        this._iconMainSize = '72'
        return 'dip-icon-recycle-rectangle'
      case 'document-tray':
        this._iconMainSize = '55'
        return 'dip-icon-document-tray'
      case 'order-list':
        this._iconMainSize = '50'
        return 'dip-icon-order-list'
      case 'announce':
        this._iconMainSize = '55'
        return 'dip-icon-announce'
      case 'signature':
        this._iconMainSize = '55'
        return 'dip-icon-signature'
      case 'arrow-right-circle':
        this._iconMainSize = '55'
        return 'dip-icon-arrow-right-circle'
        case 'pluple':
        this._iconMainSize = '55'
        return 'dip-icon-arrow-top'
        case 'dip-icon-person':
        this._iconMainSize = '55'
        return 'dip-icon-person'
        case 'dip-icon-check-2':
        this._iconMainSize = '55'
        return 'dip-icon-check-2'
      default:
        return null
    }
  }

  get iconSub(): string {
    switch (this._iconSub) {
      case 'play':
        this._iconSubSize = '9'
        return 'dip-icon-play'
      case 'edit':
        this._iconSubSize = '9'
        return 'dip-icon-edit-pen'
      case 'search':
        this._iconSubSize = '9'
        return 'dip-icon-search'
      default:
        return null
    }
  }

  // Component Props
  @Input() set className(className: string) { this._className = className }
  @Input() set theme(theme: string) { this._theme = theme }
  @Input() quantity: string
  @Input() title: string
  @Input() description: string
  @Input() set iconMain(iconMain: string) { this._iconMain = iconMain }
  @Input() set iconSub(iconSub: string) { this._iconSub = iconSub }
}
