import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router'
import { GlobalService } from '../../../global.service'
// import { StaffService } from '../../services'

@Component({
  selector: 'app-dashboard-card-summary-block',
  templateUrl: './dashboard-card-summary-block.component.html',
  styleUrls: ['./dashboard-card-summary-block.component.scss']
})

export class DashboardCardSummaryBlockComponent implements OnInit {

  //TODO >>> Declarations <<<

  constructor(
    private router: Router,
    private global: GlobalService,
    //private staffService: StaffService
  ) { }

  ngOnInit() {
    //TODO >>> Init value <<<
  }

  // Component Props
  @Input() caption: any
  @Input() summary: any
}
