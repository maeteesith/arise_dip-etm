import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

/* lib */
import { NgxPaginationModule } from "ngx-pagination";
import { ClickOutsideModule } from "ng-click-outside";

/* angular material */
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import {
  MatDatepickerModule,
  MatAutocompleteModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatStepperModule,
  MatInputModule,
  MatRippleModule,
  MatDialogModule,
  MatTabsModule,
  MatTooltipModule
} from "@angular/material";
import { DateAdapter } from "@angular/material/core";
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS,
} from "@angular/material-moment-adapter";
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from "@angular/material";

/* component */
import {
  HeaderComponent,
  // NavComponent,
  ModalComponent,
  ModalEformOwnerComponent,
  ModalEformAgentComponent,
  ModalMadridRole01MM02Component,
  PopupComponent,
  AddressComponent,
  AutocompleteComponent,
  TableComponent,
  PagingComponent,
  PaginationComponent,
  TabsComponent,
  TabComponent,
  DashboardCardComponent,
  DashboardCardSummaryBlockComponent,
  ConfirmationDialogComponent,
  EditorDialogComponent,
  DragdropImageComponent,
  DndDirective,
  ModalExampleComponent,
  ModalDialogComponent,
  AppealBlueTabsComponent,
  AppealBlueTabComponent,
  AppealWhiteTabsComponent,
  AppealWhiteTabComponent,
} from "../shared";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ArchwizardModule } from "angular-archwizard";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    ClickOutsideModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatAutocompleteModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatDialogModule,
    MatTabsModule,
    MatIconModule,
    MatListModule,
    MatStepperModule,
    MatInputModule,
    MatRippleModule,
    MatTooltipModule,
    NgbModule,
    ArchwizardModule
  ],
  declarations: [
    HeaderComponent,
    // NavComponent,
    ModalComponent,
    ConfirmationDialogComponent,
    EditorDialogComponent,
    ModalEformOwnerComponent,
    ModalEformAgentComponent,
    ModalMadridRole01MM02Component,
    PopupComponent,
    AddressComponent,
    AutocompleteComponent,
    TableComponent,
    PagingComponent,
    PaginationComponent,
    TabsComponent,
    TabComponent,
    DashboardCardComponent,
    DashboardCardSummaryBlockComponent,
    DragdropImageComponent,
    DndDirective,
    ModalExampleComponent,
    ModalDialogComponent,
    AppealBlueTabsComponent,
    AppealBlueTabComponent,
    AppealWhiteTabsComponent,
    AppealWhiteTabComponent,
  ],
  exports: [
    HeaderComponent,
    // NavComponent,
    ModalComponent,
    ConfirmationDialogComponent,
    EditorDialogComponent,
    ModalEformOwnerComponent,
    ModalEformAgentComponent,
    ModalMadridRole01MM02Component,
    PopupComponent,
    AddressComponent,
    AutocompleteComponent,
    TableComponent,
    PagingComponent,
    PaginationComponent,
    TabsComponent,
    TabComponent,
    DashboardCardComponent,
    DashboardCardSummaryBlockComponent,
    DragdropImageComponent,
    DndDirective,
    ModalExampleComponent,
    ModalDialogComponent,
    AppealBlueTabsComponent,
    AppealBlueTabComponent,
    AppealWhiteTabsComponent,
    AppealWhiteTabComponent,
  ],
  entryComponents:[ConfirmationDialogComponent,EditorDialogComponent],
  providers: [
    {
      provide: DateAdapter,
      useClass: MomentDateAdapter,
      deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS],
    },
    {
      provide: MAT_DATE_FORMATS,
      useValue: {
        parse: {
          dateInput: ["l", "LL"],
        },
        display: {
          dateInput: "L",
          monthYearLabel: "MMMM YYYY",
          dateA11yLabel: "LL",
          monthYearA11yLabel: "MMMM YYYY",
        },
      },
    },
    {
      provide: MAT_DATE_LOCALE,
      useValue: "th",
    },
  ],
})
export class SharedModule { }
