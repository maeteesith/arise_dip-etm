import { Component, Input, Output, OnInit, EventEmitter } from "@angular/core";
import { eFormSaveProcessService } from "../../services/eform-save-process.service";
import { CONSTANTS, validateService } from "../../helpers";

@Component({
  selector: "app-modal-eform-agent",
  templateUrl: "./modal-eform-agent.component.html",
  styleUrls: ["./modal-eform-agent.component.scss"],
})
export class ModalEformAgentComponent implements OnInit {
  //TODO >>> Declarations <<<
  public validate: any;
  public autocompleteList: any;
  public isShowAutocomplete: any;
  public timeout: any;

  //TODO >>> Declarations (Input) <<<
  @Input() title: any;
  @Input() isOpen: any;
  @Input() item: any;
  @Input() master: any;
  @Input() isDisabledInfo: any;

  //TODO >>> Declarations (Output) <<<
  @Output() toggleModal = new EventEmitter();
  @Output() onClickSaveModalEform = new EventEmitter();

  constructor(private eformSaveProcessService: eFormSaveProcessService) {}

  ngOnInit() {
    this.validate = {};
    this.autocompleteList = {
      address_sub_district_name: [],
    };
    this.isShowAutocomplete = {
      address_sub_district_name: false,
    };
  }

  //! <<<< Emit >>>
  _toggleModal() {
    this.validate = {};
    this.toggleModal.emit();
  }
  _onClickSaveModalEform(item: any) {
    if (this.validateSaveModalEform(item)) {
      this.onClickSaveModalEform.emit(item);
    }
  }

  //! <<<< Event >>>
  bindingInTable(item: any, name: any, value: any): void {
    item[name] = value;
    if (name === "receiver_type_code") {
      if (value === "CORPORATE") {
        this.item.card_type_code = "TAX_ID";
      } else if (value == "PEOPLE") {
        this.item.card_type_code = "NAT_ID";
      }
    }
  }
  clearValidate(name: string): void {
    this.validate[name] = null;
  }
  validateSaveModalEform(item: any): boolean {
    let nameValidate = this.isDisabledInfo
      ? "validateEFormModalAddressOnly"
      : "validateEFormModalAgent";

    let result = validateService(nameValidate, item);
    this.validate = result.validate;
    if (
      result.validate.address_district_name ||
      result.validate.address_province_name ||
      result.validate.postal_code
    ) {
      this.validate.address_sub_district_name = "กรุณากรอกและเลือก ตำบล";
    }
    return result.isValid;
  }

  //! <<< Autocomplete >>>
  callAutocomplete(params: any, name: any): void {
    this.eformSaveProcessService
      .SearchLocation(params)
      .subscribe((data: any) => {
        if (data) {
          this.autocompleteList[name] = data;
          this.isShowAutocomplete[name] = true;
        } else {
          this.isShowAutocomplete[name] = false;
          this.autocompleteList[name] = [];
        }
      });
  }
  onChangeAutocomplete(obj: any, name: any): void {
    this.validate[name] = null;
    this.clearValue(name);
    clearTimeout(this.timeout);
    this.timeout = setTimeout(() => {
      if (this[obj][name]) {
        if (name === "address_sub_district_name") {
          this.callAutocomplete(
            {
              filter: { name: this[obj][name] },
              paging: {
                item_per_page: CONSTANTS.AUTOCOMPLETE.item_per_page,
              },
            },
            name
          );
        }
      } else {
        this.onClickOutsideAutocomplete(name);
      }
    }, CONSTANTS.DELAY_CALL_API);
  }
  onSelectAutocomplete(name: any, item: any): void {
    if (name === "address_sub_district_name") {
      // ตำบล
      this.item.address_sub_district_code = item.code;
      this.item.address_sub_district_name = item.name;
      // อำเภอ
      this.item.address_district_code = item.district_code;
      this.item.address_district_name = item.district_name;
      // จังหวัด
      this.item.address_province_code = item.province_code;
      this.item.address_province_name = item.province_name;
      // รหัสไปรษณีย์
      this.item.postal_code = item.postal_code;
    }
    this.onClickOutsideAutocomplete(name);
  }
  onClickOutsideAutocomplete(name: any): void {
    this.isShowAutocomplete[name] = false;
    this.autocompleteList[name] = [];
  }
  clearValue(name: any): void {
    if (name === "address_sub_district_name") {
      // อำเภอ
      this.item.address_district_code = "";
      this.item.address_district_name = "";
      // จังหวัด
      this.item.address_province_code = "";
      this.item.address_province_name = "";
      // รหัสไปรษณีย์
      this.item.postal_code = "";
    }
  }
}
