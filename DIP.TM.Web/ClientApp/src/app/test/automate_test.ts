import { Injectable } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { SaveProcessService } from '../services/save-process-buffer.service'
import { RequestProcessService } from '../services/request-process-buffer.service'

import {
    CONSTANTS,
    getMoment,
    validateService,
    displayMoney,
    displayString,
    clone
} from '../helpers'


@Injectable({
    providedIn: 'root'
})
export class AutomateTest {
    constructor(
        private route: ActivatedRoute,
        private SaveProcessService: SaveProcessService,
        private RequestProcessService: RequestProcessService,
    ) { }

    public document_item_list_list_1: any = 1
    public document_item_list_list_1_Array: any[]
    public document_item_list_list_2: any = 1

    public post_round_instruction_rule_code: any
    public save_id: any
    public reference_number: any

    test(pThis: any, param: any = null): void {
        this.route.queryParams.subscribe((param_url: any) => {
            var test = param_url.test
            if (!test) return;

            console.log("AutomateTest: param", param);
            console.log("AutomateTest: param_url", param_url);

            if (window.location.href.indexOf("/registration-request") >= 0) {
                console.log("AutomateTest: registration-request");
                if (!param) {
                    console.log("AutomateTest: registration-request", "Do");
                    pThis.input.requester_name = "ผู้ยื่นคำขอ #" + test
                    pThis.input.item_count = "2"
                    pThis.input.telephone = "087541650" + test

                    // Add Item 1
                    pThis.onClickAddRequest()
                    pThis.onClickItemType(pThis.requestList[0])
                    pThis.subItemList[0].item_sub_type_1_code = "1"
                    pThis.subItemList[0].product_count = 5

                    pThis.onClickAddItemType()
                    pThis.subItemList[1].item_sub_type_1_code = "2"
                    pThis.subItemList[1].product_count = 6

                    pThis.binding('requestItem', 'sound_type_code', 'ANIMAL')
                    pThis.binding('requestItem', 'sound_description', 'คำบรรยายเสียง คำบรรยายเสียง ANIMAL #' + test)

                    pThis.onClickSaveItemType()

                    pThis.onClickAddPay(pThis.requestList[0])
                    pThis.requestList[0].size_over_cm = 5

                    // Add Item 2
                    pThis.onClickItemType(pThis.requestList[1])
                    pThis.subItemList[0].item_sub_type_1_code = "3"
                    pThis.subItemList[0].product_count = 7

                    pThis.onClickAddItemType()
                    pThis.subItemList[1].item_sub_type_1_code = "4"
                    pThis.subItemList[1].product_count = 8

                    pThis.onClickAddItemType()
                    pThis.subItemList[2].item_sub_type_1_code = "5"
                    pThis.subItemList[2].product_count = 9

                    pThis.binding('requestItem', 'sound_type_code', 'NATURE')
                    pThis.binding('requestItem', 'sound_description', 'คำบรรยายเสียง คำบรรยายเสียง ANIMAL #' + test)

                    pThis.onClickSaveItemType()

                    pThis.updateRequestList(false)

                    pThis.onSelectColumnInTable('requestList')

                    pThis.onClickSaveRequest()
                } else if (param.reference_number) {
                    console.log("AutomateTest: registration-request", "Open receipt-daily");
                    window.open("/receipt-daily?test=" + test + "&reference_number=" + param.reference_number, '_blank')
                }
            } else if (window.location.href.indexOf("/receipt-daily") >= 0) {
                console.log("AutomateTest: receipt-daily");
                if (!param) {
                    console.log("AutomateTest: receipt-daily", "Load");
                    pThis.input.reference_number = param_url.reference_number
                    pThis.onClickLoadUnpaid()
                } else if (param.reference_number) {
                    console.log("AutomateTest: receipt-daily", "Save");
                    pThis.onClickSave()
                } else if (param_url.round) {
                    //console.log("AutomateTest: receipt-daily", "Getting request01_item_id from request_id");
                    //this.document_item_list_list_1++
                    console.log("AutomateTest: receipt-daily", "document-role02-print-cover/list");
                    window.open("/document-role02-print-cover/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                    ////window.open("/public-role02-document-post/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')

                } else if (param.request_id) {
                    console.log("AutomateTest: receipt-daily", "Getting request01_item_id from request_id");
                    this.SaveProcessService.Save01List({ request_id: param.request_id }).subscribe((data: any) => {
                        console.log("AutomateTest: receipt-daily", "Open save-01-process");
                        window.open("/save-01-process/edit/" + data[0].id + "?test=" + test, '_blank')
                    })
                }
            } else if (window.location.href.indexOf("/save-01-process") >= 0) {
                console.log("AutomateTest: save-01-process");
                if (!param) {
                    console.log("AutomateTest: save-01-process", "Do");

                    // Add People
                    pThis.onClickSave01PeopleAdd()
                    pThis.onClickAddressEdit(pThis.listPeople[0])
                    pThis.modalAddress.name = "ชื่อเจ้าของ 1 #" + test
                    pThis.modalAddress.house_number = "ที่อยู่เจ้าของ 1 #" + test
                    pThis.modalAddress.card_type_code = "NAT_ID"
                    pThis.modalAddress.address_country_code = "TH"
                    pThis.modalAddress.receiver_type_code = "PEOPLE_FOREIGNER"
                    pThis.modalAddress.sex_code = "FEMALE"
                    pThis.modalAddress.nationality_code = "EE"
                    pThis.modalAddress.career_code = "อุตสาหกรรม"
                    pThis.modalAddress.telephone = "12345689 1 #" + test
                    pThis.modalAddress.email = "aaa@gmail.com 1 #" + test
                    pThis.modalAddress.card_number = "card_number 1 #" + test
                    pThis.modalAddress.fax = "fax #1" + test
                    pThis.onClickAddressSave();

                    pThis.onClickSave01PeopleAdd()
                    pThis.onClickAddressEdit(pThis.listPeople[1])
                    pThis.modalAddress.name = "ชื่อเจ้าของ 2 #" + test
                    pThis.modalAddress.house_number = "ที่อยู่เจ้าของ 2 #" + test
                    pThis.modalAddress.card_type_code = "PASS_ID"
                    pThis.modalAddress.address_country_code = "EN"
                    pThis.modalAddress.receiver_type_code = "CORPORATE"
                    pThis.modalAddress.sex_code = "FEMALE"
                    pThis.modalAddress.nationality_code = "BD"
                    pThis.modalAddress.career_code = "นักวิชาการอิสระ"
                    pThis.modalAddress.telephone = "12345689 2 #" + test
                    pThis.modalAddress.email = "aaa@gmail.com 2 #" + test
                    pThis.modalAddress.card_number = "card_number 2 #" + test
                    pThis.modalAddress.fax = "fax #2" + test
                    pThis.onClickAddressSave();

                    pThis.onClickSave01RepresentativeAdd()
                    pThis.onClickAddressEdit(pThis.listRepresentative[0])
                    pThis.modalAddress.name = "ชื่อตัวแทน 3 #" + test
                    pThis.modalAddress.house_number = "ที่อยู่ตัวแทน 3 #" + test
                    pThis.modalAddress.card_type_code = "PASS_ID"
                    pThis.modalAddress.address_country_code = "054"
                    pThis.modalAddress.receiver_type_code = "CORPORATE"
                    pThis.modalAddress.sex_code = "FEMALE"
                    pThis.modalAddress.nationality_code = "BM"
                    pThis.modalAddress.career_code = "ที่ปรึกษากฎหมาย"
                    pThis.modalAddress.telephone = "12345689 3 #" + test
                    pThis.modalAddress.email = "aaa@gmail.com 3 #" + test
                    pThis.modalAddress.card_number = "card_number 3 #" + test
                    pThis.modalAddress.fax = "fax #3" + test
                    pThis.onClickAddressSave();

                    pThis.onClickSave01RepresentativeAdd()
                    pThis.onClickAddressEdit(pThis.listRepresentative[1])
                    pThis.modalAddress.name = "ชื่อตัวแทน 4 #" + test
                    pThis.modalAddress.house_number = "ที่อยู่ตัวแทน 4 #" + test
                    pThis.modalAddress.card_type_code = "PASS_ID"
                    pThis.modalAddress.address_country_code = "174"
                    pThis.modalAddress.receiver_type_code = "CORPORATE"
                    pThis.modalAddress.sex_code = "FEMALE"
                    pThis.modalAddress.nationality_code = "CF"
                    pThis.modalAddress.career_code = "ผลิตและค้าขาย"
                    pThis.modalAddress.telephone = "12345689 4 #" + test
                    pThis.modalAddress.email = "aaa@gmail.com 4 #" + test
                    pThis.modalAddress.card_number = "card_number 4 #" + test
                    pThis.modalAddress.fax = "fax #4" + test
                    pThis.onClickAddressSave();

                    // Add Product 1
                    pThis.onClickSave01ProductAdd()
                    pThis.listProduct[0].request_item_sub_type_1_code = "1"
                    pThis.listProduct[0].request_item_sub_type_2_code = "2"
                    pThis.listProduct[0].description = "รายการสินค้า/บริการ 1 #" + test
                    // Add Product 2
                    pThis.onClickSave01ProductAdd()
                    pThis.listProduct[1].request_item_sub_type_1_code = "3"
                    pThis.listProduct[1].request_item_sub_type_2_code = "4"
                    pThis.listProduct[1].description = "รายการสินค้า/บริการ 2 #" + test

                    pThis.request_mark_feature_code_list = pThis.request_mark_feature_code_list || {}
                    pThis.master.requestMarkFeatureCodeList.forEach((item: any) => {
                        pThis.request_mark_feature_code_list[item.code] = true
                    })
                    //console.log(pThis.master.requestMarkFeatureCodeList)
                    //console.log(pThis.request_mark_feature_code_list)

                    // Add Sound Type
                    //pThis.listSoundMark = pThis.listSoundMark || [{}]
                    //console.log(pThis.listSoundMark)
                    pThis.onClickSave01SoundMarkAdd()
                    pThis.listSoundMark[0].request_sound_type = "ANIMAL"
                    pThis.onClickSave01SoundMarkAdd()
                    pThis.listSoundMark[1].request_sound_type = "NATURE"

                    pThis.onClickSave010Case28Add()
                    pThis.listSave010Case28[0].request_item_sub_type_1_code = "1"
                    pThis.listSave010Case28[0].case_28_date = getMoment()
                    pThis.listSave010Case28[0].address_country_code = "EN"


                    pThis.onClickSave01Send()
                } else if (param.request_number) {
                    console.log("AutomateTest: receipt-daily", "Open document-item/list");
                    window.open("/document-item/list?test=" + test + "&request_number=" + param.request_number, '_blank')
                }

            } else if (window.location.href.indexOf("/save-06-process") >= 0) {
                console.log("AutomateTest: save-06-process");
                if (!param) {
                    console.log("AutomateTest: save-06-process", "Do");

                    // Add People
                    pThis.onClickSave06PeopleAdd()
                    pThis.onClickAddressEdit(pThis.listPeople[0])
                    pThis.modalAddress.name = "เปลี่ยนจ้าของ 1 #" + test
                    pThis.modalAddress.house_number = "ที่อยู่เจ้าของ 1 #" + test
                    pThis.onClickAddressSave();

                    pThis.onClickSave06RepresentativeAdd()
                    pThis.onClickAddressEdit(pThis.listRepresentative[0])
                    pThis.modalAddress.name = "เปลี่ยนตัวแทน 1 #" + test
                    pThis.modalAddress.house_number = "ที่อยู่ตัวแทน 1 #" + test
                    pThis.onClickAddressSave();

                    // Add Product 1
                    pThis.onClickSave06ProductAdd()
                    pThis.listProduct[0].request_item_sub_type_1_code = "1"
                    //pThis.listProduct[0].request_item_sub_type_2_code = "2"
                    pThis.listProduct[0].description = "รายการสินค้า/บริการ 1 #" + test
                    // Add Product 2
                    pThis.onClickSave06ProductAdd()
                    pThis.listProduct[1].request_item_sub_type_1_code = "3"
                    //pThis.listProduct[1].request_item_sub_type_2_code = "4"
                    pThis.listProduct[1].description = "รายการสินค้า/บริการ 2 #" + test

                    pThis.onClickSave06Send()

                    //    console.log("AutomateTest: receipt-daily", "Do");
                    //    pThis.input.reference_number = param_url.reference_number
                    //    pThis.onClickLoadUnpaid()
                    //} else if (param.reference_number) {
                    //    console.log("AutomateTest: receipt-daily", "Save");
                    //    pThis.onClickSave()
                    //} else if (param.request_id) {
                    //    console.log("AutomateTest: receipt-daily", "Getting request06_item_id from request_id");
                    //    this.SaveProcessService.Save06List({ request_id: param.request_id }).subscribe((data: any) => {
                    //        console.log("AutomateTest: receipt-daily", "Open save-06-process");
                    //        window.open("/save-06-process/edit/" + data[0].id + "?test=" + test, '_blank')
                    //    })
                    //} else if (param.request_number) {
                    //  console.log("AutomateTest: receipt-daily", "Open document-item/list");
                    //  window.open("/document-item/list?test=" + test + "&request_number=" + param.request_number, '_blank')
                }

            } else if (window.location.href.indexOf("/document-item/list") >= 0) {
                console.log("AutomateTest: document-item/list");
                if (!param) {
                    console.log("AutomateTest: document-item/list", "List Item");
                    pThis.onClickDocumentItemList()
                } else if (param.list && this.document_item_list_list_1++ == 1) {
                    console.log("AutomateTest: document-item/list", "List Receiver");
                    pThis.input.document_people_name_receive = "ตรวจ"
                    pThis.autocompleteChangeDocumentPeopleList(pThis.input, 'document_people_name_receive')
                } else if (param.receiver) {
                    console.log("AutomateTest: document-item/list", "Receive Item");
                    pThis.listDocument.filter(item => item.request_number == param_url.request_number)[0].is_check = true
                    pThis.onClickDocumentItemReceive()
                } else if (param.DocumentItemReceive) {
                    console.log("AutomateTest: receipt-daily", "Open document-process-classification");
                    window.open("/document-process-classification?test=" + test + "&request_number=" + param_url.request_number, '_blank')
                }
            } else if (window.location.href.indexOf("/document-process-classification?") >= 0) {
                console.log("AutomateTest: document-process-classification");
                if (!param) {
                    console.log("AutomateTest: document-process-classification", "List Item");
                    pThis.onClickClassificationList()
                } else if (param.list) {
                    console.log("AutomateTest: document-process-classification", "Open document-process-classification");
                    window.open("/document-process-classification-do/edit/" + pThis.listClassification.filter(item => item.request_number == param_url.request_number)[0].id + "?test=" + test, '_blank')
                }
            } else if (window.location.href.indexOf("/document-process-classification-do/") >= 0) {
                console.log("AutomateTest: document-process-classification-do");
                if (!param) {
                    console.log("AutomateTest: document-process-classification-do", "Add Word");
                    pThis.documentClassificationWordList.push({})
                    pThis.documentClassificationWord.word_mark = "aaa #" + test
                    pThis.documentClassificationWord.word_first_code = "a" + test
                    pThis.documentClassificationWord.word_sound_last_code = "b" + test
                    pThis.documentClassificationWord.word_sound_last_other_code = "c" + test
                    pThis.documentClassificationWord.word_sound = "d" + test
                    pThis.documentClassificationWord.word_syllable_sound = "e" + test

                    pThis.documentClassificationWordList.push({})
                    pThis.onClickDocumentClassificationWordMenu(1)
                    pThis.documentClassificationWord.word_mark = "bbb #" + test
                    pThis.documentClassificationWord.word_first_code = "f"
                    pThis.documentClassificationWord.word_sound_last_code = "g"
                    pThis.documentClassificationWord.word_sound_last_other_code = "h"
                    pThis.documentClassificationWord.word_sound = "i"
                    pThis.documentClassificationWord.word_syllable_sound = "j"

                    console.log("AutomateTest: document-process-classification-do", "Add Image");
                    pThis.documentClassificationImageList.splice(pThis.documentClassificationImageList.length, 0, {})
                    pThis.documentClassificationImageList[0].document_classification_image_code = "1.1." + test
                    pThis.documentClassificationImageList.splice(pThis.documentClassificationImageList.length, 0, {})
                    pThis.documentClassificationImageList[1].document_classification_image_code = "1.2." + test

                    console.log("AutomateTest: document-process-classification-do", "Add Sound");
                    pThis.documentClassificationSoundList.splice(pThis.documentClassificationSoundList.length, 0, {})
                    pThis.documentClassificationSoundList[0].document_classification_sound_code = "2"
                    pThis.documentClassificationSoundList.splice(pThis.documentClassificationSoundList.length, 0, {})
                    pThis.documentClassificationSoundList[1].document_classification_sound_code = "3"

                    pThis.onClickClassificationSend()
                } else if (param.Send) {
                    //console.log(pThis.input.request_number)
                    console.log("AutomateTest: document-process-classification-do", "Open document-process-classification");
                    window.open("/checking-item/list?test=" + test + "&request_number=" + pThis.input.request_number, '_blank')
                }
            } else if (window.location.href.indexOf("/checking-item/list") >= 0) {
                console.log("AutomateTest: checking-item/list");
                if (!param) {
                    console.log("AutomateTest: checking-item/list", "List Item");
                    this.document_item_list_list_1 = 1
                    pThis.onClickCheckingItemList()
                } else if (param.list && this.document_item_list_list_1++ == 1) {
                    console.log("AutomateTest: checking-item/list", "List Receiver");
                    pThis.input.checking_receiver_by_name = "ตรวจ"
                    pThis.autocompleteChangeCheckerReceiveList(pThis.input, 'checking_receiver_by_name')
                } else if (param.receiver) {
                    console.log("AutomateTest: checking-item/list", "Receive Item");
                    console.log(pThis.listChecking)
                    pThis.listChecking.filter(item => item.request_number == param_url.request_number)[0].is_check = true
                    pThis.onClickCheckingItemReceive()
                } else if (param.CheckingItemReceive) {
                    if (!param_url["considering"]) {
                        console.log("AutomateTest: checking-item/list", "Open checking-similar/list");
                        window.open("/checking-similar/list?test=" + test + "&request_number=" + param_url.request_number, '_blank')
                    } else {
                        console.log("AutomateTest: checking-item/list", "Open checking-similar/list");
                        window.open("/considering-similar/list?test=" + test + "&request_number=" + param_url.request_number, '_blank')
                    }
                }
            } else if (window.location.href.indexOf("/checking-similar/list") >= 0) {
                console.log("AutomateTest: checking-similar/list");
                if (!param) {
                    console.log("AutomateTest: checking-similar/list", "List Item");
                    pThis.onClickCheckingSimilarList()
                } else if (param.list) {
                    console.log("AutomateTest: checking-similar/list", "Open document-process-classification");
                    console.log(pThis.listCheckingSimilar)
                    console.log(param_url.request_number)
                    window.open("/checking-similar/edit/" + pThis.listCheckingSimilar.filter(item => item.request_number == param_url.request_number)[0].id + "?test=" + test, '_blank')
                }
            } else if (window.location.href.indexOf("/checking-similar/edit") >= 0) {
                console.log("AutomateTest: checking-similar/edit");
                if (!param) {
                    console.log("AutomateTest: checking-similar/edit", "Word Translate");
                    pThis.onClickCheckingSimilarWordSoundTranslateAdd()
                    pThis.listWordSoundTranslate[0].word_translate_search = pThis.input.full_view.document_classification_word_list[0].word_mark
                    pThis.listWordSoundTranslate[0].word_translate_dictionary = "longdo"
                    pThis.listWordSoundTranslate[0].word_translate_sound = "คำอ่าน 1 #" + test
                    pThis.listWordSoundTranslate[0].word_translate_translate = "คำแปล 1 #" + test
                    pThis.listWordSoundTranslate[0].checking_word_translate_status_code = "TYPE2"

                    pThis.onClickCheckingSimilarWordSoundTranslateAdd()
                    pThis.listWordSoundTranslate[1].word_translate_search = pThis.input.full_view.document_classification_word_list[1].word_mark
                    pThis.listWordSoundTranslate[1].word_translate_dictionary = "google"
                    pThis.listWordSoundTranslate[1].word_translate_sound = "คำอ่าน 2 #" + test
                    pThis.listWordSoundTranslate[1].word_translate_translate = "คำแปล 2 #" + test
                    pThis.listWordSoundTranslate[1].checking_word_translate_status_code = "TYPE3"

                    pThis.onClickCheckingSimilarWordSoundTranslateSave()
                } else if (param.WordSoundTranslateSave) {
                    console.log("AutomateTest: checking-similar/edit", "Open checking-similar-word/edit/");
                    window.open("/checking-similar-word/edit/" + pThis.input.id + "?test=" + test, '_blank')
                }
            } else if (window.location.href.indexOf("/checking-similar-word/edit") >= 0) {
                console.log("AutomateTest: checking-similar-word/edit");
                if (!param) {
                    console.log("AutomateTest: checking-similar-word/edit", "Trademark Add");
                    pThis.oneWayDataCheckboxAllBinding(pThis.listTrademark, true)
                    this.document_item_list_list_1 = 1
                } else if (param.TrademarkAdd && this.document_item_list_list_1++ == 1) {
                    console.log("AutomateTest: checking-similar-word/edit", "Open checking-similar-result/edit/");
                    window.open("/checking-similar-result/edit/" + pThis.input.id + "?test=" + test, '_blank')
                    this.document_item_list_list_2 = 1;
                }
            } else if (window.location.href.indexOf("/checking-similar-result/edit") >= 0) {
                console.log("AutomateTest: checking-similar-result/edit");
                if (!param) {
                    if (this.document_item_list_list_2++ == 1) {
                        console.log("AutomateTest: checking-similar-word/edit", "Owner Same");
                        pThis.listTrademark.forEach((item: any, index: any) => {
                            if (index % 2 == 1) pThis.oneWayDataBinding('is_check', true, item)
                        })
                        pThis.onClickCheckingSimilarResultOwnerSame()
                    } else {
                        console.log("AutomateTest: checking-similar-word/edit", "Owner Same");
                        pThis.onClickCheckingSimilarResultSend()
                    }
                    //  //  console.log("AutomateTest: checking-similar-word/edit", "Open checking-similar-result/edit/");
                    //  //  window.open("/checking-similar-result/result/" + pThis.input.id + "?test=" + test, '_blank')
                } else if (param.OwnerSame) {
                    console.log("AutomateTest: checking-similar-word/edit", "Open checking-item/list");
                    window.open("/checking-item/list?test=" + test + "&considering=1&request_number=" + pThis.input.request_number, '_blank')
                }
            } else if (window.location.href.indexOf("/considering-similar/list") >= 0) {
                console.log("AutomateTest: considering-similar/list");
                if (!param) {
                    console.log("AutomateTest: considering-similar/list", "List Item");
                    pThis.onClickConsideringSimilarList()
                } else if (param.List) {
                    console.log("AutomateTest: considering-similar/list", "List");
                    window.open("/considering-similar/edit/" + param.List.list.filter(r => r.request_number == param_url.request_number)[0].id + "?test=" + test, '_blank')
                }
            } else if (window.location.href.indexOf("/considering-similar/edit") >= 0) {
                console.log("AutomateTest: considering-similar/edit");
                if (!param) {
                    console.log("AutomateTest: considering-similar/edit", "Select Item");
                    var status = {}
                    pThis.listTrademark.forEach((item: any) => {
                        if (!status[item.trademark_status_code] || status[item.trademark_status_code] < 2) {
                            status[item.trademark_status_code] = status[item.trademark_status_code] || 0;
                            status[item.trademark_status_code]++
                            item.is_same_approve = true;
                        }
                    })

                    pThis.onClickConsideringSimilarSave()
                } else if (param.Saved) {
                    console.log("AutomateTest: checking-similar-word/edit", "Open considering-similar-instruction/edit");
                    window.open("/considering-similar-instruction/edit/" + pThis.editID + "?test=" + test, '_blank')
                }
            } else if (window.location.href.indexOf("/considering-similar-instruction/edit") >= 0) {
                console.log("AutomateTest: considering-similar-instruction/edit");
                if (!param) {
                    this.document_item_list_list_1 = 0
                    this.document_item_list_list_1_Array = [7, 8, "13_1"]

                    this.test(pThis, { Add: 1 })
                } else if (param.Add) {
                    if (this.document_item_list_list_1_Array.length > this.document_item_list_list_1) {
                        pThis.onClickConsideringSimilarInstructionAdd()
                        pThis.InstructionEdit.instruction_rule_code = "ROLE_" + this.document_item_list_list_1_Array[this.document_item_list_list_1]
                        pThis.ItemUpdate()
                        for (var i = 1; i <= 5; i++) {
                            pThis.InstructionEdit["value_" + i] = "รายละเอียด value_" + i + " Role " + this.document_item_list_list_1_Array[this.document_item_list_list_1] + " #" + test
                        }

                        if (pThis.consideringSimilarInstructionRuleItemCodeList.length > 0) {
                            pThis.InstructionEdit.item_list[0].instruction_rule_item_code = pThis.consideringSimilarInstructionRuleItemCodeList[0].code
                            pThis.InstructionEdit.item_list[0].instruction_rule_description = "รายละเอียด Role " + this.document_item_list_list_1_Array[this.document_item_list_list_1] + " #" + test
                        }

                        pThis.onClickConsideringSimilarInstructionRuleSave()
                        this.document_item_list_list_1++;
                    } else {
                        pThis.onClickConsideringSimilarInstructionPublicAdd()
                    }
                } else if (param.Public) {
                    pThis.onClickConsideringSimilarInstructionSend()
                } else if (param.Send) {
                    console.log("AutomateTest: checking-similar-word/edit", "Open considering-similar-document/edit");
                    window.open("/considering-similar-document/edit/" + pThis.editID + "?test=" + test, '_blank')
                }
            } else if (window.location.href.indexOf("/considering-similar-document/edit") >= 0) {
                console.log("AutomateTest: considering-similar-document/edit", "Open public-role01-item/list");
                window.open("/public-role01-item/list?test=" + test + "&save_id=" + pThis.editID, '_blank')
            } else if (window.location.href.indexOf("/public-role01-item/list") >= 0) {
                console.log("AutomateTest: public-role01-item/list");
                if (!param) {
                    console.log("AutomateTest: public-role01-item/list", "List");
                    pThis.onClickPublicRole01ItemList()
                } else if (param.list) {
                    param.list.forEach((item: any) => {
                        if (item.id == param_url.save_id) {
                            pThis.PublicProcessService.PublicRole01ItemAutoSplit().subscribe((data: any) => {
                                console.log("AutomateTest: public-role01-item/list", "Open public-role01-check/list");
                                window.open("/public-role01-check/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                            })
                        }
                    })
                }
            } else if (window.location.href.indexOf("/public-role01-check/list") >= 0) {
                console.log("AutomateTest: public-role01-check/list");
                if (!param) {
                    console.log("AutomateTest: public-role01-check/list", "List");
                    pThis.onClickPublicRole01CheckList()
                } else if (param.list) {
                    param.list.forEach((item: any) => {
                        if (item.id == param_url.save_id) {
                            pThis.PublicProcessService.PublicRole01ItemAutoSplit().subscribe((data: any) => {
                                console.log("AutomateTest: public-role01-item/list", "Open public-role01-check/list");
                                window.open("/public-role01-check-do/edit/" + param_url.save_id + "?test=" + test, '_blank')
                            })
                        }
                    })
                }
            } else if (window.location.href.indexOf("/public-role01-check-do/edit") >= 0) {
                console.log("AutomateTest: public-role01-check-do/edit");
                if (!param) {
                    console.log("AutomateTest: public-role01-check-do/edit", "List");
                    pThis.onClickPublicRole01CheckSend()
                } else if (param.Send) {
                    console.log("AutomateTest: public-role01-check-do/edit", "Open public-role01-prepare/list");
                    window.open("/public-role01-prepare/list?test=" + test + "&save_id=" + pThis.editID, '_blank')
                }
            } else if (window.location.href.indexOf("/public-role01-prepare/list") >= 0) {
                console.log("AutomateTest: public-role01-prepare/list");
                if (!param) {
                    console.log("AutomateTest: public-role01-prepare/list", "List");
                    pThis.onClickPublicRole01PrepareList()
                } else if (param.list) {
                    pThis.PublicProcessService.PublicRoundAdd().subscribe((data: any) => {
                        console.log("AutomateTest: public-role01-prepare/list", "Open public-role01-doing/list");
                        window.open("/public-role01-doing/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                    })
                }
            } else if (window.location.href.indexOf("/public-role01-doing/list") >= 0) {
                console.log("AutomateTest: public-role01-doing/list");
                if (!param) {
                    console.log("AutomateTest: public-role01-doing/list", "List");
                    pThis.onClickPublicRole01DoingList()
                } else if (param.list) {
                    pThis.PublicProcessService.PublicRoundEnd().subscribe((data: any) => {
                        //console.log("AutomateTest: public-role01-doing/list", "Open public-role02-check/list");
                        //window.open("/public-role02-check/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                        console.log("AutomateTest: public-role01-doing/list", "Open public-role02-consider-payment/list");
                        window.open("/public-role02-consider-payment/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                    })
                }
                //} else if (window.location.href.indexOf("/public-role02-check/list") >= 0) {
                //  console.log("AutomateTest: public-role02-check/list");
                //  if (!param) {
                //    console.log("AutomateTest: public-role02-check/list", "List");
                //    this.document_item_list_list_1 = 1
                //    pThis.onClickPublicRole02CheckList()
                //  } else if (param.list && this.document_item_list_list_1 == 1) {
                //    this.document_item_list_list_1++
                //    console.log("AutomateTest: public-role02-check/list", "Check Auto");
                //    param.list.forEach((item: any) => {
                //      if (item.id == param_url.save_id) {
                //        item.is_check = true
                //      }
                //    })
                //    pThis.onClickPublicRole02CheckAutoDo();
                //    //  pThis.PublicProcessService.PublicRoundEnd().subscribe((data: any) => {
                //    //    console.log("AutomateTest: public-role01-doing/list", "Open public-role02-check/list");
                //    //    window.open("/public-role02-check/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                //    //  })
                //  } else if (param.list && this.document_item_list_list_1 == 2) {
                //    this.document_item_list_list_1++
                //    console.log("AutomateTest: public-role02-check/list", "Open public-role02-consider-payment/list");
                //    window.open("/public-role02-consider-payment/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                //  }
            } else if (window.location.href.indexOf("/public-role02-consider-payment/list") >= 0) {
                console.log("AutomateTest: public-role02-consider-payment/list");
                if (!param) {
                    console.log("AutomateTest: public-role02-consider-payment/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickPublicRole02ConsiderPaymentList()
                } else if (param.list && this.document_item_list_list_1 == 1) {

                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role02-consider-payment/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.save_id == param_url.save_id) {
                            item.is_check = true
                        }
                    })
                    pThis.onClickPublicRole02ConsiderPaymentSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role02-consider-payment/list", "Open public-role05-consider-payment/list");
                    window.open("/public-role05-consider-payment/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                }
            } else if (window.location.href.indexOf("/public-role05-consider-payment/list") >= 0) {
                console.log("AutomateTest: public-role05-consider-payment/list");
                if (!param) {
                    console.log("AutomateTest: public-role05-consider-payment/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickPublicRole05ConsiderPaymentList()
                } else if (param.list && this.document_item_list_list_1 == 1) {

                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role05-consider-payment/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.save_id == param_url.save_id) {
                            item.is_check = true
                        }
                    })
                    pThis.onClickPublicRole05ConsiderPaymentSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role02-consider-payment/list", "Open public-role02-document-payment/list");
                    window.open("/public-role02-document-payment/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                }
            } else if (window.location.href.indexOf("/public-role02-document-payment/list") >= 0) {
                console.log("AutomateTest: public-role02-document-payment/list");
                if (!param) {
                    console.log("AutomateTest: public-role02-document-payment/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.input.public_role02_document_payment_status_code = "WAIT_1"
                    pThis.onClickPublicRole02DocumentPaymentList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role02-document-payment/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.save_id == param_url.save_id) {
                            item.is_check = true
                            //console.log(item)
                            //console.log(item.save_id)
                            //console.log(item.reference_number)
                            //this.post_round_instruction_rule_code = item.post_round_instruction_rule_code
                            this.save_id = item.save_id
                            //this.reference_number = item.reference_number
                        }
                    })
                    this.post_round_instruction_rule_code = "RULE_5_3"
                    pThis.onClickPublicRole02DocumentPaymentSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    //this.document_item_list_list_1++
                    //param.list.forEach((item: any) => {
                    //  if (item.save_id == param_url.save_id) {
                    console.log(param.list)
                    console.log("AutomateTest: public-role02-document-payment/list", this.post_round_instruction_rule_code)
                    if (this.post_round_instruction_rule_code == "RULE_5_3") {
                        console.log("AutomateTest: public-role02-document-payment/list", "receipt-daily")
                        window.open("/receipt-daily?test=" + test + "&reference_number=" + this.reference_number + "&save_id=" + param_url.save_id + "&round=2", '_blank')
                    } else {
                        console.log("AutomateTest: public-role02-document-payment/list", "")
                    }
                    //  }
                    //})
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    //this.document_item_list_list_1++
                    //console.log("AutomateTest: public-role02-document-payment/list", "document-role02-print-cover/list");
                    //window.open("/document-role02-print-cover/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                    ////window.open("/public-role02-document-post/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                }
            } else if (window.location.href.indexOf("/document-role02-print-cover/list") >= 0) {
                console.log("AutomateTest: document-role02-print-cover/list");
                if (!param) {
                    console.log("AutomateTest: document-role02-print-cover/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickDocumentRole02PrintCoverList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: document-role02-print-cover/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.save_id == param_url.save_id) {
                            item.is_check = true
                        }
                    })
                    pThis.onClickDocumentRole02PrintCoverSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: document-role02-print-cover/list", "document-role02-print-list/list");
                    window.open("/document-role02-print-list/list?test=" + test + "&save_id=" + param_url.save_id + (param_url.rule9_1 ? "&rule9_1=1" : ""), '_blank')
                    //window.open("/public-role02-document-post/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                }
            } else if (window.location.href.indexOf("/document-role02-print-list/list") >= 0) {
                console.log("AutomateTest: document-role02-print-list/list");
                if (!param) {
                    console.log("AutomateTest: document-role02-print-list/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickDocumentRole02PrintListList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: document-role02-print-list/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.save_id == param_url.save_id) {
                            item.is_check = true
                            item.post_number = "post_number #" + item.object_id + " " + test
                        }
                    })
                    pThis.onClickDocumentRole02PrintListSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: document-role02-print-list/list", "public-role02-action-post/list");
                    window.open("/public-role02-action-post/list?test=" + test + "&save_id=" + param_url.save_id + (param_url.rule9_1 ? "&rule9_1=1" : ""), '_blank')
                }


                //} else if (window.location.href.indexOf("/document-role02-print-document/list") >= 0) {
                //    console.log("AutomateTest: document-role02-print-document/list");
                //    if (!param) {
                //        console.log("AutomateTest: document-role02-print-document/list", "List");
                //        this.document_item_list_list_1 = 1
                //        pThis.onClickDocumentRole02PrintDocumentList()
                //    } else if (param.list && this.document_item_list_list_1 == 1) {
                //        this.document_item_list_list_1++

                //        //if (!param_url.rule9_1) {
                //        //    console.log("AutomateTest: document-role02-print-document/list", "Check Send 1");
                //        //    param.list.forEach((item: any) => {
                //        //        if (item.object_id == param_url.save_id) {
                //        //            if (item.post_round_instruction_rule_code == "RULE_5_3") {
                //        //                item.is_check = true
                //        //                item.post_number = "post_number #" + item.object_id + " " + test
                //        //            }
                //        //        }
                //        //    })
                //        //} else {
                //        //    console.log("AutomateTest: public-role02-document-post/list", "Check Send 2");
                //        //    param.list.forEach((item: any) => {
                //        //        if (item.object_id == param_url.save_id) {
                //        //            if (item.post_round_instruction_rule_code == "RULE_9_1") {
                //        //                item.is_check = true
                //        //                item.post_number = "post_number #" + item.object_id + " " + test
                //        //            }
                //        //        }
                //        //    })
                //        //}
                //        pThis.onClickPublicRole02DocumentPostSend()
                //    }
                //} else if (window.location.href.indexOf("/public-role02-document-post/list") >= 0) {
                //  console.log("AutomateTest: public-role02-document-post/list");
                //  if (!param) {
                //    console.log("AutomateTest: public-role02-document-post/list", "List");
                //    this.document_item_list_list_1 = 1
                //    pThis.onClickPublicRole02DocumentPostList()
                //  } else if (param.list && this.document_item_list_list_1 == 1) {
                //    this.document_item_list_list_1++

                //    if (!param_url.rule9_1) {
                //      console.log("AutomateTest: public-role02-document-post/list", "Check Send 1");
                //      param.list.forEach((item: any) => {
                //        if (item.object_id == param_url.save_id) {
                //          if (item.post_round_instruction_rule_code == "RULE_5_3") {
                //            item.is_check = true
                //            item.post_number = "post_number #" + item.object_id + " " + test
                //          }
                //        }
                //      })
                //    } else {
                //      console.log("AutomateTest: public-role02-document-post/list", "Check Send 2");
                //      param.list.forEach((item: any) => {
                //        if (item.object_id == param_url.save_id) {
                //          if (item.post_round_instruction_rule_code == "RULE_9_1") {
                //            item.is_check = true
                //            item.post_number = "post_number #" + item.object_id + " " + test
                //          }
                //        }
                //      })
                //    }
                //    pThis.onClickPublicRole02DocumentPostSend()

                //  } else if (param.list && this.document_item_list_list_1 == 2) {
                //    this.document_item_list_list_1++
                //    console.log("AutomateTest: public-role02-document-post/list", "Open public-role02-action-post/list");
                //    window.open("/public-role02-action-post/list?test=" + test + (param_url.rule9_1 ? "&rule9_1=1" : "") + "&save_id=" + param_url.save_id, '_blank')
                //    //window.open("/document-role02-item/list?test=" + test + (param_url.rule9_1 ? "&rule9_1=1" : "") + "&save_id=" + param_url.save_id, '_blank')
                //  }
                //} else if (window.location.href.indexOf("/document-role02-item/list") >= 0) {
                //  console.log("AutomateTest: document-role02-item/list");
                //  if (!param) {
                //    console.log("AutomateTest: document-role02-item/list", "List");
                //    this.document_item_list_list_1 = 1
                //    pThis.onClickDocumentRole02itemList()
                //    //} else if (param.list && this.document_item_list_list_1 == 1) {
                //    //  this.document_item_list_list_1++
                //    //  console.log("AutomateTest: public-role04-item/list", "Auto Split");
                //    //  pThis.onClickPublicRole04AutoSplit()
                //    //} else if (param.list && this.document_item_list_list_1 == 2) {
                //    //  this.document_item_list_list_1++
                //    //  console.log("AutomateTest: public-role04-item/list", "Open public-role04-check/list");
                //    //  window.open("/public-role04-check/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                //  }
            } else if (window.location.href.indexOf("/public-role02-action-post/list") >= 0) {
                console.log("AutomateTest: public-role02-action-post/list");
                if (!param) {
                    console.log("AutomateTest: public-role02-action-post/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickPublicRole02ActionPostList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role02-action-post/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.object_id == param_url.save_id) {
                            if (!param_url.rule9_1) {
                                if (item.post_round_instruction_rule_code == "RULE_5_3") {
                                    item.is_check = true
                                    item.post_round_action_post_type_code = "OK"
                                    item.book_acknowledge_date = getMoment()
                                    item.post_round_remark = "post_round_remark #" + item.object_id + " " + test
                                }
                            } else {
                                if (item.post_round_instruction_rule_code == "RULE_9_1") {
                                    item.is_check = true
                                    item.post_round_action_post_type_code = "OK"
                                    item.book_acknowledge_date = getMoment()
                                    item.post_round_remark = "post_round_remark #" + item.object_id + " " + test
                                }

                            }
                        }
                    })
                    pThis.onClickPublicRole02ActionPostSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    if (!param_url.rule9_1) {
                        console.log("AutomateTest: public-role02-action-post/list", "Open public-role04-item/list");
                        window.open("/public-role04-item/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                    } else {
                        //console.log("AutomateTest: public-role02-action-post/list", "Open document-role02-item/list");
                        //window.open("/document-role02-item/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                    }
                }
            } else if (window.location.href.indexOf("/public-role04-item/list") >= 0) {
                console.log("AutomateTest: public-role04-item/list");
                if (!param) {
                    console.log("AutomateTest: public-role04-item/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickPublicRole04itemList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role04-item/list", "Auto Split");
                    pThis.onClickPublicRole04ItemAutoSplit()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role04-item/list", "Open public-role04-check/list");
                    window.open("/public-role04-check/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                }
            } else if (window.location.href.indexOf("/public-role04-check/list") >= 0) {
                console.log("AutomateTest: public-role04-check/list");
                if (!param) {
                    console.log("AutomateTest: public-role04-check/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickPublicRole04CheckList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role04-check/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.id == param_url.save_id) {
                            item.is_check = true
                        }
                    })
                    pThis.onClickPublicRole04CheckSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role04-check/list", "Open public-role05-document/list");
                    window.open("/public-role05-document/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                }
            } else if (window.location.href.indexOf("/public-role05-document/list") >= 0) {
                console.log("AutomateTest: public-role05-document/list");
                if (!param) {
                    console.log("AutomateTest: public-role05-document/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickPublicRole05DocumentList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role05-document/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.id == param_url.save_id) {
                            item.is_check = true
                        }
                    })
                    pThis.onClickPublicRole05DocumentSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role05-document/list", "Open public-role04-document/list");
                    window.open("/public-role04-document/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                }
            } else if (window.location.href.indexOf("/public-role04-document/list") >= 0) {
                console.log("AutomateTest: public-role04-document/list");
                if (!param) {
                    console.log("AutomateTest: public-role04-document/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickPublicRole04DocumentList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role04-document/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.id == param_url.save_id) {
                            item.is_check = true
                        }
                    })
                    pThis.onClickPublicRole04DocumentSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role04-document/list", "Open public-role02-document/list");
                    window.open("/public-role02-document/list?test=" + test + "&save_id=" + param_url.save_id, '_blank')
                }
            } else if (window.location.href.indexOf("/public-role02-document/list") >= 0) {
                console.log("AutomateTest: public-role02-document/list");
                if (!param) {
                    console.log("AutomateTest: public-role02-document/list", "List");
                    this.document_item_list_list_1 = 1
                    pThis.onClickPublicRole02DocumentList()
                } else if (param.list && this.document_item_list_list_1 == 1) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role02-document/list", "Check Send");
                    param.list.forEach((item: any) => {
                        if (item.id == param_url.save_id) {
                            item.is_check = true
                        }
                    })
                    pThis.onClickPublicRole02DocumentSend()
                } else if (param.list && this.document_item_list_list_1 == 2) {
                    this.document_item_list_list_1++
                    console.log("AutomateTest: public-role02-document/list", "Open document-role02-print-cover/list");
                    window.open("/document-role02-print-cover/list?test=" + test + "&rule9_1=1&save_id=" + param_url.save_id, '_blank')
                }
            }
        })
    }
}
