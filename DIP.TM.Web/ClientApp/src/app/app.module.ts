import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";

/* app */
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { NavComponent } from "./shared/nav/nav.component";

/* lib */
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from "ngx-toastr";

/* shared */
import { SharedModule } from "./shared/shared.module";

/* main layout */
import { MainLayoutComponent, MainLayoutEFormComponent } from "./pages";


@NgModule({
  declarations: [AppComponent, MainLayoutComponent, MainLayoutEFormComponent, NavComponent,],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgxSpinnerModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    SharedModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
