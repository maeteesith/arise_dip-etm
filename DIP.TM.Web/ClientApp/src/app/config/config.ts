//TODO >>> Set Server Here <<<
const SERVER = 'DEV'
// const SERVER = 'STAGING'
// const SERVER = 'PRODUCTION'

const BASE = {

  //? dev
  DEV: {
    BASE_API : "",
    BASE_PATH_API : "/api"
  },

  //? staging
  STAGING: {
    BASE_API: "",
    BASE_PATH_API: ""
  },

  //? production
  PRODUCTION: {
    BASE_API: "",
    BASE_PATH_API: ""
  }
  
}

export const BASE_API = BASE[SERVER].BASE_API
export const BASE_PATH_API = BASE[SERVER].BASE_PATH_API