import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  PublicRole02ConsiderPaymentListComponent,
  PublicRole02ConsiderPaymentComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole02ConsiderPaymentComponent
  },
  {
    path: "add",
    component: PublicRole02ConsiderPaymentComponent
  },
  {
    path: "edit/:id",
    component: PublicRole02ConsiderPaymentComponent
  },
  {
    path: "list",
    component: PublicRole02ConsiderPaymentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole02ConsiderPaymentRoutingModule { }
