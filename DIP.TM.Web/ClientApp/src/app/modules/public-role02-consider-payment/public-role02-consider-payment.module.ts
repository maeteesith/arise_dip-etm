import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02ConsiderPaymentRoutingModule } from "./public-role02-consider-payment-routing.module";
import {
  PublicRole02ConsiderPaymentListComponent,
  PublicRole02ConsiderPaymentComponent
} from "../../pages";

@NgModule({
  declarations: [
    PublicRole02ConsiderPaymentListComponent, 
    PublicRole02ConsiderPaymentComponent
  ],
  imports: [...IMPORTS, PublicRole02ConsiderPaymentRoutingModule],
  providers: PROVIDERS
})
export class PublicRole02ConsiderPaymentModule { }
