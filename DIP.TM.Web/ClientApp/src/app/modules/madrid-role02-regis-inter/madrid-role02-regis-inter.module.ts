import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02RegisInterRoutingModule } from "./madrid-role02-regis-inter-routing.module";
import { MadridRole02RegisInterListComponent, MadridRole02RegisInterComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole02RegisInterListComponent,MadridRole02RegisInterComponent],
  imports: [...IMPORTS, MadridRole02RegisInterRoutingModule],
  providers: PROVIDERS
})
export class MadridRole02RegisInterModule {}
