import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02RegisInterListComponent,MadridRole02RegisInterComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02RegisInterComponent,
  },{
    path: "add",
    component: MadridRole02RegisInterComponent
  },{
    path: "list",
    component: MadridRole02RegisInterListComponent
  }
  //{
  //  path: "list",
  //  component: MadridRole02WaitListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02RegisInterRoutingModule {}
