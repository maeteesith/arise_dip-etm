import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRoleMm02RoutingModule } from './madrid-role01-mm02-routing.module';
import { MadridRole01Mm02Component, MadridRole01AlertComponent } from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';


@NgModule({
  declarations: [MadridRole01Mm02Component, MadridRole01AlertComponent],
  imports: [...IMPORTS, MadridRoleMm02RoutingModule],
  entryComponents: [MadridRole01AlertComponent],
    providers: PROVIDERS
})
export class MadridRole01Mm02Module { }
