import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole01Mm02Component } from 'src/app/pages/madrid-role01-mm02/madrid-role01-mm02.component';


const routes: Routes = [
  {
    path:"",
    component: MadridRole01Mm02Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRoleMm02RoutingModule { }
