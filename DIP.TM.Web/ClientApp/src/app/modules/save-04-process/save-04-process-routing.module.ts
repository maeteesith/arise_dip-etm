import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save04ProcessComponent,
  Save04ProcessListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save04ProcessComponent
  },
  {
    path: "add",
    component: Save04ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save04ProcessComponent
  },
  {
    path: "list",
    component: Save04ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save04ProcessRoutingModule {}
