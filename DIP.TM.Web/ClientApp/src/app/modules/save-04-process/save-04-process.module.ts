import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save04ProcessRoutingModule } from "./save-04-process-routing.module";
import {
  Save04ProcessComponent,
  Save04ProcessListComponent
} from "../../pages";

@NgModule({
  declarations: [Save04ProcessComponent, Save04ProcessListComponent],
  imports: [...IMPORTS, Save04ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save04ProcessModule {}
