import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentProcessCollectListComponent,
  //DocumentProcessCollectComponent
} from "../../pages";

const routes: Routes = [
  {
    //  path: "",
    //  component: DocumentProcessCollectComponent
    //},
    //{
    //  path: "add",
    //  component: DocumentProcessCollectComponent
    //},
    //{
    //  path: "edit/:id",
    //  component: DocumentProcessCollectComponent
    //},
    //{
    path: "list",
    component: DocumentProcessCollectListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentProcessCollectRoutingModule { }
