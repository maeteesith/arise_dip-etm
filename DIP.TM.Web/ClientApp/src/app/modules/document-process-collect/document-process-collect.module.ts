import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentProcessCollectRoutingModule } from "./document-process-collect-routing.module";
import {
  DocumentProcessCollectListComponent,
  //DocumentProcessCollectComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentProcessCollectListComponent,
    //DocumentProcessCollectComponent
  ],
  imports: [...IMPORTS, DocumentProcessCollectRoutingModule],
  providers: PROVIDERS
})
export class DocumentProcessCollectModule { }
