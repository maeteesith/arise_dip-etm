import { NgModule } from '@angular/core';
import { MadridRole06Ni06RoutingModule } from './madrid-role06-ni06-routing.module';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06Ni06ListComponent } from "../../pages";
import { ModalAttachFileArrayComponent } from 'src/app/pages/modal-attach-file-array/modal-attach-file-array.component';


@NgModule({
  declarations: [MadridRole06Ni06ListComponent, ModalAttachFileArrayComponent],
  imports: [...IMPORTS, MadridRole06Ni06RoutingModule],
  entryComponents: [ModalAttachFileArrayComponent],
  providers: PROVIDERS
})
export class MadridRole06Ni06Module { }
