import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06Ni06ListComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole06Ni06ListComponent
  },
  {
    path: "list",
    component: MadridRole06Ni06ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06Ni06RoutingModule { }
