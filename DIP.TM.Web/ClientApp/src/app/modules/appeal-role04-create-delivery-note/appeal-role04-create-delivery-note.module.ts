import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04CreateDeliveryNoteRoutingModule } from './appeal-role04-create-delivery-note-routing.module';
import { AppealRole04CreateDeliveryNoteListComponent } from '../../pages'

@NgModule({
  declarations: [AppealRole04CreateDeliveryNoteListComponent],
  imports: [
    ...IMPORTS,
    AppealRole04CreateDeliveryNoteRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04CreateDeliveryNoteModule { }
