import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole04CreateDeliveryNoteListComponent } from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole04CreateDeliveryNoteListComponent
  },
  {
    path: "list",
    component: AppealRole04CreateDeliveryNoteListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04CreateDeliveryNoteRoutingModule { }
