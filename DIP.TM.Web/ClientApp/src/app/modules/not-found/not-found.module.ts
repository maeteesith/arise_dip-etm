import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { NotFoundRoutingModule } from "./not-found-routing.module";
import { NotFoundComponent } from "../../pages";

@NgModule({
  declarations: [NotFoundComponent],
  imports: [...IMPORTS, NotFoundRoutingModule],
  providers: PROVIDERS
})
export class NotFoundModule {}
