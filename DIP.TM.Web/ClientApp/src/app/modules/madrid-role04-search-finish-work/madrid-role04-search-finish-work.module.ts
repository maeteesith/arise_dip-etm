import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole04SearchFinishWorkRoutingModule } from './madrid-role04-search-finish-work-routing.module';
import {MadridRole04SearchFinishWorkComponent} from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [
    MadridRole04SearchFinishWorkComponent
  ],
  imports: [...IMPORTS, MadridRole04SearchFinishWorkRoutingModule],
  providers: PROVIDERS
})
export class MadridRole04SearchFinishWorkModule { }
