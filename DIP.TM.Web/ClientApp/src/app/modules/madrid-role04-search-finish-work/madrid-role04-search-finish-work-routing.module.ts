import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MadridRole04SearchFinishWorkComponent} from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole04SearchFinishWorkComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04SearchFinishWorkRoutingModule { }
