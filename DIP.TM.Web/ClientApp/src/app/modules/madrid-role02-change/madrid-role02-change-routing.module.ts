import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02ChangeListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02ChangeListComponent
  },
  //{
  //  path: "list",
  //  component: MadridRole02ChangeListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02ChangeRoutingModule {}
