import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02ChangeRoutingModule } from "./madrid-role02-change-routing.module";
import { MadridRole02ChangeListComponent } from "../../pages";
import { MadridRole02RequestWaiverModalComponent } from "src/app/pages/madrid-role02-request-waiver-modal/madrid-role02-request-waiver-modal.component";
import { MadridRole02RequestEditK20ModalComponent } from "src/app/pages/madrid-role02-request-edit-k20-model/madrid-role02-request-edit-k20-model.component";

@NgModule({
  declarations: [MadridRole02ChangeListComponent,MadridRole02RequestWaiverModalComponent,MadridRole02RequestEditK20ModalComponent],
  imports: [...IMPORTS, MadridRole02ChangeRoutingModule],
  entryComponents:[MadridRole02RequestWaiverModalComponent,MadridRole02RequestEditK20ModalComponent],
  providers: PROVIDERS
})
export class MadridRole02ChangeModule {}
