import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04ChangeListComponent,
  //DocumentRole04ChangeComponent
} from "../../pages";

const routes: Routes = [
  {
    //  path: "",
    //  component: DocumentRole04ChangeComponent
    //},
    //{
    //  path: "add",
    //  component: DocumentRole04ChangeComponent
    //},
    //{
    //  path: "edit/:id",
    //  component: DocumentRole04ChangeComponent
    //},
    //{
    path: "list",
    component: DocumentRole04ChangeListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04ChangeRoutingModule { }
