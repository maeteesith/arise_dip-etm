import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04ChangeRoutingModule } from "./document-role04-change-routing.module";
import {
  DocumentRole04ChangeListComponent,
  //DocumentRole04ChangeComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04ChangeListComponent, 
    //DocumentRole04ChangeComponent
  ],
  imports: [...IMPORTS, DocumentRole04ChangeRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04ChangeModule { }
