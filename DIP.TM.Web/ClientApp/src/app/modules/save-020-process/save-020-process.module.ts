import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save020ProcessRoutingModule } from "./save-020-process-routing.module";
import {
  Save020ProcessListComponent,
  Save020ProcessComponent
} from "../../pages";

@NgModule({
  declarations: [Save020ProcessListComponent, Save020ProcessComponent],
  imports: [...IMPORTS, Save020ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save020ProcessModule {}
