import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save020ProcessListComponent,
  Save020ProcessComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save020ProcessComponent
  },
  {
    path: "add",
    component: Save020ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save020ProcessComponent
  },
  {
    path: "list",
    component: Save020ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save020ProcessRoutingModule {}
