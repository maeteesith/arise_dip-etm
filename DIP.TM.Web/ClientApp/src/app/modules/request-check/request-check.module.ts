import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestCheckRoutingModule } from "./request-check-routing.module";
import { RequestCheckComponent } from "../../pages";

@NgModule({
  declarations: [RequestCheckComponent],
  imports: [...IMPORTS, RequestCheckRoutingModule],
  providers: PROVIDERS
})
export class RequestCheckModule {}
