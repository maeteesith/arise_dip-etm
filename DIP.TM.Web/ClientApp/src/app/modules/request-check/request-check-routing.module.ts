import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RequestCheckComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RequestCheckComponent
  },
  {
    path: "add",
    component: RequestCheckComponent
  },
  {
    path: "edit/:id",
    component: RequestCheckComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestCheckRoutingModule {}
