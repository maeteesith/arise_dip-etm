import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole01KorOtherCommercialProvinceRoutingModule } from "./appeal-role01-kor-other-commercial-province-routing.module";
import { AppealRole01KorOtherCommercialProvinceComponent } from "../../pages";

import { AppealCommercialProvinceFilter } from '../../pages/appeal-role01-kor-other-commercial-province/appeal-role01-kor-other-commercial-province-filter.pipe';

@NgModule({
  declarations: [AppealRole01KorOtherCommercialProvinceComponent, AppealCommercialProvinceFilter],
  imports: [...IMPORTS, AppealRole01KorOtherCommercialProvinceRoutingModule],
  providers: PROVIDERS
})
export class AppealRole01KorOtherCommercialProvinceModule { }
