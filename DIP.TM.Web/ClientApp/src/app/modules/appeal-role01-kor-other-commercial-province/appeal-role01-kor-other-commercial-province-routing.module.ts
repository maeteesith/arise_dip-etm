import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { AppealRole01KorOtherCommercialProvinceComponent } from '../../pages'
const routes: Routes = [
  {
    path: "",
    component: AppealRole01KorOtherCommercialProvinceComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole01KorOtherCommercialProvinceRoutingModule { }
