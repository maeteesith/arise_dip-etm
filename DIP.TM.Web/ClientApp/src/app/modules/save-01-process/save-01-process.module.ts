import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save01ProcessRoutingModule } from "./save-01-process-routing.module";
import {
  Save01ProcessListComponent,
  Save01ProcessComponent
} from "../../pages";

@NgModule({
  declarations: [Save01ProcessListComponent, Save01ProcessComponent],
  imports: [...IMPORTS, Save01ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save01ProcessModule { }
