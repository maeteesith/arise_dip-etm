import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save01ProcessListComponent,
  Save01ProcessComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save01ProcessComponent
  },
  {
    path: "add",
    component: Save01ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save01ProcessComponent
  },
  {
    path: "list",
    component: Save01ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save01ProcessRoutingModule {}
