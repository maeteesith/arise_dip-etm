import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MadridRole04CheckComponent, MadridRole04MailComponent, MadridRole04PresentComponent, MadridRole04PaymentComponent } from "../../pages";

import { MadridRole04CheckRoutingModule } from './madrid-role04-check-routing.module';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';


@NgModule({
  declarations: [MadridRole04CheckComponent, MadridRole04MailComponent, MadridRole04PresentComponent, MadridRole04PaymentComponent],
  imports: [...IMPORTS, MadridRole04CheckRoutingModule],
  entryComponents: [MadridRole04MailComponent, MadridRole04PresentComponent, MadridRole04PaymentComponent],
  providers: PROVIDERS
})
export class MadridRole04CheckModule { }
