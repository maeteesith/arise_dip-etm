import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole04CheckComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole04CheckComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04CheckRoutingModule { }
