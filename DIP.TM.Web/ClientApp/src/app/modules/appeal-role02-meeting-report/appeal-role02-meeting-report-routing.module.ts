import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole02MeetingReportComponent,
  AppealRole02MeetingReportListComponent
} from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole02MeetingReportComponent
  },
  {
    path: "list",
    component: AppealRole02MeetingReportListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole02MeetingReportRoutingModule { }
