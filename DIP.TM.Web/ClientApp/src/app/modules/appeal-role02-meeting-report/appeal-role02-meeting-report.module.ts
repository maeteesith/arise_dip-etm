import { NgModule } from '@angular/core';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers'
import { AppealRole02MeetingReportRoutingModule } from './appeal-role02-meeting-report-routing.module';
import { 
  AppealRole02MeetingReportComponent,
  AppealRole02MeetingReportListComponent 
} from '../../pages';

@NgModule({
  declarations: [AppealRole02MeetingReportListComponent, AppealRole02MeetingReportComponent],
  imports: [
    ...IMPORTS,
    AppealRole02MeetingReportRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole02MeetingReportModule { }
