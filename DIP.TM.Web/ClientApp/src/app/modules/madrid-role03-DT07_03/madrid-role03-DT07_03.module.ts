import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole03DT07_03RoutingModule } from "./madrid-role03-DT07_03-routing.module";
import { MadridRole03DT07_03Component } from "../../pages";
import { FormsModule } from "@angular/forms";


@NgModule({
  declarations: [MadridRole03DT07_03Component,],
  imports: [...IMPORTS, MadridRole03DT07_03RoutingModule,FormsModule],
  providers: PROVIDERS
})
export class MadridRole03DT07_03Module {}
