import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole03DT07_03Component } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole03DT07_03Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole03DT07_03RoutingModule {}
