import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
    SaveSendListComponent,
    //SaveSendComponent
} from "../../pages";

const routes: Routes = [
    //{
    //  path: "",
    //  component: SaveSendComponent
    //},
    //{
    //  path: "add",
    //  component: SaveSendComponent
    //},
    //{
    //  path: "edit/:id",
    //  component: SaveSendComponent
    //},
    {
        path: "list",
        component: SaveSendListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SaveSendRoutingModule { }
