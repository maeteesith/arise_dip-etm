import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { SaveSendRoutingModule } from "./save-send-routing.module";
import {
  SaveSendListComponent,
  //SaveSendComponent
} from "../../pages";

@NgModule({
  declarations: [
    SaveSendListComponent, 
    //SaveSendComponent
  ],
  imports: [...IMPORTS, SaveSendRoutingModule],
  providers: PROVIDERS
})
export class SaveSendModule { }
