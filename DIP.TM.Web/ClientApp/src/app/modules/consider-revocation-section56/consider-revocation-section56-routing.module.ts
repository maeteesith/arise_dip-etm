import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  ConsiderRevocationSection56Component,
  ConsiderRevocationSection56ListComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: ConsiderRevocationSection56Component,
},
{
path: "add",
component: ConsiderRevocationSection56Component,
},
{
path: "edit/:id",
component: ConsiderRevocationSection56Component,
},
{
path: "list",
component: ConsiderRevocationSection56ListComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ConsiderRevocationSection56RoutingModule {}

