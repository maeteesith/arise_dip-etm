import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsiderRevocationSection56RoutingModule } from './consider-revocation-section56-routing.module';
import {
  ConsiderRevocationSection56Component,
  ConsiderRevocationSection56ListComponent
} from "../../pages";

@NgModule({
declarations: [
  ConsiderRevocationSection56Component,
  ConsiderRevocationSection56ListComponent
],
imports: [...IMPORTS, ConsiderRevocationSection56RoutingModule],
providers: PROVIDERS
})
export class ConsiderRevocationSection56Module { }

