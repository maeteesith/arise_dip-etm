import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Mm08RoutingModule } from './mm08-routing.module';
import { IMPORTS } from '../imports';
import { RouterModule } from '@angular/router';
import { PROVIDERS } from '../providers';
import { Mm08Component } from 'src/app/pages/mm08/mm08.component';



@NgModule({
    declarations: [Mm08Component],
    imports: [...IMPORTS, Mm08RoutingModule],
    entryComponents: [Mm08Component],
    providers: PROVIDERS
})
export class Mm08Module { }
