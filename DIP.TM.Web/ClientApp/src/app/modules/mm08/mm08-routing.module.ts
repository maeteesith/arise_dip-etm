import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArchwizardModule } from 'angular-archwizard';
import { Mm08Component } from 'src/app/pages/mm08/mm08.component';


const routes: Routes = [
  {
    path: "",
    component: Mm08Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), ArchwizardModule],
  exports: [RouterModule]
})
export class Mm08RoutingModule { }
