import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02DocumentPostRoutingModule } from "./public-role02-document-post-routing.module";
import { PublicRole02DocumentPostListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole02DocumentPostListComponent],
  imports: [...IMPORTS, PublicRole02DocumentPostRoutingModule],
  providers: PROVIDERS
})
export class PublicRole02DocumentPostModule {}
