import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole02DocumentPostListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole02DocumentPostListComponent
  },
  {
    path: "list",
    component: PublicRole02DocumentPostListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole02DocumentPostRoutingModule {}
