import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save05ProcessComponent,
  Save05ProcessListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save05ProcessComponent
  },
  {
    path: "add",
    component: Save05ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save05ProcessComponent
  },
  {
    path: "list",
    component: Save05ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save05ProcessRoutingModule {}
