import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save05ProcessRoutingModule } from "./save-05-process-routing.module";
import {
  Save05ProcessComponent,
  Save05ProcessListComponent
} from "../../pages";

@NgModule({
  declarations: [Save05ProcessComponent, Save05ProcessListComponent],
  imports: [...IMPORTS, Save05ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save05ProcessModule {}
