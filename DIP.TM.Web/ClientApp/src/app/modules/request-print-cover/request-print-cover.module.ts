import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestPrintCoverRoutingModule } from "./request-print-cover-routing.module";
import {
    //RequestPrintCoverListComponent,
    RequestPrintCoverComponent
} from "../../pages";

@NgModule({
    declarations: [
        //RequestPrintCoverListComponent, 
        RequestPrintCoverComponent
    ],
    imports: [...IMPORTS, RequestPrintCoverRoutingModule],
    providers: PROVIDERS
})
export class RequestPrintCoverModule { }
