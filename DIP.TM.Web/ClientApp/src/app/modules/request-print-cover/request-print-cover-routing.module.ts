import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //RequestPrintCoverListComponent,
  RequestPrintCoverComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RequestPrintCoverComponent
  },
  {
    path: "add",
    component: RequestPrintCoverComponent
  },
  {
    path: "edit/:id",
    component: RequestPrintCoverComponent
  //},
  //{
  // path: "list",
  // component: RequestPrintCoverListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestPrintCoverRoutingModule {}
