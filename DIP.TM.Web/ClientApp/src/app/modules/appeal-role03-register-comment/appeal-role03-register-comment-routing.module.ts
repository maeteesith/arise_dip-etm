import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole03RegisterCommentComponent } from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole03RegisterCommentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole03RegisterCommentRoutingModule { }
