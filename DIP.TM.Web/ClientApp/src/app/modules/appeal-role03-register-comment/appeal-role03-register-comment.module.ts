import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole03RegisterCommentRoutingModule } from './appeal-role03-register-comment-routing.module';
import { AppealRole03RegisterCommentComponent } from '../../pages';


@NgModule({
  declarations: [AppealRole03RegisterCommentComponent],
  imports: [
    ...IMPORTS,
    AppealRole03RegisterCommentRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole03RegisterCommentModule { }