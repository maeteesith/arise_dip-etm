import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { R2RequestKor20ChangeRoutingModule } from './r2-request-kor20-change-routing.module';
import {
  R2RequestKor20ChangeComponent
} from "../../pages";

@NgModule({
declarations: [R2RequestKor20ChangeComponent],
imports: [...IMPORTS, R2RequestKor20ChangeRoutingModule],
providers: PROVIDERS
})
export class R2RequestKor20ChangeModule { }