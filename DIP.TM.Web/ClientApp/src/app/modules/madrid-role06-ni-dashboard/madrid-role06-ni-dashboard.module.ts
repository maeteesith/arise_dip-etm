import { NgModule } from '@angular/core';
import { MadridRole06NiDashboardRoutingModule } from './madrid-role06-ni-dashboard-routing.module';
import { MadridRole06NiDashboardComponent } from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';


@NgModule({
  declarations: [MadridRole06NiDashboardComponent],
  imports: [...IMPORTS, MadridRole06NiDashboardRoutingModule],
  providers: PROVIDERS
})

export class MadridRole06NiDashboardModule { }
