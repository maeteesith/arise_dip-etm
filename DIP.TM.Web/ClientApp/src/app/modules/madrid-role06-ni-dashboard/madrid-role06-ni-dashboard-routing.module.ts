import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06NiDashboardComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole06NiDashboardComponent
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06NiDashboardRoutingModule { }
