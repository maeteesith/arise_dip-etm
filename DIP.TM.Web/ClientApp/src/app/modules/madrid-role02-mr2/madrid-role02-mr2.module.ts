import { NgModule } from '@angular/core';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

import { MadridRole02Mr23StepComponent, MadridRole02Mr21ListComponent } from "../../pages";
import { MadridRole02Mr2RoutingModule } from './madrid-role02-mr2-routing.module';


@NgModule({
  declarations: [
    MadridRole02Mr23StepComponent
    , MadridRole02Mr21ListComponent],
  imports: [
    ...IMPORTS, MadridRole02Mr2RoutingModule],
  providers: PROVIDERS
})
export class MadridRole02Mr2Module { }
