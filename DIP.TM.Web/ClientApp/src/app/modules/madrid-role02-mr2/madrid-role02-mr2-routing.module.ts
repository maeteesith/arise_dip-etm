import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole02Mr23StepComponent, MadridRole02Mr21ListComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole02Mr21ListComponent
  },
  {
    path: "list",
    component: MadridRole02Mr21ListComponent
  },
  {
    path: "step",
    component: MadridRole02Mr23StepComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02Mr2RoutingModule { }
