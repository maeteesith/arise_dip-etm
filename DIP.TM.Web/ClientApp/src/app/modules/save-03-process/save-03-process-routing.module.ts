import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save03ProcessComponent,
  Save03ProcessListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save03ProcessComponent
  },
  {
    path: "add",
    component: Save03ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save03ProcessComponent
  },
  {
    path: "list",
    component: Save03ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save03ProcessRoutingModule {}
