import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save03ProcessRoutingModule } from "./save-03-process-routing.module";
import {
  Save03ProcessComponent,
  Save03ProcessListComponent
} from "../../pages";

@NgModule({
  declarations: [Save03ProcessComponent, Save03ProcessListComponent],
  imports: [...IMPORTS, Save03ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save03ProcessModule {}
