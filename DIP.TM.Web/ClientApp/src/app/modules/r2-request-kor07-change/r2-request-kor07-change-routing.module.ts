import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  R2RequestKor07ChangeComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: R2RequestKor07ChangeComponent,
},
{
path: "add",
component: R2RequestKor07ChangeComponent,
},
{
path: "edit/:id",
component: R2RequestKor07ChangeComponent,
},
{
path: "list",
component: R2RequestKor07ChangeComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class R2RequestKor07ChangeRoutingModule { }