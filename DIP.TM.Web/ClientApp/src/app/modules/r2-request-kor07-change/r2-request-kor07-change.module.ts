import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { R2RequestKor07ChangeRoutingModule } from './r2-request-kor07-change-routing.module';
import {
  R2RequestKor07ChangeComponent
} from "../../pages";

@NgModule({
declarations: [R2RequestKor07ChangeComponent],
imports: [...IMPORTS, R2RequestKor07ChangeRoutingModule],
providers: PROVIDERS
})
export class R2RequestKor07ChangeModule { }
