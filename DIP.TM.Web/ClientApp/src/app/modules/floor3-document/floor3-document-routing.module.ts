import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { Floor3DocumentComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Floor3DocumentComponent
  },
  {
    path: "add",
    component: Floor3DocumentComponent
  },
  {
    path: "edit/:id",
    component: Floor3DocumentComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Floor3DocumentRoutingModule {}
