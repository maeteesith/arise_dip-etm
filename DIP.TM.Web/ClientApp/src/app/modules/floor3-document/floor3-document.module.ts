import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Floor3DocumentRoutingModule } from "./floor3-document-routing.module";
import { Floor3DocumentComponent } from "../../pages";

@NgModule({
  declarations: [Floor3DocumentComponent],
  imports: [...IMPORTS, Floor3DocumentRoutingModule],
  providers: PROVIDERS
})
export class Floor3DocumentModule {}
