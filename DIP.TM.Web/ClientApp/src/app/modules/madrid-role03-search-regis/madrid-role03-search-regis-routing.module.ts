import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole03SearchRegisComponent } from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole03SearchRegisComponent
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole03SearchRegisRoutingModule { }
