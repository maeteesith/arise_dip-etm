import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole03SearchRegisRoutingModule } from './madrid-role03-search-regis-routing.module';
import { MadridRole03SearchRegisComponent } from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [MadridRole03SearchRegisComponent],
  imports: [...IMPORTS, MadridRole03SearchRegisRoutingModule],
  providers: PROVIDERS
})
export class MadridRole03SearchRegisModule { }
