import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  PrintBookComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: PrintBookComponent,
},
{
path: "add",
component: PrintBookComponent,
},
{
path: "edit/:id",
component: PrintBookComponent,
},
{
path: "list",
component: PrintBookComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class PrintBookRoutingModule {}