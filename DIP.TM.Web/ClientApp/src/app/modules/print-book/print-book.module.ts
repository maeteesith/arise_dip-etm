import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PrintBookRoutingModule } from './print-book-routing.module';
import {
  PrintBookComponent
} from "../../pages";
 
@NgModule({
declarations: [
  PrintBookComponent
],
imports: [...IMPORTS, PrintBookRoutingModule],
providers: PROVIDERS
})
export class PrintBookModule { }