import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole05SearchComponent,MadridRole05RequestInComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole05RequestInComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole05RequestInRoutingModule { }
