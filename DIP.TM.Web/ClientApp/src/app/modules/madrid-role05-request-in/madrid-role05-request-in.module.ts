import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole05SearchComponent,MadridRole05ModalComponent, MadridRole05DocAttachComponent,MadridRole05PaymentComponent,MadridRole05FeeComponent,MadridRole05RequestInComponent } from "../../pages";
import { MatTabsModule } from '@angular/material/tabs'
import { ConfirmationDialogComponent } from '../../shared';

import { MadridRole05RequestInRoutingModule } from './madrid-role05-request-in-routing.module';


@NgModule({
  declarations: [MadridRole05RequestInComponent],
  imports: [...IMPORTS, MadridRole05RequestInRoutingModule,MatTabsModule],
  providers: PROVIDERS
})
export class MadridRole05RequestInModule { }
