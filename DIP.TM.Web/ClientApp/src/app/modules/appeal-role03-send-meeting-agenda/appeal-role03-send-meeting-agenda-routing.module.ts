import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole03SendMeetingAgendaListComponent } from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole03SendMeetingAgendaListComponent
  },
  {
    path: "list",
    component: AppealRole03SendMeetingAgendaListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole03SendMeetingAgendaRoutingModule { }
