import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole03SendMeetingAgendaRoutingModule } from './appeal-role03-send-meeting-agenda-routing.module';
import { AppealRole03SendMeetingAgendaListComponent } from '../../pages';


@NgModule({
  declarations: [AppealRole03SendMeetingAgendaListComponent],
  imports: [
    ...IMPORTS,
    AppealRole03SendMeetingAgendaRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole03SendMeetingAgendaModule { }
