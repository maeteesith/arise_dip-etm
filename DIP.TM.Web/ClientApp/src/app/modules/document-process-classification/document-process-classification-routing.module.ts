import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocumentProcessClassificationComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentProcessClassificationComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentProcessClassificationRoutingModule {}
