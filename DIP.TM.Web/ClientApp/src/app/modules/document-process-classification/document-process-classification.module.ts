import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentProcessClassificationRoutingModule } from "./document-process-classification-routing.module";
import { DocumentProcessClassificationComponent } from "../../pages";

@NgModule({
  declarations: [DocumentProcessClassificationComponent],
  imports: [...IMPORTS, DocumentProcessClassificationRoutingModule],
  providers: PROVIDERS
})
export class DocumentProcessClassificationModule {}
