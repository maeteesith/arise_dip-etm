import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RegistrationRequestComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RegistrationRequestComponent
  },
  {
    path: "add",
    component: RegistrationRequestComponent
  },
  {
    path: "edit/:id",
    component: RegistrationRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationRequestRoutingModule {}
