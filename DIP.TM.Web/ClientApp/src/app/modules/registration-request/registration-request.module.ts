import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RegistrationRequestRoutingModule } from "./registration-request-routing.module";
import { RegistrationRequestComponent } from "../../pages";

@NgModule({
  declarations: [RegistrationRequestComponent],
  imports: [...IMPORTS, RegistrationRequestRoutingModule],
  providers: PROVIDERS
})
export class RegistrationRequestModule {}
