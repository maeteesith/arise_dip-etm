import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole05ConsiderPaymentRoutingModule } from "./public-role05-consider-payment-routing.module";
import {
  PublicRole05ConsiderPaymentListComponent,
  PublicRole05ConsiderPaymentComponent
} from "../../pages";

@NgModule({
  declarations: [
    PublicRole05ConsiderPaymentListComponent,
    PublicRole05ConsiderPaymentComponent
  ],
  imports: [...IMPORTS, PublicRole05ConsiderPaymentRoutingModule],
  providers: PROVIDERS
})
export class PublicRole05ConsiderPaymentModule { }
