import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  PublicRole05ConsiderPaymentListComponent,
  PublicRole05ConsiderPaymentComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole05ConsiderPaymentComponent
  },
  {
    path: "add",
    component: PublicRole05ConsiderPaymentComponent
  },
  {
    path: "edit/:id",
    component: PublicRole05ConsiderPaymentComponent
  },
  {
    path: "list",
    component: PublicRole05ConsiderPaymentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole05ConsiderPaymentRoutingModule { }
