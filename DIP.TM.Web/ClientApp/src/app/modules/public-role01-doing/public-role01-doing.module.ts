import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole01DoingRoutingModule } from "./public-role01-doing-routing.module";
import { PublicRole01DoingListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole01DoingListComponent],
  imports: [...IMPORTS, PublicRole01DoingRoutingModule],
  providers: PROVIDERS
})
export class PublicRole01DoingModule {}
