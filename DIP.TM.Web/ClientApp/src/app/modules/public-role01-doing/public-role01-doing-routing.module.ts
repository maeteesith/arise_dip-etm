import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole01DoingListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole01DoingListComponent
  },
  {
    path: "list",
    component: PublicRole01DoingListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole01DoingRoutingModule {}
