import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole04itemRoutingModule } from "./public-role04-item-routing.module";
import { PublicRole04itemListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole04itemListComponent],
  imports: [...IMPORTS, PublicRole04itemRoutingModule],
  providers: PROVIDERS
})
export class PublicRole04itemModule {}
