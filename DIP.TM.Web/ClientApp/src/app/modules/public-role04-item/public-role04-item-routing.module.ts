import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole04itemListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole04itemListComponent
  },
  {
    path: "list",
    component: PublicRole04itemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole04itemRoutingModule {}
