import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole05SearchIncomingAllComponent } from 'src/app/pages/madrid-role05-search-incoming-all/madrid-role05-search-incoming-all.component';


const routes: Routes = [
  {
    path: "",
    component: MadridRole05SearchIncomingAllComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole05SearchIncomingAllRoutingModule { }
