import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MadridRole05SearchIncomingAllComponent ,} from 'src/app/pages/madrid-role05-search-incoming-all/madrid-role05-search-incoming-all.component';
import { MadridRole05JopTransferComponent } from '../../pages';
import { IMPORTS } from '../imports';
import { MadridRole05SearchIncomingAllRoutingModule } from './madrid-role05-search-incoming-all-routing.module';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [MadridRole05SearchIncomingAllComponent,MadridRole05JopTransferComponent],
  imports: [...IMPORTS, MadridRole05SearchIncomingAllRoutingModule],
  entryComponents: [MadridRole05SearchIncomingAllComponent,MadridRole05JopTransferComponent],
  providers: PROVIDERS
})
export class MadridRole05SearchIncomingAllModule { }
