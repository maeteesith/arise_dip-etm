import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestSearchRoutingModule } from "./request-search-routing.module";
import { RequestSearchComponent } from "../../pages";

@NgModule({
  declarations: [RequestSearchComponent],
  imports: [...IMPORTS, RequestSearchRoutingModule],
  providers: PROVIDERS
})
export class RequestSearchModule {}
