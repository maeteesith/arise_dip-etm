import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RequestSearchComponent } from "../../pages";

const routes: Routes = [
    {
        path: "",
        component: RequestSearchComponent
    },
    {
        path: "edit/:id",
        component: RequestSearchComponent
    },
    {
        path: "load/:id",
        component: RequestSearchComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequestSearchRoutingModule { }
