import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save07ProcessComponent,
  Save07ProcessListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save07ProcessComponent
  },
  {
    path: "add",
    component: Save07ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save07ProcessComponent
  },
  {
    path: "list",
    component: Save07ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save07ProcessRoutingModule {}
