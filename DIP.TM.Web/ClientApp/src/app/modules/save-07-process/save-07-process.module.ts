import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save07ProcessRoutingModule } from "./save-07-process-routing.module";
import {
  Save07ProcessComponent,
  Save07ProcessListComponent
} from "../../pages";

@NgModule({
  declarations: [Save07ProcessComponent, Save07ProcessListComponent],
  imports: [...IMPORTS, Save07ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save07ProcessModule {}
