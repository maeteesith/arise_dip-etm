import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave200ProcessRoutingModule } from "./eform-save-200-process-routing.module";
import { eFormSave200ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave200ProcessComponent],
  imports: [...IMPORTS, eFormSave200ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave200ProcessModule {}
