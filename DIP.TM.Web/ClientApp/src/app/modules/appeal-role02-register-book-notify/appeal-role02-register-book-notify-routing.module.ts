import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole02RegisterBookNotifyComponent } from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole02RegisterBookNotifyComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole02RegisterBookNotifyRoutingModule { }
