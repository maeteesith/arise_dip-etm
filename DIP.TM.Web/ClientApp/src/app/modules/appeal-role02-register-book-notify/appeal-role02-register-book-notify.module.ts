import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole02RegisterBookNotifyRoutingModule } from './appeal-role02-register-book-notify-routing.module';
import { AppealRole02RegisterBookNotifyComponent } from '../../pages';


@NgModule({
  declarations: [AppealRole02RegisterBookNotifyComponent],
  imports: [
    ...IMPORTS,
    AppealRole02RegisterBookNotifyRoutingModule
  ]
})
export class AppealRole02RegisterBookNotifyModule { }
