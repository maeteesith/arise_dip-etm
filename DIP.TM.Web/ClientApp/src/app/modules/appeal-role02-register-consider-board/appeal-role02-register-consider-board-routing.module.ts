import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole02RegisterConsiderBoardComponent,
} from '../../pages'


const routes: Routes = [
  {
    path: "",
    component: AppealRole02RegisterConsiderBoardComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole02RegisterConsiderBoardRoutingModule { }
