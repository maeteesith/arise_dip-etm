import { NgModule } from '@angular/core';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers'
import { AppealRole02RegisterConsiderBoardRoutingModule } from './appeal-role02-register-consider-board-routing.module';
import { 
  AppealRole02RegisterConsiderBoardComponent,
} from '../../pages'


@NgModule({
  declarations: [AppealRole02RegisterConsiderBoardComponent],
  imports: [
    ...IMPORTS,
    AppealRole02RegisterConsiderBoardRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole02RegisterConsiderBoardModule { }
