import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave080ProcessRoutingModule } from "./eform-save-080-process-routing.module";
import { eFormSave080ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave080ProcessComponent],
  imports: [...IMPORTS, eFormSave080ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave080ProcessModule {}
