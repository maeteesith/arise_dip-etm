import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole05ReleaseListComponent,
  DocumentRole05ReleaseComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole05ReleaseComponent
  },
  {
    path: "add",
    component: DocumentRole05ReleaseComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole05ReleaseComponent
  },
  {
   path: "list",
   component: DocumentRole05ReleaseListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole05ReleaseRoutingModule {}
