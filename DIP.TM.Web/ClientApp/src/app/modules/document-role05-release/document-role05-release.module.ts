import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole05ReleaseRoutingModule } from "./document-role05-release-routing.module";
import {
  DocumentRole05ReleaseListComponent,
  DocumentRole05ReleaseComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole05ReleaseListComponent, 
    DocumentRole05ReleaseComponent
  ],
  imports: [...IMPORTS, DocumentRole05ReleaseRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole05ReleaseModule { }
