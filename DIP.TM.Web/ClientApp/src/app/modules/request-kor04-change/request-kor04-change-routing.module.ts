import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RequestKor04ChangeComponent } from "../../pages";

const routes: Routes = [
{
path: "",
component: RequestKor04ChangeComponent,
},
{
path: "add",
component: RequestKor04ChangeComponent,
},
{
path: "edit/:id",
component: RequestKor04ChangeComponent,
},
{
path: "list",
component: RequestKor04ChangeComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor04ChangeRoutingModule {}