import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor04ChangeRoutingModule } from './request-kor04-change-routing.module';
import {
  RequestKor04ChangeComponent
} from "../../pages";

@NgModule({
declarations: [RequestKor04ChangeComponent],
imports: [...IMPORTS, RequestKor04ChangeRoutingModule],
providers: PROVIDERS
})
export class RequestKor04ChangeModule { }