import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07CreateDocModalComponent } from "../../pages/madrid-role07-create-doc-modal/madrid-role07-create-doc-modal.component";
import { MatDialogModule } from "@angular/material";
import { MadridRole07ModalCheckRoutingModule } from "./madrid-role07-modal-check-routing.module";
import { MadridRole07ModalCheckComponent } from "src/app/pages/madrid-role07-modal-check/madrid-role07-modal-check.component";

@NgModule({
  declarations: [MadridRole07ModalCheckComponent],
  imports: [...IMPORTS, MadridRole07ModalCheckRoutingModule, MatDialogModule],
  entryComponents:[MadridRole07ModalCheckComponent],
  providers: PROVIDERS
})
export class MadridRole07ModalCheckModule {}
