import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07CreateDocModalComponent } from "src/app/pages/madrid-role07-create-doc-modal/madrid-role07-create-doc-modal.component";
import { MadridRole07SaveListStep1Component } from "src/app/pages/madrid-role07-save-list-step1/madrid-role07-save-list-step1.component";
import { MadridRole07SaveListStep1Module } from "../madrid-role07-save-list-step1/madrid-role07-save-list-step1.module";
import { MadridRole07ModalCheckComponent } from "src/app/pages/madrid-role07-modal-check/madrid-role07-modal-check.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07ModalCheckComponent,
  },
  // {
  //   path: "modal",
  //   component: MadridRole07CreateDocModalComponent
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07ModalCheckRoutingModule {}
