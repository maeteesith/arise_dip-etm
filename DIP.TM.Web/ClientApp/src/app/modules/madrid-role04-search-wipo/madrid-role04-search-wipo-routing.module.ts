import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MadridRole04SearchWIPOComponent} from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole04SearchWIPOComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04SearchWIPORoutingModule { }
