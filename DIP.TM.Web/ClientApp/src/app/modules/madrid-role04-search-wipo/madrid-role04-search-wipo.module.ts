import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole04SearchWIPORoutingModule } from './madrid-role04-search-wipo-routing.module';
import { MadridRole04SearchWIPOComponent} from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [
    MadridRole04SearchWIPOComponent
  ],
  imports: [...IMPORTS, MadridRole04SearchWIPORoutingModule],
  providers: PROVIDERS
})
export class MadridRole04SearchWIPOModule { }
