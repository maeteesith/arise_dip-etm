import { NgModule } from '@angular/core';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06OT03RoutingModule } from './madrid-role06-ot03-list-routing.module';
import { ModalAttachFileArrayComponent } from 'src/app/pages/modal-attach-file-array/modal-attach-file-array.component';
import { MadridRole06Ot03ListComponent } from 'src/app/pages/madrid-role06-ot03-list/madrid-role06-ot03-list.component';


@NgModule({
  declarations: [MadridRole06Ot03ListComponent, ModalAttachFileArrayComponent],
  imports: [...IMPORTS, MadridRole06OT03RoutingModule],
  entryComponents: [ModalAttachFileArrayComponent],
  providers: PROVIDERS
})
export class MadridRole06OT03Module { }
