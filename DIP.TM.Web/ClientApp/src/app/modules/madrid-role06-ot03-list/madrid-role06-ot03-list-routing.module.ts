import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06Ot03ListComponent } from 'src/app/pages/madrid-role06-ot03-list/madrid-role06-ot03-list.component';


const routes: Routes = [
  {
    path: "",
    component: MadridRole06Ot03ListComponent
  },
  {
    path: "list",
    component: MadridRole06Ot03ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06OT03RoutingModule { }
