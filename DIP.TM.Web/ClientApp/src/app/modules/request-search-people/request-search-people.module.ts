import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestSearchPeopleRoutingModule } from "./request-search-people-routing.module";
import { RequestSearchPeopleComponent } from "../../pages";

@NgModule({
  declarations: [RequestSearchPeopleComponent],
  imports: [...IMPORTS, RequestSearchPeopleRoutingModule],
  providers: PROVIDERS
})
export class RequestSearchPeopleModule {}
