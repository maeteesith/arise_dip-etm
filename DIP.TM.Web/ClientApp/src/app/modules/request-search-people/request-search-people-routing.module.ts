import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RequestSearchPeopleComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RequestSearchPeopleComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestSearchPeopleRoutingModule {}
