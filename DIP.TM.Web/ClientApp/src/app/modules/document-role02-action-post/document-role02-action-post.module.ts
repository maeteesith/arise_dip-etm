import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole02ActionPostRoutingModule } from "./document-role02-action-post-routing.module";
import {
  DocumentRole02ActionPostListComponent,
  //DocumentRole02ActionPostComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole02ActionPostListComponent, 
    //DocumentRole02ActionPostComponent
  ],
  imports: [...IMPORTS, DocumentRole02ActionPostRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole02ActionPostModule { }
