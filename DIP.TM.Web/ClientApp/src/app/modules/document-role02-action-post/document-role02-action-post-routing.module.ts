import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole02ActionPostListComponent,
  //DocumentRole02ActionPostComponent
} from "../../pages";

const routes: Routes = [
  //{
  //  path: "",
  //  component: DocumentRole02ActionPostComponent
  //},
  //{
  //  path: "add",
  //  component: DocumentRole02ActionPostComponent
  //},
  //{
  //  path: "edit/:id",
  //  component: DocumentRole02ActionPostComponent
  //},
  {
    path: "list",
    component: DocumentRole02ActionPostListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole02ActionPostRoutingModule { }
