import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsiderRevisingRequestRoutingModule } from './consider-revising-request-routing.module';
import {
  ConsiderRevisingRequestComponent
} from "../../pages";

@NgModule({
declarations: [ConsiderRevisingRequestComponent],
imports: [...IMPORTS, ConsiderRevisingRequestRoutingModule],
providers: PROVIDERS
})
export class ConsiderRevisingRequestModule { }
