import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ConsiderRevisingRequestComponent } from "../../pages";

const routes: Routes = [
{
path: "",
component: ConsiderRevisingRequestComponent,
},
{
path: "add",
component: ConsiderRevisingRequestComponent,
},
{
path: "edit/:id",
component: ConsiderRevisingRequestComponent,
},
{
path: "list",
component: ConsiderRevisingRequestComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ConsiderRevisingRequestRoutingModule {}
