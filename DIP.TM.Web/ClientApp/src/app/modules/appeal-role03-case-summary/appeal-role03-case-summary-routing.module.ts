import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole03CaseSummaryListComponent,
  AppealRole03CaseSummaryComponent
} from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole03CaseSummaryComponent
  },
  {
    path: "list",
    component: AppealRole03CaseSummaryListComponent
  }
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole03CaseSummaryRoutingModule { }