import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole03CaseSummaryRoutingModule } from './appeal-role03-case-summary-routing.module';
import { 
  AppealRole03CaseSummaryListComponent,
  AppealRole03CaseSummaryComponent
} from '../../pages';


@NgModule({
  declarations: [AppealRole03CaseSummaryListComponent, AppealRole03CaseSummaryComponent],
  imports: [
    ...IMPORTS,
    AppealRole03CaseSummaryRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole03CaseSummaryModule { }