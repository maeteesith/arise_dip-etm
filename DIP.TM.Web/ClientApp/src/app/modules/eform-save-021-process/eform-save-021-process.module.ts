import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave021ProcessRoutingModule } from "./eform-save-021-process-routing.module";
import { eFormSave021ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave021ProcessComponent],
  imports: [...IMPORTS, eFormSave021ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave021ProcessModule {}
