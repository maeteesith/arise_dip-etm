import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave040ProcessRoutingModule } from "./eform-save-040-process-routing.module";
import { eFormSave040ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave040ProcessComponent],
  imports: [...IMPORTS, eFormSave040ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave040ProcessModule {}
