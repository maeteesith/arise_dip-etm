import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save06ProcessRoutingModule } from "./save-06-process-routing.module";
import {
  Save06ProcessComponent,
  Save06ProcessListComponent
} from "../../pages";

@NgModule({
  declarations: [Save06ProcessComponent, Save06ProcessListComponent],
  imports: [...IMPORTS, Save06ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save06ProcessModule {}
