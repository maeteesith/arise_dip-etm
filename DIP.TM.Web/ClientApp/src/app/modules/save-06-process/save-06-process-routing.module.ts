import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save06ProcessComponent,
  Save06ProcessListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save06ProcessComponent
  },
  {
    path: "add",
    component: Save06ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save06ProcessComponent
  },
  {
    path: "list",
    component: Save06ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save06ProcessRoutingModule {}
