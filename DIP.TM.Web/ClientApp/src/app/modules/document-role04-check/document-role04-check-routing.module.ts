import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04CheckListComponent,
  DocumentRole04CheckComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole04CheckComponent
  },
  {
    path: "add",
    component: DocumentRole04CheckComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole04CheckComponent
  },
  {
    path: "list",
    component: DocumentRole04CheckListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04CheckRoutingModule { }
