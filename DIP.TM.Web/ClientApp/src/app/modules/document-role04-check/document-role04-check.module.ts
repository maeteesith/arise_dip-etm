import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04CheckRoutingModule } from "./document-role04-check-routing.module";
import {
  DocumentRole04CheckListComponent,
  DocumentRole04CheckComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04CheckListComponent,
    DocumentRole04CheckComponent
  ],
  imports: [...IMPORTS, DocumentRole04CheckRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04CheckModule { }
