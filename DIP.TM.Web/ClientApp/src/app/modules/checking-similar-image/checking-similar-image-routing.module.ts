import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CheckingSimilarImageComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: CheckingSimilarImageComponent
  },
  {
    path: "add",
    component: CheckingSimilarImageComponent
  },
  {
    path: "edit/:id",
    component: CheckingSimilarImageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckingSimilarImageRoutingModule { }
