import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { CheckingSimilarImageRoutingModule } from "./checking-similar-image-routing.module";
import { CheckingSimilarImageComponent } from "../../pages";

@NgModule({
  declarations: [CheckingSimilarImageComponent],
  imports: [...IMPORTS, CheckingSimilarImageRoutingModule],
  providers: PROVIDERS
})
export class CheckingSimilarImageModule {}
