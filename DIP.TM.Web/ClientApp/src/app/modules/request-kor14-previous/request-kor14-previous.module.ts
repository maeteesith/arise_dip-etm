import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor14PreviousRoutingModule } from './request-kor14-previous-routing.module';
import {
  RequestKor14PreviousComponent
} from "../../pages";
 
@NgModule({
declarations: [
  RequestKor14PreviousComponent
],
imports: [...IMPORTS, RequestKor14PreviousRoutingModule],
providers: PROVIDERS
})
export class RequestKor14PreviousModule { }