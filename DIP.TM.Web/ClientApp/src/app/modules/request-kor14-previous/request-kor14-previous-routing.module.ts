import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  RequestKor14PreviousComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: RequestKor14PreviousComponent,
},
{
path: "add",
component: RequestKor14PreviousComponent,
},
{
path: "edit/:id",
component: RequestKor14PreviousComponent,
},
{
path: "list",
component: RequestKor14PreviousComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor14PreviousRoutingModule {}