import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor07ChangeRoutingModule } from "./request-kor07-change-routing.module";
import {
  RequestKor07ChangeComponent
} from "../../pages";

@NgModule({
declarations: [RequestKor07ChangeComponent],
imports: [...IMPORTS, RequestKor07ChangeRoutingModule],
providers: PROVIDERS
})
export class RequestKor07ChangeModule { }