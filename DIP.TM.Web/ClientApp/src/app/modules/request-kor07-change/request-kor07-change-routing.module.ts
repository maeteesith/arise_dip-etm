import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RequestKor07ChangeComponent } from "../../pages";

const routes: Routes = [
{
path: "",
component: RequestKor07ChangeComponent,
},
{
path: "add",
component: RequestKor07ChangeComponent,
},
{
path: "edit/:id",
component: RequestKor07ChangeComponent,
},
{
path: "list",
component: RequestKor07ChangeComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor07ChangeRoutingModule {}