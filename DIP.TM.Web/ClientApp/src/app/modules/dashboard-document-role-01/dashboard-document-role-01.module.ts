import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DashboardDocumentRole01RoutingModule } from "./dashboard-document-role-01-routing.module";
import {
  //DashboardDocumentRole01ListComponent,
  DashboardDocumentRole01Component
} from "../../pages";

@NgModule({
  declarations: [
    //DashboardDocumentRole01ListComponent, 
    DashboardDocumentRole01Component
  ],
  imports: [...IMPORTS, DashboardDocumentRole01RoutingModule],
  providers: PROVIDERS
})
export class DashboardDocumentRole01Module { }
