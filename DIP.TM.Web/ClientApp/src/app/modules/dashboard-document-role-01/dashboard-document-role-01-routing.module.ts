import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //DashboardDocumentRole01ListComponent,
  DashboardDocumentRole01Component
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DashboardDocumentRole01Component
  },
  {
    path: "add",
    component: DashboardDocumentRole01Component
  },
  {
    path: "edit/:id",
    component: DashboardDocumentRole01Component
  //},
  //{
  // path: "list",
  // component: DashboardDocumentRole01ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardDocumentRole01RoutingModule {}
