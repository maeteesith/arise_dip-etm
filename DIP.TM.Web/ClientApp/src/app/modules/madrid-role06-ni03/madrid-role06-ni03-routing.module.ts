import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06Ni03ListComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole06Ni03ListComponent
  },
  {
    path: "list",
    component: MadridRole06Ni03ListComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06Ni03RoutingModule { }
