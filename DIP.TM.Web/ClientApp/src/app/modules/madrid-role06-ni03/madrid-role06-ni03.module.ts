import { NgModule } from '@angular/core';
import { MadridRole06Ni03RoutingModule } from './madrid-role06-ni03-routing.module';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06Ni03ListComponent } from "../../pages";


@NgModule({
  declarations: [MadridRole06Ni03ListComponent],
  imports: [...IMPORTS, MadridRole06Ni03RoutingModule],
  providers: PROVIDERS
})
export class MadridRole06Ni03Module { }
