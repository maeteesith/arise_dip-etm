import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02EditInterListComponent,MadridRole02EditInterComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02EditInterComponent,
  },{
    path: "add",
    component: MadridRole02EditInterComponent
  },{
    path: "list",
    component: MadridRole02EditInterListComponent
  }
  //{
  //  path: "list",
  //  component: MadridRole02WaitListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02EditInterRoutingModule {}
