import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02EditInterRoutingModule } from "./madrid-role02-edit-inter-routing.module";
import { MadridRole02EditInterListComponent, MadridRole02EditInterComponent,MadridRole02EditInterModalComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole02EditInterListComponent,MadridRole02EditInterComponent,MadridRole02EditInterModalComponent],
  imports: [...IMPORTS, MadridRole02EditInterRoutingModule],
  entryComponents:[MadridRole02EditInterModalComponent],
  providers: PROVIDERS
})
export class MadridRole02EditInterModule {}
