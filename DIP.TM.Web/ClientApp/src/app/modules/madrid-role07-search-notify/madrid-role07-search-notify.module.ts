import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SearchNotifyRoutingModule } from "./madrid-role07-search-notify-routing.module";
import { MadridRole07SearchNotifyComponent } from "src/app/pages/madrid-role07-search-notify/madrid-role07-search-notify.component";

@NgModule({
  declarations: [MadridRole07SearchNotifyComponent],
  imports: [...IMPORTS, MadridRole07SearchNotifyRoutingModule],
  entryComponents: [MadridRole07SearchNotifyComponent],
  providers: PROVIDERS
})
export class MadridRole07SeachNotifyModule {}
