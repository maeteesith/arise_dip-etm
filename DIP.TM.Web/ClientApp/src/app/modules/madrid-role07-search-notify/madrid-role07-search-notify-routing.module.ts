import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SearchNotifyComponent } from "src/app/pages/madrid-role07-search-notify/madrid-role07-search-notify.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SearchNotifyComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SearchNotifyRoutingModule {}
