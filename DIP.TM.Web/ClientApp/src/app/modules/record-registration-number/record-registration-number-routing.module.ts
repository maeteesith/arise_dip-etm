import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //RecordRegistrationNumberListComponent,
  RecordRegistrationNumberComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RecordRegistrationNumberComponent
  },
  {
    path: "add",
    component: RecordRegistrationNumberComponent
  },
  {
    path: "edit/:id",
    component: RecordRegistrationNumberComponent
  },
  //{
  //  path: "list",
  //  component: RecordRegistrationNumberListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordRegistrationNumberRoutingModule {}
