import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RecordRegistrationNumberRoutingModule } from "./record-registration-number-routing.module";
import {
  //RecordRegistrationNumberListComponent,
  RecordRegistrationNumberComponent
} from "../../pages";

@NgModule({
  declarations: [
    //RecordRegistrationNumberListComponent,
    RecordRegistrationNumberComponent],
  imports: [...IMPORTS, RecordRegistrationNumberRoutingModule],
  providers: PROVIDERS
})
export class RecordRegistrationNumberModule {}
