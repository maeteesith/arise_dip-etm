import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Mm05RoutingModule } from './mm05-routing.module';
import { Mm06Component } from 'src/app/pages/mm06/mm06.component';
import { IMPORTS } from '../imports';
import { RouterModule } from '@angular/router';
import { PROVIDERS } from '../providers';
import { Mm05Component } from 'src/app/pages/mm05/mm05.component';



@NgModule({
    declarations: [Mm05Component],
    imports: [...IMPORTS, Mm05RoutingModule],
    entryComponents: [Mm05Component],
    providers: PROVIDERS
})
export class Mm05Module { }
