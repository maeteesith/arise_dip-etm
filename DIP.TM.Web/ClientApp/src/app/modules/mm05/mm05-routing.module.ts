import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArchwizardModule } from 'angular-archwizard';
import { Mm05Component } from 'src/app/pages/mm05/mm05.component';


const routes: Routes = [
  {
    path: "",
    component: Mm05Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), ArchwizardModule],
  exports: [RouterModule]
})
export class Mm05RoutingModule { }
