import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  RequestKor08PreviousComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: RequestKor08PreviousComponent,
},
{
path: "add",
component: RequestKor08PreviousComponent,
},
{
path: "edit/:id",
component: RequestKor08PreviousComponent,
},
{
path: "list",
component: RequestKor08PreviousComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor08PreviousRoutingModule {}