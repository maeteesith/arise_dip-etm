import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor08PreviousRoutingModule } from './request-kor08-previous-routing.module';
import {
  RequestKor08PreviousComponent
} from "../../pages";
 
@NgModule({
declarations: [
  RequestKor08PreviousComponent
],
imports: [...IMPORTS, RequestKor08PreviousRoutingModule],
providers: PROVIDERS
})
export class RequestKor08PreviousModule { }