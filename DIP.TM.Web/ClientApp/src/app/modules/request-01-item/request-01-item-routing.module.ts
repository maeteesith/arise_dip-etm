import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { Request01ItemListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Request01ItemListComponent
  },
  {
    path: "list",
    component: Request01ItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Request01ItemRoutingModule {}
