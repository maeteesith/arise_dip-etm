import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Request01ItemRoutingModule } from "./request-01-item-routing.module";
import { Request01ItemListComponent } from "../../pages";

@NgModule({
  declarations: [Request01ItemListComponent],
  imports: [...IMPORTS, Request01ItemRoutingModule],
  providers: PROVIDERS
})
export class Request01ItemModule {}
