import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ZoneCopyRoutingModule } from "./zone-copy-routing.module";
import { ZoneCopyComponent } from "../../pages";

@NgModule({
  declarations: [ZoneCopyComponent],
  imports: [...IMPORTS, ZoneCopyRoutingModule],
  providers: PROVIDERS,
})
export class ZoneCopyModule {}
