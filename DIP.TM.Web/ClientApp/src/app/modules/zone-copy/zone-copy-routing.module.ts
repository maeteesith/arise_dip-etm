import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ZoneCopyComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: ZoneCopyComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ZoneCopyRoutingModule {}
