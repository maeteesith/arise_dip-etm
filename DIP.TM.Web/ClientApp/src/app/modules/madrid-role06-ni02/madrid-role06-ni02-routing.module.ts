import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06Ni02ListComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole06Ni02ListComponent
  },
  {
    path: "list",
    component: MadridRole06Ni02ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06Ni02RoutingModule { }
