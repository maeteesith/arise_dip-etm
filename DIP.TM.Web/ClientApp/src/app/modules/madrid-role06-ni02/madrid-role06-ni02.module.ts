import { NgModule } from '@angular/core';
import { MadridRole06Ni02RoutingModule } from './madrid-role06-ni02-routing.module';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06Ni02ListComponent } from "../../pages";


@NgModule({
  declarations: [MadridRole06Ni02ListComponent],
  imports: [...IMPORTS, MadridRole06Ni02RoutingModule],
  providers: PROVIDERS
})
export class MadridRole06Ni02Module { }
