import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole04SavePostalReceiptListComponent } from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole04SavePostalReceiptListComponent
  },
  {
    path: "list",
    component: AppealRole04SavePostalReceiptListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04SavePostalReceiptRoutingModule { }
