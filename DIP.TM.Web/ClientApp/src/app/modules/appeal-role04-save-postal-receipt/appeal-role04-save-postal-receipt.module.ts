import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04SavePostalReceiptRoutingModule } from './appeal-role04-save-postal-receipt-routing.module';
import { AppealRole04SavePostalReceiptListComponent } from '../../pages'


@NgModule({
  declarations: [AppealRole04SavePostalReceiptListComponent],
  imports: [
    ...IMPORTS,
    AppealRole04SavePostalReceiptRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04SavePostalReceiptModule { }
