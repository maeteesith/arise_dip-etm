import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocumentProcessClassificationDoComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentProcessClassificationDoComponent
  },
  {
    path: "edit/:id",
    component: DocumentProcessClassificationDoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentProcessClassificationDoRoutingModule {}
