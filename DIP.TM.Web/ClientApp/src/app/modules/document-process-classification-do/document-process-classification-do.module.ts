import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentProcessClassificationDoRoutingModule } from "./document-process-classification-do-routing.module";
import { DocumentProcessClassificationDoComponent } from "../../pages";

@NgModule({
  declarations: [DocumentProcessClassificationDoComponent],
  imports: [...IMPORTS, DocumentProcessClassificationDoRoutingModule],
  providers: PROVIDERS
})
export class DocumentProcessClassificationDoModule {}
