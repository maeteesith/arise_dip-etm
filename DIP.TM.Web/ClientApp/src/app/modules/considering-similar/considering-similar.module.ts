import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsideringSimilarRoutingModule } from "./considering-similar-routing.module";
import {
  ConsideringSimilarComponent,
  ConsideringSimilarListComponent
} from "../../pages";

@NgModule({
  declarations: [ConsideringSimilarComponent, ConsideringSimilarListComponent],
  imports: [...IMPORTS, ConsideringSimilarRoutingModule],
  providers: PROVIDERS
})
export class ConsideringSimilarModule {}
