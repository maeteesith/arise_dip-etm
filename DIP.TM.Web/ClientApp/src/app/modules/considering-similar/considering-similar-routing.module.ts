import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  ConsideringSimilarComponent,
  ConsideringSimilarListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: ConsideringSimilarComponent
  },
  {
    path: "add",
    component: ConsideringSimilarComponent
  },
  {
    path: "edit/:id",
    component: ConsideringSimilarComponent
  },
  {
    path: "list",
    component: ConsideringSimilarListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsideringSimilarRoutingModule {}
