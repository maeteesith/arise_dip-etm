import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor05PreviousRoutingModule } from './request-kor05-previous-routing.module';
import {
  RequestKor05PreviousComponent
} from "../../pages";
 
@NgModule({
declarations: [
  RequestKor05PreviousComponent
],
imports: [...IMPORTS, RequestKor05PreviousRoutingModule],
providers: PROVIDERS
})
export class RequestKor05PreviousModule { }