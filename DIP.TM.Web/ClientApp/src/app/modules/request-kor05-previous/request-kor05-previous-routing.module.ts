import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  RequestKor05PreviousComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: RequestKor05PreviousComponent,
},
{
path: "add",
component: RequestKor05PreviousComponent,
},
{
path: "edit/:id",
component: RequestKor05PreviousComponent,
},
{
path: "list",
component: RequestKor05PreviousComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor05PreviousRoutingModule {}