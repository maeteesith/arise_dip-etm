import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole02CheckListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole02CheckListComponent
  },
  {
    path: "list",
    component: PublicRole02CheckListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole02CheckRoutingModule {}
