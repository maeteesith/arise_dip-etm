import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02CheckRoutingModule } from "./public-role02-check-routing.module";
import { PublicRole02CheckListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole02CheckListComponent],
  imports: [...IMPORTS, PublicRole02CheckRoutingModule],
  providers: PROVIDERS
})
export class PublicRole02CheckModule {}
