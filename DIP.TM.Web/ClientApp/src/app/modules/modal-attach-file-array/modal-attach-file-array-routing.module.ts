import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ModalAttachFileArrayComponent } from "src/app/pages/modal-attach-file-array/modal-attach-file-array.component";

const routes: Routes = [
  {
    path: "",
    component: ModalAttachFileArrayComponent
  },
  // {
  //   path: "modal",
  //   component: MadridRole07CreateDocModalComponent
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModalAttachFileArrayRoutingModule {}
