import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MatDialogModule } from "@angular/material";
import { ModalAttachFileArrayRoutingModule } from "./modal-attach-file-array-routing.module";
import { ModalAttachFileArrayComponent } from "src/app/pages/modal-attach-file-array/modal-attach-file-array.component";

@NgModule({
  declarations: [ModalAttachFileArrayComponent],
  imports: [...IMPORTS, ModalAttachFileArrayRoutingModule, MatDialogModule],
  entryComponents: [ModalAttachFileArrayComponent],
  providers: PROVIDERS
})
export class ModalAttachFileModule { }
