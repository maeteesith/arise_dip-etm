import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole01ItemRoutingModule } from "./public-role01-item-routing.module";
import { PublicRole01ItemListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole01ItemListComponent],
  imports: [...IMPORTS, PublicRole01ItemRoutingModule],
  providers: PROVIDERS
})
export class PublicRole01ItemModule {}
