import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole01ItemListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole01ItemListComponent
  },
  {
    path: "list",
    component: PublicRole01ItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole01ItemRoutingModule {}
