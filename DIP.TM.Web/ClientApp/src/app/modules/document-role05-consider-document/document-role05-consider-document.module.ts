import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole05ConsiderDocumentRoutingModule } from "./document-role05-consider-document-routing.module";
import {
  DocumentRole05ConsiderDocumentListComponent,
  //DocumentRole05ConsiderDocumentComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole05ConsiderDocumentListComponent, 
    //DocumentRole05ConsiderDocumentComponent
  ],
  imports: [...IMPORTS, DocumentRole05ConsiderDocumentRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole05ConsiderDocumentModule { }
