import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole05ConsiderDocumentListComponent,
  //DocumentRole05ConsiderDocumentComponent
} from "../../pages";

const routes: Routes = [
  {
    //  path: "",
    //  component: DocumentRole05ConsiderDocumentComponent
    //},
    //{
    //  path: "add",
    //  component: DocumentRole05ConsiderDocumentComponent
    //},
    //{
    //  path: "edit/:id",
    //  component: DocumentRole05ConsiderDocumentComponent
    //},
    //{
    path: "list",
    component: DocumentRole05ConsiderDocumentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole05ConsiderDocumentRoutingModule { }
