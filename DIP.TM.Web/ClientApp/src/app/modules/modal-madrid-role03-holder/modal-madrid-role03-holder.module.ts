import { ModalMadridRole03Holder } from './../../pages/modal-madrid-role03-holder/modal-madrid-role03-holder.component';

import { MatDialog } from '@angular/material';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";


@NgModule({
  declarations: [ModalMadridRole03Holder],
  imports: [...IMPORTS, ModalMadridRole03Holder,MatDialog ],
  entryComponents:[ModalMadridRole03Holder],
  providers: PROVIDERS
})
export class ModalMadridRole03HolderRoutingModule {}
