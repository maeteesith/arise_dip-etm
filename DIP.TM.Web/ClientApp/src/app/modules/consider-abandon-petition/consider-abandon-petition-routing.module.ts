import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  ConsiderAbandonPetitionComponent, 
   ConsiderAbandonPetitionListComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: ConsiderAbandonPetitionComponent,
},
{
path: "add",
component: ConsiderAbandonPetitionListComponent,
},
{
path: "edit/:id",
component: ConsiderAbandonPetitionListComponent,
},
{
path: "list",
component: ConsiderAbandonPetitionListComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ConsiderAbandonPetitionRoutingModule { }