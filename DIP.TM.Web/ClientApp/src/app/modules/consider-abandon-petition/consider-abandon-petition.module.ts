import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsiderAbandonPetitionRoutingModule } from './consider-abandon-petition-routing.module';
import {
  ConsiderAbandonPetitionComponent, 
   ConsiderAbandonPetitionListComponent
} from "../../pages";

@NgModule({
declarations: [
  ConsiderAbandonPetitionComponent, 
   ConsiderAbandonPetitionListComponent
],
imports: [...IMPORTS, ConsiderAbandonPetitionRoutingModule],
providers: PROVIDERS
})
export class ConsiderAbandonPetitionModule { }