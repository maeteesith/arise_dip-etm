import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04RespondBookAppealRoutingModule } from './appeal-role04-respond-book-appeal-routing.module';
import { 
  AppealRole04RespondBookAppealComponent,
  AppealRole04RespondBookAppealListComponent
} from '../../pages';


@NgModule({
  declarations: [AppealRole04RespondBookAppealComponent,AppealRole04RespondBookAppealListComponent],
  imports: [
    ...IMPORTS,
    AppealRole04RespondBookAppealRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04RespondBookAppealModule { }
