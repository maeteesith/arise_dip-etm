import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole04RespondBookAppealComponent,
  AppealRole04RespondBookAppealListComponent
} from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole04RespondBookAppealComponent
  },
  {
    path: "list",
    component: AppealRole04RespondBookAppealListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04RespondBookAppealRoutingModule { }
