import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SaveListStep1RoutingModule } from "./madrid-role07-save-list-step1-routing.module";
import { MadridRole07SaveListStep1Component } from "src/app/pages/madrid-role07-save-list-step1/madrid-role07-save-list-step1.component";


@NgModule({
  declarations: [MadridRole07SaveListStep1Component,],
  imports: [...IMPORTS, MadridRole07SaveListStep1RoutingModule],
  providers: PROVIDERS
})
export class MadridRole07SaveListStep1Module {}
