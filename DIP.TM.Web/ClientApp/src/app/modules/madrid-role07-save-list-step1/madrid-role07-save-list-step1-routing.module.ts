import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SaveListStep1Component } from "src/app/pages/madrid-role07-save-list-step1/madrid-role07-save-list-step1.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SaveListStep1Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SaveListStep1RoutingModule {}
