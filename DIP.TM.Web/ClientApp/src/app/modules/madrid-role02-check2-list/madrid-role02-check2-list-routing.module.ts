import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02Check2ListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02Check2ListComponent
  },
  //{
  //  path: "list",
  //  component: MadridRole02WaitListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02Check2ListRoutingModule {}
