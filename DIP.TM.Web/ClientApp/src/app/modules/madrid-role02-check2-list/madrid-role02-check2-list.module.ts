import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02Check2ListRoutingModule } from "./madrid-role02-check2-list-routing.module";
import { MadridRole02Check2ListComponent } from "../../pages";
import { MadridRole02RequestProtection2ModalComponent } from "src/app/pages/madrid-role02-request-protection2-modal/madrid-role02-request-protection2-modal.component";
import { MadridRole02RequestProtection2PublishModalComponent } from "src/app/pages/madrid-role02-request-protection2-publish-modal/madrid-role02-request-protection2-publish-modal.component";
import { MadridRole02ConsiderRound2ModalComponent } from "src/app/pages/madrid-role02-consider-round2-modal/madrid-role02-consider-round2-modal.component";

@NgModule({
  declarations: [MadridRole02Check2ListComponent,MadridRole02RequestProtection2ModalComponent,MadridRole02RequestProtection2PublishModalComponent,MadridRole02ConsiderRound2ModalComponent],
  imports: [...IMPORTS, MadridRole02Check2ListRoutingModule],
  entryComponents:[MadridRole02RequestProtection2ModalComponent,MadridRole02RequestProtection2PublishModalComponent,MadridRole02ConsiderRound2ModalComponent],
  providers: PROVIDERS
})
export class MadridRole02Check2ListModule {}
