import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  RequestKor07PreviousComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: RequestKor07PreviousComponent,
},
{
path: "add",
component: RequestKor07PreviousComponent,
},
{
path: "edit/:id",
component: RequestKor07PreviousComponent,
},
{
path: "list",
component: RequestKor07PreviousComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor07PreviousRoutingModule {}