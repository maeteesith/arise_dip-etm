import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor07PreviousRoutingModule } from './request-kor07-previous-routing.module';
import {
  RequestKor07PreviousComponent
} from "../../pages";
 
@NgModule({
declarations: [
  RequestKor07PreviousComponent
],
imports: [...IMPORTS, RequestKor07PreviousRoutingModule],
providers: PROVIDERS
})
export class RequestKor07PreviousModule { }