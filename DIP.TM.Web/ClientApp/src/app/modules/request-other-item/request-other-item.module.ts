import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestOtherItemRoutingModule } from "./request-other-item-routing.module";
import { RequestOtherItemListComponent } from "../../pages";

@NgModule({
  declarations: [RequestOtherItemListComponent],
  imports: [...IMPORTS, RequestOtherItemRoutingModule],
  providers: PROVIDERS
})
export class RequestOtherItemModule {}
