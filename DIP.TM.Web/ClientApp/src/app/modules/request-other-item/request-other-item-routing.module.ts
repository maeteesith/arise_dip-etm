import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RequestOtherItemListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RequestOtherItemListComponent
  },
  {
    path: "list",
    component: RequestOtherItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RequestOtherItemRoutingModule {}
