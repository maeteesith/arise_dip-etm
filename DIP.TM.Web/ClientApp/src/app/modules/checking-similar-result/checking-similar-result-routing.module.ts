import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CheckingSimilarResultComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: CheckingSimilarResultComponent
  },
  {
    path: "add",
    component: CheckingSimilarResultComponent
  },
  {
    path: "edit/:id",
    component: CheckingSimilarResultComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckingSimilarResultRoutingModule {}
