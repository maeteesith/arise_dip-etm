import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { CheckingSimilarResultRoutingModule } from "./checking-similar-result-routing.module";
import { CheckingSimilarResultComponent } from "../../pages";

@NgModule({
  declarations: [CheckingSimilarResultComponent],
  imports: [...IMPORTS, CheckingSimilarResultRoutingModule],
  providers: PROVIDERS
})
export class CheckingSimilarResultModule {}
