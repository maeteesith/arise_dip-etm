import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole02CaseSummaryComponent,
  AppealRole02CaseSummaryListComponent 
} from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole02CaseSummaryComponent
  },
  {
    path: "list",
    component: AppealRole02CaseSummaryListComponent
  },
  {
    path: "edit/:id",
    component: AppealRole02CaseSummaryComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole02CaseSummaryRoutingModule { }
