import { NgModule } from '@angular/core';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers'
import { AppealRole02CaseSummaryRoutingModule } from './appeal-role02-case-summary-routing.module';
import { 
  AppealRole02CaseSummaryComponent,
  AppealRole02CaseSummaryListComponent 
} from '../../pages';

@NgModule({
  declarations: [AppealRole02CaseSummaryListComponent, AppealRole02CaseSummaryComponent],
  imports: [
    ...IMPORTS,
    AppealRole02CaseSummaryRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole02CaseSummaryModule { }
