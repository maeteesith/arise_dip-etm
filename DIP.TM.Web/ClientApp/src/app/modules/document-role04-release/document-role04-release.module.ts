import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04ReleaseRoutingModule } from "./document-role04-release-routing.module";
import {
  DocumentRole04ReleaseListComponent,
  DocumentRole04ReleaseComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04ReleaseListComponent, 
    DocumentRole04ReleaseComponent
  ],
  imports: [...IMPORTS, DocumentRole04ReleaseRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04ReleaseModule { }
