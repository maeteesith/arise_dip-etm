import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04ReleaseListComponent,
  DocumentRole04ReleaseComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole04ReleaseComponent
  },
  {
    path: "add",
    component: DocumentRole04ReleaseComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole04ReleaseComponent
  },
  {
    path: "list",
    component: DocumentRole04ReleaseListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04ReleaseRoutingModule { }
