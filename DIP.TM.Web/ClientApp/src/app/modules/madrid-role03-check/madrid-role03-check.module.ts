import { ModalMadridRole03PresentHeader } from './../../pages/modal-madrid-role03-present-header/modal-madrid-role03-present-header.component';
import { ModalMadridRole03Holder, MadridRole03MailComponent, MadridRole03OrderComponent, MadridRole03DocumentComponent, MadridRole03ViComponent } from "../../pages";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MadridRole03CheckComponent } from "../../pages";
import { MadridRole03CheckRoutingModule } from './madrid-role03-check-routing.module';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';
import { MadridRole03PresentComponent } from "src/app/pages/madrid-role03-present/madrid-role03-present.component";
import { MadridRole03PaymentComponent } from "src/app/pages/madrid-role03-payment/madrid-role03-payment.component";
import { ModalMadridRole03EditDetail } from "src/app/pages/modal-madrid-role03-edit-detail/modal-madrid-role03-edit-detail.component";


@NgModule({
  declarations: [MadridRole03CheckComponent, ModalMadridRole03Holder, MadridRole03PresentComponent, MadridRole03PaymentComponent, ModalMadridRole03EditDetail, MadridRole03MailComponent
    , MadridRole03OrderComponent, MadridRole03DocumentComponent, MadridRole03ViComponent,ModalMadridRole03PresentHeader],
  imports: [...IMPORTS, MadridRole03CheckRoutingModule],
  entryComponents: [ModalMadridRole03Holder, MadridRole03PresentComponent, MadridRole03PaymentComponent, ModalMadridRole03EditDetail, MadridRole03MailComponent, MadridRole03OrderComponent
    , MadridRole03DocumentComponent, MadridRole03ViComponent,ModalMadridRole03PresentHeader],
  providers: PROVIDERS
})
export class MadridRole03CheckModule { }
