import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole05Release20ListComponent,
  DocumentRole05Release20Component
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole05Release20Component
  },
  {
    path: "add",
    component: DocumentRole05Release20Component
  },
  {
    path: "edit/:id",
    component: DocumentRole05Release20Component
  },
  {
    path: "list",
    component: DocumentRole05Release20ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole05Release20RoutingModule { }
