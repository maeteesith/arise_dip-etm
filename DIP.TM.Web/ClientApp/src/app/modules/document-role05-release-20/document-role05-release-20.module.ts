import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole05Release20RoutingModule } from "./document-role05-release-20-routing.module";
import {
  DocumentRole05Release20ListComponent,
  DocumentRole05Release20Component
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole05Release20ListComponent,
    DocumentRole05Release20Component
  ],
  imports: [...IMPORTS, DocumentRole05Release20RoutingModule],
  providers: PROVIDERS
})
export class DocumentRole05Release20Module { }
