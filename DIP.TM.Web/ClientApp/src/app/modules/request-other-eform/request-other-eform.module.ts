import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestOtherEformRoutingModule } from "./request-other-eform-routing.module";
import {
    RequestOtherEformListComponent,
    RequestOtherEformComponent
} from "../../pages";

@NgModule({
    declarations: [
        RequestOtherEformListComponent,
        RequestOtherEformComponent
    ],
    imports: [...IMPORTS, RequestOtherEformRoutingModule],
    providers: PROVIDERS
})
export class RequestOtherEformModule { }
