import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
    RequestOtherEformListComponent,
    RequestOtherEformComponent
} from "../../pages";

const routes: Routes = [
    {
        path: "",
        component: RequestOtherEformComponent
    },
    {
        path: "add",
        component: RequestOtherEformComponent
    },
    {
        path: "edit/:id",
        component: RequestOtherEformComponent
    },
    {
        path: "list",
        component: RequestOtherEformListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequestOtherEformRoutingModule { }
