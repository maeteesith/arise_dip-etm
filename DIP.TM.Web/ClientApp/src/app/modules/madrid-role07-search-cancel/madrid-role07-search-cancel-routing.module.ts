import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SearchCancelComponent } from "src/app/pages/madrid-role07-search-cancel/madrid-role07-search-cancel.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SearchCancelComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SearchCancelRoutingModule {}
