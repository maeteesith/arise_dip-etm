import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SearchCancelRoutingModule } from "./madrid-role07-search-cancel-routing.module";
import { MadridRole07SearchCancelComponent } from "src/app/pages/madrid-role07-search-cancel/madrid-role07-search-cancel.component";
import { MadridRole07ModalCancelComponent } from "src/app/pages/madrid-role07-modal-cancel/madrid-role07-modal-cancel.component";

@NgModule({
  declarations: [MadridRole07SearchCancelComponent, MadridRole07ModalCancelComponent],
  imports: [...IMPORTS, MadridRole07SearchCancelRoutingModule],
  entryComponents: [MadridRole07SearchCancelComponent, MadridRole07ModalCancelComponent],
  providers: PROVIDERS
})
export class MadridRole07SearchCancelModule {}
