import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole04RespondBookRevokeComponent,
  AppealRole04RespondBookRevokeListComponent
} from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole04RespondBookRevokeComponent
  },
  {
    path: "list",
    component: AppealRole04RespondBookRevokeListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04RespondBookRevokeRoutingModule { }