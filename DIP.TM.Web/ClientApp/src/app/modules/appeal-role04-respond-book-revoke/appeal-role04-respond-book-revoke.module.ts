import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04RespondBookRevokeRoutingModule } from './appeal-role04-respond-book-revoke-routing.module';
import { 
  AppealRole04RespondBookRevokeComponent,
  AppealRole04RespondBookRevokeListComponent
} from '../../pages';

@NgModule({
  declarations: [ 
    AppealRole04RespondBookRevokeComponent, 
    AppealRole04RespondBookRevokeListComponent ],
  imports: [
    ...IMPORTS,
    AppealRole04RespondBookRevokeRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04RespondBookRevokeModule { }
