import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole02ExaminationRoutingModule } from './madrid-role02-examination-routing.module';
import { MadridRole02ExaminationComponent } from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [MadridRole02ExaminationComponent],
  imports: [...IMPORTS, MadridRole02ExaminationRoutingModule],
  entryComponents:[MadridRole02ExaminationComponent],
  providers: PROVIDERS
})

export class MadridRole02ExaminationModule { }
