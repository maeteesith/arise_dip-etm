import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole02ExaminationComponent } from 'src/app/pages/madrid-role02-examination/madrid-role02-examination.component';


const routes: Routes = [ {
  path: "",
  component:MadridRole02ExaminationComponent
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02ExaminationRoutingModule { }
