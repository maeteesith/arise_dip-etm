import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole01AppealKorAllListRoutingModule } from "./appeal-role01-appeal-kor-all-list-routing.module";
import { 
  AppealRole01AppealKorAllListComponent,
 } from "../../pages";
@NgModule({
  declarations: [
    AppealRole01AppealKorAllListComponent
  ],
  imports: [...IMPORTS, AppealRole01AppealKorAllListRoutingModule],
  providers: PROVIDERS
})

export class AppealRole01AppealKorAllListModule { }
