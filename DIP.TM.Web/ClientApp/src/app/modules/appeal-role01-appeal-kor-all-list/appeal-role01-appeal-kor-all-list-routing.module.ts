import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { 
  AppealRole01AppealKorAllListComponent
 } from "../../pages";
const routes: Routes = [
  {
    path: "",
    component: AppealRole01AppealKorAllListComponent
  },
  {
    path: "list",
    component: AppealRole01AppealKorAllListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole01AppealKorAllListRoutingModule { }
