import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Mm07Component } from 'src/app/pages/mm07/mm07.component';


const routes: Routes = [
  {
    path: "",
    component: Mm07Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Mm07RoutingModule { }
