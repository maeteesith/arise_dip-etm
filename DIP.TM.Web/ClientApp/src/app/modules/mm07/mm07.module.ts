import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Mm07RoutingModule } from './mm07-routing.module';
import { Mm07Component } from 'src/app/pages/mm07/mm07.component';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';


@NgModule({
  declarations: [Mm07Component],
  imports: [...IMPORTS, Mm07RoutingModule],
    entryComponents: [Mm07Component],
    providers: PROVIDERS
})
export class Mm07Module { }
