import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CheckingItemListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: CheckingItemListComponent
  },
  {
    path: "list",
    component: CheckingItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckingItemListRoutingModule {}
