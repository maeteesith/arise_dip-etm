import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { CheckingItemListRoutingModule } from "./checking-item-routing.module";
import { CheckingItemListComponent } from "../../pages";

@NgModule({
  declarations: [CheckingItemListComponent],
  imports: [...IMPORTS, CheckingItemListRoutingModule],
  providers: PROVIDERS
})
export class CheckingItemListModule {}
