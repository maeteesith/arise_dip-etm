import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  ConsiderLicenseRequestComponent,
  ConsiderLicenseRequestListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: ConsiderLicenseRequestComponent,
  },
  {
    path: "add",
    component: ConsiderLicenseRequestComponent,
  },
  {
    path: "edit/:id",
    component: ConsiderLicenseRequestComponent,
  },
  {
    path: "list",
    component: ConsiderLicenseRequestListComponent,
  },
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ConsiderLicenseRequestRoutingModule {}