import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsiderLicenseRequestRoutingModule } from './consider-license-request-routing.module';
import {
  ConsiderLicenseRequestComponent,
  ConsiderLicenseRequestListComponent
} from "../../pages";

@NgModule({
declarations: [
  ConsiderLicenseRequestComponent,
  ConsiderLicenseRequestListComponent
],
imports: [...IMPORTS, ConsiderLicenseRequestRoutingModule],
providers: PROVIDERS
})
export class ConsiderLicenseRequestModule { }