import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocumentRole02PrintDocumentListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole02PrintDocumentListComponent
  },
  {
    path: "list",
    component: DocumentRole02PrintDocumentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole02PrintDocumentRoutingModule {}
