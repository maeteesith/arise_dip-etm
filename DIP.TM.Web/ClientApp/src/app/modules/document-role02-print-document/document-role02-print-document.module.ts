import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole02PrintDocumentRoutingModule } from "./document-role02-print-document-routing.module";
import { DocumentRole02PrintDocumentListComponent } from "../../pages";

@NgModule({
  declarations: [DocumentRole02PrintDocumentListComponent],
  imports: [...IMPORTS, DocumentRole02PrintDocumentRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole02PrintDocumentModule {}
