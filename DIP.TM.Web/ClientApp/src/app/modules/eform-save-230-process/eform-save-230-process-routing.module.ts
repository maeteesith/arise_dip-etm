import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { eFormSave230ProcessComponent } from "../../pages";
import { CanDeactivateGuard } from "../../can-deactivate-guard.service";

const routes: Routes = [
  {
    path: "",
    component: eFormSave230ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: "add",
    component: eFormSave230ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: "edit/:id",
    component: eFormSave230ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class eFormSave230ProcessRoutingModule {}
