import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave230ProcessRoutingModule } from "./eform-save-230-process-routing.module";
import { eFormSave230ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave230ProcessComponent],
  imports: [...IMPORTS, eFormSave230ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave230ProcessModule {}
