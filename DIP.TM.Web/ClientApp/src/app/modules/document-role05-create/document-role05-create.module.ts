import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole05CreateRoutingModule } from "./document-role05-create-routing.module";
import {
  DocumentRole05CreateListComponent,
  DocumentRole05CreateComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole05CreateListComponent, 
    DocumentRole05CreateComponent
  ],
  imports: [...IMPORTS, DocumentRole05CreateRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole05CreateModule { }
