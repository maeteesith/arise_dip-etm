import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole05CreateListComponent,
  DocumentRole05CreateComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole05CreateComponent
  },
  {
    path: "add",
    component: DocumentRole05CreateComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole05CreateComponent
  },
  {
   path: "list",
   component: DocumentRole05CreateListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole05CreateRoutingModule {}
