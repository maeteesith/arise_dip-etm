import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave01ProcessRoutingModule } from "./eform-save-01-process-routing.module";
import { eFormSave01ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave01ProcessComponent],
  imports: [...IMPORTS, eFormSave01ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave01ProcessModule {}
