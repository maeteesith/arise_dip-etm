import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {  Mm02Component } from 'src/app/pages/mm02/mm02.component';


const routes: Routes = [
  {
    path: "",
    component: Mm02Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Mm02RoutingModule { }
