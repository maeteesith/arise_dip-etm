import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IMPORTS } from '../imports';
import { RouterModule } from '@angular/router';
import { PROVIDERS } from '../providers';
import { Mm02Component } from 'src/app/pages/mm02/mm02.component';
import { Mm02RoutingModule } from './mm02-routing.module';
import { Mm02_01_4_modalComponent } from 'src/app/pages/mm02_01-4-modal/mm02_01-4-modal.component';
import { Mm02_01_2_modalComponent } from 'src/app/pages/mm02_01-2-modal/mm02_01-2-modal.component';



@NgModule({
    declarations: [Mm02Component,Mm02_01_4_modalComponent,Mm02_01_2_modalComponent],
    imports: [...IMPORTS, Mm02RoutingModule],
    entryComponents:[Mm02_01_4_modalComponent,Mm02_01_2_modalComponent],
    providers: PROVIDERS
})
export class Mm02Module { }
