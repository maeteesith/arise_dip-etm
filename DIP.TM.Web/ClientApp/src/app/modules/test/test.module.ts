import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { TestRoutingModule } from "./test-routing.module";
import { TestLoginComponent, TestCkeditorComponent } from "../../pages";

@NgModule({
  declarations: [TestLoginComponent, TestCkeditorComponent],
  imports: [...IMPORTS, TestRoutingModule],
  providers: PROVIDERS
})
export class TestModule {}
