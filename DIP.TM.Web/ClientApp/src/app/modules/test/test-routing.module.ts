import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { TestLoginComponent, TestCkeditorComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: TestLoginComponent
  },
  {
    path: "login",
    component: TestLoginComponent
  },
  {
    path: "ckeditor",
    component: TestCkeditorComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TestRoutingModule {}
