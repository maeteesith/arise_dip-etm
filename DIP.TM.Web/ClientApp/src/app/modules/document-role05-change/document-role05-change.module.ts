import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole05ChangeRoutingModule } from "./document-role05-change-routing.module";
import {
  DocumentRole05ChangeListComponent,
  //DocumentRole05ChangeComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole05ChangeListComponent, 
    //DocumentRole05ChangeComponent
  ],
  imports: [...IMPORTS, DocumentRole05ChangeRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole05ChangeModule { }
