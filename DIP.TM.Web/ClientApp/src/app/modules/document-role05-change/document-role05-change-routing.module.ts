import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole05ChangeListComponent,
  //DocumentRole05ChangeComponent
} from "../../pages";

const routes: Routes = [
  {
    //  path: "",
    //  component: DocumentRole05ChangeComponent
    //},
    //{
    //  path: "add",
    //  component: DocumentRole05ChangeComponent
    //},
    //{
    //  path: "edit/:id",
    //  component: DocumentRole05ChangeComponent
    //},
    //{
    path: "list",
    component: DocumentRole05ChangeListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole05ChangeRoutingModule { }
