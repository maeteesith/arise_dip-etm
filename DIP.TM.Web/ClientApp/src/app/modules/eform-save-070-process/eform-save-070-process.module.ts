import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave070ProcessRoutingModule } from "./eform-save-070-process-routing.module";
import { eFormSave070ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave070ProcessComponent],
  imports: [...IMPORTS, eFormSave070ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave070ProcessModule {}
