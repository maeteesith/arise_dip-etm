import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CheckingSimilarWordComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: CheckingSimilarWordComponent
  },
  {
    path: "add",
    component: CheckingSimilarWordComponent
  },
  {
    path: "edit/:id",
    component: CheckingSimilarWordComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckingSimilarWordRoutingModule {}
