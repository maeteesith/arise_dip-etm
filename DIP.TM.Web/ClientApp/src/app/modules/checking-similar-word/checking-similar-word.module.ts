import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { CheckingSimilarWordRoutingModule } from "./checking-similar-word-routing.module";
import { CheckingSimilarWordComponent } from "../../pages";

@NgModule({
  declarations: [CheckingSimilarWordComponent],
  imports: [...IMPORTS, CheckingSimilarWordRoutingModule],
  providers: PROVIDERS
})
export class CheckingSimilarWordModule {}
