import { CanDeactivateGuard } from "../can-deactivate-guard.service";
import { DateAdapter } from "@angular/material/core";
import {
  MomentDateAdapter,
  MAT_MOMENT_DATE_ADAPTER_OPTIONS
} from "@angular/material-moment-adapter";
import { MAT_DATE_LOCALE, MAT_DATE_FORMATS } from "@angular/material";

export const PROVIDERS = [
  CanDeactivateGuard,
  {
    provide: DateAdapter,
    useClass: MomentDateAdapter,
    deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
  },
  {
    provide: MAT_DATE_FORMATS,
    useValue: {
      parse: {
        dateInput: ["l", "LL"]
      },
      display: {
        dateInput: "L",
        monthYearLabel: "MMMM YYYY",
        dateA11yLabel: "LL",
        monthYearA11yLabel: "MMMM YYYY"
      }
    }
  },
  {
    provide: MAT_DATE_LOCALE,
    useValue: "th"
  }
];
