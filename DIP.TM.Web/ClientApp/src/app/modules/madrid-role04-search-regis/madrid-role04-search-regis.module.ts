import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole04SearchRegisRoutingModule } from './madrid-role04-search-regis-routing.module';
import {MadridRole04SearchRegisComponent} from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [
    MadridRole04SearchRegisComponent
  ],
  imports: [...IMPORTS, MadridRole04SearchRegisRoutingModule],
  providers: PROVIDERS
})
export class MadridRole04SearchRegisModule { }
