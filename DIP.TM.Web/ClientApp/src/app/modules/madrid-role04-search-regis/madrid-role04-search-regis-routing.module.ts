import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MadridRole04SearchRegisComponent} from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole04SearchRegisComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04SearchRegisRoutingModule { }
