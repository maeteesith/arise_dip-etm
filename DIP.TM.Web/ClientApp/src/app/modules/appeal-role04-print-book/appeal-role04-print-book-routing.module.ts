import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole04PrintBookListComponent } from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole04PrintBookListComponent
  },
  {
    path: "list",
    component: AppealRole04PrintBookListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04PrintBookRoutingModule { }
