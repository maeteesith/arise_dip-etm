import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04PrintBookRoutingModule } from './appeal-role04-print-book-routing.module';
import { AppealRole04PrintBookListComponent } from '../../pages';


@NgModule({
  declarations: [ AppealRole04PrintBookListComponent ],
  imports: [
    ...IMPORTS,
    AppealRole04PrintBookRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04PrintBookModule { }
