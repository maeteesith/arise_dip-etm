import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { IssueImportantDocumentsRoutingModule } from './issue-important-documents-routing.module';
import {
  IssueImportantDocumentsComponent,
  IssueImportantDocumentsListComponent
} from "../../pages";
 
@NgModule({
declarations: [
  IssueImportantDocumentsComponent,
  IssueImportantDocumentsListComponent
],
imports: [...IMPORTS, IssueImportantDocumentsRoutingModule],
providers: PROVIDERS
})

export class IssueImportantDocumentsModule { }
