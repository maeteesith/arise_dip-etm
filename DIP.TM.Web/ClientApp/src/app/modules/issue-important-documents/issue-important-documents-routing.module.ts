import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  IssueImportantDocumentsComponent,
  IssueImportantDocumentsListComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: IssueImportantDocumentsComponent,
},
{
path: "add",
component: IssueImportantDocumentsComponent,
},
{
path: "edit/:id",
component: IssueImportantDocumentsComponent,
},
{
path: "list",
component: IssueImportantDocumentsListComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class IssueImportantDocumentsRoutingModule {}
