import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicDashboardRole01RoutingModule } from "./public-dashboard-role-01-routing.module";
import { PublicDashboardRole01Component } from "../../pages";

@NgModule({
  declarations: [PublicDashboardRole01Component],
  imports: [...IMPORTS, PublicDashboardRole01RoutingModule],
  providers: PROVIDERS
})
export class PublicDashboardRole01Module {}
