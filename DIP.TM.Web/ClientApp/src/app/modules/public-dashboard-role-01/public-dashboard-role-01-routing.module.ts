import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicDashboardRole01Component } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicDashboardRole01Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicDashboardRole01RoutingModule {}
