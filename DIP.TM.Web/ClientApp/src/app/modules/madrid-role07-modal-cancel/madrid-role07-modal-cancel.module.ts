import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07CreateDocModalComponent } from "../../pages/madrid-role07-create-doc-modal/madrid-role07-create-doc-modal.component";
import { MatDialogModule } from "@angular/material";
import { MadridRole07ModalCancelRoutingModule } from "./madrid-role07-modal-cancel-routing.module";
import { MadridRole07ModalCancelComponent } from "src/app/pages/madrid-role07-modal-cancel/madrid-role07-modal-cancel.component";

@NgModule({
  declarations: [MadridRole07ModalCancelComponent],
  imports: [...IMPORTS, MadridRole07ModalCancelRoutingModule, MatDialogModule],
  entryComponents:[MadridRole07ModalCancelComponent],
  providers: PROVIDERS
})
export class MadridRole07ModalCheckModule {}
