import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07ModalCancelComponent } from "src/app/pages/madrid-role07-modal-cancel/madrid-role07-modal-cancel.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07ModalCancelComponent,
  },
  // {
  //   path: "modal",
  //   component: MadridRole07CreateDocModalComponent
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07ModalCancelRoutingModule {}
