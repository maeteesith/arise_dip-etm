import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole01Case41CheckRoutingModule } from "./public-role01-case41-check-routing.module";
import {
  PublicRole01Case41CheckListComponent,
  //PublicRole01Case41CheckComponent
} from "../../pages";

@NgModule({
  declarations: [
    PublicRole01Case41CheckListComponent,
    //PublicRole01Case41CheckComponent
  ],
  imports: [...IMPORTS, PublicRole01Case41CheckRoutingModule],
  providers: PROVIDERS
})
export class PublicRole01Case41CheckModule { }
