import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  PublicRole01Case41CheckListComponent,
  //PublicRole01Case41CheckComponent
} from "../../pages";

const routes: Routes = [
  //{
  //  path: "",
  //  component: PublicRole01Case41CheckComponent
  //},
  //{
  //  path: "add",
  //  component: PublicRole01Case41CheckComponent
  //},
  //{
  //  path: "edit/:id",
  //  component: PublicRole01Case41CheckComponent
  //},
  {
    path: "list",
    component: PublicRole01Case41CheckListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole01Case41CheckRoutingModule { }
