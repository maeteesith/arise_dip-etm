import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole03BoardDecisionSummaryListComponent } from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole03BoardDecisionSummaryListComponent
  },
  {
    path: "list",
    component: AppealRole03BoardDecisionSummaryListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole03BoardDecisionSummaryRoutingModule { }
