import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole03BoardDecisionSummaryRoutingModule } from './appeal-role03-board-decision-summary-routing.module';
import { AppealRole03BoardDecisionSummaryListComponent } from '../../pages';


@NgModule({
  declarations: [AppealRole03BoardDecisionSummaryListComponent],
  imports: [
    ...IMPORTS,
    AppealRole03BoardDecisionSummaryRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole03BoardDecisionSummaryModule { }
