import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsiderSection55P2RoutingModule } from './consider-section55-p2-routing.module';
import {
  ConsiderSection55P2Component,
  ConsiderSection55P2ListComponent
} from "../../pages";

@NgModule({
declarations: [
  ConsiderSection55P2Component,
  ConsiderSection55P2ListComponent
],
imports: [...IMPORTS, ConsiderSection55P2RoutingModule],
providers: PROVIDERS
})
export class ConsiderSection55P2Module { }
