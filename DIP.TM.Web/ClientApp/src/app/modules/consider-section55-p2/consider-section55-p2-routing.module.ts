import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  ConsiderSection55P2Component,
  ConsiderSection55P2ListComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: ConsiderSection55P2Component,
},
{
path: "add",
component: ConsiderSection55P2Component,
},
{
path: "edit/:id",
component: ConsiderSection55P2Component,
},
{
path: "list",
component: ConsiderSection55P2ListComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ConsiderSection55P2RoutingModule {}