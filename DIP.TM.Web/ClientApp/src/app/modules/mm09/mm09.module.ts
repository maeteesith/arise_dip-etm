import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Mm09RoutingModule } from './mm09-routing.module';
import { Mm06Component } from 'src/app/pages/mm06/mm06.component';
import { IMPORTS } from '../imports';
import { RouterModule } from '@angular/router';
import { PROVIDERS } from '../providers';
import { Mm09Component } from 'src/app/pages/mm09/mm09.component';



@NgModule({
    declarations: [Mm09Component],
    imports: [...IMPORTS, Mm09RoutingModule],
    entryComponents: [Mm09Component],
    providers: PROVIDERS
})
export class Mm09Module { }
