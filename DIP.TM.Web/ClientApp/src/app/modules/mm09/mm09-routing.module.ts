import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ArchwizardModule } from 'angular-archwizard';
import { Mm09Component } from 'src/app/pages/mm09/mm09.component';


const routes: Routes = [
  {
    path: "",
    component: Mm09Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), ArchwizardModule],
  exports: [RouterModule]
})
export class Mm09RoutingModule { }
