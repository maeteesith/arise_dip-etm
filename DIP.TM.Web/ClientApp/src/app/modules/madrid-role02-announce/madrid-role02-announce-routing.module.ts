import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02AnnounceListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02AnnounceListComponent
  },
  //{
  //  path: "list",
  //  component: MadridRole02AnnounceListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02AnnounceRoutingModule {}
