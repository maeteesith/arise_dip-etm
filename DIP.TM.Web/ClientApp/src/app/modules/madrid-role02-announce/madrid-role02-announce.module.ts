import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02AnnounceRoutingModule } from "./madrid-role02-announce-routing.module";
import { MadridRole02AnnounceListComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole02AnnounceListComponent],
  imports: [...IMPORTS, MadridRole02AnnounceRoutingModule],
  providers: PROVIDERS
})
export class MadridRole02AnnounceModule {}
