import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole03MeetingSummaryRoutingModule } from './appeal-role03-meeting-summary-routing.module';
import { 
  AppealRole03MeetingSummaryComponent,
  AppealRole03MeetingSummaryListComponent
} from '../../pages';


@NgModule({
  declarations: [ AppealRole03MeetingSummaryListComponent, AppealRole03MeetingSummaryComponent ],
  imports: [
    ...IMPORTS,
    AppealRole03MeetingSummaryRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole03MeetingSummaryModule { }
