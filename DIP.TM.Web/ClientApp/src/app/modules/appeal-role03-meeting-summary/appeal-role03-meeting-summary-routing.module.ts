import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  AppealRole03MeetingSummaryComponent,
  AppealRole03MeetingSummaryListComponent
  } from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole03MeetingSummaryComponent
  },
  {
    path: "list",
    component: AppealRole03MeetingSummaryListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole03MeetingSummaryRoutingModule { }