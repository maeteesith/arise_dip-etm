import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save140ProcessComponent,
  Save140ProcessListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save140ProcessComponent
  },
  {
    path: "add",
    component: Save140ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save140ProcessComponent
  },
  {
    path: "list",
    component: Save140ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save140ProcessRoutingModule {}
