import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save140ProcessRoutingModule } from "./save-140-process-routing.module";
import {
  Save140ProcessComponent,
  Save140ProcessListComponent
} from "../../pages";

@NgModule({
  declarations: [Save140ProcessComponent, Save140ProcessListComponent],
  imports: [...IMPORTS, Save140ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save140ProcessModule {}
