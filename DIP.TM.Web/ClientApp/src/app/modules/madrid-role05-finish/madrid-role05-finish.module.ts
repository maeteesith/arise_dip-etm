import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole05FinishComponent,MadridRole05ModalComponent, MadridRole05DocAttachComponent,MadridRole05PaymentComponent,MadridRole05FeeComponent,MadridRole05RequestInComponent } from "../../pages";
import { MatTabsModule } from '@angular/material/tabs'
import { ConfirmationDialogComponent } from '../../shared';

import { MadridRole05FinishRoutingModule } from './madrid-role05-finish-routing.module';


@NgModule({
  declarations: [MadridRole05FinishComponent],
  imports: [...IMPORTS, MadridRole05FinishRoutingModule,MatTabsModule],
  providers: PROVIDERS
})
export class MadridRole05FinishModule { }
