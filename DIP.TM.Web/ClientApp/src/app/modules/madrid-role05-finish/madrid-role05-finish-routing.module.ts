import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole05SearchComponent,MadridRole05RequestInComponent,MadridRole05FinishComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole05FinishComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole05FinishRoutingModule { }
