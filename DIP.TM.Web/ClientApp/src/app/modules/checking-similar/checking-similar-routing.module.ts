import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  CheckingSimilarComponent,
  CheckingSimilarListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: CheckingSimilarComponent
  },
  {
    path: "add",
    component: CheckingSimilarComponent
  },
  {
    path: "edit/:id",
    component: CheckingSimilarComponent
  },
  {
    path: "list",
    component: CheckingSimilarListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckingSimilarListRoutingModule {}
