import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { CheckingSimilarListRoutingModule } from "./checking-similar-routing.module";
import {
  CheckingSimilarComponent,
  CheckingSimilarListComponent
} from "../../pages";

@NgModule({
  declarations: [CheckingSimilarComponent, CheckingSimilarListComponent],
  imports: [...IMPORTS, CheckingSimilarListRoutingModule],
  providers: PROVIDERS
})
export class CheckingSimilarListModule {}
