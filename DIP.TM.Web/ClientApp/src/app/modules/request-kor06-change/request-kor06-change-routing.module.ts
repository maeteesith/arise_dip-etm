import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RequestKor06ChangeComponent } from "../../pages";

const routes: Routes = [
{
path: "",
component: RequestKor06ChangeComponent,
},
{
path: "add",
component: RequestKor06ChangeComponent,
},
{
path: "edit/:id",
component: RequestKor06ChangeComponent,
},
{
path: "list",
component: RequestKor06ChangeComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor06ChangeRoutingModule {}
