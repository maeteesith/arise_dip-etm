import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor06ChangeRoutingModule } from "./request-kor06-change-routing.module";
import {
  RequestKor06ChangeComponent
} from "../../pages";

@NgModule({
declarations: [RequestKor06ChangeComponent],
imports: [...IMPORTS, RequestKor06ChangeRoutingModule],
providers: PROVIDERS
})
export class RequestKor06ChangeModule { }