import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06Ni04ListComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole06Ni04ListComponent
  },
  {
    path: "list",
    component: MadridRole06Ni04ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06Ni04RoutingModule { }
