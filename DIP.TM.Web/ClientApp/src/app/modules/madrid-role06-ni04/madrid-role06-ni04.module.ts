import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole06Ni04RoutingModule } from './madrid-role06-ni04-routing.module';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06Ni04ListComponent } from "../../pages";
import { ModalMadridRole06LicenseComponent } from 'src/app/pages/modal-madrid-role06-license/modal-madrid-role06-license.component';

@NgModule({
  declarations: [MadridRole06Ni04ListComponent, ModalMadridRole06LicenseComponent],
  imports: [...IMPORTS, MadridRole06Ni04RoutingModule],
  entryComponents: [ModalMadridRole06LicenseComponent],
  providers: PROVIDERS
})
export class MadridRole06Ni04Module { }
