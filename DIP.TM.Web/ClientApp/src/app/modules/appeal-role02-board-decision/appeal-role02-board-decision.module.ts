import { NgModule } from '@angular/core';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers'
import { AppealRole02BoardDecisionRoutingModule } from './appeal-role02-board-decision-routing.module';
import { 
  AppealRole02BoardDecisionComponent,
  AppealRole02BoardDecisionListComponent 
} from '../../pages';


@NgModule({
  declarations: [AppealRole02BoardDecisionListComponent, AppealRole02BoardDecisionComponent],
  imports: [
    ...IMPORTS,
    AppealRole02BoardDecisionRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole02BoardDecisionModule { }
