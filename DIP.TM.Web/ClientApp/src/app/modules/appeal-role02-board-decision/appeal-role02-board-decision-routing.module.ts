import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole02BoardDecisionComponent,
  AppealRole02BoardDecisionListComponent
 } from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole02BoardDecisionComponent
  },
  {
    path: "list",
    component: AppealRole02BoardDecisionListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole02BoardDecisionRoutingModule { }
