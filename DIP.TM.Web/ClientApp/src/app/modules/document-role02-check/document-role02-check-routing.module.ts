import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole02CheckListComponent,
  DocumentRole02CheckComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole02CheckComponent
  },
  {
    path: "add",
    component: DocumentRole02CheckComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole02CheckComponent
  },
  {
    path: "list",
    component: DocumentRole02CheckListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole02CheckRoutingModule {}
