import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole02CheckRoutingModule } from "./document-role02-check-routing.module";
import {
  DocumentRole02CheckListComponent,
  DocumentRole02CheckComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole02CheckListComponent,
    DocumentRole02CheckComponent
  ],
  imports: [...IMPORTS, DocumentRole02CheckRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole02CheckModule {}
