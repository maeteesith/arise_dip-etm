import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04ItemRoutingModule } from "./document-role04-item-routing.module";
import {
  DocumentRole04ItemListComponent,
  //DocumentRole04ItemComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04ItemListComponent, 
    //DocumentRole04ItemComponent
  ],
  imports: [...IMPORTS, DocumentRole04ItemRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04ItemModule { }
