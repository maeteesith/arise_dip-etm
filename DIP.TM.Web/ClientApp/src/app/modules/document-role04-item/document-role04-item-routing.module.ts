import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04ItemListComponent,
  //DocumentRole04ItemComponent
} from "../../pages";

const routes: Routes = [
  //{
  //  path: "",
  //  component: DocumentRole04ItemComponent
  //},
  //{
  //  path: "add",
  //  component: DocumentRole04ItemComponent
  //},
  //{
  //  path: "edit/:id",
  //  component: DocumentRole04ItemComponent
  //},
  {
    path: "list",
    component: DocumentRole04ItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04ItemRoutingModule { }
