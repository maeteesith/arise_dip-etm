import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave140ProcessRoutingModule } from "./eform-save-140-process-routing.module";
import { eFormSave140ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave140ProcessComponent],
  imports: [...IMPORTS, eFormSave140ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave140ProcessModule {}
