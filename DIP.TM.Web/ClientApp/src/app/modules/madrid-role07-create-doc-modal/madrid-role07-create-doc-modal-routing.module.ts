import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07CreateDocModalComponent } from "src/app/pages/madrid-role07-create-doc-modal/madrid-role07-create-doc-modal.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07CreateDocModalComponent
  },
  // {
  //   path: "modal",
  //   component: MadridRole07CreateDocModalComponent
  // },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07CreateDocModalRoutingModule {}
