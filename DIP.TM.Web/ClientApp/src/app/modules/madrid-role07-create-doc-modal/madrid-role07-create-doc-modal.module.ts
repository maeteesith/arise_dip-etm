import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07CreateDocModalRoutingModule } from "./madrid-role07-create-doc-modal-routing.module";
import { MadridRole07CreateDocModalComponent } from "../../pages/madrid-role07-create-doc-modal/madrid-role07-create-doc-modal.component";
import { MatDialogModule } from "@angular/material";

@NgModule({
  declarations: [MadridRole07CreateDocModalComponent],
  imports: [...IMPORTS, MadridRole07CreateDocModalRoutingModule, MatDialogModule],
  entryComponents:[MadridRole07CreateDocModalComponent],
  providers: PROVIDERS
})
export class MadridRole07SaveListModule {}
