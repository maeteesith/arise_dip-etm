import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { CheckingSimilarCompareRoutingModule } from "./checking-similar-compare-routing.module";
import {
  CheckingSimilarCompareListComponent,
  CheckingSimilarCompareComponent
} from "../../pages";

@NgModule({
  declarations: [
    //CheckingSimilarCompareListComponent,
    CheckingSimilarCompareComponent],
  imports: [...IMPORTS, CheckingSimilarCompareRoutingModule],
  providers: PROVIDERS
})
export class CheckingSimilarCompareModule { }
