import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //CheckingSimilarCompareListComponent,
  CheckingSimilarCompareComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: CheckingSimilarCompareComponent
  },
  {
    path: "add",
    component: CheckingSimilarCompareComponent
  },
  {
    path: "edit/:id",
    component: CheckingSimilarCompareComponent
  //},
  //{
  //  path: "list",
  //  component: CheckingSimilarCompareListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CheckingSimilarCompareRoutingModule { }
