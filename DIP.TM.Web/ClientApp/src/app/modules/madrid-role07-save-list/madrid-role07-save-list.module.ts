import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SaveListRoutingModule } from "./madrid-role07-save-list-routing.module";
import { MadridRole07SaveListComponent } from "../../pages";
import { MadridRole07CreateDocModalComponent } from "../../pages";
import { MadridRole07ModalCheckComponent } from "src/app/pages/madrid-role07-modal-check/madrid-role07-modal-check.component";
import { MadridRole07ModalSaveComponent } from "src/app/pages/madrid-role07-modal-save/madrid-role07-modal-save.component";

@NgModule({
  declarations: [MadridRole07SaveListComponent, MadridRole07CreateDocModalComponent, MadridRole07ModalCheckComponent,
    MadridRole07ModalSaveComponent],
  imports: [...IMPORTS, MadridRole07SaveListRoutingModule],
  entryComponents: [MadridRole07CreateDocModalComponent, MadridRole07ModalCheckComponent, MadridRole07ModalSaveComponent],
  providers: PROVIDERS
})
export class MadridRole07SaveListModule {}
