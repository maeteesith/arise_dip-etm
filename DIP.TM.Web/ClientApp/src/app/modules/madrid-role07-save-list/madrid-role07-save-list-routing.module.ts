import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SaveListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SaveListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SaveListRoutingModule {}
