import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole02LimitationsModalRoutingModule } from './madrid-role02-limitations-modal-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MadridRole02LimitationsModalRoutingModule
  ]
})
//MR 2-1
export class MadridRole02LimitationsModalModule { }
