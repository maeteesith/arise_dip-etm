import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RequestDocumentCollectComponent } from "../../pages";

const routes: Routes = [
    {
        path: "",
        component: RequestDocumentCollectComponent
    },
    {
        path: "add",
        component: RequestDocumentCollectComponent
    },
    {
        path: "edit/:id",
        component: RequestDocumentCollectComponent
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequestDocumentCollectRoutingModule { }
