import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestDocumentCollectRoutingModule } from "./request-document-collect-routing.module";
import { RequestDocumentCollectComponent } from "../../pages";

@NgModule({
  declarations: [RequestDocumentCollectComponent],
  imports: [...IMPORTS, RequestDocumentCollectRoutingModule],
  providers: PROVIDERS
})
export class RequestDocumentCollectModule {}
