import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ApproveKor20DocumentComponent } from "../../pages";

const routes: Routes = [
{
path: "",
component: ApproveKor20DocumentComponent,
},
{
path: "add",
component: ApproveKor20DocumentComponent,
},
{
path: "edit/:id",
component: ApproveKor20DocumentComponent,
},
{
path: "list",
component: ApproveKor20DocumentComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ApproveKor20DocumentRoutingModule {}