import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ApproveKor20DocumentRoutingModule } from './approve-kor20-document-routing.module';
import {
  ApproveKor20DocumentComponent
} from "../../pages";

@NgModule({
declarations: [ApproveKor20DocumentComponent],
imports: [...IMPORTS, ApproveKor20DocumentRoutingModule],
providers: PROVIDERS
})
export class ApproveKor20DocumentModule { }