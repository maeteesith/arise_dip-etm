import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave120ProcessRoutingModule } from "./eform-save-120-process-routing.module";
import { eFormSave120ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave120ProcessComponent],
  imports: [...IMPORTS, eFormSave120ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave120ProcessModule {}
