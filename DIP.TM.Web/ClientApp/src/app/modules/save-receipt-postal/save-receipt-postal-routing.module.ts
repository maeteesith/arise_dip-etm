import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  SaveReceiptPostalComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: SaveReceiptPostalComponent,
},
{
path: "add",
component: SaveReceiptPostalComponent,
},
{
path: "edit/:id",
component: SaveReceiptPostalComponent,
},
{
path: "list",
component: SaveReceiptPostalComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class SaveReceiptPostalRoutingModule {}