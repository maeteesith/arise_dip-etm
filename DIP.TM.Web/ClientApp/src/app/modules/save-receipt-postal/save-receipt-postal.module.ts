import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { SaveReceiptPostalRoutingModule } from './save-receipt-postal-routing.module';
import {
  SaveReceiptPostalComponent
} from "../../pages";
 
@NgModule({
declarations: [
  SaveReceiptPostalComponent
],
imports: [...IMPORTS, SaveReceiptPostalRoutingModule],
providers: PROVIDERS
})
export class SaveReceiptPostalModule { }