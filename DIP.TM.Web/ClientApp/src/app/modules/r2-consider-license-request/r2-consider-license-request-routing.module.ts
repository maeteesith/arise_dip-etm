import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  R2ConsiderLicenseRequestComponent,
  R2ConsiderLicenseRequestListComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: R2ConsiderLicenseRequestComponent,
},
{
path: "add",
component: R2ConsiderLicenseRequestComponent,
},
{
path: "edit/:id",
component: R2ConsiderLicenseRequestComponent,
},
{
path: "list",
component: R2ConsiderLicenseRequestListComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class R2ConsiderLicenseRequestRoutingModule { }
