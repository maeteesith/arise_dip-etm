import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { R2ConsiderLicenseRequestRoutingModule } from './r2-consider-license-request-routing.module';
import {
  R2ConsiderLicenseRequestComponent,
  R2ConsiderLicenseRequestListComponent
} from "../../pages";

@NgModule({
declarations: [
  R2ConsiderLicenseRequestComponent,
  R2ConsiderLicenseRequestListComponent
],
imports: [...IMPORTS, R2ConsiderLicenseRequestRoutingModule],
providers: PROVIDERS
})
export class R2ConsiderLicenseRequestModule { }