import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole05CheckRoutingModule } from "./document-role05-check-routing.module";
import {
    DocumentRole05CheckListComponent,
    DocumentRole05CheckComponent
} from "../../pages";

@NgModule({
    declarations: [
        DocumentRole05CheckListComponent,
        DocumentRole05CheckComponent
    ],
    imports: [...IMPORTS, DocumentRole05CheckRoutingModule],
    providers: PROVIDERS
})
export class DocumentRole05CheckModule { }
