import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole05CheckListComponent,
  DocumentRole05CheckComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole05CheckComponent
  },
  {
    path: "add",
    component: DocumentRole05CheckComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole05CheckComponent
  },
  {
    path: "list",
    component: DocumentRole05CheckListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole05CheckRoutingModule { }
