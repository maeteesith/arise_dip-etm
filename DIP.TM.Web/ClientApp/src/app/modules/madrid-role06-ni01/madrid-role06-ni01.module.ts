import { NgModule } from '@angular/core';
import { MadridRole06Ni01RoutingModule } from './madrid-role06-ni01-routing.module';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06Ni01ListComponent } from "../../pages";


@NgModule({
  declarations: [MadridRole06Ni01ListComponent],
  imports: [...IMPORTS, MadridRole06Ni01RoutingModule],
  providers: PROVIDERS
})

export class MadridRole06Ni01Module { }
