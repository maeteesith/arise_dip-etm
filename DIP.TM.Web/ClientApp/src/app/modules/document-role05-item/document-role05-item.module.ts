import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole05ItemRoutingModule } from "./document-role05-item-routing.module";
import {
    DocumentRole05ItemListComponent,
    //DocumentRole05ItemComponent
} from "../../pages";

@NgModule({
    declarations: [
        DocumentRole05ItemListComponent,
        //DocumentRole05ItemComponent
    ],
    imports: [...IMPORTS, DocumentRole05ItemRoutingModule],
    providers: PROVIDERS
})
export class DocumentRole05ItemModule { }
