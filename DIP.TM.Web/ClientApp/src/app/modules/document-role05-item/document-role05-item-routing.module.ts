import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole05ItemListComponent,
  //DocumentRole05ItemComponent
} from "../../pages";

const routes: Routes = [
  {
    //  path: "",
    //  component: DocumentRole05ItemComponent
    //},
    //{
    //  path: "add",
    //  component: DocumentRole05ItemComponent
    //},
    //{
    //  path: "edit/:id",
    //  component: DocumentRole05ItemComponent
    //},
    //{
    path: "list",
    component: DocumentRole05ItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole05ItemRoutingModule { }
