import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole05BoxRefComponent } from 'src/app/pages/madrid-role05-box-ref/madrid-role05-box-ref.component';


const routes: Routes = [
  {
    path: "",
    component: MadridRole05BoxRefComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole05BoxRefRoutingModule { }
