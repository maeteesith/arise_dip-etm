import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole05BoxRefRoutingModule } from './madrid-role05-box-ref-routing.module';
import { MadridRole05BoxRefComponent } from 'src/app/pages/madrid-role05-box-ref/madrid-role05-box-ref.component';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [MadridRole05BoxRefComponent],
  imports: [...IMPORTS, MadridRole05BoxRefRoutingModule],
  entryComponents: [MadridRole05BoxRefComponent],
  providers: PROVIDERS
})
export class MadridRole05BoxRefModule { }
