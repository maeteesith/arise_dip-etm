import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  PublicRole05DocumentListComponent,
  PublicRole05DocumentComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole05DocumentComponent
  },
  {
    path: "add",
    component: PublicRole05DocumentComponent
  },
  {
    path: "edit/:id",
    component: PublicRole05DocumentComponent
  },
  {
   path: "list",
   component: PublicRole05DocumentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole05DocumentRoutingModule {}
