import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole05DocumentRoutingModule } from "./public-role05-document-routing.module";
import {
  PublicRole05DocumentListComponent,
  PublicRole05DocumentComponent
} from "../../pages";

@NgModule({
  declarations: [
    PublicRole05DocumentListComponent, 
    PublicRole05DocumentComponent
  ],
  imports: [...IMPORTS, PublicRole05DocumentRoutingModule],
  providers: PROVIDERS
})
export class PublicRole05DocumentModule { }
