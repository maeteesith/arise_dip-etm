import { ModalMadridRole01ConsiderComponent } from './../../pages/modal-madrid-role01-consider/modal-madrid-role01-consider.component';
import { MatDialog } from '@angular/material';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ModalMadridRole01ConsiderRoutingModule } from './modal-madrid-role01-consider-routing.module';

@NgModule({
  declarations: [ModalMadridRole01ConsiderComponent],
  imports: [...IMPORTS, ModalMadridRole01ConsiderRoutingModule,MatDialog ],
  entryComponents:[ModalMadridRole01ConsiderComponent],
  providers: PROVIDERS
})
export class MadridRole07SaveListModule {}
