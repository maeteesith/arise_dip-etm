import { ModalMadridRole01ConsiderComponent } from './../../pages/modal-madrid-role01-consider/modal-madrid-role01-consider.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SaveListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: ModalMadridRole01ConsiderComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModalMadridRole01ConsiderRoutingModule {}
