import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { eFormSave050ProcessComponent } from "../../pages";
import { CanDeactivateGuard } from "../../can-deactivate-guard.service";

const routes: Routes = [
  {
    path: "",
    component: eFormSave050ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: "add",
    component: eFormSave050ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: "edit/:id",
    component: eFormSave050ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class eFormSave050ProcessRoutingModule {}
