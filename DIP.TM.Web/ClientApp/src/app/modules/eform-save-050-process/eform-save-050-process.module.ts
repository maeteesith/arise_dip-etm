import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave050ProcessRoutingModule } from "./eform-save-050-process-routing.module";
import { eFormSave050ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave050ProcessComponent],
  imports: [...IMPORTS, eFormSave050ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave050ProcessModule {}
