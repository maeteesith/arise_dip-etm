import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { EFormRoutingModule } from "./eform-routing.module";
import {
  EFormBrandTypeComponent,
  EFormOwnerAndAgentComponent
} from "../../pages";

@NgModule({
  declarations: [EFormBrandTypeComponent, EFormOwnerAndAgentComponent],
  imports: [...IMPORTS, EFormRoutingModule],
  providers: PROVIDERS
})
export class EFormModule {}
