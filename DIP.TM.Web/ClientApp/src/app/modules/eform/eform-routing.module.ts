import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  EFormBrandTypeComponent,
  EFormOwnerAndAgentComponent
} from "../../pages";
import {
  ROUTE_PATH
} from "../../helpers";

const routes: Routes = [
  {
    path: "",
    redirectTo: `${ROUTE_PATH.EFORM_HOME.LINK}`, pathMatch: "full"
  },
  {
    path: "brand-type",
    component: EFormBrandTypeComponent
  },
  {
    path: "owner-and-agent",
    component: EFormOwnerAndAgentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EFormRoutingModule {}
