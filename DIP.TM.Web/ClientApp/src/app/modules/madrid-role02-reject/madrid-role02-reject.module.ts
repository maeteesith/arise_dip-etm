import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02RejectRoutingModule } from "./madrid-role02-reject-routing.module";
import { MadridRole02RejectListComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole02RejectListComponent],
  imports: [...IMPORTS, MadridRole02RejectRoutingModule],
  providers: PROVIDERS
})
export class MadridRole02RejectModule {}
