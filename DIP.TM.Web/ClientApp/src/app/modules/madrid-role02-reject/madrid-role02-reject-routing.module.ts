import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02RejectListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02RejectListComponent
  },
  //{
  //  path: "list",
  //  component: MadridRole02RejectListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02RejectRoutingModule {}
