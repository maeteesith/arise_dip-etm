import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole03SearchWIPORoutingModule } from './madrid-role03-search-wipo-routing.module';
import { MadridRole03SearchWIPOComponent } from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [MadridRole03SearchWIPOComponent],
  imports: [...IMPORTS, MadridRole03SearchWIPORoutingModule],
  providers: PROVIDERS
})
export class MadridRole03SearchWIPOModule { }
