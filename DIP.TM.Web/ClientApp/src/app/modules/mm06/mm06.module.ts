import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Mm06RoutingModule } from './mm06-routing.module';
import { Mm06Component } from 'src/app/pages/mm06/mm06.component';
import { IMPORTS } from '../imports';
import { RouterModule } from '@angular/router';
import { PROVIDERS } from '../providers';



@NgModule({
    declarations: [Mm06Component],
    imports: [...IMPORTS, Mm06RoutingModule],
    entryComponents: [Mm06Component],
    providers: PROVIDERS
})
export class Mm06Module { }
