import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Mm06Component } from 'src/app/pages/mm06/mm06.component';
import { ArchwizardModule } from 'angular-archwizard';


const routes: Routes = [
  {
    path: "",
    component: Mm06Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes), ArchwizardModule],
  exports: [RouterModule]
})
export class Mm06RoutingModule { }
