import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save021ProcessRoutingModule } from "./save-021-process-routing.module";
import {
  Save021ProcessListComponent,
  Save021ProcessComponent
} from "../../pages";

@NgModule({
  declarations: [Save021ProcessListComponent, Save021ProcessComponent],
  imports: [...IMPORTS, Save021ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save021ProcessModule {}
