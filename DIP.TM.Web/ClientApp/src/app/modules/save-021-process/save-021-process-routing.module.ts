import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save021ProcessListComponent,
  Save021ProcessComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save021ProcessComponent
  },
  {
    path: "add",
    component: Save021ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save021ProcessComponent
  },
  {
    path: "list",
    component: Save021ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save021ProcessRoutingModule {}
