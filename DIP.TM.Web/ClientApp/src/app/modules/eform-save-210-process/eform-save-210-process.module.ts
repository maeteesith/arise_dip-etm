import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave210ProcessRoutingModule } from "./eform-save-210-process-routing.module";
import { eFormSave210ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave210ProcessComponent],
  imports: [...IMPORTS, eFormSave210ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave210ProcessModule {}
