import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04CreateRoutingModule } from "./document-role04-create-routing.module";
import {
  DocumentRole04CreateListComponent,
  DocumentRole04CreateComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04CreateListComponent, 
    DocumentRole04CreateComponent
  ],
  imports: [...IMPORTS, DocumentRole04CreateRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04CreateModule { }
