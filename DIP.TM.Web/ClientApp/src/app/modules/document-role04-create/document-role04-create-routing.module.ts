import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04CreateListComponent,
  DocumentRole04CreateComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole04CreateComponent
  },
  {
    path: "add",
    component: DocumentRole04CreateComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole04CreateComponent
  },
  {
    path: "list",
    component: DocumentRole04CreateListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04CreateRoutingModule { }
