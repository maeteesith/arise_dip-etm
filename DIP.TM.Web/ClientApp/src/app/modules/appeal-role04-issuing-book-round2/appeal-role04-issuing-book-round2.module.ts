import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04IssuingBookRound2RoutingModule } from './appeal-role04-issuing-book-round2-routing.module';
import { 
  AppealRole04IssuingBookRound2Component,
  AppealRole04IssuingBookRound2ListComponent
} from '../../pages';

@NgModule({
  declarations: [ AppealRole04IssuingBookRound2Component, AppealRole04IssuingBookRound2ListComponent ],
  imports: [
    ...IMPORTS,
    AppealRole04IssuingBookRound2RoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04IssuingBookRound2Module { }
