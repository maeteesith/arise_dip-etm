import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole04IssuingBookRound2Component,
  AppealRole04IssuingBookRound2ListComponent
} from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole04IssuingBookRound2Component
  },
  {
    path: "list",
    component: AppealRole04IssuingBookRound2ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04IssuingBookRound2RoutingModule { }
