import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SeachEditRoutingModule } from "./madrid-role07-search-edit-routing.module";
import { MadridRole07SearchEditComponent } from "src/app/pages/madrid-role07-search-edit/madrid-role07-search-edit.component";

@NgModule({
  declarations: [MadridRole07SearchEditComponent],
  imports: [...IMPORTS, MadridRole07SeachEditRoutingModule],
  entryComponents: [MadridRole07SearchEditComponent],
  providers: PROVIDERS
})
export class MadridRole07SaveListModule {}
