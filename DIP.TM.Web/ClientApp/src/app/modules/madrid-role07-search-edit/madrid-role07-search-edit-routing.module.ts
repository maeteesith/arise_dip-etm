import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SearchEditComponent } from "src/app/pages/madrid-role07-search-edit/madrid-role07-search-edit.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SearchEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SeachEditRoutingModule {}
