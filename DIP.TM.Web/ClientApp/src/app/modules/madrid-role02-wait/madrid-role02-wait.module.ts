import { MadridRole023111ModalComponent } from './../../pages/madrid-role02-31-1-1-modal/madrid-role02-31-1-1-modal.component';
import { MadridRole02wait231modelModalComponent } from './../../pages/madrid-role02-wait-23-1-model/madrid-role02-wait-23-1-model.component';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02WaitRoutingModule } from "./madrid-role02-wait-routing.module";
import { MadridRole02WaitListComponent } from "../../pages";
import { MadridRole02RequestProtectionModalComponent } from "src/app/pages/madrid-role02-request-protection-modal/madrid-role02-request-protection-modal.component";
import { MadridRole02ConciderProtectionModalComponent } from "src/app/pages/madrid-role02-concider-protection-modal/madrid-role02-concider-protection-modal.component";

@NgModule({
  declarations: [MadridRole02WaitListComponent,MadridRole02RequestProtectionModalComponent,MadridRole02ConciderProtectionModalComponent,MadridRole02wait231modelModalComponent,MadridRole023111ModalComponent],
  imports: [...IMPORTS, MadridRole02WaitRoutingModule],
  entryComponents:[MadridRole02RequestProtectionModalComponent,MadridRole02ConciderProtectionModalComponent,MadridRole02wait231modelModalComponent,MadridRole023111ModalComponent],
  providers: PROVIDERS
})
export class MadridRole02WaitModule {}
