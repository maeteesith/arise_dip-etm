import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DashboardPublicRole04RoutingModule } from "./dashboard-public-role-04-routing.module";
import {
  //DashboardPublicRole04ListComponent,
  DashboardPublicRole04Component
} from "../../pages";

@NgModule({
  declarations: [
    //DashboardPublicRole04ListComponent, 
    DashboardPublicRole04Component
  ],
  imports: [...IMPORTS, DashboardPublicRole04RoutingModule],
  providers: PROVIDERS
})
export class DashboardPublicRole04Module { }
