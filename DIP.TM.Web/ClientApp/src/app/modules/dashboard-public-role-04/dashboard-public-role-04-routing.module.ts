import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //DashboardPublicRole04ListComponent,
  DashboardPublicRole04Component
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DashboardPublicRole04Component
  },
  {
    path: "add",
    component: DashboardPublicRole04Component
  },
  {
    path: "edit/:id",
    component: DashboardPublicRole04Component
  //},
  //{
  // path: "list",
  // component: DashboardPublicRole04ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardPublicRole04RoutingModule {}
