import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole02RegisterCommentComponent } from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole02RegisterCommentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole02RegisterCommentRoutingModule { }
