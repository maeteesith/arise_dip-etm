import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole02RegisterCommentRoutingModule } from './appeal-role02-register-comment-routing.module';
import { AppealRole02RegisterCommentComponent } from '../../pages';


@NgModule({
  declarations: [AppealRole02RegisterCommentComponent],
  imports: [
    ...IMPORTS,
    AppealRole02RegisterCommentRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole02RegisterCommentModule { }
