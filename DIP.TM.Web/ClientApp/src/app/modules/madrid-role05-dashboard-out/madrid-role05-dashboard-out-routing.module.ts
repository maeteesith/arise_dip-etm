import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole05DashboardOutComponent } from 'src/app/pages/madrid-role05-dashboard-out/madrid-role05-dashboard-out.component';


const routes: Routes = [
  {
    path: "",
    component: MadridRole05DashboardOutComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole05DashboardOutRoutingModule { }
