import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole05DashboardOutRoutingModule } from './madrid-role05-dashboard-out-routing.module';
import { MadridRole05DashboardOutComponent } from 'src/app/pages/madrid-role05-dashboard-out/madrid-role05-dashboard-out.component';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';


@NgModule({
  declarations: [MadridRole05DashboardOutComponent],
  imports: [...IMPORTS, MadridRole05DashboardOutRoutingModule],
  entryComponents: [MadridRole05DashboardOutComponent],
  providers: PROVIDERS
})
export class MadridRole05DashboardOutModule { }
