import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentItemRoutingModule } from "./document-item-routing.module";
import { DocumentItemListComponent } from "../../pages";

@NgModule({
  declarations: [DocumentItemListComponent],
  imports: [...IMPORTS, DocumentItemRoutingModule],
  providers: PROVIDERS
})
export class DocumentItemModule {}
