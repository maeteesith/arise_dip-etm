import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocumentItemListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentItemListComponent
  },
  {
    path: "list",
    component: DocumentItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentItemRoutingModule {}
