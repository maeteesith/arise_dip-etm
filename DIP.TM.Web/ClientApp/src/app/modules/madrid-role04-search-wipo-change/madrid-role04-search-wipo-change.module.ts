import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole04SearchWIPOChangeRoutingModule } from './madrid-role04-search-wipo-change-routing.module';


import { MadridRole04SearchWIPOChangeComponent} from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [
    MadridRole04SearchWIPOChangeComponent
  ],
  imports: [...IMPORTS, MadridRole04SearchWIPOChangeRoutingModule],
  providers: PROVIDERS
})
export class MadridRole04SearchWIPOChangeModule { }
