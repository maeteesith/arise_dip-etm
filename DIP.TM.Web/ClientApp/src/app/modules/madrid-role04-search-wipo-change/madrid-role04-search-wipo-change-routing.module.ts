import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MadridRole04SearchWIPOChangeComponent} from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole04SearchWIPOChangeComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04SearchWIPOChangeRoutingModule { }
