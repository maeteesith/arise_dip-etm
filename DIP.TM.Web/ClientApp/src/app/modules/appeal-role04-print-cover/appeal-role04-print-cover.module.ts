import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04PrintCoverRoutingModule } from './appeal-role04-print-cover-routing.module';
import { AppealRole04PrintCoverListComponent } from '../../pages';


@NgModule({
  declarations: [AppealRole04PrintCoverListComponent],
  imports: [
    ...IMPORTS,
    AppealRole04PrintCoverRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04PrintCoverModule { }
