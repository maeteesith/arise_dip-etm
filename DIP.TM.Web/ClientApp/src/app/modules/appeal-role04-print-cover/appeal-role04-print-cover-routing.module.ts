import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole04PrintCoverListComponent } from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole04PrintCoverListComponent
  },
  {
    path: "list",
    component: AppealRole04PrintCoverListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04PrintCoverRoutingModule { }
