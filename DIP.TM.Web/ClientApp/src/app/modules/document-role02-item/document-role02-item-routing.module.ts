import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocumentRole02ItemListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole02ItemListComponent
  },
  {
    path: "list",
    component: DocumentRole02ItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole02ItemRoutingModule {}
