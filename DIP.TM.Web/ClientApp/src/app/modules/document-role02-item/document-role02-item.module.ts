import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole02ItemRoutingModule } from "./document-role02-item-routing.module";
import { DocumentRole02ItemListComponent } from "../../pages";

@NgModule({
  declarations: [DocumentRole02ItemListComponent],
  imports: [...IMPORTS, DocumentRole02ItemRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole02ItemModule {}
