import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole05WorkPaymentComponent,MadridRole05RequestInComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole05WorkPaymentComponent
  },
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole05WorkPaymentRoutingModule { }
