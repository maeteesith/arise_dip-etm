import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole05WorkPaymentComponent,MadridRole05ModalComponent, MadridRole05DocAttachComponent,MadridRole05PaymentComponent,MadridRole05FeeComponent,MadridRole05RequestInComponent } from "../../pages";
import { MatTabsModule } from '@angular/material/tabs'
import { ConfirmationDialogComponent } from '../../shared';

import { MadridRole05WorkPaymentRoutingModule } from './madrid-role05-work-payment-routing.module'


@NgModule({
  declarations: [MadridRole05WorkPaymentComponent],
  imports: [...IMPORTS, MadridRole05WorkPaymentRoutingModule,MatTabsModule],
  providers: PROVIDERS
})
export class MadridRole05WorkPaymentModule { }
