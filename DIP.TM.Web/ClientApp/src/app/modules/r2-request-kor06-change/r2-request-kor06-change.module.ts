import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { R2RequestKor06ChangeRoutingModule } from './r2-request-kor06-change-routing.module';
import {
  R2RequestKor06ChangeComponent
} from "../../pages";

@NgModule({
declarations: [R2RequestKor06ChangeComponent],
imports: [...IMPORTS, R2RequestKor06ChangeRoutingModule],
providers: PROVIDERS
})
export class R2RequestKor06ChangeModule { }