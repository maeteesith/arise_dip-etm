import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  R2RequestKor06ChangeComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: R2RequestKor06ChangeComponent,
},
{
path: "add",
component: R2RequestKor06ChangeComponent,
},
{
path: "edit/:id",
component: R2RequestKor06ChangeComponent,
},
{
path: "list",
component: R2RequestKor06ChangeComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class R2RequestKor06ChangeRoutingModule { }