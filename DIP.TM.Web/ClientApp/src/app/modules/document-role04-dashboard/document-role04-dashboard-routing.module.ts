import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04DashboardComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole04DashboardComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04DashboardRoutingModule { }
