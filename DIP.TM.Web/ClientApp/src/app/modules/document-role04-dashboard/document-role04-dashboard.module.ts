import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04DashboardRoutingModule } from "./document-role04-dashboard-routing.module";
import {
  //DocumentRole04AppealListComponent,
  DocumentRole04DashboardComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04DashboardComponent
  ],
  imports: [...IMPORTS, DocumentRole04DashboardRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04DashboardModule { }
