import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {
  IncludeRole01ChangeComponent,
  IncludeRole01ChangeListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: IncludeRole01ChangeComponent
  },
  // {
  //   path: "add",
  //   component: IncludeRole01ChangeComponent
  // },
  // {
  //   path: "edit/:id",
  //   component: IncludeRole01ChangeComponent
  // },
  {
    path: "list",
    component: IncludeRole01ChangeListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IncludeRole01ChangeRoutingModule { }