import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { IncludeRole01ChangeRoutingModule } from "./include-role01-change-routing.module";
import {
  IncludeRole01ChangeComponent,
  IncludeRole01ChangeListComponent
} from "../../pages";

@NgModule({
declarations: [
  IncludeRole01ChangeComponent,
  IncludeRole01ChangeListComponent
],
imports: [
  ...IMPORTS, IncludeRole01ChangeRoutingModule
],
providers: PROVIDERS
})
export class IncludeRole01ChangeModule { }