import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MadridRole03SearchListComponent } from "../../pages";
import { MadridRole03SearchListRoutingModule } from './madrid-role03-search-list-routing.module';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';


@NgModule({
  declarations: [MadridRole03SearchListComponent],
  imports: [...IMPORTS, MadridRole03SearchListRoutingModule],
  providers: PROVIDERS
})
export class MadridRole03SearchListModule { }
