import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole03SearchListComponent } from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole03SearchListComponent
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole03SearchListRoutingModule { }
