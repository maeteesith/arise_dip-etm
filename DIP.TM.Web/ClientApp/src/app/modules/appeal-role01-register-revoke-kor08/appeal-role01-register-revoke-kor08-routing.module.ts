import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { AppealRole01RegisterRevokeKor08Component } from '../../pages'
const routes: Routes = [
  {
    path: "",
    component: AppealRole01RegisterRevokeKor08Component
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole01RegisterRevokeKor08RoutingModule { }