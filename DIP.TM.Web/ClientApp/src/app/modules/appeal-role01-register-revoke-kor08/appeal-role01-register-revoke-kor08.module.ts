import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole01RegisterRevokeKor08RoutingModule } from "./appeal-role01-register-revoke-kor08-routing.module";
import { AppealRole01RegisterRevokeKor08Component } from "../../pages";

@NgModule({
  declarations: [AppealRole01RegisterRevokeKor08Component],
  imports: [...IMPORTS, AppealRole01RegisterRevokeKor08RoutingModule],
  providers: PROVIDERS
})
export class AppealRole01RegisterRevokeKor08Module { }