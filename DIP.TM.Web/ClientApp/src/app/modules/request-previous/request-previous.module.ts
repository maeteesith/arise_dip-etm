import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestPreviousRoutingModule } from './request-previous-routing.module';
import {
  RequestPreviousComponent
} from "../../pages";
 
@NgModule({
declarations: [
  RequestPreviousComponent
],
imports: [...IMPORTS, RequestPreviousRoutingModule],
providers: PROVIDERS
})
export class RequestPreviousModule { }