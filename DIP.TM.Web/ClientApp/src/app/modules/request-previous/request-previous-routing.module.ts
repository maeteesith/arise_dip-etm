import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  RequestPreviousComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: RequestPreviousComponent,
},
{
path: "add",
component: RequestPreviousComponent,
},
{
path: "edit/:id",
component: RequestPreviousComponent,
},
{
path: "list",
component: RequestPreviousComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestPreviousRoutingModule {}