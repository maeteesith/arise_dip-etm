import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole04BoardApprovalComponent,
  AppealRole04BoardApprovalListComponent
} from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole04BoardApprovalComponent
  },
  {
    path: "list",
    component: AppealRole04BoardApprovalListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04BoardApprovalRoutingModule { }
