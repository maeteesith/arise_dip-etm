import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04BoardApprovalRoutingModule } from './appeal-role04-board-approval-routing.module';
import { 
  AppealRole04BoardApprovalComponent,
  AppealRole04BoardApprovalListComponent
} from '../../pages';


@NgModule({
  declarations: [ AppealRole04BoardApprovalComponent,AppealRole04BoardApprovalListComponent ],
  imports: [
    ...IMPORTS,
    AppealRole04BoardApprovalRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04BoardApprovalModule { }
