import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole05WorkboxWipoRoutingModule } from './madrid-role05-workbox-wipo-routing.module';
import { PROVIDERS } from '../providers';
import { MadridRole05WorkboxWipoComponent } from 'src/app/pages/madrid-role05-workbox-wipo/madrid-role05-workbox-wipo.component';
import { IMPORTS } from '../imports';


@NgModule({
    declarations: [MadridRole05WorkboxWipoComponent],
    imports: [...IMPORTS, MadridRole05WorkboxWipoRoutingModule],
    entryComponents: [MadridRole05WorkboxWipoComponent],
    providers: PROVIDERS
  })
export class MadridRole05WorkboxWipoModule { }
