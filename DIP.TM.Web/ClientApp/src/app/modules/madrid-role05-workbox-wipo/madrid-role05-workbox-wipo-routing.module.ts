import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole05WorkboxWipoComponent } from 'src/app/pages/madrid-role05-workbox-wipo/madrid-role05-workbox-wipo.component';


const routes: Routes = [
  {
    path: "",
    component: MadridRole05WorkboxWipoComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole05WorkboxWipoRoutingModule { }
