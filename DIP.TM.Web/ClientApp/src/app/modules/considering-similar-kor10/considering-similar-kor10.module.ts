import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsideringSimilarKor10RoutingModule } from "./considering-similar-kor10-routing.module";
import {
  //ConsideringSimilarKor10ListComponent,
  ConsideringSimilarKor10Component
} from "../../pages";

@NgModule({
  declarations: [
    //ConsideringSimilarKor10ListComponent, 
    ConsideringSimilarKor10Component
  ],
  imports: [...IMPORTS, ConsideringSimilarKor10RoutingModule],
  providers: PROVIDERS
})
export class ConsideringSimilarKor10Module { }
