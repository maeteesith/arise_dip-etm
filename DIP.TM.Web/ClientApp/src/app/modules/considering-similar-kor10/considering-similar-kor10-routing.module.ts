import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //ConsideringSimilarKor10ListComponent,
  ConsideringSimilarKor10Component
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: ConsideringSimilarKor10Component
  },
  {
    path: "add",
    component: ConsideringSimilarKor10Component
  },
  {
    path: "edit/:id",
    component: ConsideringSimilarKor10Component
  //},
  //{
  // path: "list",
  // component: ConsideringSimilarKor10ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsideringSimilarKor10RoutingModule {}
