import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole03CheckBeforeIssuingBookRoutingModule } from './appeal-role03-check-before-issuing-book-routing.module';
import { 
  AppealRole03CheckBeforeIssuingBookListComponent,
  AppealRole03CheckBeforeIssuingBookComponent 
} from '../../pages';


@NgModule({
  declarations: [AppealRole03CheckBeforeIssuingBookListComponent, AppealRole03CheckBeforeIssuingBookComponent],
  imports: [
    ...IMPORTS,
    AppealRole03CheckBeforeIssuingBookRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole03CheckBeforeIssuingBookModule { }
