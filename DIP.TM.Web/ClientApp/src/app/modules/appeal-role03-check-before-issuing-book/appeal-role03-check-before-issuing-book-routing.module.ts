import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole03CheckBeforeIssuingBookListComponent,
  AppealRole03CheckBeforeIssuingBookComponent 
} from '../../pages';
  



const routes: Routes = [
  {
    path: "",
    component: AppealRole03CheckBeforeIssuingBookComponent
  },
  {
    path: "list",
    component: AppealRole03CheckBeforeIssuingBookListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole03CheckBeforeIssuingBookRoutingModule { }
