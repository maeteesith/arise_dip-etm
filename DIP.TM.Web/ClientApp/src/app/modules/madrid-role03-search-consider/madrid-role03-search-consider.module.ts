import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole03SearchConsiderRoutingModule } from './madrid-role03-search-consider-routing.module';
import { MadridRole03SearchConsiderComponent } from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [MadridRole03SearchConsiderComponent],
  imports: [...IMPORTS, MadridRole03SearchConsiderRoutingModule],
  providers: PROVIDERS
})
export class MadridRole03SearchConsiderModule { }
