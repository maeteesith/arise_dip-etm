import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PrintLogRoutingModule } from './print-log-routing.module';
import {
  PrintLogComponent
} from "../../pages";
 
@NgModule({
declarations: [
  PrintLogComponent
],
imports: [...IMPORTS, PrintLogRoutingModule],
providers: PROVIDERS
})
export class PrintLogModule { }
