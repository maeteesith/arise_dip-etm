import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  PrintLogComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: PrintLogComponent,
},
{
path: "add",
component: PrintLogComponent,
},
{
path: "edit/:id",
component: PrintLogComponent,
},
{
path: "list",
component: PrintLogComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class PrintLogRoutingModule {}