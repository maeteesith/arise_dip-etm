import { ModalMadridRole01ConsiderComponent } from '../../pages/modal-madrid-role01-consider/modal-madrid-role01-consider.component';
import { MatDialog } from '@angular/material';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ModalMadridRole01CheckValueRoutingModule } from './modal-madrid-role01-check-value-routing.module';
import { ModalMadridRole01CheckValueComponent } from 'src/app/pages/modal-madrid-role01-check-value/modal-madrid-role01-check-value.component';

@NgModule({
  declarations: [ModalMadridRole01CheckValueComponent],
  imports: [...IMPORTS, ModalMadridRole01CheckValueRoutingModule,MatDialog ],
  entryComponents:[ModalMadridRole01CheckValueComponent],
  providers: PROVIDERS
})
export class MadridRole07SaveListModule {}
