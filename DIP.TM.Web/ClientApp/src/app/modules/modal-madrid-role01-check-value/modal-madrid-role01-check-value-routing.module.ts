import { ModalMadridRole01CheckValueComponent } from '../../pages/modal-madrid-role01-check-value/modal-madrid-role01-check-value.component';

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";


const routes: Routes = [
  {
    path: "",
    component: ModalMadridRole01CheckValueComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModalMadridRole01CheckValueRoutingModule {}
