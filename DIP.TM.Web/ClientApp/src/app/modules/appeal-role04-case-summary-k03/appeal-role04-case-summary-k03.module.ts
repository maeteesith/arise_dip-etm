import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04CaseSummaryK03RoutingModule } from './appeal-role04-case-summary-k03-routing.module';
import { 
  AppealRole04CaseSummaryK03ListComponent,
  AppealRole04CaseSummaryK03Component 
} from '../../pages';


@NgModule({
  declarations: [ AppealRole04CaseSummaryK03ListComponent, AppealRole04CaseSummaryK03Component ],
  imports: [
    ...IMPORTS,
    AppealRole04CaseSummaryK03RoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04CaseSummaryK03Module { }
