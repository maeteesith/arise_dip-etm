import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole04CaseSummaryK03ListComponent,
  AppealRole04CaseSummaryK03Component 
} from '../../pages';
 
const routes: Routes = [
  {
    path: "",
    component: AppealRole04CaseSummaryK03Component
  },
  {
    path: "list",
    component: AppealRole04CaseSummaryK03ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04CaseSummaryK03RoutingModule { }