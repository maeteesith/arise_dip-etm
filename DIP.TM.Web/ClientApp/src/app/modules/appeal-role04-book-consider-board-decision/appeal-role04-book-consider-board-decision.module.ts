import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04BookConsiderBoardDecisionRoutingModule } from './appeal-role04-book-consider-board-decision-routing.module';
import { 
  AppealRole04BookConsiderBoardDecisionComponent,
  AppealRole04BookConsiderBoardDecisionListComponent
} from '../../pages';


@NgModule({
  declarations: [AppealRole04BookConsiderBoardDecisionComponent, AppealRole04BookConsiderBoardDecisionListComponent],
  imports: [
    ...IMPORTS,
    AppealRole04BookConsiderBoardDecisionRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04BookConsiderBoardDecisionModule { }
