import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole04BookConsiderBoardDecisionComponent,
  AppealRole04BookConsiderBoardDecisionListComponent
} from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole04BookConsiderBoardDecisionComponent
  },
  {
    path: "list",
    component: AppealRole04BookConsiderBoardDecisionListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04BookConsiderBoardDecisionRoutingModule { }
