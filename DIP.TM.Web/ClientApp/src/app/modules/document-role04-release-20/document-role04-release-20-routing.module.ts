import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04Release20ListComponent,
  DocumentRole04Release20Component
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole04Release20Component
  },
  {
    path: "add",
    component: DocumentRole04Release20Component
  },
  {
    path: "edit/:id",
    component: DocumentRole04Release20Component
  },
  {
    path: "list",
    component: DocumentRole04Release20ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04Release20RoutingModule { }
