import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04Release20RoutingModule } from "./document-role04-release-20-routing.module";
import {
  DocumentRole04Release20ListComponent,
  DocumentRole04Release20Component
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04Release20ListComponent,
    DocumentRole04Release20Component
  ],
  imports: [...IMPORTS, DocumentRole04Release20RoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04Release20Module { }
