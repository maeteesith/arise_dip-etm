import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole05OtherRoutingModule } from "./public-role05-other-routing.module";
import {
    PublicRole05OtherListComponent,
    PublicRole05OtherComponent
} from "../../pages";

@NgModule({
    declarations: [
        PublicRole05OtherListComponent,
        PublicRole05OtherComponent
    ],
    imports: [...IMPORTS, PublicRole05OtherRoutingModule],
    providers: PROVIDERS
})
export class PublicRole05OtherModule { }
