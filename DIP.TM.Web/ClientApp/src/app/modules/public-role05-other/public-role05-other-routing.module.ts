import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
    PublicRole05OtherListComponent,
    PublicRole05OtherComponent
} from "../../pages";

const routes: Routes = [
    {
        path: "",
        component: PublicRole05OtherComponent
    },
    {
        path: "add",
        component: PublicRole05OtherComponent
    },
    {
        path: "edit/:id",
        component: PublicRole05OtherComponent
    },
    {
        path: "list",
        component: PublicRole05OtherListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRole05OtherRoutingModule { }
