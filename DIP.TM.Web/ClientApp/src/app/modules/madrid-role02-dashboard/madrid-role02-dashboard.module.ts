import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02DashboardRoutingModule } from "./madrid-role02-dashboard-routing.module";
import { MadridRole02DashboardComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole02DashboardComponent],
  imports: [...IMPORTS, MadridRole02DashboardRoutingModule],
  providers: PROVIDERS
})
export class MadridRole02DashboardModule {}
