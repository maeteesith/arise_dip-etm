import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02DashboardComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02DashboardComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02DashboardRoutingModule {}
