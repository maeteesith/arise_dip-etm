import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02SearchRegisRoutingModule } from "./madrid-role02-search-regis-routing.module";
import { MadridRole02SearchRegisComponent } from "../../pages";
import { MadridRole02LimitationsModalComponent } from "src/app/pages/madrid-role02-limitations-modal/madrid-role02-limitations-modal.component";
import { MadridRole02RequestProtection94ModalComponent } from "src/app/pages/madrid-role02-request-protection94-modal/madrid-role02-request-protection94-modal.component";
import { MadridRole02RequestProtection95ModalComponent } from "src/app/pages/madrid-role02-request-protection95-modal/madrid-role02-request-protection95-modal.component";
import { MadridRole02Request261ModalComponent } from "src/app/pages/madrid-role02-request261-modal/madrid-role02-request261-modal.component";
import { MadridRole02EditChangeModalComponent } from "src/app/pages/madrid-role02-edit-change-modal/madrid-role02-edit-change-modal.component";
import { MadridRole02Protection312ModalComponent } from "src/app/pages/madrid-role02-protection312-modal/madrid-role02-protection312-modal.component";
import { MadridRole02WaiverRequesModalComponent } from "src/app/pages/madrid-role02-waiver-reques-modal/madrid-role02-waiver-reques-modal.component";


@NgModule({
  declarations: [MadridRole02SearchRegisComponent,
    MadridRole02LimitationsModalComponent,
    MadridRole02RequestProtection94ModalComponent,
    MadridRole02RequestProtection95ModalComponent,
    MadridRole02Request261ModalComponent,
    MadridRole02EditChangeModalComponent,
    MadridRole02Protection312ModalComponent,
    MadridRole02WaiverRequesModalComponent],
  imports: [...IMPORTS, MadridRole02SearchRegisRoutingModule],
  entryComponents: [MadridRole02SearchRegisComponent, 
     MadridRole02LimitationsModalComponent,
    MadridRole02RequestProtection94ModalComponent,
    MadridRole02RequestProtection95ModalComponent,
    MadridRole02Request261ModalComponent,
    MadridRole02EditChangeModalComponent,
    MadridRole02Protection312ModalComponent,
    MadridRole02WaiverRequesModalComponent],
    
  providers: PROVIDERS
})
export class MadridRole02SearcRegishModule {}
