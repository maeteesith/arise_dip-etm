import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02SearchRegisComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02SearchRegisComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02SearchRegisRoutingModule {}
