import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole04CheckRoutingModule } from "./public-role04-check-routing.module";
import {
  PublicRole04CheckListComponent,
  PublicRole04CheckComponent
} from "../../pages";

@NgModule({
  declarations: [
    PublicRole04CheckListComponent, 
    PublicRole04CheckComponent
  ],
  imports: [...IMPORTS, PublicRole04CheckRoutingModule],
  providers: PROVIDERS
})
export class PublicRole04CheckModule { }
