import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  PublicRole04CheckListComponent,
  PublicRole04CheckComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole04CheckComponent
  },
  {
    path: "add",
    component: PublicRole04CheckComponent
  },
  {
    path: "edit/:id",
    component: PublicRole04CheckComponent
  },
  {
    path: "list",
    component: PublicRole04CheckListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole04CheckRoutingModule { }
