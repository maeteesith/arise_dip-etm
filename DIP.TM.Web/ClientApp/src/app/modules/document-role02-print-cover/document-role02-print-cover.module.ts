import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole02PrintCoverRoutingModule } from "./document-role02-print-cover-routing.module";
import { DocumentRole02PrintCoverListComponent } from "../../pages";

@NgModule({
  declarations: [DocumentRole02PrintCoverListComponent],
  imports: [...IMPORTS, DocumentRole02PrintCoverRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole02PrintCoverModule {}
