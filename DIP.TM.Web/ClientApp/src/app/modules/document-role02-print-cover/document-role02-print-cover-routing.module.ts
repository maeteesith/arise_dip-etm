import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocumentRole02PrintCoverListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole02PrintCoverListComponent
  },
  {
    path: "list",
    component: DocumentRole02PrintCoverListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole02PrintCoverRoutingModule {}
