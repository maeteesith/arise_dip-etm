import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SearchEditChangeComponent } from "src/app/pages/madrid-role07-search-edit-change/madrid-role07-search-edit-change.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SearchEditChangeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SeachEditChangeRoutingModule {}
