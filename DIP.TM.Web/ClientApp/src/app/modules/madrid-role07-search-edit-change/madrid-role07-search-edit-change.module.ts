import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SeachEditChangeRoutingModule } from "./madrid-role07-search-edit-change-routing.module";
import { MadridRole07SearchEditChangeComponent } from "src/app/pages/madrid-role07-search-edit-change/madrid-role07-search-edit-change.component";

@NgModule({
  declarations: [MadridRole07SearchEditChangeComponent],
  imports: [...IMPORTS, MadridRole07SeachEditChangeRoutingModule],
  entryComponents: [MadridRole07SearchEditChangeComponent],
  providers: PROVIDERS
})
export class MadridRole07SearchEditChangeModule {}
