import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave190ProcessRoutingModule } from "./eform-save-190-process-routing.module";
import { eFormSave190ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave190ProcessComponent],
  imports: [...IMPORTS, eFormSave190ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave190ProcessModule {}
