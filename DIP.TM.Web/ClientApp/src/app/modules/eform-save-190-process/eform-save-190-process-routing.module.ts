import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { eFormSave190ProcessComponent } from "../../pages";
import { CanDeactivateGuard } from "../../can-deactivate-guard.service";

const routes: Routes = [
  {
    path: "",
    component: eFormSave190ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: "add",
    component: eFormSave190ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  },
  {
    path: "edit/:id",
    component: eFormSave190ProcessComponent,
    canDeactivate: [CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class eFormSave190ProcessRoutingModule {}
