import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole05CertifiedMeetingReportRoutingModule } from './appeal-role05-certified-meeting-report-routing.module';
import { 
  AppealRole05CertifiedMeetingReportListComponent,
  AppealRole05CertifiedMeetingReportComponent
} from '../../pages';


@NgModule({
  declarations: [ AppealRole05CertifiedMeetingReportListComponent, AppealRole05CertifiedMeetingReportComponent ],
  imports: [
    ...IMPORTS,
    AppealRole05CertifiedMeetingReportRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole05CertifiedMeetingReportModule { }
