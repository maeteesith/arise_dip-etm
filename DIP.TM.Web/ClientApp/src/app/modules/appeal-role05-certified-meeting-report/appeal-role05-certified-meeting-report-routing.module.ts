import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole05CertifiedMeetingReportListComponent ,
  AppealRole05CertifiedMeetingReportComponent
} from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole05CertifiedMeetingReportComponent
  },
  {
    path: "list",
    component: AppealRole05CertifiedMeetingReportListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole05CertifiedMeetingReportRoutingModule { }
