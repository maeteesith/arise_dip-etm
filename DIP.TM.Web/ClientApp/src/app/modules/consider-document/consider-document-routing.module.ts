import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  ConsiderDocumentComponent,
  ConsiderDocumentListComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: ConsiderDocumentComponent,
},
{
path: "add",
component: ConsiderDocumentComponent,
},
{
path: "edit/:id",
component: ConsiderDocumentComponent,
},
{
path: "list",
component: ConsiderDocumentListComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ConsiderDocumentRoutingModule { }