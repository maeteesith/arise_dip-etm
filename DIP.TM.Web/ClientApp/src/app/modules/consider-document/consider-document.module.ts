import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsiderDocumentRoutingModule } from './consider-document-routing.module';
import {
  ConsiderDocumentComponent,
  ConsiderDocumentListComponent
} from "../../pages";

@NgModule({
declarations: [
  ConsiderDocumentComponent,
  ConsiderDocumentListComponent
],
imports: [...IMPORTS, ConsiderDocumentRoutingModule],
providers: PROVIDERS
})
export class ConsiderDocumentModule { }