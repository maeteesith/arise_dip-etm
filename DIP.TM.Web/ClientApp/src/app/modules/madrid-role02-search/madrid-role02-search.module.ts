import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02SearchRoutingModule } from "./madrid-role02-search-routing.module";
import { MadridRole02SearchComponent } from "../../pages";
import { MadridRole02CheckDetailModalComponent } from "src/app/pages/madrid-role02-check-detail-modal/madrid-role02-check-detail-modal.component";

@NgModule({
  declarations: [MadridRole02SearchComponent,MadridRole02CheckDetailModalComponent],
  imports: [...IMPORTS, MadridRole02SearchRoutingModule],
  entryComponents:[MadridRole02CheckDetailModalComponent],
  providers: PROVIDERS
})
export class MadridRole02SearchModule {}
