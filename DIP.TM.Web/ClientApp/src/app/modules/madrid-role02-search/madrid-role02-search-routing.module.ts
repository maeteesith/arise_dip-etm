import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02SearchComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02SearchComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02SearchRoutingModule {}
