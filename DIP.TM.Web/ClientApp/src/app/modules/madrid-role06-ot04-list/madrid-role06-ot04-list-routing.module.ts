import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06Ot04ListComponent } from 'src/app/pages/madrid-role06-ot04-list/madrid-role06-ot04-list.component';


const routes: Routes = [
  {
    path: "",
    component: MadridRole06Ot04ListComponent
  },
  {
    path: "list",
    component: MadridRole06Ot04ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06OT04RoutingModule { }
