import { NgModule } from '@angular/core';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06Ni06ListComponent } from "../../pages";
import { MadridRole06OT04RoutingModule } from './madrid-role06-ot04-list-routing.module';
import { MadridRole06Ot04ListComponent } from 'src/app/pages/madrid-role06-ot04-list/madrid-role06-ot04-list.component';
import { ModalAttachFileArrayComponent } from 'src/app/pages/modal-attach-file-array/modal-attach-file-array.component';


@NgModule({
  declarations: [MadridRole06Ot04ListComponent, ModalAttachFileArrayComponent],
  imports: [...IMPORTS, MadridRole06OT04RoutingModule],
  entryComponents: [ModalAttachFileArrayComponent],
  providers: PROVIDERS
})
export class MadridRole06OT04Module { }
