import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole01KorOtherFloor3RoutingModule } from "./appeal-role01-kor-other-floor3-routing.module";
import { 
  AppealRole01KorOtherFloor3Component,
  AppealRole01KorOtherFloor3ListComponent
 } from "../../pages";

@NgModule({
  declarations: [
    AppealRole01KorOtherFloor3Component,
    AppealRole01KorOtherFloor3ListComponent
  ],
  imports: [...IMPORTS, AppealRole01KorOtherFloor3RoutingModule],
  providers: PROVIDERS
})

export class AppealRole01KorOtherFloor3Module { }
