import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { 
  AppealRole01KorOtherFloor3Component,
  AppealRole01KorOtherFloor3ListComponent
 } from "../../pages";
const routes: Routes = [
  {
    path: "",
    component: AppealRole01KorOtherFloor3Component
  },
  {
    path: "list",
    component: AppealRole01KorOtherFloor3ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole01KorOtherFloor3RoutingModule { }
