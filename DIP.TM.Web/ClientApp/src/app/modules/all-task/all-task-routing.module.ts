import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AllTaskComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: AllTaskComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AllTaskRoutingModule {}
