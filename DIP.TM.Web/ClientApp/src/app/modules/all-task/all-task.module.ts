import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AllTaskRoutingModule } from "./all-task-routing.module";
import { AllTaskComponent } from "../../pages";

@NgModule({
  declarations: [AllTaskComponent],
  imports: [...IMPORTS, AllTaskRoutingModule],
  providers: PROVIDERS
})
export class AllTaskModule {}
