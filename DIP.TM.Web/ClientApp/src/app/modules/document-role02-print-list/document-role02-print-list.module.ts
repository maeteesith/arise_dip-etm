import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole02PrintRoutingModule } from "./document-role02-print-list-routing.module";
import { DocumentRole02PrintListListComponent } from "../../pages";

@NgModule({
  declarations: [DocumentRole02PrintListListComponent],
  imports: [...IMPORTS, DocumentRole02PrintRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole02PrintModule {}
