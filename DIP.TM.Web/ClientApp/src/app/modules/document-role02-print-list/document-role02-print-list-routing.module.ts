import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DocumentRole02PrintListListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole02PrintListListComponent
  },
  {
    path: "list",
    component: DocumentRole02PrintListListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole02PrintRoutingModule {}
