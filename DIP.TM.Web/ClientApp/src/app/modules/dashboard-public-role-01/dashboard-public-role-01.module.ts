import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DashboardPublicRole01RoutingModule } from "./dashboard-public-role-01-routing.module";
import {
  //DashboardPublicRole01ListComponent,
  DashboardPublicRole01Component
} from "../../pages";

@NgModule({
  declarations: [
    //DashboardPublicRole01ListComponent, 
    DashboardPublicRole01Component
  ],
  imports: [...IMPORTS, DashboardPublicRole01RoutingModule],
  providers: PROVIDERS
})
export class DashboardPublicRole01Module { }
