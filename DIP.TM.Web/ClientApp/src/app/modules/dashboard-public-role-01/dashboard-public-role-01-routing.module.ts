import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //DashboardPublicRole01ListComponent,
  DashboardPublicRole01Component
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DashboardPublicRole01Component
  },
  {
    path: "add",
    component: DashboardPublicRole01Component
  },
  {
    path: "edit/:id",
    component: DashboardPublicRole01Component
  //},
  //{
  // path: "list",
  // component: DashboardPublicRole01ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardPublicRole01RoutingModule {}
