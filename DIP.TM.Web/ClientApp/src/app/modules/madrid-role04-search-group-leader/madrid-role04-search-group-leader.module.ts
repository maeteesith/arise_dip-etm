import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole04SearchGroupLeaderRoutingModule } from './madrid-role04-search-group-leader-routing.module';

import {MadridRole04SearchGroupLeaderComponent} from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';
@NgModule({
  declarations: [
    MadridRole04SearchGroupLeaderComponent
  ],
  imports: [...IMPORTS, MadridRole04SearchGroupLeaderRoutingModule],
  providers: PROVIDERS
})
export class MadridRole04SearchGroupLeaderModule { }
