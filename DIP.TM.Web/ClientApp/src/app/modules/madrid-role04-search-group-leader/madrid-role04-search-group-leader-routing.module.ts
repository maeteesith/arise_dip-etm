import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MadridRole04SearchGroupLeaderComponent} from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole04SearchGroupLeaderComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04SearchGroupLeaderRoutingModule { }
