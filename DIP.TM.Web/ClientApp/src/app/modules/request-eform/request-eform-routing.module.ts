import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
    RequestEformListComponent,
    RequestEformComponent
} from "../../pages";

const routes: Routes = [
    {
        path: "",
        component: RequestEformComponent
    },
    {
        path: "add",
        component: RequestEformComponent
    },
    {
        path: "edit/:id",
        component: RequestEformComponent
    },
    {
        path: "list",
        component: RequestEformListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class RequestEformRoutingModule { }
