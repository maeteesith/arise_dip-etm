import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestEformRoutingModule } from "./request-eform-routing.module";
import {
    RequestEformListComponent,
    RequestEformComponent
} from "../../pages";

@NgModule({
    declarations: [
        RequestEformListComponent,
        RequestEformComponent
    ],
    imports: [...IMPORTS, RequestEformRoutingModule],
    providers: PROVIDERS
})
export class RequestEformModule { }
