import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole05ApprovalDraftDecisionComponent,
  AppealRole05ApprovalDraftDecisionListComponent 
} from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole05ApprovalDraftDecisionComponent
  },
  {
    path: "list",
    component: AppealRole05ApprovalDraftDecisionListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole05ApprovalDraftDecisionRoutingModule { }
