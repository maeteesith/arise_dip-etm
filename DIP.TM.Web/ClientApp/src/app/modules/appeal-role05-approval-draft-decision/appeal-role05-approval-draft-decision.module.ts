import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole05ApprovalDraftDecisionRoutingModule } from './appeal-role05-approval-draft-decision-routing.module';
import { 
  AppealRole05ApprovalDraftDecisionListComponent,
  AppealRole05ApprovalDraftDecisionComponent
} from '../../pages';


@NgModule({
  declarations: [ AppealRole05ApprovalDraftDecisionListComponent, AppealRole05ApprovalDraftDecisionComponent ],
  imports: [
    ...IMPORTS,
    AppealRole05ApprovalDraftDecisionRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole05ApprovalDraftDecisionModule { }
