import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04DocumentInstructionRoutingModule } from "./document-role04-document-instruction-routing.module";
import {
  DocumentRole04DocumentInstructionListComponent,
  DocumentRole04DocumentInstructionComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04DocumentInstructionListComponent,
    DocumentRole04DocumentInstructionComponent
  ],
  imports: [...IMPORTS, DocumentRole04DocumentInstructionRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04DocumentInstructionModule { }
