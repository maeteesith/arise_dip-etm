import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04DocumentInstructionListComponent,
  DocumentRole04DocumentInstructionComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole04DocumentInstructionComponent
  },
  {
    path: "add",
    component: DocumentRole04DocumentInstructionComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole04DocumentInstructionComponent
  },
  {
    path: "list",
    component: DocumentRole04DocumentInstructionListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04DocumentInstructionRoutingModule { }
