import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole01CheckRoutingModule } from "./public-role01-check-routing.module";
import { PublicRole01CheckListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole01CheckListComponent],
  imports: [...IMPORTS, PublicRole01CheckRoutingModule],
  providers: PROVIDERS
})
export class PublicRole01CheckModule {}
