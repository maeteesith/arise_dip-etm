import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole01CheckListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole01CheckListComponent
  },
  {
    path: "list",
    component: PublicRole01CheckListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole01CheckRoutingModule {}
