import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RecordRegistrationNumberAllowRoutingModule } from "./record-registration-number-allow-routing.module";
import {
    //RecordRegistrationNumberAllowListComponent,
    RecordRegistrationNumberAllowComponent
} from "../../pages";

@NgModule({
    declarations: [
        //RecordRegistrationNumberAllowListComponent,
        RecordRegistrationNumberAllowComponent],
    imports: [...IMPORTS, RecordRegistrationNumberAllowRoutingModule],
    providers: PROVIDERS
})
export class RecordRegistrationNumberAllowModule { }
