import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //RecordRegistrationNumberAllowListComponent,
  RecordRegistrationNumberAllowComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RecordRegistrationNumberAllowComponent
  },
  {
    path: "add",
    component: RecordRegistrationNumberAllowComponent
  },
  {
    path: "edit/:id",
    component: RecordRegistrationNumberAllowComponent
  },
  //{
  //  path: "list",
  //  component: RecordRegistrationNumberAllowListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RecordRegistrationNumberAllowRoutingModule {}
