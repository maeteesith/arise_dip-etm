import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave030ProcessRoutingModule } from "./eform-save-030-process-routing.module";
import { eFormSave030ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave030ProcessComponent],
  imports: [...IMPORTS, eFormSave030ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave030ProcessModule {}
