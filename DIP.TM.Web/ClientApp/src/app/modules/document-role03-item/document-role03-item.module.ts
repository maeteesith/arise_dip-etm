import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole03ItemRoutingModule } from "./document-role03-item-routing.module";
import {
  DocumentRole03ItemListComponent,
  //DocumentRole03ItemComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole03ItemListComponent,
    //DocumentRole03ItemComponent
  ],
  imports: [...IMPORTS, DocumentRole03ItemRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole03ItemModule { }
