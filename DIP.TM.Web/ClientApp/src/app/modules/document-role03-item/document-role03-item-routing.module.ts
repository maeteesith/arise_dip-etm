import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole03ItemListComponent,
  //DocumentRole03ItemComponent
} from "../../pages";

const routes: Routes = [
  //{
  //  path: "",
  //  component: DocumentRole03ItemComponent
  //},
  //{
  //  path: "add",
  //  component: DocumentRole03ItemComponent
  //},
  //{
  //  path: "edit/:id",
  //  component: DocumentRole03ItemComponent
  ////},
  {
    path: "list",
    component: DocumentRole03ItemListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole03ItemRoutingModule { }
