import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02OtherRoutingModule } from "./madrid-role02-other-routing.module";
import { MadridRole02OtherListComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole02OtherListComponent],
  imports: [...IMPORTS, MadridRole02OtherRoutingModule],
  providers: PROVIDERS
})
export class MadridRole02OtherModule {}
