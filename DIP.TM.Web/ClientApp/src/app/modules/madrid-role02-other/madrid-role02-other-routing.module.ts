import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02OtherListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02OtherListComponent
  },
  //{
  //  path: "list",
  //  component: MadridRole02OtherListComponent
  //}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02OtherRoutingModule {}
