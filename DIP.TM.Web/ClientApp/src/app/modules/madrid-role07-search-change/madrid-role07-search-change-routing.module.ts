import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SearchChangeComponent } from "src/app/pages/madrid-role07-search-change/madrid-role07-search-change.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SearchChangeComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SeachChangeRoutingModule {}
