import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SaveListComponent } from "../../pages";
import { MadridRole07CreateDocModalComponent } from "../../pages";
import { MadridRole07SeachChangeRoutingModule } from "./madrid-role07-search-change-routing.module";
import { MadridRole07SearchChangeComponent } from "src/app/pages/madrid-role07-search-change/madrid-role07-search-change.component";
import { MadridRole07ModalEditInformationComponent } from "src/app/pages/madrid-role07-modal-edit-information/madrid-role07-modal-edit-information.component";

@NgModule({
  declarations: [MadridRole07SearchChangeComponent, MadridRole07ModalEditInformationComponent],
  imports: [...IMPORTS, MadridRole07SeachChangeRoutingModule],
  entryComponents: [MadridRole07SearchChangeComponent, MadridRole07ModalEditInformationComponent],
  providers: PROVIDERS
})
export class MadridRole07SearchChangeModule {}
