import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  ConsiderSection56Component,
  ConsiderSection56ListComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: ConsiderSection56Component,
},
{
path: "add",
component: ConsiderSection56Component,
},
{
path: "edit/:id",
component: ConsiderSection56Component,
},
{
path: "list",
component: ConsiderSection56ListComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ConsiderSection56RoutingModule {}
