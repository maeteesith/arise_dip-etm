import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsiderSection56RoutingModule } from './consider-section56-routing.module';
import {
  ConsiderSection56Component,
  ConsiderSection56ListComponent
} from "../../pages";
 
@NgModule({
declarations: [
  ConsiderSection56Component,
  ConsiderSection56ListComponent
],
imports: [...IMPORTS, ConsiderSection56RoutingModule],
providers: PROVIDERS
})
export class ConsiderSection56Module { }
