import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole03ChangeListComponent,
  DocumentRole03ChangeComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole03ChangeComponent
  },
  {
    path: "add",
    component: DocumentRole03ChangeComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole03ChangeComponent
  },
  {
   path: "list",
   component: DocumentRole03ChangeListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole03ChangeRoutingModule {}
