import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole03ChangeRoutingModule } from "./document-role03-change-routing.module";
import {
  DocumentRole03ChangeListComponent,
  DocumentRole03ChangeComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole03ChangeListComponent,
    DocumentRole03ChangeComponent
  ],
  imports: [...IMPORTS, DocumentRole03ChangeRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole03ChangeModule { }
