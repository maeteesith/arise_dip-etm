import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole01ImportComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole01ImportComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole01ImportRoutingModule {}
