import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole01ImportRoutingModule } from "./madrid-role01-import-routing.module";
import { MadridRole01ImportComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole01ImportComponent],
  imports: [...IMPORTS, MadridRole01ImportRoutingModule],
  providers: PROVIDERS
})
export class MadridRole01ImportModule {}
