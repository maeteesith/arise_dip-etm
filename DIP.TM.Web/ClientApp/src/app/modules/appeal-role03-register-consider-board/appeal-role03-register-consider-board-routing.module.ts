import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppealRole03RegisterConsiderBoardComponent } from '../../pages';


const routes: Routes = [
  {
    path: "",
    component: AppealRole03RegisterConsiderBoardComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole03RegisterConsiderBoardRoutingModule { }
