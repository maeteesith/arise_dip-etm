import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole03RegisterConsiderBoardRoutingModule } from './appeal-role03-register-consider-board-routing.module';
import { AppealRole03RegisterConsiderBoardComponent } from '../../pages';

@NgModule({
  declarations: [AppealRole03RegisterConsiderBoardComponent],
  imports: [
    ...IMPORTS,
    AppealRole03RegisterConsiderBoardRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole03RegisterConsiderBoardModule { }
