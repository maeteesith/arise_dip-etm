import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
  AppealRole04DecisionBookCommitteeComponent,
  AppealRole04DecisionBookCommitteeListComponent
} from '../../pages';

const routes: Routes = [
  {
    path: "",
    component: AppealRole04DecisionBookCommitteeComponent
  },
  {
    path: "list",
    component: AppealRole04DecisionBookCommitteeListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04DecisionBookCommitteeRoutingModule { }