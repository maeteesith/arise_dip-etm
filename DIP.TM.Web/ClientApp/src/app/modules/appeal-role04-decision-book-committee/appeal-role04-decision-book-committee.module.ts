import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04DecisionBookCommitteeRoutingModule } from './appeal-role04-decision-book-committee-routing.module';
import { 
  AppealRole04DecisionBookCommitteeComponent,
  AppealRole04DecisionBookCommitteeListComponent
} from '../../pages';

@NgModule({
  declarations: [ 
    AppealRole04DecisionBookCommitteeComponent, 
    AppealRole04DecisionBookCommitteeListComponent ],
  imports: [
    ...IMPORTS,
    AppealRole04DecisionBookCommitteeRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04DecisionBookCommitteeModule { }
