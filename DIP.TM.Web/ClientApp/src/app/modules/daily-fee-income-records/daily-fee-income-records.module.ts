import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DailyFeeIncomeRecordsRoutingModule } from "./daily-fee-income-records-routing.module";
import { DailyFeeIncomeRecordsComponent } from "../../pages";

@NgModule({
  declarations: [DailyFeeIncomeRecordsComponent],
  imports: [...IMPORTS, DailyFeeIncomeRecordsRoutingModule],
  providers: PROVIDERS
})
export class DailyFeeIncomeRecordsModule {}
