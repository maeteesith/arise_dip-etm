import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { DailyFeeIncomeRecordsComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DailyFeeIncomeRecordsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DailyFeeIncomeRecordsRoutingModule {}
