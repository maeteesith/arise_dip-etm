import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  RenewComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: RenewComponent,
},
{
path: "add",
component: RenewComponent,
},
{
path: "edit/:id",
component: RenewComponent,
},
{
path: "list",
component: RenewComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RenewRoutingModule {}