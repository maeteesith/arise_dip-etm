import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RenewRoutingModule } from './renew-routing.module';
import {
  RenewComponent
} from "../../pages";
 
@NgModule({
declarations: [
  RenewComponent
],
imports: [...IMPORTS, RenewRoutingModule],
providers: PROVIDERS
})
export class RenewModule { }