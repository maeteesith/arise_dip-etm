import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  RequestKor08RegistrationComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: RequestKor08RegistrationComponent,
},
{
path: "add",
component: RequestKor08RegistrationComponent,
},
{
path: "edit/:id",
component: RequestKor08RegistrationComponent,
},
{
path: "list",
component: RequestKor08RegistrationComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor08RegistrationRoutingModule { }