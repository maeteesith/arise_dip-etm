import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor08RegistrationRoutingModule } from './request-kor08-registration-routing.module';
import {
  RequestKor08RegistrationComponent
} from "../../pages";

@NgModule({
declarations: [RequestKor08RegistrationComponent],
imports: [...IMPORTS, RequestKor08RegistrationRoutingModule],
providers: PROVIDERS
})
export class RequestKor08RegistrationModule { }