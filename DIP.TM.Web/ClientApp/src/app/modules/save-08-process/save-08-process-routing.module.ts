import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  Save08ProcessComponent,
  Save08ProcessListComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: Save08ProcessComponent
  },
  {
    path: "add",
    component: Save08ProcessComponent
  },
  {
    path: "edit/:id",
    component: Save08ProcessComponent
  },
  {
    path: "list",
    component: Save08ProcessListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Save08ProcessRoutingModule {}
