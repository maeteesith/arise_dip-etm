import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { Save08ProcessRoutingModule } from "./save-08-process-routing.module";
import {
  Save08ProcessComponent,
  Save08ProcessListComponent
} from "../../pages";

@NgModule({
  declarations: [Save08ProcessComponent, Save08ProcessListComponent],
  imports: [...IMPORTS, Save08ProcessRoutingModule],
  providers: PROVIDERS
})
export class Save08ProcessModule {}
