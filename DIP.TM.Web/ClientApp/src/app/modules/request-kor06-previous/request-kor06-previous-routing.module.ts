import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  RequestKor06PreviousComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: RequestKor06PreviousComponent,
},
{
path: "add",
component: RequestKor06PreviousComponent,
},
{
path: "edit/:id",
component: RequestKor06PreviousComponent,
},
{
path: "list",
component: RequestKor06PreviousComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor06PreviousRoutingModule {}