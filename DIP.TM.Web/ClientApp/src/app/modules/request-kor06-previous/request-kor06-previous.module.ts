import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor06PreviousRoutingModule } from './request-kor06-previous-routing.module';
import {
  RequestKor06PreviousComponent
} from "../../pages";
 
@NgModule({
declarations: [
  RequestKor06PreviousComponent
],
imports: [...IMPORTS, RequestKor06PreviousRoutingModule],
providers: PROVIDERS
})
export class RequestKor06PreviousModule { }