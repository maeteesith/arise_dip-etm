import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MadridRole04SearchListComponent} from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole04SearchListComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04SearchListRoutingModule { }
