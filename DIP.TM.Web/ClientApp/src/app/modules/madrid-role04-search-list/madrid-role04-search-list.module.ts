import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole04SearchListRoutingModule } from './madrid-role04-search-list-routing.module';

import {MadridRole04SearchListComponent} from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';
@NgModule({
  declarations: [
    MadridRole04SearchListComponent
  ],
  imports: [...IMPORTS, MadridRole04SearchListRoutingModule],
  providers: PROVIDERS
})
export class MadridRole04SearchListModule { }
