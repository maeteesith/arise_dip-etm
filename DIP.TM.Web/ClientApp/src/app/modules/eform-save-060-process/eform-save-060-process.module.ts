import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave060ProcessRoutingModule } from "./eform-save-060-process-routing.module";
import { eFormSave060ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave060ProcessComponent],
  imports: [...IMPORTS, eFormSave060ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave060ProcessModule {}
