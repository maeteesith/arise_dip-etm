import { ModalMadridRole01ConsiderComponent } from '../../pages/modal-madrid-role01-consider/modal-madrid-role01-consider.component';
import { MatDialog } from '@angular/material';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ModalMadridRole01Page1RoutingModule } from './modal-madrid-role01-page1-routing.module';
import { ModalMadridRole01Page1Component } from 'src/app/pages/modal-madrid-role01-page1/modal-madrid-role01-page1.component';

@NgModule({
  declarations: [ModalMadridRole01Page1Component],
  imports: [...IMPORTS, ModalMadridRole01Page1RoutingModule,MatDialog ],
  entryComponents:[ModalMadridRole01Page1Component],
  providers: PROVIDERS
})
export class MModalMadridRole01Page1Module {}
