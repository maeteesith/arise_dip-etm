import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SaveListComponent } from "../../pages";
import { ModalMadridRole01Page1Component } from "src/app/pages/modal-madrid-role01-page1/modal-madrid-role01-page1.component";

const routes: Routes = [
  {
    path: "",
    component: ModalMadridRole01Page1Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModalMadridRole01Page1RoutingModule {}
