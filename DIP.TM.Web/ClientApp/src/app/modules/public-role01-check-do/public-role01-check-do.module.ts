import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole01CheckDoRoutingModule } from "./public-role01-check-do-routing.module";
import { PublicRole01CheckDoComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole01CheckDoComponent],
  imports: [...IMPORTS, PublicRole01CheckDoRoutingModule],
  providers: PROVIDERS
})
export class PublicRole01CheckDoModule {}
