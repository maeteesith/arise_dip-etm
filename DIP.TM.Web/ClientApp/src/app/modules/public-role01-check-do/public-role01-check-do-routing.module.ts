import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole01CheckDoComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole01CheckDoComponent
  },
  {
    path: "add",
    component: PublicRole01CheckDoComponent
  },
  {
    path: "edit/:id",
    component: PublicRole01CheckDoComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole01CheckDoRoutingModule {}
