import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02DocumentPaymentRoutingModule } from "./public-role02-document-payment-routing.module";
import {
  PublicRole02DocumentPaymentListComponent,
  PublicRole02DocumentPaymentComponent
} from "../../pages";

@NgModule({
  declarations: [
    PublicRole02DocumentPaymentListComponent,
    PublicRole02DocumentPaymentComponent
  ],
  imports: [...IMPORTS, PublicRole02DocumentPaymentRoutingModule],
  providers: PROVIDERS
})
export class PublicRole02DocumentPaymentModule { }
