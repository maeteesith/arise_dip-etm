import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  PublicRole02DocumentPaymentListComponent,
  PublicRole02DocumentPaymentComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole02DocumentPaymentComponent
  },
  {
    path: "add",
    component: PublicRole02DocumentPaymentComponent
  },
  {
    path: "edit/:id",
    component: PublicRole02DocumentPaymentComponent
  },
  {
    path: "list",
    component: PublicRole02DocumentPaymentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole02DocumentPaymentRoutingModule { }
