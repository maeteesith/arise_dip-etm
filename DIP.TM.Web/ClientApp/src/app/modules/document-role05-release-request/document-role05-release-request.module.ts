import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole05ReleaseRequestRoutingModule } from "./document-role05-release-request-routing.module";
import {
  DocumentRole05ReleaseRequestListComponent,
  DocumentRole05ReleaseRequestComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole05ReleaseRequestListComponent,
    DocumentRole05ReleaseRequestComponent
  ],
  imports: [...IMPORTS, DocumentRole05ReleaseRequestRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole05ReleaseRequestModule { }
