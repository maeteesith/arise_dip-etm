import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole05ReleaseRequestListComponent,
  DocumentRole05ReleaseRequestComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole05ReleaseRequestComponent
  },
  {
    path: "add",
    component: DocumentRole05ReleaseRequestComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole05ReleaseRequestComponent
  },
  {
   path: "list",
   component: DocumentRole05ReleaseRequestListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole05ReleaseRequestRoutingModule {}
