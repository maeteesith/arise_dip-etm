import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole05DashboardRoutingModule } from "./madrid-role05-dashboard-routing.module";
import { MadridRole05DashboardComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole05DashboardComponent],
  imports: [...IMPORTS, MadridRole05DashboardRoutingModule],
  providers: PROVIDERS
})
export class MadridRole05DashboardModule {}
