import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole05DashboardComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole05DashboardComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole05DashboardRoutingModule {}
