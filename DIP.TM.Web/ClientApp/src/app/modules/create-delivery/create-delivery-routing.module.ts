import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  CreateDeliveryComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: CreateDeliveryComponent,
},
{
path: "add",
component: CreateDeliveryComponent,
},
{
path: "edit/:id",
component: CreateDeliveryComponent,
},
{
path: "list",
component: CreateDeliveryComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class CreateDeliveryRoutingModule {}