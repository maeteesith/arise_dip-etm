import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { CreateDeliveryRoutingModule } from './create-delivery-routing.module';
import {
  CreateDeliveryComponent
} from "../../pages";
 
@NgModule({
declarations: [
  CreateDeliveryComponent
],
imports: [...IMPORTS, CreateDeliveryRoutingModule],
providers: PROVIDERS
})
export class CreateDeliveryModule { }