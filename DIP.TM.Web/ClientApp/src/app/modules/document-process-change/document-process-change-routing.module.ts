import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
    DocumentProcessChangeListComponent,
    //DocumentProcessChangeComponent
} from "../../pages";

const routes: Routes = [
    {
        //  path: "",
        //  component: DocumentProcessChangeComponent
        //},
        //{
        //  path: "add",
        //  component: DocumentProcessChangeComponent
        //},
        //{
        //  path: "edit/:id",
        //  component: DocumentProcessChangeComponent
        //},
        //{
        path: "list",
        component: DocumentProcessChangeListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DocumentProcessChangeRoutingModule { }
