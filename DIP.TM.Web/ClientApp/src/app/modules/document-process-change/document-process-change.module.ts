import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentProcessChangeRoutingModule } from "./document-process-change-routing.module";
import {
    DocumentProcessChangeListComponent,
    //DocumentProcessChangeComponent
} from "../../pages";

@NgModule({
    declarations: [
        DocumentProcessChangeListComponent,
        //DocumentProcessChangeComponent
    ],
    imports: [...IMPORTS, DocumentProcessChangeRoutingModule],
    providers: PROVIDERS
})
export class DocumentProcessChangeModule { }
