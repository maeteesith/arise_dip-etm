import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  PrintCoverComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: PrintCoverComponent,
},
{
path: "add",
component: PrintCoverComponent,
},
{
path: "edit/:id",
component: PrintCoverComponent,
},
{
path: "list",
component: PrintCoverComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class PrintCoverRoutingModule {}