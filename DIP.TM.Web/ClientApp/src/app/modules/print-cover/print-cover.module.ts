import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PrintCoverRoutingModule } from './print-cover-routing.module';
import {
  PrintCoverComponent
} from "../../pages";
 
@NgModule({
declarations: [
  PrintCoverComponent
],
imports: [...IMPORTS, PrintCoverRoutingModule],
providers: PROVIDERS
})
export class PrintCoverModule { }