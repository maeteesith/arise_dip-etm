import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04AppealRoutingModule } from "./document-role04-appeal-routing.module";
import {
  //DocumentRole04AppealListComponent,
  DocumentRole04AppealComponent
} from "../../pages";

@NgModule({
  declarations: [
    //DocumentRole04AppealListComponent, 
    DocumentRole04AppealComponent
  ],
  imports: [...IMPORTS, DocumentRole04AppealRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04AppealModule { }
