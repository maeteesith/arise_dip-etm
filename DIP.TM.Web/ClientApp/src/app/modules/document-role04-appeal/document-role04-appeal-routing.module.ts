import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //DocumentRole04AppealListComponent,
  DocumentRole04AppealComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole04AppealComponent
  },
  {
    path: "add",
    component: DocumentRole04AppealComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole04AppealComponent
  //},
  //{
  // path: "list",
  // component: DocumentRole04AppealListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04AppealRoutingModule {}
