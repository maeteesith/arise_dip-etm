import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02PrintDocumentRoutingModule } from "./public-role02-print-document-routing.module";
import {
  PublicRole02PrintDocumentListComponent,
  //PublicRole02PrintDocumentComponent
} from "../../pages";

@NgModule({
  declarations: [
    PublicRole02PrintDocumentListComponent,
    //PublicRole02PrintDocumentComponent
  ],
  imports: [...IMPORTS, PublicRole02PrintDocumentRoutingModule],
  providers: PROVIDERS
})
export class PublicRole02PrintDocumentModule { }
