import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  PublicRole02PrintDocumentListComponent,
  //PublicRole02PrintDocumentComponent
} from "../../pages";

const routes: Routes = [
  //{
  //  path: "",
  //  component: PublicRole02PrintDocumentComponent
  //},
  //{
  //  path: "add",
  //  component: PublicRole02PrintDocumentComponent
  //},
  //{
  //  path: "edit/:id",
  //  component: PublicRole02PrintDocumentComponent
  //},
  {
    path: "list",
    component: PublicRole02PrintDocumentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole02PrintDocumentRoutingModule { }
