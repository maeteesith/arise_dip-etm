import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02ConsideringOtherRoutingModule } from "./public-role02-considering-other-routing.module";
import {
    PublicRole02ConsideringOtherListComponent,
    PublicRole02ConsideringOtherComponent
} from "../../pages";

@NgModule({
    declarations: [
        PublicRole02ConsideringOtherListComponent,
        PublicRole02ConsideringOtherComponent
    ],
    imports: [...IMPORTS, PublicRole02ConsideringOtherRoutingModule],
    providers: PROVIDERS
})
export class PublicRole02ConsideringOtherModule { }
