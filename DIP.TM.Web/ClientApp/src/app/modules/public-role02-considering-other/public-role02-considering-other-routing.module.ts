import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
    PublicRole02ConsideringOtherListComponent,
    PublicRole02ConsideringOtherComponent
} from "../../pages";

const routes: Routes = [
    {
        path: "",
        component: PublicRole02ConsideringOtherComponent
    },
    {
        path: "add",
        component: PublicRole02ConsideringOtherComponent
    },
    {
        path: "edit/:id",
        component: PublicRole02ConsideringOtherComponent
    },
    {
        path: "list",
        component: PublicRole02ConsideringOtherListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRole02ConsideringOtherRoutingModule { }
