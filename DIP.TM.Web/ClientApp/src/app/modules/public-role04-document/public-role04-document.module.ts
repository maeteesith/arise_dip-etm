import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole04DocumentRoutingModule } from "./public-role04-document-routing.module";
import { PublicRole04DocumentListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole04DocumentListComponent],
  imports: [...IMPORTS, PublicRole04DocumentRoutingModule],
  providers: PROVIDERS
})
export class PublicRole04DocumentModule {}
