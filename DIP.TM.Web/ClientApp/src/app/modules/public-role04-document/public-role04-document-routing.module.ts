import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole04DocumentListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole04DocumentListComponent
  },
  {
    path: "list",
    component: PublicRole04DocumentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole04DocumentRoutingModule {}
