import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02ActionPostRoutingModule } from "./public-role02-action-post-routing.module";
import { PublicRole02ActionPostListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole02ActionPostListComponent],
  imports: [...IMPORTS, PublicRole02ActionPostRoutingModule],
  providers: PROVIDERS
})
export class PublicRole02ActionPostModule {}
