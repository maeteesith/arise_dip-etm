import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole02ActionPostListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole02ActionPostListComponent
  },
  {
    path: "list",
    component: PublicRole02ActionPostListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole02ActionPostRoutingModule {}
