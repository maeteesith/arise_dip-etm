import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  ReceiptDailyListComponent,
  ReceiptDailyComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: ReceiptDailyComponent
  },
  {
    path: "add",
    component: ReceiptDailyComponent
  },
  {
    path: "edit/:id",
    component: ReceiptDailyComponent
  },
  {
    path: "list",
    component: ReceiptDailyListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReceiptDailyRoutingModule { }
