import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ReceiptDailyRoutingModule } from "./receipt-daily-routing.module";
import {
  ReceiptDailyListComponent,
  ReceiptDailyComponent
} from "../../pages";

@NgModule({
  declarations: [
    ReceiptDailyComponent,
    ReceiptDailyListComponent],
  imports: [...IMPORTS, ReceiptDailyRoutingModule],
  providers: PROVIDERS
})
export class ReceiptDailyModule { }
