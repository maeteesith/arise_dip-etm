import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { R2RequestKor04ChangeRoutingModule } from './r2-request-kor04-change-routing.module';
import {
  R2RequestKor04ChangeComponent
} from "../../pages";

@NgModule({
declarations: [R2RequestKor04ChangeComponent],
imports: [...IMPORTS, R2RequestKor04ChangeRoutingModule],
providers: PROVIDERS
})
export class R2RequestKor04ChangeModule { }