import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  R2RequestKor04ChangeComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: R2RequestKor04ChangeComponent,
},
{
path: "add",
component: R2RequestKor04ChangeComponent,
},
{
path: "edit/:id",
component: R2RequestKor04ChangeComponent,
},
{
path: "list",
component: R2RequestKor04ChangeComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class R2RequestKor04ChangeRoutingModule { }
