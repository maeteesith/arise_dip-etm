import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MadridRole03SearchFinishWorkComponent } from "../../pages";
import { MadridRole03SearchFinishWorkRoutingModule } from './madrid-role03-search-finish-work-routing.module';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';

@NgModule({
  declarations: [MadridRole03SearchFinishWorkComponent],
  imports: [...IMPORTS, MadridRole03SearchFinishWorkRoutingModule],
  providers: PROVIDERS
})
export class MadridRole03SearchFinishWorkModule { }
