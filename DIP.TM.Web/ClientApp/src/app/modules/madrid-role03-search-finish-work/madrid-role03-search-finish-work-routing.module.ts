import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole03SearchFinishWorkComponent } from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole03SearchFinishWorkComponent
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole03SearchFinishWorkRoutingModule { }
