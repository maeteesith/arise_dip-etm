import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole04SearchChangeRoutingModule } from './madrid-role04-search-change-routing.module';
import {MadridRole04ChangeComponent} from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';


@NgModule({
  declarations: [
    MadridRole04ChangeComponent
  ],
  imports: [...IMPORTS, MadridRole04SearchChangeRoutingModule],
  providers: PROVIDERS
})
export class MadridRole04SearchChangeModule { }
