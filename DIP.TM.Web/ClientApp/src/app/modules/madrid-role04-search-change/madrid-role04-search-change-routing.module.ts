import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MadridRole04ChangeComponent} from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole04ChangeComponent
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04SearchChangeRoutingModule { }
