import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02DocumentRoutingModule } from "./public-role02-document-routing.module";
import { PublicRole02DocumentListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole02DocumentListComponent],
  imports: [...IMPORTS, PublicRole02DocumentRoutingModule],
  providers: PROVIDERS
})
export class PublicRole02DocumentModule {}
