import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole02DocumentListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole02DocumentListComponent
  },
  {
    path: "list",
    component: PublicRole02DocumentListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole02DocumentRoutingModule {}
