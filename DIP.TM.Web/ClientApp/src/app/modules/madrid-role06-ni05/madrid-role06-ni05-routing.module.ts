import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06Ni05ListComponent } from "../../pages";


const routes: Routes = [
  {
    path: "",
    component: MadridRole06Ni05ListComponent
  },
  {
    path: "list",
    component: MadridRole06Ni05ListComponent
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06Ni05RoutingModule { }
