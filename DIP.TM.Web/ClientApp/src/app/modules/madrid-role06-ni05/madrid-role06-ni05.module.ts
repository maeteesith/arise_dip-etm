import { NgModule } from '@angular/core';
import { MadridRole06Ni05RoutingModule } from './madrid-role06-ni05-routing.module';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06Ni05ListComponent } from "../../pages";


@NgModule({
  declarations: [MadridRole06Ni05ListComponent],
  imports: [...IMPORTS, MadridRole06Ni05RoutingModule],
  providers: PROVIDERS
})
export class MadridRole06Ni05Module { }
