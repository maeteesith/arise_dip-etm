import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  CheckIssueBookComponent,
  CheckIssueBookListComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: CheckIssueBookComponent,
},
{
path: "add",
component: CheckIssueBookComponent,
},
{
path: "edit/:id",
component: CheckIssueBookComponent,
},
{
path: "list",
component: CheckIssueBookListComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class CheckIssueBookRoutingModule {}