import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { CheckIssueBookRoutingModule } from './check-issue-book-routing.module';
import {
  CheckIssueBookComponent,
  CheckIssueBookListComponent
} from "../../pages";
 
@NgModule({
declarations: [
  CheckIssueBookComponent,
  CheckIssueBookListComponent
],
imports: [...IMPORTS, CheckIssueBookRoutingModule],
providers: PROVIDERS
})
export class CheckIssueBookModule { }
