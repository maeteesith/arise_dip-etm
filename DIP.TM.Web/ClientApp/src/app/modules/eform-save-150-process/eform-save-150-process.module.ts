import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave150ProcessRoutingModule } from "./eform-save-150-process-routing.module";
import { eFormSave150ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave150ProcessComponent],
  imports: [...IMPORTS, eFormSave150ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave150ProcessModule {}
