import { ModalMadridRole01CheckValueComponent } from './../../pages/modal-madrid-role01-check-value/modal-madrid-role01-check-value.component';
import { ModalMadridRole01Page1Component } from './../../pages/modal-madrid-role01-page1/modal-madrid-role01-page1.component';
import { ModalMadridRole01ConsiderComponent } from './../../pages/modal-madrid-role01-consider/modal-madrid-role01-consider.component';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole01ImportSearchRoutingModule } from "./madrid-role01-import-search-routing.module";
import { MadridRole01ImportSearchComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole01ImportSearchComponent,ModalMadridRole01ConsiderComponent,ModalMadridRole01Page1Component,ModalMadridRole01CheckValueComponent],
  imports: [...IMPORTS, MadridRole01ImportSearchRoutingModule],
  entryComponents:[ModalMadridRole01ConsiderComponent,ModalMadridRole01Page1Component,ModalMadridRole01CheckValueComponent],
  providers: PROVIDERS
})
export class MadridRole01ImportSearchModule {}
