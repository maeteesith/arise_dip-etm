import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole01ImportSearchComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole01ImportSearchComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole01ImportSearchRoutingModule {}
