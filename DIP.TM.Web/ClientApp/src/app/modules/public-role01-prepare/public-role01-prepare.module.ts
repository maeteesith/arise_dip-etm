import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole01PrepareRoutingModule } from "./public-role01-prepare-routing.module";
import { PublicRole01PrepareListComponent } from "../../pages";

@NgModule({
  declarations: [PublicRole01PrepareListComponent],
  imports: [...IMPORTS, PublicRole01PrepareRoutingModule],
  providers: PROVIDERS
})
export class PublicRole01PrepareModule {}
