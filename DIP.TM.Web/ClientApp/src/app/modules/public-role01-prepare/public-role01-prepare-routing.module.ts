import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { PublicRole01PrepareListComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole01PrepareListComponent
  },
  {
    path: "list",
    component: PublicRole01PrepareListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole01PrepareRoutingModule {}
