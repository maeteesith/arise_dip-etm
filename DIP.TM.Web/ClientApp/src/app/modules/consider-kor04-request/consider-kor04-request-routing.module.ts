import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  ConsiderKor04RequestComponent,
  ConsiderKor04RequestListComponent,
  ConsiderKor04RequestEditComponent
} from "../../pages";

const routes: Routes = [
{
path: "",
component: ConsiderKor04RequestComponent,
},
{
path: "add",
component: ConsiderKor04RequestComponent,
},
{
// path: "edit/:id",
path: "edit",
component: ConsiderKor04RequestEditComponent,
},
{
path: "list",
component: ConsiderKor04RequestListComponent,
},
];

@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class ConsiderKor04RequestRoutingModule {}