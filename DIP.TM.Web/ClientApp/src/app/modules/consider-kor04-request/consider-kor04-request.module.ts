import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsiderKor04RequestRoutingModule } from './consider-kor04-request-routing.module';
import {
  ConsiderKor04RequestComponent,
  ConsiderKor04RequestListComponent,
  ConsiderKor04RequestEditComponent
} from "../../pages";

@NgModule({
declarations: [
  ConsiderKor04RequestComponent,
  ConsiderKor04RequestListComponent,
  ConsiderKor04RequestEditComponent
],
imports: [...IMPORTS, ConsiderKor04RequestRoutingModule],
providers: PROVIDERS
})
export class ConsiderKor04RequestModule { }