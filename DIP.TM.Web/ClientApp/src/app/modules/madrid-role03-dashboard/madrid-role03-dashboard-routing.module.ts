import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole03DashboardComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole03DashboardComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole03DashboardRoutingModule {}
