import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole03DashboardRoutingModule } from "./madrid-role03-dashboard-routing.module";
import { MadridRole03DashboardComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole03DashboardComponent],
  imports: [...IMPORTS, MadridRole03DashboardRoutingModule],
  providers: PROVIDERS
})
export class MadridRole03DashboardModule {}
