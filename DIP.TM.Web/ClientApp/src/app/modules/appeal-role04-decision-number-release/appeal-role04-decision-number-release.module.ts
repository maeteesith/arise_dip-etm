import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole04DecisionNumberReleaseRoutingModule } from './appeal-role04-decision-number-release-routing.module';
import { 
  AppealRole04DecisionNumberReleaseComponent,
  AppealRole04DecisionNumberReleaseListComponent
} from '../../pages';



@NgModule({
  declarations: [ AppealRole04DecisionNumberReleaseListComponent, AppealRole04DecisionNumberReleaseComponent ],
  imports: [
    ...IMPORTS,
    AppealRole04DecisionNumberReleaseRoutingModule
  ],
  providers: PROVIDERS
})
export class AppealRole04DecisionNumberReleaseModule { }
