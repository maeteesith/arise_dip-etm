import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { 
    AppealRole04DecisionNumberReleaseComponent,
    AppealRole04DecisionNumberReleaseListComponent
  } from '../../pages';
 
const routes: Routes = [
  {
    path: "",
    component: AppealRole04DecisionNumberReleaseComponent
  },
  {
    path: "list",
    component: AppealRole04DecisionNumberReleaseListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole04DecisionNumberReleaseRoutingModule { }