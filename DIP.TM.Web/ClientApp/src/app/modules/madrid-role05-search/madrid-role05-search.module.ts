import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole05SearchComponent,MadridRole05ModalComponent, MadridRole05DocAttachComponent,MadridRole05PaymentComponent,MadridRole05FeeComponent } from "../../pages";
import { MatTabsModule } from '@angular/material/tabs'
import { ConfirmationDialogComponent } from '../../shared';

import { MadridRole05SearchRoutingModule } from './madrid-role05-search-routing.module';


@NgModule({
  declarations: [MadridRole05SearchComponent,MadridRole05ModalComponent, MadridRole05DocAttachComponent,MadridRole05PaymentComponent,MadridRole05FeeComponent],
  imports: [...IMPORTS, MadridRole05SearchRoutingModule,MatTabsModule],
  entryComponents: [MadridRole05ModalComponent, MadridRole05DocAttachComponent,MadridRole05PaymentComponent,MadridRole05FeeComponent],
  providers: PROVIDERS
})
export class MadridRole05SearchModule { }
