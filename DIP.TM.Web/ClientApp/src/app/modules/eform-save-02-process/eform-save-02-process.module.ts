import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave02ProcessRoutingModule } from "./eform-save-02-process-routing.module";
import { eFormSave02ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave02ProcessComponent],
  imports: [...IMPORTS, eFormSave02ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave02ProcessModule {}
