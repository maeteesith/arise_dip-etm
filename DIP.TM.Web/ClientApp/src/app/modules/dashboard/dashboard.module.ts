import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "../../pages";

@NgModule({
  declarations: [DashboardComponent],
  imports: [...IMPORTS, DashboardRoutingModule],
  providers: PROVIDERS
})
export class DashboardModule {}
