import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SaveListComponent } from "../../pages";
import { MadridRole07SearchRecieveIrnComponent } from "src/app/pages/madrid-role07-search-recieve-irn/madrid-role07-search-recieve-irn.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SearchRecieveIrnComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SeachReceiveIrnRoutingModule {}
