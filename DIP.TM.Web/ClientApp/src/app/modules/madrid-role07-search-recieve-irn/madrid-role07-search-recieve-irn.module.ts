import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SeachReceiveIrnRoutingModule } from "./madrid-role07-search-recieve-irn-routing.module";
import { MadridRole07CreateDocModalComponent } from "../../pages";
import { MadridRole07SearchRecieveIrnComponent } from "src/app/pages/madrid-role07-search-recieve-irn/madrid-role07-search-recieve-irn.component";

@NgModule({
  declarations: [MadridRole07SearchRecieveIrnComponent],
  imports: [...IMPORTS, MadridRole07SeachReceiveIrnRoutingModule],
  entryComponents: [MadridRole07SearchRecieveIrnComponent],
  providers: PROVIDERS
})
export class MadridRole07SearchRecieveIrnModule {}
