import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RequestKor04PreviousRoutingModule } from './request-kor04-previous-routing.module';
import {
  RequestKor04PreviousComponent
} from "../../pages";
 
@NgModule({
declarations: [
  RequestKor04PreviousComponent
],
imports: [...IMPORTS, RequestKor04PreviousRoutingModule],
providers: PROVIDERS
})
export class RequestKor04PreviousModule { }