import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { 
  RequestKor04PreviousComponent
} from "../../pages";
 
const routes: Routes = [
{
path: "",
component: RequestKor04PreviousComponent,
},
{
path: "add",
component: RequestKor04PreviousComponent,
},
{
path: "edit/:id",
component: RequestKor04PreviousComponent,
},
{
path: "list",
component: RequestKor04PreviousComponent,
},
];
 
@NgModule({
imports: [RouterModule.forChild(routes)],
exports: [RouterModule],
})
export class RequestKor04PreviousRoutingModule {}