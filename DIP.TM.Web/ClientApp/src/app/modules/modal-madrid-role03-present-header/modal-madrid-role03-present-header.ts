import { ModalMadridRole03PresentHeader } from './../../pages/modal-madrid-role03-present-header/modal-madrid-role03-present-header.component';
import { ModalMadridRole03EditDetail } from '../../pages/modal-madrid-role03-edit-detail/modal-madrid-role03-edit-detail.component';


import { MatDialog } from '@angular/material';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";


@NgModule({
  declarations: [ModalMadridRole03PresentHeader],
  imports: [...IMPORTS, ModalMadridRole03PresentHeader,MatDialog ],
  entryComponents:[ModalMadridRole03PresentHeader],
  providers: PROVIDERS
})
export class ModalMadridRole03PresentHeaderRoutingModule {}
