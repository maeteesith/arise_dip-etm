import { ModalMadridRole03PresentHeader } from './../../pages/modal-madrid-role03-present-header/modal-madrid-role03-present-header.component';
import { ModalMadridRole03EditDetail } from '../../pages/modal-madrid-role03-edit-detail/modal-madrid-role03-edit-detail.component';

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";


const routes: Routes = [
  {
    path: "",
    component: ModalMadridRole03PresentHeader
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModalMadridRole03PresentHeaderRoutingModule {}
