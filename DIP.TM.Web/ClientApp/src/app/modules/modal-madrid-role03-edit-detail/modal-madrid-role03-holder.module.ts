import { ModalMadridRole03EditDetail } from './../../pages/modal-madrid-role03-edit-detail/modal-madrid-role03-edit-detail.component';


import { MatDialog } from '@angular/material';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";


@NgModule({
  declarations: [ModalMadridRole03EditDetail],
  imports: [...IMPORTS, ModalMadridRole03EditDetail,MatDialog ],
  entryComponents:[ModalMadridRole03EditDetail],
  providers: PROVIDERS
})
export class ModalMadridRole03EditDetailRoutingModule {}
