import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole03SearchGroupLeaderComponent } from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole03SearchGroupLeaderComponent
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole03SearchGroupLeaderRoutingModule { }
