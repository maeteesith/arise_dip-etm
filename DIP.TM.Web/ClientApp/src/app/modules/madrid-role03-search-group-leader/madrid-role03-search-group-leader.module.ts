import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MadridRole03SearchGroupLeaderComponent } from "../../pages";
import { MadridRole03SearchGroupLeaderRoutingModule } from './madrid-role03-search-group-leader-routing.module';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';


@NgModule({
  declarations: [MadridRole03SearchGroupLeaderComponent],
  imports: [...IMPORTS, MadridRole03SearchGroupLeaderRoutingModule],
  providers: PROVIDERS
})
export class MadridRole03SearchGroupLeaderModule { }
