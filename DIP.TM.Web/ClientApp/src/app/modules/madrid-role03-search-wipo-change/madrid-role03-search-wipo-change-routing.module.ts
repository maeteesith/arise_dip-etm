import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole03SearchWIPOChangeComponent } from "../../pages";

const routes: Routes = [ {
  path: "",
  component: MadridRole03SearchWIPOChangeComponent
},];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole03SearchWIPOChangeRoutingModule { }
