import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MadridRole03SearchWIPOChangeComponent } from "../../pages";
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';
import { MadridRole03SearchWIPOChangeRoutingModule } from './madrid-role03-search-wipo-change-routing.module';

@NgModule({
  declarations: [MadridRole03SearchWIPOChangeComponent],
  imports: [...IMPORTS, MadridRole03SearchWIPOChangeRoutingModule],
  providers: PROVIDERS
})
export class MadridRole03SearchWIPOChangeModule { }
