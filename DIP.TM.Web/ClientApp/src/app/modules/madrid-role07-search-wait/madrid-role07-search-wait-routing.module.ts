import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole07SaveListComponent } from "../../pages";
import { MadridRole07SearchWaitComponent } from "src/app/pages/madrid-role07-search-wait/madrid-role07-search-wait.component";

const routes: Routes = [
  {
    path: "",
    component: MadridRole07SearchWaitComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole07SearchWaitRoutingModule {}
