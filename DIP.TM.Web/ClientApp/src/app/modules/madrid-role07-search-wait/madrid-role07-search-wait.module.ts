import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole07SearchWaitComponent } from "src/app/pages/madrid-role07-search-wait/madrid-role07-search-wait.component";
import { MadridRole07SearchWaitRoutingModule } from "./madrid-role07-search-wait-routing.module";

@NgModule({
  declarations: [MadridRole07SearchWaitComponent],
  imports: [...IMPORTS, MadridRole07SearchWaitRoutingModule],
  entryComponents: [MadridRole07SearchWaitComponent],
  providers: PROVIDERS
})
export class MadridRole07SearchWaitModule {}
