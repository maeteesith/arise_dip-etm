import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { eFormHomeComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: eFormHomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class eFormHomeRoutingModule {}
