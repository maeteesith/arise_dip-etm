import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormHomeRoutingModule } from "./eform-home-routing.module";
import { eFormHomeComponent } from "../../pages";

@NgModule({
  declarations: [eFormHomeComponent],
  imports: [...IMPORTS, eFormHomeRoutingModule],
  providers: PROVIDERS
})
export class eFormHomeModule {}
