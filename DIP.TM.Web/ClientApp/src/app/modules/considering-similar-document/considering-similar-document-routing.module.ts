import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ConsideringSimilarDocumentComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: ConsideringSimilarDocumentComponent
  },
  {
    path: "add",
    component: ConsideringSimilarDocumentComponent
  },
  {
    path: "edit/:id",
    component: ConsideringSimilarDocumentComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsideringSimilarDocumentRoutingModule {}
