import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsideringSimilarDocumentRoutingModule } from "./considering-similar-document-routing.module";
import { ConsideringSimilarDocumentComponent } from "../../pages";

@NgModule({
  declarations: [ConsideringSimilarDocumentComponent],
  imports: [...IMPORTS, ConsideringSimilarDocumentRoutingModule],
  providers: PROVIDERS
})
export class ConsideringSimilarDocumentModule {}
