import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MadridRole07DocEditRoutingModule } from './madrid-role07-doc-edit-routing.module';
import { MadridRole07DocEditComponent } from 'src/app/pages/madrid-role07-doc-edit/madrid-role07-doc-edit.component';
import { IMPORTS } from '../imports';
import { PROVIDERS } from '../providers';



@NgModule({
  declarations: [MadridRole07DocEditComponent],
  imports: [...IMPORTS,MadridRole07DocEditRoutingModule],
  providers: PROVIDERS
})
export class MadridRole07DocEditModule { }
