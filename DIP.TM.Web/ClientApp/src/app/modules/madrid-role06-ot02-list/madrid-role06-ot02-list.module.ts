import { NgModule } from '@angular/core';
import { PROVIDERS } from '../providers';
import { IMPORTS } from '../imports';
import { MadridRole06OT02RoutingModule } from './madrid-role06-ot02-list-routing.module';
import { MadridRole06Ot02ListComponent } from 'src/app/pages/madrid-role06-ot02-list/madrid-role06-ot02-list.component';
import { ModalMadridRole06LicenseComponent } from 'src/app/pages/modal-madrid-role06-license/modal-madrid-role06-license.component';


@NgModule({
  declarations: [MadridRole06Ot02ListComponent, ModalMadridRole06LicenseComponent],
  imports: [...IMPORTS, MadridRole06OT02RoutingModule],
  entryComponents: [ModalMadridRole06LicenseComponent],
  providers: PROVIDERS
})
export class MadridRole06OT02Module { }
