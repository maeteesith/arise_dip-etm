import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MadridRole06Ot02ListComponent } from 'src/app/pages/madrid-role06-ot02-list/madrid-role06-ot02-list.component';


const routes: Routes = [
  {
    path: "",
    component: MadridRole06Ot02ListComponent
  },
  {
    path: "list",
    component: MadridRole06Ot02ListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole06OT02RoutingModule { }
