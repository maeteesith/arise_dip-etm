import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { eFormSave220ProcessRoutingModule } from "./eform-save-220-process-routing.module";
import { eFormSave220ProcessComponent } from "../../pages";

@NgModule({
  declarations: [eFormSave220ProcessComponent],
  imports: [...IMPORTS, eFormSave220ProcessRoutingModule],
  providers: PROVIDERS
})
export class eFormSave220ProcessModule {}
