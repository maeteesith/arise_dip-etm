import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole02SearchEditRoutingModule } from "./madrid-role02-search-edit-routing.module";
import { MadridRole02SearchEditComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole02SearchEditComponent],
  imports: [...IMPORTS, MadridRole02SearchEditRoutingModule],
  providers: PROVIDERS
})
export class MadridRole02SearchEditModule {}
