import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole02SearchEditComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole02SearchEditComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole02SearchEditRoutingModule {}
