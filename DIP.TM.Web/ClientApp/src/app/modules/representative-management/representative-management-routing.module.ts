import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RepresentativeManagementComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RepresentativeManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RepresentativeManagementRoutingModule {}
