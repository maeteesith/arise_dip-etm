import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RepresentativeManagementRoutingModule } from "./representative-management-routing.module";
import { RepresentativeManagementComponent } from "../../pages";

@NgModule({
  declarations: [RepresentativeManagementComponent],
  imports: [...IMPORTS, RepresentativeManagementRoutingModule],
  providers: PROVIDERS
})
export class RepresentativeManagementModule {}
