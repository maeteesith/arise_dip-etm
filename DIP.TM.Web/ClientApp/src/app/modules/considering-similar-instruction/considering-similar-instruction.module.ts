import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ConsideringSimilarInstructionRoutingModule } from "./considering-similar-instruction-routing.module";
import { ConsideringSimilarInstructionComponent } from "../../pages";

@NgModule({
  declarations: [ConsideringSimilarInstructionComponent],
  imports: [...IMPORTS, ConsideringSimilarInstructionRoutingModule],
  providers: PROVIDERS
})
export class ConsideringSimilarInstructionModule {}
