import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ConsideringSimilarInstructionComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: ConsideringSimilarInstructionComponent
  },
  {
    path: "add",
    component: ConsideringSimilarInstructionComponent
  },
  {
    path: "edit/:id",
    component: ConsideringSimilarInstructionComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConsideringSimilarInstructionRoutingModule {}
