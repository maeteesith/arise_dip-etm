import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  DocumentRole04ReleaseRequestListComponent,
  DocumentRole04ReleaseRequestComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: DocumentRole04ReleaseRequestComponent
  },
  {
    path: "add",
    component: DocumentRole04ReleaseRequestComponent
  },
  {
    path: "edit/:id",
    component: DocumentRole04ReleaseRequestComponent
  },
  {
    path: "list",
    component: DocumentRole04ReleaseRequestListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentRole04ReleaseRequestRoutingModule { }
