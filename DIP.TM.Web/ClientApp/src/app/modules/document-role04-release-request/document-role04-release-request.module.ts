import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { DocumentRole04ReleaseRequestRoutingModule } from "./document-role04-release-request-routing.module";
import {
  DocumentRole04ReleaseRequestListComponent,
  DocumentRole04ReleaseRequestComponent
} from "../../pages";

@NgModule({
  declarations: [
    DocumentRole04ReleaseRequestListComponent,
    DocumentRole04ReleaseRequestComponent
  ],
  imports: [...IMPORTS, DocumentRole04ReleaseRequestRoutingModule],
  providers: PROVIDERS
})
export class DocumentRole04ReleaseRequestModule { }
