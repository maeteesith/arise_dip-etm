import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";

/* lib */
import { NgxPaginationModule } from "ngx-pagination";
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from "ngx-toastr";
import { ClickOutsideModule } from "ng-click-outside";
import { CKEditorModule } from "@ckeditor/ckeditor5-angular";
import { ArchwizardModule } from 'angular-archwizard';

/* shared */
import { SharedModule } from "../shared/shared.module";

/* pipe */
import { PipeModule } from "../helpers/pipe/pipe.module";

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


/* angular material */
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { DragDropModule } from "@angular/cdk/drag-drop";
import {
  MatDatepickerModule,
  MatAutocompleteModule,
  MatNativeDateModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatStepperModule,
  MatInputModule,
  MatRippleModule,
  MatTooltipModule

} from "@angular/material";

export const IMPORTS = [
  CommonModule,
  FormsModule,
  NgxPaginationModule,
  NgxSpinnerModule,
  ToastrModule.forRoot(),
  ClickOutsideModule,
  CKEditorModule,
  DragDropModule,
  MatDatepickerModule,
  MatAutocompleteModule,
  MatNativeDateModule,
  MatMomentDateModule,
  MatFormFieldModule,
  MatToolbarModule,
  MatButtonModule,
  MatSidenavModule,
  MatIconModule,
  MatListModule,
  MatStepperModule,
  MatInputModule,
  MatRippleModule,
  PipeModule,
  SharedModule,
  MatTooltipModule,
  NgbModule,
  ArchwizardModule
];
