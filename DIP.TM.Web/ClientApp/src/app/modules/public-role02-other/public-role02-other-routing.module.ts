import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
    PublicRole02OtherListComponent,
    //PublicRole02OtherComponent
} from "../../pages";

const routes: Routes = [
    {
        //  path: "",
        //  component: PublicRole02OtherComponent
        //},
        //{
        //  path: "add",
        //  component: PublicRole02OtherComponent
        //},
        //{
        //  path: "edit/:id",
        //  component: PublicRole02OtherComponent
        //},
        //{
        path: "list",
        component: PublicRole02OtherListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PublicRole02OtherRoutingModule { }
