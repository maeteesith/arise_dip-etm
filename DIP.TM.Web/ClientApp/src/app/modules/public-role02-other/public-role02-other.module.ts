import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole02OtherRoutingModule } from "./public-role02-other-routing.module";
import {
    PublicRole02OtherListComponent,
    //PublicRole02OtherComponent
} from "../../pages";

@NgModule({
    declarations: [
        PublicRole02OtherListComponent,
        //PublicRole02OtherComponent
    ],
    imports: [...IMPORTS, PublicRole02OtherRoutingModule],
    providers: PROVIDERS
})
export class PublicRole02OtherModule { }
