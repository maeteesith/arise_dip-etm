import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { RegistrationOtherRequestRoutingModule } from "./registration-other-request-routing.module";
import { RegistrationOtherRequestComponent } from "../../pages";

@NgModule({
  declarations: [RegistrationOtherRequestComponent],
  imports: [...IMPORTS, RegistrationOtherRequestRoutingModule],
  providers: PROVIDERS
})
export class RegistrationOtherRequestModule {}
