import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { RegistrationOtherRequestComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: RegistrationOtherRequestComponent
  },
  {
    path: "add",
    component: RegistrationOtherRequestComponent
  },
  {
    path: "edit/:id",
    component: RegistrationOtherRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistrationOtherRequestRoutingModule {}
