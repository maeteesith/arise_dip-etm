import { ModalMadridRole03FileAttachment } from './../../pages/modal-madrid-role03-file-attachment/modal-madrid-role03-file-attachment.component';
import { ModalMadridRole01ConsiderComponent } from '../../pages/modal-madrid-role01-consider/modal-madrid-role01-consider.component';
import { MatDialog } from '@angular/material';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";


@NgModule({
  declarations: [ModalMadridRole03FileAttachment],
  imports: [...IMPORTS, ModalMadridRole03FileAttachmentRoutingModule,MatDialog ],
  entryComponents:[ModalMadridRole03FileAttachment],
  providers: PROVIDERS
})
export class ModalMadridRole03FileAttachmentRoutingModule {}
