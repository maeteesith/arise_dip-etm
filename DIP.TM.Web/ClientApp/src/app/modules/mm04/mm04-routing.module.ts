import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Mm04Component } from 'src/app/pages/mm04/mm04.component';


const routes: Routes = [
  {
    path: "",
    component: Mm04Component
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Mm04RoutingModule { }
