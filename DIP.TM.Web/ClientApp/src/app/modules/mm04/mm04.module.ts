import { Mm04_01_2_modalComponent } from './../../pages/mm04_01-2-modal/mm04_01-2-modal.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IMPORTS } from '../imports';
import { RouterModule } from '@angular/router';
import { PROVIDERS } from '../providers';
import { Mm02Component } from 'src/app/pages/mm02/mm02.component';
import {  Mm04RoutingModule } from './mm04-routing.module';
import { Mm02_01_4_modalComponent } from 'src/app/pages/mm02_01-4-modal/mm02_01-4-modal.component';
import { Mm02_01_2_modalComponent } from 'src/app/pages/mm02_01-2-modal/mm02_01-2-modal.component';
import { Mm04Component } from 'src/app/pages/mm04/mm04.component';



@NgModule({
    declarations: [Mm04Component,Mm04_01_2_modalComponent],
    imports: [...IMPORTS, Mm04RoutingModule],
    entryComponents:[Mm04_01_2_modalComponent],
    providers: PROVIDERS
})
export class Mm04Module { }
