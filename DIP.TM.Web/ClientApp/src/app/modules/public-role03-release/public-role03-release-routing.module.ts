import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  //PublicRole03ReleaseListComponent,
  PublicRole03ReleaseComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: PublicRole03ReleaseComponent
  },
  {
    path: "add",
    component: PublicRole03ReleaseComponent
  },
  {
    path: "edit/:id",
    component: PublicRole03ReleaseComponent
  //},
  //{
  // path: "list",
  // component: PublicRole03ReleaseListComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PublicRole03ReleaseRoutingModule {}
