import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { PublicRole03ReleaseRoutingModule } from "./public-role03-release-routing.module";
import {
  //PublicRole03ReleaseListComponent,
  PublicRole03ReleaseComponent
} from "../../pages";

@NgModule({
  declarations: [
    //PublicRole03ReleaseListComponent, 
    PublicRole03ReleaseComponent
  ],
  imports: [...IMPORTS, PublicRole03ReleaseRoutingModule],
  providers: PROVIDERS
})
export class PublicRole03ReleaseModule { }
