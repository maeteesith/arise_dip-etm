import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole04DashboardComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole04DashboardComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04DashboardRoutingModule {}
