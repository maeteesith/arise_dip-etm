import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole04DashboardRoutingModule } from "./madrid-role04-dashboard-routing.module";
import { MadridRole04DashboardComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole04DashboardComponent],
  imports: [...IMPORTS, MadridRole04DashboardRoutingModule],
  providers: PROVIDERS
})
export class MadridRole04DashboardModule {}
