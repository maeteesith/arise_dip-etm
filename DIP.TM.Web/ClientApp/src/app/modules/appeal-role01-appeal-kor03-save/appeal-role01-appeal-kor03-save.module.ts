import { NgModule } from '@angular/core';
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { AppealRole01AppealKor03SaveRoutingModule } from "./appeal-role01-appeal-kor03-save-routing.module";
import { 
  AppealRole01AppealKor03SaveComponent
 } from "../../pages";

@NgModule({
  declarations: [
    AppealRole01AppealKor03SaveComponent
  ],
  imports: [...IMPORTS, AppealRole01AppealKor03SaveRoutingModule],
  providers: PROVIDERS
})

export class AppealRole01AppealKor03SaveModule { }
