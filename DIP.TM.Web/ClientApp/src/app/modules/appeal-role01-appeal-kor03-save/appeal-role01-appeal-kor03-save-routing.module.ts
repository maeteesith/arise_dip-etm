import { NgModule } from '@angular/core';
import { Routes, RouterModule } from "@angular/router";
import { 
  AppealRole01AppealKor03SaveComponent,
 } from "../../pages";
const routes: Routes = [
  {
    path: "",
    component: AppealRole01AppealKor03SaveComponent
  },
  {
    path: "edit/:id",
    component: AppealRole01AppealKor03SaveComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppealRole01AppealKor03SaveRoutingModule { }
