import { ModalMadridRole03FileAttachment } from './../../pages/modal-madrid-role03-file-attachment/modal-madrid-role03-file-attachment.component';
import { ModalAttachFileArrayComponent } from './../../pages/modal-attach-file-array/modal-attach-file-array.component';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole03SearchRoutingModule } from "./madrid-role03-search-routing.module";
import { MadridRole03SearchComponent } from "../../pages";

@NgModule({
  declarations: [MadridRole03SearchComponent,ModalMadridRole03FileAttachment],
  imports: [...IMPORTS, MadridRole03SearchRoutingModule],
  entryComponents:[ModalMadridRole03FileAttachment],
  providers: PROVIDERS
})
export class MadridRole03SearchModule {}
