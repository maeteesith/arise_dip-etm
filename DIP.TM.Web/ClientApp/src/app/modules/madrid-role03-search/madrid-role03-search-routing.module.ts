import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MadridRole03SearchComponent } from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole03SearchComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole03SearchRoutingModule {}
