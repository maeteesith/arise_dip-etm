import { ModalMadridRole03Holder } from './../../pages/modal-madrid-role03-holder/modal-madrid-role03-holder.component';

import { MatDialog } from '@angular/material';
import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { ModalMadridRole06LicenseRoutingModule } from './modal-madrid-role06-license-routing.module';
import { ModalMadridRole06LicenseComponent } from 'src/app/pages/modal-madrid-role06-license/modal-madrid-role06-license.component';


@NgModule({
  declarations: [ModalMadridRole06LicenseComponent],
  imports: [...IMPORTS, ModalMadridRole06LicenseRoutingModule,MatDialog ],
  entryComponents:[ModalMadridRole06LicenseComponent],
  providers: PROVIDERS
})
export class ModalMadridRole06LicenseModule {}
