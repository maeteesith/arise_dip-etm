import { ModalMadridRole03FileAttachment } from '../../pages/modal-madrid-role03-file-attachment/modal-madrid-role03-file-attachment.component';
import { ModalMadridRole01ConsiderComponent } from '../../pages/modal-madrid-role01-consider/modal-madrid-role01-consider.component';
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ModalMadridRole06LicenseComponent } from 'src/app/pages/modal-madrid-role06-license/modal-madrid-role06-license.component';

const routes: Routes = [
  {
    path: "",
    component: ModalMadridRole06LicenseComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ModalMadridRole06LicenseRoutingModule {}
