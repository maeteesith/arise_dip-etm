import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import {
  MadridRole04SearchComponent
} from "../../pages";

const routes: Routes = [
  {
    path: "",
    component: MadridRole04SearchComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MadridRole04SearchRoutingModule { }
