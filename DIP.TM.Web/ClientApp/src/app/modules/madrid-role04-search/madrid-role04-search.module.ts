import { NgModule } from "@angular/core";
import { IMPORTS } from "../imports";
import { PROVIDERS } from "../providers";
import { MadridRole04SearchRoutingModule } from "./madrid-role04-search-routing.module";
import {
  //DocumentRole04AppealListComponent,
  MadridRole04SearchComponent
} from "../../pages";

@NgModule({
  declarations: [
    MadridRole04SearchComponent
  ],
  imports: [...IMPORTS, MadridRole04SearchRoutingModule],
  providers: PROVIDERS
})
export class MadridRole04SearchModule { }
