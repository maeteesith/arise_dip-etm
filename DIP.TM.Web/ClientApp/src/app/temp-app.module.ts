// import { BrowserModule } from '@angular/platform-browser'
// import { NgModule } from '@angular/core'
// import { FormsModule } from '@angular/forms'
// import { HttpClientModule } from '@angular/common/http'
// import { AppRoutingModule } from './app-routing.module'
// import { AppComponent } from './app.component'
// import { NgxPaginationModule } from 'ngx-pagination'
// import { NgxSpinnerModule } from 'ngx-spinner'
// import { ToastrModule } from 'ngx-toastr'
// import { ClickOutsideModule } from 'ng-click-outside'
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
// import { DateAdapter } from '@angular/material/core'
// import { MyFilterPipe } from './helpers/myfilter'
// import {
//   MatMomentDateModule,
//   MomentDateAdapter,
//   MAT_MOMENT_DATE_ADAPTER_OPTIONS
// } from '@angular/material-moment-adapter'
// import {
//   MatDatepickerModule,
//   MatAutocompleteModule,
//   MatNativeDateModule,
//   MatFormFieldModule,
//   MatToolbarModule,
//   MatButtonModule,
//   MatSidenavModule,
//   MatIconModule,
//   MatListModule,
//   MatStepperModule,
//   MatInputModule,
//   MatRippleModule,
//   MAT_DATE_LOCALE,
//   MAT_DATE_FORMATS,
// } from '@angular/material'

// /* pages */
// import {
//   MainLayoutComponent,
//   MainLayoutEFormComponent,
//   RegistrationRequestComponent,
//   Request01ItemListComponent,
//   RepresentativeManagementComponent,
//   DailyFeeIncomeRecordsComponent,
//   DashboardComponent,
//   Save01ProcessComponent,
//   RegistrationOtherRequestComponent,
//   Save020ProcessComponent,
//   Save020ProcessListComponent,
//   Save021ProcessComponent,
//   Save021ProcessListComponent,
//   Save01ProcessListComponent,
//   Save01ProcessOwnerFormComponent,
//   Save01ProcessAgentFormComponent,
//   Save01ProcessUserFormComponent,
//   Save03ProcessComponent,
//   Save03ProcessListComponent,
//   Save04ProcessComponent,
//   Save04ProcessListComponent,
//   Save05ProcessComponent,
//   Save05ProcessListComponent,
//   Save06ProcessComponent,
//   Save06ProcessListComponent,
//   RequestOtherItemListComponent,
//   TestLoginComponent,
//   ReceiptDailyComponent,
//   Save07ProcessComponent,
//   Save07ProcessListComponent,
//   Save08ProcessComponent,
//   Save08ProcessListComponent,
//   Save140ProcessComponent,
//   RequestSearchComponent,
//   RequestDocumentCollectComponent,
//   AllTaskComponent,
//   DocumentProcessClassificationComponent,
//   DocumentProcessClassificationDoComponent,
//   Save140ProcessListComponent,
//   CheckingSimilarImageComponent,
//   CheckingItemListComponent,
//   CheckingSimilarListComponent,
//   CheckingSimilarComponent,
//   CheckingSimilarWordComponent,
//   CheckingSimilarResultComponent,
//   ConsideringSimilarListComponent,
//   ConsideringSimilarComponent,
//   PublicDashboardRole01Component,
//   eFormSave01ProcessComponent,
//   eFormSave02ProcessComponent,
//   eFormHomeComponent,
//   EFormBrandTypeComponent,
//   EFormOwnerAndAgentComponent,
//   NotFoundComponent,
// } from './pages'

// import { DocumentItemListComponent } from './pages/document-item-list/document-item-list.component'

// import { ConsideringSimilarInstructionComponent } from './pages/considering-similar-instruction/considering-similar-instruction.component'

// import { ConsideringSimilarDocumentComponent } from './pages/considering-similar-document/considering-similar-document.component'

// import { PublicRole01ItemListComponent } from './pages/public-role01-item-list/public-role01-item-list.component'

// import { PublicRole01CheckListComponent } from './pages/public-role01-check-list/public-role01-check-list.component'

// import { PublicRole01CheckDoComponent } from './pages/public-role01-check-do/public-role01-check-do.component'

// import { PublicRole01PrepareListComponent } from './pages/public-role01-prepare-list/public-role01-prepare-list.component'

// import { PublicRole01DoingListComponent } from './pages/public-role01-doing-list/public-role01-doing-list.component'

// import { PublicRole02CheckListComponent } from './pages/public-role02-check-list/public-role02-check-list.component'

// import { PublicRole02ConsiderPaymentListComponent } from './pages/public-role02-consider-payment-list/public-role02-consider-payment-list.component'

// import { PublicRole05ConsiderPaymentListComponent } from './pages/public-role05-consider-payment-list/public-role05-consider-payment-list.component'

// import { PublicRole02DocumentPaymentListComponent } from './pages/public-role02-document-payment-list/public-role02-document-payment-list.component'

// import { PublicRole02DocumentPostListComponent } from './pages/public-role02-document-post-list/public-role02-document-post-list.component'

// import { PublicRole02ActionPostListComponent } from './pages/public-role02-action-post-list/public-role02-action-post-list.component'

// import { PublicRole04itemListComponent } from './pages/public-role04-item-list/public-role04-item-list.component'

// import { PublicRole04CheckListComponent } from './pages/public-role04-check-list/public-role04-check-list.component'

// import { PublicRole05DocumentListComponent } from './pages/public-role05-document-list/public-role05-document-list.component'

// import { PublicRole04DocumentListComponent } from './pages/public-role04-document-list/public-role04-document-list.component'

// import { PublicRole02DocumentListComponent } from './pages/public-role02-document-list/public-role02-document-list.component'

// import { DocumentRole02ItemListComponent } from './pages/document-role02-item-list/document-role02-item-list.component'

// import { eFormSave140ProcessComponent } from './pages/eform-save-140-process/eform-save-140-process.component'

// import { eFormSave150ProcessComponent } from './pages/eform-save-150-process/eform-save-150-process.component'

// import { eFormSave190ProcessComponent } from './pages/eform-save-190-process/eform-save-190-process.component'

// import { eFormSave200ProcessComponent } from './pages/eform-save-200-process/eform-save-200-process.component'

// import { eFormSave030ProcessComponent } from './pages/eform-save-030-process/eform-save-030-process.component'

// import { eFormSave040ProcessComponent } from './pages/eform-save-040-process/eform-save-040-process.component'

// import { eFormSave050ProcessComponent } from './pages/eform-save-050-process/eform-save-050-process.component'

// import { eFormSave060ProcessComponent } from './pages/eform-save-060-process/eform-save-060-process.component'

// import { eFormSave070ProcessComponent } from './pages/eform-save-070-process/eform-save-070-process.component'

// import { eFormSave080ProcessComponent } from './pages/eform-save-080-process/eform-save-080-process.component'

// import { eFormSave021ProcessComponent } from './pages/eform-save-021-process/eform-save-021-process.component'

// import { eFormSave120ProcessComponent } from './pages/eform-save-120-process/eform-save-120-process.component'

// import { DocumentRole02CheckListComponent } from './pages/document-role02-check-list/document-role02-check-list.component'

// import { DocumentRole02CheckComponent } from './pages/document-role02-check/document-role02-check.component'

// import { DocumentRole02PrintDocumentListComponent } from './pages/document-role02-print-document-list/document-role02-print-document-list.component'

// import { DocumentRole02PrintCoverListComponent } from './pages/document-role02-print-cover-list/document-role02-print-cover-list.component'

// import { DocumentRole02PrintListListComponent } from './pages/document-role02-print-list-list/document-role02-print-list-list.component'

// import { RequestCheckComponent } from './pages/request-check/request-check.component'

// import { RequestSearchPeopleComponent } from './pages/request-search-people/request-search-people.component'

// import { Floor3DocumentComponent } from './pages/floor3-document/floor3-document.component'

// /* shared */
// import {
//   HeaderComponent,
//   NavComponent,
//   ModalComponent,
//   PopupComponent,
//   TabsComponent,
//   TabComponent,
//   DashboardCardComponent,
//   DashboardCardSummaryBlockComponent,
//   DragdropImageComponent,
//   DndDirective,
// } from './shared'

// @NgModule({
//   declarations: [
//     AppComponent,
//     RegistrationRequestComponent,
//     HeaderComponent,
//     NavComponent,
//     ModalComponent,
//     TabsComponent,
//     MyFilterPipe,
//     TabComponent,
//     DashboardCardComponent,
//     DashboardCardSummaryBlockComponent,
//     DashboardComponent,
//     PopupComponent,
//     MainLayoutComponent,
//     MainLayoutEFormComponent,
//     RepresentativeManagementComponent,
//     Save01ProcessComponent,
//     DailyFeeIncomeRecordsComponent,
//     RegistrationOtherRequestComponent,
//     Request01ItemListComponent,
//     Save020ProcessComponent,
//     Save020ProcessListComponent,
//     Save021ProcessComponent,
//     Save021ProcessListComponent,
//     Save01ProcessListComponent,
//     Save01ProcessOwnerFormComponent,
//     Save01ProcessAgentFormComponent,
//     Save01ProcessUserFormComponent,
//     Save03ProcessComponent,
//     Save03ProcessListComponent,
//     Save04ProcessComponent,
//     Save04ProcessListComponent,
//     Save05ProcessComponent,
//     Save05ProcessListComponent,
//     Save06ProcessComponent,
//     Save06ProcessListComponent,
//     RequestOtherItemListComponent,
//     TestLoginComponent,
//     ReceiptDailyComponent,
//     Save07ProcessComponent,
//     Save07ProcessListComponent,
//     Save08ProcessComponent,
//     Save08ProcessListComponent,
//     Save140ProcessComponent,
//     RequestSearchComponent,
//     RequestDocumentCollectComponent,
//     AllTaskComponent,
//     DocumentProcessClassificationComponent,
//     DocumentProcessClassificationDoComponent,
//     Save140ProcessListComponent,
//     CheckingSimilarImageComponent,
//     CheckingItemListComponent,
//     CheckingSimilarListComponent,
//     CheckingSimilarComponent,
//     CheckingSimilarWordComponent,
//     CheckingSimilarResultComponent,
//     ConsideringSimilarListComponent,
//     ConsideringSimilarComponent,
//     PublicDashboardRole01Component,
//     eFormSave01ProcessComponent,
//     eFormSave02ProcessComponent,
//     eFormHomeComponent,
//     EFormBrandTypeComponent,
//     EFormOwnerAndAgentComponent,
//     DocumentItemListComponent,
//     NotFoundComponent,
//     DragdropImageComponent,
//     DndDirective,
//     ConsideringSimilarInstructionComponent,
//     ConsideringSimilarDocumentComponent,
//     PublicRole01ItemListComponent,
//     PublicRole01CheckListComponent,
//     PublicRole01CheckDoComponent,
//     PublicRole01PrepareListComponent,
//     PublicRole01DoingListComponent,
//     PublicRole02CheckListComponent,
//     PublicRole02ConsiderPaymentListComponent,
//     PublicRole05ConsiderPaymentListComponent,
//     PublicRole02DocumentPaymentListComponent,
//     PublicRole02DocumentPostListComponent,
//     PublicRole02ActionPostListComponent,
//     PublicRole04itemListComponent,
//     PublicRole04CheckListComponent,
//     PublicRole05DocumentListComponent,
//     PublicRole04DocumentListComponent,
//     PublicRole02DocumentListComponent,
//     DocumentRole02ItemListComponent,
//     eFormSave140ProcessComponent,
//     eFormSave150ProcessComponent,
//     eFormSave190ProcessComponent,
//     eFormSave200ProcessComponent,
//     eFormSave030ProcessComponent,
//     eFormSave040ProcessComponent,
//     eFormSave050ProcessComponent,
//     eFormSave060ProcessComponent,
//     eFormSave070ProcessComponent,
//     eFormSave080ProcessComponent,
//     eFormSave021ProcessComponent,
//     eFormSave120ProcessComponent,
//     DocumentRole02CheckListComponent,
//     DocumentRole02CheckComponent,
//     DocumentRole02PrintDocumentListComponent,
//     DocumentRole02PrintCoverListComponent,
//     DocumentRole02PrintListListComponent,
//     RequestCheckComponent,
//     RequestSearchPeopleComponent,
//     Floor3DocumentComponent,
//   ],
//   imports: [
//     BrowserModule,
//     FormsModule,
//     HttpClientModule,
//     AppRoutingModule,
//     NgxPaginationModule,
//     NgxSpinnerModule,
//     ToastrModule.forRoot(),
//     ClickOutsideModule,
//     BrowserAnimationsModule,
//     MatDatepickerModule,
//     MatAutocompleteModule,
//     MatNativeDateModule,
//     MatMomentDateModule,
//     MatFormFieldModule,
//     MatToolbarModule,
//     MatButtonModule,
//     MatSidenavModule,
//     MatIconModule,
//     MatListModule,
//     MatStepperModule,
//     MatInputModule,
//     MatRippleModule
//   ],
//   providers: [
//     {
//       provide: DateAdapter,
//       useClass: MomentDateAdapter,
//       deps: [MAT_DATE_LOCALE, MAT_MOMENT_DATE_ADAPTER_OPTIONS]
//     },
//     {
//       provide: MAT_DATE_FORMATS,
//       useValue: {
//         parse: {
//           dateInput: ['l', 'LL'],
//         },
//         display: {
//           dateInput: 'L',
//           monthYearLabel: 'MMMM YYYY',
//           dateA11yLabel: 'LL',
//           monthYearA11yLabel: 'MMMM YYYY',
//         },
//       },
//     },
//     {
//       provide: MAT_DATE_LOCALE,
//       useValue: 'th'
//     }
//   ],
//   bootstrap: [AppComponent]
// })
// export class AppModule {
// }
