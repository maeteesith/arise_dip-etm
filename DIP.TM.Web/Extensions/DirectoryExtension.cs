﻿using DIP.TM.Models.Configs;
using DIP.TM.Utils;
using Microsoft.Extensions.DependencyInjection;
using System.IO;

namespace DIP.TM.Web.Extensions
{
    public static class DirectoryExtension
    {
        public static void ConfigContentFolders(this IServiceCollection services,AppConfigModel config)
        {
            var currentDirectory = Directory.GetCurrentDirectory();

            // users contents
            var userContentPath = Path.Combine(currentDirectory, config.ContentPath.User.PhysicalPath);
            FileHelper.CreateIfMissing(userContentPath);
            // user contents media
            var userContentMediaPath = Path.Combine(userContentPath, "Media");
            FileHelper.CreateIfMissing(userContentMediaPath);

            // guests contents
            var guestContentPath = Path.Combine(currentDirectory, config.ContentPath.Guest.PhysicalPath);
            FileHelper.CreateIfMissing(guestContentPath);
            // guest contents media
            var guestContentMediaPath = Path.Combine(guestContentPath, "Media");
            FileHelper.CreateIfMissing(guestContentMediaPath);
        }
    }
}
