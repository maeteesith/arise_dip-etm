﻿using DIP.TM.Models.Views;
using DIP.TM.Utils.Extensions;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DIP.TM.Web.Extensions
{
    public static class HttpHeaderExtensions
    {
        public static UserSessionView CurrentUser(this ControllerBase context)
        {
            var client = new UserSessionView();
            var headers = new Dictionary<string, string>();

            if (!ReferenceEquals(context.Request, null))
            {
                var reqHeader = context.Request.Headers;
                if (!ReferenceEquals(reqHeader, null))
                    foreach (var item in reqHeader)
                        headers.Add(item.Key, string.Join(string.Empty, item.Value));
            }

            client = headers.ToObject<UserSessionView>();
            if (ReferenceEquals(client, null))
                client = new UserSessionView();

            var userId = context.User.UserId();

            // TODO Fecth user info if require

            client.id = userId.Value;

            return client;
        }
    }
}
