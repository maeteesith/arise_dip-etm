﻿using System.Linq;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace DIP.TM.Web.Extensions
{
    public static class ModelStateExtensions
    {

        public static string ToErrorString(this ModelStateDictionary ModelState)
        {
            string messages = string.Join("; ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));
            return messages;
        }

    }
}
