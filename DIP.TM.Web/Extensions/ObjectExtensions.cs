﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace DIP.TM.Web.Extensions
{
    public static class ObjectExtensions
    {
        public static T ToObject<T>(this IDictionary<string, string> source)
            where T : class, new()
        {
            T someObject = new T();
            try
            {
                Type someObjectType = someObject.GetType();

                foreach (KeyValuePair<string, string> item in source)
                {
                    var keyProps = someObjectType.GetProperty(item.Key);
                    if (!ReferenceEquals(keyProps, null))
                        someObjectType.GetProperty(item.Key).SetValue(someObject, item.Value, null);
                }
            }
            catch (Exception)
            {

            }


            return someObject;
        }

        public static IDictionary<string, object> AsDictionary(this object source, BindingFlags bindingAttr = BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance)
        {
            return source.GetType().GetProperties(bindingAttr).ToDictionary
            (
                propInfo => propInfo.Name,
                propInfo => propInfo.GetValue(source, null)
            );

        }
    }
}
