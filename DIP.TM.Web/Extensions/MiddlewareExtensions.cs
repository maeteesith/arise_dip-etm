﻿using DIP.TM.Web.Middlewares;
using DIP.TM.Web.Middlewares.Builders;
using Microsoft.AspNetCore.Builder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Web.Extensions
{
    public static class MiddlewareExtensions
    {
        public static IApplicationBuilder UseLogHeadersMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<LogHeadersMiddleware>();
        }
        public static IApplicationBuilder UseSecurityHeadersMiddleware(this IApplicationBuilder app, SecurityHeadersBuilder builder)
        {
            SecurityHeadersPolicy policy = builder.Build();
            return app.UseMiddleware<SecurityHeadersMiddleware>(policy);
        }

        public static IApplicationBuilder UseErrorHandlingMiddleware(this IApplicationBuilder app)
        {
            return app.UseMiddleware<ErrorHandlingMiddleware>();
        }
    }
}
