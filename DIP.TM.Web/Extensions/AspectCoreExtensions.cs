﻿using AspectCore.Configuration;
using AspectCore.Extensions.DependencyInjection;
using DIP.TM.Services.Attributes;
using DIP.TM.Services.Auths;
using DIP.TM.Services.Checking;
using DIP.TM.Services.Email;
using DIP.TM.Services.Interfaces;
using DIP.TM.Services.Logging;
using DIP.TM.Services.ReferenceMasters;
using DIP.TM.Services.RequestChecks;
using DIP.TM.Services.Saves;
using Microsoft.Extensions.DependencyInjection;

namespace DIP.TM.Web.Extensions
{
    public static class AspectCoreExtensions
    {
        public static void ConfigAspectCore(this IServiceCollection services)
        {

            services.AddScoped<LibLogService>();
            services.AddScoped<IBaseEmailService, BaseEmailService>();
            services.AddScoped<IReferenceMasterService, ReferenceMasterService>();
            services.AddScoped<ICheckingProcessService, CheckingProcessService>();
            services.AddScoped<AuthService>();
            services.AddScoped <RequestDocumentCollectService>();
            services.AddScoped<RequestCheckService>();
            services.ConfigureDynamicProxy(config =>
            {
                config.Interceptors.AddTyped<LogAopInterceptorAttribute>(Predicates.Implement(typeof(AuthService)));
                config.Interceptors.AddTyped<LogAopInterceptorAttribute>(Predicates.Implement(typeof(RequestDocumentCollectService)));
                config.Interceptors.AddTyped<LogAopInterceptorAttribute>(Predicates.Implement(typeof(RequestCheckService)));
            });
            services.BuildAspectInjectorProvider();
        }
    }
}
