﻿using DIP.TM.Utils.Extensions;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;

namespace DIP.TM.Web.Filters
{
    /// <summary>
    /// 
    /// </summary>
    public class GuestUserFilterAttribute : ActionFilterAttribute
    {
        private readonly IConfiguration configuration;
        private long? guestUserId;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public GuestUserFilterAttribute(IConfiguration configuration)
        {
            this.configuration = configuration;
            guestUserId = configuration["GuestUserId"].ToLong();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var user = new ClaimsPrincipal(new ClaimsIdentity(new Claim[]
            {
                new Claim(ClaimTypes.NameIdentifier, guestUserId.ToString()),
            }, "Guest User"));

            // TODO mock unit test executed
            context.HttpContext.User = user;
            base.OnActionExecuting(context);
        }
    }
}
