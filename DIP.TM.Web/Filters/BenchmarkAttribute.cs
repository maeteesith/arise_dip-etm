﻿using DIP.TM.Utils.Logging;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Configuration;
using System.Diagnostics;
using System.Threading.Tasks;


namespace DIP.TM.Web.Filters
{
    public class BenchmarkAttribute : ActionFilterAttribute
    {
        private readonly IConfiguration configuration;

        public BenchmarkAttribute(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public override async Task OnActionExecutionAsync(
            ActionExecutingContext context,
            ActionExecutionDelegate next)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            await next();
            if (stopWatch.Elapsed.TotalSeconds > 3)
            {
                LineNotifyHelper.SendWait(configuration, context.HttpContext.Request.Path.Value + " : " + stopWatch.Elapsed.TotalSeconds.ToString() + " seconds");
            }

            stopWatch.Stop();

            if (!context.HttpContext.Response.Headers.ContainsKey("X-Stopwatch"))
            {
                context.HttpContext.Response.Headers.Add(
                "X-Stopwatch",
                stopWatch.Elapsed.TotalSeconds.ToString());
            }
            else
            {
                context.HttpContext.Response.Headers["X-Stopwatch"] = stopWatch.Elapsed.TotalSeconds.ToString();
            }
        }

    }
}
