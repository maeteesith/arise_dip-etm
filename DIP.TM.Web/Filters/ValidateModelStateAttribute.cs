﻿using DIP.TM.Models.Views;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace DIP.TM.Web.Filters
{
    public class ValidateModelStateAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var result = new BaseResponseView<string>()
                {
                    data = null,
                    is_error = true,
                    error_message = context.ModelState.ToErrorString()
                };

                context.Result = new OkObjectResult(result);
            }
        }
    }
}
