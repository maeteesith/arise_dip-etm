using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DIP.TM.Models.Views;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DIP.TM.Web.Views.Pdf {
    public class Save010HistoryPdfModel : PageModel {

        public vSave010View save { set; get; }
        public List<vSave010HistoryView> view { set; get; }
        //public List<vConsideringSimilaInstruction> instruction_list { set; get; }

        public void OnGet() {
        }
    }
}
