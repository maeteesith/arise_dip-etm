using DIP.TM.Models.Views;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace DIP.TM.Web.Views.Pdf {
    public class CheckingSimilarRequestItemSubTypeReportPdfModel : PageModel {
        public List<vCheckingSimilarRequestItemSubTypeReportView> view_list { set; get; }

        public void OnGet() {
        }
    }
}
