using DIP.TM.Models.Views;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;

namespace DIP.TM.Web.Views.Pdf {
    public class PostCoverPdfModel : PageModel {
        public List<vDocumentRole02GroupView> view_list { set; get; }
        public void OnGet() {
        }
    }
}
