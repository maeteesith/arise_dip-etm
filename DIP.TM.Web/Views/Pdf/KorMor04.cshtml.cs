using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DIP.TM.Models.Views;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DIP.TM.Web.Views.Pdf {
    public class KorMor04PdfModel : PageModel {
        public List<Save010ProcessView> view { set; get; }
        public bool is_show_01 { set; get; }
        public bool is_show_02 { set; get; }
        public bool is_representative { set; get; }

        public void OnGet() {
        }
    }
}
