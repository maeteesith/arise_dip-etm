using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DIP.TM.Models.Views;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DIP.TM.Web.Views.Pdf {
    public class KorMor01PdfModel : PageModel {

        public List<Save010ProcessView> view_list { set; get; }
        public bool is_show_01 { set; get; }
        public bool is_show_02 { set; get; }
        public bool is_show_03 { set; get; }
        public bool is_show_04 { set; get; }
        public bool is_show_05 { set; get; }
        public bool is_representative { set; get; }
        public string public05_registrar_by_name { set; get; }

        public void OnGet() {
        }
    }
}
