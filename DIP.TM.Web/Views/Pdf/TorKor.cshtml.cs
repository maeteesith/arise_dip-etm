using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DIP.TM.Models.Views;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace DIP.TM.Web.Views.Pdf {
    public class TorKorPdfModel : PageModel {
        public List<vDocumentRole02View> view { set; get; }
        public string barcode { set; get; }
        public string reference_number { set; get; }

        public void OnGet() {
        }
    }
}
