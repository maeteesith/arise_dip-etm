﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Web.Attributes
{
    public class ServiceInfoAttribute : Attribute
    {
        public string service_name { get; set; }
        public string method_name { get; set; }
        public string model_id { get; set; }
        public ServiceInfoAttribute(string service_name,string method_name, string model_id)
        {
            this.service_name = service_name;
            this.method_name = method_name;
            this.model_id = model_id;
        }
    }
}
