﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;

namespace DIP.TM.Web.Profiles {
    public class MapProfile : Profile {
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public MapProfile() {
            HelpMap<SendChangeAddModel, SendChangeView>();
        }
    }
}
