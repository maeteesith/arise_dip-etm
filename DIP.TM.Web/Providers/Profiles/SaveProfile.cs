﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace DIP.TM.Web.Profiles {
    public class SaveProfile : Profile {
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public SaveProfile() {
            #region Save010
            CreateMap<Save010AddModel, Save010>()
                .ForMember(dest => dest.Save010AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.Save010AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.Save010AddressContact, opt => opt.MapFrom(src => src.contact_address == null ? null : new List<SaveAddressAddModel>() { src.contact_address }))
                .ForMember(dest => dest.Save010AddressJoiner, opt => opt.MapFrom(src => src.joiner_list))
                .ForMember(dest => dest.Save010Product, opt => opt.MapFrom(src => src.product_list))
                .ForMember(dest => dest.Save010Case28, opt => opt.MapFrom(src => src.case28_list))
                .ForMember(dest => dest.Save010CheckingSimilarWordTranslate, opt => opt.MapFrom(src => src.checking_similar_wordsoundtranslate_list))
                .ForMember(dest => dest.Save010DocumentClassificationWord, opt => opt.MapFrom(src => src.document_classification_word_list))
                .ForMember(dest => dest.Save010DocumentClassificationImage, opt => opt.MapFrom(src => src.document_classification_image_list))
                .ForMember(dest => dest.Save010DocumentClassificationSound, opt => opt.MapFrom(src => src.document_classification_sound_list)).ReverseMap()
                ;
            CreateMap<Save010, Save010ProcessView>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save010AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save010AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save010AddressContact == null ? null : src.Save010AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.Save010AddressJoiner))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save010Product))
                .ForMember(dest => dest.case28_list, opt => opt.MapFrom(src => src.Save010Case28))
                .ForMember(dest => dest.save_status_codeNavigation, opt => opt.MapFrom(src => src.save_status_codeNavigation))
                .ForMember(dest => dest.document_classification_status_codeNavigation, opt => opt.MapFrom(src => src.document_classification_status_codeNavigation))
                .ForMember(dest => dest.request_item_type_codeNavigation, opt => opt.MapFrom(src => src.request_item_type_codeNavigation))
                .ForMember(dest => dest.checking_similar_wordsoundtranslate_list, opt => opt.MapFrom(src => src.Save010CheckingSimilarWordTranslate))
                //.ForMember(dest => dest.document_classification_search_method, opt => opt.MapFrom(src => src.))
                .ForMember(dest => dest.document_classification_word_list, opt => opt.MapFrom(src => src.Save010DocumentClassificationWord))
                .ForMember(dest => dest.document_classification_image_list, opt => opt.MapFrom(src => src.Save010DocumentClassificationImage))
                .ForMember(dest => dest.document_classification_sound_list, opt => opt.MapFrom(src => src.Save010DocumentClassificationSound))
                .ForMember(dest => dest.instruction_rule_list, opt => opt.MapFrom(src => src.Save010InstructionRule))
                ;
            HelpMap<SaveAddressAddModel, Save010AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, Save010AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, Save010AddressContact, SaveAddressView>();
            HelpMap<SaveAddressAddModel, Save010AddressJoiner, SaveAddressView>();
            HelpMap<SaveProductAddModel, Save010Product, SaveProductView>();
            HelpMap<Save010Case28AddModel, Save010Case28, Save010Case28View>();
            HelpMap<Save010DocumentClassificationWordAddModel, Save010DocumentClassificationWord, Save010DocumentClassificationWordView>();
            HelpMap<Save010CheckingTagSimilarMethod, Save010CheckingTagSimilarMethodView>();

            HelpMap<Save010DocumentClassificationWordAddModel, Save010DocumentClassificationVersionWord>();
            HelpMap<Save010DocumentClassificationImageAddModel, Save010DocumentClassificationVersionImage>();
            HelpMap<Save010DocumentClassificationSoundAddModel, Save010DocumentClassificationVersionSound>();

            CreateMap<Save010InstructionRuleAddModel, Save010InstructionRule>()
                .ForMember(dest => dest.Save010InstructionRuleItem, opt => opt.MapFrom(src => src.item_list))
                ;
            CreateMap<Save010InstructionRule, Save010InstructionRuleView>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.Save010InstructionRuleItem))
                ;
            CreateMap<Save010InstructionRule, Save010InstructionRuleAddModel>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.Save010InstructionRuleItem))
                ;
            HelpMap<Save010InstructionRuleItemAddModel, Save010InstructionRuleItem, Save010InstructionRuleItemView>();
            HelpMap<Save010InstructionRuleRequestNumberAddModel, Save010InstructionRuleRequestNumber, Save010InstructionRuleRequestNumberView>();
            HelpMap<vSave010View, Save010InstructionRuleRequestNumberView>();

            HelpMap<Save010DocumentClassificationImageAddModel, Save010DocumentClassificationImage, Save010DocumentClassificationImageView>();
            CreateMap<Save010DocumentClassificationImage, Save010DocumentClassificationImageView>()
                .ForMember(dest => dest.document_classification_image_codeNavigation, opt => opt.MapFrom(src => src.document_classification_image_codeNavigation))
                ;
            CreateMap<Save010DocumentClassificationImageView, Save010DocumentClassificationImage>()
                .ForMember(dest => dest.document_classification_image_codeNavigation, opt => opt.MapFrom(src => src.document_classification_image_codeNavigation))
                ;
            HelpMap<Save010DocumentClassificationSoundAddModel, Save010DocumentClassificationSound, Save010DocumentClassificationSoundView>();
            CreateMap<Save010DocumentClassificationSound, Save010DocumentClassificationSoundView>()
                .ForMember(dest => dest.document_classification_sound_codeNavigation, opt => opt.MapFrom(src => src.document_classification_sound_codeNavigation))
                ;
            CreateMap<Save010DocumentClassificationSoundView, Save010DocumentClassificationSound>()
                .ForMember(dest => dest.document_classification_sound_codeNavigation, opt => opt.MapFrom(src => src.document_classification_sound_codeNavigation))
                ;

            HelpMap<Save010CheckingSimilarWordTranslateAddModel, Save010CheckingSimilarWordTranslate>();
            HelpMap<eForm_Save010CheckingSimilarWordTranslateAddModel, eForm_Save010CheckingSimilarWordTranslate, eForm_Save010CheckingSimilarWordTranslateView>();
            //HelpMap<Save010CheckingSimilarWordTranslateAddModel, Save010CheckingSimilarWordTranslate>();
            CreateMap<Save010CheckingSimilarWordTranslate, Save010CheckingSimilarWordTranslateView>()
                .ForMember(dest => dest.checking_word_translate_status_codeNavigation, opt => opt.MapFrom(src => src.checking_word_translate_status_codeNavigation))
                ;
            CreateMap<Save010CheckingSimilarWordTranslateView, Save010CheckingSimilarWordTranslate>()
                .ForMember(dest => dest.checking_word_translate_status_codeNavigation, opt => opt.MapFrom(src => src.checking_word_translate_status_codeNavigation))
                ;

            HelpMap<Save010ProcessView, Save010AddModel>();

            HelpMap<vSave010CheckingTagSimilar, vSave010CheckingTagSimilarView>();
            #endregion

            #region Save010DocumentClassificationVersion
            CreateMap<Save010DocumentClassificationVersion, Save010DocumentClassificationVersionView>()
                .ForMember(dest => dest.document_classification_word_list, opt => opt.MapFrom(src => src.Save010DocumentClassificationVersionWord))
                .ForMember(dest => dest.document_classification_image_list, opt => opt.MapFrom(src => src.Save010DocumentClassificationVersionImage))
                .ForMember(dest => dest.document_classification_sound_list, opt => opt.MapFrom(src => src.Save010DocumentClassificationVersionSound))
                ;
            HelpMap<Save010DocumentClassificationVersionWord, Save010DocumentClassificationWordView>();
            HelpMap<Save010DocumentClassificationVersionImage, Save010DocumentClassificationImageView>();
            HelpMap<Save010DocumentClassificationVersionSound, Save010DocumentClassificationSoundView>();
            #endregion

            #region
            HelpMap<Save010Case28ProcessAddModel, Save010Case28Process, Save010Case28ProcessView>();
            HelpMap<Save010Case28AddModel, Save010Case28, Save010Case28View>();

            HelpMap<Save010RequestGroup, Save010RequestGroupAddModel>();
            HelpMap<Save010CertificationFile, Save010CertificationFileAddModel>();
            HelpMap<vSave010History, vSave010HistoryView>();

            #endregion

            #region Save020
            CreateMap<Save020AddModel, Save020>()
                .ForMember(dest => dest.Save020AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.Save020AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.Save020AddressContact, opt => opt.MapFrom(src => src.contact_address == null ? null : new List<SaveAddressAddModel>() { src.contact_address })).ReverseMap();
            CreateMap<Save020, Save020ProcessView>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save020AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save020AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save020AddressContact == null ? null : src.Save020AddressContact.ToList().LastOrDefault()));
            HelpMap<SaveAddressAddModel, Save020AddressPeople>();
            HelpMap<SaveAddressAddModel, Save020AddressRepresentative>();
            HelpMap<SaveAddressAddModel, Save020AddressContact>();
            HelpMap<Save020AddressPeople, SaveAddressView>();
            HelpMap<Save020AddressRepresentative, SaveAddressView>();
            HelpMap<Save020AddressContact, SaveAddressView>();
            HelpMap<Save020ProcessView, Save020AddModel>();
            HelpMap<SaveModel, Save020>();
            HelpMap<SaveModel, Save020AddModel>();
            HelpMap<Save020ProcessView, Save020AddModel>();
            #endregion
            #region Save021
            HelpMap<Save021, Save021ProcessView>();
            HelpMap<SaveModel, Save021>();
            HelpMap<SaveModel, Save021AddModel>();
            HelpMap<Save021AddModel, Save021>();
            HelpMap<Save021ProcessView, Save021AddModel>();
            #endregion
            #region Save030
            CreateMap<Save030AddModel, Save030>()
                .ForMember(dest => dest.Save030AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.Save030AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.Save030AddressContact, opt => opt.MapFrom(src => src.contact_address == null ? null : new List<SaveAddressAddModel>() { src.contact_address })).ReverseMap();
            CreateMap<Save030, Save030ProcessView>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save030AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save030AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save030AddressContact == null ? null : src.Save030AddressContact.ToList().LastOrDefault()));
            HelpMap<SaveAddressAddModel, Save030AddressPeople>();
            HelpMap<SaveAddressAddModel, Save030AddressRepresentative>();
            HelpMap<SaveAddressAddModel, Save030AddressContact>();
            HelpMap<Save030AddressPeople, SaveAddressView>();
            HelpMap<Save030AddressRepresentative, SaveAddressView>();
            HelpMap<Save030AddressContact, SaveAddressView>();
            HelpMap<Save030ProcessView, Save030AddModel>();
            HelpMap<SaveModel, Save030>();
            HelpMap<SaveModel, Save030AddModel>();
            HelpMap<Save030ProcessView, Save030AddModel>();
            #endregion
            #region Save040
            CreateMap<Save040AddModel, Save040>()
                .ForMember(dest => dest.Save040AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.Save040AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.Save040AddressContact, opt => opt.MapFrom(src => src.contact_address == null ? null : new List<SaveAddressAddModel>() { src.contact_address }))
                .ForMember(dest => dest.Save040Product, opt => opt.MapFrom(src => src.product_list)).ReverseMap();
            CreateMap<Save040, Save040ProcessView>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save040AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save040AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save040AddressContact == null ? null : src.Save040AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save040Product))
                .ForMember(dest => dest.save_status_codeNavigation, opt => opt.MapFrom(src => src.save_status_codeNavigation));
            HelpMap<SaveAddressAddModel, Save040AddressPeople>();
            HelpMap<Save040AddressRepresentativeAddModel, Save040AddressRepresentative>();
            HelpMap<SaveAddressAddModel, Save040AddressContact>();
            HelpMap<SaveProductAddModel, Save040Product>();
            HelpMap<Save040AddressPeople, SaveAddressView>();
            CreateMap<Save040AddressRepresentative, Save040AddressRepresentativeView>()
                .ForMember(dest => dest.save040_representative_type_codeNavigation, opt => opt.MapFrom(src => src.save040_representative_type_codeNavigation));
            HelpMap<Save040AddressContact, SaveAddressView>();
            HelpMap<Save040Product, SaveProductView>();
            CreateMap<Save040, Save040AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save040AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save040AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save040AddressContact == null ? null : src.Save040AddressContact.FirstOrDefault()))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save040Product));
            HelpMap<Save040AddressPeople, SaveAddressAddModel>();
            HelpMap<Save040AddressRepresentative, Save040AddressRepresentativeAddModel>();
            HelpMap<Save040AddressContact, SaveAddressAddModel>();
            HelpMap<Save040Product, SaveProductAddModel>();
            #endregion
            #region Save050
            CreateMap<Save050AddModel, Save050>()
                .ForMember(dest => dest.Save050AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.Save050AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.Save050AddressContact, opt => opt.MapFrom(src => src.contact_address == null ? null : new List<SaveAddressAddModel>() { src.contact_address }))
                .ForMember(dest => dest.Save050ReceiverAddressPeople, opt => opt.MapFrom(src => src.receiver_people_list))
                .ForMember(dest => dest.Save050ReceiverAddressRepresentative, opt => opt.MapFrom(src => src.receiver_representative_list))
                .ForMember(dest => dest.Save050Product, opt => opt.MapFrom(src => src.product_list)).ReverseMap();
            CreateMap<Save050, Save050ProcessView>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save050AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save050AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save050AddressContact == null ? null : src.Save050AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.Save050ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.Save050ReceiverAddressRepresentative))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save050Product))
                .ForMember(dest => dest.save_status_codeNavigation, opt => opt.MapFrom(src => src.save_status_codeNavigation));

            HelpMap<SaveAddressAddModel, Save050AddressPeople>();
            HelpMap<Save050AddressRepresentativeAddModel, Save050AddressRepresentative>();
            HelpMap<SaveAddressAddModel, Save050AddressContact>();
            HelpMap<SaveProductAddModel, Save050Product>();
            HelpMap<Save050AddressPeople, SaveAddressView>();
            HelpMap<Save050AddressRepresentative, Save050AddressRepresentativeView>();
            //.ForMember(dest => dest.save050_representative_type_codeNavigation, opt => opt.MapFrom(src => src.save050_representative_type_codeNavigation));
            HelpMap<Save050AddressContact, SaveAddressView>();
            HelpMap<Save050Product, SaveProductView>();
            CreateMap<Save050, Save050AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save050AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save050AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save050AddressContact == null ? null : src.Save050AddressContact.FirstOrDefault()))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save050Product));
            HelpMap<Save050AddressPeople, SaveAddressAddModel>();
            HelpMap<Save050AddressRepresentative, Save050AddressRepresentativeAddModel>();
            HelpMap<Save050AddressContact, SaveAddressAddModel>();
            HelpMap<Save050Product, SaveProductAddModel>();
            HelpMap<SaveAddressAddModel, Save050ReceiverAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, Save050ReceiverAddressRepresentative, SaveAddressView>();
            #endregion
            #region Save060
            CreateMap<Save060AddModel, Save060>()
                .ForMember(dest => dest.Save060AddressAllowPeople, opt => opt.MapFrom(src => src.allow_people_list))
                .ForMember(dest => dest.Save060AddressContact, opt => opt.MapFrom(src => src.contact_address == null ? null : new List<SaveAddressAddModel>() { src.contact_address }))
                .ForMember(dest => dest.Save060AddressContact06, opt => opt.MapFrom(src => src.contact_address_06 == null ? null : new List<SaveAddressAddModel>() { src.contact_address_06 }))
                .ForMember(dest => dest.Save060AddressJoiner, opt => opt.MapFrom(src => src.joiner_list))
                .ForMember(dest => dest.Save060AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.Save060AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.Save060AllowProduct, opt => opt.MapFrom(src => src.allow_product_list))
                .ForMember(dest => dest.Save060Product, opt => opt.MapFrom(src => src.product_list)).ReverseMap();
            CreateMap<Save060, Save060ProcessView>()
                .ForMember(dest => dest.allow_people_list, opt => opt.MapFrom(src => src.Save060AddressAllowPeople))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save060AddressContact == null ? null : src.Save060AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.contact_address_06, opt => opt.MapFrom(src => src.Save060AddressContact06 == null ? null : src.Save060AddressContact06.ToList().LastOrDefault()))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.Save060AddressJoiner))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save060AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save060AddressRepresentative))
                .ForMember(dest => dest.allow_product_list, opt => opt.MapFrom(src => src.Save060AllowProduct))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save060Product));
            HelpMap<SaveAddressAddModel, Save060AddressAllowPeople>();
            HelpMap<SaveAddressAddModel, Save060AddressContact>();
            HelpMap<SaveAddressAddModel, Save060AddressContact06>();
            HelpMap<SaveAddressAddModel, Save060AddressJoiner>();
            HelpMap<SaveAddressAddModel, Save060AddressPeople>();
            HelpMap<Save060AllowContactAddModel, Save060AddressAllowPeople, Save060AllowContactView>();
            HelpMap<Save060AddressRepresentativeAddModel, Save060AddressRepresentative>();
            HelpMap<SaveProductAddModel, Save060AllowProduct>();
            HelpMap<SaveProductAddModel, Save060Product>();

            HelpMap<Save060AddressAllowPeople, SaveAddressView>();
            HelpMap<Save060AddressContact, SaveAddressView>();
            HelpMap<Save060AddressContact06, SaveAddressView>();
            HelpMap<Save060AddressJoiner, SaveAddressView>();
            HelpMap<Save060AddressPeople, SaveAddressView>();
            HelpMap<Save060AddressRepresentative, Save060AddressRepresentativeView>();
            HelpMap<Save060AllowProduct, SaveProductView>();
            HelpMap<Save060Product, SaveProductView>();

            CreateMap<Save060, Save060AddModel>()
                .ForMember(dest => dest.allow_people_list, opt => opt.MapFrom(src => src.Save060AddressAllowPeople))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save060AddressContact == null ? null : src.Save060AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.contact_address_06, opt => opt.MapFrom(src => src.Save060AddressContact06 == null ? null : src.Save060AddressContact06.ToList().LastOrDefault()))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.Save060AddressJoiner))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save060AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save060AddressRepresentative))
                .ForMember(dest => dest.allow_product_list, opt => opt.MapFrom(src => src.Save060AllowProduct))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save060Product));
            HelpMap<Save060AddressAllowPeople, SaveAddressAddModel>();
            HelpMap<Save060AddressContact, SaveAddressAddModel>();
            HelpMap<Save060AddressContact06, SaveAddressAddModel>();
            HelpMap<Save060AddressJoiner, SaveAddressAddModel>();
            HelpMap<Save060AddressPeople, SaveAddressAddModel>();
            HelpMap<Save060AddressRepresentative, Save060AddressRepresentativeAddModel>();
            HelpMap<Save060AllowProduct, SaveProductAddModel>();
            HelpMap<Save060Product, SaveProductAddModel>();

            CreateMap<Save060ProcessView, Save060AddModel>()
                .ForMember(dest => dest.allow_people_list, opt => opt.MapFrom(src => src.allow_people_list))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.contact_address))
                .ForMember(dest => dest.contact_address_06, opt => opt.MapFrom(src => src.contact_address_06))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.joiner_list))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.allow_product_list, opt => opt.MapFrom(src => src.allow_product_list))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.product_list));

            #endregion
            #region Save070
            CreateMap<Save070AddModel, Save070>()
                .ForMember(dest => dest.Save070AddressContact, opt => opt.MapFrom(src => src.contact_address == null ? null : new List<SaveAddressAddModel>() { src.contact_address }))
                .ForMember(dest => dest.Save070Product, opt => opt.MapFrom(src => src.product_list)).ReverseMap();
            CreateMap<Save070, Save070ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save070AddressContact == null ? null : src.Save070AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save070Product));
            HelpMap<SaveAddressAddModel, Save070AddressContact>();
            HelpMap<SaveProductAddModel, Save070Product>();


            HelpMap<Save070AddressContact, SaveAddressView>();
            HelpMap<Save070Product, SaveProductView>();

            CreateMap<Save070, Save070AddModel>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save070AddressContact == null ? null : src.Save070AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save070Product));
            HelpMap<Save070AddressContact, SaveAddressAddModel>();
            HelpMap<Save070Product, SaveProductAddModel>();
            #endregion
            #region Save080
            CreateMap<Save080AddModel, Save080>()
                .ForMember(dest => dest.Save080AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.Save080AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.Save080AddressContact, opt => opt.MapFrom(src => src.contact_address == null ? null : new List<SaveAddressAddModel>() { src.contact_address }))
                .ForMember(dest => dest.Save080AddressJoiner, opt => opt.MapFrom(src => src.joiner_list))
                .ForMember(dest => dest.Save080AddressJoinerRepresentative, opt => opt.MapFrom(src => src.joiner_representative_list)).ReverseMap();
            CreateMap<Save080, Save080ProcessView>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save080AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save080AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save080AddressContact == null ? null : src.Save080AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.Save080AddressJoiner))
                .ForMember(dest => dest.joiner_representative_list, opt => opt.MapFrom(src => src.Save080AddressJoinerRepresentative));
            HelpMap<SaveAddressAddModel, Save080AddressPeople>();
            HelpMap<Save080AddressRepresentativeAddModel, Save080AddressRepresentative>();
            HelpMap<Save080AddressRepresentativeAddModel, Save080AddressJoinerRepresentative>();
            HelpMap<SaveAddressAddModel, Save080AddressContact>();
            HelpMap<SaveAddressAddModel, Save080AddressJoiner>();
            HelpMap<SaveAddressAddModel, Save080AddressJoinerRepresentative>();

            HelpMap<Save080AddressPeople, SaveAddressView>();
            HelpMap<Save080AddressRepresentative, Save080AddressRepresentativeView>();
            HelpMap<Save080AddressJoinerRepresentative, Save080AddressRepresentativeView>();
            HelpMap<Save080AddressContact, SaveAddressView>();
            HelpMap<Save080AddressJoiner, SaveAddressView>();
            HelpMap<Save080AddressJoinerRepresentative, SaveAddressView>();

            CreateMap<Save080, Save080AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save080AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save080AddressRepresentative))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save080AddressContact == null ? null : src.Save080AddressContact.ToList().LastOrDefault()))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.Save080AddressJoiner))
                .ForMember(dest => dest.joiner_representative_list, opt => opt.MapFrom(src => src.Save080AddressJoinerRepresentative));
            HelpMap<Save080AddressPeople, SaveAddressAddModel>();
            HelpMap<Save080AddressRepresentative, Save080AddressRepresentativeAddModel>();
            HelpMap<Save080AddressContact, SaveAddressAddModel>();
            HelpMap<Save080AddressJoiner, SaveAddressAddModel>();
            HelpMap<Save080AddressJoinerRepresentative, SaveAddressAddModel>();


            CreateMap<Save080ProcessView, Save080AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.contact_address))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.joiner_list))
                .ForMember(dest => dest.joiner_representative_list, opt => opt.MapFrom(src => src.joiner_representative_list));


            #endregion
            #region Save140
            HelpMap<Save140AddModel, Save140>();
            HelpMap<Save140, Save140ProcessView>();
            HelpMap<Save140, Save140AddModel>();
            HelpMap<SaveModel, Save140>();
            HelpMap<SaveModel, Save140AddModel>();
            HelpMap<Save140ProcessView, Save140AddModel>();
            #endregion

            #region SaveInstructionRule
            CreateMap<SaveInstructionRuleAddModel, SaveInstructionRule>()
                .ForMember(dest => dest.SaveInstructionRuleItem, opt => opt.MapFrom(src => src.item_list))
                ;
            CreateMap<SaveInstructionRule, SaveInstructionRuleView>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.SaveInstructionRuleItem))
                ;
            CreateMap<SaveInstructionRule, SaveInstructionRuleAddModel>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.SaveInstructionRuleItem))
                ;
            HelpMap<SaveInstructionRuleItemAddModel, SaveInstructionRuleItem, SaveInstructionRuleItemView>();
            HelpMap<SaveInstructionRule9ItemSubType1AddModel, SaveInstructionRule9ItemSubType1, SaveInstructionRule9ItemSubType1View>();
            HelpMap<SaveInstructionRuleRequestNumberAddModel, SaveInstructionRuleRequestNumber, SaveInstructionRuleRequestNumberView>();
            #endregion

            HelpMap<SaveOtherAddModel, SaveOther>();
            HelpMap<SaveOther, SaveOtherProcessView>();
            HelpMap<SaveOther, SaveOtherAddModel>();
            HelpMap<SaveModel, SaveOther>();
            HelpMap<SaveModel, SaveOtherAddModel>();
            HelpMap<SaveOtherProcessView, SaveOtherAddModel>();

            HelpMap<SaveModel, Save040>();
            HelpMap<SaveModel, Save040AddModel>();
            HelpMap<SaveModel, Save050>();
            HelpMap<SaveModel, Save050AddModel>();
            HelpMap<SaveModel, Save060>();
            HelpMap<SaveModel, Save060AddModel>();
            HelpMap<SaveModel, Save070>();
            HelpMap<SaveModel, Save070AddModel>();
            HelpMap<SaveModel, Save080>();
            HelpMap<SaveModel, Save080AddModel>();
            HelpMap<SaveModel, Save140>();
            HelpMap<SaveModel, Save140AddModel>();

            HelpMap<Save010AddModel, Save010AddModel>();
            HelpMap<Save020AddModel, Save020AddModel>();
            HelpMap<Save021AddModel, Save021AddModel>();
            HelpMap<Save030AddModel, Save030AddModel>();
            HelpMap<Save040AddModel, Save040AddModel>();
            HelpMap<Save050AddModel, Save050AddModel>();
            HelpMap<Save060AddModel, Save060AddModel>();
            HelpMap<Save070AddModel, Save070AddModel>();
            HelpMap<Save080AddModel, Save080AddModel>();
            HelpMap<Save140AddModel, Save140AddModel>();
            HelpMap<SaveOtherAddModel, SaveOtherAddModel>();
        }
    }

    public class Save010Profile : Profile {
        public void HelpMap<A, B, C>() {
            HelpMap<A, B>();
            HelpMap<A, C>();
            HelpMap<B, C>();
        }
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }

        public Save010Profile() {

            HelpMap<vSave010Model, Save010, vSave010View>();

            HelpMap<vSave010Model, vSave010, vSave010View>();

            HelpMap<vSave010CheckingTagSimilarAddModel, Save010CheckingTagSimilar, vSave010CheckingTagSimilarView>();

            CreateMap<vSave010CheckingTagSimilar, vSave010View>();

            CreateMap<vSave010Model, Save010AddModel>();

            HelpMap<vSave010CheckingInstructionDocument, vSave010CheckingInstructionDocumentView>();
        }
    }
}
