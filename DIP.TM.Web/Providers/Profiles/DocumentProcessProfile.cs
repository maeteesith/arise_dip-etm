﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;

namespace DIP.TM.Web.Profiles {
    public class DocumentProcessProfile : Profile {
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public DocumentProcessProfile() {
            HelpMap<Save010DocumentClassificationCollect, Save010DocumentClassificationCollectAddModel, Save010DocumentClassificationCollectView>();

            HelpMap<DocumentRole03ChangeAddressPeople, SaveAddressAddModel, SaveAddressView>();
            HelpMap<DocumentRole03ChangeAddressRepresentative, SaveAddressAddModel, SaveAddressView>();
            HelpMap<DocumentRole03ChangeAddressContact, SaveAddressAddModel, SaveAddressView>();
            HelpMap<DocumentRole03ChangeProduct, SaveProductAddModel, SaveProductView>();

            HelpMap<DocumentRole03ChangeAddressJoiner, SaveAddressAddModel, SaveAddressView>();

            HelpMap<vDocumentRole03ChangeCertificationFileAddModel, DocumentRole03ChangeCertificationFile, Save010CertificationFile>();
            HelpMap<vDocumentRole03ChangeRequestGroupAddModel, DocumentRole03ChangeRequestGroup, Save010RequestGroup>();
        }
    }
}
