﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;

namespace DIP.TM.Web.Profiles
{
    public class FileProfile : Profile
    {
        public FileProfile()
        {
            CreateMap<File, FileView>();
            CreateMap<FileView, File>();

            CreateMap<FileGuest, FileView>();
            CreateMap<FileView, FileGuest>();
        }
    }
}
