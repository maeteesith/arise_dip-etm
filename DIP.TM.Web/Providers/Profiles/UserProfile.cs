﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;

namespace DIP.TM.Web.Profiles {
    public class UserProfile : Profile {
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public UserProfile() {
            HelpMap<UM_User, UserView>();

            CreateMap<vUserSession, vUserSessionView>();

            CreateMap<vUM_User, vUM_UserView>();
        }
    }
}
