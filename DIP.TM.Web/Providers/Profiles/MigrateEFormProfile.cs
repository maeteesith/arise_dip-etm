﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;

namespace DIP.TM.Web.Providers.Profiles
{
    public class MigrateEFormProfile : Profile
    {
        public void HelpMap<A, B>()
        {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }

        public MigrateEFormProfile()
        {

            #region 010
            CreateMap<eForm_Save010AddressContact, Save010AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save010AddressJoiner, Save010AddressJoiner>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save010AddressPeople, Save010AddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save010AddressRepresentative, Save010AddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save010Product, Save010Product>().ForMember(dest => dest.id, opt => opt.Ignore());

            CreateMap<eForm_Save010, Save010>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.Save010AddressContact, opt => opt.MapFrom(src => src.eForm_Save010AddressContact))
                .ForMember(dest => dest.Save010AddressJoiner, opt => opt.MapFrom(src => src.eForm_Save010AddressJoiner))
                .ForMember(dest => dest.Save010AddressPeople, opt => opt.MapFrom(src => src.eForm_Save010AddressPeople))
                .ForMember(dest => dest.Save010AddressRepresentative, opt => opt.MapFrom(src => src.eForm_Save010AddressRepresentative))
                .ForMember(dest => dest.Save010Product, opt => opt.MapFrom(src => src.eForm_Save010Product))
                ;

            CreateMap<Save010AddressContact, SaveAddressView>();
            CreateMap<Save010AddressJoiner, SaveAddressView>();
            CreateMap<Save010AddressPeople, SaveAddressView>();
            CreateMap<Save010AddressRepresentative, SaveAddressView>();
            CreateMap<Save010Product, SaveProductView>();

            CreateMap<Save010, Save010ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save010AddressContact))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.Save010AddressJoiner))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save010AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save010AddressRepresentative))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save010Product))
                ;
            #endregion

            #region 020
            CreateMap<eForm_Save020AddressContact, Save020AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save020AddressPeople, Save020AddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save020AddressRepresentative, Save020AddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore());

            CreateMap<eForm_Save020, Save020>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.Save020AddressContact, opt => opt.MapFrom(src => src.eForm_Save020AddressContact))
                .ForMember(dest => dest.Save020AddressPeople, opt => opt.MapFrom(src => src.eForm_Save020AddressPeople))
                .ForMember(dest => dest.Save020AddressRepresentative, opt => opt.MapFrom(src => src.eForm_Save020AddressRepresentative))
                ;

            CreateMap<Save020AddressContact, SaveAddressView>();
            CreateMap<Save020AddressPeople, SaveAddressView>();
            CreateMap<Save020AddressRepresentative, SaveAddressView>();

            CreateMap<Save020, Save020ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save020AddressContact))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save020AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save020AddressRepresentative))
                ;
            #endregion

            #region 021
            CreateMap<eForm_Save021, Save021>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<Save021, Save021ProcessView>();
            #endregion

            #region 030
            CreateMap<eForm_Save030AddressContact, Save030AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save030AddressPeople, Save030AddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save030AddressRepresentative, Save030AddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore());

            CreateMap<eForm_Save030, Save030>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.Save030AddressContact, opt => opt.MapFrom(src => src.eForm_Save030AddressContact))
                .ForMember(dest => dest.Save030AddressPeople, opt => opt.MapFrom(src => src.eForm_Save030AddressPeople))
                .ForMember(dest => dest.Save030AddressRepresentative, opt => opt.MapFrom(src => src.eForm_Save030AddressRepresentative))
                ;

            CreateMap<Save030AddressContact, SaveAddressView>();
            CreateMap<Save030AddressPeople, SaveAddressView>();
            CreateMap<Save030AddressRepresentative, SaveAddressView>();

            CreateMap<Save030, Save030ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save030AddressContact))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save030AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save030AddressRepresentative))
                ;
            #endregion

            #region 040
            CreateMap<eForm_Save040AddressContact, Save040AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save040AddressPeople, Save040AddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save040AddressRepresentative, Save040AddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save040Product, Save040Product>().ForMember(dest => dest.id, opt => opt.Ignore());

            CreateMap<eForm_Save040, Save040>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.Save040AddressContact, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressContact))
                .ForMember(dest => dest.Save040AddressPeople, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressPeople))
                .ForMember(dest => dest.Save040AddressRepresentative, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressRepresentative))
                .ForMember(dest => dest.Save040Product, opt => opt.MapFrom(src => src.eForm_Save040Product))
                ;

            CreateMap<eForm_Save040ReceiverAddressPeople, Save040AddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore()).ForMember(dest => dest.save_id, opt => opt.Ignore());
            CreateMap<eForm_Save040ReceiverAddressRepresentative, Save040AddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore()).ForMember(dest => dest.save_id, opt => opt.Ignore());
            CreateMap<eForm_Save040ReceiverAddressContact, Save040AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore()).ForMember(dest => dest.save_id, opt => opt.Ignore());
            CreateMap<eForm_Save040Product, Save040Product>().ForMember(dest => dest.id, opt => opt.Ignore()).ForMember(dest => dest.save_id, opt => opt.Ignore());

            CreateMap<Save040AddressContact, SaveAddressView>();
            CreateMap<Save040AddressPeople, SaveAddressView>();
            CreateMap<Save040AddressRepresentative, SaveAddressView>();
            CreateMap<Save040Product, SaveProductView>();

            CreateMap<Save040, Save040ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save040AddressContact))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save040AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save040AddressRepresentative))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save040Product))
                ;
            #endregion

            #region 050
            CreateMap<eForm_Save050AddressContact, Save050AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save050AddressPeople, Save050AddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save050AddressRepresentative, Save050AddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save050ReceiverAddressPeople, Save050ReceiverAddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save050ReceiverAddressRepresentative, Save050ReceiverAddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save050Product, Save050Product>().ForMember(dest => dest.id, opt => opt.Ignore());

            CreateMap<eForm_Save050, Save050>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.Save050AddressContact, opt => opt.MapFrom(src => src.eForm_Save050AddressContact))
                .ForMember(dest => dest.Save050AddressPeople, opt => opt.MapFrom(src => src.eForm_Save050AddressPeople))
                .ForMember(dest => dest.Save050AddressRepresentative, opt => opt.MapFrom(src => src.eForm_Save050AddressRepresentative))
                .ForMember(dest => dest.Save050ReceiverAddressPeople, opt => opt.MapFrom(src => src.eForm_Save050ReceiverAddressPeople))
                .ForMember(dest => dest.Save050ReceiverAddressRepresentative, opt => opt.MapFrom(src => src.eForm_Save050ReceiverAddressRepresentative))
                .ForMember(dest => dest.Save050Product, opt => opt.MapFrom(src => src.eForm_Save050Product))
                ;

            CreateMap<Save050AddressContact, SaveAddressView>();
            CreateMap<Save050AddressPeople, SaveAddressView>();
            CreateMap<Save050AddressRepresentative, SaveAddressView>();
            CreateMap<Save050ReceiverAddressPeople, SaveAddressView>();
            CreateMap<Save050ReceiverAddressRepresentative, SaveAddressView>();
            CreateMap<Save050Product, SaveProductView>();

            CreateMap<Save050, Save050ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save050AddressContact))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save050AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save050AddressRepresentative))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.Save050ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.Save050ReceiverAddressRepresentative))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save050Product))
                ;
            #endregion

            #region 060
            CreateMap<eForm_Save060AddressContact, Save060AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save060AddressPeople, Save060AddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save060AddressRepresentative, Save060AddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore());

            CreateMap<eForm_Save060, Save060>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.Save060AddressContact, opt => opt.MapFrom(src => src.eForm_Save060AddressContact))
                .ForMember(dest => dest.Save060AddressPeople, opt => opt.MapFrom(src => src.eForm_Save060AddressPeople))
                .ForMember(dest => dest.Save060AddressRepresentative, opt => opt.MapFrom(src => src.eForm_Save060AddressRepresentative))
                ;

            CreateMap<Save060AddressContact, SaveAddressView>();
            CreateMap<Save060AddressPeople, SaveAddressView>();
            CreateMap<Save060AddressRepresentative, SaveAddressView>();

            CreateMap<Save060, Save060ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save060AddressContact))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save060AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save060AddressRepresentative))
                ;
            #endregion

            #region 070
            CreateMap<eForm_Save070AddressContact, Save070AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save070Product, Save070Product>().ForMember(dest => dest.id, opt => opt.Ignore());

            CreateMap<eForm_Save070, Save070>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.Save070AddressContact, opt => opt.MapFrom(src => src.eForm_Save070AddressContact))
                .ForMember(dest => dest.Save070Product, opt => opt.MapFrom(src => src.eForm_Save070Product))
                ;

            CreateMap<Save070AddressContact, SaveAddressView>();
            CreateMap<Save070Product, SaveProductView>();

            CreateMap<Save070, Save070ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save070AddressContact))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.Save070Product))
                ;
            #endregion 070

            #region 080
            CreateMap<eForm_Save080AddressContact, Save080AddressContact>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save080AddressPeople, Save080AddressPeople>().ForMember(dest => dest.id, opt => opt.Ignore());
            CreateMap<eForm_Save080AddressRepresentative, Save080AddressRepresentative>().ForMember(dest => dest.id, opt => opt.Ignore());

            CreateMap<eForm_Save080, Save080>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.Save080AddressContact, opt => opt.MapFrom(src => src.eForm_Save080AddressContact))
                .ForMember(dest => dest.Save080AddressPeople, opt => opt.MapFrom(src => src.eForm_Save080AddressPeople))
                .ForMember(dest => dest.Save080AddressRepresentative, opt => opt.MapFrom(src => src.eForm_Save080AddressRepresentative))
                ;

            CreateMap<Save080AddressContact, SaveAddressView>();
            CreateMap<Save080AddressPeople, SaveAddressView>();
            CreateMap<Save080AddressRepresentative, SaveAddressView>();

            CreateMap<Save080, Save080ProcessView>()
                .ForMember(dest => dest.contact_address, opt => opt.MapFrom(src => src.Save080AddressContact))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.Save080AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.Save080AddressRepresentative))
                ;
            #endregion

            #region 120
            CreateMap<eForm_Save120, SaveOther>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                ;

            CreateMap<SaveOther, SaveOtherProcessView>();
            #endregion

            #region 140
            CreateMap<eForm_Save140, Save140>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                ;

            CreateMap<Save140, Save140ProcessView>();
            #endregion

            #region 150
            CreateMap<eForm_Save150, SaveOther>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                ;

            CreateMap<SaveOther, SaveOtherProcessView>();
            #endregion

            #region 190
            CreateMap<eForm_Save190, SaveOther>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                ;

            CreateMap<SaveOther, SaveOtherProcessView>();
            #endregion

            #region 200
            CreateMap<eForm_Save200, SaveOther>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                ;

            CreateMap<SaveOther, SaveOtherProcessView>();
            #endregion
        }
    }
}
