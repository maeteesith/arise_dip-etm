﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Logging;

namespace DIP.TM.Web.Providers.Profiles
{
    public class LogProfile : Profile
    {
        public LogProfile()
        {
            CreateMap<AuditLog, AuditLogModel>();
            CreateMap<AuditLogModel, AuditLog>()
                .ForMember(dest => dest.request, opt => opt.MapFrom(src => src.RequestBody))
                .ForMember(dest => dest.response, opt => opt.MapFrom(src => src.ResponseBody))
                .ForMember(dest => dest.method_name, opt => opt.MapFrom(src => src.Path));


            CreateMap<EventLog, AuditLogModel>();
            CreateMap<AuditLogModel, EventLog>()
                .ForMember(dest => dest.request, opt => opt.MapFrom(src => src.RequestBody))
                .ForMember(dest => dest.response, opt => opt.MapFrom(src => src.ResponseBody))
                .ForMember(dest => dest.method_name, opt => opt.MapFrom(src => src.Method))
                .ForMember(dest => dest.eventlog_type_code, opt => opt.MapFrom(src => src.StatusCode));
        }
    }
}
