﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads.Appeal;
using DIP.TM.Models.Views.Appeal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Web.Providers.Profiles
{
    public class AppealProcessProfile : Profile
    {
        public void HelpMap<A, B>()
        {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public AppealProcessProfile()
        {
            HelpMap<vAppeal_CaseSummary, vCaseSummaryView>();

            //CreateMap<Appeal_Role02SaveCaseSummaryAddModel, Appeal_Role02SaveCaseSummary>()
            //    .ForMember(dest => dest.Appeal_Role02SaveCaseSummaryEvidence, opt => opt.MapFrom(src => src.item_evidence));

            //CreateMap<Appeal_Role02SaveCaseSummary, Appeal_Role02SaveCaseSummaryAddModel>();



            CreateMap<Appeal_Role02SaveCaseSummaryAddModel, Appeal_Role02SaveCaseSummary>()
            .ForMember(dest => dest.Appeal_Role02SaveCaseSummaryEvidence, opt => opt.MapFrom(src => src.item_evidence))
            .ForMember(dest => dest.Appeal_Role02SaveCaseSummaryMarkReferent, opt => opt.MapFrom(src => src.item_mark_referent))
            .ForMember(dest => dest.Appeal_Role02SaveCaseSummaryRequestFKCountry, opt => opt.MapFrom(src => src.item_fk_country))
            .ForMember(dest => dest.Appeal_Role02SaveCaseSummaryRequestSimilar, opt => opt.MapFrom(src => src.item_similar))
            .ForMember(dest => dest.Appeal_Role02SaveCaseSummarySection, opt => opt.MapFrom(src => src.item_section))
            .ForMember(dest => dest.Appeal_Role02SaveCaseSummarySubcommitteeProposal, opt => opt.MapFrom(src => src.item_subcommittee_proposal))
            .ForMember(dest => dest.Appeal_Role02SaveCaseSummarySubcommitteeResulotion, opt => opt.MapFrom(src => src.item_subcommittee_resulotion));
            CreateMap<Appeal_Role02SaveCaseSummaryEvidenceAddModel, Appeal_Role02SaveCaseSummaryEvidence>();
            CreateMap<Appeal_Role02SaveCaseSummaryMarkReferentAddModel, Appeal_Role02SaveCaseSummaryMarkReferent>();
            CreateMap<Appeal_Role02SaveCaseSummaryRequestFKCountryAddModel, Appeal_Role02SaveCaseSummaryRequestFKCountry>();
            CreateMap<Appeal_Role02SaveCaseSummaryRequestSimilarAddModel, Appeal_Role02SaveCaseSummaryRequestSimilar>();
            CreateMap<Appeal_Role02SaveCaseSummarySectionAddModel, Appeal_Role02SaveCaseSummarySection>();
            CreateMap<Appeal_Role02SaveCaseSummarySubcommitteeProposalAddModel, Appeal_Role02SaveCaseSummarySubcommitteeProposal>();
            CreateMap<Appeal_Role02SaveCaseSummarySubcommitteeResulotionAddModel, Appeal_Role02SaveCaseSummarySubcommitteeResulotion>();

            CreateMap<Appeal_Role02SaveCaseSummary, Appeal_Role02SaveCaseSummaryView>()
            .ForMember(dest => dest.item_evidence, opt => opt.MapFrom(src => src.Appeal_Role02SaveCaseSummaryEvidence))
            .ForMember(dest => dest.item_mark_referent, opt => opt.MapFrom(src => src.Appeal_Role02SaveCaseSummaryMarkReferent))
            .ForMember(dest => dest.item_fk_country, opt => opt.MapFrom(src => src.Appeal_Role02SaveCaseSummaryRequestFKCountry))
            .ForMember(dest => dest.item_similar, opt => opt.MapFrom(src => src.Appeal_Role02SaveCaseSummaryRequestSimilar))
            .ForMember(dest => dest.item_section, opt => opt.MapFrom(src => src.Appeal_Role02SaveCaseSummarySection))
            .ForMember(dest => dest.item_subcommittee_proposal, opt => opt.MapFrom(src => src.Appeal_Role02SaveCaseSummarySubcommitteeProposal))
            .ForMember(dest => dest.item_subcommittee_resulotion, opt => opt.MapFrom(src => src.Appeal_Role02SaveCaseSummarySubcommitteeResulotion));
            CreateMap<Appeal_Role02SaveCaseSummaryEvidence, Appeal_Role02SaveCaseSummaryEvidenceView>();
            CreateMap<Appeal_Role02SaveCaseSummaryMarkReferent, Appeal_Role02SaveCaseSummaryMarkReferentView>();
            CreateMap<Appeal_Role02SaveCaseSummaryRequestFKCountry, Appeal_Role02SaveCaseSummaryRequestFKCountryView>();
            CreateMap<Appeal_Role02SaveCaseSummaryRequestSimilar, Appeal_Role02SaveCaseSummaryRequestSimilarView>();
            CreateMap<Appeal_Role02SaveCaseSummarySection, Appeal_Role02SaveCaseSummarySectionView>();
            CreateMap<Appeal_Role02SaveCaseSummarySubcommitteeProposal, Appeal_Role02SaveCaseSummarySubcommitteeProposalView>();
            CreateMap<Appeal_Role02SaveCaseSummarySubcommitteeResulotion, Appeal_Role02SaveCaseSummarySubcommitteeResulotionView>();

            CreateMap<Appeal_Role02SaveCaseSummaryView, Appeal_Role02SaveCaseSummaryAddModel>()
            .ForMember(dest => dest.item_evidence, opt => opt.MapFrom(src => src.item_evidence))
            .ForMember(dest => dest.item_mark_referent, opt => opt.MapFrom(src => src.item_mark_referent))
            .ForMember(dest => dest.item_fk_country, opt => opt.MapFrom(src => src.item_fk_country))
            .ForMember(dest => dest.item_similar, opt => opt.MapFrom(src => src.item_similar))
            .ForMember(dest => dest.item_section, opt => opt.MapFrom(src => src.item_section))
            .ForMember(dest => dest.item_subcommittee_proposal, opt => opt.MapFrom(src => src.item_subcommittee_proposal))
            .ForMember(dest => dest.item_subcommittee_resulotion, opt => opt.MapFrom(src => src.item_subcommittee_resulotion));
            CreateMap<Appeal_Role02SaveCaseSummaryEvidenceView, Appeal_Role02SaveCaseSummaryEvidenceAddModel>();
            CreateMap<Appeal_Role02SaveCaseSummaryMarkReferentView, Appeal_Role02SaveCaseSummaryMarkReferentAddModel>();
            CreateMap<Appeal_Role02SaveCaseSummaryRequestFKCountryView, Appeal_Role02SaveCaseSummaryRequestFKCountryAddModel>();
            CreateMap<Appeal_Role02SaveCaseSummaryRequestSimilarView, Appeal_Role02SaveCaseSummaryRequestSimilarAddModel>();
            CreateMap<Appeal_Role02SaveCaseSummarySectionView, Appeal_Role02SaveCaseSummarySectionAddModel>();
            CreateMap<Appeal_Role02SaveCaseSummarySubcommitteeProposalView, Appeal_Role02SaveCaseSummarySubcommitteeProposalAddModel>();
            CreateMap<Appeal_Role02SaveCaseSummarySubcommitteeResulotionView, Appeal_Role02SaveCaseSummarySubcommitteeResulotionAddModel>();






        }
    }
}
