﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;

namespace DIP.TM.Web.Profiles {
    public class Request01Profile : Profile {
        public Request01Profile() {
            CreateMap<Request01ProcessAddModel, Request01Process>()
                .ForMember(dest => dest.Request01Item, opt => opt.MapFrom(src => src.item_list));
            CreateMap<Request01ItemAddModel, Request01Item>()
                .ForMember(dest => dest.Request01ItemSub, opt => opt.MapFrom(src => src.item_sub_list));
            CreateMap<Request01ItemSubAddModel, Request01ItemSub>();

            CreateMap<Request01Process, Request01ProcessView>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.Request01Item));
            CreateMap<Request01Item, Request01ItemView>()
                .ForMember(dest => dest.sound_file_, opt => opt.MapFrom(src => src.sound_file_))
                .ForMember(dest => dest.item_sub_list, opt => opt.MapFrom(src => src.Request01ItemSub))
                .ForMember(dest => dest.item_type_codeNavigation, opt => opt.MapFrom(src => src.item_type_codeNavigation))
                .ForMember(dest => dest.sound_type_codeNavigation, opt => opt.MapFrom(src => src.sound_type_codeNavigation))
                .ForMember(dest => dest.facilitation_act_status_codeNavigation, opt => opt.MapFrom(src => src.facilitation_act_status_codeNavigation));
            CreateMap<Request01ItemSub, Request01ItemSubView>();

            CreateMap<vRequest01Item, vRequest01ItemView>();

            CreateMap<RequestDocumentCollect, vRequestDocumentCollectView>();

            ////For Test
            CreateMap<Request01ProcessView, Request01ProcessAddModel>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.item_list));
            CreateMap<Request01ItemView, Request01ItemAddModel>()
                .ForMember(dest => dest.item_sub_list, opt => opt.MapFrom(src => src.item_sub_list));
            CreateMap<Request01ItemSubView, Request01ItemSubAddModel>();
        }
    }

    public class RequestOtherProfile : Profile {
        public RequestOtherProfile() {
            CreateMap<RequestOtherProcessAddModel, RequestOtherProcess>()
                .ForMember(dest => dest.RequestOtherItem, opt => opt.MapFrom(src => src.item_list))
                .ForMember(dest => dest.RequestOtherRequest01Item, opt => opt.MapFrom(src => src.request_01_item_list));
            CreateMap<RequestOtherItemAddModel, RequestOtherItem>()
                .ForMember(dest => dest.RequestOtherItemSub, opt => opt.MapFrom(src => src.RequestOtherItemSub));
            CreateMap<RequestOtherRequest01ItemAddModel, RequestOtherRequest01Item>();
            CreateMap<RequestOtherItemSubAddModel, RequestOtherItemSub>();

            CreateMap<RequestOtherProcess, RequestOtherProcessView>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.RequestOtherItem))
                .ForMember(dest => dest.request_01_item_list, opt => opt.MapFrom(src => src.RequestOtherRequest01Item));
            CreateMap<RequestOtherItem, RequestOtherItemView>()
                .ForMember(dest => dest.RequestOtherItemSub, opt => opt.MapFrom(src => src.RequestOtherItemSub));
            CreateMap<RequestOtherItemSub, RequestOtherItemSubView>();
            CreateMap<RequestOtherRequest01Item, RequestOtherRequest01ItemView>();

            CreateMap<RequestOtherProcessView, RequestOtherProcessAddModel>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.item_list))
                .ForMember(dest => dest.request_01_item_list, opt => opt.MapFrom(src => src.request_01_item_list));
            CreateMap<RequestOtherItemView, RequestOtherItemAddModel>()
                .ForMember(dest => dest.RequestOtherItemSub, opt => opt.MapFrom(src => src.RequestOtherItemSub));
            CreateMap<RequestOtherItemSubView, RequestOtherItemSubAddModel>();
            CreateMap<RequestOtherRequest01ItemView, RequestOtherRequest01ItemAddModel>();

            CreateMap<vRequestOtherItem, vRequestOtherItemView>();
        }
    }

    public class RequestEFormProfile : Profile {
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public RequestEFormProfile() {
            HelpMap<vEForm_eForm_SaveOtherAddModel, RequestOtherItemAddModel>();
            HelpMap<vEForm_eForm_SaveOtherAddModel, RequestOtherItemSubAddModel>();
            HelpMap<vEForm_eForm_SaveOtherAddModel, RequestOtherRequest01ItemAddModel>();
            HelpMap<RequestOtherItemView, vEForm_eForm_SaveOtherView>();

            //HelpMap<RequestEForm01ItemAddModel, RequestEForm01Item, RequestEForm01ItemView>();
            //HelpMap<RequestEForm01ItemSubAddModel, RequestEForm01ItemSub, RequestEForm01ItemSubView>();

            //CreateMap<vEForm_eForm_Save010_Product, RequestEForm01ItemSub>()
            //    .ForMember(dest => dest.item_sub_type_1_code, opt => opt.MapFrom(src => src.request_item_sub_type_1_code));

            //CreateMap<RequestEFormAddModel, RequestEFormView>()
            //    .ForMember(dest => dest.request01_item_list, opt => opt.MapFrom(src => src.request01_item_list));
            //CreateMap<RequestEForm01ItemAddModel, RequestEForm01ItemView>()
            //    .ForMember(dest => dest.request01_item_sub_list, opt => opt.MapFrom(src => src.request01_item_sub_list));

            //CreateMap<RequestEFormView, RequestEFormAddModel>()
            //.ForMember(dest => dest.request01_item_list, opt => opt.MapFrom(src => src.request01_item_list));
            //CreateMap<RequestEForm01ItemView, RequestEForm01ItemAddModel>()
            //    .ForMember(dest => dest.request01_item_sub_list, opt => opt.MapFrom(src => src.request01_item_sub_list));

            CreateMap<RequestEFormAddModel, Request01Process>()
                .ForMember(dest => dest.Request01Item, opt => opt.MapFrom(src => src.request01_item_list));
            CreateMap<RequestEForm01ItemAddModel, Request01Item>()
                .ForMember(dest => dest.Request01ItemSub, opt => opt.MapFrom(src => src.request01_item_sub_list));
            CreateMap<RequestEForm01ItemSubAddModel, Request01ItemSub>();

            CreateMap<Request01Process, RequestEFormView>()
                .ForMember(dest => dest.request01_item_list, opt => opt.MapFrom(src => src.Request01Item));
            CreateMap<Request01Item, RequestEForm01ItemView>()
                .ForMember(dest => dest.request01_item_sub_list, opt => opt.MapFrom(src => src.Request01ItemSub));
            CreateMap<Request01ItemSub, Request01ItemSubView>();

            
        }
    }
}
