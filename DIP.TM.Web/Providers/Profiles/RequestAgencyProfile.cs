﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.ExternalModels;
using DIP.TM.Models.Views;
using Newtonsoft.Json;

namespace DIP.TM.Web.Providers.Profiles {
    public class RequestAgency : Profile {
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public RequestAgency() {
            //CreateMap<AuthSsoResponseModel, UM_SSO_User>()
            //    .ForMember(dest => dest.user_sso_id, opt => opt.MapFrom(src => src.info.user_sso_id))
            //    .ForMember(dest => dest.data_json, opt => opt.MapFrom(src => JsonConvert.SerializeObject(src)));
            CreateMap<vRequestAgency, vRequestAgencyView>();
        }
    }
}
