﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using System.Linq;

namespace DIP.TM.Web.Profiles {
    public class ReceiptProfile : Profile {
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public ReceiptProfile() {
            CreateMap<ReceiptAddModel, Receipt>()
                .ForMember(dest => dest.ReceiptItem, opt => opt.MapFrom(src => src.item_list))
                .ForMember(dest => dest.ReceiptPaid, opt => opt.MapFrom(src => src.paid_list));
            HelpMap<ReceiptItemAddModel, ReceiptItem, ReceiptItemView>();
            HelpMap<ReceiptPaidAddModel, ReceiptPaid, ReceiptPaidView>();

            CreateMap<Receipt, ReceiptAddModel>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.ReceiptItem))
                .ForMember(dest => dest.paid_list, opt => opt.MapFrom(src => src.ReceiptPaid));

            CreateMap<Receipt, ReceiptView>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.ReceiptItem))
                .ForMember(dest => dest.paid_list, opt => opt.MapFrom(src => src.ReceiptPaid))
                .ForMember(dest => dest.created_by_name, opt => opt.MapFrom(src => src.created_byNavigation != null ? src.created_byNavigation.name : ""));

            ////For Test
            CreateMap<ReceiptView, ReceiptAddModel>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.item_list));
        }

        private bool OnlyActive(ReceiptItem arg) {
            return arg.is_deleted == false;
        }
    }
}
