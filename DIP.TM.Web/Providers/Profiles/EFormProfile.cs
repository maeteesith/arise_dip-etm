﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace DIP.TM.Web.Profiles {
    public class EFormProfile : Profile {
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public EFormProfile() {
            #region Post Round
            //CreateMap<PostRoundAddModel, PostRound>()
            //    .ForMember(dest => dest., opt => opt.MapFrom(src => src.))
            //    ;
            //CreateMap<PostRound, PostRoundView>()
            //    .ForMember(dest => dest., opt => opt.MapFrom(src => src.))
            //    ;
            //CreateMap<PostRound, PostRoundAddModel>()
            //    .ForMember(dest => dest., opt => opt.MapFrom(src => src.))
            //    ;
            HelpMap<PostRoundAddModel, PostRound, PostRoundView>();
            #endregion
            #region Public Round
            CreateMap<PublicRoundAddModel, PublicRound>()
                .ForMember(dest => dest.Save010, opt => opt.MapFrom(src => src.save010_list))
                .ForMember(dest => dest.PublicRoundItem, opt => opt.MapFrom(src => src.public_round_item))
                ;
            CreateMap<PublicRound, PublicRoundView>()
                .ForMember(dest => dest.save010_list, opt => opt.MapFrom(src => src.Save010))
                .ForMember(dest => dest.public_round_item, opt => opt.MapFrom(src => src.PublicRoundItem))
                ;
            CreateMap<PublicRound, PublicRoundAddModel>()
                .ForMember(dest => dest.save010_list, opt => opt.MapFrom(src => src.Save010))
                .ForMember(dest => dest.public_round_item, opt => opt.MapFrom(src => src.PublicRoundItem))
                ;

            HelpMap<PublicRoundItemAddModel, PublicRoundItem, PublicRoundItemView>();
            #endregion

            #region 010
            CreateMap<eForm_Save010AddModel, eForm_Save010>()
                .ForMember(dest => dest.eForm_Save010AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save010AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save010AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save010AddressJoiner, opt => opt.MapFrom(src => src.joiner_list))
                .ForMember(dest => dest.eForm_Save010AddressRepresentativeKor18, opt => opt.MapFrom(src => src.representative_kor_18_list))
                .ForMember(dest => dest.eForm_Save010Product, opt => opt.MapFrom(src => src.product_list))
                .ForMember(dest => dest.eForm_Save010PublicPayment, opt => opt.MapFrom(src => src.public_payment_list))
                .ForMember(dest => dest.eForm_Save010Kor10, opt => opt.MapFrom(src => src.kor10_list))
                .ForMember(dest => dest.eForm_Save010CheckingSimilarWordTranslate, opt => opt.MapFrom(src => src.checking_similar_translate_list))
                ;
            CreateMap<eForm_Save010, eForm_Save010View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save010AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save010AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save010AddressContact))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.eForm_Save010AddressJoiner))
                .ForMember(dest => dest.representative_kor_18_list, opt => opt.MapFrom(src => src.eForm_Save010AddressRepresentativeKor18))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save010Product))
                .ForMember(dest => dest.public_payment_list, opt => opt.MapFrom(src => src.eForm_Save010PublicPayment))
                .ForMember(dest => dest.kor10_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10))
                .ForMember(dest => dest.checking_similar_translate_list, opt => opt.MapFrom(src => src.eForm_Save010CheckingSimilarWordTranslate))
                .ForMember(dest => dest.save010_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save010_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.sound_file, opt => opt.MapFrom(src => src.sound_file_))
                .ForMember(dest => dest.sound_jpg_file, opt => opt.MapFrom(src => src.sound_jpg_file_))
                .ForMember(dest => dest.img_2D_file, opt => opt.MapFrom(src => src.img_file_2d_))
                .ForMember(dest => dest.img_3D_file_1, opt => opt.MapFrom(src => src.img_file_3d_id_1Navigation))
                .ForMember(dest => dest.img_3D_file_2, opt => opt.MapFrom(src => src.img_file_3d_id_2Navigation))
                .ForMember(dest => dest.img_3D_file_3, opt => opt.MapFrom(src => src.img_file_3d_id_3Navigation))
                .ForMember(dest => dest.img_3D_file_4, opt => opt.MapFrom(src => src.img_file_3d_id_4Navigation))
                .ForMember(dest => dest.img_3D_file_5, opt => opt.MapFrom(src => src.img_file_3d_id_5Navigation))
                .ForMember(dest => dest.img_3D_file_6, opt => opt.MapFrom(src => src.img_file_3d_id_6Navigation))
                ;
            ////E->M
            CreateMap<eForm_Save010, eForm_Save010AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save010AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save010AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save010AddressContact))
                .ForMember(dest => dest.joiner_list, opt => opt.MapFrom(src => src.eForm_Save010AddressJoiner))
                .ForMember(dest => dest.representative_kor_18_list, opt => opt.MapFrom(src => src.eForm_Save010AddressRepresentativeKor18))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save010Product))
                .ForMember(dest => dest.public_payment_list, opt => opt.MapFrom(src => src.eForm_Save010PublicPayment))
                .ForMember(dest => dest.kor10_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10))
                .ForMember(dest => dest.checking_similar_translate_list, opt => opt.MapFrom(src => src.eForm_Save010CheckingSimilarWordTranslate))
                ;

            CreateMap<eForm_Save010Kor10AddModel, eForm_Save010Kor10>()
                .ForMember(dest => dest.eForm_Save010Kor10AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save010Kor10AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save010Kor10Event, opt => opt.MapFrom(src => src.event_list))
                .ForMember(dest => dest.eForm_Save010Kor10Product, opt => opt.MapFrom(src => src.product_list))
                .ForMember(dest => dest.eForm_Save010Kor19, opt => opt.MapFrom(src => src.kor19_list))
                ;

            CreateMap<eForm_Save010Kor10, eForm_Save010Kor10View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10AddressRepresentative))
                .ForMember(dest => dest.event_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10Event))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10Product))
                .ForMember(dest => dest.kor19_list, opt => opt.MapFrom(src => src.eForm_Save010Kor19))
                ;

            CreateMap<eForm_Save010Kor10, eForm_Save010Kor10AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10AddressRepresentative))
                .ForMember(dest => dest.event_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10Event))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save010Kor10Product))
                .ForMember(dest => dest.kor19_list, opt => opt.MapFrom(src => src.eForm_Save010Kor19))
                ;

            CreateMap<eForm_Save010Kor19AddModel, eForm_Save010Kor19>()
                .ForMember(dest => dest.eForm_Save010Kor19AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save010Kor19AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                ;

            CreateMap<eForm_Save010Kor19, eForm_Save010Kor19View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save010Kor19AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save010Kor19AddressRepresentative))
                ;

            CreateMap<eForm_Save010Kor19, eForm_Save010Kor19AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save010Kor19AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save010Kor19AddressRepresentative))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save010AddressContact, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save010AddressJoiner, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save010AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save010AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save010AddressRepresentativeKor18, SaveAddressView>();
            HelpMap<SaveProductAddModel, eForm_Save010Product, SaveProductView>();
            HelpMap<eForm_Save010PublicPaymentAddModel, eForm_Save010PublicPayment, eForm_Save010PublicPaymentView>();
            HelpMap<eForm_Save010CheckingSimilarWordTranslateAddModel, eForm_Save010CheckingSimilarWordTranslate, eForm_Save010CheckingSimilarWordTranslateView>();

            HelpMap<SaveAddressAddModel, eForm_Save010Kor10AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save010Kor10AddressRepresentative, SaveAddressView>();
            HelpMap<eForm_Save010Kor10ProductAddModel, eForm_Save010Kor10Product, eForm_Save010Kor10ProductView>();
            HelpMap<eForm_Save010Kor10EventAddModel, eForm_Save010Kor10Event, eForm_Save010Kor10EventView>();

            HelpMap<SaveAddressAddModel, eForm_Save010Kor19AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save010Kor19AddressRepresentative, SaveAddressView>();
            #endregion
            #region 020
            CreateMap<eForm_Save020AddModel, eForm_Save020>()
                .ForMember(dest => dest.eForm_Save020AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save020AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save020AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                ;
            CreateMap<eForm_Save020, eForm_Save020View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save020AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save020AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save020AddressContact))
                .ForMember(dest => dest.save020_contact_type_codeNavigation, opt => opt.MapFrom(src => src.save020_contact_type_codeNavigation))
                .ForMember(dest => dest.save020_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save020_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.evidence_file, opt => opt.MapFrom(src => src.evidence_file_))
                ;
            CreateMap<eForm_Save020, eForm_Save020AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save020AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save020AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save020AddressContact))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save020AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save020AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save020AddressContact, SaveAddressView>();
            #endregion
            #region 021
            CreateMap<eForm_Save021AddModel, eForm_Save021>()
                .ForMember(dest => dest.eForm_Save021AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save021AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save021AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save021Dispute, opt => opt.MapFrom(src => src.dispute_list))
                ;
            CreateMap<eForm_Save021, eForm_Save021View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save021AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save021AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save021AddressContact))
                .ForMember(dest => dest.dispute_list, opt => opt.MapFrom(src => src.eForm_Save021Dispute))
                .ForMember(dest => dest.save021_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save021_representative_condition_type_codeNavigation))
                ;
            CreateMap<eForm_Save021, eForm_Save021AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save021AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save021AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save021AddressContact))
                .ForMember(dest => dest.dispute_list, opt => opt.MapFrom(src => src.eForm_Save021Dispute))
                ;

            CreateMap<eForm_Save021DisputeAddModel, eForm_Save021Dispute>()
                .ForMember(dest => dest.challenge_, opt => opt.MapFrom(src => src.challenge))
                ;
            CreateMap<eForm_Save021Dispute, eForm_Save021DisputeView>()
                .ForMember(dest => dest.challenge, opt => opt.MapFrom(src => src.challenge_))
                ;
            CreateMap<eForm_Save021Dispute, eForm_Save021DisputeAddModel>()
                .ForMember(dest => dest.challenge, opt => opt.MapFrom(src => src.challenge_))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save021AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save021AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save021AddressContact, SaveAddressView>();
            #endregion
            #region 030
            CreateMap<eForm_Save030AddModel, eForm_Save030>()
                .ForMember(dest => dest.eForm_Save030AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save030AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save030AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save030Evidence, opt => opt.MapFrom(src => src.evidence_list))
                .ForMember(dest => dest.eForm_Save030AppealOrder, opt => opt.MapFrom(src => src.eform_appeal_order_list))
                ;
            CreateMap<eForm_Save030, eForm_Save030View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save030AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save030AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save030AddressContact))
                .ForMember(dest => dest.evidence_list, opt => opt.MapFrom(src => src.eForm_Save030Evidence))
                .ForMember(dest => dest.eform_appeal_order_list, opt => opt.MapFrom(src => src.eForm_Save030AppealOrder))
                .ForMember(dest => dest.save030_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save030_representative_condition_type_codeNavigation))
                ;
            CreateMap<eForm_Save030, eForm_Save030AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save030AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save030AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save030AddressContact))
                .ForMember(dest => dest.evidence_list, opt => opt.MapFrom(src => src.eForm_Save030Evidence))
                .ForMember(dest => dest.eform_appeal_order_list, opt => opt.MapFrom(src => src.eForm_Save030AppealOrder))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save030AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save030AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save030AddressContact, SaveAddressView>();
            HelpMap<eForm_Save030EvidenceAddModel, eForm_Save030Evidence, eForm_Save030EvidenceView>();
            HelpMap<eForm_Save030AppealOrderAddModel, eForm_Save030AppealOrder, eForm_Save030AppealOrderView>();
            #endregion
            #region 040
            CreateMap<eForm_Save040AddModel, eForm_Save040>()
                .ForMember(dest => dest.eForm_Save040AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save040AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save040AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save040ReceiverAddressPeople, opt => opt.MapFrom(src => src.receiver_people_list))
                .ForMember(dest => dest.eForm_Save040ReceiverAddressRepresentative, opt => opt.MapFrom(src => src.receiver_representative_list))
                .ForMember(dest => dest.eForm_Save040ReceiverAddressContact, opt => opt.MapFrom(src => src.receiver_contact_address_list))
                .ForMember(dest => dest.eForm_Save040Product, opt => opt.MapFrom(src => src.product_list))
                ;
            CreateMap<eForm_Save040, eForm_Save040View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save040AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save040AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save040AddressContact))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressRepresentative))
                .ForMember(dest => dest.receiver_contact_address_list, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressContact))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save040Product))
                .ForMember(dest => dest.save040_transfer_form_codeNavigation, opt => opt.MapFrom(src => src.save040_transfer_form_codeNavigation))
                .ForMember(dest => dest.save040_transfer_type_codeNavigation, opt => opt.MapFrom(src => src.save040_transfer_type_codeNavigation))
                .ForMember(dest => dest.save040_transfer_part_codeNavigation, opt => opt.MapFrom(src => src.save040_transfer_part_codeNavigation))
                .ForMember(dest => dest.save040_submit_type_codeNavigation, opt => opt.MapFrom(src => src.save040_submit_type_codeNavigation))
                .ForMember(dest => dest.save040_receiver_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save040_receiver_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.save040_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save040_representative_condition_type_codeNavigation))
                ;
            CreateMap<eForm_Save040, eForm_Save040AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save040AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save040AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save040AddressContact))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressRepresentative))
                .ForMember(dest => dest.receiver_contact_address_list, opt => opt.MapFrom(src => src.eForm_Save040ReceiverAddressContact))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save040Product))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save040AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save040AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save040AddressContact, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save040ReceiverAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save040ReceiverAddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save040ReceiverAddressContact, SaveAddressView>();
            HelpMap<SaveProductAddModel, eForm_Save040Product, SaveProductView>();
            #endregion
            #region 050
            CreateMap<eForm_Save050AddModel, eForm_Save050>()
                .ForMember(dest => dest.eForm_Save050AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save050AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save050AddressPeopleChange, opt => opt.MapFrom(src => src.people_change_list))
                .ForMember(dest => dest.eForm_Save050AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save050ReceiverAddressPeople, opt => opt.MapFrom(src => src.receiver_people_list))
                .ForMember(dest => dest.eForm_Save050ReceiverAddressRepresentative, opt => opt.MapFrom(src => src.receiver_representative_list))
                .ForMember(dest => dest.eForm_Save050Product, opt => opt.MapFrom(src => src.product_list))
                ;
            CreateMap<eForm_Save050, eForm_Save050View>()
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save050AddressContact))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save050AddressPeople))
                .ForMember(dest => dest.people_change_list, opt => opt.MapFrom(src => src.eForm_Save050AddressPeopleChange))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save050AddressRepresentative))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save050ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save050ReceiverAddressRepresentative))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save050Product))
                .ForMember(dest => dest.save050_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save050_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.save050_allow_type_codeNavigation, opt => opt.MapFrom(src => src.save050_allow_type_codeNavigation))
                .ForMember(dest => dest.save050_contact_type_codeNavigation, opt => opt.MapFrom(src => src.save050_contact_type_codeNavigation))
                .ForMember(dest => dest.save050_extend_type_codeNavigation, opt => opt.MapFrom(src => src.save050_extend_type_codeNavigation))
                .ForMember(dest => dest.save050_people_type_codeNavigation, opt => opt.MapFrom(src => src.save050_people_type_codeNavigation))
                .ForMember(dest => dest.save050_receiver_people_type_codeNavigation, opt => opt.MapFrom(src => src.save050_receiver_people_type_codeNavigation))
                .ForMember(dest => dest.save050_transfer_form_codeNavigation, opt => opt.MapFrom(src => src.save050_transfer_form_codeNavigation))
                ;
            CreateMap<eForm_Save050, eForm_Save050AddModel>()
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save050AddressContact))
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save050AddressPeople))
                .ForMember(dest => dest.people_change_list, opt => opt.MapFrom(src => src.eForm_Save050AddressPeopleChange))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save050AddressRepresentative))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save050ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save050ReceiverAddressRepresentative))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save050Product))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save050AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save050AddressPeopleChange, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save050AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save050AddressContact, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save050ReceiverAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save050ReceiverAddressRepresentative, SaveAddressView>();
            HelpMap<SaveProductAddModel, eForm_Save050Product, SaveProductView>();

            #endregion
            #region 060
            CreateMap<eForm_Save060AddModel, eForm_Save060>()
                .ForMember(dest => dest.eForm_Save060AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save060AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save060AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save060ChallengerAddressPeople, opt => opt.MapFrom(src => src.challenger_people_list))
                .ForMember(dest => dest.eForm_Save060ChallengerAddressRepresentative, opt => opt.MapFrom(src => src.challenger_representative_list))
                .ForMember(dest => dest.eForm_Save060ChallengerAddressContact, opt => opt.MapFrom(src => src.challenger_contact_address_list))
                .ForMember(dest => dest.eForm_Save060ContractReceiverAddressPeople, opt => opt.MapFrom(src => src.contract_receiver_people_list))
                .ForMember(dest => dest.eForm_Save060ContractReceiverAddressRepresentative, opt => opt.MapFrom(src => src.contract_receiver_representative_list))
                .ForMember(dest => dest.eForm_Save060LegacyAddressPeople, opt => opt.MapFrom(src => src.legacy_people_list))
                .ForMember(dest => dest.eForm_Save060LegacyAddressRepresentative, opt => opt.MapFrom(src => src.legacy_representative_list))
                .ForMember(dest => dest.eForm_Save060ReceiverAddressPeople, opt => opt.MapFrom(src => src.receiver_people_list))
                .ForMember(dest => dest.eForm_Save060ReceiverAddressRepresentative, opt => opt.MapFrom(src => src.receiver_representative_list))
                .ForMember(dest => dest.eForm_Save060TransferAddressPeople, opt => opt.MapFrom(src => src.transfer_people_list))
                .ForMember(dest => dest.eForm_Save060TransferAddressRepresentative, opt => opt.MapFrom(src => src.transfer_representative_list))
                .ForMember(dest => dest.eForm_Save060TransferReceiverAddressPeople, opt => opt.MapFrom(src => src.transfer_receiver_people_list))
                .ForMember(dest => dest.eForm_Save060TransferReceiverAddressRepresentative, opt => opt.MapFrom(src => src.transfer_receiver_representative_list))
                .ForMember(dest => dest.eForm_Save060Product, opt => opt.MapFrom(src => src.product_list))
                .ForMember(dest => dest.eForm_Save060CheckingSimilarWordTranslate, opt => opt.MapFrom(src => src.checking_similar_translate_list))
                ;
            CreateMap<eForm_Save060, eForm_Save060View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save060AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save060AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save060AddressContact))
                .ForMember(dest => dest.challenger_people_list, opt => opt.MapFrom(src => src.eForm_Save060ChallengerAddressPeople))
                .ForMember(dest => dest.challenger_representative_list, opt => opt.MapFrom(src => src.eForm_Save060ChallengerAddressRepresentative))
                .ForMember(dest => dest.challenger_contact_address_list, opt => opt.MapFrom(src => src.eForm_Save060ChallengerAddressContact))
                .ForMember(dest => dest.contract_receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save060ContractReceiverAddressPeople))
                .ForMember(dest => dest.contract_receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save060ContractReceiverAddressRepresentative))
                .ForMember(dest => dest.legacy_people_list, opt => opt.MapFrom(src => src.eForm_Save060LegacyAddressPeople))
                .ForMember(dest => dest.legacy_representative_list, opt => opt.MapFrom(src => src.eForm_Save060LegacyAddressRepresentative))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save060ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save060ReceiverAddressRepresentative))
                .ForMember(dest => dest.transfer_people_list, opt => opt.MapFrom(src => src.eForm_Save060TransferAddressPeople))
                .ForMember(dest => dest.transfer_representative_list, opt => opt.MapFrom(src => src.eForm_Save060TransferAddressRepresentative))
                .ForMember(dest => dest.transfer_receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save060TransferReceiverAddressPeople))
                .ForMember(dest => dest.transfer_receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save060TransferReceiverAddressRepresentative))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save060Product))
                .ForMember(dest => dest.checking_similar_translate_list, opt => opt.MapFrom(src => src.eForm_Save060CheckingSimilarWordTranslate))
                .ForMember(dest => dest.save060_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save060_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.sound_file, opt => opt.MapFrom(src => src.sound_file_))
                .ForMember(dest => dest.sound_jpg_file, opt => opt.MapFrom(src => src.sound_jpg_file_))
                .ForMember(dest => dest.img_2D_file, opt => opt.MapFrom(src => src.img_file_2d_))
                .ForMember(dest => dest.img_3D_file_1, opt => opt.MapFrom(src => src.img_file_3d_id_1Navigation))
                .ForMember(dest => dest.img_3D_file_2, opt => opt.MapFrom(src => src.img_file_3d_id_2Navigation))
                .ForMember(dest => dest.img_3D_file_3, opt => opt.MapFrom(src => src.img_file_3d_id_3Navigation))
                .ForMember(dest => dest.img_3D_file_4, opt => opt.MapFrom(src => src.img_file_3d_id_4Navigation))
                .ForMember(dest => dest.img_3D_file_5, opt => opt.MapFrom(src => src.img_file_3d_id_5Navigation))
                .ForMember(dest => dest.img_3D_file_6, opt => opt.MapFrom(src => src.img_file_3d_id_6Navigation))
                ;
            CreateMap<eForm_Save060, eForm_Save060AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save060AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save060AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save060AddressContact))
                .ForMember(dest => dest.challenger_people_list, opt => opt.MapFrom(src => src.eForm_Save060ChallengerAddressPeople))
                .ForMember(dest => dest.challenger_representative_list, opt => opt.MapFrom(src => src.eForm_Save060ChallengerAddressRepresentative))
                .ForMember(dest => dest.challenger_contact_address_list, opt => opt.MapFrom(src => src.eForm_Save060ChallengerAddressContact))
                .ForMember(dest => dest.contract_receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save060ContractReceiverAddressPeople))
                .ForMember(dest => dest.contract_receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save060ContractReceiverAddressRepresentative))
                .ForMember(dest => dest.legacy_people_list, opt => opt.MapFrom(src => src.eForm_Save060LegacyAddressPeople))
                .ForMember(dest => dest.legacy_representative_list, opt => opt.MapFrom(src => src.eForm_Save060LegacyAddressRepresentative))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save060ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save060ReceiverAddressRepresentative))
                .ForMember(dest => dest.transfer_people_list, opt => opt.MapFrom(src => src.eForm_Save060TransferAddressPeople))
                .ForMember(dest => dest.transfer_representative_list, opt => opt.MapFrom(src => src.eForm_Save060TransferAddressRepresentative))
                .ForMember(dest => dest.transfer_receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save060TransferReceiverAddressPeople))
                .ForMember(dest => dest.transfer_receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save060TransferReceiverAddressRepresentative))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save060Product))
                .ForMember(dest => dest.checking_similar_translate_list, opt => opt.MapFrom(src => src.eForm_Save060CheckingSimilarWordTranslate))
                ;


            HelpMap<SaveAddressAddModel, eForm_Save060AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060AddressContact, SaveAddressView>();

            HelpMap<SaveAddressAddModel, eForm_Save060ChallengerAddressContact, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060ChallengerAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060ChallengerAddressRepresentative, SaveAddressView>();

            HelpMap<SaveAddressAddModel, eForm_Save060ContractReceiverAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060ContractReceiverAddressRepresentative, SaveAddressView>();

            HelpMap<SaveAddressAddModel, eForm_Save060LegacyAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060LegacyAddressRepresentative, SaveAddressView>();

            HelpMap<SaveAddressAddModel, eForm_Save060ReceiverAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060ReceiverAddressRepresentative, SaveAddressView>();

            HelpMap<SaveAddressAddModel, eForm_Save060TransferAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060TransferAddressRepresentative, SaveAddressView>();

            HelpMap<SaveAddressAddModel, eForm_Save060TransferReceiverAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save060TransferReceiverAddressRepresentative, SaveAddressView>();

            HelpMap<SaveProductAddModel, eForm_Save060Product, SaveProductView>();
            HelpMap<eForm_Save060CheckingSimilarWordTranslateAddModel, eForm_Save060CheckingSimilarWordTranslate, eForm_Save060CheckingSimilarWordTranslateView>();
            #endregion
            #region 070
            CreateMap<eForm_Save070AddModel, eForm_Save070>()
                .ForMember(dest => dest.eForm_Save070AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save070AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save070AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save070Product, opt => opt.MapFrom(src => src.product_list))
                ;
            CreateMap<eForm_Save070, eForm_Save070View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save070AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save070AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save070AddressContact))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save070Product))
                .ForMember(dest => dest.save070_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save070_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.save070_request_item_type_codeNavigation, opt => opt.MapFrom(src => src.save070_request_item_type_codeNavigation))
                .ForMember(dest => dest.save070_extend_type_codeNavigation, opt => opt.MapFrom(src => src.save070_extend_type_codeNavigation))
                .ForMember(dest => dest.save070_contact_type_codeNavigation, opt => opt.MapFrom(src => src.save070_contact_type_codeNavigation))
                ;
            CreateMap<eForm_Save070, eForm_Save070AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save070AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save070AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save070AddressContact))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save070Product))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save070AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save070AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save070AddressContact, SaveAddressView>();
            HelpMap<SaveProductAddModel, eForm_Save070Product, SaveProductView>();
            #endregion
            #region 080
            CreateMap<eForm_Save080AddModel, eForm_Save080>()
                .ForMember(dest => dest.eForm_Save080AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save080AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save080AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save080ReceiverAddressPeople, opt => opt.MapFrom(src => src.receiver_people_list))
                .ForMember(dest => dest.eForm_Save080ReceiverAddressRepresentative, opt => opt.MapFrom(src => src.receiver_representative_list))
                .ForMember(dest => dest.eForm_Save080ReceiverAddressContact, opt => opt.MapFrom(src => src.receiver_contact_address_list))
                .ForMember(dest => dest.eForm_Save080Evidence, opt => opt.MapFrom(src => src.evidence_list))
                ;
            CreateMap<eForm_Save080, eForm_Save080View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save080AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save080AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save080AddressContact))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save080ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save080ReceiverAddressRepresentative))
                .ForMember(dest => dest.receiver_contact_address_list, opt => opt.MapFrom(src => src.eForm_Save080ReceiverAddressContact))
                .ForMember(dest => dest.evidence_list, opt => opt.MapFrom(src => src.eForm_Save080Evidence))
                .ForMember(dest => dest.save080_revoke_type_codeNavigation, opt => opt.MapFrom(src => src.save080_revoke_type_codeNavigation))
                .ForMember(dest => dest.save080_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save080_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.save080_request_item_type_codeNavigation, opt => opt.MapFrom(src => src.save080_request_item_type_codeNavigation))
                ;
            CreateMap<eForm_Save080, eForm_Save080AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save080AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save080AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save080AddressContact))
                .ForMember(dest => dest.receiver_people_list, opt => opt.MapFrom(src => src.eForm_Save080ReceiverAddressPeople))
                .ForMember(dest => dest.receiver_representative_list, opt => opt.MapFrom(src => src.eForm_Save080ReceiverAddressRepresentative))
                .ForMember(dest => dest.receiver_contact_address_list, opt => opt.MapFrom(src => src.eForm_Save080ReceiverAddressContact))
                .ForMember(dest => dest.evidence_list, opt => opt.MapFrom(src => src.eForm_Save080Evidence))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save080AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save080AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save080AddressContact, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save080ReceiverAddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save080ReceiverAddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save080ReceiverAddressContact, SaveAddressView>();
            HelpMap<eForm_Save080EvidenceAddModel, eForm_Save080Evidence, eForm_Save080EvidenceView>();
            #endregion
            #region 120
            CreateMap<eForm_Save120AddModel, eForm_Save120>()
                .ForMember(dest => dest.eForm_Save120AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save120AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                ;
            CreateMap<eForm_Save120, eForm_Save120View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save120AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save120AddressRepresentative))
                .ForMember(dest => dest.save120_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save120_representative_condition_type_codeNavigation))
                ;
            CreateMap<eForm_Save120, eForm_Save120AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save120AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save120AddressRepresentative))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save120AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save120AddressRepresentative, SaveAddressView>();
            #endregion
            #region 140
            CreateMap<eForm_Save140AddModel, eForm_Save140>()
                .ForMember(dest => dest.eForm_Save140AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save140AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save140AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save140Sue, opt => opt.MapFrom(src => src.sue_list))
                .ForMember(dest => dest.eForm_Save140Judgment, opt => opt.MapFrom(src => src.judgment_list))
                .ForMember(dest => dest.eForm_Save140FinalJudgment, opt => opt.MapFrom(src => src.final_judgment_list))
                ;
            CreateMap<eForm_Save140, eForm_Save140View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save140AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save140AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save140AddressContact))
                .ForMember(dest => dest.sue_list, opt => opt.MapFrom(src => src.eForm_Save140Sue))
                .ForMember(dest => dest.judgment_list, opt => opt.MapFrom(src => src.eForm_Save140Judgment))
                .ForMember(dest => dest.final_judgment_list, opt => opt.MapFrom(src => src.eForm_Save140FinalJudgment))
                .ForMember(dest => dest.save140_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save140_representative_condition_type_codeNavigation))
                ;
            CreateMap<eForm_Save140, eForm_Save140AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save140AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save140AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save140AddressContact))
                .ForMember(dest => dest.sue_list, opt => opt.MapFrom(src => src.eForm_Save140Sue))
                .ForMember(dest => dest.judgment_list, opt => opt.MapFrom(src => src.eForm_Save140Judgment))
                .ForMember(dest => dest.final_judgment_list, opt => opt.MapFrom(src => src.eForm_Save140FinalJudgment))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save140AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save140AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save140AddressContact, SaveAddressView>();
            HelpMap<eForm_Save140SueAddModel, eForm_Save140Sue, eForm_Save140SueView>();
            HelpMap<eForm_Save140JudgmentAddModel, eForm_Save140Judgment, eForm_Save140JudgmentView>();
            HelpMap<eForm_Save140FinalJudgmentAddModel, eForm_Save140FinalJudgment, eForm_Save140FinalJudgmentView>();
            #endregion
            #region 150
            CreateMap<eForm_Save150AddModel, eForm_Save150>()
                .ForMember(dest => dest.eForm_Save150AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save150AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save150Evidence, opt => opt.MapFrom(src => src.evidence_list))
                ;
            CreateMap<eForm_Save150, eForm_Save150View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save150AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save150AddressRepresentative))
                .ForMember(dest => dest.evidence_list, opt => opt.MapFrom(src => src.eForm_Save150Evidence))
                .ForMember(dest => dest.save150_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save150_representative_condition_type_codeNavigation))
                ;
            CreateMap<eForm_Save150, eForm_Save150AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save150AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save150AddressRepresentative))
                .ForMember(dest => dest.evidence_list, opt => opt.MapFrom(src => src.eForm_Save150Evidence))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save150AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save150AddressRepresentative, SaveAddressView>();
            HelpMap<eForm_Save150EvidenceAddModel, eForm_Save150Evidence, eForm_Save150EvidenceView>();
            #endregion
            #region 190
            CreateMap<eForm_Save190AddModel, eForm_Save190>()
                .ForMember(dest => dest.eForm_Save190AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save190AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save190AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                ;
            CreateMap<eForm_Save190, eForm_Save190View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save190AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save190AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save190AddressContact))
                .ForMember(dest => dest.save190_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save190_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.save190_request_type_codeNavigation, opt => opt.MapFrom(src => src.save190_request_type_codeNavigation))
                ;
            CreateMap<eForm_Save190, eForm_Save190AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save190AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save190AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save190AddressContact))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save190AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save190AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save190AddressContact, SaveAddressView>();
            #endregion
            #region 200
            CreateMap<eForm_Save200AddModel, eForm_Save200>()
                .ForMember(dest => dest.eForm_Save200RequestRef, opt => opt.MapFrom(src => src.request_ref_list))
                ;

            CreateMap<eForm_Save200, eForm_Save200View>()
                .ForMember(dest => dest.request_ref_list, opt => opt.MapFrom(src => src.eForm_Save200RequestRef))
                .ForMember(dest => dest.save200_recipient_codeNavigation, opt => opt.MapFrom(src => src.save200_recipient_codeNavigation))
                ;

            CreateMap<eForm_Save200, eForm_Save200AddModel>()
                .ForMember(dest => dest.request_ref_list, opt => opt.MapFrom(src => src.eForm_Save200RequestRef))
                ;

            HelpMap<eForm_Save200RequestRefAddModel, eForm_Save200RequestRef>();

            CreateMap<eForm_Save200RequestRef, eForm_Save200RequestRefView>()
                .ForMember(dest => dest.save200_request_type_codeNavigation, opt => opt.MapFrom(src => src.save200_request_type_codeNavigation))
                ;

            HelpMap<eForm_Save200RequestRef, RequestEForm01ItemSubAddModel>();

            #endregion
            #region 210
            CreateMap<eForm_Save210AddModel, eForm_Save210>()
                .ForMember(dest => dest.eForm_Save210AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save210AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                .ForMember(dest => dest.eForm_Save210AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
                .ForMember(dest => dest.eForm_Save210Product, opt => opt.MapFrom(src => src.product_list))
                ;
            CreateMap<eForm_Save210, eForm_Save210View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save210AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save210AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save210AddressContact))
                .ForMember(dest => dest.save210_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save210_representative_condition_type_codeNavigation))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save210Product))
                ;
            CreateMap<eForm_Save210, eForm_Save210AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save210AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save210AddressRepresentative))
                .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save210AddressContact))
                .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save210Product))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save210AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save210AddressRepresentative, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save210AddressContact, SaveAddressView>();
            HelpMap<SaveProductAddModel, eForm_Save210Product, SaveProductView>();
            #endregion
            #region 220
            //CreateMap<eForm_Save220AddModel, eForm_Save220>()
            //    .ForMember(dest => dest.eForm_Save220AddressPeople, opt => opt.MapFrom(src => src.people_list))
            //    .ForMember(dest => dest.eForm_Save220AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
            //    .ForMember(dest => dest.eForm_Save220AddressContact, opt => opt.MapFrom(src => src.contact_address_list))
            //    .ForMember(dest => dest.eForm_Save220Product, opt => opt.MapFrom(src => src.product_list))
            //    .ForMember(dest => dest.eForm_Save220Kor10, opt => opt.MapFrom(src => src.kor10_list))
            //    .ForMember(dest => dest.eForm_Save220CheckingSimilarWordTranslate, opt => opt.MapFrom(src => src.checking_similar_translate_list))
            //    ;
            //CreateMap<eForm_Save220, eForm_Save220View>()
            //    .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save220AddressPeople))
            //    .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save220AddressRepresentative))
            //    .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save220AddressContact))
            //    .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save220Product))
            //    .ForMember(dest => dest.kor10_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10))
            //    .ForMember(dest => dest.checking_similar_translate_list, opt => opt.MapFrom(src => src.eForm_Save220CheckingSimilarWordTranslate))
            //    .ForMember(dest => dest.save220_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save220_representative_condition_type_codeNavigation))
            //    .ForMember(dest => dest.sound_file, opt => opt.MapFrom(src => src.sound_file_))
            //    .ForMember(dest => dest.sound_jpg_file, opt => opt.MapFrom(src => src.sound_jpg_file_))
            //    ;
            //////E->M
            //CreateMap<eForm_Save220, eForm_Save220AddModel>()
            //    .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save220AddressPeople))
            //    .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save220AddressRepresentative))
            //    .ForMember(dest => dest.contact_address_list, opt => opt.MapFrom(src => src.eForm_Save220AddressContact))
            //    .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save220Product))
            //    .ForMember(dest => dest.kor10_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10))
            //    .ForMember(dest => dest.checking_similar_translate_list, opt => opt.MapFrom(src => src.eForm_Save220CheckingSimilarWordTranslate))
            //    ;

            //CreateMap<eForm_Save220Kor10AddModel, eForm_Save220Kor10>()
            //    .ForMember(dest => dest.eForm_Save220Kor10AddressPeople, opt => opt.MapFrom(src => src.people_list))
            //    .ForMember(dest => dest.eForm_Save220Kor10AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
            //    .ForMember(dest => dest.eForm_Save220Kor10Event, opt => opt.MapFrom(src => src.event_list))
            //    .ForMember(dest => dest.eForm_Save220Kor10Product, opt => opt.MapFrom(src => src.product_list))
            //    .ForMember(dest => dest.eForm_Save220Kor19, opt => opt.MapFrom(src => src.kor19_list))
            //    ;

            //CreateMap<eForm_Save220Kor10, eForm_Save220Kor10View>()
            //    .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10AddressPeople))
            //    .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10AddressRepresentative))
            //    .ForMember(dest => dest.event_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10Event))
            //    .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10Product))
            //    .ForMember(dest => dest.kor19_list, opt => opt.MapFrom(src => src.eForm_Save220Kor19))
            //    ;

            //CreateMap<eForm_Save220Kor10, eForm_Save220Kor10AddModel>()
            //    .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10AddressPeople))
            //    .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10AddressRepresentative))
            //    .ForMember(dest => dest.event_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10Event))
            //    .ForMember(dest => dest.product_list, opt => opt.MapFrom(src => src.eForm_Save220Kor10Product))
            //    .ForMember(dest => dest.kor19_list, opt => opt.MapFrom(src => src.eForm_Save220Kor19))
            //    ;

            //CreateMap<eForm_Save220Kor19AddModel, eForm_Save220Kor19>()
            //    .ForMember(dest => dest.eForm_Save220Kor19AddressPeople, opt => opt.MapFrom(src => src.people_list))
            //    .ForMember(dest => dest.eForm_Save220Kor19AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
            //    ;

            //CreateMap<eForm_Save220Kor19, eForm_Save220Kor19View>()
            //    .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save220Kor19AddressPeople))
            //    .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save220Kor19AddressRepresentative))
            //    ;

            //CreateMap<eForm_Save220Kor19, eForm_Save220Kor19AddModel>()
            //    .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save220Kor19AddressPeople))
            //    .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save220Kor19AddressRepresentative))
            //    ;

            //HelpMap<SaveAddressAddModel, eForm_Save220AddressContact, SaveAddressView>();
            //HelpMap<SaveAddressAddModel, eForm_Save220AddressPeople, SaveAddressView>();
            //HelpMap<SaveAddressAddModel, eForm_Save220AddressRepresentative, SaveAddressView>();
            //HelpMap<SaveProductAddModel, eForm_Save220Product, SaveProductView>();
            //HelpMap<eForm_Save220CheckingSimilarWordTranslateAddModel, eForm_Save220CheckingSimilarWordTranslate, eForm_Save220CheckingSimilarWordTranslateView>();

            //HelpMap<SaveAddressAddModel, eForm_Save220Kor10AddressPeople, SaveAddressView>();
            //HelpMap<SaveAddressAddModel, eForm_Save220Kor10AddressRepresentative, SaveAddressView>();
            //HelpMap<eForm_Save220Kor10ProductAddModel, eForm_Save220Kor10Product, eForm_Save220Kor10ProductView>();
            //HelpMap<eForm_Save220Kor10EventAddModel, eForm_Save220Kor10Event, eForm_Save220Kor10EventView>();

            //HelpMap<SaveAddressAddModel, eForm_Save220Kor19AddressPeople, SaveAddressView>();
            //HelpMap<SaveAddressAddModel, eForm_Save220Kor19AddressRepresentative, SaveAddressView>();
            #endregion
            #region 230
            CreateMap<eForm_Save230AddModel, eForm_Save230>()
                .ForMember(dest => dest.eForm_Save230AddressPeople, opt => opt.MapFrom(src => src.people_list))
                .ForMember(dest => dest.eForm_Save230AddressRepresentative, opt => opt.MapFrom(src => src.representative_list))
                ;
            CreateMap<eForm_Save230, eForm_Save230View>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save230AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save230AddressRepresentative))
                .ForMember(dest => dest.save230_document_type_codeNavigation, opt => opt.MapFrom(src => src.save230_document_type_codeNavigation))
                .ForMember(dest => dest.save230_informer_type_codeNavigation, opt => opt.MapFrom(src => src.save230_informer_type_codeNavigation))
                .ForMember(dest => dest.save230_representative_condition_type_codeNavigation, opt => opt.MapFrom(src => src.save230_representative_condition_type_codeNavigation))
                ;
            CreateMap<eForm_Save230, eForm_Save230AddModel>()
                .ForMember(dest => dest.people_list, opt => opt.MapFrom(src => src.eForm_Save230AddressPeople))
                .ForMember(dest => dest.representative_list, opt => opt.MapFrom(src => src.eForm_Save230AddressRepresentative))
                ;

            HelpMap<SaveAddressAddModel, eForm_Save230AddressPeople, SaveAddressView>();
            HelpMap<SaveAddressAddModel, eForm_Save230AddressRepresentative, SaveAddressView>();
            #endregion

            //HelpMap<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>();
            //HelpMap<eForm_Save021AddModel, eForm_Save021, eForm_Save021View>();
            //HelpMap<eForm_Save030AddModel, eForm_Save030, eForm_Save030View>();
            //HelpMap<eForm_Save040AddModel, eForm_Save040, eForm_Save040View>();
            //HelpMap<eForm_Save050AddModel, eForm_Save050, eForm_Save050View>();
            //HelpMap<eForm_Save060AddModel, eForm_Save060, eForm_Save060View>();
            //HelpMap<eForm_Save070AddModel, eForm_Save070, eForm_Save070View>();
            //HelpMap<eForm_Save080AddModel, eForm_Save080, eForm_Save080View>();
            //HelpMap<eForm_Save120AddModel, eForm_Save120, eForm_Save120View>();
            //HelpMap<eForm_Save190AddModel, eForm_Save190, eForm_Save190View>();
            //HelpMap<eForm_Save200AddModel, eForm_Save200, eForm_Save200View>();

            //
            HelpMap<eForm_Save010, Save010AddModel>();

            //
            HelpMap<Save010CheckingSimilarWordTranslateView, eForm_Save060CheckingSimilarWordTranslateView>();
        }
    }
}
