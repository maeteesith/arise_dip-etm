﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.ExternalModels;
using Newtonsoft.Json;

namespace DIP.TM.Web.Providers.Profiles
{
    public class UserSSOProfile : Profile
    {
        public UserSSOProfile()
        {
            // sso vs um_sso_user
            CreateMap<AuthSsoResponseModel, UM_SSO_User>()
                .ForMember(dest => dest.user_sso_id, opt => opt.MapFrom(src => src.info.user_sso_id))
                .ForMember(dest => dest.data_json, opt => opt.MapFrom(src => JsonConvert.SerializeObject(src)));

            CreateMap<UM_SSO_User, AuthSsoResponseModel>();


            // sso vs um_user
            CreateMap<AuthSsoResponseModel, UM_User>()
               .ForMember(dest => dest.name, opt => opt.MapFrom(src => src.info.name))
               .ForMember(dest => dest.name_in_department, opt => opt.MapFrom(src => src.info.name))
               .ForMember(dest => dest.email, opt => opt.MapFrom(src => src.info.email))
               //.ForMember(dest => dest.department_id, 1)
               .ForMember(dest => dest.username, opt => opt.MapFrom(src => src.info.username));

            CreateMap<UM_User, AuthSsoResponseModel>();

        }
    }
}
