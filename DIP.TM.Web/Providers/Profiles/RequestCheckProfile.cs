﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.ExternalModels;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using Newtonsoft.Json;
using System.Linq;

namespace DIP.TM.Web.Providers.Profiles {
    public class RequestCheckProfile : Profile {
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public RequestCheckProfile() {
            CreateMap<RequestCheckAddModel, RequestCheck>()
                .ForMember(dest => dest.RequestCheckItem, opt => opt.MapFrom(src => src.request_check_item_list))
                .ForMember(dest => dest.RequestCheckRequestNumber, opt => opt.MapFrom(src => src.request_check_request_number_list));

            CreateMap<RequestCheck, RequestCheckView>()
                .ForMember(dest => dest.created_byNavigation, opt => opt.MapFrom(src => src.created_byNavigation))
                .ForMember(dest => dest.request_check_item_list, opt => opt.MapFrom(src => src.RequestCheckItem))
                .ForMember(dest => dest.request_check_request_number_list, opt => opt.MapFrom(src => src.RequestCheckRequestNumber))
                ;

            //CreateMap<RequestCheck, RequestCheckView>();
            //CreateMap<RequestCheckItem, RequestCheckItemView>();

            CreateMap<RequestCheckView, RequestCheck>()
                .ForMember(dest => dest.created_byNavigation, opt => opt.MapFrom(src => src.created_byNavigation))
                .ForMember(dest => dest.RequestCheckItem, opt => opt.MapFrom(src => src.request_check_item_list))
                .ForMember(dest => dest.RequestCheckRequestNumber, opt => opt.MapFrom(src => src.request_check_request_number_list));

            HelpMap<RequestCheckItemAddModel, RequestCheckItem, RequestCheckItemView>();
            HelpMap<RequestCheckRequestNumberAddModel, RequestCheckRequestNumber, RequestCheckRequestNumberView>();
            //HelpMap<RequestCheckAddModel, RequestCheck, RequestCheckView>();
        }
    }
}
