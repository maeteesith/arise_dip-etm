﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;

namespace DIP.TM.Web.Profiles {
    public class PriceMasterProfile : Profile {
        public PriceMasterProfile() {
            CreateMap<PriceMaster, PriceMasterView>();
            CreateMap<PriceMasterView, PriceMaster>();
        }
    }

    public class MasterProfile : Profile {
        public MasterProfile() {
            CreateMap<ReferenceMasterView, ReferenceMasterModel>();

            CreateMap<RM_AddressCardType, ReferenceMasterView>();
            CreateMap<RM_AddressCareer, ReferenceMasterView>();
            CreateMap<RM_AddressCountry, ReferenceMasterView>();
            CreateMap<RM_AddressDistrict, ReferenceMasterView>();
            CreateMap<RM_AddressEFormCardType, ReferenceMasterView>();
            CreateMap<RM_AddressNationality, ReferenceMasterView>();
            CreateMap<RM_AddressPostal, ReferenceMasterView>();
            CreateMap<RM_AddressProvince, ReferenceMasterView>();
            CreateMap<RM_AddressReceiverType, ReferenceMasterView>();
            CreateMap<RM_AddressRepresentativeConditionTypeCode, ReferenceMasterView>();
            CreateMap<RM_AddressSex, ReferenceMasterView>();
            CreateMap<RM_AddressSubDistrict, ReferenceMasterView>();
            CreateMap<RM_AddressType, ReferenceMasterView>();
            CreateMap<RM_CheckingOrganizeTypeCode, ReferenceMasterView>();
            CreateMap<RM_CheckingReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_CheckingStatusCode, ReferenceMasterView>();
            CreateMap<RM_CheckingTypeCode, ReferenceMasterView>();
            CreateMap<RM_CheckingWordSameConditionCode, ReferenceMasterView>();
            CreateMap<RM_CheckingWordTranslateDictionaryCode, ReferenceMasterView>();
            CreateMap<RM_CheckingWordTranslateStatusCode, ReferenceMasterView>();
            CreateMap<RM_CommercialAffairsProvince, ReferenceMasterView>();
            CreateMap<RM_ConsideringBookStatusCode, ReferenceMasterView>();
            CreateMap<RM_ConsideringInstructionStatusCode, ReferenceMasterView>();
            CreateMap<RM_ConsideringReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_ConsideringSimilarDocumentStatusCode, ReferenceMasterView>();
            CreateMap<RM_ConsideringSimilarEvidence27Code, ReferenceMasterView>();
            CreateMap<RM_ConsideringSimilarInstructionRuleCode, ReferenceMasterView>();
            CreateMap<RM_ConsideringSimilarInstructionRuleItemCode, ReferenceMasterView>();
            CreateMap<RM_ConsideringSimilarInstructionRuleStatusCode, ReferenceMasterView>();
            CreateMap<RM_ConsideringSimilarInstructionTypeCode, ReferenceMasterView>();
            CreateMap<RM_Department, ReferenceMasterView>();
            CreateMap<RM_DepartmentGroupCode, ReferenceMasterView>();
            CreateMap<RM_DocumentClassificationCollectStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentClassificationStatus, ReferenceMasterView>();
            CreateMap<RM_DocumentClassificationVersionStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole02ChangeKor120TypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole02CheckStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole02PrintCoverStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole02PrintDocumentStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole02PrintListStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole02ReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole02StatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole03ReceiveReturnTypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole03ReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole03RequestTypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole03StatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04CheckList2Code, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04CheckListCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04CreateStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04CreateTypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04ReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04Release20StatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04Release20TypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04ReleaseCaseCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04ReleaseReasonCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04ReleaseRequestSendTypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04ReleaseRequestTypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04ReleaseStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04ReleaseTypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04SendTypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04StatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole04TypeCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole05CreateStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole05ReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole05Release20StatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole05ReleaseStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole05StatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentRole2CheckSendBackDepartmentCode, ReferenceMasterView>();
            CreateMap<RM_DocumentStatusCode, ReferenceMasterView>();
            CreateMap<RM_DocumentTypeCode, ReferenceMasterView>();
            CreateMap<RM_FacilitationActStatus, ReferenceMasterView>();
            CreateMap<RM_Floor3DocumentTypeCode, ReferenceMasterView>();
            CreateMap<RM_Floor3RegistrarStatusCode, ReferenceMasterView>();
            CreateMap<RM_PaidChannelType, ReferenceMasterView>();
            CreateMap<RM_PostRoundActionPostStatusCode, ReferenceMasterView>();
            CreateMap<RM_PostRoundActionPostTypeCode, ReferenceMasterView>();
            CreateMap<RM_PostRoundDocumentPostStatusCode, ReferenceMasterView>();
            CreateMap<RM_PostRoundInstructionRuleCode, ReferenceMasterView>();
            CreateMap<RM_PostRoundInstructionRuleFileTypeCode, ReferenceMasterView>();
            CreateMap<RM_PostRoundTypeCode, ReferenceMasterView>();
            CreateMap<RM_PublicDocumentPaymentInstructionRuleCode, ReferenceMasterView>();
            CreateMap<RM_PublicReceiveDoStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole01Case41StatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole01SendBackDepartment, ReferenceMasterView>();
            CreateMap<RM_PublicRole02CheckStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02ConsiderDocumentStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02ConsiderPaymentStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02DocumentPaymentStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02DocumentRegisterStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02DocumentStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02DocumentTypeCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02ReceivePostStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02ReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole02SendDocumentStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole04CheckInstructionRuleTypeCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole04DocumentStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole04ReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole04SendBackDepartment, ReferenceMasterView>();
            CreateMap<RM_PublicRole04StatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole05Case41StatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole05ConsiderPaymentStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRole05SendBackDepartment, ReferenceMasterView>();
            CreateMap<RM_PublicRole05StatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRoundCase41StatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicRoundStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicSourceCode, ReferenceMasterView>();
            CreateMap<RM_PublicStatusCode, ReferenceMasterView>();
            CreateMap<RM_PublicTypeCode, ReferenceMasterView>();
            CreateMap<RM_ReceiptChannelType, ReferenceMasterView>();
            CreateMap<RM_ReceiptStatus, ReferenceMasterView>();
            CreateMap<RM_ReceiverTypeCode, ReferenceMasterView>();
            CreateMap<RM_RepresentativeType, ReferenceMasterView>();
            CreateMap<RM_Request01ReceiptChangeCode, ReferenceMasterView>();
            CreateMap<RM_RequestCheckItem, ReferenceMasterView>();
            CreateMap<RM_RequestDocumentCollectStatusCode, ReferenceMasterView>();
            CreateMap<RM_RequestDocumentCollectTypeCode, ReferenceMasterView>();
            CreateMap<RM_RequestEvidenceType, ReferenceMasterView>();
            CreateMap<RM_RequestImageMarkType, ReferenceMasterView>();
            CreateMap<RM_RequestItemSubType1, ReferenceMasterView>();
            CreateMap<RM_RequestItemSubType2, ReferenceMasterView>();
            CreateMap<RM_RequestItemSubTypeGroup, ReferenceMasterView>();
            CreateMap<RM_RequestItemType, ReferenceMasterView>();
            CreateMap<RM_RequestItemTypeSplit, ReferenceMasterView>();
            CreateMap<RM_RequestMarkFeatureCode, ReferenceMasterView>();
            CreateMap<RM_RequestPrintCoverTypeCode, ReferenceMasterView>();
            CreateMap<RM_RequestSoundType, ReferenceMasterView>();
            CreateMap<RM_RequestSource, ReferenceMasterView>();
            CreateMap<RM_RequestStatus, ReferenceMasterView>();
            CreateMap<RM_RequestType, ReferenceMasterView>();
            CreateMap<RM_Save010AssertTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save010ConsideringSimilarDocument04TypeCode, ReferenceMasterView>();
            CreateMap<RM_Save010ConsideringSimilarDocument06TypeCode, ReferenceMasterView>();
            CreateMap<RM_Save010ConsideringSimilarDocument12TypeCode, ReferenceMasterView>();
            CreateMap<RM_Save010DocumentClassificationImageType, ReferenceMasterView>();
            CreateMap<RM_Save010DocumentClassificationLanguage, ReferenceMasterView>();
            CreateMap<RM_Save010DocumentClassificationSoundLast, ReferenceMasterView>();
            CreateMap<RM_Save010DocumentClassificationSoundLastOther, ReferenceMasterView>();
            CreateMap<RM_Save010DocumentClassificationSoundType, ReferenceMasterView>();
            CreateMap<RM_Save010DocumentClassificationWordFirst, ReferenceMasterView>();
            CreateMap<RM_Save010InstructionRuleDocumentReceiveStatusCode, ReferenceMasterView>();
            CreateMap<RM_Save010InstructionRuleDocumentStatusCode, ReferenceMasterView>();
            CreateMap<RM_Save010RepresentativeType, ReferenceMasterView>();
            CreateMap<RM_Save010RequestGroupStatusCode, ReferenceMasterView>();
            CreateMap<RM_Save020AddressType, ReferenceMasterView>();
            CreateMap<RM_Save030AddressType, ReferenceMasterView>();
            CreateMap<RM_Save030AppealMakerType, ReferenceMasterView>();
            CreateMap<RM_Save030AppealReason, ReferenceMasterView>();
            CreateMap<RM_Save030AppealType, ReferenceMasterView>();
            CreateMap<RM_Save030InformerChallengeTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save030InformerPeopleTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save030InformerPeriodTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save030InformerReceiverTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save030SearchTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save040DonorAddressType, ReferenceMasterView>();
            CreateMap<RM_Save040ReceiverAddressType, ReferenceMasterView>();
            CreateMap<RM_Save040RepresentativeType, ReferenceMasterView>();
            CreateMap<RM_Save040SubmitType, ReferenceMasterView>();
            CreateMap<RM_Save040TransferForm, ReferenceMasterView>();
            CreateMap<RM_Save040TransferPart, ReferenceMasterView>();
            CreateMap<RM_Save040TransferRequestType, ReferenceMasterView>();
            CreateMap<RM_Save040TransferType, ReferenceMasterView>();
            CreateMap<RM_Save050AddressType, ReferenceMasterView>();
            CreateMap<RM_Save050AllowAddressType, ReferenceMasterView>();
            CreateMap<RM_Save050AllowTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save050ExtendTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save050PeopleTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save050PeriodAddressType, ReferenceMasterView>();
            CreateMap<RM_Save050ReceiverPeopleTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save050RepresentativeType, ReferenceMasterView>();
            CreateMap<RM_Save050SearchTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save050SubmitTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save050TransferForm, ReferenceMasterView>();
            CreateMap<RM_Save060AddressTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save060AllowContactCode, ReferenceMasterView>();
            CreateMap<RM_Save060ChallengerAddressTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save060MenuCode, ReferenceMasterView>();
            CreateMap<RM_Save060MenuSubContractCode, ReferenceMasterView>();
            CreateMap<RM_Save060MenuSubOthersCode, ReferenceMasterView>();
            CreateMap<RM_Save060PeriodRequestCode, ReferenceMasterView>();
            CreateMap<RM_Save060ReceiverTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save060RepresentativeChangeCode, ReferenceMasterView>();
            CreateMap<RM_Save060RepresentativeType, ReferenceMasterView>();
            CreateMap<RM_Save060SearchTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save070ExtendType, ReferenceMasterView>();
            CreateMap<RM_Save070SubmitTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save080AddressOthersType, ReferenceMasterView>();
            CreateMap<RM_Save080AddressPeopleType, ReferenceMasterView>();
            CreateMap<RM_Save080AddressPeriodType, ReferenceMasterView>();
            CreateMap<RM_Save080AddressReceiverType, ReferenceMasterView>();
            CreateMap<RM_Save080AddressType, ReferenceMasterView>();
            CreateMap<RM_Save080InformerOthersTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save080InformerPeopleTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save080InformerPeriodTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save080InformerReceiverTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save080PeopleType, ReferenceMasterView>();
            CreateMap<RM_Save080RepresentativeType, ReferenceMasterView>();
            CreateMap<RM_Save080RequestItemTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save080RevokeTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save080SearchTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save080SubmitTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save120SubmitTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save140DepartmentCode, ReferenceMasterView>();
            CreateMap<RM_Save140InformerTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save140InformTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save140SubmitTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save140SueReportTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save140SueTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save150InformerPeopleTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save150InformerPeriodTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save150InformerReceiverTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save150MarkTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save150SearchTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save190InformerChallengerTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save190InformerPeopleTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save190InformerPeriodTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save190InformerReceiverTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save190RequestTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save190SearchTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save200ReceipientCode, ReferenceMasterView>();
            CreateMap<RM_Save200RequestTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save200SubjectTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save210AddressTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save210InformerTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save210SearchTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save220AddressTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save220InformerTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save220SearchTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save230DocumentTypeCode, ReferenceMasterView>();
            CreateMap<RM_Save230InformerTypeCode, ReferenceMasterView>();
            CreateMap<RM_SaveConsiderStatusCode, ReferenceMasterView>();
            CreateMap<RM_SaveOtopType, ReferenceMasterView>();
            CreateMap<RM_SaveStatus, ReferenceMasterView>();
            CreateMap<RM_TranslationLanguage, ReferenceMasterView>();

            //appeal master referent
            CreateMap<RM_AppealBoardResolution, ReferenceMasterView>();
            CreateMap<RM_AppealRole02CaseSummaryProcessCode, ReferenceMasterView>();
            CreateMap<RM_AppealRole02CaseSummaryStatusCode, ReferenceMasterView>();
            CreateMap<RM_AppealRole02CaseSummaryStepCode, ReferenceMasterView>();
            CreateMap<RM_AppealSection, ReferenceMasterView>();
            

        }
    }
    public class Location : Profile {
        public Location() {
            CreateMap<vLocation, vLocationView>();
        }
    }
}
