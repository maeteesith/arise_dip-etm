﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace DIP.TM.Web.Profiles {
    public class ViewProfile : Profile {
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public ViewProfile() {
            HelpMap<vSave010CertificationFile, vDocumentRole03ChangeCertificationFileView>();
            HelpMap<vSave010RequestGroup, vDocumentRole03ChangeRequestGroupView>();


            //HelpMap<vCheckingItemAddModel, vCheckingItem, vCheckingItemView>();
            HelpMap<vCheckingSimilarRequestItemSubTypeReport, vCheckingSimilarRequestItemSubTypeReportView>();
            HelpMap<vCheckingSimilarResultDuplicate_SaveAddModel, vCheckingSimilarResultDuplicate_Save, vCheckingSimilarResultDuplicate_SaveView>();
            HelpMap<vCheckingSimilarResultDuplicate_WordAddModel, vCheckingSimilarResultDuplicate_Word, vCheckingSimilarResultDuplicate_WordView>();
            HelpMap<vCheckingSimilarSave010, vCheckingSimilarSave010ListView>();
            HelpMap<vCheckingSimilarSave010List, vCheckingSimilarSave010ListView>();
            HelpMap<vDashboardConsidering, vDashboardConsideringView>();
            HelpMap<vDashboardDocumentRole01, vDashboardDocumentRole01View>();
            HelpMap<vDocumentItemModel, vDocumentItem, vDocumentItemView>();
            HelpMap<vDocumentProcessChangeAddModel, vDocumentProcessChange, vDocumentProcessChangeView>();
            HelpMap<vDocumentProcessCollectAddModel, vDocumentProcessCollect, vDocumentProcessCollectView>();
            HelpMap<vDocumentRole02AddModel, vDocumentRole02, vDocumentRole02View>();
            HelpMap<vDocumentRole02GroupAddModel, vDocumentRole02Group, vDocumentRole02GroupView>();
            HelpMap<vDocumentRole03ItemAddModel, vDocumentRole03Item, vDocumentRole03ItemView>();
            HelpMap<vDocumentRole03ItemGroupAddModel, vDocumentRole03ItemGroup, vDocumentRole03ItemGroupView>();
            HelpMap<vDocumentRole03ChangeCertificationFileAddModel, vDocumentRole03ChangeCertificationFile, vDocumentRole03ChangeCertificationFileView>();
            HelpMap<vDocumentRole03ChangeRequestGroupAddModel, vDocumentRole03ChangeRequestGroup, vDocumentRole03ChangeRequestGroupView>();
            HelpMap<vDocumentRole04AddModel, vDocumentRole04, vDocumentRole04View>();
            HelpMap<vDocumentRole04CheckAddModel, vDocumentRole04Check, vDocumentRole04CheckView>();
            HelpMap<vDocumentRole04CreateAddModel, vDocumentRole04Create, vDocumentRole04CreateView>();
            HelpMap<vDocumentRole04CreateFileAddModel, vDocumentRole04CreateFile, vDocumentRole04CreateFileView>();
            HelpMap<vDocumentRole04ReleaseAddModel, vDocumentRole04Release, vDocumentRole04ReleaseView>();
            HelpMap<vDocumentRole04Release20AddModel, vDocumentRole04Release20, vDocumentRole04Release20View>();
            HelpMap<vDocumentRole04ReleaseRequestAddModel, vDocumentRole04ReleaseRequest, vDocumentRole04ReleaseRequestView>();
            HelpMap<vDocumentRole04ReleaseRequestGroupAddModel, vDocumentRole04ReleaseRequestGroup, vDocumentRole04ReleaseRequestGroupView>();
            HelpMap<vEForm_eForm_Save010AddModel, vEForm_eForm_Save010, vEForm_eForm_Save010View>();
            HelpMap<vEForm_eForm_Save010_ProductAddModel, vEForm_eForm_Save010_Product, vEForm_eForm_Save010_ProductView>();
            HelpMap<vEForm_eForm_SaveOtherAddModel, vEForm_eForm_SaveOther, vEForm_eForm_SaveOtherView>();
            //HelpMap<vEForm_Request01ItemAddModel, vEForm_Request01Item, vEForm_Request01ItemView>();
            HelpMap<vFloor3DocumentAddModel, vFloor3Document, vFloor3DocumentView>();
            HelpMap<vLocation, vLocationView>();
            HelpMap<vOnlinePublicRound, vOnlinePublicRoundView>();
            HelpMap<vOnlinePublicRoundItem, vOnlinePublicRoundItemView>();
            HelpMap<vOnlinePublicRoundMonth, vOnlinePublicRoundMonthView>();
            HelpMap<vOnlinePublicRoundProduct, vOnlinePublicRoundProductView>();
            HelpMap<vPostRoundAddModel, vPostRound, vPostRoundView>();
            HelpMap<vPostRoundInstructionRuleFileAddModel, vPostRoundInstructionRuleFile, vPostRoundInstructionRuleFileView>();
            HelpMap<vPublicItemAddModel, vPublicItem, vPublicItemView>();
            HelpMap<vPublicRegisterAddModel, vPublicRegister, vPublicRegisterView>();
            HelpMap<vPublicRequestItemAddModel, vPublicRequestItem, vPublicRequestItemView>();
            HelpMap<vPublicRole01Doing, vPublicRole01DoingView>();
            HelpMap<vPublicRole01Prepare, vPublicRole01PrepareView>();
            HelpMap<vPublicRoundCase41, vPublicRoundCase41View>();
            HelpMap<vPublicConsideringOtherAddModel, vPublicConsideringOther, vPublicConsideringOtherView>();
            HelpMap<vPublicOtherAddModel, vPublicOther, vPublicOtherView>();
            HelpMap<vReceiptItem, vReceiptItemView>();
            HelpMap<vRecordRegistrationNumberAddModel, vRecordRegistrationNumber, vRecordRegistrationNumberView>();
            HelpMap<vRecordRegistrationNumberAllowAddModel, vRecordRegistrationNumberAllow, vRecordRegistrationNumberAllowView>();
            HelpMap<vRequest01Item, vRequest01ItemView>();
            HelpMap<vRequest01ReceiptChange, vRequest01ReceiptChangeView>();
            HelpMap<vRequestAgency, vRequestAgencyView>();
            HelpMap<vRequestDocumentCollectAddModel, vRequestDocumentCollect, vRequestDocumentCollectView>();
            HelpMap<vRequestOtherItem, vRequestOtherItemView>();
            //HelpMap<vRequestWaitSendAddModel, vRequestWaitSend, vRequestWaitSendView>();
            HelpMap<vSave010, vSave010View>();
            HelpMap<vSave010CertificationFile, vSave010CertificationFileView>();
            HelpMap<vSave010CheckingInstructionDocumentAddModel, vSave010CheckingInstructionDocument, vSave010CheckingInstructionDocumentView>();
            HelpMap<vSave010CheckingSaveDocumentAddModel, vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>();
            HelpMap<vSave010CheckingTagSimilarAddModel, vSave010CheckingTagSimilar, vSave010CheckingTagSimilarView>();
            HelpMap<vSave010DocumentClassificationSearchSimilarAddModel, vSave010DocumentClassificationSearchSimilar, vSave010DocumentClassificationSearchSimilarView>();
            HelpMap<vSave010History, vSave010HistoryView>();
            HelpMap<vSave010InstructionRule, vSave010InstructionRuleView>();
            HelpMap<vSave010PublicPaymentAddModel, vSave010PublicPayment, vSave010PublicPaymentView>();
            HelpMap<vSave010RequestGroup, vSave010RequestGroupView>();
            HelpMap<vSaveAddressAll, vSaveAddressAllView>();
            HelpMap<vSaveAddressRepresentative, vSaveAddressRepresentativeView>();
            HelpMap<vSaveSend, vSaveSendView>();
            HelpMap<vUM_UserAddModel, vUM_User, vUM_UserView>();
            HelpMap<vUserSession, vUserSessionView>();
        }
    }
}

