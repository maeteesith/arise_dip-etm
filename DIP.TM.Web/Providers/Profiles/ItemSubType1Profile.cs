﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;

namespace DIP.TM.Web.Profiles {
    public class ItemSubType1Profile : Profile {
        public void HelpMap<A, B>() {
            CreateMap<A, B>();
            CreateMap<B, A>();
        }
        public void HelpMap<A, B, C>() {
            CreateMap<A, B>();
            CreateMap<A, C>();
            CreateMap<B, C>();
            CreateMap<B, A>();
            CreateMap<C, A>();
            CreateMap<C, B>();
        }

        public ItemSubType1Profile() {
            HelpMap<Save010Product, ItemSubType1SuggestionView>();
        }
    }
}

