﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;

namespace DIP.TM.Web.Providers.Profiles
{
    public class RequestChangeProfile : Profile
    {
        public RequestChangeProfile()
        {
            // Map preview exist request01 change
            CreateMap<vRequest01ReceiptChange, Request01ReceiptChangeView>();

            // Map from edit payload to change table
            CreateMap<Request01ProcessAddModel, Request01ProcessChange>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.source_id, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.Request01ItemChange, opt => opt.MapFrom(src => src.item_list))
                ;
            CreateMap<Request01ItemAddModel, Request01ItemChange>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.source_id, opt => opt.MapFrom(src => src.id))
                .ForMember(dest => dest.Request01ItemSubChange, opt => opt.MapFrom(src => src.item_sub_list))
                ;
            CreateMap<Request01ItemSubAddModel, Request01ItemSubChange>()
                .ForMember(dest => dest.id, opt => opt.Ignore())
                .ForMember(dest => dest.source_id, opt => opt.MapFrom(src => src.id))
                ;

            // Map preview new request01 change
            CreateMap<Request01ReceiptChange, Request01ReceiptChangeView>();
            // Map preview request01 source
            CreateMap<Request01Process, Request01ProcessView>()
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.Request01Item))
                ;
            CreateMap<Request01Item, Request01ItemView>()
                .ForMember(dest => dest.item_sub_list, opt => opt.MapFrom(src => src.Request01ItemSub))
                ;
            CreateMap<Request01ItemSub, Request01ItemSubView>();

            // Map preview request01 change
            CreateMap<Request01ProcessChange, Request01ProcessView>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.source_id))
                .ForMember(dest => dest.item_list, opt => opt.MapFrom(src => src.Request01ItemChange))
                ;
            CreateMap<Request01ItemChange, Request01ItemView>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.source_id))
                .ForMember(dest => dest.item_sub_list, opt => opt.MapFrom(src => src.Request01ItemSubChange))
                ;
            CreateMap<Request01ItemSubChange, Request01ItemSubView>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.source_id))
                ;

            // Map Update Change after receipt approved
            CreateMap<Request01ProcessChange, Request01Process>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.source_id))
                .ForMember(dest => dest.Request01Item, opt => opt.MapFrom(src => src.Request01ItemChange))
                ;

            CreateMap<Request01ItemChange, Request01Item>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.source_id))
                .ForMember(dest => dest.Request01ItemSub, opt => opt.MapFrom(src => src.Request01ItemSubChange))
                ;

            CreateMap<Request01ItemSubChange, Request01ItemSub>()
                .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.source_id))
                ;
        }
    }
}
