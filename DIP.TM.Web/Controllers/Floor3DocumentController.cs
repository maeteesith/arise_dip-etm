﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.RequestChecks;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DIP.TM.Web.Extensions;
using System.Collections.Generic;
using DIP.TM.Services.ReferenceMasters;
using System;
using DIP.TM.Models;
using DIP.TM.Services.Checking;
using DIP.TM.Services.Auths;
using DIP.TM.Models.Pagers;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Web.Controllers {

    public class Floor3DocumentController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public Floor3DocumentController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("Floor3DocumentRequestNumberAdd")]
        public IActionResult Floor3DocumentRequestNumberAdd([FromBody]vSave010Model payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
            var view = service.List(payload);
            return Json(new BaseResponseView<vSave010View>() { data = view.FirstOrDefault() });
        }
    }
}
