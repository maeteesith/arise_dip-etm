﻿using AutoMapper;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Requests;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;
using DIP.TM.Web.Extensions;
using DIP.TM.Datas;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using DIP.TM.Models;
using DIP.TM.Uows.DataAccess.Paging;
using System.IO;
using Newtonsoft.Json;
using DIP.TM.Models.Pagers;
using DIP.TM.Services.Saves;
using DIP.TM.Utils;

namespace DIP.TM.Web.Controllers {


    public class RequestProcessController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestProcessController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        // POST api/<module>/<controller>
        [ProducesResponseType(typeof(BaseResponseView<Request01ProcessView>), 200)]
        [HttpPost]
        [Route("Request01Add")]
        public IActionResult Request01Add([FromBody]Request01ProcessAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<Request01ProcessView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.Request01Add(payload);
            return Json(new BaseResponseView<Request01ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<Request01ProcessView>), 200)]
        [HttpPost]
        [Route("Request01Split")]
        public IActionResult Request01Split([FromBody]Request01ProcessAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<Request01ProcessView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.Request01Split(payload);
            return Json(new BaseResponseView<Request01ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<Request01ProcessView>), 200)]
        [HttpPost]
        [Route("Request01Save")]
        public IActionResult Request01Save([FromBody]Request01ProcessAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<Request01ProcessView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.Request01Save(payload);
            return Json(new BaseResponseView<Request01ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<Request01ProcessView>), 200)]
        [HttpPost]
        [Route("Request01Load/{id}")]
        public IActionResult Request01Load(int id) {
            var currentUser = this.CurrentUser();
            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.Request01Load(id);

            result.item_list = result.item_list.OrderBy(r => r.request_number).ToList();

            return Json(new BaseResponseView<Request01ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<Request01ItemView>), 200)]
        [HttpPost]
        [Route("Request01ItemDelete")]
        public IActionResult Request01ItemDelete([FromBody]Request01ItemAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<Request01ProcessView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.Request01ItemDelete(payload);
            return Json(new BaseResponseView<Request01ItemView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vRequest01ItemView>>), 200)]
        [HttpPost]
        [Route("Request01ItemList")]
        public IActionResult Request01ItemList([FromBody]PageRequest payload) {
            var service = new BaseService<vRequest01Item, vRequest01ItemView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);

            using (var uow = _uowProvider.CreateUnitOfWork()) {
                foreach (var view in result.data.list) {
                    var product_repo = uow.GetRepository<Save010Product>();
                    var product = product_repo.Query(r => r.save_id == view.save_id);
                    view.product_list = _mapper.Map<List<SaveProductView>>(product);
                }
            }

            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<ItemCalculatorView>), 200)]
        [HttpGet]
        [Route("Request01GetItemCalculator")]
        public IActionResult Request01GetItemCalculator(int qty) {
            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.CalculateItemprice(qty);
            return Json(new BaseResponseView<ItemCalculatorView>() { data = result });
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponseView<List<PriceMasterView>>), 200)]
        [HttpGet]
        [Route("Request01PriceMaster")]
        public IActionResult Request01PriceMaster() {
            var service = new RequestMasterService(_configuration, _uowProvider, _mapper);
            var result = service.GetPriceRequestMaster();
            return Json(new BaseResponseView<List<PriceMasterView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<RequestOtherProcessView>), 200)]
        [HttpPost]
        [Route("RequestOtherAdd")]
        public IActionResult RequestOtherAdd([FromBody]RequestOtherProcessAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<RequestOtherProcessView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RequestOtherAdd(payload);
            return Json(new BaseResponseView<RequestOtherProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<RequestOtherProcessView>), 200)]
        [HttpPost]
        [Route("RequestOtherSave")]
        public IActionResult RequestOtherSave([FromBody]RequestOtherProcessAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<RequestOtherProcessView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RequestOtherSave(payload);
            return Json(new BaseResponseView<RequestOtherProcessView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<RequestOtherItemSubView>), 200)]
        [HttpPost]
        [Route("RequestOtherItemSubDelete")]
        public IActionResult RequestOtherItemSubDelete([FromBody]RequestOtherItemSubAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<RequestOtherItemView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RequestOtherItemSubDelete(payload);
            return Json(new BaseResponseView<RequestOtherItemSubView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<RequestOtherProcessView>), 200)]
        [HttpPost]
        [Route("RequestOtherLoad/{id}")]
        public IActionResult RequestOtherLoad(int id) {
            var currentUser = this.CurrentUser();
            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RequestOtherLoad(id);
            return Json(new BaseResponseView<RequestOtherProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<RequestOtherProcessView>), 200)]
        [HttpPost]
        [Route("RequestOtherPrint/{id}")]
        public IActionResult RequestOtherPrint(long id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<RequestOtherProcessView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RequestOtherPrint(id);
            return Json(new BaseResponseView<RequestOtherProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RequestOtherItemSubList")]
        public IActionResult RequestOtherItemSubList([FromBody]PageRequest payload) {
            var service = new BaseService<vRequestOtherItem, vRequestOtherItemView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);

            //var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            //var result = service.RequestOtherItemSubList(payload);
            //return Json(new BaseResponseView<List<vRequestOtherItemView>>() { data = result });
        }



        [ProducesResponseType(typeof(BaseResponsePageView<List<vRequest01ItemView>>), 200)]
        [HttpPost]
        [Route("RequestTypeCodeList")]
        public IActionResult RequestTypeCodeList([FromBody]PageRequest payload) {
            var service = new BaseService<RM_RequestType, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel() {
                key = "is_showed",
                value = "True",
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RequestItemTypeCodeList")]
        public IActionResult RequestItemTypeCodeList([FromBody]PageRequest payload) {
            var service = new BaseService<RM_RequestItemType, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        //[ProducesResponseType(typeof(BaseResponseView<List<Request01ItemView>>), 200)]
        //[HttpPost]
        //[Route("Request01ItemListTest")]
        //public IActionResult Request01ItemListTest([FromBody]PageRequest payload, [FromServices] IDataPager<Request01Item> pager)
        //{
        //    var service = new RequestProcessService(_configuration, _uowProvider, _mapper, pager);
        //    var result = service.Request01List(ref payload);
        //    return Json(new BaseResponsePageView<List<Request01ItemView>>() { data = result, paging= payload });
        //}


        [ProducesResponseType(typeof(BaseResponseView<bool>), 200)]
        [HttpPost]
        [Route("RequestEformSend")]
        public IActionResult RequestEformSend([FromBody]Request01ProcessAddModel payload) {
            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RequestEformSend(payload);
            return Json(new BaseResponseView<Request01ProcessView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<bool>), 200)]
        [HttpPost]
        [Route("RequestOtherEformSend")]
        public IActionResult RequestOtherEformSend([FromBody]RequestOtherProcessAddModel payload) {
            var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RequestOtherEformSend(payload);
            return Json(new BaseResponseView<RequestOtherProcessView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("List/{view_name}")]
        public IActionResult List(string view_name, [FromBody]PageRequest payload) {
            var service = BaseController.GetService(view_name, _configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }
    }
}
