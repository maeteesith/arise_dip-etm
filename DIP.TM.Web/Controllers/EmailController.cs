﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using DIP.TM.Models.Email;
using DIP.TM.Models.Views;
using DIP.TM.Services.Email;
using DIP.TM.Services.Interfaces;
using FluentEmail.Core;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DIP.TM.Web.Controllers
{
    
    public class EmailController : BaseApiController
    {
        private readonly IBaseEmailService emailService;
        public EmailController(IBaseEmailService emailService)
        {
            this.emailService = emailService;
        }


        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponseView<bool>), 200)]
        [HttpPost]
        [Route("Send")]
        public IActionResult Send([FromBody] EmailModel model)
        {
            var result = this.emailService.Send(model);
            return Json(new BaseResponseView<bool>() { data = result });
        }

        /*[AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponseView<bool>), 200)]
        [HttpGet]
        [Route("SendTest")]
        public IActionResult SendTest()
        {
            using (MailMessage mm = new MailMessage("dimtmnoreply@gmail.com", "dotnetnat@gmail.com"))
            {
                mm.Subject = "Hello title";
                mm.Body = "Hello body";

                mm.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential("dimtmnoreply@gmail.com", "DIP123###");
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                smtp.Send(mm);
            }

            return Json(new BaseResponseView<bool>() { data = true });
        }*/
    }
}