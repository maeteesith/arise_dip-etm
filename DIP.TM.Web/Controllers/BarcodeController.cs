﻿using DIP.TM.Models.Views;
using DIP.TM.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DIP.TM.Web.Controllers
{
    public class BarcodeController : BaseApiController
    {
        private readonly IConfiguration configuration;
        public BarcodeController(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        [ProducesResponseType(typeof(BaseResponseView<string>), 200)]
        [HttpGet]
        [Route("Gererate")]
        public IActionResult Gererate(string barcodeValue)
        {
            var result = BarcodeHelper.Generate(barcodeValue);
            return Json(new BaseResponseView<string>() { data = result });
        }
    }
}