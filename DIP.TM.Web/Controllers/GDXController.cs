﻿using AutoMapper;
using System;
using DIP.TM.Models.Views;
using DIP.TM.Services.GDX;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Models.Payloads;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace DIP.TM.Web.Controllers {
    
    public class GDXController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public GDXController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        [ProducesResponseType(typeof(BaseResponseView<GDXView>), 200)]
        [HttpPost]
        [Route("Load")]
        public IActionResult Load([FromBody] RegistrationRequestGDXModel model) {
           
            var service = new GDXService(_configuration, _uowProvider, _mapper);
            var result = service.Load(model);
            return Json(new BaseResponseView<GDXView>() { data = result });
          //return Ok();
        }
    }
}