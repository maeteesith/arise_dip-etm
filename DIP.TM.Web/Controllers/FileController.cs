﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Configs;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Medias;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace DIP.TM.Web.Controllers
{
    public class FileController : BaseController
    {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        private AppConfigModel _config;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public FileController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _config = configuration.ToConfig();
        }


        [Route("File/Content/{fileId}")]
        //public ActionResult Content(int fileName)
        public ActionResult Content(int fileId)
        {

            var service = new FileService(_configuration, _uowProvider, _mapper);
            var result = service.Get(fileId);

            // TODO Remove for mock test
            //result = new Models.Views.FileView() { physical_path = "Contents/Users/Media/ed262884-6.jpg", file_name = "ed262884-6.jpg" };

            if (ReferenceEquals(result, null))
                return new EmptyResult();

            string contentType;

            string FileExtension = "." + result.file_name.Split(".").Last().ToLower();
            contentType = ContentType(FileExtension);
            string fullPath = string.Empty;
            if (_config.ContentPath.BasePath == string.Empty)
            {
                var baseFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                fullPath = Path.Combine(baseFolder, result.physical_path);
            }
            else
            {
                var baseFolder = _config.ContentPath.BasePath;
                fullPath = Path.Combine(baseFolder, result.physical_path);
            }
            var stream = GetStream(_configuration, fullPath);
            if (ReferenceEquals(stream, null))
            {
                return new EmptyResult();
            }
            else
            {
                return new FileStreamResult(stream, contentType);
            }
        }

        /*[Route("File/Download/{fileName}")]
        public ActionResult Download(string fileName)
        {

            string contentType;

            string FileExtension = "." + fileName.Split(".").Last().ToLower();
            contentType = ContentType(FileExtension);

            var stream = GetStream(_configuration, _config.BaseUrl + "Contents/Media/" + fileName);
            if (ReferenceEquals(stream, null))
            {
                return new EmptyResult();
            }
            else
            {
                var mBytes = ReadToEnd(stream);
                Response.ContentType = contentType;
                Response.Headers.Add("Content-Disposition", "attachment; filename=" + fileName);
                return new FileStreamResult(new MemoryStream(mBytes, false), contentType)
                {
                    FileDownloadName = fileName
                };
            }
        }*/


        [Route("File/RequestDocumentCollect/{type}/{id}")]
        public ActionResult RequestDocumentCollect(string type, string id)
        {
            using (var uow = _uowProvider.CreateUnitOfWork())
            {
                var repo = uow.GetRepository<vRequestDocumentCollect>();
                var physical_path = "";

                if (type == "010" || type == "SAVE01")
                {
                    var entity = repo.Query(r => r.save_id == Convert.ToInt64(id) && r.request_document_collect_type_code == "010").FirstOrDefault();
                    if (entity != null) physical_path = entity.physical_path;
                }
                else
                {
                    var receipt_item_repo = uow.GetRepository<ReceiptItem>();
                    var ids = id.Split(new string[] { "_" }, StringSplitOptions.None);
                    if (ids.Length == 1)
                    {
                        var entity = repo.Query(r => r.file_id == Convert.ToInt64(ids[0])).FirstOrDefault();
                        if (entity != null) physical_path = entity.physical_path;
                    }
                    else if (ids.Length == 2)
                    {
                        var request_number = ids[0];
                        var request_id = ids[1];

                        var receipt_item_entity = receipt_item_repo.Query(r => r.request_number == request_number &&
                        r.request_id == Convert.ToInt64(request_id)).FirstOrDefault();
                        if (receipt_item_entity != null)
                        {
                            var entity = repo.Query(r => r.id == receipt_item_entity.id).FirstOrDefault();
                            if (entity != null) physical_path = entity.physical_path;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(physical_path))
                {
                    string contentType;

                    string FileExtension = "." + physical_path.Split(".").Last().ToLower();
                    contentType = ContentType(FileExtension);

                    string fullPath = string.Empty;
                    if (_config.ContentPath.BasePath == string.Empty)
                    {
                        var baseFolder = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot");
                        fullPath = Path.Combine(baseFolder, physical_path);
                    }
                    else
                    {
                        var baseFolder = _config.ContentPath.BasePath;
                        fullPath = Path.Combine(baseFolder, physical_path);
                    }
                    var stream = GetStream(_configuration, fullPath);
                    if (ReferenceEquals(stream, null))
                    {
                        return new EmptyResult();
                    }
                    else
                    {
                        return new FileStreamResult(stream, contentType);
                    }


                    //string FileExtension = "." + physical_path.Split(".").Last().ToLower();
                    //string contentType = ContentType(FileExtension);

                    //var stream = GetStream(_configuration["BaseUrl"] + physical_path);
                    //if (ReferenceEquals(stream, null)) {
                    //    return new EmptyResult();
                    //} else {
                    //    var mBytes = ReadToEnd(stream);
                    //    Response.ContentType = contentType;
                    //    Response.Headers.Add("Content-Disposition", "attachment; filename=" + entity.request_number + "_" + entity.request_document_collect_type_code + "." + FileExtension);
                    //    return new FileStreamResult(new MemoryStream(mBytes, false), contentType) {
                    //        FileDownloadName = entity.request_number + "_" + entity.request_document_collect_type_code + "." + FileExtension
                    //    };
                    //}
                }
                else
                {
                    return new EmptyResult();
                }
            }
        }
    }
}
