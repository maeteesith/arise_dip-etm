﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DIP.TM.Web.Extensions;
using System.Collections.Generic;
using DIP.TM.Services.ReferenceMasters;
using System;
using DIP.TM.Models;
using System.Threading.Tasks;
using DIP.TM.Models.Pagers;
using Newtonsoft.Json;

namespace DIP.TM.Web.Controllers {

    public class SaveProcessController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        protected Dictionary<string, dynamic> save_service_list;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public SaveProcessController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;

            save_service_list = new Dictionary<string, dynamic>();
            save_service_list.Add("01", new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("020", new SaveProcessService<Save020AddModel, Save020, Save020ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("021", new SaveProcessService<Save021AddModel, Save021, Save021ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("03", new SaveProcessService<Save030AddModel, Save030, Save030ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("04", new SaveProcessService<Save040AddModel, Save040, Save040ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("05", new SaveProcessService<Save050AddModel, Save050, Save050ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("06", new SaveProcessService<Save060AddModel, Save060, Save060ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("07", new SaveProcessService<Save070AddModel, Save070, Save070ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("08", new SaveProcessService<Save080AddModel, Save080, Save080ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("14", new SaveProcessService<Save140AddModel, Save140, Save140ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("Other", new SaveProcessService<SaveOtherAddModel, SaveOther, SaveOtherProcessView>(_configuration, _uowProvider, _mapper));
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("Save010FullLoad/{id}")]
        public IActionResult Save010FullLoad(string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessService<Save010AddModel, Save010, vSave010View>(_configuration, _uowProvider, _mapper);
            var result = service.Save010Load(Convert.ToInt64(id), true);
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        [HttpPost]
        [Route("Save{id1}Load/{id2}")]
        public IActionResult Load(string id1, string id2) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = save_service_list[id1];
            var result = service.Get(Convert.ToInt64(id2));
            return Json(service.GetResponseView(result));
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        [HttpPost]
        [Route("Save{id}List")]
        public IActionResult List([FromBody]dynamic payload, string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<Request01ProcessAddModel>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = save_service_list[id];
            var result = service.List(payload);
            return Json(service.GetResponseListView(result));
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseSaveView>>), 200)]
        [HttpPost]
        [Route("Save{id}ListPage")]
        public IActionResult ListPage([FromBody]PageRequest payload, string id) {
            var service = save_service_list[id];
            var result = service.ListPage(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        [HttpPost]
        [Route("Save{id}ListNotSent")]
        public IActionResult ListNotSent([FromBody]BasePageModelPayload<SaveListDraftModel> payload, string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = save_service_list[id];
            var result = service.ListNotSent(payload);
            return Json(service.GetResponseListView(result));
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        [HttpPost]
        [Route("Save{id}Save")]
        public IActionResult Save([FromBody]dynamic payload, string id) {
            var service = save_service_list[id];
            if (id == "01") {
                var model = (Save010AddModel)JsonConvert.DeserializeObject<Save010AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Save(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save010Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "04") {
                var model = (Save040AddModel)JsonConvert.DeserializeObject<Save040AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Save(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save040Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "05") {
                var model = (Save050AddModel)JsonConvert.DeserializeObject<Save050AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Save(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save050Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "06") {
                var model = (Save060AddModel)JsonConvert.DeserializeObject<Save060AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Save(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save060Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "07") {
                var model = (Save070AddModel)JsonConvert.DeserializeObject<Save070AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Save(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save070Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else {
                var result = service.Save(payload);
                return Json(service.GetResponseView(result));
            }
        }


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("SaveOtherSend")]
        public IActionResult SaveOtherSend([FromBody]List<vSaveSendModel> payload) {
            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = save_service_list["Other"].Send(payload);
            return Json(new BaseResponseView<List<SaveOtherProcessView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        [HttpPost]
        [Route("Save{id}Send")]
        public IActionResult Send([FromBody]dynamic payload, string id) {
            var service = save_service_list[id];
            if (id == "01") {
                var model = (Save010AddModel)JsonConvert.DeserializeObject<Save010AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Send(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save010Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "04") {
                var model = (Save040AddModel)JsonConvert.DeserializeObject<Save040AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Send(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save040Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "05") {
                var model = (Save050AddModel)JsonConvert.DeserializeObject<Save050AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Send(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save050Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "06") {
                var model = (Save060AddModel)JsonConvert.DeserializeObject<Save060AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Send(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save060Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "07") {
                var model = (Save070AddModel)JsonConvert.DeserializeObject<Save070AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.Send(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save070Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else {
                var result = service.Send(payload);
                return Json(service.GetResponseView(result));
            }
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Save{id}RequestBack")]
        public IActionResult RequestBack([FromBody]dynamic payload, string id) {
            var service = save_service_list[id];
            if (id == "01") {
                var model = (Save010AddModel)JsonConvert.DeserializeObject<Save010AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.RequestBack(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save010Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "04") {
                var model = (Save040AddModel)JsonConvert.DeserializeObject<Save040AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.RequestBack(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save040Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "05") {
                var model = (Save050AddModel)JsonConvert.DeserializeObject<Save050AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.RequestBack(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save050Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "06") {
                var model = (Save060AddModel)JsonConvert.DeserializeObject<Save060AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.RequestBack(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save060Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else if (id == "07") {
                var model = (Save070AddModel)JsonConvert.DeserializeObject<Save070AddModel>(Convert.ToString(payload));
                var product_list = model.product_list;
                model.product_list = null;
                var result = service.RequestBack(model);

                var product_service = new SaveProductService<SaveProductAddModel, Save070Product, SaveProductView>(_configuration, _uowProvider, _mapper);
                result.product_list = product_service.Save(product_list, result.id);

                return Json(service.GetResponseView(result));
            } else {
                var result = service.RequestBack(payload);
                return Json(service.GetResponseView(result));
            }
        }

        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("Save{id}Delete")]
        public IActionResult Delete([FromBody]dynamic payload, string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = save_service_list[id];
            var result = service.Delete(payload);
            return Json(new BaseResponseView<BaseSaveView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<ReferenceMasterView>), 200)]
        [HttpPost]
        [Route("Save{id}ListCommercialAffairsProvince")]
        public async Task<IActionResult> ListCommercialAffairsProvince([FromBody]BasePageModelPayload<ReferenceMasterModel> payload, string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new ReferenceMasterService(_configuration, _uowProvider, _mapper);
            var result = await service.List(new BasePageModelPayload<ReferenceMasterModel>() {
                filter = new ReferenceMasterModel() {
                    topic = "COMMERCIAL_AFFAIRS_PROVINCE_CODE",
                    name = payload.filter.name,
                },
                paging = new PagingModel() {
                    item_per_page = payload.paging != null ? payload.paging.item_per_page : 5
                }
            });
            return Json(new BaseResponseView<List<ReferenceMasterView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vLocationView>), 200)]
        [HttpPost]
        [Route("Save{id}ListLocation")]
        [Route("Save{id}ListModalLocation")]
        public IActionResult ListModalLocation([FromBody]BasePageModelPayload<RS_AddressAddModel> payload, string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new LocationService(_configuration, _uowProvider, _mapper);
            var result = service.List(new BasePageModelPayload<RS_AddressAddModel>() {
                filter = new RS_AddressAddModel() { name = payload.filter.name },
                paging = new PagingModel() {
                    item_per_page = payload.paging != null ? payload.paging.item_per_page : 5
                }
            });
            return Json(new BaseResponseView<List<vLocationView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<ReferenceMasterView>), 200)]
        [HttpPost]
        [Route("ListItemSubType{id}Code")]
        public async Task<IActionResult> ListItemSubType([FromBody]BasePageModelPayload<ReferenceMasterModel> payload, string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new ReferenceMasterService(_configuration, _uowProvider, _mapper);
            var result = await service.List(new BasePageModelPayload<ReferenceMasterModel>() {
                filter = new ReferenceMasterModel() {
                    topic = "REQUEST_ITEM_SUB_TYPE_" + id,
                    name = payload.filter.name,
                },
                paging = new PagingModel() {
                    item_per_page = payload.paging != null ? payload.paging.item_per_page : 5
                }
            }); ;
            return Json(new BaseResponseView<List<ReferenceMasterView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("SaveSendList")]
        public IActionResult List([FromBody]PageRequest payload) {
            var service = new BaseService<vSaveSend, vSaveSendView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

    }
}
