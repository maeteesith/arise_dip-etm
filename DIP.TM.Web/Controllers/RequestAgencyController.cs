﻿using AutoMapper;
using DIP.TM.Models.Views;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DIP.TM.Web.Extensions;
using System.Collections.Generic;
using DIP.TM.Services.RequestAgencys;
using DIP.TM.Models.Payloads;

namespace DIP.TM.Web.Controllers {
    
    public class RequestAgencyController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestAgencyController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        [ProducesResponseType(typeof(BaseResponseView<List<vRequestAgencyView>>), 200)]
        [HttpPost]
        [Route("ListView")]
        public IActionResult ListView([FromBody]RequestAgencyModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<Request01ProcessAddModel>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new RequestAgencyService(_configuration, _uowProvider, _mapper);
            var result = service.ListView(payload);
            return Json(new BaseResponseView<List<vRequestAgencyView>>() { data = result });
        }
    }
}
