﻿using DIP.TM.Models;
using DIP.TM.Models.Configs;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Interfaces;
using DIP.TM.Utils.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DIP.TM.Utils.Extensions;

namespace DIP.TM.Web.Controllers
{


    public class ReferenceMasterController : BaseApiController
    {
        private readonly IReferenceMasterService referenceService;
        private readonly AppConfigModel configModel;
        private readonly IConfiguration configuration;
        private readonly IMemoryCache cache;
        private MemoryCacheEntryOptions cacheOptions;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="referenceService"></param>
        /// <param name="cache"></param>
        public ReferenceMasterController(IConfiguration configuration, IReferenceMasterService referenceService, IMemoryCache cache)
        {
            this.referenceService = referenceService;
            this.cache = cache;
            this.configuration = configuration;
            this.configModel = configuration.ToConfig();
            cacheOptions = new MemoryCacheEntryOptions
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(this.configModel.MasterCacheDuration),
                SlidingExpiration = TimeSpan.FromMinutes(this.configModel.MasterCacheDuration - 2)
            };
        }
        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponseView<List<ReferenceMasterView>>), 200)]
        [HttpPost]
        [Route("List")]
        public async Task<IActionResult> List([FromBody] BasePageModelPayload<ReferenceMasterModel> payload)
        {
            if (payload.paging == null || payload.paging.item_per_page == 0 || payload.paging.item_per_page == 50) payload.paging = new PagingModel() { item_per_page = 5 };

            var result = new List<ReferenceMasterView>();
            if (this.configModel.EnableMasterCache)
            {
                var key = JsonConvert.SerializeObject(payload).ToBase64String();
                result = LookupCache(key);
                if (ReferenceEquals(result, null))
                {
                    result = await this.referenceService.List(payload);
                    cache.Set(key, result, cacheOptions);
                }
            }
            else
            {
                result = await this.referenceService.List(payload);
            }

            if (payload.filter != null && !string.IsNullOrEmpty(payload.filter.name))
            {
                result = result.Where(r => r.name.IndexOf(payload.filter.name.Trim()) >= 0 || r.code.IndexOf(payload.filter.name.Trim()) >= 0).ToList();
            }
            return Json(new BaseResponseView<List<ReferenceMasterView>>() { data = result });
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private List<ReferenceMasterView> LookupCache(string key)
        {
            if (!cache.TryGetValue(key, out List<ReferenceMasterView> results))
                return null;
            else
                return results;
        }
    }
}
