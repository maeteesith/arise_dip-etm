﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Views;
using DIP.TM.Services.Requests;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;

namespace DIP.TM.Web.Controllers
{
    public class Request01ReceiptChangeController : BaseApiController
    {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public Request01ReceiptChangeController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Add")]
        public IActionResult Request01ReceiptChangeAdd([FromBody]Request01ProcessAddModel payload)
        {
            var service = new RequestReceiptChangeService(_configuration, _uowProvider, _mapper);
            var result = service.Request01ReceiptChangeAdd(payload);
            if (result == null)
                return Json(new BaseResponseView<BaseView>() { data = null, is_error = true, error_message = "Not found Request01 ID: [" + payload.id + "]" });

            if (result != null && (result.request01_receipt_change_code == Request01ReceiptChangeCodeType.RECEIPT_APPROVE_WAIT.ToString() || result.request01_receipt_change_code == Request01ReceiptChangeCodeType.REQUEST_APPROVE_WAIT.ToString()))
                return Json(new BaseResponseView<BaseView>() { data = null, is_error = true, error_message = $"ใบคำขอร้องนี้อยู่ในกระบวนการ ({result.request01_receipt_change_name}) กรุณาอนุมัติ หรือยกเลิกใบคำร้องนี้ที่หน้า \"รายการคำขอแก้ไขใบคำร้อง\" ก่อนดำเนินการต่อไป" });

            return Json(new BaseResponseView<Request01ReceiptChangeView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("List")]
        public IActionResult Request01ReceiptChangeList([FromBody]PageRequest payload)
        {
            var service = new BaseService<vRequest01ReceiptChange, vRequest01ReceiptChangeView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Load/{id}")]
        public IActionResult Request01ReceiptChangeLoad(int id)
        {
            var currentUser = this.CurrentUser();
            var service = new RequestReceiptChangeService(_configuration, _uowProvider, _mapper);
            var result = service.Request01ReceiptChangeLoad(id);
            return Json(new BaseResponseView<Request01ReceiptChangeView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("RequestApprove")]
        public IActionResult Request01ReceiptChangeRequestApprove(string id)
        {
            var service = new RequestReceiptChangeService(_configuration, _uowProvider, _mapper);
            var result = service.Request01ReceiptChangeRequestApprove(int.Parse(id));
            return Json(new BaseResponseView<Request01ReceiptChangeView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("RequestReject")]
        public IActionResult Request01ReceiptChangeRequestReject(string id)
        {
            var service = new RequestReceiptChangeService(_configuration, _uowProvider, _mapper);
            var result = service.Request01ReceiptChangeRequestReject(int.Parse(id));
            return Json(new BaseResponseView<Request01ReceiptChangeView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("ReceiptApprove")]
        public IActionResult Request01ReceiptChangeReceiptApprove(string id)
        {
            var service = new RequestReceiptChangeService(_configuration, _uowProvider, _mapper);
            var result = service.Request01ReceiptChangeReceiptApprove(int.Parse(id));
            return Json(new BaseResponseView<Request01ReceiptChangeView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("ReceiptReject")]
        public IActionResult Request01ReceiptChangeReceiptReject(string id)
        {
            var service = new RequestReceiptChangeService(_configuration, _uowProvider, _mapper);
            var result = service.Request01ReceiptChangeReceiptReject(int.Parse(id));
            return Json(new BaseResponseView<Request01ReceiptChangeView>() { data = result });
        }
    }
}