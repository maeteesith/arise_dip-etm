﻿using AutoMapper;
using DIP.TM.Models.Configs;
using DIP.TM.Models.Views;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.Medias;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils.Logging;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Web.Controllers
{
    public class GuestUploadController : BaseGuestController
    {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        private readonly AppConfigModel _configModel;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public GuestUploadController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            _configuration = configuration;
            _configModel = configuration.ToConfig();
            _uowProvider = uowProvider;
            _mapper = mapper;
        }
        //IConfiguration Configuration;
        //public UploadController(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponseView<FileView>), 200)]
        [HttpPost]
        public async Task<IActionResult> UploadFile(IFormFile file)
        {
            string file_name = Guid.NewGuid().ToString().Substring(0, 10);

            string phPath = "wwwroot/Contents/Users/data/upload/Guests/" + DateTime.Now.ToString("yyyy-MM");
            string publicPath = "Contents/Users/data/upload/Guests/" + DateTime.Now.ToString("yyyy-MM");
            string file_url = "/File/Content/{0}";

            var path = _configModel.ContentPath.BasePath == string.Empty ? Path.Combine(Directory.GetCurrentDirectory(), phPath) :
                                Path.Combine(_configModel.ContentPath.BasePath, phPath.Replace("wwwroot/", ""));


            LineNotifyHelper.SendWait(_configuration, "guest upload path : " + path);

            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            if (file.Length > 0)
            {
                string fileExtension = "." + file.FileName.Split(".").Last().ToLower();

                using (var fileStream = new FileStream(Path.Combine(path, file_name + fileExtension), FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                var payload = new FileView()
                {
                    file_name = file.FileName,
                    file_path = path,
                    file_size = file.Length,
                    physical_path = publicPath + "/" + file_name + fileExtension
                };

                var service = new FileGuestService(_configuration, _uowProvider, _mapper);
                var result = service.Add(payload);
                result.file_url = string.Format(file_url, result.id);
                return Json(new BaseResponseView<FileView>() { data = result, is_error = false });
            }
            return Json(new BaseResponseView<FileView>() { is_error = true, error_message = "can not upload" });
        }
    }
}
