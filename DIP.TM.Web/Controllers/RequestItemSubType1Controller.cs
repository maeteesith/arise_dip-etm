﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DIP.TM.Web.Extensions;
using System.Collections.Generic;
using DIP.TM.Services.ReferenceMasters;
using System;
using DIP.TM.Models;
using DIP.TM.Services.ReferenceMaster;
using DIP.TM.Models.Pagers;
using DIP.TM.Services.Checking;
using DIP.TM.Services.Interfaces;
using DIP.TM.Utils;
using Microsoft.AspNetCore.Authorization;
using Newtonsoft.Json;
using System.Linq;

namespace DIP.TM.Web.Controllers {
    
    public class RequestItemSubType1Controller : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        protected Dictionary<string, dynamic> save_service_list;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestItemSubType1Controller(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper; }

        //[ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        [ProducesResponseType(typeof(BaseResponseView<List<string>>), 200)]
        [HttpPost]
        [Route("CheckTag")]
        public IActionResult CheckTag(ReferenceMasterModel payload) {
        //public ActionResult<List<BaseSaveView>> CheckTag(ReferenceMasterModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() });

                var service = new RequestItemSubType1Service(_configuration, _uowProvider, _mapper);
                var result = service.CheckTag(payload);
            return Json(new BaseResponseView<List<ReferenceMasterView>>() { data = result });
            //return Json(new BaseResponseView<List<string>>() { data = result });
        }
    }
}
