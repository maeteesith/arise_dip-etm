﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.RequestChecks;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DIP.TM.Web.Extensions;
using System.Collections.Generic;
using DIP.TM.Services.ReferenceMasters;
using System;
using DIP.TM.Models;
using DIP.TM.Services.Checking;
using DIP.TM.Services.Auths;
using DIP.TM.Models.Pagers;
using System.Linq;
using System.Threading.Tasks;
using DIP.TM.Services.Receipts;
using DIP.TM.Utils;

namespace DIP.TM.Web.Controllers {

    public class RequestCheckController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        private readonly RequestCheckService _requestCheckService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestCheckController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, RequestCheckService requestCheckService) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _requestCheckService = requestCheckService;
        }

        [ProducesResponseType(typeof(BaseResponseView<RequestCheckView>), 200)]
        [HttpPost]
        [Route("RequestCheckLoad/{id}")]
        public IActionResult RequestCheckLoad(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new RequestCheckService(_configuration, _uowProvider, _mapper);
            var view = _requestCheckService.RequestCheckLoad(Convert.ToInt64(id));
            return Json(new BaseResponseView<RequestCheckView>() { data = view });
        }

        [ProducesResponseType(typeof(BaseResponseView<RequestCheckView>), 200)]
        [HttpPost]
        [Route("RequestCheckList")]
        public IActionResult RequestCheckList([FromBody]PageRequest payload) {
            var service = new BaseService<RequestCheck, RequestCheckView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);

            foreach (var item in result.data.list) {
                var receipt_service = new ReceiptService(_configuration, _uowProvider, _mapper);
                var receipt = receipt_service.Load(new ReceiptAddModel() {
                    reference_number = item.reference_number,
                });

                if (receipt != null && receipt.status_code == ReceiptStatusCodeConstant.PAID.ToString()) {
                    item.is_paid = true;
                }
            }

            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<RequestCheckView>), 200)]
        [HttpPost]
        [Route("RequestCheckSave")]
        public IActionResult RequestCheckSave([FromBody]RequestCheckAddModel payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new RequestCheckService(_configuration, _uowProvider, _mapper);
            var view = _requestCheckService.RequestCheckSave(payload);
            return Json(new BaseResponseView<RequestCheckView>() { data = view });
        }

        [ProducesResponseType(typeof(BaseResponseView<RequestCheckView>), 200)]
        [HttpPost]
        [Route("RequestCheckPrint")]
        public IActionResult RequestCheckPrint([FromBody]RequestCheckAddModel payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new RequestCheckService(_configuration, _uowProvider, _mapper);
            payload.id = null;

            var view = _requestCheckService.RequestCheckPrint(payload);


            //foreach (var item in result.data.list) {
            var receipt_service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var receipt = receipt_service.Load(new ReceiptAddModel() {
                reference_number = view.reference_number,
            });

            if (receipt != null && receipt.status_code == ReceiptStatusCodeConstant.PAID.ToString()) {
                view.is_paid = true;
            }
            //}

            return Json(new BaseResponseView<RequestCheckView>() { data = view });
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("RequestCheckRequestNumberAdd")]
        public IActionResult RequestCheckRequestNumberAdd([FromBody]vSave010Model payload) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
            var view = service.List(payload);
            return Json(new BaseResponseView<vSave010View>() { data = view.FirstOrDefault() });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vUM_UserView>>), 200)]
        [HttpPost]
        [Route("RequestCheckRegistrarList")]
        public IActionResult RequestCheckRegistrarList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vUM_User, vUM_UserView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<ReferenceMasterView>>), 200)]
        [HttpPost]
        [Route("DocumentClassificationImageTypeCodeList")]
        public IActionResult DocumentClassificationImageTypeCodeList([FromBody]PageRequest payload) {
            var service = new BaseService<RM_Save010DocumentClassificationImageType, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<ReferenceMasterView>>), 200)]
        [HttpPost]
        [Route("RequestSoundTypeCodeList")]
        public IActionResult RequestSoundTypeCodeList([FromBody]PageRequest payload) {
            var service = new BaseService<RM_RequestSoundType, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<ReferenceMasterView>>), 200)]
        [HttpPost]
        [Route("RequestItemSubTypeGroupList")]
        public IActionResult RequestItemSubTypeGroupList([FromBody]PageRequest payload) {
            var service = new BaseService<RM_RequestItemSubTypeGroup, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<ReferenceMasterView>>), 200)]
        [HttpPost]
        [Route("DocumentClassificationImageTypeList")]
        public IActionResult DocumentClassificationImageTypeList([FromBody]PageRequest payload) {
            var service = new BaseService<RM_Save010DocumentClassificationImageType, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<ReferenceMasterView>>), 200)]
        [HttpPost]
        [Route("RequestSoundTypeList")]
        public IActionResult RequestSoundTypeList([FromBody]PageRequest payload) {
            var service = new BaseService<RM_RequestSoundType, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<ReferenceMasterView>>), 200)]
        [HttpPost]
        [Route("RequestAgencyList")]
        public IActionResult RequestAgencyList([FromBody]PageRequest payload) {
            var service = new BaseService<vRequestAgency, vRequestAgencyView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }
    }
}
