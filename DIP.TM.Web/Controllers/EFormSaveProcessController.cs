﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Checking;
using DIP.TM.Services.eForms;
using DIP.TM.Services.ReferenceMasters;
using DIP.TM.Services.Saves;
using DIP.TM.Services.SaveInstructionRules;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;

namespace DIP.TM.Web.Controllers {

    public class EFormSaveProcessController : BaseGuestController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        protected Dictionary<string, dynamic> eform_service_list;
        protected Dictionary<string, dynamic> save_service_list;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public EFormSaveProcessController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;

            eform_service_list = new Dictionary<string, dynamic>();
            eform_service_list.Add("010", new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("020", new EFormProcessService<eForm_Save020AddModel, eForm_Save020, eForm_Save020View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("021", new EFormProcessService<eForm_Save021AddModel, eForm_Save021, eForm_Save021View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("030", new EFormProcessService<eForm_Save030AddModel, eForm_Save030, eForm_Save030View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("040", new EFormProcessService<eForm_Save040AddModel, eForm_Save040, eForm_Save040View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("050", new EFormProcessService<eForm_Save050AddModel, eForm_Save050, eForm_Save050View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("060", new EFormProcessService<eForm_Save060AddModel, eForm_Save060, eForm_Save060View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("070", new EFormProcessService<eForm_Save070AddModel, eForm_Save070, eForm_Save070View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("080", new EFormProcessService<eForm_Save080AddModel, eForm_Save080, eForm_Save080View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("120", new EFormProcessService<eForm_Save120AddModel, eForm_Save120, eForm_Save120View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("140", new EFormProcessService<eForm_Save140AddModel, eForm_Save140, eForm_Save140View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("150", new EFormProcessService<eForm_Save150AddModel, eForm_Save150, eForm_Save150View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("190", new EFormProcessService<eForm_Save190AddModel, eForm_Save190, eForm_Save190View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("200", new EFormProcessService<eForm_Save200AddModel, eForm_Save200, eForm_Save200View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("210", new EFormProcessService<eForm_Save210AddModel, eForm_Save210, eForm_Save210View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("220", new EFormProcessService<eForm_Save220AddModel, eForm_Save220, eForm_Save220View>(_configuration, _uowProvider, _mapper));
            eform_service_list.Add("230", new EFormProcessService<eForm_Save230AddModel, eForm_Save230, eForm_Save230View>(_configuration, _uowProvider, _mapper));

            save_service_list = new Dictionary<string, dynamic>();
            save_service_list.Add("010", new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("020", new SaveProcessService<Save020AddModel, Save020, Save020ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("021", new SaveProcessService<Save021AddModel, Save021, Save021ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("030", new SaveProcessService<Save030AddModel, Save030, Save030ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("040", new SaveProcessService<Save040AddModel, Save040, Save040ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("050", new SaveProcessService<Save050AddModel, Save050, Save050ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("060", new SaveProcessService<Save060AddModel, Save060, Save060ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("070", new SaveProcessService<Save070AddModel, Save070, Save070ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("080", new SaveProcessService<Save080AddModel, Save080, Save080ProcessView>(_configuration, _uowProvider, _mapper));
            save_service_list.Add("000", new SaveProcessService<SaveOtherAddModel, SaveOther, SaveOtherProcessView>(_configuration, _uowProvider, _mapper));
        }

        [ProducesResponseType(typeof(BaseResponseView<vEForm210DateListView>), 200)]
        [HttpGet]
        [Route("DateList{id}ByRequestNumber/{request_number_list}")]
        public IActionResult DateListByRequestNumber([FromRoute]string request_number_list, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<vEForm210DateListView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() });

            if (!eform_service_list.ContainsKey(id))
            {
                return Json(null);
            }

            var service = eform_service_list[id];
            var result = service.DateListByRequestNumber(request_number_list);

            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<EFormView>), 200)]
        [HttpGet]
        [Route("EForm060SearchDataRequestNumber/{request_number}")]
        public IActionResult EForm060SearchDataRequestNumber([FromRoute]string request_number)
        {
            var service = new EFormProcessService<eForm_Save060AddModel, eForm_Save060, eForm_Save060View>(_configuration, _uowProvider, _mapper);
            var result = service.EForm060SearchDataRequestNumber(request_number);

            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<EFormView>), 200)]
        [HttpGet]
        [Route("EForm060SearchDataChallengerNumber/{challenger_number}")]
        public IActionResult EForm060SearchDataChallengerNumber([FromRoute]string challenger_number)
        {
            var service = new EFormProcessService<eForm_Save060AddModel, eForm_Save060, eForm_Save060View>(_configuration, _uowProvider, _mapper);
            var result = service.EForm060SearchDataChallengerNumber(challenger_number);

            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<EFormView>), 200)]
        [HttpGet]
        [Route("EForm060SearchDataRuleNumber/{rule_number}")]
        public IActionResult EForm060SearchDataRuleNumber([FromRoute]string rule_number)
        {
            var service = new EFormProcessService<eForm_Save060AddModel, eForm_Save060, eForm_Save060View>(_configuration, _uowProvider, _mapper);
            var result = service.EForm060SearchDataRuleNumber(rule_number);

            return Json(result);
        }

        //new load by challenger number
        [ProducesResponseType(typeof(BaseResponseView<EFormView>), 200)]
        [HttpPost]
        [Route("LoadEForm{id}ByChallengerNumber/{challenger_number}")]
        public IActionResult LoadEFormByChallengerNumber([FromRoute]string challenger_number, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<EFormView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
            if (!eform_service_list.ContainsKey(id))
            {
                return Json(null);
            }

            var service = eform_service_list[id];
            var result = service.LoadEFormByChallengerNumber(challenger_number);

            return Json(result);
        }
        //new load by contract number
        [ProducesResponseType(typeof(BaseResponseView<EFormView>), 200)]
        [HttpPost]
        [Route("LoadEForm{id}ByContractNumber/{contract_number}")]
        public IActionResult LoadEFormByContractNumber([FromRoute]string contract_number, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<EFormView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
            if (!eform_service_list.ContainsKey(id))
            {
                return Json(null);
            }

            var service = eform_service_list[id];
            var result = service.LoadEFormByContractNumber(contract_number);

            return Json(result);
        }
        //new load by request number
        [ProducesResponseType(typeof(BaseResponseView<EFormView>), 200)]
        [HttpPost]
        [Route("LoadEForm{id}ByRequestNumber/{request_number}")]
        public IActionResult LoadEFormByRequestNumber([FromRoute]string request_number, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<EFormView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
            if (!eform_service_list.ContainsKey(id))
            {
                return Json(null);
            }

            var service = eform_service_list[id];
            var result = service.LoadEFormByRequestNumber(request_number);

            return Json(result);
        }
        //new load by id
        [ProducesResponseType(typeof(BaseResponseView<EFormView>), 200)]
        [HttpPost]
        [Route("LoadEForm{id}ByID/{eform_id}")]
        public IActionResult LoadEFormByID([FromRoute]int eform_id, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<EFormView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
            if (!eform_service_list.ContainsKey(id))
            {
                return Json(null);
            }

            var service = eform_service_list[id];
            var result = service.LoadEFormByID(eform_id);

            return Json(result);
        }

        //new load by email_uuid

        //old only eform021 waiting for remove after tell netcube
        [ProducesResponseType(typeof(BaseResponseView<eForm_Save021View>), 200)]
        [HttpPost]
        [Route("eForm021LoadFromRequestNumber/{request_number}")]
        public IActionResult eForm021LoadFromRequestNumber(string request_number)
        {
            var currentUser = this.CurrentUser();
            var service = new EFormProcessService<eForm_Save021AddModel, eForm_Save021, eForm_Save021View>(_configuration, _uowProvider, _mapper);
            var result = service.LoadEFormByRequestNumber(request_number);
            return Json(result);
        }
        // old only save010
        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("eFormFromRequestNumberLoad/{request_number}")]
        public IActionResult eFormFromRequestNumberLoad(string request_number) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
            var result = service.eFormFromRequestNumberLoad(request_number, "010");
            return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        }

        // new only save010
        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("eForm{id}FromRequestNumberLoad/{request_number}")]
        public IActionResult eFormFromRequestNumberLoadNew(string request_number, [FromRoute] string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<Save010ProcessView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
            var result = service.eFormFromRequestNumberLoad(request_number, id);
            return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("eFormSave{id}Load/{eform_number}")]
        public IActionResult Load(string eform_number, string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
            if (!eform_service_list.ContainsKey(id))
            {
                return Json(null);
            }

            var service = eform_service_list[id];
            var result = service.Load(eform_number);

            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Load{id}RequestList/{request_number}")]
        public IActionResult LoadRequestList([FromRoute]string id, [FromRoute]string request_number)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
            if (!save_service_list.ContainsKey(id))
            {
                return Json(null);
            }

            var service = save_service_list[id];
            var result = service.LoadRequestList(request_number);

            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("eFormSave{id}LoadConsideringSimilarDocument/{save_id}")]
        public IActionResult LoadConsideringSimilarDocument(long save_id, string id) {
            var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = service.ConsideringSimilarDocumentLoad(save_id);

            result.instruction_rule_list = result.instruction_rule_list.Where(r => r.considering_instruction_rule_status_code == ConsideringSimilarInstructionRuleStatusCodeConstant.WAIT_ACTION.ToString()).ToList();

            return Json(new BaseResponseView<ConsideringSimilarDocumentView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("eFormSave{id}Save")]
        public IActionResult Save([FromBody]object payload, string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
            if (!eform_service_list.ContainsKey(id))
            {
                return Json(null);
            }

            var service = eform_service_list[id];
            var result = service.Save(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<SaveProductView>>), 200)]
        [HttpPost]
        [Route("ProductCatagory01ItemList")]
        public IActionResult ProductCatagory01ItemList([FromBody]PageRequest payload) {
            var service = new BaseService<RM_RequestItemSubType1, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<vLocationView>), 200)]
        [HttpPost]
        [Route("eFormSave{id}ListLocation")]
        [Route("eFormSave{id}ListModalLocation")]
        public IActionResult eFormListModalLocation([FromBody]BasePageModelPayload<RS_AddressAddModel> payload, string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new LocationService(_configuration, _uowProvider, _mapper);
            var result = service.List(new BasePageModelPayload<RS_AddressAddModel>()
            {
                filter = new RS_AddressAddModel() { name = payload.filter.name },
                paging = new PagingModel()
                {
                    item_per_page = payload.paging != null ? payload.paging.item_per_page : 5
                }
            });
            return Json(new BaseResponseView<List<vLocationView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("LoadInstructionRuleByInstructionNumber/{instruction_number}")]
        public IActionResult LoadInstructionRuleByInstructionNumber([FromRoute]string instruction_number)
        {
            var service = new SaveInstructionRuleService(_configuration, _uowProvider, _mapper);
            var result = service.LoadInstructionRuleByInstructionNumber(instruction_number);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<string>), 200)]
        [HttpGet]
        [Route("ProductCategory/{productName}")]
        public IActionResult ProductCatagory([FromRoute]string productName)
        {
            var service = new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper);
            var result = service.GetProductCategory(productName);

            return Json(new BaseResponseView<string>() { data = result });
        }

        //[ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        //[HttpPost]
        //[Route("Save010FullLoad/{id}")]
        //public IActionResult Save010FullLoad(string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save010AddModel, Save010, vSave010View>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save010Load(Convert.ToInt64(id), true);
        //    return Json(new BaseResponseView<vSave010View>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        //[HttpPost]
        //[Route("Save{id1}Load/{id2}")]
        //public IActionResult Load(string id1, string id2) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    if (id1 == "01") {
        //        var service = new SaveProcessService<SaveModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        //    } else if (id1 == "020") {
        //        var service = new SaveProcessService<SaveModel, Save020, Save020ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save020ProcessView>() { data = result });
        //    } else if (id1 == "021") {
        //        var service = new SaveProcessService<SaveModel, Save021, Save021ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save021ProcessView>() { data = result });
        //    } else if (id1 == "03") {
        //        var service = new SaveProcessService<SaveModel, Save030, Save030ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save030ProcessView>() { data = result });
        //    } else if (id1 == "04") {
        //        var service = new SaveProcessService<SaveModel, Save040, Save040ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save040ProcessView>() { data = result });
        //    } else if (id1 == "05") {
        //        var service = new SaveProcessService<SaveModel, Save050, Save050ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save050ProcessView>() { data = result });
        //    } else if (id1 == "06") {
        //        var service = new SaveProcessService<SaveModel, Save060, Save060ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save060ProcessView>() { data = result });
        //    } else if (id1 == "07") {
        //        var service = new SaveProcessService<SaveModel, Save070, Save070ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save070ProcessView>() { data = result });
        //    } else if (id1 == "08") {
        //        var service = new SaveProcessService<SaveModel, Save080, Save080ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save080ProcessView>() { data = result });
        //    } else if (id1 == "140") {
        //        var service = new SaveProcessService<SaveModel, Save140, Save140ProcessView>(_configuration, _uowProvider, _mapper);
        //        var result = service.Get(Convert.ToInt64(id2));
        //        return Json(new BaseResponseView<Save140ProcessView>() { data = result });
        //    }
        //    return null;
        //}

        //[ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        //[HttpPost]
        //[Route("Save{id}List")]
        //public IActionResult List([FromBody]dynamic payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<Request01ProcessAddModel>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = save_service_list[id];
        //    var result = service.List(payload);
        //    return Json(service.GetResponseView(result));

        //    //if (id == "01") {
        //    //    var service = new SaveProcessService<SaveModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save010ProcessView>>() { data = result });
        //    //} else if (id == "020") {
        //    //    var service = new SaveProcessService<SaveModel, Save020, Save020ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save020ProcessView>>() { data = result });
        //    //} else if (id == "021") {
        //    //    var service = new SaveProcessService<SaveModel, Save021, Save021ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save021ProcessView>>() { data = result });
        //    //} else if (id == "03") {
        //    //    var service = new SaveProcessService<SaveModel, Save030, Save030ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save030ProcessView>>() { data = result });
        //    //} else if (id == "04") {
        //    //    var service = new SaveProcessService<SaveModel, Save040, Save040ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save040ProcessView>>() { data = result });
        //    //} else if (id == "05") {
        //    //    var service = new SaveProcessService<SaveModel, Save050, Save050ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save050ProcessView>>() { data = result });
        //    //} else if (id == "06") {
        //    //    var service = new SaveProcessService<SaveModel, Save060, Save060ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save060ProcessView>>() { data = result });
        //    //} else if (id == "07") {
        //    //    var service = new SaveProcessService<SaveModel, Save070, Save070ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save070ProcessView>>() { data = result });
        //    //} else if (id == "08") {
        //    //    var service = new SaveProcessService<SaveModel, Save080, Save080ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save080ProcessView>>() { data = result });
        //    //} else if (id == "14") {
        //    //    var service = new SaveProcessService<SaveModel, Save140, Save140ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.List(payload);
        //    //    return Json(new BaseResponseView<List<Save140ProcessView>>() { data = result });
        //    //}
        //    //return Json(new BaseResponseView<BaseView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
        //}

        //[ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        //[HttpPost]
        //[Route("Save{id}ListNotSent")]
        //public IActionResult ListNotSent([FromBody]BasePageModelPayload<SaveListDraftModel> payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = save_service_list[id];
        //    var result = service.ListNotSent(payload);
        //    return Json(service.GetResponseView(result));

        //    //if (id == "01") {
        //    //    var service = new SaveProcessService<SaveModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload.filter);
        //    //    return Json(new BaseResponseView<List<Save010ProcessView>>() { data = result });
        //    //} else if (id == "020") {
        //    //    var service = new SaveProcessService<SaveModel, Save020, Save020ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save020ProcessView>>() { data = result });
        //    //} else if (id == "021") {
        //    //    var service = new SaveProcessService<SaveModel, Save021, Save021ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save021ProcessView>>() { data = result });
        //    //} else if (id == "03") {
        //    //    var service = new SaveProcessService<SaveModel, Save030, Save030ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save030ProcessView>>() { data = result });
        //    //} else if (id == "04") {
        //    //    var service = new SaveProcessService<SaveModel, Save040, Save040ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save040ProcessView>>() { data = result });
        //    //} else if (id == "05") {
        //    //    var service = new SaveProcessService<SaveModel, Save050, Save050ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save050ProcessView>>() { data = result });
        //    //} else if (id == "06") {
        //    //    var service = new SaveProcessService<SaveModel, Save060, Save060ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save060ProcessView>>() { data = result });
        //    //} else if (id == "07") {
        //    //    var service = new SaveProcessService<SaveModel, Save070, Save070ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save070ProcessView>>() { data = result });
        //    //} else if (id == "08") {
        //    //    var service = new SaveProcessService<SaveModel, Save080, Save080ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save080ProcessView>>() { data = result });
        //    //} else if (id == "140") {
        //    //    var service = new SaveProcessService<SaveModel, Save140, Save140ProcessView>(_configuration, _uowProvider, _mapper);
        //    //    var result = service.ListNotSent(payload);
        //    //    return Json(new BaseResponseView<List<Save140ProcessView>>() { data = result });
        //    //}
        //    //return null;
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save01Save")]
        //public IActionResult Save01Save([FromBody]Save010AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save(payload);
        //    return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save020Save")]
        //public IActionResult Save020Save([FromBody]Save020AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save020AddModel, Save020, Save020ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save020AddModel)payload);
        //    return Json(new BaseResponseView<Save020ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save021Save")]
        //public IActionResult Save021Save([FromBody]Save021AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save021AddModel, Save021, Save021ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save021AddModel)payload);
        //    return Json(new BaseResponseView<Save021ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save03Save")]
        //public IActionResult Save030Save([FromBody]Save030AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save030AddModel, Save030, Save030ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save030AddModel)payload);
        //    return Json(new BaseResponseView<Save030ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save04Save")]
        //public IActionResult Save040Save([FromBody]Save040AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save040AddModel, Save040, Save040ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save040AddModel)payload);
        //    return Json(new BaseResponseView<Save040ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save05Save")]
        //public IActionResult Save050Save([FromBody]Save050AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save050AddModel, Save050, Save050ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save050AddModel)payload);
        //    return Json(new BaseResponseView<Save050ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save06Save")]
        //public IActionResult Save060Save([FromBody]Save060AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save060AddModel, Save060, Save060ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save060AddModel)payload);
        //    return Json(new BaseResponseView<Save060ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save07Save")]
        //public IActionResult Save070Save([FromBody]Save070AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save070AddModel, Save070, Save070ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save070AddModel)payload);
        //    return Json(new BaseResponseView<Save070ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save08Save")]
        //public IActionResult Save080Save([FromBody]Save080AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save080AddModel, Save080, Save080ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save080AddModel)payload);
        //    return Json(new BaseResponseView<Save080ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        //[HttpPost]
        //[Route("Save140Save")]
        //public IActionResult Save140Save([FromBody]Save140AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save140AddModel, Save140, Save140ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Save((Save140AddModel)payload);
        //    return Json(new BaseResponseView<Save140ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save{id}Delete")]
        //public IActionResult Delete([FromBody]dynamic payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = save_service_list[id];
        //    var result = service.Delete(payload);
        //    return Json(new BaseResponseView<BaseSaveView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save01Send")]
        //public IActionResult Save01Send([FromBody]Save010AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save020ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save020Send")]
        //public IActionResult Save020Send([FromBody]Save020AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save020AddModel, Save020, Save020ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save020ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save021ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save021Send")]
        //public IActionResult Save021Send([FromBody]Save021AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save021AddModel, Save021, Save021ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save021ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save030ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save03Send")]
        //public IActionResult Save030Send([FromBody]Save030AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save030AddModel, Save030, Save030ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save030ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save040ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save04Send")]
        //public IActionResult Save040Send([FromBody]Save040AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save040AddModel, Save040, Save040ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save040ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save050ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save05Send")]
        //public IActionResult Save050Send([FromBody]Save050AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save050AddModel, Save050, Save050ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save050ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save060ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save06Send")]
        //public IActionResult Save060Send([FromBody]Save060AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save060AddModel, Save060, Save060ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save060ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save070ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save07Send")]
        //public IActionResult Save070Send([FromBody]Save070AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save070AddModel, Save070, Save070ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save070ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<Save080ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save08Send")]
        //public IActionResult Save080Send([FromBody]Save080AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save080AddModel, Save080, Save080ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save080ProcessView>() { data = result });
        //}


        //[ProducesResponseType(typeof(BaseResponseView<Save140ProcessView>), 200)]
        //[HttpPost]
        //[Route("Save140Send")]
        //public IActionResult Save140Send([FromBody]Save140AddModel payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new SaveProcessService<Save140AddModel, Save140, Save140ProcessView>(_configuration, _uowProvider, _mapper);
        //    var result = service.Send(payload);
        //    return Json(new BaseResponseView<Save140ProcessView>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<ReferenceMasterView>), 200)]
        //[HttpPost]
        //[Route("Save{id}ListCommercialAffairsProvince")]
        //public IActionResult ListCommercialAffairsProvince([FromBody]BasePageModelPayload<ReferenceMasterModel> payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new ReferenceMasterService(_configuration, _uowProvider, _mapper);
        //    var result = service.List(new BasePageModelPayload<ReferenceMasterModel>() {
        //        filter = new ReferenceMasterModel() {
        //            topic = "COMMERCIAL_AFFAIRS_PROVINCE_CODE",
        //            name = payload.filter.name,
        //        },
        //        paging = new PagingModel() {
        //            item_per_page = payload.paging != null ? payload.paging.item_per_page : 5
        //        }
        //    });
        //    return Json(new BaseResponseView<List<ReferenceMasterView>>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<vLocationView>), 200)]
        //[HttpPost]
        //[Route("Save{id}ListLocation")]
        //[Route("Save{id}ListModalLocation")]
        //public IActionResult ListModalLocation([FromBody]BasePageModelPayload<RS_AddressAddModel> payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new LocationService(_configuration, _uowProvider, _mapper);
        //    var result = service.List(new BasePageModelPayload<RS_AddressAddModel>() {
        //        filter = new RS_AddressAddModel() { name = payload.filter.name },
        //        paging = new PagingModel() {
        //            item_per_page = payload.paging != null ? payload.paging.item_per_page : 5
        //        }
        //    });
        //    return Json(new BaseResponseView<List<vLocationView>>() { data = result });
        //}

        //[ProducesResponseType(typeof(BaseResponseView<ReferenceMasterView>), 200)]
        //[HttpPost]
        //[Route("ListItemSubType{id}Code")]
        //public IActionResult ListItemSubType([FromBody]BasePageModelPayload<ReferenceMasterModel> payload, string id) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new ReferenceMasterService(_configuration, _uowProvider, _mapper);
        //    var result = service.List(new BasePageModelPayload<ReferenceMasterModel>() {
        //        filter = new ReferenceMasterModel() {
        //            topic = "REQUEST_ITEM_SUB_TYPE_" + id,
        //            name = payload.filter.name,
        //        },
        //        paging = new PagingModel() {
        //            item_per_page = payload.paging != null ? payload.paging.item_per_page : 5
        //        }
        //    }); ;
        //    return Json(new BaseResponseView<List<ReferenceMasterView>>() { data = result });
        //}
    }
}
