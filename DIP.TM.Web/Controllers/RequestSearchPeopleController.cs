﻿using AutoMapper;
using System;
using DIP.TM.Models.Views;
using DIP.TM.Services.GDX;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Models.Payloads;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using DIP.TM.Services.Saves;
using DIP.TM.Datas;
using System.Linq;
using DIP.TM.Services.RequestChecks;

namespace DIP.TM.Web.Controllers {
    public class RequestSearchPeopleController : BaseGuestController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestSearchPeopleController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        [ProducesResponseType(typeof(BaseResponseView<RequestCheckView>), 200)]
        [HttpPost]
        [Route("RequestCheckVoucherNumberLoad/{id}")]
        public IActionResult RequestCheckVoucherNumberLoad(string id) {

            var service = new RequestCheckService(_configuration, _uowProvider, _mapper);
            var result = service.RequestCheckVoucherNumberLoad(id);
            return Json(new BaseResponseView<RequestCheckView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("RequestSearchLoad/{request_number}")]
        public IActionResult RequestSearchLoad(string request_number) {
            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
            var result = service.List(new vSave010Model() {
                request_number = request_number,
            }).FirstOrDefault();

            var service_full_view = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
            result.full_view = service_full_view.List(new Save010AddModel() {
                request_number = request_number,
            }).FirstOrDefault();

            return Json(new BaseResponseView<vSave010View>() { data = result });
        }
    }
}