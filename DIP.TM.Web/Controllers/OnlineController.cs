﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Views;
using DIP.TM.Services.Interfaces;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;

namespace DIP.TM.Web.Controllers {
    public class OnlineController : BaseGuestController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        private readonly ICheckingProcessService _checkingProcessService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public OnlineController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, ICheckingProcessService checkingProcessService) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _checkingProcessService = checkingProcessService;
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpGet, HttpPost]
        [Route("PublicRoundList")]
        public IActionResult PublicRoundList() {
            var service = new BaseService<vOnlinePublicRound, vOnlinePublicRoundView>(_configuration, _uowProvider, _mapper);
            var payload = new PageRequest();
            payload.item_per_page = 1000;
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpGet, HttpPost]
        [Route("PublicRoundMonthList/{year}/{month}")]
        public IActionResult PublicRoundMonthList(string year, string month) {
            var service = new BaseService<vOnlinePublicRoundMonth, vOnlinePublicRoundMonthView>(_configuration, _uowProvider, _mapper);

            var payload = new PageRequest();
            payload.item_per_page = 1000;
            payload.search_by = new List<SearchByModel>() {
                 new SearchByModel() {
                      key = "public_year",
                      value = year,
                      operation = 0,
                 },
                 new SearchByModel() {
                      key = "public_month",
                      value = month,
                      operation = 0,
                 }
            };

            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpGet, HttpPost]
        [Route("PublicRoundProductList/{pr_id}")]
        public IActionResult PublicRoundMonthList(string pr_id) {
            var service = new BaseService<vOnlinePublicRoundProduct, vOnlinePublicRoundProductView>(_configuration, _uowProvider, _mapper);

            var payload = new PageRequest();
            payload.item_per_page = 1000;
            payload.search_by = new List<SearchByModel>() {
                 new SearchByModel() {
                      key = "pr_id",
                      value = pr_id,
                      operation = 0,
                 }
            };

            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpGet, HttpPost]
        [Route("PublicRoundItemList/{pr_id}/{product_id}")]
        public IActionResult PublicRoundItemList(string pr_id, string product_id) {
            var service = new BaseService<vOnlinePublicRoundItem, vOnlinePublicRoundItemView>(_configuration, _uowProvider, _mapper);

            var payload = new PageRequest();
            payload.item_per_page = 1000;
            payload.search_by = new List<SearchByModel>() {
                 new SearchByModel() {
                      key = "pr_id",
                      value = pr_id,
                      operation = 0,
                 },
                 new SearchByModel() {
                      key = "request_item_sub_type_1_code",
                      value = product_id,
                      operation = 0,
                 },
            };

            var result = service.List(payload);
            return Json(result);
        }
    }
}
