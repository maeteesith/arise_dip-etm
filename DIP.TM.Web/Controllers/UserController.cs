﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Auths;
using DIP.TM.Services.Requests;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using DIP.TM.Web.Extensions;

namespace DIP.TM.Web.Controllers
{
   
    public class UserController : BaseApiController
    {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public UserController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponseView<UserSessionView>), 200)]
        [HttpPost]
        public IActionResult Login([FromBody]LoginRequestModel payload)
        {
            var service = new UserService(_configuration, _uowProvider, _mapper);
            var result = service.Login(payload);
            return Json(new BaseResponseView<UserSessionView>() { is_error=false,data=result});
        }


        [ProducesResponseType(typeof(string), 200)]
        [HttpGet]
        [Route("GetToken")]
        public IActionResult GetToken()
        {
            return Json("GGGGG");
        }

        [ProducesResponseType(typeof(DateTime), 200)]
        [HttpGet]
        [Route("GetExpired")]
        public IActionResult GetExpired()
        {
            return Json(new DateTime().AddMinutes(20));
        }

        [ProducesResponseType(typeof(BaseResponseView<vUserSessionView>), 200)]
        [HttpGet]
        [Route("GetUserInfo")]
        public IActionResult GetUserInfo() {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<vUserSessionView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new UserService(_configuration, _uowProvider, _mapper);
            var result = service.GetUserInfo();
            return Json(new BaseResponseView<vUserSessionView>() { data = result });
        }

    }
}
