﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Receipts;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Web.Controllers {

    public class ReceiptProcessController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public ReceiptProcessController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        [ProducesResponseType(typeof(BaseResponseView<ReceiptView>), 200)]
        [HttpPost]
        [Route("LoadUnpaid/{reference_number}")]
        public IActionResult LoadUnpaid(string reference_number) {
            var service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var result = service.Load(new ReceiptAddModel() {
                reference_number = reference_number,
                //status_code = ReceiptStatusCodeConstant.UNPAID.ToString(),
            });

            result.item_list = result.item_list.Where(r => r.total_price > 0).ToList();

            return Json(new BaseResponseView<ReceiptView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Load")]
        public IActionResult Load([FromBody]ReceiptAddModel payload) {
            var service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var result = service.Load(payload);
            return Json(new BaseResponseView<ReceiptView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Save")]
        public IActionResult Save([FromBody]ReceiptAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<ReceiptView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var result = service.Save(payload);
            return Json(new BaseResponseView<ReceiptView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Paid")]
        public IActionResult Paid([FromBody]ReceiptAddModel payload) {
            var service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var result = service.Paid(payload.id.Value);
            return Json(new BaseResponseView<ReceiptView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Paid/{id}")]
        public IActionResult Paid(string id) {
            var service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var result = service.Paid(Convert.ToInt64(id));
            return Json(new BaseResponseView<ReceiptView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Delete/{id}")]
        public IActionResult Delete(string id) {
            var service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var result = service.Delete(Convert.ToInt64(id));
            return Json(new BaseResponseView<ReceiptView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("Print")]
        public IActionResult Print([FromBody]ReceiptAddModel payload) {
            var service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var result = service.Print(payload);
            return Json(new BaseResponseView<FileView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("ReceiptDailyList")]
        public IActionResult ReceiptDailyList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vReceiptItem, vReceiptItemView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("ReceiptDailyDelete")]
        public IActionResult ReceiptDailyDelete([FromBody]ReceiptAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<ReceiptView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new ReceiptService(_configuration, _uowProvider, _mapper);
            var result = service.Delete(payload);
            return Json(new BaseResponseView<ReceiptView>() { data = result });
        }


    }
}
