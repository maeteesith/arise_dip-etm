﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Batch;
using DIP.TM.Services.Interfaces;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Web.Controllers
{
    [Route("api/[controller]")]
    public class BatchController : BaseApiController
    {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public BatchController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }
        [AllowAnonymous]
        [HttpGet]
        [Route("test")]
        public IActionResult test()
        {
            try
            {
                var service = new BatchService(_configuration, _uowProvider, _mapper);
                var xmlName = service.ListFiles();
                // var imagesName = service.ListImages();
                // var irregsName = service.ListIrregs();
                // var pdfName = service.ListPDF();

                // if (!string.IsNullOrEmpty(xmlName))
                // {
                //     service.DownloadFile("", xmlName);
                //     service.ExtractFiles("xml", xmlName);
                // }
                // if (!string.IsNullOrEmpty(imagesName))
                // {
                //     service.DownloadFile("images/", imagesName);
                //     service.ExtractFiles("images", imagesName);
                // }
                // if (!string.IsNullOrEmpty(irregsName))
                // {
                //     service.DownloadFile("irregs/", irregsName);
                //     service.ExtractFiles("irregs", irregsName);
                // }
                // if (!string.IsNullOrEmpty(pdfName))
                // {
                //     service.DownloadFile("PDF/", pdfName);
                //     service.ExtractFiles("PDF", pdfName);
                // }

                 var data = service.Writefile();
                //var data2 = service.Mapdata();
                return Ok(new {data = data});
            }
            catch(Exception ex)
            {
                return Ok(new { data = ex.Message });
            }

        }

        [AllowAnonymous]
        [HttpGet]
        [Route("test2")]
        public IActionResult test2()
        {
            try
            {
                var service = new BatchService(_configuration, _uowProvider, _mapper);
                var data2 = service.Mapdata();
                service.AddMadrid_EnotifCount(data2);
                //var res = service.GetPriceRequestMaster();
                return Ok("success");
            }
            catch(Exception ex)
            {
                return Ok(new { data = ex.Message });
            }

        }
    }
}