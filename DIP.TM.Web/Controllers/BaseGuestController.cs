﻿
using DIP.TM.Web.Filters;
using Microsoft.AspNetCore.Mvc;

namespace DIP.TM.Web.Controllers
{

    [Route("api/[controller]")]
    [TypeFilter(typeof(BenchmarkAttribute))]
    [TypeFilter(typeof(GuestUserFilterAttribute))]
    public class BaseGuestController : ControllerBase
    {
        [ApiExplorerSettings(IgnoreApi = true)]
        public IActionResult Json(object result)
        {
            return Ok(result);
        }
    }
}
