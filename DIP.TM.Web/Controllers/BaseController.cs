﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Views;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils.Logging;
using DIP.TM.Web.Filters;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace DIP.TM.Web.Controllers
{
    [AllowAnonymous]
    [TypeFilter(typeof(BenchmarkAttribute))]
    public class BaseController : Controller
    {
        /*[ApiExplorerSettings(IgnoreApi = true)]
        protected Stream GetStream(IConfiguration configuration, string fileUrl) {
            try {
                ServicePointManager.ServerCertificateValidationCallback = new RemoteCertificateValidationCallback(validarCertificado);
                WebRequest req = WebRequest.Create(fileUrl);
                WebResponse response = req.GetResponse();
                Stream stream = response.GetResponseStream();
                return stream;
            } catch (Exception ex) {
                LineNotifyHelper.SendWait(configuration, ex.ToString());
                return null;
            }
        }*/

        protected Stream GetStream(IConfiguration configuration, string path)
        {
            try
            {
                var stream = System.IO.File.OpenRead(path);
                return stream;
            }
            catch (Exception ex)
            {
                LineNotifyHelper.SendWait(configuration, ex.ToString());
                return null;
            }
        }


        [ApiExplorerSettings(IgnoreApi = true)]

        protected byte[] ReadToEnd(Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }
            try
            {
                var readBuffer = new byte[4096];

                var totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead != readBuffer.Length) continue;
                    var nextByte = stream.ReadByte();
                    if (nextByte == -1) continue;
                    var temp = new byte[readBuffer.Length * 2];
                    Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                    Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                    readBuffer = temp;
                    totalBytesRead++;
                }

                var buffer = readBuffer;
                if (readBuffer.Length == totalBytesRead) return buffer;
                buffer = new byte[totalBytesRead];
                Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                    stream.Position = originalPosition;
            }
        }
        [ApiExplorerSettings(IgnoreApi = true)]
        protected string ContentType(string FileExtension)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            // Images'
            d.Add(".bmp", "image/bmp");
            d.Add(".gif", "image/gif");
            d.Add(".jpeg", "image/jpeg");
            d.Add(".jpg", "image/jpeg");
            d.Add(".png", "image/png");
            d.Add(".tif", "image/tiff");
            d.Add(".tiff", "image/tiff");
            // Documents'
            d.Add(".doc", "application/msword");
            d.Add(".docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            d.Add(".pdf", "application/pdf");
            // Slideshows'
            d.Add(".ppt", "application/vnd.ms-powerpoint");
            d.Add(".pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
            // Data'
            d.Add(".xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
            d.Add(".xls", "application/vnd.ms-excel");
            d.Add(".csv", "text/csv");
            d.Add(".xml", "text/xml");
            d.Add(".txt", "text/plain");
            // Compressed Folders'
            d.Add(".zip", "application/zip");
            // Audio'
            d.Add(".ogg", "application/ogg");
            d.Add(".mp3", "audio/mpeg");
            d.Add(".wma", "audio/x-ms-wma");
            d.Add(".wav", "audio/x-wav");
            // Video'
            d.Add(".wmv", "audio/x-ms-wmv");
            d.Add(".swf", "application/x-shockwave-flash");
            d.Add(".avi", "video/avi");
            d.Add(".mp4", "video/mp4");
            d.Add(".mpeg", "video/mpeg");
            d.Add(".mpg", "video/mpeg");
            d.Add(".qt", "video/quicktime");
            return d[FileExtension];
        }

        public static dynamic GetService(string view_name, IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            if (view_name == "Save010CheckingInstructionDocument")
                return new BaseService<vSave010CheckingInstructionDocument, vSave010CheckingInstructionDocumentView>(configuration, uowProvider, mapper);
            else if (view_name == "SaveAddressAll")
                return new BaseService<vSaveAddressAll, vSaveAddressAllView>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole04")
                return new BaseService<vDocumentRole04, vDocumentRole04View>(configuration, uowProvider, mapper);
            else if (view_name == "PostRoundInstructionRuleFile")
                return new BaseService<vPostRoundInstructionRuleFile, vPostRoundInstructionRuleFileView>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole04ReleaseRequest")
                return new BaseService<vDocumentRole04ReleaseRequest, vDocumentRole04ReleaseRequestView>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole04ReleaseRequestGroup")
                return new BaseService<vDocumentRole04ReleaseRequestGroup, vDocumentRole04ReleaseRequestGroupView>(configuration, uowProvider, mapper);
            else if (view_name == "Save010")
                return new BaseService<vSave010, vSave010View>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole03Item")
                return new BaseService<vDocumentRole03Item, vDocumentRole03ItemView>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole03ItemGroup")
                return new BaseService<vDocumentRole03ItemGroup, vDocumentRole03ItemGroupView>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole04Create")
                return new BaseService<vDocumentRole04Create, vDocumentRole04CreateView>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole04CreateFile")
                return new BaseService<vDocumentRole04CreateFile, vDocumentRole04CreateFileView>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole04Release")
                return new BaseService<vDocumentRole04Release, vDocumentRole04ReleaseView>(configuration, uowProvider, mapper);
            else if (view_name == "DocumentRole04Release20")
                return new BaseService<vDocumentRole04Release20, vDocumentRole04Release20View>(configuration, uowProvider, mapper);
            else if (view_name == "RequestAgency")
                return new BaseService<vRequestAgency, vRequestAgencyView>(configuration, uowProvider, mapper);
            else if (view_name == "EForm_eForm_Save010")
                return new BaseService<vEForm_eForm_Save010, vEForm_eForm_Save010View>(configuration, uowProvider, mapper);
            else if (view_name == "EForm_eForm_Save010_Product")
                return new BaseService<vEForm_eForm_Save010_Product, vEForm_eForm_Save010_ProductView>(configuration, uowProvider, mapper);
            else if (view_name == "EForm_eForm_SaveOther")
                return new BaseService<vEForm_eForm_SaveOther, vEForm_eForm_SaveOtherView>(configuration, uowProvider, mapper);
            else if (view_name == "PublicOther")
                return new BaseService<vPublicOther, vPublicOtherView>(configuration, uowProvider, mapper);
            else if (view_name == "PublicConsideringOther")
                return new BaseService<vPublicConsideringOther, vPublicConsideringOtherView>(configuration, uowProvider, mapper);
            else if (view_name == "SaveAddressRepresentative")
                return new BaseService<vSaveAddressRepresentative, vSaveAddressRepresentativeView>(configuration, uowProvider, mapper);
            
            throw new Exception("Not found view name");
        }
        protected bool validarCertificado(Object sender,
                              X509Certificate certificado,
                              X509Chain cadena,
                              SslPolicyErrors sslErrores)
        {
            return true;
        }
    }
}
