﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Checking;
using DIP.TM.Services.Interfaces;
using DIP.TM.Services.OneStopServices;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Web.Controllers {
    public class OneStopServiceProcessController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        private readonly ICheckingProcessService _checkingProcessService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public OneStopServiceProcessController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, ICheckingProcessService checkingProcessService) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _checkingProcessService = checkingProcessService;
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("List")]
        public IActionResult List([FromBody]PageRequest payload) {
            var service = new BaseService<vRecordRegistrationNumber, vRecordRegistrationNumberView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RecordRegistrationNumberSave")]
        public IActionResult RecordRegistrationNumberSave([FromBody]List<vRecordRegistrationNumberAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new OneStopServiceProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RecordRegistrationNumberSave(payload);
            return Json(new BaseResponseView<List<vRecordRegistrationNumberView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("Floor3DocumentLoad/{id}")]
        public IActionResult Floor3DocumentLoad(string id) {
            var payload = new PageRequest();

            var service = new BaseService<vFloor3Document, vFloor3DocumentView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel() {
                key = "request_number",
                values = id.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries).ToList()
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("Floor3RegistrarList")]
        public IActionResult Floor3RegistrarList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vUM_User, vUM_UserView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RecordRegistrationNumberAllowList")]
        public IActionResult RecordRegistrationNumberAllowList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vRecordRegistrationNumberAllow, vRecordRegistrationNumberAllowView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RecordRegistrationNumberAllowDone")]
        public IActionResult RecordRegistrationNumberAllowDone([FromBody]List<vRecordRegistrationNumberAllowAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new OneStopServiceProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RecordRegistrationNumberAllowDone(payload);
            return Json(new BaseResponseView<List<vRecordRegistrationNumberAllowView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RecordRegistrationNumberAllowCancel")]
        public IActionResult RecordRegistrationNumberAllowCancel([FromBody]List<vRecordRegistrationNumberAllowAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new OneStopServiceProcessService(_configuration, _uowProvider, _mapper);
            var result = service.RecordRegistrationNumberAllowCancel(payload);
            return Json(new BaseResponseView<List<vRecordRegistrationNumberAllowView>>() { data = result });
        }
    }
}
