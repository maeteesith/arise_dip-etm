﻿using DIP.TM.Services.Auths;
using DIP.TM.Services.Storages;
using DIP.TM.Web.Attributes;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DIP.TM.Web.Controllers
{

    public class AuthController : BaseApiController
    {
        private readonly AuthService authService;
        public AuthController(AuthService authService)
        {
            this.authService = authService;
        }
       

        [AllowAnonymous]
        [HttpGet]
        [Route("AuthCallBack")]
        [ServiceInfo("User", "AuthCallBack","")]
        public void AuthCallBack([FromServices]ICookieService _cookieService)
        {
            var obj_auth_code = HttpContext.Request.Query["auth_code"];
            var obj_last_action = HttpContext.Request.Query["last_action"];
            if (!ReferenceEquals(obj_auth_code, null))
            {
                var auth_code = obj_auth_code.ToString();
                var result = this.authService.VerifyAuthCode(auth_code);
                if(ReferenceEquals(result,null))
                    Response.Redirect("/login");
                else
                {
                    _cookieService.Set<string>("DIM-TM-TOKEN", result.bearer_token);
                    _cookieService.WriteToResponse(HttpContext);
                    RedirectRoute(obj_last_action.ToString());
                }
            }
            else
            {
                Response.Redirect("/login");
            }
        }
        /// <summary>
        /// Redirect after sso done
        /// </summary>
        /// <param name="last_action"></param>
        private void RedirectRoute(string last_action)
        {
            // TODO verify from client
            if (!string.IsNullOrWhiteSpace(last_action))
                if (last_action.ToLower().Contains("auth"))
                    Response.Redirect("/dashboard");

            if (string.IsNullOrWhiteSpace(last_action))
                Response.Redirect("/dashboard");
            else
                Response.Redirect(last_action);
        }
    }
}