﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.eForms;
using DIP.TM.Services.Pdf;
using DIP.TM.Services.Receipts;
using DIP.TM.Services.RequestChecks;
using DIP.TM.Services.Requests;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Utils.Logging;
using DIP.TM.Web.Extensions;
using DIP.TM.Web.Views.Pdf;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Rotativa.AspNetCore;
using Rotativa.AspNetCore.Options;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace DIP.TM.Web.Controllers {

    [AllowAnonymous]
    public class PdfController : BaseController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        private readonly RequestCheckService _requestCheckService;
        private readonly IHostingEnvironment _env;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public PdfController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, RequestCheckService requestCheckService, IHostingEnvironment env) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _requestCheckService = requestCheckService;
            _env = env;
            //bool isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
            //LineNotifyHelper.SendWait(configuration, "is windows >> "+ isWindows);
        }


        public IActionResult Index() {
            return new ViewAsPdf("Index");
        }

        //[HttpGet("pdf/RequestDocumentCollect/{name}/{id}", Name = "PDF Template")]
        //public IActionResult GetRequestDocumentCollectPDF(string name, string id) {
        //    //var model = new DIP.TM.Web.Views.Pdf.TM03Model();
        //    //model.test = "DDD";
        //    return new ViewAsPdf(name);
        //}

        [HttpGet("pdf/{name}", Name = "PDF Template")]
        public IActionResult GetPdfName(string name) {
            //var model = new DIP.TM.Web.Views.Pdf.TM03Model();
            //model.test = "DDD";

            return new ViewAsPdf(name);
        }

        [HttpPost("ViewPDF/{name}", Name = "PDF Template View")]
        public IActionResult ViewPDF([FromRoute] string name) {
            var payload = Request.Form["payload"];
            LineNotifyHelper.SendWait(_configuration, payload);

            if (payload != "") {
                if (name.StartsWith("TM") || name.StartsWith("tm")) {
                    switch (name) {
                        case "tm01":
                        case "TM01": {
                                var service = new PdfService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM01PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm02":
                        case "TM02": {
                                var service = new PdfService<eForm_Save020AddModel, eForm_Save020, eForm_Save020View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM02PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm021":
                        case "TM021": {
                                var service = new PdfService<eForm_Save021AddModel, eForm_Save021, eForm_Save021View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM021PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm03":
                        case "TM03": {
                                var service = new PdfService<eForm_Save030AddModel, eForm_Save030, eForm_Save030View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM03PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm04":
                        case "TM04": {
                                var service = new PdfService<eForm_Save040AddModel, eForm_Save040, eForm_Save040View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM04PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm05":
                        case "TM05": {
                                var service = new PdfService<eForm_Save050AddModel, eForm_Save050, eForm_Save050View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM05PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        //case "tm06":
                        //case "TM06":
                        //    {
                        //        var service = new PdfService<eForm_Save060AddModel, eForm_Save060, eForm_Save060View>(_configuration, _uowProvider, _mapper, _env);
                        //        var model = new TM06PdfModel()
                        //        {
                        //            view = service.ViewPDF(payload),
                        //        };

                        //        TempData[GuidString] = model;
                        //    }
                        case "tm07":
                        case "TM07": {
                                var service = new PdfService<eForm_Save070AddModel, eForm_Save070, eForm_Save070View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM07PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm08":
                        case "TM08": {
                                var service = new PdfService<eForm_Save080AddModel, eForm_Save080, eForm_Save080View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM08PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm10":
                        case "TM10": {
                                var service = new PdfService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM10PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_01":
                        case "TM11_01": {
                                var service = new PdfService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_01PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_02":
                        case "TM11_02": {
                                var service = new PdfService<eForm_Save020AddModel, eForm_Save020, eForm_Save020View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_02PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_021":
                        case "TM11_021": {
                                var service = new PdfService<eForm_Save021AddModel, eForm_Save021, eForm_Save021View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_021PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_03":
                        case "TM11_03": {
                                var service = new PdfService<eForm_Save030AddModel, eForm_Save030, eForm_Save030View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_03PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_04":
                        case "TM11_04": {
                                var service = new PdfService<eForm_Save040AddModel, eForm_Save040, eForm_Save040View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_04PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_05":
                        case "TM11_05": {
                                var service = new PdfService<eForm_Save050AddModel, eForm_Save050, eForm_Save050View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_05PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        //case "tm11_06":
                        //case "TM11_06":
                        //    {
                        //        var service = new PdfService<eForm_Save060AddModel, eForm_Save060, eForm_Save060View>(_configuration, _uowProvider, _mapper, _env);
                        //        var model = new TM11_06PdfModel()
                        //        {
                        //            view = service.ViewPDF(payload),
                        //        };

                        //        TempData[GuidString] = model;
                        //    }
                        case "tm11_07":
                        case "TM11_07": {
                                var service = new PdfService<eForm_Save070AddModel, eForm_Save070, eForm_Save070View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_07PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_08":
                        case "TM11_08": {
                                var service = new PdfService<eForm_Save080AddModel, eForm_Save080, eForm_Save080View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_08PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_10":
                        case "TM11_10": {
                                var service = new PdfService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_10PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_12":
                        case "TM11_12": {
                                var service = new PdfService<eForm_Save120AddModel, eForm_Save120, eForm_Save120View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_12PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_14":
                        case "TM11_14": {
                                var service = new PdfService<eForm_Save140AddModel, eForm_Save140, eForm_Save140View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_14PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_15":
                        case "TM11_15": {
                                var service = new PdfService<eForm_Save150AddModel, eForm_Save150, eForm_Save150View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_15PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_18":
                        case "TM11_18": {
                                var service = new PdfService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM11_18PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_19":
                        case "TM11_19": {
                                var service = new PdfService<eForm_Save190AddModel, eForm_Save190, eForm_Save190View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM19PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm11_20":
                        case "TM11_20": {
                                var service = new PdfService<eForm_Save200AddModel, eForm_Save200, eForm_Save200View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM20PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm12":
                        case "TM12": {
                                var service = new PdfService<eForm_Save120AddModel, eForm_Save120, eForm_Save120View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM12PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm14":
                        case "TM14": {
                                var service = new PdfService<eForm_Save140AddModel, eForm_Save140, eForm_Save140View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM14PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm15":
                        case "TM15": {
                                var service = new PdfService<eForm_Save150AddModel, eForm_Save150, eForm_Save150View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM15PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm17":
                        case "TM17": {
                                var service = new PdfService<eForm_Save040AddModel, eForm_Save040, eForm_Save040View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM17PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm18":
                        case "TM18": {
                                var service = new PdfService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM18PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm19":
                        case "TM19": {
                                var service = new PdfService<eForm_Save190AddModel, eForm_Save190, eForm_Save190View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM19PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                        case "tm20":
                        case "TM20": {
                                var service = new PdfService<eForm_Save200AddModel, eForm_Save200, eForm_Save200View>(_configuration, _uowProvider, _mapper, _env);
                                var model = new TM20PdfModel() {
                                    view = service.ViewPDF(payload),
                                };

                                return new ViewAsPdf(name, model);
                            }
                    }
                }
            }

            return null;
        }

        [HttpGet("pdf/{name}/{id}", Name = "PDF Template ID")]
        public IActionResult GetPdfNameId(string name, string id) {
            if (name == "Receipt01Reference") {
                var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
                var model = new Receipt01ReferencePdfModel() {
                    view = service.Request01LoadFromReferenceNumber(id),
                };
                return new ViewAsPdf(name, model);
            } else if (name == "ReceiptOtherReference") {
                var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
                var view = service.RequestOtherLoad(Convert.ToInt64(id));

                view.item_list = view.item_list.Where(r => r.total_price > 0).ToList();
                //foreach (var item in view.item_list) {
                //    item.tot

                //}

                var model = new ReceiptOtherReferencePdfModel() {
                    view = view,
                };
                return new ViewAsPdf(name, model);
                //} else if (name == "ReceiptEForm01Reference") {
                //var service = new RequestProcessService(_configuration, _uowProvider, _mapper);
                //var view = service.RequestOtherLoad(Convert.ToInt64(id));

                //view.item_list = view.item_list.Where(r => r.total_price > 0).ToList();
                ////foreach (var item in view.item_list) {
                ////    item.tot

                ////}

                //var model = new ReceiptOtherReferencePdfModel() {
                //    view = view,
                //};
                //return new ViewAsPdf(name, model);
            } else if (name == "ReceiptRequestCheckReference") {
                var service = new ReceiptService(_configuration, _uowProvider, _mapper);
                var model = new ReceiptRequestCheckReferencePdfModel() {
                    view = service.Load(new ReceiptAddModel() {
                        reference_number = id,
                    }),
                };
                return new ViewAsPdf(name, model);
            } else if (name == "Receipt") {
                var service = new ReceiptService(_configuration, _uowProvider, _mapper);
                var item_list = service.ItemList(new ReceiptAddModel() {
                    reference_number = id,
                });

                item_list = item_list.Where(r => r.total_price > 0).ToList();

                var model = new ReceiptPdfModel() {
                    item_list = item_list,
                };

                //return View(name, model);
                return new ViewAsPdf(name, model);
            } else if (name == "RequestCheckKor09") {
                //var service = new RequestCheckService(_configuration, _uowProvider, _mapper);
                var service = new ReceiptService(_configuration, _uowProvider, _mapper);

                var model = new RequestCheckKor09PdfModel() {
                    view = _requestCheckService.RequestCheckList(new RequestCheckAddModel() {
                        reference_number = id,
                    }).FirstOrDefault(),

                    view2 = service.Load(new ReceiptAddModel() {
                        reference_number = id,
                    }),
                };
                //return View(name, model);
                return new ViewAsPdf(name, model);
            } else if (name == "DocumentProcessClassification") {
                var model = new DocumentProcessClassificationPdfModel() {
                    view = GetSave010(id).FirstOrDefault()
                };
                return new ViewAsPdf(name, model);
            } else if (name == "ConsideringSimilaInstruction") {
                var id_list = id.Split(new string[] { "_", "|" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                var save_service = new BaseService<vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
                var save_page_request = new PageRequest() {
                    search_by = new List<SearchByModel>() {
                        new SearchByModel() {
                            key = "request_number",
                            value = id_list[0],
                        }
                    }
                };

                var save = save_service.List(save_page_request).data.list[0];

                var instruction_rule_service = new BaseService<vSave010InstructionRule, vSave010InstructionRuleView>(_configuration, _uowProvider, _mapper);
                var instruction_rule_page_request = new PageRequest() {
                    search_by = new List<SearchByModel>() {
                        new SearchByModel() {
                            key = "save_id",
                            value = save.id.Value.ToString(),
                        }
                    }
                };

                if (id_list.Count > 1) {
                    instruction_rule_page_request.search_by.Add(new SearchByModel() {
                        key = "id",
                        values = id_list.Skip(1).ToList(),
                    });
                }

                var model = new ConsideringSimilaInstructionPdfModel() {
                    save = save,
                    view = instruction_rule_service.List(instruction_rule_page_request).data.list,
                };
                return new ViewAsPdf(name, model);
            } else if (name == "Save010History") {
                var id_list = id.Split(new string[] { "_", "|" }, StringSplitOptions.RemoveEmptyEntries).ToList();

                var save_service = new BaseService<vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
                var save_page_request = new PageRequest() {
                    search_by = new List<SearchByModel>() {
                        new SearchByModel() {
                            key = "request_number",
                            value = id_list[0],
                        }
                    }
                };

                var save = save_service.List(save_page_request).data.list[0];

                var history_service = new BaseService<vSave010History, vSave010HistoryView>(_configuration, _uowProvider, _mapper);
                var history_page_request = new PageRequest() {
                    search_by = new List<SearchByModel>() {
                        new SearchByModel() {
                            key = "save_id",
                            value = save.id.Value.ToString(),
                        }
                    }
                };

                if (id_list.Count > 1) {
                    history_page_request.search_by.Add(new SearchByModel() {
                        key = "id",
                        values = id_list.Skip(1).ToList(),
                    });
                }

                var model = new Save010HistoryPdfModel() {
                    save = save,
                    view = history_service.List(history_page_request).data.list,
                };
                return new ViewAsPdf(name, model);
            } else if (name.StartsWith("TM") || name.StartsWith("tm")) {
                switch (name) {
                    case "tm01":
                    case "TM01": {

                            //var service = new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper);
                            //var model = new TM03PdfModel()
                            //{
                            //    view = service.Load(JsonConvert.SerializeObject(new eForm_Save010AddModel() { eform_number = id })).data,
                            //};
                            //return new ViewAsPdf(name, model);

                            var service = new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper);
                            var model = new TM01PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm02":
                    case "TM02": {
                            var service = new EFormProcessService<eForm_Save020AddModel, eForm_Save020, eForm_Save020View>(_configuration, _uowProvider, _mapper);
                            var model = new TM02PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm021":
                    case "TM021": {
                            var service = new EFormProcessService<eForm_Save021AddModel, eForm_Save021, eForm_Save021View>(_configuration, _uowProvider, _mapper);
                            var model = new TM021PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm03":
                    case "TM03": {
                            var service = new EFormProcessService<eForm_Save030AddModel, eForm_Save030, eForm_Save030View>(_configuration, _uowProvider, _mapper);
                            var model = new TM03PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm04":
                    case "TM04": {
                            var service = new EFormProcessService<eForm_Save040AddModel, eForm_Save040, eForm_Save040View>(_configuration, _uowProvider, _mapper);
                            var model = new TM04PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm05":
                    case "TM05": {
                            var service = new EFormProcessService<eForm_Save050AddModel, eForm_Save050, eForm_Save050View>(_configuration, _uowProvider, _mapper);
                            var model = new TM05PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm07":
                    case "TM07": {
                            var service = new EFormProcessService<eForm_Save070AddModel, eForm_Save070, eForm_Save070View>(_configuration, _uowProvider, _mapper);
                            var model = new TM07PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm08":
                    case "TM08": {
                            var service = new EFormProcessService<eForm_Save080AddModel, eForm_Save080, eForm_Save080View>(_configuration, _uowProvider, _mapper);
                            var model = new TM08PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm10":
                    case "TM10": {
                            var service = new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper);
                            var model = new TM10PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_01":
                    case "TM11_01": {
                            var service = new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_01PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_02":
                    case "TM11_02": {
                            var service = new EFormProcessService<eForm_Save020AddModel, eForm_Save020, eForm_Save020View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_02PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_021":
                    case "TM11_021": {
                            var service = new EFormProcessService<eForm_Save021AddModel, eForm_Save021, eForm_Save021View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_021PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_03":
                    case "TM11_03": {
                            var service = new EFormProcessService<eForm_Save030AddModel, eForm_Save030, eForm_Save030View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_03PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_04":
                    case "TM11_04": {
                            var service = new EFormProcessService<eForm_Save040AddModel, eForm_Save040, eForm_Save040View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_04PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_05":
                    case "TM11_05": {
                            var service = new EFormProcessService<eForm_Save050AddModel, eForm_Save050, eForm_Save050View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_05PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    //case "tm11_06":
                    //case "TM11_06":
                    //    {
                    //        var service = new EFormProcessService<eForm_Save060AddModel, eForm_Save060, eForm_Save060View>(_configuration, _uowProvider, _mapper);
                    //        var model = new TM11_06PdfModel()
                    //        {
                    //            view = service.Load(id).data,
                    //        };

                    //        TempData[GuidString] = model;
                    //    }
                    case "tm11_07":
                    case "TM11_07": {
                            var service = new EFormProcessService<eForm_Save070AddModel, eForm_Save070, eForm_Save070View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_07PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_08":
                    case "TM11_08": {
                            var service = new EFormProcessService<eForm_Save080AddModel, eForm_Save080, eForm_Save080View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_08PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_10":
                    case "TM11_10": {
                            var service = new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_10PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_12":
                    case "TM11_12": {
                            var service = new EFormProcessService<eForm_Save120AddModel, eForm_Save120, eForm_Save120View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_12PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_14":
                    case "TM11_14": {
                            var service = new EFormProcessService<eForm_Save140AddModel, eForm_Save140, eForm_Save140View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_14PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_15":
                    case "TM11_15": {
                            var service = new EFormProcessService<eForm_Save150AddModel, eForm_Save150, eForm_Save150View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_15PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_18":
                    case "TM11_18": {
                            var service = new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_18PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_19":
                    case "TM11_19": {
                            var service = new EFormProcessService<eForm_Save190AddModel, eForm_Save190, eForm_Save190View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_19PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm11_20":
                    case "TM11_20": {
                            var service = new EFormProcessService<eForm_Save200AddModel, eForm_Save200, eForm_Save200View>(_configuration, _uowProvider, _mapper);
                            var model = new TM11_20PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm12":
                    case "TM12": {
                            var service = new EFormProcessService<eForm_Save120AddModel, eForm_Save120, eForm_Save120View>(_configuration, _uowProvider, _mapper);
                            var model = new TM12PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm14":
                    case "TM14": {
                            var service = new EFormProcessService<eForm_Save140AddModel, eForm_Save140, eForm_Save140View>(_configuration, _uowProvider, _mapper);
                            var model = new TM14PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm15":
                    case "TM15": {
                            var service = new EFormProcessService<eForm_Save150AddModel, eForm_Save150, eForm_Save150View>(_configuration, _uowProvider, _mapper);
                            var model = new TM15PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm17":
                    case "TM17": {
                            var service = new EFormProcessService<eForm_Save040AddModel, eForm_Save040, eForm_Save040View>(_configuration, _uowProvider, _mapper);
                            var model = new TM17PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm18":
                    case "TM18": {
                            var service = new EFormProcessService<eForm_Save010AddModel, eForm_Save010, eForm_Save010View>(_configuration, _uowProvider, _mapper);
                            var model = new TM18PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm19":
                    case "TM19": {
                            var service = new EFormProcessService<eForm_Save190AddModel, eForm_Save190, eForm_Save190View>(_configuration, _uowProvider, _mapper);
                            var model = new TM19PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                    case "tm20":
                    case "TM20": {
                            var service = new EFormProcessService<eForm_Save200AddModel, eForm_Save200, eForm_Save200View>(_configuration, _uowProvider, _mapper);
                            var model = new TM20PdfModel() {
                                view = service.Load(id).data,
                            };

                            return new ViewAsPdf(name, model);
                        }
                }
            } else if (name.StartsWith("KorMor")) {
                switch (name) {
                    case "KorMor01":
                    case "KorMor01_Product":
                    case "KorMor01_JoinerName":
                    case "KorMor01_Description":
                    case "KorMor01_Owner":
                    case "KorMor01_ALL":
                    case "KorMor02":
                    case "KorMor03":
                    case "KorMor04": {
                            var is_show_01 = true;
                            var is_show_02 = name == "KorMor01_Product" || name == "KorMor01_ALL";
                            var is_show_03 = name == "KorMor01_JoinerName" || name == "KorMor01_ALL";
                            var is_show_04 = name == "KorMor01_Description" || name == "KorMor01_ALL";
                            var is_show_05 = name == "KorMor01_Owner" || name == "KorMor01_ALL";
                            var is_representative = name == "KorMor04" || name == "KorMor01_ALL";

                            if (is_show_02 || is_show_03 || is_show_04 || is_show_05) {
                                is_show_01 = false;
                            }

                            var param = id.Split(new string[] { "&" }, StringSplitOptions.RemoveEmptyEntries);
                            var model = new KorMor01PdfModel() {
                                view_list = GetSave010(param[0]),
                                is_show_01 = is_show_01,
                                is_show_02 = is_show_02,
                                is_show_03 = is_show_03,
                                is_show_04 = is_show_04,
                                is_show_05 = is_show_05,
                                public05_registrar_by_name = param.Length > 1 ? param[1] : "",
                                is_representative = is_representative,
                            };
                            return new ViewAsPdf("KorMor01", model);
                        }
                    case "KorMor002": {
                            var model = new KorMor002PdfModel() {
                                view = GetSave010(id)
                            };
                            return new ViewAsPdf(name, model);
                        }
                }
            } else if (name.StartsWith("Kor09")) {
                switch (name) {
                    case "Kor09CopyDocument": {
                            var model = new Kor09CopyDocumentPdfModel() {
                                view = GetSave010(id)
                            };
                            return new ViewAsPdf(name, model);
                        }
                    case "Kor09RegisterForm": {
                            var model = new Kor09RegisterFormPdfModel() {
                                view_list = GetSave010(id)
                            };
                            //return View(name, model);
                            return new ViewAsPdf(name, model);
                        }
                    default:
                        return new ViewAsPdf(name) {
                            CustomSwitches = "--page-offset 0 --footer-right \"[page]/[topage]\" --footer-font-size 8"
                        };
                }
            } else if (name == "TorKor") {
                var _post_round_id = id.Split('_')[0].Split('|');

                var service = new BaseService<vDocumentRole02, vDocumentRole02View>(_configuration, _uowProvider, _mapper);
                var page_request = new PageRequest() {
                    search_by = new List<SearchByModel>() {
                         new SearchByModel() {
                             key = "id",
                             values = _post_round_id.ToList(),
                         }
                     }
                };

                if (id.Split('_').Length == 2) {
                    page_request.search_by.Add(new SearchByModel() {
                        key = "instruction_rule_id",
                        values = id.Split('_')[1].Split("|").ToList(),
                    });
                }

                var view = service.List(page_request);

                var model = new TorKorPdfModel() {
                    view = view.data.list,
                };

                foreach (var list in view.data.list) {
                    if (list.instruction_rule_code == "RULE_5_3" ||
                        list.instruction_rule_code == PostRoundInstructionRuleCodeConstant.RULE_5_8_FEE.ToString()) {
                        model.reference_number = list.reference_number;
                        model.barcode = BarcodeHelper.Generate(model.reference_number);
                    }
                }

                //return View(name, model);
                return new ViewAsPdf(name, model);
            } else if (name.StartsWith("RequestPrintCoverTYPE")) {
                var request_list = id.Split(new string[] { "_" }, StringSplitOptions.RemoveEmptyEntries);
                var service = new BaseService<Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
                var result = service.List(new PageRequest() {
                    search_by = new List<SearchByModel>() {
                         new SearchByModel() {
                             key = "request_number",
                             value = request_list[0],
                             operation = Operation.GreaterThanOrEqual,
                         },
                         new SearchByModel() {
                             key = "request_number",
                             value = request_list[1],
                             operation = Operation.LessThanOrEqual,
                         }
                     }
                });
                switch (name) {
                    case "RequestPrintCoverTYPE01": {
                            var model = new RequestPrintCoverTYPE01PdfModel() {
                                view = result.data.list,
                            };
                            return new ViewAsPdf(name, model) {
                                PageOrientation = Orientation.Landscape,
                                PageSize = Size.A4
                            };
                        }
                    case "RequestPrintCoverTYPE02": {
                            var model = new RequestPrintCoverTYPE02PdfModel() {
                                view = result.data.list,
                            };
                            //return View(name, model);
                            return new ViewAsPdf(name, model) {
                                //PageOrientation = Orientation.Landscape,
                                PageSize = Size.A4
                            };
                        }
                    case "RequestPrintCoverTYPE03":
                    case "RequestPrintCoverTYPE04": {
                            var model = new RequestPrintCoverTYPE03PdfModel() {
                                view = result.data.list,
                            };
                            //return View(name, model);
                            return new ViewAsPdf("RequestPrintCoverTYPE03", model) {
                                //PageOrientation = Orientation.Landscape,
                                PageSize = Size.A4
                            };
                        }
                    //case "RequestPrintCoverTYPE03": {
                    //        //var model = new RequestPrintCoverTYPE03PdfModel() {
                    //        //    view = result.data.list,
                    //        //};
                    //        //return new ViewAsPdf(name, model) {
                    //        //    PageOrientation = Orientation.Landscape,
                    //        //    PageSize = Size.A4
                    //        //};
                    //    }
                    default: {
                            return new ViewAsPdf(name) {
                                CustomSwitches = "--page-offset 0 --footer-right \"[page]/[topage]\" --footer-font-size 8"
                            };
                        }
                }
            } else if (name == "CheckingSimilarRequestItemSubTypeReport") {
                var service = new BaseService<vCheckingSimilarRequestItemSubTypeReport, vCheckingSimilarRequestItemSubTypeReportView>(_configuration, _uowProvider, _mapper);
                var model = new CheckingSimilarRequestItemSubTypeReportPdfModel() {
                    view_list = service.List(new PageRequest() {
                        search_by = new List<SearchByModel>() {
                            new SearchByModel() {
                                 key = "request_number",
                                 values = id.Split('|').ToList(),
                            }
                        }
                    }).data.list
                };
                //return View(name, model);
                return new ViewAsPdf(name, model);
            } else if (name == "PostCover") {
                var service = new BaseService<vDocumentRole02Group, vDocumentRole02GroupView>(_configuration, _uowProvider, _mapper);
                var model = new PostCoverPdfModel() {
                    view_list = service.List(new PageRequest() {
                        search_by = new List<SearchByModel>() {
                            new SearchByModel() {
                                 key = "id",
                                 values = id.Split('|').ToList(),
                            }
                        }
                    }).data.list
                };
                //return View(name, model);
                return new ViewAsPdf(name, model) {
                    PageWidth = 297,
                    PageHeight = 150,
                };
            } else if (name.StartsWith("PublicRoundItem")) {
                var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
                var model = new PublicRoundItemPdfModel() {
                    save = service.Get(Convert.ToInt64(id))
                };
                return new ViewAsPdf(name, model);
            } else if (name == "PublicRole01Check") {
                var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
                var rdc_service = new RequestDocumentCollectService(_configuration, _uowProvider, _mapper);
                var save = service.Get(Convert.ToInt64(id));
                save.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(save.id.Value);
                var model = new PublicRole01CheckPdfModel() {
                    save = save,
                };
                return new ViewAsPdf(name, model);
            } else {
                return new ViewAsPdf(name);
            }
            return null;
        }

        List<Save010ProcessView> GetSave010(string id) {
            var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
            var rdc_service = new RequestDocumentCollectService(_configuration, _uowProvider, _mapper);

            var view_list = new List<Save010ProcessView>();
            var id_list = id.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries);

            using (var uow = _uowProvider.CreateUnitOfWork()) {
                var nationality_repo = uow.GetRepository<RM_AddressNationality>();
                var career_repo = uow.GetRepository<RM_AddressCareer>();
                var user_repo = uow.GetRepository<UM_User>();
                var public_register_repo = uow.GetRepository<vPublicRegister>();

                foreach (var _id in id_list) {
                    var view = service.List(new Save010AddModel() { request_number = _id }).FirstOrDefault();

                    if (view.people_list != null) {
                        foreach (var people in view.people_list) {
                            if (!string.IsNullOrEmpty(people.nationality_code)) {
                                people.nationality_name = nationality_repo.Query(r => r.code == people.nationality_code).FirstOrDefault().name;
                            }
                            if (!string.IsNullOrEmpty(people.career_code)) {
                                people.career_name = career_repo.Query(r => r.code == people.career_code).FirstOrDefault().name;
                            }
                        }
                    }
                    if (view.contact_address != null) {
                        if (!string.IsNullOrEmpty(view.contact_address.nationality_code)) {
                            view.contact_address.nationality_name = nationality_repo.Query(r => r.code == view.contact_address.nationality_code).FirstOrDefault().name;
                        }
                        if (!string.IsNullOrEmpty(view.contact_address.career_code)) {
                            view.contact_address.career_name = career_repo.Query(r => r.code == view.contact_address.career_code).FirstOrDefault().name;
                        }
                    }

                    if (view.representative_list != null) {
                        foreach (var representative in view.representative_list) {
                            if (!string.IsNullOrEmpty(representative.nationality_code)) {
                                representative.nationality_name = nationality_repo.Query(r => r.code == representative.nationality_code).FirstOrDefault().name;
                            }
                            if (!string.IsNullOrEmpty(representative.career_code)) {
                                representative.career_name = career_repo.Query(r => r.code == representative.career_code).FirstOrDefault().name;
                            }
                        }
                    }


                    var public_register = public_register_repo.Query(r => r.id == view.id.Value).FirstOrDefault();
                    if (public_register != null) view.public_register = _mapper.Map<vPublicRegisterView>(public_register);

                    view.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(view.id.Value);

                    var create_by = user_repo.Get(view.created_by);
                    if (create_by != null) view.created_by_name = user_repo.Get(view.created_by).name;

                    if (view.considering_receiver_by.HasValue) {
                        var considering_receiver_by = user_repo.Get(view.considering_receiver_by.Value);
                        if (considering_receiver_by != null) view.considering_receiver_by_name = user_repo.Get(view.considering_receiver_by.Value).name;
                    }

                    foreach (var instruction_rule in view.instruction_rule_list) {
                        instruction_rule.created_by_name = user_repo.Get(instruction_rule.created_by).name;
                    }

                    view_list.Add(view);
                }

                return view_list;
            }
        }

        [HttpGet("pdf/{name}/{id1}/{id2}/{id3}", Name = "PDF Online")]
        public IActionResult GetPdfOnline(string name, string id1, string id2, string id3) {
            if (name == "OnlinePublicRoundItem") {
                using (var uow = _uowProvider.CreateUnitOfWork()) {
                    var repo = uow.GetRepository<vOnlinePublicRoundItem>();
                    var pri = repo.Query(r => r.pr_id == Convert.ToInt64(id1) && r.request_item_sub_type_1_code == id2 && r.line_index == Convert.ToInt64(id3)).FirstOrDefault();
                    if (pri == null) return null;

                    var service = new SaveProcessService<Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
                    var save = service.Get(pri.save_id.Value);
                    if (save == null) return null;

                    var rdc_service = new RequestDocumentCollectService(_configuration, _uowProvider, _mapper);
                    save.trademark_2d_physical_path = rdc_service.GetTrademark2DPhysicalPath(pri.save_id.Value);

                    if (save.people_list != null && save.people_list.Count() > 0) {
                        if (!string.IsNullOrEmpty(save.people_list.FirstOrDefault().career_code)) {
                            var career_repo = uow.GetRepository<RM_AddressCareer>();
                            save.people_list.FirstOrDefault().career_name = career_repo.Query(r => r.code == save.people_list.FirstOrDefault().career_code).FirstOrDefault().name;
                        }
                        if (!string.IsNullOrEmpty(save.people_list.FirstOrDefault().nationality_code)) {
                            var nationality_repo = uow.GetRepository<RM_AddressNationality>();
                            save.people_list.FirstOrDefault().nationality_name = nationality_repo.Query(r => r.code == save.people_list.FirstOrDefault().nationality_code).FirstOrDefault().name;
                        }
                    }
                    if (save.representative_list != null && save.representative_list.Count() > 0) {
                        if (!string.IsNullOrEmpty(save.representative_list.FirstOrDefault().career_code)) {
                            var career_repo = uow.GetRepository<RM_AddressCareer>();
                            save.representative_list.FirstOrDefault().career_name = career_repo.Query(r => r.code == save.representative_list.FirstOrDefault().career_code).FirstOrDefault().name;
                        }
                        if (!string.IsNullOrEmpty(save.representative_list.FirstOrDefault().nationality_code)) {
                            var nationality_repo = uow.GetRepository<RM_AddressNationality>();
                            save.representative_list.FirstOrDefault().nationality_name = nationality_repo.Query(r => r.code == save.representative_list.FirstOrDefault().nationality_code).FirstOrDefault().name;
                        }
                    }

                    var model = new OnlinePublicRoundItemPdfModel() {
                        public_round_item = _mapper.Map<vOnlinePublicRoundItemView>(pri),
                        save = save,
                    };
                    //return View(name, model);
                    return new ViewAsPdf(name, model);
                }
            }
            return null;
        }

        [HttpGet("pdfsigner_add", Name = "Pdf Signer - Add")]
        public IActionResult PdfSignerAdd(string name) {
            string formdataTemplate = "Content-Disposition: form-data; filename=\"{0}\";\r\nContent-Type: image/png\r\n\r\n";
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://localhost/iisapi/api/data/UploadFile");
            request.ServicePoint.Expect100Continue = false;
            request.Method = "POST";
            request.ContentType = "multipart/form-data; boundary=" + boundary;

            string filePath = "d:/274.png";

            using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read)) {
                using (Stream requestStream = request.GetRequestStream()) {
                    requestStream.Write(boundarybytes, 0, boundarybytes.Length);
                    string formitem = string.Format(formdataTemplate, "UploadedImage");
                    byte[] formbytes = Encoding.UTF8.GetBytes(formitem);
                    requestStream.Write(formbytes, 0, formbytes.Length);
                    byte[] buffer = new byte[fileStream.Length];
                    int bytesLeft = 0;

                    while ((bytesLeft = fileStream.Read(buffer, 0, buffer.Length)) > 0) {
                        requestStream.Write(buffer, 0, bytesLeft);
                    }

                }
            }

            try {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse()) {
                    var _stream = response.GetResponseStream();
                    byte[] _byte_read_list = new byte[_stream.Length];
                    _stream.Read(_byte_read_list, 0, _byte_read_list.Length);
                    FileStream _file = new FileStream("d:/aa.jpg", FileMode.Create);
                    _file.Write(_byte_read_list, 0, _byte_read_list.Length);
                    _file.Close();
                    _stream.Close();
                }


                return Ok("SUCCESS");
            } catch (Exception ex) {
                throw;
            }

            return new ViewAsPdf(name);
        }
    }
}