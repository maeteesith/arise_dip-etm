﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Interfaces;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Web.Controllers {

    public class CheckingProcessController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        private readonly ICheckingProcessService _checkingProcessService;

        Dictionary<string, dynamic> service_list = new Dictionary<string, dynamic>();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public CheckingProcessController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, ICheckingProcessService checkingProcessService) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _checkingProcessService = checkingProcessService;

            service_list.Add("WordFirst", new BaseService<RM_Save010DocumentClassificationWordFirst, ReferenceMasterView>(_configuration, _uowProvider, _mapper));
            service_list.Add("SoundLast", new BaseService<RM_Save010DocumentClassificationSoundLast, ReferenceMasterView>(_configuration, _uowProvider, _mapper));
            service_list.Add("SoundLastOther", new BaseService<RM_Save010DocumentClassificationSoundLastOther, ReferenceMasterView>(_configuration, _uowProvider, _mapper));
            service_list.Add("ImageType", new BaseService<RM_Save010DocumentClassificationImageType, ReferenceMasterView>(_configuration, _uowProvider, _mapper));
            service_list.Add("SoundType", new BaseService<RM_Save010DocumentClassificationSoundType, ReferenceMasterView>(_configuration, _uowProvider, _mapper));

            service_list.Add("vSave010CheckingSaveDocument", new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(_configuration, _uowProvider, _mapper));
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("{topic}/List")]
        public IActionResult List([FromBody]PageRequest payload, string topic) {
            var service = service_list[topic];
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CheckingItemList")]
        public IActionResult CheckingItemList([FromBody]PageRequest payload) {
            //if (!ModelState.IsValid)
            //return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingItemList(payload);
            return Json(result);
        }

        //[ProducesResponseType(typeof(BaseResponsePageView<List<vUM_UserView>>), 200)]
        //[HttpPost]
        //[Route("CheckingItemCheckerList")]
        //public IActionResult CheckingItemCheckerList([FromBody]PageRequest payload) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
        //    var result = service.CheckingItemCheckerList(payload);
        //    return Json(result);
        //}

        [ProducesResponseType(typeof(BaseResponsePageView<List<vUM_UserView>>), 200)]
        [HttpPost]
        [Route("CheckingItemCheckerList")]
        public IActionResult CheckingItemCheckerList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vUM_User, vUM_UserView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //    key = "register_post_round_instruction_rule_code",
            //    value = PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString(),
            //});
            var result = service.List(payload);
            return Json(result);
        }




        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CheckingItemAutoSplit")]
        public IActionResult CheckingItemAutoSplit() {
            var result = _checkingProcessService.CheckingItemAutoSplit();
            return Json(new BaseResponseView<List<Save010ProcessView>>() { data = result });
        }


        //[ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        //[HttpPost]
        //[Route("CheckingItemCheckerList")]
        //public IActionResult CheckingItemCheckerList([FromBody]BasePageModelPayload<vUM_UserAddModel> payload)
        //{
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
        //    var result = service.CheckingItemCheckerList(payload);
        //    return Json(new BaseResponseView<List<vUM_UserView>>() { data = result });
        //}


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("CheckingSimilarWordSoundTranslateSave")]
        public IActionResult CheckingSimilarWordSoundTranslateSave([FromBody]Save010AddModel payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarWordSoundTranslateSave(payload);
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CheckingItemReceive")]
        public IActionResult CheckingItemReceive([FromBody]vSave010Model payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingItemReceive(payload);
            return Json(new BaseResponseView<List<vSave010View>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CheckingSimilarList")]
        public IActionResult CheckingSimilarList([FromBody]PageRequest payload) {
            //if (!ModelState.IsValid)
            //return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarList(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("CheckingSimilarLoad/{id}")]
        public IActionResult CheckingSimilarLoad(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessController(_configuration, _uowProvider, _mapper);
            return service.Save010FullLoad(id);
        }

        [ProducesResponseType(typeof(BaseResponseView<string>), 200)]
        [HttpPost]
        [Route("CheckingSimilarGetRequestItemSubTypeGroup/{item_sub_type_list}")]
        public IActionResult CheckingSimilarGetRequestItemSubTypeGroup(string item_sub_type_list) {
            var result = _checkingProcessService.CheckingSimilarGetRequestItemSubTypeGroup(item_sub_type_list);
            return Json(new BaseResponseView<string>() { data = result });
        }



        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("CheckingSimilarWordLoad/{id}")]
        public IActionResult CheckingSimilarWordLoad(string id) {
            //if (!ModelState.IsValid)
            //return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarLoad(Convert.ToInt64(id));
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("CheckingSimilarImageLoad/{id}")]
        public IActionResult CheckingSimilarImageLoad(string id) {
            //if (!ModelState.IsValid)
            //return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarLoad(Convert.ToInt64(id));
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<bool>), 200)]
        [HttpPost]
        [Route("CheckingSimilarTagAdd")]
        public IActionResult CheckingSimilarTagAdd([FromBody]object[] payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarTagAdd(payload);
            return Json(new BaseResponseView<bool>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<bool>), 200)]
        [HttpPost]
        [Route("CheckingSimilarTagRemove")]
        public IActionResult CheckingSimilarTagRemove([FromBody]List<vSave010CheckingTagSimilarAddModel> payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarTagRemove(payload);
            return Json(new BaseResponseView<bool>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CheckingSimilarTagList/{id}")]
        public IActionResult CheckingSimilarTagList(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarTagList(Convert.ToInt64(id));
            return Json(new BaseResponseView<List<vSave010CheckingTagSimilarView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<bool>), 200)]
        [HttpPost]
        [Route("CheckingSimilarResultSend")]
        public IActionResult CheckingSimilarResultSend([FromBody]vSave010Model payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarResultSend(payload);
            return Json(new BaseResponseView<bool>() { data = result });
        }

        //[ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        //[HttpPost]
        //[Route("ConsideringSimilarList")]
        //public IActionResult ConsideringSimilarList([FromBody]PageRequest payload) {
        //    var result = _checkingProcessService.ConsideringSimilarList(payload);
        //    return Json(new BaseResponseView<List<vSave010View>>() { data = result });
        //}

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarListPage")]
        public IActionResult ConsideringSimilarListPage([FromBody]PageRequest payload) {
            if (payload.filter_queries == null) payload.filter_queries = new List<string>();


            payload.filter_queries.Add("!string.IsNullOrEmpty(considering_receive_status_code)");
            //payload.search_by.Add(new SearchByModel() {
            //    key = "considering_receive_status_code",
            //    values = new List<string>() {
            //        ConsideringReceivingStatusCodeConstant.WAIT_CHANGE.ToString(),
            //        ConsideringReceivingStatusCodeConstant.WAIT_DONE.ToString(),
            //        ConsideringReceivingStatusCodeConstant.SEND_CHANGE.ToString(),
            //        ConsideringReceivingStatusCodeConstant.DONE.ToString(),
            //    }
            //});

            var result = _checkingProcessService.ConsideringSimilarListPage(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarDashboard")]
        public IActionResult ConsideringSimilarDashboard([FromBody]PageRequest payload) {
            var service = new BaseService<vDashboardConsidering, vDashboardConsideringView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarLoad/{id}")]
        public IActionResult ConsideringSimilarLoad(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarLoad(Convert.ToInt64(id));
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarSave")]
        public IActionResult ConsideringSimilarSave([FromBody]List<vSave010CheckingTagSimilarAddModel> payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarSave(payload);
            return Json(new BaseResponseView<List<vSave010CheckingTagSimilarView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ItemSubType1SuggestionLoad/{id}")]
        public IActionResult ItemSubType1SuggestionLoad(string id) {
            // if (!ModelState.IsValid)
            //     return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ItemSubType1SuggestionLoad(Convert.ToInt32(id));
            return Json(new BaseResponseView<List<ItemSubType1SuggestionView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CheckingSimilarSave010List")]
        public IActionResult CheckingSimilarSave010List([FromBody]CheckingSimilarSave010ListModel payload) {
            var result = _checkingProcessService.CheckingSimilarSave010List(payload);
            //if (payload.is_have_image) result = result.Where(r => !string.IsNullOrEmpty(r.file_trademark_2d)).ToList();
            //if (payload.is_have_sound) result = result.Where(r => r.sound_file_id.HasValue && r.sound_file_id > 0).ToList();
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CheckingSimilarStatisticList")]
        public IActionResult CheckingSimilarStatisticList() {
            var result = _checkingProcessService.CheckingSimilarStatisticList();
            return Json(new BaseResponseView<List<StatisticView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("CheckingTagSimilarMethod")]
        public IActionResult CheckingTagSimilarMethod([FromBody]CheckingSimilarSave010ListModel payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingTagSimilarMethod(payload);
            return Json(new BaseResponseView<Save010CheckingTagSimilarMethodView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CheckingSimilarResultOwnerSame")]
        public IActionResult CheckingSimilarResultOwnerSame([FromBody]List<vSave010CheckingTagSimilarAddModel> payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.CheckingSimilarResultOwnerSame(payload);
            return Json(new BaseResponseView<List<vSave010CheckingTagSimilarView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionLoad/{id}")]
        public IActionResult ConsideringSimilarInstructionLoad(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarInstructionLoad(Convert.ToInt64(id));
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionSave")]
        public IActionResult ConsideringSimilarInstructionSave([FromBody]vSave010Model payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarInstructionSave(payload);
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionRuleSave")]
        public IActionResult ConsideringSimilarInstructionRuleSave([FromBody]Save010InstructionRuleAddModel payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarInstructionRuleSave(payload);
            result.full_view.instruction_rule_list = result.full_view.instruction_rule_list.OrderByDescending(r => r.id).ToList();
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionList")]
        public IActionResult ConsideringSimilarInstructionList([FromBody]PageRequest payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarInstructionList(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionDelete")]
        public IActionResult ConsideringSimilarInstructionDelete([FromBody]Save010InstructionRuleAddModel payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarInstructionDelete(payload);
            result.full_view.instruction_rule_list = result.full_view.instruction_rule_list.OrderByDescending(r => r.id).ToList();
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionPublicAdd/{id}")]
        public IActionResult ConsideringSimilarInstructionPublicAdd(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarInstructionPublicAdd(Convert.ToInt64(id));
            result.full_view.instruction_rule_list = result.full_view.instruction_rule_list.OrderByDescending(r => r.id).ToList();
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionPublicDelete/{id}")]
        public IActionResult ConsideringSimilarInstructionPublicDelete(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarInstructionPublicDelete(Convert.ToInt64(id));
            result.full_view.instruction_rule_list = result.full_view.instruction_rule_list.OrderByDescending(r => r.id).ToList();
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionSend/{id}")]
        public IActionResult ConsideringSimilarInstructionSend(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarInstructionSend(Convert.ToInt64(id));
            result.full_view.instruction_rule_list = result.full_view.instruction_rule_list.OrderByDescending(r => r.id).ToList();
            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarDocumentLoad/{id}")]
        public IActionResult ConsideringSimilarDocumentLoad(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new CheckingProcessService(_configuration, _uowProvider, _mapper);
            var result = _checkingProcessService.ConsideringSimilarDocumentLoad(Convert.ToInt64(id));
            return Json(new BaseResponseView<ConsideringSimilarDocumentView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vUM_UserView>>), 200)]
        [HttpPost]
        [Route("ItemSubType1SuggestionList")]
        public IActionResult ItemSubType1SuggestionList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<RM_RequestItemSubType1, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarInstructionRequestNumberAdd")]
        public IActionResult ConsideringSimilarInstructionRequestNumberAdd([FromBody]Save010InstructionRuleRequestNumberAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
            var view = service.List(new vSave010Model() {
                request_number = payload.request_number,
            });
            return Json(new BaseResponseView<vSave010View>() { data = view.FirstOrDefault() });
        }



        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarDocumentSave")]
        public IActionResult ConsideringSimilarDocumentSave([FromBody]ConsideringSimilarDocumentAddModel payload) {
            var result = _checkingProcessService.ConsideringSimilarDocumentSave(payload);
            return Json(new BaseResponseView<ConsideringSimilarDocumentView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("ClassificationListWordFirst")]
        public IActionResult ClassificationListWordFirst([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();
            var service = new BaseService<RM_Save010DocumentClassificationWordFirst, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("ClassificationListSoundLastOther")]
        public IActionResult ClassificationListSoundLastOther([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();
            var service = new BaseService<RM_Save010DocumentClassificationSoundLastOther, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("CheckingSimilarResultDuplicateList/{request_number}")]
        public IActionResult CheckingSimilarResultDuplicateList(string request_number) {
            var result = _checkingProcessService.CheckingSimilarResultDuplicateList(request_number);
            return Json(new BaseResponseView<CheckingSimilarResultDuplicateView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("CheckingSimilarResultDuplicateAdd")]
        public IActionResult CheckingSimilarResultDuplicateAdd([FromBody]CheckingSimilarResultDuplicateAddModel payload) {
            var result = _checkingProcessService.CheckingSimilarResultDuplicateAdd(payload);
            return Json(new BaseResponseView<CheckingSimilarResultDuplicateView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarKor10Load/{id}")]
        public IActionResult ConsideringSimilarKor10Load(string id) {
            var result = _checkingProcessService.ConsideringSimilarKor10Load(Convert.ToInt64(id));
            return Json(new BaseResponseView<Save010Case28ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("ConsideringSimilarKor10Save")]
        public IActionResult ConsideringSimilarKor10Save([FromBody]Save010Case28ProcessAddModel payload) {
            var result = _checkingProcessService.ConsideringSimilarKor10Save(payload);
            return Json(new BaseResponseView<Save010Case28ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("CheckingSimilarWordSoundTranslateBackSend")]
        public IActionResult CheckingSimilarWordSoundTranslateBackSend([FromBody]Save010AddModel payload) {
            var result = _checkingProcessService.CheckingSimilarWordSoundTranslateBackSend(payload);
            return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        }

    }
}
