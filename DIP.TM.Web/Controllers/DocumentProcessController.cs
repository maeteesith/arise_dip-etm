﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIP.TM.Web.Controllers
{
    public class DocumentProcessController : BaseApiController
    {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        private readonly RequestDocumentCollectService _requestDocumentCollectService;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public DocumentProcessController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, RequestDocumentCollectService requestDocumentCollectService)
        {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _requestDocumentCollectService = requestDocumentCollectService;
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        [HttpPost]
        [Route("DocumentItemDocumentPeopleList")]
        public IActionResult DocumentItemDocumentPeopleList([FromBody] BasePageModelPayload<vUM_UserAddModel> payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentItemDocumentPeopleList(payload);
            return Json(new BaseResponseView<List<vUM_UserView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseSaveView>>), 200)]
        [HttpPost]
        [Route("DocumentItemList")]
        public IActionResult DocumentItemList([FromBody] PageRequest payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentItem, vDocumentItemView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);

            //var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            //var result = service.DocumentItemList(payload);
            //return Json(new BaseResponseView<List<vDocumentItemView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("DocumentItemReceive")]
        public IActionResult DocumentItemReceive([FromBody] DocumentItemListPageModel payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentItemReceive(payload);
            return Json(new BaseResponseView<List<vDocumentItemView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("ClassificationListWordFirst")]
        public IActionResult ClassificationListWordFirst([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<RM_Save010DocumentClassificationWordFirst, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //key = "document_role02_print_list_status_code",
            //values = new List<string>() {
            //    DocumentRole02PrintListStatusCodeConstant.WAIT.ToString(),
            //    DocumentRole02PrintListStatusCodeConstant.DONE.ToString(),
            //},
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("ClassificationListWorSoundLast")]
        public IActionResult ClassificationListWorSoundLast([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<RM_Save010DocumentClassificationSoundLast, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //key = "document_role02_print_list_status_code",
            //values = new List<string>() {
            //    DocumentRole02PrintListStatusCodeConstant.WAIT.ToString(),
            //    DocumentRole02PrintListStatusCodeConstant.DONE.ToString(),
            //},
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("ClassificationListWorSoundLastOther")]
        public IActionResult ClassificationListWorSoundLastOther([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<RM_Save010DocumentClassificationSoundLastOther, ReferenceMasterView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //key = "document_role02_print_list_status_code",
            //values = new List<string>() {
            //    DocumentRole02PrintListStatusCodeConstant.WAIT.ToString(),
            //    DocumentRole02PrintListStatusCodeConstant.DONE.ToString(),
            //},
            //});
            var result = service.List(payload);
            return Json(result);
        }



        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("ClassificationLoad/{id}")]
        public IActionResult ClassificationLoad(string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessService<SaveModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
            var result = service.Get(Convert.ToInt64(id));
            result.document_classification_word_list = result.document_classification_word_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();
            result.document_classification_image_list = result.document_classification_image_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();
            result.document_classification_sound_list = result.document_classification_sound_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();

            //var rdc_service = new RequestDocumentCollectService(_configuration, _uowProvider, _mapper);
            result.trademark_2d_physical_path = _requestDocumentCollectService.GetTrademark2DPhysicalPath(result.id.Value);

            return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ClassificationVersionList/{id}")]
        public IActionResult ClassificationVersionList(string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new BaseService<Save010DocumentClassificationVersion, Save010DocumentClassificationVersionView>(_configuration, _uowProvider, _mapper);
            var result = service.List(new PageRequest()
            {
                search_by = new List<SearchByModel>() {
                     new SearchByModel() {
                          key = "save_id",
                           value = id,
                     }
                 }
            });
            foreach (var view in result.data.list)
            {
                view.document_classification_word_list = view.document_classification_word_list.OrderBy(r => r.save_index).ToList();
                view.document_classification_image_list = view.document_classification_image_list.OrderBy(r => r.save_index).ToList();
                view.document_classification_sound_list = view.document_classification_sound_list.OrderBy(r => r.save_index).ToList();
            }

            return Json(result);
            //result.document_classification_word_list = result.document_classification_word_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();
            //result.document_classification_image_list = result.document_classification_image_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();
            //result.document_classification_sound_list = result.document_classification_sound_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();

            ////var rdc_service = new RequestDocumentCollectService(_configuration, _uowProvider, _mapper);
            //result.trademark_2d_physical_path = _requestDocumentCollectService.GetTrademark2DPhysicalPath(result.id.Value);

            //return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;
            //return Json(new BaseResponseView<List<Save010ProcessView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("ClassificationLoadFromRequestNumber/{request_number}")]
        public IActionResult ClassificationLoadFromRequestNumber(string request_number)
        {
            var service = new SaveProcessService<SaveModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
            var result = service.List(new SaveModel() { request_number = request_number }).FirstOrDefault();

            if (result != null)
            {
                result.document_classification_word_list = result.document_classification_word_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();
                result.document_classification_image_list = result.document_classification_image_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();
                result.document_classification_sound_list = result.document_classification_sound_list.Where(r => !r.is_deleted).OrderBy(r => r.save_index).ToList();

                result.trademark_2d_physical_path = _requestDocumentCollectService.GetTrademark2DPhysicalPath(result.id.Value);
            }

            return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ClassificationList")]
        public IActionResult ClassificationList([FromBody] PageRequest payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessService<vSave010Model, Save010, vSave010View>(_configuration, _uowProvider, _mapper);

            payload.search_by.Add(new SearchByModel()
            {
                key = "document_status_code",
                values = new List<string>() {
                    //DocumentStatusCodeConstant.WAIT_DOCUMENTING.ToString(),
                    DocumentStatusCodeConstant.RECEIVED_DOCUMENTING.ToString(),
                },
            });

            var result = service.ListSave010(payload);
            return Json(new BaseResponsePageView<List<vSave010View>>()
            {
                data = new BaseResponsePageDataModel<List<vSave010View>>()
                {
                    list = result,
                    paging = payload
                }
            });

            //return Json(service.GetResponseListView(result));
        }

        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("ClassificationSave")]
        public IActionResult ClassificationSave([FromBody] Save010AddModel payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.ClassificationSave(payload);

            result.document_classification_word_list = result.document_classification_word_list.OrderBy(r => r.save_index).ToList();
            result.document_classification_image_list = result.document_classification_image_list.OrderBy(r => r.save_index).ToList();
            result.document_classification_sound_list = result.document_classification_sound_list.OrderBy(r => r.save_index).ToList();
            result.trademark_2d_physical_path = _requestDocumentCollectService.GetTrademark2DPhysicalPath(result.id.Value);

            return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("ClassificationSend")]
        public IActionResult ClassificationSend([FromBody] Save010AddModel payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.ClassificationSend(payload);

            result.document_classification_word_list = result.document_classification_word_list.OrderBy(r => r.save_index).ToList();
            result.document_classification_image_list = result.document_classification_image_list.OrderBy(r => r.save_index).ToList();
            result.document_classification_sound_list = result.document_classification_sound_list.OrderBy(r => r.save_index).ToList();
            result.trademark_2d_physical_path = _requestDocumentCollectService.GetTrademark2DPhysicalPath(result.id.Value);

            return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("DocumentProcessCollectHistoryImageSend")]
        public IActionResult DocumentProcessCollectHistoryImageSend([FromBody] vDocumentProcessCollectAddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentProcessCollectHistoryImageSend(payload);
            return Json(new BaseResponseView<vDocumentProcessCollectView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("DocumentRole02CheckSendChange")]
        public IActionResult DocumentRole02CheckSendChange([FromBody] List<SendChangeAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02CheckSendChange(payload);

            return Json(new BaseResponseView<List<SendChangeView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole02CheckSend")]
        public IActionResult DocumentRole02CheckSend([FromBody] List<vDocumentRole02GroupAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02CheckSend(payload);

            return Json(new BaseResponseView<List<vDocumentRole02GroupView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole02CheckIntructionRuleSave")]
        public IActionResult DocumentRole02CheckIntructionRuleSave([FromBody] vDocumentRole02AddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02CheckIntructionRuleSave(payload);
            return Json(new BaseResponseView<Save010InstructionRuleView>() { data = result });
        }

        //[ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        //[HttpPost]
        //[Route("DocumentRole02CheckSendChange")]
        //public IActionResult DocumentRole02CheckSendChange([FromBody]List<SendChangeAddModel> payload) {
        //    var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
        //    var result = service.DocumentRole02CheckSendChange(payload);

        //    return Json(new BaseResponseView<List<SendChangeView>>() { data = result });
        //}


        //[ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        //[HttpPost]
        //[Route("ClassificationVersionAdd/{id}")]
        //public IActionResult ClassificationVersionAdd(string id) {
        //    var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
        //    var result = service.ClassificationVersionAdd(Convert.ToInt64(id));
        //    return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        //}


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ClassificationListImageType")]
        public IActionResult ClassificationListImageType([FromBody] BasePageModelPayload<ReferenceMasterModel> payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.ClassificationListImageType(payload);
            return Json(new BaseResponseView<List<ReferenceMasterView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ClassificationListSoundType")]
        public IActionResult ClassificationListSoundType([FromBody] BasePageModelPayload<ReferenceMasterModel> payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.ClassificationListSoundType(payload);
            return Json(new BaseResponseView<List<ReferenceMasterView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("CollectRequestAdd")]
        public IActionResult CollectRequestAdd([FromBody] Save010DocumentClassificationCollectAddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.CollectRequestAdd(payload);
            return Json(new BaseResponseView<Save010DocumentClassificationCollectView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("CollectRequestDelete/{id}")]
        public IActionResult CollectRequestDelete(long id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.CollectRequestDelete(id);
            return Json(new BaseResponseView<Save010DocumentClassificationCollectView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("CollectHistoryAdd")]
        public IActionResult CollectHistoryAdd([FromBody] Save010DocumentClassificationCollectAddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.CollectHistoryAdd(payload);
            return Json(new BaseResponseView<Save010DocumentClassificationCollectView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ClassificationRequestList")]
        public IActionResult ClassificationRequestList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentProcessCollect, vDocumentProcessCollectView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel()
            {
                key = "document_classification_collect_status_code",
                value = DocumentClassificationCollectStatusCodeConstant.WAIT.ToString(),
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("ClassificationHistoryList")]
        public IActionResult ClassificationHistoryList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentProcessCollect, vDocumentProcessCollectView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //    key = "document_classification_collect_status_code",
            //    value = DocumentClassificationCollectStatusCodeConstant.DONE.ToString(),
            //});
            payload.is_order_reverse = true;
            var result = service.List(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentProcessCollectHistoryDelete/{id}")]
        public IActionResult DocumentProcessCollectHistoryDelete(long id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentProcessCollectHistoryDelete(id);
            return Json(new BaseResponseView<Save010DocumentClassificationCollectView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("DocumentRole02ItemList")]
        public IActionResult DocumentRole02ItemList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentRole02Group, vDocumentRole02GroupView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("DocumentRole02ItemListAutoSplit")]
        public IActionResult DocumentRole02ItemListAutoSplit()
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02ItemListAutoSplit();
            return Json(result);
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("DocumentRole03ItemListAutoSplit")]
        public IActionResult DocumentRole03ItemListAutoSplit()
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole03ItemListAutoSplit();
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("DocumentRole02CheckList")]
        public IActionResult DocumentRole02CheckList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentRole02Group, vDocumentRole02GroupView>(_configuration, _uowProvider, _mapper);
            payload.filter_queries.Add("!string.IsNullOrEmpty(document_role02_receive_status_code)");
            //payload.search_by.Add(new SearchByModel() {
            //    key = "document_role02_receive_status_code",
            //    values = new List<string>() {
            //        DocumentRole02ReceiveStatusCodeConstant.S.ToString(),
            //        DocumentRole02ReceiveStatusCodeConstant.WAIT_CHANGE.ToString(),
            //        DocumentRole02ReceiveStatusCodeConstant.DONE.ToString(),
            //    },
            //});
            var result = service.List(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponseView<PageDocumentRole02CheckView>), 200)]
        [HttpPost]
        [Route("DocumentRole02CheckLoad/{id}")]
        public IActionResult DocumentRole02CheckLoad(string id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02CheckLoad(Convert.ToInt32(id));

            return Json(new BaseResponseView<PageDocumentRole02CheckView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole02PrintDocumentItemLoad/{id}")]
        public IActionResult DocumentRole02PrintDocumentItemLoad(string id)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;


            var base_service = new BaseService<vDocumentRole02, vDocumentRole02View>(_configuration, _uowProvider, _mapper);
            var base_view = base_service.List(new PageRequest()
            {
                search_by = new List<SearchByModel>() {
                     new SearchByModel() {
                         key = "id",
                         value = id,
                     }
                 }
            });

            var service = new SaveProcessService<vSave010Model, vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
            var view = service.Get(base_view.data.list.FirstOrDefault().save_id.Value);

            view.document_role_02_list = base_view.data.list;

            return Json(new BaseResponseView<vSave010View>() { data = view });
        }



        [ProducesResponseType(typeof(BaseResponseView<vDocumentRole02View>), 200)]
        [HttpPost]
        [Route("DocumentRole02CheckInstructionRuleSend")]
        public IActionResult DocumentRole02CheckInstructionRuleSend([FromBody] vDocumentRole02AddModel payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02CheckInstructionRuleSend(payload);
            return Json(new BaseResponseView<vDocumentRole02View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("DocumentRole02PrintDocumentList")]
        public IActionResult DocumentRole02PrintDocumentList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentRole02Group, vDocumentRole02GroupView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //    key = "document_role02_print_document_status_code",
            //    values = new List<string>() {
            //        DocumentRole02PrintDocumentStatusCodeConstant.WAIT.ToString(),
            //        DocumentRole02PrintDocumentStatusCodeConstant.DONE.ToString(),
            //    },
            //});
            var result = service.List(payload, new string[] { "document_role02_print_document_status_code" });
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<vDocumentRole02GroupView>>), 200)]
        [HttpPost]
        [Route("DocumentRole02PrintDocumentSend")]
        public IActionResult DocumentRole02PrintDocumentSend([FromBody] List<vDocumentRole02GroupAddModel> payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02PrintDocumentSend(payload);
            return Json(new BaseResponseView<List<vDocumentRole02GroupView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("DocumentRole02PrintCoverList")]
        public IActionResult DocumentRole02PrintCoverList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentRole02Group, vDocumentRole02GroupView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel()
            {
                key = "document_role02_print_cover_status_code",
                values = new List<string>() {
                    DocumentRole02PrintCoverStatusCodeConstant.WAIT_COVER.ToString(),
                    //DocumentRole02PrintCoverStatusCodeConstant.WAIT_POST_NUMBER.ToString(),
                    DocumentRole02PrintCoverStatusCodeConstant.DONE.ToString(),
                },
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<vDocumentRole02GroupView>>), 200)]
        [HttpPost]
        [Route("DocumentRole02PrintCoverSend")]
        public IActionResult DocumentRole02PrintCoverSend([FromBody] List<vDocumentRole02GroupAddModel> payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02PrintCoverSend(payload);
            return Json(new BaseResponseView<List<vDocumentRole02GroupView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<vDocumentRole02GroupView>), 200)]
        [HttpPost]
        [Route("DocumentRole02PrintPostNumberSend")]
        public IActionResult DocumentRole02PrintPostNumberSend([FromBody] vDocumentRole02GroupAddModel payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02PrintPostNumberSend(payload);
            return Json(new BaseResponseView<vDocumentRole02GroupView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("DocumentRole02PrintListList")]
        public IActionResult DocumentRole02PrintListList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentRole02Group, vDocumentRole02GroupView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel()
            {
                key = "document_role02_print_list_status_code",
                values = new List<string>() {
                    DocumentRole02PrintListStatusCodeConstant.WAIT_POST_NUMBER.ToString(),
                    DocumentRole02PrintListStatusCodeConstant.WAIT_PRINT.ToString(),
                    DocumentRole02PrintListStatusCodeConstant.DONE.ToString(),
                },
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<vDocumentRole02GroupView>>), 200)]
        [HttpPost]
        [Route("DocumentRole02PrintListSend")]
        public IActionResult DocumentRole02PrintListSend([FromBody] List<vDocumentRole02GroupAddModel> payload)
        {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole02PrintListSend(payload);
            return Json(new BaseResponseView<List<vDocumentRole02GroupView>>() { data = result });
        }




        [ProducesResponseType(typeof(BaseResponsePageView<List<vDocumentRole02View>>), 200)]
        [HttpPost]
        [Route("SearchSimilarList")]
        public IActionResult SearchSimilarList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vSave010DocumentClassificationSearchSimilar, vSave010DocumentClassificationSearchSimilarView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }



        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("DocumentProcessChangeList")]
        public IActionResult DocumentProcessChangeList([FromBody] PageRequest payload)
        {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vDocumentProcessChange, vDocumentProcessChangeView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }



        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("DashboardDocumentRole01Load")]
        public IActionResult DashboardDocumentRole01Load()
        {
            var service = new BaseService<vDashboardDocumentRole01, vDashboardDocumentRole01View>(_configuration, _uowProvider, _mapper);
            PageRequest payload = new PageRequest();
            payload.search_by = new List<SearchByModel>() {
                new SearchByModel() {
                    key = "id",
                    value = service.GetUserId().ToString(),
                }
            };
            var result = service.List(payload);
            return Json(result);
        }


        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("DocumentRole04ItemAutoSplit")]
        public IActionResult DocumentRole04ItemAutoSplit()
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04ItemAutoSplit();
            return Json(result);
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("DocumentRole05ItemAutoSplit")]
        public IActionResult DocumentRole05ItemAutoSplit()
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole05ItemAutoSplit();
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponseView<PageDocumentRole02CheckView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CheckLoad/{id}")]
        public IActionResult DocumentRole04CheckLoad(string id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CheckLoad(Convert.ToInt32(id));

            return Json(new BaseResponseView<PageDocumentRole04CheckView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04ReleaseRequestSend")]
        public IActionResult DocumentRole04ReleaseRequestSend([FromBody] List<vDocumentRole04ReleaseRequestAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04ReleaseRequestSend(payload);

            return Json(new BaseResponseView<List<vDocumentRole04ReleaseRequestView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole05ReleaseRequestSend")]
        public IActionResult DocumentRole05ReleaseRequestSend([FromBody] List<vDocumentRole04ReleaseRequestAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole05ReleaseRequestSend(payload);

            return Json(new BaseResponseView<PageDocumentRole04ReleaseRequestView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole05ReleaseRequestSendChange")]
        public IActionResult DocumentRole05ReleaseRequestSendChange([FromBody] List<vDocumentRole04ReleaseRequestAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole05ReleaseRequestSendChange(payload);

            return Json(new BaseResponseView<PageDocumentRole04ReleaseRequestView>() { data = result });
        }



        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04ReleaseRequestLoad/{id}_{type}")]
        public IActionResult DocumentRole04ReleaseRequestLoad(string id, string type)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04ReleaseRequestLoad(Convert.ToInt32(id), type);

            return Json(new BaseResponseView<PageDocumentRole04ReleaseRequestView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04ReleaseLoad/{id}")]
        public IActionResult DocumentRole04ReleaseLoad(string id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04ReleaseLoad(Convert.ToInt32(id));

            return Json(new BaseResponseView<PageDocumentRole04ReleaseView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole03ChangeLoad/{id}")]
        public IActionResult DocumentRole03ChangeLoad(string id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole03ChangeLoad(Convert.ToInt32(id.Split('_')[0]), Convert.ToInt32(id.Split('_')[1]));

            return Json(new BaseResponseView<PageDocumentRole03ChangeView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04Release20Load/{id}")]
        public IActionResult DocumentRole04Release20Load(string id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04Release20Load(Convert.ToInt32(id));

            return Json(new BaseResponseView<PageDocumentRole04ReleaseView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CheckSend")]
        public IActionResult DocumentRole04CheckSend([FromBody] List<PageDocumentRole04CheckAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CheckSend(payload);
            return Json(new BaseResponseView<List<PageDocumentRole04CheckView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole05CheckSend")]
        public IActionResult DocumentRole05CheckSend([FromBody] List<PageDocumentRole04CheckAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole05CheckSend(payload);
            return Json(new BaseResponseView<List<PageDocumentRole04CheckView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole05CheckSendChange")]
        public IActionResult DocumentRole05CheckSendChange([FromBody] List<SendChangeAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole05CheckSendChange(payload);
            return Json(new BaseResponseView<List<SendChangeView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CreateAdd")]
        public IActionResult DocumentRole04CreateAdd([FromBody] vDocumentRole04CreateAddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CreateAdd(payload);
            return Json(new BaseResponseView<vDocumentRole04CreateView>() { data = result });
        }



        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CreateFileAdd")]
        public IActionResult DocumentRole04CreateFileAdd([FromBody] vDocumentRole04CreateFileAddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CreateFileAdd(payload);
            return Json(new BaseResponseView<vDocumentRole04CreateFileView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CreateFileDelete")]
        public IActionResult DocumentRole04CreateFileDelete([FromBody] List<long> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CreateFileDelete(payload);
            return Json(new BaseResponseView<List<long>>() { data = result });
        }



        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CheckInstructionRuleFileAdd")]
        public IActionResult DocumentRole04CheckInstructionRuleFileAdd([FromBody] vPostRoundInstructionRuleFileAddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CheckInstructionRuleFileAdd(payload);
            return Json(new BaseResponseView<vPostRoundInstructionRuleFileView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CheckInstructionRuleFileDelete")]
        public IActionResult DocumentRole04CheckInstructionRuleFileDelete([FromBody] List<long> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CheckInstructionRuleFileDelete(payload);
            return Json(new BaseResponseView<List<long>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("List/{view_name}")]
        public IActionResult List(string view_name, [FromBody] PageRequest payload)
        {
            var service = BaseController.GetService(view_name, _configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }



        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04ReleaseRequestItemLoad/{id}")]
        public IActionResult DocumentRole04ReleaseRequestItemLoad(string id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04ReleaseRequestItemLoad(Convert.ToInt32(id));

            return Json(new BaseResponseView<PageDocumentRole04ReleaseRequestItemView>() { data = result });

            //var service = new SaveProcessService<SaveModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper);
            //var result = service.Save010Load(Convert.ToInt64(id), true);

            ////var rdc_service = new RequestDocumentCollectService(configuration, uowProvider, mapper);
            ////result.file_trademark_2d = rdc_service.GetTrademark2DPhysicalPath(entity.save_id.Value);

            //var view_service = new BaseService<vDocumentRole02, vDocumentRole02View>(configuration, uowProvider, mapper);
            //var result = view_service.List(new PageRequest() {
            //    search_by = new List<SearchByModel>() {
            //              new SearchByModel() {
            //                  key = "id",
            //                  value = payload.ToString(),
            //              }
            //         }
            //});
            //view.instruction_rule_list = result.data.list;

            //var document_service = new BaseService<vSave010CheckingSaveDocument, vSave010CheckingSaveDocumentView>(configuration, uowProvider, mapper);
            //var document = document_service.List(new PageRequest() {
            //    search_by = new List<SearchByModel>() {
            //              new SearchByModel() {
            //                  key = "request_number",
            //                  value = view.view.request_number,
            //              }
            //         }
            //});
            //view.document_list = document.data.list;

            //var receipt_item_service = new BaseService<vReceiptItem, vReceiptItemView>(configuration, uowProvider, mapper);
            //var receipt_item = receipt_item_service.List(new PageRequest() {
            //    search_by = new List<SearchByModel>() {
            //              new SearchByModel() {
            //                  key = "save_id",
            //                  value = view.view.id.ToString(),
            //              }
            //         }
            //});
            //view.receipt_item_list = receipt_item.data.list;

            //var history_service = new BaseService<vSave010History, vSave010HistoryView>(configuration, uowProvider, mapper);
            //var history = history_service.List(new PageRequest() {
            //    search_by = new List<SearchByModel>() {
            //              new SearchByModel() {
            //                  key = "save_id",
            //                  value = view.view.id.ToString(),
            //              }
            //         }
            //});
            //view.history_list = history.data.list;

            //var scan_service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(configuration, uowProvider, mapper);
            //var scan = scan_service.List(new PageRequest() {
            //    search_by = new List<SearchByModel>() {
            //              new SearchByModel() {
            //                  key = "request_number",
            //                  value = view.view.request_number,
            //              },
            //              new SearchByModel() {
            //                  key = "file_id",
            //                  value = "0",
            //                  operation= Operation.GreaterThan,
            //              }
            //         }
            //});
            //view.document_scan_list = scan.data.list;

            //var document_role04_check_service = new BaseService<vDocumentRole04Check, vDocumentRole04CheckView>(configuration, uowProvider, mapper);
            //var document_role04_check = document_role04_check_service.List(new PageRequest() {
            //    search_by = new List<SearchByModel>() {
            //            new SearchByModel() {
            //                key = "id",
            //                value = payload.ToString(),
            //            }
            //        }
            //});
            //view.document_role04_check_list = document_role04_check.data.list;

            //return Json(new BaseResponseView<vSave010View>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CreateLoad/{id}")]
        public IActionResult DocumentRole04CreateLoad(string id)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CreateLoad(Convert.ToInt32(id));

            return Json(new BaseResponseView<PageDocumentRole04CreateView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04CreateSend")]
        public IActionResult DocumentRole04CreateSend([FromBody] List<vDocumentRole04CreateAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04CreateSend(payload);
            return Json(new BaseResponseView<List<vDocumentRole04CreateView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole05CreateSend")]
        public IActionResult DocumentRole05CreateSend([FromBody] List<vDocumentRole04CreateAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole05CreateSend(payload);
            return Json(new BaseResponseView<List<vDocumentRole04CreateView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04ReleaseSend")]
        public IActionResult DocumentRole04ReleaseSend([FromBody] List<vDocumentRole04ReleaseAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04ReleaseSend(payload);

            return Json(new BaseResponseView<List<vDocumentRole04ReleaseView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole05ReleaseSend")]
        public IActionResult DocumentRole05ReleaseSend([FromBody] List<vDocumentRole04ReleaseAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole05ReleaseSend(payload);

            return Json(new BaseResponseView<List<vDocumentRole04ReleaseView>>() { data = result });
        }




        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole04Release20Send")]
        public IActionResult DocumentRole04Release20Send([FromBody] List<vDocumentRole04Release20AddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole04Release20Send(payload);

            return Json(new BaseResponseView<List<vDocumentRole04Release20View>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole05Release20Send")]
        public IActionResult DocumentRole05Release20Send([FromBody] List<vDocumentRole04Release20AddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole05Release20Send(payload);

            return Json(new BaseResponseView<List<vDocumentRole04Release20View>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CertificationFileAdd")]
        public IActionResult CertificationFileAdd([FromBody] List<Save010CertificationFileAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.CertificationFileAdd(payload);
            return Json(new BaseResponseView<List<vSave010CertificationFileView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("SaveList")]
        public IActionResult SaveList([FromBody] PageRequest payload)
        {
            var service = new BaseService<vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole03ChangeSave")]
        public IActionResult DocumentRole03ChangeSave([FromBody] PageDocumentRole03ChangeVersionAddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole03ChangeSave(payload);
            return Json(new BaseResponseView<PageDocumentRole03ChangeVersionView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole03ChangeSend")]
        public IActionResult DocumentRole03ChangeSend([FromBody] PageDocumentRole03ChangeVersionAddModel payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole03ChangeSend(payload);
            return Json(new BaseResponseView<PageDocumentRole03ChangeVersionView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole03ChangeSendAll")]
        public IActionResult DocumentRole03ChangeSendAll([FromBody] List<vDocumentRole03ItemGroupAddModel> payload)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole03ChangeSendAll(payload);
            return Json(new BaseResponseView<List<vDocumentRole03ItemGroupView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("DocumentRole03ChangeVersionList/{save_id}/{request_type_code}")]
        public IActionResult DocumentRole03ChangeVersionList(string save_id, string request_type_code)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole03ChangeVersionList(Convert.ToInt64(save_id), request_type_code);
            return Json(new BaseResponseView<List<vDocumentRole03ItemView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("DocumentRole03ChangeVersionLoad/{save_id}/{id}/{request_type_code}")]
        public IActionResult DocumentRole03ChangeVersionLoad(string save_id, string id, string request_type_code)
        {
            var service = new DocumentProcessService(_configuration, _uowProvider, _mapper);
            var result = service.DocumentRole03ChangeVersionLoad(Convert.ToInt64(save_id), Convert.ToInt64(id), request_type_code);
            return Json(new BaseResponseView<PageDocumentRole03ChangeVersionView>() { data = result });
        }
    }
}
