﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Migrate;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Collections.Generic;

namespace DIP.TM.Web.Controllers
{
    public class MigrateEFormController : BaseApiController
    {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        protected Dictionary<MigrateEFormType, dynamic> migrate_eform_service_list;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public MigrateEFormController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;

            migrate_eform_service_list = new Dictionary<MigrateEFormType, dynamic>
            {
                { MigrateEFormType.eFrom010, new MigrateEFormService<eForm_Save010, Save010AddModel, Save010, Save010ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom020, new MigrateEFormService<eForm_Save020, Save020AddModel, Save020, Save020ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom021, new MigrateEFormService<eForm_Save021, Save021AddModel, Save021, Save021ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom030, new MigrateEFormService<eForm_Save030, Save030AddModel, Save030, Save030ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom040, new MigrateEFormService<eForm_Save040, Save040AddModel, Save040, Save040ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom050, new MigrateEFormService<eForm_Save050, Save050AddModel, Save050, Save050ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom060, new MigrateEFormService<eForm_Save060, Save060AddModel, Save060, Save060ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom070, new MigrateEFormService<eForm_Save070, Save070AddModel, Save070, Save070ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom080, new MigrateEFormService<eForm_Save080, Save080AddModel, Save080, Save080ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom140, new MigrateEFormService<eForm_Save140, Save140AddModel, Save140, Save140ProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom120, new MigrateEFormService<eForm_Save120, SaveOtherAddModel, SaveOther, SaveOtherProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom150, new MigrateEFormService<eForm_Save150, SaveOtherAddModel, SaveOther, SaveOtherProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom190, new MigrateEFormService<eForm_Save190, SaveOtherAddModel, SaveOther, SaveOtherProcessView>(_configuration, _uowProvider, _mapper) },
                { MigrateEFormType.eFrom200, new MigrateEFormService<eForm_Save200, SaveOtherAddModel, SaveOther, SaveOtherProcessView>(_configuration, _uowProvider, _mapper) }
            };
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseSaveView>), 200)]
        [AllowAnonymous] // for test
        [HttpPost]
        [Route("{eformType}/{eform_number}")]
        public IActionResult MigrateFromEForm([JsonConverter(typeof(StringEnumConverter))]MigrateEFormType eformType, string eform_number)
        {
            var service = migrate_eform_service_list[eformType];
            if (service == null)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = "Not found ID: [" + eformType.ToString() + "]" });

            var result = service.DownloadFromEForm(eform_number);
            if (result == null)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = "Not found eForm Number: [" + eform_number + "]" });
            return Json(new BaseResponseView<BaseSaveView>() { data = result });
        }
    }
}