﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Views;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using DIP.TM.Utils.Extensions;
using Microsoft.AspNetCore.Hosting;

namespace DIP.TM.Web.Controllers {
    /// <summary>
    /// reference https://riptutorial.com/epplus
    /// </summary>
    [AllowAnonymous]
    public class ExcelsController : BaseController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        private readonly IHostingEnvironment _env;
        public ExcelsController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, IHostingEnvironment env) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _env = env;
        }

        [Route("excels/PostDocumentList/{fileId}")]
        public IActionResult PostDocumentList(string fileId) {
            byte[] fileContents;
            string contentRootPath = _env.ContentRootPath;
            string webRootPath = _env.WebRootPath; 
            FileInfo fileinfo = new FileInfo(Path.Combine(webRootPath, "Templates/MSExcels/PostDocumentList.xlsx"));

            //if (!Directory.Exists(Path.Combine(Directory.GetCurrentDirectory(), "Contents/excels/"))) {
            //    Directory.CreateDirectory(Path.Combine(Directory.GetCurrentDirectory(), "Contents/excels/"));
            //}

            var service = new BaseService<vDocumentRole02Group, vDocumentRole02GroupView>(_configuration, _uowProvider, _mapper);
            var result = service.List(new PageRequest() {
                search_by = new List<SearchByModel>() {
                    new SearchByModel() {
                        key= "id",
                        values = fileId.Split("|").ToList(),
                    }
                }
            });
            using (var package = new ExcelPackage(fileinfo)) {
                var worksheet = package.Workbook.Worksheets["Sheet1"];

                worksheet.Cells[3, 10].Value = "วันที่ฝากส่ง " + DateTime.Now.ToThaiDateString();
                worksheet.Cells[8, 3].Value = DateTime.Now.ToThaiDateString();

                var row_index = 1;
                foreach (var view in result.data.list) {
                    var column_index = 1;

                    worksheet.Cells[row_index + 12, column_index++].Value = row_index;
                    worksheet.Cells[row_index + 12, column_index++].Value = view.name;
                    worksheet.Cells[row_index + 12, column_index++].Value = view.province_name;
                    worksheet.Cells[row_index + 12, column_index++].Value = "";
                    worksheet.Cells[row_index + 12, column_index++].Value = view.post_number;
                    worksheet.Cells[row_index + 12, column_index++].Value = "";
                    worksheet.Cells[row_index + 12, column_index++].Value = "";
                    worksheet.Cells[row_index + 12, column_index++].Value = "TH";
                    worksheet.Cells[row_index + 12, column_index++].Value = "";
                    worksheet.Cells[row_index + 12, column_index++].Value = "";
                    worksheet.Cells[row_index + 12, column_index++].Value = view.request_number + "5(3)." + view.book_number;

                    row_index++;
                }
                ////worksheet.Cells[2, 5].Value = "ใบนำส่งสิ่งของ;


                ////add some dummy data, note that row and column indexes start at 1
                //for (int i = 1; i <= 30; i++) {
                //    for (int j = 1; j <= 15; j++) {
                //        worksheet.Cells[i, j].Value = "Row " + i + ", Column " + j;
                //    }
                //}

                ////fill column A with solid red color
                //worksheet.Column(1).Style.Fill.PatternType = ExcelFillStyle.Solid;
                //worksheet.Column(1).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FF0000"));

                ////set the font type for cells C1 - C30
                //worksheet.Cells["C1:C30"].Style.Font.Size = 13;
                //worksheet.Cells["C1:C30"].Style.Font.Name = "Calibri";
                //worksheet.Cells["C1:C30"].Style.Font.Bold = true;
                //worksheet.Cells["C1:C30"].Style.Font.Color.SetColor(Color.Blue);

                ////fill row 4 with striped orange background
                //worksheet.Row(4).Style.Fill.PatternType = ExcelFillStyle.DarkHorizontal;
                //worksheet.Row(4).Style.Fill.BackgroundColor.SetColor(Color.Orange);

                ////make the borders of cell F6 thick
                //worksheet.Cells[6, 6].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                //worksheet.Cells[6, 6].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                //worksheet.Cells[6, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                //worksheet.Cells[6, 6].Style.Border.Left.Style = ExcelBorderStyle.Thick;

                ////make the borders of cells A18 - J18 double and with a purple color
                //worksheet.Cells["A18:J18"].Style.Border.Top.Style = ExcelBorderStyle.Double;
                //worksheet.Cells["A18:J18"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                //worksheet.Cells["A18:J18"].Style.Border.Top.Color.SetColor(Color.Purple);
                //worksheet.Cells["A18:J18"].Style.Border.Bottom.Color.SetColor(Color.Purple);

                ////make all text fit the cells
                //worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                ////i use this to make all columms just a bit wider, text would sometimes still overflow after AutoFitColumns(). Bug?
                //for (int col = 1; col <= worksheet.Dimension.End.Column; col++) {
                //    worksheet.Column(col).Width = worksheet.Column(col).Width + 1;
                //}

                ////make column H wider and set the text align to the top and right
                //worksheet.Column(8).Width = 25;
                //worksheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                //worksheet.Column(8).Style.VerticalAlignment = ExcelVerticalAlignment.Top;


                fileContents = package.GetAsByteArray();
                //package.Dispose();
            }

            if (fileContents == null || fileContents.Length == 0) {
                return NotFound();
            }

            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "sheet.xlsx"
            );
        }

        public IActionResult Test() {
            byte[] fileContents;


            using (var package = new ExcelPackage()) {
                var worksheet = package.Workbook.Worksheets.Add("งานเอกสาร");
                //add some dummy data, note that row and column indexes start at 1
                for (int i = 1; i <= 30; i++) {
                    for (int j = 1; j <= 15; j++) {
                        worksheet.Cells[i, j].Value = "Row " + i + ", Column " + j;
                    }
                }

                //fill column A with solid red color
                worksheet.Column(1).Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Column(1).Style.Fill.BackgroundColor.SetColor(ColorTranslator.FromHtml("#FF0000"));

                //set the font type for cells C1 - C30
                worksheet.Cells["C1:C30"].Style.Font.Size = 13;
                worksheet.Cells["C1:C30"].Style.Font.Name = "Calibri";
                worksheet.Cells["C1:C30"].Style.Font.Bold = true;
                worksheet.Cells["C1:C30"].Style.Font.Color.SetColor(Color.Blue);

                //fill row 4 with striped orange background
                worksheet.Row(4).Style.Fill.PatternType = ExcelFillStyle.DarkHorizontal;
                worksheet.Row(4).Style.Fill.BackgroundColor.SetColor(Color.Orange);

                //make the borders of cell F6 thick
                worksheet.Cells[6, 6].Style.Border.Top.Style = ExcelBorderStyle.Thick;
                worksheet.Cells[6, 6].Style.Border.Right.Style = ExcelBorderStyle.Thick;
                worksheet.Cells[6, 6].Style.Border.Bottom.Style = ExcelBorderStyle.Thick;
                worksheet.Cells[6, 6].Style.Border.Left.Style = ExcelBorderStyle.Thick;

                //make the borders of cells A18 - J18 double and with a purple color
                worksheet.Cells["A18:J18"].Style.Border.Top.Style = ExcelBorderStyle.Double;
                worksheet.Cells["A18:J18"].Style.Border.Bottom.Style = ExcelBorderStyle.Double;
                worksheet.Cells["A18:J18"].Style.Border.Top.Color.SetColor(Color.Purple);
                worksheet.Cells["A18:J18"].Style.Border.Bottom.Color.SetColor(Color.Purple);

                //make all text fit the cells
                worksheet.Cells[worksheet.Dimension.Address].AutoFitColumns();

                //i use this to make all columms just a bit wider, text would sometimes still overflow after AutoFitColumns(). Bug?
                for (int col = 1; col <= worksheet.Dimension.End.Column; col++) {
                    worksheet.Column(col).Width = worksheet.Column(col).Width + 1;
                }

                //make column H wider and set the text align to the top and right
                worksheet.Column(8).Width = 25;
                worksheet.Column(8).Style.HorizontalAlignment = ExcelHorizontalAlignment.Right;
                worksheet.Column(8).Style.VerticalAlignment = ExcelVerticalAlignment.Top;


                fileContents = package.GetAsByteArray();
            }

            if (fileContents == null || fileContents.Length == 0) {
                return NotFound();
            }

            return File(
                fileContents: fileContents,
                contentType: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                fileDownloadName: "test.xlsx"
            );
        }
    }
}