﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models.Payloads.Appeal;
using DIP.TM.Models.Views;
using DIP.TM.Models.Views.Appeal;
using DIP.TM.Services;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DIP.TM.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AppealProcessController : BaseApiController
    {

        private readonly IUowProvider uowProvider;
        private readonly IConfiguration configuration;
        private readonly IMapper mapper;

        public AppealProcessController(IUowProvider uowProvider, IConfiguration configuration, IMapper mapper)
        {
            this.uowProvider = uowProvider;
            this.configuration = configuration;
            this.mapper = mapper;
        }

        [HttpPost]
        [Route("StartRequestCaseSummary/{request_id}")]
        public IActionResult StartRequestCaseSummary(int request_id, string request_type_code)
        {
            try
            {
                var service = new AppealProcessService(configuration, uowProvider, mapper);
                var result = service.StartRequestCaseSummary(request_id, request_type_code);
                return Json(new BaseResponseView<Appeal_Role02SaveCaseSummaryView>() { data = result });
            }
            catch (Exception ex)
            {
                return Json(new BaseResponseView<Appeal_Role02SaveCaseSummaryView>()
                {
                    data = null,
                    is_error = true,
                    error_message = ex.ToString()
                });
            }
        }

        [HttpPost]
        [Route("SaveDraftRequestCaseSummary")]
        public IActionResult SaveDraftRequestCaseSummary([FromBody]Appeal_Role02SaveCaseSummaryAddModel payload)
        {
            try
            {
                var service = new AppealProcessService(configuration, uowProvider, mapper);
                var result = service.SaveDraftRequestCaseSummary(payload);
                return Json(new BaseResponseView<Appeal_Role02SaveCaseSummaryView>() { data = result });
            }
            catch (Exception ex)
            {
                return Json(new BaseResponseView<Appeal_Role02SaveCaseSummaryView>() { data = null, is_error = true, error_message = ex.ToString() });
            }
        }

        [HttpPost]
        [Route("SaveRequestCaseSummarySend")]
        public IActionResult SaveRequestCaseSummarySend([FromBody]Appeal_Role02SaveCaseSummaryAddModel payload)
        {
            try
            {
                var service = new AppealProcessService(configuration, uowProvider, mapper);
                var result = service.SaveRequestCaseSummarySend(payload);
                return Json(new BaseResponseView<Appeal_Role02SaveCaseSummaryView>() { data = result });
            }
            catch (Exception ex)
            {
                return Json(new BaseResponseView<Appeal_Role02SaveCaseSummaryView>()
                {
                    data = null,
                    is_error = true,
                    error_message = ex.ToString()
                });
            }
        }

        [HttpPost]
        [Route("GetRequestCaseSummaryList")]
        public IActionResult GetRequestCaseSummaryList()
        {
            try
            {
                var service = new AppealProcessService(configuration, uowProvider, mapper);
                var result = service.GetRequestCaseSummaryList();
                return Json(new BaseResponseView<List<vCaseSummaryView>>() { data = result });
            }
            catch (Exception ex)
            {
                return Json(new BaseResponseView<List<vCaseSummaryView>>()
                {
                    data = null,
                    is_error = true,
                    error_message = ex.ToString()
                });
            }
        }

        [HttpPost]
        [Route("GetRequestCaseSummary/{request_id}")]
        public IActionResult GetRequestCaseSummary(int request_id)
        {
            try
            {
                var service = new AppealProcessService(configuration, uowProvider, mapper);
                var result = service.GetRequestCaseSummary(request_id);
                return Json(new BaseResponseView<Appeal_Role02SaveCaseSummaryView>() { data = result });
            }
            catch (Exception ex)
            {
                return Json(new BaseResponseView<Appeal_Role02SaveCaseSummaryView>()
                {
                    data = null,
                    is_error = true,
                    error_message = ex.ToString()
                });
            }
        }

    }
}
