﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Checking;
using DIP.TM.Services.Interfaces;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Web.Controllers {
    public class PublicProcessController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        private readonly ICheckingProcessService _checkingProcessService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public PublicProcessController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, ICheckingProcessService checkingProcessService) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _checkingProcessService = checkingProcessService;
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("PublicRole01CheckLoad/{id}")]
        public IActionResult PublicRole01CheckLoad(string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new SaveProcessService<Save010AddModel, Save010, vSave010View>(_configuration, _uowProvider, _mapper);
            var result = service.Save010Load(Convert.ToInt64(id), true);


            var base_service = new BaseService<vSave010RequestGroup, vSave010RequestGroupView>(_configuration, _uowProvider, _mapper);
            var base_resutlt = base_service.List(new PageRequest() {
                search_by = new List<SearchByModel>() {
                     new SearchByModel() {
                         key = "save_id",
                         value = id,
                     }
                 }
            });

            result.full_view.request_group_list = base_resutlt.data.list;



            return Json(new BaseResponseView<vSave010View>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PostRoundActionImageList/{id}")]
        public IActionResult PostRoundActionImageList(string id) {
            var service = new BaseService<PostRoundActionImage, PostRoundActionImageView>(_configuration, _uowProvider, _mapper);
            var result = service.List(new PageRequest() {
                search_by = new List<SearchByModel>() {
                     new SearchByModel() {
                         key = "post_round_id",
                         value = id,
                     }
                 }
            });
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PostRoundActionImageSave")]
        public IActionResult PostRoundActionImageSave([FromBody]List<PostRoundActionImageAddModel> payload) {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PostRoundActionImageSave(payload);
            return Json(new BaseResponseView<List<PostRoundActionImageView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("PublicRole01CheckSendChange")]
        public IActionResult PublicRole01CheckSendChange([FromBody]SendChangeAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole01CheckSendChange(payload);
            return Json(new BaseResponseView<BaseView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("PublicRole02ConsiderPaymentSendChange")]
        public IActionResult PublicRole02ConsiderPaymentSendChange([FromBody]List<SendChangeAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02ConsiderPaymentSendChange(payload);
            return Json(new BaseResponseView<List<SendChangeView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("PublicRole02DocumentPaymentSendChange")]
        public IActionResult PublicRole02DocumentPaymentSendChange([FromBody]List<SendChangeAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02DocumentPaymentSendChange(payload);
            return Json(new BaseResponseView<List<SendChangeView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("PublicRole05ConsiderPaymentSendChange")]
        public IActionResult PublicRole05ConsiderPaymentSendChange([FromBody]List<SendChangeAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole05ConsiderPaymentSendChange(payload);
            return Json(new BaseResponseView<List<SendChangeView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("PublicRole04CheckSendChange")]
        public IActionResult PublicRole04CheckSendChange([FromBody]List<SendChangeAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole04CheckSendChange(payload);
            return Json(new BaseResponseView<List<SendChangeView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("PublicRole04DocumentSendChange")]
        public IActionResult PublicRole04DocumentSendChange([FromBody]SendChangeAddModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole04DocumentSendChange(payload);
            return Json(new BaseResponseView<BaseView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole05DocumentSendChange")]
        public IActionResult PublicRole05DocumentSendChange([FromBody]List<SendChangeAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole05DocumentSendChange(payload);
            return Json(new BaseResponseView<List<SendChangeView>>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<Save010ProcessView>), 200)]
        [HttpPost]
        [Route("PublicRole01CheckSend/{id}")]
        public IActionResult PublicRole01CheckSend(string id) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole01CheckSend(Convert.ToInt64(id));
            return Json(new BaseResponseView<Save010ProcessView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("PublicRole01CheckSave")]
        public IActionResult PublicRole01CheckSave([FromBody]Save010AddModel payload) {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole01CheckSave(payload);

            return PublicRole01CheckLoad(payload.id.ToString());
        }


        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicItemView>>), 200)]
        [HttpPost]
        [Route("PublicRole01ItemAutoSplit")]
        public IActionResult PublicRole01ItemAutoSplit() {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole01ItemAutoSplit();
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicItemView>>), 200)]
        [HttpPost]
        [Route("PublicRole01ItemList")]
        public IActionResult PublicRole01ItemList([FromBody]PageRequest payload) {
            var service = new BaseService<vPublicItem, vPublicItemView>(_configuration, _uowProvider, _mapper);
            if (payload.filter_queries == null) payload.filter_queries = new List<string>();
            var result = service.List(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicItemView>>), 200)]
        [HttpPost]
        [Route("PublicRole01CheckList")]
        public IActionResult PublicRole01CheckList([FromBody]PageRequest payload) {
            var service = new BaseService<vPublicItem, vPublicItemView>(_configuration, _uowProvider, _mapper);
            if (payload.filter_queries == null) payload.filter_queries = new List<string>();

            payload.search_by.Add(new SearchByModel() {
                key = "public_type_code",
                value = "PUBLIC",
            });
            //TODO
            //x.public_receiver_by == userId &&

            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole01CheckDoAuto")]
        public IActionResult PublicRole01CheckDoAuto([FromBody]List<vPublicItemAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole01CheckDoAuto(payload);
            return Json(new BaseResponseView<List<vPublicItemView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicItemView>>), 200)]
        [HttpPost]
        [Route("PublicRole01Case41CheckList")]
        public IActionResult PublicRole01Case41CheckList([FromBody]PageRequest payload) {
            var service = new BaseService<vPublicRoundCase41, vPublicRoundCase41View>(_configuration, _uowProvider, _mapper);

            //TODO
            //x.public_receiver_by == userId &&

            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicInstructionList")]
        public IActionResult PublicInstructionList([FromBody]PageRequest payload) {
            var service = new BaseService<vSave010InstructionRule, vSave010InstructionRuleView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RequestDocumentCollectList")]
        public IActionResult RequestDocumentCollectList([FromBody]PageRequest payload) {
            var service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(_configuration, _uowProvider, _mapper);
            if (payload.filter_queries == null) payload.filter_queries = new List<string>();
            payload.filter_queries.Add("file_id > 0");
            var result = service.List(payload);
            return Json(result);
        }



        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CertificationFileAdd")]
        public IActionResult CertificationFileAdd([FromBody]List<Save010CertificationFileAddModel> payload) {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.CertificationFileAdd(payload);
            return Json(new BaseResponseView<List<vSave010CertificationFileView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("CertificationFileList")]
        public IActionResult CertificationFileList([FromBody]PageRequest payload) {
            var service = new BaseService<vSave010CertificationFile, vSave010CertificationFileView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("HistoryList")]
        public IActionResult HistoryList([FromBody]PageRequest payload) {
            var service = new BaseService<vSave010History, vSave010HistoryView>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vReceiptItemView>>), 200)]
        [HttpPost]
        [Route("ReceiptList")]
        public IActionResult ReceiptList([FromBody]PageRequest payload) {
            var service = new BaseService<vReceiptItem, vReceiptItemView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel() {
                key = "",
                value = "",
            });
            var result = service.List(payload);
            return Json(result);

            //var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            //var result = service.ReceiptList(payload);
            //return Json(result);
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponseView<vPublicRole01DoingView>), 200)]
        //[HttpPost]
        [Route("PublicRoundAdd")]
        public IActionResult PublicRoundAdd() {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRoundAdd();
            return Json(new BaseResponseView<vPublicRole01DoingView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01PrepareView>>), 200)]
        [HttpPost]
        [Route("PublicRole01PrepareList")]
        public IActionResult PublicRole01PrepareList([FromBody]PageRequest payload) {
            var service = new BaseService<vPublicRole01Prepare, vPublicRole01PrepareView>(_configuration, _uowProvider, _mapper);

            //payload.search_by.Add(new SearchByModel() {
            //    key = "public_type_code",
            //    value = "PUBLIC",
            //});
            //payload.search_by.Add(new SearchByModel() {
            //    key = "",
            //    value = "",
            //});

            var result = service.List(payload);
            return Json(result);
        }



        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("PublicRole01PrepareDelete")]
        public IActionResult PublicRole01PrepareDelete([FromBody]BaseModel payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<ReceiptView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole01PrepareDelete(payload);
            return Json(new BaseResponseView<BaseView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01PrepareView>>), 200)]
        [HttpPost]
        [Route("PublicRole01DoingList")]
        public IActionResult PublicRole01DoingList([FromBody]PageRequest payload) {
            var service = new BaseService<vPublicRole01Doing, vPublicRole01DoingView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //    key = "",
            //    value = "",
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponseView<List<vPublicRequestItemView>>), 200)]
        [HttpPost]
        [Route("PublicRoundEnd")]
        public IActionResult PublicRoundEnd() {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRoundEnd();
            return Json(new BaseResponseView<List<vPublicRequestItemView>>() { data = result });
        }


        //[ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        //[HttpPost]
        //[Route("PublicRole02CheckAutoDo")]
        //public IActionResult PublicRole02CheckAutoDo([FromBody]List<vPublicRequestItemAddModel> payload) {
        //    if (!ModelState.IsValid)
        //        return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

        //    var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
        //    var result = service.PublicRole02CheckAutoDo(payload);
        //    return Json(new BaseResponseView<List<vPublicRequestItemView>>() { data = result });
        //}

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole02CheckList")]
        public IActionResult PublicRole02CheckList([FromBody]PageRequest payload) {
            var service = new BaseService<vPublicRequestItem, vPublicRequestItemView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //    key = "",
            //    value = "",
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole02ConsiderPaymentList")]
        public IActionResult PublicRole02ConsiderPaymentList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vSave010PublicPayment, vSave010PublicPaymentView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //    key = "",
            //    value = "",
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<vSave010View>), 200)]
        [HttpPost]
        [Route("PublicRole02ConsiderPaymentLoad/{id}")]
        public IActionResult PublicRole02ConsiderPaymentLoad(string id) {
            var service = new SaveProcessService<Save010AddModel, Save010, vSave010View>(_configuration, _uowProvider, _mapper);
            var result = service.Save010Load(Convert.ToInt64(id), true);

            var base_service = new BaseService<vSave010RequestGroup, vSave010RequestGroupView>(_configuration, _uowProvider, _mapper);
            var base_resutlt = base_service.List(new PageRequest() {
                search_by = new List<SearchByModel>() {
                     new SearchByModel() {
                         key = "save_id",
                         value = id,
                     }
                 }
            });
            result.full_view.request_group_list = base_resutlt.data.list;

            var payment_service = new BaseService<vSave010PublicPayment, vSave010PublicPaymentView>(_configuration, _uowProvider, _mapper);
            var payment_resutlt = payment_service.List(new PageRequest() {
                search_by = new List<SearchByModel>() {
                     new SearchByModel() {
                         key = "save_id",
                         value = id,
                     }
                 }
            });
            result.full_view.save010_public_payment = payment_resutlt.data.list.FirstOrDefault();

            return Json(new BaseResponseView<vSave010View>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole05ConsiderPaymentList")]
        public IActionResult PublicRole05ConsiderPaymentList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vSave010PublicPayment, vSave010PublicPaymentView>(_configuration, _uowProvider, _mapper);
            payload.filter_queries.Add("!string.IsNullOrEmpty(public_role05_consider_payment_status_code)");
            //payload.search_by.Add(new SearchByModel() {
            //    key = "public_role05_consider_payment_status_code",
            //    values = new List<string>() { "WAIT", "DONE" },
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole02ConsiderPaymentSend")]
        public IActionResult PublicRole02ConsiderPaymentSend([FromBody]List<vSave010PublicPaymentAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02ConsiderPaymentSend(payload);
            return Json(new BaseResponseView<List<vSave010PublicPaymentView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole05ConsiderPaymentSend")]
        public IActionResult PublicRole05ConsiderPaymentSend([FromBody]List<vSave010PublicPaymentAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole05ConsiderPaymentSend(payload);
            return Json(new BaseResponseView<List<vSave010PublicPaymentView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole02DocumentPaymentList")]
        public IActionResult PublicRole02DocumentPaymentList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vSave010PublicPayment, vSave010PublicPaymentView>(_configuration, _uowProvider, _mapper);
            payload.filter_queries.Add("!string.IsNullOrEmpty(public_role02_document_payment_status_code)");
            //payload.search_by.Add(new SearchByModel() {
            //    key = "public_role02_document_payment_status_code",
            //    values = new List<string>() {
            //        PublicRole02DocumentPaymentStatusCodeConstant.WAIT_1.ToString(),
            //        PublicRole02DocumentPaymentStatusCodeConstant.WAIT_2.ToString(),
            //        PublicRole02DocumentPaymentStatusCodeConstant.WAIT_PAYMENT_1.ToString(),
            //        PublicRole02DocumentPaymentStatusCodeConstant.WAIT_PAYMENT_2.ToString(),
            //        PublicRole02DocumentPaymentStatusCodeConstant.DONE.ToString(),
            //    },
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole02DocumentPaymentSend")]
        public IActionResult PublicRole02DocumentPaymentSend([FromBody]List<vSave010PublicPaymentAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02DocumentPaymentSend(payload);
            return Json(new BaseResponseView<List<vSave010PublicPaymentView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole02DocumentPostList")]
        public IActionResult PublicRole02DocumentPostList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vPostRound, vPostRoundView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel() {
                key = "post_round_instruction_rule_code",
                values = new List<string>() {
                    PostRoundInstructionRuleCodeConstant.RULE_5_3.ToString(),
                    PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString(),
                },
                operation = Operation.Contains,
            });
            payload.search_by.Add(new SearchByModel() {
                key = "post_round_document_post_status_code",
                values = new List<string>() {
                    PostRoundDocumentPostStatusCodeConstant.WAIT.ToString(),
                    PostRoundDocumentPostStatusCodeConstant.DONE.ToString(),
                },
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole02DocumentPostSend")]
        public IActionResult PublicRole02DocumentPostSend([FromBody]List<vPostRoundAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02DocumentPostSend(payload);
            return Json(new BaseResponseView<List<vPostRoundView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole02ActionPostList")]
        public IActionResult PublicRole02ActionPostList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vPostRound, vPostRoundView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel() {
                key = "post_round_action_post_status_code",
                values = new List<string>() {
                    PostRoundDocumentPostStatusCodeConstant.WAIT.ToString(),
                    PostRoundDocumentPostStatusCodeConstant.DONE.ToString(),
                },
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole02ActionPostSend")]
        public IActionResult PublicRole02ActionPostSend([FromBody]List<vPostRoundAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02ActionPostSend(payload);
            return Json(new BaseResponseView<List<vPostRoundView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole04itemList")]
        public IActionResult PublicRole04itemList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vPublicRegister, vPublicRegisterView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel() {
                //key = "post_round_action_post_status_code",
                //values = new List<string>() {
                //    PostRoundDocumentPostStatusCodeConstant.WAIT.ToString(),
                //    PostRoundDocumentPostStatusCodeConstant.DONE.ToString(),
                //},
            });
            var result = service.List(payload);
            return Json(result);
        }

        [AllowAnonymous]
        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRegisterView>>), 200)]
        [HttpPost]
        [Route("PublicRole04ItemAutoSplit")]
        public IActionResult PublicRole04ItemAutoSplit() {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole04ItemAutoSplit();
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole04CheckList")]
        public IActionResult PublicRole04CheckList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vPublicRegister, vPublicRegisterView>(_configuration, _uowProvider, _mapper);
            payload.filter_queries.Add("!string.IsNullOrEmpty(public_role04_receive_status_code)");
            //payload.search_by.Add(new SearchByModel() {
            //    key = "public_role04_receive_status_code",
            //    values = new List<string>() {
            //        PublicRole04ReceiveStatusCodeConstant.NOT_YET.ToString(),
            //        PublicRole04ReceiveStatusCodeConstant.WAIT.ToString(),
            //        PublicRole04ReceiveStatusCodeConstant.DONE.ToString(),
            //    },
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole04CheckSend")]
        public IActionResult PublicRole04CheckSend([FromBody]List<vPublicRegisterAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole04CheckSend(payload);
            return Json(new BaseResponseView<List<vPublicRegisterView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole05DocumentList")]
        public IActionResult PublicRole05DocumentList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vPublicRegister, vPublicRegisterView>(_configuration, _uowProvider, _mapper);

            if (payload.filter_queries == null) payload.filter_queries = new List<string>();
            payload.filter_queries.Add("!string.IsNullOrEmpty(public_role05_status_code)");
            //payload.search_by.Add(new SearchByModel() {
            //    key = "public_role05_status_code",
            //    values = new List<string>() {
            //        PublicRole05StatusCodeConstant.WAIT.ToString(),
            //        PublicRole05StatusCodeConstant.DONE.ToString(),
            //    },
            //});

            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole05DocumentSend")]
        public IActionResult PublicRole05DocumentSend([FromBody]List<vPublicRegisterAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole05DocumentSend(payload);
            return Json(new BaseResponseView<List<vPublicRegisterView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole04DocumentList")]
        public IActionResult PublicRole04DocumentList([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vPublicRegister, vPublicRegisterView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel() {
                key = "public_role04_document_status_code",
                values = new List<string>() {
                    PublicRole04DocumentStatusCodeConstant.WAIT.ToString(),
                    PublicRole04DocumentStatusCodeConstant.DONE.ToString(),
                },
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole04DocumentSend")]
        public IActionResult PublicRole04DocumentSend([FromBody]List<vPublicRegisterAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole04DocumentSend(payload);
            return Json(new BaseResponseView<List<vPublicRegisterView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("PublicRole02DocumentList")]
        public IActionResult PublicRole02DocumentList([FromBody]PageRequest payload) {
            var service = new BaseService<vPublicRegister, vPublicRegisterView>(_configuration, _uowProvider, _mapper);
            payload.search_by.Add(new SearchByModel() {
                key = "register_post_round_instruction_rule_code",
                value = PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString(),
            });
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole02DocumentSend")]
        public IActionResult PublicRole02DocumentSend([FromBody]List<vPublicRegisterAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02DocumentSend(payload);
            return Json(new BaseResponseView<List<vPublicRegisterView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("SaveList")]
        public IActionResult SaveList([FromBody]PageRequest payload) {
            var service = new BaseService<vSave010, vSave010View>(_configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("List/{view_name}")]
        public IActionResult List(string view_name, [FromBody]PageRequest payload) {
            var service = BaseController.GetService(view_name, _configuration, _uowProvider, _mapper);
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("PublicOtherLoad/{id}")]
        public IActionResult PublicOtherLoad(string id) {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicOtherLoad(Convert.ToInt32(id));

            return Json(new BaseResponseView<PagePublicOtherView>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<BaseView>), 200)]
        [HttpPost]
        [Route("PublicConsideringOtherLoad/{id}")]
        public IActionResult PublicConsideringOtherLoad(string id) {
            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicConsideringOtherLoad(Convert.ToInt32(id));

            return Json(new BaseResponseView<PagePublicConsideringOtherView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole05OtherSend")]
        public IActionResult PublicRole05OtherSend([FromBody]List<vPublicOtherAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole05OtherSend(payload);
            return Json(new BaseResponseView<List<vPublicOtherView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole02OtherSend")]
        public IActionResult PublicRole02OtherSend([FromBody]List<vPublicOtherAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02OtherSend(payload);
            return Json(new BaseResponseView<List<vPublicOtherView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole02OtherSendChange")]
        public IActionResult PublicRole02OtherSendChange([FromBody]List<vPublicOtherAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02OtherSendChange(payload);
            return Json(new BaseResponseView<List<vPublicOtherView>>() { data = result });
        }

        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("PublicRole02ConsideringOtherSend")]
        public IActionResult PublicRole02ConsideringOtherSend([FromBody]List<vPublicConsideringOtherAddModel> payload) {
            if (!ModelState.IsValid)
                return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new PublicProcessService(_configuration, _uowProvider, _mapper);
            var result = service.PublicRole02ConsideringOtherSend(payload);
            return Json(new BaseResponseView<List<vPublicConsideringOtherView>>() { data = result });
        }
    }
}
