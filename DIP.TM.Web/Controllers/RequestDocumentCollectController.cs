﻿using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Models;
using DIP.TM.Models.Pagers;
using DIP.TM.Models.Payloads;
using DIP.TM.Models.Views;
using DIP.TM.Services.Checking;
using DIP.TM.Services.Medias;
using DIP.TM.Services.Saves;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Utils;
using DIP.TM.Web.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DIP.TM.Web.Controllers {
    public class RequestDocumentCollectController : BaseApiController {
        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;
        private readonly RequestDocumentCollectService _requestDocumentCollectService;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public RequestDocumentCollectController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper, RequestDocumentCollectService requestDocumentCollectService) {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
            _requestDocumentCollectService = requestDocumentCollectService;
        }

        [ProducesResponseType(typeof(BaseResponsePageView<List<vPublicRole01DoingView>>), 200)]
        [HttpPost]
        [Route("List")]
        public IActionResult List([FromBody]PageRequest payload) {
            if (payload == null) payload = new PageRequest();

            var service = new BaseService<vRequestDocumentCollect, vRequestDocumentCollectView>(_configuration, _uowProvider, _mapper);
            //payload.search_by.Add(new SearchByModel() {
            //    key = "register_post_round_instruction_rule_code",
            //    value = PostRoundInstructionRuleCodeConstant.RULE_9_1.ToString(),
            //});
            //payload.search_by.Add(new SearchByModel() {
            //    key = "register_post_round_document_post_status_code",
            //    values = new List<string>() {
            //        PostRoundDocumentPostStatusCodeConstant.WAIT.ToString(),
            //        PostRoundDocumentPostStatusCodeConstant.DONE.ToString(),
            //    },
            //});
            var result = service.List(payload);
            return Json(result);
        }

        [ProducesResponseType(typeof(BaseResponseView<FileView>), 200)]
        [HttpPost]
        [Route("FileLoad/{id}")]
        public IActionResult FileLoad(string id) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            var service = new FileService(_configuration, _uowProvider, _mapper);
            return Json(new BaseResponseView<FileView>() { data = service.Get(Convert.ToInt32(id)) });
        }


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RequestDocumentCollectItemSave")]
        public IActionResult RequestDocumentCollectItemSave([FromBody]vRequestDocumentCollectAddModel payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new RequestDocumentCollectService(_configuration, _uowProvider, _mapper);
            var result = _requestDocumentCollectService.RequestDocumentCollectItemSave(payload);
            return Json(new BaseResponseView<vRequestDocumentCollectView>() { data = result });
        }


        [ProducesResponseType(typeof(BaseResponseView<List<BaseView>>), 200)]
        [HttpPost]
        [Route("RequestDocumentCollectItemRemove")]
        public IActionResult RequestDocumentCollectItemRemove([FromBody]vRequestDocumentCollectAddModel payload) {
            //if (!ModelState.IsValid)
            //    return Json(new BaseResponseView<BaseSaveView>() { data = null, is_error = true, error_message = ModelState.ToErrorString() }); ;

            //var service = new RequestDocumentCollectService(_configuration, _uowProvider, _mapper);
            var result = _requestDocumentCollectService.RequestDocumentCollectItemRemove(payload);
            return Json(new BaseResponseView<vRequestDocumentCollectView>() { data = result });
        }
    }
}
