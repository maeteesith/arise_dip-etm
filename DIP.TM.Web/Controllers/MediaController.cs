﻿using AutoMapper;
using DIP.TM.Uows.DataAccess;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.IO;
using System.Reflection;

namespace DIP.TM.Web.Controllers
{
    public class MediaController : BaseController
    {

        private readonly IConfiguration _configuration;
        private readonly IUowProvider _uowProvider;
        private readonly IMapper _mapper;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        /// <param name="uowProvider"></param>
        /// <param name="mapper"></param>
        public MediaController(IConfiguration configuration, IUowProvider uowProvider, IMapper mapper)
        {
            _configuration = configuration;
            _uowProvider = uowProvider;
            _mapper = mapper;
        }

        [HttpGet]
        public ActionResult Get(string url)
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("headless");
            var driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), options);
            driver.Navigate().GoToUrl(url);
            var screenshot = (driver as ITakesScreenshot).GetScreenshot();
            string phPath = "wwwroot/Contents/Media";
            var path = Path.Combine(Directory.GetCurrentDirectory(), phPath);
            var fileWithPath = "/Screenshots/" + "screenshot-" + Guid.NewGuid().ToString() + ".png";
            screenshot.SaveAsFile(path + fileWithPath);
            driver.Close();
            driver.Quit();

            string contentType;

            string FileExtension = ".png";
            contentType = ContentType(FileExtension);

            var stream = GetStream(_configuration,_configuration["BaseUrl"] + "/Contents/Media/"+fileWithPath);
            if (ReferenceEquals(stream, null))
            {
                return new EmptyResult();
            }
            else
            {
                return new FileStreamResult(stream, contentType);
            }
        }
    }
}