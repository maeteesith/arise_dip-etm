﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Web.Middlewares.Constants
{
    public static class ServerConstants
    {
        /// <summary>
        /// The header value for X-Powered-By
        /// </summary>
        public static readonly string Header = "Server";
    }
}
