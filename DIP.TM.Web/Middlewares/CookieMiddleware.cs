﻿using DIP.TM.Services.Storages;
using DIP.TM.Utils.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace DIP.TM.Web.Middlewares
{
	public class CookieMiddleware
    {
		private readonly RequestDelegate _next;

		public CookieMiddleware(RequestDelegate next)
		{
			_next = next;
		}

		public async Task Invoke(HttpContext context, ICookieService cookieService)
		{
			// write cookies to response right before it starts writing out from MVC/api responses...
			context.Response.OnStarting(() =>
			{
				// cookie service should not write out cookies on 500, possibly others as well
				if (!context.Response.StatusCode.IsInRange(500, 599))
				{
					cookieService.WriteToResponse(context);
				}
				return Task.CompletedTask;
			});

			await _next(context);
		}
	}
	public static class CookieMiddlewareExtensions
	{
		public static IApplicationBuilder UseCookieService(this IApplicationBuilder builder)
		{
			return builder.UseMiddleware<CookieMiddleware>();
		}
	}
}
