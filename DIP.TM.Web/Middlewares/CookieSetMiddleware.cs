﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Web.Middlewares
{
    public class CookieSetMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly SessionOptions _options;
        private HttpContext _context;
        public CookieSetMiddleware(RequestDelegate next, IOptions<SessionOptions> options)
        {
            _next = next;
            _options = options.Value;
        }

        public async Task Invoke(HttpContext context)
        {
            _context = context;
            context.Response.OnStarting(OnStartingCallBack);
            await _next.Invoke(context);
        }

        private Task OnStartingCallBack()
        {
            var cookieOptions = new CookieOptions()
            {
                //Path = "/",
                Expires = DateTimeOffset.UtcNow.AddHours(3),
                IsEssential = true,
                HttpOnly = false,
                Secure = false,
            };
            if(_context.Request.Cookies["DIP-TM-TOKEN"] ==null)
                _context.Response.Cookies.Append("DIP-TM-TOKEN", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1laWQiOiIxIiwibmJmIjoxNTc4NTM4ODE0LCJleHAiOjE2MTAxNjEyMTQsImlhdCI6MTU3ODUzODgxNH0.Ir5MaPVs2I_uJU34enhS45p0oUDMb-LvVzWkC4vcQMw", cookieOptions);
            return Task.FromResult(0);
        }
    }
}
