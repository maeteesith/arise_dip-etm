﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Headers;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;

namespace DIP.TM.Web.Middlewares
{
    /// <summary>
    /// 
    /// </summary>
    public class RoutePermissionMiddleware
    {
        private readonly RequestDelegate next;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="next"></param>
        public RoutePermissionMiddleware(RequestDelegate next)
        {
            this.next = next;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task Invoke(HttpContext context)
        {
            var hasPermission = true;
            var path = context.Request.Path;
            if (!ReferenceEquals(path, null))
            {
                //TODO Handle db map permissions
            }

            if (hasPermission)
                await next(context);
            else await HandleNotPermissionAsync(context);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        private static Task HandleNotPermissionAsync(HttpContext context)
        {

            var code = HttpStatusCode.NonAuthoritativeInformation;
            var result = JsonConvert.SerializeObject(new { is_error = true, error_message = "Permission denied" });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public static class RoutePermissionMiddlewareExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <returns></returns>
        public static IApplicationBuilder UseRoutePermissionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<RoutePermissionMiddleware>();
        }
    }
}
