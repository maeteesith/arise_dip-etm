﻿using DIP.TM.Utils.Logging;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace DIP.TM.Web.Middlewares
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate next;
        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            this.next = next;
        }

        public async Task Invoke(HttpContext context , IConfiguration configuration)
        {
            try
            {
                await next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, configuration, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, IConfiguration configuration, Exception exception)
        {
            // TODO
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            var errorMessages = exception.Message;

            if (exception is HttpResponseException)
            {
                code = HttpStatusCode.NotFound;
            }else if (exception is DbUpdateException)
            {
                if (!ReferenceEquals(exception.InnerException, null))
                    errorMessages = exception.InnerException.Message;
                else
                    errorMessages = exception.Message;
            }

            LineNotifyHelper.SendWait(configuration, errorMessages);

            var result = JsonConvert.SerializeObject(new { is_error=true, error_message = errorMessages });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
