﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DIP.TM.Web.Middlewares
{
    public class SecurityHeadersMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly SecurityHeadersPolicy _policy;

        public SecurityHeadersMiddleware(RequestDelegate next, SecurityHeadersPolicy policy)
        {
            _next = next;
            _policy = policy;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                IHeaderDictionary headers = context.Response.Headers;

                foreach (var headerValuePair in _policy.SetHeaders)
                {
                    headers[headerValuePair.Key] = headerValuePair.Value;
                }

                foreach (var header in _policy.RemoveHeaders)
                {
                    headers.Remove(header);
                }

                await _next(context);
            }
            catch (Exception ex)
            {
                await _next(context);
            }
        }
    }
    public class SecurityHeadersPolicy
    {
        public IDictionary<string, string> SetHeaders { get; }
             = new Dictionary<string, string>();

        public ISet<string> RemoveHeaders { get; }
            = new HashSet<string>();
    }
}
