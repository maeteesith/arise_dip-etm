using AutoMapper;
using DIP.TM.Datas;
using DIP.TM.Datas.Interceptors;
using DIP.TM.Models.Configs;
using DIP.TM.Services.Extensions;
using DIP.TM.Services.InitDbs;
using DIP.TM.Services.Logging;
using DIP.TM.Services.Storages;
using DIP.TM.Uows.DataAccess;
using DIP.TM.Uows.DataAccess.Uow;
using DIP.TM.Web.Extensions;
using DIP.TM.Web.Filters;
using DIP.TM.Web.Middlewares;
using DIP.TM.Web.Middlewares.Builders;
using HealthChecks.UI.Client;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.SpaServices.AngularCli;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;
using Rotativa.AspNetCore;
using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Extensions;
using IHostingEnvironment = Microsoft.AspNetCore.Hosting.IHostingEnvironment;

namespace DIP.TM.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {

            //var SecretKey = Encoding.ASCII.GetBytes(Configuration.GetSection("SecretKey").ToString());
            //services.AddAuthentication(auth =>
            //{
            //    auth.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    auth.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //.AddJwtBearer(token =>
            //{
            //    token.RequireHttpsMetadata = false;
            //    token.SaveToken = true;
            //    token.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(SecretKey),
            //        ValidateIssuer = true,
            //        ValidIssuer = Configuration.GetSection("BaseUrl").ToString(),
            //        ValidateAudience = true,
            //        ValidAudience = Configuration.GetSection("BaseUrl").ToString(),
            //        ValidateLifetime = true,
            //        RequireExpirationTime = true,
            //        ClockSkew = TimeSpan.Zero
            //    };
            //});

            // load config to model
            var config = Configuration.ToConfig();

            services.AddControllersWithViews(options =>
            {
                // TODO Validate in attribute
                options.Filters.Add(typeof(ValidateModelStateAttribute));
            });

            var key = Encoding.ASCII.GetBytes(config.SecretKey);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            //services.AddResponseCompression();

            services.AddOpenApiDocument(config =>
            {
                config.DocumentName = "OpenAPI 3";
                config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                //config.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT Token")); -> Replaced the line above with this with no difference
                config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                    new NSwag.OpenApiSecurityScheme()
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = nameof(Authorization),
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Copy this into the value field: Bearer {token}"
                    }
                );
            });

            // Register the Swagger services
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "DIP API";
                    document.Info.Description = "DIP ASP.NET Core web API";
                    document.Info.TermsOfService = "None";
                    document.Schemes = new[] { OpenApiSchema.Https, OpenApiSchema.Http };
                };

                config.AddSecurity("JWT Bearer Token", Enumerable.Empty<string>(),
                    new NSwag.OpenApiSecurityScheme()
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = nameof(Authorization),
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Copy this into the value field: Bearer {token}"
                    }
                );
            });
            services
                .AddFluentEmail(config.Email.From, config.Email.Name)
                .AddSmtpSender(PrepareSmtpClientSsl(config));




            var connection = BuildConnectionString(services);

            services.AddEntityFrameworkProxies();
            services.AddDbContext<DIPTMContext>(opt =>
            {
                opt.UseSqlServer(connection.ToString(), sqlServerOptions => sqlServerOptions.CommandTimeout(config.ConnectionStrings.DefaultConnection.Timeout));
                opt.AddInterceptors(new HintCommandInterceptor());
                opt.UseLazyLoadingProxies(true);
            });

            services.AddDataAccess<DIPTMContext>();


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ICookieService, CookieService>();
            services.AddTransient<ApiLogService>();


            // cache in memory
            services.AddMemoryCache();
            // caching response for middlewares
            services.AddResponseCaching();



            /*services.AddHealthChecks()
                .AddSqlServer(connectionString: connection.ToString(),
              healthQuery: "SELECT 1;",
              name: "sql",
              failureStatus: HealthStatus.Degraded,
              tags: new string[] { "db", "sql", "sqlserver" });*/

            //services.AddHealthChecksUI();

            // enable cors
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder => builder.AllowAnyOrigin()
                      .AllowAnyMethod()
                      .AllowAnyHeader()
                      .AllowCredentials()
                .Build());
            });

            services.AddAutoMapper(typeof(Startup));

            //services.AddSwaggerGen(c =>
            //{
            //    c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo { Title = "DIP API", Version = "v1" });
            //    //var basePath = AppContext.BaseDirectory;
            //    //var xmlPath = Path.Combine(basePath, "MyApi.xml");
            //    //c.IncludeXmlComments(xmlPath);

            //});

            /*CultureInfo[] supportedCultures = new[]
               {
                new CultureInfo("th"),
                new CultureInfo("en")
            };

            services.Configure<RequestLocalizationOptions>(options =>
            {
                options.DefaultRequestCulture = new RequestCulture("th");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.RequestCultureProviders = new List<IRequestCultureProvider>
                {
                    new QueryStringRequestCultureProvider(),
                    new CookieRequestCultureProvider()
                };

            });*/


            // In production, the Angular files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/dist/dip-app";
            });

            // map path provider
            if (config.ContentPath.BasePath != string.Empty)
            {
                services.AddSingleton<IFileProvider>(new PhysicalFileProvider(config.ContentPath.BasePath));
            }

            services.Configure<BrotliCompressionProviderOptions>(options =>
            {
                options.Level = CompressionLevel.Fastest;
            });


            services.Configure<CookiePolicyOptions>(options =>
            {
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddResponseCompression(options =>
            {
                IEnumerable<string> MimeTypes = new[]
               {
                     // General
                     "text/plain",
                     "text/html",
                     "text/css",
                     "font/woff2",
                     "application/javascript",
                     "image/x-icon",
                     "image/png"
                 };

                options.EnableForHttps = true;
                options.ExcludedMimeTypes = MimeTypes;
                //options.Providers.Add<GzipCompressionProvider>();
                options.Providers.Add<BrotliCompressionProvider>();
            });


            services.AddSession(options =>
            {
                options.IdleTimeout = TimeSpan.FromHours(60);
            });

            services.AddHttpCustomContextAccessor();


            services.AddDistributedMemoryCache();



            // If using Kestrel:
            //services.Configure<KestrelServerLimits>(options =>
            //{
            //    options.MaxRequestLineSize = 8192000;
            //});

            // If using IIS:
            services.Configure<IISServerOptions>(options =>
            {
                options.AllowSynchronousIO = true;
                options.MaxRequestBodySize = null;
            });






            // Recheck config and create folder

            services.ConfigContentFolders(config);


            // DI Configuration
            /*Action<AppConfigModel> appConfig = (opt =>
            {
                opt = config;
            });

            services.Configure(appConfig);
            services.AddSingleton(resolver => resolver.GetRequiredService<IOptions<AppConfigModel>>().Value);*/


            // compare db with model
            services.CompareDatabaseWithModel();


            // TODO Remove
            var _uowProvider = (IUowProvider)services.BuildServiceProvider().GetServices(typeof(IUowProvider)).FirstOrDefault();
            new BootService(_uowProvider).BootDatabase();


            services.ConfigAspectCore();

        }

        private SmtpClient PrepareSmtpClientSsl(AppConfigModel config)
        {
            //var client= new SmtpClient()
            //{
            //    Host= configuration["Email:Host"],
            //    Port= int.Parse(configuration["Email:Port"]),
            //    UseDefaultCredentials = false,
            //    //DeliveryMethod = SmtpDeliveryMethod.Network,
            //    Credentials = new NetworkCredential(configuration["Email:Username"], configuration["Email:Password"]),
            //    EnableSsl = true,
            //};


            SmtpClient smtp = new SmtpClient();
            smtp.Host = config.Email.Host;
            smtp.EnableSsl = true;
            NetworkCredential NetworkCred = new NetworkCredential(config.Email.Username, config.Email.Password);
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = config.Email.Port;


            return smtp;
        }

        private ConnectionString BuildConnectionString(IServiceCollection services)
        {
            var section = Configuration.GetSection("ConnectionStrings:DefaultConnection");
            services.Configure<ConnectionString>(section);

            var configureOptions = services.BuildServiceProvider().GetRequiredService<IConfigureOptions<ConnectionString>>();
            var connectionString = new ConnectionString();
            configureOptions.Configure(connectionString);
            return connectionString;
        }

        [Obsolete]
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            var configModel = Configuration.ToConfig();
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                // caching response for middlewares
                // TODO caching only develop
                app.UseResponseCaching();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                app.UseHsts();
                //app.UseHttpsRedirection();
            }

            //app.UseSwagger();
            //app.UseSwaggerUI(options =>
            //{
            //    options.SwaggerEndpoint("/swagger/v1/swagger.json", "DIP API");
            //});

            //app.UseHttpsRedirection();
            app.UseStaticFiles();/* (new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    // Cache static files for 30 days
                    ctx.Context.Response.Headers.Append("Cache-Control", "public,max-age=2592000");
                    ctx.Context.Response.Headers.Append("Expires", DateTime.UtcNow.AddDays(30).ToString("R", CultureInfo.InvariantCulture));
                }
            });*/

            if (configModel.ContentPath.BasePath != string.Empty)
            {
                app.UseStaticFiles(new StaticFileOptions
                {
                    FileProvider = new PhysicalFileProvider(configModel.ContentPath.BasePath),
                    RequestPath = "/files"
                });
            }

            if (!env.IsDevelopment())
            {
                app.UseSpaStaticFiles();

            }

            //app.UseCorsMiddleware();
            //app.UseCors("CorsPolicy");

            app.UseSession();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
            });

            app.UseSecurityHeadersMiddleware(new SecurityHeadersBuilder()
              .AddDefaultSecurePolicy()
              .AddCustomHeader("X-Api-Version", Configuration["Version"])
              .AddCustomHeader("X-Host-Name", Dns.GetHostName())
            );


            app.UseErrorHandlingMiddleware();

            app.UseRoutePermissionMiddleware();

            app.UseAuthentication();


            app.UseStaticHttpContext();

            app.UseResponseCompression();

            /*if (!env.IsDevelopment())
            {
                app.UseMiddleware<ApiLoggingMiddleware>();
            }*/

            if (configModel.EnableAuditLog)
                app.UseMiddleware<ApiLoggingMiddleware>();

            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(config =>
                {
                    /*config.MapHealthChecks("/health", new HealthCheckOptions
                    {
                        Predicate = _ => true,
                        ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                    });*/
                    //config.MapHealthChecksUI();
                });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            // Register the Swagger generator and the Swagger UI middlewares
            app.UseOpenApi();
            app.UseSwaggerUi3();

            //app.UseRequestLocalization();


            // TODO Fix Cookie Token
            // app.UseMiddleware<CookieSetMiddleware>();




            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {

                    // for angular+dotnet core
                    spa.Options.StartupTimeout = TimeSpan.FromSeconds(200);
                    spa.UseAngularCliServer(npmScript: "start");
                    

                    // for seperate ui and api
                    //spa.UseProxyToSpaDevelopmentServer("http://localhost:4200");
                }
            });




            RotativaConfiguration.Setup(env, "Rotativa");
        }
    }
}
