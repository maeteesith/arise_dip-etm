﻿using Microsoft.EntityFrameworkCore;

namespace DIP.TM.Uows.DataAccess.Repositories
{
    public interface IRepositoryInjection
    {
        IRepositoryInjection SetContext(DbContext context);
    }
}