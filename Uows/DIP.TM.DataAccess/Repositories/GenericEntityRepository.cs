﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using DIP.TM.Uows.DataAccess.Entities;

namespace DIP.TM.Uows.DataAccess.Repositories
{
    public class GenericEntityRepository<TEntity> : EntityRepositoryBase<DbContext, TEntity> where TEntity : EntityBase, new()
    {
		public GenericEntityRepository(ILogger<DataAccess> logger) : base(logger, null)
		{ }
	}
}