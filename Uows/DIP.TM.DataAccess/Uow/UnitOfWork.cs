﻿using System;
using DIP.TM.Uows.DataAccess;
using Microsoft.EntityFrameworkCore;

namespace DIP.TM.Uows.DataAccess.Uow
{
    public class UnitOfWork : UnitOfWorkBase<DbContext>, IUnitOfWork
    {
        public UnitOfWork(DbContext context, IServiceProvider provider) : base(context, provider)
        { }
    }
}
