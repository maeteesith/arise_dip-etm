﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DIP.TM.Uows.DataAccess.Entities;
using DIP.TM.Uows.DataAccess.Repositories;
using Microsoft.EntityFrameworkCore;

namespace DIP.TM.Uows.DataAccess
{
    public interface IUnitOfWork : IDisposable
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        IRepository<TEntity> GetRepository<TEntity>();
        TRepository GetCustomRepository<TRepository>();

        DbContext GetContext<TRepository>();

        void OnCompleteTransaction(Action func);
    }
}
