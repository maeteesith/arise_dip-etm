﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DIP.TM.Uows.DataAccess.Exceptions;
using DIP.TM.Uows.DataAccess.Repositories;
using DIP.TM.Uows.DataAccess.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace DIP.TM.Uows.DataAccess.Uow
{
    public abstract class UnitOfWorkBase<TContext> : IUnitOfWorkBase where TContext : DbContext
    {
        private List<Action> OnCommitActions;
        protected internal UnitOfWorkBase(TContext context, IServiceProvider serviceProvider)
        {
            _context = context;
            _serviceProvider = serviceProvider;
        }

        protected TContext _context;
        protected readonly IServiceProvider _serviceProvider;

        public int SaveChanges()
        {
            CheckDisposed();
            var iSaveChanged= _context.SaveChanges();

            if (ReferenceEquals(null, OnCommitActions))
                return iSaveChanged;

            foreach (var onCommitAction in OnCommitActions)
                onCommitAction.Invoke();
            OnCommitActions.Clear();

            return iSaveChanged;
        }

        public Task<int> SaveChangesAsync()
        {
            CheckDisposed();
            var iSaveChanged = _context.SaveChangesAsync();

            if (ReferenceEquals(null, OnCommitActions))
                return iSaveChanged;

            foreach (var onCommitAction in OnCommitActions)
                onCommitAction.Invoke();
            OnCommitActions.Clear();

            return iSaveChanged;
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            CheckDisposed();
            var iSaveChanged= _context.SaveChangesAsync(cancellationToken);

            if (ReferenceEquals(null, OnCommitActions))
                return iSaveChanged;

            foreach (var onCommitAction in OnCommitActions)
                onCommitAction.Invoke();
            OnCommitActions.Clear();

            return iSaveChanged;
        }

        public DbContext GetContext<TEntity>()
        {
            return _context;
        }

        public void OnCompleteTransaction(Action func)
        {
            if (ReferenceEquals(OnCommitActions, null))
                OnCommitActions = new List<Action>();

            OnCommitActions.Add(func);
        }

        public IRepository<TEntity> GetRepository<TEntity>()
        {
            CheckDisposed();
            var repositoryType = typeof(IRepository<TEntity>);
            var repository = (IRepository<TEntity>)_serviceProvider.GetService(repositoryType);
            if (repository == null)
            {
                throw new RepositoryNotFoundException(repositoryType.Name, String.Format("Repository {0} not found in the IOC container. Check if it is registered during startup.", repositoryType.Name));
            }

            ((IRepositoryInjection)repository).SetContext(_context);
            return repository;
        }

        public TRepository GetCustomRepository<TRepository>()
        {
            CheckDisposed();
            var repositoryType = typeof(TRepository);
            var repository = (TRepository)_serviceProvider.GetService(repositoryType);
            if (repository == null)
            {
                throw new RepositoryNotFoundException(repositoryType.Name, String.Format("Repository {0} not found in the IOC container. Check if it is registered during startup.", repositoryType.Name));
            }

            ((IRepositoryInjection)repository).SetContext(_context);
            return repository;
        }

        #region IDisposable Implementation

        protected bool _isDisposed;

        protected void CheckDisposed()
        {
            if (_isDisposed) throw new ObjectDisposedException("The UnitOfWork is already disposed and cannot be used anymore.");
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_isDisposed)
            {
                if (disposing)
                {
                    if (_context != null)
                    {
                        _context.Dispose();
                        _context = null;
                    }
                }
            }
            _isDisposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public void CompleteTransaction()
        {
            throw new NotImplementedException();
        }

        ~UnitOfWorkBase()
        {
            Dispose(false);
        }

        #endregion
    }
}