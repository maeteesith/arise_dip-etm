using System;
using System.Threading;
using System.Threading.Tasks;
using DIP.TM.Uows.DataAccess.Entities;
using DIP.TM.Uows.DataAccess.Repositories;


namespace DIP.TM.Uows.DataAccess.Uow
{
    public interface IUnitOfWorkBase : IDisposable
    {
        int SaveChanges();
        Task<int> SaveChangesAsync();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);

        IRepository<TEntity> GetRepository<TEntity>();
        TRepository GetCustomRepository<TRepository>();

        void OnCompleteTransaction(Action func);

    }
}