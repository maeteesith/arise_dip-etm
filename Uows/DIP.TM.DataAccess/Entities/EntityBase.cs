using System;
using System.ComponentModel.DataAnnotations;

namespace DIP.TM.Uows.DataAccess.Entities {
    public class EntityBase {
        // This is the base class for all entities.
        // The DataAccess repositories have this class as constraint for entities that are persisted in the database.

        [Key]
        public long id { get; set; }
        public bool is_deleted { get; set; }
        // public DateTime? created_date { get; set; }
        // public long? created_by { get; set; }
        // public DateTime? updated_date { get; set; }
        // public long? updated_by { get; set; }
    }
}
