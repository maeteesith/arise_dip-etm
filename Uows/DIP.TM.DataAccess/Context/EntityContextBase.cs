﻿using Microsoft.EntityFrameworkCore;

namespace DIP.TM.Uows.DataAccess.Context
{
    public class EntityContextBase<TContext> : DbContext, IEntityContext where TContext : DbContext
    {
        public EntityContextBase(DbContextOptions<TContext> options) : base(options)
        {
        }
    }
}
