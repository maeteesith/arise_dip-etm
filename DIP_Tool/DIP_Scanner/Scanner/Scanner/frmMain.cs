﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;
using WIA;

namespace Scanner
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }
        public static HttpListener listener;
        string CONFIG_PATH = "config.txt";
        ScannerConfig scanner = new ScannerConfig();

        private void frmMain_Load(object sender, EventArgs e)
        {
            //Scan();
            //Close();

            XmlSerializer _serializer = new XmlSerializer(typeof(ScannerConfig));
            if (File.Exists(CONFIG_PATH))
            {
                using (FileStream _stream = new FileStream(CONFIG_PATH, FileMode.Open))
                {
                    scanner = (ScannerConfig)_serializer.Deserialize(_stream);
                }
            }
            else
            {
                using (FileStream _stream = new FileStream(CONFIG_PATH, FileMode.Create))
                {
                    _serializer.Serialize(_stream, scanner);
                }

            }

            listener = new HttpListener();
            listener.Prefixes.Add(scanner.listen_url);
            listener.Start();

            Console.WriteLine("Listening for connections on {0}", scanner.listen_url);

            thread = new Thread(HandleIncomingConnections);
            thread.Start();

            //niMain.Visible = true;
            niMain.ContextMenu = new ContextMenu();
            niMain.ContextMenu.MenuItems.Add(new MenuItem("E&xit", ExitHandle));

            //this.Visible = false;
            //this.Hide();
            BeginInvoke(new MethodInvoker(delegate
            {
                Hide();
            }));
        }

        private void ExitHandle(object sender, EventArgs e)
        {
            //this.Visible = false;
            //this.Hide();
            Close();
        }

        Thread thread;
        bool runServer = true;
        private void HandleIncomingConnections()
        {

            // While a user hasn't visited the `shutdown` url, keep on handling requests
            while (runServer)
            {
                try
                {
                    // Will wait here until we hear from a connection
                    // HttpListenerContext ctx = await listener.GetContextAsync();
                    HttpListenerContext ctx = listener.GetContext();
                    // Peel out the requests and response objects
                    HttpListenerRequest req = ctx.Request;
                    HttpListenerResponse resp = ctx.Response;

                    // Print out some info about the request
                    Console.WriteLine(req.Url.ToString());
                    Console.WriteLine(req.HttpMethod);
                    Console.WriteLine(req.UserHostName);
                    Console.WriteLine(req.UserAgent);
                    var text = "";
                    using (var reader = new StreamReader(req.InputStream,
                                         req.ContentEncoding))
                    {
                        text = reader.ReadToEnd();
                    }
                    //  Console.WriteLine(text);
                    Console.WriteLine("method :" + req.QueryString["method"]);
                    resp.ContentType = "Application/json";
                    resp.ContentEncoding = Encoding.UTF8;

                    resp.Headers.Add("Access-Control-Allow-Origin", "*");
                    resp.Headers.Add("Access-Control-Allow-Credentials", "true");
                    resp.Headers.Add("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                    resp.Headers.Add("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");

                    var method = req.QueryString["scan_type"];
                    try
                    {
                        Scan(resp, method);
                    }
                    catch (Exception _ex)
                    {
                        var data = Encoding.UTF8.GetBytes("{ \"version\": \"0.0.1\", \"event_log_id\": 0, \"data\": null, \"is_error\": true, \"error_code\": 0, \"error_message\": \"" + _ex.Message + "\" }");
                        //resp.StatusCode = (int)HttpStatusCode.NoContent;
                        resp.OutputStream.WriteAsync(data, 0, data.Length);
                        File.AppendAllText("log.txt", "2 " + _ex.Message + Environment.NewLine, Encoding.UTF8);
                        resp.Close();
                    }

                    // Write the response info
                }
                catch (Exception e)
                {
                    File.AppendAllText("log.txt", "1 " + e.Message, Encoding.UTF8);
                }
            }
        }

        private static void SetWIAProperty(IProperties properties, object propName, object propValue)
        {
            Property prop = properties.get_Item(ref propName);
            prop.set_Value(ref propValue);
        }

        void Scan(HttpListenerResponse resp, string method)
        {
            // Create a DeviceManager instance
            var deviceManager = new DeviceManager();

            // Create an empty variable to store the scanner instance
            DeviceInfo firstScannerAvailable = null;

            // Loop through the list of devices to choose the first available
            for (int i = 1; i <= deviceManager.DeviceInfos.Count; i++)
            {
                // Skip the device if it's not a scanner
                if (deviceManager.DeviceInfos[i].Type != WiaDeviceType.ScannerDeviceType)
                {
                    continue;
                }

                firstScannerAvailable = deviceManager.DeviceInfos[i];

                break;
            }

            // Connect to the first available scanner
            var device = firstScannerAvailable.Connect();

            // Select the scanner
            var scannerItem = device.Items[1];
            SetWIAProperty(scannerItem.Properties, "6147", 100);
            SetWIAProperty(scannerItem.Properties, "6148", 100);

            // Retrieve a image in JPEG format and store it into a variable
            int _i = 0;
            string _file_name = "";
            int page_max = method == "PDF" ? int.MaxValue : 1;
            try
            {
                for (_i = 0; _i < page_max; _i++)
                {
                    var imageFile = (ImageFile)scannerItem.Transfer(FormatID.wiaFormatJPEG);
                    _file_name = scanner.jpg_path.Replace("###", (_i + 1).ToString("000"));
                    // Save the image in some path with filename

                    if (File.Exists(_file_name))
                    {
                        File.Delete(_file_name);
                    }

                    // Save image !
                    imageFile.SaveFile(_file_name);
                }
            }
            finally
            {
                if (_i > 0)
                {
                    if (method != "PDF")
                    {
                        using (WebClient client = new WebClient())
                        {
                            var data = client.UploadFile(scanner.web_server_url, _file_name);
                            //string _d = Encoding.UTF8.GetString(bytes);
                            resp.StatusCode = (int)HttpStatusCode.OK;
                            resp.OutputStream.WriteAsync(data, 0, data.Length);
                            resp.Close();
                        }
                    }
                    else
                    {
                        PdfDocument document = new PdfDocument();

                        for (int _j = 0; _j < _i; _j++)
                        {
                            PdfPage page = document.AddPage();
                            XGraphics gfx = XGraphics.FromPdfPage(page);
                            _file_name = scanner.jpg_path.Replace("###", (_j + 1).ToString("000"));
                            XImage image = XImage.FromFile(_file_name);
                            gfx.DrawImage(image, new XPoint(0, 0));
                            image.Dispose();
                        }

                        document.Save(scanner.pdf_path);
                        document.Close();

                        using (WebClient client = new WebClient())
                        {
                            var data = client.UploadFile(scanner.web_server_url, scanner.pdf_path);
                            //string _d = Encoding.UTF8.GetString(bytes);
                            resp.StatusCode = (int)HttpStatusCode.OK;
                            resp.OutputStream.WriteAsync(data, 0, data.Length);
                        }
                        resp.Close();
                    }
                }
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            runServer = false;
            //WebClient _client = new WebClient();
            //_client.
            new WebClient().DownloadString(scanner.listen_url);
        }

        private void niMain_MouseDoubleClick(object sender, MouseEventArgs e)
        {

        }
    }
}
