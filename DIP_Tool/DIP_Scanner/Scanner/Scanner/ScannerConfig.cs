﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Scanner {
    public class ScannerConfig {
        public string jpg_path { set; get; } = @"C:\scan_###.jpeg";
        public string pdf_path { set; get; } = @"C:\scan.pdf";
        public string listen_url { set; get; } = "http://127.0.0.1:8081/";
        //public string web_server_url { set; get; } = "http://localhost:5000/api/upload";
        public string web_server_url { set; get; } = "https://tmuat.ipthailand.go.th/api/upload";
    }
}
