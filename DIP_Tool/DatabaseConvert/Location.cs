﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace DatabaseConvert {
    public class DBLocation {
        public int index { set; get; }
        public string buffer_04 { set; get; }
        public string province_code { set; get; }
        public string province_name { set; get; }
        public string district_code { set; get; }
        public string district_name { set; get; }
        public string district_sub_code { set; get; }
        public string district_sub_name { set; get; }

        public static void Convert() {
            string _raw = File.ReadAllText("CTLT_LOCATION_DATA_TABLE.csv");
            string[] _line_list = _raw.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            List<DBLocation> location_list = new List<DBLocation>();
            string[] _column_list = _line_list[0].Split(new string[] { "," }, StringSplitOptions.None);
            for (int _i = 1; _i < _line_list.Length; _i++) {
                string[] _line_element_list = _line_list[_i].Split(new string[] { "," }, StringSplitOptions.None);

                var location = new DBLocation() {
                    index = _i,
                    province_code = _line_element_list[7].Replace("\"", ""),
                    province_name = _line_element_list[9].Replace("\"", "").Replace("จังหวัด", "").Trim(),
                    district_code = _line_element_list[6].Replace("\"", ""),
                    district_name = _line_element_list[10].Replace("\"", "").Replace("อำเภอ", "").Replace("เขต", "").Trim(),
                    district_sub_code = _line_element_list[1].Replace("\"", ""),
                    district_sub_name = _line_element_list[11].Replace("\"", "").Replace("ตำบล", "").Replace("แขวง", "").Trim(),
                };

                location_list.Add(location);
            }

            //var province_list = location_list.Where(r => !string.IsNullOrEmpty(r.province_code) && !string.IsNullOrEmpty(r.province_name)).ToList();
            //var province_distinct_list = province_list.GroupBy(r => r.province_code).Select(r => new {
            //    code = r.Key,
            //    name = r.Max(s => s.province_name),
            //}).ToList();
            //File.WriteAllLines("province.sql", province_distinct_list.Select(r => "INSERT [dbo].[RM_AddressProvince] ([country_code], [code], [name]) VALUES ('TH', '" + r.code + "', '" + r.name + "');"));

            //var district_list = location_list.Where(r => !string.IsNullOrEmpty(r.district_code) && !string.IsNullOrEmpty(r.district_name) && !string.IsNullOrEmpty(r.province_code)).ToList();
            //var district_distinct_list = district_list.GroupBy(r => r.district_code).Select(r => new {
            //    province_code = r.Max(s => s.province_code),
            //    code = r.Key,
            //    name = r.Max(s => s.province_name),
            //}).ToList();
            //File.WriteAllLines("district.sql", district_distinct_list.Select(r => "INSERT [dbo].[RM_AddressDistrict] ([province_code], [code], [name]) VALUES ('" + r.province_code + "', '" + r.code + "', '" + r.name + "');"));

            var district_sub_list = location_list.Where(r => !string.IsNullOrEmpty(r.district_sub_code) && !string.IsNullOrEmpty(r.district_sub_name) && !string.IsNullOrEmpty(r.district_code)).ToList();
            var district_sub_distinct_list = district_sub_list.GroupBy(r => r.district_sub_code).Select(r => new {
                district_code = r.Max(s => s.district_code),
                code = r.Key,
                name = r.Max(s => s.district_sub_name),
            }).ToList();
            File.WriteAllLines("district_sub.sql", district_sub_distinct_list.Select(r => "INSERT [dbo].[RM_AddressSubDistrict] ([district_code], [code], [name]) VALUES ('" + r.district_code + "', '" + r.code + "', '" + r.name + "');"));

        }
    }
}