﻿using System;
using System.Windows.Forms;

namespace DatabaseConvert {
    public partial class frmMain : Form {
        public frmMain() {
            InitializeComponent();
        }

        private void frmMain_Load(object sender, EventArgs e) {
            DBLocation.Convert();
        }
    }
}
