﻿using EDC_KTB_DIP.Lib;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace EDC_KTB_DIP {
    public class EDCPort {
        SerialPort port;

        public string COM { set; get; } = "COM3";
        public int BAUD_RATE { set; get; } = 9600;
        public Parity PARITY { set; get; } = Parity.None;
        public int DATA_BIT { set; get; } = 8;
        public StopBits STOP_BITS { set; get; } = StopBits.One;
        public int READ_TIMEOUT_SECOND { set; get; } = 60;

        //public string STX { set; get; } = "02";
        //public string VERSION { set; get; } = "1";
        //public string ETX { set; get; } = "03";
        //public string ACKNOWLEDGE { set; get; } = "06";
        //public string ACKNOWLEDGE { set; get; } = "06";

        public char STX { set; get; } = (char)0x02;
        public string VERSION { set; get; } = "1";
        public char ETX { set; get; } = (char)0x03;
        public char ACKNOWLEDGE { set; get; } = (char)0x06;
        public string MORE_DATA_INDICATOR { set; get; } = "0";
        public char FIELD_SEPARATOR { set; get; } = (char)0x1C;
        public int RESERVE_FOR_FURFURES_USE_LENGTH { set; get; } = 10;

        public string recieveHex { set; get; }

        public string totalPrice { get; set; }

        internal EDCRcvModel Decode(string _data) {
            ////Reserve for Furfures Use
            //string _payload = ("").PadLeft(10, '0');
            ////Presentation Header
            //_payload += VERSION;
            //_payload += MESSAGE_TYPE.REQUEST;
            //_payload += TRANSACTION_CODE.QR_CODE;
            //_payload += RESPONSE_CODE.COMPLETE;
            //_payload += MORE_DATA_INDICATOR;
            //_payload += FIELD_SEPARATOR.ToString();
            ////Field Data
            //_payload += FIELD_TYPE.REQUEST_AMOUNT;
            //_payload += CalculateBCD(12, 4);
            //_payload += _amount.ToString("0.00").Replace(".", "").PadLeft(12, '0');
            //_payload += FIELD_SEPARATOR.ToString();
            ////Field Data
            //_payload += FIELD_TYPE.REQUEST_REF_1;
            //_payload += CalculateBCD(20, 4);
            //_payload += ("1").PadLeft(20, '0');
            //_payload += FIELD_SEPARATOR.ToString();

            //DATA
            //string _data = STX.ToString();
            //_data += CalculateBCD(_payload.Length, 4);
            //_data += _payload;
            //_data += ETX.ToString();
            //_data += CalculateLRC(_data).ToString();

            //byte[] _data_bytes = Encoding.ASCII.GetBytes(_data);

            //string _hex = BitConverter.ToString(_data_bytes);
            //File.AppendAllText("log.txt", DateTime.Now.ToString("yyyyMMdd HHmmss") + ": 1 - " + _hex + Environment.NewLine, Encoding.UTF8);

            //if (!port.IsOpen) port.Open();
            //port.ReadTimeout = READ_TIMEOUT_SECOND * 1000;
            //port.Write(_data_bytes, 0, _data_bytes.Length);
            EDCRcvModel edcRcvModel = new EDCRcvModel();
            //DATA
            var _data_list = _data.Split('-');
            var _index = 0;
            if (STX == Convert.ToInt32(_data_list[_index++])) {
                int _payload_length = CalculateBCDInverse(_data_list, _index, 2);
                //var a = (int.Parse(_data_list[_index + 1], NumberStyles.HexNumber) >> 4) * 10 + ((int.Parse(_data_list[_index + 1], NumberStyles.HexNumber) >> 4) % 8);
                _index += 2;

                for (int _i = 0; _i < RESERVE_FOR_FURFURES_USE_LENGTH; _i++) {
                    if ('0' != (char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)) {
                    }
                }

                if (VERSION == ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString()) {
                    if (MESSAGE_TYPE.RESPONSE == ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString()) {
                        var _transaction_code = ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString() + ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString();
                        var _response_code = ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString() + ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString();
                        if (MORE_DATA_INDICATOR == ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString()) {
                            if (FIELD_SEPARATOR == int.Parse(_data_list[_index++], NumberStyles.HexNumber)) {
                                int _index_start = _index;
                                while (_payload_length + 2 > _index) {
                                    var _field_type = ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString() + ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString();

                                    var _field_length = CalculateBCDInverse(_data_list, _index, 2);
                                    _index += 2;

                                    var _field_data = "";
                                    for (int _i = 0; _i < _field_length; _i++) {
                                        _field_data += ((char)int.Parse(_data_list[_index++], NumberStyles.HexNumber)).ToString();
                                    }
                                    edcRcvModel.Mapper(_field_type, _field_data);
                                    if (FIELD_SEPARATOR != int.Parse(_data_list[_index++], NumberStyles.HexNumber)) {
                                    }
                                }
                                if (ETX == int.Parse(_data_list[_index++], NumberStyles.HexNumber)) {
                                    return edcRcvModel;
                                }
                            }
                        }
                    }
                }
            } else {

            }
            return edcRcvModel;
        }

        internal void Connect() {
            if (port == null) {
                port = new SerialPort(COM, BAUD_RATE, PARITY, DATA_BIT, STOP_BITS);
                port.DataReceived += new SerialDataReceivedEventHandler(Port_DataReceived);
            }
        }

        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e) {
            byte[] _read_bytes = new byte[1024];
            int _read_int = port.Read(_read_bytes, 0, _read_bytes.Length);

            string _hex = BitConverter.ToString(_read_bytes, 0, _read_int);
            recieveHex = _hex;

            File.AppendAllText("log.txt", DateTime.Now.ToString("yyyyMMdd HHmmss") + ": Port_DataReceived 1 - " + BitConverter.ToString(_read_bytes, 0, _read_int) + Environment.NewLine, Encoding.UTF8);
            //_read_int = port.Read(_read_bytes, 0, _read_bytes.Length);
            //File.AppendAllText("log.txt", DateTime.Now.ToString("yyyyMMdd HHmmss") + ": 2 - " + BitConverter.ToString(_read_bytes, 0, _read_int) + Environment.NewLine, Encoding.UTF8);

            //{
            //    int _index = 0;
            //    string _stx = BitConverter.ToString(_read_bytes.Skip(_index).Take(1).ToArray());
            //    _index += 1;
            //    int _payload_length = Convert.ToInt32(BitConverter.ToString(_read_bytes.Skip(_index).Take(2).ToArray()).Replace("-", ""));
            //    _index += 2;
            //    string _reserve = string.Join("", _read_bytes.Skip(_index).Take(10).Select(r => (char)r));
            //    int _payload_count = 0;
            //    _index += 10;
            //    _payload_count += 10;
            //    string _version = string.Join("", _read_bytes.Skip(_index).Take(1).Select(r => (char)r));
            //    _index += 1;
            //    _payload_count += 1;
            //    string _message_type = string.Join("", _read_bytes.Skip(_index).Take(1).Select(r => (char)r));
            //    _index += 1;
            //    _payload_count += 1;
            //    string _transaction_code = string.Join("", _read_bytes.Skip(_index).Take(2).Select(r => (char)r));
            //    _index += 2;
            //    _payload_count += 2;
            //    string _response_code = string.Join("", _read_bytes.Skip(_index).Take(2).Select(r => (char)r));
            //    _index += 2;
            //    _payload_count += 2;
            //    string _data_indicator = string.Join("", _read_bytes.Skip(_index).Take(1).Select(r => (char)r));
            //    _index += 1;
            //    _payload_count += 1;
            //    string _field_separator = BitConverter.ToString(_read_bytes.Skip(_index).Take(1).ToArray()).Replace("-", "");
            //    _index += 1;
            //    _payload_count += 1;

            //    while (_payload_count < _payload_length) {
            //        string _field_type = string.Join("", _read_bytes.Skip(_index).Take(2).Select(r => (char)r));
            //        _index += 2;
            //        _payload_count += 2;
            //        int _field_length = Convert.ToInt32(BitConverter.ToString(_read_bytes.Skip(_index).Take(2).ToArray()).Replace("-", ""));
            //        _index += 2;
            //        _payload_count += 2;
            //        string _field_data = string.Join("", _read_bytes.Skip(_index).Take(_field_length).Select(r => (char)r));
            //        _index += _field_length;
            //        _payload_count += _field_length;
            //        _field_separator = BitConverter.ToString(_read_bytes.Skip(_index).Take(1).ToArray()).Replace("-", "");
            //        _index += 1;
            //        _payload_count += 1;
            //    }

            //    string _etx = BitConverter.ToString(_read_bytes.Skip(_index).Take(1).ToArray()).Replace("-", "");
            //    _index += 1;
            //    string _lrc = BitConverter.ToString(_read_bytes.Skip(_index).Take(1).ToArray()).Replace("-", "");
            //    _index += 1;
            //}

            if (_read_int > 10) {
                List<byte> _acknowledge_list = new List<byte>();
                string _data = ACKNOWLEDGE.ToString();
                byte[] _data_bytes = Encoding.ASCII.GetBytes(_data);

                File.AppendAllText("log.txt", DateTime.Now.ToString("yyyyMMdd HHmmss") + ": Port_DataReceived 2 - " + _hex + Environment.NewLine, Encoding.UTF8);
                port.Write(_data_bytes, 0, _data_bytes.Length);
            }
        }

        byte[] GetBytes(string _value) {
            return Encoding.ASCII.GetBytes(_value);
        }

        byte[] GetBytesFromHex(string _value) {
            return Enumerable.Range(0, _value.Length).Where(x => x % 2 == 0).Select(x => Convert.ToByte(_value.Substring(x, 2), 16)).ToArray();
        }
        public static Char CalculateLRC(string _value) {
            byte _lrc = 0;
            Encoding.ASCII.GetBytes(_value).ToList().ForEach(r => _lrc ^= r); ;
            return Convert.ToChar(_lrc);
        }

        public static string CalculateBCD(int _value, int _degit_length) {
            string _value_string = _value.ToString().PadLeft(_degit_length, '0');
            return string.Join("", Enumerable.Range(0, _degit_length - 1).Where(x => x % 2 == 0).Select(x => (char)((Convert.ToInt32(_value_string[x].ToString()) << 4) + (Convert.ToInt32(_value_string[x + 1].ToString())))));
        }

        public static int CalculateBCDInverse(string[] _data_list, int _digit_index, int _degit_length) {
            return (int)Enumerable.Range(_digit_index, _degit_length).Sum(r => (((int.Parse(_data_list[r], NumberStyles.HexNumber) >> 4) * 10) + (int.Parse(_data_list[r], NumberStyles.HexNumber) % 16)) * Math.Pow(100, _digit_index + _degit_length - r - 1));
        }

        internal void Send(string channel_type, double _amount) {
            //Reserve for Furfures Use
            string _payload = ("").PadLeft(10, '0');
            //Presentation Header
            _payload += VERSION;
            _payload += MESSAGE_TYPE.REQUEST;
            if (channel_type == "QRCODE") {
                _payload += TRANSACTION_CODE.QR_CODE;
            } else if (channel_type == "CREDIT" || channel_type == "DEBIT") {
                _payload += TRANSACTION_CODE.CARD_QR_CODE;
            } else {

            }
            _payload += RESPONSE_CODE.COMPLETE;
            _payload += MORE_DATA_INDICATOR;
            _payload += FIELD_SEPARATOR.ToString();
            //Field Data
            _payload += FIELD_TYPE.REQUEST_AMOUNT;
            _payload += CalculateBCD(12, 4);
            _payload += _amount.ToString("0.00").Replace(".", "").PadLeft(12, '0');
            _payload += FIELD_SEPARATOR.ToString();
            //Field Data
            _payload += FIELD_TYPE.REQUEST_REF_1;
            _payload += CalculateBCD(20, 4);
            _payload += ("1").PadLeft(20, '0');
            _payload += FIELD_SEPARATOR.ToString();

            //DATA
            string _data = STX.ToString();
            _data += CalculateBCD(_payload.Length, 4);
            _data += _payload;
            _data += ETX.ToString();
            _data += CalculateLRC(_data).ToString();

            byte[] _data_bytes = Encoding.ASCII.GetBytes(_data);

            string _hex = BitConverter.ToString(_data_bytes);
            File.AppendAllText("log.txt", DateTime.Now.ToString("yyyyMMdd HHmmss") + ": 1 - " + _hex + Environment.NewLine, Encoding.UTF8);

            if (!port.IsOpen) port.Open();
            port.ReadTimeout = READ_TIMEOUT_SECOND * 1000;
            port.Write(_data_bytes, 0, _data_bytes.Length);
        }
    }
}
