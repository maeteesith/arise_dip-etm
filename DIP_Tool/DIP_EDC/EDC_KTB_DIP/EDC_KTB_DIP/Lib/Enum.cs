﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;

namespace EDC_KTB_DIP {
    class MESSAGE_TYPE {
        public const string REQUEST = "0";
        public const string RESPONSE = "1";
    }
    class TRANSACTION_CODE {
        public const string CARD_QR_CODE = "20";
        public const string QR_CODE = "QR";
    }
    class RESPONSE_CODE {
        public const string COMPLETE = "00";
    }

    class FIELD_TYPE {
        //REQUEST
        public const string REQUEST_AMOUNT = "40";
        public const string REQUEST_REF_1 = "A1";
        public const string REQUEST_REF_2 = "A2";
        public const string REQUEST_REF_3 = "A3";
        //RESPONSE
        public const string APPROVAL_CODE = "01";
        public const string RESPONSE_MSG = "02";
        public const string INVOICE_NUMBER = "65";
        public const string REFERENCE_NO = "D3";
        public const string TERMINAL_ID = "16";
        public const string MERCHANT_ID = "D1";
        public const string DATE = "03";
        public const string TIME = "04";
        public const string CARD_NO = "30";
        public const string CARD_TYPE = "F1";
        public const string REF1 = "A1";
        public const string REF2 = "A2";
        public const string REF3 = "A2";
        
    }
}
