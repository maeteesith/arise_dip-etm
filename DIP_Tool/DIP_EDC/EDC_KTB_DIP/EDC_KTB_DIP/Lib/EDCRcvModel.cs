﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EDC_KTB_DIP.Lib
{
    class EDCRcvModel
    {
        public string approvalCode { get; set; }
        public string responseMessage { get; set; }
        public string invoiceNumber { get; set; }
        public string referenceNo { get; set; }
        public string terminalID { get; set; }
        public string merchantID { get; set; }
        public string date { get; set; }
        public string time { get; set; }
        public string cardNo { get; set; }
        public string cardType { get; set; }
        public string ref1 { get; set; }
        public string ref2 { get; set; }
        public string ref3 { get; set; }

        public string statusText { get; set; }

        public string totalPrice { get; set; }

        //This function used to mapping hex number from EDC to model
        //Please refer to the reference document from KTB
        public void Mapper(string fieldType, string fieldData)
        {
            if (fieldType == FIELD_TYPE.APPROVAL_CODE) this.approvalCode = fieldData;
            if (fieldType == FIELD_TYPE.RESPONSE_MSG) this.responseMessage = fieldData;
            if (fieldType == FIELD_TYPE.INVOICE_NUMBER) this.invoiceNumber = fieldData;
            if (fieldType == FIELD_TYPE.REFERENCE_NO) this.referenceNo = fieldData;
            if (fieldType == FIELD_TYPE.TERMINAL_ID) this.terminalID = fieldData;
            if (fieldType == FIELD_TYPE.MERCHANT_ID) this.merchantID = fieldData;
            if (fieldType == FIELD_TYPE.DATE) this.date = fieldData;
            if (fieldType == FIELD_TYPE.TIME) this.time = fieldData;
            if (fieldType == FIELD_TYPE.CARD_NO) this.cardNo = fieldData;
            if (fieldType == FIELD_TYPE.CARD_TYPE) this.cardType = fieldData;
            if (fieldType == FIELD_TYPE.REF1) this.ref1 = fieldData;
            if (fieldType == FIELD_TYPE.REF2) this.ref2 = fieldData;
            if (fieldType == FIELD_TYPE.REF3) this.ref3 = fieldData;
        }
    }
}
