﻿using EDC_KTB_DIP.Lib;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace EDC_KTB_DIP {
    public partial class frmMain : Form {
        public frmMain() {
            InitializeComponent();
        }

        string CONFIG_PATH = "config.txt";
        public static HttpListener listener;
        public static string url = "http://127.0.0.1:8080/";
        private EDCPort _edc;
        private void btnConnect_Click(object sender, EventArgs e) {



            //OK
            /*            _edc.Decode("02-02-01-52-30-30-30-30-30-30-30-30-30-30-31-31-51-52-30-30-30-1C-30-31-00-09-30-30-30-30-30-30-30-30-30-1C-30-32-00-20-41-50-50-52-4F-56-45-44-20-20-20-20-20-20-20-20-20-20-20-20-1C-36-35-00-06-30-30-30-30-31-38-1C-44-33-00-14-34-33-30-30-30-31-31-31-31-39-31-32-32-37-1C-31-36-00-08-34-33-30-30-30-31-31-31-1C-44-31-00-15-30-30-30-30-30-30-30-30-30-30-33-36-36-31-31-1C-30-33-00-06-31-39-31-32-32-37-1C-30-34-00-06-31-33-33-38-35-38-1C-46-31-00-05-51-52-20-20-20-1C-03-11");

                        CANCEL
                        _edc.Decode("02-00-43-30-30-30-30-30-30-30-30-30-30-31-31-51-52-4E-44-30-1C-30-32-00-20-54-58-4E-20-43-41-4E-43-45-4C-20-20-20-20-20-20-20-20-20-20-1C-03-3D");*/

            //_edc.Connect();
            //_edc.Send(0.01);
            //Close();
        }

        Thread thread;
        private void frmMain_Load(object sender, EventArgs e) {
            //niMain.Visible = true;
            niMain.ContextMenu = new ContextMenu();
            niMain.ContextMenu.MenuItems.Add(new MenuItem("E&xit", ExitHandle));

            btnConnect.PerformClick();

            thread = new Thread(TaskDo);
            thread.Start();
        }

        void TaskDo() {
            _edc = new EDCPort();
            //_edc.Decode("0202-01-52-30-30-30-30-30-30-30-30-30-30-31-31-51-52-30-30-30-1C-30-31-00-09-30-30-30-30-30-30-30-30-30-1C-30-32-00-20-41-50-50-52-4F-56-45-44-20-20-20-20-20-20-20-20-20-20-20-20-1C-36-35-00-06-30-30-30-30-31-38-1C-44-33-00-14-34-33-30-30-30-31-31-31-31-39-31-32-32-37-1C-31-36-00-08-34-33-30-30-30-31-31-31-1C-44-31-00-15-30-30-30-30-30-30-30-30-30-30-33-36-36-31-31-1C-30-33-00-06-31-39-31-32-32-37-1C-30-34-00-06-31-33-33-38-35-38-1C-46-31-00-05-51-52-20-20-20-1C-03-11");
            XmlSerializer _serializer = new XmlSerializer(typeof(EDCPort));
            if (false && File.Exists(CONFIG_PATH)) {
                using (FileStream _stream = new FileStream(CONFIG_PATH, FileMode.Open)) {
                    _edc = (EDCPort)_serializer.Deserialize(_stream);
                }
            } else {
                using (FileStream _stream = new FileStream(CONFIG_PATH, FileMode.Create)) {
                    _serializer.Serialize(_stream, _edc);
                }

            }
            //EDCRcvModel model = _edc.Decode("02-01-52-30-30-30-30-30-30-30-30-30-30-31-31-51-52-30-30-30-1C-30-31-00-09-30-30-30-30-30-30-30-30-30-1C-30-32-00-20-41-50-50-52-4F-56-45-44-20-20-20-20-20-20-20-20-20-20-20-20-1C-36-35-00-06-30-30-30-30-31-38-1C-44-33-00-14-34-33-30-30-30-31-31-31-31-39-31-32-32-37-1C-31-36-00-08-34-33-30-30-30-31-31-31-1C-44-31-00-15-30-30-30-30-30-30-30-30-30-30-33-36-36-31-31-1C-30-33-00-06-31-39-31-32-32-37-1C-30-34-00-06-31-33-33-38-35-38-1C-46-31-00-05-51-52-20-20-20-1C-03-11");

            //var jsonStr = JsonConvert.SerializeObject(model);
            // Create a Http server and start listening for incoming connections
            listener = new HttpListener();
            listener.Prefixes.Add(url);
            listener.Prefixes.Add("http://localhost:8080/");
            listener.Prefixes.Add("http://127.0.0.1:8080/");
            //listener.Prefixes.Add("http://localhost:5000/");
            listener.Start();
            Console.WriteLine("Listening for connections on {0}", url);

            // Handle requests
            Task listenTask = HandleIncomingConnections();
            listenTask.GetAwaiter().GetResult();

            // Close the listener
            listener.Close();
        }

        private void ExitHandle(object sender, EventArgs e) {
            Close();
        }

        private string EdcSendHandler(string channel_type, Double amount) {
            try {
                _edc.Connect();
                _edc.Send(channel_type, amount);
            } catch (Exception e) {
                return "Error :" + e.Message;
            }
            return "{\"statusText\":\"OK\"}";
        }

        bool runServer = true;
        private async Task HandleIncomingConnections() {
            // While a user hasn't visited the `shutdown` url, keep on handling requests
            while (runServer) {
                // Will wait here until we hear from a connection
                // HttpListenerContext ctx = await listener.GetContextAsync();
                HttpListenerContext ctx = listener.GetContext();
                // Peel out the requests and response objects
                HttpListenerRequest req = ctx.Request;
                HttpListenerResponse resp = ctx.Response;

                // Print out some info about the request
                Console.WriteLine(req.Url.ToString());
                Console.WriteLine(req.HttpMethod);
                Console.WriteLine(req.UserHostName);
                Console.WriteLine(req.UserAgent);
                var text = "";
                using (var reader = new StreamReader(req.InputStream,
                                     req.ContentEncoding)) {
                    text = reader.ReadToEnd();
                }
                //  Console.WriteLine(text);
                Console.WriteLine("method :" + req.QueryString["method"]);
                resp.ContentType = "Application/json";
                resp.ContentEncoding = Encoding.UTF8;

                resp.StatusCode = (int)HttpStatusCode.OK;

                //resp.Headers.Add("Access-Control-Allow-Origin", "*");
                //resp.Headers.Add("Access-Control-Allow-Methods", "POST, GET");

                resp.Headers.Add("Access-Control-Allow-Origin", "*");
                resp.Headers.Add("Access-Control-Allow-Credentials", "true");
                resp.Headers.Add("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
                resp.Headers.Add("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers, Authorization");

                var method = req.QueryString["method"];
                bool is_force_pass = true;
                bool is_force_fail = true;
                //If recieve tricker from app then start interface to EDC device
                if (!String.IsNullOrEmpty(method) && method.Equals("start")) {
                    //Start edc
                    ///_edc.Connect();
                    //_edc.Send(0.01);
                    //_edc.Decode("02-00-43-30-30-30-30-30-30-30-30-30-30-31-31-51-52-4E-44-30-1C-30-32-00-20-54-58-4E-20-43-41-4E-43-45-4C-20-20-20-20-20-20-20-20-20-20-1C-03-3D");
                    string channel_type = req.QueryString["channel_type"];
                    Double amount = Double.Parse(req.QueryString["amt"]);
                    _edc.recieveHex = null;
                    _edc.totalPrice = req.QueryString["amt"];
                    string _result_of_edc = EdcSendHandler(channel_type, amount);

                    if (_result_of_edc.StartsWith("Error :The port '")) {
                        if (is_force_pass || is_force_fail) {
                            //OK
                            if (is_force_pass)
                                _edc.recieveHex = "02-01-52-30-30-30-30-30-30-30-30-30-30-31-31-51-52-30-30-30-1C-30-31-00-09-30-30-30-30-30-30-30-30-30-1C-30-32-00-20-41-50-50-52-4F-56-45-44-20-20-20-20-20-20-20-20-20-20-20-20-1C-36-35-00-06-30-30-30-30-31-38-1C-44-33-00-14-34-33-30-30-30-31-31-31-31-39-31-32-32-37-1C-31-36-00-08-34-33-30-30-30-31-31-31-1C-44-31-00-15-30-30-30-30-30-30-30-30-30-30-33-36-36-31-31-1C-30-33-00-06-31-39-31-32-32-37-1C-30-34-00-06-31-33-33-38-35-38-1C-46-31-00-05-51-52-20-20-20-1C-03-11";
                            else if (is_force_fail)
                                _edc.recieveHex = "02-00-43-30-30-30-30-30-30-30-30-30-30-31-31-51-52-4E-44-30-1C-30-32-00-20-54-58-4E-20-43-41-4E-43-45-4C-20-20-20-20-20-20-20-20-20-20-1C-03-3D";

                            //byte[] data = Encoding.UTF8.GetBytes(_result_of_edc);
                            //byte[] data = Encoding.UTF8.GetBytes("OK");
                            EDCRcvModel model = new EDCRcvModel();
                            model.statusText = "OK";
                            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
                            resp.ContentLength64 = data.LongLength;
                            // Write out to the response stream (asynchronously), then close it
                            await resp.OutputStream.WriteAsync(data, 0, data.Length);
                        } else {
                            //byte[] data = Encoding.UTF8.GetBytes(_result_of_edc);
                            //byte[] data = Encoding.UTF8.GetBytes(_result_of_edc);
                            EDCRcvModel model = new EDCRcvModel();
                            model.statusText = "FAIL";
                            model.responseMessage = _result_of_edc;
                            byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
                            resp.ContentLength64 = data.LongLength;
                            // Write out to the response stream (asynchronously), then close it
                            await resp.OutputStream.WriteAsync(data, 0, data.Length);
                        }
                    } else {
                        //byte[] data = Encoding.UTF8.GetBytes(_result_of_edc);
                        //byte[] data = Encoding.UTF8.GetBytes("OK");
                        EDCRcvModel model = new EDCRcvModel();
                        model.statusText = "OK";
                        byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
                        resp.ContentLength64 = data.LongLength;
                        // Write out to the response stream (asynchronously), then close it
                        await resp.OutputStream.WriteAsync(data, 0, data.Length);
                    }
                } else if (!String.IsNullOrEmpty(method) && method.Equals("recieve")) {
                    //if recieveHex still null response processing
                    var resTxt = "";
                    EDCRcvModel model = new EDCRcvModel();
                    if (_edc.recieveHex == null) {
                        model.statusText = "PROCESSING";
                        //resTxt = "{\"statusText\":\"PROCESSING\"}";
                    } else if (_edc.recieveHex == "06") {
                        model.statusText = "WAITING PAYMENT";
                        //resTxt = "{\"statusText\":\"WAITING PAYMENT\"}";
                    } else {
                        // If recieveHex has value, it mean EDC already return result to the server
                        // Then mapping this result to json string
                        model = _edc.Decode(_edc.recieveHex);
                        model.totalPrice = _edc.totalPrice;
                        model.statusText = "COMPLETED";
                    }
                    byte[] data = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(model));
                    //    byte[] data = Encoding.UTF8.GetBytes(resTxt);
                    resp.ContentLength64 = data.LongLength;

                    await resp.OutputStream.WriteAsync(data, 0, data.Length);
                } else {
                    string disableSubmit = !runServer ? "disabled" : "";
                    byte[] data = Encoding.UTF8.GetBytes(String.Format("Hello"));
                    resp.ContentType = "text/html";
                    resp.ContentEncoding = Encoding.UTF8;
                    resp.ContentLength64 = data.LongLength;
                    resp.StatusCode = (int)HttpStatusCode.OK;
                    // Write out to the response stream (asynchronously), then close it
                    await resp.OutputStream.WriteAsync(data, 0, data.Length);
                }

                // Write the response info

                resp.Close();
            }
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e) {
            runServer = false;
            new WebClient().DownloadString(url);
        }




        //private void btnGetSerialPorts_Click(object sender, EventArgs e) {
        //    string[] ArrayComPortsNames = null;
        //    int index = -1;
        //    string ComPortName = null;

        //    ArrayComPortsNames = SerialPort.GetPortNames();
        //    do {
        //        index += 1;
        //        rtbIncoming.Text += ArrayComPortsNames[index] + "\n";
        //    }
        //    while (!((ArrayComPortsNames[index] == ComPortName) ||
        //                        (index == ArrayComPortsNames.GetUpperBound(0))));
        //}
    }
}
