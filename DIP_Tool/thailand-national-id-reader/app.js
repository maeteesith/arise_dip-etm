const express = require('express')
const cors = require('cors')
const app = express()

app.use(cors())

app.get('/', (req, res) => {	
    console.log("app.get('/', (req, res)")
    console.log(reader.data)
    res.header('Content-Type', 'application/json; charset=utf-8');
    res.end(JSON.stringify(reader.data));
})

app.post('/', (req, res) => {	
    console.log("app.post('/', (req, res)")
    console.log(reader.data)
    res.header('Content-Type', 'application/json; charset=utf-8');
    res.end(JSON.stringify(reader.data));
})

app.delete('/', (req, res) => {
    console.log("app.delete('/', (req, res)")
    reader.data = { data: [] };
    console.log(reader.data)
    res.end(JSON.stringify(reader.data));
})

app.get('/delete', (req, res) => {
    console.log("app.get('/delete', (req, res)")
    console.log(reader.data)
    reader.data = { data: [] };
    res.end(JSON.stringify(reader.data));
})

app.post('/delete', (req, res) => {
    console.log("app.post('/delete', (req, res)")
    console.log(reader.data)
    reader.data = { data: [] };
    res.end(JSON.stringify(reader.data));
})

app.listen(3000, () => {
    console.log('Start server at port 3000.')
})

const { ThaiCardReader, EVENTS, MODE } =
require('@privageapp/thai-national-id-reader')

const reader = new ThaiCardReader()
reader.data = { data: [] }
reader.readMode = MODE.PERSIONAL_PHOTO
reader.autoRecreate = true
reader.startListener()

reader.on(EVENTS.CARD_INSERTED, () => {
    console.log('CARD INSERTED')
})

reader.on(EVENTS.PCSC_INITIAL, () => {
    console.log('SAMRT CARD READER INITIAL')
})

reader.on(EVENTS.READING_COMPLETE, (card) => {
    reader.data.data.push(card)
    console.log(reader.data)
})