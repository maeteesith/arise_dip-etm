USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_SaveOtopType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_SaveOtopType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_SaveOtopType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_SaveOtopType] ON 

INSERT [dbo].[RM_SaveOtopType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'GROUP', N'กลุ่มผู้ประกอบการ', 0, CAST(N'2019-12-14T23:45:43.3033333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_SaveOtopType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'SINGLE', N'ผู้ประกอบการรายเดียว', 0, CAST(N'2019-12-14T23:45:37.0300000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_SaveOtopType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'SME', N'SME', 0, CAST(N'2019-12-14T23:45:45.5833333' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_SaveOtopType] OFF
ALTER TABLE [dbo].[RM_SaveOtopType] ADD  CONSTRAINT [DF_RM_SaveOtopType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_SaveOtopType] ADD  CONSTRAINT [DF_RM_SaveOtopType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_SaveOtopType] ADD  CONSTRAINT [DF_RM_SaveOtopType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_SaveOtopType] ADD  CONSTRAINT [DF_RM_SaveOtopType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
