USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentRole03Item]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vDocumentRole03Item] as 

select 

sc.id,
sc.save_id,
--di.save_01_index,
isnull(di.save_01_index, 
(select max(v.save_01_index) from DocumentRole03Item v where v.save_01_id = sc.save_id and left(v.document_role03_receive_status_code, 5) = 'DRAFT')
) save_01_index,
sc.request_number,
sc.consider_similar_document_file_id,
sc.consider_similar_document_remark,

sc.request_type_code,
sc.request_type_name,

sc.consider_similar_document_item_status_list,

di.document_role03_receive_allow_list,

di.document_role03_split_date,
di.document_role03_receive_date,

sc.consider_similar_document_date,
sc.request_item_sub_type_1_code_list_text,

di.document_role03_receiver_by,
u_r3r.name document_role03_receiver_by_name,

rm_d.code department_send_code,
rm_d.name department_send_name,

di.document_role03_receive_return_type_code,
rm_r3rr.name document_role03_receive_return_name,
di.document_role03_receive_return_remark,
di.document_role03_receive_return_date,

rm_r3s.code document_role03_status_code,
rm_r3s.name document_role03_status_name,
di.document_role03_receive_status_code,
rm_r3r.[index] document_role03_receive_status_index,
rm_r3r.name document_role03_receive_status_name,

di.value_01,
di.value_02,
di.value_03,
di.value_04,
di.value_05,

sc.is_deleted,
sc.created_by,
sc.created_date,
sc.updated_by,
sc.updated_date

from vSave010CheckingSaveDocument sc
left join DocumentRole03Item di on di.save_id = sc.id and di.save_01_id = sc.save_id and di.request_type_code = sc.request_type_code
left join UM_User u_r3r on u_r3r.id = di.document_role03_receiver_by
left join RM_Department rm_d on rm_d.code = 'CHECKING'

left join RM_DocumentRole03StatusCode rm_r3s on rm_r3s.code = isnull(di.document_role03_status_code, 'WAIT')
left join RM_DocumentRole03ReceiveStatusCode rm_r3r on rm_r3r.code = di.document_role03_receive_status_code

left join RM_DocumentRole03ReceiveReturnTypeCode rm_r3rr on rm_r3rr.code = di.document_role03_receive_return_type_code

where sc.consider_similar_document_status_code like '%ALLOW%' and

(sc.request_type_code <> '60' or 
(LEN(sc.consider_similar_document_item_status_list) - 
LEN(REPLACE(sc.consider_similar_document_item_status_list, '"ALLOW"', ''))) / 7 
- (case when sc.consider_similar_document_item_status_list like '%"TRADEMARK":"ALLOW"%' then 1 else 0 end)
- (case when sc.consider_similar_document_item_status_list like '%"SOUND_TRANSLATE":"ALLOW"%' then 1 else 0 end)
> 0)

--select * from RM_DocumentRole03StatusCode
GO
