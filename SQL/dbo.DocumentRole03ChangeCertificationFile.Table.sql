USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[DocumentRole03ChangeCertificationFile]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentRole03ChangeCertificationFile](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[document_role03_item_id] [bigint] NOT NULL,
	[file_id] [bigint] NULL,
	[remark] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_DocumentRole03ChangeCertificationFile] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DocumentRole03ChangeCertificationFile] ADD  CONSTRAINT [DF_DocumentRole03ChangeCertificationFile_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[DocumentRole03ChangeCertificationFile] ADD  CONSTRAINT [DF_DocumentRole03ChangeCertificationFile_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[DocumentRole03ChangeCertificationFile] ADD  CONSTRAINT [DF_DocumentRole03ChangeCertificationFile_created_by]  DEFAULT ((0)) FOR [created_by]
GO
