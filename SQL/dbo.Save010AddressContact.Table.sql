USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010AddressContact]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010AddressContact](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[descrition] [nvarchar](1000) NULL,
	[address_type_code] [nvarchar](50) NULL,
	[nationality_code] [nvarchar](50) NULL,
	[career_code] [nvarchar](50) NULL,
	[card_type_code] [nvarchar](50) NULL,
	[card_number] [nvarchar](200) NULL,
	[receiver_type_code] [nvarchar](50) NULL,
	[name] [nvarchar](1000) NULL,
	[house_number] [nvarchar](1000) NULL,
	[village_number] [nvarchar](200) NULL,
	[alley] [nvarchar](200) NULL,
	[street] [nvarchar](200) NULL,
	[address_sub_district_code] [nvarchar](50) NULL,
	[address_district_code] [nvarchar](50) NULL,
	[address_province_code] [nvarchar](50) NULL,
	[postal_code] [nvarchar](50) NULL,
	[address_country_code] [nvarchar](50) NULL,
	[telephone] [nvarchar](200) NULL,
	[fax] [nvarchar](200) NULL,
	[email] [nvarchar](200) NULL,
	[sex_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[address_representative_condition_type_code] [nvarchar](50) NULL,
	[other_text] [nvarchar](1000) NULL,
	[is_contact_person] [bit] NULL,
	[is_specific] [bit] NULL,
 CONSTRAINT [PK_Save01AddressContact] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010AddressContact] ADD  CONSTRAINT [DF_Save01AddressContact_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010AddressContact] ADD  CONSTRAINT [DF_Save01AddressContact_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010AddressContact] ADD  CONSTRAINT [DF_Save01AddressContact_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Save010AddressContact]  WITH CHECK ADD  CONSTRAINT [FK_Save010AddressContact_RM_AddressCardType] FOREIGN KEY([card_type_code])
REFERENCES [dbo].[RM_AddressCardType] ([code])
GO
ALTER TABLE [dbo].[Save010AddressContact] CHECK CONSTRAINT [FK_Save010AddressContact_RM_AddressCardType]
GO
ALTER TABLE [dbo].[Save010AddressContact]  WITH CHECK ADD  CONSTRAINT [FK_Save010AddressContact_RM_AddressCountry] FOREIGN KEY([address_country_code])
REFERENCES [dbo].[RM_AddressCountry] ([code])
GO
ALTER TABLE [dbo].[Save010AddressContact] CHECK CONSTRAINT [FK_Save010AddressContact_RM_AddressCountry]
GO
ALTER TABLE [dbo].[Save010AddressContact]  WITH CHECK ADD  CONSTRAINT [FK_Save010AddressContact_RM_AddressDistrict] FOREIGN KEY([address_district_code])
REFERENCES [dbo].[RM_AddressDistrict] ([code])
GO
ALTER TABLE [dbo].[Save010AddressContact] CHECK CONSTRAINT [FK_Save010AddressContact_RM_AddressDistrict]
GO
ALTER TABLE [dbo].[Save010AddressContact]  WITH CHECK ADD  CONSTRAINT [FK_Save010AddressContact_RM_AddressProvince] FOREIGN KEY([address_province_code])
REFERENCES [dbo].[RM_AddressProvince] ([code])
GO
ALTER TABLE [dbo].[Save010AddressContact] CHECK CONSTRAINT [FK_Save010AddressContact_RM_AddressProvince]
GO
ALTER TABLE [dbo].[Save010AddressContact]  WITH CHECK ADD  CONSTRAINT [FK_Save010AddressContact_RM_AddressSubDistrict] FOREIGN KEY([address_sub_district_code])
REFERENCES [dbo].[RM_AddressSubDistrict] ([code])
GO
ALTER TABLE [dbo].[Save010AddressContact] CHECK CONSTRAINT [FK_Save010AddressContact_RM_AddressSubDistrict]
GO
ALTER TABLE [dbo].[Save010AddressContact]  WITH CHECK ADD  CONSTRAINT [FK_Save010AddressContact_Save010] FOREIGN KEY([save_id])
REFERENCES [dbo].[Save010] ([id])
GO
ALTER TABLE [dbo].[Save010AddressContact] CHECK CONSTRAINT [FK_Save010AddressContact_Save010]
GO
