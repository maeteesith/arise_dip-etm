USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PostRoundInstructionRuleCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PostRoundInstructionRuleCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[description] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PostRoundInstructionRuleCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PostRoundInstructionRuleCode] ON 

INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 20200, N'CASE_10', N'หนังสือแจ้งเพื่อทราบ', N'นายทะเบียนพิจารณาขอแจ้งให้ทราบว่า', 0, CAST(N'2020-01-28T20:00:51.5733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1200, N'CASE_7', N'มาตรา 12', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เว้นแต่จะได้ดำเนินการ', 0, CAST(N'2020-01-28T20:00:52.1333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 20100, N'PUBLIC', N'ประกาศโฆษณา', NULL, 0, CAST(N'2020-01-28T20:00:52.4433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 1000, N'ROLE_10', N'มาตรา 10', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เพราะไม่ชอบด้วยมาตรา 10 เนื่องจากไม่มีสำนักงานหรือสถานที่ ที่นายทะเบียนสามารถติดต่อตั้งอยู่ในประเทศไทย เว้นแต่ จะได้ดำเนินการแก้ไขเปลี่ยนแปลงตามมาตรา 15', 0, CAST(N'2020-01-28T20:00:52.7633333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 1100, N'ROLE_11', N'มาตรา 11', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เนื่องจากมีข้อบกพร่องตามมาตรา 11 เพราะไม่เป็นไปตามหลักเกณฑ์ วิธีการ ที่กำหนดไว้ในกฎกระทรวง เว้นแต่ จะได้ดำเนินการให้ถูกต้องภายใน 60 วัน นับแต่วันที่ได้รับหนังสือฉบับนี้', 0, CAST(N'2020-01-28T20:00:53.0800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1301, N'ROLE_13_1', N'มาตรา 13 (1)', N'เป็นเครื่องหมายที่ไม่มีลักษณะอันพึงรับจดทะเบียนได้ ตามมาตรา 6 เนื่องจากคล้ายกับเครื่องหมายของบุคคลอื่น ที่ได้จดทะเบียนไว้แล้วตามมาตรา 13', 0, CAST(N'2020-01-28T20:00:53.3533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (20, 1302, N'ROLE_13_2', N'มาตรา 13 (2)', N'เป็นเครื่องหมายที่ไม่มีลักษณะอันพึงรับจดทะเบียนได้ ตามมาตรา 6 เนื่องจากเหมือนกับเครื่องหมายของบุคคลอื่น ที่ได้จดทะเบียนไว้แล้วตามมาตรา 13', 0, CAST(N'2020-05-11T17:50:36.4166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 1700, N'ROLE_17', N'มาตรา 17', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เว้นแต่ จะได้ดำเนินการ ตามมาตรา 17 โดยให้ยื่นแบบ ก.12 ภายใน 60 วัน นับแต่วันที่ได้รับหนังสือฉบับนี้', 0, CAST(N'2020-01-28T20:00:53.7000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, 2001, N'ROLE_20_1', N'มาตรา 20 (1)', N'คล้ายกับเครื่องหมายของบุคคลอื่น ที่ยื่นขอจดทะเบียนไว้ก่อนท่าน', 0, CAST(N'2020-01-28T20:00:53.9433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (19, 2002, N'ROLE_20_2', N'มาตรา 20 (2)', N'เหมือนกับเครื่องหมายของบุคคลอื่น ที่ยื่นขอจดทะเบียนไว้ก่อนท่าน', 0, CAST(N'2020-05-11T17:50:12.2566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (13, 2700, N'ROLE_27', N'มาตรา 27', N'เป็นเครื่องหมายที่เหมือนหรือคล้ายกับเครื่องหมายของบุคคลอื่น', 0, CAST(N'2020-01-28T20:00:54.2600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (14, 2900, N'ROLE_29', N'มาตรา 29', N'เหมือนหรือคล้ายกับเครื่องหมายของบุคคลอื่นที่นายทะเบียนได้มีคำสั่งให้ประกาศโฆษณาตามมาตรา 29', 0, CAST(N'2020-01-28T20:00:54.5766667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (15, 700, N'ROLE_7', N'มาตรา 7', N'เป็นเครื่องหมายที่ไม่มีลักษณะอันพึงรับจดทะเบียนได้ ตามมาตรา 6 เนื่องจากไม่มีลักษณะบ่งเฉพาะ ตามมาตรา 7', 0, CAST(N'2020-01-28T20:00:54.8766667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (16, 800, N'ROLE_8', N'มาตรา 8', N'เป็นเครื่องหมายที่ไม่มีลักษณะอันพึงรับจดทะเบียนได้ ตามมาตรา 6 เนื่องจากต้องห้ามรับจดทะเบียนตามมาตรา 8', 0, CAST(N'2020-01-28T20:00:55.1866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (17, 900, N'ROLE_9', N'มาตรา 9', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เนื่องจากมีข้อบกพร่องตามมาตรา 9 เพราะไม่ระบุรายการสินค้าแต่ละอย่าง โดยชัดแจ้ง เว้นแต่ จะได้ยื่นแบบ ก.06 เพื่อแก้ไขให้ถูกต้องภายใน 60 วัน นับแต่วันที่ได้รับหนังสือฉบับนี้ โดยให้แก้ไขรายการ สินค้า', 0, CAST(N'2020-01-28T20:00:55.8566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 10503, N'RULE_5_3', N'ตค. 5(3)', N'หนังสือแจ้งชำระเงิน', 0, CAST(N'2020-01-19T13:58:18.0133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (23, 10508, N'RULE_5_8_EXTEND', N'ตค. 5(8)', N'ต่ออายุ', 0, CAST(N'2020-06-15T00:56:57.4133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (22, 10508, N'RULE_5_8_FEE', N'ตค. 5(8)', N'หนังสือแจ้งชำระเงิน', 0, CAST(N'2020-06-15T00:19:56.0133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (18, 10601, N'RULE_6_1', N'ตค. 6(1)', NULL, 0, CAST(N'2020-03-30T03:58:17.1933333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 10901, N'RULE_9_1', N'ตค. 9(1)
', N'หนังสือสำคัญ', 0, CAST(N'2020-01-20T17:59:59.7700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (21, 10910, N'RULE_9_10', N'ตค. 9(10)', NULL, 0, CAST(N'2020-06-02T16:55:35.0100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (24, 10911, N'RULE_9_11_CANCEL_PUBLIC', N'ตค. 9(11)', N'ยกเลิกประกาศ', 0, CAST(N'2020-06-15T00:57:56.7666667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PostRoundInstructionRuleCode] OFF
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
