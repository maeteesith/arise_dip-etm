USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save220]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save220](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save220_representative_condition_type_code] [nvarchar](50) NULL,
	[inter_registration_number] [nvarchar](50) NULL,
	[inter_registration_date] [date] NULL,
	[inter_registration_cancel_date] [date] NULL,
	[protection_date] [date] NULL,
	[case_28_date] [date] NULL,
	[save220_img_type_type_code] [nvarchar](50) NULL,
	[img_file_2d_id] [bigint] NULL,
	[img_file_3d_id_1] [bigint] NULL,
	[img_file_3d_id_2] [bigint] NULL,
	[img_file_3d_id_3] [bigint] NULL,
	[img_file_3d_id_4] [bigint] NULL,
	[img_file_3d_id_5] [bigint] NULL,
	[img_file_3d_id_6] [bigint] NULL,
	[img_w] [int] NULL,
	[img_h] [int] NULL,
	[is_sound_mark] [bit] NULL,
	[is_sound_mark_human] [bit] NULL,
	[is_sound_mark_animal] [bit] NULL,
	[is_sound_mark_sound] [bit] NULL,
	[is_sound_mark_other] [bit] NULL,
	[remark_5_2_2] [nvarchar](max) NULL,
	[sound_file_id] [bigint] NULL,
	[sound_jpg_file_id] [bigint] NULL,
	[is_color_protect] [bit] NULL,
	[remark_8] [nvarchar](1000) NULL,
	[is_model_protect] [bit] NULL,
	[remark_9] [nvarchar](1000) NULL,
	[is_before_request] [bit] NULL,
	[is_11_1] [bit] NULL,
	[is_11_2] [bit] NULL,
	[is_11_3] [bit] NULL,
	[is_11_4] [bit] NULL,
	[is_11_5] [bit] NULL,
	[is_11_6] [bit] NULL,
	[is_11_7] [bit] NULL,
	[is_11_8] [bit] NULL,
	[is_11_9] [bit] NULL,
	[is_11_10] [bit] NULL,
	[is_11_11] [bit] NULL,
	[sign_inform_person_list] [nvarchar](300) NULL,
	[sign_inform_representative_list] [nvarchar](300) NULL,
	[wizard] [nvarchar](300) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_eForm_Save220] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save220] ADD  CONSTRAINT [DF_eForm_Save220_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save220] ADD  CONSTRAINT [DF_eForm_Save220_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save220] ADD  CONSTRAINT [DF_eForm_Save220_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save220]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220_FileGuest1] FOREIGN KEY([sound_file_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save220] CHECK CONSTRAINT [FK_eForm_Save220_FileGuest1]
GO
ALTER TABLE [dbo].[eForm_Save220]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220_FileGuest2] FOREIGN KEY([sound_jpg_file_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save220] CHECK CONSTRAINT [FK_eForm_Save220_FileGuest2]
GO
ALTER TABLE [dbo].[eForm_Save220]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save220_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save220] CHECK CONSTRAINT [FK_eForm_Save220_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save220]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220_RM_RequestImageMarkType] FOREIGN KEY([save220_img_type_type_code])
REFERENCES [dbo].[RM_RequestImageMarkType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save220] CHECK CONSTRAINT [FK_eForm_Save220_RM_RequestImageMarkType]
GO
