USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save021Dispute]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save021Dispute](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NOT NULL,
	[challenge_id] [bigint] NOT NULL,
	[challenge_number] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_eForm_Save021Dispute] PRIMARY KEY CLUSTERED 
(
	[save_id] ASC,
	[challenge_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[eForm_Save021Dispute] ON 

INSERT [dbo].[eForm_Save021Dispute] ([id], [save_id], [challenge_id], [challenge_number], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 3, 1, NULL, 0, CAST(N'2020-06-22T12:26:32.2766667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[eForm_Save021Dispute] OFF
/****** Object:  Index [IX_eForm_Save021Dispute]    Script Date: 6/23/2020 1:40:35 AM ******/
ALTER TABLE [dbo].[eForm_Save021Dispute] ADD  CONSTRAINT [IX_eForm_Save021Dispute] UNIQUE NONCLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save021Dispute] ADD  CONSTRAINT [DF_eForm_Save021Dispute_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save021Dispute] ADD  CONSTRAINT [DF_eForm_Save021Dispute_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save021Dispute] ADD  CONSTRAINT [DF_eForm_Save021Dispute_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save021Dispute]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save021Dispute_eForm_Save021] FOREIGN KEY([save_id])
REFERENCES [dbo].[eForm_Save021] ([id])
GO
ALTER TABLE [dbo].[eForm_Save021Dispute] CHECK CONSTRAINT [FK_eForm_Save021Dispute_eForm_Save021]
GO
ALTER TABLE [dbo].[eForm_Save021Dispute]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save021Dispute_Save020] FOREIGN KEY([challenge_id])
REFERENCES [dbo].[Save020] ([id])
GO
ALTER TABLE [dbo].[eForm_Save021Dispute] CHECK CONSTRAINT [FK_eForm_Save021Dispute_Save020]
GO
