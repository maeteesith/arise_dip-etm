USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[SaveInstructionRule9ItemSubType1]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SaveInstructionRule9ItemSubType1](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[Save_product_id] [bigint] NULL,
	[suggestion_item_id] [bigint] NULL,
	[item_id] [bigint] NULL,
	[item_code] [nvarchar](50) NULL,
	[item_name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_SaveInstructionRule9ItemSubType1] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SaveInstructionRule9ItemSubType1] ADD  CONSTRAINT [DF_SaveInstructionRule9ItemSubType1_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[SaveInstructionRule9ItemSubType1] ADD  CONSTRAINT [DF_SaveInstructionRule9ItemSubType1_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[SaveInstructionRule9ItemSubType1] ADD  CONSTRAINT [DF_SaveInstructionRule9ItemSubType1_created_by]  DEFAULT ((0)) FOR [created_by]
GO
