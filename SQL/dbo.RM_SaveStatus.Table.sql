USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_SaveStatus]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_SaveStatus](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_SaveStatus] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_SaveStatus] ON 

INSERT [dbo].[RM_SaveStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 3, N'DELETE', N'ลบ', 0, CAST(N'2019-12-11T19:16:50.3566667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_SaveStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'DRAFT', N'ยังไม่ส่งงาน
', 0, CAST(N'2019-12-11T19:16:40.1833333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_SaveStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 2, N'SENT', N'ส่งงานแล้ว
', 0, CAST(N'2019-12-11T19:16:43.4666667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_SaveStatus] OFF
ALTER TABLE [dbo].[RM_SaveStatus] ADD  CONSTRAINT [DF_RM_SaveStatus_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_SaveStatus] ADD  CONSTRAINT [DF_RM_SaveStatus_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_SaveStatus] ADD  CONSTRAINT [DF_RM_SaveStatus_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_SaveStatus] ADD  CONSTRAINT [DF_RM_SaveStatus_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_SaveStatus] ADD  CONSTRAINT [DF_RM_SaveStatus_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RM_SaveStatus]  WITH NOCHECK ADD  CONSTRAINT [FK_RM_SaveStatus_RM_SaveStatus] FOREIGN KEY([code])
REFERENCES [dbo].[RM_SaveStatus] ([code])
GO
ALTER TABLE [dbo].[RM_SaveStatus] NOCHECK CONSTRAINT [FK_RM_SaveStatus_RM_SaveStatus]
GO
ALTER TABLE [dbo].[RM_SaveStatus]  WITH CHECK ADD  CONSTRAINT [FK_RM_SaveStatus_RM_SaveStatus1] FOREIGN KEY([code])
REFERENCES [dbo].[RM_SaveStatus] ([code])
GO
ALTER TABLE [dbo].[RM_SaveStatus] CHECK CONSTRAINT [FK_RM_SaveStatus_RM_SaveStatus1]
GO
ALTER TABLE [dbo].[RM_SaveStatus]  WITH CHECK ADD  CONSTRAINT [FK_RM_SaveStatus_RM_SaveStatus2] FOREIGN KEY([code])
REFERENCES [dbo].[RM_SaveStatus] ([code])
GO
ALTER TABLE [dbo].[RM_SaveStatus] CHECK CONSTRAINT [FK_RM_SaveStatus_RM_SaveStatus2]
GO
