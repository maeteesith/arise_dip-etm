USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[EventLogType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventLogType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_EventLogType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[EventLogType] ON 

INSERT [dbo].[EventLogType] ([id], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, N'ERROR', NULL, 0, CAST(N'2020-02-06T07:38:08.3300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[EventLogType] ([id], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, N'SUCCESS', NULL, 0, CAST(N'2020-02-06T07:38:07.3366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[EventLogType] ([id], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, N'WARNING', NULL, 0, CAST(N'2020-02-06T07:38:13.0533333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[EventLogType] OFF
ALTER TABLE [dbo].[EventLogType] ADD  CONSTRAINT [DF_EventLogType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[EventLogType] ADD  CONSTRAINT [DF_EventLogType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[EventLogType] ADD  CONSTRAINT [DF_EventLogType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
