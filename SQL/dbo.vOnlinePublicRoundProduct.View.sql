USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vOnlinePublicRoundProduct]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vOnlinePublicRoundProduct] as

select 
max(pr.id) id,
pr.pr_id,

year(pr.public_start_date) public_year,
month(pr.public_start_date) public_month,

pr.public_start_date,
pr.request_item_sub_type_1_code,

count(1) item_count,

cast(max(cast(pr.is_deleted as int)) as bit) is_deleted,
max(pr.created_by) created_by,
max(pr.created_date) created_date,
max(pr.updated_by) updated_by,
max(pr.updated_date) updated_date

from vOnlinePublicRoundItem pr

group by 
pr.pr_id,
pr.public_start_date,
pr.request_item_sub_type_1_code
GO
