USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestType]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestType] ON 

INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (78, 1, N'010', N'คำขอจดทะเบียนเครื่องหมายการค้า / บริการ/รับรอง / เครื่องหมายร่วมใช้', N'0', NULL, N'ก. 01', 0, 0, CAST(N'2019-12-14T19:27:00.3666667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (59, 16, N'100', N'คำขอถือสิทธิวันที่ยื่นคำขอนอกราชอาณาจักรครั้งแรก', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:21.8100000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (60, 17, N'120', N'หนังสือแสดงการปฏิเสธ', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:22.2433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (61, 18, N'130', N'หนังสือจดทะเบียนเครื่องหมายชุด', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:22.6733333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (62, 19, N'140', N'หนังสือแจ้งฟ้องคดีต่อศาล', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:23.0966667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (63, 20, N'150', N'หนังสือชี้แจงการถกร้องขอให้เพิกถอนการจดทะเบียน', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:23.5366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (64, 21, N'180', N'หนังสือมอบอำนาจ', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:23.9700000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (65, 22, N'190', N'หนังสือขอผ่อนผันการส่งเอกสารหลักฐาน', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:24.4100000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (44, 1, N'20', N'คัดค้านการจดทะเบียนเครื่องหมาย ฯ', N'1000.00', N'2000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:15.3433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (66, 23, N'200', N'หนังสือนำส่งเอกสารหลักฐานและคำชี้แจง', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:24.8500000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (45, 2, N'21', N'คำขอโต้แย้งการจดทะเบียน', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:15.7800000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (67, 24, N'210', N'แม่พิมพ์รูปเครื่องหมายการค้า/บริการ/รับรอง/ร่วมใช้ ที่เกิน 5 ซม.', N'100.00', N'200.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:25.2800000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (68, 25, N'220', N'การจดทะเบียนเครื่องหมายการค้า / บริการ/รับรอง / เครื่องหมายร่วมใช้', N'300.00', N'600.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:25.7000000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (69, 26, N'240', N'ชำระค่าธรรมเนียมเพิ่ม (ก. 01)', N'500.00', N'1000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:26.1333333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (70, 27, N'250', N'ชำระค่าธรรมเนียมเพิ่ม (ก. 06)', N'100.00', N'200.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:26.5566667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (71, 28, N'260', N'ชำระค่าธรรมเนียมการต่ออายุเพิ่ม (ก. 07)', N'1000.00', N'2000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:26.9900000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (72, 29, N'270', N'ค่าธรรมเนียมเพิ่มเติมรายการสินค้าและบริการ', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:27.4166667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (73, 30, N'280', N'ผลการพิจารณาจากการฟ้องศาล (สำนักกฏหมาย)', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:27.8366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (74, 31, N'290', N'ชำระค่าธรรมเนียมเพิ่ม การจดทะเบียนเครื่องหมายการค้า', N'300.00', N'600.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:28.2600000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (75, 32, N'291', N'ชำระค่าธรรมเนียมเพิ่มเพื่อรับจดสัญญาอนุญาต', N'1000.00', N'2000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:28.6866667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (46, 3, N'30', N'อุทธรณ์คำสั่งนายทะเบียน หรือคำวินิจฉัยนายทะเบียน', N'2000.00', N'4000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:16.2100000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (81, 31, N'310', N'ขอตรวจดูทะเบียนหรือสารบบ', NULL, NULL, NULL, 0, 0, CAST(N'2020-02-10T17:51:16.9866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (82, 32, N'320', N'ขอสำเนาทะเบียนพร้อมคำรับรอง', NULL, NULL, NULL, 0, 0, CAST(N'2020-02-10T17:51:41.6833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (83, 33, N'330', N'ขอคัดสำเนาเอกสาร', NULL, NULL, NULL, 0, 0, CAST(N'2020-02-10T17:51:44.1333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (84, 34, N'340', N'ขอให้รับรองสำเนาเอกสารเรื่องเดียวกัน', NULL, NULL, NULL, 0, 0, CAST(N'2020-02-10T17:51:45.3466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (85, 35, N'350', N'ขอคำรับรองจากนายทะเบียนเกี่ยวกับรายการการจดทะเบียน', NULL, NULL, NULL, 0, 0, CAST(N'2020-02-10T17:51:46.3600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (86, 36, N'360', N'ขอใบแทนหนังสือสำคัญแสดงการจดทะเบียน', NULL, NULL, NULL, 0, 0, CAST(N'2020-02-10T17:51:47.4000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (89, 37, N'370', N'อื่นๆ', NULL, NULL, NULL, 1, 0, CAST(N'2020-02-10T20:00:50.1300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (87, 38, N'380', N'ขอตรวจดูแฟ้มคำขอจดทะเบียน', NULL, NULL, NULL, 0, 0, CAST(N'2020-02-10T17:51:50.5700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (47, 4, N'40', N'คำขอโอนหรือรับมรดกสิทธิ', N'1000.00', N'2000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:16.6366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (48, 5, N'41', N'คำขอจดทะเบียนโอนหรือรับมรดกสิทธิในเครื่องหมาย ฯ', N'1000.00', N'2000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:17.0700000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (49, 6, N'50', N'คำขอจดทะเบียนสัญญาอนุญาตให้ใช้เครื่องหมายเครื่องหมายการค้า / บริการ', N'500.00', N'1000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:17.4900000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (90, 39, N'51', N'คำขอจดทะเบียนต่ออายุสัญญาอนุญาตให้ใช้เครื่องหมายการค้า/บริการ', NULL, NULL, NULL, 1, 0, CAST(N'2020-03-30T07:13:41.5600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (50, 7, N'60', N'คำขอเปลี่ยนแปลงรายการในคำขอจดทะเบียน(ก่อนจดทะเบียน)', N'100.00', N'200.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:17.9300000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (51, 8, N'61', N'คำขอแก้ไขเปลี่ยนแปลงรายการจดทะเบียน(หลังจดทะเบียน)', N'200.00', N'400.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:18.3733333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (52, 9, N'62', N'คำขอแก้ไขเปลี่ยนแปลงคำขอจดทะเบียนสัญญาอนุญาตให้ใช้สิทธิ', N'200.00', N'400.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:18.8233333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (53, 10, N'64', N'คำขอแก้ไขเปลี่ยนแปลงข้อบังคับว่าด้วยการใช้เครื่องหมายรับรอง (ก่อนจดทะเบียน)', N'100.00', N'200.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:19.2566667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (54, 11, N'65', N'คำขอแก้ไขเปลี่ยนแปลงข้อบังคับว่าด้วยการใช้เครื่องหมายรับรอง (หลังจดทะเบียน)', N'200.00', N'400.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:19.6733333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (55, 12, N'70', N'คำขอต่ออายุการจดทะเบียนเครื่องหมาย ฯ', N'1000.00', N'2000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:20.0900000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (56, 13, N'80', N'คำร้องขอต่อคณะกรรมการสั่งให้เพิกถอนการจดทะเบียน', N'500.00', N'1000.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:20.5066667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (57, 14, N'81', N'คำขอให้เพิกถอนการจดทะเบียนสัญญาอนุญาตให้ใช้', N'200.00', N'400.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:20.9366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (58, 15, N'82', N'เพิกถอนคำขอจดทะเบียน', N'100.00', N'200.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:21.3500000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (76, 33, N'998', N'แก้ไขตามคำสั่งนายทะเบียนในแฟ้มคำขอ', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:29.1266667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (77, 34, N'999', N'หนังสืออื่นๆ', N'0.00', N'0.00', NULL, 1, 0, CAST(N'2019-12-12T21:07:29.5533333' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_RequestType] OFF
ALTER TABLE [dbo].[RM_RequestType] ADD  CONSTRAINT [DF_RM_RequestType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RequestType] ADD  CONSTRAINT [DF_RM_RequestType_is_deleted1]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_RequestType] ADD  CONSTRAINT [DF_RequestType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_RequestType] ADD  CONSTRAINT [DF_RequestType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_RequestType] ADD  CONSTRAINT [DF_RequestType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
