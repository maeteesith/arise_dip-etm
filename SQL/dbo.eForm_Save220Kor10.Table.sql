USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save220Kor10]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save220Kor10](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_11_1_1] [bit] NULL,
	[is_11_1_2] [bit] NULL,
	[is_11_1_3] [bit] NULL,
	[is_11_1_4] [bit] NULL,
	[is_11_1_5] [bit] NULL,
	[is_11_1_6] [bit] NULL,
	[is_11_1_7] [bit] NULL,
	[is_11_1_8] [bit] NULL,
	[is_11_1_9] [bit] NULL,
	[remark_11_1_9] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[is_11_1_2_1] [bit] NULL,
	[is_11_1_2_2] [bit] NULL,
	[save220_kor10_representative_condition_type_code] [nvarchar](50) NULL,
	[sign_inform_person_list] [nvarchar](50) NULL,
	[sign_inform_representative_list] [nvarchar](50) NULL,
 CONSTRAINT [PK_eForm_Save220Kor10] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save220Kor10] ADD  CONSTRAINT [DF_eForm_Save220Kor10_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save220Kor10] ADD  CONSTRAINT [DF_eForm_Save220Kor10_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save220Kor10] ADD  CONSTRAINT [DF_eForm_Save220Kor10_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save220Kor10]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220Kor10_eForm_Save220] FOREIGN KEY([save_id])
REFERENCES [dbo].[eForm_Save220] ([id])
GO
ALTER TABLE [dbo].[eForm_Save220Kor10] CHECK CONSTRAINT [FK_eForm_Save220Kor10_eForm_Save220]
GO
