USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save060RepresentativeChangeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save060RepresentativeChangeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save060RepresentativeChangeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save060RepresentativeChangeCode] ON 

INSERT [dbo].[RM_Save060RepresentativeChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 3, N'ADD', N'แต่งตั้งตัวแทน(เพิ่ม)', 0, CAST(N'2020-03-11T21:03:08.9033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save060RepresentativeChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 5, N'CANCEL', N'ยกเลิกตัวแทนทั้งหมด', 0, CAST(N'2020-01-26T21:10:16.8033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save060RepresentativeChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 4, N'CANCEL_SOME', N'ยกเลิกตัวแทนบางส่วน', 0, CAST(N'2020-03-11T21:03:34.5033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save060RepresentativeChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'CHANGE', N'แก้ไขรายละเอียดตัวแทน', 0, CAST(N'2020-01-26T21:10:13.2500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save060RepresentativeChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 2, N'NEW', N'แต่งตั้งตัวแทนใหม่', 0, CAST(N'2020-01-26T21:10:14.9133333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save060RepresentativeChangeCode] OFF
ALTER TABLE [dbo].[RM_Save060RepresentativeChangeCode] ADD  CONSTRAINT [DF_RM_Save060RepresentativeChangeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save060RepresentativeChangeCode] ADD  CONSTRAINT [DF_RM_Save060RepresentativeChangeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save060RepresentativeChangeCode] ADD  CONSTRAINT [DF_RM_Save060RepresentativeChangeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save060RepresentativeChangeCode] ADD  CONSTRAINT [DF_RM_Save060RepresentativeChangeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
