USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[SaveInstructionRuleItem]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SaveInstructionRuleItem](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[intruction_rule_id] [bigint] NULL,
	[instruction_rule_item_code] [nvarchar](50) NULL,
	[instruction_rule_description] [nvarchar](1000) NULL,
	[value_01] [nvarchar](1000) NULL,
	[value_02] [nvarchar](1000) NULL,
	[value_03] [nvarchar](1000) NULL,
	[value_04] [nvarchar](1000) NULL,
	[value_05] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_SaveInstructionRuleItem] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SaveInstructionRuleItem] ADD  CONSTRAINT [DF_SaveInstructionRuleItem_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[SaveInstructionRuleItem] ADD  CONSTRAINT [DF_SaveInstructionRuleItem_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[SaveInstructionRuleItem] ADD  CONSTRAINT [DF_SaveInstructionRuleItem_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[SaveInstructionRuleItem]  WITH CHECK ADD  CONSTRAINT [FK_SaveInstructionRuleItem_RM_ConsideringSimilarInstructionRuleItemCode] FOREIGN KEY([instruction_rule_item_code])
REFERENCES [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([code])
GO
ALTER TABLE [dbo].[SaveInstructionRuleItem] CHECK CONSTRAINT [FK_SaveInstructionRuleItem_RM_ConsideringSimilarInstructionRuleItemCode]
GO
ALTER TABLE [dbo].[SaveInstructionRuleItem]  WITH CHECK ADD  CONSTRAINT [FK_SaveInstructionRuleItem_SaveInstructionRule] FOREIGN KEY([intruction_rule_id])
REFERENCES [dbo].[SaveInstructionRule] ([id])
GO
ALTER TABLE [dbo].[SaveInstructionRuleItem] CHECK CONSTRAINT [FK_SaveInstructionRuleItem_SaveInstructionRule]
GO
