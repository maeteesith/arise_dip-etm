USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[DocumentRole04Release]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentRole04Release](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NOT NULL,
	[document_role04_release_type_code] [nvarchar](50) NULL,
	[document_role04_release_check_list] [nvarchar](1000) NULL,
	[document_role04_release_status_code] [nvarchar](50) NULL,
	[document_role04_release_instruction_rule_code] [nvarchar](50) NULL,
	[document_role04_release_send_date] [datetime2](7) NULL,
	[value_01] [nvarchar](1000) NULL,
	[value_02] [nvarchar](1000) NULL,
	[value_03] [nvarchar](1000) NULL,
	[value_04] [nvarchar](1000) NULL,
	[value_05] [nvarchar](1000) NULL,
	[document_role05_by] [bigint] NULL,
	[document_role05_date] [datetime2](7) NULL,
	[document_role05_status_code] [nvarchar](50) NULL,
	[document_role05_release_status_code] [nvarchar](50) NULL,
	[document_role05_release_send_date] [datetime2](7) NULL,
	[document_role05_release_by] [bigint] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_DocumentRole04Release] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DocumentRole04Release] ADD  CONSTRAINT [DF_DocumentRole04Release_document_role05_receiver_by]  DEFAULT ((0)) FOR [document_role05_release_by]
GO
ALTER TABLE [dbo].[DocumentRole04Release] ADD  CONSTRAINT [DF_DocumentRole04Release_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[DocumentRole04Release] ADD  CONSTRAINT [DF_DocumentRole04Release_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[DocumentRole04Release] ADD  CONSTRAINT [DF_DocumentRole04Release_created_by]  DEFAULT ((0)) FOR [created_by]
GO
