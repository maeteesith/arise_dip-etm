USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ReceiptChannelType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ReceiptChannelType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ReceiptChannelType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ReceiptChannelType] ON 

INSERT [dbo].[RM_ReceiptChannelType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'CASH', N'เงินสด', 0, CAST(N'2019-12-14T13:37:42.2000000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_ReceiptChannelType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 6, N'CHANGE', N'เงินทอน', 0, CAST(N'2019-12-14T13:38:19.2166667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_ReceiptChannelType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10002, 5, N'CHEQUE', N'เช็ค', 0, CAST(N'2019-12-29T16:58:37.6233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ReceiptChannelType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 3, N'CREDIT', N'บัตรเครดิต', 0, CAST(N'2019-12-14T13:37:47.7500000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_ReceiptChannelType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 4, N'DEBIT', N'บัตรเดบิต', 0, CAST(N'2019-12-14T13:37:58.7200000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_ReceiptChannelType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 2, N'QRCODE', N'QR Code', 0, CAST(N'2019-12-14T13:38:06.8266667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_ReceiptChannelType] OFF
ALTER TABLE [dbo].[RM_ReceiptChannelType] ADD  CONSTRAINT [DF_RM_ReceiptChannelType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ReceiptChannelType] ADD  CONSTRAINT [DF_RM_ReceiptChannelType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ReceiptChannelType] ADD  CONSTRAINT [DF_RM_ReceiptChannelType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ReceiptChannelType] ADD  CONSTRAINT [DF_RM_ReceiptChannelType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
