USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010PublicPayment]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010PublicPayment](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[total_price] [decimal](18, 2) NULL,
	[public_role02_consider_payment_status_code] [nvarchar](50) NULL,
	[public_role02_consider_payment_date] [datetime2](7) NULL,
	[public_role02_consider_payment_by] [bigint] NULL,
	[public_role05_consider_payment_status_code] [nvarchar](50) NULL,
	[public_role05_consider_payment_date] [datetime2](7) NULL,
	[public_role05_consider_payment_by] [bigint] NULL,
	[public_role02_document_payment_status_code] [nvarchar](50) NULL,
	[value_01] [nvarchar](1000) NULL,
	[value_02] [nvarchar](1000) NULL,
	[value_03] [nvarchar](1000) NULL,
	[value_04] [nvarchar](1000) NULL,
	[value_05] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010PublicPayment] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010PublicPayment] ADD  CONSTRAINT [DF_Save010PublicPayment_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010PublicPayment] ADD  CONSTRAINT [DF_Save010PublicPayment_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010PublicPayment] ADD  CONSTRAINT [DF_Save010PublicPayment_created_by]  DEFAULT ((0)) FOR [created_by]
GO
