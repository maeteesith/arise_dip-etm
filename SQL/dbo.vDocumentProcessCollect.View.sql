USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentProcessCollect]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vDocumentProcessCollect] as

select 

sc.id,
sc.save_id,

s.request_number,
sc.make_date,
sc.sender_by_name,
sc.department_name,
sc.remark,

(select top 1 rd.file_id
from RequestDocumentCollect rd 
where rd.save_id = s.id and rd.request_document_collect_type_code = 'TRADEMARK_2D' and rd.request_document_collect_status_code = 'ADD' and rd.file_id > 0
) file_id,
(select top 1 'File/Content/' + cast(rd.file_id as varchar) 
from RequestDocumentCollect rd 
where rd.save_id = s.id and rd.request_document_collect_type_code = 'TRADEMARK_2D' and rd.request_document_collect_status_code = 'ADD' and rd.file_id > 0
) file_trademark_2d,

sc.document_classification_collect_status_code,
rm_sc.name document_classification_collect_status_name,

sc.maker_by,
um_m.name maker_by_name,

(select top 1 sp.name from Save010AddressPeople sp where sp.save_id = s.id and sp.is_deleted = 0) name,

sc.is_deleted,
sc.created_date,
sc.created_by,
sc.updated_date,
sc.updated_by

from Save010 s 
join Save010DocumentClassificationCollect sc on sc.save_id = s.id
left join RM_DocumentClassificationCollectStatusCode rm_sc on rm_sc.code = sc.document_classification_collect_status_code
left join UM_User um_m on um_m.id = sc.maker_by
GO
