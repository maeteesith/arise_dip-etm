USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[PublicRound]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PublicRound](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[book_index] [nvarchar](50) NULL,
	[public_start_date] [date] NULL,
	[public_end_date] [date] NULL,
	[public_round_status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_PublicRound] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PublicRound] ADD  CONSTRAINT [DF_PublicRound_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[PublicRound] ADD  CONSTRAINT [DF_PublicRound_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[PublicRound] ADD  CONSTRAINT [DF_PublicRound_created_by]  DEFAULT ((0)) FOR [created_by]
GO
