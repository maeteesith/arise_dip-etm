USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[UM_PageMenuSub]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UM_PageMenuSub](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[menu_id] [bigint] NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_UM_PageMenuSub] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UM_PageMenuSub] ON 

INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'งานรับคำร้อง', 0, CAST(N'2019-12-10T16:48:02.2233333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'การเงิน', 0, CAST(N'2019-12-10T16:51:09.3733333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'งานบันทึกข้อมูล', 0, CAST(N'2019-12-10T16:51:38.3200000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 2, N'เจ้าหน้าที่บันทึกรูปคำ', 0, CAST(N'2019-12-10T16:51:44.9766667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 3, N'เจ้าหน้าที่ตรวจสอบ', 0, CAST(N'2019-12-10T16:51:52.4233333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10002, 4, N'เจ้าหน้าที่ประกาศ', 0, CAST(N'2020-01-24T09:26:37.4566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10006, 4, N'เจ้าหน้าที่ออกหนังสือ', 0, CAST(N'2020-01-24T09:50:37.8300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10007, 4, N'เจ้าหน้าที่เสนอออกหนังสือ', 0, CAST(N'2020-01-24T09:50:39.1033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10008, 4, N'เจ้าหน้าที่รับจดทะเบียน', 0, CAST(N'2020-01-24T09:50:42.4100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10009, 4, N'นายทะเบียนประกาศโฆษณาและรับจด', 0, CAST(N'2020-01-24T09:50:43.3100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10012, 1, N'จัดเก็บเอกสารอิเล็กทรอนิกส์', 0, CAST(N'2020-01-28T06:21:10.4833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10013, 2, N'เจ้าหน้าที่ออกหนังสือ', 0, CAST(N'2020-02-01T07:53:46.3000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10014, 3, N'นายทะเบียนตรวจสอบเครื่องหมายการค้า', 0, CAST(N'2020-02-01T08:05:57.9666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10015, 1, N'สอบถาม', 0, CAST(N'2020-02-08T10:22:14.6700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10016, 1, N'งานตรวจค้น', 0, CAST(N'2020-02-11T09:45:46.4166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10017, 1, N'พิมพ์หนังสือต่างๆ', 0, CAST(N'2020-02-12T19:40:22.7000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10018, 1, N'รายงาน', 0, CAST(N'2020-03-12T16:06:26.6833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10019, 2, N'เจ้าหน้าที่เปลี่ยนแปลง', 0, CAST(N'2020-03-20T11:07:43.1233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10020, 2, N'เจ้าหน้าที่กำกับ', 0, CAST(N'2020-06-02T17:21:02.9933333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10021, 2, N'นายทะเบียนสารบบ', 0, CAST(N'2020-06-02T17:21:09.3566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10022, 5, N'นายทะเบียนคัดค้าน', 0, CAST(N'2020-06-17T23:18:38.1833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10023, 5, N'เจ้าหน้าที่คัดค้าน', 0, CAST(N'2020-06-17T23:18:50.8233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10024, 5, N'หัวหน้ากลุ่มคัดค้าน', 0, CAST(N'2020-06-17T23:18:57.7800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10025, 6, N'เจ้าหน้าที่รับคำร้อง', 0, CAST(N'2020-06-17T23:19:00.9833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10026, 6, N'เจ้าหน้าที่จัดทำสำนวน', 0, CAST(N'2020-06-17T23:19:08.2733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10027, 6, N'หัวหน้ากลุ่มอุทธรณ์และคดี', 0, CAST(N'2020-06-17T23:19:16.0900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10028, 6, N'เจ้าหน้าที่ธุรการอุทธรณ์และคดี', 0, CAST(N'2020-06-17T23:19:23.7966667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10029, 6, N'คณะกรรมการอุทธรณ์', 0, CAST(N'2020-06-17T23:19:30.3266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10030, 7, N'ผู้ช่วยนายทะเบียนกลุ่มเปลี่ยนแปลง', 0, CAST(N'2020-06-17T23:19:39.9233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10031, 7, N'นายทะเบียนกลุ่มเปลี่ยนแปลง', 0, CAST(N'2020-06-17T23:19:44.9100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10032, 7, N'เจ้าหน้าที่บันทึกข้อมูล', 0, CAST(N'2020-06-17T23:19:51.7000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10033, 7, N'เจ้าหน้าที่ Fast Track', 0, CAST(N'2020-06-17T23:19:54.5833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10034, 7, N'เจ้าหน้าที่คุมเวลา', 0, CAST(N'2020-06-17T23:20:07.7966667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10035, 7, N'หัวหน้ากลุ่มเปลี่ยนแปลง', 0, CAST(N'2020-06-17T23:20:13.0300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10036, 8, N'เจ้าหน้าที่ขาเข้า', 0, CAST(N'2020-06-17T23:20:28.9066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10037, 8, N'นายทะเบียนมาดริดขาเข้า', 0, CAST(N'2020-06-17T23:20:53.1633333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10038, 8, N'เจ้าหน้าที่ขาออก', 0, CAST(N'2020-06-17T23:20:59.6533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10039, 8, N'นายทะเบียนขาออก', 0, CAST(N'2020-06-17T23:21:05.2466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10040, 8, N'หัวหน้ากลุ่มมาดริด', 0, CAST(N'2020-06-17T23:21:10.2566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10041, 8, N'เจ้าหน้าที่ออกหนังสือ', 0, CAST(N'2020-06-17T23:21:15.7800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenuSub] ([id], [menu_id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10042, 8, N'ผู้ยื่นคำขอมาดริด', 0, CAST(N'2020-06-17T23:21:21.2366667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UM_PageMenuSub] OFF
ALTER TABLE [dbo].[UM_PageMenuSub] ADD  CONSTRAINT [DF_UM_PageMenuSub_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[UM_PageMenuSub] ADD  CONSTRAINT [DF_UM_PageMenuSub_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[UM_PageMenuSub] ADD  CONSTRAINT [DF_UM_PageMenuSub_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[UM_PageMenuSub]  WITH CHECK ADD  CONSTRAINT [FK_UM_PageMenuSub_UM_PageMenu] FOREIGN KEY([menu_id])
REFERENCES [dbo].[UM_PageMenu] ([id])
GO
ALTER TABLE [dbo].[UM_PageMenuSub] CHECK CONSTRAINT [FK_UM_PageMenuSub_UM_PageMenu]
GO
