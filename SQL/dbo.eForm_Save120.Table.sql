USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save120]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save120](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[remark_5] [nvarchar](1000) NULL,
	[remark_6] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[save120_submit_type_code] [nvarchar](50) NULL,
	[sign_inform_person_list] [nvarchar](300) NULL,
	[sign_inform_representative_list] [nvarchar](300) NULL,
	[rule_number] [nvarchar](50) NULL,
	[save120_representative_condition_type_code] [nvarchar](50) NULL,
	[wizard] [nvarchar](300) NULL,
 CONSTRAINT [PK_eForm_Save120] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[eForm_Save120] ON 

INSERT [dbo].[eForm_Save120] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [remark_5], [remark_6], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save120_submit_type_code], [sign_inform_person_list], [sign_inform_representative_list], [rule_number], [save120_representative_condition_type_code], [wizard]) VALUES (1, NULL, N'2006000009', N'123@hotmail.com', N'704252d6-c0b3-4e16-8c90-3741e4683e5e', N'081024818', NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T05:18:55.6540279' AS DateTime2), 1, CAST(N'2020-06-21T05:18:55.6540395' AS DateTime2), 1, NULL, NULL, NULL, NULL, NULL, N'1|1|1|1')
INSERT [dbo].[eForm_Save120] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [remark_5], [remark_6], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save120_submit_type_code], [sign_inform_person_list], [sign_inform_representative_list], [rule_number], [save120_representative_condition_type_code], [wizard]) VALUES (2, NULL, N'2006000016', N'123@hotmail.com', NULL, N'0898014812', N'request001', N'registration001', NULL, NULL, N'111', N'222', 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, CAST(N'2020-06-21T11:43:26.8366148' AS DateTime2), 1, N'INSTRUCTION', N'0', N'', N'123', N'AND_OR', N'7|7|1|1')
INSERT [dbo].[eForm_Save120] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [remark_5], [remark_6], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save120_submit_type_code], [sign_inform_person_list], [sign_inform_representative_list], [rule_number], [save120_representative_condition_type_code], [wizard]) VALUES (3, NULL, N'2006000019', N'shama@hotmail.com', N'27d1772f-148a-40cc-a2d6-ad99f15f28a7', N'0987654321', NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T11:49:44.4280221' AS DateTime2), 1, CAST(N'2020-06-22T11:49:44.4280386' AS DateTime2), 1, NULL, NULL, NULL, NULL, NULL, N'1|1|1|1')
INSERT [dbo].[eForm_Save120] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [remark_5], [remark_6], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save120_submit_type_code], [sign_inform_person_list], [sign_inform_representative_list], [rule_number], [save120_representative_condition_type_code], [wizard]) VALUES (4, NULL, N'2006000020', N'testeform@hotmail.com', NULL, N'0811111111', N'request001', N'registration001', N'testeform', CAST(2000.00 AS Decimal(18, 2)), N'123', N'123', 0, CAST(N'2020-06-22T12:08:35.8300000' AS DateTime2), 0, NULL, NULL, N'INSTRUCTION', N'0', N'0', NULL, N'AND', NULL)
INSERT [dbo].[eForm_Save120] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [remark_5], [remark_6], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save120_submit_type_code], [sign_inform_person_list], [sign_inform_representative_list], [rule_number], [save120_representative_condition_type_code], [wizard]) VALUES (5, NULL, N'2006000020', N'shama@hotmail.com', N'ebc269ee-1fbb-4c58-a088-91472e1129a9', N'0987654321', NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T13:27:04.1659500' AS DateTime2), 1, CAST(N'2020-06-22T13:27:04.1659615' AS DateTime2), 1, NULL, NULL, NULL, NULL, NULL, N'1|1|1|1')
SET IDENTITY_INSERT [dbo].[eForm_Save120] OFF
ALTER TABLE [dbo].[eForm_Save120] ADD  CONSTRAINT [DF_eForm_Save120_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save120] ADD  CONSTRAINT [DF_eForm_Save120_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save120] ADD  CONSTRAINT [DF_eForm_Save120_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save120]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save120_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save120_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save120] CHECK CONSTRAINT [FK_eForm_Save120_RM_AddressRepresentativeConditionTypeCode]
GO
