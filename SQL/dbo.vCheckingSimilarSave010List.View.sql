USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vCheckingSimilarSave010List]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vCheckingSimilarSave010List] as

select

s.id,

s.request_number,
s.trademark_status_code,

(select '|' + case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.request_item_sub_type_1_code + '|' as [text()] from Save010Product sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.request_item_sub_type_1_code order by sp.request_item_sub_type_1_code for xml path('')) code) sp) + '|' 
request_item_sub_type_1_code_text,
(select '|' + case when len(sp.description) = 0 then null else left(sp.description, len(sp.description)-1) end from (select (select sp.description + '|' as [text()] from Save010Product sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.description order by sp.description for xml path('')) description) sp) + '|' 
request_item_sub_type_1_description_text,
(select '|' + case when len(sp.word_mark) = 0 then null else left(sp.word_mark, len(sp.word_mark)-1) end from (select (select sp.word_mark + '|' as [text()] from Save010DocumentClassificationWord sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.word_mark order by sp.word_mark for xml path('')) word_mark) sp) + '|' 
word_mark_text,
(select '|' + case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.word_first_code + '|' as [text()] from Save010DocumentClassificationWord sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.word_first_code order by sp.word_first_code for xml path('')) code) sp) + '|' 
word_first_code_text,
(select '|' + case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.word_sound_last_code + '|' as [text()] from Save010DocumentClassificationWord sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.word_sound_last_code order by sp.word_sound_last_code for xml path('')) code) sp) + '|' 
word_sound_last_code_text,
(select '|' + case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.word_sound_last_other_code + '|' as [text()] from Save010DocumentClassificationWord sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.word_sound_last_other_code order by sp.word_sound_last_other_code for xml path('')) code) sp) + '|' 
word_sound_last_other_code_text,
(select '|' + case when len(sp.name) = 0 then null else left(sp.name, len(sp.name)-1) end from (select (select sp.name + '|' as [text()] from Save010AddressPeople sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.name order by sp.name for xml path('')) name) sp) + '|' 
name_text,

(select '|' + case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.document_classification_image_code + '|' as [text()] from Save010DocumentClassificationImage sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.document_classification_image_code order by sp.document_classification_image_code for xml path('')) code) sp) + '|' 
document_classification_image_code_text,

(select '|' + case when len(sp.document_classification_sound_code) = 0 then null else left(sp.document_classification_sound_code, len(sp.document_classification_sound_code)-1) end from (select (select sp.document_classification_sound_code + '|' as [text()] from Save010DocumentClassificationSound sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.document_classification_sound_code order by sp.document_classification_sound_code for xml path('')) document_classification_sound_code) sp) + '|' 
document_classification_sound_code_text,
(select '|' + case when len(rm_d.name) = 0 then null else left(rm_d.name, len(rm_d.name)-1) end from (select (select rm_d.name + '|' as [text()] from Save010DocumentClassificationSound sp join RM_Save010DocumentClassificationSoundType rm_d on rm_d.code = sp.document_classification_sound_code where sp.save_id = s.id and sp.is_deleted = 0 group by rm_d.name order by rm_d.name for xml path('')) name) rm_d) + '|' 
document_classification_sound_name_text,

(select top 1 'File/Content/' + cast(rd.file_id as varchar) from RequestDocumentCollect rd where rd.save_id = s.id and rd.request_document_collect_type_code = 'TRADEMARK_2D' and rd.request_document_collect_status_code = 'ADD' and rd.file_id > 0) file_trademark_2d,
s.sound_file_id,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from Save010 s
where document_classification_status_code like 'SEND%'
GO
