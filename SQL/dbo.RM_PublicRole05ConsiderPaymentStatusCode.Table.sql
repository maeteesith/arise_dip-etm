USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PublicRole05ConsiderPaymentStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PublicRole05ConsiderPaymentStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PublicRole05ConsiderPaymentStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ON 

INSERT [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 3, N'DONE', N'เสร็จสิ้น', 1, 0, CAST(N'2020-01-18T10:01:32.2100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'DRAFT_CHANGE', N'รอพิจารณา', 0, 0, CAST(N'2020-05-22T16:08:30.8533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'WAIT', N'รอพิจารณา', 1, 0, CAST(N'2020-01-18T10:01:30.8833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 2, N'WAIT_CHANGE', N'รอแก้', 1, 0, CAST(N'2020-05-22T16:08:20.2300000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] OFF
ALTER TABLE [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05ConsiderPaymentStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05ConsiderPaymentStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05ConsiderPaymentStatusCode_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05ConsiderPaymentStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05ConsiderPaymentStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PublicRole05ConsiderPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05ConsiderPaymentStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
