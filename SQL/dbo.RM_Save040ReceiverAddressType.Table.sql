USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save040ReceiverAddressType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save040ReceiverAddressType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save040ReceiverAddressType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save040ReceiverAddressType] ON 

INSERT [dbo].[RM_Save040ReceiverAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'OTHERS', N'อื่นๆ', 0, CAST(N'2020-04-23T12:33:55.3866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save040ReceiverAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'RECEIVER', N'ผู้รับโอน', 0, CAST(N'2020-04-23T12:33:28.8700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save040ReceiverAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'REPRESENTATIVE', N'ตัวแทน', 0, CAST(N'2020-04-23T12:33:41.5733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save040ReceiverAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'REPRESENTATIVE_PERIOD', N'ตัวแทนช่วง', 0, CAST(N'2020-04-23T12:33:51.5366667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save040ReceiverAddressType] OFF
ALTER TABLE [dbo].[RM_Save040ReceiverAddressType] ADD  CONSTRAINT [DF_RM_Save040ReceiverAddressType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save040ReceiverAddressType] ADD  CONSTRAINT [DF_RM_Save040ReceiverAddressType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save040ReceiverAddressType] ADD  CONSTRAINT [DF_RM_Save040ReceiverAddressType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save040ReceiverAddressType] ADD  CONSTRAINT [DF_RM_Save040ReceiverAddressType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
