USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save040SubmitType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save040SubmitType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save040SubmitType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save040SubmitType] ON 

INSERT [dbo].[RM_Save040SubmitType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 2, N'INSTRUCTION', N'ยื่นโดยคำสั่งนายทะเบียน', 0, CAST(N'2020-03-19T16:54:00.9833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save040SubmitType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'NEW', N'ยื่นใหม่', 0, CAST(N'2020-03-19T16:53:50.6333333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save040SubmitType] OFF
ALTER TABLE [dbo].[RM_Save040SubmitType] ADD  CONSTRAINT [DF_Save040SubmitType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save040SubmitType] ADD  CONSTRAINT [DF_Save040SubmitType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save040SubmitType] ADD  CONSTRAINT [DF_Save040SubmitType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save040SubmitType] ADD  CONSTRAINT [DF_Save040SubmitType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
