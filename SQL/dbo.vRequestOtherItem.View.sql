USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vRequestOtherItem]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vRequestOtherItem] as
select 
min(rs.id) id,
r.id request_other_process_id,
rs.request_index,
rs.eform_id,
r.request_date,
rr.request_number,
r.requester_name,
ri.request_type_code,
count(rs.item_sub_type_1_code) item_sub_type_1_count,
sum(isnull(rs.total_price,0)) total_price,

(select top 1 sp.name from Save010 s join Save010AddressPeople sp on sp.save_id = s.id where s.request_number = rr.request_number) name,


r.created_date,
rr.reference_number,
rs_rs.code status_code,
rs_rs.name status_name,
rs.cancel_reason,
rc_rs.code receipt_status_code,
rc_rs.name receipt_status_name,
ri.is_deleted,
r.created_by,
us.name created_by_name,
ri.updated_by,
ri.updated_date
from RequestOtherProcess r
join RequestOtherRequest01Item rr on rr.request_other_process_id = r.id
join RequestOtherItem ri on ri.request_other_process_id = r.id
join RequestOtherItemSub rs on rs.request_other_item_id = ri.id and rr.request_number = rs.request_number
join RM_RequestStatus rs_rs on rs_rs.code = isnull(rs.status_code, 'DRAFT')
left join [UM_User] us on us.id = r.created_by
left join Receipt rc on rc.reference_number = rr.reference_number
left join RM_ReceiptStatus rc_rs on (rs.total_price > 0 and rc_rs.code = rc.status_code) or (rs.total_price = 0 and rc_rs.code = 'PAID')
--where --r.is_deleted = 0 and ri.is_deleted = 0 and rs.is_deleted = 0 and us.is_deleted = 0 and rc.is_deleted = 0 and rm_rs.is_deleted = 0 rr.reference_number = '63000004'
group by 
--rs.id,
r.id,
rs.request_index,
rs.eform_id,
r.request_date,
rr.request_number,
r.requester_name,
ri.request_type_code,
r.created_date,
rr.reference_number,
rs_rs.code,
rs_rs.name,
rs.cancel_reason,
rc_rs.code,
rc_rs.name,
ri.is_deleted,
r.created_by,
us.name,
ri.updated_by,
ri.updated_date

GO
