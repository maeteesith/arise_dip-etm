USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save010DocumentClassificationLanguage]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save010DocumentClassificationLanguage](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save010DocumentClassificationLanguage] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save010DocumentClassificationLanguage] ON 

INSERT [dbo].[RM_Save010DocumentClassificationLanguage] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 2, N'C', N'จีน', 0, CAST(N'2020-03-25T09:00:10.1366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationLanguage] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 3, N'E', N'อังกฤษ', 0, CAST(N'2020-03-25T08:59:45.1466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationLanguage] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 4, N'J', N'ญี่ปุ่น', 0, CAST(N'2020-03-25T08:59:46.7500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationLanguage] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 5, N'O', N'อื่นๆ', 0, CAST(N'2020-03-25T08:59:48.4233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationLanguage] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'T', N'ไทย', 0, CAST(N'2020-03-25T08:59:43.8000000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save010DocumentClassificationLanguage] OFF
ALTER TABLE [dbo].[RM_Save010DocumentClassificationLanguage] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationLanguage_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationLanguage] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationLanguage_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationLanguage] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationLanguage_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationLanguage] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationLanguage_created_by]  DEFAULT ((0)) FOR [created_by]
GO
