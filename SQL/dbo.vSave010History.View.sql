USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vSave010History]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vSave010History] as

select 
sr.id,

sr.save_id,

sr.instruction_date make_date,

sr.instruction_rule_code code,
rm_ir.name,

trim(isnull(rm_ir.name, '') + ' ' + isnull(sr.value_1, '')) description,

sr.created_by created_by_id,
um_i.name created_by_name,

0 file_id,

sr.is_deleted,
sr.created_by,
sr.created_date,
sr.updated_by,
sr.updated_date
from Save010InstructionRule sr
left join RM_ConsideringSimilarInstructionRuleCode rm_ir on rm_ir.code = sr.instruction_rule_code
left join UM_User um_i on um_i.id = sr.created_by

union 

select 
r.id,

r.save_id,

r.created_date make_date,

r.request_document_collect_type_code code,
rm_r.name,

N'Â×è¹àÍ¡ÊÒÃ ' + rm_r.name + '' description,

r.created_by created_by_id,
um_i.name created_by_name,

r.file_id,

r.is_deleted,
r.created_by,
r.created_date,
r.updated_by,
r.updated_date
from RequestDocumentCollect r
left join RM_RequestDocumentCollectTypeCode rm_r on rm_r.code = r.request_document_collect_type_code
left join UM_User um_i on um_i.id = r.created_by
where rm_r.name like N'%¡.%'
GO
