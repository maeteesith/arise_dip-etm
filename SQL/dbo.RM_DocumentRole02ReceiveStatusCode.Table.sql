USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole02ReceiveStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole02ReceiveStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole02ReceiveStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole02ReceiveStatusCode] ON 

INSERT [dbo].[RM_DocumentRole02ReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'DRAFT', N'รอดำเนินการ', 1, 0, CAST(N'2020-01-28T10:17:08.4566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'DRAFT_CHANGE', N'รอดำเนินการ', 0, 0, CAST(N'2020-05-26T16:08:57.1500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'SEND', N'เสร็จสิ้น', 1, 0, CAST(N'2020-01-28T10:17:15.1400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'WAIT_CHANGE', N'รอการแก้ไข', 1, 0, CAST(N'2020-01-28T10:17:10.5033333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole02ReceiveStatusCode] OFF
ALTER TABLE [dbo].[RM_DocumentRole02ReceiveStatusCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ReceiveStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ReceiveStatusCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ReceiveStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ReceiveStatusCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ReceiveStatusCode_is_deleted1]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ReceiveStatusCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ReceiveStatusCode_is_deleted]  DEFAULT ((1)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ReceiveStatusCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ReceiveStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ReceiveStatusCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ReceiveStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
