USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vPublicRole01Doing]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vPublicRole01Doing] as

select 
p.id,
p.book_index,
p.public_start_date,
p.public_end_date,
isnull(sp.request_item_sub_type_1_count, 0) request_item_sub_type_1_count,
0 save020_count,
datediff(day, GETDATE(), p.public_end_date) - 1 public_remain_day,

'' public_doing_remark,

p.public_round_status_code,
rm_prs.name public_round_status_name,

p.is_deleted,
p.created_by,
p.created_date,
p.updated_by,
p.updated_date
from PublicRound p
left join RM_PublicRoundStatusCode rm_prs on rm_prs.code = p.public_round_status_code
left join (select s.public_round_id, count(distinct sp.request_item_sub_type_1_code) request_item_sub_type_1_count from Save010 s left join Save010Product sp on sp.save_id = s.id group by s.public_round_id) sp on sp.public_round_id = p.id
GO
