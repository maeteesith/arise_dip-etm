USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Request01ItemSub]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Request01ItemSub](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request01_item_id] [bigint] NOT NULL,
	[item_sub_index] [int] NULL,
	[item_sub_type_1_code] [nvarchar](50) NULL,
	[product_count] [int] NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Request01ItemSub] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Request01ItemSub] ADD  CONSTRAINT [DF_Request01ItemSub_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Request01ItemSub] ADD  CONSTRAINT [DF_Request01ItemSub_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Request01ItemSub] ADD  CONSTRAINT [DF_Request01ItemSub_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Request01ItemSub]  WITH CHECK ADD  CONSTRAINT [FK_Request01ItemSub_Request01Item] FOREIGN KEY([request01_item_id])
REFERENCES [dbo].[Request01Item] ([id])
GO
ALTER TABLE [dbo].[Request01ItemSub] CHECK CONSTRAINT [FK_Request01ItemSub_Request01Item]
GO
