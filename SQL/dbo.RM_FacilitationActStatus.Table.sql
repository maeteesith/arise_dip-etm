USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_FacilitationActStatus]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_FacilitationActStatus](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_FacilitationActStatus] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_FacilitationActStatus] ON 

INSERT [dbo].[RM_FacilitationActStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'DOCUMENT_COMPLETED', N'เอกสารครบถ้วน', 0, CAST(N'2019-12-01T17:11:55.4066667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_FacilitationActStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'DOCUMENT_NOT_COMPLETED_AGREEMENT', N'ไม่ครบ มีเอกสารข้อตกลง', 0, CAST(N'2019-12-01T17:11:55.4066667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_FacilitationActStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'DOCUMENT_NOT_COMPLETED_NOT_AGREEMENT', N'ไม่ครบ ไม่มีเอกสารข้อตกลง', 0, CAST(N'2019-12-01T17:11:55.4066667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_FacilitationActStatus] OFF
ALTER TABLE [dbo].[RM_FacilitationActStatus] ADD  CONSTRAINT [DF_RM_FacilitationActStatus_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_FacilitationActStatus] ADD  CONSTRAINT [DF_RM_FacilitationActStatus_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_FacilitationActStatus] ADD  CONSTRAINT [DF_RM_FacilitationActStatus_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_FacilitationActStatus] ADD  CONSTRAINT [DF_RM_FacilitationActStatus_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RM_FacilitationActStatus]  WITH CHECK ADD  CONSTRAINT [FK_RM_FacilitationActStatus_RM_FacilitationActStatus] FOREIGN KEY([code])
REFERENCES [dbo].[RM_FacilitationActStatus] ([code])
GO
ALTER TABLE [dbo].[RM_FacilitationActStatus] CHECK CONSTRAINT [FK_RM_FacilitationActStatus_RM_FacilitationActStatus]
GO
