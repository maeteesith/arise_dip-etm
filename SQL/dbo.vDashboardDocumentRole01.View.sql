USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDashboardDocumentRole01]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vDashboardDocumentRole01] as
select 

u.id,

(select count(1) from Save010 s where s.document_status_code is not null) [all],
(select count(1) from Save010 s where s.document_status_code is not null and s.document_classification_status_code is null) [all_wait_documenting],
(select count(1) from Save010 s where s.document_classification_status_code is not null) [all_received],

(select count(1) from Save010 s where s.document_classification_by = u.id) [save],
(select count(1) from Save010 s where s.document_classification_by = u.id and s.document_classification_status_code = 'WAIT_DO') [save_wait_do],
(select count(1) from Save010 s where s.document_classification_by = u.id and s.document_classification_status_code = 'WAIT_SEND') [save_wait_save],
(select count(1) from Save010 s where s.document_classification_by = u.id and s.document_classification_status_code = 'SEND') [save_send],

(select count(1) from Save010 s join Save010DocumentClassificationVersion sv on sv.save_id = s.id and sv.is_master = 1 where s.document_classification_by = u.id) [change],
(select count(1) from Save010 s join Save010DocumentClassificationVersion sv on sv.save_id = s.id and sv.is_master = 1 where s.document_classification_by = u.id and sv.document_classification_version_status_code = 'WAIT_DO') [change_wait_do],
(select count(1) from Save010 s join Save010DocumentClassificationVersion sv on sv.save_id = s.id and sv.is_master = 1 where s.document_classification_by = u.id and sv.document_classification_version_status_code = 'WAIT_SEND') [change_wait_send],
(select count(1) from Save010 s join Save010DocumentClassificationVersion sv on sv.save_id = s.id and sv.is_master = 1 where s.document_classification_by = u.id and sv.document_classification_version_status_code = 'SEND') [change_send],

(select count(1) from Save010DocumentClassificationCollect sc where sc.maker_by = u.id) [collect],

u.is_deleted,
u.created_by,
u.created_date,
u.updated_by,
u.updated_date

from UM_User u
GO
