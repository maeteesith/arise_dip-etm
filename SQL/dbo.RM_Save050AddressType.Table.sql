USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save050AddressType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save050AddressType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save050AddressType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save050AddressType] ON 

INSERT [dbo].[RM_Save050AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (13, 7, N'OTHERS', N'อื่นๆ', 0, CAST(N'2020-04-27T05:40:05.6100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'OWNER', N'เจ้าของ', 0, CAST(N'2020-04-27T05:38:56.0666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 4, N'RECEIVER', N'ผู้ได้รับอนุญาต', 0, CAST(N'2020-04-27T05:39:23.7900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 5, N'RECEIVER_REPRESENTATIVE', N'ตัวแทนผู้ได้รับอนุญาต', 0, CAST(N'2020-04-27T05:39:33.8800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, 6, N'RECEIVER_REPRESENTATIVE_PERIOD', N'ตัวแทนช่วงผู้ได้รับอนุญาต', 0, CAST(N'2020-04-27T05:40:01.8866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 2, N'REPRESENTATIVE', N'ตัวแทน', 0, CAST(N'2020-04-27T05:39:03.1100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 3, N'REPRESENTATIVE_PERIOD', N'ตัวแทนช่วง', 0, CAST(N'2020-04-27T05:39:18.4033333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save050AddressType] OFF
ALTER TABLE [dbo].[RM_Save050AddressType] ADD  CONSTRAINT [DF_RM_Save050AddressType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save050AddressType] ADD  CONSTRAINT [DF_RM_Save050AddressType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save050AddressType] ADD  CONSTRAINT [DF_RM_Save050AddressType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save050AddressType] ADD  CONSTRAINT [DF_RM_Save050AddressType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
