alter view vPublicConsideringOther as

select 
sir.id,
s.id save_id,
s.request_number,

pr.public_start_date,
pr.public_end_date,
pr.book_index,

rm_prt.code post_round_type_code,
rm_prt.name post_round_type_name,

isnull(p.document_role04_receive_date, pr.updated_date) document_role04_receive_date,
p.document_role05_receive_send_date,

p.document_role05_receiver_by_name,

rm_r4r.code document_role04_receive_status_code,
rm_r4r.name document_role04_receive_status_name,

p.value_01,
p.value_02,
p.value_03,
p.value_04,
p.value_05,

'' public_considering_other_remark,

sir.is_deleted,
sir.created_by,
sir.created_date,
sir.updated_by,
sir.updated_date

from Save010InstructionRule sir
join Save010 s on s.id = sir.save_id
join PublicRound pr on pr.id = s.public_round_id
join RM_PostRoundTypeCode rm_prt on rm_prt.code = 'PUBLIC_OTHER_CANCEL'
left join vPublicOther p on p.save_id = s.id and p.post_round_type_code = rm_prt.code

join RM_DocumentRole04ReceiveStatusCode rm_r4r on rm_r4r.code = isnull(p.document_role04_receive_status_code, 'DRAFT')

where sir.instruction_rule_code = 'PUBLIC' and sir.considering_instruction_status_code = 'DELETE'