alter view vRequest01Item as
select 
ri.id,
ri.request01_id,
ri.eform_id,

s.id save_id,
(select top 1 s010p.name from Save010AddressPeople s010p where s010p.save_id = s.id) name,
s.registration_number,
s.trademark_expired_start_date,
s.trademark_expired_date,
s.trademark_expired_end_date,

(
select top 1 _ri.created_date 
from Receipt _r 
join ReceiptItem _ri on 
_ri.receipt_id = _r.id and 
_ri.request_type_code = '70' and 
--_ri.created_date >= s.trademark_expired_start_date and
_ri.request_number = ri.request_number
where _r.status_code = 'PAID'
order by _ri.id desc
) last_extend_date,

ri.request_number,
r.request_date,
r.requester_name,
ri.income_type,
ri.total_price,
count(rs.id) item_sub_type_1_count,
sum(rs.product_count) product_count,
ri.size_over_cm,
ri.reference_number,
ri_rs.code status_code,
ri_rs.name status_name,
rci.receipt_number,
rc_rs.code receipt_status_code,
rc_rs.name receipt_status_name,
ri.cancel_reason,
ri.is_deleted,
r.created_by,
us.name created_by_name,
ri.created_date,
ri.updated_by,
ri.updated_date
from Request01Process r
join Request01Item ri on ri.request01_id = r.id
left join Save010 s on s.request_number = ri.request_number
join RM_RequestStatus ri_rs on ri_rs.code = ri.status_code
left join Request01ItemSub rs on rs.request01_item_id = ri.id
left join [UM_User] us on us.id = r.created_by
left join Receipt rc on rc.reference_number = ri.reference_number and rc.is_deleted = 0
left join RM_ReceiptStatus rc_rs on rc_rs.code = rc.status_code
left join ReceiptItem rci on rci.receipt_id = rc.id and rci.request_number = ri.request_number and rci.request_type_code = '010'
--where r.is_deleted = 0 and ri.is_deleted = 0 and rs.is_deleted = 0 and us.is_deleted = 0 and rc.is_deleted = 0 and rm_rs.is_deleted = 0
--ri.request_number = '190100059'
--where ri.request_number = '333682'

group by
ri.id,
ri.request01_id,
ri.eform_id,
s.id,
s.registration_number,
s.trademark_expired_start_date,
s.trademark_expired_date,
s.trademark_expired_end_date,
ri.request_number,
r.request_date,
r.requester_name,
ri.income_type,
ri.total_price,
ri.size_over_cm,
us.name,
r.created_by,
ri.created_date,
ri.reference_number,
ri_rs.code,
ri_rs.name,
rc_rs.code,
rc_rs.name,
rci.receipt_number,
ri.cancel_reason,
ri.is_deleted,
ri.updated_by,
ri.updated_date
--order by ri.id desc