alter view vPublicRegister as

select
s.id,
s.request_number,
s.registration_number,
s.trademark_expired_date,

s.request_item_type_code,
case when s.request_item_type_code = 'COLLECTIVE_MARK' then N'����' when s.request_item_type_code = 'CERTIFICATION_MARK' then N'�Ѻ�ͧ' end request_item_type_text,

p.book_index,
s.public_page_index,
s.public_line_index,
s.public_page_index + ' / ' + s.public_line_index public_page_index_line_index,

(select left(sp.code, len(sp.code)-1) from (select (select sp.request_item_sub_type_1_code + ', ' as [text()] 
from Save010Product sp 
where sp.save_id = s.id and sp.is_deleted = 0
group by sp.request_item_sub_type_1_code order by sp.request_item_sub_type_1_code for xml path('')) code) sp) request_item_sub_type_1_code_text,

s.public_role04_payment_date,
s.public_role04_date,
s.public_role04_receive_date,
s.public_role05_date,
s.public_role04_document_date,
s.public_role02_document_date,

pr.post_round_instruction_rule_code,
rm_prir.name post_round_instruction_rule_name,
pr.book_number,
pr.book_start_date,
pr.book_end_date,
pr.book_acknowledge_date,

pr_d_1.post_round_instruction_rule_code register_post_round_instruction_rule_code,
rm_prir_d.name register_post_round_instruction_rule_name,
pr_d_1.book_number register_book_number_1,
pr_d_1.book_start_date register_book_start_1_date,
pr_d_1.book_end_date register_book_end_1_date,
pr_d_1.book_acknowledge_date register_book_acknowledge_1_date,
pr_d_1.post_round_document_post_status_code register_post_round_document_post_status_1_code,
rm_prdp_d_1.name register_post_round_document_post_status_1_name,
pr_d_1.post_round_action_post_status_code post_round_action_post_status_1_code,
rm_prap_d_1.name post_round_action_post_status_1_name,

pr_d_2.book_number register_book_number_2,
pr_d_2.book_start_date register_book_start_2_date,
pr_d_2.book_end_date register_book_end_2_date,
pr_d_2.book_acknowledge_date register_book_acknowledge_2_date,
pr_d_2.post_round_document_post_status_code register_post_round_document_post_status_2_code,
rm_prdp_d_2.name register_post_round_document_post_status_2_name,
pr_d_2.post_round_action_post_status_code post_round_action_post_status_2_code,
rm_prap_d_2.name post_round_action_post_status_2_name,

isnull(pr_d_2.book_number, pr_d_1.book_number) register_book_number,
isnull(pr_d_2.book_start_date, pr_d_1.book_start_date) register_book_start_date,
isnull(pr_d_2.book_end_date, pr_d_1.book_end_date) register_book_end_2_date_date,
isnull(pr_d_2.book_acknowledge_date, pr_d_1.book_acknowledge_date) register_book_acknowledge_date,
isnull(pr_d_2.post_round_document_post_status_code, pr_d_1.post_round_document_post_status_code) register_post_round_document_post_status_code,
isnull(rm_prdp_d_2.name, rm_prdp_d_1.name) register_post_round_document_post_status_name,
isnull(pr_d_2.post_round_action_post_status_code, pr_d_1.post_round_action_post_status_code) post_round_action_post_status_code,
isnull(rm_prap_d_2.name, rm_prap_d_1.name) post_round_action_post_status_name,

s.public_role04_receiver_by,
um_r.name public_role04_receiver_by_name,
s.public_role05_by,
um_r5.name public_role05_by_name,


rmdg.code department_group_code,
rmdg.name department_group_name,

s.public_role04_status_code,
rm_pr.name public_role04_status_name,
s.public_role04_receive_status_code ,
rm_prs.name public_role04_receive_status_name,
s.public_role05_status_code,
rm_pr5.name public_role05_status_name,
s.public_role04_document_status_code ,
rm_pds.name public_role04_document_status_name,
s.public_role02_document_status_code ,
rm_pds2.name public_role02_document_status_name,

'' public_role04_item_remark,
'' public_role04_check_remark,
'' public_role02_document_remark,
'' public_role04_document_remark,

pr.id pr_id,
pr_d_1.id pr_d_id_1,
pr_d_2.id pr_d_id_2,

isnull(pr_d_2.id, pr_d_1.id) pr_d_id,

rci.*,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from Save010 s
left join RM_PublicRole04StatusCode rm_pr on rm_pr.code = s.public_role04_status_code
left join RM_PublicRole04ReceiveStatusCode rm_prs on rm_prs.code = s.public_role04_receive_status_code
left join RM_PublicRole05StatusCode rm_pr5 on rm_pr5.code = s.public_role05_status_code
left join RM_PublicRole04DocumentStatusCode rm_pds on rm_pds.code = s.public_role04_document_status_code
left join RM_PublicRole02DocumentStatusCode rm_pds2 on rm_pds2.code = s.public_role02_document_status_code
left join RM_DepartmentGroupCode rmdg on rmdg.code = 'PUBLIC'
left join UM_User um_r on um_r.id = s.public_role04_receiver_by
left join UM_User um_r5 on um_r5.id = s.public_role05_by
left join PublicRound p on p.id = s.public_round_id
left join PostRound pr on pr.object_id = s.id and pr.post_round_type_code = 'INFORM_PAYMENT'

outer apply (select rc.payer_name, sum(rci.total_price) total_price, max(rc.receive_date) receipt_receive_date
from Receipt rc 
join ReceiptItem rci on rci.receipt_id = rc.id
where rc.reference_number = pr.reference_number and rc.status_code = 'PAID'
group by rc.payer_name
) rci 

left join RM_PostRoundInstructionRuleCode rm_prir on rm_prir.code = pr.post_round_instruction_rule_code

left join PostRound pr_d_1 on pr_d_1.object_id = s.id and pr_d_1.post_round_type_code = 'REGISTERED' and pr_d_1.round_index = 1
left join RM_PostRoundInstructionRuleCode rm_prir_d on rm_prir_d.code = pr_d_1.post_round_instruction_rule_code
left join RM_PostRoundDocumentPostStatusCode rm_prdp_d_1 on rm_prdp_d_1.code = pr_d_1.post_round_document_post_status_code
left join RM_PostRoundActionPostStatusCode rm_prap_d_1 on rm_prap_d_1.code = pr_d_1.post_round_action_post_status_code

left join PostRound pr_d_2 on pr_d_2.object_id = s.id and pr_d_2.post_round_type_code = 'REGISTERED' and pr_d_2.round_index = 2
left join RM_PostRoundDocumentPostStatusCode rm_prdp_d_2 on rm_prdp_d_2.code = pr_d_2.post_round_document_post_status_code
left join RM_PostRoundActionPostStatusCode rm_prap_d_2 on rm_prap_d_2.code = pr_d_2.post_round_action_post_status_code
where s.public_role04_status_code is not null

