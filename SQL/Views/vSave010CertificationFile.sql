alter view vSave010CertificationFile as 

select
sc.id,

sc.save_id,

sc.file_id,
f.file_name,
f.file_size,

sc.remark,

sc.is_deleted,
sc.created_by,
sc.created_date,
sc.updated_by,
sc.updated_date
from Save010CertificationFile sc
left join [File] f on f.id = sc.file_id and f.is_deleted = 0