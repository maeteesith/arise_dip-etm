alter view vUserSession as
select 
u.id, u.username, u.name, u.name_in_department, u.email, u.enable_start_date, u.enable_end_date, u.is_enabled, 
rm_d.code department_code,
rm_d.name department_name,
r.id role_id, r.name role_name,
pm.id page_menu_id, pm.name page_menu_name,
ps.id page_menu_sub_id, ps.name page_menu_sub_name,
p.code page_code, p.name page_name, p.url page_url, 
pc.code policy_code, pc.name policy_name,
p.page_link,
p.is_show_menu,
u.is_deleted, u.created_by, u.created_date, u.updated_by, u.updated_date
from UM_User u
left join UM_UserRole ur on ur.user_id = u.id and ur.is_deleted = 0
left join rm_department rm_d on rm_d.id = u.department_id
left join UM_Role r on --r.id = ur.role_id and 
r.is_deleted = 0
left join UM_RolePagePolicy rpp on rpp.role_id = r.id and rpp.is_deleted = 0
left join UM_Page p on p.code = rpp.page_code
left join UM_PageMenuSub ps on ps.id = p.menu_sub_id
left join UM_PageMenu pm on pm.id = ps.menu_id
left join UM_Policy pc on pc.code = rpp.policy_code
where u.is_deleted = 0