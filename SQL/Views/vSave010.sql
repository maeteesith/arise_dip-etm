alter view vSave010 as
select 

s010.id,
s010.request_id,
s010.request_index,
s010.irn_number,
s010.total_price,
s010.request_source_code,
rm_rs.name request_source_name,
s010.request_number,
s010.registration_number,
s010.request_date,
s010.make_date,

s010.sound_file_id,
(select top 1 f.physical_path from [File] f where f.id = s010.sound_file_id) sound_file_physical_path,

s010.trademark_expired_start_date,
s010.trademark_expired_date,
s010.trademark_expired_end_date,


(
select top 1 _ri.created_date 
from Receipt _r 
join ReceiptItem _ri on 
_ri.receipt_id = _r.id and 
_ri.request_type_code = '70' and 
--_ri.created_date >= s.trademark_expired_start_date and
_ri.request_number = s010.request_number
where _r.status_code = 'PAID'
order by _ri.id desc
) last_extend_date,


s010.request_item_type_code,
rm_ri.name request_item_type_name,

people.name name,
--(select top 1 s010p.name from Save010AddressPeople s010p where s010p.save_id = s010.id and s010p.is_deleted = 0) name,
people.name people_name,
people.information people_information,

sp.request_item_sub_type_1_code,
sp.request_item_sub_type_1_description,
/*
(select top 1 sp.request_item_sub_type_1_code from Save010Product sp where sp.save_id = s010.id order by sp.id) request_item_sub_type_1_code,
(select top 1 sp.description from Save010Product sp where sp.save_id = s010.id order by sp.id) request_item_sub_type_1_description,
*/
--(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.request_item_sub_type_1_code + ', ' as [text()] from Save010Product sp where sp.save_id = s010.id group by sp.request_item_sub_type_1_code order by sp.request_item_sub_type_1_code for xml path('')) code) sp) request_item_sub_type_1_code_text,
'' request_item_sub_type_1_code_text,

(select top 1 s010p.name from Save010AddressRepresentative s010p where s010p.save_id = s010.id) representative_name,
(select top 1 s010p.name from Save010AddressJoiner s010p where s010p.save_id = s010.id) joiner_name,
(select top 1 'File/Content/' + cast(rd.file_id as varchar) from RequestDocumentCollect rd where rd.save_id = s010.id and rd.request_document_collect_type_code = 'SAVE01' and rd.request_document_collect_status_code = 'ADD' and rd.file_id > 0) file_save01,
(select top 1 'File/Content/' + cast(rd.file_id as varchar) from RequestDocumentCollect rd where rd.save_id = s010.id and rd.request_document_collect_type_code = 'TRADEMARK_2D' and rd.request_document_collect_status_code = 'ADD' and rd.file_id > 0) file_trademark_2d,

s010.sound_description,

s010.saved_process_date,

s010.document_status_code,
rm_ds.name document_status_name,
s010.document_spliter_by,
s010.document_classification_by,
s010.document_classification_date,
s010.document_classification_remark,
s010.document_classification_status_code,
rm_dc.name document_classification_status_name,
s010.save010_document_classification_language_code,

rm_ct.code checking_type_code,
rm_ct.name checking_type_name,
rm_cs.code checking_status_code,
rm_cs.name checking_status_name,
s010.checking_spliter_by,
u_checking_spliter_by.name checking_spliter_by_name,
s010.checking_receiver_by,
u_checking_receiver_by.name checking_receiver_by_name,
s010.checking_receive_date,
s010.checking_receive_status_code,
rm_cr.name checking_receive_status_name,
s010.checking_send_date,
s010.checking_remark,

s010.considering_spliter_by,
u_considering_spliter_by.name considering_spliter_by_name,
s010.considering_receiver_by,
u_considering_receiver_by.name considering_receiver_by_name,
rm_rsc.code considering_receive_status_code,
rm_rsc.name considering_receive_status_name,
s010.considering_receive_date,
s010.considering_receive_remark,
s010.considering_send_date,
s010.considering_reason_send_back,
s010.considering_remark,

(case when (select count(1) intruction_count from Save010InstructionRule si where si.save_id = s010.id) > 0 then 2 else 1 end) considering_similar_round_index,


s010.considering_similar_instruction_type_code,
rm_csi.name considering_similar_instruction_type_name,
s010.considering_similar_evidence_27_code,
rm_cse.name considering_similar_evidence_27_name,
s010.considering_similar_instruction_description,

s010.cancel_reason,
s010.save_status_code,
s010.trademark_status_code,

s010.department_send_code,
rm_d.name department_send_name,
s010.department_send_date,


s010.consider_similar_document_status_code,
s010.consider_similar_document_date,
s010.consider_similar_document_remark,
s010.consider_similar_document_item_status_list,

s010.public_type_code,
s010.public_status_code,
s010.public_spliter_by,
s010.public_receiver_by,
s010.public_receive_status_code,
s010.public_receive_date,
s010.public_send_date,
s010.public_receive_do_status_code,
s010.public_round_id,
s010.public_page_index,
s010.public_line_index,
s010.public_role02_check_status_code,
s010.public_role02_check_by,
s010.public_role02_check_date,
s010.public_role04_payment_date,
s010.public_role04_status_code,
s010.public_role04_date,
s010.public_role04_spliter_by,
s010.public_role04_receiver_by,
s010.public_role04_receive_status_code,
s010.public_role04_receive_date,
s010.public_role05_by,
s010.public_role05_status_code,
s010.public_role05_date,


s010.floor3_proposer_by,
s010.floor3_proposer_date,
s010.floor3_registrar_by,
s010.floor3_registrar_date,
s010.floor3_registrar_status_code,
s010.floor3_registrar_description,

s010.is_save_past,

s010.is_deleted,
s010.created_by,
u_created.name created_by_name,
s010.created_date,
s010.updated_by,
u_updated.name updated_by_name,
isnull(s010.updated_date, s010.created_date) updated_date

from Save010 s010
left join RM_RequestSource rm_rs on rm_rs.code = s010.request_source_code
left join RM_RequestItemType rm_ri on rm_ri.code = s010.request_item_type_code

left join RM_DocumentStatusCode rm_ds on rm_ds.code = s010.document_status_code
left join RM_DocumentClassificationStatus rm_dc on rm_dc.code = s010.document_classification_status_code

left join RM_CheckingTypeCode rm_ct on rm_ct.code = s010.checking_type_code
left join RM_CheckingStatusCode rm_cs on rm_cs.code = s010.checking_status_code

left join RM_CheckingReceiveStatusCode rm_cr on rm_cr.code = s010.checking_receive_status_code

left join UM_User u_checking_spliter_by on u_checking_spliter_by.id = s010.checking_spliter_by
left join UM_User u_checking_receiver_by on u_checking_receiver_by.id = s010.checking_receiver_by

left join UM_User u_considering_spliter_by on u_considering_spliter_by.id = s010.considering_spliter_by
left join UM_User u_considering_receiver_by on u_considering_receiver_by.id = s010.considering_receiver_by

left join RM_ConsideringReceiveStatusCode rm_rsc on rm_rsc.code = s010.considering_receive_status_code

left join RM_ConsideringSimilarInstructionTypeCode rm_csi on rm_csi.code = s010.considering_similar_instruction_type_code
left join RM_ConsideringSimilarEvidence27Code rm_cse on rm_cse.code = s010.considering_similar_evidence_27_code

left join UM_User u_created on u_created.id = s010.created_by
left join UM_User u_updated on u_updated.id = s010.updated_by
left join RM_SaveStatus rm_ss on rm_ss.code = s010.save_status_code


left join RM_Department rm_d on rm_d.code = s010.department_send_code

outer apply (
select top 1 s010p.name, 
s010p.address_information information
from vSaveAddressAll s010p 
where s010p.save_id = s010.id
) people


outer apply (
select top 1 sp.request_item_sub_type_1_code, sp.description request_item_sub_type_1_description
from Save010Product sp 
where sp.save_id = s010.id and sp.is_deleted = 0
) sp



/*
(select top 1 sp.request_item_sub_type_1_code from Save010Product sp where sp.save_id = s010.id order by sp.id) request_item_sub_type_1_code,
(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.request_item_sub_type_1_code + ', ' as [text()] from Save010Product sp where sp.save_id = s010.id group by sp.request_item_sub_type_1_code order by sp.request_item_sub_type_1_code for xml path('')) code) sp) request_item_sub_type_1_code_text,
(select top 1 sp.description from Save010Product sp where sp.save_id = s010.id order by sp.id) request_item_sub_type_1_description,
*/