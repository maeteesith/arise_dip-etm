alter view vPublicRoundCase41 as
select

s.id,

s.request_number,
s.request_date,
s.request_item_type_code,

s.public_receive_date,

(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.request_item_sub_type_1_code + ', ' as [text()] 
from Save010Product sp 
where sp.save_id = s.id and sp.is_deleted = 0
group by sp.request_item_sub_type_1_code order by sp.request_item_sub_type_1_code for xml path('')) code) sp) request_item_sub_type_1_code_text,
rm_n.code nationality_code,
rm_n.name nationality_name,


p41.id pr_id,
p41.make_date,

p41.maker_by,
um.name maker_by_name,

p41.department_code,
rmd.name department_name,

p41.public_case41_remark,
p41.public_case41_additional_remark,

s.public_receiver_by public_role01_case41_by,
u1.name public_role01_case41_by_name,
p41.public_role01_case41_status_code,
rm1_41.name public_role01_case41_status_name,
p41.public_role01_case41_date,

p41.public_role05_case41_by,
u5.name public_role05_case41_by_name,
p41.public_role05_case41_status_code,
rm5_41.name public_role05_case41_status_name,
p41.public_role05_case41_date,


p41.public_round_case41_status_code,
rm_41.name public_round_case41_status_name,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from Save010 s
join PublicRoundCase41 p41 on p41.save_id = s.id
left join UM_User um on um.id = p41.maker_by
left join UM_User u1 on u1.id = s.public_receiver_by
left join UM_User u5 on u5.id = p41.public_role05_case41_by

left join RM_Department rmd on rmd.code = p41.department_code

left join RM_PublicRole01Case41StatusCode rm1_41 on rm1_41.code = p41.public_role01_case41_status_code
left join RM_PublicRole05Case41StatusCode rm5_41 on rm5_41.code = p41.public_role05_case41_status_code
left join RM_PublicRoundCase41StatusCode rm_41 on rm_41.code = p41.public_round_case41_status_code

outer apply (select top 1 rm_n.code, rm_n.name from Save010AddressPeople sp join RM_AddressNationality rm_n on rm_n.code = sp.nationality_code where sp.save_id = s.id) rm_n

where p41.public_role01_case41_status_code is not null