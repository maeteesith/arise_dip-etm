alter view vDocumentRole03ChangeRequestGroup as

select

sr.id,
sr.document_role03_item_id,

s.request_number,
s.registration_number,
s.trademark_status_code,
s.name,

sr.save010_registration_group_status_code,
sr.make_date,
sr.allow_date,

sr.is_deleted,
sr.created_by,
sr.created_date,
sr.updated_by,
sr.updated_date

from DocumentRole03ChangeRequestGroup sr 
join vSave010 s on s.request_number = sr.request_number