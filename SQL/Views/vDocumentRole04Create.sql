alter view vDocumentRole04Create as

select
d.id,
d.save_id,

s.request_number,

d.document_role04_create_type_code,
rm_dc.name document_role04_create_type_name,

d.document_role04_create_check_list,

d.document_role04_create_status_code,
rm_d4s.name document_role04_create_status_name,
d.document_role05_create_status_code,
rm_d5s.name document_role05_create_status_name,

d.document_role04_create_instruction_rule_code instruction_rule_code,
rm_pir.name instruction_rule_name,
rm_pir.description instruction_rule_description,

d.document_role04_create_send_date,
d.document_role05_create_send_date,

d.value_01,
d.value_02,
d.value_03,
d.value_04,
d.value_05,

(select top 1 sa.name from Save010AddressPeople sa where sa.save_id = s.id and sa.is_deleted = 0) name,

d.document_role05_by,
d.document_role05_date,

d.document_role05_status_code,
rm_ds.name document_role05_status_name,

d.document_role05_create_by,
um_d5c.name document_role05_create_by_name,

d.is_deleted,
d.created_by,
d.created_date,
d.updated_by,
d.updated_date
from DocumentRole04Create d
join Save010 s on s.id = d.save_id
left join RM_DocumentRole04CreateTypeCode rm_dc on rm_dc.code = d.document_role04_create_type_code
left join RM_DocumentRole04CreateStatusCode rm_d4s on rm_d4s.code = d.document_role04_create_status_code
left join RM_DocumentRole05CreateStatusCode rm_d5s on rm_d5s.code = d.document_role05_create_status_code
left join RM_PostRoundInstructionRuleCode rm_pir on rm_pir.code = d.document_role04_create_instruction_rule_code
left join RM_DocumentRole05StatusCode rm_ds on rm_ds.code = d.document_role05_status_code
left join UM_User um_d5c on um_d5c.id = d.document_role05_create_by
