alter view vDashboardConsidering as

select 
max(s.id) id,

count(s.round_1_wait) round_1_wait,
count(s.round_1_done) round_1_done,
count(s.round_2_wait) round_2_wait,
count(s.round_2_done) round_2_done,

--s.considering_receiver_by,

cast(max(cast(s.is_deleted as int)) as bit) is_deleted,
max(s.created_by) created_by,
max(s.created_date) created_date,
max(s.updated_by) updated_by,
max(s.updated_date) updated_date

from (
select 
s.id,

case when s.considering_similar_round_index = 1 and (s.considering_receive_status_code like 'DRAFT%') then 1 else null end round_1_wait,
case when s.considering_similar_round_index = 1 and (s.considering_receive_status_code like 'SEND%') and s.considering_send_date > dateadd(dd, datediff(dd, 0, getdate()) - day(getdate()) + 1, 0) then 1 else null end round_1_done,
case when s.considering_similar_round_index = 2 and (s.considering_receive_status_code like 'DRAFT%') then 1 else null end round_2_wait,
case when s.considering_similar_round_index = 2 and (s.considering_receive_status_code like 'SEND%') and s.considering_send_date > dateadd(dd, datediff(dd, 0, getdate()) - day(getdate()) + 1, 0) then 1 else null end round_2_done,

s.considering_receiver_by,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from vSave010 s
) s
--group by s.considering_receiver_by

