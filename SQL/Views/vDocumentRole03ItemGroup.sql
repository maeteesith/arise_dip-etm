alter view vDocumentRole03ItemGroup as

select 
vv.*,

rm_r3r.code document_role03_receive_status_code,
rm_r3r.name document_role03_receive_status_name

from (
	select
	max(v.id) id,

	v.save_id,
	v.save_01_index,
	v.request_number,

	max(v.consider_similar_document_date) consider_similar_document_date,
	max(v.document_role03_split_date) document_role03_split_date,
	max(v.document_role03_receive_date) document_role03_receive_date,
	max(v.document_role03_receive_return_date) document_role03_receive_return_date,

	max(v.document_role03_receiver_by_name) document_role03_receiver_by_name,

	max(v.document_role03_receive_return_remark) document_role03_receive_return_remark,

	max(v.request_item_sub_type_1_code_list_text) request_item_sub_type_1_code_list_text,

	max(v.document_role03_status_code) document_role03_status_code,
	max(v.document_role03_status_name) document_role03_status_name,

	min(v.document_role03_receive_status_index) document_role03_receive_status_index,

	max(v.department_send_code) department_send_code,
	max(v.department_send_name) department_send_name,

	(select case when len(vr.request_type_name) = 0 then null else left(vr.request_type_name, len(vr.request_type_name)-1) end from (select (
	select vr.request_type_name + ', ' as [text()] 
	from vDocumentRole03Item vr
	where vr.save_id = v.save_id and isnull(vr.save_01_index, 1) = isnull(v.save_01_index, 1)
	group by vr.request_type_name 
	order by vr.request_type_name for xml path('')
	) request_type_name) vr) request_type_list_name,

	v.is_deleted,
	max(v.created_by) created_by,
	max(v.created_date) created_date,
	max(v.updated_by) updated_by,
	max(v.updated_date) updated_date

	from vDocumentRole03Item v

	where v.is_deleted = 0

	group by 
	v.save_id, 
	v.save_01_index, 
	v.request_number, 
	v.is_deleted
) vv
left join RM_DocumentRole03ReceiveStatusCode rm_r3r on rm_r3r.[index] = vv.document_role03_receive_status_index
