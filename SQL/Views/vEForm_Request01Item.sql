alter view vEForm_Request01Item as

select 
ri.id,
ri.request01_id,
ri.eform_id,
ri.item_index,

ri.item_type_code,
rm_ri.name item_type_name,
ri.income_type,
ri.income_description,

ri.sound_file_id,
ri.sound_type_code,
ri.sound_description,

ri.facilitation_act_status_code,
rm_fa.name facilitation_act_status_name,

ri.request_number,

ri.size_over_cm,
ri.size_over_price,
ri.size_over_income_type,
ri.size_over_income_description,

ri.request_evidence_list,

(select count(distinct sp.item_sub_type_1_code) from RequestEForm01ItemSub sp where sp.request01_item_id = ri.id and sp.is_deleted = 0) request_01_item_sub_count,
(select count(1) from RequestEForm01ItemSub sp where sp.request01_item_id = ri.id and sp.is_deleted = 0) request_01_product_count,

ri.total_price,
ri.book_index,
ri.page_index,
ri.is_otop,
ri.reference_number,
ri.cancel_reason,
ri.status_code,

ri.is_deleted,
ri.created_by,
ri.created_date,
ri.updated_by,
ri.updated_date

from RequestEForm01Item ri
left join RM_RequestItemType rm_ri on rm_ri.code = ri.item_type_code
left join RM_FacilitationActStatus rm_fa on rm_fa.code = ri.facilitation_act_status_code