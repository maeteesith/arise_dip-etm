alter view vDocumentRole02 as

select

pr.id,
pri.id post_round_instruction_rule_id,
sir.id instruction_rule_id,
isnull(pr.object_id , s.id) save_id,
s.request_number,
s.registration_number,
s.request_date,

s.trademark_expired_start_date,
s.trademark_expired_date,
s.trademark_expired_end_date,

spp.public_role05_consider_payment_by,

(case 
when (select count(1) from PostRoundAddress pra where pra.post_round_id = pr.id and pra.is_check = 1 and pra.is_deleted = 0) > 0 then (select count(1) from PostRoundAddress pra where pra.post_round_id = pr.id and pra.is_check = 1 and pra.is_deleted = 0)
else 1 
end) send_address_count,

pr.post_round_type_code,
rm_pr.name post_round_type_name,

isnull(rm_prirc.code, sir.instruction_rule_code) instruction_rule_code,
isnull(rm_prirc.name, rm_csi.name) instruction_rule_name,

sir.created_date instruction_date,
isnull(sir.instruction_send_date, spp.public_role05_consider_payment_date) instruction_send_date,
pr.post_round_document_post_date,
isnull(pri.document_role02_check_date, pr.document_role02_receive_date) document_role02_check_date,
pr.document_role02_date,
pr.document_role02_receive_date,
pr.document_role02_print_document_date,
pr.document_role02_print_cover_date,
pr.document_role02_post_number_date,
pr.document_role02_print_list_date,

pr.book_acknowledge_date,
pr.book_payment_date,
pr.reference_number,

case when pr.round_index = 1 then pr.book_start_date else pr_1.book_start_date end book_round_01_start_date,
case when pr.round_index = 1 then pr.book_end_date else pr_1.book_end_date end book_round_01_end_date,
case when pr.round_index > 1 then pr.book_start_date else null end book_round_02_start_date,
case when pr.round_index > 1 then pr.book_end_date else null end book_round_02_end_date,

pr.book_number,
pr.post_number,

um_dr.id document_role02_receiver_by,
um_dr.name document_role02_receiver_by_name, 

um_sir.name created_by_name,
rm_dg.name department_code,
rm_dg.name department_name,

sir.considering_instruction_rule_status_code,
rm_crsi.name considering_instruction_rule_status_name,
pr.document_role02_status_code,
rm_sirds.name document_role02_status_name,
pr.document_role02_receive_status_code,
rm_sirdrs.name document_role02_receive_status_name,

rm_cbs.code considering_book_status_code,
rm_cbs.name considering_book_status_name,

rm_rcsc.code document_role02_check_status_code,
rm_rcsc.name document_role02_check_status_name,

pr.document_role02_print_document_status_code,
rm_1.name document_role02_print_document_status_name,
pr.document_role02_print_cover_status_code,
rm_2.name document_role02_print_cover_status_name,
pr.document_role02_print_list_status_code,
rm_3.name document_role02_print_list_status_name,

pr.post_round_action_post_status_code,
rm_4.name post_round_action_post_status_name,
pr.post_round_action_post_type_code,
rm_5.name post_round_action_post_type_name,

(select left(sp.code, len(sp.code)-1) from (select (select 
distinct sp.request_item_sub_type_1_code + ', ' as [text()] 
from Save010Product sp 
where sp.save_id = s.id and sp.is_deleted = 0
for xml path('')) code) sp) request_item_sub_type_1_code_text,

s.public_round_id,
prd.public_start_date,
(select left(sp.code, len(sp.code)-1) from (select (
select distinct sp.request_item_sub_type_1_code + '|', cast(sp.line_index as varchar) + ',' as [text()] from PublicRoundItem sp where sp.public_round_id = s.public_round_id for xml path('')
) code) sp) request_item_sub_type_1_code_public_line_text,


sap.name,
sap.house_number,
sap.province_name, 
sap.postal_name,

cast(0 as bit) is_add_more_address,

isnull(sir.value_1, pri.value_01) value_1,
isnull(sir.value_2, pri.value_02) value_2,
isnull(sir.value_3, pri.value_03) value_3,
isnull(sir.value_4, pri.value_04) value_4,
isnull(sir.value_5, pri.value_05) value_5,

'' considering_instruction_rule_remark,
'' document_role02_remark,
'' document_role02_receive_remark,
pr.post_round_remark,

--Contact.address_type_code
case 
when Contact.address_type_code = 'OWNER' then People.name 
when Contact.address_type_code = 'REPRESENTATIVE' then Representative.name
else Contact.name 
end contact_name,

case 
when Contact.address_type_code = 'OWNER' then People.information 
when Contact.address_type_code = 'REPRESENTATIVE' then Representative.information
else Contact.information 
end contact_information,

pr.is_deleted,
pr.created_by,
pr.created_date,
pr.updated_by,
pr.updated_date

from
PostRound pr
left join PostRoundInstructionRule pri on pri.post_round_id = pr.id
left join Save010InstructionRule sir on sir.id = pri.save010_instruction_rule_id and (sir.instruction_rule_code  like 'ROLE_%' or sir.instruction_rule_code  like 'CASE_%')
left join Save010 s on s.id = sir.save_id or s.id = pr.object_id
left join PublicRound prd on prd.id = s.public_round_id
left join RM_PostRoundTypeCode rm_pr on rm_pr.code = pr.post_round_type_code

outer apply (
	select top 1 pr_1.book_start_date, pr_1.book_end_date
	from PostRound pr_1
	join PostRoundInstructionRule pri_1 on pri_1.post_round_id = pr_1.id
	where pr_1.round_index = 1 and sir.id = pri_1.save010_instruction_rule_id
	order by pr_1.id desc
) pr_1 



---------------
left join Save010PublicPayment spp on spp.save_id = pr.object_id and spp.public_role05_consider_payment_status_code = 'DONE'
---

left join UM_User um_dr on um_dr.id = isnull(pr.document_role02_receiver_by, spp.public_role05_consider_payment_by)
left join UM_User um_sir on um_sir.id = sir.created_by
left join RM_Department rm_dg on rm_dg.id = um_sir.department_id
left join RM_DocumentRole02StatusCode rm_sirds on rm_sirds.code = pr.document_role02_status_code
left join RM_DocumentRole02ReceiveStatusCode rm_sirdrs on rm_sirdrs.code = pr.document_role02_receive_status_code
left join RM_ConsideringSimilarInstructionRuleCode rm_csi on rm_csi.code = sir.instruction_rule_code 
left join RM_PostRoundInstructionRuleCode rm_prirc on rm_prirc.code = pr.post_round_instruction_rule_code
left join RM_ConsideringSimilarInstructionRuleStatusCode rm_crsi on rm_crsi.code = sir.considering_instruction_rule_status_code
left join RM_DocumentRole02CheckStatusCode rm_rcsc on rm_rcsc.code = pri.document_role02_check_status_code

left join RM_ConsideringBookStatusCode rm_cbs on rm_cbs.code = sir.considering_book_status_code

left join RM_DocumentRole02PrintDocumentStatusCode rm_1 on rm_1.code = pr.document_role02_print_document_status_code
left join RM_DocumentRole02PrintCoverStatusCode rm_2 on rm_2.code = pr.document_role02_print_cover_status_code
left join RM_DocumentRole02PrintListStatusCode rm_3 on rm_3.code = pr.document_role02_print_list_status_code

left join RM_PostRoundActionPostStatusCode rm_4 on rm_4.code = pr.post_round_action_post_status_code
left join RM_PostRoundActionPostTypeCode rm_5 on rm_5.code = pr.post_round_action_post_type_code

outer apply (
select top 1 sap.name, sap.house_number, rm_pv.name province_name, rm_p.name postal_name
from Save010AddressPeople sap 
left join RM_AddressProvince rm_pv on rm_pv.code = sap.address_province_code
left join RM_AddressPostal rm_p on rm_p.code = sap.postal_code
where sap.save_id = s.id
) sap

outer apply (select top 1 s010p.address_type_code, s010p.name, 
s010p.house_number + N' เขต' + rm_sd.name + N' แขวง' + rm_sd.name + ' ' + rm_p.name + ' ' + rm_pt.name information
from Save010AddressContact s010p 
left join RM_AddressSubDistrict rm_sd on rm_sd.code = s010p.address_sub_district_code
left join RM_AddressDistrict rm_d on rm_d.code = s010p.address_district_code
left join RM_AddressProvince rm_p on rm_p.code = s010p.address_province_code
left join RM_AddressPostal rm_pt on rm_pt.code = s010p.postal_code
where s010p.save_id = s.id
) Contact

outer apply (select top 1 s010p.address_type_code, s010p.name, 
s010p.house_number + N' เขต' + rm_sd.name + N' แขวง' + rm_sd.name + ' ' + rm_p.name + ' ' + rm_pt.name information
from Save010AddressPeople s010p 
left join RM_AddressSubDistrict rm_sd on rm_sd.code = s010p.address_sub_district_code
left join RM_AddressDistrict rm_d on rm_d.code = s010p.address_district_code
left join RM_AddressProvince rm_p on rm_p.code = s010p.address_province_code
left join RM_AddressPostal rm_pt on rm_pt.code = s010p.postal_code
where s010p.save_id = s.id
) People

outer apply (select top 1 s010p.address_type_code, s010p.name, 
s010p.house_number + N' เขต' + rm_sd.name + N' แขวง' + rm_sd.name + ' ' + rm_p.name + ' ' + rm_pt.name information
from Save010AddressRepresentative s010p 
left join RM_AddressSubDistrict rm_sd on rm_sd.code = s010p.address_sub_district_code
left join RM_AddressDistrict rm_d on rm_d.code = s010p.address_district_code
left join RM_AddressProvince rm_p on rm_p.code = s010p.address_province_code
left join RM_AddressPostal rm_pt on rm_pt.code = s010p.postal_code
where s010p.save_id = s.id
) Representative

where 
--pr.post_round_type_code = 'SEND_MORE' 
pr.document_role02_status_code is not null or 
pr.document_role02_print_cover_status_code is not null or 
pr.document_role02_print_document_status_code is not null