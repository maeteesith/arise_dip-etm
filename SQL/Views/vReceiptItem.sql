﻿alter view vReceiptItem as
select 
r.id,
s.id save_id,
r01i.id request_01_item_id,
pr.id post_round_id,
r.reference_number,
r.receive_date,
--year(r.receipt_date) receipt_year,
ri.receipt_number,
ri.name,
ri.request_number,
r.requester_name,
r.payer_name,
ri.request_type_code,
rt.name request_type_name,
ri.total_price,
r.receiver_name,
N'นักวิชาการเงินและบัญชีปฏิบัติการ' position_name,

(select top 1 s010p.name from Save010AddressPeople s010p where s010p.save_id = s.id) people_name,
(select case when len(rs.code) = 0 then null else left(rs.code, len(rs.code)-1) end 
from (select (select rs.item_sub_type_1_code + ', ' as [text()] 
from Request01ItemSub rs where rs.request01_item_id = r01i.id 
group by rs.item_sub_type_1_code
order by rs.item_sub_type_1_code for xml path('')) code) rs) 
item_sub_type_1_code_text,

r.status_code receipt_status_code,
rm_r.name receipt_status_name,

r.is_deleted,
r.created_by,
r.created_date,
r.updated_by,
r.updated_date

from Receipt r
join ReceiptItem ri on ri.receipt_id = r.id
join RM_RequestType rt on rt.code = ri.request_type_code

left join Request01Item r01i on r01i.request_number = ri.request_number
left join PostRound pr on pr.id = ri.post_round_id and pr.reference_number = r.reference_number
left join save010 s on (s.request_number = ri.request_number or s.id = pr.object_id) and s.is_deleted = 0

--left join UM_User u on u.id = r.created_by
--left join RM_Department d on d.id = u.department_id
left join RM_ReceiptStatus rm_r on rm_r.code = r.status_code
--(ri.total_price > 0 and rm_r.code = r.status_code)-- or (ri.total_price = 0 and rm_r.code = 'PAID')

where ri.total_price > 0