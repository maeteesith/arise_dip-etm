alter view vCheckingSimilarResultDuplicate_Word as

select

sc.id,
sc.save_id,

s.request_number,

sc.word_translate_search,
sc.word_translate_sound,
sc.word_translate_translate,

sc.word_translate_dictionary_code,
rm_d.name word_translate_dictionary_name,
sc.word_translate_dictionary_other,

sc.checking_word_translate_status_code,
rm_s.name checking_word_translate_status_name,



sc.is_deleted,
sc.created_by,
sc.created_date,
sc.updated_by,
sc.updated_date

from Save010CheckingSimilarWordTranslate sc
join Save010 s on s.id = sc.save_id
left join RM_CheckingWordTranslateDictionaryCode rm_d on rm_d.code = sc.word_translate_dictionary_code
left join RM_CheckingWordTranslateStatusCode rm_s on rm_s.code = sc.checking_word_translate_status_code