alter view vOnlinePublicRound as
select 

max(pr.id) id, 

year(pr.public_start_date) public_year,
month(pr.public_start_date) public_month,

cast(0 as bit) is_deleted,
min(pr.created_by) created_by,
min(pr.created_date) created_date,
max(pr.updated_by) updated_by,
max(pr.updated_date) updated_date

from
PublicRound pr
where pr.is_deleted = 0 --and pr.public_round_status_code = 'DOING_PUBLIC'

group by
year(pr.public_start_date),
month(pr.public_start_date)