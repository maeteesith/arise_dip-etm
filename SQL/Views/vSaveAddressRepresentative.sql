﻿alter view vSaveAddressRepresentative as

select
b.*,

case when b.address_country_code != 'TH' then b.house_number
else 
b.house_number +
case when rm_asd.name is null then '' else N' เขต' + rm_asd.name end +
case when rm_ad.name is null then '' else N' แขวง' + rm_ad.name end +
case when rm_ap.name is null then '' else ' ' + rm_ap.name end +
case when b.postal_code is null then '' else b.postal_code end +
case when rm_ac.name is null then '' else N' ประเทศ' + rm_ac.name end
end address_information

from (
	select a.*
	from Save010AddressRepresentative a 
) b

left join RM_AddressSubDistrict rm_asd on rm_asd.code = b.address_sub_district_code
left join RM_AddressDistrict rm_ad on rm_ad.code = b.address_district_code
left join RM_AddressProvince rm_ap on rm_ap.code = b.address_province_code
--left join RM_AddressPostal rm_apt on rm_apt.code = b.postal_code
left join RM_AddressCountry rm_ac on rm_ac.code = b.address_country_code
where b.is_deleted = 0