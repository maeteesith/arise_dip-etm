alter view vPublicRequestItem as
select
s.id,

s.public_round_id,
s.request_number,
s.request_date,
pr.book_index,
pr.public_start_date,
pr.public_end_date,
s.public_role02_check_date,
sap.address_country_code,
sap.address_country_name,
sap.nationality_code address_nationality_code,
sap.address_nationality_name,

s.public_receive_do_status_code,
rm_prsd.name public_receive_do_status_name,
pr.public_round_status_code,
rm_prs.name public_round_status_name,
s.public_role02_check_status_code,
rm_pr2cs.name public_role02_check_status_name,

s.request_item_type_code,
rm_rit.name request_item_type_name,
s.sound_mark_list,

s.public_page_index,
s.public_line_index,
s.public_page_index + ' / ' + s.public_line_index public_page_index_line_index,

(select left(sp.code, len(sp.code)-1) from (select 
(select distinct sp.request_item_sub_type_1_code + ', ' as [text()] 
from Save010Product sp 
where sp.save_id = s.id and sp.is_deleted = 0
for xml path('')) 
code) sp) request_item_sub_type_1_code_text,

s.considering_receive_remark public_role2_check_remark,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from PublicRound pr
left join Save010 s on s.public_round_id = pr.id
left join RM_PublicReceiveDoStatusCode rm_prsd on rm_prsd.code = s.public_receive_do_status_code
left join RM_PublicRoundStatusCode rm_prs on rm_prs.code = pr.public_round_status_code
left join RM_RequestItemType rm_rit on rm_rit.code = s.request_item_type_code
cross apply (select top 1 
sap.address_country_code, rm_ac.name address_country_name,
sap.nationality_code, rm_an.name address_nationality_name 
from Save010AddressPeople sap 
left join RM_AddressCountry rm_ac on rm_ac.code = sap.address_country_code
left join RM_AddressNationality rm_an on rm_an.code = sap.nationality_code
where sap.save_id = s.id) sap
left join RM_PublicRole02CheckStatusCode rm_pr2cs on rm_pr2cs.code = s.public_role02_check_status_code