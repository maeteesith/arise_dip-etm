alter view vDocumentItem as
select s.*,
u.name document_classification_by_name,
rm_rt.value_3 request_type_value_3,
rm_dt.code document_type_code,
rm_dt.name document_type_name
from vSave010 s
left join RM_RequestType rm_rt on rm_rt.code = '010'
left join UM_User u on u.id = s.document_classification_by
left join RM_DocumentTypeCode rm_dt on rm_dt.code = 'DOCUMENT_CLASSIFICATION'
where s.save_status_code = 'SENT'
