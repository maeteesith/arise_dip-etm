alter view vCheckingSimilarResultDuplicate_Save as

select 

sc.id,
sc.save_id,
sc.save_tag_id,

s010.request_number save_request_number, 
s.request_number,

(select top 1 'File/Content/' + cast(rd.file_id as varchar) from RequestDocumentCollect rd where rd.save_id = s.id and rd.request_document_collect_type_code = 'TRADEMARK_2D' and rd.request_document_collect_status_code = 'ADD' and rd.file_id > 0) file_trademark_2d,
(select top 1 sp.name from Save010AddressPeople sp where sp.save_id = s.id and sp.is_deleted = 0) name,
(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.request_item_sub_type_1_code + ', ' as [text()] from Save010Product sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.request_item_sub_type_1_code order by sp.request_item_sub_type_1_code for xml path('')) code) sp) request_item_sub_type_1_code_text,
(select case when len(sp.word_mark) = 0 then null else left(sp.word_mark, len(sp.word_mark)-1) end from (select (select sp.word_mark + ', ' as [text()] from Save010DocumentClassificationWord sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.word_mark order by sp.word_mark for xml path('')) word_mark) sp) word_mark_list_text,

s.sound_file_id,
(select f.physical_path from [File] f where f.id = s.sound_file_id) sound_file_physical_path,
s.trademark_status_code,

sc.method_1,
sc.method_2,
sc.method_3,
sc.method_4,
sc.method_5,
sc.method_6,
sc.method_7,

sc.is_deleted,
sc.created_by,
sc.created_date,
sc.updated_by,
sc.updated_date

from Save010CheckingTagSimilar sc
join Save010 s010 on s010.id = sc.save_id
join Save010 s on s.id = sc.save_tag_id
