alter view vSave010PublicPayment as
select

spp.id,
spp.save_id,
s.public_round_id,
s.request_number,
s.request_date,

pr.public_start_date,
pr.public_end_date,

pr.book_index,
s.public_page_index,
s.public_line_index,
s.public_page_index + ' / ' + s.public_line_index public_page_index_line_index,

s.public_role02_check_date,
'' intruction_rule41_date,
/*
--�ѹ����·���¹͹��ѵ�
--sir.instruction_send_date,
--�ѹ����դ����
*/
spp.public_role02_consider_payment_date,
spp.public_role05_consider_payment_date,

(select left(sp.code, len(sp.code)-1) from (select (
select distinct sp.request_item_sub_type_1_code + ', ' as [text()] 
from Save010Product sp 
where sp.save_id = s.id and sp.is_deleted = 0
for xml path('')) 
code) sp) request_item_sub_type_1_code_text,

spp.total_price,

spp.public_role02_consider_payment_by,
um_r2.name public_role02_consider_payment_by_name,
spp.public_role05_consider_payment_by,
um_r5.name public_role05_consider_payment_by_name,

spp.public_role02_consider_payment_status_code,
rm_pr2cps.name public_role02_consider_payment_status_name,

spp.public_role05_consider_payment_status_code,
rm_pr5cps.name public_role05_consider_payment_status_name,

spp.public_role02_document_payment_status_code,
rm_pr2dps.name public_role02_document_payment_status_name,

ps_1.post_round_document_post_status_code post_round_document_post_status_1_code,
rm_prdpos_1.name post_round_document_post_status_1_name,
ps_2.post_round_document_post_status_code post_round_document_post_status_2_code,
rm_prdpos_2.name post_round_document_post_status_2_name,
isnull(ps_2.post_round_document_post_status_code, ps_1.post_round_document_post_status_code) post_round_document_post_status_code,

ps_1.id post_round_id_1,
ps_1.post_number post_number_1,
ps_1.book_number book_number_1,
ps_1.book_start_date book_start_1_date,
ps_1.book_end_date book_end_1_date,
ps_1.book_expired_day book_expired_day_1,
ps_1.book_acknowledge_date book_acknowledge_1_date,
ps_1.book_payment_date book_payment_1_date,
ps_1.reference_number reference_number_1,

ps_2.id post_round_id_2,
ps_2.post_number post_number_2,
ps_2.book_number book_number_2,
ps_2.book_start_date book_start_2_date,
ps_2.book_end_date book_end_2_date,
ps_2.book_expired_day book_expired_day_2,
ps_2.book_acknowledge_date book_acknowledge_2_date,
ps_2.book_payment_date book_payment_2_date,
ps_2.reference_number reference_number_2,

'' save010_public_payment_remark,
--��ҧ˹ѧ��� / ��. ���
isnull(ps_1.post_round_instruction_rule_code, 'RULE_5_3') post_round_instruction_rule_code,
rm_pdpi.name post_round_instruction_rule_name,

sac.name, sac.house_number,

'' book_remark_1,
'' book_remark_2,
'' book_remark,

spp.value_01,
spp.value_02,
spp.value_03,
spp.value_04,
spp.value_05,

spp.is_deleted,
spp.created_by,
spp.created_date,
spp.updated_by,
spp.updated_date

from Save010PublicPayment spp
join Save010 s on s.id = spp.save_id
left join PublicRound pr on pr.id = s.public_round_id

outer apply (select top 1 ps.* from PostRound ps where ps.object_id = s.id and ps.post_round_instruction_rule_code = 'RULE_5_3' and ps.round_index = 1 order by ps.id desc) ps_1
outer apply (select top 1 ps.* from PostRound ps where ps.object_id = s.id and ps.post_round_instruction_rule_code = 'RULE_5_3' and ps.round_index = 2 order by ps.id desc) ps_2

left join RM_PublicRole02ConsiderPaymentStatusCode rm_pr2cps on rm_pr2cps.code = spp.public_role02_consider_payment_status_code

left join UM_User um_r2 on um_r2.id = spp.public_role02_consider_payment_by
left join UM_User um_r5 on um_r5.id = spp.public_role05_consider_payment_by

left join RM_PublicRole05ConsiderPaymentStatusCode rm_pr5cps on rm_pr5cps.code = spp.public_role05_consider_payment_status_code
left join RM_PublicRole02DocumentPaymentStatusCode rm_pr2dps on rm_pr2dps.code = spp.public_role02_document_payment_status_code

left join RM_PublicDocumentPaymentInstructionRuleCode rm_pdpi on rm_pdpi.code = isnull(ps_1.post_round_instruction_rule_code, 'RULE_5_3')

left join RM_PostRoundDocumentPostStatusCode rm_prdpos_1 on rm_prdpos_1.code = ps_1.post_round_document_post_status_code
left join RM_PostRoundDocumentPostStatusCode rm_prdpos_2 on rm_prdpos_2.code = ps_2.post_round_document_post_status_code

cross apply (select top 1 
sac.name, sac.house_number
from Save010AddressPeople sac 
where sac.save_id = s.id) sac