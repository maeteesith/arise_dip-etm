alter view vOnlinePublicRoundItem as

select 
pri.id,
pr.id pr_id,
pri.request_item_sub_type_1_code,
pri.line_index,
pri.request_number,
pr.public_start_date,
pr.public_end_date,
pri.trademark_2d_file_id,
pri.sound_mark_file_id,
pri.sound_mark_list,
pri.request_item_type_code,

pri.save_id,
pri.request_item_sub_status_code,
pri.cancel_by,
pri.cancel_by_name,
pri.cancel_reason,

pr.is_deleted,
pri.created_by,
pri.created_date,
pri.updated_by,
pri.updated_date

from PublicRound pr
join PublicRoundItem pri on pri.public_round_id = pr.id and pri.is_deleted = 0