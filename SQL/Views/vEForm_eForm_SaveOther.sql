﻿alter view vEForm_eForm_SaveOther as

select 
cast(0 as bigint) id,
cast(0 as bigint) request_other_process_id,
s.id save_id,
cast(0 as bigint) request_other_item_sub_id,
cast(0 as bigint) request_other_request_01_item_id,

e.eform_id,
e.eform_number,
e.telephone,

s.request_number,
s.registration_number,
'' reference_number,
'' request_index,
s.trademark_expired_start_date,
s.trademark_expired_date,
s.trademark_expired_end_date,
s.total_price save_total_price,

1 item_count,
0 item_sub_type_1_count,
0 product_count,
rm_rt.code request_type_code,
rm_rt.name request_type_name,
rm_rt.value_1 total_price,
0 fine,
cast(0 as bit) is_evidence_floor7,
cast(0 as bit) is_sue,

(select top 1 sp.name from Save010AddressPeople sp where sp.save_id = s.id and sp.is_deleted = 0) name,

e.is_deleted,
e.created_by,
e.created_date,
e.updated_by,
e.updated_date

from 

(
select  
e.id eform_id,
e.eform_number,
e.telephone,
e.request_number,
'20' request_type_code,
e.is_deleted,
e.created_by,
e.created_date,
e.updated_by,
e.updated_date
from
eForm_Save020 e

union all

select  
e.id eform_id,
e.eform_number,
e.telephone,
e.request_number,
'40' request_type_code,
e.is_deleted,
e.created_by,
e.created_date,
e.updated_by,
e.updated_date
from
eForm_Save040 e

union all

select  
e.id eform_id,
e.eform_number,
e.telephone,
e.request_number,
'120' request_type_code,
e.is_deleted,
e.created_by,
e.created_date,
e.updated_by,
e.updated_date
from
eForm_Save120 e


) e
join Save010 s on s.request_number = e.request_number
join RM_RequestType rm_rt on rm_rt.code = e.request_type_code
/*
join RM_RequestItemType rm_ri on rm_ri.code = e.save010_mark_type_type_code
left join [FileGuest] f on f.id = e.sound_file_id
left join RM_RequestType rm_ry1 on rm_ry1.code = '010'
left join RM_RequestType rm_ry2 on rm_ry2.code = (case when e.img_w > 5 or e.img_h > 5 then '210' else null end)
*/
--where e.id = 170