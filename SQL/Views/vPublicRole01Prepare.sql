alter view vPublicRole01Prepare as

select 
s.id,
s.public_send_date,
sp.request_item_sub_type_1_code,

--sap.address_nationality_code,
--sap.address_nationality_name,

(select top 1 rm_an.code from Save010AddressPeople s010p 
left join RM_AddressNationality rm_an on rm_an.code = s010p.nationality_code 
where s010p.save_id = s.id) address_nationality_code,
(select top 1 rm_an.name from Save010AddressPeople s010p 
left join RM_AddressNationality rm_an on rm_an.code = s010p.nationality_code 
where s010p.save_id = s.id) address_nationality_name,

s.public_receiver_by,
u_prb.name public_receiver_name,

 pr.book_index,

s.request_item_type_code,
rm_rit.name request_item_type_name,
s.sound_mark_list,

s.request_number,
s.request_date, 

''  public_receive_do_remark,
--s.considering_receive_remark

s.public_receive_do_status_code,
rm_pds.name public_recevie_do_status_name,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from Save010 s
left join PublicRound pr on pr.id = s.public_round_id
left join UM_User u_prb on u_prb.id = s.public_receiver_by
--join Save010Product sp on sp.save_id = s.id
outer apply (
select sp.save_id, sp.request_item_sub_type_1_code 
from Save010Product sp 
where sp.save_id = s.id and sp.is_deleted = 0
group by sp.save_id, sp.request_item_sub_type_1_code) sp 



left join RM_RequestItemType rm_rit on rm_rit.code = s.request_item_type_code
left join RM_PublicReceiveDoStatusCode rm_pds on rm_pds.code = s.public_receive_do_status_code

where s.public_receive_do_status_code is not null