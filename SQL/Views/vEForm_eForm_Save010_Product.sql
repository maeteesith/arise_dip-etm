alter view vEForm_eForm_Save010_Product as

select 
cast(0 as bigint) id,
cast(0 as bigint) request01_item_id,

e.id eform_id,

p.request_item_sub_type_1_code,
p.product_count,
(case when p.product_count > 5 then 9000 else p.product_count * 1000 end) total_price,

e.is_deleted,
e.created_by,
e.created_date,
e.updated_by,
e.updated_date

from eForm_Save010 e

cross apply (
select p.request_item_sub_type_1_code, count(1) product_count
from eForm_Save010Product p 
where p.save_id = e.id and p.is_deleted = 0
group by p.request_item_sub_type_1_code
) p
--join RM_RequestItemType rm_ri on rm_ri.code = p.save010_mark_type_type_code
