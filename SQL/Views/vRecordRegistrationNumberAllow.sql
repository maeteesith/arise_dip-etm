alter view vRecordRegistrationNumberAllow as
select

s.id,

s.request_number,

s.floor3_proposer_by,
um_u_fp.name floor3_proposer_by_name,
s.floor3_proposer_date,
s.floor3_registrar_by,
s.floor3_registrar_date,
s.floor3_registrar_description,

s.request_item_type_code,
rm_rit.name request_item_type_name,

s.floor3_registrar_status_code,
rm_rsc.name floor3_registrar_status_name,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from Save010 s
left join RM_RequestItemType rm_rit on rm_rit.code = s.request_item_type_code
left join RM_Floor3RegistrarStatusCode rm_rsc on rm_rsc.code = s.floor3_registrar_status_code
left join UM_User um_u_fp on um_u_fp.id = s.floor3_proposer_by

where s.floor3_registrar_status_code is not null