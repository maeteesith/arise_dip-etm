alter view vOnlinePublicList as

select 
max(pr.id) id,
year(pr.public_start_date) public_year,
month(pr.public_start_date)public_month,
cast(max(cast(pr.is_deleted as int)) as bit) is_deleted,
max(pr.created_by) created_by,
max(pr.created_date) created_date,
max(pr.updated_by) updated_by,
max(pr.updated_date) updated_date

from PublicRound pr
where pr.public_round_status_code = 'DOING_PUBLIC' and pr.is_deleted = 0

group by pr.public_start_date