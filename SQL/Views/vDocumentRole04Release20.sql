alter view vDocumentRole04Release20 as
select 

isnull(d.id, 0) id,
s.id save_id,
s.request_number,

so.department_send_date,
so.request_type_code,
rm_rt.name request_type_name,

d.document_role04_release_20_type_code,
rm_dc.name document_role04_release_20_type_name,

d.document_role04_release_20_check_list,

rm_d4s.code document_role04_release_20_status_code,
rm_d4s.name document_role04_release_20_status_name,
d.document_role05_release_20_status_code,
rm_d5s.name document_role05_release_20_status_name,

d.document_role04_release_20_instruction_rule_code instruction_rule_code,
rm_pir.name instruction_rule_name,
rm_pir.description instruction_rule_description,

d.document_role04_release_20_send_date,
d.document_role05_release_20_send_date,

d.value_01,
d.value_02,
d.value_03,
d.value_04,
d.value_05,

d.document_role05_by,
d.document_role05_date,

d.document_role05_status_code,
rm_ds.name document_role05_status_name,

d.document_role05_release_20_by,
um_d5c.name document_role05_release_20_by_name,

sp.name,
	
isnull(d.is_deleted, 0) is_deleted,
isnull(d.created_by, 0) created_by,
isnull(d.created_date, GETDATE()) created_date,
d.updated_by,
d.updated_date

from SaveOther so
join Save010 s on s.request_number = so.request_number
left join DocumentRole04Release20 d on d.save_id = s.id

left join RM_RequestType rm_rt on rm_rt.code = so.request_type_code

left join RM_DocumentRole04Release20TypeCode rm_dc on rm_dc.code = d.document_role04_release_20_type_code
left join RM_DocumentRole04Release20StatusCode rm_d4s on rm_d4s.code = isnull(d.document_role04_release_20_status_code, 'DRAFT')
left join RM_DocumentRole05Release20StatusCode rm_d5s on rm_d5s.code = d.document_role05_release_20_status_code
left join RM_PostRoundInstructionRuleCode rm_pir on rm_pir.code = d.document_role04_release_20_instruction_rule_code
left join RM_DocumentRole05StatusCode rm_ds on rm_ds.code = d.document_role05_status_code
left join UM_User um_d5c on um_d5c.id = d.document_role05_release_20_by

outer apply (
select top 1 sp.name 
from Save010AddressPeople sp 
where sp.save_id = s.id and sp.is_deleted = 0	
) sp
	
where s.is_deleted = 0 and so.request_type_code = '200' and so.department_send_code = 'DOCUMENT'