alter view vPostRound as

select 
p.id,
p.object_id,
p.save_id,

p.book_number,
s.request_number,
p.post_number,

s.request_date,
pr.public_end_date,
spp.public_role05_consider_payment_date post_check_date,
spp.public_role05_consider_payment_date,
p.book_start_date,
p.book_acknowledge_date,
p.book_payment_date,
p.reference_number,

p.instruction_rule_code post_round_instruction_rule_code,
p.instruction_rule_name post_round_instruction_rule_name,

p.post_round_type_code ,
rm_prt.name post_round_type_name,

p.post_round_document_post_status_code,
rm_prdp.name post_round_document_post_status_name,

p.post_round_action_post_status_code ,
rm_prap.name post_round_action_post_status_name,

p.post_round_action_post_type_code,
rm_prapt.name post_round_action_post_type_name,

sc.name,
sc.house_number,

p.post_round_remark,


p.is_deleted,
p.created_by,
p.created_date,
p.updated_by,
p.updated_date

from 
(

select  
p_.*, p_.object_id save_id, p_.post_round_instruction_rule_code instruction_rule_code, rm_prir.name instruction_rule_name
from PostRound p_
left join RM_PostRoundInstructionRuleCode rm_prir on rm_prir.code = p_.post_round_instruction_rule_code
where p_.object_id is not null
union all
select  
p_.*, sir.save_id, 
(
select case when sp.code is null or len(sp.code) = 0 then '' else left(sp.code, len(sp.code)-1)  end
from (select (select sir.instruction_rule_code + ', ' as [text()] 
from Save010InstructionRule sir
join PostRoundInstructionRule pri on pri.save010_instruction_rule_id = sir.id
where pri.post_round_id = p_.id
for xml path('')) code) sp) instruction_rule_code, 
(
select case when sp.name is null or len(sp.name) = 0 then '' else left(sp.name, len(sp.name)-1)  end
from (select (select rm_1.name + ', ' as [text()] 
from Save010InstructionRule sir
join PostRoundInstructionRule pri on pri.save010_instruction_rule_id = sir.id
left join RM_PostRoundInstructionRuleCode rm_1 on rm_1.code = sir.instruction_rule_code
where pri.post_round_id = p_.id
for xml path('')) name) sp) instruction_rule_name
from PostRound p_
outer apply (select top 1 sir.* from Save010InstructionRule sir join PostRoundInstructionRule pri on pri.save010_instruction_rule_id = sir.id where pri.post_round_id = p_.id) sir
where p_.object_id is null
) p
left join Save010 s on s.id = p.object_id or s.id = p.save_id
left join PublicRound pr on pr.id = s.public_round_id and pr.public_round_status_code = 'DONE'
left join Save010PublicPayment spp on spp.save_id = s.id and spp.public_role05_consider_payment_status_code = 'DONE'
left join RM_PostRoundTypeCode rm_prt on rm_prt.code = p.post_round_type_code
left join RM_PostRoundDocumentPostStatusCode rm_prdp on rm_prdp.code = p.post_round_document_post_status_code 
left join RM_PostRoundActionPostStatusCode rm_prap on rm_prap.code = p.post_round_action_post_status_code
left join RM_PostRoundActionPostTypeCode rm_prapt on rm_prapt.code = p.post_round_action_post_type_code
outer apply (select top 1 sc.name, sc.house_number from Save010AddressContact sc where sc.save_id = s.id order by sc.id desc) sc
--where p.post_round_type_code = 'SEND_MORE' or (spp.id is not null and pr.id is not null)