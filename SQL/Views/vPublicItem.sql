alter view vPublicItem as
select 
s.id,
s.request_date,
sr.instruction_send_date,
s.public_receive_date,
s.request_number,

s.request_item_type_code,
rm_rit.name request_item_type_name,
s.sound_mark_list,

s.public_type_code,
rm_pt.name public_type_name,

s.public_source_code,
rm_pso.name public_source_name,

s.public_status_code,
rm_pst.name public_status_name,

s.public_receive_status_code,
rm_prs.name public_receive_status_name,

s.public_receiver_by,
u_r.name public_receiver_name, 

'' public_remark,
'' public_receive_remark,

(select count(1) from Save010AddressJoiner sa where sa.save_id = s.id and sa.is_deleted = 0) count_joiner,
(select count(1) from Save010CertificationFile sa where sa.save_id = s.id and sa.is_deleted = 0) count_certification_file,
cast (CHARINDEX('SOUND', s.request_mark_feature_code_list) as bit) is_sound,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from Save010 s
join Save010InstructionRule sr on sr.save_id = s.id and sr.instruction_rule_code = 'PUBLIC' and sr.considering_instruction_status_code != 'DELETE'
left join RM_RequestItemType rm_rit on rm_rit.code = s.request_item_type_code
left join RM_PublicTypeCode rm_pt on rm_pt.code = s.public_type_code
left join RM_PublicSourceCode rm_pso on rm_pso.code = s.public_source_code
left join RM_PublicStatusCode rm_pst on rm_pst.code = s.public_status_code
left join RM_PublicReceiveStatusCode rm_prs on rm_prs.code = s.public_receive_status_code
left join UM_User u_r on u_r.id = s.public_receiver_by
where s.public_status_code is not null