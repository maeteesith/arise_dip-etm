alter view vCheckingSimilarRequestItemSubTypeReport as

select 
s.id,

s.request_number,
s.request_date,

(select top 1 'File/Content/' + cast(rd.file_id as varchar) from RequestDocumentCollect rd where rd.save_id = s.id and rd.request_document_collect_type_code = 'TRADEMARK_2D' and rd.request_document_collect_status_code = 'ADD' and rd.file_id > 0) file_trademark_2d,
(select top 1 sp.name from Save010AddressPeople sp where sp.save_id = s.id and sp.is_deleted = 0) name,
(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.request_item_sub_type_1_code + ', ' as [text()] from Save010Product sp where sp.save_id = s.id and sp.is_deleted = 0 group by sp.request_item_sub_type_1_code order by sp.request_item_sub_type_1_code for xml path('')) code) sp) request_item_sub_type_1_code_text,
(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select rm_r.value_1 + ', ' as [text()] from Save010Product sp left join RM_RequestItemSubTypeGroup rm_r on rm_r.code = sp.request_item_sub_type_1_code  where sp.save_id = s.id and sp.is_deleted = 0 group by sp.request_item_sub_type_1_code, rm_r.value_1 order by sp.request_item_sub_type_1_code for xml path('')) code) sp) request_item_sub_type_group_code_text,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
isnull(s.updated_date, s.created_date) updated_date

from Save010 s