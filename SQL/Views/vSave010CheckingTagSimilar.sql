alter view [dbo].[vSave010CheckingTagSimilar] as
select 
s.*, 
m.save_id, 
m.save_tag_id, 
m.is_same, 
m.is_like_approve, 
m.is_same_approve, 
m.method_1, m.method_2, m.method_3, m.method_4, m.method_5, m.method_6, m.method_7, 
m.is_owner_same,
(select isnull(count(1), 0) from Save010Case28 s10 where s10.save_id = s.id and s10.is_deleted = 0) case10_count,

m.is_created_instruction_rule,
m.instruction_rule_code,
m.instruction_rule_value_1

from vSave010 s
join Save010CheckingTagSimilar m on m.save_tag_id = s.id
