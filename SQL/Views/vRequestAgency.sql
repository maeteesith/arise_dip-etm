alter view vRequestAgency as
select 
a.id, a.name, a.is_wave_fee, 
a.agency_group_id, ag.name agency_group_name,
a.is_deleted,a.created_by,a.created_date, a.updated_by, a.updated_date 
from RequestAgency a
join RequestAgencyGroup ag on ag.id = a.agency_group_id
where a.is_deleted = 0 and ag.is_deleted = 0