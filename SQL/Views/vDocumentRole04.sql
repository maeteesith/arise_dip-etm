﻿alter view vDocumentRole04 as

select 

pr.id,

s.id save_id,
s.request_number,

pr.book_end_date,

rm_d4s.code document_role04_status_code,
rm_d4s.name document_role04_status_name,

N'กลุ่มงานออกหนังสือ' document_role04_from_department_name,
rm_d4t.code document_role04_type_code,
rm_d4t.name document_role04_type_name,
pr.document_role04_receive_date,
pr.document_role04_receiver_by,
u_r4.name document_role04_receiver_by_name,
pr.document_role04_receive_remark,
rm_d4r.code document_role04_receive_status_code,
rm_d4r.name document_role04_receive_status_name,
pr.document_role04_receive_send_date,

rm_d5s.code document_role05_status_code,
rm_d5s.name document_role05_status_name,
pr.document_role05_receive_date,
pr.document_role05_receiver_by,
u_r5.name document_role05_receiver_by_name,
rm_d5r.code document_role05_receive_status_code,
rm_d5r.name document_role05_receive_status_name,
pr.document_role05_receive_send_date,

/*
rm04_i.code public_role04_item_type_code,
rm04_i.name public_role04_item_type_name,
*/

pr.is_deleted,
pr.created_by,
pr.created_date,
pr.updated_by,
pr.updated_date

from PostRound pr
left join UM_User u_r4 on u_r4.id = pr.document_role04_receiver_by
left join RM_DocumentRole04TypeCode rm_d4t on rm_d4t.code = 'DOCUMENT'
left join RM_DocumentRole04StatusCode rm_d4s on rm_d4s.code = isnull(pr.document_role04_status_code, 'WAIT')
left join RM_DocumentRole04ReceiveStatusCode rm_d4r on rm_d4r.code = pr.document_role04_receive_status_code

left join UM_User u_r5 on u_r5.id = pr.document_role05_receiver_by
left join RM_DocumentRole05StatusCode rm_d5s on rm_d5s.code = isnull(pr.document_role05_status_code, 'WAIT')
left join RM_DocumentRole05ReceiveStatusCode rm_d5r on rm_d5r.code = pr.document_role05_receive_status_code

cross apply (
select top 1 s.id, s.request_number
from Save010 s
left join Save010InstructionRule sr on sr.save_id = s.id 
left join PostRoundInstructionRule pri on pri.save010_instruction_rule_id = sr.id
where pri.post_round_id = pr.id or s.id = pr.object_id
) s


where pr.round_index = 1
--and pr.book_end_date < GETDATE()