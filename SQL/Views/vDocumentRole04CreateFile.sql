alter view vDocumentRole04CreateFile as
select 

df.id,
d.id document_role04_create_id,

df.remark,

df.created_date file_created_date,
um_df.id file_created_by,
um_df.name file_created_by_name,

df.file_id,
f.file_name,
f.file_size,

df.is_deleted,
df.created_by,
df.created_date,
df.updated_by,
df.updated_date

from DocumentRole04Create d
join DocumentRole04CreateFile df on df.document_role04_create_id = d.id
join [File] f on f.id = df.file_id
left join UM_User um_df on um_df.id = df.created_by