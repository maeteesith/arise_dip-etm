﻿alter view vSaveAddressAll as

select
b.id, b.save_id, 
b.name,
case when b.address_country_code != 'TH' then b.house_number
else 
b.house_number +
case when rm_asd.name is null then '' else N' เขต' + rm_asd.name end +
case when rm_ad.name is null then '' else N' แขวง' + rm_ad.name end +
case when rm_ap.name is null then '' else ' ' + rm_ap.name end +
case when b.postal_code is null then '' else b.postal_code end +
case when rm_ac.name is null then '' else N' ประเทศ' + rm_ac.name end
end address_information,

b.is_deleted, b.created_by, b.created_date, b.updated_by, b.updated_date 

from (
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save010AddressPeople a 
union all
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save010AddressRepresentative a 
union all
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save010AddressContact a where a.address_type_code = 'OTHERS'

union all
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save040AddressPeople a 
union all
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save040AddressRepresentative a 
union all
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save040AddressContact a where a.address_type_code = 'OTHERS'

union all
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save060AddressPeople a 
union all
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save060AddressRepresentative a 
union all
select a.id, a.save_id, a.name, a.house_number, a.address_sub_district_code, a.address_district_code, a.address_province_code, a.address_country_code, a.postal_code, a.is_deleted, a.created_by, a.created_date, a.updated_by, a.updated_date 
from Save060AddressContact a where a.address_type_code = 'OTHERS'
) b
left join RM_AddressSubDistrict rm_asd on rm_asd.code = b.address_sub_district_code
left join RM_AddressDistrict rm_ad on rm_ad.code = b.address_district_code
left join RM_AddressProvince rm_ap on rm_ap.code = b.address_province_code
--left join RM_AddressPostal rm_apt on rm_apt.code = b.postal_code
left join RM_AddressCountry rm_ac on rm_ac.code = b.address_country_code
where b.is_deleted = 0