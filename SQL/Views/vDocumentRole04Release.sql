alter view vDocumentRole04Release as

select
isnull(d.id, 0) id,

p.*,

d.document_role04_release_type_code,
rm_dc.name document_role04_release_type_name,

d.document_role04_release_check_list,

rm_d4s.code document_role04_release_status_code,
rm_d4s.name document_role04_release_status_name,
d.document_role05_release_status_code,
rm_d5s.name document_role05_release_status_name,

d.document_role04_release_instruction_rule_code instruction_rule_code,
rm_pir.name instruction_rule_name,
rm_pir.description instruction_rule_description,

d.document_role04_release_send_date,
d.document_role05_release_send_date,

d.value_01,
d.value_02,
d.value_03,
d.value_04,
d.value_05,

d.document_role05_by,
d.document_role05_date,

d.document_role05_status_code,
rm_ds.name document_role05_status_name,

d.document_role05_release_by,
um_d5c.name document_role05_release_by_name,
	
isnull(d.is_deleted, 0) is_deleted,
isnull(d.created_by, 0) created_by,
isnull(d.created_date, GETDATE()) created_date,
d.updated_by,
d.updated_date

from (
	select
	sir.save_id,
	s.request_number,

	min(pr.book_end_date) book_end_date,

	max(sp.name) name,
	max(pa.address_information) contact_address_information

	from PostRound pr
	
	outer apply (
	select top 1 sir.save_id
	from PostRoundInstructionRule pir
	join Save010InstructionRule sir on sir.id = pir.save010_instruction_rule_id
	where pir.post_round_id = pr.id
	) sir
	
	outer apply (
	select top 1 sp.name 
	from Save010AddressPeople sp 
	where sp.save_id = sir.save_id and sp.is_deleted = 0	
	) sp
	
	outer apply (
	select top 1 address_information 
	from PostRoundAddress pa 
	where pa.post_round_id = pr.id and pa.is_deleted = 0
	) pa

	join Save010 s on s.id = sir.save_id

	where --pr.round_index = 2 and 
	--pr.book_end_date > getdate() and 
	isnull(pr.post_round_action_post_type_code, 'NOT_COMPLETE') != 'OK'

	group by 
	sir.save_id,
	s.request_number
) p
left join DocumentRole04Release d on d.save_id = p.save_id

left join RM_DocumentRole04ReleaseTypeCode rm_dc on rm_dc.code = d.document_role04_release_type_code
left join RM_DocumentRole04ReleaseStatusCode rm_d4s on rm_d4s.code = isnull(d.document_role04_release_status_code, 'DRAFT')
left join RM_DocumentRole05ReleaseStatusCode rm_d5s on rm_d5s.code = d.document_role05_release_status_code
left join RM_PostRoundInstructionRuleCode rm_pir on rm_pir.code = d.document_role04_release_instruction_rule_code
left join RM_DocumentRole05StatusCode rm_ds on rm_ds.code = d.document_role05_status_code
left join UM_User um_d5c on um_d5c.id = d.document_role05_release_by