USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole04ReleaseRequestTypeCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole04ReleaseRequestTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole04ReleaseRequestTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] ON 

INSERT [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'CHANGE_STATUS', N'เปลี่ยนสถานะ', 0, CAST(N'2020-06-03T09:03:21.2266667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'PUBLIC', N'ประกาศโฆษณา', 0, CAST(N'2020-05-31T21:46:47.8466667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'REGISTER', N'จดทะเบียน', 0, CAST(N'2020-05-31T21:47:04.3166667' AS DateTime2), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] OFF
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseRequestTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseRequestTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseRequestTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseRequestTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseRequestTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
