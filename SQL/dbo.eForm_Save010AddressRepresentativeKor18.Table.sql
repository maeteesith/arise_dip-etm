USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save010AddressRepresentativeKor18]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save010AddressRepresentativeKor18](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[descrition] [nvarchar](1000) NULL,
	[address_type_code] [nvarchar](50) NULL,
	[nationality_code] [nvarchar](50) NULL,
	[career_code] [nvarchar](50) NULL,
	[card_type_code] [nvarchar](50) NULL,
	[card_number] [nvarchar](50) NULL,
	[receiver_type_code] [nvarchar](50) NULL,
	[name] [nvarchar](1000) NULL,
	[house_number] [nvarchar](1000) NULL,
	[village_number] [nvarchar](500) NULL,
	[alley] [nvarchar](500) NULL,
	[street] [nvarchar](500) NULL,
	[address_sub_district_code] [nvarchar](50) NULL,
	[address_district_code] [nvarchar](50) NULL,
	[address_province_code] [nvarchar](50) NULL,
	[postal_code] [nvarchar](50) NULL,
	[address_country_code] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[fax] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[sex_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[address_representative_condition_type_code] [nvarchar](50) NULL,
	[country_text] [nvarchar](1000) NULL,
	[is_contact_person] [bit] NULL,
	[other_text] [nvarchar](1000) NULL,
	[representative_type_code] [nvarchar](50) NULL,
 CONSTRAINT [PK_eForm_Save010AddressRepresentativeKor18] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] ADD  CONSTRAINT [DF_eForm_Save010AddressRepresentativeKor18_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] ADD  CONSTRAINT [DF_eForm_Save010AddressRepresentativeKor18_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] ADD  CONSTRAINT [DF_eForm_Save010AddressRepresentativeKor18_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_eForm_Save010] FOREIGN KEY([save_id])
REFERENCES [dbo].[eForm_Save010] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_eForm_Save010]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressCardType] FOREIGN KEY([card_type_code])
REFERENCES [dbo].[RM_AddressCardType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressCardType]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressCareer] FOREIGN KEY([career_code])
REFERENCES [dbo].[RM_AddressCareer] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressCareer]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressCountry] FOREIGN KEY([address_country_code])
REFERENCES [dbo].[RM_AddressCountry] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressCountry]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressDistrict] FOREIGN KEY([address_district_code])
REFERENCES [dbo].[RM_AddressDistrict] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressDistrict]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressNationality] FOREIGN KEY([nationality_code])
REFERENCES [dbo].[RM_AddressNationality] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressNationality]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressProvince] FOREIGN KEY([address_province_code])
REFERENCES [dbo].[RM_AddressProvince] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressProvince]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressSubDistrict] FOREIGN KEY([address_sub_district_code])
REFERENCES [dbo].[RM_AddressSubDistrict] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_AddressSubDistrict]
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_RepresentativeType] FOREIGN KEY([representative_type_code])
REFERENCES [dbo].[RM_RepresentativeType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010AddressRepresentativeKor18] CHECK CONSTRAINT [FK_eForm_Save010AddressRepresentativeKor18_RM_RepresentativeType]
GO
