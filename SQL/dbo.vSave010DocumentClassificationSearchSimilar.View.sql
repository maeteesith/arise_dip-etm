USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vSave010DocumentClassificationSearchSimilar]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vSave010DocumentClassificationSearchSimilar] as

select

s.id,

s.request_number,

(select top 1 'File/Content/' + cast(rd.file_id as varchar) from RequestDocumentCollect rd where rd.save_id = s.id and rd.request_document_collect_type_code = 'TRADEMARK_2D' and rd.request_document_collect_status_code = 'ADD' and rd.file_id > 0) file_trademark_2d,


(select case when len(sp.name) = 0 then null else left(sp.name, len(sp.name)-1) end 
from (select (select sp.name + ', ' as [text()] 
from Save010AddressPeople sp where sp.save_id = s.id and sp.is_deleted=0
order by sp.id for xml path('')) name) sp
) people_name_list_text,

(select case when len(sp.word_mark) = 0 then null else left(sp.word_mark, len(sp.word_mark)-1) end 
from (select (select sd.word_mark + ', ' as [text()] 
from Save010DocumentClassificationWord sd where sd.save_id = s.id and sd.is_deleted=0
order by sd.id for xml path('')) word_mark) sp
) word_mark_list_text,

(select case when len(sp.document_classification_image_code) = 0 then null else left(sp.document_classification_image_code, len(sp.document_classification_image_code)-1) end 
from (select (select sd.document_classification_image_code + ', ' as [text()] 
from Save010DocumentClassificationImage sd where sd.save_id = s.id and sd.is_deleted=0
order by sd.id for xml path('')) document_classification_image_code) sp
) document_classification_image_code_list_text,

(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end 
from (select (select sp.request_item_sub_type_1_code + ', ' as [text()] 
from Save010Product sp where sp.save_id = s.id group by sp.request_item_sub_type_1_code 
order by sp.request_item_sub_type_1_code for xml path('')) code) sp
) request_item_sub_type_1_code_text,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by
 
from Save010 s
where s.document_classification_status_code = 'SEND'
GO
