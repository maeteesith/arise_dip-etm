USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentRole04Check]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vDocumentRole04Check] as
select 

pr.id,
pri.id post_round_instruction_rule_id,
sir.id instruction_rule_id,
sir.save_id,

rm_ir.code instruction_rule_code,
rm_ir.name instruction_rule_name,
rm_ir.description instruction_rule_description,

pri.new_instruction_rule_code,
rm_ir_n.name new_instruction_rule_name,
rm_ir_n.description new_instruction_rule_description,

pri.document_role04_check_remark,

isnull(sir.created_date, pr.created_date) make_date,
pr.book_start_date,
pr.book_end_date,

pr.document_role04_receive_check_list,

pr.post_round_action_post_type_code,
rm_pat.name post_round_action_post_type_name,

pr.book_acknowledge_date,

pri.document_role04_send_type_code,
rm_d4s.name document_role04_send_type_name,

pr.document_role04_receive_status_code,
rm_d4r.name document_role04_receive_status_name,

pri.document_role04_check_date,

pr.document_role05_receiver_by,
um_r5.name document_role05_receiver_by_name,

pr.document_role05_receive_status_code,
rm_d5r.name document_role05_receive_status_name,

--s.considering_receiver_by,
--um_cd.name considering_receiver_by_name,

pr.is_deleted,
pr.created_by,
pr.created_date,
pr.updated_by,
pr.updated_date

from PostRound pr
left join PostRoundInstructionRule pri on pri.post_round_id = pr.id
left join Save010InstructionRule sir on sir.id = pri.save010_instruction_rule_id
left join Save010 s on sir.save_id = s.id or s.id = pr.object_id
left join RM_ConsideringSimilarInstructionRuleCode rm_ir on rm_ir.code = isnull(sir.instruction_rule_code, pr.post_round_instruction_rule_code)
left join RM_PostRoundActionPostTypeCode rm_pat on rm_pat.code = pr.post_round_action_post_type_code
left join RM_DocumentRole04SendTypeCode rm_d4s on rm_d4s.code = pri.document_role04_send_type_code
left join RM_DocumentRole04ReceiveStatusCode rm_d4r on rm_d4r.code = pr.document_role04_receive_status_code
left join RM_DocumentRole05ReceiveStatusCode rm_d5r on rm_d5r.code = pr.document_role05_receive_status_code

left join RM_ConsideringSimilarInstructionRuleCode rm_ir_n on rm_ir_n.code = pri.new_instruction_rule_code

left join UM_User um_r5 on um_r5.id = pr.document_role05_receiver_by
--left join UM_User um_cd on um_cd.id = s.considering_receiver_by
--where pr.round_index = 1
where pr.document_role04_receive_status_code is not null
GO
