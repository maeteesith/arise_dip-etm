USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ConsideringInstructionStatusCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ConsideringInstructionStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ConsideringInstructionStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ConsideringInstructionStatusCode] ON 

INSERT [dbo].[RM_ConsideringInstructionStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 3, N'DELETE', N'ลบ', 0, CAST(N'2020-01-14T09:29:39.3300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringInstructionStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'DRAFT', N'ร่าง', 0, CAST(N'2020-01-14T09:29:35.8400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringInstructionStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 2, N'DRAFT_FIX', N'รอแก้ไข', 0, CAST(N'2020-05-26T16:47:19.6900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringInstructionStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 4, N'SEND', N'ส่งคำสั่งแล้ว', 0, CAST(N'2020-01-14T14:19:05.2900000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_ConsideringInstructionStatusCode] OFF
ALTER TABLE [dbo].[RM_ConsideringInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringInstructionStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ConsideringInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringInstructionStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_ConsideringInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringInstructionStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ConsideringInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringInstructionStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ConsideringInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringInstructionStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
