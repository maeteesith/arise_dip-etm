USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestSoundType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestSoundType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_ItemSubSoundType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestSoundType] ON 

INSERT [dbo].[RM_RequestSoundType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'ANIMAL', N'เสียงสัตว์', 0, CAST(N'2019-12-07T22:31:10.1433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestSoundType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'HUMAN', N'เสียงคน', 0, CAST(N'2019-12-01T16:14:58.3100000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestSoundType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'NATURE', N'เสียงเพลง/เสียง', 0, CAST(N'2019-12-07T22:31:12.8866667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestSoundType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'OTHERS', N'เสียงอื่นๆ', 0, CAST(N'2019-12-07T22:31:15.3466667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_RequestSoundType] OFF
ALTER TABLE [dbo].[RM_RequestSoundType] ADD  CONSTRAINT [DF_RM_RequestSoundType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RequestSoundType] ADD  CONSTRAINT [DF_RM_RequestSoundType_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_RequestSoundType] ADD  CONSTRAINT [DF_RequestSoundType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_RequestSoundType] ADD  CONSTRAINT [DF_RequestSoundType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_RequestSoundType] ADD  CONSTRAINT [DF_RequestSoundType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
