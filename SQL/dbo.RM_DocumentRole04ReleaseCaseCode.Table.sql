USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole04ReleaseCaseCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole04ReleaseCaseCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[description] [nvarchar](1000) NULL,
	[value_01] [nvarchar](1000) NULL,
	[value_02] [nvarchar](1000) NULL,
	[value_03] [nvarchar](1000) NULL,
	[value_04] [nvarchar](1000) NULL,
	[value_05] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole04ReleaseCaseCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ON 

INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE_01', N'001', N'รอผลคดี ศาล… คดีหมายเลขคำที่... วันที่', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5733333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'TYPE_02', N'102', N'ไม่รับจดทะเบียนตามคำวินิจฉัยคณะกรรมการฯ ที่', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5733333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'TYPE_03', N'109', N'ละทิ้งคำขอตาม ม.26', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5733333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'TYPE_04', N'112', N'ละทิ้งตามมาตรา 19 (7)', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5733333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'TYPE_05', N'113', N'ละทิ้งตามมาตรา 19 (8)', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5733333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'TYPE_06', N'116', N'ละทิ้งตามมาตรา 19', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5733333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'TYPE_07', N'118', N'ไม่รับจดทะเบียนตามหนังสือขอถอนลงวันที่', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5733333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 1, N'TYPE_08', N'119', N'ละทิ้งตามมาตรา 12', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5733333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 1, N'TYPE_09', N'123', N'ไม่รับจดทะเบียนตามคำพิพากษาศาล', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5900000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1, N'TYPE_10', N'124', N'ไม่รับจดทะเบียนตามคำวินิจฉัยนายทะเบียนฯ ที่', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5900000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 1, N'TYPE_11', N'901', N'ฟื้นคำขอ ตามคำพิพากษา ศาล... คดีหมายเลขคำที่... วันที่', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:08:34.5900000' AS DateTime2), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04ReleaseCaseCode] OFF
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseCaseCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseCaseCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseCaseCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseCaseCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseCaseCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseCaseCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseCaseCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseCaseCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
