USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save150SearchTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save150SearchTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save150SearchTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save150SearchTypeCode] ON 

INSERT [dbo].[RM_Save150SearchTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'CONTRACT_NUMBER', N'ค้นหาด้วยเลขที่สัญญา', 0, CAST(N'2020-04-21T11:48:12.7766667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save150SearchTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'REQUEST_NUMBER', N'ค้นหาด้วยเลขที่คำขอ', 0, CAST(N'2020-04-21T11:48:06.0366667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save150SearchTypeCode] OFF
ALTER TABLE [dbo].[RM_Save150SearchTypeCode] ADD  CONSTRAINT [DF_RM_Save150SearchTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save150SearchTypeCode] ADD  CONSTRAINT [DF_RM_Save150SearchTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save150SearchTypeCode] ADD  CONSTRAINT [DF_RM_Save150SearchTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save150SearchTypeCode] ADD  CONSTRAINT [DF_RM_Save150SearchTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
