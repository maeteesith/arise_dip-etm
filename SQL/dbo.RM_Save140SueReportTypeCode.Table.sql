USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save140SueReportTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save140SueReportTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save140SueReportTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save140SueReportTypeCode] ON 

INSERT [dbo].[RM_Save140SueReportTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE1', N'ยื่นเพื่อรอผลคดี', 0, CAST(N'2019-12-19T02:03:49.6366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save140SueReportTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'TYPE2', N'หยุดเพื่อรอคำสั่งศาล', 0, CAST(N'2019-12-19T02:03:57.0366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save140SueReportTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'TYPE3', N'ไม่อนุญาตให้รอ', 0, CAST(N'2019-12-19T02:03:58.5733333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save140SueReportTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'TYPE4', N'แก้ไขข้อบกพร่อง', 0, CAST(N'2019-12-19T02:03:59.7166667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save140SueReportTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'TYPE5', N'ยกเลิกคำสั่งหยุดรอ', 0, CAST(N'2019-12-19T02:04:00.7633333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save140SueReportTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'TYPE6', N'เพิกถอนคำขอ', 0, CAST(N'2019-12-19T02:04:04.3000000' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_Save140SueReportTypeCode] OFF
ALTER TABLE [dbo].[RM_Save140SueReportTypeCode] ADD  CONSTRAINT [DF_RM_Save140SueReportTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save140SueReportTypeCode] ADD  CONSTRAINT [DF_RM_Save140SueReportTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save140SueReportTypeCode] ADD  CONSTRAINT [DF_RM_Save140SueReportTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save140SueReportTypeCode] ADD  CONSTRAINT [DF_RM_Save140SueReportTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
