USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vRequest01ReceiptChange]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vRequest01ReceiptChange] as 
select 
rc.id,
rc.request_id,

rm_rcc.id rm_rcc_id,
rc.request01_receipt_change_code,
rm_rcc.name request01_receipt_change_name,

rc.request_approve_by,
rc.request_approve_date,
um_request_approve.id request_approve_by_name, 

rc.receipt_approve_by,
rc.receipt_approve_date,
um_receipt_approve.name receipt_approve_by_name,

rc.is_deleted,
rc.created_by,
rc.created_date,
um_created_by.name created_by_name,

rc.updated_by,
rc.updated_date,
um_updated_by.name updated_by_name

from Request01ReceiptChange rc
join RM_Request01ReceiptChangeCode rm_rcc on rm_rcc.code = rc.request01_receipt_change_code
left join UM_User um_request_approve on um_request_approve.id = rc.request_approve_by
left join UM_User um_receipt_approve on um_receipt_approve.id = rc.receipt_approve_by
left join UM_User um_created_by on um_created_by.id = rc.created_by
left join UM_User um_updated_by on um_updated_by.id = rc.updated_by
GO
