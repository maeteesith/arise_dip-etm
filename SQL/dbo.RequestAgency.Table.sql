USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RequestAgency]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestAgency](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[agency_group_id] [bigint] NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_wave_fee] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestAgency] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RequestAgency] ON 

INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (22, 7, N'อนุชา 01', 1, 0, CAST(N'2019-12-16T13:56:55.3066667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (23, 7, N'อนุชา 02', 0, 0, CAST(N'2019-12-16T13:56:55.9900000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (24, 7, N'อนุชา 03', 1, 0, CAST(N'2019-12-16T13:56:56.4400000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (25, 7, N'อนุชา 04', 0, 0, CAST(N'2019-12-16T13:56:56.8600000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (26, 7, N'อนุชา 05', 1, 0, CAST(N'2019-12-16T13:56:57.2266667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (27, 7, N'อนุชา 06', 0, 0, CAST(N'2019-12-16T13:56:57.6433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (28, 7, N'อนุชา 07', 1, 0, CAST(N'2019-12-16T13:56:57.9366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (29, 7, N'อนุชา 08', 0, 0, CAST(N'2019-12-16T13:56:58.2700000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (30, 7, N'อนุชา 09', 1, 0, CAST(N'2019-12-16T13:56:58.7200000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (31, 7, N'อนุชาติ 10', 0, 0, CAST(N'2019-12-16T13:57:00.6266667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RequestAgency] ([id], [agency_group_id], [name], [is_wave_fee], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (33, 7, N'อนุชา จิตร์สุภาพ', 0, 0, CAST(N'2020-05-16T18:16:39.7200000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RequestAgency] OFF
ALTER TABLE [dbo].[RequestAgency] ADD  CONSTRAINT [DF_RequestAgency_is_wave_fee]  DEFAULT ((0)) FOR [is_wave_fee]
GO
ALTER TABLE [dbo].[RequestAgency] ADD  CONSTRAINT [DF_RequestAgency_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RequestAgency] ADD  CONSTRAINT [DF_RequestAgency_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RequestAgency] ADD  CONSTRAINT [DF_RequestAgency_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RequestAgency]  WITH CHECK ADD  CONSTRAINT [FK_RequestAgency_RequestAgencyGroup] FOREIGN KEY([agency_group_id])
REFERENCES [dbo].[RequestAgencyGroup] ([id])
GO
ALTER TABLE [dbo].[RequestAgency] CHECK CONSTRAINT [FK_RequestAgency_RequestAgencyGroup]
GO
