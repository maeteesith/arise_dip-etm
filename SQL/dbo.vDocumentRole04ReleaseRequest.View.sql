USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentRole04ReleaseRequest]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vDocumentRole04ReleaseRequest] as
select
si.id,
ss.id save010_checking_tag_similar_id,
pr.id post_round_id,

pr.book_number,
pr.book_start_date,
pr.book_acknowledge_date,
pr.book_end_date,

si.document_role04_receive_remark,

si.post_round_instruction_rule_code,
rm_prir.name post_round_instruction_rule_name,
rm_prir.description post_round_instruction_rule_description,
si.post_round_type_code,
rm_prt.name post_round_type_name,

rm_d4rrt.code document_role04_release_request_type_code,
rm_d4rrt.name document_role04_release_request_type_name,

si.document_role04_release_request_send_type_code,
rm_d4rst.name document_role04_release_request_send_type_name,

si.document_role04_receive_date,
si.document_role04_receive_send_date,
si.document_role05_receive_date,
si.document_role05_receive_send_date,

si.document_role04_receiver_by,
um_r4.name document_role04_receiver_by_name,
si.document_role05_receiver_by,
um_r5.name document_role05_receiver_by_name,

si.document_role04_status_code,
rm_dr4s.name document_role04_status_name,
rm_dr4r.[index] document_role04_receive_status_index,
si.document_role04_receive_status_code,
rm_dr4r.name document_role04_receive_status_name,
si.document_role05_status_code,
rm_dr5s.name document_role05_status_name,
rm_dr5r.[index] document_role05_receive_status_index,
si.document_role05_receive_status_code,
rm_dr5r.name document_role05_receive_status_name,

ss.save_id,
[save].request_number save_request_number,
(select top 1 sa.name from Save010AddressPeople sa where sa.save_id = [save].id and sa.is_deleted = 0) name,
(select count(1) from Save010Case28 sc where sc.save_id = [save].id and sc.is_deleted = 0) save010_case28_count,
[save].considering_receiver_by,
um_cr.name considering_receiver_by_name,
[save].trademark_status_code,

ss.save_tag_id,
save_tag.request_number save_tag_request_number,


sir.considering_instruction_rule_status_code,
rm_cir.name considering_instruction_rule_status_name,

si.is_deleted,
si.created_by,
si.created_date,
si.updated_by,
si.updated_date

from Save010CheckingTagSimilarInstructionRule si
join Save010CheckingTagSimilar ss on ss.id = si.save010_checking_tag_similar_id
left join PostRoundInstructionRule pir on pir.id = si.post_round_instruction_rule_id
left join Save010InstructionRule sir on sir.id = pir.save010_instruction_rule_id
left join PostRound pr on pr.id = pir.post_round_id
join Save010 [save] on [save].id = ss.save_id
join Save010 save_tag on save_tag.id = ss.save_tag_id

left join RM_PostRoundInstructionRuleCode rm_prir on rm_prir.code = si.post_round_instruction_rule_code
left join RM_PostRoundTypeCode rm_prt on rm_prt.code = si.post_round_type_code

left join RM_DocumentRole04StatusCode rm_dr4s on rm_dr4s.code = si.document_role04_status_code
left join RM_DocumentRole04ReceiveStatusCode rm_dr4r on rm_dr4r.code = si.document_role04_receive_status_code
left join RM_DocumentRole05StatusCode rm_dr5s on rm_dr5s.code = si.document_role05_status_code
left join RM_DocumentRole05ReceiveStatusCode rm_dr5r on rm_dr5r.code = si.document_role05_receive_status_code
left join RM_ConsideringSimilarInstructionRuleStatusCode rm_cir on rm_cir.code = sir.considering_instruction_rule_status_code

left join RM_DocumentRole04ReleaseRequestTypeCode rm_d4rrt on rm_d4rrt.code = 
(
case 
when left(si.post_round_instruction_rule_code, 8) = 'ROLE_20_' then 'PUBLIC'
when left(si.post_round_instruction_rule_code, 8) = 'ROLE_13_' then 'REGISTER'
else 'CHANGE_STATUS' end)

left join RM_DocumentRole04ReleaseRequestSendTypeCode rm_d4rst on rm_d4rst.code = si.document_role04_release_request_send_type_code

left join UM_User um_r4 on um_r4.id = si.document_role04_receiver_by
left join UM_User um_r5 on um_r5.id = si.document_role05_receiver_by
left join UM_User um_cr on um_cr.id = [save].considering_receiver_by
GO
