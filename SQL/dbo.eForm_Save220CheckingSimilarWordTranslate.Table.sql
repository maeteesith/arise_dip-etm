USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save220CheckingSimilarWordTranslate]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[save_index] [int] NULL,
	[word_translate_search] [nvarchar](500) NULL,
	[word_translate_dictionary] [nvarchar](500) NULL,
	[word_translate_sound] [nvarchar](500) NULL,
	[word_translate_translate] [nvarchar](500) NULL,
	[checking_word_translate_status_code-] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[translation_language_code] [nvarchar](50) NULL,
	[word_translate] [nvarchar](500) NULL,
 CONSTRAINT [PK_eForm_Save220CheckingSimilarWordTranslate] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate] ADD  CONSTRAINT [DF_eForm_Save220CheckingSimilarWordTranslate_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate] ADD  CONSTRAINT [DF_eForm_Save220CheckingSimilarWordTranslate_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate] ADD  CONSTRAINT [DF_eForm_Save220CheckingSimilarWordTranslate_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220CheckingSimilarWordTranslate_eForm_Save220] FOREIGN KEY([save_id])
REFERENCES [dbo].[eForm_Save220] ([id])
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate] CHECK CONSTRAINT [FK_eForm_Save220CheckingSimilarWordTranslate_eForm_Save220]
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220CheckingSimilarWordTranslate_RM_CheckingWordTranslateStatusCode] FOREIGN KEY([checking_word_translate_status_code-])
REFERENCES [dbo].[RM_CheckingWordTranslateStatusCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate] CHECK CONSTRAINT [FK_eForm_Save220CheckingSimilarWordTranslate_RM_CheckingWordTranslateStatusCode]
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220CheckingSimilarWordTranslate_RM_TranslationLanguage] FOREIGN KEY([translation_language_code])
REFERENCES [dbo].[RM_TranslationLanguage] ([code])
GO
ALTER TABLE [dbo].[eForm_Save220CheckingSimilarWordTranslate] CHECK CONSTRAINT [FK_eForm_Save220CheckingSimilarWordTranslate_RM_TranslationLanguage]
GO
