USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestPrintCoverTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestPrintCoverTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_RequestPrintCoverTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestPrintCoverTypeCode] ON 

INSERT [dbo].[RM_RequestPrintCoverTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE01', N'สติ๊กเกอร์หน้าแฟ้มคำขอจดทะเบียน', 0, CAST(N'2020-03-12T15:58:11.7466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestPrintCoverTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'TYPE02', N'สติ๊กเกอร์คำขอและบาร์โค้ดหน้าแฟ้มคำขอจดทะเบียน', 0, CAST(N'2020-03-12T15:58:19.1066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestPrintCoverTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'TYPE03', N'สติ๊กเกอร์เลขที่คำขอหน้าแฟ้ม', 0, CAST(N'2020-03-12T15:58:21.0700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestPrintCoverTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'TYPE04', N'สติ๊กเกอร์เลขที่คำขอ ก.01', 0, CAST(N'2020-03-12T15:58:24.4266667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_RequestPrintCoverTypeCode] OFF
ALTER TABLE [dbo].[RM_RequestPrintCoverTypeCode] ADD  CONSTRAINT [DF_RM_RequestPrintCoverTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RequestPrintCoverTypeCode] ADD  CONSTRAINT [DF_RM_RequestPrintCoverTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_RequestPrintCoverTypeCode] ADD  CONSTRAINT [DF_RM_RequestPrintCoverTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_RequestPrintCoverTypeCode] ADD  CONSTRAINT [DF_RM_RequestPrintCoverTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_RequestPrintCoverTypeCode] ADD  CONSTRAINT [DF_RM_RequestPrintCoverTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
