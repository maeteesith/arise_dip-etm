USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save050]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save050](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save050_allow_type_code] [nvarchar](50) NULL,
	[save050_people_type_code] [nvarchar](50) NULL,
	[save050_receiver_people_type_code] [nvarchar](50) NULL,
	[save050_contact_type_code] [nvarchar](50) NULL,
	[save050_transfer_form_code] [nvarchar](50) NULL,
	[contract_date] [date] NULL,
	[contract_start_date] [date] NULL,
	[is_time_limit] [bit] NULL,
	[contract_end_date] [date] NULL,
	[contract_duration] [nvarchar](100) NULL,
	[remark_8_2] [nvarchar](max) NULL,
	[remark_8_3] [nvarchar](max) NULL,
	[is_people_rights] [bit] NULL,
	[is_people_authorize] [bit] NULL,
	[is_receiver_transfer] [bit] NULL,
	[is_receiver_authorize] [bit] NULL,
	[save050_extend_type_code] [nvarchar](50) NULL,
	[is_10_1] [bit] NULL,
	[is_10_2] [bit] NULL,
	[is_10_3] [bit] NULL,
	[sign_inform_person_list] [nvarchar](300) NULL,
	[sign_inform_representative_list] [nvarchar](300) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[save050_search_type_code] [nvarchar](50) NULL,
	[contract_ref_number] [nvarchar](50) NULL,
	[save050_submit_type_code] [nvarchar](50) NULL,
	[rule_number] [nvarchar](50) NULL,
	[save050_representative_condition_type_code] [nvarchar](50) NULL,
	[save050_receiver_representative_condition_type_code] [nvarchar](50) NULL,
	[sign_inform_receiver_person_list] [nvarchar](50) NULL,
	[sign_inform_receiver_representative_list] [nvarchar](50) NULL,
	[wizard] [nvarchar](300) NULL,
 CONSTRAINT [PK_eForm_Save050] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[eForm_Save050] ON 

INSERT [dbo].[eForm_Save050] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save050_allow_type_code], [save050_people_type_code], [save050_receiver_people_type_code], [save050_contact_type_code], [save050_transfer_form_code], [contract_date], [contract_start_date], [is_time_limit], [contract_end_date], [contract_duration], [remark_8_2], [remark_8_3], [is_people_rights], [is_people_authorize], [is_receiver_transfer], [is_receiver_authorize], [save050_extend_type_code], [is_10_1], [is_10_2], [is_10_3], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save050_search_type_code], [contract_ref_number], [save050_submit_type_code], [rule_number], [save050_representative_condition_type_code], [save050_receiver_representative_condition_type_code], [sign_inform_receiver_person_list], [sign_inform_receiver_representative_list], [wizard]) VALUES (2, NULL, N'2006000003', N'123@hotmail.com', N'71b15291-1565-4195-a4a0-0bbd64a8fd44', N'0240184018', NULL, NULL, N'', CAST(1000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T04:15:11.3318824' AS DateTime2), 1, CAST(N'2020-06-21T04:15:11.3318900' AS DateTime2), 1, NULL, NULL, NULL, NULL, N'AND_OR', NULL, NULL, NULL, NULL)
INSERT [dbo].[eForm_Save050] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save050_allow_type_code], [save050_people_type_code], [save050_receiver_people_type_code], [save050_contact_type_code], [save050_transfer_form_code], [contract_date], [contract_start_date], [is_time_limit], [contract_end_date], [contract_duration], [remark_8_2], [remark_8_3], [is_people_rights], [is_people_authorize], [is_receiver_transfer], [is_receiver_authorize], [save050_extend_type_code], [is_10_1], [is_10_2], [is_10_3], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save050_search_type_code], [contract_ref_number], [save050_submit_type_code], [rule_number], [save050_representative_condition_type_code], [save050_receiver_representative_condition_type_code], [sign_inform_receiver_person_list], [sign_inform_receiver_representative_list], [wizard]) VALUES (3, NULL, N'2006000005', N'123@hotmail.com', N'eb3f3fe1-d338-478f-9db4-b7d53417401e', N'0120581210', NULL, NULL, N'', CAST(1000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T04:49:31.5324667' AS DateTime2), 1, CAST(N'2020-06-21T04:49:31.5324733' AS DateTime2), 1, NULL, NULL, NULL, NULL, N'AND_OR', NULL, NULL, NULL, NULL)
INSERT [dbo].[eForm_Save050] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save050_allow_type_code], [save050_people_type_code], [save050_receiver_people_type_code], [save050_contact_type_code], [save050_transfer_form_code], [contract_date], [contract_start_date], [is_time_limit], [contract_end_date], [contract_duration], [remark_8_2], [remark_8_3], [is_people_rights], [is_people_authorize], [is_receiver_transfer], [is_receiver_authorize], [save050_extend_type_code], [is_10_1], [is_10_2], [is_10_3], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save050_search_type_code], [contract_ref_number], [save050_submit_type_code], [rule_number], [save050_representative_condition_type_code], [save050_receiver_representative_condition_type_code], [sign_inform_receiver_person_list], [sign_inform_receiver_representative_list], [wizard]) VALUES (4, NULL, N'2006000011', N'123@hotmail.com', N'e77dc426-54da-4675-8cd7-1f09f9cf2505', N'0240184018', NULL, NULL, N'', CAST(1000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T10:55:42.2178638' AS DateTime2), 1, CAST(N'2020-06-21T10:55:42.2178710' AS DateTime2), 1, NULL, NULL, NULL, NULL, N'AND_OR', NULL, NULL, NULL, NULL)
INSERT [dbo].[eForm_Save050] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save050_allow_type_code], [save050_people_type_code], [save050_receiver_people_type_code], [save050_contact_type_code], [save050_transfer_form_code], [contract_date], [contract_start_date], [is_time_limit], [contract_end_date], [contract_duration], [remark_8_2], [remark_8_3], [is_people_rights], [is_people_authorize], [is_receiver_transfer], [is_receiver_authorize], [save050_extend_type_code], [is_10_1], [is_10_2], [is_10_3], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save050_search_type_code], [contract_ref_number], [save050_submit_type_code], [rule_number], [save050_representative_condition_type_code], [save050_receiver_representative_condition_type_code], [sign_inform_receiver_person_list], [sign_inform_receiver_representative_list], [wizard]) VALUES (5, NULL, N'2006000012', N'123@hotmail.com', N'a4a61744-138c-429c-9e84-8aef9e095e84', N'0240184018', NULL, NULL, N'', CAST(1000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T10:57:09.0246102' AS DateTime2), 1, CAST(N'2020-06-21T10:57:09.0246159' AS DateTime2), 1, NULL, NULL, NULL, NULL, N'AND_OR', NULL, NULL, NULL, NULL)
INSERT [dbo].[eForm_Save050] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save050_allow_type_code], [save050_people_type_code], [save050_receiver_people_type_code], [save050_contact_type_code], [save050_transfer_form_code], [contract_date], [contract_start_date], [is_time_limit], [contract_end_date], [contract_duration], [remark_8_2], [remark_8_3], [is_people_rights], [is_people_authorize], [is_receiver_transfer], [is_receiver_authorize], [save050_extend_type_code], [is_10_1], [is_10_2], [is_10_3], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save050_search_type_code], [contract_ref_number], [save050_submit_type_code], [rule_number], [save050_representative_condition_type_code], [save050_receiver_representative_condition_type_code], [sign_inform_receiver_person_list], [sign_inform_receiver_representative_list], [wizard]) VALUES (6, NULL, N'2006000013', N'123@hotmail.com', N'1facc1ff-a554-4165-96b1-fd9a462c8ab4', N'0855555555', NULL, NULL, N'', CAST(1000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T11:05:56.8593954' AS DateTime2), 1, CAST(N'2020-06-21T11:05:56.8594107' AS DateTime2), 1, NULL, NULL, NULL, NULL, N'AND_OR', NULL, NULL, NULL, NULL)
INSERT [dbo].[eForm_Save050] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save050_allow_type_code], [save050_people_type_code], [save050_receiver_people_type_code], [save050_contact_type_code], [save050_transfer_form_code], [contract_date], [contract_start_date], [is_time_limit], [contract_end_date], [contract_duration], [remark_8_2], [remark_8_3], [is_people_rights], [is_people_authorize], [is_receiver_transfer], [is_receiver_authorize], [save050_extend_type_code], [is_10_1], [is_10_2], [is_10_3], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save050_search_type_code], [contract_ref_number], [save050_submit_type_code], [rule_number], [save050_representative_condition_type_code], [save050_receiver_representative_condition_type_code], [sign_inform_receiver_person_list], [sign_inform_receiver_representative_list], [wizard]) VALUES (7, NULL, N'2006000014', N'123@hotmail.com', N'7f06a3ed-6047-4a64-acc6-a790320a9e0f', N'0877777777', N'request001', NULL, N'', CAST(1000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T11:09:50.4035644' AS DateTime2), 1, CAST(N'2020-06-21T11:09:50.4035707' AS DateTime2), 1, NULL, NULL, NULL, NULL, N'AND_OR', NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[eForm_Save050] OFF
ALTER TABLE [dbo].[eForm_Save050] ADD  CONSTRAINT [DF_eForm_Save050_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save050] ADD  CONSTRAINT [DF_eForm_Save050_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save050] ADD  CONSTRAINT [DF_eForm_Save050_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save050]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050_Receiver_Representative_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save050_receiver_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save050] CHECK CONSTRAINT [FK_eForm_Save050_Receiver_Representative_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save050]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050_Representative_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save050_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save050] CHECK CONSTRAINT [FK_eForm_Save050_Representative_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save050]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050_RM_Save050AddressType] FOREIGN KEY([save050_contact_type_code])
REFERENCES [dbo].[RM_Save050AddressType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save050] CHECK CONSTRAINT [FK_eForm_Save050_RM_Save050AddressType]
GO
ALTER TABLE [dbo].[eForm_Save050]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050_RM_Save050AllowTypeCode1] FOREIGN KEY([save050_allow_type_code])
REFERENCES [dbo].[RM_Save050AllowTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save050] CHECK CONSTRAINT [FK_eForm_Save050_RM_Save050AllowTypeCode1]
GO
ALTER TABLE [dbo].[eForm_Save050]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050_RM_Save050ExtendTypeCode] FOREIGN KEY([save050_extend_type_code])
REFERENCES [dbo].[RM_Save050ExtendTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save050] CHECK CONSTRAINT [FK_eForm_Save050_RM_Save050ExtendTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save050]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050_RM_Save050PeopleTypeCode] FOREIGN KEY([save050_people_type_code])
REFERENCES [dbo].[RM_Save050PeopleTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save050] CHECK CONSTRAINT [FK_eForm_Save050_RM_Save050PeopleTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save050]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050_RM_Save050ReceiverPeopleTypeCode] FOREIGN KEY([save050_receiver_people_type_code])
REFERENCES [dbo].[RM_Save050ReceiverPeopleTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save050] CHECK CONSTRAINT [FK_eForm_Save050_RM_Save050ReceiverPeopleTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save050]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050_RM_Save050TransferForm] FOREIGN KEY([save050_transfer_form_code])
REFERENCES [dbo].[RM_Save050TransferForm] ([code])
GO
ALTER TABLE [dbo].[eForm_Save050] CHECK CONSTRAINT [FK_eForm_Save050_RM_Save050TransferForm]
GO
