USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DepartmentGroupCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DepartmentGroupCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DepartmentGroupCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DepartmentGroupCode] ON 

INSERT [dbo].[RM_DepartmentGroupCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'CHECK', N'กลุ่มงานตรวจสอบ', 0, CAST(N'2020-01-27T16:14:15.5033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DepartmentGroupCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'PUBLIC', N'กลุ่มประกาศโฆษณา
', 0, CAST(N'2020-01-19T18:19:58.8600000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DepartmentGroupCode] OFF
ALTER TABLE [dbo].[RM_DepartmentGroupCode] ADD  CONSTRAINT [DF_RM_DepartmentGroupCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DepartmentGroupCode] ADD  CONSTRAINT [DF_RM_DepartmentGroupCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DepartmentGroupCode] ADD  CONSTRAINT [DF_RM_DepartmentGroupCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DepartmentGroupCode] ADD  CONSTRAINT [DF_RM_DepartmentGroupCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_DepartmentGroupCode] ADD  CONSTRAINT [DF_RM_DepartmentGroupCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
