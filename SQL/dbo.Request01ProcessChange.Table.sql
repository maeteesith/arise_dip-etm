USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Request01ProcessChange]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Request01ProcessChange](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_date] [datetime2](7) NULL,
	[requester_name] [nvarchar](1000) NULL,
	[telephone] [nvarchar](50) NULL,
	[is_via_post] [bit] NULL,
	[is_wave_fee] [bit] NULL,
	[is_commercial_affairs_province] [bit] NULL,
	[commercial_affairs_province_code] [nvarchar](50) NULL,
	[item_count] [int] NULL,
	[total_price] [decimal](18, 2) NULL,
	[source_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[source_id] [bigint] NULL,
	[request01_receipt_change_id] [bigint] NULL,
 CONSTRAINT [PK__Request0__3213E83FF49245F0] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Request01ProcessChange]  WITH CHECK ADD  CONSTRAINT [FK_Request01ProcessChange_Request01ReceiptChange] FOREIGN KEY([request01_receipt_change_id])
REFERENCES [dbo].[Request01ReceiptChange] ([id])
GO
ALTER TABLE [dbo].[Request01ProcessChange] CHECK CONSTRAINT [FK_Request01ProcessChange_Request01ReceiptChange]
GO
