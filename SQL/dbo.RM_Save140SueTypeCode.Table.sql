USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save140SueTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save140SueTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save140SueTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save140SueTypeCode] ON 

INSERT [dbo].[RM_Save140SueTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE1', N'ขอแจ้งว่าได้ยื่นฟ้องคดีต่อศาล', 0, CAST(N'2019-12-19T02:04:37.1000000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save140SueTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'TYPE2', N'ขอแจ้งผลคำพิพากษาของศาล', 0, CAST(N'2019-12-19T02:04:38.3366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save140SueTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'TYPE3', N'ขอแจ้งผลคำพิพากษาของศาลฎีกา', 0, CAST(N'2019-12-19T02:04:42.0800000' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_Save140SueTypeCode] OFF
ALTER TABLE [dbo].[RM_Save140SueTypeCode] ADD  CONSTRAINT [DF_RM_Save140SueTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save140SueTypeCode] ADD  CONSTRAINT [DF_RM_Save140SueTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save140SueTypeCode] ADD  CONSTRAINT [DF_RM_Save140SueTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save140SueTypeCode] ADD  CONSTRAINT [DF_RM_Save140SueTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
