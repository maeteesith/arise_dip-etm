USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PublicDocumentPaymentInstructionRuleCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PublicDocumentPaymentInstructionRuleCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[description] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[value_4] [nvarchar](1000) NULL,
	[value_5] [nvarchar](1000) NULL,
	[is_show] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PublicDocumentPaymentInstructionRuleCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] ON 

INSERT [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'RULE_5_3', N'ตค. 5(3)
', NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-19T10:33:43.8066667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] OFF
ALTER TABLE [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PublicDocumentPaymentInstructionRuleCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PublicDocumentPaymentInstructionRuleCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PublicDocumentPaymentInstructionRuleCode_is_show]  DEFAULT ((1)) FOR [is_show]
GO
ALTER TABLE [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PublicDocumentPaymentInstructionRuleCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PublicDocumentPaymentInstructionRuleCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PublicDocumentPaymentInstructionRuleCode] ADD  CONSTRAINT [DF_RM_PublicDocumentPaymentInstructionRuleCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
