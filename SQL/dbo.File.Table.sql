USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[File]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[File](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[file_name] [nvarchar](1000) NULL,
	[physical_path] [nvarchar](250) NULL,
	[display_path] [nvarchar](250) NULL,
	[file_size] [decimal](18, 2) NULL,
	[archived_date] [datetime2](7) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_File] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
