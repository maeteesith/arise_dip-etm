USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[PublicRoundCase41]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PublicRoundCase41](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[maker_by] [bigint] NULL,
	[make_date] [datetime2](7) NULL,
	[department_code] [nvarchar](50) NULL,
	[public_case41_remark] [nvarchar](1000) NULL,
	[public_case41_additional_remark] [nvarchar](1000) NULL,
	[public_role01_case41_status_code] [nvarchar](50) NULL,
	[public_role01_case41_date] [datetime2](7) NULL,
	[public_role05_case41_by] [bigint] NULL,
	[public_role05_case41_status_code] [nvarchar](50) NULL,
	[public_role05_case41_date] [datetime2](7) NULL,
	[public_round_case41_status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_PublicRoundCase41] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PublicRoundCase41] ADD  CONSTRAINT [DF_Table_1_public_role05_by]  DEFAULT ((0)) FOR [public_role05_case41_by]
GO
ALTER TABLE [dbo].[PublicRoundCase41] ADD  CONSTRAINT [DF_PublicRoundCase41_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[PublicRoundCase41] ADD  CONSTRAINT [DF_PublicRoundCase41_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[PublicRoundCase41] ADD  CONSTRAINT [DF_PublicRoundCase41_created_by]  DEFAULT ((0)) FOR [created_by]
GO
