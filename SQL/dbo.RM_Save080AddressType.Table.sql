USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save080AddressType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save080AddressType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save080AddressType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save080AddressType] ON 

INSERT [dbo].[RM_Save080AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 5, N'OTHERS', N'อื่นๆ', 0, CAST(N'2020-02-28T12:52:08.9400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save080AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 6, N'OWNER', N'เจ้าของ', 0, CAST(N'2020-03-12T14:45:40.0900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save080AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'RECEIVER', N'ผู้ได้รับอนุญาต', 0, CAST(N'2020-02-28T12:22:39.3433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save080AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 2, N'RECEIVER_PERIOD', N'ผู้ได้รับอนุญาตช่วง', 0, CAST(N'2020-02-28T12:47:38.2800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save080AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 3, N'REPRESENTATIVE', N'ตัวเเทน', 0, CAST(N'2019-12-15T11:22:08.8500000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save080AddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 4, N'REPRESENTATIVE_PERIOD', N'ตัวเเทนช่วง', 0, CAST(N'2020-02-28T12:22:47.1966667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save080AddressType] OFF
ALTER TABLE [dbo].[RM_Save080AddressType] ADD  CONSTRAINT [DF_RM_Save080AddressType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save080AddressType] ADD  CONSTRAINT [DF_RM_Save080AddressType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save080AddressType] ADD  CONSTRAINT [DF_RM_Save080AddressType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save080AddressType] ADD  CONSTRAINT [DF_RM_Save080AddressType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
