USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Request01Process]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Request01Process](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_date] [datetime2](7) NULL,
	[requester_name] [nvarchar](1000) NULL,
	[telephone] [nvarchar](50) NULL,
	[is_via_post] [bit] NULL,
	[is_wave_fee] [bit] NULL,
	[is_commercial_affairs_province] [bit] NULL,
	[commercial_affairs_province_code] [nvarchar](50) NULL,
	[item_count] [int] NULL,
	[total_price] [decimal](18, 2) NULL,
	[source_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Request] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Request01Process] ADD  CONSTRAINT [DF_Request01Process_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Request01Process] ADD  CONSTRAINT [DF_Request01Process_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Request01Process] ADD  CONSTRAINT [DF_Request01Process_created_by]  DEFAULT ((0)) FOR [created_by]
GO
