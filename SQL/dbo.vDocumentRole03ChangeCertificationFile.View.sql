USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentRole03ChangeCertificationFile]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vDocumentRole03ChangeCertificationFile] as 

select
sc.id,

sc.document_role03_item_id,

sc.file_id,
f.file_name,
f.file_size,

sc.remark,

sc.is_deleted,
sc.created_by,
sc.created_date,
sc.updated_by,
sc.updated_date
from DocumentRole03ChangeCertificationFile sc
left join [File] f on f.id = sc.file_id and f.is_deleted = 0
GO
