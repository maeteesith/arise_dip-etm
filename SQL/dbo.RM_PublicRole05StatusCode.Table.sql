USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PublicRole05StatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PublicRole05StatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PublicRole05StatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PublicRole05StatusCode] ON 

INSERT [dbo].[RM_PublicRole05StatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1, N'DRAFT', N'รอให้เลขทะเบียน', 1, 0, CAST(N'2020-05-14T13:56:53.7866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole05StatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 4, N'DRAFT_CHANGE', N'รอการตรวจสอบจากแก้ไข', 1, 0, CAST(N'2020-05-14T13:56:54.5433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole05StatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, 2, N'DRAFT_FIX', N'รอให้เลขทะเบียนอีกครั้ง', 1, 0, CAST(N'2020-05-14T13:56:55.3033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole05StatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (13, 10, N'SEND', N'เสร็จสิ้น', 1, 0, CAST(N'2020-05-14T13:56:56.0533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole05StatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (14, 11, N'SEND_FIX', N'เสร็จสิ้น', 0, 0, CAST(N'2020-05-14T13:56:56.8100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole05StatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (15, 3, N'WAIT_CHANGE', N'รอแก้ไข', 1, 0, CAST(N'2020-05-14T13:56:57.5600000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PublicRole05StatusCode] OFF
ALTER TABLE [dbo].[RM_PublicRole05StatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05StatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PublicRole05StatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05StatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PublicRole05StatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05StatusCode_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_PublicRole05StatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05StatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PublicRole05StatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05StatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PublicRole05StatusCode] ADD  CONSTRAINT [DF_RM_PublicRole05StatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
