USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save050Product]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save050Product](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[request_item_sub_type_1_code] [nvarchar](50) NULL,
	[request_item_sub_type_2_code] [nvarchar](50) NULL,
	[description] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_eForm_Save050Product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[eForm_Save050Product] ON 

INSERT [dbo].[eForm_Save050Product] ([id], [save_id], [request_item_sub_type_1_code], [request_item_sub_type_2_code], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 7, N'1', NULL, N'123', 0, CAST(N'2020-06-22T18:30:49.2300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[eForm_Save050Product] ([id], [save_id], [request_item_sub_type_1_code], [request_item_sub_type_2_code], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 7, N'2', NULL, N'122', 0, CAST(N'2020-06-22T18:31:09.0633333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[eForm_Save050Product] OFF
ALTER TABLE [dbo].[eForm_Save050Product] ADD  CONSTRAINT [DF_eForm_Save050Product_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save050Product] ADD  CONSTRAINT [DF_eForm_Save050Product_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save050Product] ADD  CONSTRAINT [DF_eForm_Save050Product_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save050Product]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save050Product_eForm_Save050] FOREIGN KEY([save_id])
REFERENCES [dbo].[eForm_Save050] ([id])
GO
ALTER TABLE [dbo].[eForm_Save050Product] CHECK CONSTRAINT [FK_eForm_Save050Product_eForm_Save050]
GO
