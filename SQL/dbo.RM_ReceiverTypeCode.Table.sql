USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ReceiverTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ReceiverTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_ReceiverTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ReceiverTypeCode] ON 

INSERT [dbo].[RM_ReceiverTypeCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'RECEIVER', N'ผู้ได้รับอนุญาต', NULL, NULL, NULL, 1, 0, CAST(N'2020-04-22T10:23:14.4000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ReceiverTypeCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'RECEIVER_PERIOD', N'ผู้ได้รับอนุญาตช่วง', NULL, NULL, NULL, 1, 0, CAST(N'2020-04-22T10:23:36.0700000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_ReceiverTypeCode] OFF
ALTER TABLE [dbo].[RM_ReceiverTypeCode] ADD  CONSTRAINT [DF_RM_ReceiverTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ReceiverTypeCode] ADD  CONSTRAINT [DF_RM_ReceiverTypeCode_is_deleted1]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_ReceiverTypeCode] ADD  CONSTRAINT [DF_ReceiverTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ReceiverTypeCode] ADD  CONSTRAINT [DF_ReceiverTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ReceiverTypeCode] ADD  CONSTRAINT [DF_ReceiverTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
