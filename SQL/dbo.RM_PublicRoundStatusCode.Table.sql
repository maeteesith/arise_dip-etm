USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PublicRoundStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PublicRoundStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PublicRoundStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PublicRoundStatusCode] ON 

INSERT [dbo].[RM_PublicRoundStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'DOING_PUBLIC', N'กำลังประกาศโฆษณา', 0, CAST(N'2020-01-18T22:33:05.4200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRoundStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'DONE', N'เสร็จสิ้น', 0, CAST(N'2020-01-18T22:33:11.1200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRoundStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'WAIT_PUBLIC', N'รอประกาศโฆษณา', 0, CAST(N'2020-01-18T22:33:15.2166667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PublicRoundStatusCode] OFF
ALTER TABLE [dbo].[RM_PublicRoundStatusCode] ADD  CONSTRAINT [DF_RM_PublicRoundStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PublicRoundStatusCode] ADD  CONSTRAINT [DF_RM_PublicRoundStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PublicRoundStatusCode] ADD  CONSTRAINT [DF_RM_PublicRoundStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PublicRoundStatusCode] ADD  CONSTRAINT [DF_RM_PublicRoundStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PublicRoundStatusCode] ADD  CONSTRAINT [DF_RM_PublicRoundStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
