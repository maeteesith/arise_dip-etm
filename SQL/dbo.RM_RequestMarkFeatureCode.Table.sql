USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestMarkFeatureCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestMarkFeatureCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_RequestMarkFeatureCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestMarkFeatureCode] ON 

INSERT [dbo].[RM_RequestMarkFeatureCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'2D', N'เครื่องหมายรูป 2D', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[RM_RequestMarkFeatureCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'2D_WORD', N'เครื่องหมายผสมรูปคำ', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[RM_RequestMarkFeatureCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'3D', N'เครื่องหมายรูป 3D', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[RM_RequestMarkFeatureCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'SOUND', N'เครื่องหมายเสียง', 0, NULL, NULL, NULL, NULL)
INSERT [dbo].[RM_RequestMarkFeatureCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'WORD', N'เครื่องหมายคำ', 0, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_RequestMarkFeatureCode] OFF
ALTER TABLE [dbo].[RM_RequestMarkFeatureCode] ADD  CONSTRAINT [DF_RM_RequestMarkFeatureCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RequestMarkFeatureCode] ADD  CONSTRAINT [DF_RM_RequestMarkFeatureCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
