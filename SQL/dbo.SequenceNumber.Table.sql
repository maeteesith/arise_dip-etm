USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[SequenceNumber]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SequenceNumber](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[topic] [nvarchar](50) NULL,
	[code] [nvarchar](50) NULL,
	[number] [int] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_SequenceNumber] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[SequenceNumber] ADD  CONSTRAINT [DF_SequenceNumber_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[SequenceNumber] ADD  CONSTRAINT [DF_SequenceNumber_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[SequenceNumber] ADD  CONSTRAINT [DF_SequenceNumber_created_by]  DEFAULT ((0)) FOR [created_by]
GO
