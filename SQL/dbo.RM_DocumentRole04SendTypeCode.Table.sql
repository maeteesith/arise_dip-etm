USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole04SendTypeCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole04SendTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole04SendTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04SendTypeCode] ON 

INSERT [dbo].[RM_DocumentRole04SendTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 3, N'CHECKING', N'เสนอนายทะเบียนตรวจสอบ', 0, CAST(N'2020-05-28T18:59:03.6833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04SendTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 4, N'PUBLIC', N'เสนอประกาศโฆษณา', 0, CAST(N'2020-05-28T18:59:29.5666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04SendTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'ROUND_1', N'เสนอออกหนังสือรอบ 1 ใหม่', 0, CAST(N'2020-05-28T18:58:10.3466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04SendTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 2, N'ROUND_2', N'เสนอออกหนังสือรอบ 2', 0, CAST(N'2020-05-28T18:58:15.1600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04SendTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 5, N'SOME', N'เสนอหลุดพ้นบ้างคำสั่ง', 0, CAST(N'2020-05-28T18:59:12.0000000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04SendTypeCode] OFF
ALTER TABLE [dbo].[RM_DocumentRole04SendTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04SendTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole04SendTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04SendTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole04SendTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04SendTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole04SendTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04SendTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_DocumentRole04SendTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04SendTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
