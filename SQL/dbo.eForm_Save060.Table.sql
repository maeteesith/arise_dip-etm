USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save060]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save060](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[wizard] [nvarchar](300) NULL,
	[challenger_number] [nvarchar](50) NULL,
	[rule_number] [nvarchar](50) NULL,
	[save060_period_request_code] [nvarchar](50) NULL,
	[save060_search_type_code] [nvarchar](50) NULL,
	[is_menu_1] [bit] NULL,
	[is_menu_2] [bit] NULL,
	[is_menu_3] [bit] NULL,
	[is_menu_4] [bit] NULL,
	[is_menu_4_1] [bit] NULL,
	[is_menu_4_2] [bit] NULL,
	[is_menu_5] [bit] NULL,
	[is_menu_6] [bit] NULL,
	[is_menu_7] [bit] NULL,
	[is_menu_8] [bit] NULL,
	[is_menu_9] [bit] NULL,
	[is_menu_10] [bit] NULL,
	[is_menu_11] [bit] NULL,
	[is_menu_12] [bit] NULL,
	[is_menu_12_1] [bit] NULL,
	[is_menu_12_2] [bit] NULL,
	[is_menu_12_3] [bit] NULL,
	[is_menu_12_4] [bit] NULL,
	[is_menu_12_5] [bit] NULL,
	[is_menu_12_6] [bit] NULL,
	[is_menu_12_7] [bit] NULL,
	[is_menu_12_8] [bit] NULL,
	[is_menu_12_9] [bit] NULL,
	[is_menu_12_10] [bit] NULL,
	[is_menu_12_11] [bit] NULL,
	[is_menu_12_12] [bit] NULL,
	[save060_change_type_code] [nvarchar](50) NULL,
	[save060_representative_change_code] [nvarchar](50) NULL,
	[save060_representative_condition_type_code] [nvarchar](50) NULL,
	[save060_receiver_representative_change_code] [nvarchar](50) NULL,
	[save060_receiver_representative_condition_type_code] [nvarchar](50) NULL,
	[save060_contract_receiver_type_code] [nvarchar](50) NULL,
	[save060_contract_receiver_representative_change_code] [nvarchar](50) NULL,
	[save060_contract_receiver_representative_condition_type_code] [nvarchar](50) NULL,
	[save060_detail_receiver_type_code] [nvarchar](50) NULL,
	[contract_start_date] [date] NULL,
	[is_contract] [bit] NULL,
	[contract_end_date] [date] NULL,
	[contract_date] [date] NULL,
	[remark_8_2] [nvarchar](max) NULL,
	[is_people_rights] [bit] NULL,
	[is_people_authorize] [bit] NULL,
	[is_receiver_transfer] [bit] NULL,
	[is_receiver_authorize] [bit] NULL,
	[save060_legacy_representative_change_code] [nvarchar](50) NULL,
	[save060_legacy_representative_condition_type_code] [nvarchar](50) NULL,
	[save060_img_type_type_code] [nvarchar](50) NULL,
	[img_file_2d_id] [bigint] NULL,
	[img_file_3d_id_1] [bigint] NULL,
	[img_file_3d_id_2] [bigint] NULL,
	[img_file_3d_id_3] [bigint] NULL,
	[img_file_3d_id_4] [bigint] NULL,
	[img_file_3d_id_5] [bigint] NULL,
	[img_file_3d_id_6] [bigint] NULL,
	[img_w] [int] NULL,
	[img_h] [int] NULL,
	[remark_5_1_3] [nvarchar](max) NULL,
	[is_sound_mark_human] [bit] NULL,
	[is_sound_mark_animal] [bit] NULL,
	[is_sound_mark_sound] [bit] NULL,
	[is_sound_mark_other] [bit] NULL,
	[remark_5_2_2] [nvarchar](max) NULL,
	[save060_contact_type_code] [nvarchar](50) NULL,
	[rule_file_id] [bigint] NULL,
	[people_kor4_number] [nvarchar](50) NULL,
	[save060_transfer_representative_change_code] [nvarchar](50) NULL,
	[save060_transfer_representative_condition_type_code] [nvarchar](50) NULL,
	[receiver_kor4_number] [nvarchar](50) NULL,
	[save060_transfer_receiver_representative_change_code] [nvarchar](50) NULL,
	[save060_transfer_receiver_representative_condition_type_code] [nvarchar](50) NULL,
	[save060_challenger_representative_change_code] [nvarchar](50) NULL,
	[save060_challenger_representative_period_change_type_code] [nvarchar](50) NULL,
	[save060_challenger_representative_condition_type_code] [nvarchar](50) NULL,
	[save060_challenger_representative_period_condition_type_code] [nvarchar](50) NULL,
	[save060_challenger_contact_type_code] [nvarchar](50) NULL,
	[is_1] [bit] NULL,
	[is_2] [bit] NULL,
	[is_3] [bit] NULL,
	[is_4] [bit] NULL,
	[is_5] [bit] NULL,
	[is_6] [bit] NULL,
	[sign_inform_person_list] [nvarchar](50) NULL,
	[sign_inform_representative_list] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[sound_file_id] [bigint] NULL,
	[sound_jpg_file_id] [bigint] NULL,
	[contract_detail_ref_id] [bigint] NULL,
 CONSTRAINT [PK_eForm_Save060] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save060] ADD  CONSTRAINT [DF_eForm_Save060_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save060] ADD  CONSTRAINT [DF_eForm_Save060_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save060] ADD  CONSTRAINT [DF_eForm_Save060_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_1] FOREIGN KEY([sound_file_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_1]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_2] FOREIGN KEY([sound_jpg_file_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_2]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_3] FOREIGN KEY([img_file_2d_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_3]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_4] FOREIGN KEY([img_file_3d_id_1])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_4]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_5] FOREIGN KEY([img_file_3d_id_2])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_5]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_6] FOREIGN KEY([img_file_3d_id_3])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_6]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_7] FOREIGN KEY([img_file_3d_id_4])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_7]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_8] FOREIGN KEY([img_file_3d_id_5])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_8]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_FileGuest_9] FOREIGN KEY([img_file_3d_id_6])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_FileGuest_9]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_1] FOREIGN KEY([save060_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_1]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_2] FOREIGN KEY([save060_challenger_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_2]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_3] FOREIGN KEY([save060_contract_receiver_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_3]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_4] FOREIGN KEY([save060_legacy_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_4]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_5] FOREIGN KEY([save060_receiver_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_5]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_6] FOREIGN KEY([save060_transfer_receiver_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_6]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_7] FOREIGN KEY([save060_transfer_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_AddressRepresentativeConditionTypeCode_7]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_Save060AddressTypeCode] FOREIGN KEY([save060_contact_type_code])
REFERENCES [dbo].[RM_Save060AddressTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_Save060AddressTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_Save060ChallengerAddressTypeCode] FOREIGN KEY([save060_challenger_contact_type_code])
REFERENCES [dbo].[RM_Save060ChallengerAddressTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_Save060ChallengerAddressTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_Save060ReceiverTypeCode] FOREIGN KEY([save060_contract_receiver_type_code])
REFERENCES [dbo].[RM_Save060ReceiverTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_Save060ReceiverTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save060]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save060_RM_Save060ReceiverTypeCode1] FOREIGN KEY([save060_detail_receiver_type_code])
REFERENCES [dbo].[RM_Save060ReceiverTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save060] CHECK CONSTRAINT [FK_eForm_Save060_RM_Save060ReceiverTypeCode1]
GO
