USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RS_Address]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RS_Address](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[object_type_code] [nvarchar](50) NULL,
	[object_id] [bigint] NULL,
	[descrition] [nvarchar](1000) NULL,
	[address_type_code] [nvarchar](50) NULL,
	[nationality_code] [nvarchar](50) NULL,
	[career_code] [nvarchar](50) NULL,
	[card_type_code] [nvarchar](50) NULL,
	[card_number] [nvarchar](50) NULL,
	[receiver_type_code] [nvarchar](50) NULL,
	[name] [nvarchar](1000) NULL,
	[house_number] [nvarchar](1000) NULL,
	[village_number] [nvarchar](500) NULL,
	[alley] [nvarchar](500) NULL,
	[street] [nvarchar](500) NULL,
	[address_sub_district_code] [nvarchar](50) NULL,
	[address_district_code] [nvarchar](50) NULL,
	[address_province_code] [nvarchar](50) NULL,
	[postal_code] [nvarchar](50) NULL,
	[address_country_code] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[fax] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[sex_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[address_representative_condition_type_code] [nvarchar](50) NULL,
	[other_text] [nvarchar](1000) NULL,
 CONSTRAINT [PK_Contact] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RS_Address] ADD  CONSTRAINT [DF_Contact_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RS_Address] ADD  CONSTRAINT [DF_Contact_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RS_Address] ADD  CONSTRAINT [DF_Contact_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RS_Address]  WITH CHECK ADD  CONSTRAINT [FK_RS_Address_RM_AddressCountry] FOREIGN KEY([address_country_code])
REFERENCES [dbo].[RM_AddressCountry] ([code])
GO
ALTER TABLE [dbo].[RS_Address] CHECK CONSTRAINT [FK_RS_Address_RM_AddressCountry]
GO
ALTER TABLE [dbo].[RS_Address]  WITH CHECK ADD  CONSTRAINT [FK_RS_Address_RM_AddressDistrict] FOREIGN KEY([address_district_code])
REFERENCES [dbo].[RM_AddressDistrict] ([code])
GO
ALTER TABLE [dbo].[RS_Address] CHECK CONSTRAINT [FK_RS_Address_RM_AddressDistrict]
GO
ALTER TABLE [dbo].[RS_Address]  WITH CHECK ADD  CONSTRAINT [FK_RS_Address_RM_AddressProvince] FOREIGN KEY([address_province_code])
REFERENCES [dbo].[RM_AddressProvince] ([code])
GO
ALTER TABLE [dbo].[RS_Address] CHECK CONSTRAINT [FK_RS_Address_RM_AddressProvince]
GO
ALTER TABLE [dbo].[RS_Address]  WITH CHECK ADD  CONSTRAINT [FK_RS_Address_RM_AddressSubDistrict] FOREIGN KEY([address_sub_district_code])
REFERENCES [dbo].[RM_AddressSubDistrict] ([code])
GO
ALTER TABLE [dbo].[RS_Address] CHECK CONSTRAINT [FK_RS_Address_RM_AddressSubDistrict]
GO
