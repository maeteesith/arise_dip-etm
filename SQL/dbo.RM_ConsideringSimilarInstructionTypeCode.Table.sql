USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ConsideringSimilarInstructionTypeCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ConsideringSimilarInstructionTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ConsidieringSimilarInstructionTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarInstructionTypeCode] ON 

INSERT [dbo].[RM_ConsideringSimilarInstructionTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'N/A', N'ไม่ระบุ
', 0, CAST(N'2020-01-12T12:10:13.8500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'NO', N'ไม่บ่งเฉพาะ
', 0, CAST(N'2020-01-12T12:09:57.6533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'WAIT', N'รอพิจารณา
', 0, CAST(N'2020-01-12T12:09:53.7466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'YES', N'บ่งเฉพาะแล้ว
', 0, CAST(N'2020-01-12T12:09:51.4233333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarInstructionTypeCode] OFF
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionTypeCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionTypeCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionTypeCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionTypeCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionTypeCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
