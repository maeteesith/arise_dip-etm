USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RequestAgencyGroup]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestAgencyGroup](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_AgencyGroup] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RequestAgencyGroup] ON 

INSERT [dbo].[RequestAgencyGroup] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, N'กลุ่มทดสอบ', 0, CAST(N'2019-12-16T13:56:51.5500000' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RequestAgencyGroup] OFF
ALTER TABLE [dbo].[RequestAgencyGroup] ADD  CONSTRAINT [DF_AgencyGroup_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RequestAgencyGroup] ADD  CONSTRAINT [DF_AgencyGroup_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RequestAgencyGroup] ADD  CONSTRAINT [DF_AgencyGroup_created_by]  DEFAULT ((0)) FOR [created_by]
GO
