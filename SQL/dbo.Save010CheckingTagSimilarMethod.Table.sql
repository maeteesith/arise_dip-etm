USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010CheckingTagSimilarMethod]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010CheckingTagSimilarMethod](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NOT NULL,
	[method_1] [bit] NULL,
	[method_2] [bit] NULL,
	[method_3] [bit] NULL,
	[method_4] [bit] NULL,
	[method_5] [bit] NULL,
	[method_6] [bit] NULL,
	[method_7] [bit] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010CheckingTagSimilarMethod] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarMethod] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarMethod_save_id]  DEFAULT ((0)) FOR [save_id]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarMethod] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarMethod_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarMethod] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarMethod_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarMethod] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarMethod_created_by]  DEFAULT ((0)) FOR [created_by]
GO
