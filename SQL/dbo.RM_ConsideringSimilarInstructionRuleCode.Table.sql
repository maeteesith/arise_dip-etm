USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ConsideringSimilarInstructionRuleCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ConsideringSimilarInstructionRuleCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[description] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[value_4] [nvarchar](1000) NULL,
	[value_5] [nvarchar](1000) NULL,
	[is_show] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ConsidieringSimilarInstructionRuleCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ON 

INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1010, N'CASE_10', N'หนังสือแจ้งเพื่อทราบ', N'นายทะเบียนพิจารณาขอแจ้งให้ทราบว่า', N'รายละเอียด', NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-14T22:46:24.4200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 1007, N'CASE_7', N'มาตรา 12', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เว้นแต่จะได้ดำเนินการ', N'รายละเอียด', NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-14T22:45:16.8433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (14, 1, N'NOT_SPECIFIC', N'ไม่บ่งจำเพาะ', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-01-29T16:44:40.2266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'PUBLIC', N'ประกาศโฆษณา', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-01-14T13:49:33.0800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 10, N'ROLE_10', N'มาตรา 10', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เพราะไม่ชอบด้วยมาตรา 10 เนื่องจากไม่มีสำนักงานหรือสถานที่ ที่นายทะเบียนสามารถติดต่อตั้งอยู่ในประเทศไทย เว้นแต่ จะได้ดำเนินการแก้ไขเปลี่ยนแปลงตามมาตรา 15', N'รายละเอียด', NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-14T09:59:39.2100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 11, N'ROLE_11', N'มาตรา 11', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เนื่องจากมีข้อบกพร่องตามมาตรา 11 เพราะไม่เป็นไปตามหลักเกณฑ์ วิธีการ ที่กำหนดไว้ในกฎกระทรวง เว้นแต่ จะได้ดำเนินการให้ถูกต้องภายใน 60 วัน นับแต่วันที่ได้รับหนังสือฉบับนี้', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-14T10:00:19.2600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 13, N'ROLE_13_1', N'มาตรา 13 (1)', N'เป็นเครื่องหมายที่ไม่มีลักษณะอันพึงรับจดทะเบียนได้ ตามมาตรา 6 เนื่องจากคล้ายกับเครื่องหมายของบุคคลอื่น ที่ได้จดทะเบียนไว้แล้วตามมาตรา 13', N'หมายเหตุ', N'เพราะว่า (ม.27)', N'เงื่อนไข (ม.27)', NULL, NULL, 1, 0, CAST(N'2020-01-14T22:47:49.6866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (16, 13, N'ROLE_13_2', N'มาตรา 13 (2)', N'เป็นเครื่องหมายที่ไม่มีลักษณะอันพึงรับจดทะเบียนได้ ตามมาตรา 6 เนื่องจากเหมือนกับเครื่องหมายของบุคคลอื่น ที่ได้จดทะเบียนไว้แล้วตามมาตรา 13', N'หมายเหตุ', N'เพราะว่า (ม.27)', N'เงื่อนไข (ม.27)', NULL, NULL, 1, 0, CAST(N'2020-01-14T22:47:49.6866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 17, N'ROLE_17', N'มาตรา 17', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เว้นแต่ จะได้ดำเนินการ ตามมาตรา 17 โดยให้ยื่นแบบ ก.12 ภายใน 60 วัน นับแต่วันที่ได้รับหนังสือฉบับนี้', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-14T10:00:25.8133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 20, N'ROLE_20_1', N'มาตรา 20 (1)', N'คล้ายกับเครื่องหมายของบุคคลอื่น ที่ยื่นขอจดทะเบียนไว้ก่อนท่าน', N'หมายเหตุ', N'เพราะว่า (ม.27)', N'เงื่อนไข (ม.27)', NULL, NULL, 1, 0, CAST(N'2020-01-14T22:00:01.7066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (17, 20, N'ROLE_20_2', N'มาตรา 20 (2)', N'เหมือนกับเครื่องหมายของบุคคลอื่น ที่ยื่นขอจดทะเบียนไว้ก่อนท่าน', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-05-11T17:48:45.0200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, 27, N'ROLE_27', N'มาตรา 27', N'เป็นเครื่องหมายที่เหมือนหรือคล้ายกับเครื่องหมายของบุคคลอื่น', N'หมายเหตุ', N'เพราะว่า (ม.27)', N'เงื่อนไข (ม.27)', NULL, NULL, 1, 0, CAST(N'2020-01-14T22:48:14.1900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (13, 29, N'ROLE_29', N'มาตรา 29', N'เหมือนหรือคล้ายกับเครื่องหมายของบุคคลอื่นที่นายทะเบียนได้มีคำสั่งให้ประกาศโฆษณาตามมาตรา 29', N'หมายเหตุ', N'เพราะว่า (ม.27)', N'เงื่อนไข (ม.27)', NULL, NULL, 1, 0, CAST(N'2020-01-14T22:48:46.4100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 7, N'ROLE_7', N'มาตรา 7', N'เป็นเครื่องหมายที่ไม่มีลักษณะอันพึงรับจดทะเบียนได้ ตามมาตรา 6 เนื่องจากไม่มีลักษณะบ่งเฉพาะ ตามมาตรา 7', N'รายละเอียด', NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-12T13:22:41.0366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 8, N'ROLE_8', N'มาตรา 8', N'เป็นเครื่องหมายที่ไม่มีลักษณะอันพึงรับจดทะเบียนได้ ตามมาตรา 6 เนื่องจากต้องห้ามรับจดทะเบียนตามมาตรา 8', N'รายละเอียด', NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-12T13:57:52.8400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 9, N'ROLE_9', N'มาตรา 9', N'เครื่องหมายนี้ยังไม่สามารถรับจดทะเบียนได้ เนื่องจากมีข้อบกพร่องตามมาตรา 9 เพราะไม่ระบุรายการสินค้าแต่ละอย่าง โดยชัดแจ้ง เว้นแต่ จะได้ยื่นแบบ ก.06 เพื่อแก้ไขให้ถูกต้องภายใน 60 วัน นับแต่วันที่ได้รับหนังสือฉบับนี้ โดยให้แก้ไขรายการ สินค้า', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-01-12T13:58:42.8300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (18, 10503, N'RULE_5_3', N'ตค. 5(3)', N'หนังสือแจ้งชำระเงิน', NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-06-03T00:09:02.6133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (23, 10508, N'RULE_5_8_EXTEND', N'ตค. 5(8)', N'ต่ออายุ', NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-06-15T01:00:21.6400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (22, 10508, N'RULE_5_8_FEE', N'ตค. 5(8)', N'หนังสือแจ้งชำระเงิน', NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-06-15T01:00:20.2866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (19, 10601, N'RULE_6_1', N'ตค. 6(1)', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-06-03T00:09:03.3566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (20, 10901, N'RULE_9_1', N'ตค. 9(1)  ', N'หนังสือสำคัญ', NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-06-03T00:09:04.1700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (21, 10910, N'RULE_9_10', N'ตค. 9(10)', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-06-03T00:09:04.9266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (24, 10911, N'RULE_9_11_CANCEL_PUBLIC', N'ตค. 9(11)', N'ยกเลิกประกาศ', NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-06-15T01:00:22.9366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([id], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_show], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (15, 1, N'SPECIFIC', N'บ่งจำเพาะ', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, CAST(N'2020-01-29T16:44:45.1800000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarInstructionRuleCode] OFF
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionRuleCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionRuleCode_is_show]  DEFAULT ((1)) FOR [is_show]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
