USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_AddressReceiverType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_AddressReceiverType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_AddressReceiverType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_AddressReceiverType] ON 

INSERT [dbo].[RM_AddressReceiverType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'CORPORATE', N'นิติบุคคล', 0, CAST(N'2019-12-15T11:35:02.3733333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_AddressReceiverType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 4, N'CORPORATE_FOREIGNER', N'นิติบุคคลต่างชาติ', 0, CAST(N'2020-02-25T09:33:54.1700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressReceiverType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 5, N'GOVERNMENT', N'หน่วยงานราชการ', 0, CAST(N'2020-02-28T16:09:43.9133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressReceiverType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 6, N'OTHERS', N'อื่นๆ', 0, CAST(N'2019-12-15T11:35:56.2466667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_AddressReceiverType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1, N'PEOPLE', N'บุคคลธรรมดา', 0, CAST(N'2020-02-25T09:34:08.1933333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressReceiverType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 3, N'PEOPLE_FOREIGNER', N'บุคคลธรรมดาต่างชาติ', 0, CAST(N'2020-02-25T09:34:08.1933333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_AddressReceiverType] OFF
ALTER TABLE [dbo].[RM_AddressReceiverType] ADD  CONSTRAINT [DF_RM_AddressReceiverType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_AddressReceiverType] ADD  CONSTRAINT [DF_RM_AddressReceiverType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_AddressReceiverType] ADD  CONSTRAINT [DF_RM_AddressReceiverType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_AddressReceiverType] ADD  CONSTRAINT [DF_RM_AddressReceiverType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
