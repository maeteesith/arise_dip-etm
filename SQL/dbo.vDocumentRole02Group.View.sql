USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentRole02Group]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vDocumentRole02Group] as

select

dr.id,
dr.save_id,
dr.request_number,
dr.request_date,
dr.post_number,
dr.book_number,

count(isnull(dr.instruction_rule_id,1)) rule_count,

max(dr.instruction_send_date) instruction_send_date,
dr.post_round_document_post_date,
dr.document_role02_date,
dr.document_role02_receive_date,
dr.document_role02_print_document_date,
dr.document_role02_print_cover_date,
dr.document_role02_post_number_date,
dr.document_role02_print_list_date,

dr.post_round_type_code,
dr.post_round_type_name,

dr.document_role02_receiver_by,
dr.document_role02_receiver_by_name, 

max(dr.created_by_name) created_by_name,
max(dr.department_code) department_code,
max(dr.department_name) department_name,

dr.document_role02_status_code,
max(dr.document_role02_status_name) document_role02_status_name,
dr.document_role02_receive_status_code,
max(dr.document_role02_receive_status_name) document_role02_receive_status_name,

dr.document_role02_print_document_status_code,
dr.document_role02_print_document_status_name,
dr.document_role02_print_cover_status_code,
dr.document_role02_print_cover_status_name,
dr.document_role02_print_list_status_code,
dr.document_role02_print_list_status_name,

max(dr.request_item_sub_type_1_code_text) request_item_sub_type_1_code_text,

max(dr.name) name,
max(dr.house_number) house_number,
max(dr.province_name) province_name,
max(dr.postal_name) postal_name,

cast(max(cast(dr.is_add_more_address as int)) as bit) is_add_more_address,

max(dr.document_role02_remark) document_role02_remark,
max(dr.document_role02_receive_remark) document_role02_receive_remark,

dr.is_deleted,
dr.created_by,
dr.created_date,
dr.updated_by,
dr.updated_date

from vDocumentRole02 dr

group by 
dr.id,
dr.save_id,
dr.request_number,
dr.request_date,
dr.post_number,
dr.book_number,

dr.post_round_document_post_date,
dr.document_role02_date,
dr.document_role02_receive_date,
dr.document_role02_print_document_date,
dr.document_role02_print_cover_date,
dr.document_role02_post_number_date,
dr.document_role02_print_list_date,

dr.post_round_type_code,
dr.post_round_type_name,

dr.document_role02_receiver_by,
dr.document_role02_receiver_by_name, 

dr.document_role02_status_code,
dr.document_role02_receive_status_code,

dr.document_role02_print_document_status_code,
dr.document_role02_print_document_status_name,
dr.document_role02_print_cover_status_code,
dr.document_role02_print_cover_status_name,
dr.document_role02_print_list_status_code,
dr.document_role02_print_list_status_name,

dr.is_deleted,
dr.created_by,
dr.created_date,
dr.updated_by,
dr.updated_date
GO
