USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010Case28Process]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010Case28Process](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[is_considering_similar_kor10_1_1] [bit] NULL,
	[considering_similar_kor10_1_1_description] [nvarchar](1000) NULL,
	[is_considering_similar_kor10_1_2] [bit] NULL,
	[considering_similar_kor10_1_2_description] [nvarchar](1000) NULL,
	[is_considering_similar_kor10_1_4] [bit] NULL,
	[considering_similar_kor10_1_4_description] [nvarchar](1000) NULL,
	[is_considering_similar_kor10_2_1] [bit] NULL,
	[is_considering_similar_kor10_2_2] [bit] NULL,
	[is_considering_similar_kor10_2_3] [bit] NULL,
	[is_considering_similar_kor10_2_4] [bit] NULL,
	[is_considering_similar_kor10_2_5] [bit] NULL,
	[is_considering_similar_kor10_2_6] [bit] NULL,
	[is_considering_similar_kor10_3] [bit] NULL,
	[considering_similar_kor10_3_description] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010Case28Process] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010Case28Process] ADD  CONSTRAINT [DF_Save010Case28Process_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010Case28Process] ADD  CONSTRAINT [DF_Save010Case28Process_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010Case28Process] ADD  CONSTRAINT [DF_Save010Case28Process_created_by]  DEFAULT ((0)) FOR [created_by]
GO
