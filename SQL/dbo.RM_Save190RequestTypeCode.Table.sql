USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save190RequestTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save190RequestTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save190RequestTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save190RequestTypeCode] ON 

INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'010', N'ก.01 คำขอจดทะเบียนเครื่องหมาย', 0, CAST(N'2020-04-16T15:22:01.6800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'020', N'ก.02 คำคัดค้าน การขอจดทะเบียนเครื่องหมายการค้า', 0, CAST(N'2020-04-30T17:39:02.1000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 3, N'021', N'ก.02 คำคัดค้าน/โต้แย้ง การขอจดทะเบียนเครื่องหมายการค้า', 0, CAST(N'2020-04-30T17:39:36.7100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 4, N'030', N'ก.03 คำอุทธรณ์', 0, CAST(N'2020-04-30T17:39:53.9033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 5, N'040', N'ก.04 คำขอโอนหรือรับมรดกสิทธิ', 0, CAST(N'2020-04-30T17:40:00.7233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 6, N'050', N'ก.05 คำขอจดทะเบียนสัญญาอนุญาตให้ใช้เครื่องหมายการค้า/บริการ', 0, CAST(N'2020-04-30T17:40:06.1666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 7, N'060', N'ก.06 คำขอแก้ไขเปลี่ยนแปลงรายการจดทะเบียน', 0, CAST(N'2020-04-30T17:40:15.0466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 8, N'070', N'ก.07 คำขอต่ออายุการจดทะเบียน', 0, CAST(N'2020-04-30T17:40:22.2566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 9, N'080', N'ก.08 คำขอให้เพิกถอนการจดทะเบียน', 0, CAST(N'2020-04-30T17:40:28.4033333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save190RequestTypeCode] OFF
ALTER TABLE [dbo].[RM_Save190RequestTypeCode] ADD  CONSTRAINT [DF_RM_Save190RequestTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save190RequestTypeCode] ADD  CONSTRAINT [DF_RM_Save190RequestTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save190RequestTypeCode] ADD  CONSTRAINT [DF_RM_Save190RequestTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save190RequestTypeCode] ADD  CONSTRAINT [DF_RM_Save190RequestTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
