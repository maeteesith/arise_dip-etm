USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[DocumentRole03Item]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentRole03Item](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_01_id] [bigint] NOT NULL,
	[save_01_index] [bigint] NULL,
	[save_id] [bigint] NOT NULL,
	[request_type_code] [nvarchar](50) NULL,
	[document_role03_status_code] [nvarchar](50) NULL,
	[document_role03_split_date] [datetime2](7) NULL,
	[document_role03_spliter_by] [bigint] NULL,
	[document_role03_receiver_by] [bigint] NULL,
	[document_role03_receive_status_code] [nvarchar](50) NULL,
	[document_role03_receive_date] [datetime2](7) NULL,
	[document_role03_receive_allow_list] [nvarchar](1000) NULL,
	[document_role03_receive_return_type_code] [nvarchar](50) NULL,
	[document_role03_receive_return_remark] [nvarchar](1000) NULL,
	[document_role03_receive_return_date] [datetime2](7) NULL,
	[value_01] [nvarchar](1000) NULL,
	[value_02] [nvarchar](1000) NULL,
	[value_03] [nvarchar](1000) NULL,
	[value_04] [nvarchar](1000) NULL,
	[value_05] [nvarchar](1000) NULL,
	[remark] [nvarchar](1000) NULL,
	[request_mark_feature_code_list] [nvarchar](500) NULL,
	[sound_description] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_DocumentRole03Item] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DocumentRole03Item] ADD  CONSTRAINT [DF_Table_1_public_role04_spliter_by]  DEFAULT ((0)) FOR [document_role03_spliter_by]
GO
ALTER TABLE [dbo].[DocumentRole03Item] ADD  CONSTRAINT [DF_Table_1_public_role04_receiver_by]  DEFAULT ((0)) FOR [document_role03_receiver_by]
GO
ALTER TABLE [dbo].[DocumentRole03Item] ADD  CONSTRAINT [DF_DocumentRole03Item_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[DocumentRole03Item] ADD  CONSTRAINT [DF_DocumentRole03Item_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[DocumentRole03Item] ADD  CONSTRAINT [DF_DocumentRole03Item_created_by]  DEFAULT ((0)) FOR [created_by]
GO
