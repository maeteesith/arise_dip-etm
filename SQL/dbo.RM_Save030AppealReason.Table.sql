USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save030AppealReason]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save030AppealReason](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save030AppealReason] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save030AppealReason] ON 

INSERT [dbo].[RM_Save030AppealReason] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE01', N'บ่งเฉพาะ', 0, CAST(N'2019-12-17T18:02:17.4766667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save030AppealReason] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'TYPE02', N'เหมือนคล้าย', 0, CAST(N'2019-12-17T18:02:17.4766667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save030AppealReason] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'TYPE03', N'คัดค้าน', 0, CAST(N'2019-12-17T18:02:17.4766667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save030AppealReason] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'TYPE04', N'เพิกถอน', 0, CAST(N'2019-12-17T18:02:17.4766667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_Save030AppealReason] OFF
ALTER TABLE [dbo].[RM_Save030AppealReason] ADD  CONSTRAINT [DF_RM_Save030AppealReason_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save030AppealReason] ADD  CONSTRAINT [DF_RM_Save030AppealReason_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save030AppealReason] ADD  CONSTRAINT [DF_RM_Save030AppealReason_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save030AppealReason] ADD  CONSTRAINT [DF_RM_Save030AppealReason_created_by]  DEFAULT ((0)) FOR [created_by]
GO
