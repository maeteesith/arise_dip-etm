USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RequestCheckRequestNumber]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestCheckRequestNumber](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_check_id] [bigint] NULL,
	[request_number] [nvarchar](50) NULL,
	[is_2_2] [bit] NULL,
	[number_2_3] [int] NULL,
	[is_2_5] [bit] NULL,
	[is_2_6] [bit] NULL,
	[is_2_7] [bit] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestCheckRequestNumber] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RequestCheckRequestNumber] ADD  CONSTRAINT [DF_RequestCheckRequestNumber_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RequestCheckRequestNumber] ADD  CONSTRAINT [DF_RequestCheckRequestNumber_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RequestCheckRequestNumber] ADD  CONSTRAINT [DF_RequestCheckRequestNumber_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RequestCheckRequestNumber]  WITH CHECK ADD  CONSTRAINT [FK_RequestCheckRequestNumber_RequestCheck] FOREIGN KEY([request_check_id])
REFERENCES [dbo].[RequestCheck] ([id])
GO
ALTER TABLE [dbo].[RequestCheckRequestNumber] CHECK CONSTRAINT [FK_RequestCheckRequestNumber_RequestCheck]
GO
