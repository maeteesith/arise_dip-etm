USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vUM_User]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vUM_User] as
select 
u.id,
u.name,
u.department_id,
d.name department_name,
u.is_deleted,
u.created_by,
u_created.name created_by_name,
u.created_date,
u.updated_by,
u_updated.name updated_by_name,
u.updated_date
from UM_User u
left join RM_Department d on d.id = u.department_id
left join UM_User u_created on u_created.id = u.created_by
left join UM_User u_updated on u_updated.id = u.updated_by
GO
