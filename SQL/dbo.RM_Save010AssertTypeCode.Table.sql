USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save010AssertTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save010AssertTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save010AssertTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save010AssertTypeCode] ON 

INSERT [dbo].[RM_Save010AssertTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 2, N'ASSERT_1', N'ขอถือสิทธิ์และได้ยื่นคำขอพร้อมเอกสารหลักฐาน', 0, CAST(N'2020-03-27T06:39:49.8600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010AssertTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 3, N'ASSERT_2', N'ขอถือสิทธิ์และได้ยื่นหนังสือขอผ่อนผันส่งเอกสารหลักฐาน', 0, CAST(N'2020-03-27T06:40:36.9500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010AssertTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'NOT_ASSERT', N'ไม่ขอถือสิทธิ์', 0, CAST(N'2020-03-27T06:35:11.3966667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save010AssertTypeCode] OFF
ALTER TABLE [dbo].[RM_Save010AssertTypeCode] ADD  CONSTRAINT [DF_RM_Save010AssertTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save010AssertTypeCode] ADD  CONSTRAINT [DF_RM_Save010AssertTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_Save010AssertTypeCode] ADD  CONSTRAINT [DF_Save010AssertTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save010AssertTypeCode] ADD  CONSTRAINT [DF_Save010AssertTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save010AssertTypeCode] ADD  CONSTRAINT [DF_Save010AssertTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
