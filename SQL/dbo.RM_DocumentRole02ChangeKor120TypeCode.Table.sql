USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole02ChangeKor120TypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole02ChangeKor120TypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[value_01] [nvarchar](1000) NULL,
	[value_02] [nvarchar](1000) NULL,
	[value_03] [nvarchar](1000) NULL,
	[value_04] [nvarchar](1000) NULL,
	[value_05] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole02ChangeKor120TypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ON 

INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE_01', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิแต่เพียงผู้เดียวที่จะใช้อักษรโรมัน คำว่า', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:28.9233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'TYPE_02', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิแต่เพียงผู้เดียวที่จะใช้อักษรไทย คำว่า', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:39.2666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'TYPE_03', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิแต่เพียงผู้เดียวที่จะใช้อักษรโรมันและอักษรไทย คำว่า', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:45.4066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'TYPE_04', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิแต่เพียงผู้เดียวที่จะใช้อักษรจีน คำว่า', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:47.3133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 5, N'TYPE_05', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิแต่เพียงผู้เดียวที่จะใช้อักษรญี่ปุ่น คำว่า', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:48.6733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 6, N'TYPE_06', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิแต่เพียงผู้เดียวที่จะใช้อักษรไทย คำว่า', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:50.1233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 7, N'TYPE_07', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิแต่เพียงผู้เดียวที่จะใช้อักษรโรมันทั้งหมด ยกเว้น คำว่า', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:51.7633333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 8, N'TYPE_08', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิแต่เพียงผู้เดียวที่จะใช้อักษรโรมัน คำว่า จะไม่ใช้สีแถบให้เหมือนหรือคล้ายสีธงชาติหรือสีอื่นที่คล้ายคลึงกับสีดังกล่าว', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:53.3833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 9, N'TYPE_09', N'ข้าพเจ้าขอรับรองว่าจะไม่ใช้สีแถบให้เหมือนหรือคล้ายสีธงชาติ หรือสีอื่นที่คล้ายคลึงกับสีดังกล่าว', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:41:58.5433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ([id], [index], [code], [name], [value_01], [value_02], [value_03], [value_04], [value_05], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 10, N'TYPE_10', N'ข้าพเจ้าไม่ขอถือเป็นสิทธิของตนแต่เพียงผู้เดียวที่จะใช้ ข้าพเจ้าขอรับรองว่าจะไม่ใช้รูปกากบาทให้เป็นสีแดงหรือสีเขียวบนพื้นสีขาวหรือสีเงิน 
หรือรูปกากบาทสีขาวหรือสีเงินบนพื้นสีแดง หรือสีอื่นที่คล้ายคลึงกับสีดังกล่าว', NULL, NULL, NULL, NULL, NULL, 1, 0, CAST(N'2020-06-21T21:42:04.1466667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole02ChangeKor120TypeCode] OFF
ALTER TABLE [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ChangeKor120TypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ChangeKor120TypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ChangeKor120TypeCode_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ChangeKor120TypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ChangeKor120TypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_DocumentRole02ChangeKor120TypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole02ChangeKor120TypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
