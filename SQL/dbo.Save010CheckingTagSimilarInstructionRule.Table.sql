USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010CheckingTagSimilarInstructionRule]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010CheckingTagSimilarInstructionRule](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save010_checking_tag_similar_id] [bigint] NULL,
	[post_round_instruction_rule_id] [bigint] NULL,
	[post_round_instruction_rule_code] [nvarchar](50) NULL,
	[post_round_type_code] [nvarchar](50) NULL,
	[document_role04_status_code] [nvarchar](50) NULL,
	[document_role04_receiver_by] [bigint] NULL,
	[document_role04_receive_date] [datetime2](7) NULL,
	[document_role04_receive_remark] [nvarchar](1000) NULL,
	[document_role04_receive_additional_remark] [nvarchar](1000) NULL,
	[document_role04_release_request_send_type_code] [nvarchar](50) NULL,
	[document_role04_receive_status_code] [nvarchar](50) NULL,
	[document_role04_receive_check_list] [nvarchar](1000) NULL,
	[document_role04_receive_send_date] [datetime2](7) NULL,
	[document_role05_by] [bigint] NULL,
	[document_role05_date] [datetime2](7) NULL,
	[document_role05_status_code] [nvarchar](50) NULL,
	[document_role05_receiver_by] [bigint] NULL,
	[document_role05_receive_date] [datetime2](7) NULL,
	[document_role05_receive_status_code] [nvarchar](50) NULL,
	[document_role05_receive_send_date] [datetime2](7) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010CheckingTagSimilarPostRound] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarInstructionRule] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarPostRound_document_role04_receiver_by]  DEFAULT ((0)) FOR [document_role04_receiver_by]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarInstructionRule] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarPostRound_document_role05_receiver_by]  DEFAULT ((0)) FOR [document_role05_receiver_by]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarInstructionRule] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarPostRound_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarInstructionRule] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarPostRound_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilarInstructionRule] ADD  CONSTRAINT [DF_Save010CheckingTagSimilarPostRound_created_by]  DEFAULT ((0)) FOR [created_by]
GO
