USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save010ConsideringSimilarDocument06TypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ON 

INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'CONTACT', N'สถานที่ติดต่อ', 0, CAST(N'2020-02-29T12:28:58.3066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'OWNER', N'เจ้าของ', 0, CAST(N'2020-02-29T12:28:51.2466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 5, N'PRODUCT_DESCRIPTION', N'รายการสินค้า', 0, CAST(N'2020-02-29T12:29:17.8033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'REPRESENTATIVE', N'ตัวแทน', 0, CAST(N'2020-02-29T12:28:55.7066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'REQUEST_ITEM_TYPE_01', N'จำพวก', 0, CAST(N'2020-02-29T12:29:07.1100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 8, N'RULE', N'ข้อบังคับ', 0, CAST(N'2020-02-29T12:29:46.4300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 7, N'SOUND_TRANSLATE', N'คำอ่านและคำแปล', 0, CAST(N'2020-02-29T12:29:38.4066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 6, N'TRADEMARK', N'รูปเครื่องหมาย', 0, CAST(N'2020-02-29T12:29:27.0000000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] OFF
ALTER TABLE [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ADD  CONSTRAINT [DF_RM_Save010ConsideringSimilarDocument06TypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ADD  CONSTRAINT [DF_RM_Save010ConsideringSimilarDocument06TypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ADD  CONSTRAINT [DF_RM_Save010ConsideringSimilarDocument06TypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save010ConsideringSimilarDocument06TypeCode] ADD  CONSTRAINT [DF_RM_Save010ConsideringSimilarDocument06TypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
