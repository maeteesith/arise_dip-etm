USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[PostRoundInstructionRule]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostRoundInstructionRule](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[post_round_id] [bigint] NOT NULL,
	[save010_instruction_rule_id] [bigint] NOT NULL,
	[document_role02_check_status_code] [nvarchar](50) NULL,
	[document_role02_check_date] [datetime2](7) NULL,
	[document_role04_send_type_code] [nvarchar](50) NULL,
	[document_role04_check_date] [datetime2](7) NULL,
	[document_role04_check_remark] [nvarchar](1000) NULL,
	[new_instruction_rule_code] [nvarchar](50) NULL,
	[value_01] [nvarchar](1000) NULL,
	[value_02] [nvarchar](1000) NULL,
	[value_03] [nvarchar](1000) NULL,
	[value_04] [nvarchar](1000) NULL,
	[value_05] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_PostRoundInstructionRule] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PostRoundInstructionRule] ADD  CONSTRAINT [DF_PostRoundInstructionRule_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[PostRoundInstructionRule] ADD  CONSTRAINT [DF_PostRoundInstructionRule_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[PostRoundInstructionRule] ADD  CONSTRAINT [DF_PostRoundInstructionRule_created_by]  DEFAULT ((0)) FOR [created_by]
GO
