USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Request01ItemChange]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Request01ItemChange](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request01_id] [bigint] NULL,
	[item_index] [int] NULL,
	[item_type_code] [nvarchar](50) NULL,
	[income_type] [nvarchar](50) NULL,
	[income_description] [nvarchar](1000) NULL,
	[sound_file_id] [bigint] NULL,
	[sound_type_code] [nvarchar](50) NULL,
	[sound_description] [nvarchar](1000) NULL,
	[facilitation_act_status_code] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[size_over_cm] [decimal](18, 2) NULL,
	[size_over_price] [decimal](18, 2) NULL,
	[size_over_income_type] [nvarchar](250) NULL,
	[size_over_income_description] [nvarchar](1000) NULL,
	[request_evidence_list] [nvarchar](250) NULL,
	[total_price] [decimal](18, 2) NULL,
	[book_index] [nvarchar](50) NULL,
	[page_index] [nvarchar](50) NULL,
	[is_otop] [bit] NULL,
	[reference_number] [nvarchar](50) NULL,
	[cancel_reason] [nvarchar](500) NULL,
	[status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[source_id] [bigint] NULL,
	[request01_process_change_id] [bigint] NULL,
 CONSTRAINT [PK__Request0__3213E83F2889C68E] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Request01ItemChange]  WITH CHECK ADD  CONSTRAINT [FK_Request01ItemChange_Request01ProcessChange] FOREIGN KEY([request01_process_change_id])
REFERENCES [dbo].[Request01ProcessChange] ([id])
GO
ALTER TABLE [dbo].[Request01ItemChange] CHECK CONSTRAINT [FK_Request01ItemChange_Request01ProcessChange]
GO
