USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[ReceiptItem]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReceiptItem](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[receipt_id] [bigint] NOT NULL,
	[request_id] [bigint] NULL,
	[post_round_id] [bigint] NULL,
	[request_type_code] [nvarchar](50) NULL,
	[name] [nvarchar](1000) NULL,
	[request_number] [nvarchar](50) NULL,
	[receipt_number] [nvarchar](50) NULL,
	[book_index] [nvarchar](50) NULL,
	[page_index] [nvarchar](50) NULL,
	[source_code] [nvarchar](50) NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_ReceiptItem] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReceiptItem] ADD  CONSTRAINT [DF_ReceiptItem_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[ReceiptItem] ADD  CONSTRAINT [DF_ReceiptItem_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ReceiptItem] ADD  CONSTRAINT [DF_ReceiptItem_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[ReceiptItem]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptItem_Receipt] FOREIGN KEY([receipt_id])
REFERENCES [dbo].[Receipt] ([id])
GO
ALTER TABLE [dbo].[ReceiptItem] CHECK CONSTRAINT [FK_ReceiptItem_Receipt]
GO
ALTER TABLE [dbo].[ReceiptItem]  WITH NOCHECK ADD  CONSTRAINT [FK_ReceiptItem_RM_RequestType] FOREIGN KEY([request_type_code])
REFERENCES [dbo].[RM_RequestType] ([code])
GO
ALTER TABLE [dbo].[ReceiptItem] NOCHECK CONSTRAINT [FK_ReceiptItem_RM_RequestType]
GO
