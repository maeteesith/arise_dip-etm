USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_CheckingStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_CheckingStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_CheckingStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_CheckingStatusCode] ON 

INSERT [dbo].[RM_CheckingStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'RECEIVED_CHECKING', N'รับงานตรวจสอบแล้ว', 0, CAST(N'2019-12-19T17:24:33.6700000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_CheckingStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'RECEIVED_CONSIDERING', N'รับงานพิจารณาแล้ว', 0, CAST(N'2019-12-19T19:35:33.6333333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_CheckingStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'WAIT_CHECKING', N'รอรับงานตรวจสอบ', 0, CAST(N'2019-12-19T17:24:25.6900000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_CheckingStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'WAIT_CONSIDERING', N'รอรับงานพิจารณา', 0, CAST(N'2019-12-19T19:35:14.1166667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_CheckingStatusCode] OFF
ALTER TABLE [dbo].[RM_CheckingStatusCode] ADD  CONSTRAINT [DF_RM_CheckingStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_CheckingStatusCode] ADD  CONSTRAINT [DF_RM_CheckingStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_CheckingStatusCode] ADD  CONSTRAINT [DF_RM_CheckingStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_CheckingStatusCode] ADD  CONSTRAINT [DF_RM_CheckingStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
