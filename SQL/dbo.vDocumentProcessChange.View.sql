USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentProcessChange]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vDocumentProcessChange] as

select 

sv.id,
sv.save_id,

s.request_number,

s.request_date,
sv.check_date,

sv.document_classification_version_status_code,
rm_dv.name document_classification_version_status_name,

(select top 1 sp.name from Save010AddressPeople sp where sp.save_id = s.id and sp.is_deleted = 0) name,
(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (select sp.request_item_sub_type_1_code + ', ' as [text()] from Save010Product sp where sp.save_id = s.id group by sp.request_item_sub_type_1_code order by sp.request_item_sub_type_1_code for xml path('')) code) sp) request_item_sub_type_1_code_text,

sv.is_deleted,
sv.created_date,
sv.created_by,
sv.updated_date,
sv.updated_by

from Save010 s
join Save010DocumentClassificationVersion sv on sv.save_id = s.id
left join RM_DocumentClassificationVersionStatusCode rm_dv on rm_dv.code = sv.document_classification_version_status_code
where sv.version_index > 1
GO
