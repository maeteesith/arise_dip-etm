USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PostRoundInstructionRuleFileTypeCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PostRoundInstructionRuleFileTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PostRoundInstructionRuleFileTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ON 

INSERT [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'CONSIDERING', N'เอกสารประกอบพิจารณา', 1, 0, CAST(N'2020-05-29T14:10:32.8800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'SCAN', N'เอกสารสแกน', 1, 0, CAST(N'2020-05-29T14:10:36.5166667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PostRoundInstructionRuleFileTypeCode] OFF
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleFileTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleFileTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleFileTypeCode_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleFileTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleFileTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PostRoundInstructionRuleFileTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundInstructionRuleFileTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
