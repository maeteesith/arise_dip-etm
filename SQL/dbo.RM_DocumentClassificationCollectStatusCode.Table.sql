USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentClassificationCollectStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentClassificationCollectStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentClassificationCollectStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentClassificationCollectStatusCode] ON 

INSERT [dbo].[RM_DocumentClassificationCollectStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'DELETE', N'ลบ', 0, CAST(N'2020-04-10T06:57:00.3200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentClassificationCollectStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'DONE', N'บันทึกแล้ว', 0, CAST(N'2020-04-10T05:44:46.2500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentClassificationCollectStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'WAIT', N'รอดำเนินการ', 0, CAST(N'2020-04-10T05:44:44.2600000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentClassificationCollectStatusCode] OFF
ALTER TABLE [dbo].[RM_DocumentClassificationCollectStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationCollectStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentClassificationCollectStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationCollectStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentClassificationCollectStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationCollectStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_DocumentClassificationCollectStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationCollectStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
