USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole04CheckList2Code]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole04CheckList2Code](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole04CheckList2Code] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04CheckList2Code] ON 

INSERT [dbo].[RM_DocumentRole04CheckList2Code] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 8, N'APPEAL', N'มีอุทธรณ์ / คำวินิจฉัยฯ', 1, 0, CAST(N'2020-06-02T13:19:22.1833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CheckList2Code] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 4, N'COMMAND', N'การปฏิบัติตามคำสั่ง', 1, 0, CAST(N'2020-05-29T11:05:12.1000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CheckList2Code] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'CONTACT', N'สถานที่ติดต่อ', 1, 0, CAST(N'2020-05-29T11:04:59.9300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CheckList2Code] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'INFORM', N'หนังสือแจ้งคำสั่ง', 1, 0, CAST(N'2020-05-29T11:04:53.6033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CheckList2Code] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 7, N'PARTIES', N'ชื่อคู่กรณี (ถ้ามี)', 1, 0, CAST(N'2020-06-02T13:18:45.9033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CheckList2Code] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 5, N'PAYMENT', N'การชำระค่าธรรมเนียม', 1, 0, CAST(N'2020-05-29T11:05:15.3100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CheckList2Code] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 6, N'PROSECUTE', N'การแจ้งฟ้องคดี / คำพิพากษา', 1, 0, CAST(N'2020-06-02T13:18:25.0400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CheckList2Code] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 3, N'RECEIVE', N'ใบตอบรับ / ครบกำหนด', 1, 0, CAST(N'2020-05-29T11:05:05.7133333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04CheckList2Code] OFF
ALTER TABLE [dbo].[RM_DocumentRole04CheckList2Code] ADD  CONSTRAINT [DF_RM_DocumentRole04CheckList2Code_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole04CheckList2Code] ADD  CONSTRAINT [DF_RM_DocumentRole04CheckList2Code_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole04CheckList2Code] ADD  CONSTRAINT [DF_RM_DocumentRole04CheckList2Code_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_DocumentRole04CheckList2Code] ADD  CONSTRAINT [DF_RM_DocumentRole04CheckList2Code_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole04CheckList2Code] ADD  CONSTRAINT [DF_RM_DocumentRole04CheckList2Code_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_DocumentRole04CheckList2Code] ADD  CONSTRAINT [DF_RM_DocumentRole04CheckList2Code_created_by]  DEFAULT ((0)) FOR [created_by]
GO
