USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RequestCheck]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestCheck](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[reference_number] [nvarchar](50) NULL,
	[requester_name] [nvarchar](1000) NULL,
	[registrar_by] [bigint] NULL,
	[id_card] [nvarchar](500) NULL,
	[telephone] [nvarchar](500) NULL,
	[registrar_by_name] [nvarchar](1000) NULL,
	[maker_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_wave_fee] [bit] NULL,
	[is_appointment] [bit] NULL,
	[is_commercial_province] [bit] NULL,
	[voucher_number] [nvarchar](50) NULL,
	[voucher_remain_second] [int] NULL,
	[voucher_log_in_date] [datetime2](7) NULL,
	[is_voucher_log_in] [bit] NULL,
	[request_item_sub_type1_code] [nvarchar](50) NULL,
	[request_item_sub_type1_similar_list] [nvarchar](500) NULL,
	[document_classification_image_type_code] [nvarchar](50) NULL,
	[request_sound_type_code] [nvarchar](50) NULL,
	[request_sound_type_name] [nvarchar](1000) NULL,
	[status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestCheck] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RequestCheck] ADD  CONSTRAINT [DF_RequestCheck_voucher_remain_second]  DEFAULT ((0)) FOR [voucher_remain_second]
GO
ALTER TABLE [dbo].[RequestCheck] ADD  CONSTRAINT [DF_RequestCheck_is_voucher_log_in]  DEFAULT ((0)) FOR [is_voucher_log_in]
GO
ALTER TABLE [dbo].[RequestCheck] ADD  CONSTRAINT [DF_RequestCheck_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RequestCheck] ADD  CONSTRAINT [DF_RequestCheck_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RequestCheck] ADD  CONSTRAINT [DF_RequestCheck_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RequestCheck]  WITH CHECK ADD  CONSTRAINT [FK_RequestCheck_UM_User] FOREIGN KEY([created_by])
REFERENCES [dbo].[UM_User] ([id])
GO
ALTER TABLE [dbo].[RequestCheck] CHECK CONSTRAINT [FK_RequestCheck_UM_User]
GO
