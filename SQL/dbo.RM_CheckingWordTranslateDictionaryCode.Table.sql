USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_CheckingWordTranslateDictionaryCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_CheckingWordTranslateDictionaryCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_CheckingWordTranslateDictionaryCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_CheckingWordTranslateDictionaryCode] ON 

INSERT [dbo].[RM_CheckingWordTranslateDictionaryCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'CAMBRIDGE', N'Cambridge', N'https://dictionary.cambridge.org/dictionary/english/[WORD]', NULL, NULL, 0, CAST(N'2020-04-15T13:49:35.2400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingWordTranslateDictionaryCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'DICTIONARY', N'Dictionary', N'https://www.dictionary.com/browse/[WORD]', NULL, NULL, 0, CAST(N'2020-04-15T13:46:08.3533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingWordTranslateDictionaryCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 3, N'LONGDO', N'Longdo Dict', N'https://dict.longdo.com/search/[WORD]', NULL, NULL, 0, CAST(N'2020-04-13T05:46:06.2000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingWordTranslateDictionaryCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 5, N'OTHERS', N'อื่นๆ', NULL, NULL, NULL, 0, CAST(N'2020-04-27T08:17:57.1500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingWordTranslateDictionaryCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 4, N'OXFORD', N'Oxford', N'https://www.oxfordlearnersdictionaries.com/spellcheck/english/?q=[WORD]', NULL, NULL, 0, CAST(N'2020-04-15T13:46:22.3500000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_CheckingWordTranslateDictionaryCode] OFF
ALTER TABLE [dbo].[RM_CheckingWordTranslateDictionaryCode] ADD  CONSTRAINT [DF_RM_CheckingWordTranslateDictionaryCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_CheckingWordTranslateDictionaryCode] ADD  CONSTRAINT [DF_RM_CheckingWordTranslateDictionaryCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_CheckingWordTranslateDictionaryCode] ADD  CONSTRAINT [DF_RM_CheckingWordTranslateDictionaryCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_CheckingWordTranslateDictionaryCode] ADD  CONSTRAINT [DF_RM_CheckingWordTranslateDictionaryCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_CheckingWordTranslateDictionaryCode] ADD  CONSTRAINT [DF_RM_CheckingWordTranslateDictionaryCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
