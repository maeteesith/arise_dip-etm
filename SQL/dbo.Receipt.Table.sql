USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Receipt]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Receipt](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[reference_number] [nvarchar](50) NULL,
	[receipt_date] [date] NULL,
	[receive_date] [date] NULL,
	[request_date] [date] NULL,
	[is_paid_past] [bit] NULL,
	[is_manual] [bit] NULL,
	[receiver_name] [nvarchar](1000) NULL,
	[requester_name] [nvarchar](1000) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Receipt] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Receipt] ADD  CONSTRAINT [DF_Receipt_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Receipt] ADD  CONSTRAINT [DF_Receipt_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Receipt] ADD  CONSTRAINT [DF_Receipt_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Receipt]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_RM_ReceiptStatusCode] FOREIGN KEY([status_code])
REFERENCES [dbo].[RM_ReceiptStatus] ([code])
GO
ALTER TABLE [dbo].[Receipt] CHECK CONSTRAINT [FK_Receipt_RM_ReceiptStatusCode]
GO
ALTER TABLE [dbo].[Receipt]  WITH CHECK ADD  CONSTRAINT [FK_Receipt_UM_User] FOREIGN KEY([created_by])
REFERENCES [dbo].[UM_User] ([id])
GO
ALTER TABLE [dbo].[Receipt] CHECK CONSTRAINT [FK_Receipt_UM_User]
GO
