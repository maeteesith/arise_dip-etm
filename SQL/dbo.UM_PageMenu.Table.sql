USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[UM_PageMenu]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UM_PageMenu](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_UM_PageMenu] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UM_PageMenu] ON 

INSERT [dbo].[UM_PageMenu] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, N'ระบบงานบริการแนะนำและรับคำร้อง', 0, CAST(N'2019-12-10T16:47:45.9700000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[UM_PageMenu] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, N'ระบบงานสารบบและกำกับการจดทะเบียนเครื่องหมายการค้า', 0, CAST(N'2019-12-10T16:50:18.2933333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[UM_PageMenu] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, N'ระบบงานตรวจสอบเครื่องหมายการค้า', 0, CAST(N'2019-12-10T16:50:21.7800000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[UM_PageMenu] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, N'ระบบงานประกาศโฆษณา และหนังสือสำคัญ', 0, CAST(N'2020-01-24T09:25:02.4600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenu] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, N'ระบบงานกลุ่มคัดค้าน', 0, CAST(N'2020-06-17T23:17:15.5400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenu] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, N'ระบบงานกลุ่มอุทธรณ์และคดี', 0, CAST(N'2020-06-17T23:17:26.6500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenu] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, N'ระบบงานกลุ่มเปลี่ยนแปลงและต่ออายุ', 0, CAST(N'2020-06-17T23:17:30.9266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[UM_PageMenu] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, N'ระบบงานกลุ่มมาดริด', 0, CAST(N'2020-06-17T23:17:39.7133333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[UM_PageMenu] OFF
ALTER TABLE [dbo].[UM_PageMenu] ADD  CONSTRAINT [DF_UM_PageMenu_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[UM_PageMenu] ADD  CONSTRAINT [DF_UM_PageMenu_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[UM_PageMenu] ADD  CONSTRAINT [DF_UM_PageMenu_created_by]  DEFAULT ((0)) FOR [created_by]
GO
