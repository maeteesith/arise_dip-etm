USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_CheckingReceiveStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_CheckingReceiveStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_CheckingReceiveStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_CheckingReceiveStatusCode] ON 

INSERT [dbo].[RM_CheckingReceiveStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'CHANGE', N'รอการแก้ไข', 0, CAST(N'2020-04-30T05:34:49.4166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingReceiveStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'DONE', N'ส่งงานแล้ว', 0, CAST(N'2019-12-19T23:18:07.6633333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingReceiveStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'WAIT_CHANGE', N'รอแก้ไข', 0, CAST(N'2019-12-19T23:18:04.5966667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingReceiveStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'WAIT_DONE', N'รอดำเนินการ', 0, CAST(N'2019-12-19T23:17:55.4200000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_CheckingReceiveStatusCode] OFF
ALTER TABLE [dbo].[RM_CheckingReceiveStatusCode] ADD  CONSTRAINT [DF_RM_CheckingReceiveStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_CheckingReceiveStatusCode] ADD  CONSTRAINT [DF_RM_CheckingReceiveStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_CheckingReceiveStatusCode] ADD  CONSTRAINT [DF_RM_CheckingReceiveStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_CheckingReceiveStatusCode] ADD  CONSTRAINT [DF_RM_CheckingReceiveStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
