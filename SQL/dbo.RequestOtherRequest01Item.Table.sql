USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RequestOtherRequest01Item]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestOtherRequest01Item](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_other_process_id] [bigint] NOT NULL,
	[request_number] [nvarchar](50) NULL,
	[total_price] [decimal](18, 2) NULL,
	[reference_number] [nvarchar](50) NULL,
	[name] [nvarchar](1000) NULL,
	[facilitation_act_status_code] [nvarchar](50) NULL,
	[remark] [nvarchar](1000) NULL,
	[book_index] [nvarchar](50) NULL,
	[page_index] [nvarchar](50) NULL,
	[status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestOtherRequest01Item] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RequestOtherRequest01Item] ADD  CONSTRAINT [DF_RequestOtherRequest01Item_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RequestOtherRequest01Item] ADD  CONSTRAINT [DF_RequestOtherRequest01Item_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RequestOtherRequest01Item] ADD  CONSTRAINT [DF_RequestOtherRequest01Item_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RequestOtherRequest01Item]  WITH CHECK ADD  CONSTRAINT [FK_RequestOtherRequest01Item_RequestOtherProcess] FOREIGN KEY([request_other_process_id])
REFERENCES [dbo].[RequestOtherProcess] ([id])
GO
ALTER TABLE [dbo].[RequestOtherRequest01Item] CHECK CONSTRAINT [FK_RequestOtherRequest01Item_RequestOtherProcess]
GO
ALTER TABLE [dbo].[RequestOtherRequest01Item]  WITH CHECK ADD  CONSTRAINT [FK_RequestOtherRequest01Item_RM_RequestStatus] FOREIGN KEY([status_code])
REFERENCES [dbo].[RM_RequestStatus] ([code])
GO
ALTER TABLE [dbo].[RequestOtherRequest01Item] CHECK CONSTRAINT [FK_RequestOtherRequest01Item_RM_RequestStatus]
GO
