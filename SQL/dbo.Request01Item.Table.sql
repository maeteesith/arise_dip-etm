USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Request01Item]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Request01Item](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request01_id] [bigint] NOT NULL,
	[eform_id] [bigint] NULL,
	[item_index] [int] NULL,
	[item_type_code] [nvarchar](50) NULL,
	[income_type] [nvarchar](50) NULL,
	[income_description] [nvarchar](1000) NULL,
	[sound_file_id] [bigint] NULL,
	[sound_type_code] [nvarchar](50) NULL,
	[sound_description] [nvarchar](1000) NULL,
	[facilitation_act_status_code] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[size_over_cm] [decimal](18, 2) NULL,
	[size_over_price] [decimal](18, 2) NULL,
	[size_over_income_type] [nvarchar](250) NULL,
	[size_over_income_description] [nvarchar](1000) NULL,
	[request_evidence_list] [nvarchar](250) NULL,
	[total_price] [decimal](18, 2) NULL,
	[book_index] [nvarchar](50) NULL,
	[page_index] [nvarchar](50) NULL,
	[is_otop] [bit] NULL,
	[reference_number] [nvarchar](50) NULL,
	[cancel_reason] [nvarchar](500) NULL,
	[status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Request01Item] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Request01Item] ADD  CONSTRAINT [DF_Request01Item_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Request01Item] ADD  CONSTRAINT [DF_Request01Item_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Request01Item] ADD  CONSTRAINT [DF_Request01Item_created_by]  DEFAULT ((1)) FOR [created_by]
GO
ALTER TABLE [dbo].[Request01Item]  WITH NOCHECK ADD  CONSTRAINT [FK_Request01Item_File] FOREIGN KEY([sound_file_id])
REFERENCES [dbo].[File] ([id])
GO
ALTER TABLE [dbo].[Request01Item] NOCHECK CONSTRAINT [FK_Request01Item_File]
GO
ALTER TABLE [dbo].[Request01Item]  WITH NOCHECK ADD  CONSTRAINT [FK_Request01Item_Request01] FOREIGN KEY([request01_id])
REFERENCES [dbo].[Request01Process] ([id])
GO
ALTER TABLE [dbo].[Request01Item] NOCHECK CONSTRAINT [FK_Request01Item_Request01]
GO
ALTER TABLE [dbo].[Request01Item]  WITH CHECK ADD  CONSTRAINT [FK_Request01Item_RequestItemType] FOREIGN KEY([item_type_code])
REFERENCES [dbo].[RM_RequestItemType] ([code])
GO
ALTER TABLE [dbo].[Request01Item] CHECK CONSTRAINT [FK_Request01Item_RequestItemType]
GO
ALTER TABLE [dbo].[Request01Item]  WITH CHECK ADD  CONSTRAINT [FK_Request01Item_RequestSoundType] FOREIGN KEY([sound_type_code])
REFERENCES [dbo].[RM_RequestSoundType] ([code])
GO
ALTER TABLE [dbo].[Request01Item] CHECK CONSTRAINT [FK_Request01Item_RequestSoundType]
GO
ALTER TABLE [dbo].[Request01Item]  WITH CHECK ADD  CONSTRAINT [FK_Request01Item_RM_FacilitationActStatus] FOREIGN KEY([facilitation_act_status_code])
REFERENCES [dbo].[RM_FacilitationActStatus] ([code])
GO
ALTER TABLE [dbo].[Request01Item] CHECK CONSTRAINT [FK_Request01Item_RM_FacilitationActStatus]
GO
ALTER TABLE [dbo].[Request01Item]  WITH NOCHECK ADD  CONSTRAINT [FK_Request01Item_UM_User] FOREIGN KEY([created_by])
REFERENCES [dbo].[UM_User] ([id])
GO
ALTER TABLE [dbo].[Request01Item] NOCHECK CONSTRAINT [FK_Request01Item_UM_User]
GO
