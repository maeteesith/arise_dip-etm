USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[PublicRoundItem]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PublicRoundItem](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[public_round_id] [bigint] NULL,
	[line_index] [bigint] NULL,
	[request_number] [nvarchar](50) NULL,
	[request_item_type_code] [nvarchar](50) NULL,
	[sound_mark_list] [nvarchar](1000) NULL,
	[sound_mark_file_id] [bigint] NULL,
	[trademark_2d_file_id] [bigint] NULL,
	[remark] [nvarchar](1000) NULL,
	[save_id] [bigint] NULL,
	[request_item_sub_type_1_code] [nvarchar](50) NULL,
	[request_item_sub_status_code] [nvarchar](50) NULL,
	[cancel_reason] [nvarchar](500) NULL,
	[cancel_by] [bigint] NULL,
	[cancel_by_name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_PublicRoundItem] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PublicRoundItem] ADD  CONSTRAINT [DF_PublicRoundItem_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[PublicRoundItem] ADD  CONSTRAINT [DF_PublicRoundItem_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[PublicRoundItem] ADD  CONSTRAINT [DF_PublicRoundItem_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[PublicRoundItem]  WITH CHECK ADD  CONSTRAINT [FK_PublicRoundItem_PublicRound] FOREIGN KEY([public_round_id])
REFERENCES [dbo].[PublicRound] ([id])
GO
ALTER TABLE [dbo].[PublicRoundItem] CHECK CONSTRAINT [FK_PublicRoundItem_PublicRound]
GO
