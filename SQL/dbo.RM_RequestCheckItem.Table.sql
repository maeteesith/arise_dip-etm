USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestCheckItem]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestCheckItem](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[value_4] [nvarchar](1000) NULL,
	[value_5] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_RequestCheckItem] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestCheckItem] ON 

INSERT [dbo].[RM_RequestCheckItem] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'310', N'2.1 ขอตรวจดูทะเบียนหรือสารบบ', N'จำนวนชั่วโมง', N'200', NULL, NULL, NULL, 0, CAST(N'2020-02-10T13:08:33.7233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestCheckItem] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'320', N'2.2 ขอสำเนาทะเบียนพร้อมคำรับรอง (ทะเบียนแบบ)', N'จำนวนฉบับ', N'400', NULL, NULL, N'M', 0, CAST(N'2020-02-10T13:09:01.3733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestCheckItem] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'330', N'2.3 ขอคัดสำเนาเอกสาร (ปริ้นจากคอมฯ)', N'จำนวนหน้า', N'20', NULL, NULL, N'M', 0, CAST(N'2020-02-10T13:09:02.5500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestCheckItem] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'340', N'2.4 ขอให้รับรองสำเนาเอกสารเรื่องเดียวกัน', N'จำนวนหน้า/แฟ้ม', N'20', NULL, NULL, NULL, 0, CAST(N'2020-02-10T13:09:03.5666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestCheckItem] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 5, N'350', N'2.5 ขอคำรับรองจากนายทะเบียนเกี่ยวกับรายการการจดทะเบียน', N'จำนวนฉบับ', N'100', N'is_englist', N'ภาษาอังกฤษ', N'M', 0, CAST(N'2020-02-10T13:09:04.5333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestCheckItem] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 6, N'360', N'2.6 ขอใบแทนหนังสือสำคัญแสดงการจดทะเบียน', N'จำนวนฉบับ', N'200', NULL, NULL, N'M', 0, CAST(N'2020-02-10T13:09:05.5800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestCheckItem] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 8, N'370', N'2.8 อื่นๆ', N'คำขอ', N'200', NULL, NULL, N'O', 0, CAST(N'2020-02-10T13:09:14.9366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestCheckItem] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 7, N'380', N'2.7 ขอตรวจดูแฟ้มคำขอจดทะเบียน', N'จำนวนแฟ้ม', N'200', NULL, NULL, N'M', 0, CAST(N'2020-02-10T13:09:06.8866667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_RequestCheckItem] OFF
ALTER TABLE [dbo].[RM_RequestCheckItem] ADD  CONSTRAINT [DF_RM_RequestCheckItem_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RequestCheckItem] ADD  CONSTRAINT [DF_RM_RequestCheckItem_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_RequestCheckItem] ADD  CONSTRAINT [DF_RM_RequestCheckItem_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_RequestCheckItem] ADD  CONSTRAINT [DF_RM_RequestCheckItem_created_by]  DEFAULT ((0)) FOR [created_by]
GO
