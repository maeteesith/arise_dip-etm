USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save080RevokeTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save080RevokeTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save080RevokeTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save080RevokeTypeCode] ON 

INSERT [dbo].[RM_Save080RevokeTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'TYPE1', N'เพิกถอนเครื่องหมายตนเอง', 0, CAST(N'2019-12-19T00:52:22.2200000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save080RevokeTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 2, N'TYPE2', N'เพิกถอนเครื่องหมายผู้อื่น', 0, CAST(N'2019-12-19T00:52:24.6966667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_Save080RevokeTypeCode] OFF
ALTER TABLE [dbo].[RM_Save080RevokeTypeCode] ADD  CONSTRAINT [DF_RM_Save080RevokeTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save080RevokeTypeCode] ADD  CONSTRAINT [DF_RM_Save080RevokeTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save080RevokeTypeCode] ADD  CONSTRAINT [DF_RM_Save080RevokeTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
