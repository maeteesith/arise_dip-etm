USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentRole04ReleaseRequestGroup]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vDocumentRole04ReleaseRequestGroup] as

select 
d.* ,
rm_dr4r.code document_role04_receive_status_code,
rm_dr4r.name document_role04_receive_status_name,
rm_dr5r.code document_role05_receive_status_code,
rm_dr5r.name document_role05_receive_status_name
from (
select
d.save_tag_id id,
d.document_role04_release_request_type_code,
max(d.document_role04_release_request_type_name) document_role04_release_request_type_name,

max(d.name) name,
min(d.save_tag_request_number) request_number,

max(d.considering_receiver_by_name) considering_receiver_by_name,
max(d.document_role04_receive_send_date) document_role04_receive_send_date,
max(d.document_role05_receive_send_date) document_role05_receive_send_date,

max(d.document_role04_receiver_by_name) document_role04_receiver_by_name,
max(d.document_role05_receiver_by_name) document_role05_receiver_by_name,

min(d.document_role04_status_code) document_role04_status_code,
min(d.document_role04_status_name) document_role04_status_name,
min(d.document_role04_receive_status_index) document_role04_receive_status_index,
min(d.document_role05_status_code) document_role05_status_code,
min(d.document_role05_status_name) document_role05_status_name,
min(d.document_role05_receive_status_index) document_role05_receive_status_index,

cast(min(cast(d.is_deleted as int)) as bit) is_deleted,
min(d.created_by) created_by,
min(d.created_date) created_date,
min(d.updated_by) updated_by,
min(d.updated_date) updated_date

from vDocumentRole04ReleaseRequest d
group by 
d.save_tag_id,
d.document_role04_release_request_type_code
) d
left join RM_DocumentRole04ReceiveStatusCode rm_dr4r on rm_dr4r.[index] = d.document_role04_receive_status_index
left join RM_DocumentRole05ReceiveStatusCode rm_dr5r on rm_dr5r.[index] = d.document_role05_receive_status_index
GO
