USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestEvidenceType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestEvidenceType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_EvidenceType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestEvidenceType] ON 

INSERT [dbo].[RM_RequestEvidenceType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'A10', N'ก.10', 0, NULL, 1, NULL, 1)
INSERT [dbo].[RM_RequestEvidenceType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'A12', N'ก.12', 0, NULL, 1, NULL, 1)
INSERT [dbo].[RM_RequestEvidenceType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'A13', N'ก.13', 0, NULL, 1, NULL, 1)
INSERT [dbo].[RM_RequestEvidenceType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'A18', N'ก.18', 0, NULL, 1, NULL, 1)
INSERT [dbo].[RM_RequestEvidenceType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'A19', N'ก.19', 0, NULL, 1, NULL, 1)
INSERT [dbo].[RM_RequestEvidenceType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'A20', N'ก.20', 0, NULL, 1, NULL, 1)
INSERT [dbo].[RM_RequestEvidenceType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 1, N'OTHER_ATTACHMENT', N'หลักฐานอื่นๆที่แนบกับคำขอ', 0, NULL, 1, NULL, 1)
INSERT [dbo].[RM_RequestEvidenceType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'OTHER_FLOOR7', N'หลักฐานอื่นๆ (ชั้น 7)', 0, NULL, 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_RequestEvidenceType] OFF
ALTER TABLE [dbo].[RM_RequestEvidenceType] ADD  CONSTRAINT [DF_RM_RequestEvidenceType_index]  DEFAULT ((1)) FOR [index]
GO
