USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ConsideringReceiveStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ConsideringReceiveStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ConsiderReceiveStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ConsideringReceiveStatusCode] ON 

INSERT [dbo].[RM_ConsideringReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'DRAFT', N'รอการตรวจสอบ', 1, 0, CAST(N'2020-05-14T13:07:14.6733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 4, N'DRAFT_CHANGE', N'รอการตรวจสอบจากแก้ไข', 1, 0, CAST(N'2020-05-14T13:07:17.0400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 2, N'DRAFT_FIX', N'รอการตรวจสอบอีกครั้ง', 1, 0, CAST(N'2020-05-14T13:07:15.3966667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 10, N'SEND', N'เสร็จสิ้น', 1, 0, CAST(N'2020-05-14T13:07:17.7500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, 12, N'SEND_CHANGE', N'เสร็จสิ้น', 1, 0, CAST(N'2020-05-14T19:14:44.0866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 11, N'SEND_FIX', N'เสร็จสิ้น', 0, 0, CAST(N'2020-05-14T13:07:18.4566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringReceiveStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 3, N'WAIT_CHANGE', N'รอแก้ไข', 1, 0, CAST(N'2020-05-14T13:07:16.1433333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_ConsideringReceiveStatusCode] OFF
ALTER TABLE [dbo].[RM_ConsideringReceiveStatusCode] ADD  CONSTRAINT [DF_RM_ConsiderReceiveStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ConsideringReceiveStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringReceiveStatusCode_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_ConsideringReceiveStatusCode] ADD  CONSTRAINT [DF_RM_ConsiderReceiveStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ConsideringReceiveStatusCode] ADD  CONSTRAINT [DF_RM_ConsiderReceiveStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ConsideringReceiveStatusCode] ADD  CONSTRAINT [DF_RM_ConsiderReceiveStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
