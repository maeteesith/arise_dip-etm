USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save040TransferPart]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save040TransferPart](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save040TransferPart] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save040TransferPart] ON 

INSERT [dbo].[RM_Save040TransferPart] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'TRADEMARK_ONLY', N'เครื่องหมายแต่เพียงอย่างเดียว', 0, CAST(N'2020-01-26T20:23:46.4600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save040TransferPart] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 2, N'TRADEMARK_OTHER', N'เครื่องหมาย พร้อมกับกิจการที่เกี่ยวข้องกัน', 0, CAST(N'2020-01-26T20:24:00.8100000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save040TransferPart] OFF
ALTER TABLE [dbo].[RM_Save040TransferPart] ADD  CONSTRAINT [DF_Save040TransferPart_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save040TransferPart] ADD  CONSTRAINT [DF_Save040TransferPart_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save040TransferPart] ADD  CONSTRAINT [DF_Save040TransferPart_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save040TransferPart] ADD  CONSTRAINT [DF_Save040TransferPart_created_by]  DEFAULT ((0)) FOR [created_by]
GO
