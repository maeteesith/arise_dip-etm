USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RepresentativeType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RepresentativeType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RepresentativeType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RepresentativeType] ON 

INSERT [dbo].[RM_RepresentativeType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'REPRESENTATIVE', N'ตัวแทน', NULL, NULL, NULL, 1, 0, CAST(N'2020-04-09T03:07:40.2233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RepresentativeType] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'REPRESENTATIVE_PERIOD', N'ตัวแทนช่วง', NULL, NULL, NULL, 1, 0, CAST(N'2020-04-09T03:07:47.8000000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_RepresentativeType] OFF
ALTER TABLE [dbo].[RM_RepresentativeType] ADD  CONSTRAINT [DF_RM_RepresentativeType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RepresentativeType] ADD  CONSTRAINT [DF_RM_RepresentativeType_is_deleted1]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_RepresentativeType] ADD  CONSTRAINT [DF_RepresentativeType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_RepresentativeType] ADD  CONSTRAINT [DF_RepresentativeType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_RepresentativeType] ADD  CONSTRAINT [DF_RepresentativeType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
