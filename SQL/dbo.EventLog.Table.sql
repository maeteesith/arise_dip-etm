USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[EventLog]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EventLog](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[eventlog_type_code] [nvarchar](50) NULL,
	[controller_name] [nvarchar](1000) NULL,
	[method_name] [nvarchar](1000) NULL,
	[token] [nvarchar](50) NULL,
	[request] [nvarchar](max) NULL,
	[response] [nvarchar](max) NULL,
	[from_ip_address] [nvarchar](50) NULL,
	[to_url] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_EventLog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[EventLog] ADD  CONSTRAINT [DF_EventLog_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[EventLog] ADD  CONSTRAINT [DF_EventLog_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[EventLog] ADD  CONSTRAINT [DF_EventLog_created_by]  DEFAULT ((0)) FOR [created_by]
GO
