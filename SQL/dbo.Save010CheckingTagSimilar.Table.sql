USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010CheckingTagSimilar]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010CheckingTagSimilar](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NOT NULL,
	[save_tag_id] [bigint] NOT NULL,
	[is_same] [bit] NOT NULL,
	[is_like_approve] [bit] NOT NULL,
	[is_same_approve] [bit] NOT NULL,
	[method_1] [bit] NULL,
	[method_2] [bit] NULL,
	[method_3] [bit] NULL,
	[method_4] [bit] NULL,
	[method_5] [bit] NULL,
	[method_6] [bit] NULL,
	[method_7] [bit] NULL,
	[is_owner_same] [bit] NOT NULL,
	[is_created_instruction_rule] [bit] NOT NULL,
	[instruction_rule_code] [nvarchar](50) NULL,
	[instruction_rule_value_1] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010CheckingTagSimilar] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Table_1_request_id]  DEFAULT ((0)) FOR [save_id]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Save010CheckingTagSimilar_is_same]  DEFAULT ((1)) FOR [is_same]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Save010CheckingTagSimilar_is_same_approve1]  DEFAULT ((0)) FOR [is_like_approve]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Save010CheckingTagSimilar_is_same1]  DEFAULT ((0)) FOR [is_same_approve]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Save010CheckingTagSimilar_is_deleted1]  DEFAULT ((0)) FOR [is_owner_same]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Save010CheckingTagSimilar_is_show]  DEFAULT ((0)) FOR [is_created_instruction_rule]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Save010CheckingTagSimilar_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Save010CheckingTagSimilar_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010CheckingTagSimilar] ADD  CONSTRAINT [DF_Save010CheckingTagSimilar_created_by]  DEFAULT ((0)) FOR [created_by]
GO
