USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010RequestGroup]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010RequestGroup](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[request_number] [nvarchar](50) NULL,
	[save010_registration_group_status_code] [nvarchar](50) NULL,
	[make_date] [datetime2](7) NULL,
	[allow_date] [datetime2](7) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010RequestGroup] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010RequestGroup] ADD  CONSTRAINT [DF_Save010RequestGroup_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010RequestGroup] ADD  CONSTRAINT [DF_Save010RequestGroup_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010RequestGroup] ADD  CONSTRAINT [DF_Save010RequestGroup_created_by]  DEFAULT ((0)) FOR [created_by]
GO
