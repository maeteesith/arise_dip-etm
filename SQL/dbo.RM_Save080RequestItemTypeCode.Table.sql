USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save080RequestItemTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save080RequestItemTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save080RequestItemTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save080RequestItemTypeCode] ON 

INSERT [dbo].[RM_Save080RequestItemTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE1', N'เครื่องหมายการค้า/บริการ/รับรอง/ร่วม', 0, CAST(N'2020-02-28T12:44:05.7466667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_Save080RequestItemTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'TYPE2', N'สัญญาอณุญาตให้ใช้เครื่องหมายการค้า/บริการ', 0, CAST(N'2020-02-28T12:44:08.2500000' AS DateTime2), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save080RequestItemTypeCode] OFF
ALTER TABLE [dbo].[RM_Save080RequestItemTypeCode] ADD  CONSTRAINT [DF_RM_Save080RequestItemTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save080RequestItemTypeCode] ADD  CONSTRAINT [DF_RM_Save080RequestItemTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save080RequestItemTypeCode] ADD  CONSTRAINT [DF_RM_Save080RequestItemTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
