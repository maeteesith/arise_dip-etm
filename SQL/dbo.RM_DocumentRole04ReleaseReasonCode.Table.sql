USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole04ReleaseReasonCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole04ReleaseReasonCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[description] [nvarchar](1000) NULL,
	[value_01] [nvarchar](1000) NULL,
	[value_02] [nvarchar](1000) NULL,
	[value_03] [nvarchar](1000) NULL,
	[value_04] [nvarchar](1000) NULL,
	[value_05] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole04ReleaseReasonCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ON 

INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (135, 1, N'TYPE_01', N'เสนอ D ทั่วไป', N'ครบกำหนดเวลาตาม มาตรา/ตค. ....  ลว. ..... ตอบรับ ...... ผู้ขอมิได้ดำเนินการแต่อย่างใดได้ตรวจสอบคำร้องแล้วไม่ปรากฎว่ามีคำร้องหรือหนังสือชี้แจงใดๆ
เห็นควรจำหน่ายคำขอนี้ตามมาตรา 19', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9800000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (136, 1, N'TYPE_02', N'เสนอ D ขอถอน', N'เห็นควรจำหน่ายคำขอรายนี้ตามหนังสือขอถอนต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9800000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (137, 1, N'TYPE_03', N'เสนอ D ตามคำวินิจฉัยฯ', N'เห็นควรจำหน่ายคำขอรายนี้ตามคำวินิจฉัยคณะกรรมการฯ ต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9800000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (138, 1, N'TYPE_04', N'เสนอ D ไม่ปฏิบัติตามคำวินิจฉัยฯ', N'ผู้ขอได้ยื่นอุทธรณ์คำสั่งนายทะเบียนตามมาตรา ...  ต่อมาคณะกรรมการฯ ได้มีคำวินิจฉัยที่ .....  โดยมีมติยืนตามคำสั่งนายทะเบียน/ให้แก้ไขคำสั่งนายทะเบียน และได้ออกคำสั่งให้รับจดทะเบียนแบบมีเงื่อนไขตาม ตค.7(3) ลว. ..... ตอบรับ ...... เมื่อครบกำหนดเวลาไม่ปรากฎว่าผู้ขอได้ปฏิบัติตามคำสั่งหรือมีหนังสือชี้แจงแต่อย่างใด 
เห็นควรจำหน่ายคำขอรายนี้ตามมาตรา 19', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9800000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (139, 1, N'TYPE_05', N'เสนอ D ตามคำพิพากษา', N'เห็นควรจำหน่ายคำขอรายนี้ตามคำพิพากษาศาลต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9800000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (140, 1, N'TYPE_06', N'ตีกลับ/ใบตอบรับไม่กลับ', N'ด้วยคำขอรายนี้การส่งจดหมายตาม มาตรา/ตค. ..... ลว. .....  .... เห็นควรส่งไปอีกครั้งตามที่อยู่ในแบบ...  ก่อนดำเนินการต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9800000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (141, 1, N'TYPE_07', N'ส่งผิด', N'ด้วยการส่งจดหมายตาม มาตรา/ตค. .... ลว. .... สถานที่ส่งบัตรหมายผิดไป  เห็นควรส่งคำสั่งดังกล่าวอีกครั้ง โดยส่งตามที่อยู่….', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9800000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (142, 1, N'TYPE_08', N'คู่ D', N'ด้วยคำขอ ..... ซึ่งเป็นคู่กรณีกับคำขอรายนี้ตาม มาตรา/ตค. .... ได้ถูกจำหน่ายแล้วตาม......     
จึงเรียนมาเพื่อโปรดพิจารณาสั่งการต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (143, 1, N'TYPE_09', N'คู่ R', N'ด้วยคำขอ......  ซึ่งเป็นคู่กรณีกับคำขอรายนี้ตาม มาตรา/ตค. .... ได้รับการจดทะเบียนแล้ว
จึงเรียนมาเพื่อโปรดพิจารณาสั่งการต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (144, 1, N'TYPE_10', N'คู่โอน', N'ด้วยคำขอ ..... ซึ่งเป็นคู่กรณีกับคำขอรายนี้ตาม มาตรา/ตค. ..... ได้โอนเป็นเจ้าของเดียวกันกับคำขอรายนี้แล้ว
จึงเรียนมาเพื่อโปรดพิจารณาสั่งการต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (145, 1, N'TYPE_11', N'ตัวโอน', N'เนื่องจากคำขอรายนี้ได้โอนเป็นเจ้าของเดียวกันกับคำขอ .....  แล้ว', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (146, 1, N'TYPE_12', N'ให้ส่งหลักฐาน/ชี้แจง', N'ด้วยนายทะเบียนได้มีคำสั่งตาม ตค. 9(10)/มาตรา... ลว. ..... ตอบรับ .....ว่าให้ผู้ขอส่งหลักฐาน/ชี้แจง ภายใน 60 วัน  ได้ตรวจสอบแล้วไม่ปรากฎว่าผู้ขอได้ปฏิบัติตามคำสั่งแต่อย่างใด
จึงเรียนมาเพื่อโปรดพิจารณาสั่งการต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (147, 1, N'TYPE_13', N'ปลด ม. 20', N'ด้วยคำขอ .... ได้ปฏิบัติตามคำสั่งนายทะเบียนครบถ้วนแล้ว
จึงเรียนมาเพื่อโปรดพิจารณาปลดปล่อยมาตรา 20 ต่อไป
(คำขอแรก/คำขอหลัง)', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (148, 1, N'TYPE_14', N'คู่คัดค้าน/โต้แย้ง/อุทธรณ์/ฟ้องคดี', N'ด้วยคำขอ .... ซึ่งเป็นคู่กรณีกับคำขอรายนี้ตามมาตรา.... ได้ยื่น ...  ลว. ....     
จึงเรียนมาเพื่อโปรดรอผล.....   ก่อนดำเนินการกับคำขอรายนี้ต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (149, 1, N'TYPE_15', N'คำวินิจฉัยฯหลุดพ้น', N'ด้วยคำขอรายนี้หลุดพ้นคำสั่ง มาตรา .....  ตามคำวินิจฉัยคณะกรรมการฯ ที่ ..... ', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (150, 1, N'TYPE_16', N'คำวินิจฉัยฯ หลุดพ้น แบบมีเงื่อนไข (ปฏิบัติ)', N'- ด้วยคำขอรายนี้หลุดพ้นคำสั่ง มาตรา .... ตามคำวินิจฉัยคณะกรรมการฯ ที่ .... โดยคณะกรรมการฯได้พิจารณาแล้วมีมติให้ยืนตามคำสั่งนายทะเบียน/ให้แก้ไขคำสั่งนายทะเบียน และได้ออกคำสั่งให้รับจดทะเบียนแบบมีเงื่อนไข ตามมาตรา ....  ตค.7(3) ลว......  ตอบรับ  ....... 
- เห็นควร ....', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (151, 1, N'TYPE_17', N'ยกอุทธรณ์', N'ด้วยคำขอรายนี้หลุดพ้นคำสั่งตามมาตรา 13  ตามคำวินิจฉัยคณะกรรมการฯ ที่  .... โดยคณะกรรมการฯ ได้มีคำสั่งให้ยกอุทธรณ์ เนื่องจากไม่มีประเด็นเหมือนคล้ายเพราะคู่กรณีได้ถูกจำหน่ายแล้วตามมาตรา 56 / แก้ไขชื่อเป็นเจ้าของเดียวกันแล้ว/นายทะเบียนได้มีคำสั่งหลุดพ้น', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (152, 1, N'TYPE_18', N'ไม่พิจารณาอุทธรณ์', N'คณะกรรมการฯ ได้มีคำสั่งไม่พิจารณาอุทธรณ์เนื่องจาก
- คู่กรณีตาม ต.ค.2 ได้ถูกจำหน่ายแล้วตามมาตรา 56
- มีการแก้ไขชื่อเป็นเจ้าของเดียวกันแล้ว
- ผู้ขอได้มีหนังสือขอถอนอุทธรณ์ ลว….
- นายทะเบียนได้มีคำสั่งหลุดพ้น', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (153, 1, N'TYPE_19', N'คำวินิจฉัยฯ ให้รอผล ม.20', N'ด้วยผู้ขอรายนี้ได้ยื่นอุทธรณ์คำสั่งตามมาตรา 20 กับคำขอ .... ต่อมาคณะกรรมการฯ ได้พิจารณาอุทธรณ์แล้วเห็นว่าเครื่องหมายของทั้งสองฝ่ายมีลักษณะเหมือนหรือคล้ายกัน จึงมีมติยืนตามคำสั่งนายทะเบียน  รายละเอียดตามคำวินิจฉัยคณะกรรมการฯ ที่…..
(รอดำเนินการตามขั้นตอนมาตรา 20 ต่อไป)', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (154, 1, N'TYPE_20', N'ตค.6(3) ไป ตค. 5(3)', N'ครบกำหนดเวลาตาม ตค. 6(3) ลว. .... ตอบรับ .... ผู้คัดค้านมิได้ดำเนินการแต่อย่างใดและได้ตรวจสอบคำร้องแล้วไม่ปรากฎว่ามีคำร้องหรือหนังสือชี้แจงใดๆ
จึงเห็นควรรับจดทะเบียนคำขอรายนี้ต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (155, 1, N'TYPE_21', N'ให้ส่งคำฟ้องใหม่', N'เห็นควรออก ตค.9(10) เพื่อแจ้งให้ส่งสำเนาคำฟ้องใหม่ ดังนี้
ตามที่ท่านได้ยื่นแบบ ก.14  ลว. ... เพื่อนำส่งสำเนาคำฟ้อง นั้น นายทะเบียนได้พิจารณาแล้วเห็นว่า สำเนาคำฟ้องดังกล่าวไม่ปรากฏการรับรองจากเจ้าหน้าที่ของศาล จึงขอให้ท่านนำส่งสำเนาคำฟ้องใหม่ โดยให้ท่านนำส่งสำเนาคำฟ้อง ฉบับรับรองสำเนาถูกต้องโดยเจ้าหน้าที่ของศาลก่อนจะได้มีคำสั่งรอผลคดีต่อไป (ถ้ามีหลายคำขอให้ส่งต้นฉบับที่รับรองจากศาล 1 คำขอ ส่วนคำขออื่นๆ ให้ส่งสำเนา และรับรองสำเนาถูกต้องพร้อมระบุว่าต้นฉบับอยู่ที่คำขอใด)', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (156, 1, N'TYPE_22', N'รอคดี', N'เห็นควรออก ตค.9(10) เพื่อแจ้งให้หยุดรอคดี ดังนี้
เนื่องจากมีการยื่นฟ้องคดีต่อศาลทรัพย์สินทางปัญญาฯ คดีหมายเลขดำที่ ทป.  ระหว่าง......โจทก์ ,  .......จำเลย  จึงให้หยุดการดำเนินการใดๆ เกี่ยวกับคำขอรายนี้ไว้ก่อน ตามระเบียบกรมทรัพย์สินทางปัญญา ว่าด้วยการระงับการดำเนินการจดทะเบียนเครื่องหมายการค้าชั่วคราว ลงวันที่ 12 ก.พ. 2558
หากท่านเป็นโจทก์หรือจำเลยในคดีและท่านได้ทราบผลความคืบหน้าของคดีแล้ว  ขอให้ท่านแจ้งผลความคืบหน้าดังกล่าว เข้ามาในคำขอรายนี้เพื่อทราบและจะได้ดำเนินการต่อไปด้วย', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (157, 1, N'TYPE_23', N'ให้ส่งผลคดี/หนังสือรับรองคดีถึงที่สุด', N'เห็นควรออก ตค.9(10) เพื่อแจ้งให้ส่งคำพิพากษาถึงที่สุด ดังนี้
ขอให้ท่านนำส่งต้นฉบับหนังสือรับรองว่าคดีถึงที่สุดจากศาล ตามคดีหมายเลขดำที่ ทป. .... หรือรายงานความคืบหน้าของคดีเพื่อจะได้ดำเนินการกับคำขอรายนี้ต่อไป', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (158, 1, N'TYPE_24', N'พิจารณาคำพิพากษา', N'', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (159, 1, N'TYPE_25', N'เสนอฟื้นคำขอ', N'เห็นควรเสนอผู้อำนวยการกองสำนักเครื่องหมายการค้าเพื่ออนุมัติให้ฟื้นคำขอ 
(รายละเอียดปรากฏตามเอกสารประกอบ)', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] ([id], [index], [code], [name], [description], [value_01], [value_02], [value_03], [value_04], [value_05], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (160, 1, N'TYPE_26', N'อื่นๆ', N'', NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T20:02:27.9966667' AS DateTime2), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04ReleaseReasonCode] OFF
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseReasonCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseReasonCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseReasonCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseReasonCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseReasonCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseReasonCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseReasonCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseReasonCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
