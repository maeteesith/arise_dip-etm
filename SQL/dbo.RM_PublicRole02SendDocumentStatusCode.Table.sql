USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PublicRole02SendDocumentStatusCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PublicRole02SendDocumentStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PublicRole02SendDocumentStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PublicRole02SendDocumentStatusCode] ON 

INSERT [dbo].[RM_PublicRole02SendDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'DONE', N'เสร็จสิ้น', 0, CAST(N'2020-01-18T08:34:49.9200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole02SendDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'WAIT_1', N'รอออกหนังสือ 1', 0, CAST(N'2020-01-18T08:34:53.2233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole02SendDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'WAIT_2', N'รอออกหนังสือ 2', 0, CAST(N'2020-01-18T08:34:56.1666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole02SendDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'WAIT_PAYMENT', N'รอชำระค่าทำเนียม', 0, CAST(N'2020-01-18T08:34:59.3900000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PublicRole02SendDocumentStatusCode] OFF
ALTER TABLE [dbo].[RM_PublicRole02SendDocumentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02SendDocumentStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PublicRole02SendDocumentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02SendDocumentStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PublicRole02SendDocumentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02SendDocumentStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PublicRole02SendDocumentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02SendDocumentStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PublicRole02SendDocumentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02SendDocumentStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
