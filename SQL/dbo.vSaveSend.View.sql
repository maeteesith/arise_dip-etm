USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vSaveSend]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vSaveSend] as
select
--ri.*,
--*, 
so.id,

so.request_date,
so.make_date,
so.request_number,
so.department_send_date,

rdc.file_id,

so.request_type_code,
rm_rt.name request_type_name,
so.save_status_code,
rm_ss.name save_status_name,
so.request_source_code,
rm_rs.name request_source_name,
so.department_send_code,
rm_d.name department_send_name,

so.evidence_address,

so.is_deleted,
so.created_by,
so.created_date,
so.updated_by,
so.updated_date

from SaveOther so
join RM_RequestType rm_rt on rm_rt.code = so.request_type_code
join RM_SaveStatus rm_ss on rm_ss.code = so.save_status_code
left join RM_RequestSource rm_rs on rm_rs.code = so.request_source_code
left join RM_Department rm_d on rm_d.code = so.department_send_code

join ReceiptItem ri on ri.request_id = so.request_id and ri.request_number = so.request_number and ri.request_type_code = so.request_type_code
join Receipt r on r.id = ri.receipt_id
join RequestOtherRequest01Item roi on roi.reference_number = r.reference_number

left join RequestDocumentCollect rdc on rdc.receipt_item_id = ri.id and rdc.request_document_collect_status_code = 'ADD'


--where so.request_number = 200100533 and so.request_type_code = 61
GO
