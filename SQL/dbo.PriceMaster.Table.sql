USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[PriceMaster]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PriceMaster](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[price_type_id] [int] NOT NULL,
	[price_channel_id] [int] NOT NULL,
	[min_qty] [int] NOT NULL,
	[max_qty] [int] NULL,
	[is_total_price] [bit] NOT NULL,
	[price] [decimal](18, 2) NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_PriceMaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PriceMaster] ON 

INSERT [dbo].[PriceMaster] ([id], [price_type_id], [price_channel_id], [min_qty], [max_qty], [is_total_price], [price], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, 1, 1, 5, 0, CAST(1000.00 AS Decimal(18, 2)), 0, CAST(N'2019-12-07T16:38:53.6866667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[PriceMaster] ([id], [price_type_id], [price_channel_id], [min_qty], [max_qty], [is_total_price], [price], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, 2, 6, 0, 1, CAST(9000.00 AS Decimal(18, 2)), 0, CAST(N'2019-12-07T16:39:10.8466667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[PriceMaster] OFF
ALTER TABLE [dbo].[PriceMaster] ADD  CONSTRAINT [DF_PriceMaster_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[PriceMaster] ADD  CONSTRAINT [DF_PriceMaster_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[PriceMaster] ADD  CONSTRAINT [DF_PriceMaster_created_by]  DEFAULT ((0)) FOR [created_by]
GO
