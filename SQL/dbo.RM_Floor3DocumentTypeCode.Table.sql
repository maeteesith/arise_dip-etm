USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Floor3DocumentTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Floor3DocumentTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Floor3DocumentTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Floor3DocumentTypeCode] ON 

INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE_01', N'ทะเบียนแบบ', 0, CAST(N'2020-02-12T17:04:38.1400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'TYPE_02', N'หนังสือสำคัญแสดงการรับจดทะเบียน', 0, CAST(N'2020-02-12T17:04:47.8866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'TYPE_03', N'หนังสือสำคัญแสดงการรับจดทะเบียน(ใบแทน)', 0, CAST(N'2020-02-12T17:04:51.8900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'TYPE_04', N'หนังสือรับรองการจดทะเบียนภาษาอังกฤษ (ก่อนจดทะเบียน)', 1, CAST(N'2020-02-12T17:04:57.2366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'TYPE_05', N'หนังสือรับรองการจดทะเบียนภาษาอังกฤษ (หลังจดทะเบียน)', 1, CAST(N'2020-02-12T17:04:58.1733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'TYPE_06', N'ประวัติการต่ออายุทะเบียนเครื่องหมาย', 1, CAST(N'2020-02-12T17:04:59.0533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'TYPE_07', N'รายชื่อผู้ร่วมใช้รายการจดทะเบียน (ด้านหลัง)', 0, CAST(N'2020-02-12T17:05:00.0300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 1, N'TYPE_08', N'รายละเอียดคู่มือรับจดทะเบียน (รายการสินค้า)', 1, CAST(N'2020-02-12T17:05:01.0866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 1, N'TYPE_09', N'รายละเอียดคู่มือรับจดทะเบียน (รายการสินค้า) แสดงเลขหน้า', 0, CAST(N'2020-02-12T17:05:03.3133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1, N'TYPE_10', N'รายชื่อเจ้าของมากกว่า 2 คน สำหรับงานรับจดทะเบียน', 0, CAST(N'2020-02-12T17:05:05.6866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Floor3DocumentTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 1, N'TYPE_11', N'ใบต่อคำบรรยายเครื่องหมายการค้า', 0, CAST(N'2020-02-12T17:05:10.7833333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Floor3DocumentTypeCode] OFF
ALTER TABLE [dbo].[RM_Floor3DocumentTypeCode] ADD  CONSTRAINT [DF_RM_Floor3DocumentTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Floor3DocumentTypeCode] ADD  CONSTRAINT [DF_RM_Floor3DocumentTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_Floor3DocumentTypeCode] ADD  CONSTRAINT [DF_RM_Floor3DocumentTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Floor3DocumentTypeCode] ADD  CONSTRAINT [DF_RM_Floor3DocumentTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Floor3DocumentTypeCode] ADD  CONSTRAINT [DF_RM_Floor3DocumentTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
