USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vSave010RequestGroup]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vSave010RequestGroup] as

select

sr.id,
sr.save_id,

s.request_number,
s.registration_number,
s.trademark_status_code,
s.name,

sr.save010_registration_group_status_code,
sr.make_date,
sr.allow_date,

sr.is_deleted,
sr.created_by,
sr.created_date,
sr.updated_by,
sr.updated_date

from Save010RequestGroup sr 
join vSave010 s on s.request_number = sr.request_number
GO
