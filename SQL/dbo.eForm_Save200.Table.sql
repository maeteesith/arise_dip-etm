USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save200]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save200](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_instruction] [bit] NULL,
	[save200_recipient_code] [nvarchar](50) NULL,
	[remark_5] [nvarchar](1000) NULL,
	[remark_6] [nvarchar](1000) NULL,
	[sign_inform_person_list] [nvarchar](500) NULL,
	[sign_inform_representative_list] [nvarchar](500) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[wizard] [nvarchar](300) NULL,
	[request_name] [nvarchar](200) NULL,
 CONSTRAINT [PK_eForm_Save200] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save200] ADD  CONSTRAINT [DF_eForm_Save200_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save200] ADD  CONSTRAINT [DF_eForm_Save200_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save200] ADD  CONSTRAINT [DF_eForm_Save200_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save200]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save200_RM_Save200ReceipientCode] FOREIGN KEY([save200_recipient_code])
REFERENCES [dbo].[RM_Save200ReceipientCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save200] CHECK CONSTRAINT [FK_eForm_Save200_RM_Save200ReceipientCode]
GO
