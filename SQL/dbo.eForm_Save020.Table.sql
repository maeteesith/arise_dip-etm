USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save020]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save020](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save020_contact_type_code] [nvarchar](50) NULL,
	[evidence_file_id] [bigint] NULL,
	[is_6_1] [bit] NULL,
	[is_6_2] [bit] NULL,
	[is_6_3] [bit] NULL,
	[is_6_4] [bit] NULL,
	[is_6_5] [bit] NULL,
	[is_6_6] [bit] NULL,
	[sign_inform_person_list] [nvarchar](300) NULL,
	[sign_inform_representative_list] [nvarchar](300) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[save020_representative_condition_type_code] [nvarchar](50) NULL,
	[wizard] [nvarchar](300) NULL,
	[rule_number] [nvarchar](50) NULL,
 CONSTRAINT [PK_eForm_Save020] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save020] ADD  CONSTRAINT [DF_eForm_Save020_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save020] ADD  CONSTRAINT [DF_eForm_Save020_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save020] ADD  CONSTRAINT [DF_eForm_Save020_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save020]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save020_eForm_Save020] FOREIGN KEY([id])
REFERENCES [dbo].[eForm_Save020] ([id])
GO
ALTER TABLE [dbo].[eForm_Save020] CHECK CONSTRAINT [FK_eForm_Save020_eForm_Save020]
GO
ALTER TABLE [dbo].[eForm_Save020]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save020_FileGuest] FOREIGN KEY([evidence_file_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save020] CHECK CONSTRAINT [FK_eForm_Save020_FileGuest]
GO
ALTER TABLE [dbo].[eForm_Save020]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save020_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save020_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save020] CHECK CONSTRAINT [FK_eForm_Save020_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save020]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save020_RM_Save020AddressType] FOREIGN KEY([save020_contact_type_code])
REFERENCES [dbo].[RM_Save020AddressType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save020] CHECK CONSTRAINT [FK_eForm_Save020_RM_Save020AddressType]
GO
