USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save070ExtendType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save070ExtendType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save070ExtendType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save070ExtendType] ON 

INSERT [dbo].[RM_Save070ExtendType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'TYPE1', N'จำพวกและรายการสินค้า/บริการคงเดิม', 0, CAST(N'2019-12-18T00:29:23.8933333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save070ExtendType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'TYPE2', N'เปลี่ยนแปลงรายการสินค้า/บริการ', 0, CAST(N'2019-12-18T00:29:28.9866667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_Save070ExtendType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 3, N'TYPE3', N'เปลี่ยนแปลงจำพวกสินค้า/บริการ', 0, CAST(N'2020-02-28T12:08:12.7900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save070ExtendType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 4, N'TYPE4', N'เปลี่ยนแปลงจำพวกสินค้า/บริการบางส่วน หรือกรณีอื่นๆ', 0, CAST(N'2020-02-28T12:08:15.5500000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save070ExtendType] OFF
ALTER TABLE [dbo].[RM_Save070ExtendType] ADD  CONSTRAINT [DF_Save070ExtendType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save070ExtendType] ADD  CONSTRAINT [DF_Save070ExtendType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save070ExtendType] ADD  CONSTRAINT [DF_Save070ExtendType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save070ExtendType] ADD  CONSTRAINT [DF_Save070ExtendType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
