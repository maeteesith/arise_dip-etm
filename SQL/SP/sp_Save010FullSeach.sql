ALTER PROCEDURE [dbo].[sp_Save010FullSeach] AS
BEGIN

DECLARE @view_count bigint

--truncate table Save010FullSeach

select @view_count = count(1) from vCheckingSimilarSave010List

WHILE @view_count > 0
BEGIN
	print @view_count

	insert into Save010FullSeach(save_id, request_item_sub_type_1_code_text, request_item_sub_type_1_description_text, word_mark_text, word_first_code_text, word_sound_last_code_text, word_sound_last_other_code_text, name_text, document_classification_image_code_text, document_classification_sound_code_text, document_classification_sound_name_text) 
	select id, request_item_sub_type_1_code_text, request_item_sub_type_1_description_text, word_mark_text, word_first_code_text, word_sound_last_code_text, word_sound_last_other_code_text, name_text, document_classification_image_code_text, document_classification_sound_code_text, document_classification_sound_name_text 
	from (
		select ROW_NUMBER() OVER (order by id) row_id, id, request_item_sub_type_1_code_text, request_item_sub_type_1_description_text, word_mark_text, word_first_code_text, word_sound_last_code_text, word_sound_last_other_code_text, name_text, document_classification_image_code_text, document_classification_sound_code_text, document_classification_sound_name_text 
		from vCheckingSimilarSave010List
	) a
	where row_id <= 1000
	
	select @view_count = count(1) from vCheckingSimilarSave010List
END
END
