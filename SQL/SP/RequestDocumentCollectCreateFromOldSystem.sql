USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[RequestDocumentCollectCreateFromOldSystem]    Script Date: 6/28/2020 1:44:52 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER procedure [dbo].[RequestDocumentCollectCreateFromOldSystem] AS
begin

declare @physical_path varchar(1000)-- = 'Contents/Users/data/image/TRS_SCAN/scan/old/750001/782501/7826910001/2d_7826910001.jpeg'
declare @save_id bigint-- = 2012012

declare @request_number varchar(1000)
declare @file_id bigint

declare @id bigint

declare @total_price decimal(18, 2)

declare p_cursor cursor local static read_only forward_only
for select sr.id from Save010RequestDocumentCollectFromOldSystem sr left join RequestDocumentCollect rd on rd.save_id = sr.save_id where rd.id is null 
--SELECT DISTINCT PractitionerId FROM Practitioner

open p_cursor
fetch next from p_cursor INTO @id
while @@fetch_status = 0
begin 
    select @physical_path = physical_path, @save_id = save_id from Save010RequestDocumentCollectFromOldSystem where id = @id
	
	if not (exists (select * from Request01Process where id = 0))
	begin
		SET IDENTITY_INSERT [dbo].Request01Process ON 
		insert into Request01Process(id, request_date, requester_name, created_by) values(0, GETDATE(), 'DIP Admin', 15)
		SET IDENTITY_INSERT [dbo].Request01Process OFF
	end
	
	if not (exists (select * from Receipt where id = 0))
	begin
		SET IDENTITY_INSERT [dbo].Receipt ON 
		insert into Receipt(id, requester_name, status_code, created_by) values(0, 'DIP Admin', 'PAID', 15)
		SET IDENTITY_INSERT [dbo].Receipt OFF
	end

	if (exists (select * from Save010 where id = @save_id))
	begin
		update Save010 set document_classification_status_code = 'SEND' where id = @save_id
		select @total_price = total_price from Save010 where id = @save_id

		select @request_number = request_number from Save010 where id = @save_id

		delete Request01Item where id = -@save_id
		SET IDENTITY_INSERT [dbo].Request01Item ON 
		insert into Request01Item(id, request01_id, total_price, item_type_code, request_number, status_code) 
		values(-@save_id, 0, @total_price, 'TRADE_MARK_AND_SERVICE_MARK', @request_number, 'RECEIPT')
		SET IDENTITY_INSERT [dbo].Request01Item OFF 

		delete ReceiptItem where id = -@save_id
		SET IDENTITY_INSERT [dbo].ReceiptItem ON 
		insert into ReceiptItem(id, receipt_id, request_number, request_type_code) values(-@save_id, 0, @request_number, 'TRADEMARK_2D')
		SET IDENTITY_INSERT [dbo].ReceiptItem OFF 
	
		delete [File] where id in (select file_id from RequestDocumentCollect where save_id = @save_id and request_document_collect_type_code = 'TRADEMARK_2D')
		insert into [File](file_name, physical_path, display_path) values('img.jpg', @physical_path, 'img.jpg')

		SELECT @file_id =  SCOPE_IDENTITY()	
		--print @file_id

		delete RequestDocumentCollect where save_id = @save_id and request_document_collect_type_code = 'TRADEMARK_2D';
		insert into RequestDocumentCollect(save_id, receipt_item_id, request_document_collect_type_code, file_id, request_document_collect_status_code) 
		values(@save_id, -@save_id, 'TRADEMARK_2D', abs(@file_id), 'ADD')
	end

	fetch next from p_cursor INTO @id
end
close p_cursor
deallocate p_cursor
end