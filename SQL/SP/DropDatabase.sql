USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[DropDatabase]    Script Date: 6/24/2020 9:05:38 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[DropDatabase] AS
BEGIN

DECLARE @oneid varchar(1000) 
DECLARE the_cursor CURSOR FAST_FORWARD
FOR 
select 'drop table [' + name + ']' from sys.tables where --name = 'UM_Page' and 
name not like 'zBackupByAJ_%' 
union
select 'drop view [' + name + ']' from sys.views
;

OPEN the_cursor
FETCH NEXT FROM the_cursor INTO @oneid

WHILE @@FETCH_STATUS = 0
BEGIN
	EXECUTE (@oneid);
	print @oneid

    FETCH NEXT FROM the_cursor INTO @oneid
END

CLOSE the_cursor
DEALLOCATE the_cursor

END
