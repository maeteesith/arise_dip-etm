USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[RestoreDatabase]    Script Date: 6/24/2020 9:06:02 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[RestoreDatabase] AS
BEGIN

DECLARE @oneid varchar(4000) 

DECLARE the_cursor CURSOR FAST_FORWARD
FOR select name from sys.tables where --name = 'UM_Page' and 
name not like 'zBackupByAJ_%' and name <> 'AuditLog' and name <> 'EventLog'
order by name

OPEN the_cursor FETCH NEXT FROM the_cursor INTO @oneid

DECLARE @sql nvarchar(max)
DECLARE @sql_1 nvarchar(4000)
DECLARE @sql_2 nvarchar(4000)
DECLARE @rowcount_1 int
DECLARE @rowcount_2 int
DECLARE @column_list nvarchar(4000)

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (EXISTS (SELECT * FROM sys.tables  WHERE name = 'zBackupByAJ_' + @oneid))
	BEGIN			
		--EXECUTE ('delete ' + @oneid);

		SELECT @SQL = 'SELECT @rowcount_1=count(*) FROM ' + 'zBackupByAJ_' + @oneid
		EXEC sp_executesql @SQL, N'@rowcount_1 INT OUTPUT', @rowcount_1 OUTPUT
		
		IF (@rowcount_1 > 0)
		BEGIN

			SELECT @SQL = 'SELECT @rowcount_2=count(*) FROM [' + @oneid + ']'
			EXEC sp_executesql @SQL, N'@rowcount_2 INT OUTPUT', @rowcount_2 OUTPUT		
	
			IF (@rowcount_2 = 0)
			BEGIN		
				print @oneid + ' row count: ' + cast(@rowcount_1 as varchar) + ' to ' + cast(@rowcount_2 as varchar)

				SELECT @SQL = '
					select @column_list = case when len(code) = 0 then null else left(code, len(code)-1) end 
					from (select (
					select name + '', '' as [text()] 
					from sys.all_columns 
					where OBJECT_NAME(object_id) = ''zBackupByAJ_' + @oneid + ''' for xml path('''')
					) code) column_list'
			
				EXEC sp_executesql @SQL, N'@column_list nvarchar(4000) OUTPUT', @column_list OUTPUT				

				--print @column_list

				
				SELECT @sql_1 = 'SET IDENTITY_INSERT [dbo].[' + @oneid + '] ON;
				insert into [' + @oneid + '](' + @column_list + ') '
				
				SELECT @sql_2 = 'select ' + @column_list + ' from ' + 'zBackupByAJ_' + @oneid + ';
				SET IDENTITY_INSERT [dbo].[' + @oneid + '] ON;'
				
				SELECT @sql = 'SET IDENTITY_INSERT [dbo].[' + @oneid + '] ON;
				insert into [' + @oneid + '](' + @column_list + ') select ' + @column_list + ' from ' + 'zBackupByAJ_' + @oneid + ';
				SET IDENTITY_INSERT [dbo].[' + @oneid + '] ON;'

				--print (@sql_1 + @sql_2)

				EXEC (@sql_1 + @sql_2)

				print 'restore table ' + 'zBackupByAJ_' + @oneid + ' to ' + @oneid;
			
			END
		END
		
	END	
    FETCH NEXT FROM the_cursor INTO @oneid
END

CLOSE the_cursor
DEALLOCATE the_cursor

END
