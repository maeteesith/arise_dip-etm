USE [tmdb]
GO
/****** Object:  StoredProcedure [dbo].[BackupDatabase]    Script Date: 6/24/2020 9:05:17 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[BackupDatabase] AS
BEGIN

DECLARE @oneid varchar(1000) 
DECLARE the_cursor CURSOR FAST_FORWARD
FOR select name from sys.tables where --name = 'UM_Page' and 
name not like 'zBackupByAJ_%' order by name;

OPEN the_cursor
FETCH NEXT FROM the_cursor INTO @oneid

WHILE @@FETCH_STATUS = 0
BEGIN
	IF (EXISTS (SELECT * FROM sys.tables  WHERE name = 'zBackupByAJ_' + @oneid))
	BEGIN
		EXECUTE ('drop table ' +  'zBackupByAJ_' + @oneid + ';');
		print 'drop table ' + 'zBackupByAJ_' + @oneid;
	END
	
	EXECUTE ('select * into ' +  'zBackupByAJ_' + @oneid + ' from [' + @oneid + '];');
	print 'create table ' + 'zBackupByAJ_' + @oneid;

    FETCH NEXT FROM the_cursor INTO @oneid
END

CLOSE the_cursor
DEALLOCATE the_cursor

END
