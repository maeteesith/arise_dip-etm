USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save010DocumentClassificationSoundLastOther]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save010DocumentClassificationSoundLastOther](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save010DocumentClassificationSoundLastOther] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ON 

INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (16, 1, N'B', N'บ', 0, CAST(N'2020-03-25T10:29:43.6733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1, N'CH', N'ฉ ช ฌ', 0, CAST(N'2020-03-25T10:29:39.7666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, 1, N'D', N'ด ฎ', 0, CAST(N'2020-03-25T10:29:41.2400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (19, 1, N'F', N'ฝ ฟ', 0, CAST(N'2020-03-25T10:29:44.6333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (24, 1, N'H', N'ห ฮ', 0, CAST(N'2020-03-25T10:29:46.2800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 1, N'J', N'จ', 0, CAST(N'2020-03-25T10:29:39.1100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'K', N'ก', 0, CAST(N'2020-03-25T10:29:37.4533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'KH', N'ข ค ฆ', 0, CAST(N'2020-03-25T10:29:38.0666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (20, 1, N'M', N'ม', 0, CAST(N'2020-03-25T10:29:45.0400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (15, 1, N'N', N'น', 0, CAST(N'2020-03-25T10:29:43.3400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 1, N'NG', N'ง', 0, CAST(N'2020-03-25T10:29:38.5200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (17, 1, N'P', N'ป', 0, CAST(N'2020-03-25T10:29:43.9866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (18, 1, N'PH', N'ผ พ ภ', 0, CAST(N'2020-03-25T10:29:44.3366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (21, 1, N'R', N'ร ล ฤ ฬ', 0, CAST(N'2020-03-25T10:29:45.3600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (23, 1, N'S', N'ช(ทร) ศ ษ ส', 0, CAST(N'2020-03-25T10:29:45.9766667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (13, 1, N'T', N'ต', 0, CAST(N'2020-03-25T10:29:42.0766667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (14, 1, N'TH', N'ฐ ฑ ฒ ท ธ', 0, CAST(N'2020-03-25T10:29:42.8366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (22, 1, N'W', N'ว', 0, CAST(N'2020-03-25T10:29:45.6566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 1, N'Y', N'ญ ย', 0, CAST(N'2020-03-25T10:29:40.5000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (25, 1, N'อ', N'A E I O U', 0, CAST(N'2020-03-25T10:29:46.8900000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save010DocumentClassificationSoundLastOther] OFF
ALTER TABLE [dbo].[RM_Save010DocumentClassificationSoundLastOther] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSoundLastOther_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationSoundLastOther] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSoundLastOther_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationSoundLastOther] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSoundLastOther_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationSoundLastOther] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSoundLastOther_created_by]  DEFAULT ((0)) FOR [created_by]
GO
