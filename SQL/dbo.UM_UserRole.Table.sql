USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[UM_UserRole]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UM_UserRole](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[user_id] [bigint] NOT NULL,
	[role_id] [bigint] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_UserRole] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[UM_UserRole] ADD  CONSTRAINT [DF_UM_UserRole_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[UM_UserRole] ADD  CONSTRAINT [DF_UM_UserRole_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[UM_UserRole] ADD  CONSTRAINT [DF_UM_UserRole_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[UM_UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UM_UserRole_UM_Role] FOREIGN KEY([role_id])
REFERENCES [dbo].[UM_Role] ([id])
GO
ALTER TABLE [dbo].[UM_UserRole] CHECK CONSTRAINT [FK_UM_UserRole_UM_Role]
GO
ALTER TABLE [dbo].[UM_UserRole]  WITH NOCHECK ADD  CONSTRAINT [FK_UM_UserRole_UM_User] FOREIGN KEY([user_id])
REFERENCES [dbo].[UM_User] ([id])
GO
ALTER TABLE [dbo].[UM_UserRole] NOCHECK CONSTRAINT [FK_UM_UserRole_UM_User]
GO
ALTER TABLE [dbo].[UM_UserRole]  WITH CHECK ADD  CONSTRAINT [FK_UM_UserRole_UM_UserRole] FOREIGN KEY([id])
REFERENCES [dbo].[UM_UserRole] ([id])
GO
ALTER TABLE [dbo].[UM_UserRole] CHECK CONSTRAINT [FK_UM_UserRole_UM_UserRole]
GO
