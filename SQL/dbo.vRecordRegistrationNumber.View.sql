USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vRecordRegistrationNumber]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vRecordRegistrationNumber] as

select 
pr.id,

s.id save_id,
s.request_number,
s.public_role05_date,
s.public_role05_by,
s.public_role05_status_code,
s.registration_number,

pr.book_payment_date,

p.book_index,
s.public_page_index,
p.public_start_date,

pr.is_deleted,
pr.created_by,
pr.created_date,
pr.updated_by,
pr.updated_date

from PostRound pr
join Save010 s on s.id = pr.object_id
join PublicRound p on p.id = s.public_round_id
where pr.post_round_instruction_rule_code = 'RULE_5_3'
GO
