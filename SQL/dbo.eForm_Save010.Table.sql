USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save010]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save010](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save010_mark_type_type_code] [nvarchar](50) NULL,
	[save010_requestor_type_code] [nvarchar](50) NULL,
	[save010_img_type_type_code] [nvarchar](50) NULL,
	[img_path] [nvarchar](500) NULL,
	[img_w] [int] NULL,
	[img_h] [int] NULL,
	[remark_5_1_3] [nvarchar](1000) NULL,
	[product_ctg_path] [nvarchar](500) NULL,
	[remark_8] [nvarchar](1000) NULL,
	[remark_9] [nvarchar](1000) NULL,
	[save010_assert_type_code] [nvarchar](50) NULL,
	[save010_otop_type_code] [nvarchar](50) NULL,
	[otop_number] [nvarchar](50) NULL,
	[save010_postpone_type_code] [nvarchar](50) NULL,
	[is_14_1] [bit] NULL,
	[is_14_2] [bit] NULL,
	[is_14_3] [bit] NULL,
	[is_14_4] [bit] NULL,
	[is_14_5] [bit] NULL,
	[is_14_6] [bit] NULL,
	[is_14_7] [bit] NULL,
	[is_14_8] [bit] NULL,
	[translation_language_code] [nvarchar](50) NULL,
	[save010_contact_type_code] [nvarchar](50) NULL,
	[img_file_2d_id] [bigint] NULL,
	[save010_sound_mark_type_type_code] [nvarchar](50) NULL,
	[remark_5_2_2] [nvarchar](max) NULL,
	[sound_jpg_file_id] [bigint] NULL,
	[product_ctg_file_id] [bigint] NULL,
	[guarantee_rule_file_id] [bigint] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[sound_file_id] [bigint] NULL,
	[sign_inform_person_list] [nvarchar](500) NULL,
	[save010_representative_condition_type_code] [nvarchar](50) NULL,
	[img_file_3d_id_1] [bigint] NULL,
	[img_file_3d_id_2] [bigint] NULL,
	[img_file_3d_id_3] [bigint] NULL,
	[img_file_3d_id_4] [bigint] NULL,
	[img_file_3d_id_5] [bigint] NULL,
	[img_file_3d_id_6] [bigint] NULL,
	[is_sound_mark] [bit] NULL,
	[is_color_protect] [bit] NULL,
	[is_model_protect] [bit] NULL,
	[is_before_request] [bit] NULL,
	[is_14_9] [bit] NULL,
	[is_14_10] [bit] NULL,
	[wizard] [nvarchar](300) NULL,
	[sign_inform_representative_list] [nvarchar](500) NULL,
	[is_sound_mark_human] [bit] NULL,
	[is_sound_mark_animal] [bit] NULL,
	[is_sound_mark_sound] [bit] NULL,
	[is_sound_mark_other] [bit] NULL,
	[rule_number] [nvarchar](50) NULL,
 CONSTRAINT [PK_eForm_Save010] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save010] ADD  CONSTRAINT [DF_eForm_Save010_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save010] ADD  CONSTRAINT [DF_eForm_Save010_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save010] ADD  CONSTRAINT [DF_eForm_Save010_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest_2D] FOREIGN KEY([img_file_2d_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest_2D]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest_3D_1] FOREIGN KEY([img_file_3d_id_1])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest_3D_1]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest_3D_2] FOREIGN KEY([img_file_3d_id_2])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest_3D_2]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest_3D_3] FOREIGN KEY([img_file_3d_id_3])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest_3D_3]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest_3D_4] FOREIGN KEY([img_file_3d_id_4])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest_3D_4]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest_3D_5] FOREIGN KEY([img_file_3d_id_5])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest_3D_5]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest_3D_6] FOREIGN KEY([img_file_3d_id_6])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest_3D_6]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH NOCHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest1] FOREIGN KEY([sound_file_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest1]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH NOCHECK ADD  CONSTRAINT [FK_eForm_Save010_FileGuest2] FOREIGN KEY([sound_jpg_file_id])
REFERENCES [dbo].[FileGuest] ([id])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_FileGuest2]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save010_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save010]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save010_RM_RequestImageMarkType] FOREIGN KEY([save010_img_type_type_code])
REFERENCES [dbo].[RM_RequestImageMarkType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save010] CHECK CONSTRAINT [FK_eForm_Save010_RM_RequestImageMarkType]
GO
