USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vEForm_eForm_Save010]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vEForm_eForm_Save010] as

select 
0 id,
0 request01_id,

e.id eform_id,
--e.request01_id,
e.eform_number,
e.telephone,

e.request_number,

e.save010_mark_type_type_code item_type_code,
rm_ri.name item_type_name,
rm_ry1.code income_type,
rm_ry1.name income_description,

rm_ry2.code size_over_income_type,
rm_ry2.name size_over_income_description,



 (case when e.img_w > 5 then e.img_w - 5 else 0 end) + (case when e.img_h > 5 then e.img_h - 5 else 0 end) size_over_cm,
 (case when e.img_w > 5 then e.img_w - 5 else 0 end) + (case when e.img_h > 5 then e.img_h - 5 else 0 end) * 200 size_over_price,
cast(case when e.otop_number = '' then 0 else 1 end as bit) is_otop,

(select count(distinct sp.request_item_sub_type_1_code) from eForm_Save010Product sp where sp.save_id = e.id and sp.is_deleted = 0) request_01_item_sub_count,
(select count(1) from eForm_Save010Product sp where sp.save_id = e.id and sp.is_deleted = 0) request_01_product_count,

(
select sum(case when request_product_count > 5 then 9000 else request_product_count * 1000 end) 
from (
	select sp.request_item_sub_type_1_code, count(1) request_product_count
	from eForm_Save010Product sp 
	where sp.save_id = e.id and sp.is_deleted = 0
	group by sp.request_item_sub_type_1_code
	) sp
) total_price,

e.sound_file_id,
f.physical_path sound_file_physical_path,
f.file_size sound_file_size,

(case 
when e.is_sound_mark_human = 1 then 'HUMAN' 
when e.is_sound_mark_animal = 1 then 'ANIMAL'
when e.is_sound_mark_sound = 1 then 'NATURE'
when e.is_sound_mark_other = 1 then 'OTHERS'
end) sound_type_code,


e.remark_5_2_2 sound_description,

e.is_deleted,
e.created_by,
e.created_date,
e.updated_by,
e.updated_date
--,*

from eForm_Save010 e
join RM_RequestItemType rm_ri on rm_ri.code = e.save010_mark_type_type_code
left join [FileGuest] f on f.id = e.sound_file_id
left join RM_RequestType rm_ry1 on rm_ry1.code = '010'
left join RM_RequestType rm_ry2 on rm_ry2.code = (case when e.img_w > 5 or e.img_h > 5 then '210' else null end)

--where e.id = 170
GO
