USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vSave010CheckingSaveDocument]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vSave010CheckingSaveDocument] as

select 

v.*,

s.id save_id,

(select case when len(sp.code) = 0 then null else left(sp.code, len(sp.code)-1) end from (select (
select sp.request_item_sub_type_1_code + ', ' as [text()] 
from Save010Product sp 
where sp.save_id = s.id
group by sp.request_item_sub_type_1_code 
order by sp.request_item_sub_type_1_code for xml path('')
) code) sp ) request_item_sub_type_1_code_list_text

from (
	select
	s.id,
	s.request_number,

	'40' request_type_code, N'ใบ (ก.04)' request_type_name,
	make_date,  
	s.consider_similar_document_status_code,
	rm_csd.name consider_similar_document_status_name,
	s.consider_similar_document_date,
	s.consider_similar_document_remark,
	s.consider_similar_document_item_status_list,

	(select top 1 'File/Content/' + cast(rd.file_id as varchar) 
	from RequestDocumentCollect rd 
	join Save010 s010 on s010.id = rd.save_id
	where s010.request_number = s.request_number and (rd.request_document_collect_type_code = 'SAVE04' or rd.request_document_collect_type_code = '40') and rd.request_document_collect_status_code = 
	'ADD' and rd.file_id > 0) consider_similar_document_file_id,
	
	s.is_deleted,
	s.created_by,
	u.name created_by_name,
	s.created_date,
	s.updated_by,
	s.updated_date

	from Save040 s 
	join RM_ConsideringSimilarDocumentStatusCode rm_csd on rm_csd.code = s.consider_similar_document_status_code
	left join UM_User u on u.created_by = s.created_by
	where s.save_status_code = 'SENT'

	union all


	select
	s.id,
	s.request_number,

	'60' request_type_code, N'ใบ (ก.06)' request_type_name,
	make_date,  
	s.consider_similar_document_status_code,
	rm_csd.name consider_similar_document_status_name,
	s.consider_similar_document_date,
	s.consider_similar_document_remark,
	s.consider_similar_document_item_status_list,

	(select top 1 'File/Content/' + cast(rd.file_id as varchar) 
	from RequestDocumentCollect rd 
	join Save010 s010 on s010.id = rd.save_id
	where s010.request_number = s.request_number and (rd.request_document_collect_type_code = 'SAVE06' or rd.request_document_collect_type_code = '60') and rd.request_document_collect_status_code = 
	'ADD' and rd.file_id > 0) consider_similar_document_file_id,
	
	--s.*,

	s.is_deleted,
	s.created_by,
	u.name created_by_name,
	s.created_date,
	s.updated_by,
	s.updated_date

	from Save060 s
	join RM_ConsideringSimilarDocumentStatusCode rm_csd on rm_csd.code = s.consider_similar_document_status_code
	left join UM_User u on u.created_by = s.created_by
	where s.save_status_code = 'SENT'

	union all


	select
	s.id,
	s.request_number,

	s.request_type_code, 
	case when s.request_type_code = '120' then N'ใบ (ก.12)' else rm_rt.name end request_type_name,
	make_date,  
	s.consider_similar_document_status_code,
	rm_csd.name consider_similar_document_status_name,
	s.consider_similar_document_date,
	s.consider_similar_document_remark,
	s.consider_similar_document_item_status_list,

	(select top 1 'File/Content/' + cast(rd.file_id as varchar) 
	from RequestDocumentCollect rd 
	join Save010 s010 on s010.id = rd.save_id
	where s010.request_number = s.request_number and rd.request_document_collect_type_code = 'SAVE06' and rd.request_document_collect_status_code = 
	'ADD' and rd.file_id > 0) consider_similar_document_file_id,
	
	s.is_deleted,
	s.created_by,
	u.name created_by_name,
	s.created_date,
	s.updated_by,
	s.updated_date

	from SaveOther s
	join RM_ConsideringSimilarDocumentStatusCode rm_csd on rm_csd.code = s.consider_similar_document_status_code
	join RM_RequestType rm_rt on rm_rt.code = s.request_type_code
	left join UM_User u on u.created_by = s.created_by
	where s.department_send_code = 'CHECKING' and s.save_status_code = 'SENT'
) v
join Save010 s on s.request_number = v.request_number
GO
