USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vSave010CheckingInstructionDocument]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vSave010CheckingInstructionDocument] as

select 
sr.id, 
sr.save_id,
pr.id post_round_id,

s.request_number,
s.trademark_status_code,
sr.instruction_rule_code,
rm_cir.name instruction_rule_name,
sr.considering_instruction_rule_status_code,
rm_cirs.name considering_instruction_rule_status_name,

sr.change_status_date,
sr.change_reason,

pr.book_number,
pr.book_start_date,

'' save010_checking_instruction_document_remark,

sr.is_deleted,
sr.created_by,
sr.created_date,
sr.updated_by,
sr.updated_date
--,* 
from
Save010InstructionRule sr

--join (select top 1 sir.id from Save010InstructionRule sir where sir.id ) sir on sir.id = sr.id

join Save010 s on s.id = sr.save_id
join RM_ConsideringSimilarInstructionRuleCode rm_cir on rm_cir.code = sr.instruction_rule_code
join RM_ConsideringSimilarInstructionRuleStatusCode rm_cirs on rm_cirs.code = sr.considering_instruction_rule_status_code
--left join PostRoundInstructionRule pir on pir.save010_instruction_rule_id = sr.id

/*
left join 
(select top 1 pr_c.id, pr_c.book_number, pr_c.book_start_date, pr_c.object_id, pr_c.post_round_instruction_rule_code 
from PostRound pr_c 
where  prc.id ) 
pr on 
*/

outer apply (
select top 1 
pr.id, pr.book_number, pr.book_start_date --, pr_c.object_id, pr_c.post_round_instruction_rule_code 
from PostRound pr
join PostRoundInstructionRule pir on pir.post_round_id = pr.id
where pir.save010_instruction_rule_id = sr.id --(pr.object_id = sr.id and pr.post_round_instruction_rule_code = sr.instruction_rule_code) or pr.id = pir.post_round_id
order by pr.id desc
) pr

where sr.considering_instruction_status_code = 'SEND'
--and sr.id = 9
GO
