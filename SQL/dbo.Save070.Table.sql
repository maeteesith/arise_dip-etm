USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save070]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save070](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_id] [bigint] NULL,
	[request_number] [nvarchar](50) NULL,
	[request_date] [date] NULL,
	[make_date] [date] NULL,
	[request_index] [nvarchar](50) NULL,
	[irn_number] [nvarchar](50) NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_save_past] [bit] NULL,
	[request_source_code] [nvarchar](50) NULL,
	[cancel_reason] [nvarchar](500) NULL,
	[save_status_code] [nvarchar](50) NULL,
	[department_send_code] [nvarchar](50) NULL,
	[department_send_date] [date] NULL,
	[consider_similar_document_status_code] [nvarchar](50) NULL,
	[consider_similar_document_date] [date] NULL,
	[consider_similar_document_remark] [nvarchar](1000) NULL,
	[consider_similar_document_item_status_list] [nvarchar](500) NULL,
	[is_split_request] [bit] NOT NULL,
	[register_number] [nvarchar](50) NULL,
	[fine] [float] NULL,
	[split_request_number] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[eform_number] [nvarchar](50) NULL,
	[rule_number] [nvarchar](50) NULL,
 CONSTRAINT [PK_Save07] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save070] ADD  CONSTRAINT [DF_Table_1_is_work_group_change_code]  DEFAULT ((0)) FOR [is_split_request]
GO
ALTER TABLE [dbo].[Save070] ADD  CONSTRAINT [DF_Save07_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save070] ADD  CONSTRAINT [DF_Save07_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save070] ADD  CONSTRAINT [DF_Save07_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Save070]  WITH CHECK ADD  CONSTRAINT [FK_Save070_RM_SaveStatus] FOREIGN KEY([save_status_code])
REFERENCES [dbo].[RM_SaveStatus] ([code])
GO
ALTER TABLE [dbo].[Save070] CHECK CONSTRAINT [FK_Save070_RM_SaveStatus]
GO
