USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vRequestDocumentCollect]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vRequestDocumentCollect] as

select
ri.id,
rd.id rd_id,

rc.requester_name,
s.id save_id,
ri.request_number,
rc.reference_number,
rc.request_date,
ri.total_price,

rm_rd.code request_document_collect_type_code,
rm_rd.name request_document_collect_type_name,
isnull(rm_rd.value_1, 'PDF') request_document_collect_type_file_extension,

rd.file_id,
f.physical_path,

rd.request_document_collect_status_code,
rm_rds.name request_document_collect_status_name,
rd.cancel_reason,

uc.name updated_by_name,

ri.is_deleted,
ri.created_by,
ri.created_date,
ri.updated_by,
ri.updated_date

--select * 
from Receipt rc
join ReceiptItem ri on ri.receipt_id = rc.id and ri.request_type_code != '210'
join Save010 s on s.request_number = ri.request_number
join RM_RequestDocumentCollectTypeCode rm_rd on rm_rd.code = ri.request_type_code or (ri.request_type_code = '360' and rm_rd.code = '1000')
or (rm_rd.code = 'TRADEMARK_2D' and ri.request_type_code = '010')
or (rm_rd.code like 'TRADEMARK_3D%' and ri.request_type_code = '010')
or (rm_rd.code = 'REQUEST_FILE_COVER' and ri.request_type_code = '010')
or (rm_rd.code = '1001' and ri.request_type_code = '010' and s.floor3_registrar_status_code = 'DONE')

or (rm_rd.code = 'CERTIFICATION_MARK' and ri.request_type_code = '010' and s.request_item_type_code = 'CERTIFICATION_MARK')

--or (ri.code = '1001' and ri.request_type_code = '010' and s.floor3_registrar_status_code = 'DONE')
left join RequestDocumentCollect rd on rd.receipt_item_id = ri.id and rd.request_document_collect_type_code=rm_rd.code and rd.request_document_collect_status_code = 'ADD'
left join RM_RequestDocumentCollectStatusCode rm_rds on rm_rds.code = rd.request_document_collect_status_code
left join [File] f on f.id = rd.file_id

left join UM_User uc on uc.id = isnull(rd.updated_by, rd.created_by)

where rc.status_code = 'PAID'
--where ri.request_number = 200100591;

GO
