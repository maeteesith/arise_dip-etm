USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010DocumentClassificationCollect]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010DocumentClassificationCollect](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NOT NULL,
	[file_id] [bigint] NOT NULL,
	[sender_by_name] [nvarchar](1000) NULL,
	[department_name] [nvarchar](1000) NULL,
	[remark] [nvarchar](1000) NULL,
	[document_classification_collect_status_code] [nvarchar](50) NULL,
	[maker_by] [bigint] NULL,
	[is_deleted] [bit] NOT NULL,
	[make_date] [datetime2](7) NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010DocumentClassificationCollect] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010DocumentClassificationCollect] ADD  CONSTRAINT [DF_Save010DocumentClassificationCollect_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010DocumentClassificationCollect] ADD  CONSTRAINT [DF_Save010DocumentClassificationCollect_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010DocumentClassificationCollect] ADD  CONSTRAINT [DF_Save010DocumentClassificationCollect_created_by]  DEFAULT ((0)) FOR [created_by]
GO
