USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RequestOtherItem]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestOtherItem](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_other_process_id] [bigint] NOT NULL,
	[item_index] [int] NULL,
	[request_type_code] [nvarchar](50) NULL,
	[item_count] [int] NULL,
	[item_sub_type_1_count] [int] NULL,
	[product_count] [int] NULL,
	[total_price] [decimal](18, 2) NULL,
	[book_index] [nvarchar](50) NULL,
	[page_index] [nvarchar](50) NULL,
	[fine] [decimal](18, 2) NULL,
	[is_evidence_floor7] [bit] NULL,
	[is_sue] [bit] NULL,
	[is_sue_text] [nvarchar](500) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestOtherItem] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RequestOtherItem] ADD  CONSTRAINT [DF_RequestOtherItem_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RequestOtherItem] ADD  CONSTRAINT [DF_RequestOtherItem_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RequestOtherItem] ADD  CONSTRAINT [DF_RequestOtherItem_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RequestOtherItem]  WITH CHECK ADD  CONSTRAINT [FK_RequestOtherItem_RequestOtherProcess] FOREIGN KEY([request_other_process_id])
REFERENCES [dbo].[RequestOtherProcess] ([id])
GO
ALTER TABLE [dbo].[RequestOtherItem] CHECK CONSTRAINT [FK_RequestOtherItem_RequestOtherProcess]
GO
ALTER TABLE [dbo].[RequestOtherItem]  WITH NOCHECK ADD  CONSTRAINT [FK_RequestOtherItem_RM_RequestType] FOREIGN KEY([request_type_code])
REFERENCES [dbo].[RM_RequestType] ([code])
GO
ALTER TABLE [dbo].[RequestOtherItem] CHECK CONSTRAINT [FK_RequestOtherItem_RM_RequestType]
GO
