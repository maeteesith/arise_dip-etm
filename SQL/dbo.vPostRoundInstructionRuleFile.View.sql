USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vPostRoundInstructionRuleFile]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vPostRoundInstructionRuleFile] as
select 

pr.id,
pri.id post_round_instruction_rule_id,
prif.id post_round_instruction_rule_file_id,

prif.remark,

prif.post_round_instruction_rule_file_type_code,
rm_prf.name post_round_instruction_rule_file_type_name,

prif.created_date file_created_date,
um_prif.id file_created_by,
um_prif.name file_created_by_name,

prif.file_id,
f.file_name,
f.file_size,

pr.is_deleted,
pr.created_by,
pr.created_date,
pr.updated_by,
pr.updated_date

from PostRound pr
join PostRoundInstructionRule pri on pri.post_round_id = pr.id
join PostRoundInstructionRuleFile prif on prif.post_round_instruction_rule_id = pri.id
join [File] f on f.id = prif.file_id
left join RM_PostRoundInstructionRuleFileTypeCode rm_prf on rm_prf.code = prif.post_round_instruction_rule_file_type_code
left join UM_User um_prif on um_prif.id = prif.created_by

where prif.is_deleted = 0
GO
