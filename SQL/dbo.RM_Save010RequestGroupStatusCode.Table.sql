USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save010RequestGroupStatusCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save010RequestGroupStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save010RequestGroupStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save010RequestGroupStatusCode] ON 

INSERT [dbo].[RM_Save010RequestGroupStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'ALLOW', N'อนุญาต', 1, 0, CAST(N'2020-05-16T12:43:40.9533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010RequestGroupStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'COMMAND', N'มีคำสั่ง', 1, 0, CAST(N'2020-05-16T12:43:35.8366667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save010RequestGroupStatusCode] OFF
ALTER TABLE [dbo].[RM_Save010RequestGroupStatusCode] ADD  CONSTRAINT [DF_RM_Save010RequestGroupStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save010RequestGroupStatusCode] ADD  CONSTRAINT [DF_RM_Save010RequestGroupStatusCode_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_Save010RequestGroupStatusCode] ADD  CONSTRAINT [DF_RM_Save010RequestGroupStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save010RequestGroupStatusCode] ADD  CONSTRAINT [DF_RM_Save010RequestGroupStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save010RequestGroupStatusCode] ADD  CONSTRAINT [DF_RM_Save010RequestGroupStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
