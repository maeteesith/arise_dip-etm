USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestDocumentCollectStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestDocumentCollectStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_RequestDocumentCollectStatusCodeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestDocumentCollectStatusCode] ON 

INSERT [dbo].[RM_RequestDocumentCollectStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'ADD', N'เพิ่ม', 0, CAST(N'2020-01-24T18:15:38.7600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_RequestDocumentCollectStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'DELETED', N'ลบ', 0, CAST(N'2020-01-24T18:15:43.4433333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_RequestDocumentCollectStatusCode] OFF
ALTER TABLE [dbo].[RM_RequestDocumentCollectStatusCode] ADD  CONSTRAINT [DF_RM_RequestDocumentCollectStatusCodeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RequestDocumentCollectStatusCode] ADD  CONSTRAINT [DF_RM_RequestDocumentCollectStatusCodeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_RequestDocumentCollectStatusCode] ADD  CONSTRAINT [DF_RM_RequestDocumentCollectStatusCodeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_RequestDocumentCollectStatusCode] ADD  CONSTRAINT [DF_RM_RequestDocumentCollectStatusCodeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
