USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save080]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save080](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save080_request_item_type_code] [nvarchar](50) NULL,
	[save080_revoke_type_code] [nvarchar](50) NULL,
	[save080_informer_type_code] [nvarchar](50) NULL,
	[save080_contact_type_code] [nvarchar](50) NULL,
	[contract_request_date] [date] NULL,
	[contract_end_date] [date] NULL,
	[is_9_1] [bit] NULL,
	[is_9_2] [bit] NULL,
	[is_9_3] [bit] NULL,
	[is_9_4] [bit] NULL,
	[sign_inform_person_list] [nvarchar](500) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[contract_ref_number] [nvarchar](50) NULL,
	[save080_search_type_code] [nvarchar](50) NULL,
	[save080_submit_type_code] [nvarchar](50) NULL,
	[rule_number] [nvarchar](50) NULL,
	[save080_representative_condition_type_code] [nvarchar](50) NULL,
	[evidence_name] [nvarchar](1000) NULL,
	[evidence_telephone] [nvarchar](50) NULL,
	[sign_inform_representative_list] [nvarchar](50) NULL,
	[remark_7] [nvarchar](max) NULL,
	[sign_inform_receiver_person_list] [nvarchar](50) NULL,
	[sign_inform_receiver_representative_list] [nvarchar](50) NULL,
	[save080_receiver_representative_condition_type_code] [nvarchar](50) NULL,
	[save080_receiver_contact_type_code] [nvarchar](50) NULL,
	[wizard] [nvarchar](300) NULL,
 CONSTRAINT [PK_eForm_Save080] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save080] ADD  CONSTRAINT [DF_eForm_Save080_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save080] ADD  CONSTRAINT [DF_eForm_Save080_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save080] ADD  CONSTRAINT [DF_eForm_Save080_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save080]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save080_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save080_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save080] CHECK CONSTRAINT [FK_eForm_Save080_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save080]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save080_RM_Save080RequestItemTypeCode] FOREIGN KEY([save080_request_item_type_code])
REFERENCES [dbo].[RM_Save080RequestItemTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save080] CHECK CONSTRAINT [FK_eForm_Save080_RM_Save080RequestItemTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save080]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save080_RM_Save080RevokeTypeCode] FOREIGN KEY([save080_revoke_type_code])
REFERENCES [dbo].[RM_Save080RevokeTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save080] CHECK CONSTRAINT [FK_eForm_Save080_RM_Save080RevokeTypeCode]
GO
