USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save220Kor10Product]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save220Kor10Product](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[request_number] [nvarchar](50) NULL,
	[request_date] [date] NULL,
	[request_country] [nvarchar](50) NULL,
	[request_nationality] [nvarchar](50) NULL,
	[request_domicile] [nvarchar](50) NULL,
	[product_class] [nvarchar](50) NULL,
	[product] [nvarchar](500) NULL,
	[status] [nvarchar](50) NULL,
	[is_deleted] [bit] NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_eForm_Save220Kor10Product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save220Kor10Product] ADD  CONSTRAINT [DF_eForm_Save220Kor10Product_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save220Kor10Product] ADD  CONSTRAINT [DF_eForm_Save220Kor10Product_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save220Kor10Product] ADD  CONSTRAINT [DF_eForm_Save220Kor10Product_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save220Kor10Product]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save220Kor10Product_eForm_Save220Kor10] FOREIGN KEY([save_id])
REFERENCES [dbo].[eForm_Save220Kor10] ([id])
GO
ALTER TABLE [dbo].[eForm_Save220Kor10Product] CHECK CONSTRAINT [FK_eForm_Save220Kor10Product_eForm_Save220Kor10]
GO
