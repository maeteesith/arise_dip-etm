USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[PostRound]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostRound](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[post_round_instruction_rule_code] [nvarchar](50) NULL,
	[post_round_type_code] [nvarchar](50) NULL,
	[object_id] [bigint] NULL,
	[round_index] [int] NULL,
	[book_by] [bigint] NULL,
	[book_number] [nvarchar](50) NULL,
	[book_start_date] [datetime2](7) NULL,
	[book_end_date] [datetime2](7) NULL,
	[book_expired_day] [int] NULL,
	[post_round_document_post_status_code] [nvarchar](50) NULL,
	[post_round_document_post_date] [datetime2](7) NULL,
	[document_role04_status_code] [nvarchar](50) NULL,
	[document_role04_receiver_by] [bigint] NULL,
	[document_role04_receive_date] [datetime2](7) NULL,
	[document_role04_receive_remark] [nvarchar](1000) NULL,
	[document_role04_receive_status_code] [nvarchar](50) NULL,
	[document_role04_receive_check_list] [nvarchar](1000) NULL,
	[document_role04_receive_send_date] [datetime2](7) NULL,
	[document_role05_by] [bigint] NULL,
	[document_role05_date] [datetime2](7) NULL,
	[document_role05_status_code] [nvarchar](50) NULL,
	[document_role05_receiver_by] [bigint] NULL,
	[document_role05_receive_date] [datetime2](7) NULL,
	[document_role05_receive_status_code] [nvarchar](50) NULL,
	[document_role05_receive_send_date] [datetime2](7) NULL,
	[document_role02_status_code] [nvarchar](50) NULL,
	[document_role02_date] [datetime2](7) NULL,
	[document_role02_spliter_by] [bigint] NULL,
	[document_role02_receiver_by] [bigint] NULL,
	[document_role02_receive_status_code] [nvarchar](50) NULL,
	[document_role02_receive_date] [datetime2](7) NULL,
	[post_number] [nvarchar](50) NULL,
	[document_role02_print_document_status_code] [nvarchar](50) NULL,
	[document_role02_print_document_date] [datetime2](7) NULL,
	[document_role02_print_cover_status_code] [nvarchar](50) NULL,
	[document_role02_print_cover_date] [datetime2](7) NULL,
	[document_role02_post_number_date] [datetime2](7) NULL,
	[document_role02_print_list_status_code] [nvarchar](50) NULL,
	[document_role02_print_list_date] [datetime2](7) NULL,
	[post_round_action_post_status_code] [nvarchar](50) NULL,
	[post_round_action_post_type_code] [nvarchar](50) NULL,
	[book_acknowledge_date] [datetime2](7) NULL,
	[reference_number] [nvarchar](50) NULL,
	[book_payment_date] [datetime2](7) NULL,
	[post_round_remark] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_PostRound] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PostRound] ADD  CONSTRAINT [DF_PostRound_book_by]  DEFAULT ((0)) FOR [book_by]
GO
ALTER TABLE [dbo].[PostRound] ADD  CONSTRAINT [DF_PostRound_document_role02_spliter_by1]  DEFAULT ((0)) FOR [document_role04_receiver_by]
GO
ALTER TABLE [dbo].[PostRound] ADD  CONSTRAINT [DF_PostRound_document_role04_receiver_by1]  DEFAULT ((0)) FOR [document_role05_receiver_by]
GO
ALTER TABLE [dbo].[PostRound] ADD  CONSTRAINT [DF_PostRound_public_role04_spliter_by]  DEFAULT ((0)) FOR [document_role02_spliter_by]
GO
ALTER TABLE [dbo].[PostRound] ADD  CONSTRAINT [DF_PostRound_public_role04_receiver_by]  DEFAULT ((0)) FOR [document_role02_receiver_by]
GO
ALTER TABLE [dbo].[PostRound] ADD  CONSTRAINT [DF_PostRound_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[PostRound] ADD  CONSTRAINT [DF_PostRound_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[PostRound] ADD  CONSTRAINT [DF_PostRound_created_by]  DEFAULT ((0)) FOR [created_by]
GO
