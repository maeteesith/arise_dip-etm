USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save040]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save040](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_id] [bigint] NULL,
	[request_number] [nvarchar](50) NULL,
	[request_date] [date] NULL,
	[make_date] [date] NULL,
	[request_index] [nvarchar](50) NULL,
	[irn_number] [nvarchar](50) NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_save_past] [bit] NULL,
	[request_source_code] [nvarchar](50) NULL,
	[cancel_reason] [nvarchar](500) NULL,
	[save_status_code] [nvarchar](50) NULL,
	[department_send_code] [nvarchar](50) NULL,
	[department_send_date] [date] NULL,
	[consider_similar_document_status_code] [nvarchar](50) NULL,
	[consider_similar_document_date] [date] NULL,
	[consider_similar_document_remark] [nvarchar](1000) NULL,
	[consider_similar_document_item_status_list] [nvarchar](500) NULL,
	[register_number] [nvarchar](50) NULL,
	[save040_transfer_type_code] [nvarchar](50) NULL,
	[save040_transfer_form_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[eform_number] [nvarchar](50) NULL,
	[rule_number] [nvarchar](50) NULL,
 CONSTRAINT [PK_Save04] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save040] ADD  CONSTRAINT [DF_Save04_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save040] ADD  CONSTRAINT [DF_Save04_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save040] ADD  CONSTRAINT [DF_Save04_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Save040]  WITH CHECK ADD  CONSTRAINT [FK_Save040_RM_Save040TransferForm] FOREIGN KEY([save040_transfer_form_code])
REFERENCES [dbo].[RM_Save040TransferForm] ([code])
GO
ALTER TABLE [dbo].[Save040] CHECK CONSTRAINT [FK_Save040_RM_Save040TransferForm]
GO
ALTER TABLE [dbo].[Save040]  WITH CHECK ADD  CONSTRAINT [FK_Save040_RM_Save040TransferType] FOREIGN KEY([save040_transfer_type_code])
REFERENCES [dbo].[RM_Save040TransferType] ([code])
GO
ALTER TABLE [dbo].[Save040] CHECK CONSTRAINT [FK_Save040_RM_Save040TransferType]
GO
ALTER TABLE [dbo].[Save040]  WITH CHECK ADD  CONSTRAINT [FK_Save040_RM_SaveStatus] FOREIGN KEY([save_status_code])
REFERENCES [dbo].[RM_SaveStatus] ([code])
GO
ALTER TABLE [dbo].[Save040] CHECK CONSTRAINT [FK_Save040_RM_SaveStatus]
GO
