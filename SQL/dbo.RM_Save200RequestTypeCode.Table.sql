USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save200RequestTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save200RequestTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save200RequestTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save200RequestTypeCode] ON 

INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 10, N'000', N'อื่นๆ', 0, CAST(N'2020-05-07T12:34:32.1400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'010', N'ก.01 คำขอจดทะเบียนเครื่องหมายการค้า/เครื่องหมายบริการ/เครื่องหมายรับรอง/เครื่องหมายร่วม', 0, CAST(N'2020-04-17T10:47:44.9800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'20', N'ก.02 คำคัดค้าน การขอจดทะเบียนเครื่องหมายการค้า', 0, CAST(N'2020-04-17T10:47:54.6800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'21', N'ก.02 โต้แย้ง การขอจดทะเบียนเครื่องหมายการค้า', 0, CAST(N'2020-04-17T10:50:17.2633333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'30', N'ก.03 คำอุทธรณ์', 0, CAST(N'2020-04-17T10:50:23.6500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 5, N'40', N'ก.04 คำขอโอนหรือรับมรดกสิทธิ', 0, CAST(N'2020-04-17T10:50:28.9800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 6, N'50', N'ก.05 คำขอจดทะเบียนสัญญาอนุญาตให้ใช้เครื่องหมายการค้า/บริการ', 0, CAST(N'2020-04-17T10:50:34.7800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 7, N'60', N'ก.06 คำขอแก้ไขเปลี่ยนแปลงรายการจดทะเบียน', 0, CAST(N'2020-04-17T10:50:40.4033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 8, N'70', N'ก.07 คำขอต่ออายุการจดทะเบียน', 0, CAST(N'2020-04-17T10:50:46.1600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save200RequestTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 9, N'80', N'ก.08 คำขอให้เพิกถอนการจดทะเบียน', 0, CAST(N'2020-04-30T17:41:10.4300000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save200RequestTypeCode] OFF
ALTER TABLE [dbo].[RM_Save200RequestTypeCode] ADD  CONSTRAINT [DF_RM_Save200RequestTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save200RequestTypeCode] ADD  CONSTRAINT [DF_RM_Save200RequestTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save200RequestTypeCode] ADD  CONSTRAINT [DF_RM_Save200RequestTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save200RequestTypeCode] ADD  CONSTRAINT [DF_RM_Save200RequestTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
