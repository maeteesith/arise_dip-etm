USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Appeal_Role01SaveKorOtherCommercialProvince]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Appeal_Role01SaveKorOtherCommercialProvince](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[is_deleted] [bit] NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Appeal_Role01SaveKorOtherCommercialProvince] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Appeal_Role01SaveKorOtherCommercialProvince] ON 

INSERT [dbo].[Appeal_Role01SaveKorOtherCommercialProvince] ([id], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, N'Add', 0, CAST(N'2020-06-22T22:18:55.5503618' AS DateTime2), 0, CAST(N'2020-06-22T22:18:55.5504107' AS DateTime2), NULL)
SET IDENTITY_INSERT [dbo].[Appeal_Role01SaveKorOtherCommercialProvince] OFF
