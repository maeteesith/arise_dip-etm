USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save030]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save030](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save030_informer_type_code] [nvarchar](50) NULL,
	[save030_contact_type_code] [nvarchar](50) NULL,
	[evidence_file_id] [bigint] NULL,
	[is_8_1] [bit] NULL,
	[is_8_2] [bit] NULL,
	[is_8_3] [bit] NULL,
	[is_8_4] [bit] NULL,
	[sign_inform_person_list] [nvarchar](300) NULL,
	[sign_inform_representative_list] [nvarchar](300) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[appeal_order] [nvarchar](max) NULL,
	[contract_ref_number] [nvarchar](50) NULL,
	[challenger_ref_number] [nvarchar](50) NULL,
	[save030_representative_condition_type_code] [nvarchar](50) NULL,
	[wizard] [nvarchar](300) NULL,
	[rule_number] [nvarchar](50) NULL,
	[save030_search_type_code] [nvarchar](50) NULL,
 CONSTRAINT [PK_eForm_Save030] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[eForm_Save030] ON 

INSERT [dbo].[eForm_Save030] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save030_informer_type_code], [save030_contact_type_code], [evidence_file_id], [is_8_1], [is_8_2], [is_8_3], [is_8_4], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [appeal_order], [contract_ref_number], [challenger_ref_number], [save030_representative_condition_type_code], [wizard], [rule_number], [save030_search_type_code]) VALUES (1, NULL, N'2006000001', N'a@hotmail.com', N'33d378b4-f603-4064-b084-44b5f3082193', N'0812401841', N'', NULL, N'', CAST(4000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'', 0, CAST(N'2020-06-21T03:14:25.1809432' AS DateTime2), 1, CAST(N'2020-06-21T03:14:25.1809537' AS DateTime2), 1, NULL, NULL, NULL, N'AND_OR', N'1|1|1|1', NULL, N'REQUEST_NUMBER')
INSERT [dbo].[eForm_Save030] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save030_informer_type_code], [save030_contact_type_code], [evidence_file_id], [is_8_1], [is_8_2], [is_8_3], [is_8_4], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [appeal_order], [contract_ref_number], [challenger_ref_number], [save030_representative_condition_type_code], [wizard], [rule_number], [save030_search_type_code]) VALUES (2, NULL, N'2006000010', N'asd@hotmail.com', N'6f29589b-9b9b-444a-965c-f0f391e2d4dd', N'0128401841', N'', NULL, N'', CAST(4000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'', 0, CAST(N'2020-06-21T10:52:58.2037568' AS DateTime2), 1, CAST(N'2020-06-21T10:52:58.2037775' AS DateTime2), 1, NULL, NULL, NULL, N'AND_OR', N'1|1|1|1', NULL, N'REQUEST_NUMBER')
INSERT [dbo].[eForm_Save030] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save030_informer_type_code], [save030_contact_type_code], [evidence_file_id], [is_8_1], [is_8_2], [is_8_3], [is_8_4], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [appeal_order], [contract_ref_number], [challenger_ref_number], [save030_representative_condition_type_code], [wizard], [rule_number], [save030_search_type_code]) VALUES (3, NULL, N'2006000015', N'123@hotmail.com', NULL, N'0018241024', N'request001', NULL, N'11', CAST(4000.00 AS Decimal(18, 2)), NULL, N'APPEAL', NULL, 1, 1, NULL, NULL, N'0', N'', 0, CAST(N'0001-01-01T00:00:00.0000000' AS DateTime2), 0, CAST(N'2020-06-21T11:25:43.6061124' AS DateTime2), 1, NULL, NULL, NULL, N'AND_OR', N'10|10|1|1', NULL, N'REQUEST_NUMBER')
INSERT [dbo].[eForm_Save030] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save030_informer_type_code], [save030_contact_type_code], [evidence_file_id], [is_8_1], [is_8_2], [is_8_3], [is_8_4], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [appeal_order], [contract_ref_number], [challenger_ref_number], [save030_representative_condition_type_code], [wizard], [rule_number], [save030_search_type_code]) VALUES (4, NULL, N'2006000023', N'shamalahlah@hotmail.com', N'e077e9c5-bccf-4e7a-84cc-169e55741635', N'0987654321', N'', NULL, N'', CAST(4000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'', 0, CAST(N'2020-06-22T13:45:53.6090557' AS DateTime2), 1, CAST(N'2020-06-22T13:45:53.6090680' AS DateTime2), 1, NULL, NULL, NULL, N'AND_OR', N'1|1|1|1', NULL, N'REQUEST_NUMBER')
INSERT [dbo].[eForm_Save030] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save030_informer_type_code], [save030_contact_type_code], [evidence_file_id], [is_8_1], [is_8_2], [is_8_3], [is_8_4], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [appeal_order], [contract_ref_number], [challenger_ref_number], [save030_representative_condition_type_code], [wizard], [rule_number], [save030_search_type_code]) VALUES (5, NULL, N'2006000024', N'shamalahlah@hotmail.com', N'8db21d5b-cff3-49e9-af2e-b6e1ea913260', N'0987654321', N'', NULL, N'', CAST(4000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'', 0, CAST(N'2020-06-22T13:52:41.4924623' AS DateTime2), 1, CAST(N'2020-06-22T13:52:41.4924679' AS DateTime2), 1, NULL, NULL, NULL, N'AND_OR', N'1|1|1|1', NULL, N'REQUEST_NUMBER')
INSERT [dbo].[eForm_Save030] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save030_informer_type_code], [save030_contact_type_code], [evidence_file_id], [is_8_1], [is_8_2], [is_8_3], [is_8_4], [sign_inform_person_list], [sign_inform_representative_list], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [appeal_order], [contract_ref_number], [challenger_ref_number], [save030_representative_condition_type_code], [wizard], [rule_number], [save030_search_type_code]) VALUES (6, NULL, N'2006000026', N'shamalahlah@hotmail.com', N'd9f0b554-be71-4fcd-ac02-3a968f4b394e', N'0987654321', N'', NULL, N'', CAST(4000.00 AS Decimal(18, 2)), NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'', N'', 0, CAST(N'2020-06-22T13:54:41.1455372' AS DateTime2), 1, CAST(N'2020-06-22T13:54:41.1455413' AS DateTime2), 1, NULL, NULL, NULL, N'AND_OR', N'1|1|1|1', NULL, N'REQUEST_NUMBER')
SET IDENTITY_INSERT [dbo].[eForm_Save030] OFF
ALTER TABLE [dbo].[eForm_Save030] ADD  CONSTRAINT [DF_eForm_Save030_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save030] ADD  CONSTRAINT [DF_eForm_Save030_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save030] ADD  CONSTRAINT [DF_eForm_Save030_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save030]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save030_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save030_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save030] CHECK CONSTRAINT [FK_eForm_Save030_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save030]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save030_RM_Save030SearchTypeCode] FOREIGN KEY([save030_search_type_code])
REFERENCES [dbo].[RM_Save030SearchTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save030] CHECK CONSTRAINT [FK_eForm_Save030_RM_Save030SearchTypeCode]
GO
