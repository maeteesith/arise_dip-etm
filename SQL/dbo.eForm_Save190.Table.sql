USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save190]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save190](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save190_informer_type_code] [nvarchar](50) NULL,
	[save190_request_type_code] [nvarchar](50) NULL,
	[sign_inform_person_list] [nvarchar](500) NULL,
	[postpone_day] [int] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[save190_representative_condition_type_code] [nvarchar](50) NULL,
	[wizard] [nvarchar](300) NULL,
	[contract_ref_number] [nvarchar](50) NULL,
	[remark_5] [nvarchar](max) NULL,
	[remark_6] [nvarchar](max) NULL,
	[sign_inform_representative_list] [nvarchar](500) NULL,
	[sign_inform_others_list] [nvarchar](500) NULL,
	[save190_search_type_code] [nvarchar](50) NULL,
	[challenger_ref_number] [nvarchar](50) NULL,
	[rule_number] [nvarchar](50) NULL,
 CONSTRAINT [PK_eForm_Save190] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save190] ADD  CONSTRAINT [DF_eForm_Save190_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save190] ADD  CONSTRAINT [DF_eForm_Save190_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save190] ADD  CONSTRAINT [DF_eForm_Save190_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save190]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save190_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save190_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save190] CHECK CONSTRAINT [FK_eForm_Save190_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save190]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save190_RM_Save190RequestTypeCode] FOREIGN KEY([save190_request_type_code])
REFERENCES [dbo].[RM_Save190RequestTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save190] CHECK CONSTRAINT [FK_eForm_Save190_RM_Save190RequestTypeCode]
GO
