USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save190InformerChallengerTypeCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save190InformerChallengerTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save190InformerChallengerTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save190InformerChallengerTypeCode] ON 

INSERT [dbo].[RM_Save190InformerChallengerTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'CHALLENGER', N'ผู้คัดค้าน', 0, CAST(N'2020-04-28T05:32:53.8366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190InformerChallengerTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'OTHERS', N'อื่นๆ', 0, CAST(N'2020-04-28T05:33:08.5600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save190InformerChallengerTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'REPRESENTATIVE', N'ตัวแทน', 0, CAST(N'2020-04-28T05:33:01.3100000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save190InformerChallengerTypeCode] OFF
ALTER TABLE [dbo].[RM_Save190InformerChallengerTypeCode] ADD  CONSTRAINT [DF_RM_Save190InformerChallengerTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save190InformerChallengerTypeCode] ADD  CONSTRAINT [DF_RM_Save190InformerChallengerTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save190InformerChallengerTypeCode] ADD  CONSTRAINT [DF_RM_Save190InformerChallengerTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save190InformerChallengerTypeCode] ADD  CONSTRAINT [DF_RM_Save190InformerChallengerTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
