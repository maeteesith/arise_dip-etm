USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Request01ReceiptChangeCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Request01ReceiptChangeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Request01ReceiptChangeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Request01ReceiptChangeCode] ON 

INSERT [dbo].[RM_Request01ReceiptChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 5, N'APPROVED', N'การขอแก้ไขเรียบร้อย', 0, CAST(N'2020-03-23T17:07:15.7233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Request01ReceiptChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 4, N'RECEIPT_APPROVE_REJECT', N'ยกเลิกโดยเจ้าหน้าที่การเงิน', 0, CAST(N'2020-03-23T17:07:02.4933333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Request01ReceiptChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 3, N'RECEIPT_APPROVE_WAIT', N'รอเจ้าหน้าที่การเงินอนุมัติ', 0, CAST(N'2020-03-23T17:06:51.4333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Request01ReceiptChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 2, N'REQUEST_APPROVE_REJECT', N'ยกเลิกโดยนายทะเบียน', 0, CAST(N'2020-03-23T17:06:36.0366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Request01ReceiptChangeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'REQUEST_APPROVE_WAIT', N'รอนายทะเบียนอนุมัติ', 0, CAST(N'2020-03-23T17:02:08.0600000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Request01ReceiptChangeCode] OFF
ALTER TABLE [dbo].[RM_Request01ReceiptChangeCode] ADD  CONSTRAINT [DF_RM_Request01ReceiptChangeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Request01ReceiptChangeCode] ADD  CONSTRAINT [DF_RM_Request01ReceiptChangeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Request01ReceiptChangeCode] ADD  CONSTRAINT [DF_RM_Request01ReceiptChangeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Request01ReceiptChangeCode] ADD  CONSTRAINT [DF_RM_Request01ReceiptChangeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
