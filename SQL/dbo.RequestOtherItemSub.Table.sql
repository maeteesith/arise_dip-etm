USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RequestOtherItemSub]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestOtherItemSub](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_other_item_id] [bigint] NOT NULL,
	[eform_id] [bigint] NULL,
	[item_index] [int] NULL,
	[request_number] [nvarchar](50) NULL,
	[item_sub_type_1_code] [nvarchar](50) NULL,
	[product_count] [int] NULL,
	[total_price] [decimal](18, 2) NULL,
	[fine] [decimal](18, 2) NULL,
	[request_index] [nvarchar](50) NULL,
	[cancel_reason] [nvarchar](500) NULL,
	[status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestOtherItemSub] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RequestOtherItemSub] ADD  CONSTRAINT [DF_RequestOtherItemSub_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RequestOtherItemSub] ADD  CONSTRAINT [DF_RequestOtherItemSub_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RequestOtherItemSub] ADD  CONSTRAINT [DF_RequestOtherItemSub_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RequestOtherItemSub]  WITH CHECK ADD  CONSTRAINT [FK_RequestOtherItemSub_RequestOtherItem] FOREIGN KEY([request_other_item_id])
REFERENCES [dbo].[RequestOtherItem] ([id])
GO
ALTER TABLE [dbo].[RequestOtherItemSub] CHECK CONSTRAINT [FK_RequestOtherItemSub_RequestOtherItem]
GO
