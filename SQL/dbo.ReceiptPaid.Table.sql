USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[ReceiptPaid]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReceiptPaid](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[receipt_id] [bigint] NULL,
	[paid_channel_type_code] [nvarchar](50) NULL,
	[paid_reference_number_1] [nvarchar](50) NULL,
	[paid_reference_number_2] [nvarchar](50) NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_ReceiptPaid] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ReceiptPaid] ADD  CONSTRAINT [DF_ReceiptPaid_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[ReceiptPaid] ADD  CONSTRAINT [DF_ReceiptPaid_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[ReceiptPaid] ADD  CONSTRAINT [DF_ReceiptPaid_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[ReceiptPaid]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptPaid_ReceiptPaid] FOREIGN KEY([receipt_id])
REFERENCES [dbo].[Receipt] ([id])
GO
ALTER TABLE [dbo].[ReceiptPaid] CHECK CONSTRAINT [FK_ReceiptPaid_ReceiptPaid]
GO
ALTER TABLE [dbo].[ReceiptPaid]  WITH CHECK ADD  CONSTRAINT [FK_ReceiptPaid_RM_ReceiptChannelType] FOREIGN KEY([paid_channel_type_code])
REFERENCES [dbo].[RM_ReceiptChannelType] ([code])
GO
ALTER TABLE [dbo].[ReceiptPaid] CHECK CONSTRAINT [FK_ReceiptPaid_RM_ReceiptChannelType]
GO
