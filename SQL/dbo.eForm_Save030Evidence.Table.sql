USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save030Evidence]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save030Evidence](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[evidence_type] [nvarchar](1000) NULL,
	[evidence_subject] [nvarchar](1000) NULL,
	[number] [int] NULL,
	[note] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_eForm_Save030Evidence] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save030Evidence] ADD  CONSTRAINT [DF_eForm_Save030Evidence_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save030Evidence] ADD  CONSTRAINT [DF_eForm_Save030Evidence_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save030Evidence] ADD  CONSTRAINT [DF_eForm_Save030Evidence_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save030Evidence]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save030Evidence_eForm_Save030] FOREIGN KEY([save_id])
REFERENCES [dbo].[eForm_Save030] ([id])
GO
ALTER TABLE [dbo].[eForm_Save030Evidence] CHECK CONSTRAINT [FK_eForm_Save030Evidence_eForm_Save030]
GO
