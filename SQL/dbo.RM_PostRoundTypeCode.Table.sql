USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PostRoundTypeCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PostRoundTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[description] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PostRoundTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PostRoundTypeCode] ON 

INSERT [dbo].[RM_PostRoundTypeCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'INFORM_PAYMENT', N'แจ้งชำระเงิน', NULL, 0, CAST(N'2020-01-19T16:42:37.4266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundTypeCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'PUBLIC_OTHER_CANCEL', N'ยกเลิกประกาศ', NULL, 0, CAST(N'2020-01-19T16:43:15.8700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundTypeCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'PUBLIC_OTHER_EVIDENCE', N'รับหลักฐาน', NULL, 0, CAST(N'2020-01-19T16:42:59.2033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundTypeCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'PUBLIC_OTHER_EXTEND', N'ต่ออายุ', NULL, 0, CAST(N'2020-01-19T16:43:10.3733333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundTypeCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'PUBLIC_OTHER_FEE', N'ชำระเงินเพิ่มเติม', NULL, 0, CAST(N'2020-01-19T16:43:04.0600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundTypeCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'REGISTERED', N'รับจดทะเบียน
', NULL, 0, CAST(N'2020-01-19T16:42:49.6166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PostRoundTypeCode] ([id], [index], [code], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'SEND_MORE', N'ส่งเอกสารเพิ่ม', NULL, 0, CAST(N'2020-01-28T08:01:12.1233333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PostRoundTypeCode] OFF
ALTER TABLE [dbo].[RM_PostRoundTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PostRoundTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PostRoundTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PostRoundTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PostRoundTypeCode] ADD  CONSTRAINT [DF_RM_PostRoundTypeCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
