USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_PublicRole02DocumentPaymentStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_PublicRole02DocumentPaymentStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_PublicRole02DocumentPaymentStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ON 

INSERT [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 7, N'DONE', N'เสร็จสิ้น', 1, 0, CAST(N'2020-01-18T08:29:56.1000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 2, N'WAIT_1', N'รอออกหนังสือ 1', 1, 0, CAST(N'2020-01-18T08:29:48.3400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 4, N'WAIT_2', N'รอออกหนังสือ 2', 1, 0, CAST(N'2020-01-18T08:29:50.5400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 6, N'WAIT_CHANGE', N'รอแก้', 1, 0, CAST(N'2020-05-22T18:58:58.2500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'WAIT_PAYMENT_1', N'รอชำระค่าทำเนียม 1', 1, 0, CAST(N'2020-01-18T08:29:54.7233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 5, N'WAIT_PAYMENT_2', N'รอชำระค่าทำเนียม 2', 1, 0, CAST(N'2020-01-19T14:12:23.9333333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_PublicRole02DocumentPaymentStatusCode] OFF
ALTER TABLE [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02DocumentPaymentStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02DocumentPaymentStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02DocumentPaymentStatusCode_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02DocumentPaymentStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02DocumentPaymentStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_PublicRole02DocumentPaymentStatusCode] ADD  CONSTRAINT [DF_RM_PublicRole02DocumentPaymentStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
