USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestStatus]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestStatus](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_RequestStatus] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestStatus] ON 

INSERT [dbo].[RM_RequestStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'DELETE', N'ลบ', 0, CAST(N'2019-12-08T12:37:23.1633333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'DRAFT', N'ร่าง', 0, CAST(N'2019-12-08T09:01:09.4633333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'RECEIPT', N'ออกใบเสร็จ', 0, CAST(N'2019-12-08T12:37:18.3433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestStatus] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'SAVE', N'บันทึก', 0, CAST(N'2019-12-08T09:01:18.5066667' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_RequestStatus] OFF
ALTER TABLE [dbo].[RM_RequestStatus] ADD  CONSTRAINT [DF_RM_RequestStatus_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RequestStatus] ADD  CONSTRAINT [DF_RM_RequestStatus_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_RequestStatus] ADD  CONSTRAINT [DF_RM_RequestStatus_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_RequestStatus] ADD  CONSTRAINT [DF_RM_RequestStatus_created_by]  DEFAULT ((0)) FOR [created_by]
GO
