USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[parent_code] [nvarchar](50) NULL,
	[index] [int] NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ConsideringSimilarInstructionRuleStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ON 

INSERT [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ([id], [parent_code], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, NULL, 4, N'CANCEL', N'ยกเลิก', 0, CAST(N'2020-01-14T16:53:47.8100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ([id], [parent_code], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, NULL, 2, N'FAILED', N'หลุดพ้น', 0, CAST(N'2020-01-14T16:53:18.4766667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ([id], [parent_code], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, NULL, 5, N'NEW', N'ออกใหม่', 0, CAST(N'2020-01-14T16:54:07.4133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ([id], [parent_code], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, NULL, 3, N'PROCESSED', N'ดำเนินการแล้ว', 0, CAST(N'2020-01-14T16:53:36.8533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ([id], [parent_code], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, NULL, 1, N'WAIT_ACTION', N'ยังไม่หลุดพ้น', 0, CAST(N'2020-01-14T16:53:06.9233333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] OFF
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionRuleStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionRuleStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionRuleStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionRuleStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionRuleStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
