USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[PostRoundAddress]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PostRoundAddress](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[post_round_id] [bigint] NULL,
	[name] [nvarchar](1000) NULL,
	[address_information] [nvarchar](1000) NULL,
	[is_check] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_PostRoundAddress] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[PostRoundAddress] ADD  CONSTRAINT [DF_Table_1_is_deleted1_1]  DEFAULT ((0)) FOR [is_check]
GO
ALTER TABLE [dbo].[PostRoundAddress] ADD  CONSTRAINT [DF_PostRoundAddress_is_deleted]  DEFAULT ((1)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[PostRoundAddress] ADD  CONSTRAINT [DF_PostRoundAddress_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[PostRoundAddress] ADD  CONSTRAINT [DF_PostRoundAddress_created_by]  DEFAULT ((0)) FOR [created_by]
GO
