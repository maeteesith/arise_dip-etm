USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vPublicOther]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vPublicOther] as

select 
p.id,
--p.id post_round_1_id,
p2.id post_round_2_id,
isnull(p2.id, p.id) post_round_id,
pir.id post_round_instruction_rule_id,
sir.id save010_instruction_rule_id,
s.id save_id,
pr.id public_round_id,

s.request_number,

pr.public_start_date,
pr.public_end_date,

p.book_start_date book_start_1_date,
p.book_end_date book_end_1_date,
p.book_number book_number_1,
p.book_acknowledge_date book_acknowledge_1_date,
p.post_round_action_post_type_code post_round_action_post_type_1_code,
rm_prat.name post_round_action_post_type_1_name,
p.document_role02_receiver_by document_role02_receiver_1_by,
um_r2.name document_role02_receiver_1_by_name,

p2.book_start_date book_start_2_date,
p2.book_end_date book_end_2_date,
p2.book_number book_number_2,
p2.book_acknowledge_date book_acknowledge_2_date,
p2.post_round_action_post_type_code post_round_action_post_type_2_code,
rm_prat2.name post_round_action_post_type_2_name,
p2.document_role02_receiver_by document_role02_receiver_2_by,
um_r22.name document_role02_receiver_2_by_name,

p.document_role04_receive_date,
p.document_role05_receive_date,
p.document_role05_receive_send_date,
p.document_role02_receive_date,

(select top 1 name from vSaveAddressAll a where a.save_id = s.id) name,
(select top 1 address_information from vSaveAddressAll a where a.save_id = s.id) address_information,

p.document_role04_receiver_by,
um_r4.name document_role04_receiver_by_name,
p.document_role05_receiver_by,
um_r5.name document_role05_receiver_by_name,
isnull(p2.document_role02_receiver_by, p.document_role02_receiver_by) document_role02_receiver_by,
isnull(um_r22.name, um_r2.name) document_role02_receiver_by_name,

p.post_round_instruction_rule_code,
rm_i.name post_round_instruction_rule_name,
p.post_round_type_code,
rm_p.name post_round_type_name,

p.document_role04_receive_status_code,
rm_r4r.name document_role04_receive_status_name,
p.document_role05_receive_status_code,
rm_r5r.name document_role05_receive_status_name,
isnull(p2.document_role02_receive_status_code, p.document_role02_receive_status_code) document_role02_receive_status_code,
isnull(rm_r2r2.name, rm_r2r.name) document_role02_receive_status_name,

pir.value_01,
pir.value_02,
pir.value_03,
pir.value_04,
pir.value_05,

'' document_other_remark,

p.is_deleted,
p.created_by,
p.created_date,
p.updated_by,
p.updated_date

from PostRound p
join PostRoundInstructionRule pir on pir.post_round_id = p.id
left join Save010InstructionRule sir on sir.id = pir.save010_instruction_rule_id
join Save010 s on s.id = p.object_id
join PublicRound pr on pr.id = s.public_round_id

left join PostRound p2 on p2.object_id = p.object_id and p2.round_index = 2

left join UM_User um_r4 on um_r4.id = p.document_role04_receiver_by
left join UM_User um_r5 on um_r5.id = p.document_role05_receiver_by
left join UM_User um_r2 on um_r2.id = p.document_role02_receiver_by
left join UM_User um_r22 on um_r22.id = p2.document_role02_receiver_by

left join RM_PostRoundInstructionRuleCode rm_i on rm_i.code = p.post_round_instruction_rule_code
left join RM_PostRoundTypeCode rm_p on rm_p.code = p.post_round_type_code
left join RM_DocumentRole04ReceiveStatusCode rm_r4r on rm_r4r.code = p.document_role04_receive_status_code
left join RM_DocumentRole05ReceiveStatusCode rm_r5r on rm_r5r.code = p.document_role05_receive_status_code
left join RM_DocumentRole02ReceiveStatusCode rm_r2r on rm_r2r.code = p.document_role02_receive_status_code
left join RM_DocumentRole02ReceiveStatusCode rm_r2r2 on rm_r2r2.code = p2.document_role02_receive_status_code

left join RM_PostRoundActionPostTypeCode rm_prat on rm_prat.code = p.post_round_action_post_type_code
left join RM_PostRoundActionPostTypeCode rm_prat2 on rm_prat2.code = p2.post_round_action_post_type_code

where p.round_index = 1 and p.post_round_type_code like 'PUBLIC_OTHER%'

--delete PostRound where post_round_instruction_rule_code = 'RULE_OTHER_5_8_PAYMENT'
GO
