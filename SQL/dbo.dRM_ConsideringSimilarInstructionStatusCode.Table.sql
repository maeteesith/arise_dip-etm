USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[dRM_ConsideringSimilarInstructionStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dRM_ConsideringSimilarInstructionStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[parent_code] [nvarchar](50) NULL,
	[index] [int] NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ConsideringSimilarInstructionStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dRM_ConsideringSimilarInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[dRM_ConsideringSimilarInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[dRM_ConsideringSimilarInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[dRM_ConsideringSimilarInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[dRM_ConsideringSimilarInstructionStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarInstructionStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
