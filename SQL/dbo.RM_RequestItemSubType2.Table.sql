USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_RequestItemSubType2]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_RequestItemSubType2](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestItemSubType2] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_RequestItemSubType2] ON 

INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'1', N'ผลิตภัณฑ์เคมีที่ใช้ในอุตสาหกรรม วิทยาศาสตร์ การถ่ายภาพ เกษตรกรรม การทำสวน การป่า...', 0, CAST(N'2019-12-17T03:26:50.4833333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1, N'10', N'เครื่องมือและอุปรณ์ที่ใช้ในทางศัลยกรรม อายุกรรม ทันตกรรมและสัตวแพทย์ (ทั้งแขน ขา...', 0, CAST(N'2019-12-17T03:26:55.6433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 1, N'11', N'ส่วนประกอบที่ใช้ในการติดตั้งอุปกรณ์ที่ให้แสงสว่าง ความร้อน กำเนิดไอน้ำปรุงอาหารท...', 0, CAST(N'2019-12-17T03:26:56.1933333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, 1, N'12', N'ยานพาหนะ เครื่องที่ใช้สำหรับเดินทางโดยทางบก ทางอากาศและทางน้ำ...', 0, CAST(N'2019-12-17T03:26:56.7400000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (13, 1, N'13', N'อาวุธปืน อุทธโธปกรณ์ที่ใช้ในการยิงกระสุน วัตถุระเบิด ดอกไม้ไฟ...', 0, CAST(N'2019-12-17T03:26:57.2700000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (14, 1, N'14', N'โลหะมีค่าและโลหะผสม และสิ่งซึ่งมีส่วนผสม ของหรือเคลือบด้วยโลหะมีค่า(ยกเว้นเครื่...', 0, CAST(N'2019-12-17T03:26:57.8066667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (15, 1, N'15', N'เครื่องดนตรี (เว้นแต่เครื่องพูดติดต่อและอุปรณ์ที่ไม่ใช้สาย)...', 0, CAST(N'2019-12-17T03:26:58.3166667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (16, 1, N'16', N'กระดาษและผลิตภัณฑ์กระดาษ กระดาษแข็งและผลิตภัณฑ์กระดาษแข็ง สิ่งตีพิมพ์ หนังสือพิม...', 0, CAST(N'2019-12-17T03:26:58.8466667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (17, 1, N'17', N'ยางกัดตะเพอชา ยางลบ ยางบาลาทาและสิ่งที่ใช้แทนยางบาลาทา วัสดุซึ่งทำจากสารเหล่านี้...', 0, CAST(N'2019-12-17T03:26:59.3733333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (18, 1, N'18', N'หนังฟอกและหนังเทียม และสิ่งที่ทำด้วยหนัง หรือหนังเทียมที่ไม่อยู่ในจำพวกอื่น หนัง...', 0, CAST(N'2019-12-17T03:26:59.9100000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (19, 1, N'19', N'วัสดุก่อสร้าง หินธรรมชาติ และหินเทียม ซีเมนต์ ปูนขาว ปูนพลาสเตอร์ และกรวด ท่อดิน...', 0, CAST(N'2019-12-17T03:27:00.5333333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'2', N'สีทา น้ำมันเคลือบทำให้เกิดเงา แล็คเกอร์ สารกันสนิมและสารป้องกันการผุของเนื้อไม้ว...', 0, CAST(N'2019-12-17T03:26:51.0666667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (20, 1, N'20', N'เครื่องเฟอร์นิเจอร์ กระจก กรอบรูป ของ (ที่ไม่อยู่จำพวกอื่น) ที่ทำด้วยไม้ ไม้ก๊อก...', 0, CAST(N'2019-12-17T03:27:01.1066667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (21, 1, N'21', N'เครื่องใช้และภาชนะขนาดเล็ก (ซึ่งไม่ได้ทำหรือเคลือบด้วยโลหะมีค่า) หวีและฟองน้ำ แป...', 0, CAST(N'2019-12-17T03:27:01.6900000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (22, 1, N'22', N'เชือก สายป่าน ตาข่าย เต๊นท์ ผ้าใบบังแดด ผ้าคลุม ผ้าใบเรือ กระสอบ วัสดุทำเบาะและย...', 0, CAST(N'2019-12-17T03:27:02.2566667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (23, 1, N'23', N'เส้นด้ายดิบ เส้นด้ายหลอด...', 0, CAST(N'2019-12-17T03:27:02.8200000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (24, 1, N'24', N'ผ้า (ชนิดที่เป็นชิ้น ๆ ) ผ้าคลุมเตียงและผ้าคลุมโต๊ะ และสิ่งทอที่ไม่อยู่ในจำพวกอื...', 0, CAST(N'2019-12-17T03:27:03.3466667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (25, 1, N'25', N'เสื้อผ้ารวมทั้งรองเท้าบู๊ท รองเท้าและรอง เท้าแตะ...', 0, CAST(N'2019-12-17T03:27:03.8933333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (26, 1, N'26', N'ลูกไม้และผลิตภัณฑ์เย็บปักถักร้อย ริบบิ้น และดิ้นเงินดิ้นทอง กระดุม ตะขอ และห่วง ...', 0, CAST(N'2019-12-17T03:27:04.4300000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (27, 1, N'27', N'พรม พรมผืนเล็ก ๆ เสื่อและเครื่องปูลาด พรมน้ำมันและวัสดุอื่นที่ใช้ปูพื้นห้อง ม่าน...', 0, CAST(N'2019-12-17T03:27:04.9566667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (28, 1, N'28', N'เครื่องเล่นเกมส์และของเล่น อุปกรณ์เล่นยิมนาสติคและกีฬา (ยกเว้นเสื้อผ้า) สิ่งที่ใ...', 0, CAST(N'2019-12-17T03:27:05.5033333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (29, 1, N'29', N'เนื้อสัตว์ เนื้อปลา เนื้อเบ็ดและไก่ และเนื้อสัตว์ล่า เนื้อสกัด ผลไม้และผักที่ได้...', 0, CAST(N'2019-12-17T03:27:06.0433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'3', N'วัสดุที่เตรียมสำหรับใช้ในการชักฟอกและสารที่ใช้ในการชักรีด วัสดุที่เตรียมสำหรับใช...', 0, CAST(N'2019-12-17T03:26:51.6433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (30, 1, N'30', N'กาแฟ ชา โกโก้ น้ำตาล ข้าว มันสำปะหลัง สา คู สารที่ใช้แทนกาแฟ แป้งและสิ่งที่ทำจ...', 0, CAST(N'2019-12-17T03:27:06.5566667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (31, 1, N'31', N'ผลิตภัณฑ์และผลิตผลทางเกษตร สวนครัวและป่า ไม้ซึ่งไม่อยู่ในจำพวกอื่นสัตว์ที่มีชีว...', 0, CAST(N'2019-12-17T03:27:07.1033333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (32, 1, N'32', N'เบียร์ เหล้าเอล เบียร์ดำ น้ำแร่และน้ำอัด ลมและเครื่องดื่มไม่ผสมแอลกอฮอล์อื่น ๆ ...', 0, CAST(N'2019-12-17T03:27:07.7733333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (33, 1, N'33', N'เหล้าองุ่น เหล้า...', 0, CAST(N'2019-12-17T03:27:08.3300000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (34, 1, N'34', N'ยาสูบทั้งที่ยังเป็นวัตถุดิบและผลิตแล้ว อุปกรณ์ที่ใช้ในการสูบบุหรี่ ไม้ขีดไฟ...', 0, CAST(N'2019-12-17T03:27:08.8833333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (35, 1, N'35', N'การโฆษณาและธุรกิจ...', 0, CAST(N'2019-12-17T03:27:09.4433333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (36, 1, N'36', N'การประกันภัยและการเงิน...', 0, CAST(N'2019-12-17T03:27:10.0600000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (37, 1, N'37', N'การก่อสร้างและซ่อมแซม...', 0, CAST(N'2019-12-17T03:27:10.6400000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (38, 1, N'38', N'การสื่อสาร...', 0, CAST(N'2019-12-17T03:27:11.2000000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (39, 1, N'39', N'การขนส่งและจัดเก็บ...', 0, CAST(N'2019-12-17T03:27:11.7600000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'4', N'น้ำมันและน้ำมันชนิดหนาที่ใช้ในอุตสาหกรรม(เว้นแต่น้ำมันที่มีไขมัน และน้ำมันหอมระเ...', 0, CAST(N'2019-12-17T03:26:52.2300000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (40, 1, N'40', N'การจัดการเกี่ยวกับวัสดุ...', 0, CAST(N'2019-12-17T03:27:12.3566667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (41, 1, N'41', N'การศึกษาและบันเทิง...', 0, CAST(N'2019-12-17T03:27:12.9133333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (42, 1, N'42', N'การบริการด้านวิทยาศาสตร์และเทคโนโลยี รวมทั้งการวิจัยและออกแบบที่เกี่ยวข้อง...', 0, CAST(N'2019-12-17T03:27:13.4700000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (43, 1, N'43', N'การบริการด้านการจักหาอาหารและเครื่องดื่ม การจัดที่พักชั่วคราว...', 0, CAST(N'2019-12-17T03:27:13.9866667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (44, 1, N'44', N'การบริการรักษาพยาบาล การบริการด้านสัตวแพทย์ การดูแลรักษาสุขอนามัยและความงามของมน...', 0, CAST(N'2019-12-17T03:27:14.5400000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (45, 1, N'45', N'การบริการทั่วไปตามความประสงค์ของบุคคลและสังคม การบริการรักษาความปลอดภัยแก่ทรัพย์...', 0, CAST(N'2019-12-17T03:27:15.0933333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (46, 1, N'46', N'คำขอจดทะเบียนเครื่องหมายรับรอง...', 0, CAST(N'2019-12-17T03:27:15.6366667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (47, 1, N'47', N'คำขอจดทะเบียนเครื่องหมายร่วม...', 0, CAST(N'2019-12-17T03:27:16.2166667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 1, N'5', N'สารที่ใช้เป็นยารักษาโรค รักษาสัตว์และรักษาความสะอาด อาหารสำหรับเด็กและคนไข้ พลาส...', 0, CAST(N'2019-12-17T03:26:52.8200000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 1, N'6', N'โลหะธรรมดาและโลหะผสม สมอ ทั่งตีเหล็ก ระฆ ัง วัสดุก่อสร้างทั้งชนิดม้วนและหล่อรางแ...', 0, CAST(N'2019-12-17T03:26:53.3866667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'7', N'เครื่องจักรและเครื่องมือ มอเตอร์ (เว้นแต่มอเตอร์ที่ใช้สำหรับยานพาหนะทางบก) เครื่...', 0, CAST(N'2019-12-17T03:26:53.9766667' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 1, N'8', N'เครื่องมือขนาดเล็กที่ใช้มือและอุปกรณ์ เครื่องตัด ส้อมและช้อน อาวุที่ใช้สะพาย...', 0, CAST(N'2019-12-17T03:26:54.5600000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_RequestItemSubType2] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 1, N'9', N'เครื่องมือและอุปกรณ์ทางวิทยาศาสตร์ การเดินเรือ การสำรวจและไฟฟ้า (รวมทั้งชนิดที่ไ...', 0, CAST(N'2019-12-17T03:26:55.1133333' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_RequestItemSubType2] OFF
ALTER TABLE [dbo].[RM_RequestItemSubType2] ADD  CONSTRAINT [DF_RM_RequestItemSubType2_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_RequestItemSubType2] ADD  CONSTRAINT [DF_RequestItemSubType2_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_RequestItemSubType2] ADD  CONSTRAINT [DF_RequestItemSubType2_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_RequestItemSubType2] ADD  CONSTRAINT [DF_RequestItemSubType2_created_by]  DEFAULT ((0)) FOR [created_by]
GO
