USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ConsideringSimilarDocumentStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ConsideringSimilarDocumentStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ConsideringSimilarDocumentStatusCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarDocumentStatusCode] ON 

INSERT [dbo].[RM_ConsideringSimilarDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'ALLOW', N'อนุญาต', 0, CAST(N'2020-01-14T16:19:45.5833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 5, N'ALLOW_SOME', N'อนุญาตบางข้อ', 0, CAST(N'2020-01-14T16:20:40.2400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 4, N'INCLUDE', N'รวมเรื่อง', 0, CAST(N'2020-01-14T16:20:28.0800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 3, N'KNOW', N'รับทราบ', 0, CAST(N'2020-01-14T16:20:19.7666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'NOT_ALLOW', N'ไม่อนุญาต', 0, CAST(N'2020-01-14T16:19:49.7233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarDocumentStatusCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 6, N'WAIT_CONSIDER', N'รอพิจารณา', 0, CAST(N'2020-01-14T16:20:53.6133333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarDocumentStatusCode] OFF
ALTER TABLE [dbo].[RM_ConsideringSimilarDocumentStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarDocumentStatusCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarDocumentStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarDocumentStatusCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarDocumentStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarDocumentStatusCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarDocumentStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarDocumentStatusCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarDocumentStatusCode] ADD  CONSTRAINT [DF_RM_ConsideringSimilarDocumentStatusCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
