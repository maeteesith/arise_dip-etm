USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save040Product]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save040Product](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[request_item_sub_type_1_code] [nvarchar](50) NULL,
	[request_item_sub_type_2_code] [nvarchar](50) NULL,
	[description] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save040Product] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save040Product] ADD  CONSTRAINT [DF_Save040Product_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save040Product] ADD  CONSTRAINT [DF_Save040Product_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save040Product] ADD  CONSTRAINT [DF_Save040Product_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Save040Product]  WITH CHECK ADD  CONSTRAINT [FK_Save040Product_Save040] FOREIGN KEY([save_id])
REFERENCES [dbo].[Save040] ([id])
GO
ALTER TABLE [dbo].[Save040Product] CHECK CONSTRAINT [FK_Save040Product_Save040]
GO
