USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save010DocumentClassificationSoundType]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save010DocumentClassificationSoundType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save010DocumentClassificationSound] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save010DocumentClassificationSoundType] ON 

INSERT [dbo].[RM_Save010DocumentClassificationSoundType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'1', N'เสียงคน', 0, CAST(N'2020-01-01T19:02:04.5166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'2', N'เสียงสัตว์', 0, CAST(N'2020-01-01T19:02:05.0333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'3', N'เสียงเพลง/เสียงดนตรี', 0, CAST(N'2020-01-01T19:02:05.5466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationSoundType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'4', N'เสียงอื่นๆ', 0, CAST(N'2020-01-01T19:02:06.0633333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save010DocumentClassificationSoundType] OFF
ALTER TABLE [dbo].[RM_Save010DocumentClassificationSoundType] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSound_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationSoundType] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSound_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationSoundType] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSound_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationSoundType] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSound_created_by]  DEFAULT ((0)) FOR [created_by]
GO
