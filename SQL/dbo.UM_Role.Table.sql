USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[UM_Role]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UM_Role](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[description] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[UM_Role] ON 

INSERT [dbo].[UM_Role] ([id], [name], [description], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, N'Administrator', NULL, 0, CAST(N'2019-12-10T16:42:51.3700000' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[UM_Role] OFF
ALTER TABLE [dbo].[UM_Role] ADD  CONSTRAINT [DF_UM_Role_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[UM_Role] ADD  CONSTRAINT [DF_UM_Role_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[UM_Role] ADD  CONSTRAINT [DF_UM_Role_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
