USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010InstructionRule]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010InstructionRule](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[instruction_date] [datetime2](7) NULL,
	[instruction_rule_code] [nvarchar](50) NULL,
	[rule_remark] [nvarchar](1000) NULL,
	[rule_description] [nvarchar](1000) NULL,
	[considering_instruction_status_code] [nvarchar](50) NULL,
	[considering_book_status_code] [nvarchar](50) NULL,
	[instruction_send_date] [datetime2](7) NULL,
	[instruction_number] [nvarchar](50) NULL,
	[considering_instruction_rule_status_code] [nvarchar](50) NULL,
	[change_status_date] [datetime2](7) NULL,
	[change_reason] [nvarchar](500) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[value_4] [nvarchar](1000) NULL,
	[value_5] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010InstructionRule] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010InstructionRule] ADD  CONSTRAINT [DF_Save010InstructionRule_considering_instruction_status_code]  DEFAULT (N'DRAFT') FOR [considering_instruction_status_code]
GO
ALTER TABLE [dbo].[Save010InstructionRule] ADD  CONSTRAINT [DF_Save010InstructionRule_considering_book_status_code]  DEFAULT (N'NORMAL') FOR [considering_book_status_code]
GO
ALTER TABLE [dbo].[Save010InstructionRule] ADD  CONSTRAINT [DF_Save010InstructionRule_considering_instruction_status_code1]  DEFAULT (N'WAIT_ACTION') FOR [considering_instruction_rule_status_code]
GO
ALTER TABLE [dbo].[Save010InstructionRule] ADD  CONSTRAINT [DF_Save010InstructionRule_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010InstructionRule] ADD  CONSTRAINT [DF_Save010InstructionRule_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010InstructionRule] ADD  CONSTRAINT [DF_Save010InstructionRule_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Save010InstructionRule]  WITH NOCHECK ADD  CONSTRAINT [FK_Save010InstructionRule_RM_ConsideringBookStatusCode] FOREIGN KEY([considering_book_status_code])
REFERENCES [dbo].[RM_ConsideringBookStatusCode] ([code])
GO
ALTER TABLE [dbo].[Save010InstructionRule] CHECK CONSTRAINT [FK_Save010InstructionRule_RM_ConsideringBookStatusCode]
GO
ALTER TABLE [dbo].[Save010InstructionRule]  WITH CHECK ADD  CONSTRAINT [FK_Save010InstructionRule_RM_ConsideringInstructionStatusCode] FOREIGN KEY([considering_instruction_status_code])
REFERENCES [dbo].[RM_ConsideringInstructionStatusCode] ([code])
GO
ALTER TABLE [dbo].[Save010InstructionRule] CHECK CONSTRAINT [FK_Save010InstructionRule_RM_ConsideringInstructionStatusCode]
GO
ALTER TABLE [dbo].[Save010InstructionRule]  WITH NOCHECK ADD  CONSTRAINT [FK_Save010InstructionRule_RM_ConsideringSimilarInstructionRuleCode] FOREIGN KEY([instruction_rule_code])
REFERENCES [dbo].[RM_ConsideringSimilarInstructionRuleCode] ([code])
GO
ALTER TABLE [dbo].[Save010InstructionRule] NOCHECK CONSTRAINT [FK_Save010InstructionRule_RM_ConsideringSimilarInstructionRuleCode]
GO
ALTER TABLE [dbo].[Save010InstructionRule]  WITH CHECK ADD  CONSTRAINT [FK_Save010InstructionRule_RM_ConsideringSimilarInstructionRuleStatusCode] FOREIGN KEY([considering_instruction_rule_status_code])
REFERENCES [dbo].[RM_ConsideringSimilarInstructionRuleStatusCode] ([code])
GO
ALTER TABLE [dbo].[Save010InstructionRule] CHECK CONSTRAINT [FK_Save010InstructionRule_RM_ConsideringSimilarInstructionRuleStatusCode]
GO
ALTER TABLE [dbo].[Save010InstructionRule]  WITH CHECK ADD  CONSTRAINT [FK_Save010InstructionRule_Save010] FOREIGN KEY([save_id])
REFERENCES [dbo].[Save010] ([id])
GO
ALTER TABLE [dbo].[Save010InstructionRule] CHECK CONSTRAINT [FK_Save010InstructionRule_Save010]
GO
