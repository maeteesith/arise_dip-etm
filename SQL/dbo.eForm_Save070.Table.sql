USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save070]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save070](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save070_request_item_type_code] [nvarchar](50) NULL,
	[save070_contact_type_code] [nvarchar](50) NULL,
	[save070_extend_type_code] [nvarchar](50) NULL,
	[fee] [float] NULL,
	[fine] [float] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[save070_submit_type_code] [nvarchar](50) NULL,
	[sign_inform_person_list] [nvarchar](500) NULL,
	[sign_inform_representative_list] [nvarchar](500) NULL,
	[save070_representative_condition_type_code] [nvarchar](50) NULL,
	[wizard] [nvarchar](300) NULL,
	[rule_number] [nvarchar](50) NULL,
 CONSTRAINT [PK_eForm_Save070] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[eForm_Save070] ON 

INSERT [dbo].[eForm_Save070] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save070_request_item_type_code], [save070_contact_type_code], [save070_extend_type_code], [fee], [fine], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save070_submit_type_code], [sign_inform_person_list], [sign_inform_representative_list], [save070_representative_condition_type_code], [wizard], [rule_number]) VALUES (1, NULL, N'2006000004', N'200@hotmail.com', N'35b05b00-0cc0-496f-b1bf-31b8d275bc15', N'0120481240', NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T04:36:43.1799323' AS DateTime2), 1, CAST(N'2020-06-21T04:36:43.1799394' AS DateTime2), 1, NULL, NULL, NULL, N'AND_OR', NULL, NULL)
INSERT [dbo].[eForm_Save070] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save070_request_item_type_code], [save070_contact_type_code], [save070_extend_type_code], [fee], [fine], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [save070_submit_type_code], [sign_inform_person_list], [sign_inform_representative_list], [save070_representative_condition_type_code], [wizard], [rule_number]) VALUES (2, NULL, N'2006000006', N'21@hotmail.com', N'cb7ac32f-3548-4cee-9d45-6d6d77ef2ea0', N'0811111111', NULL, NULL, N'', NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T04:51:31.6108390' AS DateTime2), 1, CAST(N'2020-06-21T11:01:31.6904403' AS DateTime2), 1, NULL, NULL, NULL, N'AND_OR', NULL, NULL)
SET IDENTITY_INSERT [dbo].[eForm_Save070] OFF
ALTER TABLE [dbo].[eForm_Save070] ADD  CONSTRAINT [DF_eForm_Save070_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save070] ADD  CONSTRAINT [DF_eForm_Save070_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save070] ADD  CONSTRAINT [DF_eForm_Save070_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save070]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save070_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save070_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save070] CHECK CONSTRAINT [FK_eForm_Save070_RM_AddressRepresentativeConditionTypeCode]
GO
ALTER TABLE [dbo].[eForm_Save070]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save070_RM_AddressType] FOREIGN KEY([save070_contact_type_code])
REFERENCES [dbo].[RM_AddressType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save070] CHECK CONSTRAINT [FK_eForm_Save070_RM_AddressType]
GO
ALTER TABLE [dbo].[eForm_Save070]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save070_RM_RequestItemType] FOREIGN KEY([save070_request_item_type_code])
REFERENCES [dbo].[RM_RequestItemType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save070] CHECK CONSTRAINT [FK_eForm_Save070_RM_RequestItemType]
GO
ALTER TABLE [dbo].[eForm_Save070]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save070_RM_Save070ExtendType] FOREIGN KEY([save070_extend_type_code])
REFERENCES [dbo].[RM_Save070ExtendType] ([code])
GO
ALTER TABLE [dbo].[eForm_Save070] CHECK CONSTRAINT [FK_eForm_Save070_RM_Save070ExtendType]
GO
