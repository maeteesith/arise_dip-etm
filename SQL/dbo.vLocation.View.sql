USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vLocation]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vLocation] as
select 
s.id,
s.[index],
s.code,
s.name,
z.code postal_code,
z.name postal_name,
d.code district_code,
d.name district_name,
p.code province_code,
p.name province_name,
c.code country_code,
c.name country_name,
c.is_deleted,
c.created_by,
c.created_date,
c.updated_by,
c.updated_date
from RM_AddressCountry c 
join RM_AddressProvince p on p.country_code = c.code
join RM_AddressDistrict d on d.province_code = p.code
join RM_AddressSubDistrict s on s.district_code = d.code
join RM_AddressPostal z on z.sub_district_code = s.code
GO
