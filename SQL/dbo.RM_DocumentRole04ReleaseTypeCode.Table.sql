USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole04ReleaseTypeCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole04ReleaseTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole04ReleaseTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04ReleaseTypeCode] ON 

INSERT [dbo].[RM_DocumentRole04ReleaseTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'BACK_STATUS', N'เสนอเพื่อย้อนสถานะ', 1, CAST(N'2020-06-14T18:32:52.0400000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 2, N'CHECKING', N'เสนอคืนนายทะเบียนตรวจสอบ', 0, CAST(N'2020-06-22T19:16:28.1933333' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 3, N'NEW', N'เสนอออกหนังสือใหม่', 0, CAST(N'2020-06-22T19:16:41.3266667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'RELEASE', N'เสนอจำหน่าย', 0, CAST(N'2020-06-22T19:16:05.9466667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'RELEASE_BACKUP', N'งานเสนอเพื่อทราบ', 1, CAST(N'2020-06-02T21:13:41.9366667' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04ReleaseTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 4, N'WAIT', N'เสนอรอพิจารณาคำขอ', 0, CAST(N'2020-06-22T19:16:45.0233333' AS DateTime2), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04ReleaseTypeCode] OFF
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole04ReleaseTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04ReleaseTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
