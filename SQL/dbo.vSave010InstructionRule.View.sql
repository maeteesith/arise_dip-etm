USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vSave010InstructionRule]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vSave010InstructionRule] as

select 
sr.id,

sr.save_id,

sr.instruction_date,
sr.instruction_send_date,

sr.instruction_rule_code,
rm_ir.name instruction_rule_name,

sr.value_1,

sr.created_by instructor_by_id,
um_i.name instructor_by_name,

sr.considering_instruction_rule_status_code,
rm_irs.name considering_instruction_rule_status_name,

0 file_id,

sr.is_deleted,
sr.created_by,
sr.created_date,
sr.updated_by,
sr.updated_date
from Save010InstructionRule sr
left join RM_ConsideringSimilarInstructionRuleCode rm_ir on rm_ir.code = sr.instruction_rule_code
left join UM_User um_i on um_i.id = sr.created_by
left join RM_ConsideringSimilarInstructionRuleStatusCode rm_irs on rm_irs.code = sr.considering_instruction_rule_status_code
GO
