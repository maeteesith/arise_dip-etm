USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Request01ItemSubChange]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Request01ItemSubChange](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request01_item_id] [bigint] NULL,
	[item_sub_index] [int] NULL,
	[item_sub_type_1_code] [nvarchar](50) NULL,
	[product_count] [int] NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_deleted] [bit] NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[source_id] [bigint] NULL,
	[request01_item_change_id] [bigint] NULL,
 CONSTRAINT [PK__Request0__3213E83FE12A58D2] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Request01ItemSubChange]  WITH CHECK ADD  CONSTRAINT [FK_Request01ItemSubChange_Request01ItemChange] FOREIGN KEY([request01_item_change_id])
REFERENCES [dbo].[Request01ItemChange] ([id])
GO
ALTER TABLE [dbo].[Request01ItemSubChange] CHECK CONSTRAINT [FK_Request01ItemSubChange_Request01ItemChange]
GO
