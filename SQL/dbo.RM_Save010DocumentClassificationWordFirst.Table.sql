USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save010DocumentClassificationWordFirst]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save010DocumentClassificationWordFirst](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save010DocumentClassificationWordFirstCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ON 

INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (17, 1, N'B', N'บ', 0, CAST(N'2020-03-25T10:26:14.3866667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, 1, N'CH', N'ฉ ช ฌ', 0, CAST(N'2020-03-25T10:26:12.0266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (13, 1, N'D', N'ด ฎ', 0, CAST(N'2020-03-25T10:26:12.6900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (20, 1, N'F', N'ฝ ฟ', 0, CAST(N'2020-03-25T10:26:15.3133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (25, 1, N'H', N'ห ฮ', 0, CAST(N'2020-03-25T10:26:16.9000000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 1, N'J', N'จ', 0, CAST(N'2020-03-25T10:26:11.5900000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 1, N'K', N'ก', 0, CAST(N'2020-03-25T10:26:09.3966667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 1, N'KH', N'ข ค ฆ', 0, CAST(N'2020-03-25T10:26:10.1833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (21, 1, N'M', N'ม', 0, CAST(N'2020-03-25T10:26:15.8233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (16, 1, N'N', N'น', 0, CAST(N'2020-03-25T10:26:13.9400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 1, N'NG', N'ง', 0, CAST(N'2020-03-25T10:26:10.9400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (18, 1, N'P', N'ป', 0, CAST(N'2020-03-25T10:26:14.7433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (19, 1, N'PH', N'ผ พ ภ', 0, CAST(N'2020-03-25T10:26:15.0233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (22, 1, N'R', N'ร ล ฤ ฬ', 0, CAST(N'2020-03-25T10:26:16.0433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (24, 1, N'S', N'ช(ทร) ศ ษ ส', 0, CAST(N'2020-03-25T10:26:16.5633333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (14, 1, N'T', N'ต', 0, CAST(N'2020-03-25T10:26:13.1033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (15, 1, N'TH', N'ฐ ฑ ฒ ท ธ', 0, CAST(N'2020-03-25T10:26:13.4966667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (23, 1, N'W', N'ว', 0, CAST(N'2020-03-25T10:26:16.2833333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, 1, N'Y', N'ญ ย', 0, CAST(N'2020-03-25T10:26:12.3466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (26, 1, N'อ', N'A E I O U', 0, CAST(N'2020-03-25T10:26:17.2066667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save010DocumentClassificationWordFirst] OFF
ALTER TABLE [dbo].[RM_Save010DocumentClassificationWordFirst] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationWordFirstCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationWordFirst] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationWordFirstCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationWordFirst] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationWordFirstCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save010DocumentClassificationWordFirst] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationWordFirstCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
