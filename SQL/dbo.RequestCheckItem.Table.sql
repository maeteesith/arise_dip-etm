USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RequestCheckItem]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RequestCheckItem](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_check_id] [bigint] NULL,
	[request_check_item_code] [nvarchar](50) NULL,
	[is_select] [bit] NULL,
	[value_other] [nvarchar](1000) NULL,
	[value_quantity] [int] NULL,
	[value_total_price] [decimal](18, 2) NULL,
	[is_check_3] [bit] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RequestCheckItem] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[RequestCheckItem] ADD  CONSTRAINT [DF_RequestCheckItem_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RequestCheckItem] ADD  CONSTRAINT [DF_RequestCheckItem_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RequestCheckItem] ADD  CONSTRAINT [DF_RequestCheckItem_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[RequestCheckItem]  WITH CHECK ADD  CONSTRAINT [FK_RequestCheckItem_RequestCheck] FOREIGN KEY([request_check_id])
REFERENCES [dbo].[RequestCheck] ([id])
GO
ALTER TABLE [dbo].[RequestCheckItem] CHECK CONSTRAINT [FK_RequestCheckItem_RequestCheck]
GO
