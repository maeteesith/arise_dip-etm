USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vDocumentRole04ReleaseNewInstructionRule]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vDocumentRole04ReleaseNewInstructionRule] as

select 

sir.id,
s.id save_id,
sir.id save010_instruction_rule_id,
sir.instruction_rule_code,

pr.*,

sir.is_deleted,
sir.created_by,
sir.created_date,
sir.updated_by,
sir.updated_date

from Save010 s
join Save010InstructionRule sir on sir.save_id = s.id

cross apply (
select top 1 pr.id post_round_id, pr.post_round_type_code , pr.round_index, pr.book_end_date, pr.post_round_action_post_type_code, pr.reference_number,
pr.book_number,
isnull(sir.value_1, pir.value_01) value_01,
isnull(sir.value_2, pir.value_02) value_02,
isnull(sir.value_3, pir.value_03) value_03,
isnull(sir.value_4, pir.value_04) value_04,
isnull(sir.value_5, pir.value_05) value_05
from PostRound pr
join PostRoundInstructionRule pir on pir.post_round_id = pr.id
where pir.save010_instruction_rule_id = sir.id
order by pr.id desc
) pr

--where --pr.round_index > 0 and
--pr.round_index = 2 and 
--pr.book_end_date > getdate() and 
--isnull(pr.post_round_action_post_type_code, 'NOT_COMPLETE') != 'OK'
GO
