USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010DocumentClassificationVersionSound]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010DocumentClassificationVersionSound](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[save_index] [int] NULL,
	[document_classification_sound_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_Save010DocumentClassificationVersionSound] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010DocumentClassificationVersionSound] ADD  CONSTRAINT [DF_Save010DocumentClassificationVersionSound_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010DocumentClassificationVersionSound] ADD  CONSTRAINT [DF_Save010DocumentClassificationVersionSound_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010DocumentClassificationVersionSound] ADD  CONSTRAINT [DF_Save010DocumentClassificationVersionSound_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Save010DocumentClassificationVersionSound]  WITH NOCHECK ADD  CONSTRAINT [FK_Save010DocumentClassificationVersionSound_RM_Save010DocumentClassificationSound] FOREIGN KEY([document_classification_sound_code])
REFERENCES [dbo].[RM_Save010DocumentClassificationSoundType] ([code])
GO
ALTER TABLE [dbo].[Save010DocumentClassificationVersionSound] NOCHECK CONSTRAINT [FK_Save010DocumentClassificationVersionSound_RM_Save010DocumentClassificationSound]
GO
ALTER TABLE [dbo].[Save010DocumentClassificationVersionSound]  WITH CHECK ADD  CONSTRAINT [FK_Save010DocumentClassificationVersionSound_Save010DocumentClassificationVersion] FOREIGN KEY([save_id])
REFERENCES [dbo].[Save010DocumentClassificationVersion] ([id])
GO
ALTER TABLE [dbo].[Save010DocumentClassificationVersionSound] CHECK CONSTRAINT [FK_Save010DocumentClassificationVersionSound_Save010DocumentClassificationVersion]
GO
