USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save021]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save021](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[make_date] [date] NULL,
	[eform_number] [nvarchar](50) NULL,
	[email] [nvarchar](50) NULL,
	[email_uuid] [nvarchar](50) NULL,
	[telephone] [nvarchar](50) NULL,
	[request_number] [nvarchar](50) NULL,
	[registration_number] [nvarchar](50) NULL,
	[payer_name] [nvarchar](1000) NULL,
	[total_price] [decimal](18, 2) NULL,
	[save021_contact_type_code] [nvarchar](50) NULL,
	[save021_remark_4] [nvarchar](1000) NULL,
	[kor_19_inform_person_type_code] [nvarchar](50) NULL,
	[kor_19_remark] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[is_6_1] [bit] NULL,
	[is_6_2] [bit] NULL,
	[is_6_3] [bit] NULL,
	[sign_inform_person_list] [nvarchar](300) NULL,
	[sign_inform_representative_list] [nvarchar](300) NULL,
	[argue_file_id] [bigint] NULL,
	[save021_representative_condition_type_code] [nvarchar](50) NULL,
	[wizard] [nvarchar](300) NULL,
	[rule_number] [nvarchar](50) NULL,
 CONSTRAINT [PK_eForm_Save021] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[eForm_Save021] ON 

INSERT [dbo].[eForm_Save021] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save021_contact_type_code], [save021_remark_4], [kor_19_inform_person_type_code], [kor_19_remark], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [is_6_1], [is_6_2], [is_6_3], [sign_inform_person_list], [sign_inform_representative_list], [argue_file_id], [save021_representative_condition_type_code], [wizard], [rule_number]) VALUES (1, NULL, N'2006000007', N'asd@hotmail.com', N'aa1f371a-c325-4a2f-a57b-b9adb8c7ec27', N'0182401841', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T05:02:01.5900820' AS DateTime2), 1, CAST(N'2020-06-21T05:02:01.5901038' AS DateTime2), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1|1|1|1', NULL)
INSERT [dbo].[eForm_Save021] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save021_contact_type_code], [save021_remark_4], [kor_19_inform_person_type_code], [kor_19_remark], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [is_6_1], [is_6_2], [is_6_3], [sign_inform_person_list], [sign_inform_representative_list], [argue_file_id], [save021_representative_condition_type_code], [wizard], [rule_number]) VALUES (2, NULL, N'2006000008', N'asd@hotmail.com', N'8975b35b-2bbe-4631-a8be-131e9cae9108', N'0182401841', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-21T05:02:25.7361903' AS DateTime2), 1, CAST(N'2020-06-21T05:02:25.7361983' AS DateTime2), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1|1|1|1', NULL)
INSERT [dbo].[eForm_Save021] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save021_contact_type_code], [save021_remark_4], [kor_19_inform_person_type_code], [kor_19_remark], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [is_6_1], [is_6_2], [is_6_3], [sign_inform_person_list], [sign_inform_representative_list], [argue_file_id], [save021_representative_condition_type_code], [wizard], [rule_number]) VALUES (3, NULL, NULL, N'testeform@hotmail.com', NULL, N'0822222222', N'request001', N'registration001', N'test', CAST(2000.00 AS Decimal(18, 2)), N'OWNER', N'123', NULL, N'123', 0, CAST(N'2020-06-22T12:06:04.2200000' AS DateTime2), 0, NULL, NULL, 1, 1, 0, N'0', N'0', NULL, N'AND', NULL, NULL)
INSERT [dbo].[eForm_Save021] ([id], [make_date], [eform_number], [email], [email_uuid], [telephone], [request_number], [registration_number], [payer_name], [total_price], [save021_contact_type_code], [save021_remark_4], [kor_19_inform_person_type_code], [kor_19_remark], [is_deleted], [created_date], [created_by], [updated_date], [updated_by], [is_6_1], [is_6_2], [is_6_3], [sign_inform_person_list], [sign_inform_representative_list], [argue_file_id], [save021_representative_condition_type_code], [wizard], [rule_number]) VALUES (4, NULL, N'2006000030', N'shamalahlah@hotmail.com', N'37d54ae0-781e-4a9a-939f-9de8eb2c0949', N'0987654321', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, CAST(N'2020-06-22T14:14:30.1491760' AS DateTime2), 1, CAST(N'2020-06-22T14:14:30.1491838' AS DateTime2), 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, N'1|1|1|1', NULL)
SET IDENTITY_INSERT [dbo].[eForm_Save021] OFF
ALTER TABLE [dbo].[eForm_Save021] ADD  CONSTRAINT [DF_eForm_Save021_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save021] ADD  CONSTRAINT [DF_eForm_Save021_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save021] ADD  CONSTRAINT [DF_eForm_Save021_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save021]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save021_RM_AddressRepresentativeConditionTypeCode] FOREIGN KEY([save021_representative_condition_type_code])
REFERENCES [dbo].[RM_AddressRepresentativeConditionTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save021] CHECK CONSTRAINT [FK_eForm_Save021_RM_AddressRepresentativeConditionTypeCode]
GO
