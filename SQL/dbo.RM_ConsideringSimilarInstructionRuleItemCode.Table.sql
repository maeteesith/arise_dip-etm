USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_ConsideringSimilarInstructionRuleItemCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_ConsideringSimilarInstructionRuleItemCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[parent_code] [nvarchar](50) NULL,
	[index] [int] NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[description] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[value_4] [nvarchar](1000) NULL,
	[value_5] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_ConsidieringSimilarInstructionRuleItemCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ON 

INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, N'ROLE_9', 1, N'1', N'แก้ไขเป็น จำพวก', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-12T14:02:04.5200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, N'ROLE_11', 10, N'10', N'ให้ระบุคำอ่านและคำแปลอักษรในภาษาต่างประเทศที่ปรากฏในเครื่องหมายเป็นภาษาไทย ดังนี้................', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:04.0933333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (11, N'ROLE_11', 11, N'11', N'ให้ระบุคำอ่านอักษรจีนที่ปรากฏในเครื่องหมายเป็นสำเนียงจีนกลางและจีนแต้จิ๋วพร้อมคำแปลเป็นภาษาไทย ดังนี้.....', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:04.6666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (12, N'ROLE_11', 12, N'12', N'ให้ชำระค่าธรรมเนียมรายการสินค้า/บริการเพิ่ม............รายการ เป็นจำนวนเงิน..........บาท ...........', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:05.2400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (13, N'ROLE_11', 13, N'13', N'ให้ชำระค่าธรรมเนียมเพิ่ม จำนวน............บาท เนื่องจากรูปเครื่องหมายมีขนาดเกินกว่าที่กำหนด (5x5 ซม.)', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:05.8166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (14, N'ROLE_11', 14, N'14', N'อื่นๆ...................', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:06.3633333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (15, N'ROLE_17', 15, N'15', N'แสดงเจตนาว่าไม่ขอถือเป็นสิทธิของตนแต่เพียงผู้เดียวที่จะใช้.................เพราะ......', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:14:57.1700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (16, N'ROLE_17', 16, N'16', N'รับรองว่าจะไม่ใช้สีแถบให้เหมือนหรือคล้ายสีธงชาติ หรือสีอื่นที่คล้ายคลึงกับสีดังกล่าว', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:14:57.7566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (17, N'ROLE_17', 17, N'17', N'รับรองว่าจะไม่ใช้รูปกากบาทเป็นสีแดงหรือสีเขียวบนพื้นสีขาวหรือสีเงิน หรือรูปกากบาทสีขาว', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:14:58.3066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, N'ROLE_9', 2, N'2', N'แก้ไข สินค้า หรือ บริการ', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-12T14:02:17.1133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, N'ROLE_11', 3, N'3', N'ให้นำตราสำคัญของบริษัทมาประทับเพื่อประกอบลายมือชื่อกรรมการในแบบ......', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:02:59.9600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, N'ROLE_11', 4, N'4', N'ให้ส่งสำเนาหนังสือมอบอำนาจที่ประทับตราสำคัญของบริษัทครบถ้วนและรับรองสำเนาถูกต้อง', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:00.5400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, N'ROLE_11', 5, N'5', N'ให้ส่งสำเนาหนังสือรับรองนิติบุคคลที่ออกให้ไม่เกิน 6 เดือนของ..... และรับรองสำเนาถูกต้อง', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:01.1433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, N'ROLE_11', 6, N'6', N'ให้ส่งสำเนาบัตรประจำตัวประชาชนหรือบัตรประจำตัวอื่นที่ทางราชการออกให้ของ......และรับรองสำเนาถูกต้อง', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:01.7166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, N'ROLE_11', 7, N'7', N'ให้เปลี่ยนรูปเครื่องหมายใหม่ โดยตัด (R) ออก และไม่อนุญาตให้ขีดฆ่าขูดลบในเครื่องหมายเดิม โดยให้ยื่นแบบ ก.06', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:02.3566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, N'ROLE_11', 8, N'8', N'ให้เปลี่ยนรูปเครื่องหมายใหม่ เนื่องจาก.............โดยให้ยื่นแบบ ก.06', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:02.9100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ([id], [parent_code], [index], [code], [name], [description], [value_1], [value_2], [value_3], [value_4], [value_5], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, N'ROLE_11', 9, N'9', N'ให้ส่งสำเนาหนังสือมอบอำนาจที่มีโนตารีพับลิครับรองลายมือชื่อผู้มอบอำนาจ โดยมีข้อความรับรองความเป็นนิติบุคคล', NULL, N'รายละเอียด', NULL, NULL, NULL, NULL, 0, CAST(N'2020-01-14T10:03:03.4800000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] OFF
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleItemCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleItemCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleItemCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleItemCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_ConsideringSimilarInstructionRuleItemCode] ADD  CONSTRAINT [DF_RM_ConsidieringSimilarInstructionRuleItemCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
