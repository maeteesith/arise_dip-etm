USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_CheckingWordSameConditionCode]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_CheckingWordSameConditionCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_CheckingWordSameConditionCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_CheckingWordSameConditionCode] ON 

INSERT [dbo].[RM_CheckingWordSameConditionCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 1, N'LIKE', N'บางส่วนเหมือน', 0, CAST(N'2019-12-20T01:11:50.1800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingWordSameConditionCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 1, N'LIKE_BACK', N'ส่วนท้ายเหมือน', 0, CAST(N'2019-12-20T01:12:03.8133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingWordSameConditionCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 1, N'LIKE_FRONT', N'ส่วนหน้าเหมือน', 0, CAST(N'2019-12-20T01:11:55.3300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_CheckingWordSameConditionCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'WORD_SAME', N'คำเหมือน', 0, CAST(N'2019-12-20T01:11:42.8300000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_CheckingWordSameConditionCode] OFF
ALTER TABLE [dbo].[RM_CheckingWordSameConditionCode] ADD  CONSTRAINT [DF_RM_CheckingWordSameConditionCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_CheckingWordSameConditionCode] ADD  CONSTRAINT [DF_RM_CheckingWordSameConditionCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_CheckingWordSameConditionCode] ADD  CONSTRAINT [DF_RM_CheckingWordSameConditionCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_CheckingWordSameConditionCode] ADD  CONSTRAINT [DF_RM_CheckingWordSameConditionCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_CheckingWordSameConditionCode] ADD  CONSTRAINT [DF_RM_CheckingWordSameConditionCode_created_by]  DEFAULT ((0)) FOR [created_by]
GO
