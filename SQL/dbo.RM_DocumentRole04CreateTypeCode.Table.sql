USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentRole04CreateTypeCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentRole04CreateTypeCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentRole04CreateTypeCode] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04CreateTypeCode] ON 

INSERT [dbo].[RM_DocumentRole04CreateTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 4, N'CHECKING', N'เสนอนายทะเบียนตรวจสอบ', 0, CAST(N'2020-06-01T17:19:40.9200000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CreateTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'PUBLIC', N'เสนอประกาศโฆษณา', 0, CAST(N'2020-06-01T17:19:23.3700000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CreateTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 3, N'PUBLIC_41', N'เสนอประกาศโฆษณาตามมาตรา 41', 0, CAST(N'2020-06-02T12:09:39.2400000' AS DateTime2), NULL, NULL, NULL)
INSERT [dbo].[RM_DocumentRole04CreateTypeCode] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'RECOVER', N'เสนอฟื้นคำขอ', 0, CAST(N'2020-06-01T17:19:04.9900000' AS DateTime2), NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_DocumentRole04CreateTypeCode] OFF
ALTER TABLE [dbo].[RM_DocumentRole04CreateTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04CreateTypeCode_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentRole04CreateTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04CreateTypeCode_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_DocumentRole04CreateTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04CreateTypeCode_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentRole04CreateTypeCode] ADD  CONSTRAINT [DF_RM_DocumentRole04CreateTypeCode_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
