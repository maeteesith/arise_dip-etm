USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Save010]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Save010](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_id] [bigint] NOT NULL,
	[request_number] [nvarchar](50) NOT NULL,
	[request_date] [date] NULL,
	[make_date] [date] NULL,
	[request_index] [nvarchar](50) NULL,
	[irn_number] [nvarchar](50) NULL,
	[total_price] [decimal](18, 2) NULL,
	[is_save_past] [bit] NULL,
	[request_source_code] [nvarchar](50) NULL,
	[cancel_reason] [nvarchar](500) NULL,
	[save_status_code] [nvarchar](50) NULL,
	[department_send_code] [nvarchar](50) NULL,
	[department_send_date] [date] NULL,
	[consider_similar_document_status_code] [nvarchar](50) NULL,
	[consider_similar_document_date] [date] NULL,
	[consider_similar_document_remark] [nvarchar](1000) NULL,
	[consider_similar_document_item_status_list] [nvarchar](500) NULL,
	[sound_file_id] [bigint] NULL,
	[is_convert_irn] [bit] NULL,
	[irn_reference_number] [nvarchar](500) NULL,
	[request_item_type_code] [nvarchar](50) NULL,
	[commercial_affairs_province_code] [nvarchar](50) NULL,
	[save_otop_type_code] [nvarchar](50) NULL,
	[evidence_address] [nvarchar](500) NULL,
	[otop_reference_number] [nvarchar](500) NULL,
	[request_mark_feature_code_list] [nvarchar](500) NULL,
	[receive_date] [date] NULL,
	[address_country_code] [nvarchar](50) NULL,
	[contact_address_id] [bigint] NULL,
	[trademark_role] [nvarchar](500) NULL,
	[sound_description] [nvarchar](1000) NULL,
	[sound_mark_list] [nvarchar](1000) NULL,
	[saved_process_date] [datetime2](7) NULL,
	[document_status_code] [nvarchar](50) NULL,
	[document_spliter_by] [bigint] NULL,
	[document_classification_by] [bigint] NULL,
	[document_classification_date] [datetime2](7) NULL,
	[document_classification_remark] [nvarchar](1000) NULL,
	[document_classification_status_code] [nvarchar](50) NULL,
	[save010_document_classification_language_code] [nvarchar](50) NULL,
	[checking_type_code] [nvarchar](50) NULL,
	[checking_status_code] [nvarchar](50) NULL,
	[checking_spliter_by] [bigint] NULL,
	[checking_receiver_by] [bigint] NULL,
	[checking_receive_date] [datetime2](7) NULL,
	[checking_receive_status_code] [nvarchar](50) NULL,
	[checking_send_date] [datetime2](7) NULL,
	[checking_remark] [nvarchar](1000) NULL,
	[considering_spliter_by] [bigint] NULL,
	[considering_receiver_by] [bigint] NULL,
	[considering_receive_status_code] [nvarchar](50) NULL,
	[considering_receive_date] [datetime2](7) NULL,
	[considering_receive_remark] [nvarchar](1000) NULL,
	[considering_send_date] [datetime2](7) NULL,
	[considering_reason_send_back] [nvarchar](1000) NULL,
	[considering_remark] [nvarchar](1000) NULL,
	[considering_similar_instruction_type_code] [nvarchar](50) NULL,
	[considering_similar_evidence_27_code] [nvarchar](50) NULL,
	[considering_similar_instruction_description] [nvarchar](1000) NULL,
	[public_source_code] [nvarchar](50) NULL,
	[public_type_code] [nvarchar](50) NULL,
	[public_status_code] [nvarchar](50) NULL,
	[public_spliter_by] [bigint] NULL,
	[public_receiver_by] [bigint] NULL,
	[public_receive_status_code] [nvarchar](50) NULL,
	[public_receive_date] [datetime2](7) NULL,
	[public_send_date] [datetime2](7) NULL,
	[public_receive_do_status_code] [nvarchar](50) NULL,
	[public_round_id] [bigint] NULL,
	[public_page_index] [nvarchar](50) NULL,
	[public_line_index] [nvarchar](50) NULL,
	[public_role02_check_status_code] [nvarchar](50) NULL,
	[public_role02_check_by] [bigint] NULL,
	[public_role02_check_date] [datetime2](7) NULL,
	[public_role04_payment_date] [datetime2](7) NULL,
	[public_role04_status_code] [nvarchar](50) NULL,
	[public_role04_date] [datetime2](7) NULL,
	[public_role04_spliter_by] [bigint] NULL,
	[public_role04_receiver_by] [bigint] NULL,
	[public_role04_receive_status_code] [nvarchar](50) NULL,
	[public_role04_receive_date] [datetime2](7) NULL,
	[public_role05_by] [bigint] NULL,
	[public_role05_status_code] [nvarchar](50) NULL,
	[public_role05_date] [datetime2](7) NULL,
	[floor3_proposer_by] [bigint] NULL,
	[floor3_proposer_date] [datetime2](7) NULL,
	[floor3_registrar_by] [bigint] NULL,
	[floor3_registrar_status_code] [nvarchar](50) NULL,
	[floor3_registrar_date] [datetime2](7) NULL,
	[floor3_registrar_description] [nvarchar](1000) NULL,
	[trademark_expired_start_date] [datetime2](7) NULL,
	[trademark_expired_date] [datetime2](7) NULL,
	[trademark_expired_end_date] [datetime2](7) NULL,
	[public_role04_document_status_code] [nvarchar](50) NULL,
	[public_role04_document_date] [datetime2](7) NULL,
	[public_role02_document_status_code] [nvarchar](50) NULL,
	[public_role02_document_date] [datetime2](7) NULL,
	[registration_number] [nvarchar](50) NULL,
	[trademark_status_code] [nvarchar](50) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[eform_number] [nvarchar](50) NULL,
	[rule_number] [nvarchar](50) NULL,
 CONSTRAINT [PK_Save01] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_request_id1]  DEFAULT ((0)) FOR [contact_address_id]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_checking_spliter_by1_1]  DEFAULT ((0)) FOR [document_spliter_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_checking_receiver_by1_1]  DEFAULT ((0)) FOR [document_classification_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_created_by1]  DEFAULT ((0)) FOR [checking_spliter_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_checker_by1]  DEFAULT ((0)) FOR [checking_receiver_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_created_date1]  DEFAULT (getdate()) FOR [checking_receive_date]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_checking_receive_date1]  DEFAULT (getdate()) FOR [checking_send_date]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_checking_spliter_by1]  DEFAULT ((0)) FOR [considering_spliter_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_checking_receiver_by1]  DEFAULT ((0)) FOR [considering_receiver_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_checking_receive_date1_1]  DEFAULT (getdate()) FOR [considering_receive_date]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_checking_send_date1]  DEFAULT (getdate()) FOR [considering_send_date]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_considering_spliter_by1]  DEFAULT ((0)) FOR [public_spliter_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_considering_receiver_by1]  DEFAULT ((0)) FOR [public_receiver_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_public_do_by1]  DEFAULT ((0)) FOR [public_round_id]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_public_round_id1]  DEFAULT ((0)) FOR [public_page_index]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_public_round_id2]  DEFAULT ((0)) FOR [public_line_index]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_public_receiver_by1_1]  DEFAULT ((0)) FOR [public_role02_check_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_public_spliter_by1]  DEFAULT ((0)) FOR [public_role04_spliter_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_public_receiver_by1]  DEFAULT ((0)) FOR [public_role04_receiver_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_public_role04_receiver_by1]  DEFAULT ((0)) FOR [public_role05_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_floor3_registrar_by1]  DEFAULT ((0)) FOR [floor3_proposer_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_public_role05_by1]  DEFAULT ((0)) FOR [floor3_registrar_by]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save010_trademark_status_code]  DEFAULT (N'P') FOR [trademark_status_code]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save01_is_deleted_1]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save01_created_date_1]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Save010] ADD  CONSTRAINT [DF_Save01_created_by_1]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Save010]  WITH NOCHECK ADD  CONSTRAINT [FK_Save010_PublicRound] FOREIGN KEY([public_round_id])
REFERENCES [dbo].[PublicRound] ([id])
GO
ALTER TABLE [dbo].[Save010] NOCHECK CONSTRAINT [FK_Save010_PublicRound]
GO
ALTER TABLE [dbo].[Save010]  WITH NOCHECK ADD  CONSTRAINT [FK_Save010_RM_DocumentClassificationStatus] FOREIGN KEY([document_classification_status_code])
REFERENCES [dbo].[RM_DocumentClassificationStatus] ([code])
GO
ALTER TABLE [dbo].[Save010] NOCHECK CONSTRAINT [FK_Save010_RM_DocumentClassificationStatus]
GO
ALTER TABLE [dbo].[Save010]  WITH CHECK ADD  CONSTRAINT [FK_Save010_RM_RequestItemType] FOREIGN KEY([request_item_type_code])
REFERENCES [dbo].[RM_RequestItemType] ([code])
GO
ALTER TABLE [dbo].[Save010] CHECK CONSTRAINT [FK_Save010_RM_RequestItemType]
GO
ALTER TABLE [dbo].[Save010]  WITH CHECK ADD  CONSTRAINT [FK_Save010_RM_SaveStatus] FOREIGN KEY([save_status_code])
REFERENCES [dbo].[RM_SaveStatus] ([code])
GO
ALTER TABLE [dbo].[Save010] CHECK CONSTRAINT [FK_Save010_RM_SaveStatus]
GO
