USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[DocumentRole04CreateFile]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocumentRole04CreateFile](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[document_role04_create_id] [bigint] NULL,
	[file_id] [bigint] NULL,
	[document_role04_create_file_type_code] [nvarchar](50) NULL,
	[remark] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_DocumentRole04CreateFile] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DocumentRole04CreateFile] ADD  CONSTRAINT [DF_DocumentRole04CreateFile_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[DocumentRole04CreateFile] ADD  CONSTRAINT [DF_DocumentRole04CreateFile_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[DocumentRole04CreateFile] ADD  CONSTRAINT [DF_DocumentRole04CreateFile_created_by]  DEFAULT ((0)) FOR [created_by]
GO
