USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Department]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Department](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](100) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Department] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Department] ON 

INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (87, 1, N'CHECKING', N'กลุ่มตรวจสอบ', 1, 0, CAST(N'2020-02-26T11:33:22.0266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (86, 1, N'DOCUMENT', N'กลุ่มกำกับการจดทะเบียนและสารบบ', 1, 0, CAST(N'2020-02-26T11:33:22.0200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (97, 1, N'กรรมการและเลขานุการ', N'กรรมการและเลขานุการ', 1, 0, CAST(N'2020-02-26T11:33:22.0466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (88, 1, N'กลุ่มคัดค้าน สคม.', N'กลุ่มคัดค้าน สคม.', 1, 0, CAST(N'2020-02-26T11:33:22.0300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (82, 1, N'กลุ่มงานเปลี่ยนแปลง', N'กลุ่มงานเปลี่ยนแปลง', 1, 0, CAST(N'2020-02-26T11:33:22.0166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (79, 1, N'กลุ่มงานกฎหมาย 2', N'กลุ่มงานกฎหมาย 2', 1, 0, CAST(N'2020-02-26T11:33:22.0100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (80, 1, N'กลุ่มงานกฎหมาย 4 (งานคดี)  ', N'กลุ่มงานกฎหมาย 4 (งานคดี)  ', 1, 0, CAST(N'2020-02-26T11:33:22.0133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (83, 1, N'กลุ่มงานต่ออายุ', N'กลุ่มงานต่ออายุ', 1, 0, CAST(N'2020-02-26T11:33:22.0200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (84, 1, N'กลุ่มงานทะเบียน', N'กลุ่มงานทะเบียน', 1, 0, CAST(N'2020-02-26T11:33:22.0200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (85, 1, N'กลุ่มงานบริการ', N'กลุ่มงานบริการ', 1, 0, CAST(N'2020-02-26T11:33:22.0200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (81, 1, N'กลุ่มงานประกาศโฆษณา', N'กลุ่มงานประกาศโฆษณา', 1, 0, CAST(N'2020-02-26T11:33:22.0166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (96, 1, N'กลุ่มงานรับจดทะเบียน', N'กลุ่มงานรับจดทะเบียน', 1, 0, CAST(N'2020-02-26T11:33:22.0466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (89, 1, N'กลุ่มตรวจสอบ 1 (เคมีภัณฑ์ สินค้าทั่วไป และเครื่องใช้สำนักงาน)', N'กลุ่มตรวจสอบ 1 (เคมีภัณฑ์ สินค้าทั่วไป และเครื่องใช้สำนักงาน)', 1, 0, CAST(N'2020-02-26T11:33:22.0300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (90, 1, N'กลุ่มตรวจสอบ 2 (เครื่องสำอางและยารักษาโรค)', N'กลุ่มตรวจสอบ 2 (เครื่องสำอางและยารักษาโรค)', 1, 0, CAST(N'2020-02-26T11:33:22.0333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (91, 1, N'กลุ่มตรวจสอบ 3 (เครื่องมือ เครื่องจักรกล)', N'กลุ่มตรวจสอบ 3 (เครื่องมือ เครื่องจักรกล)', 1, 0, CAST(N'2020-02-26T11:33:22.0333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (92, 1, N'กลุ่มตรวจสอบ 4 (สิ่งทอ และเครื่องหนัง)', N'กลุ่มตรวจสอบ 4 (สิ่งทอ และเครื่องหนัง)', 1, 0, CAST(N'2020-02-26T11:33:22.0366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (93, 1, N'กลุ่มตรวจสอบ 5 (อาหาร และ ผลิตผลทางการเกษตร)', N'กลุ่มตรวจสอบ 5 (อาหาร และ ผลิตผลทางการเกษตร)', 1, 0, CAST(N'2020-02-26T11:33:22.0400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (94, 1, N'กลุ่มตรวจสอบ 6 (บริการ)', N'กลุ่มตรวจสอบ 6 (บริการ)', 1, 0, CAST(N'2020-02-26T11:33:22.0400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (95, 1, N'กลุ่มตรวจสอบ 7', N'กลุ่มตรวจสอบ 7', 1, 0, CAST(N'2020-02-26T11:33:22.0466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (98, 1, N'กองคลัง', N'กองคลัง', 1, 0, CAST(N'2020-02-26T11:33:22.0500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (101, 1, N'กองส่งเสริมและพัฒนาทรัพย์สินทางปัญญา', N'กองส่งเสริมและพัฒนาทรัพย์สินทางปัญญา', 1, 0, CAST(N'2020-02-26T11:33:22.0533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (100, 1, N'คณะกรรมการ', N'คณะกรรมการ', 1, 0, CAST(N'2020-02-26T11:33:22.0500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (99, 1, N'ธุรการการเงิน(เครื่องหมายการค้า)', N'ธุรการการเงิน(เครื่องหมายการค้า)', 1, 0, CAST(N'2020-02-26T11:33:22.0500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (102, 1, N'สำนักเทคโนโลยีสารสนเทศกรมทรัพย์สินทางปัญญา', N'สำนักเทคโนโลยีสารสนเทศกรมทรัพย์สินทางปัญญา', 1, 0, CAST(N'2020-02-26T11:33:22.0533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Department] ([id], [index], [code], [name], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (103, 1, N'สำนักป้องปรามการละเมิดทรัพย์สินทางปัญญา', N'สำนักป้องปรามการละเมิดทรัพย์สินทางปัญญา', 1, 0, CAST(N'2020-02-26T11:33:22.0566667' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Department] OFF
ALTER TABLE [dbo].[RM_Department] ADD  CONSTRAINT [DF_RM_Department_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Department] ADD  CONSTRAINT [DF_RM_Department_code]  DEFAULT ((1)) FOR [code]
GO
ALTER TABLE [dbo].[RM_Department] ADD  CONSTRAINT [DF_RM_Department_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_Department] ADD  CONSTRAINT [DF_RM_Department_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Department] ADD  CONSTRAINT [DF_RM_Department_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Department] ADD  CONSTRAINT [DF_RM_Department_created_by]  DEFAULT ((0)) FOR [created_by]
GO
