USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vEForm_eForm_Save010_Product]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vEForm_eForm_Save010_Product] as

select 
cast(0 as bigint) id,
cast(0 as bigint) request01_item_id,

e.id eform_id,

p.request_item_sub_type_1_code,
p.product_count,
(case when p.product_count > 5 then 9000 else p.product_count * 1000 end) total_price,

e.is_deleted,
e.created_by,
e.created_date,
e.updated_by,
e.updated_date

from eForm_Save010 e

cross apply (
select p.request_item_sub_type_1_code, count(1) product_count
from eForm_Save010Product p 
where p.save_id = e.id and p.is_deleted = 0
group by p.request_item_sub_type_1_code
) p
--join RM_RequestItemType rm_ri on rm_ri.code = p.save010_mark_type_type_code
GO
