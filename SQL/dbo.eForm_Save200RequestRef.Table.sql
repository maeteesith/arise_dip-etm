USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[eForm_Save200RequestRef]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[eForm_Save200RequestRef](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[rule_number] [nvarchar](50) NULL,
	[save200_request_type_code] [nvarchar](50) NULL,
	[request_ref_id] [bigint] NULL,
	[is_deleted] [bit] NULL,
	[created_date] [datetime2](7) NULL,
	[created_by] [bigint] NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_eForm_Save200RequestRef] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[eForm_Save200RequestRef] ADD  CONSTRAINT [DF_eForm_Save200RequestRef_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[eForm_Save200RequestRef] ADD  CONSTRAINT [DF_eForm_Save200RequestRef_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[eForm_Save200RequestRef] ADD  CONSTRAINT [DF_eForm_Save200RequestRef_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[eForm_Save200RequestRef]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save200RequestRef_eForm_Save200] FOREIGN KEY([save_id])
REFERENCES [dbo].[eForm_Save200] ([id])
GO
ALTER TABLE [dbo].[eForm_Save200RequestRef] CHECK CONSTRAINT [FK_eForm_Save200RequestRef_eForm_Save200]
GO
ALTER TABLE [dbo].[eForm_Save200RequestRef]  WITH CHECK ADD  CONSTRAINT [FK_eForm_Save200RequestRef_RM_Save200RequestTypeCode] FOREIGN KEY([save200_request_type_code])
REFERENCES [dbo].[RM_Save200RequestTypeCode] ([code])
GO
ALTER TABLE [dbo].[eForm_Save200RequestRef] CHECK CONSTRAINT [FK_eForm_Save200RequestRef_RM_Save200RequestTypeCode]
GO
