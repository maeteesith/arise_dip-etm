USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_AddressCareer]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_AddressCareer](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_AddressCareer] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_AddressCareer] ON 

INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (79, 3, N'เจ้าของธุรกิจ', N'เจ้าของธุรกิจ', 0, CAST(N'2020-02-26T11:11:08.6133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (94, 30, N'ให้บริการทางธุรกิจอื่นๆ', N'ให้บริการทางธุรกิจอื่นๆ', 0, CAST(N'2020-02-26T11:11:08.6400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (73, 1, N'ค้าขาย', N'ค้าขาย', 0, CAST(N'2020-02-26T11:11:08.6033333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (76, 2, N'ค้าขายและอุตสาหกรรม', N'ค้าขายและอุตสาหกรรม', 0, CAST(N'2020-02-26T11:11:08.6100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (92, 4, N'ช่างหัตถกรรมและค้าขาย', N'ช่างหัตถกรรมและค้าขาย', 0, CAST(N'2020-02-26T11:11:08.6400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (105, 5, N'ช่างหัตถกรรมและพาณิชยกรรม', N'ช่างหัตถกรรมและพาณิชยกรรม', 0, CAST(N'2020-02-26T11:11:08.6633333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (81, 6, N'ทนายความ', N'ทนายความ', 0, CAST(N'2020-02-26T11:11:08.6166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (83, 7, N'ที่ปรึกษากฎหมาย', N'ที่ปรึกษากฎหมาย', 0, CAST(N'2020-02-26T11:11:08.6166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (85, 8, N'ที่ปรึกษากฎหมายและธุรกิจ', N'ที่ปรึกษากฎหมายและธุรกิจ', 0, CAST(N'2020-02-26T11:11:08.6200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (97, 9, N'ธุรกิจด้านการเงิน', N'ธุรกิจด้านการเงิน', 0, CAST(N'2020-02-26T11:11:08.6500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (95, 10, N'ธุรกิจบริการ', N'ธุรกิจบริการ', 0, CAST(N'2020-02-26T11:11:08.6433333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (82, 11, N'นักกฎหมาย', N'นักกฎหมาย', 0, CAST(N'2020-02-26T11:11:08.6166667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (93, 12, N'นักธุรกิจ', N'นักธุรกิจ', 0, CAST(N'2020-02-26T11:11:08.6400000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (100, 13, N'นักบัญชี', N'นักบัญชี', 0, CAST(N'2020-02-26T11:11:08.6533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (103, 14, N'นักวิชาการอิสระ', N'นักวิชาการอิสระ', 0, CAST(N'2020-02-26T11:11:08.6600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (99, 15, N'บริษัทผู้ถือหุ้นซึ่งคุมหุ้นของกิจการอื่น', N'บริษัทผู้ถือหุ้นซึ่งคุมหุ้นของกิจการอื่น', 0, CAST(N'2020-02-26T11:11:08.6533333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (104, 16, N'บริหารด้านการเงิน', N'บริหารด้านการเงิน', 0, CAST(N'2020-02-26T11:11:08.6600000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (74, 17, N'ประกอบการค้า', N'ประกอบการค้า', 0, CAST(N'2020-02-26T11:11:08.6066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (106, 18, N'ประกอบการหัตถกรรมและค้าขาย', N'ประกอบการหัตถกรรมและค้าขาย', 0, CAST(N'2020-02-26T11:11:08.6666667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (77, 19, N'ประกอบกิจการธนาคาร', N'ประกอบกิจการธนาคาร', 0, CAST(N'2020-02-26T11:11:08.6100000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (78, 20, N'ประกอบธุรกิจจัดสรรที่ดิน', N'ประกอบธุรกิจจัดสรรที่ดิน', 0, CAST(N'2020-02-26T11:11:08.6133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (96, 21, N'ประกอบธุรกิจบริการและค้าขาย', N'ประกอบธุรกิจบริการและค้าขาย', 0, CAST(N'2020-02-26T11:11:08.6466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (98, 22, N'ประกอบธุรกิจหลักทรัพย์และธุรกิจที่เกี่ยว', N'ประกอบธุรกิจหลักทรัพย์และธุรกิจที่เกี่ยว', 0, CAST(N'2020-02-26T11:11:08.6500000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (84, 23, N'ประกอบอาชีพทางกฎหมาย', N'ประกอบอาชีพทางกฎหมาย', 0, CAST(N'2020-02-26T11:11:08.6200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (75, 24, N'ผลิตและค้าขาย', N'ผลิตและค้าขาย', 0, CAST(N'2020-02-26T11:11:08.6066667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (88, 25, N'พาณิชยกรรม', N'พาณิชยกรรม', 0, CAST(N'2020-02-26T11:11:08.6233333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (89, 26, N'พาณิชยกรรมและบริการ', N'พาณิชยกรรมและบริการ', 0, CAST(N'2020-02-26T11:11:08.6266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (80, 27, N'รับจ้าง', N'รับจ้าง', 0, CAST(N'2020-02-26T11:11:08.6133333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (90, 28, N'รับราชการ', N'รับราชการ', 0, CAST(N'2020-02-26T11:11:08.6266667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (91, 29, N'หัตถกรรม', N'หัตถกรรม', 0, CAST(N'2020-02-26T11:11:08.6366667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (101, 31, N'องค์กรด้านมนุษยธรรมที่ไม่แสวงหากำไร', N'องค์กรด้านมนุษยธรรมที่ไม่แสวงหากำไร', 0, CAST(N'2020-02-26T11:11:08.6566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (102, 32, N'อื่นๆ', N'อื่นๆ', 0, CAST(N'2020-02-26T11:11:08.6566667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (86, 33, N'อุตสาหกรรม', N'อุตสาหกรรม', 0, CAST(N'2020-02-26T11:11:08.6200000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_AddressCareer] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (87, 34, N'อุตสาหกรรมและพาณิชยกรรม', N'อุตสาหกรรมและพาณิชยกรรม', 0, CAST(N'2020-02-26T11:11:08.6233333' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_AddressCareer] OFF
ALTER TABLE [dbo].[RM_AddressCareer] ADD  CONSTRAINT [DF_RM_AddressCareer_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_AddressCareer] ADD  CONSTRAINT [DF_RM_AddressCareer_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_AddressCareer] ADD  CONSTRAINT [DF_RM_AddressCareer_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_AddressCareer] ADD  CONSTRAINT [DF_RM_AddressCareer_created_by]  DEFAULT ((0)) FOR [created_by]
GO
