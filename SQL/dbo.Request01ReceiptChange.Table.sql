USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[Request01ReceiptChange]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Request01ReceiptChange](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[request_id] [bigint] NULL,
	[request01_receipt_change_code] [nvarchar](50) NULL,
	[request_approve_date] [datetime2](7) NULL,
	[request_approve_by] [bigint] NULL,
	[receipt_approve_date] [datetime2](7) NULL,
	[receipt_approve_by] [bigint] NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
	[remark] [nvarchar](max) NULL,
 CONSTRAINT [PK_Request01ReceiptChange] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[Request01ReceiptChange] ADD  CONSTRAINT [DF_Request01ReceiptChange_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[Request01ReceiptChange] ADD  CONSTRAINT [DF_Request01ReceiptChange_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[Request01ReceiptChange] ADD  CONSTRAINT [DF_Request01ReceiptChange_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[Request01ReceiptChange]  WITH CHECK ADD  CONSTRAINT [FK_Request01ReceiptChange_RM_Request01ReceiptChangeCode] FOREIGN KEY([request01_receipt_change_code])
REFERENCES [dbo].[RM_Request01ReceiptChangeCode] ([code])
GO
ALTER TABLE [dbo].[Request01ReceiptChange] CHECK CONSTRAINT [FK_Request01ReceiptChange_RM_Request01ReceiptChangeCode]
GO
ALTER TABLE [dbo].[Request01ReceiptChange]  WITH CHECK ADD  CONSTRAINT [FK_Request01ReceiptChange_UM_User] FOREIGN KEY([receipt_approve_by])
REFERENCES [dbo].[UM_User] ([id])
GO
ALTER TABLE [dbo].[Request01ReceiptChange] CHECK CONSTRAINT [FK_Request01ReceiptChange_UM_User]
GO
ALTER TABLE [dbo].[Request01ReceiptChange]  WITH CHECK ADD  CONSTRAINT [FK_Request01ReceiptChange_UM_User1] FOREIGN KEY([request_approve_by])
REFERENCES [dbo].[UM_User] ([id])
GO
ALTER TABLE [dbo].[Request01ReceiptChange] CHECK CONSTRAINT [FK_Request01ReceiptChange_UM_User1]
GO
