USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_Save050PeriodAddressType]    Script Date: 6/23/2020 1:39:07 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_Save050PeriodAddressType](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save050PeriodAddressType] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_Save050PeriodAddressType] ON 

INSERT [dbo].[RM_Save050PeriodAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (10, 7, N'OTHERS', N'อื่นๆ', 0, CAST(N'2020-04-27T05:46:16.3700000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050PeriodAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (1, 1, N'RECEIVER', N'ผู้ได้รับอนุญาต', 0, CAST(N'2020-04-27T05:16:12.0333333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050PeriodAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (7, 4, N'RECEIVER_PERIOD', N'ผู้ได้รับอนุญาตช่วง', 0, CAST(N'2020-04-27T05:17:31.7300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050PeriodAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (8, 5, N'RECEIVER_PERIOD_REPRESENTATIVE', N'ตัวแทนผู้ได้รับอนุญาตช่วง', 0, CAST(N'2020-04-27T05:18:14.4300000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050PeriodAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (9, 6, N'RECEIVER_PERIOD_REPRESENTATIVE_PERIOD', N'ตัวแทนช่วงผู้ได้รับอนุญาตช่วง', 0, CAST(N'2020-04-27T05:46:04.6933333' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050PeriodAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 2, N'RECEIVER_REPRESENTATIVE', N'ตัวแทนผู้ได้รับอนุญาต', 0, CAST(N'2020-04-27T05:17:00.1800000' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_Save050PeriodAddressType] ([id], [index], [code], [name], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (6, 3, N'RECEIVER_REPRESENTATIVE_PERIOD', N'ตัวแทนช่วงผู้ได้รับอนุญาต', 0, CAST(N'2020-04-27T05:17:24.0000000' AS DateTime2), 0, NULL, NULL)
SET IDENTITY_INSERT [dbo].[RM_Save050PeriodAddressType] OFF
ALTER TABLE [dbo].[RM_Save050PeriodAddressType] ADD  CONSTRAINT [DF_RM_Save050PeriodAddressType_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_Save050PeriodAddressType] ADD  CONSTRAINT [DF_RM_Save050PeriodAddressType_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_Save050PeriodAddressType] ADD  CONSTRAINT [DF_RM_Save050PeriodAddressType_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_Save050PeriodAddressType] ADD  CONSTRAINT [DF_RM_Save050PeriodAddressType_created_by]  DEFAULT ((0)) FOR [created_by]
GO
