USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[dSave010DocumentClassificationSearchMethod]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dSave010DocumentClassificationSearchMethod](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[save_id] [bigint] NULL,
	[search_method] [nvarchar](50) NULL,
	[keyword_1] [nvarchar](500) NULL,
	[keyword_2] [nvarchar](500) NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_Save010DocumentClassificationSearchMethod] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[dSave010DocumentClassificationSearchMethod] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSearchMethod_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[dSave010DocumentClassificationSearchMethod] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSearchMethod_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[dSave010DocumentClassificationSearchMethod] ADD  CONSTRAINT [DF_RM_Save010DocumentClassificationSearchMethod_created_by]  DEFAULT ((0)) FOR [created_by]
GO
ALTER TABLE [dbo].[dSave010DocumentClassificationSearchMethod]  WITH CHECK ADD  CONSTRAINT [FK_Save010DocumentClassificationSearchMethod_Save010] FOREIGN KEY([save_id])
REFERENCES [dbo].[Save010] ([id])
GO
ALTER TABLE [dbo].[dSave010DocumentClassificationSearchMethod] CHECK CONSTRAINT [FK_Save010DocumentClassificationSearchMethod_Save010]
GO
