USE [DIPTM_dev]
GO
/****** Object:  View [dbo].[vFloor3Document]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vFloor3Document] as
select

s.id,

s.request_number,
s.request_item_type_code,
rm_rit.name request_item_type_name,

(select top 1 s010p.name from Save010AddressPeople s010p where s010p.save_id = s.id) name,

s.registration_number,
pr.book_index,
s.public_page_index,

s.is_deleted,
s.created_by,
s.created_date,
s.updated_by,
s.updated_date

from Save010 s
join RM_RequestItemType rm_rit on rm_rit.code = s.request_item_type_code
join PublicRound pr on pr.id = s.public_round_id
GO
