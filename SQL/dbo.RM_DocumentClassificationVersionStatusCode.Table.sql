USE [DIPTM_dev]
GO
/****** Object:  Table [dbo].[RM_DocumentClassificationVersionStatusCode]    Script Date: 6/23/2020 1:39:06 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RM_DocumentClassificationVersionStatusCode](
	[id] [bigint] IDENTITY(1,1) NOT NULL,
	[index] [int] NOT NULL,
	[code] [nvarchar](50) NOT NULL,
	[name] [nvarchar](1000) NULL,
	[value_1] [nvarchar](1000) NULL,
	[value_2] [nvarchar](1000) NULL,
	[value_3] [nvarchar](1000) NULL,
	[is_showed] [bit] NOT NULL,
	[is_deleted] [bit] NOT NULL,
	[created_date] [datetime2](7) NOT NULL,
	[created_by] [bigint] NOT NULL,
	[updated_date] [datetime2](7) NULL,
	[updated_by] [bigint] NULL,
 CONSTRAINT [PK_RM_DocumentClassificationVersionStatus] PRIMARY KEY CLUSTERED 
(
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[RM_DocumentClassificationVersionStatusCode] ON 

INSERT [dbo].[RM_DocumentClassificationVersionStatusCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (5, 5, N'DELETE', N'ไม่อนุญาต', NULL, NULL, NULL, 1, 0, CAST(N'2020-06-12T23:14:10.4466667' AS DateTime2), 0, NULL, NULL)
INSERT [dbo].[RM_DocumentClassificationVersionStatusCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (4, 6, N'SEND', N'บันทึกแล้ว', NULL, NULL, NULL, 1, 0, CAST(N'2019-12-17T05:40:42.8333333' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_DocumentClassificationVersionStatusCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (2, 2, N'WAIT_DO', N'รอดำเนินการ', NULL, NULL, NULL, 1, 0, CAST(N'2019-12-17T05:40:38.1800000' AS DateTime2), 1, NULL, 1)
INSERT [dbo].[RM_DocumentClassificationVersionStatusCode] ([id], [index], [code], [name], [value_1], [value_2], [value_3], [is_showed], [is_deleted], [created_date], [created_by], [updated_date], [updated_by]) VALUES (3, 4, N'WAIT_SEND', N'บันทึกชั่วคราว', NULL, NULL, NULL, 1, 0, CAST(N'2019-12-17T05:40:41.2000000' AS DateTime2), 1, NULL, 1)
SET IDENTITY_INSERT [dbo].[RM_DocumentClassificationVersionStatusCode] OFF
ALTER TABLE [dbo].[RM_DocumentClassificationVersionStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationVersionStatus_index]  DEFAULT ((1)) FOR [index]
GO
ALTER TABLE [dbo].[RM_DocumentClassificationVersionStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationVersionStatus_is_showed]  DEFAULT ((1)) FOR [is_showed]
GO
ALTER TABLE [dbo].[RM_DocumentClassificationVersionStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationVersionStatus_is_deleted]  DEFAULT ((0)) FOR [is_deleted]
GO
ALTER TABLE [dbo].[RM_DocumentClassificationVersionStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationVersionStatus_created_date]  DEFAULT (getdate()) FOR [created_date]
GO
ALTER TABLE [dbo].[RM_DocumentClassificationVersionStatusCode] ADD  CONSTRAINT [DF_RM_DocumentClassificationVersionStatus_created_by]  DEFAULT ((0)) FOR [created_by]
GO
